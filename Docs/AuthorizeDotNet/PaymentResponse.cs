﻿namespace Squeezed.WebApplication.Utilities.Sales.AuthorizePaymentGateway
{
	public class PaymentResponse
	{
		public AuthorizeNet.IGatewayResponse Response { get; set; }
		public bool Approved { get; set; }
		public string ErrorMessage { get; set; }
		public string SubscriptionId { get; set; }
	}
}