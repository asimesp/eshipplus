﻿using System;
using System.Collections.Generic;
using AuthorizeNet;
using Squeezed.WebApplication.Core;

namespace Squeezed.WebApplication.Utilities.Sales.AuthorizePaymentGateway
{
	public class Authorize
	{
		public string CardNumber { get; set; }
		public int ExpMonth { get; set; }
		public int ExpYear { get; set; }
		public string SecurityCode { get; set; }
		public double ChargeAmount { get; set; }
		public string CustomerEmail { get; set; }
		public bool EmailCustomer { get; set; }
		public string BillingFirstName { get; set; }
		public string BillingLastName { get; set; }
		public string BillingAddress { get; set; }
		public string BillingZip { get; set; }
		public string BillingCity { get; set; }
		public string BillingState { get; set; }
		public string BillingPhone { get; set; }
		public string BillingFax { get; set; }
		public string BillingCompany { get; set; }
		public string BillingCountry { get; set; }
		public string ShippingFirstName { get; set; }
		public string ShippingLastName { get; set; }
		public string ShippingAddress { get; set; }
		public string ShippingZip { get; set; }
		public string ShippingCity { get; set; }
		public string ShippingState { get; set; }
		public string ShippingPhone { get; set; }
		public string ShippingCompany { get; set; }
		public string ShippingCountry { get; set; }
		public string InvoiceNumber { get; set; }
		public string OrderDescription { get; set; }
		public Gateway Gate { get; set; }
		public SubscriptionGateway ArbGate { get; set; }
		public CustomerGateway CustGate { get; set; }
		public bool TestMode { get; set; }

		public List<AuthorizeLineItem> LineItems { get; set; }

		public Authorize()
		{
			LineItems = new List<AuthorizeLineItem>();
		}

		public Authorize(string apiLogin, string transactionKey, string cardNumber, int expMonth, int expYear, string securityCode,
		           double chargeAmount, string customerEmail, bool emailCustomer, string billingFirstName,
		           string billingLastName, string billingAddress, string billingZip, string billingCity, string billingState,
		           string billingPhone, string billingCompany, string billingCountry, string billingFax,
		           string shippingCompany,
		           string shippingFirstName, string shippingLastName, string shippingAddress, string shippingZip,
		           string shippingCity, string shippingState, string shippingPhone, string shippingCountry,
		           string invoiceNumber, string orderDescription,

		           List<AuthorizeLineItem> lineItems, bool testMode = false)
		{
			CardNumber = cardNumber;
			ExpMonth = expMonth;
			ExpYear = expYear;
			SecurityCode = securityCode;
			ChargeAmount = chargeAmount;

			BillingFirstName = billingFirstName;
			BillingLastName = billingLastName;
			BillingAddress = billingAddress;
			BillingZip = billingZip;
			BillingCity = billingCity;
			BillingState = billingState;
			BillingPhone = billingPhone;
			BillingFax = billingFax;
			BillingCompany = billingCompany;
			BillingCountry = billingCountry;

			ShippingFirstName = shippingFirstName;
			ShippingLastName = shippingLastName;
			ShippingAddress = shippingAddress;
			ShippingZip = shippingZip;
			ShippingCity = shippingCity;
			ShippingState = shippingState;
			ShippingPhone = shippingPhone;
			ShippingCompany = shippingCompany;
			ShippingCountry = shippingCountry;

			InvoiceNumber = invoiceNumber;

			OrderDescription = orderDescription;

			CustomerEmail = customerEmail;
			EmailCustomer = emailCustomer;
			TestMode = testMode;

			Gate = new Gateway(apiLogin, transactionKey, TestMode);
			if ((TestMode))
			{
				ArbGate = new SubscriptionGateway(apiLogin, transactionKey, ServiceMode.Test);
				CustGate = new CustomerGateway(apiLogin, transactionKey, ServiceMode.Test);
			}
			else
			{
				ArbGate = new SubscriptionGateway(apiLogin, transactionKey, ServiceMode.Live);
				CustGate = new CustomerGateway(apiLogin, transactionKey, ServiceMode.Live);
			}

			LineItems = lineItems;

		}

		public PaymentResponse ChargeCard()
		{
			return ProcessTransaction();
		}

		private PaymentResponse ProcessTransaction()
		{
            LoggingUtils.AddMessage("ProcessCreditCard", "Process Transaction");
			try
			{
                LoggingUtils.AddMessage("ProcessCreditCard", "Try Entered");
				var request = new AuthorizationRequest(CardNumber, ExpMonth.ToString() + ExpYear.ToString(), ChargeAmount.ToDecimal(), OrderDescription);
				request.AddCardCode(SecurityCode);
				request.AddCustomer(InvoiceNumber, CustomerEmail, BillingFirstName, BillingLastName, BillingAddress, BillingCity, BillingState, BillingZip);
				request.Company = BillingCompany;
				request.City = BillingCity;
				request.Email = CustomerEmail;
				request.Phone = BillingPhone;
				request.Country = BillingCountry;
				request.Fax = BillingFax;
				request.EmailCustomer = (EmailCustomer) ? "TRUE" : "FALSE";

				LineItems.ForEach(x => request.AddLineItem(x.Id, x.Name, x.Description, x.Quantity, x.Price, x.Taxable));

				request.ShipToFirstName = ShippingFirstName;
				request.ShipToLastName = ShippingLastName;
				request.ShipToCompany = ShippingCompany;
				request.ShipToAddress = ShippingAddress;
				request.ShipToCity = ShippingCity;
				request.ShipToState = ShippingState;
				request.ShipToZip = ShippingZip;
				request.ShipToCountry = ShippingCountry;

				request.InvoiceNum = InvoiceNumber;

				request.DuplicateWindow = "10";

				request.TestRequest = (TestMode) ? "TRUE" : "FALSE";

                LoggingUtils.AddMessage("ProcessCreditCard", "Gateway send request");
                LoggingUtils.AddMessage("ProcessCreditCard", "Request: " + request.ToString());
				var response = Gate.Send(request);

				var retVal = new PaymentResponse {Response = response};

                LoggingUtils.AddMessage("ProcessCreditCard", "Read Response");
				if ((response.ResponseCode == "1"))
				{
					retVal.Approved = true;
					retVal.ErrorMessage = string.Empty;
					LoggingUtils.AddMessage("ProcessCreditCard", "Response Read == 1");
				}
				else
				{
					retVal.Approved = false;
					retVal.ErrorMessage = response.Message;
					LoggingUtils.AddMessage("ProcessCreditCard", "Response Read != 1");
				}

				return retVal;
			}
			catch (Exception ex)
			{
				var retVal = new PaymentResponse {Approved = false, Response = null, ErrorMessage = ex.Message};
				return retVal;
			}
		}

	}
}