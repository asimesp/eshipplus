﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using AuthorizeNet;
using Squeezed.WebApplication.Core;
using Squeezed.WebApplication.Core.Sales;
using Squeezed.WebApplication.Core.Sales.Search;
using Customer = AuthorizeNet.Customer;

namespace Squeezed.WebApplication.Utilities.Sales.AuthorizePaymentGateway
{
	public class AuthorizeCustomer
	{
		private readonly CustomerGateway _gateway;
		private readonly bool _testMode;

		public AuthorizeCustomer()
		{
			var login = WebApplicationSettings.AuthorizeLogin;
			var key = WebApplicationSettings.AuthorizeTransactionKey;
			var testMode = WebApplicationSettings.AuthorizeTestMode;

			_testMode = testMode;
			_gateway = (_testMode)
				           ? new CustomerGateway(login, key, ServiceMode.Test)
				           : new CustomerGateway(login, key, ServiceMode.Live);
		}

		public string AddCustomerInformation(int customerId)
		{
			var profileId = string.Empty;

			var cust = new Core.Sales.Customer(customerId);
			if (cust.KeyLoaded)
			{
				profileId =
					_gateway.CreateCustomer(cust.Email, cust.FirstName + " " + cust.LastName, customerId.ToString()).ProfileID;
				cust.CIMID = profileId;
				cust.Save();
			}
			return profileId;
		}

        public string AddCustomerInformation(Core.Sales.Customer cust)
        {
            var profileId = string.Empty;

            if (cust.IsNew)
            {
				//TODO - refactor - this needs changed! shouldn't be saving the customer in here!
				cust.Save();
                profileId =
                    _gateway.CreateCustomer(cust.Email, cust.FirstName + " " + cust.LastName, cust.Id.GetString()).ProfileID;
                cust.CIMID = profileId;
                cust.Save();
            }
            return profileId;
        }

		public void AddAllCustomers()
		{
			var customers = new CustomerSearch().FetchCustomers();
			foreach (var customer in customers.Where(c => string.IsNullOrEmpty(c.CIMID)))
			{
				customer.CIMID = _gateway
					.CreateCustomer(customer.Email, customer.FirstName + " " + customer.LastName, customer.Id.ToString())
					.ProfileID;
				customer.Save();
			}
		}

		public void RemoveAllCustomers()
		{
			var customers = new CustomerSearch().FetchCustomers();
			foreach (var customer in customers.Where(c => !string.IsNullOrEmpty(c.CIMID)))
			{
				customer.CIMID = _gateway
					.CreateCustomer(customer.Email, customer.FirstName + " " + customer.LastName, customer.Id.ToString())
					.ProfileID;
				_gateway.DeleteCustomer(customer.CIMID);
				customer.CIMID = string.Empty;
				customer.Save();
			}
		}

		// Does not charge the card, just determines if payment method is valid
		public IGatewayResponse Authorize(string profileId, string paymentProfileId, decimal amount)
		{
			return _gateway.Authorize(profileId, paymentProfileId, amount);
		}

		// Charges the card only if valid
		public IGatewayResponse AuthorizeAndCapture(string profileId, string paymentProfileId, decimal amount)
		{
			return _gateway.AuthorizeAndCapture(profileId, paymentProfileId, amount);
		}

		// Charges a previously approved card (Authorize function must be run first to get a transactionID)
		public IGatewayResponse PriorAuthorizationAndCapture(string transId, decimal amount)
		{
			return _gateway.PriorAuthCapture(transId, amount);
		}

		public bool RemoveCustomerInformation(int customerId)
		{
			var customer = new Core.Sales.Customer(customerId);
			if (customer.KeyLoaded)
			{
				var profileId = customer.CIMID;

				var cust = GetCustomer(profileId);
				foreach (var profile in cust.PaymentProfiles)
				{
					_gateway.DeletePaymentProfile(profileId, profile.ProfileID);
				}

				customer.CIMID = string.Empty;
				customer.Save();

				return _gateway.DeleteCustomer(profileId);
			}

			return false;
		}

		// Gets primary credit card on file for customer or, if no primary card is specified, gets the first card
		public PaymentProfile GetPaymentProfile(int recurringOrderId)
		{
			PaymentProfile paymentProfile = null;

			var order = new RecurringOrder(recurringOrderId);
			if (order.KeyLoaded)
			{
				var customer = GetCustomer(order.CimId);
				paymentProfile = customer.PaymentProfiles.FirstOrDefault(p => p.ProfileID == order.PaymentProfileId);
			}

			return paymentProfile;
		}

        // Gets all credit cards on file for customer
        public List<ListItem> GetPaymentProfiles(string CimId)
        {
            List<PaymentProfile> paymentProfiles = null;

            var customer = GetCustomer(CimId);
            paymentProfiles = customer.PaymentProfiles.ToList();

            List<ListItem> pProfiles = new List<ListItem>();

            foreach(PaymentProfile p in paymentProfiles)
            {
                pProfiles.Add(new ListItem() { Value = p.ProfileID, Text = p.CardNumber });
            }

            return pProfiles;
        }


		public Customer GetCustomer(string profileId)
		{
			return _gateway.GetCustomer(profileId);
		}

		public Customer GetCustomerByCustomerId(int customerId)
		{
			var customer = new Core.Sales.Customer(customerId);
			return customer.KeyLoaded ? GetCustomer(customer.CIMID) : null;
		}

		public string AddPaymentProfile(int customerId, string cardNumber, int expirationMonth, int expirationYear)
		{
			var paymentProfileId = string.Empty;

			if (cardNumber.Length > 4)
			{
				try
				{
					var customer = new Core.Sales.Customer(customerId);

					if (customer.KeyLoaded)
					{
						var cust = GetCustomer(customer.CIMID);
						var profile = cust
							.PaymentProfiles
							.FirstOrDefault(p => p.CardNumber.EndsWith(cardNumber.Substring(cardNumber.Length - 4)));
						paymentProfileId = profile == null
							                   ? _gateway.AddCreditCard(customer.CIMID, cardNumber, expirationMonth, expirationYear)
							                   : profile.ProfileID;
					}
				}
				catch (Exception)
				{
				}
			}

			return paymentProfileId;
		}

		private string GetExistingPaymentProfile(string cimId, string cardNumber)
		{
			var paymentProfileId = string.Empty;

			try
			{
				var cust = GetCustomer(cimId);
				var profile = cust
					.PaymentProfiles
					.FirstOrDefault(p => p.CardNumber.EndsWith(cardNumber.Substring(cardNumber.Length - 4)));
				if (profile != null) paymentProfileId = profile.ProfileID;

			}
			catch (Exception)
			{
			}

			return paymentProfileId;
		}

		public string AddPaymentProfileByCimId(string cimId, string cardNumber, int expirationMonth, int expirationYear,
		                                       string cardCode, string billingStreet, string billingCity, string billingState,
		                                       string billingZip, string firstName,
		                                       string lastName)
		{
            LoggingUtils.AddMessage("Authorize Customer", "Check for existing ");
			var paymentProfileId = GetExistingPaymentProfile(cimId, cardNumber);
			if (!string.IsNullOrWhiteSpace(paymentProfileId)) return paymentProfileId;

			var cleanCardNumber = cardNumber.Replace(" ", string.Empty);
			var address = new Address
				{
					City = billingCity,
					Street = billingStreet,
					State = billingState,
					Zip = billingZip,
					First = firstName,
					Last = lastName
				};
            LoggingUtils.AddMessage("Authorize Customer", "Gateway Call");
            LoggingUtils.AddMessage("Authorize Customer", "Values: " + cimId + ":" + cleanCardNumber + ":" + expirationMonth + ":" + expirationYear + ":" + cardCode + ":" + address.ToString());
			return _gateway.AddCreditCard(cimId, cleanCardNumber, expirationMonth, expirationYear, cardCode, address);
		}

		public bool RemovePaymentProfile(int recurringOrderId)
		{
			var order = new RecurringOrder(recurringOrderId);
			return order.KeyLoaded && RemovePaymentProfile(order);
		}

		public bool RemovePaymentProfile(RecurringOrder order)
		{
			return _gateway.DeletePaymentProfile(order.CimId, order.PaymentProfileId);
		}

		public void RemoveAllPaymentProfilesForCustomer(int customerId, List<string> exclusionList = null)
		{
			var customer = new Core.Sales.Customer(customerId);
			if (!customer.KeyLoaded) return;

			var cust = GetCustomer(customer.CIMID);
			if (exclusionList == null) exclusionList = new List<string>();

			foreach (var profile in cust.PaymentProfiles.Where(p => !exclusionList.Contains(p.ProfileID)))
			{
				_gateway.DeletePaymentProfile(cust.ProfileID, profile.ProfileID);
			}
		}

		public bool UpdatePaymentProfile(int recurringOrderId, string cardNumber, string cardCode, int expirationMonth,
		                                 int expirationYear)
		{
			var order = new RecurringOrder(recurringOrderId);

			if (order.KeyLoaded)
			{
				_gateway.DeletePaymentProfile(order.CimId, order.PaymentProfileId);
				order.PaymentProfileId = string.Empty;

				try
				{
					order.PaymentProfileId = _gateway.AddCreditCard(order.CimId, cardNumber, expirationMonth, expirationYear);
				}
				catch (Exception)
				{
					return false;
				}

				if (!string.IsNullOrWhiteSpace(order.PaymentProfileId)) order.Save();
			}

			return true;
		}
	}



}