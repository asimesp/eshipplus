Current Documentation:
https://test.p-44.com/docs/apidocs.html# (Username: logisticsplus_testing@p-44.com Password: Logplus2019!)

Alternatively to view the documentation file ("p44.json") offline, load it into Swagger:

https://editor.swagger.io/

settings:
    <!-- Project 44 Settings -->
    <add key="Project44Host" value="https://test.p-44.com" />
    <add key="Project44Username" value="logisticsplus_testing@p-44.com" />
    <add key="Project44Password" value="project44!" />

    <add key="Project44PrimaryLocationId" value="a62b8e6f-100a-495a-95f9-7ee1f39573ed" />
    <add key="EnableProject44RateProcessor" value="false" />
    <!-- P44 PROD Settings
    <add key="Project44Host" value="https://cloud.p-44.com" />
    <add key="Project44Username" value="logisticsplus.prod@p-44.com" />
    <add key="Project44Password" value="Logisticsplus2018!" />