declare @latA float = 0
declare @latB float = 0
declare @longA float = 0
declare @longB float = 0
declare @UseKilometers int = 0


select 
		2.0*
			(case when @UseKilometers = 1 
				then 6370.97327862273 -- earth radius in kilometers
				else 3958.73926185 -- earth radius in miles
			end)
			*asin(sqrt((power(sin(radians((
			@latB -- lat B
			-
			@latA -- lat A
			)/2.0)),2))	+ (cos(radians(
			@latA -- lat A
			))*cos(radians(
			@latB -- lat B
			))*power(sin(radians((
			@longB -- long B
			-
			@longA -- long A
			)/2.0)),2))))