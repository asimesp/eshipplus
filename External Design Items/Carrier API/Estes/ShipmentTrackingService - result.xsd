<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:ship="http://ws.estesexpress.com/schema/2012/12/shipmenttracking" attributeFormDefault="unqualified" elementFormDefault="qualified" targetNamespace="http://ws.estesexpress.com/schema/2012/12/shipmenttracking" xml:lang="en">
<xsd:annotation>
<xsd:documentation xml:lang="en">Shipment tracking results. Copyright 2012 Estes Express Lines, Inc.</xsd:documentation>
</xsd:annotation>
<xsd:include schemaLocation="ShipmentTrackingService?xsd=shipmentTrackingCommon.xsd"/>
<xsd:element name="trackingInfo">
<xsd:complexType>
<xsd:sequence>
<xsd:element ref="ship:requestID"/>
<xsd:element name="shipments" type="ship:ShipmentsType"/>
</xsd:sequence>
</xsd:complexType>
</xsd:element>
<xsd:simpleType name="AddressLineType">
<xsd:restriction base="xsd:string">
<xsd:minLength value="1"/>
<xsd:maxLength value="30"/>
</xsd:restriction>
</xsd:simpleType>
<xsd:complexType name="AddressType">
<xsd:sequence>
<xsd:element name="line1" type="ship:AddressLineType"/>
<xsd:element minOccurs="0" name="line2" type="ship:AddressLineType"/>
<xsd:element name="city" type="ship:CityType"/>
<xsd:element name="stateProvince" type="ship:StateProvinceType"/>
<xsd:element name="postalCode" type="ship:PostalCodeType"/>
<xsd:element minOccurs="0" name="countryCode" type="ship:CountryCodeType"/>
</xsd:sequence>
</xsd:complexType>
<xsd:complexType name="AppointmentType">
<xsd:sequence>
<xsd:element name="apptDate" type="xsd:date"/>
<xsd:element minOccurs="0" name="apptTime" type="xsd:time"/>
<xsd:element minOccurs="0" name="status" type="ship:StatusType"/>
</xsd:sequence>
</xsd:complexType>
<xsd:simpleType name="CarrierType">
<xsd:restriction base="xsd:string">
<xsd:minLength value="1"/>
<xsd:maxLength value="20"/>
</xsd:restriction>
</xsd:simpleType>
<xsd:simpleType name="ChargesType">
<xsd:restriction base="xsd:decimal">
<xsd:totalDigits value="7"/>
<xsd:fractionDigits value="2"/>
</xsd:restriction>
</xsd:simpleType>
<xsd:simpleType name="CityType">
<xsd:restriction base="xsd:string">
<xsd:minLength value="1"/>
<xsd:maxLength value="20"/>
</xsd:restriction>
</xsd:simpleType>
<xsd:simpleType name="CompanyNameType">
<xsd:restriction base="xsd:string">
<xsd:minLength value="1"/>
<xsd:maxLength value="30"/>
</xsd:restriction>
</xsd:simpleType>
<xsd:complexType name="CompanyType">
<xsd:annotation>
<xsd:documentation>Company information</xsd:documentation>
</xsd:annotation>
<xsd:sequence>
<xsd:element minOccurs="0" name="referenceNumber" type="ship:ReferenceNumberType"/>
<xsd:element name="name" type="ship:CompanyNameType"/>
<xsd:element name="address" type="ship:AddressType"/>
</xsd:sequence>
</xsd:complexType>
<xsd:simpleType name="CountryCodeType">
<xsd:restriction base="xsd:string">
<xsd:minLength value="2"/>
<xsd:maxLength value="2"/>
</xsd:restriction>
</xsd:simpleType>
<xsd:complexType name="InterlineInfoType">
<xsd:annotation>
<xsd:documentation>Interline carrier information</xsd:documentation>
</xsd:annotation>
<xsd:sequence>
<xsd:element name="freightBill" type="ship:InterlineProType"/>
<xsd:element name="scac" type="ship:ScacType"/>
<xsd:element minOccurs="0" name="name" type="ship:CompanyNameType"/>
<xsd:element name="type" type="ship:CarrierType"/>
</xsd:sequence>
</xsd:complexType>
<xsd:simpleType name="MessageType">
<xsd:restriction base="xsd:string">
<xsd:maxLength value="200"/>
</xsd:restriction>
</xsd:simpleType>
<xsd:complexType name="MessagesType">
<xsd:annotation>
<xsd:documentation>List of messages</xsd:documentation>
</xsd:annotation>
<xsd:sequence>
<xsd:element maxOccurs="unbounded" name="message" type="ship:MessageType"/>
</xsd:sequence>
</xsd:complexType>
<xsd:complexType name="MovementType">
<xsd:sequence>
<xsd:element name="movementDate" type="xsd:date"/>
<xsd:element name="movementTime" type="xsd:time"/>
<xsd:element name="message" type="ship:MessageType"/>
</xsd:sequence>
</xsd:complexType>
<xsd:complexType name="MovementHistoryType">
<xsd:annotation>
<xsd:documentation>Shipment movement history</xsd:documentation>
</xsd:annotation>
<xsd:sequence>
<xsd:element maxOccurs="unbounded" name="message" type="ship:MovementType"/>
</xsd:sequence>
</xsd:complexType>
<xsd:simpleType name="PersonNameType">
<xsd:restriction base="xsd:string">
<xsd:minLength value="1"/>
<xsd:maxLength value="50"/>
</xsd:restriction>
</xsd:simpleType>
<xsd:simpleType name="PiecesType">
<xsd:restriction base="xsd:unsignedInt">
<xsd:minInclusive value="1"/>
<xsd:totalDigits value="6"/>
</xsd:restriction>
</xsd:simpleType>
<xsd:simpleType name="PhoneAreaCodeType">
<xsd:restriction base="xsd:unsignedInt">
<xsd:minInclusive value="1"/>
<xsd:maxInclusive value="99999"/>
</xsd:restriction>
</xsd:simpleType>
<xsd:simpleType name="PhoneCountryCodeType">
<xsd:restriction base="xsd:unsignedShort">
<xsd:minInclusive value="1"/>
<xsd:maxInclusive value="999"/>
</xsd:restriction>
</xsd:simpleType>
<xsd:simpleType name="PhoneSubscriberType">
<xsd:annotation>
<xsd:documentation>Telephone subscriber number - up to 14 digits</xsd:documentation>
</xsd:annotation>
<xsd:restriction base="xsd:unsignedLong">
<xsd:minInclusive value="1"/>
<xsd:maxInclusive value="99999999999999"/>
</xsd:restriction>
</xsd:simpleType>
<xsd:complexType name="PhoneType">
<xsd:annotation>
<xsd:documentation>Full international telephone number</xsd:documentation>
</xsd:annotation>
<xsd:sequence>
<xsd:element minOccurs="0" name="country" type="ship:PhoneCountryCodeType"/>
<xsd:element minOccurs="0" name="areaCode" type="ship:PhoneAreaCodeType"/>
<xsd:element name="subscriber" type="ship:PhoneSubscriberType"/>
<xsd:element minOccurs="0" name="extension" type="xsd:nonNegativeInteger"/>
</xsd:sequence>
</xsd:complexType>
<xsd:simpleType name="PostalCodeType">
<xsd:restriction base="xsd:string">
<xsd:minLength value="5"/>
<xsd:maxLength value="6"/>
</xsd:restriction>
</xsd:simpleType>
<xsd:simpleType name="ReferenceNumberType">
<xsd:restriction base="xsd:string">
<xsd:minLength value="1"/>
<xsd:maxLength value="35"/>
</xsd:restriction>
</xsd:simpleType>
<xsd:simpleType name="ScacType">
<xsd:restriction base="xsd:string">
<xsd:minLength value="1"/>
<xsd:maxLength value="4"/>
</xsd:restriction>
</xsd:simpleType>
<xsd:simpleType name="ServiceType">
<xsd:annotation>
<xsd:documentation>Estes service level</xsd:documentation>
</xsd:annotation>
<xsd:restriction base="xsd:string">
<xsd:maxLength value="30"/>
</xsd:restriction>
</xsd:simpleType>
<xsd:complexType name="ShipmentType">
<xsd:sequence>
<xsd:element name="pro" type="ship:ProType"/>
<xsd:element minOccurs="0" name="bol" type="ship:BolType"/>
<xsd:element minOccurs="0" name="po" type="ship:PoType"/>
<xsd:element minOccurs="0" name="pickupDate" type="xsd:date"/>
<xsd:element minOccurs="0" name="service" type="ship:ServiceType"/>
<xsd:element name="status" type="ship:StatusType"/>
<xsd:element minOccurs="0" name="movementHistory" type="ship:MovementHistoryType"/>
<xsd:element minOccurs="0" name="firstDeliveryDate" type="xsd:date"/>
<xsd:element minOccurs="0" name="estimatedDeliveryDate" type="xsd:date"/>
<xsd:element minOccurs="0" name="estimatedDeliveryTime" type="xsd:time"/>
<xsd:element minOccurs="0" name="deliveryDate" type="xsd:date"/>
<xsd:element minOccurs="0" name="deliveryTime" type="xsd:time"/>
<xsd:element minOccurs="0" name="receivedBy" type="ship:PersonNameType"/>
<xsd:element minOccurs="0" name="appointment" type="ship:AppointmentType"/>
<xsd:element minOccurs="0" name="pieces" type="ship:PiecesType"/>
<xsd:element minOccurs="0" name="dimensionalWeight" type="ship:WeightType"/>
<xsd:element minOccurs="0" name="weight" type="ship:WeightType"/>
<xsd:element minOccurs="0" name="shipper" type="ship:CompanyType"/>
<xsd:element minOccurs="0" name="consignee" type="ship:CompanyType"/>
<xsd:element minOccurs="0" name="thirdParty" type="ship:CompanyType"/>
<xsd:element name="destinationTerminal" type="ship:TerminalType"/>
<xsd:element minOccurs="0" name="interlineInfo" type="ship:InterlineInfoType"/>
<xsd:element minOccurs="0" name="freightCharges" type="ship:ChargesType"/>
<xsd:element minOccurs="0" name="messages" type="ship:MessagesType"/>
</xsd:sequence>
</xsd:complexType>
<xsd:complexType name="ShipmentsType">
<xsd:annotation>
<xsd:documentation>List of PROs</xsd:documentation>
</xsd:annotation>
<xsd:sequence>
<xsd:element maxOccurs="unbounded" name="shipment" type="ship:ShipmentType"/>
</xsd:sequence>
</xsd:complexType>
<xsd:simpleType name="StateProvinceType">
<xsd:annotation>
<xsd:documentation>State or province abbreviation</xsd:documentation>
</xsd:annotation>
<xsd:restriction base="xsd:string">
<xsd:minLength value="2"/>
<xsd:maxLength value="2"/>
</xsd:restriction>
</xsd:simpleType>
<xsd:simpleType name="StatusType">
<xsd:restriction base="xsd:string">
<xsd:maxLength value="30"/>
</xsd:restriction>
</xsd:simpleType>
<xsd:simpleType name="TerminalNameType">
<xsd:restriction base="xsd:string">
<xsd:minLength value="1"/>
<xsd:maxLength value="40"/>
</xsd:restriction>
</xsd:simpleType>
<xsd:simpleType name="TerminalNumberType">
<xsd:restriction base="xsd:unsignedShort">
<xsd:minInclusive value="1"/>
<xsd:totalDigits value="3"/>
</xsd:restriction>
</xsd:simpleType>
<xsd:complexType name="TerminalType">
<xsd:sequence>
<xsd:element name="number" type="ship:TerminalNumberType"/>
<xsd:element name="name" type="ship:TerminalNameType"/>
<xsd:element name="address" type="ship:AddressType"/>
<xsd:element name="phone" type="ship:PhoneType"/>
<xsd:element name="fax" type="ship:PhoneType"/>
</xsd:sequence>
</xsd:complexType>
<xsd:simpleType name="WeightType">
<xsd:restriction base="xsd:unsignedInt">
<xsd:minInclusive value="1"/>
<xsd:totalDigits value="7"/>
</xsd:restriction>
</xsd:simpleType>
</xsd:schema>
