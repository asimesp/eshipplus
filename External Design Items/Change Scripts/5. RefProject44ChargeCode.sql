USE [PacmanDevelopment]
GO

/****** Object:  Table [dbo].[RefProject44ChargeCode]    Script Date: 3/12/2019 2:02:58 PM ******/
DROP TABLE [dbo].[RefProject44ChargeCode]
GO

/****** Object:  Table [dbo].[RefProject44ChargeCode]    Script Date: 3/12/2019 2:02:58 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RefProject44ChargeCode](
	[Project44ChargeCodeIndex] [int] NOT NULL,
	[Project44ChargeCodeText] [nvarchar](50) NOT NULL,
	[Project44ChargeCodeDescription] [nvarchar](500) NOT NULL,
 CONSTRAINT [PK_RefProject44Code] PRIMARY KEY CLUSTERED 
(
	[Project44ChargeCodeIndex] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


