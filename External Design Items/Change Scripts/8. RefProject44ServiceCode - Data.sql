INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(0,'AIRPU','Airport Pickup');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(1,'APPTPU','Pickup Appointment');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(2,'CAMPPU','Camp Pickup');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(3,'CHRCPU','Church Pickup');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(4,'CLUBPU','Country Club Pickup');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(5,'CNVPU','Convention/Tradeshow Pickup');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(6,'CONPU','Construction Site Pickup');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(7,'DOCKPU','Dock Pickup');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(8,'EDUPU','School Pickup');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(9,'FARMPU','Farm Pickup');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(10,'GROPU','Grocery Warehouse Pickup');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(11,'HOSPU','Hospital Pickup');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(12,'HOTLPU','Hotel Pickup');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(13,'INPU','Inside Pickup');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(14,'LGPU','Liftgate Pickup');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(15,'LTDPU','Limited Access Pickup');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(16,'MILPU','Military Installation Pickup');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(17,'MINEPU','Mine Site Pickup');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(18,'NARPU','Native American Reservation Pickup');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(19,'PARKPU','Fair/Amusement/Park Pickup');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(20,'PIERPU','Pier Pickup');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(21,'PRISPU','Prison Pickup');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(22,'RESPU','Residential Pickup');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(23,'SATPU','Saturday Pickup');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(24,'SORTPU','Sort/Segregate Pickup');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(25,'SSTORPU','Self-Storage Pickup');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(26,'AIRDEL','Airport Delivery');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(27,'APPT','Delivery Appointment');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(28,'APPTDEL','Delivery Appointment');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(29,'CAMPDEL','Camp Delivery');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(30,'CHRCDEL','Church Delivery');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(31,'CLUBDEL','Country Club Delivery');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(32,'CNVDEL','Convention/Tradeshow Delivery');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(33,'CONDEL','Construction Site Delivery');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(34,'DCDEL','Distribution Center Delivery');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(35,'EDUDEL','School Delivery');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(36,'FARMDEL','Farm Delivery');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(37,'GOVDEL','Government Site Delivery');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(38,'GRODEL','Grocery Warehouse Delivery');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(39,'HOSDEL','Hospital Delivery');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(40,'HOTLDEL','Hotel Delivery');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(41,'INDEL','Inside Delivery');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(42,'INEDEL','Inside Delivery With Elevator');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(43,'INGDEL','Inside Delivery Ground Floor');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(44,'INNEDEL','Inside Delivery No Elevator');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(45,'LGDEL','Liftgate Delivery');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(46,'LTDDEL','Limited Access Delivery');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(47,'MALLDEL','Mall Delivery');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(48,'MILDEL','Military Installation Delivery');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(49,'MINEDEL','Mine Site Delivery');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(50,'NBDEL','Non-Business Hours Delivery');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(51,'NCDEL','Non-Commercial Delivery');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(52,'NOTIFY','Delivery Notification');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(53,'PARKDEL','Fair/Amusement/Park Delivery');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(54,'PIERDEL','Pier Delivery');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(55,'PRISDEL','Prison Delivery');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(56,'RESDEL','Residential Delivery');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(57,'RSRTDEL','Resort Delivery');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(58,'SATDEL','Saturday Delivery');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(59,'SORTDEL','Sort/Segregate Delivery');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(60,'SSTORDEL','Self-Storage Delivery');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(61,'UNLOADDEL','Unload at Destination');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(62,'WEDEL','Weekend Delivery');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(63,'ACCELERATED','Expedite Shipment Non-Guaranteed');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(64,'ARCHR','Arbitrary Charge');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(65,'BLIND','Blind Shipment');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(66,'CANFEE','Northbound Canadian Border Crossing Document Handling Fee');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(67,'CAPLOAD','Capacity Load');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(68,'COD','COD');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(69,'CREDEL','Redeliver to Consignee');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(70,'DRAY','Drayage');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(71,'ELS','Excessive Length Shipment');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(72,'ELS12','Excessive Length, Over 12ft');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(73,'ELS12_20','Excessive Length, 12 to 20ft');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(74,'ELS12_24','Excessive Length, 12 to 24ft');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(75,'ELS13_27','Excessive Length, 13 to 27ft');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(76,'ELS14','Excessive Length, Over 14ft');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(77,'ELS14_24','Excessive Length, 14 to 24ft');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(78,'ELS14_26','Excessive Length, 14 to 26ft');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(79,'ELS15','Excessive Length, Over 15ft');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(80,'ELS15_27','Excessive Length, 15 to 27ft');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(81,'ELS20','Excessive Length, Over 20ft');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(82,'ELS20_28','Excessive Length, 20 to 28ft');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(83,'ELS21','Excessive Length, Over 21ft');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(84,'ELS24','Excessive Length, Over 24ft');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(85,'ELS27','Excessive Length, Over 27ft');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(86,'ELS28','Excessive Length, Over 28ft');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(87,'ELS30','Excessive Length, Over 30ft');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(88,'ELS8_12','Excessive Length, 8 to 12ft');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(89,'EV','Excessive value');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(90,'EXPEDITE','Expedite Shipment');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(91,'EXPEDITE17','Expedite before 5pm');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(92,'EXPEDITEAM','Expedite before 12pm');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(93,'EXPORT','Export Shipment');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(94,'FRZABLE','Freezable');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(95,'GS10','Guaranteed by 10AM');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(96,'GS11','Guaranteed by 11AM');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(97,'GS14','Guaranteed by 2PM');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(98,'GS15','Guaranteed by 3PM');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(99,'GS1530','Guaranteed by 3:30PM');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(100,'GS8','Guaranteed by 8AM');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(101,'GS9','Guaranteed by 9AM');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(102,'GSAM','Guaranteed by AM (Noon)');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(103,'GSMUL','Guaranteed Multi-Hour Window');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(104,'GUR','Guaranteed');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(105,'GURWIN','Guaranteed Within Window');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(106,'HAWAII','Hawaiian Will Call');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(107,'HAZM','Hazmat');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(108,'IMPORT','Import Shipment');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(109,'LUMPER','Lumper Service');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(110,'MAINTEMP','Maintain Temperature');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(111,'MARKING','Marking or Tagging');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(112,'O750U6','Over 750, Under 6');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(113,'OVDIM','Over Dimension Freight');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(114,'PFH','Protect From Heat');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(115,'PFZ','Protect From Freezing');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(116,'POISON','Poison (Hazmat B)');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(117,'RECON','Reconsignment Fee');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(118,'SINGSHIP','Single Shipment');
INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(119,'WINSPC','Weight Inspection');
