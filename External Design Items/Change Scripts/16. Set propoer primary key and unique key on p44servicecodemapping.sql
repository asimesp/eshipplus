/*
   Thursday, March 14, 20195:47:10 PM
   User: 
   Server: devserver01\sql2012_2
   Database: PacmanDevelopment
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.P44ServiceMapping
	DROP CONSTRAINT PK_P44ServiceMapping
GO
ALTER TABLE dbo.P44ServiceMapping ADD CONSTRAINT
	PK_P44ServiceMapping PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE UNIQUE NONCLUSTERED INDEX IX_P44ServiceMapping ON dbo.P44ServiceMapping
	(
	Project44Code,
	TenantId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.P44ServiceMapping SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
