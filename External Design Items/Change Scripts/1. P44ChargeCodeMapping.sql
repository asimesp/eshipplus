USE [PacmanDevelopment]
GO

ALTER TABLE [dbo].[P44ChargeCodeMapping] DROP CONSTRAINT [FK_P44ChargeCodeMapping_Tenant]
GO

ALTER TABLE [dbo].[P44ChargeCodeMapping] DROP CONSTRAINT [FK_P44ChargeCodeMapping_ChargeCode]
GO

ALTER TABLE [dbo].[P44ChargeCodeMapping] DROP CONSTRAINT [DF__P44Charge__Proje__6E17FC74]
GO

/****** Object:  Table [dbo].[P44ChargeCodeMapping]    Script Date: 3/12/2019 3:22:29 PM ******/
DROP TABLE [dbo].[P44ChargeCodeMapping]
GO

/****** Object:  Table [dbo].[P44ChargeCodeMapping]    Script Date: 3/12/2019 3:22:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[P44ChargeCodeMapping](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Project44Code] [int] NOT NULL,
	[ChargeCodeId] [bigint] NOT NULL,
	[TenantId] [bigint] NOT NULL,
 CONSTRAINT [PK_P44ChargeCodeMapping] PRIMARY KEY CLUSTERED 
(
	[Project44Code] ASC,
	[ChargeCodeId] ASC,
	[TenantId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[P44ChargeCodeMapping] ADD  CONSTRAINT [DF__P44Charge__Proje__6E17FC74]  DEFAULT ((0)) FOR [Project44Code]
GO

ALTER TABLE [dbo].[P44ChargeCodeMapping]  WITH CHECK ADD  CONSTRAINT [FK_P44ChargeCodeMapping_ChargeCode] FOREIGN KEY([ChargeCodeId])
REFERENCES [dbo].[ChargeCode] ([Id])
GO

ALTER TABLE [dbo].[P44ChargeCodeMapping] CHECK CONSTRAINT [FK_P44ChargeCodeMapping_ChargeCode]
GO

ALTER TABLE [dbo].[P44ChargeCodeMapping]  WITH CHECK ADD  CONSTRAINT [FK_P44ChargeCodeMapping_Tenant] FOREIGN KEY([TenantId])
REFERENCES [dbo].[Tenant] ([Id])
GO

ALTER TABLE [dbo].[P44ChargeCodeMapping] CHECK CONSTRAINT [FK_P44ChargeCodeMapping_Tenant]
GO


