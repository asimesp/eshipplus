/*
   Tuesday, March 12, 20193:10:31 PM
   User: developmentUser
   Server: devserver01\Sql2012_2
   Database: PacmanDevelopment
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.SearchProfileDefault ADD
	RateAndScheduleProfiles nvarchar(MAX) NOT NULL CONSTRAINT DF_SearchProfileDefault_RateAndScheduleProfiles DEFAULT ''
GO
ALTER TABLE dbo.SearchProfileDefault SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
