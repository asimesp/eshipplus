USE [PacmanDevelopment]
GO
/****** Object:  UserDefinedFunction [dbo].[ChargeCodeIdUsageCount]    Script Date: 3/13/2019 3:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[ChargeCodeIdUsageCount]
(
	@ChargeCodeId bigint
)
RETURNS bigint
AS
BEGIN
	declare @count bigint

	select @count = (
		(select COUNT(*) from AverageWeeklyFuel where ChargeCodeId = @ChargeCodeId) +
		(select COUNT(*) from BatchRateData where ChargeCodeId = @ChargeCodeId) +
		(select COUNT(*) from CustomerRating where InsuranceChargeCodeId = @ChargeCodeId) +
		(select COUNT(*) from CustomerRating where TLFreightChargeCodeId = @ChargeCodeId) +
		(select COUNT(*) from CustomerRating where TLFuelChargeCodeId = @ChargeCodeId) +
		(select COUNT(*) from CustomerChargeCodeMap where ChargeCodeId = @ChargeCodeId)+
		(select COUNT(*) from DiscountTier where FreightChargeCodeId = @ChargeCodeId) +
		(select COUNT(*) from InvoiceDetail where ChargeCodeId = @ChargeCodeId) +
		(select COUNT(*) from LoadOrderCharge where ChargeCodeId = @ChargeCodeId) +
		(select COUNT(*) from LTLAdditionalCharge where ChargeCodeId = @ChargeCodeId) +
		(select COUNT(*) from LTLOverLengthRule where ChargeCodeId = @ChargeCodeId) +
		(select COUNT(*) from P44ChargeCodeMapping where ChargeCodeId = @ChargeCodeId)+
		(select COUNT(*) from [Service] where ChargeCodeId = @ChargeCodeId) +
		(select COUNT(*) from ServiceTicketCharge where ChargeCodeId = @ChargeCodeId) +
		(select COUNT(*) from SmallPackRate where ChargeCodeId = @ChargeCodeId) +
		(select COUNT(*) from ShipmentCharge where ChargeCodeId = @ChargeCodeId) +
		(select COUNT(*) from Tenant where MacroPointChargeCodeId = @ChargeCodeId) +
		(select COUNT(*) from VendorRating where FuelChargeCodeId = @ChargeCodeId) +
		(select COUNT(*) from Tenant where DefaultUnmappedChargeCodeId = @ChargeCodeId)
	)

	return @count

END