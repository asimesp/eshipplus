USE [PacmanDevelopment]
GO
/****** Object:  UserDefinedFunction [dbo].[ServiceIdUsageCount]    Script Date: 3/13/2019 3:05:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Christopher Oyesiku
-- Create date: November 15 2011	Modified: May 28 2014 By: Christopher Oyesiku
-- Description:	Returns usage of ServiceId across database tables
-- Updates
--		2015-05-28	Pending Vendor Service Id check
-- =============================================
ALTER FUNCTION [dbo].[ServiceIdUsageCount]
(
	@ServiceId bigint
)
RETURNS bigint
AS
BEGIN
	declare @count bigint

	select @count = (
		(select COUNT(*) from AddressBookService where ServiceId = @ServiceId) +
		(select COUNT(*) from CustomerServiceMarkup where ServiceId = @ServiceId) +
		(select COUNT(*) from LoadOrderService where ServiceId = @ServiceId) +
		(select COUNT(*) from LTLAccessorial where ServiceId = @ServiceId) +
		(select COUNT(*) from P44ServiceMapping where ServiceId = @ServiceId)+ 
		(select COUNT(*) from PendingVendorService where ServiceId = @ServiceId) +
		(select COUNT(*) from SmallPackageServiceMap where ServiceId = @ServiceId)+
		(select COUNT(*) from ShipmentService where ServiceId = @ServiceId) +
		(select COUNT(*) from ShoppedRateService where ServiceId = @ServiceId) + 
		(select COUNT(*) from Tenant where BorderCrossingServiceId = @ServiceId) +
		(select COUNT(*) from Tenant where HazardousMaterialServiceId = @ServiceId) +
		(select COUNT(*) from VendorService where ServiceId = @ServiceId)		
	)

	return @count

END
