ALTER TABLE P44ChargeCodeMapping
DROP CONSTRAINT PK_P44ChargeCodeMapping;

ALTER TABLE P44ChargeCodeMapping
ADD CONSTRAINT PK_P44ChargeCodeMapping PRIMARY KEY (Project44Code,ChargeCodeId,TenantId);