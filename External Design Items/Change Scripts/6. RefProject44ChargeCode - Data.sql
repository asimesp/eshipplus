INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(0,'ACC','Accessorial Charge');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(1,'ACCELERATED','Accelerated');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(2,'ADD_CHRG','Additional Charge');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(3,'ADVCOL','Advanced Collection');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(4,'AFSC','Air Fuel Surcharge');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(5,'AFTRHR','After Hours Charge');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(6,'AFTRHRDEL','After Hours Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(7,'AIR','Air Freight');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(8,'AIRBAG','Air Bag');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(9,'AIRDEL','Airport Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(10,'AIRPU','Airport Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(11,'APPT','Delivery Appointment');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(12,'APPTDEL','Delivery Appointment');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(13,'APPTPU','Pickup Appointment');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(14,'ARCHR','Arbitrary Charge');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(15,'ASWGHT','Charged as Weight');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(16,'BAGLINER','Bag Liner');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(17,'BLIND','Blind Shipment');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(18,'BOBTAIL','Bobtail');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(19,'BOND','In Bond Shipment');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(20,'BORDCROSS','Border Crossing Fee');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(21,'BULKHEAD','Bulkhead');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(22,'BYDIND','Beyond-Indirect Service');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(23,'CAMPDEL','Camp Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(24,'CAMPPU','Camp Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(25,'CANFEE','Northbound Canadian Border Crossing Document Handling Fee');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(26,'CAPLOAD','Capacity Load');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(27,'CAPLOADPUP','Capacity Load Pup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(28,'CAPLOADTRA','Capacity Load Trailer');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(29,'CARBFEE','CARB Compliance Fee');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(30,'CERT','Certified Pickup fee');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(31,'CERTPU','Certified Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(32,'CHASSIS','Chassis Truck');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(33,'CHNGBOL','Change of BOL');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(34,'CHRCDEL','Church Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(35,'CHRCPU','Church Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(36,'CLUBDEL','Country Club Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(37,'CLUBPU','Country Club Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(38,'CNTER20','Container 20 Feet Long');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(39,'CNTER40','Container 40 Feet Long');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(40,'CNTER40H','Container 40H Feet Long');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(41,'CNTER45','Container 45 Feet Long');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(42,'CNTER53','Container 53 Feet Long');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(43,'CNV','Convention/Tradeshow Pickup or Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(44,'CNVDEL','Convention/Tradeshow Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(45,'CNVPU','Convention/Tradeshow Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(46,'COD','COD');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(47,'COMDEL','Commercial Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(48,'COMPU','Commercial Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(49,'CONDEL','Construction Site Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(50,'CONPU','Construction Site Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(51,'CONS','Construction Site Pickup or Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(52,'CONSOL','Consolidation');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(53,'CORBOL','Corrected BOL');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(54,'CORFEE','Correction/Verification Fee');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(55,'CRANE','Crane Permits');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(56,'CREDEL','Redeliver to Consignee');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(57,'CROSS','Cross Dock');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(58,'CUSINBOND','Customs or In-Bond Freight charge');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(59,'DCDEL','Distribution Center Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(60,'DECVAL','Declared Value Pickup or Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(61,'DEF','Deficit Charge');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(62,'DELBY','Delivery By Charge');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(63,'DELC','Delivery Charge');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(64,'DELON','Delivery On Charge');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(65,'DESCINS','Description Inspection');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(66,'DET','Detention');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(67,'DIMWGHT','Dimensional Weight');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(68,'DLYCHRG','Delay Charge');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(69,'DOCFEE','Document Fee');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(70,'DOCKDEL','Dock Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(71,'DOCKPU','Dock Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(72,'DRASST','Driver Assistance');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(73,'DRAY','Drayage');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(74,'DROPPULL','Drop Pull');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(75,'DRYRU','Dry Run');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(76,'DSC','Discount');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(77,'DVALDEL','Declared Value Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(78,'DVALPU','Declared Value Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(79,'EDU','School Pickup or Delviery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(80,'EDUDEL','School Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(81,'EDUPU','School Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(82,'ELS','Excessive Length Shipment');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(83,'ELS12','Excessive Length, Over 12ft');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(84,'ELS12_20','Excessive Length, 12 to 20ft');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(85,'ELS12_24','Excessive Length, 12 to 24ft');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(86,'ELS13_27','Excessive Length, 13 to 27ft');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(87,'ELS14','Excessive Length, Over 14ft');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(88,'ELS14_24','Excessive Length, 14 to 24ft');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(89,'ELS14_26','Excessive Length, 14 to 26ft');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(90,'ELS15','Excessive Length, Over 15ft');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(91,'ELS15_27','Excessive Length, 15 to 27ft');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(92,'ELS20','Excessive Length, Over 20ft');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(93,'ELS20_28','Excessive Length, 20 to 28ft');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(94,'ELS21','Excessive Length, Over 21ft');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(95,'ELS24','Excessive Length, Over 24ft');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(96,'ELS27','Excessive Length, Over 27ft');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(97,'ELS28','Excessive Length, Over 28ft');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(98,'ELS30','Excessive Length, Over 30ft');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(99,'ELS8_12','Excessive Length, 8 to 12ft');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(100,'EMERGENCYDEL','Emergency Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(101,'EV','Excessive value');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(102,'EXCHG','Pallet Exchange Pickup or Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(103,'EXLIAB','Excess Liability Coverage Charge');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(104,'EXPEDITE','Expedite Shipment');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(105,'EXPEDITE17','Expedite before 5pm');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(106,'EXPEDITEAM','Expedite before 12pm');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(107,'EXPORT','Export Shipment');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(108,'FARM','Farm Pickup or Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(109,'FARMDEL','Farm Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(110,'FARMPU','Farm Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(111,'FBEDDEL','Flatbed Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(112,'FBEDPU','Flatbed Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(113,'FERRY','Ferry Charge');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(114,'FINSUR','Full Value Insurance Charge');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(115,'FLTTRCK','Flat Track');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(116,'FOOD','Shipment Contains Food');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(117,'FORK','Forklift');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(118,'FRIDGE','Refrigerate');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(119,'FRZABLE','Freezable');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(120,'FSC','Fuel Surcharge');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(121,'GFC','Gross Freight Charge');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(122,'GOV','Government Site Pickup or Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(123,'GOVDEL','Government Site Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(124,'GOVPU','Government Site Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(125,'GRO','Grocery Warehouse Pickup or Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(126,'GRODEL','Grocery Warehouse Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(127,'GROPU','Grocery Warehouse Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(128,'GS8','Guaranteed by 8AM');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(129,'GS9','Guaranteed by 9AM');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(130,'GS10','Guaranteed by 10AM');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(131,'GS11','Guaranteed by 11AM');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(132,'GSAM','Guaranteed by AM (Noon)');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(133,'GS14','Guaranteed by 2PM');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(134,'GS15','Guaranteed by 3PM');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(135,'GS1530','Guaranteed by 3:30PM');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(136,'GUR','Guaranteed');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(137,'GSFM','Guaranteed Friday to Monday');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(138,'GSMUL','Guaranteed Multi-Hour Window');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(139,'GSSING','Guaranteed Single-Hour Window');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(140,'GST','Goods and Services Tax');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(141,'GURWIN','Guaranteed Within Window');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(142,'HAWAII','Hawaiian Will Call');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(143,'HAZM','Hazmat');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(144,'HCD','High Cost Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(145,'HCP','High Cost Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(146,'HCSER','High Cost Service Area');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(147,'HDAYDEL','Holiday Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(148,'HDAYPU','Holiday Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(149,'HEAT','Heat');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(150,'HIGHDOCK','High Dock Equipment Needed');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(151,'HMLAND','HomeLand Security');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(152,'HOLIWEEK','Holiday/Weekend Service');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(153,'HOSDEL','Hospital Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(154,'HOSPU','Hospital Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(155,'HOTLDEL','Hotel Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(156,'HOTLPU','Hotel Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(157,'HST','Harmonized Sales Tax');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(158,'IMPORT','Import Shipment');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(159,'INBND','Inbound Freight');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(160,'INDEL','Inside Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(161,'INEDEL','Inside Delivery With Elevator');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(162,'INGDEL','Inside Delivery Ground Floor');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(163,'INNEDEL','Inside Delivery No Elevator');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(164,'INPU','Inside Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(165,'INS','Inside Pickup or Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(166,'INT','Interline Shipment');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(167,'IREG','Inter-Regional Shipment');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(168,'ITEM','Item Charge');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(169,'LABEL','Handling and Labeling');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(170,'LGDEL','Liftgate Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(171,'LGPU','Liftgate Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(172,'LGSER','Liftgate Pickup or Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(173,'LH','Linehaul');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(174,'LIQUOR','Liquor Permit');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(175,'LOAD','Driver Load');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(176,'LOADCNT','Driver Load and Count');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(177,'LOADUNLOAD','Driver Load/Unload');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(178,'LTDACC','Limited Access Pickup or Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(179,'LTDDEL','Limited Access Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(180,'LTDPU','Limited Access Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(181,'LUMPER','Lumper Service');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(182,'LYOVR','Layover');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(183,'MAINTEMP','Maintain Temperature');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(184,'MALL','Mall Pickup or Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(185,'MALLDEL','Mall Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(186,'MALLPU','Mall Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(187,'MARKING','Marking or Tagging');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(188,'MILDEL','Military Installation Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(189,'MILPU','Military Installation Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(190,'MIN','Minimum Charge');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(191,'MINEDEL','Mine Site Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(192,'MINEPU','Mine Site Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(193,'NARDEL','Native American Reservation Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(194,'NARPU','Native American Reservation Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(195,'NBDEL','Non-Business Hours Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(196,'NBPU','Non-Business Hours Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(197,'NCDEL','Non-Commercial Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(198,'NCPU','Non-Commercial Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(199,'NCSER','Non-Commercial Pickup or Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(200,'NFC','Net Freight Charge');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(201,'NOTIFY','Notify Before Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(202,'NURSDEL','Nursing Home Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(203,'NURSPU','Nursing Home Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(204,'O750U6','Over 750, Under 6');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(205,'OCEAN','Ocean Charge');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(206,'OFSC','Ocean Fuel Surcharge');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(207,'OILSANDS','Oil Sands');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(208,'OPENTOP','Open Top Truck');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(209,'OVDIM','Over Dimension Freight');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(210,'OVERWGHT','Overweight');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(211,'OVR_UNDR','Over Weight Under Feet');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(212,'OVSZEDEL','Oversized Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(213,'OVSZEPU','Oversized Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(214,'PALLET','Pallet');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(215,'PAPER','Paper Invoice Fee');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(216,'PARKDEL','Fair/Amusement/Park Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(217,'PARKPU','Fair/Amusement/Park Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(218,'PFH','Protect From Heat');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(219,'PFZ','Protect From Freezing');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(220,'PFZC','Protect From Freezing Canada');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(221,'PIERDEL','Pier Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(222,'PIERPU','Pier Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(223,'PJACK','Pallet Jack Pickup or Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(224,'PJACKDEL','Pallet Jack Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(225,'PJACKPU','Pallet Jack Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(226,'PKGFEE','Packaging Fee');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(227,'POISON','Poison (Hazmat B)');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(228,'PORTCON','Port Congestion Charge');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(229,'PRISDEL','Prison Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(230,'PRISON','Prison Pickup or Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(231,'PRISPU','Prison Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(232,'PRMIT','Permit');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(233,'PROTECT','Protective Service');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(234,'PROTECTDRM','Protective Drum Cover');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(235,'PROTECTPAL','Protective Pallet Cover');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(236,'PST','Provencial Sales Tax');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(237,'PUC','Pickup Charge');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(238,'QST','Quebec Sales Tax');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(239,'RECON','Reconsignment Fee');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(240,'REDEL','Redelivery Fee');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(241,'REEFER','Refrigerated Van');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(242,'REG','Regional Shipment');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(243,'RES','Residential Pickup or Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(244,'RESDEL','Residential Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(245,'RESPU','Residential Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(246,'RMVDEB','Remove Debris');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(247,'RSRTDEL','Resort Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(248,'RSRTPU','Resort Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(249,'SATDEL','Saturday Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(250,'SATPU','Saturday Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(251,'SECINS','Security Inspection');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(252,'SHIPDIV','Secure Shipment Divider (Bulkhead) Charge');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(253,'SIGN','Requires Signature');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(254,'SINGSHIP','Single Shipment');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(255,'SORT','Sort and/or Segregate Pickup or Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(256,'SORTDEL','Sort/Segregate Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(257,'SORTPU','Sort/Segregate Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(258,'SPECIALEQUIP','Special Equipment Needed');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(259,'SSTORDEL','Self-Storage Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(260,'SSTORPU','Self-Storage Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(261,'STD','Standard Rate');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(262,'STORAGE','Storage');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(263,'STPOFF','Stop Off');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(264,'STRAIGHT','Straight Truck');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(265,'SUNDEL','Sunday Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(266,'SUNPU','Sunday Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(267,'TANK','Tanker Truck');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(268,'TARP','Tarp Charge');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(269,'TAX','Tax');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(270,'TCKNU','Truck Not Used');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(271,'TMCHG','Team Charge');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(272,'TOLL','Toll Charge');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(273,'TPBOXCHARGE','TruckPack Charge per Box');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(274,'TPDELCHARGE','TruckPack Delivery Charge');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(275,'TRIAXLE','Triaxle Trailer Charge');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(276,'TSAC','TSA Certification');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(277,'TWOMAN','Two Man Delivery Required');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(278,'UNKCHRG','Unknown Charge');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(279,'UNLOAD','Driver Unload');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(280,'UNLOADCNT','Driver Unload and Count');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(281,'UNLOADDEL','Driver Unload at Destination');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(282,'UNPCK','Unpacking');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(283,'UTL','Utility Site Pickup or Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(284,'UTLDEL','Utility Site Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(285,'UTLPU','Utility Site Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(286,'WEDEL','Weekend Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(287,'WEPU','Weekend Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(288,'WESER','Weekend Service');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(289,'WGHT','Weight Fee');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(290,'WGTV','Weight Verification Fee');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(291,'WHITEGLOVE','White Glove Service');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(292,'WINSPC','Weight Inspection');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(293,'WIRE','Wire Transfer Fee');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(294,'WVERI','Weight Verification');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(295,'XCHGDEL','Pallet Exchange Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(296,'XCHGPU','Pallet Exchange Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(297,'XFERDEL','Dock Transfer Delivery');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(298,'XFERPU','Dock Transfer Pickup');
INSERT INTO RefProject44ChargeCode(Project44ChargeCodeIndex,Project44ChargeCodeText,Project44ChargeCodeDescription)
                                       VALUES(299,'XTRLBR','Extra Labor');
