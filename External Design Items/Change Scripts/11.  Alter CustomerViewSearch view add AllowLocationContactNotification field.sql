USE [PacmanDevelopment]
GO

ALTER VIEW [dbo].[CustomerViewSearch]
AS
SELECT        c.Id, c.CustomerNumber, c.Name, c.DefaultAccountBucketId, ab.Code AS DefaultAccountBucketCode, ab.Description AS DefaultAccountBucketDescription, c.PrefixId, 
                         ISNULL(p.Code, '') AS PrefixCode, ISNULL(p.Description, '') AS PrefixDescription, c.HidePrefix, c.Active, c.RequiredMileageSourceId, ISNULL(ms.Code, '') 
                         AS RequiredMileageSourceCode, ISNULL(ms.Description, '') AS RequiredMileageSourceDescription, c.CustomerType, c.AdditionalBillOfladingText, c.InvoiceTerms, 
                         c.InvoiceRequiresCarrierProNumber, c.InvoiceRequiresControlAccount, c.InvoiceRequiresShipperReference, c.ShipmentRequiresPurchaseOrderNumber, 
                         c.ValidatePurchaseOrderNumber, c.InvalidPurchaseOrderNumberMessage, c.CustomVendorSelectionMessage, c.ShipmentRequiresNMFC, c.DateCreated, c.LogoUrl, 
                         c.ShipperBillOfLadingSeed, c.EnableShipperBillOfLading, c.ShipperBillOfLadingPrefix, c.ShipperBillOfLadingSuffix, c.CreditLimit, c.Notes, c.AuditInstructions, 
                         c.CustomerToleranceEnabled, c.TruckloadPickupTolerance, c.TruckloadDeliveryTolerance, c.RatingId, c.CommunicationId, c.SalesRepresentativeId, ISNULL(sr.Name, 
                         '') AS SalesRepresentativeName, ISNULL(sr.SalesRepresentativeNumber, '') AS SalesRepresentativeNumber, c.TierId, t.Name AS TierName, t.TierNumber, 
                         c.TenantId, c.CareOfAddressFormatEnabled, 
                         (CASE WHEN c.CustomerType = 0 THEN 'Reseller' WHEN c.CustomerType = 1 THEN 'Direct Shipper Receiver' WHEN c.CustomerType = 2 THEN 'Broker Customer' END)
                          AS CustomerTypeText, c.CustomField1, c.CustomField2, c.CustomField3, c.CustomField4, c.PaymentGatewayKey, c.IsCashOnly, c.CanPayByCreditCard, 
                         c.RateAndScheduleInDemo, c.CanInvoiceNonDeliveredShipment, c.PurgeExpiredQuotes, c.AllowLocationContactNotification
FROM            dbo.Customer AS c INNER JOIN
                         dbo.Tier AS t ON t.Id = c.TierId INNER JOIN
                         dbo.AccountBucket AS ab ON ab.Id = c.DefaultAccountBucketId LEFT OUTER JOIN
                         dbo.Prefix AS p ON p.Id = c.PrefixId LEFT OUTER JOIN
                         dbo.MileageSource AS ms ON ms.Id = c.RequiredMileageSourceId LEFT OUTER JOIN
                         dbo.SalesRepresentative AS sr ON sr.Id = c.SalesRepresentativeId
