USE [PacmanDevelopment]
GO

ALTER TABLE [dbo].[P44ServiceMapping] DROP CONSTRAINT [FK_P44ServiceMapping_Tenant]
GO

ALTER TABLE [dbo].[P44ServiceMapping] DROP CONSTRAINT [FK_P44ServiceMapping_Service]
GO

ALTER TABLE [dbo].[P44ServiceMapping] DROP CONSTRAINT [DF__P44Service__Proje__6E17FC74]
GO

/****** Object:  Table [dbo].[P44ServiceMapping]    Script Date: 3/7/2019 2:50:53 PM ******/
DROP TABLE [dbo].[P44ServiceMapping]
GO

/****** Object:  Table [dbo].[P44ServiceMapping]    Script Date: 3/7/2019 2:50:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[P44ServiceMapping](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Project44Code] [int] NOT NULL,
	[ServiceId] [bigint] NOT NULL,
	[TenantId] [bigint] NOT NULL,
 CONSTRAINT [PK_P44ServiceMapping] PRIMARY KEY CLUSTERED (Project44Code,ServiceId,TenantId)
 )
GO

ALTER TABLE [dbo].[P44ServiceMapping] ADD  CONSTRAINT [DF__P44Service__Proje__6E17FC74]  DEFAULT ((0)) FOR [Project44Code]
GO

ALTER TABLE [dbo].[P44ServiceMapping]  WITH CHECK ADD  CONSTRAINT [FK_P44ServiceMapping_Service] FOREIGN KEY([ServiceId])
REFERENCES [dbo].[Service] ([Id])
GO

ALTER TABLE [dbo].[P44ServiceMapping] CHECK CONSTRAINT [FK_P44ServiceMapping_Service]
GO

ALTER TABLE [dbo].[P44ServiceMapping]  WITH CHECK ADD  CONSTRAINT [FK_P44ServiceMapping_Tenant] FOREIGN KEY([TenantId])
REFERENCES [dbo].[Tenant] ([Id])
GO

ALTER TABLE [dbo].[P44ServiceMapping] CHECK CONSTRAINT [FK_P44ServiceMapping_Tenant]
GO


