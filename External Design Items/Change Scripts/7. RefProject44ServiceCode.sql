USE [PacmanDevelopment]
GO

/****** Object:  Table [dbo].[RefProject44ServiceCode]    Script Date: 3/12/2019 2:03:22 PM ******/
DROP TABLE [dbo].[RefProject44ServiceCode]
GO

/****** Object:  Table [dbo].[RefProject44ServiceCode]    Script Date: 3/12/2019 2:03:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RefProject44ServiceCode](
	[Project44ServiceCodeIndex] [int] NOT NULL,
	[Project44ServiceCodeText] [nvarchar](50) NOT NULL,
	[Project44ServiceCodeDescription] [nvarchar](500) NOT NULL,
 CONSTRAINT [PK_RefProject44ServiceCode] PRIMARY KEY CLUSTERED 
(
	[Project44ServiceCodeIndex] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


