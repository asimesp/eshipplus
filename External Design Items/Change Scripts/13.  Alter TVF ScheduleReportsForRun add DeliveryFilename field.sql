USE [PacmanDevelopment]
GO
/****** Object:  UserDefinedFunction [dbo].[ScheduleReportsForRun]    Script Date: 3/13/2019 3:52:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Christopher Oyesiku
-- Create date: December 05 2011 Modified: September 18 2014
-- Description:	Returns a list of scheduled reports for run based on configured date/times
-- Updates:		5/17/18 (KG): Explicitly stated columns
--				3/13/2019 (BSR): Add DeliveryFilename field
-- =============================================
ALTER FUNCTION [dbo].[ScheduleReportsForRun]
(	
	@NeverRunDate datetime,
	@CheckDate datetime,
	@CurrentTime nvarchar(5)
)
RETURNS TABLE 
AS
RETURN 
(
	-- NOTE:	
	--	DateDiff(WEEK,x,y) will return difference in week by week number.
	--	for weekly difference, we only need to know that we are more than 6 days apart hence the use of
	--	DateDiff(Day,x,y) > 6 for weekly calculcations

	Select
		r.Id
      ,r.[ScheduleInterval]
      ,r.[Start]
      ,r.[End]
      ,r.[LastRun]
      ,r.[Enabled]
      ,r.[Time]
      ,r.[ExcludeSunday]
      ,r.[ExcludeMonday]
      ,r.[ExcludeTuesday]
      ,r.[ExcludeWednesday]
      ,r.[ExcludeThursday]
      ,r.[ExcludeFriday]
      ,r.[ExcludeSaturday]
      ,r.[NotifyEmails]
      ,r.[ReportConfigurationId]
      ,r.[Notify]
      ,r.[TenantId]
      ,r.[UserId]
      ,r.[CustomerGroupId]
      ,r.[VendorGroupId]
      ,r.[Extension]
      ,r.[FtpEnabled]
      ,r.[FtpUrl]
      ,r.[SecureFtp]
      ,r.[FtpUsername]
      ,r.[FtpPassword]
      ,r.[FtpDefaultFolder]
      ,r.[SuppressNotificationForEmptyReport]
	  ,r.DeliveryFilename
	From
		ReportSchedule r
		Inner Join [user] u on r.UserId = u.Id
	Where
		r.[Start] <= @CheckDate And @CheckDate <= r.[End]
		And r.[Enabled] = 1
		And u.[Enabled] = 1
		And r.[Time] = @CurrentTime
		And (
			(
				r.LastRun = @NeverRunDate -- never run
				And
					(
						(r.[Start] = CONVERT(datetime,(
							CONVERT(nvarchar(4), YEAR(@CheckDate)) + '-' + 
							CONVERT(nvarchar(2), MONTH(@CheckDate)) + '-' + 
							CONVERT(nvarchar(4), DAY(@CheckDate)) + ' 00:00:00')))
						Or (r.ScheduleInterval = 0 And ABS(DATEDIFF(YEAR, r.[Start], @CheckDate)) > 0 
							And DATEPART(MONTH, r.[Start]) = DATEPART(MONTH, @CheckDate) 
							And DATEPART(DAY, r.[Start]) = DATEPART(DAY, @CheckDate)) -- yearly
						Or (r.ScheduleInterval = 1 And ABS(DATEDIFF(MONTH, r.[Start], @CheckDate)) > 0 
							And DATEPART(DAY, r.[Start]) = DATEPART(DAY, @CheckDate)) -- monthly
						Or (r.ScheduleInterval = 2 And ABS(DATEDIFF(DAY, r.[Start], @CheckDate)) > 6) -- weekly
						Or (r.ScheduleInterval = 3 And ABS(DATEDIFF(DAY, r.[Start], @CheckDate)) > 0) -- daily
					)
			)
			Or
			(
				r.LastRun <> @NeverRunDate -- never run
				And r.LastRun <= @CheckDate
				And
					(
						(r.ScheduleInterval = 0 And ABS(DATEDIFF(YEAR, r.LastRun, @CheckDate)) > 0 
							And DATEPART(MONTH, r.LastRun) = DATEPART(MONTH, @CheckDate) 
							And DATEPART(DAY, r.LastRun) = DATEPART(DAY, @CheckDate)) -- yearly
						Or (r.ScheduleInterval = 1 And ABS(DATEDIFF(MONTH, r.LastRun, @CheckDate)) > 0 
							And DATEPART(DAY, r.LastRun) = DATEPART(DAY, @CheckDate)) -- monthly
						Or (r.ScheduleInterval = 2 And ABS(DATEDIFF(DAY, r.LastRun, @CheckDate)) > 6) -- weekly
						Or (r.ScheduleInterval = 3 And ABS(DATEDIFF(DAY, r.LastRun, @CheckDate)) > 0) -- daily
					)
			)
		)
		And (r.ExcludeSunday = 0 Or (r.ExcludeSunday = 1 And DATEPART(WEEKDAY, @CheckDate) <> 1)) -- Sunday
		And (r.ExcludeMonday = 0 Or (r.ExcludeMonday = 1 And DATEPART(WEEKDAY, @CheckDate) <> 2)) -- Monday
		And (r.ExcludeTuesday = 0 Or (r.ExcludeTuesday = 1 And DATEPART(WEEKDAY, @CheckDate) <> 3)) -- Tuesday
		And (r.ExcludeWednesday = 0 Or (r.ExcludeWednesday = 1 And DATEPART(WEEKDAY, @CheckDate) <> 4)) -- Wednesday
		And (r.ExcludeThursday = 0 Or (r.ExcludeThursday = 1 And DATEPART(WEEKDAY, @CheckDate) <> 5)) -- Thursday
		And (r.ExcludeFriday = 0 Or (r.ExcludeFriday = 1 And DATEPART(WEEKDAY, @CheckDate) <> 6)) -- Friday
		And (r.ExcludeSaturday = 0 Or (r.ExcludeSaturday = 1 And DATEPART(WEEKDAY, @CheckDate) <> 7)) -- Saturday
)
