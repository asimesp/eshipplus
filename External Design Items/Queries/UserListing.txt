SELECT 
	{0} 
FROM 
	[User] AS u 
	INNER JOIN Customer AS c ON c.Id = u.CustomerId
	LEFT JOIN UserDepartment AS ud ON ud.Id = u.UserDepartmentId 
	LEFT JOIN Country as co on co.Id = u.CountryId

WHERE 
	@TenantId <> 0 
		AND @UserId <> 0 
		AND {1} 
ORDER BY {2}