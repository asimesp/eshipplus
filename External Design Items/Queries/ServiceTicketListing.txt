SELECT 
	{0}
FROM 
	ServiceTicket st
		INNER JOIN [User] u ON u.Id = st.UserId
		INNER JOIN Customer c ON c.Id = st.CustomerId
		INNER JOIN AccountBucket ab ON ab.Id = st.AccountBucketId
		LEFT JOIN ServiceTicketChargeStatistics scs ON scs.ServiceTicketId = st.Id 
		LEFT JOIN AccountBucketUnit au ON au.Id = st.AccountBucketUnitId
		LEFT JOIN Prefix p ON p.Id = st.PrefixId
		LEFT JOIN SalesRepresentative sr ON sr.Id = st.SalesRepresentativeId
		LEFT JOIN RefServiceTicketStatus sts ON sts.ServiceTicketStatusIndex = st.[Status]
WHERE
	@TenantId <> 0
		AND @UserId <> 0
		AND c.TenantId = @TenantId
		AND ((Select COUNT(*) FROM [User] where Id = @UserId and TenantEmployee = 1) > 0
				OR
				(st.CustomerId IN 
					(SELECT CustomerId FROM UserShipAs WHERE TenantId = @TenantId AND UserId = @UserId
					 UNION
					 SELECT [User].CustomerId FROM [User] WHERE Id = @UserId)))
		AND (@CustomerGroupId = 0 OR (Select COUNT(*) from CustomerGroupMap WHERE CustomerId = st.CustomerId and CustomerGroupId = @CustomerGroupId) > 0)
		AND {1}
ORDER BY
		{2}