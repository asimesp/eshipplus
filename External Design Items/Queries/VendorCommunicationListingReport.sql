DECLARE @TenantId bigint = 4;
DECLARE @VendorGroupId bigint = 0;

SELECT
	v.Name 'VendorName'
	,v.VendorNumber
	,vc.ConnectGuid
	,vc.EdiEnabled
	,vc.EdiCode
	,vc.EdiVANUrl
	,vc.SecureEdiVAN
	,vc.EdiVANUsername
	,vc.EdiVANPassword
	,vc.FtpEnabled
	,vc.FtpUrl
	,vc.SecureFtp
	,vc.FtpUsername
	,vc.FtpPassword
	,vc.FtpDefaultFolder
	,vc.EdiVANEnvelopePath
	,vc.UseSelectiveDropOff
	,vc.Pickup997
	,vc.Pickup214
	,vc.Pickup210
	,vc.AcknowledgePickup214
	,vc.AcknowledgePickup210
	,vc.FtpDeleteFileAfterPickup
	,vc.EdiDeleteFileAfterPickup
	,vc.LoadTenderExpAllowance
	,vc.Pickup990
	,vc.AcknowledgePickup990
	,vc.DropOff204
	,vc.ImgRtrvEnabled
	,vc.CarrierIntegrationEngine
	,vc.CarrierIntegrationUsername
	,vc.CarrierIntegrationPassword
	,vc.ShipmentDispatchEnabled
	,vc.DisableGuaranteedServiceDispatch
	,vc.IsInSMC3Testing
	,vc.SMC3EvaEnabled
	,vc.SMC3EvaSupportsDispatch
	,vc.SMC3EvaSupportsDocumentRetrieval
	,vc.SMC3EvaSupportsTracking
	,vc.SMC3ProductionAccountToken
	,vc.SMC3TestAccountToken
FROM VendorCommunication vc
	INNER JOIN Vendor v ON v.Id = vc.VendorId
WHERE vc.TenantId = @TenantId
	AND (@VendorGroupId = 0 OR (SELECT COUNT(*) FROM VendorGroupMap WHERE VendorId = v.Id AND VendorGroupId = @VendorGroupId) > 0)