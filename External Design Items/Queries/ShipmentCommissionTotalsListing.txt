Select
	[#Columns#]

From Shipment sh
	INNER JOIN Customer c on c.Id = sh.CustomerId
	INNER JOIN ShipmentChargeStatistics AS scs ON sh.Id = scs.ShipmentId
	INNER JOIN ShipmentLocation shlo ON sh.OriginId = shlo.Id
	INNER JOIN ShipmentLocation shld ON sh.DestinationId = shld.Id
	INNER JOIN ShipmentAccountBucket shab ON sh.Id = shab.ShipmentId AND shab.[Primary] = 1
	INNER JOIN AccountBucket ab ON shab.AccountBucketId = ab.Id 
	LEFT JOIN SalesRepresentative sr ON sr.Id = sh.SalesRepresentativeId
	Left Join Prefix p On sh.PrefixId = p.Id
	LEFT JOIN ShipmentActualCosts(@TenantId, '1753-01-01 00:00:00', '9999-12-31 23:59:59') as sac ON sac.Id = sh.Id
	LEFT JOIN ShipmentActualRevenues(@TenantId, '1753-01-01 00:00:00', '9999-12-31 23:59:59') as sar ON sar.Id = sh.Id
	LEFT JOIN ShipmentInvoices si ON sh.Id = si.ShipmentId
	LEFT JOIN (Select 
					ReferenceNumber
					,SUM(CASE WHEN [Type] = 0 AND AdditionalEntityCommission = 1 THEN AmountPaid ELSE 0 END) 'Addl Entity Total Comm. Paid' 
					,SUM(CASE WHEN [Type] = 1 AND AdditionalEntityCommission = 1 THEN AmountPaid ELSE 0 END) 'Addl Entity Total Comm. Reversed' 
					,SUM(CASE WHEN [Type] = 0 AND AdditionalEntityCommission = 1 THEN AmountPaid WHEN [Type] = 1 AND AdditionalEntityCommission = 1 THEN AmountPaid * -1 END) 'Net Addl Entity Comm. Paid'  
					,SUM(CASE WHEN [Type] = 0 THEN AmountPaid ELSE 0 END) 'Total Comm. Paid' 
					,SUM(CASE WHEN [Type] = 1 THEN AmountPaid ELSE 0 END) 'Total Comm. Reversed' 
					,SUM(CASE WHEN [Type] = 0 THEN AmountPaid WHEN [Type] = 1 THEN AmountPaid * -1 END) 'Net Comm. Paid' 
				From CommissionPayment 
				Group By ReferenceNumber) as PaymentTotals on PaymentTotals.ReferenceNumber = sh.ShipmentNumber
	LEFT JOIN RefShipmentStatus ss ON ss.ShipmentStatusIndex = sh.[Status]
	LEFT JOIN RefServiceMode sm ON sm.ServiceModeIndex = sh.ServiceMode
Where
	1=1 AND [#Filters#]

[#OrderBy#]

OPTION (RECOMPILE)