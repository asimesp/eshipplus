DECLARE @TenantId bigint = 4;
DECLARE @UserId bigint = 3;

SELECT 
	r.Name
FROM 
	Region r
WHERE 
	@TenantId <> 0 
		AND @UserId <> 0
		AND r.TenantId = @TenantId 

SELECT COUNT(*) FROM Region WHERE TenantId = @TenantId