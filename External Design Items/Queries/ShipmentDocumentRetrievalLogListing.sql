Declare @PodWniBolUpdatedOn_BlankIfAll datetime = '2015-01-08'


Select
	sdr.ShipmentNumber
	,sdr.DateCreated
	,(
		CASE 
			WHEN sdr.DateCreated = '1753-01-01 00:00:00.000' THEN '' 
			ELSE CONVERT(nvarchar(10),sdr.DateCreated,121) 
		END
	) 'Date Created Text'
	,sdr.ProNumber
	,sdr.PodDate
	,(
		CASE 
			WHEN sdr.PodDate = '1753-01-01 00:00:00.000' THEN '' 
			ELSE CONVERT(nvarchar(10),sdr.PodDate,121) 
		END
	) 'POD Date Text'
	,sdr.BolDate
	,(
		CASE 
			WHEN sdr.BolDate = '1753-01-01 00:00:00.000' THEN '' 
			ELSE CONVERT(nvarchar(10),sdr.BolDate,121) 
		END
	) 'BOL Date Text'
	,sdr.WniDate
	,(
		CASE 
			WHEN sdr.WniDate = '1753-01-01 00:00:00.000' THEN '' 
			ELSE CONVERT(nvarchar(10),sdr.WniDate,121) 
		END
	) 'WNI Date Text'
	,sdr.ExpirationDate
	,(
		CASE 
			WHEN sdr.ExpirationDate = '1753-01-01 00:00:00.000' THEN '' 
			ELSE CONVERT(nvarchar(10),sdr.ExpirationDate,121) 
		END
	) 'Expiration Date Text'
	,u.FirstName 'UserFirstName'
	,u.LastName 'UserLastName'
	,u.Username
	,v.VendorNumber 'Vendor Number'
	,v.Name 'Vendor Name'
	,v.Scac 'Vendor Scac'
	,vc.ImgRtrvEngine 'Image Retrieval Engine'
	,vc.ImgRtrvEnabled 'Image Retrieval Enabled'

From ShipmentDocRtrvLog sdr

Inner Join VendorCommunication vc on vc.Id = sdr.CommunicationId
Inner Join Vendor v on v.Id = vc.VendorId
Inner Join [User] u on sdr.UserId = u.Id

Where (
		CASE 
			WHEN CAST(@PodWniBolUpdatedOn_BlankIfAll as Date) = CAST(sdr.BolDate as Date) THEN 1 
			WHEN CAST(@PodWniBolUpdatedOn_BlankIfAll as Date) = CAST(sdr.PodDate as Date) THEN 1 
			WHEN CAST(@PodWniBolUpdatedOn_BlankIfAll as Date) = CAST(sdr.WniDate as Date) THEN 1 
			WHEN @PodWniBolUpdatedOn_BlankIfAll = '1753-01-01 00:00:00.000' THEN 1
			ELSE 0 
		END
	  ) = 1