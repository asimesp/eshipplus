IF EXISTS (SELECT * FROM tempdb.sys.objects WHERE name LIKE '#tmpTrendActivity%')  
		DROP TABLE #tmpTrendActivity

DECLARE @TenantId bigint = 4;
DECLARE @UserId bigint = 2;
DECLARE @CustomerGroupId bigint = 0;
DECLARE @VendorGroupId bigint = 316;
DECLARE @ShipmentDateStart datetime = Dateadd(year, -1, getdate())
DECLARE @ShipmentDateEnd datetime = getdate()
DECLARE @Mode nvarchar(20) = 'all';
DECLARE @UseCity int = 1;
DECLARE @CustomerNumber nvarchar(50) = 'all';
DECLARE @VendorNumber nvarchar(50) = 'all';

CREATE TABLE #tmpTrendActivity
(
	Lane NVarChar(250),
	ShipmentCount Int,
	
	TotalEstimatedCost decimal(18,4),
	TotalEstimatedCharge decimal(18,4),
	TotalEstimatedDiscount decimal(18,4),
	TotalEstimatedAmountDue decimal(18,4),
	TotalEstimatedProfit decimal(18,4),
	
	FreightEstimatedCost decimal(18,4),
	FreightEstimatedCharge decimal(18,4),
	FuelEstimatedCost decimal(18,4),
	FuelEstimatedCharge decimal(18,4),
	AccessorialEstimatedCost decimal(18,4),
	AccessorialEstimatedCharge decimal(18,4),
	ServiceEstimatedCost decimal(18,4),
	ServiceEstimatedCharge decimal(18,4),
	
	TotalActualCost decimal(18,4),
	TotalActualCostCredit decimal(18,4),
	TotalActualCharge decimal(18,4),
	TotalActualChargeSupplemental decimal(18,4),
	TotalActualChargeCredit decimal(18,4),
	
	TotalActualFreightCost decimal(18,4),
	TotalActualFreightCostCredit decimal(18,4),
	TotalActualFreightCharge decimal(18,4),
	TotalActualSupplementalFreightCharge decimal(18,4),
	TotalActualCreditFreightCharge decimal(18,4),
	
	TotalActualFuelCost decimal(18,4),
	TotalActualFuelCostCredit decimal(18,4),
	TotalActualFuelCharge decimal(18,4),
	TotalActualSupplementalFuelCharge decimal(18,4),
	TotalActualCreditFuelCharge decimal(18,4),
	
	TotalActualAccessorialCost decimal(18,4),
	TotalActualAccessorialCostCredit decimal(18,4),
	TotalActualAccessorialCharge decimal(18,4),
	TotalActualSupplementalAccessorialCharge decimal(18,4),
	TotalActualCreditAccessorialCharge decimal(18,4),
	
	TotalActualServiceCost decimal(18,4),
	TotalActualServiceCostCredit decimal(18,4),
	TotalActualServiceCharge decimal(18,4),
	TotalActualSupplementalServiceCharge decimal(18,4),
	TotalActualCreditServiceCharge decimal(18,4)
)

INSERT INTO #tmpTrendActivity 
EXEC ShipmentLaneTrendActivityReport @TenantId, @UserId, @CustomerGroupId, @VendorGroupId, @ShipmentDateStart, @ShipmentDateEnd,@Mode, @UseCity, @CustomerNumber, @VendorNumber

SELECT 
	Lane,
	ShipmentCount,
	
	TotalEstimatedCost,
	TotalEstimatedCharge,
	TotalEstimatedDiscount,
	TotalEstimatedAmountDue,
	TotalEstimatedProfit,
	
	FreightEstimatedCost,
	FreightEstimatedCharge,
	FuelEstimatedCost,
	FuelEstimatedCharge,
	AccessorialEstimatedCost,
	AccessorialEstimatedCharge,
	ServiceEstimatedCost,
	ServiceEstimatedCharge,
	
	TotalActualCost,
	TotalActualCostCredit,
	TotalActualCharge,
	TotalActualChargeSupplemental,
	TotalActualChargeCredit,
	
	TotalActualFreightCost,
	TotalActualFreightCostCredit,
	TotalActualFreightCharge,
	TotalActualSupplementalFreightCharge,
	TotalActualCreditFreightCharge,
	
	TotalActualFuelCost,
	TotalActualFuelCostCredit,
	TotalActualFuelCharge,
	TotalActualSupplementalFuelCharge,
	TotalActualCreditFuelCharge,
	
	TotalActualAccessorialCost,
	TotalActualAccessorialCostCredit,
	TotalActualAccessorialCharge,
	TotalActualSupplementalAccessorialCharge,
	TotalActualCreditAccessorialCharge,
	
	TotalActualServiceCost,
	TotalActualServiceCostCredit,
	TotalActualServiceCharge,
	TotalActualSupplementalServiceCharge,
	TotalActualCreditServiceCharge
	
FROM #tmpTrendActivity
WHERE 
		1 = 1
--		AND {1}
--ORDER BY 
--	{2} 

DROP TABLE #tmpTrendActivity