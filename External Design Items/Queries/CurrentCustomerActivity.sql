IF EXISTS (SELECT * FROM tempdb.sys.objects WHERE name LIKE '#tmpCurrentActivity%')  
		DROP TABLE #tmpCurrentActivity

DECLARE @TenantId bigint = 4;
DECLARE @Year bigint = '2012';
DECLARE @Mode nvarchar(20) = 'all';

CREATE TABLE #tmpCurrentActivity
(
	TenantId bigint,
	CustomerId bigint,
	CustomerNumber nvarchar(50),
	CustomerName nvarchar(50),
	TierNumber nvarchar(50),
	TierName nvarchar(50),
	CustomerCreateDate datetime,
	SalesRepresentativeNumber nvarchar(50),
	SalesRepresentativeName nvarchar(50),
	Wk1Cnt int,
	Wk2Cnt int,
	Wk3Cnt int,
	Wk4Cnt int,
	Wk5Cnt int,
	Wk6Cnt int,
	Wk7Cnt int,
	Wk8Cnt int,
	Wk9Cnt int,
	Wk10Cnt int,
	Wk11Cnt int,
	Wk12Cnt int,
	Wk13Cnt int,
	Wk14Cnt int,
	Wk15Cnt int,
	Wk16Cnt int,
	Wk17Cnt int,
	Wk18Cnt int,
	Wk19Cnt int,
	Wk20Cnt int,
	Wk21Cnt int,
	Wk22Cnt int,
	Wk23Cnt int,
	Wk24Cnt int,
	Wk25Cnt int,
	Wk26Cnt int,
	Wk27Cnt int,
	Wk28Cnt int,
	Wk29Cnt int,
	Wk30Cnt int,
	Wk31Cnt int,
	Wk32Cnt int,
	Wk33Cnt int,
	Wk34Cnt int,
	Wk35Cnt int,
	Wk36Cnt int,
	Wk37Cnt int,
	Wk38Cnt int,
	Wk39Cnt int,
	Wk40Cnt int,
	Wk41Cnt int,
	Wk42Cnt int,
	Wk43Cnt int,
	Wk44Cnt int,
	Wk45Cnt int,
	Wk46Cnt int,
	Wk47Cnt int,
	Wk48Cnt int,
	Wk49Cnt int,
	Wk50Cnt int,
	Wk51Cnt int,
	Wk52Cnt int,
	Wk53Cnt int,
	YrWklyAvg float,
	CurrentYrLast12WkAvg float,
	CurrentYrCurrentWk int
)
INSERT INTO #tmpCurrentActivity 

EXEC CurrentCustomerActivityReport2 @TenantId, @Year, @Mode

SELECT 
	c.CustomerNumber,
	c.CustomerName,
	c.TierNumber,
	c.TierName,
	c.CustomerCreateDate,
	c.SalesRepresentativeNumber,
	c.SalesRepresentativeName,
	Wk1Cnt,
	c.Wk2Cnt,
	c.Wk3Cnt,
	c.Wk4Cnt,
	c.Wk5Cnt,
	c.Wk6Cnt,
	c.Wk7Cnt,
	c.Wk8Cnt,
	c.Wk9Cnt,
	c.Wk10Cnt,
	c.Wk11Cnt,
	c.Wk12Cnt,
	c.Wk13Cnt,
	c.Wk14Cnt,
	c.Wk15Cnt,
	c.Wk16Cnt,
	c.Wk17Cnt,
	c.Wk18Cnt,
	c.Wk19Cnt,
	c.Wk20Cnt,
	c.Wk21Cnt,
	c.Wk22Cnt,
	c.Wk23Cnt,
	c.Wk24Cnt,
	c.Wk25Cnt,
	c.Wk26Cnt,
	c.Wk27Cnt,
	c.Wk28Cnt,
	c.Wk29Cnt,
	c.Wk30Cnt,
	c.Wk31Cnt,
	c.Wk32Cnt,
	c.Wk33Cnt,
	c.Wk34Cnt,
	c.Wk35Cnt,
	c.Wk36Cnt,
	c.Wk37Cnt,
	c.Wk38Cnt,
	c.Wk39Cnt,
	c.Wk40Cnt,
	c.Wk41Cnt,
	c.Wk42Cnt,
	c.Wk43Cnt,
	c.Wk44Cnt,
	c.Wk45Cnt,
	c.Wk46Cnt,
	c.Wk47Cnt,
	c.Wk48Cnt,
	c.Wk49Cnt,
	c.Wk50Cnt,
	c.Wk51Cnt,
	c.Wk52Cnt,
	c.Wk53Cnt,
	c.YrWklyAvg,
	c.CurrentYrLast12WkAvg,
	c.CurrentYrCurrentWk
FROM #tmpCurrentActivity c
--WHERE 
--	c.TenantId = @TenantId
--		AND (
--				(Select COUNT(*) from [User] where id = @UserId and TenantEmployee = 1) > 0
--				OR
--				(c.CustomerId in (SELECT CustomerId FROM UserShipAs WHERE TenantId = @TenantId and UserId = @UserId
--					UNION
--					SELECT [User].CustomerId FROM [User] WHERE Id = @UserId)))
--		AND (@CustomerGroupId = 0 OR (Select COUNT(*) from CustomerGroupMap WHERE CustomerId = c.CustomerId and CustomerGroupId = @CustomerGroupId) > 0) 
--		AND {1}
--ORDER BY 
--	{2} 

DROP TABLE #tmpCurrentActivity

SELECT COUNT(*) FROM Customer WHERE @TenantId = TenantId