DECLARE @StartPostDate date = '2011-01-01';
DECLARE @EndPostDate date = '2014-01-01';
DECLARE @StartDateCreated date = '2011-01-01';
DECLARE @EndDateCreated date = '2014-01-31';
DECLARE @TenantId bigint = 4;
DECLARE @UserId bigint = 2;

SELECT DISTINCT
	st.ServiceTicketNumber,
	st.DateCreated,
	ISNULL(p.Code,'') 'PrefixCode',
	ISNULL(p.[Description],'') 'PrefixDescription',
	Isnull(rsts.ServiceTicketStatusText,'') 'StatusText',
	c.CustomerNumber,
	c.Name,
	v.VendorNumber,
	v.Name,
	ab.Code 'AccountBucketCode',
	ab.[Description] 'AccountBucketDescription',
	scs.TotalNettedCharge 'EstimateTotalCharge',
	scs.TotalCost 'EstimateTotalCost',
	ISNULL(sac.TotalActualCost, 0) 'TotalActualCost',
	ISNULL(sac.TotalActualCostCredit, 0) 'TotalActualCostCredit',
	
	ISNULL(sac.ActualFreightCost, 0) 'ActualFreightCost',
	ISNULL(sac.ActualFreightCostCredit, 0) 'ActualFreightCostCredit',
	ISNULL(sac.ActualFuelCost, 0) 'ActualFuelCost',
	ISNULL(sac.ActualFuelCostCredit, 0) 'ActualFuelCostCredit',
	ISNULL(sac.ActualAccessorialCost, 0) 'ActualAccessorialCost',
	ISNULL(sac.ActualAccessorialCostCredit, 0) 'ActualAccessorialCostCredit',
	ISNULL(sac.ActualServiceCost, 0) 'ActualServiceCost',
	ISNULL(sac.ActualServiceCostCredit, 0) 'ActualServiceCostCredit',
	
	ISNULL(sar.TotalActualCharges, 0) 'TotalActualCharges',
	ISNULL(sar.TotalSupplementalCharges, 0) 'TotalSupplementalCharges',
	ISNULL(sar.TotalCredit, 0) 'TotalRevenueCredits',
	
	ISNULL(sar.ActualFreightCharges, 0) 'ActualFreightCharges',
	ISNULL(sar.ActualFuelCharges, 0) 'ActualFuelCharges',
	ISNULL(sar.ActualAccessorialCharges, 0) 'ActualAccessorialCharges',
	ISNULL(sar.ActualServiceCharges, 0) 'ActualServiceCharges',
	ISNULL(sar.AccessorialCredit, 0) 'AccessorialCredit',
	ISNULL(sar.FreightCredit, 0) 'FreightCredit',
	ISNULL(sar.FuelCredit, 0) 'FuelCredit',
	ISNULL(sar.ServiceCredit, 0) 'ServiceCredit',
	ISNULL(sar.SupplementalAccessorialCharges, 0) 'SupplementalAccessorialCharges',
	ISNULL(sar.SupplementalFreightCharges, 0) 'SupplementalFreightCharges',
	ISNULL(sar.SupplementalFuelCharges, 0) 'SupplementalFuelCharges',
	ISNULL(sar.SupplementalServiceCharges, 0) 'SupplementalServiceCharges',

	c.CustomField1,	
	c.CustomField2,	
	c.CustomField3,	
	c.CustomField4
FROM
	ServiceTicket AS st
		INNER JOIN Customer AS c ON st.CustomerId = c.Id
		INNER JOIN ServiceTicketVendor AS sv ON st.Id = sv.ServiceTicketId AND sv.[Primary] = 1
		INNER JOIN Vendor AS v ON sv.VendorId = v.Id 
		INNER JOIN ServiceTicketChargeStatistics AS scs ON st.Id = scs.ServiceTicketId
		INNER JOIN AccountBucket ab ON st.AccountBucketId = ab.Id
		LEFT JOIN ServiceTicketActualCosts(@TenantId, @StartPostDate, @EndPostDate) as sac ON sac.Id = st.Id
		LEFT JOIN ServiceTicketActualRevenues(@TenantId, @StartPostDate, @EndPostDate) as sar ON sar.Id = st.Id
		LEFT JOIN Prefix AS p ON st.PrefixId = p.Id
		Left Join RefServiceTicketStatus rsts on rsts.ServiceTicketStatusIndex = st.[Status]
WHERE
	st.TenantId = @TenantId
	AND (
		(SELECT COUNT(*) FROM [User] WHERE Id = @UserId and TenantEmployee = 1) > 0
		OR
		(st.CustomerId IN 
			(SELECT CustomerId FROM UserShipAs WHERE TenantId = @TenantId AND UserId = @UserId
			 UNION
			 SELECT [User].CustomerId FROM [User] WHERE Id = @UserId)))
	AND @StartDateCreated <= st.DateCreated AND st.DateCreated <= @EndDateCreated