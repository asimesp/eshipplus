
SELECT COUNT(*)FROM AssetLog

DECLARE @TenantId BIGINT = 4
DECLARE @UserId BIGINT = 2

SELECT
	al.Comment,
	al.DateCreated,
	(CASE WHEN al.DateCreated = '1753-01-01 00:00:00.000' 
		THEN '' 
		ELSE CONVERT(nvarchar(20),al.DateCreated,101)
	END) AS 'ViewDateCreated',
	al.LogDate,
	(CASE WHEN al.LogDate = '1753-01-01 00:00:00.000' 
		THEN '' 
		ELSE CONVERT(nvarchar(20),al.LogDate,101)
	END) AS 'ViewLogDate',
	 ISNULL(me.MileageEngineText,'') AS 'MileageEngine',
	 al.MilesRun,
	 ISNULL(sh.ShipmentNumber,'') AS 'ShipmentNumber',
	 (CASE
		WHEN sh.Id IS NULL THEN 'No' ELSE 'Yes'
	  END) AS 'Shipment Exists',
	 al.Street1,
	 al.street2,
	 al.City,
	 al.[State],
	 al.PostalCode,
	 c.Name,
	 c.Code,
	 a.AssetNumber,
	 a.[Description]
	 
FROM
	AssetLog al
	INNER JOIN Asset AS a ON a.Id = al.AssetId
	INNER JOIN Country AS c On c.ID = al.CountryId
	LEFT JOIN Shipment AS sh ON sh.Id = al.ShipmentId
	LEFT JOIN RefMileageEngine AS me on me.MileageEngineIndex = al.MileageEngine
WHERE
	@TenantId <> 0
	AND @UserId <> 0
	AND al.TenantId = @TenantId
--	AND {1}
--ORDER BY {2}
	