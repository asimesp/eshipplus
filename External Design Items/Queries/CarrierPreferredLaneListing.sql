DECLARE @TenantId bigint = 4;
DECLARE @UserId bigint = 4;
DECLARE @VendorGroupId bigint = 0;

SELECT 
	COUNT(*)
FROM 
	VendorPreferredLane

SELECT
	(CASE
		WHEN v.Active = 0 THEN 'No'
		ELSE 'Yes'
	END),
	v.VendorNumber,
	v.Name,
	ISNULL(vc.vendorcount,0)
	
FROM
	Vendor AS v
		LEFT JOIN (SELECT
						COUNT(vpl.VendorId) AS vendorcount,
						vpl.VendorId
					FROM 
						VendorPreferredLane AS vpl 
						INNER JOIN Vendor AS v ON v.Id = vpl.VendorId
					GROUP BY
						vpl.VendorId) AS vc ON vc.VendorId = v.Id
WHERE
	@TenantId <> 0
		AND @UserId <> 0
		AND v.TenantId = @TenantId
		AND (@VendorGroupId = 0 OR (SELECT COUNT(*) FROM VendorGroupMap WHERE VendorId = v.Id AND VendorGroupId = @VendorGroupId) > 0)
		      --AND {1} 
--ORDER BY {2}