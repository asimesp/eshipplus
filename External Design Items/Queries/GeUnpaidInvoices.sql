DECLARE @UserId BIGINT = 4
DECLARE @TenantId BIGINT = 4
declare @CustomerGroupId bigint = 0

SELECT COUNT(*) FROM InvoiceDashboardViewSearch(@TenantId, @UserId)

SELECT
	i.InvoiceNumber,
	i.InvoiceDate,
	(CASE WHEN i.InvoiceDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),i.InvoiceDate,101) END) AS 'ViewInvoiceDate',
	i.PostDate,
	(CASE WHEN i.PostDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),i.PostDate,101) END) AS 'ViewPostDate',
	(CASE WHEN i.Posted = 0 THEN 'No' Else 'Yes' END) AS 'Posted',
	i.DueDate,
	(CASE WHEN i.DueDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),i.DueDate,101) END) AS 'ViewDueDate',
	i.ExportDate,
	(CASE WHEN i.ExportDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),i.ExportDate,101) END) AS 'ViewExportDate',
	(CASE WHEN i.Exported = 0 THEN 'No' ELSE 'Yes' END) AS 'Exported', 
	ISNULL(it.InvoiceTypeText, '') AS 'InvoiceType',
	i.SpecialInstruction,
	i.PaidAmount,
	ISNULL(p.Code,'') AS 'PrefixCode',
	ISNULL(p.[Description], '') AS 'PrefixDescription',
	i.CustomerNumber,
	i.CustomerName,
	i.LocationNumber AS 'CustomerLocationNumber',
	i.LocationStreet1 AS 'CustomerLocationStreet1',
	i.LocationStreet2 AS 'CustomerLocationStreet2',
	i.LocationCity AS 'CustomerLocationCity',
	i.LocationState AS 'CustomerLocationState',
	i.LocationPostalCode AS 'CustomerLocationPostalCode',
	i.LocationCountryName AS 'CustomerLocationCoutnry',
	i.Username,
	i.UserFirstName,
	i.UserLastName,
	i.OriginalInvoiceNumber,
	i.OriginalInvoiceId,
	i.AmountDue AS 'TotalAmountDue',
	i.Shipments,
	i.ServiceTickets,
	ISNULL((SELECT SUM(icass.TotalSupplementalCharges)FROM InvoiceCreditsAndSupplementalsSummary AS icass WHERE icass.OriginalInvoiceId = i.Id),0) AS 'TotalSupplementalCharges',
	ISNULL((SELECT SUM(icass.TotalCredit)FROM InvoiceCreditsAndSupplementalsSummary AS icass WHERE icass.OriginalInvoiceId = i.Id),0) AS 'TotalCredits',
	
	ISNULL(i2.InvoiceDate, '1753-01-01 00:00:00.000') AS 'OriginalInvoiceDate',
	(CASE WHEN i2.InvoiceDate IS NULL OR i2.InvoiceDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),i2.InvoiceDate,101) END) AS 'ViewOriginalInvoiceDate',
	ISNULL(i2.PostDate, '1753-01-01 00:00:00.000') AS 'OriginalInvoicePostDate',
	(CASE WHEN i2.PostDate IS NULL OR i2.PostDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),i2.PostDate,101) END) AS 'ViewOriginalInvoicePostDate',
	(CASE WHEN i2.PostDate IS NULL OR i2.Posted = 0 THEN 'No' Else 'Yes' END) AS 'OriginalInvoicePosted',
	ISNULL(i2.DueDate, '1753-01-01 00:00:00.000') AS 'OriginalInvoiceDueDate',
	(CASE WHEN i2.DueDate IS NULL OR i2.DueDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),i2.DueDate,101) END) AS 'ViewOriginalInvoiceDueDate',
	ISNULL(i2.ExportDate, '1753-01-01 00:00:00.000') AS 'OriginalInvoiceExportDate',
	(CASE WHEN i2.ExportDate IS NULL OR i2.ExportDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),i2.ExportDate,101) END) AS 'ViewOriginalInvoiceExportDate',
	(CASE WHEN i2.Exported IS NULL OR i2.Exported = 0 THEN 'No' ELSE 'Yes' END) AS 'OriginalInvoiceExported'
FROM	
	 InvoiceDashboardViewSearch(@TenantId, @UserId) AS i
		LEFT JOIN Prefix AS p ON p.Id = i.PrefixId
		LEFT JOIN Invoice AS i2 ON i.OriginalInvoiceId = i2.Id
		LEFT JOIN RefInvoiceType AS it ON it.InvoiceTypeIndex = i.InvoiceType
WHERE
	@TenantId <> 0
	AND @UserId <> 0
	AND i.CustomerNumber = 36
	AND i.InvoiceDate > '2013-01-19 00:00:00'
	AND ROUND(i.AmountDue,2) < ROUND(i.PaidAmount,2)
