declare @TenantId bigint = 4
declare @UserId bigint = 2
declare @CustomerGroupId bigint = 0
declare @VendorGroupId bigint = 0


SELECT
	sh.ShipmentNumber,
	(CASE
		WHEN sh.AuditedForInvoicing = 0 THEN 'No'
		ELSE 'Yes'
	END),
	shv.ProNumber,
	sh.PurchaseOrderNumber,
	sh.DesiredPickupDate,
	sh.ActualPickupDate,
	sh.EstimatedDeliveryDate,
	sh.ActualDeliveryDate,
	sh.DateCreated,
	sh.DriverName,
	sh.DriverPhoneNumber,
	sh.DriverTrailerNumber,
	(CASE WHEN sh.ShipmentAutorated = 1 THEN 'Yes' ELSE 'No' END) 'Shipment Auto Rated',
	(CASE WHEN sh.CreatedInError = 1 THEN 'Yes' ELSE 'No' END) 'Created In Error',
	ISNULL(sc.Username,''),
	ISNULL(sc.FirstName,''),
	ISNULL(sc.LastName,''),
	ISNULL(cc.Username,''),
	ISNULL(cc.FirstName,''),
	ISNULL(cc.LastName,''),
	sh.ShipmentCoordinatorUserId,
	sh.CarrierCoordinatorUserId,
	sh.MiscField1,
	sh.MiscField2,
	CAST ( '1' AS Integer ),
	ISNULL(p.Code,'') 'Prefix Code',
	ISNULL(p.[Description],'') 'Prefix Description',
	ISNULL(si.PostDate,CONVERT(datetime,'1753-01-01 00:00:00.000')), --INVOICE POSTED DATE
	shlo.[Description],
	shlo.Street1,
	shlo.Street2,
	shlo.City,
	shlo.[State],
	shlo.PostalCode,
	shloc.Code,
	shloc.Name,
	ISNULL(pco.City, '') 'Standard Shipper City Name',
	ISNULL(pco.[State], '') 'Standard Shipper State Name',
	shld.[Description],
	shld.Street1,
	shld.Street2,
	shld.City,
	shld.[State],
	shld.PostalCode,
	shldc.Code,
	shldc.Name,
	ISNULL(pcd.City, '') 'Standard Consignee City Name',
	ISNULL(pcd.[State], '') 'Standard Consignee State Name',
	v.VendorNumber,
	v.Name,
	v.Scac,
	v.DOT,
	v.MC,
	shv.FailureComments,
	ISNULL(fc.Code,''),
	ISNULL(fc.[Description],''),
	(CASE
		WHEN ISNULL(fc.ExcludeOnScorecard,'') = 0 THEN 'No'
		WHEN ISNULL(fc.ExcludeOnScorecard,'')= 1 THEN 'Yes'
	END),
	ISNULL(sp.Code, '') 'PriorityCode',
	ISNULL(sp.[Description], '') 'PriorityDescription',
	ab.Code,
	ab.[Description],
	c.CustomerNumber,
	c.Name,
	ISNULL(ct.CustomerTypeText, '') AS 'CustomerType',
	c.AuditInstructions,
	t.TierNumber,
	t.Name,
	u.Username,
	u.FirstName,
	u.LastName,
	u.TenantEmployee,
	ISNULL(sr.SalesRepresentativeNumber,''),
	ISNULL(sr.Name,''),
	sh.Mileage,
	sh.EmptyMileage,
	sh.ShipperReference,
	scs.TotalCost,
	scs.TotalAmountDue,
	scs.TotalCharges,
	scs.TotalDiscount,
	scs.TotalNettedCharge,
	scs.TotalProfit,
	scs.FreightCharges,
	scs.FreightCost,
	scs.FuelCharges,
	scs.FuelCost,
	scs.AccessorialCharges,
	scs.AccessorialCost,
	scs.ServiceCharges,
	scs.ServiceCost,
	sh.SalesRepresentativeCommissionPercent,
	sh.SalesRepMinCommValue,
	sh.SalesRepMaxCommValue,
	sh.SalesRepAddlEntityName,
	sh.SalesRepAddlEntityCommPercent,
	sis.TotalPieceCount,
	sis.TotalWeight,
	sis.HighestFreightClass,
	ISNULL(abu.Name,''),
	ISNULL(da.AssetNumber,''),
	ISNULL(da.[Description],''),
	ISNULL(tcta.AssetNumber,''),
	ISNULL(tcta.[Description],''),
	ISNULL(tlra.AssetNumber,''),
	ISNULL(tlra.[Description],''),
	ISNULL((SELECT (dt.Code + ', ')
				FROM ShipmentDocument sd, DocumentTag dt
				WHERE sd.ShipmentId = sh.Id AND sd.DocumentTagId = dt.Id 
				ORDER BY sd.Id Asc
				FOR XML PATH('')),''),-- AS 'DocumentTags',
	ISNULL((SELECT (sn.[Message] + ', ')
				FROM ShipmentNote sn
				WHERE sn.ShipmentId = sh.Id AND sn.[Type] = 2 
				ORDER BY sn.Id Asc
				FOR XML PATH('')),''),--Finance Notes	
	ISNULL((SELECT (sn.[Message] + ', ')
				FROM ShipmentNote sn
				WHERE sn.ShipmentId = sh.Id AND sn.[Type] = 3
				ORDER BY sn.Id Asc
				FOR XML PATH('')),''),--Operation Notes
	ISNULL((SELECT (et.TypeName + ', ')
				FROM ShipmentEquipment she INNER JOIN EquipmentType et ON she.EquipmentTypeId = et.Id AND she.ShipmentId = sh.Id
				ORDER BY et.Id Desc
				FOR XML PATH('')),''),--Equipment Types,
	ISNULL((SELECT (s.Code + ', ')
				FROM ShipmentService shs INNER JOIN [Service] s ON shs.ServiceId = s.Id AND shs.ShipmentId = sh.Id
				ORDER BY s.Code Desc
				FOR XML PATH('')),'') 'Service Codes', --Service Codes
	ISNULL((SELECT ('['+ sr.Name + ': ' + sr.Value + '] ')
				FROM ShipmentReference sr 
				Where sr.ShipmentId = sh.Id
				ORDER BY sr.Name
				FOR XML PATH('')),'') 'Customer References',
	(CASE WHEN sh.ActualDeliveryDate = '1753-01-01 00:00:00.000' 
		THEN '' 
		ELSE CONVERT(nvarchar(20),sh.ActualDeliveryDate,101)
	END),-- view actual delivery date
	(CASE WHEN sh.DateCreated = '1753-01-01 00:00:00.000' 
		THEN '' 
		ELSE CONVERT(nvarchar(20),sh.DateCreated,101) 
	END),--View Date Created
	(CASE WHEN (si.InvoiceDate) = '1753-01-01 00:00:00.000' 
		THEN '' 
		ELSE CONVERT(nvarchar(20),(si.InvoiceDate),101) 
	END),--View Invoice Date
	
	(CASE WHEN ra.Id IS NULL OR scs.FreightCharges = 0 
			 THEN 0
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalFreightMarkupIsPercent = 1	
		     THEN (scs.FreightCharges / (1+(sh.ResellerAdditionalFreightMarkup/100)))
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalFreightMarkupIsPercent = 0   
			 THEN scs.FreightCharges - sh.ResellerAdditionalFreightMarkup
		  ELSE 0
	END), -- FreightChargesReseller,
	(CASE WHEN ra.Id IS NULL OR scs.FuelCharges = 0 
			 THEN 0
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalFuelMarkupIsPercent = 1	
		     THEN (scs.FuelCharges / (1+(sh.ResellerAdditionalFuelMarkup/100)))
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalFuelMarkupIsPercent = 0   
			 THEN scs.FuelCharges - sh.ResellerAdditionalFuelMarkup
		  ELSE 0
	END), -- FuelChargesReseller
	(CASE WHEN ra.Id IS NULL OR scs.AccessorialCharges = 0 
			 THEN 0
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalAccessorialMarkupIsPercent = 1	
		     THEN (scs.AccessorialCharges / (1+(sh.ResellerAdditionalAccessorialMarkup/100)))
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalAccessorialMarkupIsPercent = 0   
			 THEN scs.AccessorialCharges - sh.ResellerAdditionalAccessorialMarkup
		  ELSE 0
	END), --AS AccessorialChargesReseller
	(CASE WHEN ra.Id IS NULL OR scs.ServiceCharges = 0 
			 THEN 0
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalServiceMarkupIsPercent = 1	
		     THEN (scs.ServiceCharges / (1+(sh.ResellerAdditionalServiceMarkup/100)))
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalServiceMarkupIsPercent = 0   
			 THEN scs.ServiceCharges - sh.ResellerAdditionalServiceMarkup
		  ELSE 0
	END), --AS ServiceChargesReseller
	
	((CASE WHEN ra.Id IS NULL OR scs.FreightCharges = 0 
			 THEN 0
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalFreightMarkupIsPercent = 1	
		     THEN (scs.FreightCharges / (1+(sh.ResellerAdditionalFreightMarkup/100)))
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalFreightMarkupIsPercent = 0   
			 THEN scs.FreightCharges - sh.ResellerAdditionalFreightMarkup
		  ELSE 0
	END) +
	(CASE WHEN ra.Id IS NULL OR scs.FuelCharges = 0 
			 THEN 0
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalFuelMarkupIsPercent = 1	
		     THEN (scs.FuelCharges / (1+(sh.ResellerAdditionalFuelMarkup/100)))
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalFuelMarkupIsPercent = 0   
			 THEN scs.FuelCharges - sh.ResellerAdditionalFuelMarkup
		  ELSE 0
	END) +
	(CASE WHEN ra.Id IS NULL OR scs.AccessorialCharges = 0 
			 THEN 0
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalAccessorialMarkupIsPercent = 1	
		     THEN (scs.AccessorialCharges / (1+(sh.ResellerAdditionalAccessorialMarkup/100)))
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalAccessorialMarkupIsPercent = 0   
			 THEN scs.AccessorialCharges - sh.ResellerAdditionalAccessorialMarkup
		  ELSE 0
	END) +
	(CASE WHEN ra.Id IS NULL OR scs.ServiceCharges = 0 
			 THEN 0
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalServiceMarkupIsPercent = 1	
		     THEN (scs.ServiceCharges / (1+(sh.ResellerAdditionalServiceMarkup/100)))
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalServiceMarkupIsPercent = 0   
			 THEN scs.ServiceCharges - sh.ResellerAdditionalServiceMarkup
		  ELSE 0
	END)), --AS TotalResellerCharges
	
	ISNULL(ss.ShipmentStatusText, '') AS 'Status',
	(CASE 
		WHEN sh.ActualPickupDate = '1753-01-01 00:00:00.000' AND  CONVERT(date,GETDATE()) > shd.OriginalEstimatedPickupDate THEN 'No'
		WHEN sh.ActualPickupDate > '1753-01-01 00:00:00.000' AND  CONVERT(date,sh.ActualPickupDate) > shd.OriginalEstimatedPickupDate THEN 'No' 
		ELSE 'Yes' END) 'PICKUP ON TIME',
	(CASE 
		WHEN sh.ActualDeliveryDate = '1753-01-01 00:00:00.000' AND CONVERT(date, GETDATE()) > shd.OriginalEstimatedDeliveryDate THEN 'No'
		WHEN sh.ActualDeliveryDate > '1753-01-01 00:00:00.000' AND CONVERT(date, sh.ActualDeliveryDate) > shd.OriginalEstimatedDeliveryDate THEN 'No' 
		ELSE 'Yes' END) 'DELIVERY ON TIME',
	ISNULL(sm.ServiceModeText, '') AS 'ServiceMode',
	(CASE WHEN si.InvoiceNumber IS NULL THEN '' ELSE si.InvoiceNumber END), --INVOICE NUMBER
	(CASE WHEN (sh.[Status] = 3 OR sh.[Status] = 4) THEN 0 ELSE DATEDIFF(day, sh.DateCreated, GETDATE() )END) -- DAYS UNINVOICED
	,(CASE 
		WHEN sh.ActualDeliveryDate = '1753-01-01 00:00:00.000' OR si.PostDate = '1753-01-01 00:00:00.000' OR si.PostDate IS NULL THEN -1
		ELSE DATEDIFF(dd, sh.ActualDeliveryDate,si.PostDate)
	 END) AS ' DaysFromDeliveredToInvoicePosting'
	 ,(CASE WHEN si.PostDate = '1753-01-01 00:00:00.000' OR  si.PostDate IS NULL THEN -1 
	 ELSE DATEDIFF(dd, sh.DateCreated, si.PostDate) END) 'Days from Ship Created to Inv Posting'
	 ,(SELECT COUNT(*) FROM ShipmentVendor shvc WHERE shvc.ShipmentId = sh.id ) as 'Vendor Count'
	 ,Isnull(ShipmentEquipmentType.EquipmentTypeCode,'') 'EquipmentTypeCode'
	 ,IsNull(ShipmentEquipmentType.EquipmentTypeName,'') 'EquipmentTypeName'
	 ,Isnull(ShipmentEquipmentType.EquipmentTypeGroup,'') 'EquipmentTypeGroup'	
	,sh.OriginalRateValue 
	,sh.VendorDiscountPercentage 
	,sh.VendorFreightFloor
	,sh.VendorFreightCeiling
	,sh.VendorFuelPercent
	,sh.CustomerFreightMarkupValue
	,sh.CustomerFreightMarkupPercent
	,sh.UseLowerCustomerFreightMarkup
	,sh.RatedCubicFeet
	,sh.ApplyDiscount
	,sh.LineHaulProfitAdjustmentRatio
	,sh.FuelProfitAdjustmentRatio
	,sh.AccessorialProfitAdjustmentRatio
	,sh.ServiceProfitAdjustmentRatio
	,sh.LinearFootRuleBypassed
	,sh.DirectPointRate
	,sh.ResellerAdditionalFreightMarkup
	,sh.ResellerAdditionalFreightMarkupIsPercent
	,sh.ResellerAdditionalFuelMarkup
	,sh.ResellerAdditionalFuelMarkupIsPercent
	,sh.ResellerAdditionalAccessorialMarkup
	,sh.ResellerAdditionalAccessorialMarkupIsPercent
	,sh.ResellerAdditionalServiceMarkup
	,sh.ResellerAdditionalServiceMarkupIsPercent
	,sh.SmallPackType
	,ISNULL(spe.SmallPackageEngineText, '') AS 'SmallPackEngine'
	,sh.IsLTLPackageSpecificRate
	,sh.BilledWeight
	,sh.RatedWeight
	,sh.CareOfAddressFormatEnabled
	,Cast((Case 
		When (Select Count(*) From ShipmentAutoRatingAccessorial sara Where sara.ShipmentId = sh.Id) > 0 Then 1
		Else 0
	End) as bit) 'AutoRatingAccessorialsPresent'
	,sh.IsGuaranteedDeliveryService
	,sh.GuaranteedDeliveryServiceTime
	,sh.GuaranteedDeliveryServiceRate
	,ISNULL(rt.RateTypeText, '') AS  'GuaranteedDeliveryServiceRateType'
	,sh.InDispute
	,ISNULL(idr.InDisputeReasonText, '') AS  'InDisputeReason'
	,c.CustomField1
	,c.CustomField2
	,c.CustomField3
	,c.CustomField4
	,shd.WasInDispute
	,shd.OriginalEstimatedDeliveryDate
	,shd.OriginalEstimatedPickupDate
	,(CASE WHEN shd.OriginalEstimatedDeliveryDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),shd.OriginalEstimatedDeliveryDate,101) END) 'ViewOriginalEstimatedDeliveryDate'
	,(CASE WHEN shd.OriginalEstimatedPickupDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),shd.OriginalEstimatedPickupDate,101) END) 'ViewOriginalEstimatedPickupDate'
	
FROM 
	Shipment sh 
		INNER JOIN ShipmentVendor shv ON shv.ShipmentId = sh.Id AND shv.[Primary] = 1
		INNER JOIN Vendor v ON shv.VendorId = v.Id
		INNER JOIN ShipmentLocation shlo ON sh.OriginId = shlo.Id
		INNER JOIN Country shloc ON shloc.Id = shlo.CountryId
		INNER JOIN ShipmentLocation shld ON sh.DestinationId = shld.Id
		INNER JOIN Country shldc ON shldc.Id = shld.CountryId
		INNER JOIN ShipmentAccountBucket shab ON sh.Id = shab.ShipmentId AND shab.[Primary] = 1
		INNER JOIN AccountBucket ab ON shab.AccountBucketId = ab.Id 
		INNER JOIN Customer c ON sh.CustomerId = c.Id
		INNER JOIN [User] u ON sh.UserId = u.Id
		INNER JOIN ShipmentChargeStatistics scs ON scs.ShipmentId = sh.Id
		INNER JOIN Tier t ON t.Id = c.TierId
		INNER JOIN ShipmentHistoricalData  shd on shd.ShipmentId = sh.Id
		LEFT JOIN PostalCode pco ON shlo.PostalCode = pco.Code AND shlo.CountryId = pco.CountryId AND pco.[Primary] = 1
		LEFT JOIN PostalCode pcd ON shld.PostalCode = pcd.Code AND shld.CountryId = pcd.CountryId AND pcd.[Primary] = 1
		LEFT JOIN FailureCode fc ON fc.Id = shv.FailureCodeId
		LEFT JOIN Prefix p ON p.Id = sh.PrefixId
		LEFT JOIN SalesRepresentative sr ON sh.SalesRepresentativeId = sr.Id 
		LEFT JOIN ShipmentItemStatistics sis ON sis.ShipmentId = sh.Id
		LEFT JOIN dbo.ShipmentInvoices si ON sh.Id = si.ShipmentId
		LEFT JOIN ResellerAddition ra ON ra.Id = sh.ResellerAdditionId
		LEFT JOIN AccountBucketUnit abu ON abu.Id = sh.AccountBucketUnitId
		LEFT JOIN [USER] sc ON sc.Id = sh.ShipmentCoordinatorUserId
		LEFT JOIN [USER] cc ON cc.Id = sh.CarrierCoordinatorUserId
		LEFT JOIN ShipmentAsset sa ON sa.ShipmentId = sh.Id AND sa.[Primary] = 1
		LEFT JOIN Asset da ON da.Id = sa.DriverAssetId
		LEFT JOIN Asset tcta ON tcta.Id = sa.TractorAssetId
		LEFT JOIN Asset tlra ON tlra.Id = sa.TrailerAssetId
		LEFT JOIN ShipmentPriority sp ON sp.Id = sh.ShipmentPriorityId
		
		LEFT JOIN (Select 
						ShipmentId
						,TypeName 'EquipmentTypeName'
						,Code 'EquipmentTypeCode'
						,GroupText 'EquipmentTypeGroup'
					From 
						(Select
							ShipmentId 
							,Min(EquipmentTypeId) 'EquipmentTypeId'
						From ShipmentEquipment 
						Group By ShipmentId) as SingleShipmentEquipment
					Inner Join EquipmentTypeViewSearch et on et.Id = SingleShipmentEquipment.EquipmentTypeId
				) as ShipmentEquipmentType on ShipmentEquipmentType.ShipmentId = sh.Id 
		LEFT JOIN RefCustomerType ct ON ct.CustomerTypeIndex = c.CustomerType
		LEFT JOIN RefShipmentStatus ss ON ss.ShipmentStatusIndex = sh.[Status]
		LEFT JOIN RefServiceMode sm ON sm.ServiceModeIndex = sh.ServiceMode
		LEFT JOIN RefSmallPackageEngine spe ON spe.SmallPackageEngineIndex = sh.SmallPackageEngine
		LEFT JOIN RefRateType rt ON rt.RateTypeIndex = sh.GuaranteedDeliveryServiceRateType
		LEFT JOIN RefInDisputeReason idr ON idr.InDisputeReasonIndex = sh.InDisputeReason
		
WHERE 
	1=1
OPTION (RECOMPILE)