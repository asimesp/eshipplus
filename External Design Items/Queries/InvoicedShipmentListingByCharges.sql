declare @TenantId bigint = 4
declare @UserId bigint = 2
declare @CustomerGroupId bigint = 0
declare @VendorGroupId bigint = 0


SELECT
	sh.ShipmentNumber,
	shv.ProNumber,
	sh.PurchaseOrderNumber,
	sh.DesiredPickupDate,
	sh.ActualPickupDate,
	sh.EstimatedDeliveryDate,
	sh.ActualDeliveryDate,
	sh.DateCreated,
	sh.ShipperReference,
	(CASE
		WHEN sh.AuditedForInvoicing = 0 THEN 'No'
		ELSE 'Yes'
	END),
	c.CustomerNumber,
	c.Name,
	ISNULL(sm.ServiceModeText, '') AS 'ServiceMode',
	(CASE WHEN si.InvoiceNumber IS NULL THEN '' ELSE si.InvoiceNumber END), 

	shlo.[Description],
	shlo.Street1,
	shlo.Street2,
	shlo.City,
	shlo.[State],
	shlo.PostalCode,
	shloc.Code,
	shloc.Name,
	ISNULL(pco.City, '') 'Standard Shipper City Name',
	ISNULL(pco.[State], '') 'Standard Shipper State Name',
	shld.[Description],
	shld.Street1,
	shld.Street2,
	shld.City,
	shld.[State],
	shld.PostalCode,
	shldc.Code,
	shldc.Name,
	ISNULL(pcd.City, '') 'Standard Consignee City Name',
	ISNULL(pcd.[State], '') 'Standard Consignee State Name',
	v.VendorNumber,
	v.Name,
	v.Scac,
	v.DOT,
	v.MC,
	sis.TotalPieceCount,
	sis.TotalWeight,
	sis.HighestFreightClass,

	(CASE WHEN sh.DateCreated = '1753-01-01 00:00:00.000' 
		THEN '' 
		ELSE CONVERT(nvarchar(20),sh.DateCreated,101) 
	END),--View Date Created
	
	cc.Code as 'ChargeCode',
	cc.Category as 'ChargeCategory',
	cc.[Description] as 'ChargeDescription',
	sc.Quantity as 'ChargeQuantity',
	sc.UnitBuy,
	sc.UnitSell,
	sc.UnitDiscount,
	(sc.UnitSell - sc.UnitDiscount) * sc.Quantity as 'Charge amount'

FROM 
	Shipment sh 
		INNER JOIN ShipmentVendor shv ON shv.ShipmentId = sh.Id AND shv.[Primary] = 1
		INNER JOIN Vendor v ON shv.VendorId = v.Id
		INNER JOIN ShipmentLocation shlo ON sh.OriginId = shlo.Id
		INNER JOIN Country shloc ON shloc.Id = shlo.CountryId
		INNER JOIN ShipmentLocation shld ON sh.DestinationId = shld.Id
		INNER JOIN Country shldc ON shldc.Id = shld.CountryId
		INNER JOIN ShipmentAccountBucket shab ON sh.Id = shab.ShipmentId AND shab.[Primary] = 1
		INNER JOIN AccountBucket ab ON shab.AccountBucketId = ab.Id 
		INNER JOIN Customer c ON sh.CustomerId = c.Id
		INNER JOIN [User] u ON sh.UserId = u.Id
		LEFT JOIN ShipmentCharge sc on sh.Id = sc.ShipmentId
		LEFT JOIN RefServiceMode sm ON sm.ServiceModeIndex = sh.ServiceMode
		LEFT JOIN dbo.ShipmentInvoices si ON sh.Id = si.ShipmentId
		LEFT JOIN PostalCode pco ON shlo.PostalCode = pco.Code AND shlo.CountryId = pco.CountryId AND pco.[Primary] = 1
		LEFT JOIN PostalCode pcd ON shld.PostalCode = pcd.Code AND shld.CountryId = pcd.CountryId AND pcd.[Primary] = 1
		LEFT JOIN ShipmentItemStatistics sis ON sis.ShipmentId = sh.Id
		INNER JOIN ChargeCode cc on sc.ChargeCodeId = cc.Id

WHERE Sh.DateCreated > '7/11/2017' and
	@TenantId <> 0 
              AND @UserId <> 0
              AND sh.TenantId = @TenantId
              AND (
				(Select COUNT(*) from [User] where id = @UserId and TenantEmployee = 1) > 0
				OR
				(sh.CustomerId in (SELECT CustomerId FROM UserShipAs WHERE TenantId = @TenantId and UserId = @UserId
					UNION
					SELECT [User].CustomerId FROM [User] WHERE Id = @UserId)))
			  AND (@CustomerGroupId = 0 OR (Select COUNT(*) from CustomerGroupMap WHERE CustomerId = sh.CustomerId and CustomerGroupId = @CustomerGroupId) > 0)
			  AND (@VendorGroupId = 0 OR (Select COUNT(*) from VendorGroupMap WHERE VendorId = shv.VendorId and VendorGroupId = @VendorGroupId) > 0)
