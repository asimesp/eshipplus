declare @TenantId bigint = 4
declare @CustomerGroupId bigint = 0

SELECT
	i.InvoiceNumber,
	it.InvoiceTypeText AS 'InvoiceType',
	ISNULL(p.Code,'') AS 'InvoicePrefixCode',
	c.CustomerNumber,
	c.Name AS 'CustomerName',
	dt.DetailReferenceTypeText AS 'InvoiceDetailReferenceType',
	id.ReferenceNumber AS 'InvoiceDetailReferenceNumber',
	id.Comment AS 'InvoiceDetailChargeComment',
	id.UnitSell AS 'InvoiceDetailUnitSell',
	id.UnitDiscount AS 'InvoiceDetailUnitDiscount',
	id.Quantity AS 'InvoiceDetailQuantity',
	cc.Code AS 'InvoiceDetailChargeCode',
	cc.[Description] AS 'InvoiceDetailChargeDescription',
	(CASE
		WHEN dt.DetailReferenceTypeIndex = 0 THEN s.MiscField1 -- Shipment
		WHEN dt.DetailReferenceTypeIndex = 1 THEN st.ExternalReference1 -- Service Ticket
		WHEN dt.DetailReferenceTypeIndex = 2 THEN '' -- Miscellaneous
	END)  AS 'MiscReference1',
	(CASE
		WHEN dt.DetailReferenceTypeIndex = 0 THEN s.MiscField2 -- Shipment
		WHEN dt.DetailReferenceTypeIndex = 1 THEN st.ExternalReference2 -- Service Ticket
		WHEN dt.DetailReferenceTypeIndex = 2 THEN '' -- Miscellaneous
	END)  AS 'MiscReference2'
FROM Invoice i
INNER JOIN InvoiceDetail id ON id.InvoiceId = i.Id
INNER JOIN CustomerLocation cl ON cl.Id = i.CustomerLocationId
INNER JOIN Customer c ON c.Id = cl.CustomerId
INNER JOIN ChargeCode cc ON cc.Id = id.ChargeCodeId
INNER JOIN RefDetailReferenceType dt ON dt.DetailReferenceTypeIndex = id.ReferenceType
INNER JOIN RefInvoiceType it ON it.InvoiceTypeIndex = i.InvoiceType
LEFT JOIN Prefix p ON p.Id = i.PrefixId
LEFT JOIN Shipment s ON s.ShipmentNumber = id.ReferenceNumber AND dt.DetailReferenceTypeIndex = 0
LEFT JOIN ServiceTicket st ON st.ServiceTicketNumber = id.ReferenceNumber AND dt.DetailReferenceTypeIndex = 1
WHERE 
	i.TenantId = @TenantId
	AND (@CustomerGroupId = 0 OR (Select COUNT(*) from CustomerGroupMap WHERE CustomerId = c.Id and CustomerGroupId = @CustomerGroupId) > 0) 