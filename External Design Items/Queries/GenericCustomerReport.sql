DECLARE @TenantId bigint = 4;

SELECT
	 c.CustomerNumber,
	 c.Name,
	 ab.Code,
	 ab.[Description],
	 p.Code,
	 p.[Description],
	 ISNULL(ms.Code,''),
	 c.Active,
	 c.InvoiceTerms, 
	 c.InvoiceRequiresCarrierProNumber,
	 c.InvoiceTerms,
	 c.InvoiceRequiresCarrierProNumber,
	 c.InvoiceRequiresShipperReference,
	 c.InvoiceRequiresControlAccount,
	 c.ShipmentRequiresPurchaseOrderNumber,
	 c.ValidatePurchaseOrderNumber,
	 c.ShipmentRequiresNMFC,
	 c.DateCreated,
	 c.EnableShipperBillOfLading,
	 c.CreditLimit,
	 ISNULL(sr.SalesRepresentativeNumber,''),
	 ISNULL(sr.Name,''),
	 t.TierNumber,
	 t.Name,
	 ISNULL(cr.BillReseller,0),
	 ISNULL(cr.NoLineHaulProfit,0),
	 ISNULL(cr.NoFuelProfit,0),
	 ISNULL(cr.NoAccessorialProfit,0),
	 ISNULL(cr.NoServiceProfit,0),
	 
	 --Customer Rating LineHaul Ceiling
	 ISNUll(cr.LineHaulProfitCeilingValue,0),			
	 ISNULL(cr.LineHaulProfitCeilingPercentage,0),		
	 ISNULL(cr.UseLineHaulMinimumCeiling,0),
	 --Customer Rating Fuel Ceiling
	 ISNUll(cr.FuelProfitCeilingValue,0),				
	 ISNULL(cr.FuelProfitCeilingPercentage,0),
	 ISNULL(cr.UseFuelMinimumCeiling,0),
	  --Customer Rating Accessorial Ceiling
	 ISNUll(cr.AccessorialProfitCeilingValue,0),		
	 ISNULL(cr.AccessorialProfitCeilingPercentage,0),
	 ISNULL(cr.UseAccessorialMinimumCeiling,0),
	  --Customer Rating Service Ceiling
	 ISNUll(cr.ServiceProfitCeilingValue,0),			
	 ISNULL(cr.ServiceProfitCeilingPercentage,0),
	 ISNULL(cr.UseServiceMinimumCeiling,0),
	
	 --Customer Rating LineHaul Floor
	 ISNUll(cr.LineHaulProfitFloorValue,0),			
	 ISNULL(cr.LineHaulProfitFloorPercentage,0),		
	 ISNULL(cr.UseLineHaulMinimumFloor,0),
	 --Customer Rating Fuel Floor
	 ISNUll(cr.FuelProfitFloorValue,0),				
	 ISNULL(cr.FuelProfitFloorPercentage,0),
	 ISNULL(cr.UseFuelMinimumFloor,0),
	  --Customer Rating Accessorial Floor
	 ISNUll(cr.AccessorialProfitFloorValue,0),		
	 ISNULL(cr.AccessorialProfitFloorPercentage,0),
	 ISNULL(cr.UseAccessorialMinimumFloor,0),
	  --Customer Rating Service Profits
	 ISNUll(cr.ServiceProfitFloorValue,0),			
	 ISNULL(cr.ServiceProfitFloorPercentage,0),
	 ISNULL(cr.UseServiceMinimumFloor,0),
	 
	 --LTL Sell Rate Summary Info
	 ISNULL( lsrs.[AvgMarkupPercent],0),
	 ISNULL(lsrs.[MaxMarkupPercent], 0),
	 ISNULL(lsrs.AvgMarkupValue,0),
	 ISNULL(lsrs.MinMarkupValue,0),
	 ISNULL(lsrs.[Count],0),
	 
	 --Customer Markup Summary Info
	 ISNULL(csms.AvgMarkupPercent, 0),
	 ISNULL(csms.MaxMarkupPercent, 0),
	 ISNULL(csms.AvgMarkupValue, 0),
	 ISNULL(csms.MinMarkupValue, 0),
	 ISNULL(csms.[Count], 0),
	 ISNULL(csms.MinServiceChargeFloor, 0),
	 ISNULL(csms.MaxServiceChargeFloor, 0),
	 ISNULL(csms.AvgServiceChargeFloor, 0),
	 ISNULL(csms.MinServiceChargeCeiling, 0),
	 ISNULL(csms.MaxServiceChargeCeiling, 0),
	 ISNULL(csms.AvgServiceChargeCeiling, 0),
   
	ISNULL(ct.CustomerTypeText, '') AS 'CustomerType',
    
    (CASE
		WHEN cr.LineHaulProfitCeilingType IS NULL THEN ''
		WHEN cr.LineHaulProfitCeilingType =  0 THEN 'Value'
		WHEN cr.LineHaulProfitCeilingType = 1 THEN 'Percentage'
		WHEN cr.LineHaulProfitCeilingType = 2 THEN 'Both'
    END)'LineHaulCeilingType',
    
    (CASE
		WHEN cr.FuelProfitCeilingType IS NULL THEN ''
		WHEN cr.FuelProfitCeilingType =  0 THEN 'Value'
		WHEN cr.FuelProfitCeilingType = 1 THEN 'Percentage'
		WHEN cr.FuelProfitCeilingType = 2 THEN 'Both'
    END)'FuelProfitCeilingType',
    
    (CASE
		WHEN cr.AccessorialProfitCeilingType IS NULL THEN ''
		WHEN cr.AccessorialProfitCeilingType =  0 THEN 'Value'
		WHEN cr.AccessorialProfitCeilingType = 1 THEN 'Percentage'
		WHEN cr.AccessorialProfitCeilingType = 2 THEN 'Both'
    END)'AccessorialProfitCeilingType',
    
    (CASE
		WHEN cr.ServiceProfitCeilingType IS NULL THEN ''
		WHEN cr.ServiceProfitCeilingType =  0 THEN 'Value'
		WHEN cr.ServiceProfitCeilingType = 1 THEN 'Percentage'
		WHEN cr.ServiceProfitCeilingType = 2 THEN 'Both'
    END)'ServiceProfitCeilingType',
    
    (CASE
		WHEN cr.LineHaulProfitFloorType IS NULL THEN ''
		WHEN cr.LineHaulProfitFloorType =  0 THEN 'Value'
		WHEN cr.LineHaulProfitFloorType = 1 THEN 'Percentage'
		WHEN cr.LineHaulProfitFloorType = 2 THEN 'Both'
    END)'LineHaulFloorType',
    
    (CASE
		WHEN cr.FuelProfitFloorType IS NULL THEN ''
		WHEN cr.FuelProfitFloorType =  0 THEN 'Value'
		WHEN cr.FuelProfitFloorType = 1 THEN 'Percentage'
		WHEN cr.FuelProfitFloorType = 2 THEN 'Both'
    END)'FuelProfitFloorType',
    
    (CASE
		WHEN cr.AccessorialProfitFloorType IS NULL THEN ''
		WHEN cr.AccessorialProfitFloorType =  0 THEN 'Value'
		WHEN cr.AccessorialProfitFloorType = 1 THEN 'Percentage'
		WHEN cr.AccessorialProfitFloorType = 2 THEN 'Both'
    END)'AccessorialProfitFloorType',
    
    (CASE
		WHEN cr.ServiceProfitFloorType IS NULL THEN ''
		WHEN cr.ServiceProfitFloorType =  0 THEN 'Value'
		WHEN cr.ServiceProfitFloorType = 1 THEN 'Percentage'
		WHEN cr.ServiceProfitFloorType = 2 THEN 'Both'
    END)'ServiceProfitFloorType'
    
FROM 
	Customer c 
		LEfT JOIN AccountBucket ab ON c.DefaultAccountBucketId = ab.Id
		LEFT JOIN Prefix p ON p.Id = c.PrefixId
		LEFT JOin MileageSource ms ON ms.Id = c.RequiredMileageSourceId
		LEFT JOIN CustomerRating cr ON c.Id = cr.CustomerId
		LEFT JOIN SalesRepresentative sr ON sr.Id = c.SalesRepresentativeId
		LEFT JOIN Tier t ON c.TierId = t.Id
		LEFT JOIN LTLSellRateSummaries lsrs ON cr.Id = lsrs.CustomerRatingId
		LEFT JOIN CustomerServiceMarkupSummaries csms ON cr.Id = csms.CustomerRatingId
		LEFT JOIN RefCustomerType ct ON ct.CustomerTypeIndex = c.CustomerType
WHERE
	@TenantId = c.TenantId 
	
SELECT COUNT(*) FROM Customer WHERE @TenantId = TenantId