DECLARE @TenantId bigint = 4;
DECLARE @UserId bigint = 3;

SELECT
	g.Name,
	u.Username,
	u.FirstName,
	u.LastName,
	u.Email,
	u.TenantEmployee
FROM
	[Group] g
		INNER JOIN GroupUser gu ON gu.GroupId = g.Id
		INNER JOIN [User] u ON gu.UserId = u.Id
WHERE
	@TenantId <> 0
		AND @UserId <> 0
