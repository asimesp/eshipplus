declare @TenantId bigint = 4
declare @ActiveuserId bigint = 2

select distinct
	s.ShipmentNumber, 
	s.DateCreated,
	(CASE WHEN s.CreatedInError = 0 THEN 'NO' ELSE 'YES' END) AS 'CreatedInError',
	c.Name as 'CustomerName',
	c.CustomerNumber,
	v.Name as 'VendorName',
	v.VendorNumber, 
	(case when sv.[primary] = 1 then 'Yes' else 'No' end) as 'PrimaryVendor',
	q1.[Count] as 'ProUsageCount',
	sv.ProNumber	
from ShipmentVendor sv
	inner join Shipment s on sv.ShipmentId = s.Id
	inner join Customer c on s.CustomerId = c.Id
	inner join Vendor v on sv.VendorId = v.Id
	inner join (select pronumber, vendorId, COUNT(ShipmentId) as 'Count'
					from ShipmentVendor
					where ProNumber is not null and ProNumber <> ''
					group by ProNumber, VendorId
					having COUNT(ShipmentId) > 1
				) as q1
		on q1.ProNumber = sv.ProNumber
where
	s.TenantId  = @TenantId
	AND (
		(SELECT COUNT(*) FROM [User] WHERE Id = @ActiveUserId and TenantEmployee = 1) > 0
		OR
		(s.CustomerId IN 
			(SELECT CustomerId FROM UserShipAs WHERE TenantId = @TenantId AND UserId = @ActiveUserId
			 UNION
			 SELECT [User].CustomerId FROM [User] WHERE Id = @ActiveUserId))) 