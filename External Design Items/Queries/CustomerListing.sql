DECLARE @TenantId bigint = 4;
DECLARE @UserId bigint = 3;

SELECT 
	c.CustomerNumber,
	c.Name,
	ISNULL(ct.CustomerTypeText, '') AS 'CustomerType',
	(CASE WHEN c.Active = 0 THEN 'False' ELSE  'True' END),
	c.DateCreated,
	(CASE 
		WHEN c.DateCreated = '1753-01-01 00:00:00.000' THEN '' 
		ELSE CONVERT(nvarchar(20),c.DateCreated,101) 
	END),--view date created
	c.InvoiceTerms,
	c.CreditLimit,
	c.AuditInstructions,
	c.TruckloadPickupTolerance,
	c.TruckloadDeliveryTolerance,
	(CASE WHEN c.ShipmentRequiresNmfc = 0 THEN 'No' ELSE 'Yes' END) AS 'ShipmentRequiresNmfc',
	cl.Street1,
	cl.Street2,
	cl.City,
	cl.[State],
	cl.PostalCode,
	clc.Name,
	ISNULL(cc.Name,''),
	ISNULL(cc.Phone,''),
	ISNULL(cc.Mobile,''),
	ISNULL(cc.Fax,''),
	ISNULL(cc.Email,''),
	t.Name,
	t.TierNumber,
	ISNULL(r.SalesRepresentativeNumber,''),
	ISNULL(r.Name,''),
	ab.Code,
	ab.[Description],
    (SELECT TOP 1 * from CustomerOutstandingBalance(c.Id)) 'Outstanding',
	c.CustomField1,
	c.CustomField2,
	c.CustomField3,
	c.CustomField4,
	c.Notes,
	c.CanPayByCreditCard
FROM Customer c
	INNER JOIN CustomerLocation cl ON cl.CustomerId = c.Id AND cl.[Primary] = 1
	INNER JOIN Country clc ON clc.Id = cl.CountryId
	INNER JOIN Tier t ON t.Id = c.TierId
	INNER JOIN AccountBucket ab ON ab.Id = c.DefaultAccountBucketId
	LEFT JOIN CustomerContact cc ON cc.CustomerLocationId = cl.Id AND (cc.[Primary] IS NULL OR cc.[Primary] = 1)
	LEFT JOIN SalesRepresentative r ON r.Id = c.SalesRepresentativeId
	LEFT JOIN Prefix p ON p.Id = c.PrefixId
	LEFT JOIN MileageSource ms on ms.Id = c.RequiredMileageSourceId
	LEFT JOIN RefCustomerType ct ON ct.CustomerTypeIndex = c.CustomerType
WHERE 
	c.TenantId = @TenantId
		AND @UserId <> 0

SELECT COUNT(*) FROM Customer WHERE @TenantId = TenantId