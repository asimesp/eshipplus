SELECT COUNT(*) FROM ShipmentItem

declare @TenantId bigint = 4
declare @UserId bigint = 2
declare @CustomerGroupId bigint = 0
declare @VendorGroupId bigint = 0

SELECT
	sh.ShipmentNumber,
	sh.DateCreated,
	CASE WHEN sh.DateCreated = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),sh.DateCreated,101) END,--view date created
	sh.DesiredPickupDate,
	CASE WHEN sh.DesiredPickupDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),sh.DesiredPickupDate,101) END,--view desired pickup date
	sh.ActualPickupDate,
	CASE WHEN sh.ActualPickupDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),sh.ActualPickupDate,101) END,--view actual pickup date
	sh.EstimatedDeliveryDate,
	CASE WHEN sh.EstimatedDeliveryDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),sh.EstimatedDeliveryDate,101) END,--view estimated del date
	sh.ActualDeliveryDate,
	CASE WHEN sh.ActualDeliveryDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),sh.ActualDeliveryDate,101) END,--view actual del date
	sh.LateDelivery,

	scs.AccessorialCharges,
	scs.AccessorialCost,
	scs.FreightCharges,
	scs.FreightCost,
	scs.FuelCharges,
	scs.FuelCost,
	scs.ServiceCharges,
	scs.ServiceCost,
	scs.TotalAmountDue,
	scs.TotalCost,
	scs.TotalCharges,
	
	sh.AccessorialProfitAdjustmentRatio,
	(CASE
		WHEN sh.ApplyDiscount = 0 THEN 'No'
		ELSE 'Yes'
	END),
	sh.BilledWeight,
	(CASE
		WHEN sh.BillReseller = 0 THEN 'No'
		ELSE 'Yes'
	END),
	sh.RatedWeight,
	sh.RatedCubicFeet,
	sh.CriticalBolComments,
	sh.CustomerFreightMarkupPercent,
	sh.CustomerFreightMarkupValue,
	(CASE
		WHEN sh.DeclineInsurance = 0 THEN 'No'
		ELSE 'Yes'
	END),
	(CASE
		WHEN sh.DirectPointRate = 0 THEN 'No'
		ELSE 'Yes'
	End),
	sh.EmptyMileage,
	(CASE
		WHEN sh.EnableQuickPay = 0 THEN 'No'
		ELSE 'Yes'
	END),
	sh.FuelProfitAdjustmentRatio,
	sh.GeneralBolComments,
	(CASE
		WHEN sh.HazardousMaterial = 0 THEN 'No'
		ELSE 'Yes'
	END),
	sh.HazardousMaterialContactEmail,
	sh.HazardousMaterialContactMobile,
	sh.HazardousMaterialContactName,
	sh.HazardousMaterialContactPhone,
	(CASE 
		WHEN sh.HidePrefix = 0 THEN 'No'
		ELSE 'Yes'
	END),
	(CASE
		WHEN sh.IsPartialTruckload = 0 THEN 'No'
		ELSE 'Yes'
	END),
	sh.LineHaulProfitAdjustmentRatio,
	sh.Mileage,
	sh.MiscField1,
	sh.MiscField2,
	sh.OriginalRateValue,
	sh.PurchaseOrderNumber,
	sh.ResellerAdditionalAccessorialMarkup,
	(CASE
		WHEN sh.ResellerAdditionalAccessorialMarkupIsPercent = 0 THEN 'No'
		ELSE 'yes'
	END),
	sh.ResellerAdditionalFreightMarkup,
	(CASE
		WHEN sh.ResellerAdditionalFreightMarkupIsPercent = 0 THEN 'No'
		ELSE 'Yes'
	END),
	sh.ResellerAdditionalFuelMarkup,
	(CASE
		WHEN sh.ResellerAdditionalFuelMarkupIsPercent = 0 THEN 'No'
		ELSE 'Yes'
	END),
	sh.ResellerAdditionalServiceMarkup,
	(CASE
		WHEN sh.ResellerAdditionalServiceMarkupIsPercent = 0 THEN 'No'
		ELSE 'Yes'
	END),
	sh.SalesRepresentativeCommissionPercent,
	ISNULL(sm.ServiceModeText, '') AS 'ServiceMode',
	(CASE WHEN si.InvoiceNumber IS NULL THEN '' ELSE si.InvoiceNumber END) AS 'InvoiceNumber', --INVOICE NUMBER
	sh.ServiceProfitAdjustmentRatio,
	(CASE 
		WHEN sh.ShipmentAutorated = 0 THEN 'No'
		ELSE 'Yes'
	END),
	sh.ShipperBol,
	sh.ShipperReference,
	ISNULL(spe.SmallPackageEngineText, '') AS 'SmallPackageEngine',
	sh.SmallPackType,
	(CASE
		WHEN sh.UseLowerCustomerFreightMarkup = 0 THEN 'No'
		ELSE 'Yes'
	END),
	sh.VendorDiscountPercentage,
	sh.VendorFreightCeiling,
	sh.VendorFreightFloor,
	sh.VendorFuelPercent,
	sh.VendorRatingOverrideAddress,
	shi.ActualFreightClass,
	shi.ActualHeight,
	shi.ActualLength,
	shi.ActualWeight,
	shi.ActualWidth,
	shi.Comment,
	shi.Delivery,
	shi.[Description],
	shi.HTSCode,
	shi.NMFCCode,
	u.Username,
	c.CustomerNumber,
	c.Name,
	t.TierNumber,
	t.Name,
	shol.[Description],
	shol.Street1,
	shol.Street2,
	shol.City,
	shol.[State],
	shol.PostalCode,
	oc.Name,
	shdl.[Description],
	shdl.Street1,
	shdl.Street2,
	shdl.City,
	shdl.[State],
	shdl.PostalCode,
	dc.Name,
	v.VendorNumber,
	v.Name,
	ISNULL(ss.ShipmentStatusText, '') AS 'Status',
	ISNULL((SELECT (et.TypeName + ', ')
				FROM ShipmentEquipment she INNER JOIN EquipmentType et ON she.EquipmentTypeId = et.Id AND she.ShipmentId = sh.Id
				WHERE sh.Id= she.ShipmentId 
				ORDER BY et.Id Desc
				FOR XML PATH('')),'') 'EquipmentTypes',
	ISNULL((SELECT (s.Code + ', ')
				FROM ShipmentService shs INNER JOIN [Service] s ON shs.ServiceId = s.Id AND shs.ShipmentId = sh.Id
				ORDER BY s.Code Desc
				FOR XML PATH('')),'') 'Service Codes',
	ab.Code, 
	ab.[Description],
	c.CustomField1,
	c.CustomField2,
	c.CustomField3,
	c.CustomField4,
	(Case When IsDate(spsiv.SentDate)=1 Then Cast(spsiv.SentDate as date) Else '1753-01-01 00:00:00.000' End), --POD Sent Date
	(Case When IsDate(spsiv.SentDate)=1 Then Cast(Cast(spsiv.SentDate as date) as nvarchar(20)) Else '' End) -- View POD Sent Date

FROM	
	Shipment AS sh
	INNER JOIN
		ShipmentItem AS shi ON shi.ShipmentId = sh.Id
	INNER JOIN 
		[User] AS u ON u.Id = sh.UserId
	INNER JOIN 
		Customer AS c ON c.Id = sh.CustomerId
	INNER JOIN
		Tier as t ON t.Id = c.TierId--check on this
	INNER JOIN 
		ShipmentLocation AS shol ON shol.Id = sh.OriginId
	INNER JOIN 
		Country AS oc ON oc.Id = shol.CountryId
	INNER JOIN 
		ShipmentLocation AS shdl ON shdl.Id = sh.DestinationId
	INNER JOIN 
		Country AS dc ON dc.Id = shdl.CountryId
	INNER JOIN 
		ShipmentVendor AS shv ON shv.ShipmentId = sh.Id 
	INNER JOIN 
		Vendor AS v ON v.Id = shv.VendorId AND shv.[Primary] = 1
	INNER JOIN 
		ShipmentChargeStatistics AS scs ON scs.ShipmentId = sh.Id 
	INNER JOIN 
		ShipmentAccountBucket shab ON sh.Id = shab.ShipmentId AND shab.[Primary] = 1
	INNER JOIN 
		AccountBucket ab ON ab.Id = shab.AccountBucketId
	LEFT JOIN
		RefServiceMode sm ON sm.ServiceModeIndex = sh.ServiceMode
	LEFT JOIN
		RefSmallPackageEngine spe ON spe.SmallPackageEngineIndex = sh.SmallPackageEngine
	LEFT JOIN
		RefShipmentStatus ss ON ss.ShipmentStatusIndex = sh.[Status]
	LEFT JOIN
		ShipmentInvoices si ON si.ShipmentId = sh.Id
	LEFT JOIN
		ShipmentPodSentInfoView spsiv On sh.Id=spsiv.ShipmentId And spsiv.PodSentEntered=1
	
WHERE
	@TenantId <> 0 
              AND @UserId <> 0
              AND sh.TenantId = @TenantId
              AND (
				(Select COUNT(*) from [User] where id = @UserId and TenantEmployee = 1) > 0
				OR
				(sh.CustomerId in (SELECT CustomerId FROM UserShipAs WHERE TenantId = @TenantId and UserId = @UserId
					UNION
					SELECT [User].CustomerId FROM [User] WHERE Id = @UserId)))
			  AND (@CustomerGroupId = 0 OR (Select COUNT(*) from CustomerGroupMap WHERE CustomerId = sh.CustomerId and CustomerGroupId = @CustomerGroupId) > 0)
			  AND (@VendorGroupId = 0 OR (Select COUNT(*) from VendorGroupMap WHERE VendorId = shv.VendorId and VendorGroupId = @VendorGroupId) > 0)
			  
			  AND 	DATEDIFF(MONTH, sh.DateCreated, GETDATE()) <= 6
--AND {1} 
--ORDER BY 
--	{2}