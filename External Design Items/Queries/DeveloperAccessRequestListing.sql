DECLARE @TenantId bigint = 4
DECLARE @UserId bigint = 2

SELECT COUNT(*) FROM DeveloperAccessRequest

SELECT 
	c.CustomerNumber,
	c.Name AS 'CustomerName',
	dar.ContactEmail,
	dar.ContactName,
	dar.DateCreated,
	(CASE WHEN dar.DateCreated = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),dar.DateCreated,101) END) AS 'ViewDateCreated',
	(CASE WHEN dar.AccessGranted = 0 THEN 'No' ELSE 'Yes' END) AS 'DevelopmentAccess',
	(CASE WHEN dar.ProductionAccess = 0 THEN 'No' ELSE 'Yes' END) AS 'ProductionAccess'
FROM
	DeveloperAccessRequest AS dar
	INNER JOIN Customer AS c on c.Id = dar.CustomerId
WHERE
	@TenantId <> 0
	AND @UserId <> 0
	AND @TenantId = dar.TenantId
--		AND {1}
--ORDER BY 