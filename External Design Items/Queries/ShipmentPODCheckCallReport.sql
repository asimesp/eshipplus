declare @TenantId bigint = 4
declare @UserId bigint = 2
declare @CustomerGroupId bigint = 0
declare @VendorGroupId bigint = 0


SELECT
	sh.ShipmentNumber,
	sh.PurchaseOrderNumber,
	sh.DesiredPickupDate,
	sh.ActualPickupDate,
	sh.EstimatedDeliveryDate,
	sh.ActualDeliveryDate,
	sh.DateCreated,
	ISNULL(p.Code,'') 'Prefix Code',
	ISNULL(p.[Description],'') 'Prefix Description',
	shlo.[Description],
	shlo.Street1,
	shlo.Street2,
	shlo.City,
	shlo.[State],
	shlo.PostalCode,
	shloc.Code,
	shloc.Name,
	ISNULL(pco.City, '') 'Standard Shipper City Name',
	ISNULL(pco.[State], '') 'Standard Shipper State Name',
	shld.[Description],
	shld.Street1,
	shld.Street2,
	shld.City,
	shld.[State],
	shld.PostalCode,
	shldc.Code,
	shldc.Name,
	ISNULL(pcd.City, '') 'Standard Consignee City Name',
	ISNULL(pcd.[State], '') 'Standard Consignee State Name',
	v.VendorNumber,
	v.Name,
	v.Scac,
	v.DOT,
	v.MC,
	shv.FailureComments,
	ISNULL(fc.Code,''),
	ISNULL(fc.[Description],''),
	(CASE
		WHEN ISNULL(fc.ExcludeOnScorecard,'') = 0 THEN 'No'
		WHEN ISNULL(fc.ExcludeOnScorecard,'')= 1 THEN 'Yes'
	END),
	ab.Code,
	ab.[Description],
	c.CustomerNumber,
	c.Name,
	t.TierNumber,
	t.Name,
	sh.ShipperReference,
	sis.TotalPieceCount,
	sis.TotalWeight,
	sis.HighestFreightClass,
	ISNULL((SELECT (dt.Code + ', ')
				FROM ShipmentDocument sd, DocumentTag dt
				WHERE sd.ShipmentId = sh.Id AND sd.DocumentTagId = dt.Id 
				ORDER BY sd.Id Asc
				FOR XML PATH('')),''),-- AS 'DocumentTags',
	ISNULL((SELECT (s.Code + ', ')
				FROM ShipmentService shs INNER JOIN [Service] s ON shs.ServiceId = s.Id AND shs.ShipmentId = sh.Id
				ORDER BY s.Code Desc
				FOR XML PATH('')),'') 'Service Codes', --Service Codes
	ISNULL((SELECT ('['+ sr.Name + ': ' + sr.Value + '] ')
				FROM ShipmentReference sr 
				Where sr.ShipmentId = sh.Id
				ORDER BY sr.Name
				FOR XML PATH('')),'') 'Customer References',
	(CASE WHEN sh.ActualDeliveryDate = '1753-01-01 00:00:00.000' 
		THEN '' 
		ELSE CONVERT(nvarchar(20),sh.ActualDeliveryDate,101)
	END),-- view actual delivery date
	(CASE WHEN sh.DateCreated = '1753-01-01 00:00:00.000' 
		THEN '' 
		ELSE CONVERT(nvarchar(20),sh.DateCreated,101) 
	END),--View Date Created
	
	
	ISNULL(ss.ShipmentStatusText, '') AS 'Status',
	ISNULL(sm.ServiceModeText, '') AS 'ServiceMode',
	
	 Isnull(ShipmentEquipmentType.EquipmentTypeCode,'') 'EquipmentTypeCode'
	 ,IsNull(ShipmentEquipmentType.EquipmentTypeName,'') 'EquipmentTypeName'
	 ,Isnull(ShipmentEquipmentType.EquipmentTypeGroup,'') 'EquipmentTypeGroup'	
	
	,isnull(spsiv.PodSentEntered, 0) 'PodSent'
	,isnull(spsiv.LogDate, '1753-01-01 00:00:00') 'PodSentLogEntryDate'
	,isnull(spsiv.SentDate, '1753-01-01 00:00:00') 'PodSentSendDate'
	,isnull(spsiv.Username, '') 'PodSentByUsername'
	,isnull(spsiv.UserFirstName, '') 'PodSentByUserFirstName'
	,isnull(spsiv.UserLastName, '') 'PodSentByUserLastName'
FROM 
	Shipment sh 
		INNER JOIN ShipmentVendor shv ON shv.ShipmentId = sh.Id AND shv.[Primary] = 1
		INNER JOIN Vendor v ON shv.VendorId = v.Id
		INNER JOIN ShipmentLocation shlo ON sh.OriginId = shlo.Id
		INNER JOIN Country shloc ON shloc.Id = shlo.CountryId
		INNER JOIN ShipmentLocation shld ON sh.DestinationId = shld.Id
		INNER JOIN Country shldc ON shldc.Id = shld.CountryId
		INNER JOIN ShipmentAccountBucket shab ON sh.Id = shab.ShipmentId AND shab.[Primary] = 1
		INNER JOIN AccountBucket ab ON shab.AccountBucketId = ab.Id 
		INNER JOIN Customer c ON sh.CustomerId = c.Id
		INNER JOIN Tier t ON t.Id = c.TierId
		LEFT JOIN PostalCode pco ON shlo.PostalCode = pco.Code AND shlo.CountryId = pco.CountryId AND pco.[Primary] = 1
		LEFT JOIN PostalCode pcd ON shld.PostalCode = pcd.Code AND shld.CountryId = pcd.CountryId AND pcd.[Primary] = 1
		LEFT JOIN FailureCode fc ON fc.Id = shv.FailureCodeId
		LEFT JOIN Prefix p ON p.Id = sh.PrefixId
		LEFT JOIN ShipmentItemStatistics sis ON sis.ShipmentId = sh.Id
		
		LEFT JOIN (Select 
						ShipmentId
						,TypeName 'EquipmentTypeName'
						,Code 'EquipmentTypeCode'
						,GroupText 'EquipmentTypeGroup'
					From 
						(Select
							ShipmentId 
							,Min(EquipmentTypeId) 'EquipmentTypeId'
						From ShipmentEquipment 
						Group By ShipmentId) as SingleShipmentEquipment
					Inner Join EquipmentTypeViewSearch et on et.Id = SingleShipmentEquipment.EquipmentTypeId
				) as ShipmentEquipmentType on ShipmentEquipmentType.ShipmentId = sh.Id 
		LEFT JOIN RefCustomerType ct ON ct.CustomerTypeIndex = c.CustomerType
		LEFT JOIN RefShipmentStatus ss ON ss.ShipmentStatusIndex = sh.[Status]
		LEFT JOIN RefServiceMode sm ON sm.ServiceModeIndex = sh.ServiceMode
		LEFT JOIN ShipmentPodSentInfoView spsiv ON spsiv.ShipmentId = sh.Id
WHERE 
	@TenantId <> 0 
              AND @UserId <> 0
              AND sh.TenantId = @TenantId
              AND (
				(Select COUNT(*) from [User] where id = @UserId and TenantEmployee = 1) > 0
				OR
				(sh.CustomerId in (SELECT CustomerId FROM UserShipAs WHERE TenantId = @TenantId and UserId = @UserId
					UNION
					SELECT [User].CustomerId FROM [User] WHERE Id = @UserId)))
			  AND (@CustomerGroupId = 0 OR (Select COUNT(*) from CustomerGroupMap WHERE CustomerId = sh.CustomerId and CustomerGroupId = @CustomerGroupId) > 0)
			  AND (@VendorGroupId = 0 OR (Select COUNT(*) from VendorGroupMap WHERE VendorId = shv.VendorId and VendorGroupId = @VendorGroupId) > 0)
			  AND 1=1
OPTION (RECOMPILE)