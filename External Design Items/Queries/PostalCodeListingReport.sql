Select 
	pc.City
	,pc.CityAlias
	,pc.[State]
	,pc.Code
	,c.Code
	,c.Name
	,pc.Latitude
	,pc.Longitude
	,pc.[Primary]

From PostalCode pc
	Inner Join Country c on c.Id = pc.CountryId