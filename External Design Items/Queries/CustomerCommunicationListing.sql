DECLARE @TenantId bigint = 4;
DECLARE @CustomerGroupId bigint = 0;

SELECT
	cc.ConnectGuid,
	cc.EnableWebServiceAPIAccess,
	c.CustomerNumber,
	c.Name AS 'CustomerName',
	cc.EdiEnabled,
	cc.EdiCode,
	cc.EdiVANUrl,
	cc.SecureEdiVAN,
	cc.EdiVANUsername,
	cc.EdiVANPassword,
	cc.EdiVANDefaultFolder,
	cc.FtpEnabled,
	cc.FtpUrl,
	cc.SecureFtp,
	cc.FtpUsername,
	cc.FtpPassword,
	cc.FtpDefaultFolder,
	cc.StopVendorNotifications,
	cc.EdiVANEnvelopePath,
	cc.UseSelectiveDropOff,
	cc.Pickup997,
	cc.Pickup204,
	cc.AcknowledgePickup204,
	cc.DocDeliveryFtpUrl,
	cc.DocDeliverySecureFtp,
	cc.DocDeliveryFtpUsername,
	cc.DocDeliveryFtpPassword,
	cc.DocDeliveryFtpDefaultFolder,
	cc.DeliverBolDoc,
	cc.DeliverShipmentStatementDoc,
	cc.DeliverInvoiceDoc,
	cc.StartDocDeliveriesFrom,
	cc.DeliverInternalShipmentDocs,
	cc.FtpDocDeliveryEnabled,
	cc.HoldShipmentDocsTillInvoiced,
	cc.FtpDeleteFileAfterPickup,
	cc.EdiDeleteFileAfterPickup,
	cc.EmailDocDeliveryEnabled
FROM CustomerCommunication cc
	INNER JOIN Customer c ON c.Id = cc.CustomerId
WHERE cc.TenantId = @TenantId
	AND (@CustomerGroupId = 0 OR (SELECT COUNT(*) FROM CustomerGroupMap WHERE CustomerId = c.Id AND CustomerGroupId = @CustomerGroupId) > 0)