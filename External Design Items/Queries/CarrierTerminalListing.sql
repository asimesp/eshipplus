SELECT
	COUNT(*)
FROM 
	VendorTerminal
	
DECLARE @TenantId bigint = 4;
DECLARE @UserId bigint = 4;
DECLARE @VendorGroupId bigint = 0;

SELECT	
	(CASE
		WHEN v.Active = 0 THEN 'No'
		ELSE 'Yes'
	END),
	v.VendorNumber,
	v.Name,
	vt.Name,
	vt.City,
	vt.Street1,
	vt.Street2,
	vt.City,
	vt.[State],
	vt.PostalCode,
	c.Name,
	vt.ContactName,
	vt.Phone,
	vt.Fax,
	vt.Mobile,
	vt.Email,
	vt.Comment,
	vt.DateCreated 
FROM 
	VendorTerminal AS vt
	INNER JOIN Vendor AS v ON v.Id = vt.VendorId
	INNER JOIN Country AS c on c.Id = vt.CountryId
WHERE
	@TenantId <> 0
		AND @UserId <> 0
		AND vt.TenantId = @TenantId
		AND (@VendorGroupId = 0 OR (SELECT COUNT(*) FROM VendorGroupMap WHERE VendorId = vt.VendorId AND VendorGroupId = @VendorGroupId) > 0)
		      --AND {1} 
--ORDER BY {2}