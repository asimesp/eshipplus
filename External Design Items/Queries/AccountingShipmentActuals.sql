DECLARE @StartARPostDate date = '20140901';
DECLARE @EndARPostDate date = '20141112';
DECLARE @StartAPPostDate date = '20140901';
DECLARE @EndAPPostDate date = '20141112';
DECLARE @TenantId bigint = 4;
DECLARE @UserId bigint = 2;

SELECT 
	ShipmentNumber
	,ShipmentStatus
	,PostDateApplicableInvoices
	,PostDateApplicableVendorBills
	,DateCreated
	,CreatedInError
	,PrefixCode
	,PrefixDescription
	,CustomerNumber
	,CustomerName
	,VendorNumber
	,VendorName
	,ProNumber
	,AccountBucketCode
	,AccountBucketDescription
	,EstimateTotalCharge
	,EstimateTotalCost
	,TotalActualCost
	,TotalActualCostCredit
	,ActualFreightCost
	,ActualFreightCostCredit
	,ActualFuelCost
	,ActualFuelCostCredit
	,ActualAccessorialCost
	,ActualAccessorialCostCredit
	,ActualServiceCost
	,ActualServiceCostCredit
	,TotalActualCharges
	,TotalSupplementalCharges
	,TotalRevenueCredits
	,ActualFreightCharges
	,ActualFuelCharges
	,ActualAccessorialCharges
	,ActualServiceCharges
	,AccessorialCredit
	,FreightCredit
	,FuelCredit
	,ServiceCredit
	,SupplementalAccessorialCharges
	,SupplementalFreightCharges
	,SupplementalFuelCharges
	,SupplementalServiceCharges
	,VendorCount
	,DocumentTags
	,FinanceNotes
	,AuditInstructions
	,InvoiceRequiresCarrierProNumber
	,InvoiceRequiresControlAccount
	,InvoiceRequiresShipperReference
	,Username
	,FirstName
	,LastName
	,ServiceMode
	,ActualPickupDate
	,ActualPickupDateText
	,DaysUnInvoiced
	,SalesRepresentativeCommissionPercent
	,SalesRepMinCommValue
	,SalesRepMaxCommValue
	,SalesRepAddlEntityName
	,SalesRepAddlEntityCommPercent
	,SalesRepNumber
	,SalesRepName
	,CustomField1
	,CustomField2
	,CustomField3
	,CustomField4
	,ShipperReference
	,PurchaseOrderNumber
FROM
(SELECT DISTINCT 
	sh.ShipmentNumber,
	Isnull(rss.ShipmentStatusText,'') as 'ShipmentStatus',
	ISNULL((SELECT Distinct (i.invoiceNumber + ', ')
		FROM Invoice i, InvoiceDetail d
		WHERE d.InvoiceId = i.Id and d.ReferenceNumber = sh.ShipmentNumber and d.ReferenceType = 0 -- ref 0 = shipment
			AND i.PostDate BETWEEN @StartARPostDate AND @EndARPostDate
		FOR XML PATH('')),'')AS 'PostDateApplicableInvoices',
	ISNULL((SELECT Distinct (b.DocumentNumber + ', ')
		FROM VendorBill b, VendorBillDetail d 
		WHERE d.VendorBillId = b.Id and d.ReferenceNumber = sh.ShipmentNumber and d.ReferenceType = 0 -- ref 0 = shipment
			AND b.PostDate BETWEEN @StartAPPostDate AND @EndAPPostDate
		FOR XML PATH('')),'')AS 'PostDateApplicableVendorBills',
	sh.DateCreated,
	(CASE WHEN sh.CreatedInError = 1 THEN 'Yes' ELSE 'No' END) 'CreatedInError',
	ISNULL(p.Code,'') 'PrefixCode',
	ISNULL(p.[Description],'') 'PrefixDescription',
	c.CustomerNumber,
	c.Name as 'CustomerName',
	v.VendorNumber,
	v.Name as 'VendorName',
	sv.ProNumber,
	ab.Code 'AccountBucketCode',
	ab.[Description] 'AccountBucketDescription',
	scs.TotalNettedCharge 'EstimateTotalCharge',
	scs.TotalCost 'EstimateTotalCost',
	ISNULL(sac.TotalActualCost, 0) 'TotalActualCost',
	ISNULL(sac.TotalActualCostCredit, 0) 'TotalActualCostCredit',
	
	ISNULL(sac.ActualFreightCost, 0) 'ActualFreightCost',
	ISNULL(sac.ActualFreightCostCredit, 0) 'ActualFreightCostCredit',
	ISNULL(sac.ActualFuelCost, 0) 'ActualFuelCost',
	ISNULL(sac.ActualFuelCostCredit, 0) 'ActualFuelCostCredit',
	ISNULL(sac.ActualAccessorialCost, 0) 'ActualAccessorialCost',
	ISNULL(sac.ActualAccessorialCostCredit, 0) 'ActualAccessorialCostCredit',
	ISNULL(sac.ActualServiceCost, 0) 'ActualServiceCost',
	ISNULL(sac.ActualServiceCostCredit, 0) 'ActualServiceCostCredit',
	
	ISNULL(sar.TotalActualCharges, 0) 'TotalActualCharges',
	ISNULL(sar.TotalSupplementalCharges, 0) 'TotalSupplementalCharges',
	ISNULL(sar.TotalCredit, 0) 'TotalRevenueCredits',
	
	ISNULL(sar.ActualFreightCharges, 0) 'ActualFreightCharges',
	ISNULL(sar.ActualFuelCharges, 0) 'ActualFuelCharges',
	ISNULL(sar.ActualAccessorialCharges, 0) 'ActualAccessorialCharges',
	ISNULL(sar.ActualServiceCharges, 0) 'ActualServiceCharges',
	ISNULL(sar.AccessorialCredit, 0) 'AccessorialCredit',
	ISNULL(sar.FreightCredit, 0) 'FreightCredit',
	ISNULL(sar.FuelCredit, 0) 'FuelCredit',
	ISNULL(sar.ServiceCredit, 0) 'ServiceCredit',
	ISNULL(sar.SupplementalAccessorialCharges, 0) 'SupplementalAccessorialCharges',
	ISNULL(sar.SupplementalFreightCharges, 0) 'SupplementalFreightCharges',
	ISNULL(sar.SupplementalFuelCharges, 0) 'SupplementalFuelCharges',
	ISNULL(sar.SupplementalServiceCharges, 0) 'SupplementalServiceCharges',
	(SELECT COUNT(*) FROM ShipmentVendor shvc WHERE shvc.ShipmentId = sh.id ) as 'VendorCount',
	ISNULL((SELECT (dt.Code + ', ')
				FROM ShipmentDocument sd, DocumentTag dt
				WHERE sd.ShipmentId = sh.Id AND sd.DocumentTagId = dt.Id 
				ORDER BY sd.Id Asc
				FOR XML PATH('')),'') AS 'DocumentTags',
	ISNULL((SELECT (sn.[Message] + ', ')
				FROM ShipmentNote sn
				WHERE sn.ShipmentId = sh.Id AND sn.[Type] = 2 
				ORDER BY sn.Id Asc
				FOR XML PATH('')),'') 'FinanceNotes',
	c.AuditInstructions,
	c.InvoiceRequiresCarrierProNumber,
	c.InvoiceRequiresControlAccount,
	c.InvoiceRequiresShipperReference,
	su.Username,
	su.FirstName,
	su.LastName,
	Isnull(rsm.ServiceModeText,'') 'ServiceMode',
	sh.ActualPickupDate,
	(CASE WHEN sh.ActualPickupDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(10),sh.ActualPickupDate,101) END) 'ActualPickupDateText',
	(CASE WHEN (sh.[Status] = 3 OR sh.[Status] = 4) THEN 0 ELSE DATEDIFF(day, sh.DateCreated, GETDATE() )END) 'DaysUnInvoiced',
	sh.SalesRepresentativeCommissionPercent,
	sh.SalesRepMinCommValue,
	sh.SalesRepMaxCommValue,
	sh.SalesRepAddlEntityName,
	sh.SalesRepAddlEntityCommPercent,
	ISNULL(sr.SalesRepresentativeNumber,'') 'SalesRepNumber',
	ISNULL(sr.Name,'') 'SalesRepName',
	c.CustomField1,
	c.CustomField2,
	c.CustomField3,
	c.CustomField4,
	ISNULL(sh.ShipperReference,'') 'ShipperReference',
	ISNULL(sh.PurchaseOrderNumber,'') 'PurchaseOrderNumber'
FROM
	Shipment AS sh
		INNER JOIN [User] AS su on sh.UserId = su.Id
		INNER JOIN Customer AS c ON sh.CustomerId = c.Id
		INNER JOIN ShipmentVendor AS sv ON sh.Id = sv.ShipmentId AND sv.[Primary] = 1
		INNER JOIN Vendor AS v ON sv.VendorId = v.Id 
		INNER JOIN ShipmentChargeStatistics AS scs ON sh.Id = scs.ShipmentId
		INNER JOIN ShipmentAccountBucket AS sab ON sh.Id = sab.ShipmentId AND sab.[Primary] = 1
		INNER JOIN AccountBucket ab ON sab.AccountBucketId = ab.Id
		LEFT JOIN ShipmentActualCosts(@TenantId, @StartAPPostDate, @EndAPPostDate) as sac ON sac.Id = sh.Id
		LEFT JOIN ShipmentActualRevenues(@TenantId, @StartARPostDate, @EndARPostDate) as sar ON sar.Id = sh.Id
		LEFT JOIN Prefix AS p ON sh.PrefixId = p.Id
		LEFT JOIN SalesRepresentative sr ON sh.SalesRepresentativeId = sr.Id 
		LEFT JOIN RefServiceMode rsm on rsm.ServiceModeIndex = sh.ServiceMode
		LEFT JOIN RefShipmentStatus rss on rss.ShipmentStatusIndex = sh.[Status]
WHERE
	sh.TenantId = @TenantId
	AND (
		(SELECT COUNT(*) FROM [User] WHERE Id = @UserId and TenantEmployee = 1) > 0
		OR
		(sh.CustomerId IN 
			(SELECT CustomerId FROM UserShipAs WHERE TenantId = @TenantId AND UserId = @UserId
			 UNION
			 SELECT [User].CustomerId FROM [User] WHERE Id = @UserId)))
)
 as ShipmentActuals

 WHERE ShipmentActuals.DateCreated > '20150101'
 ORDER BY [DateCreated] ASC