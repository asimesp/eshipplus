DECLARE @TenantId bigint = 4;
DECLARE @UserId bigint = 3;

SELECT 
	c.ClaimNumber,
	c.DateCreated,
	c.ClaimDate,
	(CASE WHEN c.[Status] = 0 THEN 'Open'	
		  WHEN c.[Status] = 1 THEN 'Closed Resolved' 
		  WHEN c.[Status] = 2 THEN 'Closed Unresolved' 
		  WHEN c.[Status] = 3 THEN 'Cancelled' 
	END),
	c.ClaimDetail,
	u.Username,
	r.CustomerNumber,
	r.Name
FROM 
	Claim c
		INNER JOIN [User] u ON u.Id = c.UserId
		INNER JOIN Customer r ON r.Id = c.CustomerId
WHERE
	@TenantId <> 0
		AND @UserId <> 0
		AND c.TenantId = @TenantId
		AND (
				(Select COUNT(*) from [User] where id = @UserId and TenantEmployee = 1) > 0
				OR
				(c.CustomerId in (SELECT CustomerId FROM UserShipAs WHERE TenantId = @TenantId and UserId = @UserId
								  UNION
								  SELECT [User].CustomerId FROM [User] WHERE Id = @UserId))
			)

SELECT COUNT(*) FROM Claim WHERE @TenantId = TenantId