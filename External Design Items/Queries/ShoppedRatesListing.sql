DECLARE @TenantId BIGINT = 4
DECLARE @UserId BIGINT = 2
DECLARE @ShowDuplicates BIT = 1
DECLARE @CustomerGroupId BIGINT = 55


If @ShowDuplicates = 0
Begin

	IF EXISTS (SELECT * FROM tempdb.sys.objects WHERE name LIKE '#ratesFilter%')  
		DROP TABLE #ratesFilter
	IF EXISTS (SELECT * FROM tempdb.sys.objects WHERE name LIKE '#ratesFilter2%')  
		DROP TABLE #ratesFilter2

	CREATE TABLE #ratesFilter
	(
		Id bigint
		,Ref nvarchar(50)
		,FilterKey nvarchar(1000)
	)

	CREATE TABLE #ratesFilter2
	(
		Id bigint
		,FilterKey nvarchar(1000)
	)

	-- date with reference numbers
	insert into #ratesFilter (Id, Ref, FilterKey)
		Select
			srvs.Id
			,srvs.ReferenceNumber
			,(
				convert(nvarchar(10), srvs.DateCreated, 112)
					+ convert(nvarchar(10), srvs.UserId)
					+ convert(nvarchar(10), srvs.CustomerId)
					+ srvs.OriginCity
					+ srvs.OriginState
					+ srvs.OriginPostalCode
					+ convert(nvarchar(5), srvs.OriginCountryId)
					+ srvs.DestinationCity
					+ srvs.DestinationState
					+ srvs.DestinationPostalCode
					+ convert(nvarchar(5), srvs.DestinationCountryId)
					+ convert(nvarchar(10), srvs.Quantity)
					+ convert(nvarchar(5), srvs.NumberLineItems)
					+ convert(nvarchar(18), srvs.[Weight])
					+ convert(nvarchar(5), srvs.HighestFreightClass)
					+ convert(nvarchar(5), srvs.MostSignificantFreightClass)
					+ ISNULL((SELECT (s.Code + ',')
						FROM ShoppedRateService srs INNER JOIN [Service] s ON srs.ServiceId = s.Id AND srs.ShoppedRateId =  srvs.Id
						ORDER -- hack to ensure sort is not removed by eShipPlus report engine.  Do no change placement!
						BY s.Code Desc 
						FOR XML PATH('')),'')
			) 
		From 
			ShoppedRateViewSearch2(@TenantId, @UserId) srvs
		Where
			@TenantId <> 0
			And @UserId <> 0
			And (@CustomerGroupId = 0 OR (SELECT COUNT(*) FROM CustomerGroupMap cgm WHERE cgm.CustomerId = srvs.CustomerId AND cgm.CustomerGroupId = @CustomerGroupId) > 0) 
			And srvs.ReferenceNumber <> ''
			And srvs.[Type] in (1, 2) -- 1 = Shipment, 2 = WSBooking
			--And ??? -- additional query items go here!!!

	-- data without reference numbers
	insert into #ratesFilter2 (Id, FilterKey)
		Select
			srvs.Id
			,(
				convert(nvarchar(10), srvs.DateCreated, 112)
					+ convert(nvarchar(10), srvs.UserId)
					+ convert(nvarchar(10), srvs.CustomerId)
					+ srvs.OriginCity
					+ srvs.OriginState
					+ srvs.OriginPostalCode
					+ convert(nvarchar(5), srvs.OriginCountryId)
					+ srvs.DestinationCity
					+ srvs.DestinationState
					+ srvs.DestinationPostalCode
					+ convert(nvarchar(5), srvs.DestinationCountryId)
					+ convert(nvarchar(10), srvs.Quantity)
					+ convert(nvarchar(5), srvs.NumberLineItems)
					+ convert(nvarchar(18), srvs.[Weight])
					+ convert(nvarchar(5), srvs.HighestFreightClass)
					+ convert(nvarchar(5), srvs.MostSignificantFreightClass)
					+ ISNULL((SELECT (s.Code + ',')
						FROM ShoppedRateService srs INNER JOIN [Service] s ON srs.ServiceId = s.Id AND srs.ShoppedRateId =  srvs.Id
						ORDER -- hack to ensure sort is not removed by eShipPlus report engine.  Do no change placement!
						BY s.Code Desc 
						FOR XML PATH('')),'')
			) 
		From 
			ShoppedRateViewSearch2(@TenantId, @UserId) srvs
		Where
			@TenantId <> 0
			And @UserId <> 0
			And (@CustomerGroupId = 0 OR (SELECT COUNT(*) FROM CustomerGroupMap cgm WHERE cgm.CustomerId = srvs.CustomerId AND cgm.CustomerGroupId = @CustomerGroupId) > 0) 
			And (srvs.ReferenceNumber = '' Or srvs.[Type] not in (1, 2)) -- 1 = Shipment, 2 = WSBooking
			--And ??? -- additional query items go here!!!

	-- final data when not allowing duplicates
	Select
		*
	From 
		ShoppedRateViewSearch2(@TenantId, @UserId) srvs
	Where
		srvs.Id in (
			Select min(Id) from #ratesFilter group by Ref
			union
			Select max(Id) from #ratesFilter2 
				where FilterKey not in (Select min(#ratesFilter.FilterKey) from #ratesFilter group by Ref)
				group by FilterKey
		)
	-- Order By goes here

	-- clean up
	DROP TABLE #ratesFilter
	DROP TABLE #ratesFilter2
End
Else
Begin
	-- final data when allow duplicates
	Select
		* 
	From 
		ShoppedRateViewSearch2(@TenantId, @UserId) srvs
	Where
		@TenantId <> 0
		And @UserId <> 0
		And (@CustomerGroupId = 0 OR (SELECT COUNT(*) FROM CustomerGroupMap cgm WHERE cgm.CustomerId = srvs.CustomerId AND cgm.CustomerGroupId = @CustomerGroupId) > 0) 
		-- And additional filters go here
	-- Order By Goes here
End