SELECT 
	COUNT(*)
FROM
	[User]

SELECT 
	COUNT(*) 
FROM 
	UserShipAs

declare @TenantId bigint = 4
declare @UserId bigint = 2
declare @CustomerGroupId bigint = 0

SELECT
	 u.Username,
	 u.FirstName,
	 u.LastName,
	 u.Email,
	 (CASE
		WHEN u.[Enabled] = 0 THEN 'No'
		ELSE 'Yes'
	 END),
	 dc.CustomerNumber,
	 dc.Name,
	  (CASE
		WHEN  dc.Active = 0 THEN 'No'
		ELSE 'Yes'
	 END),
	 ISNULL(usac.CustomerNumber,''),
	 ISNULL(usac.Name,''),
	 ISNULL(
		(CASE
			WHEN usac.Active = 0 THEN 'No'
			WHEN usac.Active = 1 THEN 'Yes'
		END)
	,'')
FROM
	[User] AS u
	INNER JOIN Customer AS dc On dc.Id = u.CustomerId
	LEFT JOIN UserShipAs AS usa ON usa.UserId = u.Id
	LEFT JOIN Customer AS usac ON usac.Id = usa.CustomerId
WHERE
	@TenantId <> 0 
	AND @UserId <> 0
	AND (@CustomerGroupId = 0 OR (Select COUNT(*) from CustomerGroupMap WHERE CustomerId = dc.Id and CustomerGroupId = @CustomerGroupId) > 0)		 
--	AND {1} 
--ORDER BY {2}	