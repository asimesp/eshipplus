Declare @UserId bigint = 2;
Declare @TenantId bigint = 4;
Declare @CustomerGroupId bigint = 0;
Declare @VendorGroupId bigint = 0;

IF EXISTS (SELECT * FROM tempdb.sys.objects WHERE name LIKE '#shipmentStatusReport%')  
		DROP TABLE #shipmentStatusReport

Create Table #shipmentStatusReport
(
	ShipmentNumber nvarchar(50)
	,ShipmentStatus nvarchar (10)
	,MiscField1 nvarchar(100)
	,Quantity int
	,ShipperReference nvarchar(50)
	,Origin nvarchar(50)
	,OriginCity nvarchar(50)
	,OriginPostalCode nvarchar(10)
	,OriginState nvarchar(50)
	,Destination nvarchar(50)
	,DestinationCity nvarchar(50)
	,DestinationState nvarchar(50)
	,DestinationPostalCode nvarchar(10)
	,ItemDescription nvarchar(200)
	,PurchaseOrderNumber nvarchar(50)
	,ServiceMode nvarchar(20)
	,ActualWeight decimal(12,4)
	,DesiredPickupDateText nvarchar(10)
	,ActualDeliveryDate datetime
	,ActualPickupDateText nvarchar(10)
	,EstimatedDeliveryDateText nvarchar(10)
	,CustomerName nvarchar(50)
	,CustomerNumber nvarchar(50)
	,DateCreated datetime
)

Insert Into #shipmentStatusReport(ShipmentNumber,ShipmentStatus,MiscField1,Quantity,ShipperReference,Origin,OriginCity,OriginPostalCode,OriginState
											,Destination,DestinationCity,DestinationState,DestinationPostalCode,ItemDescription,PurchaseOrderNumber,ServiceMode 
											,ActualWeight,DesiredPickupDateText,ActualDeliveryDate,ActualPickupDateText,EstimatedDeliveryDateText, CustomerName, CustomerNumber, DateCreated)
	Select * From
		(Select
			sh.ShipmentNumber
			,Isnull(rss.ShipmentStatusText,'') as 'ShipmentStatus'
			,sh.MiscField1
			,shi.Quantity
			,sh.ShipperReference
			,shol.[Description] 'Origin'
			,shol.City 'OriginCity'
			,shol.PostalCode 'OriginPostalCode'
			,shol.[State] 'OriginState'
			,shdl.[Description] 'Destination'
			,shdl.City 'DestinationCity'
			,shdl.[State] 'DestinationState'
			,shdl.PostalCode 'DestinationPostalCode'
			,shi.[Description] 'ItemDescription'
			,sh.PurchaseOrderNumber
			,Isnull(rsm.ServiceModeText,'') 'ServiceMode'
			,shi.ActualWeight
			,(CASE WHEN sh.DesiredPickupDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(10),sh.DesiredPickupDate,121) END) 'DesiredPickupDateText'
			,sh.ActualDeliveryDate
			,(CASE WHEN sh.ActualPickupDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(10),sh.ActualPickupDate,121) END) 'ActualPickupDateText'
			,(CASE WHEN sh.EstimatedDeliveryDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(10),sh.EstimatedDeliveryDate,121) END) 'EstimatedDeliveryDateText'
			,c.Name 'CustomerName'
			,c.CustomerNumber 'CustomerNumber'
			,sh.DateCreated
		From Shipment sh
			Inner Join ShipmentItem AS shi ON shi.ShipmentId = sh.Id
			Inner Join ShipmentLocation AS shol ON shol.Id = sh.OriginId
			Inner Join Country AS oc ON oc.Id = shol.CountryId
			Inner Join ShipmentLocation AS shdl ON shdl.Id = sh.DestinationId
			Inner Join Country AS dc ON dc.Id = shdl.CountryId
			Inner Join Customer c on sh.customerId = c.Id
			Left Join RefServiceMode rsm on rsm.ServiceModeIndex = sh.ServiceMode
			Left Join RefShipmentStatus rss on rss.ShipmentStatusIndex = sh.[Status]
		Where
			@TenantId <> 0 
				  AND @UserId <> 0
				  AND sh.TenantId = @TenantId
				  AND (
					(Select COUNT(*) from [User] where id = @UserId and TenantEmployee = 1) > 0
					OR
					(sh.CustomerId in (SELECT CustomerId FROM UserShipAs WHERE TenantId = @TenantId and UserId = @UserId
						UNION
						SELECT [User].CustomerId FROM [User] WHERE Id = @UserId)))
				  AND (@CustomerGroupId = 0 OR (Select COUNT(*) from CustomerGroupMap WHERE CustomerId = sh.CustomerId and CustomerGroupId = @CustomerGroupId) > 0)) as ssr
	Where
		1=1
		--[#Filters#]

Insert Into #shipmentStatusReport(ShipmentNumber,ShipmentStatus,MiscField1,Quantity,ShipperReference,Origin,OriginCity,OriginPostalCode,OriginState
											,Destination,DestinationCity,DestinationState,DestinationPostalCode,ItemDescription,PurchaseOrderNumber,ServiceMode 
											,ActualWeight,DesiredPickupDateText,ActualDeliveryDate,ActualPickupDateText,EstimatedDeliveryDateText, CustomerName, CustomerNumber, DateCreated)
	Select * From
		(Select
			lo.LoadOrderNumber 'ShipmentNumber'
			,Isnull(rss.LoadOrderStatusText,'') as 'ShipmentStatus'
			,lo.MiscField1 'MiscField1'
			,loi.Quantity 'Quantity'
			,lo.ShipperReference 'ShipperReference'
			,lool.[Description] 'Origin'
			,lool.City 'OriginCity'
			,lool.PostalCode 'OriginPostalCode'
			,lool.[State] 'OriginState'
			,lodl.[Description] 'Destination'
			,lodl.City 'DestinationCity'
			,lodl.[State] 'DestinationState'
			,lodl.PostalCode 'DestinationPostalCode'
			,loi.[Description] 'ItemDescription'
			,lo.PurchaseOrderNumber 'PurchaseOrderNumber'
			,Isnull(rsm.ServiceModeText,'') 'ServiceMode'
			,loi.[Weight] 'ActualWeight'
			,(CASE WHEN lo.DesiredPickupDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(10),lo.DesiredPickupDate,121) END) 'DesiredPickupDateText'
			,Convert(datetime,'1753-01-01 00:00:00.000') 'ActualDeliveryDate'
			,Convert(nvarchar(10),'') 'ActualPickupDateText'
			,(CASE WHEN lo.EstimatedDeliveryDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(10),lo.EstimatedDeliveryDate,121) END) 'EstimatedDeliveryDateText'
			,c.Name 'CustomerName'
			,c.CustomerNumber 'CustomerNumber'
			,lo.DateCreated
		From LoadOrder lo
			Inner Join LoadOrderItem AS loi ON loi.LoadOrderId = lo.Id
			Inner Join LoadOrderLocation AS lool ON lool.Id = lo.OriginId
			Inner Join Country AS oc ON oc.Id = lool.CountryId
			Inner Join LoadOrderLocation AS lodl ON lodl.Id = lo.DestinationId
			Inner Join Country AS dc ON dc.Id = lodl.CountryId
			Inner Join Customer c on lo.customerId = c.Id
			Left Join RefServiceMode rsm on rsm.ServiceModeIndex = lo.ServiceMode
			Left Join RefLoadOrderStatus rss on rss.LoadOrderStatusIndex = lo.[Status]
		Where
				@TenantId <> 0 
				AND @UserId <> 0
				AND lo.TenantId = @TenantId
				AND (
					(Select COUNT(*) from [User] where id = @UserId and TenantEmployee = 1) > 0
				OR
				(lo.CustomerId in (SELECT CustomerId FROM UserShipAs WHERE TenantId = @TenantId and UserId = @UserId
					UNION
						SELECT [User].CustomerId FROM [User] WHERE Id = @UserId)))
				  AND (@CustomerGroupId = 0 OR (Select COUNT(*) from CustomerGroupMap WHERE CustomerId = lo.CustomerId and CustomerGroupId = @CustomerGroupId) > 0)) as ssr
	Where 
			ssr.ShipmentNumber Not In (Select ShipmentNumber From #shipmentStatusReport)
			And 1=1
			
Select * From #shipmentStatusReport ssr order by ShipmentNumber

DROP TABLE #shipmentStatusReport

