SELECT COUNT(*) FROM XmlTransmission

DECLARE @TenantId bigint = 4
DECLARE @UserId bigint = 4

SELECT
	x.Id,--Source Control
	x.ReferenceNumber,
	(CASE
		WHEN x.ReferenceNumberType = 0 THEN 'Shipemnt'
		WHEN x.ReferenceNumberType = 1 THEN 'Invoice'
		WHEN x.ReferenceNumberType = 2 THEN 'External'
	END),
	x.TransmissionDateTime,
	(CASE 
		WHEN x.TransmissionDateTime = '1753-01-01 00:00:00.000' THEN '' 
		ELSE CONVERT(nvarchar(20),x.TransmissionDateTime,101) 
	END),--view transmission Date
	x.AcknowledgementDateTime,
	(CASE 
		WHEN x.AcknowledgementDateTime = '1753-01-01 00:00:00.000' THEN '' 
		ELSE CONVERT(nvarchar(20),x.AcknowledgementDateTime,101) 
	END),--view response Date
	x.TransmissionKey,
	x.DocumentType,
	(CASE
		WHEN x.NotificationMethod = 0 THEN 'FTP'
		WHEN x.NotificationMethod = 1 THEN 'EDI'
		WHEN x.NotificationMethod = 2 THEN 'Email'
		WHEN x.NotificationMethod = 3 THEN 'Fax'
	END),
	(CASE
		WHEN x.Direction = 0 THEN 'Inbound'
		ELSE 'Outbound'
	END),
	(CASE
		WHEN x.SendOkay = 0 THEN 'No'
		ELSE 'Yes'
	END),
	x.AcknowledgementMessage,
	u.Username
FROM
	XmlTransmission AS x
		LEFT JOIN [User] AS u ON u.Id = x.UserId
WHERE
	x.TenantId = @TenantId
		AND @UserId <> 0
--		AND {1}
--ORDER BY 
--	{2} 	