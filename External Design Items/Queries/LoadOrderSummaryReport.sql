Declare @TenantId bigint = 4
Declare @UserId bigint = 2
Declare @AccountBucketCode nvarchar(50) = 'NAD001'
Declare @ServiceMode nvarchar(50) = 'all' 
Declare @IsEmployee nvarchar(50) = 'all' 
Declare @StartDateCreated datetime = '2000-01-01 00:00:00.000'
Declare @EndDateCreated datetime = '2020-01-01 00:00:00.000'
Declare @SummaryType nvarchar(50) = 'customer' -- values 'customer', 'user', or 'both'

Declare @DefaultTxt nvarchar(50) = 'all'

--validate service mode 
declare @sm int = -1
Set @ServiceMode = lower(replace(@ServiceMode, ' ', ''))
IF @ServiceMode = 'lessthantruckLoad' OR @ServiceMode = 'ltl' BEGIN SET @sm = 0 END
ELSE IF @ServiceMode = 'truckload' OR @ServiceMode = 'ftl'BEGIN SET @sm = 1 END
ELSE IF @ServiceMode = 'air' BEGIN SET @sm = 2 END
ELSE IF @ServiceMode = 'rail' BEGIN SET @sm = 3 END
ELSE IF @ServiceMode = 'smallPackage' OR @ServiceMode = 'sp' BEGIN SET @sm = 4 END
ELSE IF @ServiceMode = 'n/a' OR @ServiceMode = 'notapplicable' BEGIN SET @sm = 5 END
ELSE IF @ServiceMode = 'all' or @ServiceMode = '' BEGIN SET @sm = -1 END
ELSE BEGIN SET @sm = -2 END

--validate employee bool
declare @ie int = -1
Set @IsEmployee = lower(replace(@IsEmployee, ' ', ''))
IF @IsEmployee = 'yes' BEGIN SET @ie = 1 END
ELSE IF @IsEmployee = 'no' BEGIN SET @ie = 0 END
ELSE IF @IsEmployee = 'all' BEGIN SET @ie = -1 END
ELSE BEGIN SET @IE = -2 END

--validate summary type
Set @SummaryType = lower(@summaryType)
If @SummaryType <> 'user' And @SummaryType <> 'customer' And @SummaryType <> 'both' Begin Set @SummaryType = 'both' End

--create temp table
If object_id('tempdb..#loadOrderReport') is not null
	Drop Table #loadOrderReport

create table #loadOrderReport (
	[CustomerName] varchar(50)
	,[CustomerNumber] varchar(50)
	,[UserName] varchar(50)
	,[UserFirstName] varchar(50)
	,[UserLastName] varchar(50)
	,CurrentTotalLoads int
	,CurrentOfferedLoads int
	,CurrentQuotedLoads int
	,CurrentCancelledLoads int
	,CurrentCoveredLoads int
	,CurrentLostLoads int
	,CurrentAcceptedLoads int
	,CurrentInShipmentLoads int
	,OfferedLoads int
	,QuotedLoads int
	,CancelledLoads int
	,CoveredLoads int
	,LostLoads int
	,AcceptedLoads int
	,ShippedLoads int
	,WasQuotedAndShipped int
)

If @SummaryType = 'customer'
	Begin
		Insert into #loadOrderReport([CustomerName], [CustomerNumber], [UserName], [UserFirstName], [UserLastName], CurrentTotalLoads, CurrentOfferedLoads, CurrentQuotedLoads, 
		CurrentCancelledLoads, CurrentCoveredLoads, CurrentLostLoads, CurrentAcceptedLoads, CurrentInShipmentLoads, OfferedLoads, QuotedLoads, CancelledLoads, CoveredLoads,
		LostLoads, AcceptedLoads, ShippedLoads, WasQuotedAndShipped)
			Select
				c.Name 'CustomerName',
				c.CustomerNumber 'CustomerNumber',
				'',
				'',
				'',
				count(*) 'LoadOrderInAnyStatus'
				,SUM(case when lo.Status = 0 then 1 else 0 end) 'CurrentOfferedLoads'
				,SUM(case when lo.Status = 1 then 1 else 0 end) 'CurrentQuotedLoads'
				,SUM(case when lo.Status = 2 then 1 else 0 end) 'CurrentCancelledLoads'
				,SUM(case when lo.Status = 3 then 1 else 0 end) 'CurrentCoveredLoads'
				,SUM(case when lo.Status = 4 then 1 else 0 end) 'CurrentLostLoads'
				,SUM(case when lo.Status = 5 then 1 else 0 end) 'CurrentAcceptedLoads'
				,SUM(case when lo.Status = 6 then 1 else 0 end) 'CurrentInShipmentLoads'
				,SUM(case when lohd.WasOffered = 1 then 1 else 0 end) 'OfferedLoads'
				,SUM(case when lohd.WasQuoted = 1 then 1 else 0 end) 'QuotedLoads'
				,SUM(case when lohd.WasCancelled = 1 then 1 else 0 end) 'CancelledLoads'
				,SUM(case when lohd.WasCovered = 1 then 1 else 0 end) 'CoveredLoads'
				,SUM(case when lohd.WasLost = 1 then 1 else 0 end) 'LostLoads'
				,SUM(case when lohd.WasAccepted = 1 then 1 else 0 end) 'AcceptedLoads'
				,SUM(case when lohd.WasShipment = 1 then 1 else 0 end) 'ShippedLoads'
				,SUM((case when lohd.WasQuoted = 1 AND lohd.WasShipment = 1 then 1 else 0 end)) 'WasQuotedAndShipped'
			From 
				LoadOrder lo
					INNER JOIN Customer c on  lo.CustomerId = c.Id
					INNER JOIN [User] u on lo.UserId = u.Id
					INNER JOIN LoadOrderHistoricalData lohd on lo.Id = lohd.LoadOrderId
					LEFT JOIN LoadOrderAccountBucket loab on loab.LoadOrderId = lo.Id and loab.[Primary] = 1
					LEFT JOIN AccountBucket ab on loab.AccountBucketId = ab.Id
				Where
				lo.TenantId = @TenantId
				And
					(
						(Select COUNT(*) from [User] where id = @UserId and TenantEmployee = 1) > 0
						Or
						(c.Id in (Select CustomerId from UserShipAs where TenantId = @TenantId and UserId = @UserId
							union
							Select [User].CustomerId from [User] where Id = @UserID))
					)
				AND lo.DateCreated between @StartDateCreated and @EndDateCreated
				AND (@ie = -1 OR u.TenantEmployee = @ie)
				AND (@AccountBucketCode = '' or lower(@AccountBucketCode) = @DefaultTxt or ab.Code = @AccountBucketCode) 
				AND (@sm = -1 OR lo.ServiceMode = @sm)
			Group By 
				c.Name, c.CustomerNumber
	End
Else if @SummaryType = 'user'
	begin
		Insert into #loadOrderReport([CustomerName], [CustomerNumber], [Username], [UserFirstName], [UserLastName], CurrentTotalLoads, CurrentOfferedLoads, CurrentQuotedLoads,
		 CurrentCancelledLoads, CurrentCoveredLoads, CurrentLostLoads, CurrentAcceptedLoads, CurrentInShipmentLoads, OfferedLoads, QuotedLoads, CancelledLoads, CoveredLoads,
		 LostLoads, AcceptedLoads, ShippedLoads, WasQuotedAndShipped)
			Select
				'',
				'',
				u.Username, 
				u.FirstName,
				u.LastName,
				count(*) 'LoadOrderInAnyStatus'
				,SUM(case when lo.Status = 0 then 1 else 0 end) 'CurrentOfferedLoads'
				,SUM(case when lo.Status = 1 then 1 else 0 end) 'CurrentQuotedLoads'
				,SUM(case when lo.Status = 2 then 1 else 0 end) 'CurrentCancelledLoads'
				,SUM(case when lo.Status = 3 then 1 else 0 end) 'CurrentCoveredLoads'
				,SUM(case when lo.Status = 4 then 1 else 0 end) 'CurrentLostLoads'
				,SUM(case when lo.Status = 5 then 1 else 0 end) 'CurrentAcceptedLoads'
				,SUM(case when lo.Status = 6 then 1 else 0 end) 'CurrentInShipmentLoads'
				,SUM(case when lohd.WasOffered = 1 then 1 else 0 end) 'OfferedLoads'
				,SUM(case when lohd.WasQuoted = 1 then 1 else 0 end) 'QuotedLoads'
				,SUM(case when lohd.WasCancelled = 1 then 1 else 0 end) 'CancelledLoads'
				,SUM(case when lohd.WasCovered = 1 then 1 else 0 end) 'CoveredLoads'
				,SUM(case when lohd.WasLost = 1 then 1 else 0 end) 'LostLoads'
				,SUM(case when lohd.WasAccepted = 1 then 1 else 0 end) 'AcceptedLoads'
				,SUM(case when lohd.WasShipment = 1 then 1 else 0 end) 'ShippedLoads'
				,SUM((case when lohd.WasQuoted = 1 AND lohd.WasShipment = 1 then 1 else 0 end)) 'WasQuotedAndShipped'
			From 
				LoadOrder lo
					INNER JOIN [User] u on  lo.UserId = u.Id
					INNER JOIN Customer c on lo.CustomerId = c.Id
					INNER JOIN LoadOrderHistoricalData lohd on lo.Id = lohd.LoadOrderId
					LEFT JOIN LoadOrderAccountBucket loab on loab.LoadOrderId = lo.Id and loab.[Primary] = 1
					LEFT JOIN AccountBucket ab on loab.AccountBucketId = ab.Id
				Where
				lo.TenantId = @TenantId
				And
					(
						(Select COUNT(*) from [User] where id = @UserId and TenantEmployee = 1) > 0
						Or
						(c.Id in (Select CustomerId from UserShipAs where TenantId = @TenantId and UserId = @UserId
							union
							Select [User].CustomerId from [User] where Id = @UserID))
					)
				AND lo.DateCreated between @StartDateCreated and @EndDateCreated
				AND (@ie = -1 OR u.TenantEmployee = @ie)
				AND (@AccountBucketCode = '' or lower(@AccountBucketCode) = @DefaultTxt or ab.Code = @AccountBucketCode)
				AND (@sm = -1 OR lo.ServiceMode = @sm)
			Group By 
				u.Username, u.FirstName, u.LastName
	end
Else if @SummaryType = 'both'
	begin
			Insert into #loadOrderReport([CustomerName], [CustomerNumber], [Username], [UserFirstName], [UserLastName], CurrentTotalLoads, CurrentOfferedLoads, 
			CurrentQuotedLoads, CurrentCancelledLoads, CurrentCoveredLoads, CurrentLostLoads, CurrentAcceptedLoads, CurrentInShipmentLoads, OfferedLoads,
			QuotedLoads, CancelledLoads, CoveredLoads, LostLoads, AcceptedLoads, ShippedLoads, WasQuotedAndShipped)
				Select
					ISNULL(c.Name, ''),
					ISNULL(c.CustomerNumber,''),
					ISNULL(u.Username, ''),
					ISNULL(u.FirstName, ''),
					ISNULL(u.LastName,''),
					count(*) 'LoadOrderInAnyStatus'
					,SUM(case when lo.Status = 0 then 1 else 0 end) 'CurrentOfferedLoads'
					,SUM(case when lo.Status = 1 then 1 else 0 end) 'CurrentQuotedLoads'
					,SUM(case when lo.Status = 2 then 1 else 0 end) 'CurrentCancelledLoads'
					,SUM(case when lo.Status = 3 then 1 else 0 end) 'CurrentCoveredLoads'
					,SUM(case when lo.Status = 4 then 1 else 0 end) 'CurrentLostLoads'
					,SUM(case when lo.Status = 5 then 1 else 0 end) 'CurrentAcceptedLoads'
					,SUM(case when lo.Status = 6 then 1 else 0 end) 'CurrentInShipmentLoads'
					,SUM(case when lohd.WasOffered = 1 then 1 else 0 end) 'OfferedLoads'
					,SUM(case when lohd.WasQuoted = 1 then 1 else 0 end) 'QuotedLoads'
					,SUM(case when lohd.WasCancelled = 1 then 1 else 0 end) 'CancelledLoads'
					,SUM(case when lohd.WasCovered = 1 then 1 else 0 end) 'CoveredLoads'
					,SUM(case when lohd.WasLost = 1 then 1 else 0 end) 'LostLoads'
					,SUM(case when lohd.WasAccepted = 1 then 1 else 0 end) 'AcceptedLoads'
					,SUM(case when lohd.WasShipment = 1 then 1 else 0 end) 'ShippedLoads'
					,SUM((case when lohd.WasQuoted = 1 AND lohd.WasShipment = 1 then 1 else 0 end)) 'WasQuotedAndShipped'
				From 
					LoadOrder lo
						INNER JOIN [User] u on lo.UserId = u.Id
						INNER JOIN Customer c on  lo.CustomerId = c.Id
						INNER JOIN LoadOrderHistoricalData lohd on lo.Id = lohd.LoadOrderId
						LEFT JOIN LoadOrderAccountBucket loab on loab.LoadOrderId = lo.Id and loab.[Primary] = 1
						LEFT JOIN AccountBucket ab on loab.AccountBucketId = ab.Id
					Where
					lo.TenantId = @TenantId
					And
						(
							(Select COUNT(*) from [User] where id = @UserId and TenantEmployee = 1) > 0
							Or
							(c.Id in (Select CustomerId from UserShipAs where TenantId = @TenantId and UserId = @UserId
							union
							Select [User].CustomerId from [User] where Id = @UserID))
						)
					AND lo.DateCreated between @StartDateCreated and @EndDateCreated
					AND (@ie = -1 OR u.TenantEmployee = @ie)
					AND (@AccountBucketCode = '' or lower(@AccountBucketCode) = @DefaultTxt or ab.Code = @AccountBucketCode)
					AND (@sm = -1 OR lo.ServiceMode = @sm)
				Group By 
					c.Name, c.CustomerNumber, u.Username, u.FirstName, u.LastName
	end

Select * from #loadOrderReport
drop table #loadOrderReport

