DECLARE @TenantId BIGINT = 4
DECLARE @UserId BIGINT = 4

SELECT
	c.CustomerNumber,
	c.Name,
	(CASE WHEN c.Active = 0 THEN 'No' ELSE 'Yes' END) AS 'CustomerIsActive', 
	(SELECT COUNT(cca.Id) FROM CustomerControlAccount AS cca WHERE cca.CustomerId = c.Id) AS 'CountOfCustomerControlAccounts',
	(CASE WHEN c.InvoiceRequiresControlAccount = 0 THEN 'No' ELSE 'Yes' END) AS 'InvoiceRequiresControlAccount',
	(CASE WHEN c.InvoiceRequiresCarrierProNumber = 0 THEN 'No' ELSE 'Yes' END) AS 'InvoiceRequiresCarrierProNumber',
	(CASE WHEN c.InvoiceRequiresShipperReference = 0 THEN 'No' ELSE 'Yes' END) AS 'InvoiceRequiresShipperReference',
	c.AuditInstructions
FROM
	Customer AS c
WHERE
	@TenantId <> 0
	AND @UserId <> 0
	AND @TenantId = c.TenantId
--		AND {1}
--ORDER BY 
--	{2}

	
