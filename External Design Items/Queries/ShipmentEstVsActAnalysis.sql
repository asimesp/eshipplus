SELECT 
	COUNT(*)
FROM
	Shipment

declare @TenantId bigint = 4
declare @UserId bigint = 2
DECLARE @StartARPostDate date = '2013-01-01';
DECLARE @EndARPostDate date = '2013-06-01';
DECLARE @StartAPPostDate date = '2013-01-01';
DECLARE @EndAPPostDate date = '2013-06-01';
declare @startDateCreated date = '2013-01-01';
declare @endDateCreated date = '2013-05-09';


SELECT -- top 1000
	sh.ShipmentNumber,
	(CASE
		WHEN sh.AuditedForInvoicing = 0 THEN 'No'
		ELSE 'Yes'
	END),
	shv.ProNumber,
	sh.DesiredPickupDate,
	sh.ActualPickupDate,
	sh.EstimatedDeliveryDate,
	sh.ActualDeliveryDate,
	sh.DateCreated,
	(CASE WHEN sh.CreatedInError = 1 THEN 'Yes' ELSE 'No' END) 'Created In Error',
	shlo.[Description],
	shlo.City,
	shlo.[State],
	shlo.PostalCode,
	shloc.Code,
	shloc.Name,
	shld.[Description],
	shld.City,
	shld.[State],
	shld.PostalCode,
	shldc.Code,
	shldc.Name,
	v.VendorNumber,
	v.Name,
	ab.Code,
	ab.[Description],
	c.CustomerNumber,
	c.Name,
	t.TierNumber,
	t.Name,
	u.Username,
	u.FirstName,
	u.LastName,
	u.TenantEmployee,
	ISNULL(sr.SalesRepresentativeNumber,''),
	ISNULL(sr.Name,''),
	sh.SalesRepresentativeCommissionPercent,
	sh.SalesRepMinCommValue,
	sh.SalesRepMaxCommValue,
	sh.SalesRepAddlEntityName,
	sh.SalesRepAddlEntityCommPercent,
	sis.TotalPieceCount,
	sis.TotalWeight,
	sis.TotalPackageQty,
	ISNULL(ss.ShipmentStatusText, '') AS 'Status',
	ISNULL(sm.ServiceModeText, '') AS 'ServiceMode',
	
	(CASE WHEN ra.Id IS NULL OR scs.FreightCharges = 0 
			 THEN 0
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalFreightMarkupIsPercent = 1	
		     THEN (scs.FreightCharges / (1+(sh.ResellerAdditionalFreightMarkup/100)))
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalFreightMarkupIsPercent = 0   
			 THEN scs.FreightCharges - sh.ResellerAdditionalFreightMarkup
		  ELSE 0
	END), -- FreightChargesReseller,
	(CASE WHEN ra.Id IS NULL OR scs.FuelCharges = 0 
			 THEN 0
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalFuelMarkupIsPercent = 1	
		     THEN (scs.FuelCharges / (1+(sh.ResellerAdditionalFuelMarkup/100)))
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalFuelMarkupIsPercent = 0   
			 THEN scs.FuelCharges - sh.ResellerAdditionalFuelMarkup
		  ELSE 0
	END), -- FuelChargesReseller
	(CASE WHEN ra.Id IS NULL OR scs.AccessorialCharges = 0 
			 THEN 0
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalAccessorialMarkupIsPercent = 1	
		     THEN (scs.AccessorialCharges / (1+(sh.ResellerAdditionalAccessorialMarkup/100)))
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalAccessorialMarkupIsPercent = 0   
			 THEN scs.AccessorialCharges - sh.ResellerAdditionalAccessorialMarkup
		  ELSE 0
	END), --AS AccessorialChargesReseller
	(CASE WHEN ra.Id IS NULL OR scs.ServiceCharges = 0 
			 THEN 0
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalServiceMarkupIsPercent = 1	
		     THEN (scs.ServiceCharges / (1+(sh.ResellerAdditionalServiceMarkup/100)))
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalServiceMarkupIsPercent = 0   
			 THEN scs.ServiceCharges - sh.ResellerAdditionalServiceMarkup
		  ELSE 0
	END), --AS ServiceChargesReseller
	
	scs.FreightCharges 'EstimateFreightCharge',
	scs.FuelCharges 'EstimateFuelCharge',
	scs.AccessorialCharges 'EstimateAccessorialCharge',
	scs.ServiceCharges 'EstimateServiceCharge',
	
	scs.FreightCost 'EstimateFreightCost',
	scs.FuelCost 'EstimateFuelCost',
	scs.AccessorialCost 'EstimateAccessorialCost',
	scs.ServiceCost 'EstimateServiceCost',
	
	scs.TotalCost 'EstimateTotalCost',
	scs.TotalDiscount 'EstimateTotalDiscount',
	scs.TotalAmountDue 'EstimateTotalAmountDue',
	scs.TotalProfit 'EstimateTotalProfit',
	
	ISNULL(sac.TotalActualCost, 0) 'TotalActualCost',
	ISNULL(sac.TotalActualCostCredit, 0) 'TotalActualCostCredit',
	
	ISNULL(sac.ActualFreightCost, 0) 'ActualFreightCost',
	ISNULL(sac.ActualFreightCostCredit, 0) 'ActualFreightCostCredit',
	ISNULL(sac.ActualFuelCost, 0) 'ActualFuelCost',
	ISNULL(sac.ActualFuelCostCredit, 0) 'ActualFuelCostCredit',
	ISNULL(sac.ActualAccessorialCost, 0) 'ActualAccessorialCost',
	ISNULL(sac.ActualAccessorialCostCredit, 0) 'ActualAccessorialCostCredit',
	ISNULL(sac.ActualServiceCost, 0) 'ActualServiceCost',
	ISNULL(sac.ActualServiceCostCredit, 0) 'ActualServiceCostCredit',
	
	ISNULL(sar.TotalActualCharges, 0) 'TotalActualCharges',
	ISNULL(sar.TotalSupplementalCharges, 0) 'TotalSupplementalCharges',
	ISNULL(sar.TotalCredit, 0) 'TotalRevenueCredits',
	
    ISNULL(sar.ActualFreightCharges, 0) 'ActualFreightCharges',
	ISNULL(sar.ActualFuelCharges, 0) 'ActualFuelCharges',
	ISNULL(sar.ActualAccessorialCharges, 0) 'ActualAccessorialCharges',
	ISNULL(sar.ActualServiceCharges, 0) 'ActualServiceCharges',
	ISNULL(sar.AccessorialCredit, 0) 'AccessorialCredit',
	ISNULL(sar.FreightCredit, 0) 'FreightCredit',
	ISNULL(sar.FuelCredit, 0) 'FuelCredit',
	ISNULL(sar.ServiceCredit, 0) 'ServiceCredit',
	ISNULL(sar.SupplementalAccessorialCharges, 0) 'SupplementalAccessorialCharges',
	ISNULL(sar.SupplementalFreightCharges, 0) 'SupplementalFreightCharges',
	ISNULL(sar.SupplementalFuelCharges, 0) 'SupplementalFuelCharges',
	ISNULL(sar.SupplementalServiceCharges, 0) 'SupplementalServiceCharges',
	
	c.CustomField1,	
	c.CustomField2,	
	c.CustomField3,	
	c.CustomField4
FROM 
	Shipment sh 
		INNER JOIN ShipmentVendor shv ON shv.ShipmentId = sh.Id AND shv.[Primary] = 1
		INNER JOIN Vendor v ON shv.VendorId = v.Id
		INNER JOIN ShipmentLocation shlo ON sh.OriginId = shlo.Id
		INNER JOIN Country shloc ON shloc.Id = shlo.CountryId
		INNER JOIN ShipmentLocation shld ON sh.DestinationId = shld.Id
		INNER JOIN Country shldc ON shldc.Id = shld.CountryId
		INNER JOIN ShipmentAccountBucket shab ON sh.Id = shab.ShipmentId AND shab.[Primary] = 1
		INNER JOIN AccountBucket ab ON shab.AccountBucketId = ab.Id 
		INNER JOIN Customer c ON sh.CustomerId = c.Id
		INNER JOIN [User] u ON sh.UserId = u.Id
		INNER JOIN ShipmentChargeStatistics scs ON scs.ShipmentId = sh.Id
		INNER JOIN Tier t ON t.Id = c.TierId
		LEFT JOIN SalesRepresentative sr ON sh.SalesRepresentativeId = sr.Id 
		LEFT JOIN ShipmentItemStatistics sis ON sis.ShipmentId = sh.Id
		LEFT JOIN ResellerAddition ra ON ra.Id = sh.ResellerAdditionId
		LEFT JOIN ShipmentActualCosts(@TenantId, @StartAPPostDate, @EndAPPostDate) as sac ON sac.Id = sh.Id
		LEFT JOIN ShipmentActualRevenues(@TenantId, @StartARPostDate, @EndARPostDate) as sar ON sar.Id = sh.Id
		LEFT JOIN RefShipmentStatus ss ON ss.ShipmentStatusIndex = sh.[Status]
		LEFT JOIN RefServiceMode sm ON sm.ServiceModeIndex = sh.ServiceMode
WHERE 
	sh.TenantId = @TenantId
	AND (
		(SELECT COUNT(*) FROM [User] WHERE Id = @UserId and TenantEmployee = 1) > 0
		OR
		(sh.CustomerId IN 
			(SELECT CustomerId FROM UserShipAs WHERE TenantId = @TenantId AND UserId = @UserId
			 UNION
			 SELECT [User].CustomerId FROM [User] WHERE Id = @UserId)))
	AND
		sh.datecreated between @startDateCreated and @endDateCreated
	AND (CASE 
			WHEN sh.[Status] = 0 THEN 'Scheduled'	
			WHEN sh.[Status] = 1 THEN 'In Transit' 
			WHEN sh.[Status] = 2 THEN 'Delivered' 
			WHEN sh.[Status] = 3 THEN 'Invoiced' 
			WHEN sh.[Status] = 4 THEN 'Void'
		END) not like '%void%'
	AND (CASE 
			WHEN sh.ServiceMode = 0 THEN 'LessThanTruckLoad' 
			WHEN sh.ServiceMode = 1 THEN 'Truckload' 
			WHEN sh.ServiceMode = 2 THEN 'PartialTruckload' 
			WHEN sh.ServiceMode = 3 THEN 'Air' 
			WHEN sh.ServiceMode = 4 THEN 'Rail'
			WHEN sh.ServiceMode = 5 THEN 'SmallPackage'
		END) like '%less%'
