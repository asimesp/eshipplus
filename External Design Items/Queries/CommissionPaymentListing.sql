DECLARE @TenantId bigint = 4;
DECLARE @UserId bigint = 2;

Select
	c.CustomerNumber
	,c.Name
	,sh.ShipmentNumber
	,sh.DateCreated 'Shipment Date Created'
	,(
		CASE 
			WHEN sh.DateCreated = '1753-01-01 00:00:00.000' THEN '' 
			ELSE CONVERT(nvarchar(10),sh.DateCreated,121) 
		END
	) 'Shipment Date Created Text'
	,sh.DesiredPickupDate
	,(
		CASE 
			WHEN sh.DesiredPickupDate = '1753-01-01 00:00:00.000' THEN '' 
			ELSE CONVERT(nvarchar(10),sh.DesiredPickupDate,121) 
		END
	) 'Desired Pickup Date Text'
	,sh.ActualPickupDate
	,(
		CASE 
			WHEN sh.ActualPickupDate = '1753-01-01 00:00:00.000' THEN '' 
			ELSE CONVERT(nvarchar(10),sh.ActualPickupDate,121) 
		END
	) 'Actual Pickup Date Text'
	,ISNULL(ss.ShipmentStatusText,'') AS 'Status'
	,ISNULL(sm.ServiceModeText, '') AS 'Service Mode'
	,ISNULL((SELECT (et.TypeName + ', ')
				FROM ShipmentEquipment she INNER JOIN EquipmentType et ON she.EquipmentTypeId = et.Id AND she.ShipmentId = sh.Id
				ORDER BY et.Id Desc
				FOR XML PATH('')),'') 'Equipment Types'
	,shlo.[Description] 'ShipperName'
	,shld.[Description] 'ConsigneeName'
	,scs.TotalCharges 'Total Est. Charges'
	,scs.TotalCost 'Total Est. Cost'
	,scs.TotalProfit 'Total Est. Profit'
	,(ISNULL(sar.TotalActualCharges,0) + ISNULL(sar.TotalSupplementalCharges,0) - ISNULL(sar.TotalCredit,0)) 'Total Act. Charges'
	,(ISNULL(sac.TotalActualCost, 0) - ISNULL(sac.TotalActualCostCredit,0)) 'Total Act. Cost'
	,(ISNULL(sar.TotalActualCharges,0) + ISNULL(sar.TotalSupplementalCharges,0) - ISNULL(sar.TotalCredit,0) - ISNULL(sac.TotalActualCost,0) + ISNULL(sac.TotalActualCostCredit,0)) 'Total Act. Profit'
	,sh.SalesRepresentativeCommissionPercent
	,sh.SalesRepMinCommValue
	,sh.SalesRepMaxCommValue
	,(
		CASE
			WHEN scs.TotalProfit <= 0 THEN 0
			WHEN scs.TotalProfit * sh.SalesRepresentativeCommissionPercent/ 100 >= sh.SalesRepMaxCommValue THEN sh.SalesRepMaxCommValue
			WHEN scs.TotalProfit * sh.SalesRepresentativeCommissionPercent/ 100 < sh.SalesRepMinCommValue THEN sh.SalesRepMinCommValue		
			ELSE scs.TotalProfit * sh.SalesRepresentativeCommissionPercent/ 100
			END
	) 'Est. Sales Rep Commission'
	,(
		CASE
			WHEN (ISNULL(sar.TotalActualCharges,0) + ISNULL(sar.TotalSupplementalCharges,0) - ISNULL(sar.TotalCredit,0)  - ISNULL(sac.TotalActualCost,0) + ISNULL(sac.TotalActualCostCredit,0)) * sh.SalesRepresentativeCommissionPercent/ 100 >= sh.SalesRepMaxCommValue THEN sh.SalesRepMaxCommValue
			WHEN (ISNULL(sar.TotalActualCharges,0) + ISNULL(sar.TotalSupplementalCharges,0) - ISNULL(sar.TotalCredit,0)  - ISNULL(sac.TotalActualCost,0) + ISNULL(sac.TotalActualCostCredit,0)) * sh.SalesRepresentativeCommissionPercent/ 100 < sh.SalesRepMinCommValue THEN sh.SalesRepMinCommValue
			ELSE (ISNULL(sar.TotalActualCharges,0) + ISNULL(sar.TotalSupplementalCharges,0) - ISNULL(sar.TotalCredit,0)  - ISNULL(sac.TotalActualCost,0) + ISNULL(sac.TotalActualCostCredit,0)) * sh.SalesRepresentativeCommissionPercent/ 100
		END
	) 'Act. Sales Rep Commission'
	,sh.SalesRepAddlEntityName
	,sh.SalesRepAddlEntityCommPercent
	,(CASE
			WHEN scs.TotalProfit <= 0 THEN 0
			ELSE scs.TotalProfit * sh.SalesRepAddlEntityCommPercent/ 100 
		END) 'Est. Sales Rep Addl Entity Commission'
	,(CASE
			WHEN (ISNULL(sar.TotalActualCharges,0) + ISNULL(sar.TotalSupplementalCharges,0) - ISNULL(sar.TotalCredit,0)) - ISNULL(sac.TotalActualCost, 0) + ISNULL(sac.TotalActualCostCredit,0) <= 0 THEN 0
			ELSE ((ISNULL(sar.TotalActualCharges,0) + ISNULL(sar.TotalSupplementalCharges,0) - ISNULL(sar.TotalCredit,0)) - ISNULL(sac.TotalActualCost, 0) + ISNULL(sac.TotalActualCostCredit,0)) * sh.SalesRepAddlEntityCommPercent/ 100 
		END) 'Act. Sales Rep Addl Entity Commission'
	,ISNULL(cp.PaymentDate, '1753-01-01 00:00:00') 'Payment Date'
	,ISNULL(cp.DateCreated, '1753-01-01 00:00:00') 'Commision Payment Date Created'
	,(
		CASE 
			WHEN ISNULL(cp.PaymentDate, '1753-01-01 00:00:00') = '1753-01-01 00:00:00.000' THEN '' 
			ELSE CONVERT(nvarchar(10),cp.PaymentDate,121) 
		END
	) 'Payment Date Text'
	,(
		CASE 
			WHEN ISNULL(cp.DateCreated, '1753-01-01 00:00:00') = '1753-01-01 00:00:00.000' THEN '' 
			ELSE CONVERT(nvarchar(10),cp.DateCreated,121) 
		END
	) 'Commission Payment Date Created Text'
	,ISNULL(cp.AmountPaid, 0) 'Amount Paid'
	,ISNULL((CASE WHEN cp.[Type] = 0 THEN 'Payment' WHEN cp.[Type] = 1 THEN 'Reversal' END),'') 'Payment Type'
	,ISNULL(cp.AdditionalEntityCommission, 0) 'Additional Entity Commission'
	,ISNULL(PaymentTotals.[Addl Entity Total Comm. Paid],0) 'Addl Entity Total Comm. Paid'
	,ISNULL(PaymentTotals.[Addl Entity Total Comm. Reversed],0) 'Addl Entity Total Comm. Reversed'
	,ISNULL(PaymentTotals.[Net Addl Entity Comm. Paid],0) 'Net Addl Entity Comm. Paid'
	,ISNULL(PaymentTotals.[Total Comm. Paid],0) 'Total Comm. Paid'
	,ISNULL(PaymentTotals.[Total Comm. Reversed],0) 'Total Comm. Reversed'
	,ISNULL(PaymentTotals.[Net Comm. Paid],0) 'Net Comm. Paid'
	,ISNULL(sr.SalesRepresentativeNumber, '') 'Sales Rep Number'
	,ISNULL(sr.Name, '') 'Sales Rep Name'
	,ISNULL(si.InvoiceDate, CONVERT(datetime,'1753-01-01 00:00:00.000')) 'Invoice Date' --INVOICE DATE
	,ISNULL((
		CASE WHEN (si.InvoiceDate) = '1753-01-01 00:00:00.000' 
			THEN '' 
			ELSE CONVERT(nvarchar(20),(si.InvoiceDate),101) 
		END
	),'') 'View Invoice Date' --View Invoice Date
	,ISNULL(si.InvoiceNumber, '') 'Invoice Number' --INVOICE NUMBER
	,ISNULL(si.PostDate, CONVERT(datetime,'1753-01-01 00:00:00.000')) 'Invoice Post Date' --INVOICE POSTED DATE
	,ISNULL((
		CASE WHEN (si.PostDate) = '1753-01-01 00:00:00.000' 
			THEN '' 
			ELSE CONVERT(nvarchar(20),(si.PostDate),101) 
		END
	), '') 'View Invoice Post Date' --View Invoice Post Date
	,ab.Code 'Primary Shipment Account Bucket Code'
	,ab.[Description] 'Primary Shipment Account Bucket Description'
	,IsNull(p.Code,'') 'Shipment Prefix Code'
	,IsNull(p.[Description],'') 'Shipment Prefix Description'
	,c.CustomField1
	,c.CustomField2
	,c.CustomField3
	,c.CustomField4
From Shipment sh
	INNER JOIN Customer c on c.Id = sh.CustomerId
	INNER JOIN ShipmentChargeStatistics AS scs ON sh.Id = scs.ShipmentId
	INNER JOIN ShipmentLocation shlo ON sh.OriginId = shlo.Id
	INNER JOIN ShipmentLocation shld ON sh.DestinationId = shld.Id
	INNER JOIN ShipmentAccountBucket shab ON sh.Id = shab.ShipmentId AND shab.[Primary] = 1
	INNER JOIN AccountBucket ab ON shab.AccountBucketId = ab.Id 
	Left Join Prefix p On sh.PrefixId = p.Id
	LEFT JOIN ShipmentActualCosts(@TenantId, '1753-01-01 00:00:00', '9999-12-31 23:59:59') as sac ON sac.Id = sh.Id
	LEFT JOIN ShipmentActualRevenues(@TenantId, '1753-01-01 00:00:00', '9999-12-31 23:59:59') as sar ON sar.Id = sh.Id
	LEFT JOIN CommissionPayment cp on sh.ShipmentNumber = cp.ReferenceNumber
	LEFT JOIN SalesRepresentative sr ON sr.Id = cp.SalesRepresentativeId
	LEFT JOIN ShipmentInvoices si ON sh.Id = si.ShipmentId
	LEFT JOIN (Select 
					ReferenceNumber
					,SUM(CASE WHEN [Type] = 0 AND AdditionalEntityCommission = 1 THEN AmountPaid ELSE 0 END) 'Addl Entity Total Comm. Paid' 
					,SUM(CASE WHEN [Type] = 1 AND AdditionalEntityCommission = 1 THEN AmountPaid ELSE 0 END) 'Addl Entity Total Comm. Reversed' 
					,SUM(CASE WHEN [Type] = 0 AND AdditionalEntityCommission = 1 THEN AmountPaid WHEN [Type] = 1 AND AdditionalEntityCommission = 1 THEN AmountPaid * -1 END) 'Net Addl Entity Comm. Paid'  
					,SUM(CASE WHEN [Type] = 0 THEN AmountPaid ELSE 0 END) 'Total Comm. Paid' 
					,SUM(CASE WHEN [Type] = 1 THEN AmountPaid ELSE 0 END) 'Total Comm. Reversed' 
					,SUM(CASE WHEN [Type] = 0 THEN AmountPaid WHEN [Type] = 1 THEN AmountPaid * -1 END) 'Net Comm. Paid' 
				From CommissionPayment 
				Group By ReferenceNumber) as PaymentTotals on PaymentTotals.ReferenceNumber = sh.ShipmentNumber
	LEFT JOIN RefShipmentStatus ss ON ss.ShipmentStatusIndex = sh.[Status]
	LEFT JOIN RefServiceMode sm ON SM.ServiceModeIndex = sh.ServiceMode

--Where sh.DateCreated > '2013-12-25'

--Order By ShipmentNumber