SELECT COUNT(*) FROM Vendor 

SELECT
	v.VendorNumber,
	v.Name,
	v.DateCreated,
	ISNULL(isr.Username,'') AS 'InternalServiceRep',
	v.MC,
	v.DOT,
	v.Scac,
	vl.City,
	vl.[State],
	vl.PostalCode,
	c.Name,
	(CASE WHEN v.Active = 0 THEN 'No' ELSE 'Yes' END)AS 'Active',
	(CASE WHEN v.IsBroker = 0 THEN 'No' ELSE 'Yes' END)AS 'Is Broker',
	(CASE WHEN v.IsAgent = 0 THEN 'No' ELSE 'Yes' END)AS 'Is Agent',
	(CASE WHEN v.IsCarrier = 0 THEN 'No' ELSE 'Yes'	END)AS 'Is Carrier',
	(CASE WHEN V.HandlesAir = 0 THEN 'No' ELSE 'Yes' END)AS 'Handles Air',
	(CASE WHEN V.HandlesRail= 0 THEN 'No' ELSE 'Yes' END)AS 'Handles Rail',
	(CASE WHEN V.HandlesSmallPack = 0 THEN 'No' ELSE 'Yes' END)AS 'Handles Small Package',
	(CASE WHEN V.HandlesLessThanTruckload = 0 THEN 'No' ELSE 'Yes' END)AS 'Handles LTL',
	(CASE WHEN V.HandlesPartialTruckload = 0 THEN 'No' ELSE 'Yes' END)AS 'Handles Partial Truckload',
	(CASE WHEN V.HandlesTruckload = 0 THEN 'No' ELSE 'Yes'END)AS 'Handles Truckload',
	(CASE WHEN v.TSACertified = 0 THEN 'No' ELSE 'Yes' END)AS 'TSA Certified',
	(CASE WHEN v.SmartWayCertified = 0 THEN 'No' ELSE 'Yes'	END)AS 'Smartway Certified', 
	(CASE WHEN v.CTPATCertified = 0 THEN 'No' ELSE 'Yes' END)AS 'CTPAT Certified',
	v.CTPATNumber,
	(CASE WHEN v.PIPCertified = 0 THEN 'No' ELSE 'Yes' END)AS 'PIP Certified',
	v.PIPNumber,
	ISNULL((SELECT (et.TypeName + ', ')
				FROM VendorEquipment ve INNER JOIN EquipmentType et ON ve.EquipmentTypeId = et.Id AND ve.VendorId = v.Id
				WHERE v.Id= ve.VendorId
				ORDER BY et.Id Desc
				FOR XML PATH('')),'') AS 'Equipment Types',
	vi.InsuranceTypeCode,
	vi.InsuranceTypeDescription,
	vi.CoverageAmount,
	v.Notes,
	vi.SpecialInstruction,
	vi.[Required],
	vi.CarrierName,
	vi.PolicyNumber,
	 vi.ExpirationDate,
	(CASE WHEN vi.ExpirationDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),vi.ExpirationDate,101) END) AS 'ViewExpirationDate',
	(CASE WHEN v.DateCreated = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),v.DateCreated,101) END) AS 'ViewDateCreated',
	vi.CertificateHolder,
	vi.Expired,
	vi.ExpiringWithin7Days,
	vi.ExpiringWithin30Days,
	vi.EffectiveDate,
	(CASE WHEN vi.EffectiveDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),vi.EffectiveDate,101) END) AS 'ViewEffectiveDate'


FROM
	Vendor AS v
	INNER JOIN VendorLocation AS vl ON vl.VendorId = v.Id AND vl.[Primary] = 1
	INNER JOIN Country AS c ON c.Id = vl.CountryId
	INNER JOIN VendorInsuranceInformation vi ON vi.VendorId = v.Id
	LEFT JOIN [User] AS isr ON isr.Id = v.VendorServiceRepUserId
	
