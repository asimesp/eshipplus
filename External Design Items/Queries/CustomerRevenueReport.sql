	declare @TenantId bigint = 4
	declare @UserId bigint = 2
	
	-- Verify temp table doesn't exist, if it does, drop it.
	IF EXISTS (SELECT * FROM tempdb.sys.objects WHERE name LIKE '#CustRevs%')  
		DROP TABLE #CustRevs


	-- Create Temp Table	
	CREATE TABLE #CustRevs
	(
		TenantId bigint,
		CustomerId bigint,
		CustomerNumber nvarchar(50),
		CustomerName nvarchar(50),
		CustomField1 nvarchar(50),
		CustomField2 nvarchar(50),
		CustomField3 nvarchar(50),
		CustomField4 nvarchar(50),
		TierNumber nvarchar(50),
		TierName nvarchar(50),
		CustomerCreateDate datetime,
		SalesRepresentativeNumber nvarchar(50),
		SalesRepresentativeName nvarchar(50),
		CountLTLShipments int, -- 0
		CountFTLShipments int, -- 1
		CountAirShipments int, -- 2
		CountRailShipments int, -- 3
		CountSmallPackageShipments int, -- 4
		EstRevenueLTL decimal(18,4),
		EstRevenueFTL decimal(18,4),
		EstRevenueAir decimal(18,4),
		EstRevenueRail decimal(18,4),
		EstRevenueSmallPackage decimal(18,4),
		EstRevenueTotal decimal(18,4),
		EstProfitLTL decimal(18,4),
		EstProfitFTL decimal(18,4),
		EstProfitAir decimal(18,4),
		EstProfitRail decimal(18,4),
		EstProfitSmallPackage decimal(18,4),
		EstProfitTotal decimal(18,4),
		
		ActRevenueTotal decimal(18,4),
		ActProfitTotal decimal(18,4),
		CustomerType nvarchar(50)
	)

	Insert into #CustRevs exec CustomerRevenueReport @TenantId, @UserId
				
	-- Return Data Set
	SELECT 
		r.CustomerNumber,
		r.CustomerName,
		r.CustomField1,
		r.CustomField2,
		r.CustomField3,
		r.CustomField4,
		r.TierNumber,
		r.TierName,
		r.CustomerCreateDate,
		r.SalesRepresentativeNumber,
		r.SalesRepresentativeName,
		r.CountLTLShipments,
		r.CountFTLShipments,
		r.CountAirShipments,
		r.CountRailShipments,
		r.CountSmallPackageShipments,
		r.EstRevenueLTL,
		r.EstRevenueFTL,
		r.EstRevenueAir,
		r.EstRevenueRail,
		r.EstRevenueSmallPackage,
		r.EstProfitLTL,
		r.EstProfitFTL,
		r.EstProfitAir,
		r.EstProfitRail,
		r.EstProfitSmallPackage,
		r.EstProfitTotal,
		r.ActRevenueTotal,
		r.ActProfitTotal,
		r.CustomerType
	FROM #CustRevs r

	-- Drop Table
	DROP TABLE #CustRevs
