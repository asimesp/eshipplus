DECLARE @TenantId bigint = 4;
DECLARE @UserId bigint = 3;

SELECT
	COUNT(*)
FROM 
	[User]
	
SELECT 
	u.Username,
	u.FirstName,
	u.LastName,
	u.Email,
	u.TenantEmployee,
	u.FailedLoginAttempts,
	ISNULL(ud.Code,''),--user department code
	ISNULL(ud.[Description],''),
	ISNULL(ud.Phone,''),
	ISNULL(ud.Fax,''),
	ISNULL(ud.Email,''),
	c.Name,
	c.CustomerNumber,
	u.Phone,
	u.Fax,
	u.Mobile,
	u.Street1,
	u.Street2,
	u.City,
	u.[State],
	co.Name,
	u.PostalCode,
		u.[Enabled],
	(CASE WHEN u.ForcePasswordReset = 0 THEN 'No' ELSE 'Yes' END),
	u.IsShipmentCoordinator,
	u.IsCarrierCoordinator
FROM 
	[User] AS u 
	INNER JOIN Customer AS c ON c.Id = u.CustomerId
	LEFT JOIN UserDepartment AS ud ON ud.Id = u.UserDepartmentId
	LEFT JOIN Country as co on co.Id = u.CountryId
WHERE 
	@TenantId <> 0 
		AND @UserId <> 0 
		
