
SELECT
	{0}
FROM
	Shipment AS sh
	INNER JOIN ShipmentCharge AS shc ON sh.Id = shc.ShipmentId
	INNER JOIN ChargeCode AS cc ON cc.Id = shc.ChargeCodeId
	INNER JOIN [User] AS u ON u.Id = sh.UserId
	INNER JOIN Customer AS c ON c.Id = sh.CustomerId
	INNER JOIN Tier as t ON t.Id = c.TierId--check on this
	INNER JOIN ShipmentLocation AS shol ON shol.Id = sh.OriginId
	INNER JOIN Country AS oc ON oc.Id = shol.CountryId
	INNER JOIN ShipmentLocation AS shdl ON shdl.Id = sh.DestinationId
	INNER JOIN Country AS dc ON dc.Id = shdl.CountryId
	INNER JOIN ShipmentVendor AS shv ON shv.ShipmentId = sh.Id 
	INNER JOIN Vendor AS v ON v.Id = shv.VendorId AND shv.[Primary] = 1
	INNER JOIN ShipmentChargeStatistics scs ON scs.ShipmentId = sh.Id
	INNER JOIN ShipmentAccountBucket sac on sac.ShipmentId = sh.Id And sac.[Primary] = 1
	INNER JOIN AccountBucket spac on spac.Id = sac.AccountBucketId

	Left Join [User] ccu on ccu.Id = sh.CarrierCoordinatorUserId
	Left Join [User] scu on scu.Id = sh.ShipmentCoordinatorUserId
	Left Join Prefix sp on sp.Id = sh.PrefixId
	Left Join ShipmentItemStatistics sis ON sis.ShipmentId = sh.Id
	Left Join RefServiceMode sm ON sm.ServiceModeIndex = sh.ServiceMode
	Left Join RefShipmentStatus ss ON ss.ShipmentStatusIndex = sh.[Status]
	Left Join ResellerAddition ra ON ra.Id = sh.ResellerAdditionId
WHERE
	@TenantId <> 0 
              AND @UserId <> 0
              AND sh.TenantId = @TenantId
              AND (
				(Select COUNT(*) from [User] where id = @UserId and TenantEmployee = 1) > 0
				OR
				(sh.CustomerId in (SELECT CustomerId FROM UserShipAs WHERE TenantId = @TenantId and UserId = @UserId
					UNION
					SELECT [User].CustomerId FROM [User] WHERE Id = @UserId)))
			  AND (@CustomerGroupId = 0 OR (Select COUNT(*) from CustomerGroupMap WHERE CustomerId = sh.CustomerId and CustomerGroupId = @CustomerGroupId) > 0)
			  AND (@VendorGroupId = 0 OR (Select COUNT(*) from VendorGroupMap WHERE VendorId = shv.VendorId and VendorGroupId = @VendorGroupId) > 0)
		      AND {1} 
ORDER BY 
	{2}

OPTION (RECOMPILE)