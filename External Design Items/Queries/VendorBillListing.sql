
declare @TenantId bigint = 4
declare @UserId bigint = 2

SELECT 
		vb.DocumentNumber,
		vb.DateCreated,
		vb.DocumentDate,
		(CASE
			WHEN vb.Posted = 0 THEN 'Posted'
			WHEN vb.Posted = 1 THEN 'Not Posted'
		END),
		v.Name,-- 'VendorName',
		v.VendorNumber,
		(SELECT SUM(d.UnitBuy * d.Quantity ) 
		 FROM VendorBillDetail d 
		 WHERE d.VendorBillId = vb.Id) 'AmountDue',
		ISNULL((SELECT DISTINCT (d.ReferenceNumber + ', ')
				FROM VendorBillDetail d
				WHERE d.VendorBillId = vb.Id AND d.ReferenceType = 0 --Shipment
				FOR XML PATH('')),''),-- 'Shipments'
		ISNULL((SELECT DISTINCT (d.ReferenceNumber + ', ')
				FROM VendorBillDetail d
				WHERE d.VendorBillId = vb.Id AND d.ReferenceType = 1 --Service Ticket
				FOR XML PATH('')),''),-- 'ServiceTickets'
		ISNULL(it.InvoiceTypeText, '') AS 'BillType',
		u.FirstName 'CreatedByFirstName',
		u.LastName 'CreatedByLastName',
		u.Username 'CreatedByUsername'
FROM 
	VendorBill AS vb
		INNER JOIN VendorLocation vl ON vb.VendorLocationId = vl.[Id]
		INNER JOIN Vendor v ON v.Id = vl.VendorId
		INNER JOIN [User] u ON u.Id = vb.UserId
		LEFT JOIN RefInvoiceType it ON it.InvoiceTypeIndex = vb.BillType
		
WHERE
	 vb.TenantId = @TenantId	
		AND @UserId <> 0 
		
SELECT
	COUNT(*)
FROM 
	VendorBill