declare @YearOne bigint = 2011;
declare @YearTwo bigint = 2012;
declare @TenantId bigint = 4;
declare @CustomerName nvarchar(50) = 'All';
declare @CustomerNumber nvarchar(50) = 'All';
declare @PrimaryVendorName nvarchar(50) = 'All';
declare @PrimaryVendorNumber nvarchar(50) = 'All';
declare @ServiceModeText nvarchar(50) = 'All';
declare @AccountBucketCode nvarchar(50) = 'All';
declare @AccountBucketDescription nvarchar(50) = 'All';

IF EXISTS (SELECT * FROM tempdb.sys.objects WHERE name LIKE '#RevenuePerMonth%')  
		DROP TABLE #RevenuePerMonth

Create Table #RevenuePerMonth(
	[Month] int
	,ShipmentsInYearOne int
	,ShipmentsInYearTwo int
	,EstimatedRevenueYearOne decimal(18,4)
	,ActualRevenueYearOne decimal(18,4)
	,EstimatedRevenueYearTwo decimal(18,4)
	,ActualRevenueYearTwo decimal(18,4)
);

Insert #RevenuePerMonth([Month],ShipmentsInYearOne,ShipmentsInYearTwo, EstimatedRevenueYearOne,ActualRevenueYearOne,EstimatedRevenueYearTwo,ActualRevenueYearTwo)
Select 
		(Month(s.DateCreated)) as 'Month'
		,(Sum((Case When Year(s.DateCreated) = @YearOne Then 1 Else 0 End))) as 'YearOne'
		,(Sum((Case When Year(s.DateCreated) = @YearTwo Then 1 Else 0 End))) as 'YearTwo'
		,Isnull((Sum((Case When Year(s.DateCreated) = @YearOne Then scs.TotalAmountDue Else 0 End))),0) as  'EstimatedRevenueYearOne'
		,Isnull((Sum((Case When Year(s.DateCreated) = @YearOne Then (sar.TotalActualCharges - sar.TotalCredit + sar.TotalSupplementalCharges) Else 0 End))),0) as 'ActualRevenueYearOne'
		,Isnull((Sum((Case When Year(s.DateCreated) = @YearTwo Then scs.TotalAmountDue Else 0 End))),0) as  'EstimatedRevenueYearTwo'
		,Isnull((Sum((Case When Year(s.DateCreated) = @YearTwo Then (sar.TotalActualCharges - sar.TotalCredit + sar.TotalSupplementalCharges) Else 0 End))),0) as 'ActualRevenueYearTwo'
		
From shipment s
	INNER JOIN ShipmentAccountBucket shab ON s.Id = shab.ShipmentId AND shab.[Primary] = 1
	INNER JOIN AccountBucket ab ON shab.AccountBucketId = ab.Id 
	INNER JOIN Customer c ON s.CustomerId = c.Id
	INNER JOIN ShipmentVendor shv ON shv.ShipmentId = s.Id AND shv.[Primary] = 1
	INNER JOIN Vendor v ON shv.VendorId = v.Id
	LEFT JOIN ShipmentChargeStatistics scs On scs.ShipmentId = s.Id
	LEFT JOIN ShipmentActualRevenues(@TenantId, '17530101', '99990101') sar On sar.Id = s.Id
	

Where 
	1 = 1 
	AND (c.Name Like (CASE When @CustomerName = 'All' Then '%' 
					When @CustomerName = '' Then '%'
					Else @CustomerName End))
	AND (c.CustomerNumber Like (CASE When @CustomerNumber = 'All' Then '%' 
					When @CustomerNumber = '' Then '%'
					Else @CustomerNumber End))	
	AND (v.Name Like (CASE When @PrimaryVendorName = 'All' Then '%' 
					When @PrimaryVendorName = '' Then '%'
					Else @PrimaryVendorName End))
	AND (v.VendorNumber Like (CASE When @PrimaryVendorNumber = 'All' Then '%' 
					When @PrimaryVendorNumber = '' Then '%'
					Else @PrimaryVendorNumber End))
	AND (ab.Code Like (CASE When @AccountBucketCode = 'All' Then '%' 
					When @AccountBucketCode = '' Then '%'
					Else @AccountBucketCode End))
	AND (ab.[Description] Like (CASE When @AccountBucketDescription = 'All' Then '%' 
					When @AccountBucketDescription = '' Then '%'
					Else @AccountBucketDescription End))
	AND (Convert(NVARCHAR(50), s.ServiceMode) like (CASE When @ServiceModeText = 'All' Then '%' 
					When @ServiceModeText = '' Then '%'
					WHEN @ServiceModeText = 'LessThanTruckLoad' THEN '0'
					WHEN @ServiceModeText = 'Truckload' THEN '1' 
					WHEN @ServiceModeText = 'PartialTruckload'  THEN '2'
					WHEN @ServiceModeText = 'Air' THEN '3'
					WHEN @ServiceModeText = 'Rail' THEN '4'
					WHEN @ServiceModeText = 'SmallPackage'Then '5'
					Else @ServiceModeText End))

Group By Month(s.DateCreated)

Select *
From #RevenuePerMonth
Where ShipmentsInYearTwo > 2385

DROP TABLE #RevenuePerMonth
