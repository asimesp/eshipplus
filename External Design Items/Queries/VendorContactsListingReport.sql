SELECT
	COUNT(*)
FROM 
	Vendor
	
declare @TenantId bigint = 4
declare @UserId bigint = 2
declare @CustomerGroupId bigint = 0
declare @VendorGroupId bigint = 0

SELECT
	v.DateCreated,
	(CASE 
		WHEN v.DateCreated = '1753-01-01 00:00:00.000' THEN '' 
		ELSE CONVERT(nvarchar(20),v.DateCreated,101) 
	END),--view date created
	v.VendorNumber,
	v.Name,
	(CASE
		WHEN v.Active = 0 THEN 'No'
		ELSE 'Yes'
	END),--vendor active
	v.BrokerReferenceNumber,
	ISNULL(brnt.BrokerReferenceNumberTypeText, '') AS 'BrokerReferenceNumberType',
	(CASE
		WHEN v.CTPATCertified = 0 THEN 'No'
		ELSE 'Yes'
	END),--CTPAT Certified
	v.CTPATNumber,
	v.DOT,
	v.FederalIDNumber,
	(CASE
		WHEN v.HandlesAir = 0 THEN 'No'
		ELSE 'Yes'
	END),
	(CASE
		WHEN v.HandlesLessThanTruckload = 0 THEN 'No'
		ELSE 'Yes'
	END),
	(CASE
		WHEN v.HandlesPartialTruckload = 0 THEN 'No'
		ELSE 'Yes'
	END),
	(CASE
		WHEN v.HandlesTruckload = 0 THEN 'No'
		ELSE 'Yes'
	END),
	(CASE
		WHEN v.HandlesRail = 0 THEN 'No'
		ELSE 'Yes'
	END),
	(CASE
		WHEN v.HandlesSmallPack = 0 THEN 'No'
		ELSE 'Yes'
	END),
	(CASE
		WHEN v.IsAgent = 0 THEN 'No'
		ELSE 'Yes'
	END),
	(CASE
		WHEN V.IsBroker = 0 THEN 'No'
		ELSE 'Yes'
	END),
	(CASE
		WHEN V.IsCarrier = 0 THEN 'No'
		ELSE 'Yes'
	END),
	v.MC,
	v.Notation,
	v.Notes,
	(CASE
		WHEN v.PIPCertified = 0 THEN 'No'
		ELSE 'Yes'
	END),
	v.PIPNumber,
	v.Scac,
	(CASE
		WHEN v.SmartWayCertified = 0 THEN 'False'
		ELSE 'Yes'
	END),
	(CASE
		WHEN v.TSACertified = 0 THEN 'No'
		ELSE 'Yes'
	END),
	vl.LocationNumber,
	(CASE
		WHEN vl.RemitToLocation = 0 THEN 'No'
		ELSE 'Yes'
	END),
	vl.Street1,
	vl.Street2,
	vl.City,
	vl.[State],
	vl.PostalCode,
	ct.Name,
	(CASE
		WHEN vc.[Primary] = 0 THEN 'No'
		ELSE 'Yes'
	END),-- Primary contact
	ISNULL(vc.Name,''),
	ISNULL(ctype.Code, ''),
	ISNULL(ctype.[Description],''),
	ISNULL(vc.Phone,''),
	ISNULL(vc.Mobile,''),
	ISNULL(vc.Email,''),
	ISNULL(vc.Fax,'')
FROM	
	Vendor v
	INNER JOIN
		VendorLocation AS vl ON vl.VendorId = v.Id  
	INNER JOIN
		Country AS ct ON ct.Id = vl.CountryId
	LEFT JOIN
		VendorContact AS vc ON vc.VendorLocationId = vl.Id
	LEFT JOIN
		ContactType  AS ctype ON ctype.Id = vc.ContactTypeId
	LEFT JOIN
		RefBrokerReferenceNumberType AS brnt ON brnt.BrokerReferenceNumberTypeIndex = v.BrokerReferenceNumberType
WHERE
	@TenantId <> 0 
              AND @UserId <> 0
              AND v.TenantId = @TenantId
              AND (SELECT COUNT(*) FROM [User] WHERE id = @UserId AND TenantEmployee = 1) > 0
			  AND (@VendorGroupId = 0 OR (SELECT COUNT(*) FROM VendorGroupMap WHERE VendorId = v.Id AND VendorGroupId = @VendorGroupId) > 0)
--		      AND {1} 
--ORDER BY 
--	{2}