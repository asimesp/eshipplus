
DECLARE @TenantId bigint = 4
DECLARE @UserId BIGINT = 4


SELECT
	t.Name,
	t.TierNumber,
	t.AdditionalBillOfLadingText,
	t.GeneralNotificationEmails,
	t.TollFreeContactNumber,
	t.DateCreated,
	(CASE WHEN t.DateCreated = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),t.DateCreated,101) END) AS 'ViewDateCreated', 
	(CASE WHEN t.Active = 1 THEN 'Yes' ELSE 'No' END) AS 'Active',
	t.AirNotificationEmails,
	t.RailNotificationEmails,
	t.SPNotificationEmails,
	t.FTLNotificationEmails,
	t.LTLNotificationEmails,
	t.SupportTollFree,
	t.TierSupportEmails
FROM
	Tier AS t
WHERE
	@TenantId <> 0
	AND @UserId <> 0
	AND t.TenantId = @TenantId
--	AND {1}
--ORDER BY {2}