DECLARE @TenantId bigint = 4
DECLARE @ActiveUserId bigint = 2
DECLARE @CustomerGroupId bigint = 0
DECLARE @StartDate datetime = '2012-11-19 00:00:00'
DECLARE @Mode nvarchar(50) = 'all'
DECLARE @AccountBucketCode nvarchar(50) = 'all'

-- set service mode filter
declare @sm int = -1;

SET @Mode = LOWER(REPLACE(@Mode, ' ', ''))
	
IF @Mode = 'lessthantruckload' OR @Mode = 'ltl' BEGIN SET @sm = 0 END
ELSE IF @Mode = 'truckload' or @Mode = 'ftl' BEGIN SET @sm = 1 END
ELSE IF @Mode = 'air' BEGIN	SET @sm = 2 END
ELSE IF @Mode = 'rail' BEGIN SET @sm = 3 END
ELSE IF @Mode = 'smallpackage' or @Mode = 'sp' BEGIN SET @sm = 4 END
ELSE IF @Mode = 'all' or @Mode = '' BEGIN SET @sm = -1 END
ELSE BEGIN SET @sm = -2 END

-- setup date ranges


	Select 
		c.Id 'CustomerId'
		,c.Name 'CustomerName'
		,c.CustomField1
		,c.CustomField2
		,c.CustomField3
		,c.CustomField4
		,c.CustomerNumber 'CustomerNumber'
		,ISNULL(sr.Name, '') 'SalesRepName'
		,ISNULL(sr.SalesRepresentativeNumber, '') 'SalesRepNumber'
		,ab.Code 'CustomerAccountBucketCode'
		,ab.[Description] 'CustomerAccountBucketDescription'
	
		,ISNULL((Select Top 1 (DateDiff(Day, s.DateCreated, @StartDate)) from Shipment s Inner Join ShipmentAccountBucket sab on sab.ShipmentId = s.Id And sab.[Primary] = 1 Inner Join AccountBucket psab on psab.Id = sab.AccountBucketId Where s.DateCreated < @StartDate And s.CustomerId = c.Id And s.[Status] <> 4 And (@sm = -1 OR s.ServiceMode = @sm) And (@AccountBucketCode = '' OR @AccountBucketCode = 'all' OR psab.Code = @AccountBucketCode) Order By s.DateCreated Desc),-1) 'Days since last shipment'
		,ISNULL((Select Top 1 s.DateCreated from Shipment s Inner Join ShipmentAccountBucket sab on sab.ShipmentId = s.Id And sab.[Primary] = 1 Inner Join AccountBucket psab on psab.Id = sab.AccountBucketId Where s.DateCreated < @StartDate And s.CustomerId = c.Id And s.[Status] <> 4 And (@sm = -1 OR s.ServiceMode = @sm) And (@AccountBucketCode = '' OR @AccountBucketCode = 'all' OR psab.Code = @AccountBucketCode) Order By s.DateCreated Desc),'1753-01-01 00:00:00') 'Date since last shipment'
		
		--Analysis per mode columns
		,Isnull(ShipmentInfoForLastWeek.NumberOfShipments,0) 'ShipmentsLastWeek'
		,Isnull(ShipmentInfoWklyAvgFor4WeeksPrior.NumberOfShipments,0) 'WklyAvgShipments4WeeksPrior'
		,Isnull(ShipmentInfoWklyAvgFor26WeeksPrior.NumberOfShipments,0) 'WklyAvgShipments26WeeksPrior'
		,Isnull(ShipmentInfoWklyAvgFor52WeeksPrior.NumberOfShipments,0) 'WklyAvgShipments52WeeksPrior'

		,Isnull(ShipmentInfoForLastWeek.TotalRevenue,0) 'RevenueLastWeek'
		,Isnull(ShipmentInfoWklyAvgFor4WeeksPrior.TotalRevenue,0) 'WklyAvgRevenue4WeeksPrior'
		,Isnull(ShipmentInfoWklyAvgFor26WeeksPrior.TotalRevenue,0) 'WklyAvgRevenue26WeeksPrior'
		,Isnull(ShipmentInfoWklyAvgFor52WeeksPrior.TotalRevenue,0) 'WklyAvgRevenue52WeeksPrior'

		,Isnull(ShipmentInfoForLastWeek.TotalProfit,0) 'ProfitLastWeek'
		,Isnull(ShipmentInfoWklyAvgFor4WeeksPrior.TotalProfit,0) 'WklyAvgProfit4WeeksPrior'
		,Isnull(ShipmentInfoWklyAvgFor26WeeksPrior.TotalProfit,0) 'WklyAvgProfit26WeeksPrior'
		,Isnull(ShipmentInfoWklyAvgFor52WeeksPrior.TotalProfit,0) 'WklyAvgProfit52WeeksPrior'
		
		,(Select Count(*) From Shipment s Inner Join ShipmentAccountBucket sab on sab.ShipmentId = s.Id And sab.[Primary] = 1 Inner Join AccountBucket psab on psab.Id = sab.AccountBucketId Where (DateDiff(Week, s.DateCreated, @StartDate) Between 1 And 4) And s.CustomerId = c.Id And s.[Status] <> 4  And (@sm = -1 OR s.ServiceMode = @sm) And (@AccountBucketCode = '' OR @AccountBucketCode = 'all' OR psab.Code = @AccountBucketCode)) 'ShipmentsLast4Weeks'
		,(Select Count(*) From Shipment s Inner Join ShipmentAccountBucket sab on sab.ShipmentId = s.Id And sab.[Primary] = 1 Inner Join AccountBucket psab on psab.Id = sab.AccountBucketId Where (DateDiff(Week, s.DateCreated, @StartDate) Between 5 And 16) And s.CustomerId = c.Id And s.[Status] <> 4  And (@sm = -1 OR s.ServiceMode = @sm) And (@AccountBucketCode = '' OR @AccountBucketCode = 'all' OR psab.Code = @AccountBucketCode)) / 3.0 '4WeeklyAvgShipments12WeeksPrior'

		,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s  Inner Join ShipmentAccountBucket sab on sab.ShipmentId = s.Id And sab.[Primary] = 1 Inner Join AccountBucket psab on psab.Id = sab.AccountBucketId Left Join ShipmentChargeStatistics scs on scs.ShipmentId = s.Id Where s.CustomerId = c.Id And s.[Status] <> 4 And (@sm = -1 OR s.ServiceMode = @sm) And (@AccountBucketCode = '' OR @AccountBucketCode = 'all' OR psab.Code = @AccountBucketCode) And (DateDiff(Week, s.DateCreated, @StartDate) Between 1 And 4)),0) 'RevenueLast4Weeks'
		,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s Inner Join ShipmentAccountBucket sab on sab.ShipmentId = s.Id And sab.[Primary] = 1 Inner Join AccountBucket psab on psab.Id = sab.AccountBucketId Left Join ShipmentChargeStatistics scs on scs.ShipmentId = s.Id Where s.CustomerId = c.Id And s.[Status] <> 4 And (@sm = -1 OR s.ServiceMode = @sm) And (@AccountBucketCode = '' OR @AccountBucketCode = 'all' OR psab.Code = @AccountBucketCode) And (DateDiff(Week, s.DateCreated, @StartDate) Between 5 And 16)),0) / 3.0 '4WeeklyAverageRevenue12WeeksPrior'

		,ISNULL((Select SUM(scs.TotalProfit) From Shipment s Inner Join ShipmentAccountBucket sab on sab.ShipmentId = s.Id And sab.[Primary] = 1 Inner Join AccountBucket psab on psab.Id = sab.AccountBucketId Left Join ShipmentChargeStatistics scs on scs.ShipmentId = s.Id Where s.CustomerId = c.Id And s.[Status] <> 4 And (@sm = -1 OR s.ServiceMode = @sm) And (@AccountBucketCode = '' OR @AccountBucketCode = 'all' OR psab.Code = @AccountBucketCode) And (DateDiff(Week, s.DateCreated, @StartDate) Between 1 And 4)),0)'ProfitLast4Weeks'
		,ISNULL((Select SUM(scs.TotalProfit) From Shipment s Inner Join ShipmentAccountBucket sab on sab.ShipmentId = s.Id And sab.[Primary] = 1 Inner Join AccountBucket psab on psab.Id = sab.AccountBucketId Left Join ShipmentChargeStatistics scs on scs.ShipmentId = s.Id Where s.CustomerId = c.Id And s.[Status] <> 4 And (@sm = -1 OR s.ServiceMode = @sm) And (@AccountBucketCode = '' OR @AccountBucketCode = 'all' OR psab.Code = @AccountBucketCode) And (DateDiff(Week, s.DateCreated, @StartDate) Between 5 And 16)),0) / 3.0 '4WeeklyAverageProfit12WeeksPrior'
	From 
		Customer c
		Inner Join AccountBucket ab on c.DefaultAccountBucketId = ab.Id
		Left Join SalesRepresentative sr on c.SalesRepresentativeId = sr.Id

		Left Join (
			Select 
				c.Id 'CustomerId'
				,Count(s.Id) 'NumberOfShipments' 
				,SUM(scs.TotalAmountDue) 'TotalRevenue'
				,SUM(scs.TotalProfit) 'TotalProfit'
			From Shipment s 
				Inner Join Customer c on c.Id = s.CustomerId
				Inner Join ShipmentAccountBucket sab on sab.ShipmentId = s.Id And sab.[Primary] = 1
				Inner Join AccountBucket psab on psab.Id = sab.AccountBucketId 
				Left Join ShipmentChargeStatistics scs on scs.ShipmentId = s.Id
			Where 
				(DateDiff(Week, s.DateCreated, @StartDate) = 1) 
				And s.[Status] <> 4  
				And (@sm = -1 OR s.ServiceMode = @sm) 
				And (@AccountBucketCode = '' OR @AccountBucketCode = 'all' OR psab.Code = @AccountBucketCode)
			Group By c.Id
		) as ShipmentInfoForLastWeek on ShipmentInfoForLastWeek.CustomerId = c.Id

		Left Join (
			Select 
				c.Id 'CustomerId'
				,Count(s.Id) / 4.0 'NumberOfShipments' 
				,Isnull(SUM(scs.TotalAmountDue),0) / 4.0 'TotalRevenue'
				,Isnull(SUM(scs.TotalProfit),0) / 4.0 'TotalProfit'
			From Shipment s 
				Inner Join Customer c on c.Id = s.CustomerId
				Inner Join ShipmentAccountBucket sab on sab.ShipmentId = s.Id And sab.[Primary] = 1
				Inner Join AccountBucket psab on psab.Id = sab.AccountBucketId 
				Left Join ShipmentChargeStatistics scs on scs.ShipmentId = s.Id
			Where 
				DateDiff(Week, s.DateCreated, @StartDate) Between 2 And 5
				And s.[Status] <> 4  
				And (@sm = -1 OR s.ServiceMode = @sm) 
				And (@AccountBucketCode = '' OR @AccountBucketCode = 'all' OR psab.Code = @AccountBucketCode)
			Group By c.Id
		) as ShipmentInfoWklyAvgFor4WeeksPrior on ShipmentInfoWklyAvgFor4WeeksPrior.CustomerId = c.Id

		Left Join (
			Select 
				c.Id 'CustomerId'
				,Count(s.Id) / 26.0 'NumberOfShipments' 
				,Isnull(SUM(scs.TotalAmountDue),0) / 26.0 'TotalRevenue'
				,Isnull(SUM(scs.TotalProfit),0) / 26.0 'TotalProfit'
			From Shipment s 
				Inner Join Customer c on c.Id = s.CustomerId
				Inner Join ShipmentAccountBucket sab on sab.ShipmentId = s.Id And sab.[Primary] = 1
				Inner Join AccountBucket psab on psab.Id = sab.AccountBucketId 
				Left Join ShipmentChargeStatistics scs on scs.ShipmentId = s.Id
			Where 
				DateDiff(Week, s.DateCreated, @StartDate) Between 2 And 27
				And s.[Status] <> 4  
				And (@sm = -1 OR s.ServiceMode = @sm) 
				And (@AccountBucketCode = '' OR @AccountBucketCode = 'all' OR psab.Code = @AccountBucketCode)
			Group By c.Id
		) as ShipmentInfoWklyAvgFor26WeeksPrior on ShipmentInfoWklyAvgFor26WeeksPrior.CustomerId = c.Id

		Left Join (
			Select 
				c.Id 'CustomerId'
				,Count(s.Id) / 52.0 'NumberOfShipments' 
				,Isnull(SUM(scs.TotalAmountDue),0) / 52.0 'TotalRevenue'
				,Isnull(SUM(scs.TotalProfit),0) / 52.0 'TotalProfit'
			From Shipment s 
				Inner Join Customer c on c.Id = s.CustomerId
				Inner Join ShipmentAccountBucket sab on sab.ShipmentId = s.Id And sab.[Primary] = 1
				Inner Join AccountBucket psab on psab.Id = sab.AccountBucketId 
				Left Join ShipmentChargeStatistics scs on scs.ShipmentId = s.Id
			Where 
				DateDiff(Week, s.DateCreated, @StartDate) Between 2 And 53
				And s.[Status] <> 4  
				And (@sm = -1 OR s.ServiceMode = @sm) 
				And (@AccountBucketCode = '' OR @AccountBucketCode = 'all' OR psab.Code = @AccountBucketCode)
			Group By c.Id
		) as ShipmentInfoWklyAvgFor52WeeksPrior on ShipmentInfoWklyAvgFor52WeeksPrior.CustomerId = c.Id

	where c.TenantId = @TenantId
					AND (
						(SELECT COUNT(*) FROM [User] WHERE Id = @ActiveUserId And TenantEmployee = 1) > 0
						OR
						(c.Id IN 
							(SELECT CustomerId FROM UserShipAs WHERE TenantId = @TenantId AND UserId = @ActiveUserId
							 UNION
							 SELECT [User].CustomerId FROM [User] WHERE Id = @ActiveUserId)))
					AND (@CustomerGroupId = 0 OR (Select COUNT(*) from CustomerGroupMap WHERE CustomerId = c.Id And CustomerGroupId = @CustomerGroupId) > 0)
				

Order By
	ShipmentInfoWklyAvgFor52WeeksPrior.NumberOfShipments Desc
	