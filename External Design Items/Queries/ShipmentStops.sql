DECLARE @TenantId bigint = 4;
DECLARE @UserId bigint = 9;
DECLARE @CustomerGroupId bigint = 0;
DECLARE @VendorGroupId bigint = 0;

SELECT 
	ShipmentId
	,ShipmentNumber
	,ServiceMode
	,DateCreated
	,DesiredPickupDate
	,ShipmentStatus
	,TotalWeight
	,TotalCharges
	,TotalCost
	,TotalProfit

	,SalesRepresentativeCommissionPercent
	,SalesRepName

	,CarrierCoordinatorFirstName
	,CarrierCoordinatorLastName
	,CarrierCoordinatorUsername

	,ShipmentCoordinatorFirstName
	,ShipmentCoordinatorLastName
	,ShipmentCoordinatorUsername

	,PrimaryDriverAssetName
	,PrimaryTractorAssetName

	,AccountBucketCode
	,AccountBucketDescription
	,AccountBucketUnit

	,TierName

	,UserId
	,Username
	,UserFirstName
	,UserLastName
	,IsEmployee

	,PrimaryVendorName
	,PrimaryVendorNumber

	,CustomerName
	,CustomerNumber

	,OriginName
	,OriginStreet1
	,OriginStreet2
	,OriginCity
	,OriginState
	,OriginPostalCode
	,OriginCountryCode
	,OriginContactName
	,OriginContactPhone
	,OriginContactEmail

	,DestinationName
	,DestinationStreet1
	,DestinationStreet2
	,DestinationCity
	,DestinationState
	,DestinationPostalCode
	,DestinationCountryCode
	,DestinationContactName
	,DestinationContactPhone
	,DestinationContactEmail

	,Stop1Name
	,Stop1Street1
	,Stop1Street2
	,Stop1City
	,Stop1State
	,Stop1PostalCode
	,Stop1CountryCode
	,Stop1ContactName
	,Stop1ContactPhone
	,Stop1ContactEmail

	,Stop2Name
	,Stop2Street1
	,Stop2Street2
	,Stop2City
	,Stop2State
	,Stop2PostalCode
	,Stop2CountryCode
	,Stop2ContactName
	,Stop2ContactPhone
	,Stop2ContactEmail

	,Stop3Name
	,Stop3Street1
	,Stop3Street2
	,Stop3City
	,Stop3State
	,Stop3PostalCode
	,Stop3CountryCode
	,Stop3ContactName
	,Stop3ContactPhone
	,Stop3ContactEmail

	,Stop4Name
	,Stop4Street1
	,Stop4Street2
	,Stop4City
	,Stop4State
	,Stop4PostalCode
	,Stop4CountryCode
	,Stop4ContactName
	,Stop4ContactPhone
	,Stop4ContactEmail

	,Stop5Name
	,Stop5Street1
	,Stop5Street2
	,Stop5City
	,Stop5State
	,Stop5PostalCode
	,Stop5CountryCode
	,Stop5ContactName
	,Stop5ContactPhone
	,Stop5ContactEmail
FROM
	dbo.ShipmentStops(@TenantId,@UserId, @CustomerGroupId, @VendorGroupId)