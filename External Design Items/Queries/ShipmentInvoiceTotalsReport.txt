SELECT
	{0}
FROM	
	Invoice i
 	INNER JOIN 
	 (
		Select
			InvoiceId
			,ReferenceNumber
			,sum(quantity * (UnitSell - UnitDiscount)) 'InvoiceShipmentAmount'
		From
			InvoiceDetail 
		Where
			ReferenceType = 0 -- shipment only
		Group By	
			InvoiceId, ReferenceNumber
	 ) as id ON id.InvoiceId = i.Id
 	INNER JOIN Shipment s ON s.ShipmentNumber = id.ReferenceNumber
 	INNER JOIN CustomerLocation cl ON cl.Id = i.CustomerLocationId
 	INNER JOIN Customer c on c.Id = cl.CustomerId
WHERE
	{1}