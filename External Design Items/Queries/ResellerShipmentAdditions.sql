--SELECT COUNT(*) FROM Shipment


SELECT
	sh.Id 'ShipmentId',
	sh.ShipmentNumber,
	sh.DateCreated,
	(CASE WHEN sh.DateCreated = '1753-01-01 00:00:00.000' 
		THEN '' 
		ELSE CONVERT(nvarchar(20),sh.DateCreated,101) 
	END) AS 'ViewDateCreated', 
	ISNULL(ss.ShipmentStatusText, '') AS 'Shipment Status',
	rsa.ResellerAdditionName,
	rsa.ShipmentCustomerAccountName,
	rsa.ShipmentCustomerAccountNumber,
	rsa.ResellerCustomerAccountNumber,
	rsa.ResellerCustomerAccountName,
	(CASE WHEN sh.BillReseller = 0 THEN 'No' ELSE 'Yes' END) AS 'BillingResellerCustomerAccount',
	
	-- Shipment Charges
	rsa.AccessorialCharges,
	rsa.FreightCharges,
	rsa.ServiceCharges,
	rsa.FuelCharges,
	rsa.TotalCharges,
	rsa.TotalDiscount,
	rsa.TotalAmountDue, 
	
	-- Shipment Costs
	scs.AccessorialCost,
	scs.FreightCost,
	scs.FuelCost,
	scs.ServiceCost,
	scs.TotalCost,
	
	-- Accessorials
	rsa.AccessorialCharges,
	(CASE WHEN sh.BillReseller = 0 THEN rsa.AccessorialCharges ELSE rsa.AccessorialCharges - rsa.ResellerAccessorialProfitShare END) 'Invoice Assessorial Charges',
	rsa.AccessorialMarkupIsPercent,
	rsa.ResellerAdditionalAccessorialMarkup,
	rsa.AccessorialProfit,
	rsa.ResellerAccessorialProfitShare,
	
	-- Services
	rsa.ServiceCharges,
	(CASE WHEN sh.BillReseller = 0 THEN rsa.ServiceCharges ELSE rsa.ServiceCharges - rsa.ResellerServiceProfitShare END) 'Invoice Service Charges',
	rsa.ServiceMarkupIsPercent,
	rsa.ResellerAdditionalServiceMarkup,
	rsa.ServiceProfit,
	rsa.ResellerServiceProfitShare,
	
	-- Fuel
	rsa.FuelCharges,
	(CASE WHEN sh.BillReseller = 0 THEN rsa.FuelCharges ELSE rsa.FuelCharges - rsa.ResellerFuelProfitShare END) 'Invoice Fuel Charges',
	rsa.FuelMarkupIsPercent,
	rsa.ResellerAdditionalFuelMarkup,
	rsa.FuelProfit,
	rsa.ResellerFuelProfitShare,
	
	-- Freight
	rsa.FreightCharges,
	(CASE WHEN sh.BillReseller = 0 THEN rsa.FreightCharges ELSE rsa.FreightCharges - rsa.ResellerFreightProfitShare END) 'Invoice Freight Charges',
	rsa.FreightMarkupIsPercent,
	rsa.ResellerAdditionalFreightMarkup,
	rsa.FreightProfit,
	rsa.ResellerFreightProfitShare
	
FROM
	Shipment AS sh
		INNER JOIN ShipmentChargeStatistics AS scs on scs.ShipmentId = sh.Id
		INNER JOIN ResellerShipmentAddition AS rsa ON rsa.ShipmentId = sh.Id
		LEFT JOIN RefShipmentStatus AS ss ON ss.ShipmentStatusIndex = sh.[Status]
--WHERE
--	sh.ShipmentNumber = '40299'