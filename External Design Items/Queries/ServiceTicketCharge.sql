declare @TenantId bigint = 4
declare @UserId bigint = 2
declare @CustomerGroupId bigint = 0
declare @VendorGroupId bigint = 0


SELECT COUNT(*) FROM ServiceTicket WHERE TenantId = @TenantId

SELECT 
	st.ServiceTicketNumber,
	(CASE
		WHEN st.AuditedForInvoicing = 0 THEN 'No'
		ELSE 'Yes'
	END),
	st.DateCreated,
	(CASE WHEN st.DateCreated = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),st.DateCreated,101) END)'ViewDateCreated', 
	st.TicketDate,
	(CASE WHEN st.TicketDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),st.TicketDate,101) END)'ViewDateCreated', 
	c.CustomerNumber,
	c.Name,
	u.Username,
	u.FirstName,
	u.LastName,
	cc.Code,
	cc.[Description],
	ISNULL(sts.ServiceTicketStatusText, '') AS 'Status',
	ab.Code,
	ab.[Description],
	ISNULL(p.Code,''),
	ISNULL(p.[Description],''),
	shc.UnitBuy,
	shc.UnitSell,
	shc.Quantity,
	shc.UnitDiscount,
	shc.Comment,
	st.SalesRepresentativeCommissionPercent,
	st.SalesRepMinCommValue,
	st.SalesRepMaxCommValue,
	st.SalesRepAddlEntityName,
	st.SalesRepAddlEntityCommPercent,
	st.ExternalReference1,
	st.ExternalReference2
FROM
ServiceTicket AS st
INNER JOIN ServiceTicketCharge AS shc ON st.Id = shc.ServiceTicketId
INNER JOIN ChargeCode AS cc ON cc.Id = shc.ChargeCodeId
INNER JOIN [User] AS u ON u.Id = st.UserId
INNER JOIN Customer AS c ON c.Id = st.CustomerId
INNER JOIN Tier as t ON t.Id = c.TierId--check on this
INNER JOIN ServiceTicketVendor AS shv ON shv.ServiceTicketId = st.Id
INNER JOIN Vendor AS v ON v.Id = shv.VendorId AND shv.[Primary] = 1
Inner Join AccountBucket ab on ab.Id = st.AccountBucketId
Left Join Prefix p on p.Id = st.PrefixId
--Left Join ServiceTicketChargeStatistics scs ON scs.ServiceTicketId = st.Id
Left Join RefServiceTicketStatus sts ON sts.ServiceTicketStatusIndex = st.[Status]
WHERE
@TenantId <> 0
AND @UserId <> 0
AND st.TenantId = @TenantId
AND (
(Select COUNT(*) from [User] where id = @UserId and TenantEmployee = 1) > 0
OR
(st.CustomerId in (SELECT CustomerId FROM UserShipAs WHERE TenantId = @TenantId and UserId = @UserId
UNION
SELECT [User].CustomerId FROM [User] WHERE Id = @UserId)))
AND (@CustomerGroupId = 0 OR (Select COUNT(*) from CustomerGroupMap WHERE CustomerId = st.CustomerId and CustomerGroupId = @CustomerGroupId) > 0)
AND (@VendorGroupId = 0 OR (Select COUNT(*) from VendorGroupMap WHERE VendorId = shv.VendorId and VendorGroupId = @VendorGroupId) > 0)
and st.ExternalReference1 like '%62887%'

select * from ServiceTicket where ExternalReference1 like '%62887%'
