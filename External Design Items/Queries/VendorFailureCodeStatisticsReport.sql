Declare @TenantId bigint = 4
Declare @UserId bigint = 2
Declare @CustomerNumber nvarchar(50) =''
Declare @VendorNumber nvarchar(50) = ''
Declare @Mode nvarchar(50) = ''
Declare @StartDate datetime ='2010-11-23'
Declare @EndDate datetime ='2017-03-05'

exec VendorFailureCodeStatistics @TenantId, @StartDate, @EndDate, @UserId, @CustomerNumber, @VendorNumber, @Mode