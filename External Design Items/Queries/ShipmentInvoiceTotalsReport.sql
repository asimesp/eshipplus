use PacmanVanilla;

SELECT
 i.InvoiceNumber
 ,s.ShipmentNumber
 ,s.PurchaseOrderNumber
 ,s.ShipperReference
 ,s.ServiceMode
 ,id.InvoiceShipmentAmount
 ,c.Name CustomerName
 ,c.CustomerNumber
FROM 
 Invoice i
 INNER JOIN 
 (
	Select
		InvoiceId
		,ReferenceNumber
		,sum(quantity * (UnitSell - UnitDiscount)) 'InvoiceShipmentAmount'
	From
		InvoiceDetail 
	Where
		ReferenceType = 0 -- shipment only
	Group By	
		InvoiceId, ReferenceNumber
 ) as id ON id.InvoiceId = i.Id
 INNER JOIN Shipment s ON s.ShipmentNumber = id.ReferenceNumber
 INNER JOIN CustomerLocation cl ON cl.Id = i.CustomerLocationId
 INNER JOIN Customer c on c.Id = cl.CustomerId
WHERE
 i.InvoiceNumber LIKE '520041V'
