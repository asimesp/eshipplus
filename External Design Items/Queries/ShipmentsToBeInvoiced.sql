DECLARE @TenantId bigint = 4;

SELECT 
	COUNT(*)
FROM
	Shipment
WHERE
	[Status] NOT IN (3,4)
		AND AuditedForInvoicing = 1
		AND TenantId = @TenantId
		
SELECT
	sh.ShipmentNumber,
	sh.DateCreated,
	sh.ActualPickupDate,
	sh.EstimatedDeliveryDate,
	sh.ActualDeliveryDate,
	sh.[Status],
	sh.ServiceMode,
	shol.[Description],
	shol.City,
	shol.[State],
	shol.PostalCode,
	co.Name,
	shdl.[Description],
	shdl.City,
	shdl.[State],
	shdl.PostalCode,
	cd.Name,
	c.CustomerNumber,
	c.Name,
	v.VendorNumber,
	v.Name,
	scs.TotalAmountDue,
	scs.TotalCost
FROM 
	Shipment sh
		INNER JOIN ShipmentLocation shol ON shol.Id = sh.OriginId
		INNER JOIN Country co ON co.Id = shol.CountryId
		INNER JOIN ShipmentLocation shdl ON shdl.Id = sh.DestinationId
		INNER JOIN Country cd ON cd.Id = shdl.CountryId
		INNER JOIN ShipmentChargeStatistics scs ON scs.ShipmentId = sh.Id
		INNER JOIN Customer c ON c.Id = sh.CustomerId
		INNER JOIN ShipmentVendor shv ON shv.ShipmentId = sh.Id AND shv.[Primary] = 1
		INNER JOIN Vendor v ON v.Id = shv.VendorId
WHERE
	sh.[Status] NOT IN (3,4)
		AND sh.AuditedForInvoicing = 1
		AND sh.TenantId = @TenantId
		
