DECLARE @TenantId bigint = 4;
DECLARE @UserId bigint = 3;

SELECT COUNT(*) FROM ServiceTicket WHERE TenantId = @TenantId

SELECT 
	st.ServiceTicketNumber,
	(CASE
		WHEN st.AuditedForInvoicing = 0 THEN 'No'
		ELSE 'Yes'
	END),
	st.DateCreated,
	st.TicketDate,
	c.CustomerNumber,
	c.Name,
	u.Username,
	u.FirstName,
	u.LastName,
	ISNULL(sts.ServiceTicketStatusText, '') AS 'Status',
	ab.Code,
	ab.[Description],
	ISNULL(p.Code,''),
	ISNULL(p.[Description],''),
	ISNULL(au.Name,''),
	ISNULL(sr.Name,''),
	ISNULL(sr.SalesRepresentativeNumber,''),
	st.SalesRepresentativeCommissionPercent,
	st.SalesRepMinCommValue,
	st.SalesRepMaxCommValue,
	st.SalesRepAddlEntityName,
	st.SalesRepAddlEntityCommPercent,
	ISNULL(scs.TotalCost,0),
	ISNULL(scs.TotalAmountDue,0),
	ISNULL(scs.TotalCharges,0),
	ISNULL(scs.TotalDiscount,0),
	ISNULL(scs.TotalNettedCharge,0),
	ISNULL(scs.TotalProfit,0),
	ISNULL(scs.FreightCharges,0),
	ISNULL(scs.FuelCharges,0),
	ISNULL(scs.AccessorialCharges,0),
	ISNULL(scs.ServiceCharges,0),
	st.ExternalReference1,
	st.ExternalReference2
FROM 
	ServiceTicket st
		INNER JOIN [User] u ON u.Id = st.UserId
		INNER JOIN Customer c ON c.Id = st.CustomerId
		INNER JOIN AccountBucket ab ON ab.Id = st.AccountBucketId
		LEFT JOIN ServiceTicketChargeStatistics scs ON scs.ServiceTicketId = st.Id 
		LEFT JOIN AccountBucketUnit au ON au.Id = st.AccountBucketUnitId
		LEFT JOIN Prefix p ON p.Id = st.PrefixId
		LEFT JOIN SalesRepresentative sr ON sr.Id = st.SalesRepresentativeId
		LEFT JOIN RefServiceTicketStatus sts ON sts.ServiceTicketStatusIndex = st.[Status]
WHERE
	@TenantId <> 0
		AND @UserId <> 0
		AND c.TenantId = @TenantId
		AND ((Select COUNT(*) FROM [User] where Id = @UserId and TenantEmployee = 1) > 0
				OR
				(st.CustomerId IN 
					(SELECT CustomerId FROM UserShipAs WHERE TenantId = @TenantId AND UserId = @UserId
					 UNION
					 SELECT [User].CustomerId FROM [User] WHERE Id = @UserId)))

