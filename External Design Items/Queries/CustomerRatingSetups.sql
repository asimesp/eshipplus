DECLARE @TenantId bigint = 4;

SELECT
	 c.CustomerNumber,
	 c.Name,
	 t.Name,
	 
	 ISNULL(p.Code,'') 'Prefix Code',
	 ISNULL(p.[Description],'') 'Prefix Description',
	 
	 ISNULL(cr.InsuranceEnabled,'') 'InsuranceEnabled',
	 ISNULL(cr.InsurancePurchaseFloor,0) 'InsurancePurchaseFloor',
	 ISNULL(cc.Code,'') 'ChargeCode',
	 ISNULL(cc.[Description],'') 'ChargeCodeDescription',
	 ISNULL(cr.BillReseller,0) 'BillReseller',
	 ISNULL(cr.NoLineHaulProfit,0) 'NoLineHaulProfit',
	 ISNULL(cr.NoFuelProfit,0) 'NoFuelProfit',
	 ISNULL(cr.NoAccessorialProfit,0) 'NoAccessorialProfit',
	 ISNULL(cr.NoServiceProfit,0) 'NoServiceProfit',
	
	 -- Reseller Addition Items 
	 -- Reseller line Haul
	 ISNULL(rav.Name,'') 'ResellerAdditionName',
	 ISNULL(rav.LineHaulPercentage, 0) 'ResellerAdditionalLineHaulPercentage',
	 ISNULL(rav.LineHaulValue, 0) 'ResellerAdditionalLineHaulValue',
	 ISNULL(rav.LineHaulType, '') 'ResellerAdditionalLineHaulType',
	 ISNULL(rav.UseLineHaulMinimum, '') 'UseResellerAdditionalLineHaulMinimum',
	 -- Reseller Fuel
	 ISNULL(rav.FuelPercentage, 0) 'ResellerAdditionalFuelPercentage',
	 ISNULL(rav.FuelValue, 0) 'ResellerAdditionalFuelValue',
	 ISNULL(rav.FuelType, '') 'ResellerAdditionalFuelType',
	 ISNULL(rav.UseFuelMinimum, '') 'UseResellerAdditionalFuelMinimum',
	 -- Reseller Accessorial
	 ISNULL(rav.AccessorialPercentage, 0) 'ResellerAdditionalAccessorialPercentage',
	 ISNULL(rav.AccessorialValue, 0) 'ResellerAdditionalAccessorialValue',
	 ISNULL(rav.AccessorialType, '') 'ResellerAdditionalAccessorialType',
	 ISNULL(rav.UseAccessorialMinimum, '') 'UseResellerAdditionalAccessorialMinimum',
	 -- Reseller Service
	 ISNULL(rav.ServicePercentage, 0) 'ResellerAdditionalServicePercentage',
	 ISNULL(rav.ServiceValue, 0) 'ResellerAdditionalServiceValue',
	 ISNULL(rav.ServiceType, '') 'ResellerAdditionalServiceType',
	 ISNULL(rav.UseServiceMinimum, '') 'UseResellerAdditionalServiceMinimum',
	 -- Reseller Customer Account
	 ISNULL(rav.ResellerCustomerAccount, '') 'ResellerCustomerAccount',
	 ISNULL(rav.ResellerCustomerAccountNumber, '') 'ResellerCustomerAccountNumber',
	
	 --Customer Rating LineHaul Ceiling
	 ISNUll(cr.LineHaulProfitCeilingValue,0) 'LineHaulProfitCeilingValue',			
	 ISNULL(cr.LineHaulProfitCeilingPercentage,0) 'LineHaulProfitCeilingPercentage',		
	 ISNULL(cr.UseLineHaulMinimumCeiling,0) 'UseLineHaulProfitCeiling',
	 --Customer Rating Fuel Ceiling
	 ISNUll(cr.FuelProfitCeilingValue,0) 'FuelProfitCeilingValue',				
	 ISNULL(cr.FuelProfitCeilingPercentage,0) 'FuelProfitCeilingPercentage',
	 ISNULL(cr.UseFuelMinimumCeiling,0) 'UseFuelProfitCeiling',
	  --Customer Rating Accessorial Ceiling
	 ISNUll(cr.AccessorialProfitCeilingValue,0) 'AccessorialProfitCeilingValue',		
	 ISNULL(cr.AccessorialProfitCeilingPercentage,0) 'AccessorialProfitCeilingPercentage',
	 ISNULL(cr.UseAccessorialMinimumCeiling,0) 'UseAccessorialProfitCeiling',
	  --Customer Rating Service Ceiling
	 ISNUll(cr.ServiceProfitCeilingValue,0) 'ServiceProfitCeilingValue',			
	 ISNULL(cr.ServiceProfitCeilingPercentage,0) 'ServiceProfitCeilingPercentage',
	 ISNULL(cr.UseServiceMinimumCeiling,0) 'UseServiceProfitCeiling',
	
	 --Customer Rating LineHaul Floor
	 ISNUll(cr.LineHaulProfitFloorValue,0) 'LineHaulProfitFloorValue',			
	 ISNULL(cr.LineHaulProfitFloorPercentage,0) 'LineHaulProfitFloorPercentage',		
	 ISNULL(cr.UseLineHaulMinimumFloor,0) 'UseLineHaulProfitFloor',
	 --Customer Rating Fuel Floor
	 ISNUll(cr.FuelProfitFloorValue,0) 'FuelProfitFloorValue',				
	 ISNULL(cr.FuelProfitFloorPercentage,0) 'FuelProfitFloorPercentage',
	 ISNULL(cr.UseFuelMinimumFloor,0) 'UseFuelProfitFloor',
	  --Customer Rating Accessorial Floor
	 ISNUll(cr.AccessorialProfitFloorValue,0) 'AccessorialProfitFloorValue',		
	 ISNULL(cr.AccessorialProfitFloorPercentage,0) 'AccessorialProfitFloorPercentage',
	 ISNULL(cr.UseAccessorialMinimumFloor,0) 'UseAccessorialProfitFloor',
	  --Customer Rating Service Profits
	 ISNUll(cr.ServiceProfitFloorValue,0) 'ServiceProfitFloorValue',			
	 ISNULL(cr.ServiceProfitFloorPercentage,0) 'ServiceProfitFloorPercentage',
	 ISNULL(cr.UseServiceMinimumFloor,0) 'UseServiceProfitFloor',
	 
	 --TL Sell Rate Information
	 ISNULL(cr.TLFuelRate, 0) 'TLFuelRate' ,
	 ISNULL(rt.RateTypeText, '') AS 'TLFuelRateType',
	 ISNULL(crfuel.Code, 0) 'TLFuelChargeCode' ,
	 ISNULL(crfuel.[Description], 0) 'TLFuelChargeCodeDescription' ,
	 ISNULL(crfreight.Code, 0) 'TLFreightChargeCode' ,
	 ISNULL(crfreight.[Description], 0) 'TLFreightChargeCodeDescription' ,

	 --LTL Sell Rate Summary Info
	 ISNULL(lsrs.[AvgMarkupPercent],0) 'LTLAverageMarkupPercent',
	 ISNULL(lsrs.[MaxMarkupPercent], 0) 'LTLMaxMarkupPercent',
	 ISNULL(lsrs.AvgMarkupValue,0) 'LTLAverageMarkupValue',
	 ISNULL(lsrs.MinMarkupValue,0) 'LTLMinMarkupValue',
	 ISNULL(lsrs.[Count],0) 'CountOfLTLSellRates',
	 
	 --Customer Markup Summary Info
	 ISNULL(csms.AvgMarkupPercent, 0) 'CustomerServiceAvgMarkupPercent',
	 ISNULL(csms.MaxMarkupPercent, 0) 'CustomerServiceMaxMarkupPercent',
	 ISNULL(csms.AvgMarkupValue, 0) 'CustomerServiceAvgMarkupValue',
	 ISNULL(csms.MinMarkupValue, 0) 'CustomerServiceMinMarkupValue',
	 ISNULL(csms.[Count], 0) 'CountOfCustomerServiceMarkups',
	 ISNULL(csms.MinServiceChargeFloor, 0) 'MinCustomerServiceChargeFloor',
	 ISNULL(csms.MaxServiceChargeFloor, 0)'MaxCustomerServiceChargeFloor',
	 ISNULL(csms.AvgServiceChargeFloor, 0) 'AvgCustomerServiceChargeFloor',
	 ISNULL(csms.MinServiceChargeCeiling, 0) 'MinCustomerServiceChargeCeiling',
	 ISNULL(csms.MaxServiceChargeCeiling, 0) 'MaxCustomerServiceChargeCeiling',
	 ISNULL(csms.AvgServiceChargeCeiling, 0) 'AvgCustomerServiceChargeCeiling',
    
    ISNULL(lhpct.ValuePercentageTypeText, '') AS 'LineHaulCeilingType',
    ISNULL(fpct.ValuePercentageTypeText, '') AS 'FuelProfitCeilingType',
    ISNULL(apct.ValuePercentageTypeText, '') AS 'AccessorialProfitCeilingType',
    ISNULL(spct.ValuePercentageTypeText, '') AS 'ServiceProfitCeilingType',
    ISNULL(lhft.ValuePercentageTypeText, '') AS 'LineHaulFloorType',
    ISNULL(fpft.ValuePercentageTypeText, '') AS 'FuelProfitFloorType',
    ISNULL(apft.ValuePercentageTypeText, '') AS 'AccessorialProfitFloorType',
    ISNULL(spft.ValuePercentageTypeText, '') AS 'ServiceProfitFloorType'
    
FROM 
	Customer c 
		INNER JOIN Tier t ON t.Id = c.TierId
		LEFT JOIN CustomerRating cr ON c.Id = cr.CustomerId
		LEFT JOIN ChargeCode crfuel ON crfuel.Id = cr.TLFuelChargeCodeId
		LEFT JOIN ChargeCode crfreight ON crfreight.Id = cr.TLFreightChargeCodeId
		LEFT JOIN ResellerAdditionView rav ON cr.ResellerAdditionId = rav.id
		LEfT JOIN ChargeCode cc ON cr.InsuranceChargeCodeId= cc.Id
		LEFT JOIN LTLSellRateSummaries lsrs ON cr.Id = lsrs.CustomerRatingId
		LEFT JOIN CustomerServiceMarkupSummaries csms ON cr.Id = csms.CustomerRatingId
		LEFT JOIN Prefix p ON p.Id = c.PrefixId
		LEFT JOIN RefRateType rt ON rt.RateTypeIndex = cr.TLFuelRateType
		LEFT JOIN RefValuePercentageType lhpct ON lhpct.ValuePercentageTypeIndex = cr.LineHaulProfitCeilingType
		LEFT JOIN RefValuePercentageType fpct ON fpct.ValuePercentageTypeIndex = cr.FuelProfitCeilingType
		LEFT JOIN RefValuePercentageType apct ON apct.ValuePercentageTypeIndex = cr.AccessorialProfitCeilingType
		LEFT JOIN RefValuePercentageType spct ON spct.ValuePercentageTypeIndex = cr.ServiceProfitCeilingType
		LEFT JOIN RefValuePercentageType lhft ON lhft.ValuePercentageTypeIndex = cr.LineHaulProfitFloorType
		LEFT JOIN RefValuePercentageType fpft ON fpft.ValuePercentageTypeIndex = cr.FuelProfitFloorType
		LEFT JOIN RefValuePercentageType apft ON apft.ValuePercentageTypeIndex = cr.AccessorialProfitFloorType
		LEFT JOIN RefValuePercentageType spft ON spft.ValuePercentageTypeIndex = cr.ServiceProfitFloorType
WHERE
	@TenantId = c.TenantId
