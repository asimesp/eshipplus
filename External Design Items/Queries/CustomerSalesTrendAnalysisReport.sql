	DECLARE @TenantId bigint = 4
	DECLARE @ActiveUserId bigint = 4
	DECLARE @CustomerGroupId bigint = 0
	DECLARE @Mode nvarchar(50) = 'ltl'
	
	declare @lastBusDay datetime
	declare @MonthStart datetime
	declare @MonthEnd datetime
	declare @YrStart datetime
	declare @BusDaysToDate int
	declare @CurMthDays int
	declare @Prv1MthDays int
	declare @Prv2MthDays int
	declare @Prv3MthDays int
	declare @Prv4MthDays int
	declare @Prv5MthDays int
	declare @Prv6MthDays int
	declare @YrToDateDays int
	
	declare @sm int = -1;

	-- normalizations
		
	SET @Mode = LOWER(REPLACE(@Mode, ' ', ''))
	
	IF @Mode = 'lessthantruckload' OR @Mode = 'ltl' 
	BEGIN
		SET @sm = 0
	END
	ELSE IF @Mode = 'truckload' or @Mode = 'ftl'
	BEGIN
		SET @sm = 1
	END
	ELSE IF @Mode = 'air'
	BEGIN
		SET @sm = 2
	END
	ELSE IF @Mode = 'rail'
	BEGIN
		SET @sm = 3
	END
	ELSE IF @Mode = 'smallpackage' or @Mode = 'sp'
	BEGIN
		SET @sm = 4
	END
	ELSE IF @Mode = 'all' or @Mode = ''
	BEGIN
		SET @sm = -1
	END
	ELSE
	BEGIN
		SET @sm = -2
	END

	-- last business day
	Set @lastBusDay = DATEADD(DAY, -1, GETDATE())
	Set @lastBusDay = 
		Case 
			When DATEPART(WEEKDAY, @lastBusDay) = 1 Then DATEADD(day,-2,@lastBusDay)
			When DATEPART(WEEKDAY, @lastBusDay) = 7 Then DATEADD(day,-1,@lastBusDay)
			Else @lastBusDay
		end
	-- set earlier time
	Set @lastBusDay = CONVERT(datetime, 
		Convert(nvarchar(4), Year(@lastBusDay)) + '-' + 
		Convert(nvarchar(2), Month(@lastBusDay)) + '-' +
		Convert(nvarchar(2), Day(@lastBusDay)) + ' 00:00:00'
	)

	-- month start and end
	set @MonthStart = CONVERT(datetime, 
		Convert(nvarchar(4), Year(GETDATE())) + '-' + 
		Convert(nvarchar(2), Month(GETDATE())) + '-01 00:00:00'
	)
	set @MonthEnd = DATEADD(DAY,-1,DATEADD(MONTH,1,@MonthStart))

	-- year start
	set @YrStart = CONVERT(datetime, 
		Convert(nvarchar(4), Year(GETDATE())) + '-01-01 00:00:00'
	)

	-- get days total's
	declare @date datetime = GetDate()
	Set @CurMthDays = ISNULL(
		(Select 
			Case 
				When MONTH(@date) = 1 Then January
				When MONTH(@date) = 2 Then February
				When MONTH(@date) = 3 Then March
				When MONTH(@date) = 4 Then April
				When MONTH(@date) = 5 Then May
				When MONTH(@date) = 6 Then June
				When MONTH(@date) = 7 Then July
				When MONTH(@date) = 8 Then August
				When MONTH(@date) = 9 Then September
				When MONTH(@date) = 10 Then October
				When MONTH(@date) = 11 Then November
				When MONTH(@date) = 12 Then December
			End
		from YrMthBusDay where [Year] = CONVERT(nvarchar(4),Year(@date))), 0)
		
	Set @date = DATEADD(MONTH, -1, @date)
	Set @Prv1MthDays = ISNULL(
		(Select 
			Case 
				When MONTH(@date) = 1 Then January
				When MONTH(@date) = 2 Then February
				When MONTH(@date) = 3 Then March
				When MONTH(@date) = 4 Then April
				When MONTH(@date) = 5 Then May
				When MONTH(@date) = 6 Then June
				When MONTH(@date) = 7 Then July
				When MONTH(@date) = 8 Then August
				When MONTH(@date) = 9 Then September
				When MONTH(@date) = 10 Then October
				When MONTH(@date) = 11 Then November
				When MONTH(@date) = 12 Then December
			End
		from YrMthBusDay where [Year] = CONVERT(nvarchar(4),Year(@date))), 0)

	Set @date = DATEADD(MONTH, -1, @date)
	Set @Prv2MthDays = ISNULL(
		(Select 
			Case 
				When MONTH(@date) = 1 Then January
				When MONTH(@date) = 2 Then February
				When MONTH(@date) = 3 Then March
				When MONTH(@date) = 4 Then April
				When MONTH(@date) = 5 Then May
				When MONTH(@date) = 6 Then June
				When MONTH(@date) = 7 Then July
				When MONTH(@date) = 8 Then August
				When MONTH(@date) = 9 Then September
				When MONTH(@date) = 10 Then October
				When MONTH(@date) = 11 Then November
				When MONTH(@date) = 12 Then December
			End
		from YrMthBusDay where [Year] = CONVERT(nvarchar(4),Year(@date))), 0)

	Set @date = DATEADD(MONTH, -1, @date)
	Set @Prv3MthDays = ISNULL(
		(Select 
			Case 
				When MONTH(@date) = 1 Then January
				When MONTH(@date) = 2 Then February
				When MONTH(@date) = 3 Then March
				When MONTH(@date) = 4 Then April
				When MONTH(@date) = 5 Then May
				When MONTH(@date) = 6 Then June
				When MONTH(@date) = 7 Then July
				When MONTH(@date) = 8 Then August
				When MONTH(@date) = 9 Then September
				When MONTH(@date) = 10 Then October
				When MONTH(@date) = 11 Then November
				When MONTH(@date) = 12 Then December
			End
		from YrMthBusDay where [Year] = CONVERT(nvarchar(4),Year(@date))), 0)

	Set @date = DATEADD(MONTH, -1, @date)
	Set @Prv4MthDays = ISNULL(
		(Select 
			Case 
				When MONTH(@date) = 1 Then January
				When MONTH(@date) = 2 Then February
				When MONTH(@date) = 3 Then March
				When MONTH(@date) = 4 Then April
				When MONTH(@date) = 5 Then May
				When MONTH(@date) = 6 Then June
				When MONTH(@date) = 7 Then July
				When MONTH(@date) = 8 Then August
				When MONTH(@date) = 9 Then September
				When MONTH(@date) = 10 Then October
				When MONTH(@date) = 11 Then November
				When MONTH(@date) = 12 Then December
			End
		from YrMthBusDay where [Year] = CONVERT(nvarchar(4),Year(@date))), 0)

	Set @date = DATEADD(MONTH, -1, @date)
	Set @Prv5MthDays = ISNULL(
		(Select 
			Case 
				When MONTH(@date) = 1 Then January
				When MONTH(@date) = 2 Then February
				When MONTH(@date) = 3 Then March
				When MONTH(@date) = 4 Then April
				When MONTH(@date) = 5 Then May
				When MONTH(@date) = 6 Then June
				When MONTH(@date) = 7 Then July
				When MONTH(@date) = 8 Then August
				When MONTH(@date) = 9 Then September
				When MONTH(@date) = 10 Then October
				When MONTH(@date) = 11 Then November
				When MONTH(@date) = 12 Then December
			End
		from YrMthBusDay where [Year] = CONVERT(nvarchar(4),Year(@date))), 0)

	Set @date = DATEADD(MONTH, -1, @date)
	Set @Prv6MthDays = ISNULL(
		(Select 
			Case 
				When MONTH(@date) = 1 Then January
				When MONTH(@date) = 2 Then February
				When MONTH(@date) = 3 Then March
				When MONTH(@date) = 4 Then April
				When MONTH(@date) = 5 Then May
				When MONTH(@date) = 6 Then June
				When MONTH(@date) = 7 Then July
				When MONTH(@date) = 8 Then August
				When MONTH(@date) = 9 Then September
				When MONTH(@date) = 10 Then October
				When MONTH(@date) = 11 Then November
				When MONTH(@date) = 12 Then December
			End
		from YrMthBusDay where [Year] = CONVERT(nvarchar(4),Year(@date))), 0)

	-- business days to date
	declare @cnt int = 0
	Set @BusDaysToDate = 0
	While DATEADD(day, @cnt, @MonthStart) <= GETDATE()
	Begin
		Set @BusDaysToDate += 
			Case 
				When DATEPART(WEEKDAY, DATEADD(day, @cnt, @MonthStart)) <> 1 And DATEPART(WEEKDAY, DATEADD(day, @cnt, @MonthStart)) <> 7 Then 1
				Else 0
			end
		Set @cnt += 1
	End

	-- Year to date days (must occur after @BusDaysToDate has been calculated)
	Set @YrToDateDays = 
		ISNULL(Case 
			When MONTH(GETDATE()) = 1 Then (Select @BusDaysToDate from YrMthBusDay where [Year] = CONVERT(nvarchar(4),YEAR(GETDATE())))
			When MONTH(GETDATE()) = 2 Then (Select January+@BusDaysToDate from YrMthBusDay where [Year] = CONVERT(nvarchar(4),YEAR(GETDATE())))
			When MONTH(GETDATE()) = 3 Then (Select January+February+@BusDaysToDate from YrMthBusDay where [Year] = CONVERT(nvarchar(4),YEAR(GETDATE())))
			When MONTH(GETDATE()) = 4 Then (Select January+February+March+@BusDaysToDate from YrMthBusDay where [Year] = CONVERT(nvarchar(4),YEAR(GETDATE())))
			When MONTH(GETDATE()) = 5 Then (Select January+February+March+April+@BusDaysToDate from YrMthBusDay where [Year] = CONVERT(nvarchar(4),YEAR(GETDATE())))
			When MONTH(GETDATE()) = 6 Then (Select January+February+March+April+May+@BusDaysToDate from YrMthBusDay where [Year] = CONVERT(nvarchar(4),YEAR(GETDATE())))
			When MONTH(GETDATE()) = 7 Then (Select January+February+March+April+May+June+@BusDaysToDate from YrMthBusDay where [Year] = CONVERT(nvarchar(4),YEAR(GETDATE())))
			When MONTH(GETDATE()) = 8 Then (Select January+February+March+April+May+June+July+@BusDaysToDate from YrMthBusDay where [Year] = CONVERT(nvarchar(4),YEAR(GETDATE())))
			When MONTH(GETDATE()) = 9 Then (Select January+February+March+April+May+June+July+August+@BusDaysToDate from YrMthBusDay where [Year] = CONVERT(nvarchar(4),YEAR(GETDATE())))
			When MONTH(GETDATE()) = 10 Then (Select January+February+March+April+May+June+July+August+September+@BusDaysToDate from YrMthBusDay where [Year] = CONVERT(nvarchar(4),YEAR(GETDATE())))
			When MONTH(GETDATE()) = 11 Then (Select January+February+March+April+May+June+July+August+September+October+@BusDaysToDate from YrMthBusDay where [Year] = CONVERT(nvarchar(4),YEAR(GETDATE())))
			When MONTH(GETDATE()) = 12 Then (Select January+February+March+April+May+June+July+August+September+October+November+@BusDaysToDate from YrMthBusDay where [Year] = CONVERT(nvarchar(4),YEAR(GETDATE())))
		End, 0)

	-- Verify temp table doesn't exist, if it does, drop it.
	IF EXISTS (SELECT * FROM tempdb.sys.objects WHERE name LIKE '#TrendAnalysis%')  
		DROP TABLE #TrendAnalysis

		-- Create Temp Table	
		CREATE TABLE #TrendAnalysis
		(
			CustomerId bigint,
			CustomerNumber nvarchar(50),
			CustomerName nvarchar(50),
			SalesRepresentativeName nvarchar(50),
			SalesRepresentativeNumber nvarchar(50),
			
			LastBusDay int,
			LastBusDayFreightCost decimal(38, 4),
			LastBusDayFreightRevenue decimal(38, 4),
			LastBusDayFuelCost decimal(38, 4),
			LastBusDayFuelRevenue decimal(38, 4),
			LastBusDayAccessorialCost decimal(38, 4),
			LastBusDayAccessorialRevenue decimal(38, 4),
			LastBusDayServiceCost decimal(38, 4),
			LastBusDayServiceRevenue decimal(38, 4),
			LastBusDayTotalCost decimal(38, 4),
			LastBusDayTotalRevenue decimal(38, 4),
			
			MthToDate int,
			MthDailyAvg float,
			EndOfMthProjection float,
			MthToDateFreightCost decimal(38, 4),
			MthToDateFreightRevenue decimal(38, 4),
			MthToDateFuelCost decimal(38, 4),
			MthToDateFuelRevenue decimal(38, 4),
			MthToDateAccessorialCost decimal(38, 4),
			MthToDateAccessorialRevenue decimal(38, 4),
			MthToDateServiceCost decimal(38, 4),
			MthToDateServiceRevenue decimal(38, 4),
			MthToDateTotalCost decimal(38, 4),
			MthToDateTotalRevenue decimal(38, 4),
			
			
			YrToDate int,		
			YrToDateDailyAvg float,
			YrToDateFreightCost decimal(38, 4),
			YrToDateFreightRevenue decimal(38, 4),
			YrToDateFuelCost decimal(38, 4),
			YrToDateFuelRevenue decimal(38, 4),
			YrToDateAccessorialCost decimal(38, 4),
			YrToDateAccessorialRevenue decimal(38, 4),
			YrToDateServiceCost decimal(38, 4),
			YrToDateServiceRevenue decimal(38, 4),
			YrToDateTotalCost decimal(38, 4),
			YrToDateTotalRevenue decimal(38, 4),
			
			PrvMth1Total int,
			PrvMth1DailyAvg float,
			PrvMth1FreightCost decimal(38, 4),
			PrvMth1FreightRevenue decimal(38, 4),
			PrvMth1FuelCost decimal(38, 4),
			PrvMth1FuelRevenue decimal(38, 4),
			PrvMth1AccessorialCost decimal(38, 4),
			PrvMth1AccessorialRevenue decimal(38, 4),
			PrvMth1ServiceCost decimal(38, 4),
			PrvMth1ServiceRevenue decimal(38, 4),
			PrvMth1TotalCost decimal(38, 4),
			PrvMth1TotalRevenue decimal(38, 4),
			
			PrvMth2Total int,
			PrvMth2DailyAvg float,
			PrvMth2FreightCost decimal(38, 4),
			PrvMth2FreightRevenue decimal(38, 4),
			PrvMth2FuelCost decimal(38, 4),
			PrvMth2FuelRevenue decimal(38, 4),
			PrvMth2AccessorialCost decimal(38, 4),
			PrvMth2AccessorialRevenue decimal(38, 4),
			PrvMth2ServiceCost decimal(38, 4),
			PrvMth2ServiceRevenue decimal(38, 4),
			PrvMth2TotalCost decimal(38, 4),
			PrvMth2TotalRevenue decimal(38, 4),
			
			PrvMth3Total int,
			PrvMth3DailyAvg float,
			PrvMth3FreightCost decimal(38, 4),
			PrvMth3FreightRevenue decimal(38, 4),
			PrvMth3FuelCost decimal(38, 4),
			PrvMth3FuelRevenue decimal(38, 4),
			PrvMth3AccessorialCost decimal(38, 4),
			PrvMth3AccessorialRevenue decimal(38, 4),
			PrvMth3ServiceCost decimal(38, 4),
			PrvMth3ServiceRevenue decimal(38, 4),
			PrvMth3TotalCost decimal(38, 4),
			PrvMth3TotalRevenue decimal(38, 4),
			
			PrvMth4Total int,
			PrvMth4DailyAvg float,
			PrvMth4FreightCost decimal(38, 4),
			PrvMth4FreightRevenue decimal(38, 4),
			PrvMth4FuelCost decimal(38, 4),
			PrvMth4FuelRevenue decimal(38, 4),
			PrvMth4AccessorialCost decimal(38, 4),
			PrvMth4AccessorialRevenue decimal(38, 4),
			PrvMth4ServiceCost decimal(38, 4),
			PrvMth4ServiceRevenue decimal(38, 4),
			PrvMth4TotalCost decimal(38, 4),
			PrvMth4TotalRevenue decimal(38, 4),
			
			PrvMth5Total int,
			PrvMth5DailyAvg float,
			PrvMth5FreightCost decimal(38, 4),
			PrvMth5FreightRevenue decimal(38, 4),
			PrvMth5FuelCost decimal(38, 4),
			PrvMth5FuelRevenue decimal(38, 4),
			PrvMth5AccessorialCost decimal(38, 4),
			PrvMth5AccessorialRevenue decimal(38, 4),
			PrvMth5ServiceCost decimal(38, 4),
			PrvMth5ServiceRevenue decimal(38, 4),
			PrvMth5TotalCost decimal(38, 4),
			PrvMth5TotalRevenue decimal(38, 4),
			
			PrvMth6Total int,
			PrvMth6DailyAvg float,
			PrvMth6FreightCost decimal(38, 4),
			PrvMth6FreightRevenue decimal(38, 4),
			PrvMth6FuelCost decimal(38, 4),
			PrvMth6FuelRevenue decimal(38, 4),
			PrvMth6AccessorialCost decimal(38, 4),
			PrvMth6AccessorialRevenue decimal(38, 4),
			PrvMth6ServiceCost decimal(38, 4),
			PrvMth6ServiceRevenue decimal(38, 4),
			PrvMth6TotalCost decimal(38, 4),
			PrvMth6TotalRevenue decimal(38, 4)
		)
		
	If (Select COUNT(*) from YrMthBusDay Where [Year] = CONVERT(nvarchar(4), Year(Getdate()))) = 0
	Begin
		Goto ResultAndOut
	End

	insert into #TrendAnalysis 
		(CustomerId, CustomerName, CustomerNumber, SalesRepresentativeName, SalesRepresentativeNumber,
			LastBusDay, LastBusDayTotalCost, LastBusDayTotalRevenue, LastBusDayAccessorialCost, LastBusDayAccessorialRevenue, LastBusDayFreightCost, LastBusDayFreightRevenue, 
				LastBusDayFuelCost, LastBusDayFuelRevenue, LastBusDayServiceCost, LastBusDayServiceRevenue,
			MthToDate, MthToDateTotalCost, MthToDateTotalRevenue, MthToDateAccessorialCost, MthToDateAccessorialRevenue, MthToDateFreightCost, MthToDateFreightRevenue, 
				MthToDateFuelCost, MthToDateFuelRevenue, MthToDateServiceCost, MthToDateServiceRevenue,
			YrToDate, YrToDateTotalCost, YrToDateTotalRevenue, YrToDateAccessorialCost, YrToDateAccessorialRevenue, YrToDateFreightCost, YrToDateFreightRevenue, 
				YrToDateFuelCost, YrToDateFuelRevenue, YrToDateServiceCost, YrToDateServiceRevenue,
			PrvMth1Total, PrvMth1TotalCost, PrvMth1TotalRevenue, PrvMth1AccessorialCost, PrvMth1AccessorialRevenue, PrvMth1FreightCost, PrvMth1FreightRevenue, 
				PrvMth1FuelCost, PrvMth1FuelRevenue, PrvMth1ServiceCost, PrvMth1ServiceRevenue,
			PrvMth2Total, PrvMth2TotalCost, PrvMth2TotalRevenue, PrvMth2AccessorialCost, PrvMth2AccessorialRevenue, PrvMth2FreightCost, PrvMth2FreightRevenue, 
				PrvMth2FuelCost, PrvMth2FuelRevenue, PrvMth2ServiceCost, PrvMth2ServiceRevenue,
			PrvMth3Total, PrvMth3TotalCost, PrvMth3TotalRevenue, PrvMth3AccessorialCost, PrvMth3AccessorialRevenue, PrvMth3FreightCost, PrvMth3FreightRevenue, 
				PrvMth3FuelCost, PrvMth3FuelRevenue, PrvMth3ServiceCost, PrvMth3ServiceRevenue,
			PrvMth4Total, PrvMth4TotalCost, PrvMth4TotalRevenue, PrvMth4AccessorialCost, PrvMth4AccessorialRevenue, PrvMth4FreightCost, PrvMth4FreightRevenue, 
				PrvMth4FuelCost, PrvMth4FuelRevenue, PrvMth4ServiceCost, PrvMth4ServiceRevenue,
			PrvMth5Total, PrvMth5TotalCost, PrvMth5TotalRevenue, PrvMth5AccessorialCost, PrvMth5AccessorialRevenue, PrvMth5FreightCost, PrvMth5FreightRevenue, 
				PrvMth5FuelCost, PrvMth5FuelRevenue, PrvMth5ServiceCost, PrvMth5ServiceRevenue,
			PrvMth6Total, PrvMth6TotalCost, PrvMth6TotalRevenue, PrvMth6AccessorialCost, PrvMth6AccessorialRevenue, PrvMth6FreightCost, PrvMth6FreightRevenue, 
				PrvMth6FuelCost, PrvMth6FuelRevenue, PrvMth6ServiceCost, PrvMth6ServiceRevenue)
		select	
			c.Id, 
			c.Name, 
			c.CustomerNumber,
			ISNULL(sr.Name, ''),
			ISNULL(sr.SalesRepresentativeNumber, ''),
			
			(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @lastBusDay <= s.DateCreated and s.DateCreated <= DATEADD(DAY,1,@lastBusDay)),
			(Select ISNULL(SUM(scs.TotalCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @lastBusDay <= s.DateCreated and s.DateCreated <= DATEADD(DAY,1,@lastBusDay)),
			(Select ISNULL(SUM(scs.TotalAmountDue),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @lastBusDay <= s.DateCreated and s.DateCreated <= DATEADD(DAY,1,@lastBusDay)),
			(Select ISNULL(SUM(scs.AccessorialCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @lastBusDay <= s.DateCreated and s.DateCreated <= DATEADD(DAY,1,@lastBusDay)),
			(Select ISNULL(SUM(scs.AccessorialCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @lastBusDay <= s.DateCreated and s.DateCreated <= DATEADD(DAY,1,@lastBusDay)),
			(Select ISNULL(SUM(scs.FreightCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @lastBusDay <= s.DateCreated and s.DateCreated <= DATEADD(DAY,1,@lastBusDay)),
			(Select ISNULL(SUM(scs.FreightCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @lastBusDay <= s.DateCreated and s.DateCreated <= DATEADD(DAY,1,@lastBusDay)),
			(Select ISNULL(SUM(scs.FuelCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @lastBusDay <= s.DateCreated and s.DateCreated <= DATEADD(DAY,1,@lastBusDay)),
			(Select ISNULL(SUM(scs.FuelCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @lastBusDay <= s.DateCreated and s.DateCreated <= DATEADD(DAY,1,@lastBusDay)),
			(Select ISNULL(SUM(scs.ServiceCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @lastBusDay <= s.DateCreated and s.DateCreated <= DATEADD(DAY,1,@lastBusDay)),
			(Select ISNULL(SUM(scs.ServiceCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @lastBusDay <= s.DateCreated and s.DateCreated <= DATEADD(DAY,1,@lastBusDay)),
			
			(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @MonthStart <= s.DateCreated and s.DateCreated <= @MonthEnd),
			(Select ISNULL(SUM(scs.TotalCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @MonthStart <= s.DateCreated and s.DateCreated <= @MonthEnd),
			(Select ISNULL(SUM(scs.TotalAmountDue),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @MonthStart <= s.DateCreated and s.DateCreated <= @MonthEnd),
			(Select ISNULL(SUM(scs.AccessorialCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @MonthStart <= s.DateCreated and s.DateCreated <= @MonthEnd),
			(Select ISNULL(SUM(scs.AccessorialCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @MonthStart <= s.DateCreated and s.DateCreated <= @MonthEnd),
			(Select ISNULL(SUM(scs.FreightCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @MonthStart <= s.DateCreated and s.DateCreated <= @MonthEnd),
			(Select ISNULL(SUM(scs.FreightCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @MonthStart <= s.DateCreated and s.DateCreated <= @MonthEnd),
			(Select ISNULL(SUM(scs.FuelCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @MonthStart <= s.DateCreated and s.DateCreated <= @MonthEnd),
			(Select ISNULL(SUM(scs.FuelCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @MonthStart <= s.DateCreated and s.DateCreated <= @MonthEnd),
			(Select ISNULL(SUM(scs.ServiceCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @MonthStart <= s.DateCreated and s.DateCreated <= @MonthEnd),
			(Select ISNULL(SUM(scs.ServiceCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @MonthStart <= s.DateCreated and s.DateCreated <= @MonthEnd),
			
			(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @YrStart <= s.DateCreated and s.DateCreated <= GETDATE()),
			(Select ISNULL(SUM(scs.TotalCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @YrStart <= s.DateCreated and s.DateCreated <= GETDATE()),
			(Select ISNULL(SUM(scs.TotalAmountDue),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @YrStart <= s.DateCreated and s.DateCreated <= GETDATE()),
			(Select ISNULL(SUM(scs.AccessorialCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @YrStart <= s.DateCreated and s.DateCreated <= GETDATE()),
			(Select ISNULL(SUM(scs.AccessorialCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @YrStart <= s.DateCreated and s.DateCreated <= GETDATE()),
			(Select ISNULL(SUM(scs.FreightCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @YrStart <= s.DateCreated and s.DateCreated <= GETDATE()),
			(Select ISNULL(SUM(scs.FreightCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @YrStart <= s.DateCreated and s.DateCreated <= GETDATE()),
			(Select ISNULL(SUM(scs.FuelCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @YrStart <= s.DateCreated and s.DateCreated <= GETDATE()),
			(Select ISNULL(SUM(scs.FuelCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @YrStart <= s.DateCreated and s.DateCreated <= GETDATE()),
			(Select ISNULL(SUM(scs.ServiceCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @YrStart <= s.DateCreated and s.DateCreated <= GETDATE()),
			(Select ISNULL(SUM(scs.ServiceCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And @YrStart <= s.DateCreated and s.DateCreated <= GETDATE()),
			
			(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -1, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -1, @MonthEnd)),
			(Select ISNULL(SUM(scs.TotalCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -1, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -1, @MonthEnd)),
			(Select ISNULL(SUM(scs.TotalAmountDue),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -1, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -1, @MonthEnd)),
			(Select ISNULL(SUM(scs.AccessorialCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -1, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -1, @MonthEnd)),
			(Select ISNULL(SUM(scs.AccessorialCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -1, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -1, @MonthEnd)),
			(Select ISNULL(SUM(scs.FreightCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -1, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -1, @MonthEnd)),
			(Select ISNULL(SUM(scs.FreightCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -1, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -1, @MonthEnd)),
			(Select ISNULL(SUM(scs.FuelCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -1, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -1, @MonthEnd)),
			(Select ISNULL(SUM(scs.FuelCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -1, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -1, @MonthEnd)),
			(Select ISNULL(SUM(scs.ServiceCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -1, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -1, @MonthEnd)),
			(Select ISNULL(SUM(scs.ServiceCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -1, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -1, @MonthEnd)),
			
			(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -2, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -2, @MonthEnd)),
			(Select ISNULL(SUM(scs.TotalCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -2, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -2, @MonthEnd)),
			(Select ISNULL(SUM(scs.TotalAmountDue),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -2, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -2, @MonthEnd)),
			(Select ISNULL(SUM(scs.AccessorialCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -2, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -2, @MonthEnd)),
			(Select ISNULL(SUM(scs.AccessorialCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -2, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -2, @MonthEnd)),
			(Select ISNULL(SUM(scs.FreightCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -2, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -2, @MonthEnd)),
			(Select ISNULL(SUM(scs.FreightCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -2, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -2, @MonthEnd)),
			(Select ISNULL(SUM(scs.FuelCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -2, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -2, @MonthEnd)),
			(Select ISNULL(SUM(scs.FuelCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -2, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -2, @MonthEnd)),
			(Select ISNULL(SUM(scs.ServiceCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -2, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -2, @MonthEnd)),
			(Select ISNULL(SUM(scs.ServiceCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -2, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -2, @MonthEnd)),
			
			(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -3, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -3, @MonthEnd)),
			(Select ISNULL(SUM(scs.TotalCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -3, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -3, @MonthEnd)),
			(Select ISNULL(SUM(scs.TotalAmountDue),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -3, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -3, @MonthEnd)),
			(Select ISNULL(SUM(scs.AccessorialCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -3, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -3, @MonthEnd)),
			(Select ISNULL(SUM(scs.AccessorialCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -3, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -3, @MonthEnd)),
			(Select ISNULL(SUM(scs.FreightCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -3, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -3, @MonthEnd)),
			(Select ISNULL(SUM(scs.FreightCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -3, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -3, @MonthEnd)),
			(Select ISNULL(SUM(scs.FuelCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -3, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -3, @MonthEnd)),
			(Select ISNULL(SUM(scs.FuelCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -3, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -3, @MonthEnd)),
			(Select ISNULL(SUM(scs.ServiceCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -3, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -3, @MonthEnd)),
			(Select ISNULL(SUM(scs.ServiceCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -3, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -3, @MonthEnd)),
			
			(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -4, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -4, @MonthEnd)),
			(Select ISNULL(SUM(scs.TotalCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -4, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -4, @MonthEnd)),
			(Select ISNULL(SUM(scs.TotalAmountDue),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -4, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -4, @MonthEnd)),
			(Select ISNULL(SUM(scs.AccessorialCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -4, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -4, @MonthEnd)),
			(Select ISNULL(SUM(scs.AccessorialCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -4, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -4, @MonthEnd)),
			(Select ISNULL(SUM(scs.FreightCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -4, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -4, @MonthEnd)),
			(Select ISNULL(SUM(scs.FreightCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -4, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -4, @MonthEnd)),
			(Select ISNULL(SUM(scs.FuelCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -4, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -4, @MonthEnd)),
			(Select ISNULL(SUM(scs.FuelCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -4, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -4, @MonthEnd)),
			(Select ISNULL(SUM(scs.ServiceCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -4, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -4, @MonthEnd)),
			(Select ISNULL(SUM(scs.ServiceCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -4, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -4, @MonthEnd)),
			
			(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -5, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -5, @MonthEnd)),
			(Select ISNULL(SUM(scs.TotalCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -5, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -5, @MonthEnd)),
			(Select ISNULL(SUM(scs.TotalAmountDue),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -5, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -5, @MonthEnd)),
			(Select ISNULL(SUM(scs.AccessorialCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -5, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -5, @MonthEnd)),
			(Select ISNULL(SUM(scs.AccessorialCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -5, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -5, @MonthEnd)),
			(Select ISNULL(SUM(scs.FreightCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -5, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -5, @MonthEnd)),
			(Select ISNULL(SUM(scs.FreightCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -5, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -5, @MonthEnd)),
			(Select ISNULL(SUM(scs.FuelCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -5, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -5, @MonthEnd)),
			(Select ISNULL(SUM(scs.FuelCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -5, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -5, @MonthEnd)),
			(Select ISNULL(SUM(scs.ServiceCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -5, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -5, @MonthEnd)),
			(Select ISNULL(SUM(scs.ServiceCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -5, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -5, @MonthEnd)),
			
			(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -6, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -6, @MonthEnd)),
			(Select ISNULL(SUM(scs.TotalCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -6, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -6, @MonthEnd)),
			(Select ISNULL(SUM(scs.TotalAmountDue),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -6, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -6, @MonthEnd)),
			(Select ISNULL(SUM(scs.AccessorialCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -6, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -6, @MonthEnd)),
			(Select ISNULL(SUM(scs.AccessorialCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -6, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -6, @MonthEnd)),
			(Select ISNULL(SUM(scs.FreightCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -6, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -6, @MonthEnd)),
			(Select ISNULL(SUM(scs.FreightCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -6, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -6, @MonthEnd)),
			(Select ISNULL(SUM(scs.FuelCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -6, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -6, @MonthEnd)),
			(Select ISNULL(SUM(scs.FuelCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -6, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -6, @MonthEnd)),
			(Select ISNULL(SUM(scs.ServiceCost),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -6, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -6, @MonthEnd)),
			(Select ISNULL(SUM(scs.ServiceCharges),0) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And (@sm = -1 OR s.ServiceMode = @sm) And s.[Status] <> 4 And s.CreatedInError = 0 And DATEADD(MONTH, -6, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -6, @MonthEnd))
		from 
			Customer c
			Left Join SalesRepresentative sr on c.SalesRepresentativeId = sr.Id
		where c.TenantId = @TenantId
				AND (
					(SELECT COUNT(*) FROM [User] WHERE Id = @ActiveUserId and TenantEmployee = 1) > 0
					OR
					(c.Id IN 
						(SELECT CustomerId FROM UserShipAs WHERE TenantId = @TenantId AND UserId = @ActiveUserId
						 UNION
						 SELECT [User].CustomerId FROM [User] WHERE Id = @ActiveUserId)))
				AND (@CustomerGroupId = 0 OR (Select COUNT(*) from CustomerGroupMap WHERE CustomerId = c.Id and CustomerGroupId = @CustomerGroupId) > 0)

	-- set averages
	Update #TrendAnalysis Set 
		MthDailyAvg = (Case When @BusDaysToDate = 0 Then 0 Else CONVERT(float,MthToDate)/@BusDaysToDate End),
		EndOfMthProjection = @CurMthDays * (Case When @BusDaysToDate = 0 Then 0 Else CONVERT(float,MthToDate)/@BusDaysToDate End),
		YrToDateDailyAvg = (Case When @YrToDateDays = 0 Then 0 else CONVERT(float,YrToDate) / @YrToDateDays End),
		PrvMth1DailyAvg = (Case When @Prv1MthDays = 0 Then 0 else CONVERT(float,PrvMth1Total) / @Prv1MthDays End),
		PrvMth2DailyAvg = (Case When @Prv2MthDays = 0 Then 0 else CONVERT(float,PrvMth2Total) / @Prv2MthDays End),
		PrvMth3DailyAvg = (Case When @Prv3MthDays = 0 Then 0 else CONVERT(float,PrvMth3Total) / @Prv3MthDays End),
		PrvMth4DailyAvg = (Case When @Prv4MthDays = 0 Then 0 else CONVERT(float,PrvMth4Total) / @Prv4MthDays End),
		PrvMth5DailyAvg = (Case When @Prv5MthDays = 0 Then 0 else CONVERT(float,PrvMth5Total) / @Prv5MthDays End),
		PrvMth6DailyAvg = (Case When @Prv6MthDays = 0 Then 0 else CONVERT(float,PrvMth6Total) / @Prv6MthDays End)


	ResultAndOut:

	-- results
	select 
		CustomerNumber,
		CustomerName,
		SalesRepresentativeNumber,
		SalesRepresentativeName,
		
		LastBusDay,
		LastBusDayTotalCost,
		LastBusDayTotalRevenue,
		LastBusDayAccessorialCost,
		LastBusDayAccessorialRevenue,
		LastBusDayFreightCost,
		LastBusDayFreightRevenue,
		LastBusDayFuelCost,
		LastBusDayFuelRevenue,
		LastBusDayServiceCost,
		LastBusDayServiceRevenue,
		
		MthToDate,
		CAST(ROUND(MthDailyAvg,4) As Decimal(18,4)) 'MthDailyAvg',
		CAST(ROUND(EndOfMthProjection,4) As Decimal(18, 4)) 'MthEndProjection',
		MthToDateTotalCost,
		MthToDateTotalRevenue,
		MthToDateAccessorialCost,
		MthToDateAccessorialRevenue,
		MthToDateFreightCost,
		MthToDateFreightRevenue,
		MthToDateFuelCost,
		MthToDateFuelRevenue,
		MthToDateServiceCost,
		MthToDateServiceRevenue,
		
		YrToDate,
		CAST(ROUND(YrToDateDailyAvg,4) As Decimal(18, 4)) 'YrToDateDailyAvg',
		YrToDateTotalCost,
		YrToDateTotalRevenue,
		YrToDateAccessorialCost,
		YrToDateAccessorialRevenue,
		YrToDateFreightCost,
		YrToDateFreightRevenue,
		YrToDateFuelCost,
		YrToDateFuelRevenue,
		YrToDateServiceCost,
		YrToDateServiceRevenue,
		
		PrvMth1Total,
		CAST(ROUND(PrvMth1DailyAvg,4) As Decimal(18, 4)) 'PrvMth1DailyAvg',
		PrvMth1TotalCost,
		PrvMth1TotalRevenue,
		PrvMth1AccessorialCost,
		PrvMth1AccessorialRevenue,
		PrvMth1FreightCost,
		PrvMth1FreightRevenue,
		PrvMth1FuelCost,
		PrvMth1FuelRevenue,
		PrvMth1ServiceCost,
		PrvMth1ServiceRevenue,
		
		PrvMth2Total,
		CAST(ROUND(PrvMth2DailyAvg,4) As Decimal(18, 4)) 'PrvMth2DailyAvg',
		PrvMth2TotalCost,
		PrvMth2TotalRevenue,
		PrvMth2AccessorialCost,
		PrvMth2AccessorialRevenue,
		PrvMth2FreightCost,
		PrvMth2FreightRevenue,
		PrvMth2FuelCost,
		PrvMth2FuelRevenue,
		PrvMth2ServiceCost,
		PrvMth2ServiceRevenue,
		
		PrvMth3Total,
		CAST(ROUND(PrvMth3DailyAvg,4) As Decimal(18, 4)) 'PrvMth3DailyAvg',
		PrvMth3TotalCost,
		PrvMth3TotalRevenue,
		PrvMth3AccessorialCost,
		PrvMth3AccessorialRevenue,
		PrvMth3FreightCost,
		PrvMth3FreightRevenue,
		PrvMth3FuelCost,
		PrvMth3FuelRevenue,
		PrvMth3ServiceCost,
		PrvMth3ServiceRevenue,
		
		PrvMth4Total,
		CAST(ROUND(PrvMth4DailyAvg,4) As Decimal(18, 4)) 'PrvMth4DailyAvg',
		PrvMth4TotalCost,
		PrvMth4TotalRevenue,
		PrvMth4AccessorialCost,
		PrvMth4AccessorialRevenue,
		PrvMth4FreightCost,
		PrvMth4FreightRevenue,
		PrvMth4FuelCost,
		PrvMth4FuelRevenue,
		PrvMth4ServiceCost,
		PrvMth4ServiceRevenue,
		
		PrvMth5Total,
		CAST(ROUND(PrvMth5DailyAvg,4) As Decimal(18, 4)) 'PrvMth5DailyAvg',
		PrvMth5TotalCost,
		PrvMth5TotalRevenue,
		PrvMth5AccessorialCost,
		PrvMth5AccessorialRevenue,
		PrvMth5FreightCost,
		PrvMth5FreightRevenue,
		PrvMth5FuelCost,
		PrvMth5FuelRevenue,
		PrvMth5ServiceCost,
		PrvMth5ServiceRevenue,
		
		PrvMth6Total,
		CAST(ROUND(PrvMth6DailyAvg,4) As Decimal(18, 4)) 'PrvMth6DailyAvg',
		PrvMth6TotalCost,
		PrvMth6TotalRevenue,
		PrvMth6AccessorialCost,
		PrvMth6AccessorialRevenue,
		PrvMth6FreightCost,
		PrvMth6FreightRevenue,
		PrvMth6FuelCost,
		PrvMth6FuelRevenue,
		PrvMth6ServiceCost,
		PrvMth6ServiceRevenue
		
	from #TrendAnalysis

	-- clean table
	DROP TABLE #TrendAnalysis
