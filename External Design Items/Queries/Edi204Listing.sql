DECLARE @TenantId bigint = 4;
DECLARE @UserId bigint = 2;

SELECT  xc.ShipmentIdNumber
		,xc.ControlNumber
		,xc.ExpirationDate
		,xc.DateCreated
		,xc.ReceiptDate
		,ISNULL(d.DirectionText, '') AS 'Direction'
		,ISNULL(edt.EdiDocumentTypeText, '') AS 'DocumentType'
		,xc.CustomerNumber
		,xc.VendorNumber
		,xc.VendorScac
		,xc.PurchaseOrderNumber
		,xc.ShipperReference
		,xc.EquipmentDescriptionCode
		,xc.TotalWeight
		,xc.TotalPackages
		,xc.TotalPieces
		,xc.TotalStops
		,xc.OriginCity
		,xc.OriginCountryCode
		,xc.OriginCountryName
		,xc.OriginPostalCode
		,xc.OriginState
		,xc.OriginStreet1
		,xc.OriginStreet2
		,xc.DestinationCity
		,xc.DestinationCountryCode
		,xc.DestinationCountryName
		,xc.DestinationPostalCode
		,xc.DestinationState
		,xc.DestinationStreet1
		,xc.DestinationStreet2
		,xc.Status
		,xc.StatusMessage
		,(CASE WHEN xc2.ShipmentIdNumber IS NULL THEN 'False' ELSE 'True' END) 'HasLoadTenderResponse'
		,(CASE WHEN xc.FtpTransmission = 0 THEN 'Edi' ELSE 'Ftp' END) 'TransmissionMethod'
		,xc.TransmissionOkay

FROM XmlConnect xc
	INNER JOIN dbo.Customer c ON xc.CustomerNumber = c.CustomerNumber
	LEFT JOIN XmlConnect as xc2 On xc.ShipmentIdNumber = xc2.ShipmentIdNumber AND xc2.DocumentType = 5 -- 990
	LEFT JOIN RefDirection AS d ON d.DirectionIndex = xc.Direction
	LEFT JOIN RefEdiDocumentType AS edt ON edt.EdiDocumentTypeIndex = xc.DocumentType
WHERE 
	xc.DocumentType = 0 
	AND xc.Direction = 0 -- in bound
	AND	xc.TenantId = @TenantId 
	AND
		(
			(SELECT COUNT(*) FROM [User] WHERE Id = @UserId and TenantEmployee = 1) > 0
			OR
			(c.Id IN 
				(
					SELECT CustomerId FROM UserShipAs WHERE TenantId = @TenantId AND UserId = @UserId
					UNION
					SELECT [User].CustomerId FROM [User] WHERE Id = @UserId)
			)
		)

UNION

SELECT  xc.ShipmentIdNumber
		,xc.ControlNumber
		,xc.ExpirationDate
		,xc.DateCreated
		,xc.ReceiptDate
		,ISNULL(d.DirectionText, '') AS 'Direction'
		,ISNULL(edt.EdiDocumentTypeText, '') AS 'DocumentType'
		,xc.CustomerNumber
		,xc.VendorNumber
		,xc.VendorScac
		,xc.PurchaseOrderNumber
		,xc.ShipperReference
		,xc.EquipmentDescriptionCode
		,xc.TotalWeight
		,xc.TotalPackages
		,xc.TotalPieces
		,xc.TotalStops
		,xc.OriginCity
		,xc.OriginCountryCode
		,xc.OriginCountryName
		,xc.OriginPostalCode
		,xc.OriginState
		,xc.OriginStreet1
		,xc.OriginStreet2
		,xc.DestinationCity
		,xc.DestinationCountryCode
		,xc.DestinationCountryName
		,xc.DestinationPostalCode
		,xc.DestinationState
		,xc.DestinationStreet1
		,xc.DestinationStreet2
		,xc.Status
		,xc.StatusMessage
		,(CASE WHEN xc2.ShipmentIdNumber IS NULL THEN 'False' ELSE 'True' END) 'HasLoadTenderResponse'
		,(CASE WHEN xc.FtpTransmission = 0 THEN 'Edi' ELSE 'Ftp' END) 'TransmissionMethod'
		,xc.TransmissionOkay
	

FROM XmlConnect xc
	LEFT JOIN XmlConnect as xc2 On xc.ShipmentIdNumber = xc2.ShipmentIdNumber AND xc.VendorNumber = xc2.VendorNumber AND xc2.DocumentType = 5 -- 990
	LEFT JOIN RefDirection AS d ON d.DirectionIndex = xc.Direction
	LEFT JOIN RefEdiDocumentType AS edt ON edt.EdiDocumentTypeIndex = xc.DocumentType
WHERE 
	xc.DocumentType = 0 
	AND xc.Direction = 1 -- out bound
	AND	xc.TenantId = @TenantId 
	AND	(SELECT COUNT(*) FROM [User] WHERE Id = @UserId and TenantEmployee = 1) > 0