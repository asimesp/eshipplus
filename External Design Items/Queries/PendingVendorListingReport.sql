Declare @UserId bigint = 2
Declare @TenantId bigint = 4

Select
	--[#Columns#]
	pv.VendorNumber
	,pv.Name 'VendorName'
	,pv.Scac
	,pv.MC
	,pv.DOT
	,pv.Notation
	,(Case 
			When pv.Approved = 1 Then 'Approved'
			When pv.Rejected = 1 Then 'Rejected'
			Else 'Pending'
		End) 'ApprovalStatus'
	,(Case 
		When Isnull(v.Id, '') = '' Then 'No'
		Else 'Yes'
	End) 'Vendor Created'
	,pv.DateCreated
	,(Case When pv.DateCreated = '1753-01-01 00:00:00.000' Then '' Else Convert(nvarchar(10),pv.DateCreated,121) End) 'DateCreatedText'
	,pv.TSACertified
	,pv.SmartWayCertified
	,pv.PIPCertified
	,pv.PIPNumber
	,pv.CTPATCertified
	,pv.CTPATNumber
	,pv.FederalIDNumber
	,pv.BrokerReferenceNumber
	,Isnull(brt.BrokerReferenceNumberTypeText, '') AS 'BrokerReferenceNumberType'
	,pv.TrackingUrl
	,pv.HandlesLessThanTruckload
	,pv.HandlesPartialTruckload
	,pv.HandlesTruckload
	,pv.HandlesAir
	,pv.HandlesRail
	,pv.HandlesSmallPack
	,pv.IsCarrier
	,pv.IsAgent
	,pv.IsBroker
	,pv.Notes
	
	,Isnull((Select (dt.Code + ', ')
				From PendingVendorDocument pvd, DocumentTag dt
				Where pvd.PendingVendorId = pv.Id AND pvd.DocumentTagId = dt.Id 
				Order By pvd.Id Asc
				For Xml Path('')),'') As 'DocumentTags'

	,pvl.Street1 'PrimaryPendingVendorLocationStreet1'
	,pvl.Street2 'PrimaryPendingVendorLocationStreet2'
	,pvl.City 'PrimaryPendingVendorLocationCity'
	,pvl.[State] 'PrimaryPendingVendorLocationState'
	,pvl.PostalCode 'PrimaryPendingVendorLocationPostalCode'
	,pvlc.Name 'PrimaryPendingVendorLocationCountry'

	,Isnull(pvc.Name,'') 'PrimaryPendingVendorContactName'
	,Isnull(pvc.Phone,'') 'PrimaryPendingVendorContactPhone'
	,Isnull(pvc.Mobile,'') 'PrimaryPendingVendorContactMobile'
	,Isnull(pvc.Fax,'') 'PrimaryPendingVendorContactFax'
	,Isnull(pvc.Email,'') 'PrimaryPendingVendorContactEmail'
	,Isnull(ct.Code, '') 'PrimaryPendingVendorContactTypeCode'
	,Isnull(ct.[Description], '') 'PrimaryPendingVendorContactTypeDescription'

	,Isnull(cbu.Username,'') 'CreatedByUsername'
	,Isnull(cbu.FirstName, '') 'CreatedByUserFirstName'
	,Isnull(cbu.LastName, '') 'CreatedByUserLastName'

From 
	PendingVendor pv

		Inner Join PendingVendorLocation pvl on pvl.PendingVendorId = pv.Id And pvl.[Primary] = 1
		Inner Join Country pvlc on pvlc.Id = pvl.CountryId
		Left Join PendingVendorContact pvc on pvc.PendingVendorLocationId = pvl.Id And pvc.[Primary] = 1
		Left Join Vendor v on v.VendorNumber = pv.VendorNumber
		Left Join ContactType ct on ct.Id = pvc.ContactTypeId
		Left Join [User] cbu on cbu.Id = pv.CreatedByUserId
		Left Join RefBrokerReferenceNumberType brt on  brt.BrokerReferenceNumberTypeIndex = pv.BrokerReferenceNumberType

Where
	pv.TenantId = @TenantId	
	And (SELECT COUNT(*) FROM [User] WHERE Id = @UserId and TenantEmployee = 1) > 0
	-- And [#Filters#]

-- [#OrderBy#]
