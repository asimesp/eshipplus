Declare @TenantId int = 4
Declare @ActiveUserId int = 2

Select 
	BillDocumentNumber 'Bill Document Number'
	,BillTypeText	'Bill Type Text'
	,BillReferenceNumber 'Bill Reference Number'
	,BillReferenceType 'Bill Reference Type'
	,BillLineQuantity 'Bill Line Quantity'
	,BillLineUnitBuy 'Bill Line Unit Buy'
	,BillLineTotalAmount 'Bill Line Total Amount'
	,BillLineAccountBucketCode 'Bill Line Account Bucket Code'
	,BillLineAccountBucketDescription 'Bill Line Account Bucket Description'
	,BillLineChargeCode	'Bill Line Charge Code'
	,BillLineChargeCodeDescription 'Bill Line Charge Code Description'
	,DateCreated 'Date Created'
	,BillDateCreatedText 'Bill Date Created Text'
	,BillDocumentDate 'Bill Document Date'
	,BillDocumentDateText 'Bill Document Date Text'
	,BillPostDate 'Bill Post Date'
	,BillPostDateText 'Bill Post Date Text'
	,BillExportDate 'Bill Export Date'
	,BillExportDateText	'Bill Export Date Text'
	,BillVendorName	'Bill Vendor Name'
	,BillVendorNumber 'Bill Vendor Number'
	,BillOriginalInvoiceNumber 'Bill Original Invoice Number'
	,ReferenceCustomerName 'Reference Customer Name'
	,ReferenceCustomerNumber 'Reference Customer Number'
	,ReferencePrimaryVendorName 'Reference Primary Vendor Name'
	,ReferencePrimaryVendorNumber 'Reference Primary Vendor Number'
	,StatusText 'Status Text'
	,ReferenceServiceMode 'Reference Service Mode'
	,ReferenceCreatedByUserFirstName 'Reference Created By User First Name'
	,ReferenceCreatedByUserLastName	'Reference Created By User Last Name'
	,ReferenceCreatedByUsername 'Reference Created By Username'
	,ReferenceAccountBucketCode	'Reference Account Bucket Code'
	,ReferenceAccountBucketDescription 'Reference Account Bucket Description'
	,ReferencePrefixCode 'Reference Prefix Code'
	,ReferencePrefixCodeDescription	'Reference Prefix Code Description'
	,ReferenceTotalCost	'Reference Total Cost'

From
	VendorBillDetailsReport(@TenantId, @ActiveUserId)

Where
	1=1