SELECT COUNT(*) 
FROM InvoicePrintLog

declare @TenantId bigint = 4
declare @UserId bigint = 2
declare @CustomerGroupId bigint = 0

SELECT 
	ipl.LogDate,
	(CASE 
		WHEN ipl.LogDate = '1753-01-01 00:00:00.000' THEN '' 
		ELSE CONVERT(nvarchar(20),ipl.LogDate,101) 
	END),-- view log date
	i.InvoiceNumber,
	ISNULL(it.InvoiceTypeText, '') AS 'Invoice Type',-- Invoice Type
	i.InvoiceDate,
	(CASE 
		WHEN  i.InvoiceDate = '1753-01-01 00:00:00.000' THEN '' 
		ELSE CONVERT(nvarchar(20),i.InvoiceDate,101) 
	END),-- view Invoice view date
	i.PostDate,
	(CASE 
		WHEN  i.PostDate = '1753-01-01 00:00:00.000' THEN '' 
		ELSE CONVERT(nvarchar(20),i.PostDate,101) 
	END),-- Invoice Post Date
	(CASE
		WHEN i.Exported = 0 THEN 'No'
		ELSE 'Yes' 
	END),-- Exported
	i.ExportDate,
	(CASE 
		WHEN  i.ExportDate = '1753-01-01 00:00:00.000' THEN '' 
		ELSE CONVERT(nvarchar(20),i.ExportDate,101) 
	END),-- Invoice Exported view Date
	ISNULL(oi.InvoiceNumber,''),
	ISNULL(oit.InvoiceTypeText, '') AS 'Original Invoice Type',-- Invoice Type
	ISNULL(Oi.InvoiceDate,CONVERT(datetime,'1753-01-01 00:00:00.000')), -- ORIGINAL INVOICE POSTED DATE
	ISNULL((CASE 
		WHEN oi.InvoiceDate = '1753-01-01 00:00:00.000' THEN '' 
		ELSE CONVERT(nvarchar(20),oi.InvoiceDate,101) 
	END),''),-- Original Invoice view Date
	ipl.UserFirstName,
	ipl.UserLastName,
	ipl.UserLogon,
	ipl.UserDefaultCustomerNumber,
	ipl.UserDefaultCustomerName,
	(CASE
		WHEN ipl.TenantEmployee = 0 THEN 'No'
		ELSE 'Yes'
	END),
	(SELECT SUM(InvoiceDetail.Quantity * (InvoiceDetail.UnitSell - InvoiceDetail.UnitDiscount))FROM InvoiceDetail WHERE InvoiceDetail.InvoiceId = i.Id),
	ISNULL((SELECT DISTINCT (id.ReferenceNumber + ', ')
				FROM InvoiceDetail id
				WHERE id.InvoiceId = i.Id AND id.ReferenceType = 0
				ORDER BY (id.ReferenceNumber + ', ') Desc
				FOR XML PATH('')),''),--Shipment References
	ISNULL((SELECT DISTINCT (id.ReferenceNumber + ', ')
				FROM InvoiceDetail id
				WHERE id.InvoiceId = i.Id AND id.ReferenceType = 1
				ORDER BY (id.ReferenceNumber + ', ') Desc
				FOR XML PATH('')),'') --Service Ticket References
FROM InvoicePrintLog AS ipl
	INNER JOIN Invoice AS i ON i.Id = ipl.InvoiceId
	INNER JOIN CustomerLocation AS cl ON i.CustomerLocationId = cl.Id
	INNER JOIN Customer AS c ON cl.CustomerId = c.Id
	LEFT JOIN Invoice AS oi ON oi.Id = i.OriginalInvoiceId
	LEFT JOIN RefInvoiceType AS it ON it.InvoiceTypeIndex = i.InvoiceType
	LEFT JOIN RefInvoiceType AS oit ON oit.InvoiceTypeIndex = oi.InvoiceType
WHERE
	ipl.TenantId = @TenantId
		AND @UserId <> 0
		AND (
				(Select COUNT(*) from [User] where id = @UserId and TenantEmployee = 1) > 0
				OR
				(c.Id IN (SELECT CustomerId FROM UserShipAs WHERE TenantId = @TenantId AND UserId = @UserId
					UNION
					SELECT [User].CustomerId FROM [User] WHERE Id = @UserId)))
		AND (@CustomerGroupId = 0 OR (SELECT COUNT(*) FROM CustomerGroupMap WHERE CustomerId = c.Id AND CustomerGroupId = @CustomerGroupId) > 0)
--		AND {1}
--ORDER BY 
--	{2} 