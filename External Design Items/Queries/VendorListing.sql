

DECLARE @TenantId bigint = 4;
DECLARE @UserId bigint = 3;
DECLARE @VendorGroupId bigint = 0;

SELECT 
	COUNT(*)
FROM 
	Vendor

SELECT 
	ISNULL((SELECT (dt.Code + ', ')
				FROM VendorDocument vd, DocumentTag dt
				WHERE vd.VendorId = v.Id AND vd.DocumentTagId = dt.Id 
				ORDER BY vd.Id Asc
				FOR XML PATH('')),'') AS 'DocumentTags',
	v.VendorNumber,
	v.Name,
	v.Scac,
	v.DOT,
	v.MC,
	v.DateCreated,
	(CASE WHEN v.Active = 0 THEN 'False' WHEN v.Active = 1 THEN 'True' END) 'VendorIsActive',
	(CASE WHEN v.CTPATCertified = 0 THEN 'False' WHEN v.CTPATCertified = 1 THEN 'True' END) 'CTPATCertified',
	(CASE WHEN v.PIPCertified = 0 THEN 'False' WHEN v.PIPCertified = 1 THEN 'True' END) 'PIPCertified',
	(CASE WHEN v.SmartWayCertified = 0 THEN 'False' WHEN v.SmartWayCertified = 1 THEN 'True' END) 'SmartWayCertified',
	(CASE WHEN v.TSACertified = 0 THEN 'False' WHEN v.TSACertified = 1 THEN 'True' END) 'TSACertified',
	(CASE WHEN v.HandlesLessThanTruckLoad = 1 THEN 'True' ELSE 'False'END) 'HandlesLessThanTruckLoad',
	(CASE WHEN v.HandlesTruckLoad = 1 THEN 'True' ELSE 'False'END) 'HandlesTruckLoad',
	(CASE WHEN v.HandlesAir = 1 THEN 'True' ELSE 'False'END) 'HandlesAir',
	(CASE WHEN v.HandlesPartialTruckload = 1 THEN 'True' ELSE 'False'END) 'HandlesPartialTruckload',
	(CASE WHEN v.HandlesRail = 1 THEN 'True' ELSE 'False'END) 'HandlesRail',
	(CASE WHEN v.HandlesSmallPack= 1 THEN 'True' ELSE 'False'END) 'HandlesSmallPack',
	v.PIPNumber,
	v.CTPATNumber,
	ISNULL(vsru.Username,'') 'VendorServiceRepUsername',
	ISNULL(vsru.FirstName,'') 'VendorServiceRepFirstName',
	ISNULL(vsru.LastName,'') 'VendorServiceRepLastName',
	vl.Street1 'PrimaryVendorLocationStreet1',
	vl.Street2 'PrimaryVendorLocationStreet2',
	vl.City 'PrimaryVendorLocationCity',
	vl.[State] 'PrimaryVendorLocationState',
	vl.PostalCode 'PrimaryVendorLocationPostalCode',
	vlc.Name 'PrimaryVendorLocationCountry',
	ISNULL(vc.Name,'') 'PrimaryVendorContactName',
	ISNULL(vc.Phone,'') 'PrimaryVendorContactPhone',
	ISNULL(vc.Mobile,'') 'PrimaryVendorContactMobile',
	ISNULL(vc.Fax,'') 'PrimaryVendorContactFax',
	ISNULL(vc.Email,'') 'PrimaryVendorContactEmail',
	ISNULL(lau.Username, '') 'LastAuditedByUsername',
	ISNULL(lau.FirstName, '') 'LastAuditedByFirstName',
	ISNULL(lau.LastName, '') 'LastAuditedByLastName',
	v.LastAuditDate,
	(CASE WHEN v.ExcludeFromAvailableLoadsBlast = 1 THEN 'Yes' ELSE 'No' END) 'ExcludeOnScoreCards',
	v.Notes,
	v.Notation
FROM
	Vendor v
		INNER JOIN VendorLocation vl ON vl.VendorId = v.Id AND vl.[Primary] = 1
		INNER JOIN Country vlc ON vlc.Id = vl.CountryId
		LEFT JOIN VendorContact vc ON vc.VendorLocationId = vl.Id AND (vc.[Primary] IS NULL OR vc.[Primary] = 1)
		LEFT JOIN [User] vsru ON vsru.Id = v.VendorServiceRepUserId
		LEFT JOIN [User] lau ON lau.Id = v.LastAuditedByUserId
WHERE
	v.TenantId = @TenantId
		AND @UserId <> 0
		AND (@VendorGroupId = 0 OR (SELECT COUNT(*) FROM VendorGroupMap WHERE VendorId = v.Id AND VendorGroupId = @VendorGroupId) > 0)
ORDER BY v.VendorNumber



