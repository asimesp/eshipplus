DECLARE @TenantId BIGINT = 4
DECLARE @UserId BIGINT = 4

SELECT
	c.CustomerNumber,
	c.Name, 
	(CASE WHEN c.Active = 0 THEN 'No' ELSE 'Yes' END) AS 'CustomerIsActive',
	(SELECT COUNT(cpo.Id) FROM CustomerPurchaseOrder AS cpo WHERE cpo.CustomerId = c.Id) AS 'CountOfCustomerPurchaseOrders',
	(CASE WHEN c.ShipmentRequiresPurchaseOrderNumber = 0 THEN 'No' ELSE 'Yes' END) AS 'PurchaseOrderNumberRequiredOnShipment',
	(CASE WHEN c.ValidatePurchaseOrderNumber = 0 THEN 'No' ELSE 'Yes' END) AS 'ValidatePurchaseOrderNumber',
	c.InvalidPurchaseOrderNumberMessage
FROM
	Customer AS c
WHERE
	@TenantId <> 0
	AND @UserId <> 0
	AND @TenantId = c.TenantId
--		AND {1}
--ORDER BY 
--	{2}