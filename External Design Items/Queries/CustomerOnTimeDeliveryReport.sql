declare @TenantId bigint = 4
declare @UserId bigint = 2
declare @StartDate datetime ='2015-02-23'
declare @EndDate datetime ='2016-03-05'

select 
 TotalShipments
 ,TotalOnTime
 ,EstimatedDeliveryDate
 ,CustomerNumber
 ,Name
 ,CustomerId
from
 CustomerOnTimeDeliveryReport(@TenantId, @UserId,@StartDate, @EndDate) 
 where CustomerNumber = '7248'
order by name, EstimatedDeliveryDate