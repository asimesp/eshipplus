Declare @TenantId bigint = 4
Declare @ActiveUserId bigint = 2
Declare @CustomerGroupId bigint = 0

-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
Set NoCount On;

-- ignoring voided shipments!
	
-- month start and end
Declare @MonthStart datetime = CONVERT(datetime, 
	Convert(nvarchar(4), Year(GETDATE())) + '-' + 
	Convert(nvarchar(2), Month(GETDATE())) + '-01 00:00:00'
)
Declare @MonthEnd datetime = DATEADD(SECOND,-1,DATEADD(MONTH,1,@MonthStart))

-- Verify temp table doesn't exist, if it does, drop it.
IF object_id('tempdb..#CustomerTrendingAnalysisMarketing') is not null
	Drop Table #CustomerTrendingAnalysisMarketing

-- Create Temp Table	
Create Table #CustomerTrendingAnalysisMarketing
(
	CustomerId bigint,
	CustomerNumber nvarchar(50),
	CustomField1 nvarchar(50),
	CustomField2 nvarchar(50),
	CustomField3 nvarchar(50),
	CustomField4 nvarchar(50),
	CustomerSetupDate datetime,
	SalesRepresentativeName nvarchar(50),
	ContactName nvarchar(50),
	ContactTitle nvarchar(50),
	ContactEmail nvarchar(50),
	ContactPhone nvarchar(50),
	CustomerName nvarchar(50),
	Street1 nvarchar(50),
	Street2 nvarchar(50),
	City nvarchar(50),
	[State] nvarchar(50),
	PostalCode nvarchar(50),
	Country nvarchar(50),
					
	CMTotalShpts int,
	CMTotalCharges decimal(38, 4),
	CMLTLShpts int,
	CMLTLCharges decimal(38, 4),
	CMFTLShpts int,
	CMFTLCharges decimal(38, 4),
	CMSmallPackShpts int,
	CMSmallPackCharges decimal(38, 4),
	CMAirShpts int,
	CMAirCharges decimal(38, 4),
	CMRailShpts int,
	CMRailCharges decimal(38, 4),
	
	PM1TotalShpts int,
	PM1TotalCharges decimal(38, 4),
	PM1LTLShpts int,
	PM1LTLCharges decimal(38, 4),
	PM1FTLShpts int,
	PM1FTLCharges decimal(38, 4),
	PM1SmallPackShpts int,
	PM1SmallPackCharges decimal(38, 4),
	PM1AirShpts int,
	PM1AirCharges decimal(38, 4),
	PM1RailShpts int,
	PM1RailCharges decimal(38, 4),

	PM2TotalShpts int,
	PM2TotalCharges decimal(38, 4),
	PM2LTLShpts int,
	PM2LTLCharges decimal(38, 4),
	PM2FTLShpts int,
	PM2FTLCharges decimal(38, 4),
	PM2SmallPackShpts int,
	PM2SmallPackCharges decimal(38, 4),
	PM2AirShpts int,
	PM2AirCharges decimal(38, 4),
	PM2RailShpts int,
	PM2RailCharges decimal(38, 4),

	PM3TotalShpts int,
	PM3TotalCharges decimal(38, 4),
	PM3LTLShpts int,
	PM3LTLCharges decimal(38, 4),
	PM3FTLShpts int,
	PM3FTLCharges decimal(38, 4),
	PM3SmallPackShpts int,
	PM3SmallPackCharges decimal(38, 4),
	PM3AirShpts int,
	PM3AirCharges decimal(38, 4),
	PM3RailShpts int,
	PM3RailCharges decimal(38, 4),

	PM4TotalShpts int,
	PM4TotalCharges decimal(38, 4),
	PM4LTLShpts int,
	PM4LTLCharges decimal(38, 4),
	PM4FTLShpts int,
	PM4FTLCharges decimal(38, 4),
	PM4SmallPackShpts int,
	PM4SmallPackCharges decimal(38, 4),
	PM4AirShpts int,
	PM4AirCharges decimal(38, 4),
	PM4RailShpts int,
	PM4RailCharges decimal(38, 4),

	PM5TotalShpts int,
	PM5TotalCharges decimal(38, 4),
	PM5LTLShpts int,
	PM5LTLCharges decimal(38, 4),
	PM5FTLShpts int,
	PM5FTLCharges decimal(38, 4),
	PM5SmallPackShpts int,
	PM5SmallPackCharges decimal(38, 4),
	PM5AirShpts int,
	PM5AirCharges decimal(38, 4),
	PM5RailShpts int,
	PM5RailCharges decimal(38, 4),

	PM6TotalShpts int,
	PM6TotalCharges decimal(38, 4),
	PM6LTLShpts int,
	PM6LTLCharges decimal(38, 4),
	PM6FTLShpts int,
	PM6FTLCharges decimal(38, 4),
	PM6SmallPackShpts int,
	PM6SmallPackCharges decimal(38, 4),
	PM6AirShpts int,
	PM6AirCharges decimal(38, 4),
	PM6RailShpts int,
	PM6RailCharges decimal(38, 4),
	
)

Insert into #CustomerTrendingAnalysisMarketing 
		(CustomerId , CustomerNumber, CustomField1, CustomField2, CustomField3, CustomField4, CustomerSetupDate, SalesRepresentativeName, ContactName, ContactTitle, ContactEmail, ContactPhone, CustomerName, Street1, Street2, City, [State], PostalCode, Country,     
			CMTotalShpts, CMTotalCharges, CMLTLShpts, CMLTLCharges, CMFTLShpts, CMFTLCharges, CMSmallPackShpts, CMSmallPackCharges, CMAirShpts, CMAirCharges, CMRailShpts, CMRailCharges, 
			PM1TotalShpts, PM1TotalCharges, PM1LTLShpts, PM1LTLCharges, PM1FTLShpts, PM1FTLCharges, PM1SmallPackShpts, PM1SmallPackCharges, PM1AirShpts, PM1AirCharges, PM1RailShpts, PM1RailCharges,
			PM2TotalShpts, PM2TotalCharges, PM2LTLShpts, PM2LTLCharges, PM2FTLShpts, PM2FTLCharges, PM2SmallPackShpts, PM2SmallPackCharges, PM2AirShpts, PM2AirCharges, PM2RailShpts, PM2RailCharges,
			PM3TotalShpts, PM3TotalCharges, PM3LTLShpts, PM3LTLCharges, PM3FTLShpts, PM3FTLCharges, PM3SmallPackShpts, PM3SmallPackCharges, PM3AirShpts, PM3AirCharges, PM3RailShpts, PM3RailCharges,
			PM4TotalShpts, PM4TotalCharges, PM4LTLShpts, PM4LTLCharges, PM4FTLShpts, PM4FTLCharges, PM4SmallPackShpts, PM4SmallPackCharges, PM4AirShpts, PM4AirCharges, PM4RailShpts, PM4RailCharges,
			PM5TotalShpts, PM5TotalCharges, PM5LTLShpts, PM5LTLCharges, PM5FTLShpts, PM5FTLCharges, PM5SmallPackShpts, PM5SmallPackCharges, PM5AirShpts, PM5AirCharges, PM5RailShpts, PM5RailCharges,
			PM6TotalShpts, PM6TotalCharges, PM6LTLShpts, PM6LTLCharges, PM6FTLShpts, PM6FTLCharges, PM6SmallPackShpts, PM6SmallPackCharges, PM6AirShpts, PM6AirCharges, PM6RailShpts, PM6RailCharges)

Select	 
			c.Id 'Customer Id'
			,c.CustomerNumber
			,c.CustomField1
			,c.CustomField2
			,c.CustomField3
			,c.CustomField4
			,c.DateCreated 'Customer Setup Date'
			,ISNULL(sr.Name, '') 'Sales Representative Name'
			,ISNULL(cc.Name, '') 'Contact Name'
			,ISNULL(ct.[Code], '') 'Contact Title'
			,ISNULL(cc.Email, '') 'Contact Email'
			,ISNULL(cc.Phone, '') 'Contact Phone'
			,c.Name 'Customer Name'
			,cl.Street1
			,cl.Street2
			,cl.City
			,cl.[State]
			,cl.PostalCode	
			,clc.Name 'Country'
			
			--Current Month		
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.[Status] <> 4 And @MonthStart <= s.DateCreated and s.DateCreated <= @MonthEnd) 'CM Total Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And s.[Status] <> 4 And @MonthStart <= s.DateCreated and s.DateCreated <= @MonthEnd),0) 'CM Total Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 0 And s.[Status] <> 4 And @MonthStart <= s.DateCreated and s.DateCreated <= @MonthEnd) 'CM LTL Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 0 And s.Id = scs.ShipmentId And s.[Status] <> 4 And @MonthStart <= s.DateCreated and s.DateCreated <= @MonthEnd),0) 'CM LTL Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 1 And s.[Status] <> 4 And @MonthStart <= s.DateCreated and s.DateCreated <= @MonthEnd) 'CM FTL Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 1 And s.Id = scs.ShipmentId And s.[Status] <> 4 And @MonthStart <= s.DateCreated and s.DateCreated <= @MonthEnd),0) 'CM FTL Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 4 And s.[Status] <> 4 And @MonthStart <= s.DateCreated and s.DateCreated <= @MonthEnd) 'CM Small Pack Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 4 And s.Id = scs.ShipmentId And s.[Status] <> 4 And @MonthStart <= s.DateCreated and s.DateCreated <= @MonthEnd),0) 'CM Small Pack Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 2 And s.[Status] <> 4 And @MonthStart <= s.DateCreated and s.DateCreated <= @MonthEnd) 'CM Air Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 2 And s.Id = scs.ShipmentId And s.[Status] <> 4 And @MonthStart <= s.DateCreated and s.DateCreated <= @MonthEnd),0) 'CM Air Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 3 And s.[Status] <> 4 And @MonthStart <= s.DateCreated and s.DateCreated <= @MonthEnd) 'CM Rail Total Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 3 And s.Id = scs.ShipmentId And s.[Status] <> 4 And @MonthStart <= s.DateCreated and s.DateCreated <= @MonthEnd),0) 'CM Rail Charges'

			--Previous Month 1
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.[Status] <> 4 And DATEADD(MONTH, -1, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -1, @MonthEnd)) 'PM1 Total Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -1, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -1, @MonthEnd)),0) 'PM1 Total Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 0 And s.[Status] <> 4 And DATEADD(MONTH, -1, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -1, @MonthEnd)) 'PM1 LTL Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 0 And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -1, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -1, @MonthEnd)),0) 'PM1 LTL Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 1 And s.[Status] <> 4 And DATEADD(MONTH, -1, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -1, @MonthEnd)) 'PM1 FTL Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 1 And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -1, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -1, @MonthEnd)),0) 'PM1 FTL Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 4 And s.[Status] <> 4 And DATEADD(MONTH, -1, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -1, @MonthEnd)) 'PM1 Small Pack Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 4 And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -1, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -1, @MonthEnd)),0) 'PM1 Small Pack Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 2 And s.[Status] <> 4 And DATEADD(MONTH, -1, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -1, @MonthEnd)) 'PM1 Air Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 2 And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -1, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -1, @MonthEnd)),0) 'PM1 Air Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 3 And s.[Status] <> 4 And DATEADD(MONTH, -1, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -1, @MonthEnd)) 'PM1 Rail Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 3 And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -1, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -1, @MonthEnd)),0) 'PM1 Rail Charges'

			--Previous Month 2
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.[Status] <> 4 And DATEADD(MONTH, -2, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -2, @MonthEnd)) 'PM2 Total Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -2, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -2, @MonthEnd)),0) 'PM2 Total Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 0 And s.[Status] <> 4 And DATEADD(MONTH, -2, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -2, @MonthEnd)) 'PM2 LTL Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 0 And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -2, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -2, @MonthEnd)),0) 'PM2 LTL Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 1 And s.[Status] <> 4 And DATEADD(MONTH, -2, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -2, @MonthEnd)) 'PM2 FTL Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 1 And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -2, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -2, @MonthEnd)),0) 'PM2 FTL Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 4 And s.[Status] <> 4 And DATEADD(MONTH, -2, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -2, @MonthEnd)) 'PM2 Small Pack Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 4 And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -2, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -2, @MonthEnd)),0) 'PM2 Small Pack Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 2 And s.[Status] <> 4 And DATEADD(MONTH, -2, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -2, @MonthEnd)) 'PM2 Air Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 2 And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -2, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -2, @MonthEnd)),0) 'PM2 Air Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 3 And s.[Status] <> 4 And DATEADD(MONTH, -2, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -2, @MonthEnd)) 'PM2 Rail Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 3 And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -2, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -2, @MonthEnd)),0) 'PM2 Rail Charges'

			--Previous Month 3
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.[Status] <> 4 And DATEADD(MONTH, -3, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -3, @MonthEnd)) 'PM3 Total Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -3, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -3, @MonthEnd)),0) 'PM3 Total Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 0 And s.[Status] <> 4 And DATEADD(MONTH, -3, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -3, @MonthEnd)) 'PM3 LTL Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 0 And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -3, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -3, @MonthEnd)),0) 'PM3 LTL Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 1 And s.[Status] <> 4 And DATEADD(MONTH, -3, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -3, @MonthEnd)) 'PM3 FTL Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 1 And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -3, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -3, @MonthEnd)),0) 'PM3 FTL Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 4 And s.[Status] <> 4 And DATEADD(MONTH, -3, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -3, @MonthEnd)) 'PM3 Small Pack Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 4 And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -3, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -3, @MonthEnd)),0) 'PM3 Small Pack Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 2 And s.[Status] <> 4 And DATEADD(MONTH, -3, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -3, @MonthEnd)) 'PM3 Air Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 2 And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -3, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -3, @MonthEnd)),0) 'PM3 Air Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 3 And s.[Status] <> 4 And DATEADD(MONTH, -3, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -3, @MonthEnd)) 'PM3 Rail Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 3 And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -3, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -3, @MonthEnd)),0) 'PM3 Rail Charges'

			--Previous Month 4
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.[Status] <> 4 And DATEADD(MONTH, -4, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -4, @MonthEnd)) 'PM4 Total Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -4, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -4, @MonthEnd)),0) 'PM4 Total Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 0 And s.[Status] <> 4 And DATEADD(MONTH, -4, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -4, @MonthEnd)) 'PM4 LTL Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 0 And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -4, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -4, @MonthEnd)),0) 'PM4 LTL Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 1 And s.[Status] <> 4 And DATEADD(MONTH, -4, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -4, @MonthEnd)) 'PM4 FTL Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 1 And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -4, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -4, @MonthEnd)),0) 'PM4 FTL Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 4 And s.[Status] <> 4 And DATEADD(MONTH, -4, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -4, @MonthEnd)) 'PM4 Small Pack Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 4 And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -4, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -4, @MonthEnd)),0) 'PM4 Small Pack Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 2 And s.[Status] <> 4 And DATEADD(MONTH, -4, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -4, @MonthEnd)) 'PM4 Air Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 2 And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -4, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -4, @MonthEnd)),0) 'PM4 Air Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 3 And s.[Status] <> 4 And DATEADD(MONTH, -4, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -4, @MonthEnd)) 'PM4 Rail Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 3 And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -4, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -4, @MonthEnd)),0) 'PM4 Rail Charges'

			--Previous Month 5
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.[Status] <> 4 And DATEADD(MONTH, -5, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -5, @MonthEnd)) 'PM5 Total Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -5, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -5, @MonthEnd)),0) 'PM5 Total Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 0 And s.[Status] <> 4 And DATEADD(MONTH, -5, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -5, @MonthEnd)) 'PM5 LTL Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 0 And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -5, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -5, @MonthEnd)),0) 'PM5 LTL Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 1 And s.[Status] <> 4 And DATEADD(MONTH, -5, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -5, @MonthEnd)) 'PM5 FTL Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 1 And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -5, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -5, @MonthEnd)),0) 'PM5 FTL Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 4 And s.[Status] <> 4 And DATEADD(MONTH, -5, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -5, @MonthEnd)) 'PM5 Small Pack Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 4 And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -5, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -5, @MonthEnd)),0) 'PM5 Small Pack Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 2 And s.[Status] <> 4 And DATEADD(MONTH, -5, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -5, @MonthEnd)) 'PM5 Air Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 2 And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -5, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -5, @MonthEnd)),0) 'PM5 Air Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 3 And s.[Status] <> 4 And DATEADD(MONTH, -5, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -5, @MonthEnd)) 'PM5 Rail Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 3 And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -5, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -5, @MonthEnd)),0) 'PM5 Rail Charges'

			--Previous Month 6
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.[Status] <> 4 And DATEADD(MONTH, -6, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -6, @MonthEnd)) 'PM6 Total Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -6, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -6, @MonthEnd)),0) 'PM6 Total Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 0 And s.[Status] <> 4 And DATEADD(MONTH, -6, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -6, @MonthEnd)) 'PM6 LTL Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 0 And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -6, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -6, @MonthEnd)),0) 'PM6 LTL Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 1 And s.[Status] <> 4 And DATEADD(MONTH, -6, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -6, @MonthEnd)) 'PM6 FTL Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 1 And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -6, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -6, @MonthEnd)),0) 'PM6 FTL Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 4 And s.[Status] <> 4 And DATEADD(MONTH, -6, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -6, @MonthEnd)) 'PM6 Small Pack Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 4 And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -6, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -6, @MonthEnd)),0) 'PM6 Small Pack Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 2 And s.[Status] <> 4 And DATEADD(MONTH, -6, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -6, @MonthEnd)) 'PM6 Air Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 2 And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -6, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -6, @MonthEnd)),0) 'PM6 Air Charges'
			,(Select Count(s.Id) From Shipment s Where s.CustomerId = c.Id And s.ServiceMode = 3 And s.[Status] <> 4 And DATEADD(MONTH, -6, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -6, @MonthEnd)) 'PM6 Rail Shpts'
			,ISNULL((Select SUM(scs.TotalAmountDue) From Shipment s, ShipmentChargeStatistics scs Where s.CustomerId = c.Id And s.ServiceMode = 3 And s.Id = scs.ShipmentId And s.[Status] <> 4 And DATEADD(MONTH, -6, @MonthStart) <= s.DateCreated and s.DateCreated <= DATEADD(MONTH, -6, @MonthEnd)),0) 'PM6 Rail Charges'

From 
	Customer c
	Inner Join CustomerLocation cl on cl.CustomerId = c.Id And cl.[Primary] = 1
	Inner Join Country clc on cl.CountryId = clc.Id
	Left Join CustomerContact cc on cc.CustomerLocationId = cl.Id
	Left Join ContactType ct on cc.ContactTypeId = ct.Id
	Left Join SalesRepresentative sr on c.SalesRepresentativeId = sr.Id
	
Where c.TenantId = @TenantId
				AND (
					(Select COUNT(*) From [User] Where Id = @ActiveUserId and TenantEmployee = 1) > 0
					OR
					(c.Id IN 
						(Select CustomerId From UserShipAs Where TenantId = @TenantId AND UserId = @ActiveUserId
							Union
							Select [User].CustomerId From [User] Where Id = @ActiveUserId)))
				AND (@CustomerGroupId = 0 OR (Select COUNT(*) from CustomerGroupMap Where CustomerId = c.Id and CustomerGroupId = @CustomerGroupId) > 0)


Select 
	CustomerId
	,CustomerNumber
	,CustomField1
	,CustomField2
	,CustomField3
	,CustomField4
	,CustomerSetupDate
	,SalesRepresentativeName
	,ContactName
	,ContactTitle
	,ContactEmail
	,ContactPhone
	,CustomerName
	,Street1
	,Street2
	,City
	,[State]
	,PostalCode
	,Country

	,CMTotalShpts
	,CMTotalCharges
	,CMLTLShpts
	,CMLTLCharges
	,CMFTLShpts
	,CMFTLCharges
	,CMSmallPackShpts
	,CMSmallPackCharges
	,CMAirShpts
	,CMAirCharges
	,CMRailShpts
	,CMRailCharges

	,PM1TotalShpts
	,PM1TotalCharges
	,PM1LTLShpts
	,PM1LTLCharges
	,PM1FTLShpts
	,PM1FTLCharges
	,PM1SmallPackShpts
	,PM1SmallPackCharges
	,PM1AirShpts
	,PM1AirCharges
	,PM1RailShpts
	,PM1RailCharges

	,PM2TotalShpts
	,PM2TotalCharges
	,PM2LTLShpts
	,PM2LTLCharges
	,PM2FTLShpts
	,PM2FTLCharges
	,PM2SmallPackShpts
	,PM2SmallPackCharges
	,PM2AirShpts
	,PM2AirCharges
	,PM2RailShpts
	,PM2RailCharges

	,PM3TotalShpts
	,PM3TotalCharges
	,PM3LTLShpts
	,PM3LTLCharges
	,PM3FTLShpts
	,PM3FTLCharges
	,PM3SmallPackShpts
	,PM3SmallPackCharges
	,PM3AirShpts
	,PM3AirCharges
	,PM3RailShpts
	,PM3RailCharges

	,PM4TotalShpts
	,PM4TotalCharges
	,PM4LTLShpts
	,PM4LTLCharges
	,PM4FTLShpts
	,PM4FTLCharges
	,PM4SmallPackShpts
	,PM4SmallPackCharges
	,PM4AirShpts
	,PM4AirCharges
	,PM4RailShpts
	,PM4RailCharges

	,PM5TotalShpts
	,PM5TotalCharges
	,PM5LTLShpts
	,PM5LTLCharges
	,PM5FTLShpts
	,PM5FTLCharges
	,PM5SmallPackShpts
	,PM5SmallPackCharges
	,PM5AirShpts
	,PM5AirCharges
	,PM5RailShpts
	,PM5RailCharges

	,PM6TotalShpts
	,PM6TotalCharges
	,PM6LTLShpts
	,PM6LTLCharges
	,PM6FTLShpts
	,PM6FTLCharges
	,PM6SmallPackShpts
	,PM6SmallPackCharges
	,PM6AirShpts
	,PM6AirCharges
	,PM6RailShpts
	,PM6RailCharges
 
From #CustomerTrendingAnalysisMarketing

-- clean table
Drop Table #CustomerTrendingAnalysisMarketing