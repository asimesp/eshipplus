DECLARE @TenantId bigint = 4
DECLARE @UserId bigint = 4

SELECT 
	x.ShipmentIdNumber 'Id Number'
	,x.DateCreated
    ,x.ExpirationDate
	,ISNULL(edt.EdiDocumentTypeText, '') AS 'Document Type'
	,ISNULL(dir.DirectionText, '') AS 'Direction'
	, (
			CASE
				WHEN x.DocumentType = 0 AND x.Direction = 0 THEN ISNULL((SELECT TOP 1 Id FROM LoadOrder lo WHERE x.ShipmentIdNumber <> '' AND lo.ShipperBol = x.ShipmentIdNumber AND lo.TenantId = x.TenantId), 0) -- Inbound 204
				WHEN X.DocumentType = 0 AND x.Direction = 1 THEN ISNULL((SELECT TOP 1 Id FROM LoadOrder lo WHERE lo.LoadOrderNumber = x.ShipmentIdNumber AND lo.TenantId = x.TenantId), 0) --  Outbound 204
				WHEN x.DocumentType = 5 AND x.Direction = 1 THEN ISNULL((SELECT TOP 1 Id FROM LoadOrder lo WHERE x.ShipmentIdNumber <> '' AND lo.ShipperBol = x.ShipmentIdNumber AND lo.TenantId = x.TenantId), 0) -- Outbound 990
				WHEN X.DocumentType = 5 AND x.Direction = 0 THEN ISNULL((SELECT TOP 1 Id FROM LoadOrder lo WHERE lo.LoadOrderNumber = x.ShipmentIdNumber AND lo.TenantId = x.TenantId), 0) --  Inbound 990
				ELSE 0
			END
		) 'LoadOrderId'
	
FROM XmlConnect x
	LEFT JOIN RefEdiDocumentType AS edt ON edt.EdiDocumentTypeIndex = x.DocumentType
	LEFT JOIN RefDirection AS dir ON dir.DirectionIndex = x.Direction

WHERE
		edt.EdiDocumentTypeIndex = 0
		AND dir.DirectionIndex = 0
		AND x.TenantId = @TenantId
		AND @UserId <> 0