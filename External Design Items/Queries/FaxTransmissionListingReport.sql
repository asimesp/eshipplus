SELECT COUNT(*) FROM FaxTransmission

DECLARE @TenantId bigint = 4
DECLARE @UserId bigint = 4

SELECT
	f.ShipmentNumber,
	f.SendDateTime,
	(CASE 
		WHEN f.SendDateTime = '1753-01-01 00:00:00.000' THEN '' 
		ELSE CONVERT(nvarchar(20),f.SendDateTime,101) 
	END),--view Send Date
	f.ResponseDateTime,
	(CASE 
		WHEN f.ResponseDateTime = '1753-01-01 00:00:00.000' THEN '' 
		ELSE CONVERT(nvarchar(20),f.ResponseDateTime,101) 
	END),--view Send Date
	f.[Message],
	f.ResolutionComment,
	ISNULL(fts.FaxTransmissionStatusText, '') AS 'Status',
	(CASE
		WHEN f.Resolved = 0 THEN 'No'
		ELSE 'Yes'
	END) AS  'resolved',
	f.LastStatusCheckDateTime, 
	ISNULL(sp.ServiceProviderText, '') AS 'ServiceProvider',
	(CASE WHEN f.LastStatusCheckDateTime = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),f.LastStatusCheckDateTime,101) END) AS 'ViewLastStatusCheckDateTime'
	
FROM
	FaxTransmission AS f
	LEFT JOIN RefFaxTransmissionStatus AS fts ON fts.FaxTransmissionStatusIndex = f.[Status]
	LEFT JOIN RefServiceProvider AS sp ON sp.ServiceProviderIndex = f.ServiceProvider
WHERE
	f.TenantId = @TenantId
		AND @UserId <> 0
--		AND {1}
--ORDER BY 
--	{2} 