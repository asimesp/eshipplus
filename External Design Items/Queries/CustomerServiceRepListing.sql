
SELECT 
	COUNT(*)
FROM 
	CustomerServiceRepresentative

declare @TenantId bigint = 4
declare @UserId bigint = 2
declare @CustomerGroupId bigint = 0
declare @VendorGroupId bigint = 0

SELECT
	u.Username,
	u.FirstName,
	u.LastName,
	c.DateCreated,
	(CASE 
		WHEN c.DateCreated = '1753-01-01 00:00:00.000' THEN '' 
		ELSE CONVERT(nvarchar(20),c.DateCreated,101) 
	END),--view date created
	c.CustomerNumber,
	c.Name,
	(CASE
		WHEN c.Active = 0 THEN 'No'
		ELSE 'Yes'
	END),--customer active
	t.TierNumber,
	t.Name,
	ISNULL(cut.CustomerTypeText, '') AS 'Customer Type',
	c.InvoiceTerms,
	c.Notes,
	(CASE
		WHEN ab.Active = 0 THEN 'No'
		ELSE 'Yes'
	END),--Account Bucket is Active
	ab.Code,
	ab.[Description],
	ISNULL(sr.SalesRepresentativeNumber,''),
	ISNULL(sr.Name,''),
	(CASE
		WHEN c.HidePrefix = 0 THEN 'No'
		ELSE 'Yes'
	End),--Hide Prefix
	p.Code,
	p.[Description],
	(CASE	
		WHEN cl.BillToLocation = 0 THEN 'Yes'
		ElSE 'No'
	End),--Bill to Location
	cl.Street1,
	cl.Street2,
	cl.City,
	cl.[State],
	cl.PostalCode,
	ct.Name,
	ISNULL(cc.Name,''),
	ISNULL(ctype.Code, ''),
	ISNULL(ctype.[Description],''),
	ISNULL(cc.Phone,''),
	ISNULL(cc.Mobile,''),
	ISNULL(cc.Email,''),
	ISNULL(cc.Fax,'')
FROM
	[User] AS u
	INNER JOIN 
		CustomerServiceRepresentative AS csr ON csr.UserId = u.Id
	INNER JOIN 
		Customer AS c ON c.Id = csr.CustomerId
	INNER JOIN 
		CustomerLocation AS cl ON cl.CustomerId = c.Id AND cl.[Primary] = 1
	INNER JOIN
		Country AS ct ON ct.Id = cl.CountryId
	INNER JOIN 
		Tier AS t ON t.Id = c.TierId
	INNER JOIN 
		AccountBucket AS ab On ab.Id = c.DefaultAccountBucketId
	LEFT JOIN 
		SalesRepresentative AS sr ON sr.Id = c.SalesRepresentativeId
	LEFT JOIN
		CustomerContact AS cc ON cc.CustomerLocationId = cl.Id AND cc.[Primary] = 1
	LEFT JOIN 
		ContactType AS ctype ON ctype.Id = cc.ContactTypeId 
	LEFT JOIN
		Prefix AS p ON p.Id = c.PrefixId
	LEFT JOIN
		RefCustomerType AS cut ON cut.CustomerTypeIndex = c.CustomerType
WHERE
	@TenantId <> 0
		AND @UserId <> 0
		AND u.TenantId = @TenantId
		AND (
				(SELECT COUNT(*) FROM [User] WHERE Id = @UserId AND TenantEmployee = 1) > 0
				OR
				(csr.CustomerId IN (SELECT CustomerId FROM UserShipAs WHERE TenantId = @TenantId AND UserId = @UserId
								  UNION
								  SELECT [User].CustomerId FROM [User] WHERE Id = @UserId)))
		AND (@CustomerGroupId = 0 OR (SELECT COUNT(*) FROM CustomerGroupMap WHERE CustomerId = c.Id AND CustomerGroupId = @CustomerGroupId) > 0)
			
--		AND {1}
--ORDER BY 
--	{2}