Declare @TenantId bigint = 4
Declare @UserId bigint = 2
Declare @DateFrom datetime = '2015-08-16'
Declare @DateTo datetime = '2015-12-31'
Declare @CustomerGroupId bigint = 0
Declare @SalesRepNumber nvarchar(50) = 'all'
Declare @CustomerNumber nvarchar(50) = 'all'
Declare @Mode nvarchar(50) = 'all'

Declare @DefaultTxt nvarchar(50) = 'all'

declare @sm int = -1;

SET @Mode = LOWER(REPLACE(@Mode, ' ', ''))
	
IF @Mode = 'lessthantruckload' OR @Mode = 'ltl' BEGIN SET @sm = 0 END
ELSE IF @Mode = 'truckload' or @Mode = 'ftl'BEGIN SET @sm = 1 END
ELSE IF @Mode = 'air' BEGIN	SET @sm = 2 END
ELSE IF @Mode = 'rail'BEGIN	SET @sm = 3 END
ELSE IF @Mode = 'smallpackage' or @Mode = 'sp' BEGIN SET @sm = 4 END
ELSE IF @Mode = 'all' or @Mode = '' BEGIN SET @sm = -1 END
ELSE BEGIN SET @sm = -2 END


IF object_id('tempdb..#dataTab') is not null
		DROP TABLE #dataTab

Create Table #dataTab (
		AccountBucketCode nvarchar(50)
		,Shipments int
		,EstimatedTotalRevenue decimal(18, 4)
		,EstimatedTotalCost decimal(18, 4)
		,EstimatedTotalProfit decimal(18, 4)
		,EstimatedSalesRepComm decimal(18, 4)
		,ActualTotalRevenue decimal(18,4)
		,ActualTotalCost decimal (18,4)
		,ActualTotalProfit decimal (18,4)
		,ActualSalesRepComm decimal (18,4)
	)

insert into #dataTab (AccountBucketCode, Shipments, EstimatedTotalRevenue, EstimatedTotalCost, EstimatedTotalProfit, EstimatedSalesRepComm,ActualTotalRevenue,ActualTotalCost,ActualTotalProfit,ActualSalesRepComm) 
	Select
		ab.Code 'Account Bucket Code'
		,Sum(Case when s.Id is null then 0 else 1 end) 'Shipments'
		,Sum(IsNull(scs.TotalAmountDue,0)) 'Estimated Total Revenue'
		,Sum(IsNull(scs.TotalCost,0)) 'Estimated Total Cost'
		,Sum(Isnull(scs.TotalProfit,0)) 'EstimatedTotalProfit'
		,Sum(IsNull((
					CASE
						WHEN scs.TotalProfit <= 0 THEN 0
						WHEN scs.TotalProfit * s.SalesRepresentativeCommissionPercent/ 100 >= s.SalesRepMaxCommValue THEN s.SalesRepMaxCommValue
						WHEN scs.TotalProfit * s.SalesRepresentativeCommissionPercent/ 100 < s.SalesRepMinCommValue THEN s.SalesRepMinCommValue		
						ELSE scs.TotalProfit * s.SalesRepresentativeCommissionPercent/ 100
						END
					) + (CASE
							WHEN scs.TotalProfit <= 0 THEN 0
							ELSE scs.TotalProfit * s.SalesRepAddlEntityCommPercent/ 100
						END),0)) 'EstimatedSalesRepComm'
		,Sum(IsNull(sar.TotalActualCharges,0) - IsNull(sar.TotalCredit,0) + IsNull(sar.TotalSupplementalCharges,0)) 'Actual Total Revenue'
		,Sum(IsNull(sac.TotalActualCost,0) - IsNull(sac.TotalActualCostCredit, 0)) 'Actual Total Cost'
		,Sum(IsNull(sar.TotalActualCharges,0) + IsNull(sar.TotalSupplementalCharges,0) - IsNull(sar.TotalCredit,0) - IsNull(sac.TotalActualCost,0) + IsNull(sac.TotalActualCostCredit, 0)) 'Actual Total Profit'
		,Sum(IsNull(TotalCommissionPayments.TotalAmountPaidForShipment,0)) 'Actual Sales Rep Comm'
	From 
		Shipment s
		Left Join ShipmentAccountBucket sab on sab.ShipmentId = s.Id and sab.[Primary] = 1
		Left Join AccountBucket ab on sab.AccountBucketId = ab.Id
		Left Join ShipmentChargeStatistics scs on scs.ShipmentId = s.Id
		Left Join Customer c on s.CustomerId = c.Id
		Left Join SalesRepresentative sr on s.SalesRepresentativeId = sr.Id
		Left Join (
			Select
				Sum(CASE WHEN cp.[Type] = 0 THEN cp.AmountPaid WHEN cp.[Type] = 1 THEN cp.AmountPaid * -1 END) 'TotalAmountPaidForShipment',
				cp.ReferenceNumber
			From 
				CommissionPayment cp
			Group By cp.ReferenceNumber
		) as TotalCommissionPayments on TotalCommissionPayments.ReferenceNumber = s.ShipmentNumber
		LEFT JOIN ShipmentActualCosts(@TenantId, '1753-01-01 00:00:00', '9999-12-31 23:59:59') as sac ON sac.Id = s.Id
		LEFT JOIN ShipmentActualRevenues(@TenantId, '1753-01-01 00:00:00', '9999-12-31 23:59:59') as sar ON sar.Id = s.Id
	Where
		1=1
		And 
			s.TenantId = @TenantId
			And	
				(
					(Select COUNT(*) from [User] where id = @UserId and TenantEmployee = 1) > 0
					OR
					(c.Id in (SELECT CustomerId FROM UserShipAs WHERE TenantId = @TenantId and UserId = @UserId
						UNION
						SELECT [User].CustomerId FROM [User] WHERE Id = @UserId))
				)
			
			And (@CustomerGroupId = 0 OR (Select COUNT(*) from CustomerGroupMap WHERE CustomerId = c.Id and CustomerGroupId = @CustomerGroupId) > 0)
			And s.DateCreated Between @DateFrom And @DateTo
			And (@SalesRepNumber = '' Or lower(@SalesRepNumber) = @DefaultTxt Or sr.SalesRepresentativeNumber = @SalesRepNumber)
			And (@CustomerNumber = '' Or lower(@CustomerNumber) = @DefaultTxt Or c.CustomerNumber = @CustomerNumber)
			AND (@sm = -1 OR s.ServiceMode = @sm)
			And s.[Status] <> 4
	Group By
		ab.Code

Select
	--[#Columns#]
	ab.Code
	,Isnull(d.Shipments, 0) 'Shipments'
	,Isnull(d.EstimatedTotalRevenue, 0) 'EstimatedTotalRevenue'
	,Isnull(d.EstimatedTotalCost, 0) 'EstimatedTotalCost'
	,Isnull(d.EstimatedTotalProfit, 0) 'EstimatedTotalProfit'
	,Isnull(d.EstimatedSalesRepComm, 0) 'EstimatedSalesRepComm'
	,Isnull(d.ActualTotalRevenue, 0) 'ActualTotalRevenue'
	,Isnull(d.ActualTotalCost, 0) 'ActualTotalCost'
	,Isnull(d.ActualTotalProfit, 0) 'ActualTotalProfit'
	,Isnull(d.ActualSalesRepComm, 0) 'ActualSalesRepComm'
From
	AccountBucket ab
	Left Join #dataTab d on d.AccountBucketCode = ab.Code
Where
	1 = 1
	--[#Filters#]
--[#OrderBy#]

drop table #dataTab