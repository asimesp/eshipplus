Select
	s.ShipmentNumber
	,(
		Case 
			When s.BillReseller = 1 Then scs.TotalAmountDue - Isnull(rsa.ResellerFreightProfitShare, 0) - Isnull(rsa.ResellerFuelProfitShare, 0) - Isnull(rsa.ResellerAccessorialProfitShare, 0) - Isnull(rsa.ResellerServiceProfitShare, 0)
			Else scs.TotalAmountDue 
		End) 'TotalEstimatedRevenue'
	,Revenue.TotalActualRevenue
	,ReceiptsAndCounts.TotalInvoicePayments
	,ReceiptsAndCounts.TotalCredits
	,scs.TotalCost 'TotalEstimatedCost'
	,IsNull(Cost.TotalActualCost, 0) 'TotalActualCost'
	,IsNull(Cost.TotalIncludedCredits, 0) 'TotalIncludedCredits'
	,ReceiptsAndCounts.CreditInvoicesCount
	,ReceiptsAndCounts.SupplementalInvoicesCount
	,ReceiptsAndCounts.InvoicesCount
	,IsNull(VendorBillCounts.CreditVendorBillsCount, 0) 'CreditVendorBillsCount'
	,IsNull(VendorBillCounts.VendorBillsCount, 0) 'VendorBillsCount'
	,s.DateCreated
	,s.DesiredPickupDate
	,s.ActualPickupDate
	,s.EstimatedDeliveryDate
	,s.ActualDeliveryDate
	,s.BillReseller
	,shol.[Description] 'OriginName'
	,shol.Street1 'OriginStreet1'
	,shol.Street2 'OriginStreet2'
	,shol.City 'OriginCity'
	,shol.[State] 'OriginState'
	,shol.PostalCode 'OriginPostalCode'
	,co.Name 'OriginCountryName'
	,shdl.[Description] 'DestinationName'
	,shol.Street1 'DestinationStreet1'
	,shdl.Street2 'DestinationStreet2'
	,shdl.City 'DestinationCity'
	,shdl.[State] 'DestinationState'
	,shdl.PostalCode 'DestinationPostalCode'
	,cd.Name 'DestinationCountryName'
	,ISNULL(ss.ShipmentStatusText, '') AS 'Status'
	,ISNULL(sm.ServiceModeText, '') AS 'ServiceMode'
	,ab.Code 'PrimaryAccountBucketCode'
	,ab.[Description] 'PrimaryAccountBucketDescription'
	,c.Name 'CustomerName'
	,c.CustomerNumber
	,v.Name 'PrimaryVendorName'
	,v.VendorNumber 'PrimaryVendorNumber'
	,v.Scac 'PrimaryVendorScac'
	,sv.ProNumber
	,Isnull(sr.SalesRepresentativeNumber, '') 'SalesRepresentativeNumber'
	,Isnull(sr.Name,'') 'SalesRepresentativeName'
	,s.SalesRepresentativeCommissionPercent
	,s.SalesRepMinCommValue
	,s.SalesRepMaxCommValue
	,s.SalesRepAddlEntityName
	,s.SalesRepAddlEntityCommPercent
	,Isnull(sr.CompanyName, '') 'SaleRepresentativeCompany'
	,s.SalesRepresentativeCommissionPercent
	,Isnull(p.Code, '') 'PrefixCode'
	,Isnull(p.[Description], '') 'PrefixDescription'
	,u.FirstName 'UserFirstName'
	,u.LastName 'UserLastName'
	,u.Username
	,(SELECT COUNT(*) FROM ShipmentVendor shvc WHERE shvc.ShipmentId = s.id ) as 'Vendor Count'
	,ISNULL((SELECT (dt.Code + ', ')
				FROM ShipmentDocument sd, DocumentTag dt
				WHERE sd.ShipmentId = s.Id AND sd.DocumentTagId = dt.Id 
				ORDER BY sd.Id Asc
				FOR XML PATH('')),'') AS 'DocumentTags'
	,ISNULL((SELECT (sn.[Message] + ', ')
				FROM ShipmentNote sn
				WHERE sn.ShipmentId = s.Id AND sn.[Type] = 2 
				ORDER BY sn.Id Asc
				FOR XML PATH('')),'') 'FinanceNotes'
	,c.AuditInstructions
	,c.InvoiceRequiresCarrierProNumber
	,c.InvoiceRequiresControlAccount
	,c.InvoiceRequiresShipperReference
	,s.CreatedInError
	,c.CustomField1
	,c.CustomField2
	,c.CustomField3
	,c.CustomField4
From
	Shipment s
	Inner Join ShipmentChargeStatistics scs on scs.ShipmentId = s.Id
	Inner Join Customer c on s.CustomerId = c.Id
	Inner Join ShipmentAccountBucket sab on sab.ShipmentId = s.Id and sab.[Primary] = 1
	Inner Join AccountBucket ab on sab.AccountBucketId = ab.Id
	Inner Join ShipmentLocation shol ON shol.Id = s.OriginId
	Inner Join Country co ON co.Id = shol.CountryId
	Inner Join ShipmentLocation shdl ON shdl.Id = s.DestinationId
	Inner Join Country cd ON cd.Id = shdl.CountryId
	Inner Join ShipmentVendor sv on sv.ShipmentId = s.Id and sv.[Primary] = 1
	Inner Join Vendor v on sv.VendorId = v.Id
	Inner Join [User] u on s.UserId = u.Id
	Inner Join
		(
			Select
				s.ShipmentNumber
				,Sum((Case When i.InvoiceType = 2 Then -1 Else 1 End)*(id.UnitSell-id.UnitDiscount)*id.Quantity) 'TotalActualRevenue'
			From
				Shipment s
				Inner Join InvoiceDetail id on id.ReferenceNumber = s.ShipmentNumber And id.ReferenceType = 0 -- shipment
				Inner Join Invoice i on id.invoiceId = i.Id
			Where 
				i.Posted = 1
			Group by
				s.ShipmentNumber
		) As Revenue
		On Revenue.ShipmentNumber = s.ShipmentNumber
	Inner Join
		(
			Select
				q1.ShipmentNumber
				,Sum(q1.PaidAmount) 'TotalInvoicePayments'
				,Sum(q1.TotalCredits) 'TotalCredits'
				,Sum((Case When q1.InvoiceType = 2 Then 1 Else 0 End)) 'CreditInvoicesCount'
				,Sum((Case When q1.InvoiceType = 1 Then 1 Else 0 End)) 'SupplementalInvoicesCount'
				,Sum((Case When q1.InvoiceType = 0 Then 1 Else 0 End)) 'InvoicesCount'
			From
				(
					Select distinct
						s.ShipmentNumber
						,i.InvoiceNumber
						,i.PaidAmount*(Case When q11.InvoiceTotalAmountDue = 0 Then 1 Else Sum((Case When i.InvoiceType = 2 Then -1 Else 1 End)*(id.UnitSell-id.UnitDiscount)*id.Quantity)/q11.InvoiceTotalAmountDue End) 'PaidAmount'
						,i.InvoiceType
						,Sum(Case When i.InvoiceType = 2 Then (id.UnitSell-id.UnitDiscount)*id.Quantity Else 0 End) 'TotalCredits'
					From
						Shipment s
						Inner Join InvoiceDetail id on id.ReferenceNumber = s.ShipmentNumber And id.ReferenceType = 0 -- shipment
						Inner Join Invoice i on id.invoiceId = i.Id
						Inner Join 
							(
								Select distinct
									q1i.InvoiceNumber
									,Sum((Case When q1i.InvoiceType = 2 Then -1 Else 1 End)*(q1d.UnitSell-q1d.UnitDiscount)*q1d.Quantity) 'InvoiceTotalAmountDue'
									From
										Invoice q1i
										Inner Join InvoiceDetail q1d on q1d.InvoiceId = q1i.Id
									Group By
										q1i.InvoiceNumber
							) as q11
							On q11.InvoiceNumber = i.InvoiceNumber
					Where 
						i.Posted = 1
					Group by 
						s.ShipmentNumber, i.InvoiceNumber, i.PaidAmount, i.InvoiceType, q11.InvoiceTotalAmountDue
				) As q1
			Group By q1.ShipmentNumber
		) As ReceiptsAndCounts
		On ReceiptsAndCounts.ShipmentNumber = s.ShipmentNumber
	Left Join Prefix p on s.PrefixId = p.Id
	Left Join SalesRepresentative sr on s.SalesRepresentativeId = sr.Id
	Left Join ResellerShipmentAddition rsa on rsa.ShipmentId = s.Id
	Left Join
		(		
			Select
				s.ShipmentNumber
				,Sum((Case When b.BillType = 2 Then -1 Else 1 End)*bd.UnitBuy*bd.Quantity) 'TotalActualCost'
				,Sum((Case When b.BillType = 2 Then bd.UnitBuy*bd.Quantity Else 0 End)) 'TotalIncludedCredits'
			From
				Shipment s
				Inner Join VendorBillDetail bd On bd.ReferenceNumber = s.ShipmentNumber And bd.ReferenceType = 0 -- shipment
				Inner Join VendorBill b On bd.VendorbillId = b.Id
			Where 
				b.Posted = 1
			Group By
				s.ShipmentNumber
		) As Cost
		On Cost.ShipmentNumber = s.ShipmentNumber
	Left Join
		(
			Select
				q2.ShipmentNumber
				,Sum((Case When q2.BillType = 2 Then 1 Else 0 End)) 'CreditVendorBillsCount'
				,Sum((Case When q2.BillType = 0 Then 1 Else 0 End)) 'VendorBillsCount'
			From
				(Select distinct
					s.ShipmentNumber
					,b.DocumentNumber
					,b.VendorLocationId
					,b.BillType
				From
					Shipment s
					Inner Join VendorBillDetail bd On bd.ReferenceNumber = s.ShipmentNumber And bd.ReferenceType = 0 -- shipment
					Inner Join VendorBill b On bd.VendorbillId = b.Id
				Where 
					b.Posted = 1) As q2
			Group By q2.ShipmentNumber
		) As VendorBillCounts
		On VendorBillCounts.ShipmentNumber = s.ShipmentNumber
	Left Join RefShipmentStatus ss ON ss.ShipmentStatusIndex = s.[Status]
	Left Join RefServiceMode sm ON sm.ServiceModeIndex = s.ServiceMode
Where
	s.DateCreated >= '2014-07-01'

	

	
	