DECLARE @TenantId BIGINT = 4
DECLARE @UserId BIGINT = 4

SELECT
	c.CustomerNumber,
	c.Name,
	(CASE WHEN c.Active = 0 THEN 'No' ELSE 'Yes' END) AS 'CustomerIsActive', 
	(SELECT COUNT(li.Id) FROM LibraryItem AS li WHERE li.CustomerId = c.Id) AS 'CountOfCustomerLibraryItems'
FROM
	Customer AS c
WHERE
	@TenantId <> 0
	AND @UserId <> 0
	AND @TenantId = c.TenantId
--		AND {1}
--ORDER BY 
--	{2}