Declare @TenantId bigint = 4
Declare @UserId bigint = 2764
Declare @CustomerGroupId bigint = 0
Declare @VendorGroupId bigint = 0

Select
	s.ShipmentNumber
	,v.Name 'VendorName'
	,v.VendorNumber 
	,sv.[Primary] 'ShipmentVendorIsPrimary'
	,s.DateCreated 'ShipmentDateCreated'
	,(CASE WHEN s.DateCreated = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(10),s.DateCreated,121) END) 'ShipmentDateCreatedText'
	,ISNULL(sm.ServiceModeText,'') AS 'ShipmentServiceMode'
	,ISNULL(ss.ShipmentStatusText, '') AS 'ShipmentStatus'
	,ab.Code 'ShipmentPrimaryAccountBucketCode'
	,ab.[Description] 'ShipmentPrimaryAccountBucketDescription'
	,c.Name 'ShipmentCustomerName'
	,c.CustomerNumber 'ShipmentCustomerNumber'
	,v.Active 'VendorIsActive'
	,v.DateCreated 'VendorDateCreated'
	,(CASE WHEN v.DateCreated = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(10),v.DateCreated,121) END) 'VendorDateCreatedText'
	,v.IsAgent 'VendorIsAgent'
	,v.IsBroker 'VendorIsBroker'
	,v.IsCarrier 'VendorIsCarrier'
	,Isnull(u.Username,'') 'VendorServiceRepUsername'
	,Isnull(u.FirstName,'') 'VendorServiceRepUserFirstName'
	,Isnull(u.LastName,'') 'VendorServiceRepUserLsatName'
From Shipment s
	Inner Join ShipmentVendor sv on sv.ShipmentId = s.Id
	Inner Join Vendor v on sv.VendorId = v.Id
	Inner Join ShipmentAccountBucket shab ON s.Id = shab.ShipmentId AND shab.[Primary] = 1
	Inner Join AccountBucket ab ON shab.AccountBucketId = ab.Id 
	Inner Join Customer c on s.CustomerId = c.Id

	Left Join [User] u on u.Id = v.VendorServiceRepUserId
	Left Join RefServiceMode sm on sm.ServiceModeIndex = s.ServiceMode
	Left Join RefShipmentStatus ss on ss.ShipmentStatusIndex = s.[Status]

Where
	@TenantId <> 0 
    AND @UserId <> 0
    AND s.TenantId = @TenantId
    AND (
			(Select COUNT(*) from [User] where id = @UserId and TenantEmployee = 1) > 0
			OR
			(
				s.CustomerId in (SELECT CustomerId FROM UserShipAs WHERE TenantId = @TenantId and UserId = @UserId
					UNION
					SELECT [User].CustomerId FROM [User] WHERE Id = @UserId)
			)
		)
		AND (@CustomerGroupId = 0 OR (Select COUNT(*) from CustomerGroupMap WHERE CustomerId = s.CustomerId and CustomerGroupId = @CustomerGroupId) > 0)
		AND (@VendorGroupId = 0 OR (Select COUNT(*) from VendorGroupMap WHERE VendorId = sv.VendorId and VendorGroupId = @VendorGroupId) > 0)
AND 1=1