SELECT
	st.ServiceTicketNumber,
	stc.Comment as 'Charge Comment',
	cc.Code as 'Charge Code',
	cc.[Description] as 'Charge Description',
	stc.UnitBuy,
	stc.UnitDiscount,
	stc.UnitSell,
	stc.Quantity
FROM
	ServiceTicket st
	INNER JOIN ServiceTicketCharge stc ON stc.ServiceTicketId = st.Id
	INNER JOIN ChargeCode cc ON stc.ChargeCodeId = cc.Id
WHERE
	stc.Comment LIKE '%\_%' ESCAPE '\'