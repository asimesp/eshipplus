SELECT 
	COUNT(*)
FROM 
	Shipment INNER JOIN ShipmentCharge ON ShipmentCharge.ShipmentId = Shipment.Id

declare @TenantId bigint = 4
declare @UserId bigint = 2
declare @CustomerGroupId bigint = 0
declare @VendorGroupId bigint = 0

SELECT Top 1000
	sh.ShipmentNumber
	,uscs.AccessorialCharges + uscs.FreightCharges + uscs.FuelCharges + uscs.ServiceCharges 'Total Amount Due'
	,uscs.AccessorialChargesRessellerReduced + uscs.FreightChargesRessellerReduced + uscs.FuelChargesRessellerReduced + uscs.ServiceChargesRessellerReduced 'Total Reseller Deducted'

FROM
	Shipment AS sh
	INNER JOIN ShipmentVendor AS shv ON shv.ShipmentId = sh.Id 
	Left Join (
	Select 
			shc.ShipmentId
			,SUM(CASE WHEN cc.Category = 0 THEN (shc.UnitSell - shc.UnitDiscount) * shc.Quantity ELSE 0 END) AS FreightCharges 
            ,SUM(CASE WHEN cc.Category = 1 THEN (shc.UnitSell - shc.UnitDiscount) * shc.Quantity ELSE 0 END) AS FuelCharges 
            ,SUM(CASE WHEN cc.Category = 2 THEN (shc.UnitSell - shc.UnitDiscount) * shc.Quantity ELSE 0 END) AS AccessorialCharges
            ,SUM(CASE WHEN cc.Category = 3 THEN (shc.UnitSell - shc.UnitDiscount) * shc.Quantity ELSE 0 END) AS ServiceCharges
			,SUM(CASE WHEN cc.Category = 0 THEN (shc.UnitSell-(CASE 
					WHEN ra.Id IS NULL OR shc.UnitSell = 0 
							THEN 0
						WHEN ra.Id IS NOT NULL AND ra.LineHaulType = 0	--value
							THEN ra.LineHaulValue
						WHEN ra.Id IS NOT NULL AND ra.LineHaulType = 1    --percentage
							THEN shc.UnitSell * ra.FreightPercentageValue
						WHEN ra.Id IS NOT NULL AND ra.LineHaulType = 2    --both
							THEN CASE WHEN ra.UseLineHaulMinimum = 1
							THEN CASE WHEN ra.LineHaulValue < shc.UnitSell * ra.FreightPercentageValue
								THEN ra.LineHaulValue
								ELSE shc.UnitSell * ra.FreightPercentageValue END
							ELSE CASE WHEN ra.LineHaulValue > shc.UnitSell * ra.FreightPercentageValue
								THEN ra.LineHaulValue
								ELSE shc.UnitSell * ra.FreightPercentageValue END 
							END
						ELSE 0
					END) - shc.UnitDiscount)*shc.Quantity ELSE 0 END) AS FreightChargesRessellerReduced 
			,SUM(CASE WHEN cc.Category = 1 THEN  (shc.UnitSell-(CASE 
					WHEN ra.Id IS NULL OR shc.UnitSell = 0 
							THEN 0
						WHEN ra.Id IS NOT NULL AND ra.FuelType = 0	--value
							THEN ra.FuelValue
						WHEN ra.Id IS NOT NULL AND ra.FuelType = 1    --percentage
							THEN shc.UnitSell * ra.FuelPercentageValue
						WHEN ra.Id IS NOT NULL AND ra.FuelType = 2    --both
							THEN CASE WHEN ra.UseFuelMinimum = 1
							THEN CASE WHEN ra.FuelValue < shc.UnitSell * ra.FuelPercentageValue
								THEN ra.FuelValue
								ELSE shc.UnitSell * ra.FuelPercentageValue END
							ELSE CASE WHEN ra.FuelValue > shc.UnitSell * ra.FuelPercentageValue
								THEN ra.FuelValue
								ELSE shc.UnitSell * ra.FuelPercentageValue END 
							END
						ELSE 0
					END) - shc.UnitDiscount)*shc.Quantity ELSE 0 END) AS FuelChargesRessellerReduced 
			,SUM(CASE WHEN cc.Category = 2 THEN  (shc.UnitSell-(CASE 
					WHEN ra.Id IS NULL OR shc.UnitSell = 0 
							THEN 0
						WHEN ra.Id IS NOT NULL AND ra.AccessorialType = 0	--value
							THEN ra.AccessorialValue
						WHEN ra.Id IS NOT NULL AND ra.AccessorialType = 1    --percentage
							THEN shc.UnitSell * ra.AccessorialPercentageValue
						WHEN ra.Id IS NOT NULL AND ra.AccessorialType = 2    --both
							THEN CASE WHEN ra.UseAccessorialMinimum = 1
							THEN CASE WHEN ra.AccessorialValue < shc.UnitSell * ra.AccessorialPercentageValue
								THEN ra.AccessorialValue
								ELSE shc.UnitSell * ra.AccessorialPercentageValue END
							ELSE CASE WHEN ra.AccessorialValue > shc.UnitSell * ra.AccessorialPercentageValue
								THEN ra.AccessorialValue
								ELSE shc.UnitSell * ra.AccessorialPercentageValue END 
							END
						ELSE 0
					END) - shc.UnitDiscount)*shc.Quantity ELSE 0 END) AS AccessorialChargesRessellerReduced  
			,SUM(CASE WHEN cc.Category = 3 THEN  (shc.UnitSell-(CASE 
					WHEN ra.Id IS NULL OR shc.UnitSell <= 0 
							THEN 0
						WHEN ra.Id IS NOT NULL AND ra.ServiceType = 0	--value
							THEN ra.ServiceValue
						WHEN ra.Id IS NOT NULL AND ra.ServiceType = 1    --percentage
							THEN shc.UnitSell * ra.ServicePercentageValue
						WHEN ra.Id IS NOT NULL AND ra.ServiceType = 2    --both
							THEN CASE WHEN ra.UseServiceMinimum = 1
							THEN CASE WHEN ra.ServiceValue < shc.UnitSell * ra.ServicePercentageValue
								THEN ra.ServiceValue
								ELSE shc.UnitSell * ra.ServicePercentageValue END
							ELSE CASE WHEN ra.ServiceValue > shc.UnitSell * ra.ServicePercentageValue
								THEN ra.ServiceValue
								ELSE shc.UnitSell * ra.ServicePercentageValue END 
							END
						ELSE 0
					END) - shc.UnitDiscount)*shc.Quantity ELSE 0 END) AS ServiceChargesRessellerReduced  
			FROM dbo.ShipmentCharge AS shc 
			INNER JOIN dbo.ChargeCode AS cc ON shc.ChargeCodeId = cc.Id
			INNER JOIN shipment sh on shc.shipmentId = sh.id
			Left Join (
				Select 
				r.UseServiceMinimum
				,r.UseAccessorialMinimum
				,r.UseFuelMinimum
				,r.UseLineHaulMinimum
				,r.AccessorialValue
				,r.FuelValue
				,r.LineHaulValue
				,r.ServiceValue
				,r.id
				,r.AccessorialType
				,r.FuelType
				,r.LineHaulType
				,r.ServiceType
				,r.ServicePercentage / (100 + r.ServicePercentage) as ServicePercentageValue
				,r.AccessorialPercentage / (100 + r.AccessorialPercentage) as AccessorialPercentageValue
				,r.FuelPercentage / (100 + r.FuelPercentage) as FuelPercentageValue
				,r.LineHaulPercentage / (100 + r.LineHaulPercentage) as FreightPercentageValue
				From ResellerAddition r
			) ra on ra.Id = sh.ResellerAdditionId
			GROUP BY shc.ShipmentId
	
	) as uscs on uscs.ShipmentId = sh.Id
WHERE
	@TenantId <> 0 
              AND @UserId <> 0
              AND sh.TenantId = @TenantId
              AND (
				(Select COUNT(*) from [User] where id = @UserId and TenantEmployee = 1) > 0
				OR
				(sh.CustomerId in (SELECT CustomerId FROM UserShipAs WHERE TenantId = @TenantId and UserId = @UserId
					UNION
					SELECT [User].CustomerId FROM [User] WHERE Id = @UserId)))
			  AND (@CustomerGroupId = 0 OR (Select COUNT(*) from CustomerGroupMap WHERE CustomerId = sh.CustomerId and CustomerGroupId = @CustomerGroupId) > 0)
			  AND (@VendorGroupId = 0 OR (Select COUNT(*) from VendorGroupMap WHERE VendorId = shv.VendorId and VendorGroupId = @VendorGroupId) > 0)
