
SELECT COUNT(*) FROM LTLSellRate

SELECT
	c.Name,
	v.VendorNumber,
	v.Name AS 'VendorName',
	v.Scac AS 'VendorSCAC',
	v.DOT AS 'VendorDOT',
	v.MC AS 'VendorMC',
	(CASE WHEN v.Active = 0 THEN 'No' ELSE 'Yes' END) AS 'VendorIsActive',
	(CASE WHEN v.CTPATCertified = 0 THEN 'No' ELSE 'Yes' END) AS 'VendorIsCTPATCertified',
	(CASE WHEN v.PIPCertified = 0 THEN 'No' ELSE 'Yes' END) AS 'VendorIsPIPCertified',
	(CASE WHEN v.SmartWayCertified = 0 THEN 'No' ELSE 'Yes' END) AS 'VendorSmartWayCertified',
	(CASE WHEN v.TSACertified = 0 THEN 'No' ELSE 'Yes' END) AS 'VendorTSACertified',
	(CASE WHEN v.IsAgent = 0 THEN 'No' ELSE 'Yes' END) AS 'VendorIsAgent',
	(CASE WHEN v.IsBroker = 0 THEN 'No' ELSE 'Yes' END) AS 'VendorIsBroker',
	(CASE WHEN v.IsCarrier = 0 THEN 'No' ELSE 'Yes' END) AS 'VendorIsCarrier',
	(CASE WHEN v.HandlesLessThanTruckLoad = 1 THEN 'Yes' ELSE 'False'END) AS 'VendorHandlesLessThanTruckLoad',
	(CASE WHEN v.HandlesTruckLoad = 0 THEN 'No' ELSE 'Yes'END) AS 'VendorHandlesTruckLoad',
	(CASE WHEN v.HandlesAir = 0 THEN 'No' ELSE 'Yes'END) AS 'VendorHandlesAir',
	(CASE WHEN v.HandlesPartialTruckload = 0 THEN 'Yes' ELSE 'False'END) AS 'VendorHandlesPartialTruckload',
	(CASE WHEN v.HandlesRail = 0 THEN 'No' ELSE 'Yes'END) AS 'VendorHandlesRail',
	(CASE WHEN v.HandlesSmallPack= 0 THEN 'No' ELSE 'Yes'END) AS 'VendorHandlesSmallPack',
	v.PIPNumber AS 'VendorPIPNumber',
	v.CTPATNumber AS 'VendorCTPATNumber',
	v.LastAuditDate AS 'VendorLastAuditDate',
	(CASE WHEN v.LastAuditDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),v.LastAuditDate,101) END) AS 'ViewVendorLastAuditDate',
	(CASE WHEN v.ExcludeFromAvailableLoadsBlast = 1 THEN 'Yes' ELSE 'No' END) AS 'VendorExcludeFromAvailableLoadsBlast',
	v. Notes AS 'VendorNotes',
	v.Notation AS 'VendorNotation',
	vr.Name AS 'VendorRatingName',
	(CASE WHEN vr.Active = 0 THEN 'No' ELSE 'Yes' END) AS 'VendorRatingIsActive',
	vr.CurrentLTLFuelMarkup AS 'VendorRatingCurrentLTLFuelMarkup',
	vr.DisplayName AS 'VendorRatingDisplayName',
	vr.FuelMarkupCeiling AS 'VendorRatingFuelMarkupCeiling',
	vr.FuelMarkupFloor AS 'VendorRatingFuelMarkupFloor',
	(CASE WHEN vr.HasAdditionalCharges = 0 THEN 'No' ELSE 'Yes' END) AS 'VendorRatingHasAdditionalCharges',
	(CASE WHEN vr.HasLTLCubicFootCapacityRules = 0 THEN 'No' ELSE 'Yes' END) AS 'VendorRatingHasLTLCubicFootCapacityRules',
	(CASE WHEN vr.HasLTLOverLengthRules = 0 THEN 'No' ELSE 'Yes' END) AS 'VendorRatingHasLTLOverLengthRules',
	vr.LTLMinPickupWeight AS 'VendorRatingLTLMinPickupWeight',
	vr.LTLTariff AS 'VendorRatingLTLTruckHeight',
	vr.LTLTruckHeight AS 'VendorRatingLTLTruckHeight',
	vr.LTLTruckLength AS 'VendorRatingLTLTruckLength',
	vr.LTLTruckUnitWeight AS 'VendorRatingLTLTruckUnitWeight',
	vr.LTLTruckWeight AS 'VendorRatingLTLTruckWeight',
	ltlsr.Id AS 'LTLSellRateId', -- used only for record Id, not on actual template
	(CASE WHEN ltlsr.Active = 0 THEN 'No' ELSE 'Yes' END) AS 'LTLSellRateIsActive',
	ltlsr.EffectiveDate AS 'LTLEffectiveDate',
	(CASE WHEN 	ltlsr.EffectiveDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),	ltlsr.EffectiveDate,101) END) AS 'ViewLTLSellRateEffectiveDate',
	(CASE WHEN ltlsr.LTLIndirectPointEnabled = 0 THEN 'No' ELSE 'Yes' END) AS 'LTLSellRateDirectPointEnabled',
	ltlsr.MarkupPercent AS 'LTLSellRateMarkupPercent',
	ltlsr.MarkupValue AS 'LTLSellRateMarkupValue',
	(CASE WHEN ltlsr.UseMinimum = 0 THEN 'No' ELSE 'Yes' END) AS 'LTLSellRateUseMinimum',
	(CASE 
		WHEN ltlsr.StartOverrideWeightBreak = 0 THEN 'L5C'
		WHEN ltlsr.StartOverrideWeightBreak = 1 THEN 'M5C'
		WHEN ltlsr.StartOverrideWeightBreak = 2 THEN 'M1M'
		WHEN ltlsr.StartOverrideWeightBreak = 3 THEN 'M2M'
		WHEN ltlsr.StartOverrideWeightBreak = 4 THEN 'M5M'
		WHEN ltlsr.StartOverrideWeightBreak = 5 THEN 'M10M'
		WHEN ltlsr.StartOverrideWeightBreak = 6 THEN 'M20M'
		WHEN ltlsr.StartOverrideWeightBreak = 7 THEN 'M30M'
		WHEN ltlsr.StartOverrideWeightBreak = 8 THEN 'M40M'
	END) as 'LTLSellRateStartOverrideWeightBreak',
	ltlsr.OverrideMarkupPercentL5C as 'LTLSellRateOverrideMarkupPercentL5C',
	ltlsr.OverrideMarkupValueL5C as 'LTLSellRateOverrideMarkupValueL5C',
	ltlsr.OverrideMarkupPercentM5C as 'LTLSellRateOverrideMarkupPercentM5C',
	ltlsr.OverrideMarkupValueM5C as 'LTLSellRateOverrideMarkupValueM5C',
	ltlsr.OverrideMarkupPercentM1M as 'LTLSellRateOverrideMarkupPercentM1M',
	ltlsr.OverrideMarkupValueM1M as 'LTLSellRateOverrideMarkupValueM1M',
	ltlsr.OverrideMarkupPercentM2M as 'LTLSellRateOverrideMarkupPercentM2M',
	ltlsr.OverrideMarkupValueM2M as 'LTLSellRateOverrideMarkupPercentM2M',
	ltlsr.OverrideMarkupPercentM5M as 'LTLSellRateOverrideMarkupPercentM5M',
	ltlsr.OverrideMarkupValueM5M as 'LTLSellRateOverrideMarkupValueM5M',
	ltlsr.OverrideMarkupPercentM10M as 'LTLSellRateOverrideMarkupPercentM10M',
	ltlsr.OverrideMarkupValueM10M as 'LTLSellRateOverrideMarkupValueM10M',
	ltlsr.OverrideMarkupPercentM20M as 'LTLSellRateOverrideMarkupPercentM20M',
	ltlsr.OverrideMarkupValueM20M as 'LTLSellRateOverrideMarkupValueM20M',
	ltlsr.OverrideMarkupPercentM30M as 'LTLSellRateOverrideMarkupPercentM30M',
	ltlsr.OverrideMarkupValueM30M as 'LTLSellRateOverrideMarkupValueM30M',
	ltlsr.OverrideMarkupPercentM40M as 'LTLSellRateOverrideMarkupPercentM40M',
	ltlsr.OverrideMarkupValueM40M as 'LTLSellRateOverrideMarkupValueM40M',
	(CASE WHEN ltlsr.OverrideMarkupVendorFloorEnabled = 0 THEN 'No' ELSE 'Yes' END) AS 'LTLSellRateOverrideMarkupVendorFloorEnabled',
	ltlsr.OverrideMarkupValueVendorFloor AS 'LTLSellRateOverrideMarkupValueVendorFloor',
	ltlsr.OverrideMarkupPercentVendorFloor AS 'LTLSellRateOverrideMarkupPercentVendorFloor',
	cr.Id AS 'CustomerRatingId', -- not  used in configuration, used only as record id
	
	--Customer Rating LineHaul Ceiling
	 cr.LineHaulProfitCeilingValue AS 'CustomerRatingLineHaulProfitCeilingValue',			
	 cr.LineHaulProfitCeilingPercentage AS 'CustomerRatingLineHaulProfitCeilingPercentage',		
	 cr.UseLineHaulMinimumCeiling AS 'CustomerRatingUseLineHaulProfitCeiling',
	 (CASE WHEN cr.NoLineHaulProfit = 0 THEN 'No' ELSE 'Yes' END) AS 'CustomerRatingNoLineHaulProfit',
	 
	 --Customer Rating Fuel Ceiling
	 cr.FuelProfitCeilingValue	AS 'CustomerRatingFuelProfitCeilingValue',				
	 cr.FuelProfitCeilingPercentage AS 'CustomerRatingFuelProfitCeilingPercentage',
	 cr.UseFuelMinimumCeiling AS 'CustomerRatingUseFuelProfitCeiling',
	 (CASE WHEN cr.NoFuelProfit = 0 THEN 'No' ELSE 'Yes' END) AS 'CustomerRatingNoFuelProfit',
	 
	  --Customer Rating Accessorial Ceiling
	 cr.AccessorialProfitCeilingValue AS 'CustomerRatingAccessorialProfitCeilingValue',		
	 cr.AccessorialProfitCeilingPercentage AS 'CustomerRatingAccessorialProfitCeilingPercentage',
	 cr.UseAccessorialMinimumCeiling AS 'CustomerRatingUseAccessorialProfitCeiling',
	 (CASE WHEN cr.NoAccessorialProfit = 0 THEN 'No' ELSE 'Yes' END) as 'CustomerRatingNoAccessorialProfit',
	 
	  --Customer Rating Service Ceiling
	 cr.ServiceProfitCeilingValue AS 'CustomerRatingServiceProfitCeilingValue',			
	 cr.ServiceProfitCeilingPercentage AS 'CustomerRatingServiceProfitCeilingPercentage',
	 cr.UseServiceMinimumCeiling AS 'CustomerRatingUseServiceProfitCeiling',
	 (CASE WHEN cr.NoServiceProfit = 0 THEN 'No' ELSE 'Yes' END) AS 'CustomerRatingNoServiceProfit',
	
	 --Customer Rating LineHaul Floor
	 cr.LineHaulProfitFloorValue AS 'CustomerRatingLineHaulProfitFloorValue',			
	 cr.LineHaulProfitFloorPercentage AS 'CustomerRatingLineHaulProfitFloorPercentage',		
	 (CASE WHEN cr.UseLineHaulMinimumFloor = 0 THEN 'No' ELSE 'Yes' END)  AS 'CustomerRatingUseLineHaulProfitFloor',
	 
	 --Customer Rating Fuel Floor
	 cr.FuelProfitFloorValue AS 'CustomerRatingFuelProfitFloorValue',				
	 cr.FuelProfitFloorPercentage AS 'CustomerRatingFuelProfitFloorPercentage',
	 (CASE WHEN cr.UseFuelMinimumFloor = 0 THEN 'No' ELSE 'Yes' END) AS 'CustomerRatingUseFuelProfitFloor',
	 
	  --Customer Rating Accessorial Floor
	 cr.AccessorialProfitFloorValue AS 'CustomerRatingAccessorialProfitFloorValue',		
	 cr.AccessorialProfitFloorPercentage AS 'CustomerRatingAccessorialProfitFloorPercentage',
	 (CASE WHEN cr.UseAccessorialMinimumFloor = 0 THEN 'No' ELSE 'Yes' END)	AS 'CustomerRatingUseAccessorialProfitFloor',
	 
	  --Customer Rating Service Profits
	 cr.ServiceProfitFloorValue AS 'CustomerRatingServiceProfitFloorValue',			
	 cr.ServiceProfitFloorPercentage AS 'CustomerRatingServiceProfitFloorPercentage',
	 (CASE WHEN cr.UseServiceMinimumFloor = 0 THEN 'No' ELSE 'Yes' END) AS 'CustomerRatingUseServiceProfitFloor',
	 
	
	ISNULL(lhpct.ValuePercentageTypeText, '') AS 'CustomerRatingLineHaulProfitCeilingType',
    
    ISNULL(fpct.ValuePercentageTypeText, '') AS 'CustomerRatingFuelProfitCeilingType',
    
    ISNULL(apct.ValuePercentageTypeText, '') AS 'CustomerRatingAccessorialProfitCeilingType',
    
    ISNULL(spct.ValuePercentageTypeText, '') AS 'CustomerRatingServiceProfitCeilingType',
    
    ISNULL(lhpft.ValuePercentageTypeText, '') AS 'CustomerRatingLineHaulProfitFloorType',
    
    ISNULL(fpft.ValuePercentageTypeText, '') AS 'CustomerRatingFuelProfitFloorType',
    
    ISNULL(apft.ValuePercentageTypeText, '') AS 'CustomerRatingAccessorialProfitFloorType',
    
    ISNULL(spft.ValuePercentageTypeText, '') AS 'CustomerRatingServiceProfitFloorType',
    
    c.CustomerNumber,
	c.Name AS 'CustomerName',
	ISNULL(ct.CustomerTypeText, '') AS 'Customer Type',
	(CASE WHEN c.Active = 0 THEN 'False' ELSE  'True' END) AS 'CustomerIsActive',
	c.InvoiceTerms AS 'CustomerInvoiceTerms',
	c.CreditLimit AS 'CustomerCreditLimit',
	c.AuditInstructions AS 'CustomerAuditInstructions',
	ISNULL(sr.SalesRepresentativeNumber,'') 'Sales Rep Number',
	ISNULL(sr.Name,'') 'Sales Rep Name'
    
FROM Vendor AS v
	INNER JOIN VendorRating AS vr ON vr.VendorId = v.Id
	INNER JOIN LTLSellRate AS ltlsr ON ltlsr.VendorRatingId = vr.Id
	INNER JOIN CustomerRating AS cr ON ltlsr.CustomerRatingId = cr.Id
	INNER JOIN Customer AS c ON cr.CustomerId = c.Id
	LEFT JOIN SalesRepresentative sr ON c.SalesRepresentativeId = sr.Id
	LEFT JOIN RefValuePercentageType lhpct ON lhpct.ValuePercentageTypeIndex = cr.LineHaulProfitCeilingType
	LEFT JOIN RefValuePercentageType fpct ON fpct.ValuePercentageTypeIndex = cr.FuelProfitCeilingType
	LEFT JOIN RefValuePercentageType apct ON apct.ValuePercentageTypeIndex = cr.AccessorialProfitCeilingType
	LEFT JOIN RefValuePercentageType spct ON spct.ValuePercentageTypeIndex = cr.ServiceProfitCeilingType
	LEFT JOIN RefValuePercentageType lhpft ON lhpft.ValuePercentageTypeIndex = cr.LineHaulProfitFloorType
	LEFT JOIN RefValuePercentageType fpft ON fpft.ValuePercentageTypeIndex = cr.FuelProfitFloorType
	LEFT JOIN RefValuePercentageType apft ON apft.ValuePercentageTypeIndex = cr.AccessorialProfitFloorType
	LEFT JOIN RefValuePercentageType spft ON spft.ValuePercentageTypeIndex = cr.ServiceProfitFloorType
	LEFT JOIN RefCustomerType ct on ct.CustomerTypeIndex = c.CustomerType
	order by c.Name asc