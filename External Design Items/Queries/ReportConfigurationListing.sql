DECLARE @TenantId bigint = 4;
DECLARE @UserId bigint = 3;

SELECT COUNT(*)FROM ReportConfiguration

SELECT 
	Isnull(rt.Name, '') AS 'ReportTemplateName',
	(SELECT COUNT(rs.Id) FROM ReportSchedule rs WHERE rs.ReportConfigurationId = r.Id),
	r.Name AS 'ReportConfigurationName',
	r.[Description],
	r.QlikConfiguration,
	r.LastRun,
	ISNULL(rcv.ReportConfigurationVisibilityText, '') AS 'Visibility',
	u.Username,
	ISNULL((SELECT (cu.Username + ', ')
	 FROM ReportCustomizationUser c
		INNER JOIN [User] cu ON cu.Id = c.UserId
	 WHERE c.ReportConfigurationId = r.Id
	 ORDER BY cu.Username ASC
	 FOR XML PATH('')),'')
FROM 
	ReportConfiguration r
		INNER JOIN [User] u ON u.Id = r.UserId
		LEFT JOIN ReportTemplate AS rt ON rt.Id = r.ReportTemplateId
		LEFT JOIN RefReportConfigurationVisibility AS rcv ON rcv.ReportConfigurationVisibilityIndex = r.Visibility
WHERE 
	@TenantId <> 0
		AND @UserId <> 0
		AND r.TenantId = @TenantId
