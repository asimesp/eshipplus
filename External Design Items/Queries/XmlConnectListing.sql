DECLARE @TenantId bigint = 4
DECLARE @UserId bigint = 4

SELECT 
	x.ShipmentIdNumber 'Id Number'
	, x.DateCreated
	,(CASE 
		WHEN x.DateCreated = '1753-01-01 00:00:00.000' THEN '' 
		ELSE CONVERT(nvarchar(20),x.DateCreated,101) 
	  END) 'View Date Created'
	, x.ControlNumber
	,ISNULL(edt.EdiDocumentTypeText, '') AS 'Document Type'
	,(CASE
		WHEN x.FtpTransmission = 1 THEN 'FTP'
		WHEN x.FtpTransmission = 0 THEN 'EDI'
		ELSE ''
	  END) 'Notification Method'
	,ISNULL(dir.DirectionText, '') AS 'Direction'	
	,x.TransmissionOkay
	,x.OriginStreet1
    ,x.OriginStreet2
    ,x.OriginCity
    ,x.OriginState
    ,x.OriginCountryName
    ,x.OriginCountryCode
    ,x.OriginPostalCode
    ,x.DestinationStreet1
    ,x.DestinationStreet2
    ,x.DestinationCity
    ,x.DestinationState
    ,x.DestinationCountryName
    ,x.DestinationCountryCode
    ,x.DestinationPostalCode
    ,x.ExpirationDate
    ,x.ReceiptDate
    ,x.CustomerNumber
    ,x.VendorNumber
    ,x.VendorScac
    ,x.PurchaseOrderNumber
    ,x.ShipperReference
    ,x.EquipmentDescriptionCode
    ,x.TotalStops
    ,x.TotalWeight
    ,x.TotalPackages
    ,x.TotalPieces
    ,ISNULL(xcs.XmlConnectStatusText, '') AS 'Status'
    ,x.StatusMessage
	
FROM XmlConnect x
	LEFT JOIN RefEdiDocumentType AS edt ON edt.EdiDocumentTypeIndex = x.DocumentType
	LEFT JOIN RefDirection AS dir ON dir.DirectionIndex = x.Direction
	LEFT JOIN RefXmlConnectStatus AS xcs ON xcs.XmlConnectStatusIndex = x.[Status]

WHERE
		x.TenantId = @TenantId
		AND @UserId <> 0