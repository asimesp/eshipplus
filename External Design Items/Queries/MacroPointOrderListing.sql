DECLARE @TenantId bigint = 4;
DECLARE @UserId bigint = 2;

Select
	Id
	,TenantId	
	,IdNumber
	,TrackDurationHours
	,TrackIntervalMinutes
	,TrackCost
	,OrderId
	,TrackingStatusCode
	,TrackingStatusDescription
	,DateCreated
	,NumberType
	,Number
	,DateStopRequested
	,StartTrackDateTime
	,Username 
	,UserFirstName
	,UserLastName
	,EmailCopiesOfUpdatesTo
	,Notes

From MacroPointOrderViewSearch
Where
	@TenantId = TenantId
	AND	(SELECT COUNT(*) FROM [User] WHERE Id = @UserId and TenantEmployee = 1) > 0