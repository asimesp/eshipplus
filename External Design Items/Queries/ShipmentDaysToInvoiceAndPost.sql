DECLARE @TenantId bigint = 4;
DECLARE @UserId bigint = 3;
DECLARE @CustomerGroupId bigint = 0;

SELECT DISTINCT
	i.InvoiceNumber,
	i.InvoiceDate,
	CONVERT(nvarchar(20), i.InvoiceDate, 101) AS 'ViewInvoiceDate', 
	i.PostDate,
	CASE WHEN i.PostDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),i.PostDate,101) END AS 'ViewInvoicePostDate', 	
	sh.ShipmentNumber,	
	sh.DateCreated,
	sh.ActualDeliveryDate,
	sh.ActualPickupDate,
	(d.UnitSell - d.UnitDiscount) * (CASE WHEN d.Quantity < 1 THEN 1 ELSE d.Quantity END) AS 'AmountDue',
	c.CustomerNumber,
	c.Name AS 'CustomerName',	
	CONVERT(nvarchar(20), sh.DateCreated, 101) AS 'ViewShipmentDateCreated', 	
	DATEDIFF(dd, sh.DateCreated, i.InvoiceDate), --Days To Invoice Creation
	(CASE WHEN sh.ActualDeliveryDate = '1753-01-01 00:00:00.000' THEN -1
		  ELSE DATEDIFF(dd, sh.ActualDeliveryDate, i.InvoiceDate)
	 END) 'Days Un-invoiced from Delivery',
	(CASE WHEN i.PostDate = '1753-01-01 00:00:00.000' THEN -1 ELSE DATEDIFF(dd, sh.DateCreated, i.PostDate) END) 'DaysToInvoicePosting',
	(CASE 
		WHEN sh.ActualDeliveryDate = '1753-01-01 00:00:00.000' OR i.PostDate = '1753-01-01 00:00:00.000' THEN -1
		ELSE DATEDIFF(dd, sh.ActualDeliveryDate,i.PostDate)
	 END) AS 'DaysFromDeliveredToInvoicePosting', 
	 (CASE 
		WHEN sh.ActualDeliveryDate = '1753-01-01 00:00:00.000' OR sh.ActualPickupDate = '1753-01-01 00:00:00.000' THEN -1
		ELSE DATEDIFF(dd, sh.ActualPickupDate,sh.ActualDeliveryDate)
	 END) AS 'DaysFromPickupToDelivered', 
	ab.Code 'PrimaryAccountBucketCode',  
	ab.[Description] AS ' PrimaryAccountBucketDescription',
	c.CustomField1,	
	c.CustomField2,	
	c.CustomField3,	
	c.CustomField4
FROM 
	Invoice i
		INNER JOIN InvoiceDetail d ON d.InvoiceId = i.Id
		INNER JOIN Shipment sh ON sh.ShipmentNumber = d.ReferenceNumber AND d.ReferenceType = 0
		INNER JOIN ShipmentAccountBucket shab ON shab.ShipmentId = sh.Id AND shab.[Primary] = 1
		INNER JOIN AccountBucket ab ON ab.Id = shab.AccountBucketId
		INNER JOIN Customer c On c.Id = Sh.CustomerId
		LEFT JOIN RefServiceMode sm ON sm.ServiceModeIndex = sh.ServiceMode
WHERE 
	@TenantId <> 0
		AND @UserId <> 0
		AND i.InvoiceType = 0 -- 0 = Invoice, 1 = Supplemental, 2 = Credit
		AND i.TenantId = @TenantId
		AND (
				(SELECT COUNT(*) FROM [User] WHERE Id = @UserId and TenantEmployee = 1) > 0
				OR
				(sh.CustomerId IN (SELECT CustomerId FROM UserShipAs WHERE TenantId = @TenantId AND UserId = @UserId
								  UNION
								  SELECT [User].CustomerId FROM [User] WHERE Id = @UserId))
			)
		 AND (@CustomerGroupId = 0 OR (Select COUNT(*) from CustomerGroupMap WHERE CustomerId = sh.CustomerId and CustomerGroupId = @CustomerGroupId) > 0)