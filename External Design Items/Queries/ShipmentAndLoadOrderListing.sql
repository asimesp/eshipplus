Select 
	(Case
		When RecordType = 1 Then 'Load Order'
		When RecordType = 2 Then 'Shipment'
		Else ''
	End) 'Record Type',
	ShipmentNumber,
	[Description],
	ServiceModeText as 'Service Mode',
	DateCreated,
	(CASE WHEN DateCreated = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(10),DateCreated,121) END) 'DateCreatedText',
	DesiredPickupDate,
	(CASE WHEN DesiredPickupDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(10),DesiredPickupDate,121) END) 'DesiredPickupDateText',
	ActualPickupDate,
	(CASE WHEN ActualPickupDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(10),ActualPickupDate,121) END) 'ActualPickupDateText',
	OnTimePickup,
	EstimatedDeliveryDate,
	(CASE WHEN EstimatedDeliveryDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(10),EstimatedDeliveryDate,121) END) 'EstimatedDeliveryDateText',
	ActualDeliveryDate,
	(CASE WHEN ActualDeliveryDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(10),ActualDeliveryDate,121) END) 'ActualDeliveryDateText',
	OnTimeDelivery,
	StatusText as 'Status',
	BillReseller,
	PurchaseOrderNumber,
	ShipperReference,
	AuditedForInvoicing,
	OriginName,
	OriginStreet1,
	OriginStreet2,
	OriginCity,
	OriginState,
	OriginPostalCode,
	OriginCountryName,
	DestinationName,
	DestinationStreet1,
	DestinationStreet2,
	DestinationCity,
	DestinationState,
	DestinationPostalCode,
	DestinationCountryName,
	TotalAmountDue,
	TotalCost,
	Prefix,
	PrefixDescription,
	PriorityColor,
	PriorityCode,
	CustomerNumber,
	CustomerName,
	VendorNumber,
	VendorName,
	VendorProNumber,
	VendorTrackingUrl,
	CheckCallsPresent,
	CheckCallAlert,
	VendorInsuranceAlert,
	AccountBucketCode,
	Username,
	UserFirstName,
	UserLastName,
	TotalWeight,
	Equipments,
	ShipmentCoordinatorUsername,
	ShipmentCoordinatorFirstName,
	ShipmentCoordinatorLastName,
	CarrierCoordinatorUsername,
	CarrierCoordinatorFirstName,
	CarrierCoordinatorLastName,
	IsGuaranteedDeliveryService,
	GuaranteedDeliveryServiceTime,
	InDispute,
	Mileage,
	InDisputeReasonText
From 
	((
		Select * 
		From ShipmentViewSearch2(4, 2) 
		Where 1=1
	)
		Union 
	(
		Select * 
		From LoadOrderViewSearch(4, 2) 
		Where 1=1 
			AND ShipmentNumber NOT IN (
				Select ShipmentNumber 
				From ShipmentViewSearch2(4, 2) 
				Where 1=1)) 
	) as q
Order By ShipmentNumber Desc