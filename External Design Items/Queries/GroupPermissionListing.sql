DECLARE @TenantId bigint = 4;
DECLARE @UserId bigint = 3;

SELECT 
	g.Name,
	g.[Description],
	gp.Code,
	gp.[Description],
	(CASE WHEN gp.[Grant] = 0 THEN 'Disabled' ELSE 'Enabled' END),	-- Grant
	(CASE WHEN gp.[Modify] = 0 THEN 'Disabled' ELSE 'Enabled' END), -- Modify
	(CASE WHEN gp.[Delete] = 0 THEN 'Disabled' ELSE 'Enabled' END), -- Delete
	(CASE WHEN gp.[Deny] = 0 THEN 'Disabled' ELSE 'Enabled' END)    -- Deny
	
FROM [Group] g 
	INNER JOIN GroupPermission gp ON gp.GroupId = g.Id
WHERE 
	@TenantId <> 0 
		AND @UserId <> 0