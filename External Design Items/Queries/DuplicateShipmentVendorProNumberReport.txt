SELECT DISTINCT
	{0}	
FROM ShipmentVendor sv
	INNER JOIN Shipment s ON sv.ShipmentId = s.Id
	INNER JOIN Customer c ON s.CustomerId = c.Id
	INNER JOIN Vendor v ON sv.VendorId = v.Id
	INNER JOIN (SELECT pronumber, vendorId, COUNT(ShipmentId) as 'Count'
					FROM ShipmentVendor
					WHERE ProNumber IS NOT NULL AND ProNumber <> ''
					GROUP BY ProNumber, VendorId
					HAVING COUNT(ShipmentId) > 1
				) as q1
		ON q1.ProNumber = sv.ProNumber
WHERE
	s.TenantId  = @TenantId
	AND (
		(SELECT COUNT(*) FROM [User] WHERE Id = @UserId and TenantEmployee = 1) > 0
		OR
		(s.CustomerId IN 
			(SELECT CustomerId FROM UserShipAs WHERE TenantId = @TenantId AND UserId = @UserId
			 UNION
			 SELECT [User].CustomerId FROM [User] WHERE Id = @UserId))) 
	AND {1} 
	ORDER BY {2}