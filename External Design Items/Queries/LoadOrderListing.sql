SELECT 
	COUNT(*)
FROM
	LoadOrder

declare @TenantId bigint = 4
declare @UserId bigint = 2
declare @CustomerGroupId bigint = 0
declare @VendorGroupId bigint = 0


SELECT top 1000
	lo.LoadOrderNumber,
	ISNULL(lov.ProNumber,'') 'ProNumber',
	lo.PurchaseOrderNumber,
	lo.DesiredPickupDate,
	lo.EstimatedDeliveryDate,
	lo.DateCreated,
	ISNULL(sc.Username,'') 'LoadOrderCoordinatorUsername',
	ISNULL(sc.FirstName,'') 'LoadOrderCoordinatorFirstName',
	ISNULL(sc.LastName,'') 'LoadOrderCoordinatorLastName',
	ISNULL(cc.Username,'') 'CarrierCoordinatorUsername',
	ISNULL(cc.FirstName,'') 'CarrierCoordinatorFirstName',
	ISNULL(cc.LastName,'') 'CarrierCoordinatorLastName',
	lo.MiscField1,
	lo.MiscField2,
	CAST ( '1' AS Integer ) 'Control',
	ISNULL(p.Code,'') 'PrefixCode',
	ISNULL(p.[Description],'') 'PrefixDescription',
	lolo.[Description],
	lolo.Street1,
	lolo.Street2,
	lolo.City,
	lolo.[State],
	lolo.PostalCode,
	looc.Code,
	looc.Name,
	ISNULL(pco.City, '') 'StandardShipperCityName',
	ISNULL(pco.[State], '') 'StandardShipperStateName',
	lold.[Description],
	lold.Street1,
	lold.Street2,
	lold.City,
	lold.[State],
	lold.PostalCode,
	lodc.Code,
	lodc.Name,
	ISNULL(pcd.City, '') 'StandardConsigneeCityName',
	ISNULL(pcd.[State], '') 'StandardConsigneeStateName',
	ISNULL(v.VendorNumber,'') 'VendorNumber',
	ISNULL(v.Name,'') 'Name',
	ISNULL(v.Scac,'') 'Scac',
	ISNULL(v.DOT,'') 'DOT',
	ISNULL(v.MC,'') 'MC',
	ISNULL(sp.Code, '') 'PriorityCode',
	ISNULL(sp.[Description], '') 'PriorityDescription',
	ISNULL(ab.Code,'') 'AccountBucketCode',
	ISNULL(ab.[Description],'') 'AccountBucketDescription',
	c.CustomerNumber,
	c.Name,
	c.AuditInstructions,
	t.TierNumber,
	t.Name,
	u.Username,
	u.FirstName,
	u.LastName,
	(CASE WHEN u.TenantEmployee = 0 THEN 'Yes' ELSE 'No' END) 'UserIsTenantEmployee',
	(CASE WHEN u.TenantEmployee = 0 THEN 'No' ELSE 'Yes' END) 'Is Employee',
	ISNULL(sr.SalesRepresentativeNumber,'') 'SalesRepresentativeNumber',
	ISNULL(sr.Name,'') 'SalesRepresentativeName',
	lo.SalesRepresentativeCommissionPercent,
	lo.SalesRepMinCommValue,
	lo.SalesRepMaxCommValue,
	lo.SalesRepAddlEntityName,
	lo.SalesRepAddlEntityCommPercent,
	lo.Mileage,
	lo.EmptyMileage,
	lo.ShipperReference,
	ISNULL(scs.TotalCost,0) 'TotalCost',
	ISNULL(scs.TotalAmountDue,0) 'TotalAmountDue',
	ISNULL(scs.TotalCharges, 0) 'TotalCharges',
	ISNULL(scs.TotalDiscount, 0) 'TotalDiscount',
	ISNULL(scs.TotalNettedCharge,0) 'TotalNettedCharge',
	ISNULL(scs.TotalProfit,0) 'TotalProfit',
	ISNULL(scs.FreightCharges,0) 'FreightCharges',
	ISNULL(scs.FreightCost,0) 'FreightCost',
	ISNULL(scs.FuelCharges,0) 'FuelCharges',
	ISNULL(scs.FuelCost,0) 'FuelCost',
	ISNULL(scs.AccessorialCharges,0) 'AccessorialCharges',
	ISNULL(scs.AccessorialCost,0) 'AccessorialCost',
	ISNULL(scs.ServiceCharges,0) 'ServiceCharges',
	ISNULL(scs.ServiceCost,0) 'ServiceCost',
	ISNULL(sis.TotalPieceCount,0) 'TotalPieceCount',
	ISNULL(sis.TotalWeight,0) 'TotalWeight',
	ISNULL(sis.TotalPackageQty,0) 'TotalPackageQty',
	ISNULL(sis.HighestFreightClass,'') 'HighestFreightClass',
	ISNULL(abu.Name,'') 'AccountBucketUnitName',
	ISNULL((SELECT (dt.Code + ', ')
				FROM LoadOrderDocument lod, DocumentTag dt
				WHERE lod.LoadOrderId = lo.Id AND lod.DocumentTagId = dt.Id 
				ORDER BY lod.Id Asc
				FOR XML PATH('')),'') 'DocumentTags',
	ISNULL((SELECT (lon.[Message] + ', ')
				FROM LoadOrderNote lon
				WHERE lon.LoadOrderId = lo.Id AND lon.[Type] = 2 
				ORDER BY lon.Id Asc
				FOR XML PATH('')),'') 'FinanceNotes',	
	ISNULL((SELECT (lon.[Message] + ', ')
				FROM LoadOrderNote lon
				WHERE lon.LoadOrderId = lo.Id AND lon.[Type] = 3
				ORDER BY lon.Id Asc
				FOR XML PATH('')),'') 'OperationNotes',
	ISNULL((SELECT (et.TypeName + ', ')
				FROM LoadOrderEquipment loe INNER JOIN EquipmentType et ON loe.EquipmentTypeId = et.Id AND loe.LoadOrderId = lo.Id
				ORDER BY et.Id Desc
				FOR XML PATH('')),'') 'EquipmentTypes',
	ISNULL((SELECT (s.Code + ', ')
				FROM LoadOrderService los INNER JOIN [Service] s ON los.ServiceId = s.Id AND los.LoadOrderId = lo.Id
				ORDER BY s.Code Desc
				FOR XML PATH('')),'') 'Service Codes', 
	(CASE WHEN lo.DateCreated = '1753-01-01 00:00:00.000' 
		THEN '' 
		ELSE CONVERT(nvarchar(20),lo.DateCreated,101) 
	END) 'ViewDateCreated',
	ISNULL(ls.LoadOrderStatusText, '') AS 'Status', 
	ISNULL(sm.ServiceModeText, '') AS 'Service Mode'
FROM 
	LoadOrder lo
		INNER JOIN Customer c ON lo.CustomerId = c.Id
		INNER JOIN Tier t ON t.Id = c.TierId 
		INNER JOIN [User] u ON lo.UserId = u.Id
		INNER JOIN LoadOrderLocation lolo ON lo.OriginId = lolo.Id
		INNER JOIN Country looc ON looc.Id = lolo.CountryId
		INNER JOIN LoadOrderLocation lold ON lo.DestinationId = lold.Id
		INNER JOIN Country lodc ON lodc.Id = lold.CountryId
		LEFT JOIN LoadOrderVendor lov ON lov.LoadOrderId = lo.Id AND lov.[Primary] = 1
		LEFT JOIN Vendor v ON lov.VendorId = v.Id
		LEFT JOIN LoadOrderAccountBucket loab ON lo.Id = loab.LoadOrderId AND loab.[Primary] = 1
		LEFT JOIN AccountBucket ab ON loab.AccountBucketId = ab.Id 
		LEFT JOIN LoadOrderChargeStatistics scs ON scs.LoadOrderId = lo.Id
		LEFT JOIN PostalCode pco ON lolo.PostalCode = pco.Code AND lolo.CountryId = pco.CountryId AND pco.[Primary] = 1
		LEFT JOIN PostalCode pcd ON lold.PostalCode = pcd.Code AND lold.CountryId = pcd.CountryId AND pcd.[Primary] = 1
		LEFT JOIN Prefix p ON p.Id = lo.PrefixId
		LEFT JOIN SalesRepresentative sr ON lo.SalesRepresentativeId = sr.Id 
		LEFT JOIN LoadOrderItemStatistics sis ON sis.LoadOrderId = lo.Id
		LEFT JOIN AccountBucketUnit abu ON abu.Id = lo.AccountBucketUnitId
		LEFT JOIN [USER] sc ON sc.Id = lo.LoadOrderCoordinatorUserId
		LEFT JOIN [USER] cc ON cc.Id = lo.CarrierCoordinatorUserId
		LEFT JOIN ShipmentPriority sp ON sp.Id = lo.ShipmentPriorityId
		LEFT JOIN RefLoadOrderStatus ls ON ls.LoadOrderStatusIndex = lo.[Status]
		LEFT JOIN RefServiceMode sm ON sm.ServiceModeIndex = lo.ServiceMode
WHERE 
	DATEDIFF(MONTH, lo.DateCreated, GETDATE()) <= 6