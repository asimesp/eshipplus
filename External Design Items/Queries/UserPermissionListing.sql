DECLARE @TenantId bigint = 4;
DECLARE @UserId bigint = 3;

SELECT DISTINCT
	r.Username, 
	r.FirstName, 
	r.LastName, 
	r.Email,
	ISNULL(r.[GroupName],''),
	ISNULL(r.[PermissionCode],''),
	r.[Grant],
	r.[Modify],
	r.[Delete],
	r.[Deny],
	ISNULL(Code,''),--user department code
	ISNULL([Description],''),
	ISNULL(Phone,''),
	ISNULL(Fax,''),
	ISNULL(DepartmentEmail,'')
FROM
(
	SELECT u.Username, u.FirstName, u.LastName, u.Email, g.Name AS 'GroupName', gp.Code AS 'PermissionCode',ud.Code, ud.[Description], ud.Phone,ud.Fax, ud.Email AS 'DepartmentEmail',
		(CASE WHEN gp.[Grant] = 0 THEN 'Disabled' ELSE 'Enabled' END) AS 'Grant',
		(CASE WHEN gp.[Modify] = 0 THEN 'Disabled' ELSE 'Enabled' END) AS 'Modify',
		(CASE WHEN gp.[Delete] = 0 THEN 'Disabled' ELSE 'Enabled' END) AS 'Delete',
		(CASE WHEN gp.[Deny] = 0 THEN 'Disabled' ELSE 'Enabled' END) AS 'Deny'
	FROM [User] u
			INNER JOIN GroupUser gu ON gu.UserId = u.Id
			INNER JOIN [Group] g ON g.Id = gu.GroupId
			INNER JOIN GroupPermission gp ON gp.GroupId = g.Id
			LEFT JOIN UserDepartment ud ON ud.Id = u.UserDepartmentId
	WHERE @TenantId <> 0 AND @UserId <> 0

	UNION

	SELECT u.Username, u.FirstName, u.LastName, u.Email, '' AS 'GroupName', up.Code AS 'PermissionCode',udd.Code, udd.[Description], udd.Phone, udd.Fax, udd.Email AS 'DepartmentEmail',
		(CASE WHEN up.[Grant] = 0 THEN 'Disabled' ELSE 'Enabled' END) AS 'Grant',
		(CASE WHEN up.[Modify] = 0 THEN 'Disabled' ELSE 'Enabled' END) AS 'Modify',
		(CASE WHEN up.[Delete] = 0 THEN 'Disabled' ELSE 'Enabled' END) AS 'Delete',
		(CASE WHEN up.[Deny] = 0 THEN 'Disabled' ELSE 'Enabled' END) AS 'Deny' 
	FROM [User] u
			INNER JOIN [UserPermission] up ON up.UserId = u.Id
			LEFT JOIN UserDepartment udd ON udd.Id = u.UserDepartmentId
	WHERE @TenantId <> 0 AND @UserId <> 0
) AS r
WHERE Username = 'Rick'