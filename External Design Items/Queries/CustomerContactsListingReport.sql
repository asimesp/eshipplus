Declare @TenantId bigint = 4
Declare @UserId bigint = 2
Declare @CustomerGroupId bigint = 55
Declare @VendorGroupId bigint = 0

SELECT 
	DateCreated,
	ViewDateCreated,
	CustomerNumber,
	CustomerName,
	CustomerActive,
	TierNumber,
	TierName,
	CustomerType,
	InvoiceTerms,
	Notes,
	AccountBucketIsActive,
	AccountBucketCode,
	AccountBucketDescription,
	SalesRepNumber,
	SalesRepName,
	HidePrefix,
	PrefixCode,
	PrefixDescription,
	LocationNumber,
	BillToLocation,
	Street1,
	Street2,
	City,
	[State],
	PostalCode,
	Country,
	PrimaryContact,
	ContactName,
	ContactType,
	ContactDescription,
	ContactPhone,
	ContactMobile,
	ContactEmail,
	ContactFax,
	[Source],
	LocationIsPrimary,
	MainBillToLocation,
	ContactComment,
	CustomField1,
	CustomField2,
	CustomField3,
	CustomField4
FROM
(
	SELECT
		c.DateCreated,
		(CASE WHEN c.DateCreated = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),c.DateCreated,101) END)'ViewDateCreated',
		c.CustomerNumber,
		c.Name 'CustomerName',
		(CASE WHEN c.Active = 0 THEN 'No' ELSE 'Yes' END) 'CustomerActive' ,
		t.TierNumber,
		t.Name 'TierName',
		ISNULL(cut.CustomerTypeText, '') AS 'CustomerType',
		c.InvoiceTerms,
		c.Notes,
		(CASE WHEN ab.Active = 0 THEN 'No' ELSE 'Yes' END) 'AccountBucketIsActive',
		ab.Code 'AccountBucketCode',
		ab.[Description] 'AccountBucketDescription',
		ISNULL(sr.SalesRepresentativeNumber,'') 'SalesRepNumber',
		ISNULL(sr.Name,'') 'SalesRepName',
		(CASE WHEN c.HidePrefix = 0 THEN 'No' ELSE 'Yes' End) 'HidePrefix',
		p.Code 'PrefixCode',
		p.[Description] 'PrefixDescription',
		cl.LocationNumber,
		(CASE WHEN cl.BillToLocation = 1 THEN 'Yes' ElSE 'No' End) 'BillToLocation',
		(CASE WHEN cl.[Primary] = 1 THEN 'Yes' ElSE 'No' End) 'LocationIsPrimary',
		(CASE WHEN cl.MainBillToLocation = 1 THEN 'Yes' ElSE 'No' End) 'MainBillToLocation',
		cl.Street1,
		cl.Street2,
		cl.City,
		cl.[State],
		cl.PostalCode,
		ISNULL(cc.Comment,'') 'ContactComment',
		ct.Name 'Country',
		(CASE WHEN cc.[Primary] = 0 THEN 'No' ELSE 'Yes' END) 'PrimaryContact',
		ISNULL(cc.Name,'') 'ContactName',
		ISNULL(ctype.Code, '') 'ContactType',
		ISNULL(ctype.[Description],'') 'ContactDescription',
		ISNULL(cc.Phone,'') 'ContactPhone',
		ISNULL(cc.Mobile,'') 'ContactMobile',
		ISNULL(cc.Email,'') 'ContactEmail',
		ISNULL(cc.Fax,'') 'ContactFax',
		'Customer Contact' 'Source',
		c.CustomField1,
		c.CustomField2,
		c.CustomField3,
		c.CustomField4
	FROM	
		Customer c
		INNER JOIN CustomerLocation AS cl ON cl.CustomerId = c.Id  
		INNER JOIN Country AS ct ON ct.Id = cl.CountryId
		INNER JOIN Tier AS t ON t.Id = c.TierId
		INNER JOIN AccountBucket AS ab On ab.Id = c.DefaultAccountBucketId
		LEFT JOIN CustomerContact AS cc ON cc.CustomerLocationId = cl.Id
		LEFT JOIN ContactType  AS ctype ON ctype.Id = cc.ContactTypeId
		LEFT JOIN SalesRepresentative AS sr ON sr.Id = c.SalesRepresentativeId
		LEFT JOIN Prefix AS p ON p.Id = c.PrefixId
		LEFT JOIN RefCustomerType AS cut ON cut.CustomerTypeIndex = c.CustomerType
	WHERE
		@TenantId <> 0 
				  AND @UserId <> 0
				  AND c.TenantId = @TenantId
				  AND (
					(Select COUNT(*) from [User] where id = @UserId and TenantEmployee = 1) > 0
					OR
					(c.Id IN (SELECT CustomerId FROM UserShipAs WHERE TenantId = @TenantId AND UserId = @UserId
						UNION
						SELECT [User].CustomerId FROM [User] WHERE Id = @UserId)))
				  AND (@CustomerGroupId = 0 OR (SELECT COUNT(*) FROM CustomerGroupMap WHERE CustomerId = c.Id AND CustomerGroupId = @CustomerGroupId) > 0)
	UNION

	SELECT 
		Convert(DateTime, '1753-01-01 00:00:00.000'),
		'',
		c.CustomerNumber,
		c.Name,	
		(CASE WHEN c.Active = 0 THEN 'No' ELSE 'Yes' END),
		t.TierNumber,
		t.Name,
		ISNULL(cut.CustomerTypeText, '') AS 'CustomerType',
		c.InvoiceTerms,
		c.Notes,
		(CASE WHEN ab.Active = 0 THEN 'No' ELSE 'Yes' END),
		ab.Code,
		ab.[Description],
		ISNULL(sr.SalesRepresentativeNumber,''),
		ISNULL(sr.Name,''),
		(CASE WHEN c.HidePrefix = 0 THEN 'No' ELSE 'Yes' End),
		p.Code,
		p.[Description],
		'',
		'No',
		'No',
		'No',
		u.Street1,
		u.Street2,
		u.City,
		u.[State],
		u.PostalCode,
		'',
		uc.Name,
		'No',
		u.FirstName + ' ' +	u.LastName,
		(CASE WHEN u.TenantEmployee = 1 THEN 'Employee'	ELSE 'Non-Employee' END),
		'',
		u.Phone,
		u.Mobile,
		u.Email,
		u.Fax,
		'User',
		c.CustomField1,
		c.CustomField2,
		c.CustomField3,
		c.CustomField4
	FROM 
		[User] AS u 
		INNER JOIN Customer AS c ON c.Id = u.CustomerId
		INNER JOIN Tier AS t ON t.Id = c.TierId
		INNER JOIN Country AS uc ON uc.Id = u.CountryId
		INNER JOIN AccountBucket AS ab On ab.Id = c.DefaultAccountBucketId
		LEFT JOIN SalesRepresentative AS sr ON sr.Id = c.SalesRepresentativeId
		LEFT JOIN Prefix AS p ON p.Id = c.PrefixId
		LEFT JOIN RefCustomerType AS cut ON cut.CustomerTypeIndex = c.CustomerType
	WHERE 
		@TenantId <> 0 
			AND @UserId <> 0 
			AND (@CustomerGroupId = 0 OR (SELECT COUNT(*) FROM CustomerGroupMap WHERE CustomerId = c.Id AND CustomerGroupId = @CustomerGroupId) > 0)
) As ccau

Order By CustomerNumber