SELECT
	st.ServiceTicketNumber,
	REPLACE(sti.Description, 'Container #: ', '') as 'Container Number',
	REPLACE(sti.Comment, 'Transaction Id: ', '') as 'Transaction Id'
FROM
	ServiceTicket st
	INNER JOIN ServiceTicketItem sti ON sti.ServiceTicketId = st.Id
WHERE
	sti.[Description] LIKE 'Container #:%' AND
	sti.Comment LIKE 'Transaction Id:%'