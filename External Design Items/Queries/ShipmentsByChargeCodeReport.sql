SELECT 
	COUNT(*)
FROM 
	Shipment INNER JOIN ShipmentCharge ON ShipmentCharge.ShipmentId = Shipment.Id

declare @TenantId bigint = 4
declare @UserId bigint = 2
declare @CustomerGroupId bigint = 0
declare @VendorGroupId bigint = 0

SELECT Top 1000
	sh.ShipmentNumber,
	sh.DateCreated,
	(CASE WHEN sh.DateCreated = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),sh.DateCreated,101) END)'ViewDateCreated', 
	sh.DesiredPickupDate,
	(CASE WHEN sh.DesiredPickupDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),sh.DesiredPickupDate,101) END) 'ViewDesiredPickupDate',
	sh.ActualPickupDate,
	(CASE WHEN sh.ActualPickupDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),sh.ActualPickupDate,101) END)'ViewActualPickupDate',
	sh.EstimatedDeliveryDate,
	(CASE WHEN sh.EstimatedDeliveryDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),sh.EstimatedDeliveryDate,101) END)'ViewEstimatedDelDate',
	sh.ActualDeliveryDate,
	(CASE WHEN sh.ActualDeliveryDate = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),sh.ActualDeliveryDate,101) END) 'ViewActualDelDate',
	ISNULL(sm.ServiceModeText, '') AS 'ServiceMode', 
	cc.Code,
	cc.[Description],
	shc.UnitBuy,
	shc.UnitSell,
	shc.Quantity,
	shc.UnitDiscount,
	shc.Comment,
	u.Username,
	c.CustomerNumber,
	c.Name,
	t.TierNumber,
	t.Name,
	shol.[Description],
	shol.Street1,
	shol.Street2,
	shol.City,
	shol.[State],
	shol.PostalCode,
	shol.SpecialInstructions,
	oc.Name,
	shdl.[Description],
	shdl.Street1,
	shdl.Street2,
	shdl.City,
	shdl.[State],
	shdl.PostalCode,
	dc.Name,
	v.VendorNumber,
	v.Name,
	ISNULL(ss.ShipmentStatusText, '') AS 'Status', 
	ISNULL((SELECT (et.TypeName + ', ')
				FROM ShipmentEquipment she INNER JOIN EquipmentType et ON she.EquipmentTypeId = et.Id AND she.ShipmentId = sh.Id
				WHERE sh.Id= she.ShipmentId 
				ORDER BY et.Id Desc
				FOR XML PATH('')),'')'EquipmentTypes',
	spac.Code 'Account Bucket Code',
	spac.[Description] 'Account Bucket Description',
	IsNull(sis.TotalWeight, 0) 'Total Weight',
	IsNull(sis.TotalPieceCount, 0) 'Total Piece Count',
	Isnull(ccu.Username,'') 'Carrier Coordinator Username',
	Isnull(scu.Username,'') 'Shipment Coordinator Username',
	Isnull(sp.Code, '') 'Prefix Code',
	Isnull(sp.[Description],'') 'Prefix Description',
	sh.AuditedForInvoicing,
	sh.PurchaseOrderNumber,
	sh.ShipperReference,
	shv.ProNumber,
	sh.MiscField1,
	scs.TotalCost,
	scs.TotalAmountDue,
	scs.TotalCharges,
	scs.TotalDiscount,
	scs.TotalNettedCharge,
	scs.TotalProfit,
	scs.FreightCharges,
	scs.FreightCost,
	scs.FuelCharges,
	scs.FuelCost,
	scs.AccessorialCharges,
	scs.AccessorialCost,
	scs.ServiceCharges,
	scs.ServiceCost,
	(CASE WHEN ra.Id IS NULL OR scs.FreightCharges = 0 
			 THEN 0
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalFreightMarkupIsPercent = 1	
		     THEN (scs.FreightCharges / (1+(sh.ResellerAdditionalFreightMarkup/100)))
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalFreightMarkupIsPercent = 0   
			 THEN scs.FreightCharges - sh.ResellerAdditionalFreightMarkup
		  ELSE 0
	END) 'Reseller Freight Charges', -- FreightChargesReseller,
	(CASE WHEN ra.Id IS NULL OR scs.FuelCharges = 0 
			 THEN 0
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalFuelMarkupIsPercent = 1	
		     THEN (scs.FuelCharges / (1+(sh.ResellerAdditionalFuelMarkup/100)))
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalFuelMarkupIsPercent = 0   
			 THEN scs.FuelCharges - sh.ResellerAdditionalFuelMarkup
		  ELSE 0
	END) 'Reseller Fuel Charges', -- FuelChargesReseller
	(CASE WHEN ra.Id IS NULL OR scs.AccessorialCharges = 0 
			 THEN 0
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalAccessorialMarkupIsPercent = 1	
		     THEN (scs.AccessorialCharges / (1+(sh.ResellerAdditionalAccessorialMarkup/100)))
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalAccessorialMarkupIsPercent = 0   
			 THEN scs.AccessorialCharges - sh.ResellerAdditionalAccessorialMarkup
		  ELSE 0
	END) 'Reseller Accessorial Charges', --AS AccessorialChargesReseller
	(CASE WHEN ra.Id IS NULL OR scs.ServiceCharges = 0 
			 THEN 0
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalServiceMarkupIsPercent = 1	
		     THEN (scs.ServiceCharges / (1+(sh.ResellerAdditionalServiceMarkup/100)))
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalServiceMarkupIsPercent = 0   
			 THEN scs.ServiceCharges - sh.ResellerAdditionalServiceMarkup
		  ELSE 0
	END) 'Reseller Service Charges', --AS ServiceChargesReseller
	
	((CASE WHEN ra.Id IS NULL OR scs.FreightCharges = 0 
			 THEN 0
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalFreightMarkupIsPercent = 1	
		     THEN (scs.FreightCharges / (1+(sh.ResellerAdditionalFreightMarkup/100)))
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalFreightMarkupIsPercent = 0   
			 THEN scs.FreightCharges - sh.ResellerAdditionalFreightMarkup
		  ELSE 0
	END) +
	(CASE WHEN ra.Id IS NULL OR scs.FuelCharges = 0 
			 THEN 0
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalFuelMarkupIsPercent = 1	
		     THEN (scs.FuelCharges / (1+(sh.ResellerAdditionalFuelMarkup/100)))
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalFuelMarkupIsPercent = 0   
			 THEN scs.FuelCharges - sh.ResellerAdditionalFuelMarkup
		  ELSE 0
	END) +
	(CASE WHEN ra.Id IS NULL OR scs.AccessorialCharges = 0 
			 THEN 0
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalAccessorialMarkupIsPercent = 1	
		     THEN (scs.AccessorialCharges / (1+(sh.ResellerAdditionalAccessorialMarkup/100)))
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalAccessorialMarkupIsPercent = 0   
			 THEN scs.AccessorialCharges - sh.ResellerAdditionalAccessorialMarkup
		  ELSE 0
	END) +
	(CASE WHEN ra.Id IS NULL OR scs.ServiceCharges = 0 
			 THEN 0
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalServiceMarkupIsPercent = 1	
		     THEN (scs.ServiceCharges / (1+(sh.ResellerAdditionalServiceMarkup/100)))
		  WHEN ra.Id IS NOT NULL AND sh.ResellerAdditionalServiceMarkupIsPercent = 0   
			 THEN scs.ServiceCharges - sh.ResellerAdditionalServiceMarkup
		  ELSE 0
	END)) 'Total Reseller Charges' --AS TotalResellerCharges

FROM
	Shipment AS sh
	INNER JOIN ShipmentCharge AS shc ON sh.Id = shc.ShipmentId
	INNER JOIN ChargeCode AS cc ON cc.Id = shc.ChargeCodeId
	INNER JOIN [User] AS u ON u.Id = sh.UserId
	INNER JOIN Customer AS c ON c.Id = sh.CustomerId
	INNER JOIN Tier as t ON t.Id = c.TierId--check on this
	INNER JOIN ShipmentLocation AS shol ON shol.Id = sh.OriginId
	INNER JOIN Country AS oc ON oc.Id = shol.CountryId
	INNER JOIN ShipmentLocation AS shdl ON shdl.Id = sh.DestinationId
	INNER JOIN Country AS dc ON dc.Id = shdl.CountryId
	INNER JOIN ShipmentVendor AS shv ON shv.ShipmentId = sh.Id 
	INNER JOIN Vendor AS v ON v.Id = shv.VendorId AND shv.[Primary] = 1
	INNER JOIN ShipmentChargeStatistics scs ON scs.ShipmentId = sh.Id
	INNER JOIN ShipmentAccountBucket sac on sac.ShipmentId = sh.Id And sac.[Primary] = 1
	INNER JOIN AccountBucket spac on spac.Id = sac.AccountBucketId

	Left Join [User] ccu on ccu.Id = sh.CarrierCoordinatorUserId
	Left Join [User] scu on scu.Id = sh.ShipmentCoordinatorUserId
	Left Join Prefix sp on sp.Id = sh.PrefixId
	Left Join ShipmentItemStatistics sis ON sis.ShipmentId = sh.Id
	Left Join RefServiceMode sm ON sm.ServiceModeIndex = sh.ServiceMode
	Left Join RefShipmentStatus ss ON ss.ShipmentStatusIndex = sh.[Status]
	Left Join ResellerAddition ra ON ra.Id = sh.ResellerAdditionId
WHERE
	@TenantId <> 0 
              AND @UserId <> 0
              AND sh.TenantId = @TenantId
              AND (
				(Select COUNT(*) from [User] where id = @UserId and TenantEmployee = 1) > 0
				OR
				(sh.CustomerId in (SELECT CustomerId FROM UserShipAs WHERE TenantId = @TenantId and UserId = @UserId
					UNION
					SELECT [User].CustomerId FROM [User] WHERE Id = @UserId)))
			  AND (@CustomerGroupId = 0 OR (Select COUNT(*) from CustomerGroupMap WHERE CustomerId = sh.CustomerId and CustomerGroupId = @CustomerGroupId) > 0)
			  AND (@VendorGroupId = 0 OR (Select COUNT(*) from VendorGroupMap WHERE VendorId = shv.VendorId and VendorGroupId = @VendorGroupId) > 0)
