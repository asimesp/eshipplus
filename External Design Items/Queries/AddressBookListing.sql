SELECT 
	c.CustomerNumber,
	c.Name,
	ab.Street1,
	ab.Street2,
	ab.City,
	ab.[State],
	ab.PostalCode,
	co.Code,
	ab.OriginSpecialInstruction,
	ab.DestinationSpecialInstruction,
	ab.GeneralInfo,
	ab.Direction,
	ISNULL(abc.Name, '') 'Primary Contact Name',
	ISNULL(abc.Phone,'') 'Primary Contact Phone',
	ISNULL(abc.Fax,'') 'Primary Contact Fax',
	ISNULL(abc.Email,'') 'Primary Contact Email',
	ISNULL(abc.Comment,'') 'Primary Contact Comment'
FROM
	AddressBook ab
	INNER JOIN Customer c ON c.Id = ab.CustomerId
	INNER JOIN Country co ON co.Id = ab.CountryId
	LEFT JOIN AddressBookContact abc ON abc.AddressBookId = ab.Id and abc.[Primary] = 1