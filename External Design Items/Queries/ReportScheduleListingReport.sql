SELECT
	rc.Name AS 'Report Configuration Name',
	rcu.Username AS 'Report Configuration Created By',
	rsu.Username AS 'Report Schedule Run By',
	(CASE WHEN rs.[Enabled] = 0 THEN 'No' ELSE 'Yes' END) AS 'Enabled',
	rs.[Time], 
	ISNULL(si.ScheduleIntervalText, '') AS 'Scheduled Interval',
	rs.Start AS 'Start Date',
	(CASE WHEN rs.Start = '1753-01-01 00:00:00.000' THEN ''	ELSE CONVERT(nvarchar(20),rs.Start,101) END) AS 'View Start Date ',
	rs.[End] AS 'End Date',
	(CASE WHEN rs.[End] = '1753-01-01 00:00:00.000' THEN ''	ELSE CONVERT(nvarchar(20),rs.[End],101) END) AS 'View End Date ',
	rs.LastRun,
	(CASE WHEN rs.LastRun = '1753-01-01 00:00:00.000' THEN '' ELSE CONVERT(nvarchar(20),rs.LastRun,101) END) AS 'View End Date ',
	(CASE WHEN rs.Notify = 0 THEN 'No' ELSE 'Yes' END) AS 'Send Notifications',
	rs.NotifyEmails, 
	(CASE WHEN rs.ExcludeSunday = 0 THEN 'No' ELSE 'Yes' END) AS 'No Sunday Run',
	(CASE WHEN rs.ExcludeMonday = 0 THEN 'No' ELSE 'Yes' END) AS 'No Monday Run',
	(CASE WHEN rs.ExcludeTuesday= 0 THEN 'No' ELSE 'Yes' END) AS 'No Tuesday Run',
	(CASE WHEN rs.ExcludeWednesday= 0 THEN 'No' ELSE 'Yes' END) AS 'No Wednesday Run',
	(CASE WHEN rs.ExcludeThursday = 0 THEN 'No' ELSE 'Yes' END) AS 'No Thursday Run',
	(CASE WHEN rs.ExcludeFriday = 0 THEN 'No' ELSE 'Yes' END) AS 'No Friday Run',
	(CASE WHEN rs.ExcludeSaturday = 0 THEN 'No' ELSE 'Yes' END) AS 'No Saturday Run',
	(CASE WHEN rs.Extension = 0 THEN 'Text' ELSE 'Excel' END) AS 'Output Format',
	(CASE WHEN rs.FtpEnabled = 0 THEN 'No' ELSE 'Yes' END) AS 'FTP Enabled'
	
FROM
	ReportConfiguration AS rc 
	INNER JOIN ReportSchedule AS rs ON rs.ReportConfigurationId = rc.Id
	INNER JOIN [User] AS rcu ON rcu.Id = rc.UserId
	INNER JOIN [User] AS rsu ON rsu.Id = rs.UserId
	LEFT JOIN RefScheduleInterval AS si ON si.ScheduleIntervalIndex = rs.ScheduleInterval

	
SELECT * FROM ReportSchedule