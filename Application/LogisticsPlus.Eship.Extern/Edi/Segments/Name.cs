﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
    public class Name
    {
        public EntityIdCode EntityIdentifierCode { get; set; }
        public string EntityName { get; set; }
        public string IdentificationCodeQualifier { get; set; }
        public string IdentificationCode { get; set; }
    }
}
