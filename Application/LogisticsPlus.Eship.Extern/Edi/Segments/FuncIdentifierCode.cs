﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
	public enum FuncIdentifierCode
	{
		/// <summary>
		/// Air freight details and invoice
		/// </summary>
		IA,

		/// <summary>
		/// Motor Carrier Freight Details and invoice
		/// </summary>
		IM,

		/// <summary>
		/// Ocean Shipment billing details
		/// </summary>
		IO,

		/// <summary>
		/// Rail carrier freight details and invoice
		/// </summary>
		IR,

		/// <summary>
		/// Purchase order
		/// </summary>
		PO,

		/// <summary>
		/// Transportation carrier shipment status message
		/// </summary>
		QM,

		/// <summary>
		/// Shipment information
		/// </summary>
		SI
	}
}
