﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
    public class Contact
    {
        public ContactFuncCode ContactFunctionCode { get; set; }
        public string Name { get; set; }
        public CommNumberQualifier CommunicationNumberQualifier { get; set; }
        public string CommunicationNumber { get; set; }
        public string ContactInquiryReference { get; set; }
    }
}
