﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
    public class EquipmentDetail
    {
    	public string EquipmentNumber { get; set; }
    	public string EquipmentDescriptionCode { get; set; }
    }
}
