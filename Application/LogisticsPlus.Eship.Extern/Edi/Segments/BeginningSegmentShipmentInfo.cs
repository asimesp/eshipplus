﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
    public class BeginningSegmentShipmentInfo 
    {
        public string StandardPointLocationCode { get; set; }
        public WghtUnitQualifier WeightUnitQualifier { get; set; }
        public int TotalEquipment { get; set; }
        public string ShipmentWeightCode { get; set; }
        public string CustomsDocumentHandlingCode { get; set; }
        public ShipMethodOfPay PaymentMethodCode { get; set; }
        public string TariffServiceCode { get; set; }
        public string StandardCarrierAlphaCode { get; set; }
        public string ShipmentIdNumber { get; set; }
        public string ShipmentMethodOfPayment { get; set; }
        public string ShipmentQualifier { get; set; }
        public string TransportationTermsCode { get; set; }
    }
}
