﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
	public class StopOffDetails
	{
		public int StopSequenceNumber { get; set; }
		public StopReasonCode StopReasonCode { get; set; }
		public string SpecialInstructions { get; set; }
	}
}
