﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
	public enum FreightInvDateQualifier
	{
		/// <summary>
		/// Estimated delivery
		/// </summary>
		ESD,

		/// <summary>
		/// Delivered
		/// </summary>
		ACD,

		/// <summary>
		/// Estimated Pickup
		/// </summary>
		ESP,

		/// <summary>
		/// Actual Pickup
		/// </summary>
		ACP,

		/// <summary>
		/// Not Applicable
		/// </summary>
		NA,
	}
}
