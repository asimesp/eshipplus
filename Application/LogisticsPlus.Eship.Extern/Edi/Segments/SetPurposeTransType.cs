﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
	public enum SetPurposeTransType
	{
		/// <summary>
		/// Original
		/// </summary>
		OR,

		/// <summary>
		/// Cancellation
		/// </summary>
		CA,

		/// <summary>
		/// Update
		/// </summary>
		UP,
	}
}
