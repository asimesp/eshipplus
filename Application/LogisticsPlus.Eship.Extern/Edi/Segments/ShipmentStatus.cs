﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
	/// <summary>
	/// Edi 214 Shipment Status Details
	/// </summary>
	public class ShipmentStatus
	{
		public BeginningSegmentShipStatMsg BeginningSegment { get; set; }
		public EdiLocation Location { get; set; }
		public ShipStatMsgCode ShipmentStatusCode { get; set; }
		public string ShipmentStatusReasonCode { get; set; }
		public string DateTime { get; set; }
		public Remarks Remarks { get; set; }
		public EquipmentDetail EquipmentDetail { get; set; }
		public ShipmentWghtPkgAndQtyData ShipmentWghtPkgAndQtyData { get; set; }
	}
}
