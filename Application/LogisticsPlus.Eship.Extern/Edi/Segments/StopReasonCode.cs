﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
	public enum StopReasonCode
	{
		/// <summary>
		/// complete load
		/// </summary>
		CL,

		/// <summary>
		/// Consolidate
		/// </summary>
		CN,

		/// <summary>
		/// Complete Unload
		/// </summary>
		CU,

		/// <summary>
		/// Drop trailer
		/// </summary>
		DT,

		/// <summary>
		/// Load
		/// </summary>
		LD,

		/// <summary>
		/// Part Load
		/// </summary>
		PL,

		/// <summary>
		/// Part Unload
		/// </summary>
		PU,

		/// <summary>
		/// Un Load
		/// </summary>
		UL,
	}
}
