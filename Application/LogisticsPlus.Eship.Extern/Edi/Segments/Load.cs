﻿using System.Collections.Generic;

namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
	/// <summary>
	/// Edi 204 Load details - loop 100
	/// </summary>
	public class Load
	{
		public BeginningSegmentShipmentInfo BeginningSegment { get; set; }
		public SetPurpose SetPurpose { get; set; }
		public string ExpirationDate { get; set; }
		public List<ReferenceNumber> LoadReferenceNumbers { get; set; }
		public List<HandlingRequirement> HandlingRequirements { get; set; } 
		public Remarks Remarks { get; set; }
		public EquipmentDetail EquipmentDetail { get; set; }
		public List<Edi204Stop> Stops { get; set; }
		public ShipmentWghtPkgAndQtyData ShipmentWeightAndTrackingData { get; set; }
	}
}
