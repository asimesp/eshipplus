﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
    public class BeginningSegmentShipStatMsg
    {
    	public string ShipmentIdentificationNumber { get; set; }
    	public string StandardCarrierAlphaCode { get; set; }
    	public string ReferenceIdentification { get; set; }
    	public RefIdQualifier ReferenceIdentificationQualifier { get; set; }
    }
}
