﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
    public class ShipmentWghtPkgAndQtyData
    {
        public WghtUnitQualifier WeightUnitCode { get; set; }
        public decimal Weight { get; set; }
        public int LadingPieces { get; set; }
        public int LadingPackages { get; set; }
    }
}
