﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
    public class OrderIdentificationDetail
    {
        public string ReferenceIdentification { get; set; } 
        public int Quantity { get; set; }
		public int NumberOfIndividualUnitsShipped { get; set; }
        public WghtUnitQualifier WeightUnitCode { get; set; } 
        public decimal Weight { get; set; } 
        public decimal Length { get; set; } 
        public decimal Width { get; set; } 
        public decimal Height { get; set; }
		public DimsUnitQualifier DimensionUnitQualifier { get; set; }
		public string NMFCCode { get; set; }
		public string ItemDescription { get; set; }
		public string CommodityCode { get; set; }
		public string PackageTypeCode { get; set; }
		public string FreightClass { get; set; }
    }
}
