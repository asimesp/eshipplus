﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
    public class SetPurpose
    {
        public SetPurposeTransType TransactionSetPurpose { get; set; }
        public SetPurposeAppType ApplicationType { get; set; }
    }
}
