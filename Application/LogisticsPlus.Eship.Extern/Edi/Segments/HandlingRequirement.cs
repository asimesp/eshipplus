﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
    public class HandlingRequirement
    {
        public string SpecialHandlingCode { get; set; } 
        public string SpecialServicesCode { get; set; }
        public string SpecialHandlingDescription{ get; set; }
    }
}