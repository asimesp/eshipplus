﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
	public enum ContactFuncCode
	{
		/// <summary>
		/// Customer contact granting appointment
		/// </summary>
		CA,

		/// <summary>
		/// Confirmed with
		/// </summary>
		CW,

		/// <summary>
		/// Delivery contact
		/// </summary>
		DC,

		/// <summary>
		/// General contact
		/// </summary>
		GC,

		/// <summary>
		/// Schedule contact
		/// </summary>
		SC,

		/// <summary>
		/// Hazardous Material
		/// </summary>
		HZ,
	}
}
