﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
	public enum DimsUnitQualifier
	{
		/// <summary>
		/// Inches
		/// </summary>
		I,
	}
}
