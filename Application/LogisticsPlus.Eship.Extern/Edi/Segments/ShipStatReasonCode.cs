﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
	public enum ShipStatReasonCode
	{
		/// <summary>
		/// Missed Delivery
		/// </summary>
        A1,
		
		/// <summary>
		/// Incorrect Address
		/// </summary>
		A2, 

		/// <summary>
		/// Indirect Delivery
		/// </summary>
	    A3, 
	
		/// <summary>
		/// Unable to Locate
		/// </summary>
		A5, 
	
		/// <summary>
		/// Address Corrected - Delivery Attempted
		/// </summary>
		A6, 
	
		/// <summary>
		/// Mis-sort
		/// </summary>
		AA,
	
		/// <summary>
		/// Customer Requested Future Delivery
		/// </summary>
		AD,
 
		/// <summary>
		/// Restricted Articles Unacceptable
		/// </summary>
		AE, 
	
		/// <summary>
		/// Accident
		/// </summary>
		AF, 
	
		/// <summary>
		/// Consignee Related
		/// </summary>
		AG, 
	
		/// <summary>
		/// Driver Related
		/// </summary>
		AH, 
	
		/// <summary>
		/// Mechanical Breakdown
		/// </summary>
		AI, 
	
		/// <summary>
		/// Other Carrier Related
		/// </summary>
		AJ ,
	
		/// <summary>
		/// Damaged, Rewrapped in Hub
		/// </summary>
		AK, 
	
		/// <summary>
		/// Previous Stop
		/// </summary>
		AL, 
	
		/// <summary>
		/// Shipper Related
		/// </summary>
		AM, 
	
		/// <summary>
		/// Holiday - Closed
		/// </summary>
		AN,
	
		/// <summary>
		/// Weather or Natural Disaster Related
		/// </summary>
		AO,

		/// <summary>
		/// Improper International Paperwork
		/// </summary>
		AR,
	
		/// <summary>
		/// Hold Due to Customs Documentation Problems
		/// </summary>
		AS, 
	
		/// <summary>
		/// Unable to Contact Recipient for Broker Information
		/// </summary>
		AT,
	
		/// <summary>
		/// Civil Event Related Delay
		/// </summary>
		AU,

		/// <summary>
		/// Exceeds Service Limitations
		/// </summary>
		AV,
	
		/// <summary>
		/// Past Cut-off Time
		/// </summary>
		AW,
	
		/// <summary>
		/// Insufficient Pick-up Time
		/// </summary>
		AX,
	
		/// <summary>
		/// Missed Pick-up
		/// </summary>
		AY,
	
		/// <summary>
		/// Alternate Carrier Delivered
		/// </summary>
		AZ,
	
		/// <summary>
		/// Consignee Closed
		/// </summary>
		B1,
	
		/// <summary>
		/// Trap for Customer
		/// </summary>
		B2,
	
		/// <summary>
		/// Held for Payment
		/// </summary>
		B4,

		/// <summary>
		/// Held for Consignee
		/// </summary>
		B5,
	
		/// <summary>
		/// Improper Unloading Facility or Equipment
		/// </summary>
		B8, 
	
		/// <summary>
		/// Receiving Time Restricted
		/// </summary>
		B9,
	
		/// <summary>
		/// Held per Shipper
		/// </summary>
		BB,
	
		/// <summary>
		/// Missing Documents
		/// </summary>
		BC,
	
		/// <summary>
		/// Border Clearance
		/// </summary>
		BD,
	
		/// <summary>
		/// Road Conditions
		/// </summary>
		BE,
	
		/// <summary>
		/// Carrier Keying Error
		/// </summary>
		BF,

		/// <summary>
		/// Other
		/// </summary>
		BG,
	
		/// <summary>
		/// Insufficient Time to Complete Delivery
		/// </summary>
		BH,

		/// <summary>
		/// Cartage Agent
		/// </summary>
		BI,
	
		/// <summary>
		/// Customer Wanted Earlier Delivery
		/// </summary>
		BJ,

		/// <summary>
		/// Prearranged Appointment
		/// </summary>
		BK,
	
		/// <summary>
		/// Held for Protective Service
		/// </summary>
		BL,
	
		/// <summary>
		/// Flatcar Shortage
		/// </summary>
		BM,

		/// <summary>
		/// Failed to Release Billing
		/// </summary>
		BN,

		/// <summary>
		/// Railroad Failed to Meet Schedule
		/// </summary>
		BO,

		/// <summary>
		/// Load Shifted
		/// </summary>
		BP,

		/// <summary>
		/// Shipment Overweight
		/// </summary>
		BQ,

		/// <summary>
		/// Train Derailment
		/// </summary>
		BR,

		/// <summary>
		/// Refused by Customer
		/// </summary>
		BS,
	
		/// <summary>
		/// Returned to Shipper
		/// </summary>
		BT,
	
		/// <summary>
		/// Waiting for Customer Pick-up
		/// </summary>
		C1,
	
		/// <summary>
		/// Credit Hold
		/// </summary>
		C2,
	
		/// <summary>
		/// Suspended at Customer Request
		/// </summary>
		C3,
	
		/// <summary>
		/// Customer Vacation
		/// </summary>
		C4,
	
		/// <summary>
		/// Customer Strike
		/// </summary>
		C5,
	
		/// <summary>
		/// Waiting Shipping Instructions
		/// </summary>
		C6,
	
		/// <summary>
		/// Waiting for Customer Specified Carrier
		/// </summary>
		C7,
	
		/// <summary>
		/// Collect on Delivery Required
		/// </summary>
		C8,
	
		/// <summary>
		/// Cash Not Available From Consignee
		/// </summary>
		C9,
	
		/// <summary>
		/// Customer (Import or Export)
		/// </summary>
		CA,

		/// <summary>
		/// No Requested Arrival Date Provided by Shipper
		/// </summary>
		CB,
	
		/// <summary>
		/// No Requested Arrival Time Provided by Shipper
		/// </summary>
		CC,
	
		/// <summary>
		/// Carrier Dispatch Error
		/// </summary>
		D1,
	
		/// <summary>
		/// Driver Not Available
		/// </summary>
		D2,
	
		/// <summary>
		/// Non-Express Clearance Delay
		/// </summary>
		F1,

		/// <summary>
		/// International Non-carrier Delay
		/// </summary>
		F2,

		/// <summary>
		/// Held Pending Appointment
		/// </summary>
		HB,

		/// <summary>
		/// Normal Appointment
		/// </summary>
		NA,

		/// <summary>
		/// Normal Status
		/// </summary>
		NS,
	
		/// <summary>
		/// Processing Delay
		/// </summary>
		P1,
	
		/// <summary>
		/// Waiting Inspection
		/// </summary>
		P2,
	
		/// <summary>
		/// Production Falldown
		/// </summary>
		P3,
	
		/// <summary>
		/// Held for Full Carrier Load
		/// </summary>
		P4,
	
		/// <summary>
		/// Reconsigned
		/// </summary>
		RC,
	
		/// <summary>
		/// Delivery Shortage
		/// </summary>
		S1,
	
		/// <summary>
		/// Tractor With Sleeper Car Not Available
		/// </summary>
		T1,

		/// <summary>
		/// Tractor, Conventional, Not Available
		/// </summary>
		T2,
	
		/// <summary>
		/// Trailer not Available
		/// </summary>
		T3,
	
		/// <summary>
		/// Trailer Not Usable Due to Prior Product
		/// </summary>
		T4,
	
		/// <summary>
		/// Trailer Class Not Available
		/// </summary>
		T5,
	
		/// <summary>
		/// Trailer Volume Not Available
		/// </summary>
		T6,
	
		/// <summary>
		/// Insufficient Delivery Time
		/// </summary>
		T7,
	}
}
