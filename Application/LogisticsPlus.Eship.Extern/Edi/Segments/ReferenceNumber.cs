﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
    public class ReferenceNumber
    {
    	public RefIdQualifier ReferenceIdentificationQualifier { get; set; }
    	public string ReferenceIdentification { get; set; }
    	public string Description { get; set; }
    }
}
