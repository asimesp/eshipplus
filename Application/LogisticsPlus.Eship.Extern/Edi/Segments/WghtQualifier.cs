﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
	public enum WghtQualifier
	{
		/// <summary>
		/// Billed Weight
		/// </summary>
		B,

		/// <summary>
		/// Deficit Weight
		/// </summary>
		F,

		/// <summary>
		/// Net Weight
		/// </summary>
		N,

		/// <summary>
		/// Tare Weight
		/// </summary>
		T
	}
}
