﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
	public enum ShipMethodOfPay
	{
		/// <summary>
		/// Collect
		/// </summary>
		CC,

		/// <summary>
		/// Prepaid
		/// </summary>
		PP,

		/// <summary>
		/// Third party
		/// </summary>
		TP,

	}
}
