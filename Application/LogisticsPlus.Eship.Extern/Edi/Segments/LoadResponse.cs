﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
	/// <summary>
	/// Edi 990 - Response to Load tender - loop 100
	/// </summary>
	public class LoadResponse
	{
		public BeginningSegmentBooking BeginningSegment { get; set; }
		public ReferenceNumber ReferenceIdentification { get; set; }
		public string ResponseToSetControlNumber { get; set; }
	}
}
