﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
	public enum ShipStatMsgCode
	{
		/// <summary>
		/// Pick up appointment date and/or time
		/// </summary>
		AA,

		/// <summary>
		/// Delivery appointment date and/or time
		/// </summary>
		AB,

		/// <summary>
		/// shipment returned to shipper
		/// </summary>
		A3,

		/// <summary>
		/// refused by consignee
		/// </summary>
		A7,

		/// <summary>
		/// shipment damaged
		/// </summary>
		A9,

		/// <summary>
		/// carrier departed pickup-up locaiton with shipment
		/// </summary>
		AF,

		/// <summary>
		/// estimated delivery
		/// </summary>
		AG,

		/// <summary>
		/// attempted delivery
		/// </summary>
		AH,

		/// <summary>
		/// shipment has been reconsigned
		/// </summary>
		AI,

		/// <summary>
		/// tendered for delivery
		/// </summary>
		AJ,

		/// <summary>
		/// loaded on truck
		/// </summary>
		AM,

		/// <summary>
		/// diverted to air carrier
		/// </summary>
		AN,

		/// <summary>
		/// delivery not completed
		/// </summary>
		AP,


		/// <summary>
		/// rail arrival at destination intermodal ramp
		/// </summary>
		AR,

		/// <summary>
		/// available for delivery
		/// </summary>
		AV,

		/// <summary>
		/// estimated to arrive at carrier terminal
		/// </summary>
		B6,

		/// <summary>
		/// connecting line or cartage pick-up
		/// </summary>
		BA,

		/// <summary>
		/// storage in transit
		/// </summary>
		BC,

		/// <summary>
		/// estimated to depart terminal location
		/// </summary>
		C1,

		/// <summary>
		/// shipment cancelled
		/// </summary>
		CA,

		/// <summary>
		/// carrier departed delivery location
		/// </summary>
		CD,

		/// <summary>
		/// trailer closed out
		/// </summary>
		CL,

		/// <summary>
		/// completed loading at pick-up location
		/// </summary>
		CP,

		/// <summary>
		/// completed unloading at delivery location
		/// </summary>
		D1,

		/// <summary>
		/// in-gate
		/// </summary>
		I1,

		/// <summary>
		/// delivered to connecting line
		/// </summary>
		J1,


		/// <summary>
		/// arrived at customs
		/// </summary>
		K1,

		/// <summary>
		/// loading
		/// </summary>
		L1,

		/// <summary>
		/// out-gate
		/// </summary>
		OA,

		/// <summary>
		/// paperwork received - did not recieve shipment or equipment
		/// </summary>
		OO,

		/// <summary>
		/// departed terminal location
		/// </summary>
		P1,

		/// <summary>
		/// U.S. customs hold at origin intermodal ramp
		/// </summary>
		PR,

		/// <summary>
		/// received from prior carrier
		/// </summary>
		R1,

		/// <summary>
		/// rail departure from origin intermodal ramp
		/// </summary>
		RL,

		/// <summary>
		/// trailer spotted at consignee's location
		/// </summary>
		S1,

		/// <summary>
		/// shipment delayed
		/// </summary>
		SD,

		/// <summary>
		/// arrived at delivery location
		/// </summary>
		X1,

		/// <summary>
		/// estimated date and/or time of arrival at consignee's location
		/// </summary>
		X2,

		/// <summary>
		/// arrived at pick-up location
		/// </summary>
		X3,

		/// <summary>
		/// arrived at terminal location
		/// </summary>
		X4,

		/// <summary>
		/// arrived at delivery location loading dock
		/// </summary>
		X5,

		/// <summary>
		/// en route to delivery location
		/// </summary>
		X6,

		/// <summary>
		/// arrived at pick-up locaiton loading dock
		/// </summary>
		X8,

		/// <summary>
		/// shipment acknowledged
		/// </summary>
		XB,
	}
}
