﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
    public class AddressInformation
    {
        public string AddressInfo1 { get; set; }
        public string AddressInfo2 { get; set; }
    }
}
