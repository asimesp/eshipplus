﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
	public enum EntityIdCode
	{
		/// <summary>
		///  Bill to
		/// </summary>
		BT,

		/// <summary>
		/// Consignee
		/// </summary>
		CN,

		/// <summary>
		/// Ship from
		/// </summary>
		SF,

		/// <summary>
		/// Ship to
		/// </summary>
		ST,

		/// <summary>
		/// Shipper
		/// </summary>
		SH,

		/// <summary>
		/// Payor
		/// </summary>
		PR,

		/// <summary>
		/// Origin Terminal
		/// </summary>
		OT,

		/// <summary>
		/// Destination Terminal
		/// </summary>
		DT,

		/// <summary>
		/// Event Location
		/// </summary>
		EL
	}
}
