﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
	public enum RefIdQualifier
	{
		// KEEP A-Z

		/// <summary>
		/// Booking Number
		/// </summary>
		BRQ,

		/// <summary>
		/// Custom
		/// </summary>
		CUS,

		/// <summary>
		/// Invoice Number
		/// </summary>
		INV,

		/// <summary>
		/// Pickup confirmation number
		/// </summary>
		ZH,

		/// <summary>
		/// PO Number
		/// </summary>
		PO,

		/// <summary>
		/// Pro/tracking number
		/// </summary>
		CN,

		/// <summary>
		/// Quote Number
		/// </summary>
		QUO,

		/// <summary>
		/// Shipper Bill of Lading
		/// </summary>
		SBL,

		/// <summary>
		/// Shipment Number
		/// </summary>
		SHP,

		/// <summary>
		/// Shipper Reference
		/// </summary>
		SHR,
	}
}
