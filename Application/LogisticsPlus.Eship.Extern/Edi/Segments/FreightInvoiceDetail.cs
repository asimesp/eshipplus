﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
    /// <summary>
	/// Motor Carrier Freight Details and Invoice - loop 100
    /// </summary>
    public class FreightInvoiceDetail
    {
		public decimal Weight { get; set; }
		public WghtUnitQualifier WeightUnitCode { get; set; }
		public int LadingQuantity { get; set; }
		public int LadingPieces { get; set; }

		public decimal Charge { get; set; }
		public string ChargeCode { get; set; }
		public string ChargeCodeDescription { get; set; }
        
        public Remarks Remarks { get; set; }
    }
}
