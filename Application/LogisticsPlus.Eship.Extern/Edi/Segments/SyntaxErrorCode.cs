﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
	public enum SyntaxErrorCode
	{
		/// <summary>
		/// Not Supported
		/// </summary>
		E1, 

		/// <summary>
		/// Trailer Missing
		/// </summary>
		E2, 

		/// <summary>
		/// Control Number in Header and Trailer Do Not Match
		/// </summary>
		E3, 

		/// <summary>
		/// One or More Segments in Error
		/// </summary>
		E5, 

		/// <summary>
		/// Missing or Invalid Identifier
		/// </summary>
		E6,

		/// <summary>
		///  Missing or Invalid Control Number
		/// </summary>
		E7,

		/// <summary>
		/// Syntax Error in Decrypted Text
		/// </summary>
		E16, 

		/// <summary>
		/// Control Number Not Unique within the Functional Group
		/// </summary>
		E23, 

		/// <summary>
		/// Not Applicable
		/// </summary>
		NA,
	}
}
