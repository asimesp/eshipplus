﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
	public enum ReservationCode
	{
		/// <summary>
		/// Accepted
		/// </summary>
		A,

		/// <summary>
		/// Declined
		/// </summary>
		D
	}
}
