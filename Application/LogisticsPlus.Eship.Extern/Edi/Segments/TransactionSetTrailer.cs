﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
    public class TransactionSetTrailer
    {
        public int NumberOfIncludedSegments { get; set; }
        public string TransactionSetControlNumber { get; set; }
    }
}
