﻿using System.Collections.Generic;

namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
	/// <summary>
	/// Edi 210 - Freight Invoice Details
	/// </summary>
	public class FreightInvoice
	{
		public BeginningSegmentFreightInv BeginningSegment { get; set; }
		public List<ReferenceNumber> ReferenceNumbers { get; set; }
		public Remarks Remarks { get; set; }
		public List<Edi204Stop> Stops { get; set; }
		public List<FreightInvoiceDetail> FreightInvoiceDetails { get; set; }
	}
}
