﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
    public class EdiLocation
    {
        public Name LocationName { get; set; }
        public AddressInformation LocationStreetInformation { get; set; }
        public GeographicLocation GeographicLocation { get; set; }
    }
}
