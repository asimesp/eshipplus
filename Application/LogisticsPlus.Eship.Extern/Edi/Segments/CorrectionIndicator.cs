﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
	public enum CorrectionIndicator
	{
		/// <summary>
		/// Not applicable
		/// </summary>
		NA,

		/// <summary>
		/// Balance Due
		/// </summary>
		BD,

		/// <summary>
		/// Miscellaneous
		/// </summary>
		MB,

		/// <summary>
		/// Credit
		/// </summary>
		CR,
	}
}
