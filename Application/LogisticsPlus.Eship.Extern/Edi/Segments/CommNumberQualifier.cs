﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
	/// <summary>
	/// Communication number qualifier
	/// </summary>
	public enum CommNumberQualifier
	{
		/// <summary>
		/// Fax
		/// </summary>
		FX,
		
		/// <summary>
		/// Telephone
		/// </summary>
		TE,

		/// <summary>
		/// Email
		/// </summary>
		EM,
	}
}
