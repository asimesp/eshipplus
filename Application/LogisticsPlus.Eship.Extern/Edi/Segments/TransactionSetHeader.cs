﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
    public class TransactionSetHeader
    {
        public TransSetIdentifierCode TransactionSetIdentifierCode { get; set; }
        public string TransactionSetControlNumber { get; set; }
    }
}
