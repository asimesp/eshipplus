﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
	public enum AcknowledgementCode
	{
		/// <summary>
		/// Accepted
		/// </summary>
		A,

		/// <summary>
		/// Accepted But Errors Were Noted
		/// </summary>
		E,

		/// <summary>
		///  Rejected, Message Authentication Code (MAC) Failed
		/// </summary>
		M,

		/// <summary>
		///  Rejected
		/// </summary>
		R,

		/// <summary>
		/// Rejected, Assurance Failed Validity Tests
		/// </summary>
		W,

		/// <summary>
		/// Rejected, Content After Decryption Could Not Be Analyzed
		/// </summary>
		X,
	}
}
