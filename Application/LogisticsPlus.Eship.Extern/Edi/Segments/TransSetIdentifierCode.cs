﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
	public enum TransSetIdentifierCode
	{
		/// <summary>
		/// 210 Carrier Shipment invoice
		/// </summary>
		CSI,

		/// <summary>
		/// 204 Load tender
		/// </summary>
		LTR,

		/// <summary>
		/// 990 Response to load tender
		/// </summary>
		RLT,

		/// <summary>
		/// 214 Shipment status message
		/// </summary>
		SSM,


		/// <summary>
		/// 216 Motor Carrier Pickup Notification
		/// </summary>
		CPN,

		/// <summary>
		/// 997 Functional Acknowledgement
		/// </summary>
		ACK
	}
}
