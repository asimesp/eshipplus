﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
    public class BeginningSegmentFreightInv
    {
    	public ShipmentQualifier ShipmentQualifier { get; set; }
    	public string InvoiceNumber { get; set; }
    	public string ShipmentIdNumber { get; set; }
    	public ShipMethodOfPay ShipmentMethodOfPay { get; set; }
    	public WghtUnitQualifier WeightUnitCode { get; set; }
    	public string InvoiceDate { get; set; }
    	public decimal NetAmountDue { get; set; }
    	public CorrectionIndicator CorrectionIndicator { get; set; }
    	public string PickupDate { get; set; }
		public FreightInvDateQualifier PickupDateTimeQualifier { get; set; }
    	public string DeliveryDate { get; set; }
    	public FreightInvDateQualifier DeliveryDateTimeQualifier { get; set; }
    	public string StandardCarrierAlphaCode { get; set; }
		public string Date { get; set; }
		public EquipmentDetail Equipment { get; set; }
		public decimal Mileage { get; set; }

    	public string TariffServiceCode { get; set; }
    	public string TransportationTermsCode { get; set; }
    }
}
