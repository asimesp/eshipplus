﻿using System;
using System.Collections.Generic;

namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
	public class Edi204Stop
	{
		public StopOffDetails StopOffDetails { get; set; }
		public string DateTime { get; set; }
		public Name LocationName { get; set; }
		public AddressInformation LocationStreetInformation { get; set; }
		public GeographicLocation GeographicLocation { get; set; }
		public Contact LocationContact { get; set; }
		public List<OrderIdentificationDetail> OrderIdentificationDetails { get; set; }
		public bool IsHazardousMaterial { get; set; }
		public Contact HazardousMaterialContact { get; set; }
	}
}
