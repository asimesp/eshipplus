﻿namespace LogisticsPlus.Eship.Extern.Edi.Segments
{
    public class BeginningSegmentBooking
    {
        public string StandardCarrierAlphaCode { get; set; }    
        public string ShipmentIdentificationNumber { get; set; }    
        public string Date { get; set; }
        public ReservationCode ReservationActionCode { get; set; }
		public string DeclineReason { get; set; }
    }
}
