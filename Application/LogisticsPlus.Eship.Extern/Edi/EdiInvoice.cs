using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.Extern.Edi
{
	/// <summary>
	/// Data for EDI 210 documents
	/// </summary>
	[XmlRoot("Invoice")]
	public class EdiInvoice
	{
		
		public string ReferenceKey { get; set; }
		public string InvoiceNumber { get; set; }
		public string LoadNumber { get; set; }
		public string ActualCarrierUsedScac { get; set; }
		public string Pro { get; set; }
		public DateTime ShipmentDate { get; set; }
		public DateTime InvoiceDate { get; set; }
		public string ServiceType { get; set; }
		public string GeneralNotes { get; set; }
		public string PoNumber { get; set; }
		public string Reference { get; set; }
		public string FreightAccountToCharge { get; set; }
		public string NetTerms { get; set; }
		public DateTime InvoiceDueDate { get; set; }
		public string AdditionalInformation { get; set; }
		public string MethodOfPayment
		{
			get { return "3rd Party Prepaid"; }
			set { }
		}
		public string EquipmentType { get; set; }
		public string ActualDeliveryDate { get; set; }
		public string ActualPickupDate { get; set; }
		public EdiShipper Shipper { get; set; }
		public EdiConsignee Consignee { get; set; }
		public EdiMileage Mileage { get; set; }

		[XmlArrayItem("Charge")]
		public List<EdiCharge> Charges { get; set; }

		[XmlArrayItem("ShipmentItem")]
		public List<EdiShipmentItem> Items { get; set; }

		[XmlArrayItem("Stop")]
		public List<EdiStop> Stops { get; set; }

		public EdiInvoice()
		{
			Reference = string.Empty;
			ReferenceKey = string.Empty;
			InvoiceNumber = string.Empty;
			LoadNumber = string.Empty;
			ActualCarrierUsedScac = string.Empty;
			Pro = string.Empty;
			ShipmentDate = DateTime.Now;
			InvoiceDate = DateTime.Now;
			ServiceType = string.Empty;
			GeneralNotes = string.Empty;
			PoNumber = string.Empty;
			FreightAccountToCharge = string.Empty;
			NetTerms = string.Empty;
			InvoiceDueDate = DateTime.Now;
			AdditionalInformation = string.Empty;
			EquipmentType = string.Empty;
			ActualDeliveryDate = string.Empty;
			ActualPickupDate = string.Empty;
			Shipper = new EdiShipper();
			Consignee = new EdiConsignee();
			Mileage = new EdiMileage();
			Charges = new List<EdiCharge>();
			Items = new List<EdiShipmentItem>();
			Stops = new List<EdiStop>();
		}
	}
}