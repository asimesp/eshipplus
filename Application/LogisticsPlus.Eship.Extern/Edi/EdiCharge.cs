using System.Xml.Serialization;

namespace LogisticsPlus.Eship.Extern.Edi
{
	[XmlRoot("Charge")]
	public class EdiCharge
	{
		public float Amount { get; set; }
		public string Comments { get; set; }
		public string Code { get; set; }
		public string Description { get; set; }

		public EdiCharge()
		{
			Comments = string.Empty;
			Code = string.Empty;
			Description = string.Empty;
		}
	}
}	