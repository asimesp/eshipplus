﻿using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Extern.Edi.Segments;

namespace LogisticsPlus.Eship.Extern.Edi.Validators
{
	public class Edi214Validator
	{
		public bool SetControlNumbersMatch(Edi214 edi214)
		{
			return edi214.TransactionSetHeader.TransactionSetControlNumber ==
			       edi214.TransactionSetTrailer.TransactionSetControlNumber;
		}

		public bool NumberOfSegmentsMatch(Edi214 edi214)
		{
			return edi214.Statuses.Count == edi214.TransactionSetTrailer.NumberOfIncludedSegments;
		}

		public bool HasValidData(Edi214 edi214)
		{
			var valid = edi214.Statuses.All(s => s.DateTime.IsValidSystemDateTime());
			if (edi214.Statuses.Any(s => !string.IsNullOrEmpty(s.BeginningSegment.ReferenceIdentification)))
				valid = valid && edi214.Statuses
				                       .Where(s => !string.IsNullOrEmpty(s.BeginningSegment.ReferenceIdentification))
				                       .All(s => s.BeginningSegment.ReferenceIdentificationQualifier == RefIdQualifier.CN);
			return valid;
		}
	}
}
