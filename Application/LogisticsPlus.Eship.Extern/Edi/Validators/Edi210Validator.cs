﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Extern.Edi.Validators
{
	public class Edi210Validator
	{
		public bool SetControlNumbersMatch(Edi210 edi210)
		{
			return edi210.TransactionSetHeader.TransactionSetControlNumber ==
				   edi210.TransactionSetTrailer.TransactionSetControlNumber;
		}

		public bool NumberOfSegmentsMatch(Edi210 edi210)
		{
			return edi210.Invoices.Count == edi210.TransactionSetTrailer.NumberOfIncludedSegments;
		}

		public bool HasValidChargeCodes(Edi210 edi210, List<ChargeCode> chargeCodes)
		{
			return edi210.Invoices
			             .SelectMany(l => l.FreightInvoiceDetails)
			             .All(i => chargeCodes.Any(c => c.Code == i.ChargeCode));
		}

		public bool HasValidData(Edi210 edi210, Vendor vendor)
		{
			return edi210.Invoices
			             .All(i => i.BeginningSegment.StandardCarrierAlphaCode == vendor.Scac);
		}
	}
}
