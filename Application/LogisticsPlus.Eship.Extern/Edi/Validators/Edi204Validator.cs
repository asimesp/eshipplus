﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Extern.Edi.Segments;

namespace LogisticsPlus.Eship.Extern.Edi.Validators
{
	public class Edi204Validator
	{
		public bool SetControlNumbersMatch(Edi204 edi204)
		{
			return edi204.TransactionSetHeader.TransactionSetControlNumber ==
			       edi204.TransactionSetTrailer.TransactionSetControlNumber;
		}

		public bool NumberOfSegmentsMatch(Edi204 edi204)
		{
			return edi204.Loads.Count == edi204.TransactionSetTrailer.NumberOfIncludedSegments;
		}

		public bool HasValidFreightClasses(Edi204 edi204, List<string> freightClasses)
		{
			return edi204.Loads
			             .SelectMany(l => l.Stops)
			             .SelectMany(l => l.OrderIdentificationDetails)
			             .Where(i => !string.IsNullOrEmpty(i.FreightClass))
			             .All(i => freightClasses.Contains(i.FreightClass));
		}

		public bool HasValidHanldingRequirements(Edi204 edi204, List<Service> services)
		{
			return edi204.Loads
			             .SelectMany(l => l.HandlingRequirements)
			             .Where(s => !string.IsNullOrEmpty(s.SpecialServicesCode))
			             .All(i => services.Any(s => s.Code == i.SpecialServicesCode));
		}

		public bool HasValidCountries(Edi204 edi204, List<Country> countries)
		{
			return edi204.Loads
			             .SelectMany(l => l.Stops)
			             .All(s => countries.Any(c => c.Code == s.GeographicLocation.CountryCode));
		}

		public bool HasValidCounts(Edi204 edi204)
		{
			return edi204.Loads.All(l => l.Stops.Count >= 2) &&
			       edi204.Loads
			             .All(l =>
				             {
					             var stopSequences = l.Stops.Select(s => s.StopOffDetails.StopSequenceNumber).Distinct().ToList();
					             var oid = l.Stops.SelectMany(s => s.OrderIdentificationDetails);
					             var items = new Dictionary<string, OrderIdentificationDetail>();
					             foreach (var o in oid.Where(o => !items.ContainsKey(o.ReferenceIdentification)))
						             items.Add(o.ReferenceIdentification, o);

					             return l.ShipmentWeightAndTrackingData.Weight == items.Values.Sum(i => i.Weight) &&
					                    l.Stops.Count == stopSequences.Count;
				             });

		}
	}
}
