﻿using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Extern.Edi.Segments;

namespace LogisticsPlus.Eship.Extern.Edi.Validators
{
	public class Edi990Validator
	{
		public bool SetControlNumbersMatch(Edi990 edi990)
		{
			return edi990.TransactionSetHeader.TransactionSetControlNumber ==
				   edi990.TransactionSetTrailer.TransactionSetControlNumber;
		}

		public bool NumberOfSegmentsMatch(Edi990 edi990)
		{
			return edi990.Responses.Count == edi990.TransactionSetTrailer.NumberOfIncludedSegments;
		}

		public bool VendorScacMatches(Edi990 edi990, Vendor vendor)
		{
			return edi990.Responses
						 .All(r => r.BeginningSegment.StandardCarrierAlphaCode.ToLower() == vendor.Scac.ToLower());
		}

		public bool HasValidData(Edi990 edi990)
		{
			return edi990.Responses.All(r => r.BeginningSegment.ReservationActionCode == ReservationCode.A && !string.IsNullOrEmpty(r.ReferenceIdentification.ReferenceIdentification));
		}
	}
}
