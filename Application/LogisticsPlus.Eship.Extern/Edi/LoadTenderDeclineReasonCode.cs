﻿namespace LogisticsPlus.Eship.Extern.Edi
{
    public enum LoadTenderDeclineReasonCode
    {
        // DO NOT RE-ARRANGE!  ADD TO LIST ONLY
        NoAppointAvailable,
        NoCapacity,
        DamagedEquipment,
        NoEquipmentAvailable,
        InsufficientLeadTime,
        InsufficientTransitTime,
        Other
    }
}
