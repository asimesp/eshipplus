﻿using LogisticsPlus.Eship.Extern.Edi.Segments;

namespace LogisticsPlus.Eship.Extern.Edi
{
	/// <summary>
	/// Funcational Acknowledgement
	/// </summary>
	public class Edi997
	{
		public TransactionSetHeader TransactionSetHeader { get; set; }
		public FuncIdentifierCode FunctionalIdentifierCode { get; set; }

		public TransSetIdentifierCode SetIdentifierCode { get; set; }
		public string SetControlNumber { get; set; }
		public AcknowledgementCode SetAcknowledgementCode { get; set; }
		public SyntaxErrorCode SetSyntaxErrorCode { get; set; }

		public TransactionSetTrailer TransactionSetTrailer { get; set; }

	}
}
