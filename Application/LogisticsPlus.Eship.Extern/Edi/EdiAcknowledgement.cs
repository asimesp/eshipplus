﻿using System.Xml.Serialization;

namespace LogisticsPlus.Eship.Extern.Edi
{
	/// <summary>
	/// Data for EDI 997 documents
	/// </summary>
	[XmlRoot("Item997")]
	public class EdiAcknowledgement
	{
		public string SequenceNumber { get; set; }
		public string DocumentTypeSent { get; set; }
		public string Status { get; set; }

		public EdiAcknowledgement()
		{
			Status = string.Empty;
			DocumentTypeSent = string.Empty;
			SequenceNumber = string.Empty;
		}
	}
}
