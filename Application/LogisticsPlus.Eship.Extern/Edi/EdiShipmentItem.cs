using System.Xml.Serialization;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;

namespace LogisticsPlus.Eship.Extern.Edi
{
	[XmlRoot("ShipmentItem")]
	public class EdiShipmentItem
	{
		public string Description { get; set; }
		public string PackageType { get; set; }
		public int Quantity { get; set; }
		public int PiecesInEachPackage { get; set; }
		public float Weight { get; set; }
		public float Length { get; set; }
		public float Width { get; set; }
		public float Height { get; set; }
		public string ActualClass { get; set; }
		public string Notes { get; set; }
		public string PickUpLocation { get; set; }
		public string DeliveryLocation { get; set; }

		public EdiShipmentItem()
		{
			Description = string.Empty;
			PackageType = string.Empty;
			Notes = string.Empty;
			ActualClass = string.Empty;
			PickUpLocation = string.Empty;
			DeliveryLocation = string.Empty;
		}

		public EdiShipmentItem(ShipmentItem item, int lastStopIndex)
		{
			if (item == null) return;

			Description = item.Description;
			PackageType = item.PackageType == null ? string.Empty : item.PackageType.TypeName;
			Quantity = item.Quantity;
			PiecesInEachPackage = item.PieceCount;
			Weight = (float)item.ActualWeight.ToDouble();
			Length = (float)item.ActualLength.ToDouble();
			Width = (float)item.ActualWidth.ToDouble();
			Height = (float)item.ActualHeight.ToDouble();
			ActualClass = item.ActualFreightClass.ToString();
			Notes = item.Comment;
			PickUpLocation = item.Pickup == 0 ? "Shipper" : string.Format("Stop {0}", item.Pickup);
			DeliveryLocation = item.Delivery == lastStopIndex ? "Consignee" : string.Format("Stop {0}", item.Delivery);
		}

		public EdiShipmentItem(ServiceTicketItem item)
		{
			if (item == null) return;

			Description = item.Description;
			PackageType = string.Empty;
			Quantity = 1;
			PiecesInEachPackage = 0;
			Weight = 0;
			Length = 0;
			Width = 0;
			Height = 0;
			ActualClass = string.Empty;
			Notes = item.Comment;
			PickUpLocation = string.Empty;
			DeliveryLocation = string.Empty;
		}
	}
}