﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Extern.Edi.Segments;

namespace LogisticsPlus.Eship.Extern.Edi
{
	/// <summary>
	/// Trasnportation Carrier Load Tender
	/// </summary>
	public class Edi204
	{
		public TransactionSetHeader TransactionSetHeader { get; set; }
		public List<Load> Loads { get; set; }
		public TransactionSetTrailer TransactionSetTrailer { get; set; }
	}
}
