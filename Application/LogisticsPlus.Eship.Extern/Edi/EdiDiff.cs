﻿namespace LogisticsPlus.Eship.Extern.Edi
{
    public class EdiDiff
    {
        public string Description { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }

        public override string ToString()
        {
            return string.Format("{0} changed from {1} to {2}", Description, OldValue, NewValue);
        }
    }
}
