﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Extern.Edi.Segments;

namespace LogisticsPlus.Eship.Extern.Edi
{
	/// <summary>
	/// Transportation Carrier Shipment Status Message
	/// </summary>
	public class Edi214
	{
		public TransactionSetHeader TransactionSetHeader { get; set; }
		public List<ShipmentStatus> Statuses { get; set; }
		public TransactionSetTrailer TransactionSetTrailer { get; set; }
	}
}
