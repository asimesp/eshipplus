using System.Linq;
using System.Xml.Serialization;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.Extern.Edi
{
	[XmlRoot("Consignee")]
	public class EdiConsignee
	{
		public string Name { get; set; }
		public string Street1 { get; set; }
		public string Street2 { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string PostalCode { get; set; }
		public string Country { get; set; }
		public string Contact { get; set; }
		public string Phone { get; set; }
		public string Email { get; set; }
		public string Fax { get; set; }

		public EdiConsignee()
		{

			Name = string.Empty;
			Street1 = string.Empty;
			Street2 = string.Empty;
			City = string.Empty;
			State = string.Empty;
			PostalCode = string.Empty;
			Country = string.Empty;

			Contact = string.Empty;
			Phone = string.Empty;
			Email = string.Empty;
			Fax = string.Empty;
		}

		public EdiConsignee(ShipmentLocation location)
		{
			if (location == null) return;

			Name = location.Description;
			Street1 = location.Street1;
			Street2 = location.Street2;
			City = location.City;
			State = location.State;
			PostalCode = location.PostalCode;
			Country = location.Country == null ? string.Empty : location.Country.Code;

			var contact = location.Contacts.FirstOrDefault(c => c.Primary) ?? location.Contacts.FirstOrDefault();

			Contact = contact == null ? string.Empty : contact.Name;
			Phone = contact == null ? string.Empty : contact.Phone;
			Email = contact == null ? string.Empty : contact.Email;
			Fax = contact == null ? string.Empty : contact.Fax;		
		}
	}
}