﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.Extern.Edi
{
	/// <summary>
	/// Data for EDI 214 documents
	/// </summary>
	[XmlRoot("ShipmentStatus")]
	public class EdiShipmentStatus
	{
		public string ReferenceKey { get; set; }
		public string ShipmentNumber { get; set; }
		public string ShipperBol { get; set; }
		public EdiShipper Shipper { get; set; }
		public EdiConsignee Consignee { get; set; }
		public float ShipmentWeight { get; set; }
		public int ShipmentPackageQuantity { get; set; }
		public DateTime StatusDate { get; set; }
		public string Status { get; set; }
		public string StatusMessage { get; set; }
		public string ScacCode { get; set; }
		public string ProNumber { get; set; }
		public string TrailerNumber { get; set; }
		public string TerminalLocation { get; set; }

		public EdiShipmentStatus()
		{
			ReferenceKey = string.Empty;
			ShipmentNumber = string.Empty;
			ShipperBol = string.Empty;
			Shipper = new EdiShipper();
			Consignee = new EdiConsignee();
			StatusDate = DateTime.Now;
			Status = string.Empty;
			StatusMessage = string.Empty;
			ScacCode = string.Empty;
			ProNumber = string.Empty;
			TrailerNumber = string.Empty;
			TerminalLocation = string.Empty;
		}
	}
}
