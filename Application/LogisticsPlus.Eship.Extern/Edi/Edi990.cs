﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Extern.Edi.Segments;

namespace LogisticsPlus.Eship.Extern.Edi
{
	/// <summary>
	/// Transportation Carrier Response to Load Tender
	/// </summary>
	public class Edi990
	{
		public TransactionSetHeader TransactionSetHeader { get; set; }
		public List<LoadResponse> Responses { get; set; }
		public TransactionSetTrailer TransactionSetTrailer { get; set; }
	}
}
