﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.Extern.Edi
{
    /// <summary>
    /// Data for EDI 211 documents
    /// </summary>
    [XmlRoot("Shipment")]
    public class EdiShipment
    {
        public string ReferenceKey { get; set; }
        public ShipmentStatus ShipmentStatus { get; set; }
        public string ActualCarrierUsedScac { get; set; }
    	public string MethodOfPayment
    	{
			get { return "3rd Party Prepaid "; }
    		set { }
    	}
        public string ShipperBol { get; set; }
        public string ShipmentNumber { get; set; }
		public DateTime ShipmentDate { get; set; }
        public string EquipmentType { get; set; }
    	public string ActualDeliveryDate { get; set; }
    	public string ActualPickupDate { get; set; }
        public string ServiceType { get; set; }
    	public string Reference { get; set; }
		public string PoNumber { get; set; }
    	public bool IsHazMat { get; set; }
        public string HazMatContact { get; set; }
        public string HazMatPhone { get; set; }
		
		public EdiShipper Shipper { get; set; }
		public EdiConsignee Consignee { get; set; }
        
		[XmlArrayItem("Charge")]
		public List<EdiCharge> Charges { get; set; }

		[XmlArrayItem("Stop")]
        public List<EdiStop> Stops { get; set; }

		[XmlArrayItem("ShipmentItem")]
        public List<EdiShipmentItem> Items { get; set; }

    	public EdiShipment()
    	{
			Charges = new List<EdiCharge>();
			Stops = new List<EdiStop>();
			Items = new List<EdiShipmentItem>();

    		ReferenceKey = string.Empty;
    		ActualCarrierUsedScac = string.Empty;
			ActualDeliveryDate = string.Empty;
			ActualPickupDate = string.Empty;
			Consignee = new EdiConsignee();
			EquipmentType = string.Empty;
    		HazMatContact = string.Empty;
			HazMatPhone = string.Empty;
    		IsHazMat = false;
			MethodOfPayment = string.Empty;
			PoNumber = string.Empty;
			Reference = string.Empty;
			ServiceType = string.Empty;
    		ShipmentDate = DateTime.Now;
			ShipmentNumber = string.Empty;
			ShipmentStatus = ShipmentStatus.Scheduled;
			Shipper = new EdiShipper();
			ShipperBol = string.Empty;
    	}
    }
}
