﻿using System.Xml.Serialization;

namespace LogisticsPlus.Eship.Extern.Edi
{
	[XmlRoot("Mileage")]
	public class EdiMileage
	{
		public float Direct { get; set; }
		public float Indirect { get; set; }
	}
}
