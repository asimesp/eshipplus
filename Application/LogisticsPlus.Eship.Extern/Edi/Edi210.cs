﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Extern.Edi.Segments;

namespace LogisticsPlus.Eship.Extern.Edi
{
	/// <summary>
	/// Motor Carrier Freight Details and Invoice
	/// </summary>
	public class Edi210
	{
		public TransactionSetHeader TransactionSetHeader { get; set; }
		public List<FreightInvoice> Invoices { get; set; }
		public TransactionSetTrailer TransactionSetTrailer { get; set; }
	}
}
