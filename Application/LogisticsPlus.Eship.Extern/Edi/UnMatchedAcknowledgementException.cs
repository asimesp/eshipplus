﻿using System;

namespace LogisticsPlus.Eship.Extern.Edi
{
	public class UnMatchedAcknowledgementException: Exception
	{
		private const string DefaultMessage = "Unable to match acknowledgement to transmission record.";

		public UnMatchedAcknowledgementException() : base(DefaultMessage)
		{
		}

		public UnMatchedAcknowledgementException(string message) : base(string.Format("{0} [{1}]", DefaultMessage, message))
		{
		}
	}
}
