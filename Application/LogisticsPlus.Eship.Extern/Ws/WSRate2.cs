﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSRate2 
	{
		public string CarrierKey { get; set; }
		public string CarrierName { get; set; }
		public string CarrierScac { get; set; }
		
		public decimal BilledWeight { get; set; }
		public decimal RatedWeight { get; set; }
		public decimal RatedCubicFeet { get; set; }
		public int TransitTime { get; set;}
		public DateTime EstimatedDeliveryDate { get; set; }
		public ServiceMode ServiceMode { get; set; }

		public decimal FreightCharges { get; set; }
		public decimal FuelCharges { get; set; }
		public decimal AccessorialCharges { get; set; }
		public decimal ServiceCharges { get; set; }
		public decimal TotalCharges { get; set; }

		public decimal Mileage { get; set; }
		public long MileageSourceKey { get; set; }
		public string MileageSourceDescription { get; set; }

		public WSBillingDetail[] BillingDetails { get; set; }

		public WSBillingDetail[] GuarOptions { get; set; }
		public WSBillingDetail SelectedGuarOption { get; set; }

		public WSRate2()
		{
			CarrierKey = null;
			CarrierName = null;
			CarrierScac = null;
			
			BilledWeight = 0m;
			TransitTime = 0;
			EstimatedDeliveryDate = DateUtility.SystemEarliestDateTime;
			ServiceMode = ServiceMode.NotApplicable;


			FreightCharges = 0m;
			FuelCharges = 0m;
			AccessorialCharges = 0m;
			ServiceCharges = 0m;
			TotalCharges = 0m;

			Mileage = 0m;
			MileageSourceKey = default(long);
			MileageSourceDescription = null;

			BillingDetails = null;
		}
	}
}
