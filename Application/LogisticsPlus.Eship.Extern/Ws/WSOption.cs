﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSOption
	{
		public string Key { get; set; }
		public string Value { get; set; }

        
		public bool Equals(WSOption other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return Equals(other.Key, Key);
		}
		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != typeof (WSOption)) return false;
			return Equals((WSOption) obj);
		}
		public override int GetHashCode()
		{
			return (Key != null ? Key.GetHashCode() : 0);
		}
	}
}
