﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSShipment
	{
		public WSCredentials Credentials { get; set; }

		public WSLocation Origin { get; set; }
		public WSLocation Destination { get; set; }

		public string ReferenceNumber { get; set; }
		public string PurchaseOrder { get; set; }
		public bool ByPassPurchaseOrderValidation { get; set; }

		public DateTime ShipmentDate { get; set; }
		public WSTime EarliestPickup { get; set; }
		public WSTime LatestPickup { get; set; }

		public WSItem[] Items { get; set; }

		public WSOption[] RequestedServices { get; set; }

		public WSEnums.Mode[] RequestedModes { get; set; }
		public WSRate[] AvailableRates { get; set; }
		public WSRate SelectedRate { get; set; }
		public WSRate BookedRate { get; set; }

		public string BookedShipmentNumber { get; set; }

		public bool AdditionalInsuranceRequested { get; set; }

		public WSDocument[] ReturnConfirmations { get; set; }

		public WSPackageShipmentDetails PackageShipmentDetails { get; set; }

		public WSShipment()
		{
			Credentials = null;
			
			Origin = null;
			Destination = null;

			ReferenceNumber = string.Empty;
			PurchaseOrder = string.Empty;
			ByPassPurchaseOrderValidation = false;

			ShipmentDate = DateTime.Now.Date;
			EarliestPickup = null;
			LatestPickup = null;

			Items = null;

			RequestedModes = null;
			AvailableRates = null;
			SelectedRate = null;
			BookedRate = null;
			BookedShipmentNumber = string.Empty;

			ReturnConfirmations = null;

			PackageShipmentDetails = null;
		}
	}
}
