﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSCustomerPONumber : WSReturn
	{
		public string PONumber { get; set; }
		public string ValidPostalCode { get; set; }
		public WSEnums.PONumberValidity Validity { get; set; }
		public int MaximumUses { get; set; }
		public int CurrentUses { get; set; }
		public DateTime ExpirationDate { get; set; }
		public string AdditionalPurchaseOrderNotes { get; set; }
		public WSCountry Country { get; set; }

		public WSCustomerPONumber()
		{
			PONumber = null;
			ValidPostalCode = null;
			Validity = WSEnums.PONumberValidity.Origin;
			MaximumUses = 0;
			CurrentUses = 0;
			ExpirationDate = DateTime.Now;
			AdditionalPurchaseOrderNotes = null;
			Country = null;
		}
	}
}
