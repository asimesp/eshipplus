﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSMessage
	{
		public WSEnums.MessageType Type { get; set; }
		public string Value { get; set; }

		public WSMessage()
		{
			Value = string.Empty;
			Type = WSEnums.MessageType.Null;
		}

		public static WSMessage ErrorMessage(string value, params object [] paramaters)
		{
			return new WSMessage {Type = WSEnums.MessageType.Error, Value = string.Format(value, paramaters)};
		}

		public static WSMessage WarningMessage(string value, params object[] paramaters)
		{
			return new WSMessage { Type = WSEnums.MessageType.Warning, Value = string.Format(value, paramaters) };
		}

		public static WSMessage InformationMessage(string value, params object[] paramaters)
		{
			return new WSMessage { Type = WSEnums.MessageType.Information, Value = string.Format(value, paramaters) };
		}

		public static WSMessage Message(ValidationMessage message)
		{
			switch (message.Type)
			{
				case ValidateMessageType.Error:
					return ErrorMessage(message.Message);
				case ValidateMessageType.Warning:
					return WarningMessage(message.Message);
				default:
					return InformationMessage(message.Message);
			}
		}

		public static bool ContainsError(WSMessage[] messages)
		{
			if(messages == null) return false;
			if(messages.Length == 0) return false;
			return messages.Any(t => t.Type == WSEnums.MessageType.Error);
		}
	}
}
