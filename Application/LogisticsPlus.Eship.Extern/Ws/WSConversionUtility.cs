﻿using System;
using LogisticsPlus.Eship.Processor.Dto.Operations;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public static class WSConversionUtility
	{
        public static WSTrackingInfoRequest ToWSTrackingInfoRequest (this TrackingInfoRequestDto trackingInfoRequest)
        {
            var newWSTrackingInfoRequest = new WSTrackingInfoRequest
                                   {
									   Scac = trackingInfoRequest.Scac,
                                       BillOfLadingNumber = trackingInfoRequest.ShipmentNumber,
                                       ScheduledPickupDate = trackingInfoRequest.ScheduledPickupDate,
                                       OriginPostalCode = trackingInfoRequest.OriginPostalCode,
                                       DestinationPostalCode = trackingInfoRequest.DestinationPostalCode,
                                       OriginCountryCode =  trackingInfoRequest.OriginCountryCode,
                                       DestinationCountryCode = trackingInfoRequest.DestinationCountryCode
                                   };
            return newWSTrackingInfoRequest;
        }
	}
}
