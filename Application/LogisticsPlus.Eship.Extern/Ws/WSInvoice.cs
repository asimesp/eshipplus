﻿using System;
using LogisticsPlus.Eship.Core.Accounting;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSInvoice : WSReturn
	{
		public string InvoiceNumber { get; set; }
		public DateTime Date { get; set; }
		public DateTime DueDate { get; set; }
		public int Terms { get; set; }
		public WSLocation2 Billing { get; set; }
		public string CustomerControlAccount { get; set; }
		public InvoiceType Type { get; set; }

		public decimal TotalFreightCharges { get; set; }
		public decimal TotalFuelCharges { get; set; }
		public decimal TotalAccessorialCharges { get; set; }
		public decimal TotalServiceCharges { get; set; }

		public decimal TotalAmountDue { get; set; }

		public WSBillingDetail[] BillingDetails { get; set; }

		public WSInvoice()
		{
			InvoiceNumber = null;
			Date = DateTime.Now;
			DueDate = DateTime.Now;
			Terms = 15;
			Billing = null;
			CustomerControlAccount = null;
			Type = InvoiceType.Invoice;

			TotalAccessorialCharges = 0m;
			TotalAmountDue = 0m;
			TotalFreightCharges = 0m;
			TotalFuelCharges = 0m;
			TotalServiceCharges = 0m;

			BillingDetails = null;
		}
	}
}
