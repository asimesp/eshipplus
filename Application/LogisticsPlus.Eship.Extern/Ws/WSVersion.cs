﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSVersion
	{
		public string Version { get; private set; }
		public string Major { get; private set; }
		public string Minor { get; private set; }
		public string Build { get; private set; }
		public string Revision { get; private set; }

		public WSVersion()
		{
			Version v = System.Reflection.Assembly.GetAssembly(GetType()).GetName().Version;
			Version = v.ToString();
			Major = v.Major.ToString();
			Minor = v.Minor.ToString();
			Build = v.Build.ToString();
			Revision = v.Revision.ToString();
		}
	}
}
