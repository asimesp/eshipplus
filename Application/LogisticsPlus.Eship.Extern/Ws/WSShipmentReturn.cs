﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSShipmentReturn : WSReturn
	{
		public WSShipment Shipment { get; set; }
		
		public WSShipmentReturn()
		{
			Shipment = null;
		}
	}
}
