﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSAccessorialService : WSReturn
	{
		public string ServiceCode { get; set; }
		public string ServiceDescription { get; set; }
		public string BillingCode { get; set; }

		public WSAccessorialService()
		{
			ServiceCode = null;
			ServiceDescription = null;
			BillingCode = null;
		}
	}
}

