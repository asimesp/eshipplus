﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSItem2
	{
		public decimal Weight { get; set; }

		public int PackagingQuantity { get; set; }
		public int SaidToContain { get; set; }

		public decimal Height { get; set; }
		public decimal Width { get; set; }
		public decimal Length { get; set; }

		public bool Stackable { get; set; }
		public bool HazardousMaterial { get; set; }

		public decimal DeclaredValue { get; set; }

		public string Description { get; set; }
		public string Comment { get; set; }
		public string NationalMotorFreightClassification { get; set; }
		public string HarmonizedTariffSchedule { get; set; }
		

		public WSItemPackage Packaging { get; set; }
		public WSFreightClass FreightClass { get; set; }

		public WSItem2()
		{
			Weight = 0;
			PackagingQuantity = 0;
			SaidToContain = 0;

			Height = 0;
			Width = 0;
			Length = 0;

			Stackable = false;

			DeclaredValue = 0;

			Description = null;
			Comment = null;
			NationalMotorFreightClassification = null;
			HarmonizedTariffSchedule = null;

			Packaging = null;
			FreightClass = null;
		}
	}
}
