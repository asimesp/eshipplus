﻿using LogisticsPlus.Eship.Core.Connect;

namespace LogisticsPlus.Eship.Extern.Ws
{
	public class WSFaxTransmissionUpdate
	{
		public string Key { get; set; }
		public string Message { get; set; }
		public FaxTransmissionStatus Status { get; set; }
	}
}
