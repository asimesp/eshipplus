﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Rates;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Project44;
using LogisticsPlus.Eship.Smc;

namespace LogisticsPlus.Eship.Extern.Ws
{
	public static class WsInternalUtilities
	{
		public static bool ContainsErrors(this List<WSMessage> messages)
		{
			return WSMessage.ContainsError(messages.ToArray());
		}

		public static T ToMessageReturn<T>(this List<WSMessage> messages) where T : WSReturn, new()
		{
			return messages.ToArray().ToMessageReturn<T>();
		}

		public static T ToMessageReturn<T>(this WSMessage[] messages) where T : WSReturn, new()
		{
			return new T {Messages = messages};
		}

		public static T[] ToMessageReturnCollection<T>(this List<WSMessage> messages) where T : WSReturn, new()
		{
			return new[] {messages.ToMessageReturn<T>()};
		}

		public static T[] ToMessageReturnCollection<T>(this WSMessage[] messages) where T : WSReturn, new()
		{
			return new[] { messages.ToMessageReturn<T>() };
		} 



		public static WSMessage[] ValidateMinimumForRating(this WSShipment2 shipment)
		{
			var msgs = new List<WSMessage>();

			if (shipment.Origin == null)
				msgs.Add(WSMessage.ErrorMessage("Missing origin information"));
			else
			{
				if (shipment.Origin.Country == null)
					msgs.Add(WSMessage.ErrorMessage("Missing origin country object"));
				else if (string.IsNullOrEmpty(shipment.Origin.Country.Code))
					msgs.Add(WSMessage.ErrorMessage("Invalid origin country"));

				if(string.IsNullOrEmpty(shipment.Origin.PostalCode))
					msgs.Add(WSMessage.ErrorMessage("Invalid origin postal code"));
			}

			if (shipment.Destination == null)
				msgs.Add(WSMessage.ErrorMessage("Missing destination information"));
			else
			{
				if (shipment.Destination.Country == null)
					msgs.Add(WSMessage.ErrorMessage("Missing destination country object"));
				else if (string.IsNullOrEmpty(shipment.Destination.Country.Code))
					msgs.Add(WSMessage.ErrorMessage("Invalid destination country"));

				if (string.IsNullOrEmpty(shipment.Destination.PostalCode))
					msgs.Add(WSMessage.ErrorMessage("Invalid destination postal code"));
			}

			if (shipment.Items == null || !shipment.Items.Any())
				msgs.Add(WSMessage.ErrorMessage("Missing shipment items"));

			return msgs.ToArray();
		}

		public static WSMessage[] CheckVarianceInSelectedAndBookedRates(this WSShipment2 shipment)
		{
			var messages = new List<WSMessage>();

			if (shipment.SelectedRate.AccessorialCharges != shipment.BookedRate.AccessorialCharges)
				messages.Add(WSMessage.WarningMessage("Change in accessorial charges."));
			if (shipment.SelectedRate.ServiceCharges != shipment.BookedRate.ServiceCharges)
				messages.Add(WSMessage.WarningMessage("Change in service charges."));
			if (shipment.SelectedRate.FreightCharges != shipment.BookedRate.FreightCharges)
				messages.Add(WSMessage.WarningMessage("Change in freight charges."));
			if (shipment.SelectedRate.FuelCharges != shipment.BookedRate.FuelCharges)
				messages.Add(WSMessage.WarningMessage("Change in fuel charges."));
			if (shipment.SelectedRate.BilledWeight != shipment.BookedRate.BilledWeight)
				messages.Add(WSMessage.WarningMessage("Change in specified billed weight."));
			if (shipment.SelectedRate.TransitTime != shipment.BookedRate.TransitTime)
				messages.Add(WSMessage.WarningMessage("Change in transit time."));
			if (shipment.SelectedRate.EstimatedDeliveryDate != shipment.BookedRate.EstimatedDeliveryDate)
				messages.Add(WSMessage.WarningMessage("Change in estimated delivery date."));
			if (shipment.SelectedRate.Mileage != shipment.BookedRate.Mileage)
				messages.Add(WSMessage.WarningMessage("Change in mileage."));
			if (shipment.SelectedRate.MileageSourceKey != shipment.BookedRate.MileageSourceKey)
				messages.Add(WSMessage.WarningMessage("Change in mileage source key."));


			if (shipment.SelectedRate.SelectedGuarOption != null && shipment.BookedRate.SelectedGuarOption != null)
			{
				if (shipment.SelectedRate.SelectedGuarOption.AmountDue != shipment.BookedRate.SelectedGuarOption.AmountDue)
					messages.Add(WSMessage.WarningMessage("Change in guaranteed service charge."));
			}
			else if (shipment.SelectedRate.SelectedGuarOption != shipment.BookedRate.SelectedGuarOption)
			{
				messages.Add(WSMessage.WarningMessage("Unable to match guaranteed service selected rate."));
			}

			return messages.ToArray();
		}


		public static Shipment ToShipmentForRating(this WSShipment2 shipment, User user, Customer customer)
		{
			var s = new Shipment
			        	{
			        		User = user,
			        		Tenant = user.Tenant,
			        		ShipmentNumber = string.Empty,
			        		Customer = customer,

			        		DesiredPickupDate = shipment.ShipmentDate.TimeToMinimum(),

			        		DeclineInsurance = shipment.DeclineAdditionalInsuranceIfApplicable,

			        		HazardousMaterial = shipment.HazardousMaterialShipment,
			        		HazardousMaterialContactEmail = shipment.HazardousMaterialContactEmail.GetString(),
			        		HazardousMaterialContactMobile = shipment.HazardousMaterialContactMobile.GetString(),
			        		HazardousMaterialContactName = shipment.HazardousMaterialContactName.GetString(),
			        		HazardousMaterialContactPhone = shipment.HazardousMaterialContactPhone.GetString(),

			        		EarlyPickup = shipment.EarliestPickup == null
			        		              	? TimeUtility.DefaultOpen
			        		              	: shipment.EarliestPickup.Time.GetString(),
			        		EarlyDelivery = shipment.EarliestDelivery == null
			        		                	? TimeUtility.DefaultClose
			        		                	: shipment.EarliestDelivery.Time.GetString(),
			        		LatePickup = shipment.LatestPickup == null
			        		             	? TimeUtility.DefaultOpen
			        		             	: shipment.LatestPickup.Time.GetString(),
			        		LateDelivery = shipment.LatestDelivery == null
			        		               	? TimeUtility.DefaultClose
			        		               	: shipment.LatestDelivery.Time.GetString(),

			        		EmptyMileage = 0m,
			        		MiscField1 = string.Empty,
			        		MiscField2 = string.Empty,
							DriverName = string.Empty,
							DriverPhoneNumber = string.Empty,
							DriverTrailerNumber = string.Empty,
			        		AccountBucketUnitId = default(long),
							CreatedInError = false
			        	};

			var countrySearch = new CountrySearch();
			var origin = shipment.Origin ?? new WSLocation2();
			s.Origin = new ShipmentLocation
			           	{
			           		Shipment = s,
			           		TenantId = user.TenantId,
			           		StopOrder = 0,
			           		City = origin.City.GetString(),
			           		CountryId = origin.Country == null
			           		            	? default(long)
			           		            	: countrySearch.FetchCountryIdByCode(origin.Country.Code),
			           		Description = origin.Description.GetString(),
			           		PostalCode = origin.PostalCode.GetString(),
			           		SpecialInstructions = origin.SpecialInstructions.GetString(),
							GeneralInfo = origin.GeneralInfo.GetString(),
							Direction = origin.Direction.GetString(),
			           		State = origin.State.GetString(),
			           		Street1 = origin.Street.GetString(),
			           		Street2 = origin.StreetExtra.GetString(),
							AppointmentDateTime = DateUtility.SystemEarliestDateTime,
			           		Contacts = new List<ShipmentContact>
			           		           	{
			           		           		new ShipmentContact
			           		           			{
													TenantId = user.TenantId,
			           		           				Name = origin.Contact.GetString(),
			           		           				Comment = origin.ContactComment.GetString(),
			           		           				Phone = origin.Phone.GetString(),
			           		           				Fax = origin.Fax.GetString(),
			           		           				Mobile = origin.Mobile.GetString(),
			           		           				ContactTypeId = user.Tenant.DefaultSchedulingContactTypeId,
			           		           				Email = origin.Email.GetString(),
			           		           				Primary = true
			           		           			}
			           		           	}
			           	};
			s.Origin.Contacts[0].Location = s.Origin;

			var destination = shipment.Destination ?? new WSLocation2();
			s.Destination = new ShipmentLocation
			                	{
			                		Shipment = s,
			                		TenantId = user.TenantId,
			                		StopOrder = 1,
			                		City = destination.City.GetString(),
			                		CountryId = destination.Country == null
			                		            	? default(long)
			                		            	: countrySearch.FetchCountryIdByCode(destination.Country.Code),
			                		Description = destination.Description.GetString(),
			                		PostalCode = destination.PostalCode.GetString(),
			                		SpecialInstructions = destination.SpecialInstructions.GetString(),
									GeneralInfo = destination.GeneralInfo.GetString(),
									Direction = destination.Direction.GetString(),
			                		State = destination.State.GetString(),
			                		Street1 = destination.Street.GetString(),
			                		Street2 = destination.StreetExtra.GetString(),
									AppointmentDateTime = DateUtility.SystemEarliestDateTime,
			                		Contacts = new List<ShipmentContact>
			                		           	{
			                		           		new ShipmentContact
			                		           			{
															TenantId = user.TenantId,
			                		           				Name = destination.Contact.GetString(),
			                		           				Comment = destination.ContactComment.GetString(),
			                		           				Phone = destination.Phone.GetString(),
			                		           				Fax = destination.Fax.GetString(),
			                		           				Mobile = destination.Mobile.GetString(),
			                		           				ContactTypeId = user.Tenant.DefaultSchedulingContactTypeId,
			                		           				Email = destination.Email.GetString(),
			                		           				Primary = true
			                		           			}
			                		           	}
			                	};
			s.Destination.Contacts[0].Location = s.Destination;

            //Items
            var items = shipment.Items == null
                            ? new List<ShipmentItem>()
                            : shipment.Items
                                .Select(i => new ShipmentItem
                                {
                                    Pickup = 0,
                                    Delivery = 1,
                                    ActualWeight = i.Weight,
                                    ActualLength = i.Length,
                                    ActualWidth = i.Width,
                                    ActualHeight = i.Height,
                                    Quantity = i.PackagingQuantity,
                                    PieceCount = i.SaidToContain,
                                    Description = i.Description.GetString(),
                                    Comment = i.Comment.GetString(),
                                    RatedFreightClass =
                                        i.FreightClass == null ? default(double) : i.FreightClass.FreightClass,
                                    ActualFreightClass =
                                        i.FreightClass == null ? default(double) : i.FreightClass.FreightClass,
                                    Value = i.DeclaredValue,
                                    IsStackable = i.Stackable,
                                    HazardousMaterial = i.HazardousMaterial,
                                    PackageTypeId = i.Packaging == null ? default(long) : i.Packaging.Key,
                                    NMFCCode = i.NationalMotorFreightClassification.GetString(),
                                    HTSCode = i.HarmonizedTariffSchedule.GetString(),
                                    Tenant = s.Tenant,
                                    Shipment = s,
                                })
                                .ToList();
            s.Items = items;

            // auto mark shipment as hazmat if there are hazmat items
            if (s.Items.Any(i => i.HazardousMaterial) && !s.HazardousMaterial)
                s.HazardousMaterial = true;

            // mark all items as hazmat if none are and the shipment is marked as hazmat
            if (s.HazardousMaterial && !s.Items.Any(i => i.HazardousMaterial))
                s.Items.ForEach(i => i.HazardousMaterial = true);


			//Services
			var serviceSearch = new ServiceSearch();
			s.Services = shipment.Accessorials == null
			             	? new List<ShipmentService>()
			             	: shipment.Accessorials
			             	  	.Select(service => new ShipmentService
			             	  	                   	{
			             	  	                   		Shipment = s,
			             	  	                   		ServiceId =
			             	  	                   			serviceSearch.FetchServiceIdByCode(service.ServiceCode, user.TenantId),
			             	  	                   		TenantId = user.TenantId,
			             	  	                   	})
			             	  	.ToList();
			CheckForAutoServices(s);
			if (s.Services.Any(service => service.ServiceId == s.Tenant.HazardousMaterialServiceId) && !s.HazardousMaterial)
				s.HazardousMaterial = true;

			return s;
		}

		public static void UpdateShipmentForBooking(this Shipment sh, WSShipment2 shipment, Rate rate, RateCharge selectedGuarOption, out Exception carrierConnectException)
		{

			sh.CreatedInError = false;
			sh.DateCreated = DateTime.Now;
			sh.ActualDeliveryDate = DateUtility.SystemEarliestDateTime;
			sh.ActualPickupDate = DateUtility.SystemEarliestDateTime;
			sh.Status = ShipmentStatus.Scheduled;
			sh.Notes = new List<ShipmentNote>();
			sh.Stops = new List<ShipmentLocation>();
			sh.Equipments = new List<ShipmentEquipment>();
			sh.Documents = new List<ShipmentDocument>();
			sh.Assets = new List<ShipmentAsset>();
			sh.Vendors = new List<ShipmentVendor>();
			sh.AccountBuckets = new List<ShipmentAccountBucket>();
			sh.AutoRatingAccessorials = new List<ShipmentAutoRatingAccessorial>();
			sh.GeneralBolComments = string.Empty;
			sh.CriticalBolComments = string.Empty;
			sh.ShipperBol = string.Empty;
			sh.MiscField1 = string.Empty;
			sh.MiscField2 = string.Empty;
			sh.DriverName = string.Empty;
			sh.DriverPhoneNumber = string.Empty;
			sh.DriverTrailerNumber = string.Empty;
			sh.AccountBucketUnitId = default(long);
			sh.EmptyMileage = 0m;
			sh.Mileage = shipment.BookedRate.Mileage;
			sh.MileageSourceId = shipment.BookedRate.MileageSourceKey;
			sh.ServiceMode = shipment.BookedRate.ServiceMode;
		    sh.Project44QuoteNumber = rate.Project44QuoteNumber;

		    sh.SendShipmentUpdateToOriginPrimaryContact = shipment.SendShipmentUpdateToOriginPrimaryContact;
		    sh.SendShipmentUpdateToDestinationPrimaryContact = shipment.SendShipmentUpdateToDestinationPrimaryContact;

            // TODO (EVX-53): Set these fields once we fully implement P44 expedited shipments into web service
		    sh.IsExpeditedService = false;
		    sh.ExpeditedServiceRate = 0;
		    sh.ExpeditedServiceTime = string.Empty;

			// reseller additions
			sh.ResellerAdditionId = sh.Customer.Rating.ResellerAdditionId;
			sh.BillReseller = sh.Customer.Rating.ResellerAdditionId != default(long) && sh.Customer.Rating.BillReseller;

			// account buckets, prefix, sales rep.
			sh.AccountBuckets.Add(new ShipmentAccountBucket
			                      	{
			                      		AccountBucket = sh.Customer.DefaultAccountBucket,
			                      		Shipment = sh,
			                      		TenantId = sh.TenantId,
			                      		Primary = true,
			                      	});
			sh.Prefix = sh.Customer.Prefix;
			sh.HidePrefix = sh.Customer.HidePrefix;
			
			// sales rep
			var srtier = sh.Customer.SalesRepresentative == null
							 ? null
							 : sh.Customer.SalesRepresentative.ApplicableTier(sh.DateCreated, sh.ServiceMode, sh.Customer);
			sh.SalesRepresentative = sh.Customer.SalesRepresentative;
			sh.SalesRepresentativeCommissionPercent = srtier != null ? srtier.CommissionPercent : 0m;
			sh.SalesRepAddlEntityCommPercent = sh.Customer.SalesRepresentative == null
												  ? 0m
												  : sh.Customer.SalesRepresentative.AdditionalEntityCommPercent;
			sh.SalesRepAddlEntityName = sh.Customer.SalesRepresentative == null
										   ? string.Empty
										   : sh.Customer.SalesRepresentative.AdditionalEntityName;
			sh.SalesRepMinCommValue = srtier != null ? srtier.FloorValue : 0m;
			sh.SalesRepMaxCommValue = srtier != null ? srtier.CeilingValue : 0m;

			// vendor and dates
			var vendor = new VendorSearch().FetchVendorByNumber(shipment.BookedRate.CarrierKey, sh.User.TenantId);
			sh.Vendors.Add(new ShipmentVendor
			               	{
			               		Shipment = sh,
			               		TenantId = sh.TenantId,
			               		Vendor = vendor,
			               		Primary = true,
			               		FailureComments = string.Empty,
			               		ProNumber = string.Empty,
			               	});
			sh.EstimatedDeliveryDate = shipment.BookedRate.EstimatedDeliveryDate;

			// references
			sh.PurchaseOrderNumber = shipment.PurchaseOrder.GetString();
			sh.ShipperReference = shipment.ReferenceNumber.GetString();
			sh.CustomerReferences = shipment.CustomReferences == null
			                        	? new List<ShipmentReference>()
			                        	: shipment.CustomReferences
			                        	  	.Select(r =>
			                        	  	        	{
			                        	  	        		var cf = sh.Customer.CustomFields.FirstOrDefault(f => f.Name == r.Name);
			                        	  	        		return new ShipmentReference
			                        	  	        		       	{
			                        	  	        		       		TenantId = sh.TenantId,
			                        	  	        		       		Shipment = sh,
			                        	  	        		       		DisplayOnOrigin = cf != null && cf.DisplayOnOrigin,
			                        	  	        		       		DisplayOnDestination = cf != null && cf.DisplayOnDestination,
			                        	  	        		       		Name = r.Name.GetString(),
			                        	  	        		       		Value = r.Value.GetString(),
			                        	  	        		       		Required = r.Required,
			                        	  	        		       	};
			                        	  	        	})
			                        	  	.ToList();


			// charges
			// Add base rates
			sh.Charges = rate.BaseRateCharges
				.Select(c => new ShipmentCharge
				             	{
				             		TenantId = sh.TenantId,
				             		Shipment = sh,
				             		ChargeCodeId = c.ChargeCodeId,
				             		UnitBuy = c.UnitBuy,
				             		UnitSell = c.UnitSell,
				             		UnitDiscount = c.UnitDiscount,
				             		Quantity = c.Quantity,
				             		Comment = string.Empty,
				             	})
				.ToList();
			// premium rates
			// guaranteed services
			if (selectedGuarOption != null)
			{
			    sh.CriticalBolComments = !rate.IsProject44Rate
			        ? string.IsNullOrEmpty(selectedGuarOption.LTLGuaranteedCharge.CriticalNotes)
			            ? string.Format("GUARANTEED BY {0} DELIVERY SERVICE ON {1}", selectedGuarOption.LTLGuaranteedCharge.Time,
			                sh.EstimatedDeliveryDate.FormattedShortDate())
			            : selectedGuarOption.LTLGuaranteedCharge.CriticalNotes.Replace("[#Date#]",
			                sh.EstimatedDeliveryDate.FormattedShortDate())
			        : string.Format("GUARANTEED BY {0} DELIVERY SERVICE ON {1}",
			            Project44Constants.GetGuaranteedServiceTime(selectedGuarOption.ChargeCode.Project44Code),
			            sh.EstimatedDeliveryDate.FormattedShortDate());

                sh.Charges.Add(new ShipmentCharge
					{
						TenantId = sh.TenantId,
						Shipment = sh,
						ChargeCodeId = selectedGuarOption.ChargeCodeId,
						UnitBuy = selectedGuarOption.UnitBuy,
						UnitSell = selectedGuarOption.UnitSell,
						UnitDiscount = selectedGuarOption.UnitDiscount,
						Quantity = selectedGuarOption.Quantity,
						Comment = string.Empty,
					});
            }


			// auto rating details
			sh.OriginalRateValue = rate.OriginalRateValue;
			sh.ShipmentAutorated = true;
			sh.LineHaulProfitAdjustmentRatio = rate.FreightProfitAdjustment;
			sh.FuelProfitAdjustmentRatio = rate.FuelProfitAdjustment;
			sh.ServiceProfitAdjustmentRatio = rate.ServiceProfitAdjustment;
			sh.AccessorialProfitAdjustmentRatio = rate.AccessorialProfitAdjustment;
			sh.ResellerAdditionalAccessorialMarkup = rate.ResellerAccessorialMarkup;
			sh.ResellerAdditionalAccessorialMarkupIsPercent = rate.ResellerAccessorialMarkupIsPercent;
			sh.ResellerAdditionalServiceMarkup = rate.ResellerServiceMarkup;
			sh.ResellerAdditionalServiceMarkupIsPercent = rate.ResellerServiceMarkupIsPercent;
			sh.ResellerAdditionalFuelMarkup = rate.ResellerFuelMarkup;
			sh.ResellerAdditionalFuelMarkupIsPercent = rate.ResellerFuelMarkupIsPercent;
			sh.ResellerAdditionalFreightMarkup = rate.ResellerFreightMarkup;
			sh.ResellerAdditionalFreightMarkupIsPercent = rate.ResellerFreightMarkupIsPercent;
			sh.DirectPointRate = rate.DirectPointRate;
			sh.SmallPackType = rate.SmallPackServiceType;
			sh.SmallPackageEngine = rate.SmallPackEngine;
			sh.ApplyDiscount = rate.ApplyDiscount;
			sh.BilledWeight = rate.BilledWeight;
			sh.RatedWeight = rate.RatedWeight;
			sh.RatedCubicFeet = rate.RatedCubicFeet;
			sh.RatedPcf = rate.RatedPcf;
			sh.GuaranteedDeliveryServiceTime = selectedGuarOption == null
				                                   ? string.Empty
				                                   : !selectedGuarOption.IsProject44GuaranteedCharge
                                                        ? selectedGuarOption.LTLGuaranteedCharge.Time
		                                                : Project44Constants.GetGuaranteedServiceTime(selectedGuarOption.ChargeCode.Project44Code);

		    sh.IsGuaranteedDeliveryService = selectedGuarOption != null;
		    sh.GuaranteedDeliveryServiceRate = selectedGuarOption != null
		        ? !selectedGuarOption.IsProject44GuaranteedCharge
		            ? selectedGuarOption.LTLGuaranteedCharge.Rate
		            : selectedGuarOption.UnitBuy // No markups on UnitBuy, so that is the original P44 Guaranteed rate 
		        : default(decimal);

		    sh.GuaranteedDeliveryServiceRateType =
		        selectedGuarOption != null && !selectedGuarOption.IsProject44GuaranteedCharge
		            ? selectedGuarOption.LTLGuaranteedCharge.RateType
		            : RateType.Flat;

			var tier = rate.DiscountTier ?? new DiscountTier();
			sh.VendorDiscountPercentage = tier.DiscountPercent;
			sh.VendorFreightFloor = tier.FloorValue;
			sh.VendorFreightCeiling = tier.CeilingValue;
			sh.VendorFuelPercent = rate.FuelMarkupPercent;

			var sellRate = rate.LTLSellRate ?? new LTLSellRate();

            sh.CustomerFreightMarkupValue = rate.MarkupValue;
            sh.CustomerFreightMarkupPercent = rate.MarkupPercent;
			sh.UseLowerCustomerFreightMarkup = sellRate.UseMinimum;
			sh.VendorRatingOverrideAddress = sellRate.VendorRating == null || string.IsNullOrEmpty(sellRate.VendorRating.OverrideAddress)
			                                       	? string.Empty
			                                       	: sellRate.VendorRating.OverrideAddress;
			sh.CriticalBolComments = sellRate.VendorRating.AppendVendorRatingCriticalCommentToShipment(sh.CriticalBolComments);

            // update processing
		    carrierConnectException = null;
            if (!rate.IsProject44Rate)
            {
                Rater2.UpdateShipmentAutoRatingAccessorilas(sh, sellRate);
                if (sh.ServiceMode == ServiceMode.LessThanTruckload)
                {
                    var rule = rate.CFCRule ?? new LTLCubicFootCapacityRule();
                    if (!rule.KeyLoaded) rule = null;
                    Rater2.UpdateShipmentItemRatedCalculations(sh, rule, tier);

                    // update terminal information
                    try
                    {
                        var variables = ProcessorVars.IntegrationVariables[sh.TenantId];
                        var carrierConnect =
                            new CarrierConnect(variables
                                .CarrierConnectParameters); //NOTE: without country map as country id is set below on return

                        var terminals = carrierConnect.GetLTLTerminalInfo(vendor.Scac,
                            new[] { rate.OriginTerminalCode, rate.DestinationTerminalCode });

                        sh.OriginTerminal = terminals.FirstOrDefault(t => t.Code == rate.OriginTerminalCode) ??
                                            new LTLTerminalInfo();
                        sh.OriginTerminal.Shipment = sh;
                        sh.OriginTerminal.TenantId = sh.TenantId;
                        sh.OriginTerminal.CountryId = sh.Origin.CountryId;
                        sh.DestinationTerminal = terminals.FirstOrDefault(t => t.Code == rate.DestinationTerminalCode) ??
                                                 new LTLTerminalInfo();
                        sh.DestinationTerminal.Shipment = sh;
                        sh.DestinationTerminal.TenantId = sh.TenantId;
                        sh.DestinationTerminal.CountryId = sh.Destination.CountryId;
                    }
                    catch (Exception ex)
                    {
                        carrierConnectException = ex;
                    }
                }
            }
        }



		public static LoadOrder ToOfferedLoadOrder(this WSBookingRequest request, User user, Customer customer)
		{
			var load = new LoadOrder
			         	{
			         		User = user,
			         		Tenant = user.Tenant,
			         		Customer = customer,
			         		DateCreated = DateTime.Now,
			         		Status = LoadOrderStatus.Offered,
			         		Equipments = new List<LoadOrderEquipment>(),
			         		Stops = new List<LoadOrderLocation>(),
							Notes = new List<LoadOrderNote>(),
                            Vendors = new List<LoadOrderVendor>(),
							Documents = new List<LoadOrderDocument>(),
							Charges = new List<LoadOrderCharge>(),

			         		HazardousMaterial = request.HazardousMaterialShipment,
			         		HazardousMaterialContactEmail = request.HazardousMaterialContactEmail.GetString(),
			         		HazardousMaterialContactMobile = request.HazardousMaterialContactMobile.GetString(),
			         		HazardousMaterialContactName = request.HazardousMaterialContactName.GetString(),
			         		HazardousMaterialContactPhone = request.HazardousMaterialContactPhone.GetString(),

			         		NotAvailableToLoadboards = true,
			         		Description = string.Empty,

							EmptyMileage = 0m,
							MiscField1 = string.Empty,
							MiscField2 = string.Empty,
							AccountBucketUnitId = default(long),

							DriverName = string.Empty,
							DriverPhoneNumber = string.Empty,	
							DriverTrailerNumber = string.Empty,

			         		LoadOrderNumber = string.Empty,

			         		DesiredPickupDate = request.DesiredShipmentDate.TimeToMinimum(),
			         		EstimatedDeliveryDate = request.DesiredShipmentDate.TimeToMinimum().AddDays(1),

			         		EarlyPickup = request.EarliestPickup == null
			         		              	? TimeUtility.DefaultOpen
			         		              	: request.EarliestPickup.Time.GetString(),
			         		EarlyDelivery = request.EarliestDelivery == null
			         		                	? TimeUtility.DefaultClose
			         		                	: request.EarliestDelivery.Time.GetString(),
			         		LatePickup = request.LatestPickup == null
			         		             	? TimeUtility.DefaultOpen
			         		             	: request.LatestPickup.Time.GetString(),
			         		LateDelivery = request.LatestDelivery == null
			         		               	? TimeUtility.DefaultClose
			         		               	: request.LatestDelivery.Time.GetString(),

			         		PurchaseOrderNumber = request.PurchaseOrder.GetString(),
			         		ShipperReference = request.ReferenceNumber.GetString(),

			         		ServiceMode = ServiceMode.NotApplicable,
							CriticalBolComments = string.Empty,
							GeneralBolComments = string.Empty,
							ShipperBol = string.Empty,
                                                        
			         	};


			var countrySearch = new CountrySearch();
			var origin = request.Origin ?? new WSLocation2();
			load.Origin = new LoadOrderLocation
			            	{
			            		LoadOrder = load,
			            		TenantId = user.TenantId,
			            		StopOrder = 0,
			            		City = origin.City.GetString(),
			            		CountryId = origin.Country == null
			            		            	? default(long)
			            		            	: countrySearch.FetchCountryIdByCode(origin.Country.Code),
			            		Description = origin.Description.GetString(),
			            		PostalCode = origin.PostalCode.GetString(),
			            		SpecialInstructions = origin.SpecialInstructions.GetString(),
								GeneralInfo = origin.GeneralInfo.GetString(),
								Direction = origin.Direction.GetString(),
			            		State = origin.State.GetString(),
			            		Street1 = origin.Street.GetString(),
			            		Street2 = origin.StreetExtra.GetString(),
								AppointmentDateTime = DateUtility.SystemEarliestDateTime,
			            		Contacts = new List<LoadOrderContact>
			            		           	{
			            		           		new LoadOrderContact
			            		           			{
														TenantId = user.TenantId,
			            		           				Name = origin.Contact.GetString(),
			            		           				Comment = origin.ContactComment.GetString(),
			            		           				Phone = origin.Phone.GetString(),
			            		           				Fax = origin.Fax.GetString(),
			            		           				Mobile = origin.Mobile.GetString(),
			            		           				ContactTypeId = user.Tenant.DefaultSchedulingContactTypeId,
			            		           				Email = origin.Email.GetString(),
			            		           				Primary = true
			            		           			}
			            		           	}
			            	};
			load.Origin.Contacts[0].Location = load.Origin;

			var destination = request.Destination ?? new WSLocation2();
			load.Destination = new LoadOrderLocation
			                 	{
			                 		LoadOrder = load,
			                 		TenantId = user.TenantId,
			                 		StopOrder = 1,
			                 		City = destination.City.GetString(),
			                 		CountryId = destination.Country == null
			                 		            	? default(long)
			                 		            	: countrySearch.FetchCountryIdByCode(destination.Country.Code),
			                 		Description = destination.Description.GetString(),
			                 		PostalCode = destination.PostalCode.GetString(),
			                 		SpecialInstructions = destination.SpecialInstructions.GetString(),
									GeneralInfo = destination.GeneralInfo.GetString(),
									Direction = destination.Direction.GetString(),
			                 		State = destination.State.GetString(),
			                 		Street1 = destination.Street.GetString(),
			                 		Street2 = destination.StreetExtra.GetString(),
									AppointmentDateTime = DateUtility.SystemEarliestDateTime,
									Contacts = new List<LoadOrderContact>
			                 		           	{
			                 		           		new LoadOrderContact
			                 		           			{
															TenantId = user.TenantId,
			                 		           				Name = destination.Contact.GetString(),
			                 		           				Comment = destination.ContactComment.GetString(),
			                 		           				Phone = destination.Phone.GetString(),
			                 		           				Fax = destination.Fax.GetString(),
			                 		           				Mobile = destination.Mobile.GetString(),
			                 		           				ContactTypeId = user.Tenant.DefaultSchedulingContactTypeId,
			                 		           				Email = destination.Email.GetString(),
			                 		           				Primary = true
			                 		           			}
			                 		           	}
			                 	};
			load.Destination.Contacts[0].Location = load.Destination;


			load.ResellerAdditionId = customer.Rating.ResellerAdditionId;
			load.BillReseller = customer.Rating.ResellerAdditionId != default(long) && customer.Rating.BillReseller;

			load.AccountBuckets.Add(new LoadOrderAccountBucket
			                        	{
			                        		AccountBucket = load.Customer.DefaultAccountBucket,
			                        		LoadOrder = load,
			                        		Primary = true,
			                        		TenantId = load.TenantId,
			                        	});

			load.Prefix = load.Customer.Prefix;
			load.HidePrefix = load.Customer.HidePrefix;
			
			// sales rep
			var srtier = load.Customer.SalesRepresentative == null
							 ? null
							 : load.Customer.SalesRepresentative.ApplicableTier(load.DateCreated, load.ServiceMode, load.Customer);
			load.SalesRepresentative = load.Customer.SalesRepresentative;
			load.SalesRepresentativeCommissionPercent = srtier != null ? srtier.CommissionPercent : 0m;
			load.SalesRepAddlEntityCommPercent = load.Customer.SalesRepresentative == null
												  ? 0m
												  : load.Customer.SalesRepresentative.AdditionalEntityCommPercent;
			load.SalesRepAddlEntityName = load.Customer.SalesRepresentative == null
										   ? string.Empty
										   : load.Customer.SalesRepresentative.AdditionalEntityName;
			load.SalesRepMinCommValue = srtier != null ? srtier.FloorValue : 0m;
			load.SalesRepMaxCommValue = srtier != null ? srtier.CeilingValue : 0m;

            //Items
            var items = request.Items == null
                            ? new List<LoadOrderItem>()
                            : request.Items
                                .Select(i => new LoadOrderItem
                                {
                                    Pickup = load.Origin.StopOrder,
                                    Delivery = load.Destination.StopOrder,
                                    Weight = i.Weight,
                                    Length = i.Length,
                                    Width = i.Width,
                                    Height = i.Height,
                                    Quantity = i.PackagingQuantity,
                                    PieceCount = i.SaidToContain,
                                    Description = i.Description.GetString(),
                                    Comment = i.Comment.GetString(),
                                    PackageTypeId = i.Packaging == null ? default(long) : i.Packaging.Key,
                                    FreightClass = i.FreightClass == null ? default(double) : i.FreightClass.FreightClass,
                                    Value = i.DeclaredValue,
                                    IsStackable = i.Stackable,
                                    HazardousMaterial = i.HazardousMaterial,
                                    NMFCCode = i.NationalMotorFreightClassification.GetString(),
                                    HTSCode = i.HarmonizedTariffSchedule.GetString(),
                                    Tenant = load.Tenant,
                                    LoadOrder = load,
                                })
                                .ToList();
            load.Items = items;

            // auto mark load as hazmat if there are hazmat items
            if (load.Items.Any(i => i.HazardousMaterial) && !load.HazardousMaterial)
            {
                load.HazardousMaterial = true;
            }

            // mark all items as hazmat if none are and the load is marked as hazmat
            if (load.HazardousMaterial && !load.Items.Any(i => i.HazardousMaterial))
                load.Items.ForEach(i => i.HazardousMaterial = true);


			//Services
			var serviceSearch = new ServiceSearch();
			load.Services = request.Accessorials == null
			              	? new List<LoadOrderService>()
			              	: request.Accessorials
								.Select(service => new LoadOrderService
			              	  	                   	{
			              	  	                   		LoadOrder = load,
			              	  	                   		ServiceId = serviceSearch.FetchServiceIdByCode(service.ServiceCode, user.TenantId),
			              	  	                   		TenantId = user.TenantId,
			              	  	                   	})
			              	  	.ToList();
			CheckForAutoServices(load);
			if (load.Services.Any(s => s.ServiceId == load.Tenant.HazardousMaterialServiceId) && !load.HazardousMaterial)
				load.HazardousMaterial = true;

			// references
			load.CustomerReferences = request.CustomReferences == null
			                        	? new List<LoadOrderReference>()
			                        	: request.CustomReferences
			                        	  	.Select(r =>
			                        	  	        	{
			                        	  	        		var cf = load.Customer.CustomFields.FirstOrDefault(f => f.Name == r.Name);
															return new LoadOrderReference
			                        	  	        		       	{
			                        	  	        		       		TenantId = load.TenantId,
			                        	  	        		       		LoadOrder = load,
			                        	  	        		       		DisplayOnOrigin = cf != null && cf.DisplayOnOrigin,
			                        	  	        		       		DisplayOnDestination = cf != null && cf.DisplayOnDestination,
			                        	  	        		       		Name = r.Name.GetString(),
			                        	  	        		       		Value = r.Value.GetString(),
			                        	  	        		       		Required = r.Required,
			                        	  	        		       	};
			                        	  	        	})
			                        	  	.ToList();

			return load;
		}

        public static LoadOrder ToQuotedLoadOrder(this WSShipment2 shipment, User user, Customer customer, Rate rate, RateCharge selectedGuarOption = null)
		{
			var load = new LoadOrder
			{
				User = user,
				TenantId = user.TenantId,
				Customer = customer,
				DateCreated = DateTime.Now,
				Status = LoadOrderStatus.Quoted,
				Notes = new List<LoadOrderNote>(),
				Stops = new List<LoadOrderLocation>(),
				Equipments = new List<LoadOrderEquipment>(),
				Documents = new List<LoadOrderDocument>(),
				Vendors = new List<LoadOrderVendor>(),

				NotAvailableToLoadboards = true,
				Description = string.Empty,

				HazardousMaterial = shipment.HazardousMaterialShipment,
				HazardousMaterialContactEmail = shipment.HazardousMaterialContactEmail.GetString(),
				HazardousMaterialContactMobile = shipment.HazardousMaterialContactMobile.GetString(),
				HazardousMaterialContactName = shipment.HazardousMaterialContactName.GetString(),
				HazardousMaterialContactPhone = shipment.HazardousMaterialContactPhone.GetString(),

				EmptyMileage = 0m,
				MiscField1 = string.Empty,
				MiscField2 = string.Empty,
				AccountBucketUnitId = default(long),

				DriverName = string.Empty,
				DriverPhoneNumber = string.Empty,
				DriverTrailerNumber = string.Empty,

				CriticalBolComments = string.Empty,
				GeneralBolComments = string.Empty,
				ShipperBol = string.Empty,

				EarlyPickup = shipment.EarliestPickup == null
								? TimeUtility.DefaultOpen
								: shipment.EarliestPickup.Time.GetString(),
				EarlyDelivery = shipment.EarliestDelivery == null
									? TimeUtility.DefaultClose
									: shipment.EarliestDelivery.Time.GetString(),
				LatePickup = shipment.LatestPickup == null
								? TimeUtility.DefaultOpen
								: shipment.LatestPickup.Time.GetString(),
				LateDelivery = shipment.LatestDelivery == null
								? TimeUtility.DefaultClose
								: shipment.LatestDelivery.Time.GetString(),

                EstimatedDeliveryDate = DateUtility.SystemEarliestDateTime,
                DesiredPickupDate = DateUtility.SystemEarliestDateTime
			};

			var countrySearch = new CountrySearch();
			var origin = shipment.Origin ?? new WSLocation2();
			load.Origin = new LoadOrderLocation
			              	{
			              		LoadOrder = load,
			              		TenantId = user.TenantId,
			              		StopOrder = 0,
			              		City = origin.City.GetString(),
			              		CountryId = origin.Country == null
			              		            	? default(long)
			              		            	: countrySearch.FetchCountryIdByCode(origin.Country.Code),
			              		Description = origin.Description.GetString(),
			              		PostalCode = origin.PostalCode.GetString(),
			              		SpecialInstructions = origin.SpecialInstructions.GetString(),
			              		GeneralInfo = origin.GeneralInfo.GetString(),
			              		Direction = origin.Direction.GetString(),
			              		State = origin.State.GetString(),
			              		Street1 = origin.Street.GetString(),
			              		Street2 = origin.StreetExtra.GetString(),
			              		Contacts = new List<LoadOrderContact>
			              		           	{
			              		           		new LoadOrderContact
			              		           			{
			              		           				TenantId = user.TenantId,
			              		           				Name = origin.Contact.GetString(),
			              		           				Comment = origin.ContactComment.GetString(),
			              		           				Phone = origin.Phone.GetString(),
			              		           				Fax = origin.Fax.GetString(),
			              		           				Mobile = origin.Mobile.GetString(),
			              		           				ContactTypeId = user.Tenant.DefaultSchedulingContactTypeId,
			              		           				Email = origin.Email.GetString(),
			              		           				Primary = true
			              		           			}
			              		           	},
			              		AppointmentDateTime = DateUtility.SystemEarliestDateTime,
			              	};
			load.Origin.Contacts[0].Location = load.Origin;

			var destination = shipment.Destination ?? new WSLocation2();
			load.Destination = new LoadOrderLocation
			                   	{
			                   		LoadOrder = load,
			                   		TenantId = user.TenantId,
			                   		StopOrder = 1,
			                   		City = destination.City.GetString(),
			                   		CountryId = destination.Country == null
			                   		            	? default(long)
			                   		            	: countrySearch.FetchCountryIdByCode(destination.Country.Code),
			                   		Description = destination.Description.GetString(),
			                   		PostalCode = destination.PostalCode.GetString(),
			                   		SpecialInstructions = destination.SpecialInstructions.GetString(),
			                   		GeneralInfo = destination.GeneralInfo.GetString(),
			                   		Direction = destination.Direction.GetString(),
			                   		State = destination.State.GetString(),
			                   		Street1 = destination.Street.GetString(),
			                   		Street2 = destination.StreetExtra.GetString(),
			                   		Contacts = new List<LoadOrderContact>
			                   		           	{
			                   		           		new LoadOrderContact
			                   		           			{
			                   		           				TenantId = user.TenantId,
			                   		           				Name = destination.Contact.GetString(),
			                   		           				Comment = destination.ContactComment.GetString(),
			                   		           				Phone = destination.Phone.GetString(),
			                   		           				Fax = destination.Fax.GetString(),
			                   		           				Mobile = destination.Mobile.GetString(),
			                   		           				ContactTypeId = user.Tenant.DefaultSchedulingContactTypeId,
			                   		           				Email = destination.Email.GetString(),
			                   		           				Primary = true
			                   		           			}
			                   		           	},
			                   		AppointmentDateTime = DateUtility.SystemEarliestDateTime,
			                   	};
			load.Destination.Contacts[0].Location = load.Destination;


			load.ResellerAdditionId = customer.Rating.ResellerAdditionId;
			load.BillReseller = customer.Rating.ResellerAdditionId != default(long) && customer.Rating.BillReseller;

			load.AccountBuckets.Add(new LoadOrderAccountBucket
			                        	{
			                        		AccountBucket = load.Customer.DefaultAccountBucket,
			                        		LoadOrder = load,
			                        		Primary = true,
			                        		TenantId = load.TenantId,
			                        	});
			load.Prefix = load.Customer.Prefix;
			load.HidePrefix = load.Customer.HidePrefix;
			load.DesiredPickupDate = shipment.ShipmentDate.TimeToMinimum();
			load.EstimatedDeliveryDate = load.EstimatedDeliveryDate.TimeToMinimum().AddDays(1);

			// references
			load.PurchaseOrderNumber = shipment.PurchaseOrder.GetString();
			load.ShipperReference = shipment.ReferenceNumber.GetString();
			load.CustomerReferences = shipment.CustomReferences == null
										? new List<LoadOrderReference>()
										: shipment.CustomReferences
											.Select(r =>
											{
												var cf = load.Customer.CustomFields.FirstOrDefault(f => f.Name == r.Name);
												return new LoadOrderReference
												{
													TenantId = load.TenantId,
													LoadOrder = load,
													DisplayOnOrigin = cf != null && cf.DisplayOnOrigin,
													DisplayOnDestination = cf != null && cf.DisplayOnDestination,
													Name = r.Name.GetString(),
													Value = r.Value.GetString(),
													Required = r.Required,
												};
											})
											.ToList();

			load.ServiceMode = shipment.BookedRate.ServiceMode;
			load.DeclineInsurance = shipment.DeclineAdditionalInsuranceIfApplicable;

            //Items
            var items = shipment.Items == null
                            ? new List<LoadOrderItem>()
                            : shipment.Items
                                .Select(i => new LoadOrderItem
                                {
                                    Pickup = load.Origin.StopOrder,
                                    Delivery = load.Destination.StopOrder,
                                    Weight = i.Weight,
                                    Length = i.Length,
                                    Width = i.Width,
                                    Height = i.Height,
                                    Quantity = i.PackagingQuantity,
                                    PieceCount = i.SaidToContain,
                                    Description = i.Description.GetString(),
                                    Comment = i.Comment.GetString(),
                                    PackageTypeId = i.Packaging == null ? default(long) : i.Packaging.Key,
                                    FreightClass = i.FreightClass == null ? default(double) : i.FreightClass.FreightClass,
                                    Value = i.DeclaredValue,
                                    IsStackable = i.Stackable,
                                    HazardousMaterial = i.HazardousMaterial,
                                    NMFCCode = i.NationalMotorFreightClassification.GetString(),
                                    HTSCode = i.HarmonizedTariffSchedule.GetString(),
                                    Tenant = load.Tenant,
                                    LoadOrder = load,
                                })
                                .ToList();
            load.Items = items;

            // auto mark load as hazmat if there are hazmat items
            if (load.Items.Any(i => i.HazardousMaterial) && !load.HazardousMaterial)
            {
                load.HazardousMaterial = true;
            }


            // mark all items as hazmat if none are and the load is marked as hazmat
		    if (load.HazardousMaterial && !load.Items.Any(i => i.HazardousMaterial))
		        load.Items.ForEach(i => i.HazardousMaterial = true);

			//Services
			var serviceSearch = new ServiceSearch();
			load.Services = shipment.Accessorials == null
			                	? new List<LoadOrderService>()
			                	: shipment.Accessorials
			                	  	.Select(service => new LoadOrderService
			                	  	                   	{
			                	  	                   		LoadOrder = load,
			                	  	                   		ServiceId = serviceSearch.FetchServiceIdByCode(service.ServiceCode, user.TenantId),
			                	  	                   		TenantId = user.TenantId,
			                	  	                   	})
			                	  	.ToList();
			CheckForAutoServices(load);
			if (load.Services.Any(s => s.ServiceId == user.Tenant.HazardousMaterialServiceId) && !load.HazardousMaterial)
				load.HazardousMaterial = true;

            // charges
            // Add base rates
            load.Charges = rate.BaseRateCharges
                .Select(c => new LoadOrderCharge()
                {
                    TenantId = load.TenantId,
                    LoadOrder = load,
                    ChargeCodeId = c.ChargeCodeId,
                    UnitBuy = c.UnitBuy,
                    UnitSell = c.UnitSell,
                    UnitDiscount = c.UnitDiscount,
                    Quantity = c.Quantity,
                    Comment = string.Empty,
                })
                .ToList();
            // premium rates
            // guaranteed services
            if (selectedGuarOption != null)
            {
                load.CriticalBolComments = !rate.IsProject44Rate
                    ? string.IsNullOrEmpty(selectedGuarOption.LTLGuaranteedCharge.CriticalNotes)
                        ? string.Format("GUARANTEED BY {0} DELIVERY SERVICE ON {1}", selectedGuarOption.LTLGuaranteedCharge.Time,
                            load.EstimatedDeliveryDate.FormattedShortDate())
                        : selectedGuarOption.LTLGuaranteedCharge.CriticalNotes.Replace("[#Date#]",
                            load.EstimatedDeliveryDate.FormattedShortDate())
                    : string.Format("GUARANTEED BY {0} DELIVERY SERVICE ON {1}",
                        Project44Constants.GetGuaranteedServiceTime(selectedGuarOption.ChargeCode.Project44Code),
                        load.EstimatedDeliveryDate.FormattedShortDate());

                load.Charges.Add(new LoadOrderCharge
                {
                    TenantId = load.TenantId,
                    LoadOrder = load,
                    ChargeCodeId = selectedGuarOption.ChargeCodeId,
                    UnitBuy = selectedGuarOption.UnitBuy,
                    UnitSell = selectedGuarOption.UnitSell,
                    UnitDiscount = selectedGuarOption.UnitDiscount,
                    Quantity = selectedGuarOption.Quantity,
                    Comment = string.Empty,
                });
            }

            // sales representative
            var srtier = customer.SalesRepresentative == null
                             ? null
                             : customer.SalesRepresentative.ApplicableTier(load.DateCreated, load.ServiceMode, customer);
            load.SalesRepresentative = customer.SalesRepresentative;
            load.SalesRepresentativeCommissionPercent = srtier != null ? srtier.CommissionPercent : 0m;
            load.SalesRepAddlEntityCommPercent = customer.SalesRepresentative != null ? customer.SalesRepresentative.AdditionalEntityCommPercent : 0m;
            load.SalesRepAddlEntityName = customer.SalesRepresentative != null ? customer.SalesRepresentative.AdditionalEntityName : string.Empty;
            load.SalesRepMaxCommValue = srtier != null ? srtier.CeilingValue : 0m;
            load.SalesRepMinCommValue = srtier != null ? srtier.FloorValue : 0m;

            return load;
		}




		private static void CheckForAutoServices(Shipment shipment)
		{
			if (shipment.IsInternational() && shipment.Tenant.BorderCrossingServiceId != default(long) &&
				shipment.Services.All(s => s.ServiceId != shipment.Tenant.BorderCrossingServiceId))
			{
				shipment.Services.Add(new ShipmentService
				                      	{
				                      		TenantId = shipment.TenantId,
				                      		Shipment = shipment,
				                      		ServiceId = shipment.Tenant.BorderCrossingServiceId
				                      	});

			}

			if (shipment.HazardousMaterial && shipment.Tenant.HazardousMaterialServiceId != default(long) &&
				shipment.Services.All(s => s.ServiceId != shipment.Tenant.HazardousMaterialServiceId))
			{
				shipment.Services.Add(new ShipmentService
				                      	{
				                      		TenantId = shipment.TenantId,
				                      		Shipment = shipment,
				                      		ServiceId = shipment.Tenant.HazardousMaterialServiceId
				                      	});
			}
		}

		private static void CheckForAutoServices(LoadOrder load)
		{
			if (load.IsInternational() && load.Tenant.BorderCrossingServiceId != default(long) &&
			    load.Services.All(s => s.ServiceId != load.Tenant.BorderCrossingServiceId))
			{
				load.Services.Add(new LoadOrderService
				                     	{
				                     		TenantId = load.Tenant.Id,
				                     		LoadOrder = load,
				                     		ServiceId = load.Tenant.BorderCrossingServiceId
				                     	});
			}

			if (load.HazardousMaterial && load.Tenant.HazardousMaterialServiceId != default(long) &&
			    load.Services.All(s => s.ServiceId != load.Tenant.HazardousMaterialServiceId))
			{
				load.Services.Add(new LoadOrderService
				                     	{
				                     		TenantId = load.Tenant.Id,
											LoadOrder = load,
				                     		ServiceId = load.Tenant.HazardousMaterialServiceId
				                     	});
			}
		}
	}
}
