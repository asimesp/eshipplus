﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSShipmentDocumentInfo : WSReturn
	{
		public string DocumentName { get; set; }

		public long DocumentKey { get; set; }

		public WSShipmentDocumentInfo()
		{
		    DocumentName = null;
		    DocumentKey = default(long);
		}
	}
}
