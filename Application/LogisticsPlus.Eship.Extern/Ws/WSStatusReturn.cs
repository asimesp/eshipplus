﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSStatusReturn : WSReturn
	{
		public WSStatus[] Statuses { get; set; }

		public WSStatusReturn()
		{
			Statuses = null;
		}
	}
}
