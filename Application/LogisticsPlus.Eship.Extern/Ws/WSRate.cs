﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSRate
	{
		public int CarrierKey { get; set; }
		public string CarrierName { get; set; }
		public decimal Freight { get; set; }
		public decimal Fuel { get; set; }
		public decimal Accessorials { get; set; }
		public double BillWeight { get; set; }
		public double RatedWeight { get; set; }
		public double RatedCubicFeet { get; set; }
		public int TransitTime { get; set;}
		public WSEnums.Mode Mode { get; set; }

		public bool AdditionalInsuranceAvailable { get; set; }
		public decimal AdditionalInsuranceCost { get; set; }

		public decimal TotalCharge { get { return Freight + Fuel + Accessorials + AdditionalInsuranceCost; } }

		public WSRate()
		{
			CarrierKey = 0;
			CarrierName = string.Empty;
			Freight = 0;
			Fuel = 0;
			Accessorials = 0;
			BillWeight = 0;
			RatedWeight = 0;
			RatedCubicFeet = 0;
			TransitTime = 0;
			Mode = WSEnums.Mode.Null;

			AdditionalInsuranceAvailable = false;
			AdditionalInsuranceCost = 0m;
		}
	}
}
