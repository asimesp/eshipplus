﻿using System;


namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSReference : WSReturn
	{
		public string Name { get; set; }
		public string Value { get; set; }
		public bool Required { get; set; }

		public WSReference()
		{
			Name = null;
			Value = null;
			Required = false;
		}
	}
}
