﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSLocation2
	{
		public string Description { get; set; }
		public string Street { get; set; }
		public string StreetExtra { get; set; }
		public string PostalCode { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public WSCountry Country { get; set; }

		public string SpecialInstructions { get; set; }
		public string GeneralInfo { get; set; }
		public string Direction { get; set; }

		public string Email { get; set; }
		public string Contact { get; set; }
		public string Phone { get; set; }
		public string Fax { get; set; }
		public string Mobile { get; set; }
		public string ContactComment { get; set; }

		public WSLocation2()
		{
			Description = null;
			Street = null;
			StreetExtra = null;
			PostalCode = null;
			City = null;
			State = null;
			Email = null;
			Contact = null;
			SpecialInstructions = null;
			GeneralInfo = null;
			Direction = null;
			Country = null;
			Phone = null;
			Fax = null;
			Mobile = null;
			ContactComment = null;
		}
	}
}
