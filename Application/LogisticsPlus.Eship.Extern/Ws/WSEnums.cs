﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSEnums
	{
		public enum WeightUnit { Kg, Lb }
		public enum DimensionUnit { Cm, In }
		public enum Mode { LTL, SmallPack, FTL, Null }
		public enum DocumentType { Pdf, Html, Png, Gif }
		public enum TimeUnit { AM, PM }
		public enum MessageType { Error, Information, Warning, Null }
		public enum PONumberValidity {Origin, Destination }
		public enum BookingRequestStatus { PendingCover, Covered, Cancelled }
		public enum BookingReferenceNumberType { Shipment, Quote, NotApplicable }

		public enum DutiesPayor { SENDER, RECIPIENT }
		public enum B13AFiling { FILED_ELECTRONICALLY, MANUALLY_ATTACHED, NOT_REQUIRED, SUMMARY_REPORTING }
		public enum DestinationControlStatement { DEPARTMENT_OF_COMMERCE, DEPARTMENT_OF_STATE, Both }
		public enum DropOffs { BUSINESS_SERVICE_CENTER, REGULAR_PICKUP, DROP_BOX, STATION }
		public enum ResidenceDelivery { APPOINTMENT, EVENING, NOT_APPLICABLE }
		public enum Signature { NO_SIGNATURE_REQUIRED, ADULT, DIRECT, INDIRECT, SERVICE_DEFAULT }
	}
}
