﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSLocation
	{
		public string Name { get; set; }
		public string Street { get; set; }
		public string PostalCode { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string Email { get; set; }
		public string Contact { get; set; }
		public string SpecialInstructions { get; set; }
		public WSOption Country { get; set; }

		public string Phone { get; set; }
		public string Fax { get; set; }


		public WSLocation()
		{
			Name = string.Empty;
			Street = string.Empty;
			PostalCode = string.Empty;
			City = string.Empty;
			State = string.Empty;
			Email = string.Empty;
			Contact = string.Empty;
			SpecialInstructions = string.Empty;
			Country = null;
			Phone = string.Empty;
			Fax = string.Empty;
		}
	}
}
