﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSShipment2 : WSReturn
	{
		public string BookingReferenceNumber { get; set; }
		public WSEnums.BookingReferenceNumberType BookingReferenceNumberType { get; set; }

		public WSLocation2 Origin { get; set; }
        public bool SendShipmentUpdateToOriginPrimaryContact { get; set; }

        public WSLocation2 Destination { get; set; }
        public bool SendShipmentUpdateToDestinationPrimaryContact { get; set; }


        public string ReferenceNumber { get; set; }
		public string PurchaseOrder { get; set; }
		public string ShipperBOL { get; set; }


	    [XmlIgnore]
        public bool OverrideApiRatingDates { get; set; }

        [XmlIgnore]
        public bool DisableApiBookingNotifications { get; set; }

		public WSReference[] CustomReferences { get; set; }

		public DateTime ShipmentDate { get; set; }

		public WSTime2 EarliestPickup { get; set; }
		public WSTime2 LatestPickup { get; set; }
		public WSTime2 EarliestDelivery { get; set; }
		public WSTime2 LatestDelivery { get; set; }

		public WSItem2[] Items { get; set; }

		public WSAccessorialService[] Accessorials { get; set; }

		public bool DeclineAdditionalInsuranceIfApplicable { get; set; }

		public bool HazardousMaterialShipment { get; set; }
		public string HazardousMaterialContactName { get; set; }
		public string HazardousMaterialContactPhone { get; set; }
		public string HazardousMaterialContactMobile { get; set; }
		public string HazardousMaterialContactEmail { get; set; }

		public WSRate2[] AvailableRates { get; set; }
		public WSRate2 SelectedRate { get; set; }
		public WSRate2 BookedRate { get; set; }

		public WSDocument[] ReturnConfirmations { get; set; }

		public WSShipment2()
		{
			BookingReferenceNumber = null;
			BookingReferenceNumberType = WSEnums.BookingReferenceNumberType.NotApplicable;

			ShipperBOL = null;

			Origin = null;
			Destination = null;

			ReferenceNumber = null;
			PurchaseOrder = null;
			CustomReferences = null;		

			ShipmentDate = DateTime.Now;

			EarliestDelivery = null;
			EarliestPickup = null;
			LatestDelivery = null;
			LatestPickup = null;

			Items = null;

			Accessorials = null;

			DeclineAdditionalInsuranceIfApplicable = false;

			HazardousMaterialContactEmail = null;
			HazardousMaterialContactMobile = null;
			HazardousMaterialContactPhone = null;
			HazardousMaterialContactName = null;
			HazardousMaterialShipment = false;

			AvailableRates = null;
			SelectedRate = null;
			BookedRate = null;

			ReturnConfirmations = null;

		    OverrideApiRatingDates = false;
		    DisableApiBookingNotifications = false;
        }
	}
}
