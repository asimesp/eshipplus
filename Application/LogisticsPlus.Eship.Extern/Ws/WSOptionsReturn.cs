﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSOptionsReturn : WSReturn
	{
		public WSOption[] Options { get; set; }
		
		public WSOptionsReturn()
		{
			Options = null;
		}
	}
}
