﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSCountry : WSReturn
	{
		public string Code { get; set; }
		public string Name { get; set; }
		public bool UsesPostalCode { get; set; }

		public WSCountry()
		{
			Code = null;
			Name = null;
			UsesPostalCode = false;
		}
	}
}
