﻿using System;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSBillingDetail : WSReturn
	{
		public string ReferenceNumber { get; set; }
		public DetailReferenceType ReferenceType { get; set; }
		public string BillingCode { get; set; }
		public string Description { get; set; }
		public ChargeCodeCategory Category { get; set; }
		public decimal AmountDue { get; set; }

		public WSBillingDetail()
		{
			ReferenceNumber = null;
			ReferenceType = DetailReferenceType.Miscellaneous;
			BillingCode = null;
			Description = null;
			Category = ChargeCodeCategory.Freight;
			AmountDue = 0m;
		}

		protected bool Equals(WSBillingDetail other)
		{
			return string.Equals(ReferenceNumber, other.ReferenceNumber) && ReferenceType == other.ReferenceType && string.Equals(BillingCode, other.BillingCode) && string.Equals(Description, other.Description) && Category == other.Category && AmountDue == other.AmountDue;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != GetType()) return false;
			return Equals((WSBillingDetail) obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = (ReferenceNumber != null ? ReferenceNumber.GetHashCode() : 0);
				hashCode = (hashCode*397) ^ (int) ReferenceType;
				hashCode = (hashCode*397) ^ (BillingCode != null ? BillingCode.GetHashCode() : 0);
				hashCode = (hashCode*397) ^ (Description != null ? Description.GetHashCode() : 0);
				hashCode = (hashCode*397) ^ (int) Category;
				hashCode = (hashCode*397) ^ AmountDue.GetHashCode();
				return hashCode;
			}
		}

		public static bool operator ==(WSBillingDetail obj1, WSBillingDetail obj2)
		{
			if (ReferenceEquals(obj1, obj2)) return true;
			

			return (object)obj1 != null && (object)obj2 != null && obj1.Equals(obj2);
		}

		public static bool operator !=(WSBillingDetail obj1, WSBillingDetail obj2)
		{
			return !(obj1 == obj2);
		}
	}
}
