﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSPackageShipmentDetails
	{
		public float DryIceWeight { get; set; }
		public WSEnums.ResidenceDelivery Delivery { get; set; }
		public WSEnums.DropOffs DropOff { get; set; }
		public WSEnums.Signature Signature { get; set; }

		public WSEnums.DutiesPayor DutiesPayor { get; set; }
		public string PaymentAccountNumber { get; set; }
		public decimal CustomsValue { get; set; }
		public bool ExportDetailSpecified { get; set; }
		public WSEnums.B13AFiling B13AFiling { get; set; }
		public string ExportCompliance { get; set; }
		public string PermitNumber { get; set; }
		public bool DestinationControlSpecified { get; set; }
		public WSEnums.DestinationControlStatement DestinationControlStatement { get; set; }
		public string DestinationCountries { get; set; }
		public string DestinationUser { get; set; }

		public WSPackageShipmentDetails()
		{
			DryIceWeight = 0;
			Delivery = WSEnums.ResidenceDelivery.APPOINTMENT;
			DropOff = WSEnums.DropOffs.BUSINESS_SERVICE_CENTER;
			Signature = WSEnums.Signature.NO_SIGNATURE_REQUIRED;

			DutiesPayor = WSEnums.DutiesPayor.SENDER;
			PaymentAccountNumber = string.Empty;
			CustomsValue = 0;
			ExportDetailSpecified = false;
			B13AFiling = WSEnums.B13AFiling.NOT_REQUIRED;
			ExportCompliance = string.Empty;
			PermitNumber = string.Empty;
			DestinationControlSpecified = false;
			DestinationControlStatement = WSEnums.DestinationControlStatement.Both;
			DestinationCountries = string.Empty;
			DestinationUser = string.Empty;
		}
	}
}
