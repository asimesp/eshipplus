﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSAvailableLoadsReturn : WSReturn
	{
		public WSAvailableLoad[] AvailableLoads { get; set; }


		public WSAvailableLoadsReturn()
		{
			AvailableLoads = null;
		}
	}
}
