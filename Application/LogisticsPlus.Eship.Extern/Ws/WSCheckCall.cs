﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSCheckCall
	{
		public string CallDate { get; set; }
		public string EventDate { get; set; }
		public string CallNotes { get; set; }
		public string StatusCode { get; set; }

		public WSCheckCall()
		{
			CallDate = string.Empty;
			EventDate = string.Empty;
			CallNotes = string.Empty;
			StatusCode = string.Empty;
		}
	}
}
