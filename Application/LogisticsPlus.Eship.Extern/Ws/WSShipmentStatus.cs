﻿using System;
using System.Xml.Serialization;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSShipmentStatus : WSReturn
	{
		public string ShipmentNumber { get; set; }
		public ShipmentStatus Status { get; set; }
		public DateTime DateCreated { get; set; }
		public DateTime EstimatePickupDate { get; set; }
		public DateTime EstimateDeliveryDate { get; set; }
		public DateTime ActualPickupDate { get; set; }
		public DateTime ActualDeliveryDate { get; set; }

		public bool IsPickedUp { get; set; }
		public bool IsDelivered { get; set; }

		public ServiceMode Mode { get; set; }

		public string Pro { get; set; }
		public string VendorKey { get; set; }
		public string VendorName { get; set; }
		public string VendorScac { get; set; }

		public WSCheckCall[] CheckCalls { get; set; }
	
		public WSBillingDetail[] BillingDetails { get; set; }

        [XmlIgnore]
        public WSShipmentDocumentInfo[] Documents { get; set; }

		public WSShipmentStatus()
		{
			ShipmentNumber = null;
			Status = ShipmentStatus.Scheduled;
			DateCreated = DateTime.Now;
			EstimatePickupDate = DateTime.Now;
			EstimateDeliveryDate = DateTime.Now;
			ActualDeliveryDate = DateUtility.SystemEarliestDateTime;
			ActualPickupDate = DateUtility.SystemEarliestDateTime;

			IsPickedUp = false;
			IsDelivered = false;

			Mode = ServiceMode.NotApplicable;

			Pro = null;
			VendorKey = null;
			VendorName = null;
			VendorScac = null;


			CheckCalls = null;
			BillingDetails = null;
		    Documents = null;
		}
	}
}
