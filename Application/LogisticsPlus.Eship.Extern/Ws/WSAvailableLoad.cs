﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSAvailableLoad
	{
		public string LoadNumber { get; set; }

		public DateTime PickUpDate { get; set; }
		public DateTime DeliveryDate { get; set; }

		public string OriginCity { get; set; }
		public string OriginState { get; set; }
		public string DestinationCity { get; set; }
		public string DestinationState { get; set; }
		
		public WSItem[] Items { get; set; }
		
		public string Weight { get; set; }
		public string EquipmentTypes { get; set; }
		public string ContactPhone { get; set; }
		public string Comments { get; set; }


		public WSAvailableLoad()
		{
			LoadNumber = string.Empty;

			PickUpDate = DateTime.Now.Date;
			DeliveryDate = DateTime.Now.Date;

			OriginCity = string.Empty;
			OriginState = string.Empty;
			DestinationCity = string.Empty;
			DestinationState = string.Empty;

			Items = null;

			Weight = string.Empty;
			EquipmentTypes = string.Empty;
			ContactPhone = string.Empty;
			Comments = string.Empty;
		}
	}
}
