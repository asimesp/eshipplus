﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSShipmentDocument : WSReturn
	{
		public string DocumentFileName { get; set; }

		public long DocumentKey { get; set; }

		public string EncodedDocumentBytes { get; set; }

		public WSShipmentDocument()
		{
		    DocumentFileName = null;
		    DocumentKey = default(long);
		    EncodedDocumentBytes = null;
		}
	}
}
