﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSItem
	{
		public double Weight { get; set; }
		public WSEnums.WeightUnit WeightUnit { get; set; }

		public int Quantity { get; set; }

		public int Height { get; set; }
		public int Width { get; set; }
		public int Length { get; set; }
		public WSEnums.DimensionUnit DimsUnit { get; set; }

		public bool IsStackable { get; set; }

		public decimal DeclaredValue { get; set; }

		public string Description { get; set; }
		public string NationalMotorFreightClassification { get; set; }
		public string HarmonizedTariffSchedule { get; set; }
		public int SaidToContain { get; set; }

		public WSOption Packaging { get; set; }
		public WSOption FreightClass { get; set; }
		public WSOption CommodityType { get; set; }

		public WSItem()
		{
			Weight = 0;
			WeightUnit = WSEnums.WeightUnit.Lb;

			Quantity = 1;

			Height = 0;
			Width = 0;
			Length = 0;
			DimsUnit = WSEnums.DimensionUnit.In;

			IsStackable = false;

			DeclaredValue = 0;

			Description = string.Empty;
			NationalMotorFreightClassification = string.Empty;
			SaidToContain = 0;

			Packaging = null;
			FreightClass = null;
			CommodityType = null;
		}
	}
}
