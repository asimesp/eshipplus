﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSTime
	{
		public int Hour { get; set; }
		public int Minute { get; set; }
		public int Second { get; set; }
		public WSEnums.TimeUnit TimeUnit { get; set; }

		public WSTime()
		{
			Hour = 12;
			Minute = 00;
			Second = 00;
			TimeUnit = WSEnums.TimeUnit.AM;
		}

		public bool Equals(WSTime other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return other.Hour == Hour && other.Minute == Minute && other.Second == Second && Equals(other.TimeUnit, TimeUnit);
		}
		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != typeof (WSTime)) return false;
			return Equals((WSTime) obj);
		}
		public override int GetHashCode()
		{
			unchecked
			{
				int result = Hour;
				result = (result*397) ^ Minute;
				result = (result*397) ^ Second;
				result = (result*397) ^ TimeUnit.GetHashCode();
				return result;
			}
		}
	}
}
