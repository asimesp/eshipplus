﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
    public class WSTrackingInfoRequest
    {
		public string Scac { get; set; }

    	public string BillOfLadingNumber { get; set; }

        public DateTime ScheduledPickupDate { get; set; }

        public string OriginPostalCode { get; set; }    
        
        public string DestinationPostalCode { get; set; }

        public string OriginCountryCode { get; set; }

        public string DestinationCountryCode { get; set; }

        public string ProNumber { get; set; }

        public WSTrackingInfoRequest()
        {
            BillOfLadingNumber = string.Empty;
            ScheduledPickupDate = DateTime.Now.Date;
            OriginPostalCode = string.Empty;
            DestinationPostalCode = string.Empty;
            OriginCountryCode = string.Empty;
            DestinationCountryCode = string.Empty;
            ProNumber = string.Empty;
        }
    }
}
