﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSItemPackage : WSReturn
	{
		public long Key { get; set; }
		public string PackageName { get; set; }
		public decimal DefaultLength { get; set; }
		public decimal DefaultHeight { get; set; }
		public decimal DefaultWidth { get; set; }

		public WSItemPackage()
		{
			Key = 0;
			PackageName = null;
			DefaultHeight = 0;
			DefaultLength = 0;
			DefaultWidth = 0;
		}
	}
}
