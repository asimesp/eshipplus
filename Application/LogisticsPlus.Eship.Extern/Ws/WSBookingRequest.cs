﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSBookingRequest : WSReturn
	{
		public string SubmittedRequestNumber { get; set; }

		public WSLocation2 Origin { get; set; }
		public WSLocation2 Destination { get; set; }

		public string ReferenceNumber { get; set; }
		public string PurchaseOrder { get; set; }

	    [XmlIgnore]
	    public bool OverrideApiRatingDates { get; set; }

	    [XmlIgnore]
	    public bool DisableApiBookingNotifications { get; set; }

        public WSReference[] CustomReferences { get; set; }

		public DateTime DesiredShipmentDate { get; set; }

		public WSTime2 EarliestPickup { get; set; }
		public WSTime2 LatestPickup { get; set; }
		public WSTime2 EarliestDelivery { get; set; }
		public WSTime2 LatestDelivery { get; set; }

		public WSItem2[] Items { get; set; }

		public WSAccessorialService[] Accessorials { get; set; }

		public bool DeclineAdditionalInsuranceIfApplicable { get; set; }

		public bool HazardousMaterialShipment { get; set; }
		public string HazardousMaterialContactName { get; set; }
		public string HazardousMaterialContactPhone { get; set; }
		public string HazardousMaterialContactMobile { get; set; }
		public string HazardousMaterialContactEmail { get; set; }

		public WSBookingRequest()
		{
			SubmittedRequestNumber = null;

			Origin = null;
			Destination = null;

			ReferenceNumber = null;
			PurchaseOrder = null;
			CustomReferences = null;

			DesiredShipmentDate = DateTime.Now;

			EarliestPickup = null;
			LatestPickup = null;
			EarliestDelivery = null;
			LatestDelivery = null;

			Items = null;

			Accessorials = null;

			DeclineAdditionalInsuranceIfApplicable = false;

			HazardousMaterialContactEmail = null;
			HazardousMaterialContactMobile = null;
			HazardousMaterialContactPhone = null;
			HazardousMaterialContactName = null;
			HazardousMaterialShipment = false;

		    OverrideApiRatingDates = false;
		    DisableApiBookingNotifications = false;
		}
	}
}
