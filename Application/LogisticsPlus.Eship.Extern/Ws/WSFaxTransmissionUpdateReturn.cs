﻿namespace LogisticsPlus.Eship.Extern.Ws
{
	public class WSFaxTransmissionUpdateReturn : WSReturn
	{
		public WSFaxTransmissionUpdate TransmissionUpdate { get; set; }
	}
}
