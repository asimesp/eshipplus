﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSCredentials
	{
		public string AccessCode { get; set; }
		public string Username { get; set; }
		public string Password { get; set; }
		public Guid AccessKey { get; set; }

		public WSCredentials()
		{
			Username = string.Empty;
			Password = string.Empty;
			AccessKey = Guid.Empty;
		}

		public void SetKey(string accesskey)
		{
			try
			{
				AccessKey = new Guid(accesskey);
			}
			catch
			{
				AccessKey = Guid.Empty;
			}
		}

		public bool Equals(WSCredentials other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return Equals(other.Username, Username) && Equals(other.Password, Password) && Equals(other.AccessKey, AccessKey);
		}
		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != typeof (WSCredentials)) return false;
			return Equals((WSCredentials) obj);
		}
		public override int GetHashCode()
		{
			unchecked
			{
				int result = (Username != null ? Username.GetHashCode() : 0);
				result = (result*397) ^ (Password != null ? Password.GetHashCode() : 0);
				result = (result*397) ^ (AccessKey.GetHashCode());
				return result;
			}
		}
	}
}