﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSVendorProNumber : WSReturn
	{
		public string ReferenceNumber { get; set; }
		public string VendorScac { get; set; }
		public string VendorProNumber { get; set; }

        public WSVendorProNumber()
		{
            ReferenceNumber = string.Empty;
            VendorScac = string.Empty;
            VendorProNumber = string.Empty;
		}
	}
}
