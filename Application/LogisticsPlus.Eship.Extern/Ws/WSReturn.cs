﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSReturn
	{
		public WSMessage[] Messages { get; set; }

		public bool ContainsErrorMessage { get { return WSMessage.ContainsError(Messages); } }

		public WSReturn()
		{
			Messages = null;
		}
	}
}
