﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSTrackingInfoRequestReturn : WSReturn
	{
		public WSTrackingInfoRequest[] TrackingInfoRequests { get; set; }


		public WSTrackingInfoRequestReturn()
		{
			TrackingInfoRequests = null;
		}
	}
}
