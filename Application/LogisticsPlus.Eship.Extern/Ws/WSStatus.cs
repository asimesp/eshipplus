﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSStatus
	{
		public string BillOfLading { get; set; }
		public string ProNumber { get; set; }
		public string Status { get; set; }
		public string DeliveryDate { get; set; }
	}
}
