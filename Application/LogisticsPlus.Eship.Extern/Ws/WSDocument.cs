﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSDocument
	{
		public WSEnums.DocumentType DocType { get; set; }
		public Byte[] DocImage { get; set; }
		public string Html { get; set;}
		public string Name { get; set; }

		public WSDocument()
		{
			DocType = WSEnums.DocumentType.Html;
			DocImage = null;
			Html = string.Empty;
		}
	}
}
