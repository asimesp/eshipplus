﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSTime2 : WSReturn
	{
		public string Time { get; set; }

		public WSTime2()
		{
			Time = string.Empty;
		}
	}
}
