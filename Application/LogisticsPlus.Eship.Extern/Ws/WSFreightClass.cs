﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSFreightClass : WSReturn
	{
		public double FreightClass { get; set; }

		public WSFreightClass()
		{
			FreightClass = 0;
		}
	}
}
