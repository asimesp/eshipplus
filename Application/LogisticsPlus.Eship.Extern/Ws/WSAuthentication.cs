﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSAuthentication
	{
		[XmlIgnore]
		public string UserName { get; set; }

		[XmlIgnore]
		public string Password { get; set; }

		[XmlIgnore]
		public Guid AccessKey { get; set; }

		[XmlIgnore]
		public string AccessCode { get; set; }
	}
}
