﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSBookingRequestStatus : WSReturn
	{
		public string BookingNumber { get; set; }
		public WSEnums.BookingRequestStatus Status { get; set; }

		public WSShipmentStatus[] Shipments { get; set; }

		public WSBookingRequestStatus()
		{
			BookingNumber = null;
			Status = WSEnums.BookingRequestStatus.PendingCover;
			Shipments = null;
		}
	}
}
