﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	public class WSShipmentTrackingInfo
	{
		public string Scac { get; set; }
		public string ShipmentNumber { get; set; }
		public DateTime PickUpDate { get; set; }
		public bool PickupDatePresent { get; set; }
		public DateTime DeliveryDate { get; set; }
		public bool DeliveryDatePresent { get; set; }
		public string ProNumber { get; set; }
		public bool ProNumberIsPresent { get; set; }
	}
}
