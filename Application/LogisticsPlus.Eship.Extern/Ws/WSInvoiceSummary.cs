﻿using System;
using LogisticsPlus.Eship.Core.Accounting;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSInvoiceSummary : WSReturn
	{
		public string InvoiceNumber { get; set; }
		public DateTime Date { get; set; }
		public DateTime DueDate { get; set; }
		public InvoiceType InvoiceType { get; set; }
		public string SpecialInstruction { get; set; }
		public string LocationStreet { get; set; }
		public string LocationStreetExtra { get; set; }
		public string LocationCity { get; set; }
		public string LocationState { get; set; }
		public string LocationPostalCode { get; set; }
		public string LocationCountryName { get; set; }
		public decimal TotalAmount { get; set; }
		public string OriginalInvoiceNumber { get; set; }

		public WSInvoiceSummary()
		{
			InvoiceNumber = null;
			Date = DateTime.Now;
			DueDate = DateTime.Now;
			InvoiceType = InvoiceType.Invoice;
			SpecialInstruction = null;
			LocationCity = null;
			LocationCountryName = null;
			LocationPostalCode = null;
			LocationState = null;
			LocationStreet = null;
			LocationStreetExtra = null;
			TotalAmount = 0m;
			OriginalInvoiceNumber = null;
		}
	}
}
