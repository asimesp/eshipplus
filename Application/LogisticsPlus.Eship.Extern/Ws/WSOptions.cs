﻿using System;

namespace LogisticsPlus.Eship.Extern.Ws
{
	[Serializable]
	public class WSOptions : WSReturn
	{
		public WSAccessorialService[] AccessorialServices { get; set; }
		public WSFreightClass[] LTLFreightClasses { get; set; }
		public WSItemPackage[] ItemPackaging { get; set; }
		public WSCountry[] Countries { get; set; }
		public WSTime2[] Times { get; set; }

		public WSOptions()
		{
			AccessorialServices = null;
			LTLFreightClasses = null;
			ItemPackaging = null;
			Countries = null;
			Times = null;
		}
	}
}
