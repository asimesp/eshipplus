﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Extern.Edi;
using LogisticsPlus.Eship.Extern.Edi.Segments;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using P44SDK.V4.Model;
using Contact = LogisticsPlus.Eship.Extern.Edi.Segments.Contact;

namespace LogisticsPlus.Eship.Extern
{
    public static class ExternUtilities
    {
        public const int InvalidCount = -1;


		public static Edi204 ToEdi204ForUpdate(this LoadOrder order, string controlNumber, string truckLoadBidExpirationTime = "")
        {
            var edi204 = order.ToEdi204(controlNumber, truckLoadBidExpirationTime);
            edi204.Loads[0].SetPurpose.TransactionSetPurpose = SetPurposeTransType.UP;
            return edi204;
        }

		public static Edi204 ToEdi204ForCancellation(this LoadOrder order, string controlNumber, string truckLoadBidExpirationTime = "")
        {
            var edi204 = order.ToEdi204(controlNumber, truckLoadBidExpirationTime);
            edi204.Loads[0].SetPurpose.TransactionSetPurpose = SetPurposeTransType.CA;
            return edi204;
        }

		public static Edi204 ToEdi204(this LoadOrder order, string controlNumber, string truckLoadBidExpirationTime = "")
        {
            var primaryVendor = (order.Vendors.FirstOrDefault(v => v.Primary) ?? new LoadOrderVendor()).Vendor;
            var equipment = order.Equipments.FirstOrDefault();
            var originStopTime = order.Origin.AppointmentDateTime == DateUtility.SystemEarliestDateTime
                                     ? order.DesiredPickupDate
                                     : order.Origin.AppointmentDateTime;
            var destStopTime = order.Destination.AppointmentDateTime == DateUtility.SystemEarliestDateTime
                                   ? order.EstimatedDeliveryDate
                                   : order.Destination.AppointmentDateTime;

            var allStops = order.Stops;
            allStops.Insert(0, order.Origin);
            allStops.Add(order.Destination);
            var pItems = order.Items.GroupBy(u => u.Pickup).ToDictionary(u => u.Key, u => u.Select(x => x));
            var dItems = order.Items.GroupBy(u => u.Delivery).ToDictionary(u => u.Key, u => u.Select(x => x));

            var ediStopsData = allStops
                .SelectMany(s => pItems
                                     .Where(d => d.Key == s.StopOrder)
                                     .Select(x => new
                                         {
                                             Stop = s,
                                             IsShipper = s.StopOrder == order.Origin.StopOrder,
                                             IsConsignee = s.StopOrder == order.Destination.StopOrder,
                                             IsPickup = true,
                                             Item = x.Value.ToList()
                                         }))
                .ToList();
            ediStopsData.AddRange(
                allStops
                    .SelectMany(s => dItems
                                         .Where(d => d.Key == s.StopOrder)
                                         .Select(x => new
                                             {
                                                 Stop = s,
                                                 IsShipper = s.StopOrder == order.Origin.StopOrder,
                                                 IsConsignee = s.StopOrder == order.Destination.StopOrder,
                                                 IsPickup = false,
                                                 Item = x.Value.ToList()
                                             })
                                         .ToList()));
            var seqs = ediStopsData.Select(s => s.Stop.StopOrder);
            ediStopsData.AddRange(
                allStops
                    .Where(s => !seqs.Contains(s.StopOrder))
                    .Select(s => new
                        {
                            Stop = s,
                            IsShipper = false,
                            IsConsignee = false,
                            IsPickup = false,
                            Item = new List<LoadOrderItem>()
                        })
                    .ToList());

            ediStopsData = ediStopsData.OrderBy(s => s.Stop.StopOrder).ToList();

            // reference numbers
            var refNums = order.CustomerReferences
                               .Select(r => new ReferenceNumber
                                   {
                                       Description = r.Name,
                                       ReferenceIdentification = r.Value,
                                       ReferenceIdentificationQualifier = RefIdQualifier.CUS
                                   })
                               .ToList();
            if (!string.IsNullOrEmpty(order.ShipperReference))
                refNums.Add(new ReferenceNumber
                    {
                        Description = string.Empty,
                        ReferenceIdentification = order.ShipperReference,
                        ReferenceIdentificationQualifier = RefIdQualifier.SHR
                    });
            if (!string.IsNullOrEmpty(order.PurchaseOrderNumber))
                refNums.Add(new ReferenceNumber
                    {
                        Description = string.Empty,
                        ReferenceIdentification = order.PurchaseOrderNumber,
                        ReferenceIdentificationQualifier = RefIdQualifier.PO
                    });

            // load
            var load = new Load
                {
                    BeginningSegment = new BeginningSegmentShipmentInfo
                        {
                            CustomsDocumentHandlingCode = string.Empty,
                            PaymentMethodCode = ShipMethodOfPay.TP,
                            ShipmentIdNumber = order.LoadOrderNumber,
                            ShipmentMethodOfPayment = string.Empty,
                            ShipmentQualifier = ShipmentQualifier.G.GetString(),
                            ShipmentWeightCode = string.Empty,
                            StandardCarrierAlphaCode = primaryVendor.Scac,
                            StandardPointLocationCode = string.Empty,
                            TariffServiceCode = string.Empty,
                            TotalEquipment = equipment == null ? 0 : 1,
                            TransportationTermsCode = string.Empty,
                            WeightUnitQualifier = WghtUnitQualifier.L
                        },
                    EquipmentDetail = equipment == null
                                          ? null
                                          : new EquipmentDetail
                                              {
                                                  EquipmentDescriptionCode = equipment.EquipmentType.Code,
                                                  EquipmentNumber = string.Empty
                                              },
	                ExpirationDate = truckLoadBidExpirationTime != string.Empty
		                                 ? truckLoadBidExpirationTime.FormattedLongDateAlt()
		                                 : DateTime.Now.AddMinutes(primaryVendor.Communication.LoadTenderExpAllowance)
		                                           .FormattedLongDateAlt(),
                    LoadReferenceNumbers = refNums,
                    Remarks = new Remarks
                        {
                            FreeFormMessage = order.GeneralBolComments,
                            FreeFormMessage2 = order.CriticalBolComments
                        },
                    SetPurpose = new SetPurpose
                        {
                            ApplicationType = SetPurposeAppType.LT,
                            TransactionSetPurpose = SetPurposeTransType.OR
                        },
                    ShipmentWeightAndTrackingData = new ShipmentWghtPkgAndQtyData
                        {
                            LadingPackages = order.Items.Sum(u => u.Quantity),
                            LadingPieces = order.Items.Sum(u => u.PieceCount),
                            Weight = order.Items.Sum(u => u.Weight),
                            WeightUnitCode = WghtUnitQualifier.L
                        },
                    Stops = ediStopsData
                        .Select(s =>
                            {
                                var locContact = s.Stop.Contacts.FirstOrDefault(c => c.Primary) ?? s.Stop.Contacts.FirstOrDefault();
                                return new Edi204Stop
                                    {
                                        DateTime = (s.IsShipper ? originStopTime : s.IsConsignee ? destStopTime : s.Stop.AppointmentDateTime).FormattedLongDateAlt(),
                                        HazardousMaterialContact = new Contact
                                            {
                                                CommunicationNumber = order.HazardousMaterialContactPhone,
                                                CommunicationNumberQualifier = CommNumberQualifier.TE,
                                                ContactFunctionCode = ContactFuncCode.HZ,
                                                ContactInquiryReference = string.Empty,
                                                Name = order.HazardousMaterialContactName
                                            },
                                        IsHazardousMaterial = order.HazardousMaterial,
                                        LocationContact = locContact == null
                                                              ? null
                                                              : new Contact
                                                                  {
                                                                      CommunicationNumber = locContact.Phone,
                                                                      CommunicationNumberQualifier = CommNumberQualifier.TE,
                                                                      ContactFunctionCode = ContactFuncCode.GC,
                                                                      ContactInquiryReference = string.Empty,
                                                                      Name = locContact.Name
                                                                  },
                                        GeographicLocation = new GeographicLocation
                                            {
                                                CityName = s.Stop.City,
                                                CountryCode = s.Stop.Country == null ? string.Empty : s.Stop.Country.Code,
                                                LocationIdentifier = string.Empty,
                                                LocationQualifier = string.Empty,
                                                PostalCode = s.Stop.PostalCode,
                                                StateOrProvinceCode = s.Stop.State
                                            },
                                        LocationName = new Name
                                            {
                                                EntityIdentifierCode = s.IsShipper
                                                                           ? EntityIdCode.SH
                                                                           : s.IsConsignee ? EntityIdCode.CN : s.IsPickup ? EntityIdCode.SF : EntityIdCode.ST,
                                                EntityName = s.Stop.Description,
                                                IdentificationCode = string.Empty,
                                                IdentificationCodeQualifier = string.Empty
                                            },
                                        LocationStreetInformation = new AddressInformation
                                            {
                                                AddressInfo1 = s.Stop.Street1,
                                                AddressInfo2 = s.Stop.Street2
                                            },
                                        OrderIdentificationDetails = s.Item
                                                                      .Select(i => new OrderIdentificationDetail
                                                                          {
                                                                              DimensionUnitQualifier = DimsUnitQualifier.I,
                                                                              Height = i.Height,
                                                                              Length = i.Length,
                                                                              Quantity = i.Quantity,
                                                                              ReferenceIdentification = string.IsNullOrEmpty(i.Comment) ? i.Description : i.Comment,
                                                                              Weight = i.Weight,
                                                                              WeightUnitCode = WghtUnitQualifier.L,
                                                                              Width = i.Width,
                                                                              NumberOfIndividualUnitsShipped = i.PieceCount,
                                                                              NMFCCode = i.NMFCCode,
                                                                              CommodityCode = string.Empty,
                                                                              ItemDescription = i.Description,
                                                                              PackageTypeCode = i.PackageType.EdiOid,
                                                                              FreightClass = i.FreightClass.ToInt() == default(int) ? string.Empty : i.FreightClass.GetString() // blank freight class is zero so going to int will not break code!
                                                                          })
                                                                      .ToList(),
                                        StopOffDetails = new StopOffDetails
                                            {
                                                SpecialInstructions = s.Stop.SpecialInstructions,
                                                StopReasonCode =
                                                    s.IsShipper
                                                        ? StopReasonCode.CL
                                                        : s.IsConsignee ? StopReasonCode.UL : s.IsPickup ? StopReasonCode.PL : StopReasonCode.PU,
                                                StopSequenceNumber = s.Stop.StopOrder
                                            }
                                    };
                            })
                        .ToList()
                };

            var edi204 = new Edi204
                {
                    TransactionSetHeader = new TransactionSetHeader
                        {
                            TransactionSetControlNumber = controlNumber,
                            TransactionSetIdentifierCode = TransSetIdentifierCode.LTR
                        },

                    Loads = new List<Load> { load },

                    TransactionSetTrailer = new TransactionSetTrailer
                        {
                            TransactionSetControlNumber = controlNumber,
                            NumberOfIncludedSegments = 1
                        },
                };

            return edi204;
        }

        public static Edi210 ToEdi210(this Invoice invoice, string controlNumber)
        {
            var tdetails = invoice.Details.Where(d => d.ReferenceType == DetailReferenceType.ServiceTicket).ToList();
            var sdetails = invoice.Details.Where(d => d.ReferenceType == DetailReferenceType.Shipment).ToList();
            var gdetails = invoice.Details.Where(d => d.ReferenceType == DetailReferenceType.Miscellaneous).ToList();

            var finv = new List<FreightInvoice>();

            var ci = CorrectionIndicator.NA;
            switch (invoice.InvoiceType)
            {
                case InvoiceType.Credit:
                    ci = CorrectionIndicator.CR;
                    break;
                case InvoiceType.Supplemental:
                    ci = CorrectionIndicator.BD;
                    break;
            }

            // details with Misc type
            if (gdetails.Any())
                finv.Add(new FreightInvoice
                    {
                        BeginningSegment = new BeginningSegmentFreightInv
                            {
                                ShipmentIdNumber = string.Empty,
                                WeightUnitCode = WghtUnitQualifier.L,
                                StandardCarrierAlphaCode = invoice.Tenant.TenantScac,
                                ShipmentQualifier = ShipmentQualifier.G,
                                TariffServiceCode = string.Empty,
                                TransportationTermsCode = string.Empty,
                                CorrectionIndicator = ci,
                                Date = invoice.DueDate.FormattedLongDateAlt(),
                                PickupDate = DateUtility.SystemEarliestDateTime.FormattedLongDateAlt(),
                                PickupDateTimeQualifier = FreightInvDateQualifier.NA,
                                DeliveryDateTimeQualifier = FreightInvDateQualifier.NA,
                                DeliveryDate = DateUtility.SystemEarliestDateTime.FormattedLongDateAlt(),
                                Equipment = null,
                                Mileage = 0,
                                InvoiceDate = invoice.InvoiceDate.FormattedLongDateAlt(),
                                InvoiceNumber = invoice.InvoiceNumber,
                                NetAmountDue = gdetails.Sum(i => i.AmountDue),
                                ShipmentMethodOfPay = ShipMethodOfPay.TP,
                            },
                        FreightInvoiceDetails = gdetails.Select(d => new FreightInvoiceDetail
                            {
                                Remarks = new Remarks { FreeFormMessage = d.Comment, FreeFormMessage2 = string.Empty },
                                Weight = -1,
                                WeightUnitCode = WghtUnitQualifier.L,
                                LadingPieces = -1,
                                Charge = d.AmountDue,
                                ChargeCode = d.ChargeCode.Code,
                                ChargeCodeDescription = d.ChargeCode.Description,
                                LadingQuantity = -1
                            }).ToList(),
                        ReferenceNumbers = new List<ReferenceNumber>(),
                        Remarks = new Remarks(),
                        Stops = new List<Edi204Stop>()
                    });

            // details with Service Ticket Type
            var tsearch = new ServiceTicketSearch();
            var dt = tdetails
                .GroupBy(d => d.ReferenceNumber, d => d)
                .Select(i => new
                    {
                        Number = i.Key,
                        Details = i.Select(x => x),
                        Ticket = tsearch.FetchServiceTicketByServiceTicketNumber(i.Key, invoice.TenantId),
                    })
                .ToList();
            finv.AddRange(dt.Select(i =>
                {
                    var vendor = i.Ticket.Vendors.FirstOrDefault(v => v.Primary);
                    return new FreightInvoice
                        {
                            BeginningSegment = new BeginningSegmentFreightInv
                                {
                                    ShipmentIdNumber = string.Empty,
                                    WeightUnitCode = WghtUnitQualifier.L,
                                    StandardCarrierAlphaCode = vendor == null ? string.Empty : vendor.Vendor.Scac,
                                    ShipmentQualifier = ShipmentQualifier.G,
                                    TariffServiceCode = string.Empty,
                                    TransportationTermsCode = string.Empty,
                                    CorrectionIndicator = ci,
                                    Date = invoice.DueDate.FormattedLongDateAlt(),
                                    PickupDate = DateUtility.SystemEarliestDateTime.FormattedLongDateAlt(),
                                    PickupDateTimeQualifier = FreightInvDateQualifier.NA,
                                    DeliveryDateTimeQualifier = FreightInvDateQualifier.NA,
                                    DeliveryDate = DateUtility.SystemEarliestDateTime.FormattedLongDateAlt(),
                                    Equipment = null,
                                    Mileage = 0,
                                    InvoiceDate = invoice.InvoiceDate.FormattedLongDateAlt(),
                                    InvoiceNumber = invoice.InvoiceNumber,
                                    NetAmountDue = i.Details.Sum(x => x.AmountDue),
                                    ShipmentMethodOfPay = ShipMethodOfPay.TP,
                                },
                            FreightInvoiceDetails = i.Details.Select(d => new FreightInvoiceDetail
                                {
                                    Remarks = new Remarks { FreeFormMessage = d.Comment, FreeFormMessage2 = string.Empty },
                                    Weight = -1,
                                    WeightUnitCode = WghtUnitQualifier.L,
                                    LadingPieces = -1,
                                    Charge = d.AmountDue,
                                    ChargeCode = d.ChargeCode.Code,
                                    ChargeCodeDescription = d.ChargeCode.Description,
                                    LadingQuantity = -1
                                }).ToList(),
                            ReferenceNumbers = new List<ReferenceNumber>
								{
									new ReferenceNumber
										{
											ReferenceIdentification = i.Number,
											ReferenceIdentificationQualifier = RefIdQualifier.BRQ,
											Description = "Ticket Number",
										}
								},
                            Remarks = new Remarks(),
                            Stops = new List<Edi204Stop>()
                        };
                }));

            // details with Shipment Type
            var ssearch = new ShipmentSearch();
            var st = sdetails
                .GroupBy(d => d.ReferenceNumber, d => d)
                .Select(i => new
                    {
                        Number = i.Key,
                        Details = i.Select(x => x),
                        Shipment = ssearch.FetchShipmentByShipmentNumber(i.Key, invoice.TenantId),
                    })
                .ToList();
            finv.AddRange(st.Select(i =>
                {
                    var vendor = i.Shipment.Vendors.FirstOrDefault(v => v.Primary);

                    var originStopTime = i.Shipment.Origin.AppointmentDateTime == DateUtility.SystemEarliestDateTime
                                             ? i.Shipment.DesiredPickupDate
                                             : i.Shipment.Origin.AppointmentDateTime;
                    var destStopTime = i.Shipment.Destination.AppointmentDateTime == DateUtility.SystemEarliestDateTime
                                           ? i.Shipment.EstimatedDeliveryDate
                                           : i.Shipment.Destination.AppointmentDateTime;

                    // stops and item info
                    var allStops = i.Shipment.Stops;
                    allStops.Insert(0, i.Shipment.Origin);
                    allStops.Add(i.Shipment.Destination);
                    var pItems = i.Shipment.Items.GroupBy(u => u.Pickup).ToDictionary(u => u.Key, u => u.Select(x => x));
                    var dItems = i.Shipment.Items.GroupBy(u => u.Delivery).ToDictionary(u => u.Key, u => u.Select(x => x));

                    var ediStopsData = allStops
                        .SelectMany(s => pItems
                                             .Where(d => d.Key == s.StopOrder)
                                             .Select(x => new
                                                 {
                                                     Stop = s,
                                                     IsShipper = s.StopOrder == i.Shipment.Origin.StopOrder,
                                                     IsConsignee = s.StopOrder == i.Shipment.Destination.StopOrder,
                                                     IsPickup = true,
                                                     Item = x.Value.ToList()
                                                 }))
                        .ToList();
                    ediStopsData.AddRange(
                        allStops
                            .SelectMany(s => dItems
                                                 .Where(d => d.Key == s.StopOrder)
                                                 .Select(x => new
                                                     {
                                                         Stop = s,
                                                         IsShipper = s.StopOrder == i.Shipment.Origin.StopOrder,
                                                         IsConsignee = s.StopOrder == i.Shipment.Destination.StopOrder,
                                                         IsPickup = false,
                                                         Item = x.Value.ToList()
                                                     })
                                                 .ToList()));
                    var seqs = ediStopsData.Select(s => s.Stop.StopOrder);
	                ediStopsData.AddRange(
		                allStops
			                .Where(s => !seqs.Contains(s.StopOrder))
			                .Select(s => new
				                {
					                Stop = s,
					                IsShipper = s.StopOrder == i.Shipment.Origin.StopOrder,
					                IsConsignee = s.StopOrder == i.Shipment.Destination.StopOrder,
					                IsPickup = false,
					                Item = new List<ShipmentItem>()
				                })
			                .ToList());

                    ediStopsData = ediStopsData.OrderBy(s => s.Stop.StopOrder).ToList();

                    // ref nums
                    var refNums = i.Shipment.CustomerReferences
                                   .Select(r => new ReferenceNumber
                                       {
                                           Description = r.Name,
                                           ReferenceIdentification = r.Value,
                                           ReferenceIdentificationQualifier = RefIdQualifier.CUS
                                       })
                                   .ToList();
                    if (!string.IsNullOrEmpty(i.Shipment.ShipperReference))
                        refNums.Add(new ReferenceNumber
                            {
                                Description = string.Empty,
                                ReferenceIdentification = i.Shipment.ShipperReference,
                                ReferenceIdentificationQualifier = RefIdQualifier.SHR
                            });
                    if (!string.IsNullOrEmpty(i.Shipment.PurchaseOrderNumber))
                        refNums.Add(new ReferenceNumber
                            {
                                Description = string.Empty,
                                ReferenceIdentification = i.Shipment.PurchaseOrderNumber,
                                ReferenceIdentificationQualifier = RefIdQualifier.PO
                            });
                    refNums.Add(new ReferenceNumber
                        {
                            Description = string.Empty,
                            ReferenceIdentification = vendor == null ? string.Empty : vendor.ProNumber,
                            ReferenceIdentificationQualifier = RefIdQualifier.CN
                        });

                    return new FreightInvoice
                        {
                            BeginningSegment = new BeginningSegmentFreightInv
                                {
                                    ShipmentIdNumber = i.Number,
                                    WeightUnitCode = WghtUnitQualifier.L,
                                    StandardCarrierAlphaCode = vendor == null ? string.Empty : vendor.Vendor.Scac,
                                    ShipmentQualifier = ShipmentQualifier.G,
                                    TariffServiceCode = i.Shipment.MiscField1,
                                    TransportationTermsCode = i.Shipment.ServiceMode.GetServiceModeCode(),
                                    CorrectionIndicator = ci,
                                    Date = invoice.DueDate.FormattedLongDateAlt(),
                                    PickupDateTimeQualifier =
                                        i.Shipment.ActualPickupDate != DateUtility.SystemEarliestDateTime
                                            ? FreightInvDateQualifier.ACP
                                            : FreightInvDateQualifier.ESP,
                                    PickupDate = (i.Shipment.ActualPickupDate != DateUtility.SystemEarliestDateTime
                                                     ? i.Shipment.ActualPickupDate
                                                     : i.Shipment.DesiredPickupDate).FormattedLongDateAlt(),
                                    DeliveryDateTimeQualifier =
                                        i.Shipment.ActualDeliveryDate != DateUtility.SystemEarliestDateTime
                                            ? FreightInvDateQualifier.ACD
                                            : FreightInvDateQualifier.ESD,
                                    DeliveryDate = (i.Shipment.ActualDeliveryDate != DateUtility.SystemEarliestDateTime
                                            ? i.Shipment.ActualDeliveryDate
                                            : i.Shipment.EstimatedDeliveryDate).FormattedLongDateAlt(),
                                    Equipment = i.Shipment.Equipments.Any()
                                                    ? new EquipmentDetail
                                                        {
                                                            EquipmentDescriptionCode = i.Shipment.Equipments[0].EquipmentType.Code,
                                                            EquipmentNumber = string.Empty
                                                        }
                                                    : null,
                                    Mileage = i.Shipment.Mileage,
                                    InvoiceDate = invoice.InvoiceDate.FormattedLongDateAlt(),
                                    InvoiceNumber = invoice.InvoiceNumber,
                                    NetAmountDue = i.Details.Sum(x => x.AmountDue),
                                    ShipmentMethodOfPay = ShipMethodOfPay.TP,
                                },
                            FreightInvoiceDetails = i.Details.Select(d => new FreightInvoiceDetail
                                {
                                    Remarks = new Remarks { FreeFormMessage = d.Comment, FreeFormMessage2 = string.Empty },
                                    Weight = i.Shipment.Items.Sum(x => x.ActualWeight),
                                    WeightUnitCode = WghtUnitQualifier.L,
                                    LadingPieces = i.Shipment.Items.Sum(x => x.PieceCount),
                                    Charge = d.AmountDue,
                                    ChargeCode = d.ChargeCode.Code,
                                    ChargeCodeDescription = d.ChargeCode.Description,
                                    LadingQuantity = i.Shipment.Items.Sum(x => x.Quantity)
                                }).ToList(),
                            ReferenceNumbers = refNums,
                            Remarks = new Remarks
                                {
                                    FreeFormMessage = i.Shipment.GeneralBolComments,
                                    FreeFormMessage2 = i.Shipment.CriticalBolComments,
                                },
                            Stops = ediStopsData
                                .Select(s =>
                                    {
                                        var locContact = s.Stop.Contacts.FirstOrDefault(c => c.Primary) ?? s.Stop.Contacts.FirstOrDefault();
                                        return new Edi204Stop
                                            {
                                                DateTime = (s.IsShipper ? originStopTime : s.IsConsignee ? destStopTime : s.Stop.AppointmentDateTime).FormattedLongDateAlt(),
                                                HazardousMaterialContact = new Contact
                                                    {
                                                        CommunicationNumber = i.Shipment.HazardousMaterialContactPhone,
                                                        CommunicationNumberQualifier = CommNumberQualifier.TE,
                                                        ContactFunctionCode = ContactFuncCode.HZ,
                                                        ContactInquiryReference = string.Empty,
                                                        Name = i.Shipment.HazardousMaterialContactName
                                                    },
                                                IsHazardousMaterial = i.Shipment.HazardousMaterial,
                                                LocationContact = locContact == null
                                                                      ? null
                                                                      : new Contact
                                                                          {
                                                                              CommunicationNumber = locContact.Phone,
                                                                              CommunicationNumberQualifier = CommNumberQualifier.TE,
                                                                              ContactFunctionCode = ContactFuncCode.GC,
                                                                              ContactInquiryReference = string.Empty,
                                                                              Name = locContact.Name
                                                                          },
                                                GeographicLocation = new GeographicLocation
                                                    {
                                                        CityName = s.Stop.City,
                                                        CountryCode = s.Stop.Country == null ? string.Empty : s.Stop.Country.Code,
                                                        LocationIdentifier = string.Empty,
                                                        LocationQualifier = string.Empty,
                                                        PostalCode = s.Stop.PostalCode,
                                                        StateOrProvinceCode = s.Stop.State
                                                    },
                                                LocationName = new Name
                                                    {
                                                        EntityIdentifierCode = s.IsShipper
                                                                                   ? EntityIdCode.SH
                                                                                   : s.IsConsignee ? EntityIdCode.CN : s.IsPickup ? EntityIdCode.SF : EntityIdCode.ST,
                                                        EntityName = s.Stop.Description,
                                                        IdentificationCode = string.Empty,
                                                        IdentificationCodeQualifier = string.Empty
                                                    },
                                                LocationStreetInformation = new AddressInformation
                                                    {
                                                        AddressInfo1 = s.Stop.Street1,
                                                        AddressInfo2 = s.Stop.Street2
                                                    },
                                                OrderIdentificationDetails = s.Item
                                                                              .Select(x => new OrderIdentificationDetail
                                                                                  {
                                                                                      DimensionUnitQualifier = DimsUnitQualifier.I,
                                                                                      Height = x.ActualHeight,
                                                                                      Length = x.ActualLength,
                                                                                      Quantity = x.Quantity,
                                                                                      ReferenceIdentification = string.IsNullOrEmpty(x.Comment) ? x.Description : x.Comment,
                                                                                      Weight = x.ActualWeight,
                                                                                      WeightUnitCode = WghtUnitQualifier.L,
                                                                                      Width = x.ActualWidth,
                                                                                      NumberOfIndividualUnitsShipped = x.PieceCount,
                                                                                      NMFCCode = x.NMFCCode,
                                                                                      CommodityCode = string.Empty,
                                                                                      ItemDescription = x.Description,
                                                                                      PackageTypeCode = x.PackageType.EdiOid,
                                                                                      FreightClass = x.ActualFreightClass.ToInt() == default(int) ? string.Empty : x.ActualFreightClass.GetString() // blank freight class is zero so to int will not break code
                                                                                  })
                                                                              .ToList(),
                                                StopOffDetails = new StopOffDetails
                                                    {
                                                        SpecialInstructions = s.Stop.SpecialInstructions,
                                                        StopReasonCode =
                                                            s.IsShipper
                                                                ? StopReasonCode.CL
                                                                : s.IsConsignee ? StopReasonCode.UL : s.IsPickup ? StopReasonCode.PL : StopReasonCode.PU,
                                                        StopSequenceNumber = s.Stop.StopOrder
                                                    }
                                            };
                                    })
                                .ToList()
                        };
                }));

            var edi210 = new Edi210
                {
                    TransactionSetHeader = new TransactionSetHeader
                    {
                        TransactionSetControlNumber = controlNumber,
                        TransactionSetIdentifierCode = TransSetIdentifierCode.CSI
                    },

                    Invoices = finv,

                    TransactionSetTrailer = new TransactionSetTrailer
                    {
                        TransactionSetControlNumber = controlNumber,
                        NumberOfIncludedSegments = 1
                    },
                };

            return edi210;
        }

        public static Edi214 ToEdi214(this Shipment shipment, List<CheckCall> checkCalls, string controlNumber)
        {
            var sv = shipment.Vendors.FirstOrDefault(v => v.Primary);
            var origin = new EdiLocation
                {
                    GeographicLocation = new GeographicLocation
                        {
                            PostalCode = shipment.Origin.PostalCode,
                            CityName = shipment.Origin.City,
                            CountryCode = shipment.Origin.Country.Code,
                            LocationIdentifier = string.Empty,
                            LocationQualifier = string.Empty,
                            StateOrProvinceCode = shipment.Origin.State
                        },
                    LocationName = new Name
                        {
                            EntityName = shipment.Origin.Description,
                            IdentificationCode = string.Empty,
                            IdentificationCodeQualifier = string.Empty,
                            EntityIdentifierCode = EntityIdCode.SH
                        },
                    LocationStreetInformation = new AddressInformation
                        {
                            AddressInfo1 = shipment.Origin.Street1,
                            AddressInfo2 = shipment.Origin.Street2
                        },
                };
            var dest = new EdiLocation
                {
                    GeographicLocation = new GeographicLocation
                        {
                            PostalCode = shipment.Destination.PostalCode,
                            CityName = shipment.Destination.City,
                            CountryCode = shipment.Destination.Country.Code,
                            LocationIdentifier = string.Empty,
                            LocationQualifier = string.Empty,
                            StateOrProvinceCode = shipment.Destination.State
                        },
                    LocationName = new Name
                        {
                            EntityName = shipment.Destination.Description,
                            IdentificationCode = string.Empty,
                            IdentificationCodeQualifier = string.Empty,
                            EntityIdentifierCode = EntityIdCode.SH
                        },
                    LocationStreetInformation = new AddressInformation
                        {
                            AddressInfo1 = shipment.Destination.Street1,
                            AddressInfo2 = shipment.Destination.Street2
                        },
                };

            var edi214 = new Edi214
                {
                    TransactionSetHeader = new TransactionSetHeader
                        {
                            TransactionSetControlNumber = controlNumber,
                            TransactionSetIdentifierCode = TransSetIdentifierCode.SSM
                        },
                    Statuses = checkCalls
                        .Select(cc =>
                            {
                                var ediCode = cc.EdiStatusCode.ToEnum<ShipStatMsgCode>();
                                return new Edi.Segments.ShipmentStatus
                                    {
                                        BeginningSegment = new BeginningSegmentShipStatMsg
                                            {
                                                ShipmentIdentificationNumber =
                                                    string.IsNullOrEmpty(shipment.ShipperBol) ? shipment.ShipmentNumber : shipment.ShipperBol,
                                                StandardCarrierAlphaCode = shipment.Tenant.TenantScac,
                                                ReferenceIdentification = sv.ProNumber,
                                                ReferenceIdentificationQualifier = RefIdQualifier.CN
                                            },
                                        DateTime = cc.EventDate.FormattedLongDateAlt(),
                                        Location = ediCode == ShipStatMsgCode.AF ? origin : ediCode == ShipStatMsgCode.D1 ? dest : null,
                                        Remarks = new Remarks { FreeFormMessage = cc.CallNotes, FreeFormMessage2 = string.Empty },
                                        EquipmentDetail = new EquipmentDetail
                                            {
                                                EquipmentDescriptionCode = shipment.Equipments.Any() ? shipment.Equipments[0].EquipmentType.Code : string.Empty,
                                                EquipmentNumber = string.Empty
                                            },
                                        ShipmentStatusCode = ediCode,
                                        ShipmentStatusReasonCode = ShipStatReasonCode.NS.GetString(),
                                        ShipmentWghtPkgAndQtyData = new ShipmentWghtPkgAndQtyData
                                            {
                                                LadingPackages = shipment.Items.Sum(i => i.Quantity),
                                                LadingPieces = shipment.Items.Sum(i => i.PieceCount),
                                                Weight = shipment.Items.Sum(i => i.ActualWeight),
                                                WeightUnitCode = WghtUnitQualifier.L
                                            }
                                    };
                            })
                        .ToList(),
                    TransactionSetTrailer = new TransactionSetTrailer
                        {
                            NumberOfIncludedSegments = checkCalls.Count,
                            TransactionSetControlNumber = controlNumber
                        }
                };

            return edi214;
        }

        public static LoadOrder FromIncomingEdi204(this Edi204 edi204, XmlConnect connect, Customer customer, User activeUser, List<EquipmentType> equipments, List<Service> services, List<Country> countries, List<PackageType> packageTypes,
            List<double> freightClasses)
        {
            var l = edi204.Loads.FirstOrDefault(i => i.BeginningSegment.ShipmentIdNumber == connect.ShipmentIdNumber);

            if (l == null) return null;

            var load = new LoadOrder { TenantId = activeUser.TenantId };

            load.AccountBuckets = new List<LoadOrderAccountBucket>
				{
					new LoadOrderAccountBucket
						{
							AccountBucket = customer.DefaultAccountBucket,
							TenantId = load.TenantId,
							LoadOrder = load,
							Primary = true,
						}
				};
            load.AccountBucketUnit = null;
            load.BillReseller = customer.Rating != null && customer.Rating.BillReseller;
            load.CarrierCoordinator = null;
            load.Charges = new List<LoadOrderCharge>();
            load.CriticalBolComments = string.Empty;
            load.Customer = customer;
            load.CustomerReferences = customer.CustomFields
                                              .Select(f => new LoadOrderReference
                                                  {
                                                      Name = f.Name,
                                                      TenantId = load.TenantId,
                                                      LoadOrder = load,
                                                      Value = string.Empty,
                                                      Required = f.Required,
                                                      DisplayOnDestination = f.DisplayOnDestination,
                                                      DisplayOnOrigin = f.DisplayOnOrigin,
                                                  })
                                              .ToList();
            load.DateCreated = DateTime.Now;
            load.DeclineInsurance = true;	// by default this is true
            load.Description = string.Empty;
            load.DesiredPickupDate = DateTime.Now;
            load.Destination = new LoadOrderLocation();
            load.Documents = new List<LoadOrderDocument>();
            load.EarlyDelivery = TimeUtility.DefaultClose;
            load.EarlyPickup = TimeUtility.DefaultOpen;
            load.EmptyMileage = 0;
            load.Equipments = l.EquipmentDetail != null && !string.IsNullOrEmpty(l.EquipmentDetail.EquipmentDescriptionCode)
                                  ? new List<LoadOrderEquipment>
					                  {
						                  new LoadOrderEquipment
							                  {
								                  LoadOrder = load,
								                  TenantId = load.TenantId,
								                  EquipmentType = equipments.FirstOrDefault(e => e.Code == l.EquipmentDetail.EquipmentDescriptionCode)
							                  }
					                  }
                                  : new List<LoadOrderEquipment>();
            load.Equipments = load.Equipments.Where(e => e.EquipmentTypeId != default(long)).ToList();
            load.EstimatedDeliveryDate = DateTime.Now;
            load.GeneralBolComments = string.Empty;

            load.HazardousMaterial = false;
            load.HazardousMaterialContactEmail = string.Empty;
            load.HazardousMaterialContactMobile = string.Empty;
            load.HazardousMaterialContactName = string.Empty;
            load.HazardousMaterialContactPhone = string.Empty;

            load.HidePrefix = customer.HidePrefix;
            load.IsPartialTruckload = false;

            load.Items = new List<LoadOrderItem>();

            load.LateDelivery = TimeUtility.DefaultClose;
            load.LatePickup = TimeUtility.DefaultOpen;
            load.LinearFootRuleBypassed = false;
            load.LoadOrderCoordinator = null;
            load.LoadOrderNumber = string.Empty;
            load.Mileage = 0;
            load.MileageSource = null;
            load.MiscField1 = l.Remarks == null ? string.Empty : l.Remarks.FreeFormMessage.GetString();
            load.MiscField2 = l.Remarks == null ? string.Empty : l.Remarks.FreeFormMessage2.GetString();
	        load.DriverName = string.Empty;
	        load.DriverPhoneNumber = string.Empty;
	        load.DriverTrailerNumber = string.Empty;
            load.NotAvailableToLoadboards = true; // by default true
            load.Notes = new List<LoadOrderNote>();
            load.Origin = new LoadOrderLocation();
            load.Prefix = customer.Prefix;


            var po = l.LoadReferenceNumbers.FirstOrDefault(r => r.ReferenceIdentificationQualifier == RefIdQualifier.PO);
            load.PurchaseOrderNumber = po == null ? string.Empty : po.ReferenceIdentification.GetString();

            load.ResellerAddition = customer.Rating == null ? null : customer.Rating.ResellerAddition;
            load.ServiceMode = ServiceMode.NotApplicable;

            // sales rep
            var srtier = load.Customer.SalesRepresentative == null
                             ? null
                             : load.Customer.SalesRepresentative.ApplicableTier(load.DateCreated, load.ServiceMode, load.Customer);
            load.SalesRepresentative = load.Customer.SalesRepresentative;
            load.SalesRepresentativeCommissionPercent = srtier != null ? srtier.CommissionPercent : 0m;
            load.SalesRepAddlEntityCommPercent = load.Customer.SalesRepresentative == null
                                                  ? 0m
                                                  : load.Customer.SalesRepresentative.AdditionalEntityCommPercent;
            load.SalesRepAddlEntityName = load.Customer.SalesRepresentative == null
                                           ? string.Empty
                                           : load.Customer.SalesRepresentative.AdditionalEntityName;
            load.SalesRepMinCommValue = srtier != null ? srtier.FloorValue : 0m;
            load.SalesRepMaxCommValue = srtier != null ? srtier.CeilingValue : 0m;

            load.Services = l.HandlingRequirements == null
                                ? new List<LoadOrderService>()
                                : l.HandlingRequirements
                                   .Select(h => new LoadOrderService
                                       {
                                           TenantId = load.TenantId,
                                           LoadOrder = load,
                                           Service = services.FirstOrDefault(s => s.Code == h.SpecialServicesCode)
                                       })
                                   .ToList();


            load.ShipmentPriority = null;
            load.ShipperBol = connect.ShipmentIdNumber;

            var shr = l.LoadReferenceNumbers.FirstOrDefault(r => r.ReferenceIdentificationQualifier == RefIdQualifier.SHR);
            load.ShipperReference = shr == null ? string.Empty : shr.ReferenceIdentification;

            load.Status = LoadOrderStatus.Offered;
            load.Stops = new List<LoadOrderLocation>();
            load.User = activeUser;
            load.Vendors = new List<LoadOrderVendor>();

            // handle additional reference numbers
            load.CustomerReferences
                .AddRange(l.LoadReferenceNumbers
                           .Where(r => r.ReferenceIdentificationQualifier != RefIdQualifier.SHR && r.ReferenceIdentificationQualifier != RefIdQualifier.PO)
                           .Select(r => new LoadOrderReference
                               {
                                   Name = r.ReferenceIdentificationQualifier.GetDescription(),
                                   Value = r.ReferenceIdentification.GetString(),
                                   TenantId = load.TenantId,
                                   LoadOrder = load,
                                   DisplayOnDestination = false,
                                   DisplayOnOrigin = false,
                                   Required = false,
                               }));

            // handle stops and items
            var ediItems = load.Items.Select(i => new { Key = string.Empty, Item = i }).ToList();
            const int invalidStopIdx = -1;
            var unloadCodes = new List<StopReasonCode> { StopReasonCode.CU, StopReasonCode.PU, StopReasonCode.UL };
            var loadCodes = new List<StopReasonCode> { StopReasonCode.CL, StopReasonCode.LD, StopReasonCode.PL };
            if (l.Stops != null && l.Stops.Any()) l.Stops.OrderBy(s => s.StopOffDetails.StopSequenceNumber);
            var originIndex = l.Stops.Any() ? l.Stops[0].StopOffDetails.StopSequenceNumber : invalidStopIdx;
            var destIndex = l.Stops.Any() ? l.Stops[l.Stops.Count - 1].StopOffDetails.StopSequenceNumber : invalidStopIdx;
            for (var idx = 0; idx < l.Stops.Count(); idx++)
            {
                var stop = l.Stops[idx];
                var loc = new LoadOrderLocation
                    {
                        TenantId = load.TenantId,
                        LoadOrder = load,
                        AppointmentDateTime = stop.DateTime.ToDateTime() < DateUtility.SystemEarliestDateTime ? DateUtility.SystemEarliestDateTime : stop.DateTime.ToDateTime(),
                        City = stop.GeographicLocation.CityName.GetString(),
                        Country = countries.FirstOrDefault(c => c.Code == stop.GeographicLocation.CountryCode),
                        Contacts = new List<LoadOrderContact>(),
                        Description = stop.LocationName.EntityName.GetString(),
                        Direction = string.Empty,
                        GeneralInfo = string.Empty,
                        PostalCode = stop.GeographicLocation.PostalCode.GetString(),
                        SpecialInstructions = stop.StopOffDetails.SpecialInstructions.GetString(),
                        State = stop.GeographicLocation.StateOrProvinceCode.GetString(),
                        StopOrder = stop.StopOffDetails.StopSequenceNumber,
                        Street1 = stop.LocationStreetInformation.AddressInfo1.GetString(),
                        Street2 = stop.LocationStreetInformation.AddressInfo2.GetString(),
                    };

                loc.Contacts.Add(new LoadOrderContact
                    {
                        TenantId = load.TenantId,
                        Location = loc,
                        Comment = stop.LocationContact.ContactInquiryReference.GetString(),
                        ContactTypeId = load.Tenant.DefaultSchedulingContactTypeId,
						Email = stop.LocationContact.CommunicationNumberQualifier == CommNumberQualifier.EM
								  ? stop.LocationContact.CommunicationNumber.GetString()
								  : string.Empty,
                        Fax = stop.LocationContact.CommunicationNumberQualifier == CommNumberQualifier.FX
                                  ? stop.LocationContact.CommunicationNumber.GetString()
                                  : string.Empty,
                        Phone = stop.LocationContact.CommunicationNumberQualifier == CommNumberQualifier.TE
                                    ? stop.LocationContact.CommunicationNumber.GetString()
                                    : string.Empty,
                        Mobile = string.Empty,
                        Name = stop.LocationContact.Name.GetString(),
                        Primary = true,
                    });

                if (stop.IsHazardousMaterial)
                {
                    var loadOrderContact = new LoadOrderContact
                        {
                            TenantId = load.TenantId,
                            Location = loc,
                            Comment = stop.HazardousMaterialContact.ContactFunctionCode.GetDescription(),
                            ContactTypeId = load.Tenant.DefaultSchedulingContactTypeId,
							Email = stop.LocationContact.CommunicationNumberQualifier == CommNumberQualifier.EM
								  ? stop.LocationContact.CommunicationNumber.GetString()
								  : string.Empty,
                            Fax = stop.HazardousMaterialContact.CommunicationNumberQualifier == CommNumberQualifier.FX
                                      ? stop.HazardousMaterialContact.CommunicationNumber.GetString()
                                      : string.Empty,
                            Phone = stop.HazardousMaterialContact.CommunicationNumberQualifier == CommNumberQualifier.TE
                                        ? stop.HazardousMaterialContact.CommunicationNumber.GetString()
                                        : string.Empty,
                            Mobile = string.Empty,
                            Name = stop.HazardousMaterialContact.Name.GetString(),
                            Primary = true,
                        };
                    loc.Contacts.Add(loadOrderContact);

                    if (!load.HazardousMaterial) // i.e. never set!
                    {
                        load.HazardousMaterial = true;
                        load.HazardousMaterialContactName = loadOrderContact.Name;
                        load.HazardousMaterialContactPhone = loadOrderContact.Phone;
                    }
                }

                if (stop.StopOffDetails.StopSequenceNumber == originIndex) load.Origin = loc;
                else if (stop.StopOffDetails.StopSequenceNumber == destIndex) load.Destination = loc;
                else load.Stops.Add(loc);


                // items
                foreach (var oid in stop.OrderIdentificationDetails)
                {
                    var item = ediItems.FirstOrDefault(x => x.Key == oid.ReferenceIdentification);
                    if (item == null)
                    {
                        item = new { Key = oid.ReferenceIdentification, Item = new LoadOrderItem { Pickup = invalidStopIdx, Delivery = invalidStopIdx } };
                        ediItems.Add(item);
                    }

                    item.Item.TenantId = load.TenantId;
                    item.Item.LoadOrder = load;

                    if (unloadCodes.Contains(stop.StopOffDetails.StopReasonCode))
                        item.Item.Delivery = idx;

                    if (loadCodes.Contains(stop.StopOffDetails.StopReasonCode))
                        item.Item.Pickup = idx;

                    item.Item.Description = oid.ItemDescription.GetString();
                    item.Item.Height = oid.Height;
                    item.Item.Comment = oid.ReferenceIdentification.GetString().Length > 50
                                            ? oid.ReferenceIdentification.GetString().Substring(0, 50)
                                            : oid.ReferenceIdentification.GetString();
                    item.Item.HTSCode = string.Empty;
                    item.Item.IsStackable = false;
                    item.Item.FreightClass = freightClasses.Contains(oid.FreightClass.ToDouble())
                                                 ? oid.FreightClass.ToDouble()
                                                 : default(double);
                    item.Item.Length = oid.Length;
                    item.Item.NMFCCode = oid.NMFCCode.GetString();
                    item.Item.Width = oid.Width;
                    item.Item.Weight = oid.Weight;
                    item.Item.Value = default(decimal);
                    item.Item.Quantity = oid.Quantity;
                    item.Item.PieceCount = 1; // assumed
                    item.Item.PackageType = packageTypes.FirstOrDefault(p => p.EdiOid == oid.PackageTypeCode) ?? load.Tenant.DefaultPackagingType;
                }
            }
            load.Items = ediItems.Select(i => i.Item).ToList();

            return load;
        }

        /// <summary>
        /// Process Accept/Reject of load tender.  Leave Load null to indicate rejection
        /// </summary>
        /// <param name="connect"></param>
        /// <param name="controlNumber"></param>
        /// <param name="load">Leave null to indicate rejection</param>
        /// <param name="declineReason">Reason load is being declined/rejected</param>
        /// <returns></returns>
        public static Edi990 GenerateEdi990(this XmlConnect connect, string controlNumber, LoadOrder load = null, string declineReason = "")
        {
            if (connect.DocumentType != EdiDocumentType.EDI204)
                throw new InvalidOperationException(
                    string.Format("Unable to generate EDI 990 for Xml connect with document type {0}", connect.DocumentType.GetString().FormattedString()));

            var edi990 = new Edi990
                {
                    TransactionSetHeader = new TransactionSetHeader
                        {
                            TransactionSetControlNumber = controlNumber,
                            TransactionSetIdentifierCode = TransSetIdentifierCode.RLT
                        },
                    Responses = new List<LoadResponse>
						{
							new LoadResponse
								{
									BeginningSegment = new BeginningSegmentBooking
										{
											Date = DateTime.Now.FormattedLongDateAlt(),
											DeclineReason = load == null ? declineReason : string.Empty,
											ReservationActionCode = load == null ? ReservationCode.D : ReservationCode.A,
											ShipmentIdentificationNumber = connect.ShipmentIdNumber,
											StandardCarrierAlphaCode = connect.Tenant.TenantScac
										},
									ReferenceIdentification = new ReferenceNumber
										{
											Description = string.Empty,
											ReferenceIdentification = load == null ? string.Empty : load.LoadOrderNumber,
											ReferenceIdentificationQualifier = RefIdQualifier.BRQ
										},
									ResponseToSetControlNumber = connect.ControlNumber
								}
						},
                    TransactionSetTrailer = new TransactionSetTrailer
                        {
                            NumberOfIncludedSegments = 1,
                            TransactionSetControlNumber = controlNumber
                        }
                };



            return edi990;
        }



        public static string GetDescription(this AcknowledgementCode value)
        {
            switch (value)
            {
                case AcknowledgementCode.A:
                    return "Accepted";
                case AcknowledgementCode.E:
                    return "Accepted But Errors Were Noted";
                case AcknowledgementCode.M:
                    return "Rejected, Message Authentication Code (MAC) Failed";
                case AcknowledgementCode.R:
                    return "Rejected";
                case AcknowledgementCode.W:
                    return "Rejected, Assurance Failed Validity Tests";
                case AcknowledgementCode.X:
                    return "Rejected, Content After Decryption Could Not Be Analyzed";
                default:
                    return string.Empty;
            }
        }

        public static string GetDescription(this CommNumberQualifier value)
        {
            switch (value)
            {
                case CommNumberQualifier.FX:
                    return "Fax";
                case CommNumberQualifier.TE:
                    return "Telephone";
				case CommNumberQualifier.EM:
					return "Email";
                default:
                    return string.Empty;
            }
        }

        public static string GetDescription(this ContactFuncCode value)
        {
            switch (value)
            {
                case ContactFuncCode.CA:
                    return "Customer contact granting appointment";
                case ContactFuncCode.CW:
                    return "Confirmed with";
                case ContactFuncCode.DC:
                    return "Delivery contact";
                case ContactFuncCode.GC:
                    return "General contact";
                case ContactFuncCode.SC:
                    return "Schedule contact";
                case ContactFuncCode.HZ:
                    return "Hazardous Material";
                default:
                    return string.Empty;
            }
        }

        public static string GetDescription(this EntityIdCode value)
        {
            switch (value)
            {
                case EntityIdCode.BT:
                    return "Bill to";
                case EntityIdCode.CN:
                    return "Consignee";
                case EntityIdCode.DT:
                    return "Destination Terminal";
                case EntityIdCode.EL:
                    return "Event Location";
                case EntityIdCode.SF:
                    return "Ship from";
                case EntityIdCode.ST:
                    return "Ship to";
                case EntityIdCode.SH:
                    return "Shipper";
                case EntityIdCode.OT:
                    return "Origin Terminal";
                case EntityIdCode.PR:
                    return "Payor";
                default:
                    return string.Empty;
            }
        }

        public static string GetDescription(this RefIdQualifier value)
        {
            switch (value)
            {
                case RefIdQualifier.BRQ:
                    return "Booking Number";
                case RefIdQualifier.CUS:
                    return "Custom";
                case RefIdQualifier.INV:
                    return "Invoice Number";
                case RefIdQualifier.ZH:
                    return "Pickup confirmation number";
                case RefIdQualifier.PO:
                    return "PO Number";
                case RefIdQualifier.CN:
                    return "Pro/tracking number";
                case RefIdQualifier.QUO:
                    return "Quote Number";
                case RefIdQualifier.SBL:
                    return "Shipper Bill of Lading";
                case RefIdQualifier.SHP:
                    return "Shipment Number";
                case RefIdQualifier.SHR:
                    return "Shipper Reference";
                default:
                    return string.Empty;
            }
        }

        public static string GetDescription(this ReservationCode value)
        {
            switch (value)
            {
                case ReservationCode.A:
                    return "Accepted";
                case ReservationCode.D:
                    return "Declined";
                default:
                    return string.Empty;
            }
        }

        public static string GetDescription(this SetPurposeAppType value)
        {
            switch (value)
            {
                case SetPurposeAppType.LT:
                    return "Load Tender";
                default:
                    return string.Empty;
            }
        }

        public static string GetDescription(this SetPurposeTransType value)
        {
            switch (value)
            {
                case SetPurposeTransType.CA:
                    return "Cancellation";
                case SetPurposeTransType.OR:
                    return "Original";
                case SetPurposeTransType.UP:
                    return "Update";
                default:
                    return string.Empty;
            }
        }

        public static string GetDescription(this ShipmentQualifier value)
        {
            switch (value)
            {
                case ShipmentQualifier.G:
                    return "General";
                default:
                    return string.Empty;
            }
        }

        public static string GetDescription(this ShipMethodOfPay value)
        {
            switch (value)
            {
                case ShipMethodOfPay.CC:
                    return "Collect";
                case ShipMethodOfPay.PP:
                    return "Prepaid";
                case ShipMethodOfPay.TP:
                    return "Third party";
                default:
                    return string.Empty;
            }
        }

        public static string GetDescription(this ShipStatMsgCode value)
        {
            switch (value)
            {
                case ShipStatMsgCode.AA:
                    return "Pick up appointment date and/or time";
                case ShipStatMsgCode.AB:
                    return "Delivery appointment date and/or time";
                case ShipStatMsgCode.A3:
                    return "Shipment returned to shipper";
                case ShipStatMsgCode.A7:
                    return "Refused by consignee";
                case ShipStatMsgCode.A9:
                    return "Shipment damaged";
                case ShipStatMsgCode.AF:
                    return "Carrier departed pickup location with shipment";
                case ShipStatMsgCode.AG:
                    return "Estimated delivery";
                case ShipStatMsgCode.AH:
                    return "Attempted delivery";
                case ShipStatMsgCode.AI:
                    return "Shipment has been reconsigned";
                case ShipStatMsgCode.AJ:
                    return "Tendered for delivery";
                case ShipStatMsgCode.AM:
                    return "Loaded on truck";
                case ShipStatMsgCode.AN:
                    return "Diverted to air carrier";
                case ShipStatMsgCode.AP:
                    return "Delivery not completed";
                case ShipStatMsgCode.AR:
                    return "Rail arrival at destination intermodal ramp";
                case ShipStatMsgCode.AV:
                    return "Available for delivery";
                case ShipStatMsgCode.B6:
                    return "Estimated to arrive at carrier terminal";
                case ShipStatMsgCode.BA:
                    return "Connecting line or cartage pick-up";
                case ShipStatMsgCode.BC:
                    return "Storage in transit";
                case ShipStatMsgCode.C1:
                    return "Estimated to depart terminal location";
                case ShipStatMsgCode.CA:
                    return "Shipment cancelled";
                case ShipStatMsgCode.CD:
                    return "Carrier departed delivery location";
                case ShipStatMsgCode.CL:
                    return "Trailer closed out";
                case ShipStatMsgCode.CP:
                    return "Completed loading at pick-up location";
                case ShipStatMsgCode.D1:
                    return "Completed unloading at delivery location";
                case ShipStatMsgCode.I1:
                    return "In-gate";
                case ShipStatMsgCode.J1:
                    return "Delivered to connecting line";
                case ShipStatMsgCode.K1:
                    return "Arrived at customs";
                case ShipStatMsgCode.L1:
                    return "Loading";
                case ShipStatMsgCode.OA:
                    return "Out-gate";
                case ShipStatMsgCode.OO:
                    return "Paperwork received - did not receive shipment or equipment";
                case ShipStatMsgCode.P1:
                    return "Departed terminal location";
                case ShipStatMsgCode.PR:
                    return "U.S. customs hold at origin intermodal ramp";
                case ShipStatMsgCode.R1:
                    return "Received from prior carrier";
                case ShipStatMsgCode.RL:
                    return "Rail departure from origin intermodal ramp";
                case ShipStatMsgCode.S1:
                    return "Trailer spotted at consignee's location";
                case ShipStatMsgCode.SD:
                    return "Shipment delayed";
                case ShipStatMsgCode.X1:
                    return "Arrived at delivery location";
                case ShipStatMsgCode.X2:
                    return "Estimated date and/or time of arrival at consignee's location";
                case ShipStatMsgCode.X3:
                    return "Arrived at pick-up location";
                case ShipStatMsgCode.X4:
                    return "Arrived at terminal location";
                case ShipStatMsgCode.X5:
                    return "Arrived at delivery location loading dock";
                case ShipStatMsgCode.X6:
                    return "En route to delivery location";
                case ShipStatMsgCode.X8:
                    return "Arrived at pick-up location loading dock";
                case ShipStatMsgCode.XB:
                    return "Shipment acknowledged";
                default:
                    return string.Empty;
            }
        }

        public static string GetDescription(this ShipStatReasonCode value)
        {
            switch (value)
            {
                case ShipStatReasonCode.A1:
                    return "Missed Delivery";
                case ShipStatReasonCode.A2:
                    return "Incorrect Address";
                case ShipStatReasonCode.A3:
                    return "Indirect Delivery";
                case ShipStatReasonCode.A5:
                    return "Unable to Locate";
                case ShipStatReasonCode.A6:
                    return "Address Corrected - Delivery Attempted";
                case ShipStatReasonCode.AA:
                    return "Mis-sort";
                case ShipStatReasonCode.AD:
                    return "Customer Requested Future Delivery";
                case ShipStatReasonCode.AE:
                    return "Restricted Articles Unacceptable";
                case ShipStatReasonCode.AF:
                    return "Accident";
                case ShipStatReasonCode.AG:
                    return "Consignee Related";
                case ShipStatReasonCode.AH:
                    return "Driver Related";
                case ShipStatReasonCode.AI:
                    return "Mechanical Breakdown";
                case ShipStatReasonCode.AJ:
                    return "Other Carrier Related";
                case ShipStatReasonCode.AK:
                    return "Damaged, Rewrapped in Hub";
                case ShipStatReasonCode.AL:
                    return "Previous Stop";
                case ShipStatReasonCode.AM:
                    return "Shipper Related";
                case ShipStatReasonCode.AN:
                    return "Holiday - Closed";
                case ShipStatReasonCode.AO:
                    return "Weather or Natural Disaster Related";
                case ShipStatReasonCode.AR:
                    return "Improper International Paperwork";
                case ShipStatReasonCode.AS:
                    return "Hold Due to Customs Documentation Problems";
                case ShipStatReasonCode.AT:
                    return "Unable to Contact Recipient for Broker Information";
                case ShipStatReasonCode.AU:
                    return "Civil Event Related Delay";
                case ShipStatReasonCode.AV:
                    return "Exceeds Service Limitations";
                case ShipStatReasonCode.AW:
                    return "Past Cut-off Time";
                case ShipStatReasonCode.AX:
                    return "Insufficient Pick-up Time";
                case ShipStatReasonCode.AY:
                    return "Missed Pick-up";
                case ShipStatReasonCode.AZ:
                    return "Alternate Carrier Delivered";
                case ShipStatReasonCode.B1:
                    return "Consignee Closed";
                case ShipStatReasonCode.B2:
                    return "Trap for Customer";
                case ShipStatReasonCode.B4:
                    return "Held for Payment";
                case ShipStatReasonCode.B5:
                    return "Held for Consignee";
                case ShipStatReasonCode.B8:
                    return "Improper Unloading Facility or Equipment";
                case ShipStatReasonCode.B9:
                    return "Receiving Time Restricted";
                case ShipStatReasonCode.BB:
                    return "Held per Shipper";
                case ShipStatReasonCode.BC:
                    return "Missing Documents";
                case ShipStatReasonCode.BD:
                    return "Border Clearance";
                case ShipStatReasonCode.BE:
                    return "Road Conditions";
                case ShipStatReasonCode.BF:
                    return "Carrier Keying Error";
                case ShipStatReasonCode.BG:
                    return "Other";
                case ShipStatReasonCode.BH:
                    return "Insufficient Time to Complete Delivery";
                case ShipStatReasonCode.BI:
                    return "Cartage Agent";
                case ShipStatReasonCode.BJ:
                    return "Customer Wanted Earlier Delivery";
                case ShipStatReasonCode.BK:
                    return "Prearranged Appointment";
                case ShipStatReasonCode.BL:
                    return "Held for Protective Service";
                case ShipStatReasonCode.BM:
                    return "Flatcar Shortage";
                case ShipStatReasonCode.BN:
                    return "Failed to Release Billing";
                case ShipStatReasonCode.BO:
                    return "Railroad Failed to Meet Schedule";
                case ShipStatReasonCode.BP:
                    return "Load Shifted";
                case ShipStatReasonCode.BQ:
                    return "Shipment Overweight";
                case ShipStatReasonCode.BR:
                    return "Train Derailment";
                case ShipStatReasonCode.BS:
                    return "Refused by Customer";
                case ShipStatReasonCode.BT:
                    return "Returned to Shipper";
                case ShipStatReasonCode.C1:
                    return "Waiting for Customer Pick-up";
                case ShipStatReasonCode.C2:
                    return "Credit Hold";
                case ShipStatReasonCode.C3:
                    return "Suspended at Customer Request";
                case ShipStatReasonCode.C4:
                    return "Customer Vacation";
                case ShipStatReasonCode.C5:
                    return "Customer Strike";
                case ShipStatReasonCode.C6:
                    return "Waiting Shipping Instructions";
                case ShipStatReasonCode.C7:
                    return "Waiting for Customer Specified Carrier";
                case ShipStatReasonCode.C8:
                    return "Collect on Delivery Required";
                case ShipStatReasonCode.C9:
                    return "Cash Not Available From Consignee";
                case ShipStatReasonCode.CA:
                    return "Customer (Import or Export)";
                case ShipStatReasonCode.CB:
                    return "No Requested Arrival Date Provided by Shipper";
                case ShipStatReasonCode.CC:
                    return "No Requested Arrival Time Provided by Shipper";
                case ShipStatReasonCode.D1:
                    return "Carrier Dispatch Error";
                case ShipStatReasonCode.F1:
                    return "Non-Express Clearance Delay";
                case ShipStatReasonCode.F2:
                    return "International Non-carrier Delay";
                case ShipStatReasonCode.HB:
                    return "Held Pending Appointment";
                case ShipStatReasonCode.NA:
                    return "Normal Appointment";
                case ShipStatReasonCode.D2:
                    return "Driver Not Available";
                case ShipStatReasonCode.NS:
                    return "Normal Status";
                case ShipStatReasonCode.P1:
                    return "Processing Delay";
                case ShipStatReasonCode.P2:
                    return "Waiting Inspection";
                case ShipStatReasonCode.P3:
                    return "Production Falldown";
                case ShipStatReasonCode.P4:
                    return "Held for Full Carrier Load";
                case ShipStatReasonCode.RC:
                    return "Reconsigned";
                case ShipStatReasonCode.S1:
                    return "Delivery Shortage";
                case ShipStatReasonCode.T1:
                    return "Tractor With Sleeper Car Not Available";
                case ShipStatReasonCode.T2:
                    return "Tractor, Conventional, Not Available";
                case ShipStatReasonCode.T3:
                    return "Trailer not Available";
                case ShipStatReasonCode.T4:
                    return "Trailer Not Usable Due to Prior Product";
                case ShipStatReasonCode.T5:
                    return "Trailer Class Not Available";
                case ShipStatReasonCode.T6:
                    return "Trailer Volume Not Available";
                case ShipStatReasonCode.T7:
                    return "Insufficient Delivery Time";
                default:
                    return string.Empty;
            }
        }

        public static string GetDescription(this StopReasonCode value)
        {
            switch (value)
            {
                case StopReasonCode.CL:
                    return "Complete Load";
                case StopReasonCode.CN:
                    return "Consolidate";
                case StopReasonCode.CU:
                    return "Complete Unload";
                case StopReasonCode.DT:
                    return "Drop trailer";
                case StopReasonCode.LD:
                    return "Load";
                case StopReasonCode.PL:
                    return "Part Load";
                case StopReasonCode.PU:
                    return "Part Unload";
                case StopReasonCode.UL:
                    return "Un Load";
                default:
                    return string.Empty;
            }
        }

        public static string GetDescription(this WghtUnitQualifier value)
        {
            switch (value)
            {
                case WghtUnitQualifier.L:
                    return "Pounds";
                default:
                    return string.Empty;
            }
        }


        public static string GetServiceModeCode(this ServiceMode value)
        {
            switch (value)
            {
                case ServiceMode.Air:
                    return "AIR";
                case ServiceMode.Rail:
                    return "RAIL";
                case ServiceMode.Truckload:
                    return "FTL";
                case ServiceMode.LessThanTruckload:
                    return "LTL";
                case ServiceMode.SmallPackage:
                    return "SP";
                default:
                    return string.Empty;
            }
        }


        public static List<EdiDiff> Diff<T>(this T obj1, T obj2) where T : class
        {
            var values1 = new Dictionary<string, string>();
            var values2 = new Dictionary<string, string>();

            GetPropertyValue(obj1, values1, string.Empty);
            GetPropertyValue(obj2, values2, string.Empty);

          var newFields = values1.Keys
                .Where(k => !values2.ContainsKey(k)).Select(k => new EdiDiff
                {
                    Description = k,
                    OldValue = string.Empty,
                    NewValue = values1[k]
                })
                .ToList();

            var differences = values1.Keys
                .Where(k => values2.ContainsKey(k) && values1[k] != values2[k])
                .Select(k => new EdiDiff
                {
                    Description = k,
                    OldValue = values2[k],
                    NewValue = values1[k]
                })
                .ToList();
            differences.AddRange(newFields);
            
            return differences;
        }

        internal static void GetPropertyValue(Object obj, IDictionary<string, string> values, string prefix)
        {
            if (obj == null) return;
            var type = obj.GetType();
            var ps = type.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).ToList();

            var s = string.IsNullOrEmpty(prefix) ? string.Empty : prefix + ".";
            foreach (var p in ps.Where(x => !x.PropertyType.IsClass && !x.PropertyType.IsGenericType && x.CanRead))
                values.Add(s + p.Name, p.GetValue(obj, null).GetString());
            foreach (var p in ps.Where(x => x.PropertyType == typeof(String) && x.CanRead))
                values.Add(s + p.Name, p.GetValue(obj, null).GetString());

            foreach (var p in ps.Where(x => x.PropertyType.IsGenericType && x.CanRead))
            {
                var objPv = p.GetValue(obj, null);
                if (objPv == null) continue;
                var objPvT = objPv.GetType();
                var cnt = objPvT.GetProperty("Count").GetValue(objPv, null).ToInt();

                for (var i = 0; i < cnt; i++)
                {
                    var item = objPvT.GetProperty("Item").GetValue(objPv, new object[] { i });
                    GetPropertyValue(item, values, string.Format("{0}{1}.{2}.{3}", s, p.Name, i, item.GetType().Name));
                }
            }

            foreach (var p in ps.Where(x => x.PropertyType.IsClass && x.PropertyType != typeof(String) && !x.PropertyType.IsGenericType && x.CanRead))
                GetPropertyValue(p.GetValue(obj, null), values, string.Format("{0}{1}", s, p.Name));
        }






















    }
}
