﻿using System;

namespace LogisticsPlus.Eship.Extern
{
	public class UnknownExternObjectType : Exception
	{
		private const string DefaultMessage = "Object type is unknown and cannot be processed.";

		public UnknownExternObjectType() : base(DefaultMessage)
		{
		}

		public UnknownExternObjectType(string message) : base(string.Format("{0} [{1}]", DefaultMessage, message))
		{
		}
	}
}
