﻿using System;
using System.Linq;
using System.Collections.Generic;
using My.LogisticsPlus.Net.Core.Operations;
using ObjToSql.Core;

namespace ClaimTransfer
{
    public class ClaimHandler : EntityBase
    {
        private const string ClaimLogTemplate = "Claim Number: {0}";

        /// <summary>
        ///     Saves, updates, and deletes Claims in the Mylp database.
        /// </summary>
        /// <param name="claims">The list of Mylp claims to save/update.</param>
        /// <param name="logger">The logger being used.</param>
        public bool SaveClaimsToMylpDatabase(List<MylpClaim> claims, Logger logger)
        {
            DatabaseConnection.DefaultConnectionString = Settings.MylpConnectionString;
            Connection = DatabaseConnection.DefaultConnection;

            BeginTransaction();

            try
            {
                var claimsToDelete = new MylpClaimSearch().FetchExternalClaims()
                                                          .Where(c => !claims.Select(cl => cl.Claim)
                                                                             .Select(cl => cl.ClaimNumber)
                                                                             .Contains(c.Claim.ClaimNumber)).ToList();

                // Remove deleted eShip claims from Mylp
                foreach (var existingClaim in claimsToDelete)
                {
                    ProcessDeletedClaim(existingClaim.Claim);
                }


                foreach (var claim in claims)
                {
                    claim.Claim.Save();
                    ProcessClaimShipments(claim.Claim);
                }

                CommitTransaction();

                logger.LogInfo("Successfully Transferred or Updated the following claims:");
                foreach (var claim in claims)
                {
                    logger.LogInfo(ClaimLogTemplate, claim.Claim.ClaimNumber);
                }

                logger.LogInfo("");
                logger.LogInfo("Successfully Deleted the following claims:", claimsToDelete.Count);
                foreach (var claim in claimsToDelete)
                {
                    logger.LogInfo(ClaimLogTemplate, claim.Claim.ClaimNumber);
                }

                logger.LogInfo("");
                return true;
            }
            catch (Exception err)
            {
                RollBackTransaction();
                Utilities.LogException(logger, err);
                return false;
            }
        }

        /// <summary>
        /// Saves or deletes a claim shipment.
        /// </summary>
        /// <param name="claim">The associated claim.</param>
        private void ProcessClaimShipments(Claim claim)
        {
            var shipments = claim.Shipments;
            foreach (var shipment in shipments)
            {
                shipment.ClaimId = claim.Id;

                shipment.Connection = Connection;
                shipment.Transaction = Transaction;

                if (shipment.ShipmentNumber != string.Empty)
                    shipment.Save();
                else
                    shipment.Delete();
            }
        }

        /// <summary>
        /// Deletes a claim and all associated Claim Shipments
        /// </summary>
        /// <param name="claim">Claim to delete.</param>
        private void ProcessDeletedClaim(Claim claim)
        {
            foreach (var shipment in claim.Shipments)
            {
                shipment.Connection = Connection;
                shipment.Transaction = Transaction;
                shipment.Delete();
            }

            claim.Connection = Connection;
            claim.Transaction = Transaction;
            claim.Delete();
        }

    }
}
