﻿using System.Collections.Generic;
using ObjToSql.Core;

namespace ClaimTransfer
{
    class EshipClaimSearch : EntityBase
    {
        private const int TenantId = 4;
        
        /// <summary>
        /// Fetches all eShip claims.
        /// </summary>
        public List<EshipClaim> FetchAllEshipClaims()
        {
            DatabaseConnection.DefaultConnectionString = Settings.EshipConnectionString;

            var claims = new List<EshipClaim>();

            const string query = @"SELECT * FROM Claim WHERE TenantId = @TenantId";
            var parameters = new Dictionary<string, object> { { "TenantId", TenantId }, };

            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    claims.Add(new EshipClaim(reader));

            Connection.Close();
            return claims;
        }
    }
}
