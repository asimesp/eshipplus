﻿using System;

namespace ClaimTransfer
{
    public static class DateUtility
    {
        public static DateTime SystemEarliestDateTime
        {
            get { return new DateTime(1753, 1, 1, 0, 0, 0); }
        } 
    }
}
