﻿using System.Data.Common;
using LogisticsPlus.Eship.Core.Operations;
using ObjToSql.Core;

namespace ClaimTransfer
{
    public class EshipClaim : EntityBase
    {
        private Claim _claim;

        public Claim Claim
        {
            get
            {
                DatabaseConnection.DefaultConnectionString = Settings.EshipConnectionString;
                return _claim;
            }
            set
            {
                DatabaseConnection.DefaultConnectionString = Settings.EshipConnectionString;
                _claim = value;
            }
        }

        public EshipClaim(DbDataReader reader)
        {
            Claim = new Claim(reader);
        }
    }
}
