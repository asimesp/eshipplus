﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ClaimTransfer
{
    /// <summary>
    /// Provides basic logging to a file and the console.
    /// </summary>
    public class Logger : IDisposable
    {
        // internal constants
        private const int __logLinesToKeep = 2000;

        // internal fields
        private Severity _logLevel;
        private StreamWriter _writer = null;
        private bool _consoleLogging;
        private readonly string[] _severityStrings = { "ERR", "INF", "DBG" };

        #region [ Constructors / Destructor ]

        /// <summary>
        /// Initializes a new instance of the <see cref="Logger"/> class.
        /// </summary>
        /// <param name="filePath">The file path of the log file. The messages will not be logged to a file if this argument is null.</param>
        /// <param name="overwriteExisting">if set to <c>true</c> the existing log file wil be overwritten.</param>
        /// <param name="logLevel">Log level Error will only log error messages while Debug will log Error and Debug.</param>
        /// <param name="consoleLogging">if set to <c>true</c> log messages will be also printed to the console.</param>
        public Logger(string filePath, bool overwriteExisting, Severity logLevel, bool consoleLogging, bool rotateLog)
        {
            // store our variables
            _logLevel = logLevel;
            _consoleLogging = consoleLogging;

            if (!string.IsNullOrEmpty(filePath))
            {
                if (!overwriteExisting && File.Exists(filePath))
                {
                    if (rotateLog)
                    {
                        // clean-up the log file while keeping some of the latest log lines
                        using (StreamReader reader = new StreamReader(filePath))
                        {
                            Queue<string> queue = new Queue<string>();
                            string line = null;
                            while ((line = reader.ReadLine()) != null)
                            {
                                queue.Enqueue(line);
                                if (queue.Count > __logLinesToKeep) queue.Dequeue();
                            }
                            reader.Close();

                            // write the remaining lines back to the file
                            _writer = new StreamWriter(filePath, false);
                            foreach (string s in queue)
                            {
                                _writer.WriteLine(s);
                            }
                            _writer.Flush();
                        }
                    }
                    else
                    {
                        // open the log for appending
                        _writer = new StreamWriter(filePath, true);
                    }
                }
                else
                {
                    // just open the file and override the content
                    _writer = new StreamWriter(filePath, false);
                }

                // write an empty line to the file to mark the new log section
                _writer.WriteLine("");
            }
        }

        ~Logger()
        {
            Dispose();
        }

        #endregion

        #region [ Methods ]

        /// <summary>
        /// Logs a debug message.
        /// </summary>
        /// <param name="msg">The log message.</param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        public void LogDebug(string msg, params object[] args)
        {
            Log(Severity.Debug, msg, args);
        }

        /// <summary>
        /// Logs an information message message.
        /// </summary>
        /// <param name="msg">The log message.</param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        public void LogInfo(string msg, params object[] args)
        {
            Log(Severity.Info, msg, args);
        }

        /// <summary>
        /// Logs an debug message.
        /// </summary>
        /// <param name="msg">The log message.</param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        public void LogError(string msg, params object[] args)
        {
            Log(Severity.Error, msg, args);
        }

        /// <summary>
        /// Appends a log entry to the log file / console.
        /// </summary>
        /// <param name="severity">The severity of the message.</param>
        /// <param name="msg">The log message to be added. The message composite formating.</param>
        /// <param name="args">An object array that contains zero or more objects to format</param>
        public void Log(Severity severity, string msg, params object[] args)
        {
            if (severity > _logLevel) return;

            // format the message
            string message = string.Format(msg, args);

            // write to file
            if (_writer != null)
            {
                _writer.WriteLine(string.Format("{0:yyyy-MM-dd HH:mm:ss}\t[{1}]\t{2}",
                    DateTime.Now, _severityStrings[(int)severity], message));
            }

            // write to console
            if (_consoleLogging)
            {
                Console.WriteLine(message);
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // close the log file 
            if (_writer != null) _writer.Close();
        }

        #endregion

        #region [ Static Methods ]

        /// <summary>
        /// Rotates the log files.
        /// </summary>
        /// <param name="path">The directory of the log files.</param>
        /// <param name="filesSearchPattern">The log files DOS wildcard search pattern. Only files that match the pattern will be proccessed.</param>
        /// <param name="daysToKeep">Number of days (counted from today) to keep.</param>
        /// <param name="minFilesToKeep">The min files to keep.</param>
        /// <param name="maxFilesToKeep">The max files to keep.</param>
        /// <returns></returns>
        public static int RotateLogFiles(string path, string filesSearchPattern, int daysToKeep, int minFilesToKeep, int maxFilesToKeep)
        {
            // validate arguments
            if (string.IsNullOrEmpty(path)) throw new ArgumentException("Parameter path is required.");
            if (string.IsNullOrEmpty(filesSearchPattern)) throw new ArgumentException("Parameter filesSearchPattern is required.");

            int deletedFiles = 0;

            try
            {
                string filesPath = (path.EndsWith("\\") ? path : path + "\\");
                if (!Directory.Exists(path)) return 0;

                string[] files = Directory.GetFiles(filesPath, filesSearchPattern, SearchOption.TopDirectoryOnly);
                var list = new List<FileInfo>(files.Length);
                list.AddRange(files.Select(f => new FileInfo(f)));
                list.Sort((a, b) => b.CreationTime.CompareTo(a.CreationTime));

                // do nothing if we need a min. number of files to be kept
                if (minFilesToKeep > 0 && list.Count <= minFilesToKeep) return 0;

                DateTime dt = (daysToKeep > 0 ? DateTime.Now.AddDays(-daysToKeep) : DateTime.MaxValue);
                int index = (minFilesToKeep > 0 ? minFilesToKeep : 0);
                for (int i = index; i < list.Count; i++)
                {
                    try
                    {
                        if (list[i].CreationTime < dt || (maxFilesToKeep > 0 && i >= maxFilesToKeep))
                        {
                            File.Delete(list[i].FullName);
                            deletedFiles++;
                        }
                    }
                    catch
                    {
                    }
                }
            }
            catch
            {
                // no exception should bubble out
            }

            return deletedFiles;
        }

        #endregion

        /// <summary>
        /// Logs an exception.
        /// </summary>
        /// <param name="logger">An initialized Logger object.</param>
        /// <param name="ex">The Exception object to be logged.</param>
        public static void LogException(Logger logger, Exception ex)
        {
            if (ex == null) return;

            logger.LogError("Exception: [{0}, {1}] {2}", ex.Source, ex.GetType().ToString(), ex.Message);
            var e = ex.GetBaseException();
            logger.LogError("Base exception: [{0}] {1}", e.GetType().ToString(), e.Message);
        }
    }
}
