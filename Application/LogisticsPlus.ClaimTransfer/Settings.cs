﻿using System.Configuration;

namespace ClaimTransfer
{
    public class Settings
    {
        public static string MylpConnectionString
        {
            get { return GetConnectionString("MylpConnectionString"); }
        }

        public static string EshipConnectionString
        {
            get { return GetConnectionString("eShipConnectionString"); }
        }


        public static long DefaultMylpUserId
        {
            get { return GetSetting("DefaultMylpUserId").ToLong(); }
        }

        public static string EShipShipmentLink
        {
            get { return GetSetting("EShipShipmentLink"); }
        }


        private static string GetConnectionString(string name)
        {
            return ConfigurationManager.ConnectionStrings[name].ConnectionString;
        }

        public static string GetSetting(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }
    }
}
