﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace ClaimTransfer
{
    public class Program
    {
        /// <summary>
        /// Entry point for app.
        /// </summary>
        private static void Main()
        {
            // prepare the paths
            var startupPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var logFilesFolder = startupPath + "\\Logs\\";
            var logFilePath = string.Format("{0}ClaimTransfer_{1:yyyyMMddHHmmss}.log", logFilesFolder, DateTime.Now); ;
            if (!Directory.Exists(logFilesFolder)) Directory.CreateDirectory(logFilesFolder);

            // rotate the old log files
            Logger.RotateLogFiles(logFilesFolder, "ClaimTransfer_*.log", 7, 7, 14);

            // run the converter
            using (var logger = new Logger(logFilePath, false, Severity.Info, false, false))
            {
                try
                {
                    logger.LogInfo("Starting claim transfer.");
                    logger.LogInfo("");
                    var start = DateTime.Now;

                    var eShipClaimSearcher = new EshipClaimSearch();

                    // fetch all eShip Claims
                    var eShipClaims = eShipClaimSearcher.FetchAllEshipClaims();

                    // convert all eShip Claim objects to their eTnT equivalents
                    var mylpClaims = eShipClaims.Select(EntityConversionUtility.ConvertEshipClaimToEtnt).ToList();

                    // Try to save the claims to the eTnT database
                    var handler = new ClaimHandler();
                    var success = handler.SaveClaimsToMylpDatabase(mylpClaims, logger);

                    if (!success) return;

                    logger.LogInfo("");
                    logger.LogInfo("Claims Transfer executed in {0} sec.", (DateTime.Now - start).TotalSeconds);
                }
                catch (Exception err)
                {
                    Utilities.LogException(logger, err);
                }
            }
        }

        
    }
}
