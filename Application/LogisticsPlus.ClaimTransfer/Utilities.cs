﻿using System;

namespace ClaimTransfer
{
    public static class Utilities
    {
        public static long ToLong(this object value)
        {
            var x = default(long);
            return value == null ? x : Int64.TryParse(value.ToString(), out x) ? x : default(long);
        }

        public static int ToInt(this object value)
        {
            var x = default(int);
            return value == null
                    ? x
                    : value.GetType().IsEnum
                        ? (int)value
                        : Int32.TryParse(value.ToString(), out x) ? x : default(int);
        }

        public static T ToEnum<T>(this int value)
        {
            return (T)Enum.Parse(typeof(T), value.ToString());
        }


        public static string Truncate(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }

        public static string GenerateClaimDetailString(this string value)
        {
            if (value.Length > 1900)
            {
                var trimMsg = Environment.NewLine + "... See Remaining Vendors on eShip";
                value = value.Substring(0, 1900 - trimMsg.Length);
                value += trimMsg;
            }
            return value;
        }


        /// <summary>
        /// Logs an exception.
        /// </summary>
        /// <param name="logger">An initialized Logger object.</param>
        /// <param name="ex">The Exception object to be logged.</param>
        public static void LogException(Logger logger, Exception ex)
        {
            if (ex == null) return;

            logger.LogError("Exception: [{0}, {1}] {2}", ex.Source, ex.GetType().ToString(), ex.Message);
            var e = ex.GetBaseException();
            logger.LogError("Base exception: [{0}] {1}", e.GetType().ToString(), e.Message);
        }

    }
}
