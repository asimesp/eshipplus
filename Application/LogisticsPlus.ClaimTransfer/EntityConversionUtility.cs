﻿using System;
using System.Linq;
using My.LogisticsPlus.Net.Core.Operations;
using ObjToSql.Core;

namespace ClaimTransfer
{
    public static class EntityConversionUtility
    {
        private const string ClaimDetailTemplate = "eShip User: {0} \neShip Customer: {1} \neShip Vendors: {2}";

        /// <summary>
        /// Converts an eShip claim to an eTnT claim.
        /// </summary>
        /// <param name="eShipClaim">The eShip claim to be converted.</param>
        public static MylpClaim ConvertEshipClaimToEtnt(EshipClaim eShipClaim)
        {
            var claimSearch = new MylpClaimSearch();
            var existingClaim = claimSearch.FetchClaimByNumber(eShipClaim.Claim.ClaimNumber);

            var mylpClaim = existingClaim ?? new MylpClaim { Claim = new Claim() };

            if (!mylpClaim.Claim.IsNew) mylpClaim.Claim.LoadCollections();

            mylpClaim.Claim.UserId = Settings.DefaultMylpUserId.ToLong();
            mylpClaim.Claim.ClaimNumber = eShipClaim.Claim.ClaimNumber;
            mylpClaim.Claim.DateCreated = eShipClaim.Claim.DateCreated;
            mylpClaim.Claim.ClaimDate = eShipClaim.Claim.ClaimDate;
            mylpClaim.Claim.Status = eShipClaim.Claim.Status.ToInt().ToEnum<ClaimStatus>();
            mylpClaim.Claim.ClaimType = eShipClaim.Claim.ClaimType.ToInt().ToEnum<ClaimType>();
            mylpClaim.Claim.IsRepairable = eShipClaim.Claim.IsRepairable;
            mylpClaim.Claim.EstimatedRepairCost = eShipClaim.Claim.EstimatedRepairCost;
            mylpClaim.Claim.AmountClaimed = eShipClaim.Claim.AmountClaimed;
            mylpClaim.Claim.AmountClaimedType = eShipClaim.Claim.AmountClaimedType.ToInt().ToEnum<AmountClaimedType>();
            mylpClaim.Claim.ClaimantReferenceNumber = eShipClaim.Claim.ClaimantReferenceNumber;
            mylpClaim.Claim.Source = SourceType.EShip;

            var claimId = mylpClaim.Claim.Id;

            // grab all shipments that aren't already in the Mylp Claim
            var mlypShipmentNumbers = mylpClaim.Claim.Shipments.Select(sp => sp.ShipmentNumber).ToList();
            var accountId = mylpClaim.Claim.AccountId;
            var eShipShipments = eShipClaim.Claim.Shipments;

            var claimShipments = eShipShipments.Where(s => !mlypShipmentNumbers.Contains(s.Shipment.ShipmentNumber))
                                              .Select(shipment => new ClaimShipment
                                              {
                                                  ClaimId = claimId,
                                                  ShipmentNumber = shipment.Shipment.ShipmentNumber,
                                                  ShipmentType = ShipmentType.External,
                                                  ExternalLink = (Settings.EShipShipmentLink + shipment.Shipment.ShipmentNumber),
                                                  AccountId = accountId,
                                                  UserId = Settings.DefaultMylpUserId
                                              }).ToList();

            mylpClaim.Claim.Shipments.AddRange(claimShipments);

            // External Shipments with an empty shipment number number are shipments that have been deleted from an eShip claim
            var eShipShipmentNumbers = eShipClaim.Claim.Shipments.Select(sp => sp.Shipment.ShipmentNumber).ToList();
            foreach (var ship in mylpClaim.Claim.Shipments.Where(ship => !eShipShipmentNumbers.Contains(ship.ShipmentNumber)))
            {
                ship.ShipmentNumber = string.Empty;
            }


            var vendorString = Environment.NewLine +
                               eShipClaim.Claim.Vendors.Aggregate(string.Empty,
                                                                  (current, vendor) =>
                                                                  current + (vendor.Vendor.Name + Environment.NewLine));
            var claimDetailString = string.Format(ClaimDetailTemplate, eShipClaim.Claim.User.Username,
                                                        eShipClaim.Claim.Customer.Name, vendorString);
            mylpClaim.Claim.ClaimDetail = claimDetailString.GenerateClaimDetailString();

            return mylpClaim;
        }
    }
}
