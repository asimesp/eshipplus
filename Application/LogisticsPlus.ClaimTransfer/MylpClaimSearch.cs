﻿using System.Collections.Generic;
using ObjToSql.Core;

namespace ClaimTransfer
{
    public class MylpClaimSearch : EntityBase
    {
        /// <summary>
        /// Fetchs a Claim object from the eTnT Database based on the provided claim number.
        /// </summary>
        /// <param name="number">The Claim Number you are searching for.</param>
        public MylpClaim FetchClaimByNumber(string number)
        {
            DatabaseConnection.DefaultConnectionString = Settings.MylpConnectionString;

            MylpClaim claim = null;

            const string query = @"SELECT * FROM Claim WHERE ClaimNumber = @ClaimNumber";
            var parameters = new Dictionary<string, object> { { "ClaimNumber", number } };

            using (var reader = GetReader(query, parameters))
                if (reader.Read())
                    claim = new MylpClaim(reader);
            Connection.Close();
            return claim;
        }

        /// <summary>
        /// Fetches all claims that are external; those that are marked as eShip claims.
        /// </summary>
        public List<MylpClaim> FetchExternalClaims()
        {
            DatabaseConnection.DefaultConnectionString = Settings.MylpConnectionString;

            var claims = new List<MylpClaim>();

            const string query = @"SELECT * FROM Claim WHERE Source = 1";

            using (var reader = GetReader(query, new Dictionary<string, object>()))
                while (reader.Read())
                    claims.Add(new MylpClaim(reader));

            Connection.Close();
            return claims;
        }
    }
}
