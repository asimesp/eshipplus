﻿using System.Data.Common;
using My.LogisticsPlus.Net.Core.Operations;
using ObjToSql.Core;

namespace ClaimTransfer
{
    public class MylpClaim : EntityBase
    {
        private Claim _claim;

        public Claim Claim
        {
            get
            {
                DatabaseConnection.DefaultConnectionString = Settings.MylpConnectionString;
                return _claim;
            }
            set
            {
                DatabaseConnection.DefaultConnectionString = Settings.MylpConnectionString;
                _claim = value;
            }
        }

        public MylpClaim(DbDataReader reader)
        {
            Claim = new Claim(reader);
        }

        public MylpClaim()
        {
        }
    }
}
