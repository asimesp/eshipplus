﻿namespace ClaimTransfer
{
    /// <summary>
    /// Log entry severity enum
    /// </summary>
    public enum Severity
    {
        Error,
        Info,
        Debug
    }
}
