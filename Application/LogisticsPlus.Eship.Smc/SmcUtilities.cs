﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Smc.Eva;

namespace LogisticsPlus.Eship.Smc
{
	public static class SmcUtilities
	{
		internal static string Truncate(this string value, int length)
		{
			return string.IsNullOrEmpty(value) ? value : value.Length > length ? value.Substring(0, length) : value;
		}
        
        internal static string GetString(this object value)
        {
            return value == null ? string.Empty : value.ToString();
        }

		internal static int ToInt(this object value)
		{
			var x = default(int);
			return value == null
					? x
					: value.GetType().IsEnum
						? (int)value
						: Int32.TryParse(value.ToString(), out x) ? x : default(int);
		}

		internal static double ToDouble(this object value)
		{
			var x = default(double);
			return value == null ? x : Double.TryParse(value.ToString(), out x) ? x : default(double);
		}

		internal static DateTime ToDateTime(this object value)
		{
			var x = DateUtility.SystemEarliestDateTime;

			return value == null
					? x
					: value.ToString().ToArray().All(char.IsDigit)
					   ? value.ToString().IsValidSystemDateTime() ? DateTime.FromOADate(value.ToDouble()) : x
					   : DateTime.TryParse(value.ToString(), out x) ? x : DateUtility.SystemEarliestDateTime;
			
		}

		public static string GetSmcRatingCountry(this Country country)
		{
			if (country == null) return string.Empty;

			return country.Code.GetSmcRatingCountry();
		}

		internal static string GetSmcRatingCountry(this string countryCode)
		{
			switch (countryCode)
			{
				case IataCodes.Country.Usa:
					return SmcConstants.UsaCountryKey;
				case IataCodes.Country.Canada:
					return SmcConstants.CadCountryKey;
				case IataCodes.Country.Mexico:
					return SmcConstants.MexCountryKey;
				default:
					return countryCode;
			}
		}

		public static SmcCCXL.Country GetSmcTerminalInfoCountry(this Country country)
		{
			if (country == null) return SmcCCXL.Country.USA;

			return country.Code.GetSmcTerminalInfoCountry();
		}

		internal static SmcCCXL.Country GetSmcTerminalInfoCountry(this string countryCode)
		{
			switch (countryCode)
			{
				case IataCodes.Country.Canada:
				case SmcConstants.CadCountryKey:
					return SmcCCXL.Country.CAN;
				case IataCodes.Country.Mexico:
				case SmcConstants.MexCountryKey:
					return SmcCCXL.Country.MEX;
				default:
					return SmcCCXL.Country.USA;
			}
		}

		public static string EmptyStringToNull(this string value)
		{
			return string.IsNullOrEmpty(value) ? null : value;
		}



		public static HttpWebResponse ToHttpWebResponse(this WebResponse response)
		{
			return (HttpWebResponse)response;
		}

		internal static void SendReponse(this HttpResponse response, string data)
		{
			response.Clear();
			response.ContentType = SmcConstants.PlainContentType;
			response.BufferOutput = true;
			response.AddHeader("Content-Type", SmcConstants.PlainContentType);
			response.AddHeader("Content-Length", data.Length.ToString());
			response.Write(data);
			response.Flush();
			//response.End();
			//HttpContext.Current.ApplicationInstance.CompleteRequest();
		}


		public static DateTime DateTimeFromEvaDateAndTime(this string date, string time = "")
		{
			if (string.IsNullOrEmpty(date) || date.Length < 8) return DateUtility.SystemEarliestDateTime;
			
			// regular date returned
			if (date.Length == 10)
			{
				var retDate = date.ToDateTime();
				return string.IsNullOrEmpty(time) || time.Length != 6
					       ? retDate
					       : new DateTime(retDate.Year, retDate.Month, retDate.Day,
					                      time.Substring(0, 2).ToInt(), time.Substring(2, 2).ToInt(), time.Substring(4, 2).ToInt());
			}


			return string.IsNullOrEmpty(time) || time.Length != 6
				       ? new DateTime(date.Substring(0, 4).ToInt(), date.Substring(4, 2).ToInt(), date.Substring(6, 2).ToInt())
				       : new DateTime(date.Substring(0, 4).ToInt(), date.Substring(4, 2).ToInt(), date.Substring(6, 2).ToInt(),
				                      time.Substring(0, 2).ToInt(), time.Substring(2, 2).ToInt(), time.Substring(4, 2).ToInt());
		}

		public static string ToSmc3EvaDate(this DateTime dateTime)
		{
			return dateTime == DateUtility.SystemEarliestDateTime ? string.Empty : string.Format("{0:yyyyMMdd}", dateTime);
		}

		public static string ToSmc3EvaTime(this DateTime dateTime)
		{
			return dateTime == DateUtility.SystemEarliestDateTime ? string.Empty : string.Format("{0:HHmmss}", dateTime);
		}

		public static string FormattedSmc3EvaPhone(this string value)
		{
			string phone = string.Empty;
			foreach (var ch in value.ToCharArray())
				if (Char.IsDigit(ch)) phone += ch;
			var length = phone.Length;
			for (var i = 10; i > length; i--)
				phone = "0" + phone;
			if (phone.Length > 10) phone = phone.Substring(phone.Length - 10);
			return phone;
		}

        // generates a generic DispatchRequest that excludes:
        //      shipmentCommodity, scac, identifiers, shipmentCommodityFreightCodes, serviceCode, and paymentTerms
        public static DispatchRequest GenerateGenericDispatchRequest(Shipment shipment, VendorCommunication vcom)
        {
            var destContact = (shipment.Destination.Contacts.FirstOrDefault(c => c.Primary) ?? new ShipmentContact());
            var originContact = (shipment.Origin.Contacts.FirstOrDefault(c => c.Primary) ?? new ShipmentContact());

            var hasPickup = shipment.Origin.AppointmentDateTime != DateUtility.SystemEarliestDateTime;

            var dispatchRequest = new DispatchRequest
            {
                accountToken = vcom.IsInSMC3Testing ? vcom.SMC3TestAccountToken : vcom.SMC3ProductionAccountToken,
                billToAccount = null,
                billToAddress1 = null,
                billToAddress2 = null,
                billToCity = null,
                billToCompanyName = null,
                billToCountry = null,
                billToEmail = null,
                billToName = null,
                billToPhone = null,
                billToPostalCode = null,
                billToState = null,
                destinationAccount = null,
                destinationAddress1 = shipment.Destination.Street1,
                destinationAddress2 = shipment.Destination.Street2.EmptyStringToNull(),
                destinationCity = shipment.Destination.City,
                destinationCloseTime = null,
                destinationCompanyName = null,
                destinationCountry = shipment.Destination.Country.Code,
                destinationEmail = destContact.Email.EmptyStringToNull(),
                destinationName = shipment.Destination.Description.EmptyStringToNull(),
                destinationOpenTime = null,
                destinationPhone = originContact.Phone.FormattedSmc3EvaPhone(),
                destinationPostalCode = shipment.Destination.PostalCode,
                destinationState = shipment.Destination.State,
                dispatchCode = vcom.IsInSMC3Testing ? DispatchCodes.Test : DispatchCodes.Create,
                equipmentID = null,
                equipmentType = null,
                grossWeight = Math.Ceiling(shipment.Items.TotalActualWeight()).ToInt().GetString(),
                grossWeightUnit = "LBS",
                identifiers = null,
                originEmail = originContact.Email.EmptyStringToNull(),
                originAccount = null,
                originAddress1 = shipment.Origin.Street1,
                originAddress2 = shipment.Origin.Street2.EmptyStringToNull(),
                originCity = shipment.Origin.City,
                originCloseTime = DateTime.Now.SetTime(shipment.LatePickup).ToSmc3EvaTime(),
                originCompanyName = null,
                originCountry = shipment.Origin.Country.Code,
                originName = shipment.Origin.Description.EmptyStringToNull(),
                originOpenTime = DateTime.Now.SetTime(shipment.EarlyPickup).ToSmc3EvaTime(),
                originPhone = originContact.Phone.FormattedSmc3EvaPhone(),
                originPostalCode = shipment.Origin.PostalCode,
                originState = shipment.Origin.State,
                paymentTerms = null,
                pickupCallRequired = hasPickup ? CallRequiredValues.Yes : CallRequiredValues.No,
                pickupCloseTime = hasPickup ? shipment.Origin.AppointmentDateTime.ToSmc3EvaTime() : DateTime.Now.SetTime(shipment.LatePickup).ToSmc3EvaTime(),
                pickupContactEmail = shipment.User.Email,
                pickupContactName = shipment.User.FullName,
                pickupContactPhone = shipment.User.Phone.FormattedSmc3EvaPhone(),
                pickupDate = hasPickup ? shipment.Origin.AppointmentDateTime.ToSmc3EvaDate() : shipment.DesiredPickupDate.ToSmc3EvaDate(),
                pickupReadyTime = hasPickup ? shipment.Origin.AppointmentDateTime.ToSmc3EvaTime() : DateTime.Now.SetTime(shipment.EarlyPickup).ToSmc3EvaTime(),
                pickupRequester = null,
                pickupSpecialNotes = shipment.Origin.SpecialInstructions.EmptyStringToNull(),
                scac = null,
                serviceCode = null,
                shipmentCommodity = null,
                shipmentCommodityFreightCodes = null,
                shipmentID = shipment.ShipmentNumber,
            };
            return dispatchRequest;
        }
	}
}
