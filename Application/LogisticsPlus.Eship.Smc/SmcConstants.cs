﻿namespace LogisticsPlus.Eship.Smc
{
	public static class SmcConstants
	{
		public const string UsaCountryKey = "USA";
		public const string MexCountryKey = "MEX";
		public const string CadCountryKey = "CAN";

		public const string PostMethod = "POST";
		public const string JsonContentType = "application/json";
		public const string PlainContentType = "text/plain";
	}
}
