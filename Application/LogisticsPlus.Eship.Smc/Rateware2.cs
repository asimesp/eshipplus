﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Smc.Rates;
using LogisticsPlus.Eship.Smc.SmcCCXL;
using LogisticsPlus.Eship.Smc.SmcRatewareXL;

namespace LogisticsPlus.Eship.Smc
{
	public class Rateware2 : IDisposable
	{
		private const string Direct = "DIRECT";
		private const string SuccessStatus = "0";
		private const string ServiceDateFormat = "yyyyMMdd";
		private const string TransitFunctionOnlyErrMsg = "Operation is invalid as rater configured for transit time function only";

		private RateWareXL _rateware;
		private CarrierConnectXL _carrierConnect;

		public Rateware2(SmcServiceSettings ratewareSettings, SmcServiceSettings carrierConnectSettings)
		{
			SetRatewareSettings(ratewareSettings);
			SetCarrierConnectSettings(carrierConnectSettings);
		}

		public Rateware2(SmcServiceSettings carrierConnectSettings)
		{
			SetCarrierConnectSettings(carrierConnectSettings);
		}

		public Dictionary<string, RateResponse> GetShippingRates2(Dictionary<string, RateRequest> requests)
		{
			if (_rateware == null)
				throw new InvalidOperationException(TransitFunctionOnlyErrMsg);

			// build smc freight class requests
			var freightClassRequests = requests.Values
				.Where(r => r.RateBasis == DiscountTierRateBasis.FreightClass)
				.Select(r => new LTLRateShipmentRequest
				{
					destinationCountry = r.DestinationCountry,
					destinationPostalCode = r.DestinationPostalCode,
					details = r.Items
						.Select(i => new LTLRequestDetail { nmfcClass = i.NmfcClass, weight = i.Weight })
						.ToArray(),
					originCountry = r.OriginCountry,
					originPostalCode = r.OriginPostalCode,
					shipmentID = r.ShipmentId,
					tariffName = TariffNameFormatter.ParseTariffName(r.LTLTariff),
					shipmentDateCCYYMMDD = TariffNameFormatter.ParseTariffDate(r.LTLTariff)
						.ToString(ServiceDateFormat),
					stopAlternationWeight = r.StopAlternationWeight
				})
				.ToArray();

			// build smc freight class requests
			var densityClassRequests = requests.Values
				.Where(r => r.RateBasis == DiscountTierRateBasis.Density)
				.Select(r => new DensityRateShipmentRequest
				{
					destinationCountry = r.DestinationCountry,
					destinationPostalCode = r.DestinationPostalCode,
					details = r.Items
						.Select(i => new DensityRequestDetail
						{
							weight = i.Weight,
							length = i.Length,
							width = i.Width,
							height = i.Height,
							dimensionUnits = "I",
							weightUnits = "L",
							pieces = 1.ToString()
						})
						.ToArray(),
					detailType = "LWH",
					originCountry = r.OriginCountry,
					originPostalCode = r.OriginPostalCode,
					shipmentID = r.ShipmentId,
					tariffName = TariffNameFormatter.ParseTariffName(r.LTLTariff),
					shipmentDateCCYYMMDD = TariffNameFormatter.ParseTariffDate(r.LTLTariff)
						.ToString(ServiceDateFormat),
					stopAlternationWeight = r.StopAlternationWeight
				})
				.ToArray();

			// get responses
			var responses = new List<RateResponse>();
			if (freightClassRequests.Any())
			{
				var freightClassResponses = _rateware.LTLRateShipmentMultiple(freightClassRequests);
				responses.AddRange(freightClassResponses
									.Where(r => r.errorCode.Equals(SuccessStatus))
									.Select(r => new RateResponse
									{
										BilledWeight = float.Parse(r.billedWgt),
										LineHaulCharge = decimal.Parse(r.totalCharge),
										ShipmentId = r.shipmentID,
										DirectPointRate = false,
										TransitDays = Rate.DefaultTransitTime
									}));
			}
			if (densityClassRequests.Any())
			{
				var densityResponses = _rateware.DensityRateShipmentMultiple(densityClassRequests);
				responses.AddRange(densityResponses
									.Where(r => r.errorCode.Equals(SuccessStatus))
									.Select(r => new RateResponse
									{
										BilledWeight = float.Parse(r.billedWgt),
										LineHaulCharge = decimal.Parse(r.totalCharge),
										ShipmentId = r.shipmentID,
										DirectPointRate = false,
										TransitDays = Rate.DefaultTransitTime
									}));
			}

			// get transit times
			var transitRequests = requests
				.Values
				.Select(i => new
					{
						Key = string.Format("{0}-{1}-{2}-{3}-{4}-{5}",
											i.OriginPostalCode, i.OriginCountry, i.DestinationPostalCode, i.DestinationCountry,
											i.LatePickup, i.EarlyPickup),
						Request = i
					})
				.GroupBy(i => i.Key)
				.Select(i => new { i.Key, Values = i.Select(x => x.Request).ToList() })
				.Select(i => new TransitWithShipmentDateWindowRequest
					{
						carriers = i
								 .Values
								 .Select(x => new { x.Scac, x.Mode })
								 .Distinct()
								 .Select(x => new CarrierMethodService
									 {
										 SCAC = x.Scac,
										 method = x.Mode == ServiceMode.LessThanTruckload
													  ? Method.LTL
													  : x.Mode == ServiceMode.Truckload ? Method.TL : Method.ALL_AVAILABLE
									 })
								 .ToArray(),
						shipmentId = DateTime.Now.ToString("yyyyMMddHHmmss"),
						origin = new Locale
							{
								postalCode = i.Values[0].OriginPostalCode,
								countryCode = i.Values[0].OriginCountry.GetSmcTerminalInfoCountry()
							},
						destination = new Locale
							{
								postalCode = i.Values[0].DestinationPostalCode,
								countryCode = i.Values[0].DestinationCountry.GetSmcTerminalInfoCountry()
							},
						pickUpWindow = new SmcCCXL.TransitTimeWindow
							{
								upperBounds = i.Values[0].LatePickup,
								lowerBounds = i.Values[0].EarlyPickup
							}
					})
				.ToList();

			var transitResponses = new List<TransitWithShipmentDateWindowResponse>();
			foreach (var transitRequest in transitRequests)
				try
				{
					transitResponses.Add(_carrierConnect.TransitWithShipmentDateWindow(transitRequest));
				}
				catch
				{
					// do nothing
				}

			// process transit times
			foreach (var response in responses)
				if (requests.ContainsKey(response.ShipmentId)) // have experienced situation where response key is not present in requests dictionary - SMC3???
				{
					var request = requests[response.ShipmentId];
					var method = request.Mode == ServiceMode.LessThanTruckload
									 ? Method.LTL
									 : request.Mode == ServiceMode.Truckload ? Method.TL : Method.ALL_AVAILABLE;
					var transitResponse = transitResponses
						.FirstOrDefault(r => r.origin.postalCode == request.OriginPostalCode
						                     && r.origin.countryCode == request.OriginCountry.GetSmcTerminalInfoCountry()
						                     && r.destination.postalCode == request.DestinationPostalCode
						                     && r.destination.countryCode == request.DestinationCountry.GetSmcTerminalInfoCountry()
						                     && r.carriers.Any(c => c.SCAC == request.Scac
						                                            && c.method == method
						                                            && c.statusCode.status == Status.PASS
						                                            && c.serviceDetail.code.ToLower() == request.Smc3ServiceLevel.ToLower()
						                                            && (
							                                               request.IndirectPoint
							                                               || (!request.IndirectPoint && c.serviceDetail.service != null
							                                                   && c.serviceDetail.service.Value.ToString() == Direct
							                                                   && c.serviceDetail.destination != null
							                                                   && c.serviceDetail.destination.Value.ToString() == Direct
							                                                   && c.serviceDetail.origin != null
							                                                   && c.serviceDetail.origin.Value.ToString() == Direct)
						                                               )
							                        ));
					if (transitResponse != null)
					{
						var transitTime = transitResponse
							.carriers
							.Where(r => r.SCAC == request.Scac
							            && r.method == method
							            && r.statusCode.status == Status.PASS
							            && r.serviceDetail.code.ToLower() == request.Smc3ServiceLevel.ToLower())
							.FirstOrDefault(r => request.IndirectPoint ||
							                     (!request.IndirectPoint && r.serviceDetail.service != null &&
							                      r.serviceDetail.service.Value.ToString() == Direct &&
							                      r.serviceDetail.destination != null &&
							                      r.serviceDetail.destination.Value.ToString() == Direct &&
							                      r.serviceDetail.origin != null &&
							                      r.serviceDetail.origin.Value.ToString() == Direct));
						if (transitTime != null)
						{
							response.OriginTerminalCode = transitTime.terminalCodes.origin;
							response.DestinationTerminalCode = transitTime.terminalCodes.destination;
							response.TransitDays = transitTime.minimumDays;
							response.EstimatedDeliveryDate = transitTime.minimumCalendarDays.deliveryDate.GetValueOrDefault();
							response.DirectPointRate = transitTime.serviceDetail.service != null &&
													   transitTime.serviceDetail.service.Value.ToString() == Direct &&
													   transitTime.serviceDetail.destination != null &&
													   transitTime.serviceDetail.destination.Value.ToString() == Direct &&
													   transitTime.serviceDetail.origin != null &&
													   transitTime.serviceDetail.origin.Value.ToString() == Direct;
						}
					}
					else
					{
						response.TransitDays = Rate.DefaultTransitTime;
						response.EstimatedDeliveryDate = DateUtility.SystemEarliestDateTime;
						response.DirectPointRate = false;
					}
				}
				else
				{
					response.TransitDays = Rate.DefaultTransitTime;
					response.EstimatedDeliveryDate = DateUtility.SystemEarliestDateTime;
					response.DirectPointRate = false;
				}


			return responses.ToDictionary(r => r.ShipmentId, r => r);
		}

		public List<TransitDaysInfo> GetTransitDays(List<TransitDaysRequest> requests)
		{
			var transitRequests = requests
				.Select(i => new
					{
						Key = string.Format("{0}-{1}-{2}-{3}-{4}-{5}",
											i.OriginPostalCode, i.OriginCountry, i.DestinationPostalCode, i.DestinationCountry,
											i.LatePickup, i.EarlyPickup),
						Request = i
					})
				.GroupBy(i => i.Key)
				.Select(i => new { i.Key, Values = i.Select(x => x.Request).ToList() })
				.Select(i => new TransitWithShipmentDateWindowRequest
					{
						carriers = i.Values
									.Select(x => new CarrierMethodService
										{
											SCAC = x.Scac,
											method = x.Mode == ServiceMode.LessThanTruckload
														 ? Method.LTL
														 : x.Mode == ServiceMode.Truckload ? Method.TL : Method.ALL_AVAILABLE
										})
									.ToArray(),
						shipmentId = DateTime.Now.ToString("yyyyMMddHHmmss"),
						origin = new Locale
							{
								postalCode = i.Values[0].OriginPostalCode,
								countryCode = i.Values[0].OriginCountry.GetSmcTerminalInfoCountry()
							},
						destination = new Locale
							{
								postalCode = i.Values[0].DestinationPostalCode,
								countryCode = i.Values[0].DestinationCountry.GetSmcTerminalInfoCountry()
							},
						pickUpWindow = new SmcCCXL.TransitTimeWindow
							{
								upperBounds = i.Values[0].LatePickup,
								lowerBounds = i.Values[0].EarlyPickup
							}
					})
				.ToList();

			var transitResponses = new List<TransitWithShipmentDateWindowResponse>();
			foreach (var transitRequest in transitRequests)
				try
				{
					transitResponses.Add(_carrierConnect.TransitWithShipmentDateWindow(transitRequest));
				}
				catch
				{
					// do nothing
				}

			var infos = new List<TransitDaysInfo>();
			foreach (var request in requests)
			{
				var method = request.Mode == ServiceMode.LessThanTruckload
									 ? Method.LTL
									 : request.Mode == ServiceMode.Truckload ? Method.TL : Method.ALL_AVAILABLE;
				var transitResponse = transitResponses
					.FirstOrDefault(r => r.origin.postalCode == request.OriginPostalCode
										 && r.origin.countryCode == request.OriginCountry.GetSmcTerminalInfoCountry()
										 && r.destination.postalCode == request.DestinationPostalCode
										 && r.destination.countryCode == request.DestinationCountry.GetSmcTerminalInfoCountry()
										 && r.carriers.Any(c => c.SCAC == request.Scac
																&& c.method == method
																&& c.statusCode.status == Status.PASS
																&& c.serviceDetail.code.ToLower() == request.Smc3ServiceLevel.ToLower()
																&& (
																	   request.IndirectPoint
																	   || (!request.IndirectPoint && c.serviceDetail.service != null
																		   && c.serviceDetail.service.Value.ToString() == Direct
																		   && c.serviceDetail.destination != null
																		   && c.serviceDetail.destination.Value.ToString() == Direct
																		   && c.serviceDetail.origin != null
																		   && c.serviceDetail.origin.Value.ToString() == Direct)
																   )
												));

				if (transitResponse != null)
				{
					var transitTime = transitResponse
						.carriers
						.Where(r => r.SCAC == request.Scac
									&& r.method == method
									&& r.statusCode.status == Status.PASS
									&& r.serviceDetail.code.ToLower() == request.Smc3ServiceLevel.ToLower())
						.FirstOrDefault(r => request.IndirectPoint
											 || (!request.IndirectPoint
												 && r.serviceDetail.service != null
												 && r.serviceDetail.service.Value.ToString() == Direct
												 && r.serviceDetail.destination != null
												 && r.serviceDetail.destination.Value.ToString() == Direct
												 && r.serviceDetail.origin != null
												 && r.serviceDetail.origin.Value.ToString() == Direct));
					if (transitTime != null)
					{
						infos.Add(new TransitDaysInfo
							{
								OriginTerminalCode = transitTime.terminalCodes.origin,
								DestinationTerminalCode = transitTime.terminalCodes.destination,
								TransitDays = transitTime.minimumDays,
								EstimatedDeliveryDate = transitTime.minimumCalendarDays.deliveryDate.GetValueOrDefault(),
								DirectPointTransit = transitTime.serviceDetail.service != null &&
													 transitTime.serviceDetail.service.Value.ToString() == Direct &&
													 transitTime.serviceDetail.destination != null &&
													 transitTime.serviceDetail.destination.Value.ToString() == Direct &&
													 transitTime.serviceDetail.origin != null &&
													 transitTime.serviceDetail.origin.Value.ToString() == Direct,
								Key = request.Key
							});
					}
				}
			}

			return infos;
		}

		public List<string> ListAvailableTariffs()
		{
			if (_rateware == null)
				throw new InvalidOperationException(TransitFunctionOnlyErrMsg);

			return _rateware.AvailableTariffs()
				.Select(t => TariffNameFormatter.BuildTariffNameDate(t.tariffName, t.effectiveDate))
				.Distinct()
				.ToList();
		}

		public List<TariffDetails> ListAvailableTariffsWithDetails()
		{
			if (_rateware == null)
				throw new InvalidOperationException(TransitFunctionOnlyErrMsg);

			return _rateware.AvailableTariffs()
				.Select(t => new TariffDetails
								{
									TariffName = t.tariffName,
									Description = t.description,
									EffectiveDate = t.effectiveDate,
									ProductNumber = t.productNumber,
									Release = t.release
								})
				.ToList();
		}


		public List<TariffAnalysisResponse> ProcessTariffRateAnalysis(IEnumerable<string> tariffs, TariffAnalysisRequest request, DiscountTierRateBasis rateBasis)
		{
			if (_rateware == null)
				throw new InvalidOperationException(TransitFunctionOnlyErrMsg);

			List<TariffAnalysisResponse> results;

			switch (rateBasis)
			{
				case DiscountTierRateBasis.Density:

					var drq = tariffs
						.Select(t => request.ToDensityRateShipmentRequest(ServiceDateFormat, t))
						.ToArray();
					var drp = drq.Any() ? _rateware.DensityRateShipmentMultiple(drq) : new DensityRateShipmentResponse[0];
					results = drp.Select(TariffAnalysisResponse.NewResponse).ToList();
					break;
				case DiscountTierRateBasis.FreightClass:

					var frq = tariffs
						.Select(t => request.ToLTLRateShipmentRequest(ServiceDateFormat, t))
						.ToArray();
					var frp = frq.Any() ? _rateware.LTLRateShipmentMultiple(frq) : new LTLRateShipmentResponse[0];
					results = frp.Select(TariffAnalysisResponse.NewResponse).ToList();
					break;
				default:
					results = new List<TariffAnalysisResponse>();
					break;
			}

			return results;
		}

		public List<TariffAnalysisResponse> ProcessTariffRateAnalysis(string tariff, IEnumerable<TariffAnalysisRequest> requests, DiscountTierRateBasis rateBasis)
		{
			if (_rateware == null)
				throw new InvalidOperationException(TransitFunctionOnlyErrMsg);

			List<TariffAnalysisResponse> results;

			switch (rateBasis)
			{
				case DiscountTierRateBasis.Density:

					var drq = requests
						.Select(r => r.ToDensityRateShipmentRequest(ServiceDateFormat, tariff))
						.ToArray();
					var drp = drq.Any() ? _rateware.DensityRateShipmentMultiple(drq) : new DensityRateShipmentResponse[0];
					results = drp.Select(TariffAnalysisResponse.NewResponse).ToList();
					break;
				case DiscountTierRateBasis.FreightClass:

					var frq = requests
						.Select(r => r.ToLTLRateShipmentRequest(ServiceDateFormat, tariff))
						.ToArray();
					var frp = frq.Any() ? _rateware.LTLRateShipmentMultiple(frq) : new LTLRateShipmentResponse[0];
					results = frp.Select(TariffAnalysisResponse.NewResponse).ToList();
					break;
				default:
					results = new List<TariffAnalysisResponse>();
					break;
			}

			return results;
		}


		public void Dispose()
		{
			if (_rateware != null) _rateware.Dispose();
			_carrierConnect.Dispose();
		}

		private void SetRatewareSettings(SmcServiceSettings settings)
		{
			var authenticationTokenValue = new SmcRatewareXL.AuthenticationToken
			{
				username = settings.UserId,
				password = settings.Password,
				licenseKey = settings.License
			};
			_rateware = settings.IsSet
						? new RateWareXL { Url = settings.Uri, AuthenticationTokenValue = authenticationTokenValue }
						: new RateWareXL();
		}

		private void SetCarrierConnectSettings(SmcServiceSettings settings)
		{
			var authenticationTokenValue = new SmcCCXL.AuthenticationToken
				{
					username = settings.UserId,
					password = settings.Password,
					licenseKey = settings.License
				};
			_carrierConnect = settings.IsSet
								? new CarrierConnectXL { Url = settings.Uri, AuthenticationTokenValue = authenticationTokenValue }
								: new CarrierConnectXL();
		}

		internal static string ErrorCodeDescription(string errorCode)
		{
			switch (errorCode)
			{
				case "-1":
					return "Fatal Error: Check the log file for details";
				case "-2":
					return "Warning. Turn on loggin for details";
				case "0":
					return string.Empty; // no error
				case "1":
					return "Unknown Error";
				case "2":
					return "Open File Error";
				case "3":
					return "Read Fiel Error";
				case "4":
					return "Memory Allocation Error";
				case "5":
					return "Invalid OrgDestToGateWayPoint Flag";
				case "6":
					return "Class Not Found";
				case "7":
					return "Invalid Weight";
				case "8":
					return "Maximum Weight Exceeded";
				case "9":
					return "Invalid Discount";
				case "10":
					return "Invalid Origin Postal Code";
				case "11":
					return "Origin Postal Code not in Tariff";
				case "12":
					return "Invalid Destination Postal Code";
				case "13":
					return "Destination Postal Code not in Tariff";
				case "14":
					return "Origin/Destination Combination not in Tariff";
				case "15":
					return "Tariff name must be Specified";
				case "17":
					return "Invalid Shipment Date";
				case "21":
					return "Ratebase Number not Found";
				case "22":
					return "Maximum Number of lane adjustment hits exceeded";
				case "23":
					return "No GateWay Flag";
				case "24":
					return "Invalid country code/postal code combination";
				case "25":
					return "Lane adjustments process error";
				case "26":
					return "Unable to open log file";
				case "27":
					return "No detail lines found for this shipment";
				case "28":
					return "Invalid origin country";
				case "29":
					return "Invalid destination country";
				case "30":
					return "Invalid single shipment, flag must Y/N";
				case "31":
					return "More than 100 details lines found for this shipment";
				case "32":
					return "Tariff not found";
				case "33":
					return "Invalid rate adjustment factor";
				case "34":
					return "Invalid use discount, flag must be Y/N";
				case "35":
					return "Invalid discount application, flag must be R/C";
				case "36":
					return "Invalid MC floor";
				case "37":
					return "Invalid LTL surcharge percentage";
				case "38":
					return "Invalid TL surcharge percentage";
				case "39":
					return "Invalid surcharge application, flag must be G/N";
				case "40":
					return "Invalid stop alternation weight (weight break)";
				case "41":
					return "Invalid Shipment ID";
				case "42":
					return "Currupt File";
				case "44":
					return "Invalid director version";
				case "45":
					return "Invalid state/postal code combination";
				case "46":
					return "Density not found";
				case "47":
					return "Invalid Type";
				case "48":
					return "Invalid pieces";
				case "49":
					return "Invalid density units";
				case "50":
					return "Invalid dimension Units";
				case "51":
					return "Invalid weight Units";
				case "52":
					return "Invlaid cube";
				case "53":
					return "Invalid length";
				case "54":
					return "Invalid width";
				case "55":
					return "Invalid height";
				case "56":
					return "Invalid dimming density";
				case "57":
					return "Invalid dimming factor";
				case "58":
					return "Invalid dimming units";
				case "59":
					return "Invalid use dimming minimum charg flag";
				default:
					return "Unlisted Error";
			}
		}
	}
}
