﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Smc.Carriers;
using LogisticsPlus.Eship.Smc.SmcCCXL;
using Country = LogisticsPlus.Eship.Smc.SmcCCXL.Country;

namespace LogisticsPlus.Eship.Smc
{
	public class CarrierConnect : IDisposable
	{
		private const string StandardLTL = "Standard-LTL";
		private const string Direct = "DIRECT";

		private readonly IDictionary<Country, Core.Registry.Country> _countryMap = new Dictionary<Country, Core.Registry.Country>();
		private CarrierConnectXL _carrierConnect;

		public CarrierConnect(SmcServiceSettings carrierConnectSettings)
		{
			SetCarrierConnectSettings(carrierConnectSettings);
		}

		public CarrierConnect(SmcServiceSettings carrierConnectSettings, IEnumerable<Core.Registry.Country> countries)
			: this(carrierConnectSettings)
		{
			_countryMap = new Dictionary<Country, Core.Registry.Country>();
			foreach (var country in countries)
			{
				var key = country.GetSmcTerminalInfoCountry();
				if (!_countryMap.ContainsKey(key)) _countryMap.Add(key, country);
			}
		}

		public List<Carrier> GetAvailableCarriers(DateTime? shipmentDate = null)
		{
			var request = new AvailableCarriersRequest
				{
					serviceMethod = Method.ALL_AVAILABLE,
					shipmentDate = shipmentDate
				};
			var response = _carrierConnect.AvailableCarriers(request);

			var carriers = response
				.carriers
				.Select(c => new Carrier
					{
						Name = c.name,
						Scac = c.scac,
						Services = c.services
									.Select(s => new CarrierService
										{
											ServiceCode = s.serviceCode,
											ServiceMethod = s.serviceMethod.FormattedString()
										})
									.ToList()
					})
				.ToList();

			return carriers;
		}

		public List<Terminal> GetCarrierTerminals(string scac, DateTime? shipmentDate = null)
		{
			var request = new GetTerminalsRequest
				{
					scacList = new[] { scac },
					shipmentDate = shipmentDate
				};

			var terminals = _carrierConnect
				.TerminalsByCarrier(request)
				.Where(r => r.statusCode != null && r.statusCode.status == Status.PASS)
				.SelectMany(r => r.terminalDetails
								  .Select(t => new Terminal
									  {
										  CarrierName = r.carrierName.GetString(),
										  Scac = r.SCAC.GetString(),
										  Method = r.method.FormattedString(),
										  Address1 = t.address1.GetString(),
										  Address2 = t.address2.GetString(),
										  City = t.city.GetString(),
										  Code = t.code.GetString(),
										  ContactEmail = t.contact == null ? string.Empty : t.contact.email.GetString(),
										  ContactFax = t.contact == null ? string.Empty : t.contact.fax.GetString(),
										  ContactName = t.contact == null ? string.Empty : t.contact.name.GetString(),
										  ContactPhone = t.contact == null ? string.Empty : t.contact.phone.GetString(),
										  ContactTitle = t.contact == null ? string.Empty : t.contact.title.GetString(),
										  ContactTollFree = t.contact == null ? string.Empty : t.contact.tollFree.GetString(),
										  CountryCode = t.countryCode.GetString(),
										  TerminalName = t.terminalName.GetString(),
										  PostalCode = t.postalCode.GetString(),
										  PostalCodePlus4 = t.postalCodePlus4.GetString(),
										  State = t.stateProvince.GetString(),
									  }))
				.ToList();

			return terminals;
		}

		public LTLTerminalInfo GetLTLTerminalInfo(string scac, string terminalCode, DateTime? shipmentDate = null)
		{
			var list = GetLTLTerminalInfo(scac, new[] { terminalCode }, shipmentDate);

			return list.FirstOrDefault();
		}

		public List<LTLTerminalInfo> GetLTLTerminalInfo(string scac, string[] terminalCodes, DateTime? shipmentDate = null)
		{
			var responses = _carrierConnect.TerminalByTerminalCode(new TerminalRequest { shipmentDate = shipmentDate, code = terminalCodes, scac = scac });

			return responses
				.Where(r => r.statusCode != null && r.statusCode.status == Status.PASS && r.SCAC == scac && r.method == Method.LTL)
				.SelectMany(r => r.terminalDetails)
				.Where(t => terminalCodes.Contains(t.code))
				.Select(t => new LTLTerminalInfo
					{
						Name = t.terminalName.GetString(),
						Country = _countryMap.ContainsKey(t.countryCode) ? _countryMap[t.countryCode] : null,
						State = t.stateProvince.GetString(),
						Street1 = t.address1.GetString().Truncate(50),
						Street2 = t.address2.GetString().Truncate(50),
						City = t.city.GetString().Truncate(50),
						Code = t.code.GetString(),
						ContactName = t.contact == null ? string.Empty : t.contact.name.GetString(),
						ContactTitle = t.contact == null ? string.Empty : t.contact.title.GetString(),
						Phone = t.contact == null ? string.Empty : t.contact.phone.GetString(),
						Fax = t.contact == null ? string.Empty : t.contact.fax.GetString(),
						Email = t.contact == null ? string.Empty : t.contact.email.GetString(),
						PostalCode = t.postalCode.GetString(),
						TollFree = t.contact == null ? string.Empty : t.contact.tollFree.GetString(),
					})
				.ToList();
		}

		public bool SetPrimaryVendorLTLTerminalInfo(Shipment shipment, bool indirectPointsAllowed = false)
		{
			var pv = shipment.Vendors.FirstOrDefault(v => v.Primary);
			if (pv == null) return false;

			var method = shipment.ServiceMode == ServiceMode.LessThanTruckload
							 ? Method.LTL
							 : shipment.ServiceMode == ServiceMode.Truckload ? Method.TL : Method.ALL_AVAILABLE;

			var transitRequest = new TransitWithShipmentDateWindowRequest
				{
					carriers = new[] { new CarrierMethodService { SCAC = pv.Vendor.Scac, method = method } },
					shipmentId = shipment.ShipmentNumber,
					origin = new Locale
						{
							postalCode = shipment.Origin.PostalCode,
							countryCode = shipment.Origin.Country.GetSmcTerminalInfoCountry()
						},
					destination = new Locale
						{
							postalCode = shipment.Destination.PostalCode,
							countryCode = shipment.Destination.Country.GetSmcTerminalInfoCountry()
						},
					pickUpWindow = new TransitTimeWindow
						{
							upperBounds = shipment.DesiredPickupDate.SetTime(shipment.LatePickup),
							lowerBounds = shipment.DesiredPickupDate.SetTime(shipment.EarlyPickup)
						}
				};


			try
			{
				var transitResponse = _carrierConnect.TransitWithShipmentDateWindow(transitRequest);
				var transitTime = transitResponse
					.carriers
					.Where(r => r.SCAC == pv.Vendor.Scac
								&& r.method == method
								&& r.statusCode.status == Status.PASS
								&& r.serviceDetail.code == StandardLTL)
					.FirstOrDefault(r => indirectPointsAllowed
										 || (r.serviceDetail.service != null
											 && r.serviceDetail.service.Value.ToString() == Direct
											 && r.serviceDetail.destination != null
											 && r.serviceDetail.destination.Value.ToString() == Direct
											 && r.serviceDetail.origin != null
											 && r.serviceDetail.origin.Value.ToString() == Direct));

				if (transitTime == null)
				{
					shipment.OriginTerminal = null;
					shipment.DestinationTerminal = null;
					return false;
				}

				var terminalCodes = new[] { transitTime.terminalCodes.origin, transitTime.terminalCodes.destination };
				var terminals = GetLTLTerminalInfo(pv.Vendor.Scac, terminalCodes);

				shipment.OriginTerminal = terminals.FirstOrDefault(t => t.Code == transitTime.terminalCodes.origin);
				if (shipment.OriginTerminal != null)
				{
					shipment.OriginTerminal.Shipment = shipment;
					shipment.OriginTerminal.TenantId = shipment.TenantId;
					shipment.OriginTerminal.CountryId = shipment.Origin.CountryId;
				}

				shipment.DestinationTerminal = terminals.FirstOrDefault(t => t.Code == transitTime.terminalCodes.destination);
				if (shipment.DestinationTerminal != null)
				{
					shipment.DestinationTerminal.Shipment = shipment;
					shipment.DestinationTerminal.TenantId = shipment.TenantId;
					shipment.DestinationTerminal.CountryId = shipment.Destination.CountryId;
				}
			}
			catch
			{
				shipment.OriginTerminal = null;
				shipment.DestinationTerminal = null;
				return false;
			}

			return shipment.OriginTerminal != null && shipment.DestinationTerminal != null;
		}

		public List<TerminalTransit> GetTerminalTransits(Terminal @from, List<Terminal> tos)
		{
			var transits = new List<TerminalTransit>();
			foreach (var to in tos)
				try
				{
					var request = new TransitWithShipmentDateWindowRequest
						{
							carriers = new[] { new CarrierMethodService { SCAC = @from.Scac, method = Method.ALL_AVAILABLE } },
							shipmentId = DateTime.Now.ToString("yyyyMMddHHmmss"),
							origin = new Locale
								{
									postalCode = @from.PostalCode,
									countryCode = @from.CountryCode.GetSmcTerminalInfoCountry()
								},
							destination = new Locale
								{
									postalCode = to.PostalCode,
									countryCode = to.CountryCode.GetSmcTerminalInfoCountry()
								},
							pickUpWindow = new TransitTimeWindow
								{
									upperBounds = DateTime.Now.SetTime(TimeUtility.DefaultClose),
									lowerBounds = DateTime.Now.SetTime(TimeUtility.DefaultOpen)
								}
						};
					var response = _carrierConnect.TransitWithShipmentDateWindow(request);
					if (response == null) continue;

					var carriers = response.carriers.Where(c => c.SCAC == @from.Scac && c.statusCode.status == Status.PASS).ToList();
					if (!carriers.Any()) continue;

					transits.AddRange(carriers.Select(c => new TerminalTransit
						{
							From = @from,
							To = to,
							Direct = c.serviceDetail != null
									 && c.serviceDetail.origin != null
									 && c.serviceDetail.origin.Value.GetString() == Direct
									 && c.serviceDetail.destination != null
									 && c.serviceDetail.destination.Value.GetString() == Direct
									 && c.serviceDetail.service != null
									 && c.serviceDetail.service.Value.GetString() == Direct,
							Method = c.method.FormattedString(),
							ServiceCode = c.serviceDetail != null ? c.serviceDetail.code.FormattedString() : string.Empty,
							MinimumDays = c.minimumDays,
							MaximumDays = c.maxiumDays,
							MinimumCalendarDays = c.minimumCalendarDays.calendarDays,
							MaximumCalendarDays = c.maximumCalendarDays.calendarDays,
							DataExpirationDate = c.dataGroup.expirationDate.ToDateTime(),
							DataReleaseDate = c.dataGroup.releaseDate.ToDateTime(),
							DataReleaseName = c.dataGroup.name.GetString()
						}));
				}
				catch
				{
					// do nothing
				}

			return transits;
		}

		public List<PointToPointService> GetPointToPointService(string originPostalCode, Core.Registry.Country originCountry, string destinationPostalCode, Core.Registry.Country destinationCountry)
		{
			var transits = new List<PointToPointService>();
			try
			{
				var availableCarriers = GetAvailableCarriers(DateTime.Now);
				var request = new TransitWithShipmentDateWindowRequest
					{
						carriers = availableCarriers.Select(c => new CarrierMethodService { SCAC = c.Scac, method = Method.ALL_AVAILABLE }).ToArray(),
						shipmentId = DateTime.Now.ToString("yyyyMMddHHmmss"),
						origin = new Locale
							{
								postalCode = originPostalCode,
								countryCode = originCountry.GetSmcTerminalInfoCountry()
							},
						destination = new Locale
							{
								postalCode = destinationPostalCode,
								countryCode = destinationCountry.GetSmcTerminalInfoCountry()
							},
						pickUpWindow = new TransitTimeWindow
							{
								upperBounds = DateTime.Now.SetTime(TimeUtility.DefaultClose),
								lowerBounds = DateTime.Now.SetTime(TimeUtility.DefaultOpen)
							}
					};

				var response = _carrierConnect.TransitWithShipmentDateWindow(request);
				if (response == null) return transits;

				var carriers = response.carriers.Where(c => c.statusCode.status == Status.PASS).ToList();
				if (!carriers.Any()) return transits;

				transits.AddRange(carriers.Select(c => new PointToPointService
					{
						OriginCountry = originCountry,
						OriginPostalCode = originPostalCode,
						DestinationCountry = destinationCountry,
						DestinationPostalCode = destinationPostalCode,
						Direct = c.serviceDetail != null
						         && c.serviceDetail.origin != null
						         && c.serviceDetail.origin.Value.GetString() == Direct
						         && c.serviceDetail.destination != null
						         && c.serviceDetail.destination.Value.GetString() == Direct
						         && c.serviceDetail.service != null
						         && c.serviceDetail.service.Value.GetString() == Direct,
						Method = c.method.FormattedString(),
						ServiceCode = c.serviceDetail != null ? c.serviceDetail.code.FormattedString() : string.Empty,
						MinimumDays = c.minimumDays,
						MaximumDays = c.maxiumDays,
						MinimumCalendarDays = c.minimumCalendarDays.calendarDays,
						MaximumCalendarDays = c.maximumCalendarDays.calendarDays,
						CarrierName = c.carrierName.GetString(),
						Scac = c.SCAC,
						DataExpirationDate = c.dataGroup.expirationDate.ToDateTime(),
						DataReleaseDate = c.dataGroup.releaseDate.ToDateTime(),
						DataReleaseName = c.dataGroup.name.GetString(),
						DestinationTerminalCode = c.terminalCodes.destination,
						OriginTerminalCode = c.terminalCodes.origin
					}));
			}
			catch
			{
				// do nothing
			}

			return transits;
		}

		public void Dispose()
		{
			_carrierConnect.Dispose();
		}


		private void SetCarrierConnectSettings(SmcServiceSettings settings)
		{
			var authenticationTokenValue = new AuthenticationToken
			{
				username = settings.UserId,
				password = settings.Password,
				licenseKey = settings.License
			};
			_carrierConnect = settings.IsSet
								? new CarrierConnectXL { Url = settings.Uri, AuthenticationTokenValue = authenticationTokenValue }
								: new CarrierConnectXL();
		}
	}
}
