﻿using System;
using Newtonsoft.Json;

namespace LogisticsPlus.Eship.Smc.Eva
{
	[Serializable]
	public class ShipmentCommodity
	{
		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string packagingType { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string pieces { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string classification { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string itemNumber { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string subNumber { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string density { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string densityUnit { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string length { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string width { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string height { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string dimensionUnits { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string weight { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string weightUnit { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string description { get; set; }
	}
}
