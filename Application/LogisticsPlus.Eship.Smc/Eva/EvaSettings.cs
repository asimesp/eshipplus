﻿namespace LogisticsPlus.Eship.Smc.Eva
{
	public class EvaSettings
	{
		public string BaseUri { get; set; }

		public string DispatchUri
		{
			get { return string.Format("{0}/dispatch", BaseUri); }
		}

		public string StatusUri
		{
			get { return string.Format("{0}/status", BaseUri); }
		}
	}
}
