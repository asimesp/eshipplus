﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace LogisticsPlus.Eship.Smc.Eva
{
	//NOTE: parameters names intentionally not set case sensitive from start.  Using JSON serializers so we need the name to remain this way to keep case sensitivity.

	[Serializable]
	public class TrackRequest
	{

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string accountToken { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string scac { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public List<ReferenceNumber> referenceNumbers { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string referenceType { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public List<EventType> eventTypes { get; set; }
	}
}
