﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web;
using Newtonsoft.Json;

namespace LogisticsPlus.Eship.Smc.Eva
{
	public class EvaServiceHandler
	{
		private const string ProcessingErr = "Processing Error: ";

		private readonly EvaSettings _settings;

		public List<string> Errors { get; private set; }


		private void ProcessWebException(WebException ex)
		{
			var errorResponse = ex.Response.ToHttpWebResponse();
			var responseStream = errorResponse.GetResponseStream();
			if (responseStream != null)
			{
				string xml;
				using (var reader = new StreamReader(responseStream))
					xml = reader.ReadToEnd();
				//var result = xml.FromXml<OrderResult>();
				//Errors.AddRange(result.Errors.Select(e => string.Format("{0}: {1}", e.Code, e.Message)));
				Errors.Add(xml);
			}
		}

		private EvaSynchronousResponse InitiateRequest(Uri uri, string json)
		{
			try
			{
				var request = WebRequest.Create(uri);
				request.Method = SmcConstants.PostMethod;
				request.ContentType = SmcConstants.JsonContentType;

				using (var writer = new StreamWriter(request.GetRequestStream()))
					writer.Write(json);

				var response = request.GetResponse();
				string responseData;
				var responseStream = response.GetResponseStream();
				if (responseStream == null)
				{
					Errors.Add("Error reading response stream!");
					return null;
				}
				using (var reader = new StreamReader(responseStream))
					responseData = reader.ReadToEnd();
				var result = JsonConvert.DeserializeObject<EvaSynchronousResponse>(responseData);
				switch (response.ToHttpWebResponse().StatusCode)
				{
					case HttpStatusCode.OK:
						return result;
					default:
						Errors.Add(string.Format("{0}: {1}", result.responseCode, result.responseMessage));
						return result;
				}

			}
			catch (WebException ex)
			{
				ProcessWebException(ex);
				return null;
			}
			catch (Exception ex)
			{
				Errors.Add(ex.ToString());
				return null;
			}
		}



		public EvaServiceHandler(EvaSettings settings)
		{
			if (settings == null)
				throw new ArgumentNullException("settings");

			_settings = settings;
			Errors = new List<string>();
		}



		public EvaSynchronousResponse InitiateTracking(TrackRequest trackRequest)
		{
			Errors.Clear();

			try
			{
				var json = JsonConvert.SerializeObject(trackRequest);
				return InitiateRequest(new Uri(_settings.StatusUri), json);
			}
			catch (Exception ex)
			{
				Errors.Add(ex.ToString());
				return null;
			}
		}

		public TrackingData ProcessTrackingUpdate(HttpRequest request, HttpResponse response)
		{
			try
			{
				string streamData;
				using (var reader = new StreamReader(request.InputStream))
					streamData = reader.ReadToEnd();

				var trackingData = JsonConvert.DeserializeObject<TrackingData>(streamData);

				response.StatusCode = HttpStatusCode.OK.ToInt();
				response.SendReponse(HttpStatusCode.OK.GetString());

				return trackingData;
			}
			catch (Exception e)
			{
				response.StatusCode = HttpStatusCode.BadRequest.ToInt();
				response.SendReponse(ProcessingErr + e.Message);
				Errors.Add(e.ToString());
				return null;
			}
		}



		public EvaSynchronousResponse InitiateDispatch(DispatchRequest dispatchRequest)
		{
			Errors.Clear();

			try
			{
				var json = JsonConvert.SerializeObject(dispatchRequest);
				return InitiateRequest(new Uri(_settings.DispatchUri), json);
			}
			catch (Exception ex)
			{
				Errors.Add(ex.ToString());
				return null;
			}
		}

		public DispatchData ProcessDispatchUpdate(HttpRequest request, HttpResponse response)
		{
			try
			{
				string streamData;
				using (var reader = new StreamReader(request.InputStream))
					streamData = reader.ReadToEnd();

				var dispatchData = JsonConvert.DeserializeObject<DispatchData>(streamData);

				response.StatusCode = HttpStatusCode.OK.ToInt();
				response.SendReponse(HttpStatusCode.OK.GetString());

				return dispatchData;
			}
			catch (Exception e)
			{
				response.StatusCode = HttpStatusCode.BadRequest.ToInt();
				response.SendReponse(ProcessingErr + e.Message);
				Errors.Add(e.ToString());
				return null;
			}
		}
	}
}
