﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace LogisticsPlus.Eship.Smc.Eva
{
	[Serializable]
	public class ShipmentCommodityFreightCode
	{

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string code { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public List<ShipmentCommodityFreightCodeDetail> details { get; set; }
	}
}
