﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace LogisticsPlus.Eship.Smc.Eva
{
	//NOTE: parameters names intentionally not set case sensitive from start.  Using JSON serializers so we need the name to remain this way to keep case sensitivity.

	public class DispatchRequest
	{
		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string accountToken { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string scac { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string serviceCode { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string dispatchCode { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string shipmentID { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public List<Identifier> identifiers { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string pickupCallRequired { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string pickupContactName { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string pickupContactPhone { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string pickupContactEmail { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string pickupRequester { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string pickupSpecialNotes { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string pickupDate { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string pickupReadyTime { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string pickupCloseTime { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string equipmentID { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string equipmentType { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string grossWeight { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string grossWeightUnit { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public List<ShipmentCommodity> shipmentCommodity { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public List<ShipmentCommodityFreightCode> shipmentCommodityFreightCodes { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string billToAccount { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string billToAddress1 { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string billToAddress2 { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string billToCity { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string billToCompanyName { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string billToCountry { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string billToName { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string billToPhone { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string billToState { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string billToPostalCode { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string billToEmail { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string paymentTerms { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationAccount { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationAddress1 { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationAddress2 { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationCity { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationCompanyName { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationCountry { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationName { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationPhone { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationState { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationPostalCode { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationEmail { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationOpenTime { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationCloseTime { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string originAccount { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string originAddress1 { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string originAddress2 { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string originCity { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string originCompanyName { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string originCountry { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string originName { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string originPhone { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string originState { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string originPostalCode { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string originEmail { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string originOpenTime { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string originCloseTime { get; set; }
	}
}
