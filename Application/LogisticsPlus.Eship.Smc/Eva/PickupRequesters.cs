﻿namespace LogisticsPlus.Eship.Smc.Eva
{
	public static class PickupRequesters
	{
		public const string Shipper = "Shipper";
		public const string Consignee = "Consignee";
		public const string ThirdParty = "Third Party";
	}
}
