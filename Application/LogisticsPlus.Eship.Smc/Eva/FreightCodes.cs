﻿namespace LogisticsPlus.Eship.Smc.Eva
{
	public static class FreightCodes
	{
		public const string AppointmentRequiredAtPickup = "APTP";
        public const string AppointmentRequiredAtDelivery = "APTD";
		public const string CollectOnDelivery = "COD";
		public const string ConstructionSiteDelivery = "CNSTD";
		public const string ConstructionSitePickup = "CNSTP";
		public const string TradeshowDelivery = "EBD";
		public const string TradeshowPickup = "EBP";
		public const string HazardousMaterial = "HAZ";
		public const string InsideDelivery = "IDL";
		public const string InBondShipment = "INBD";
        public const string AdditionalInsurance = "INS";
        public const string InsidePickup = "IPU";
        public const string LiftGateRequiredAtDelivery = "LFTD";
        public const string LiftGateRequiredAtPickup = "LFTP";
        public const string LimitedAccessDelivery = "LTDAD";
        public const string LimitedAccessPickup = "LTDAP";
        public const string MustNotifyConsignee = "MNC";
        public const string OverDimension = "OVR";
        public const string Perishables = "PPD";
        public const string ProtectiveFromCold = "PSC";
        public const string ProtectiveFromHeat = "PSH";
        public const string PoisonousMaterial = "PSN";
        public const string ResidentialDelivery = "RES";
        public const string ResidentialPickup = "REP";
        public const string SortAndSegregate = "SEG";
	}
}
