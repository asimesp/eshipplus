﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.UI;

namespace LogisticsPlus.Eship.Smc.Eva
{
    public abstract class EvaServiceProxyPageBase : Page
    {
        public abstract EvaSettings GetSettings();

        public abstract void QueTrackingDataAsCheckCalls(TrackingData data);
        public abstract void SaveEvaTrackingInformation(TrackingData data);
        public abstract void UpdateShipmentDispatchInformation(DispatchData data);
        public abstract void SaveDispatchInformation(DispatchData data);

        public abstract void LogErrors(List<string> errors);

        private const int AcceptedTrackingResponseStatusThreshold = 6;

        public EvaSynchronousResponse InitiateTracking(TrackRequest request)
        {
            var handler = new EvaServiceHandler(GetSettings());
            var response = handler.InitiateTracking(request);

            if (handler.Errors.Any()) LogErrors(handler.Errors);

            return response;
        }

        public void ProcessTrackingResponse()
        {
            var handler = new EvaServiceHandler(GetSettings());
            var data = handler.ProcessTrackingUpdate(Request, Response);

            if (handler.Errors.Any()) LogErrors(handler.Errors);

            var responseSuccessful = data.responseStatus == HttpStatusCode.OK.ToInt().GetString();


           // Grab the 200 Response Code if possible
           var responseCode = responseSuccessful
                                  ? data.responseCode.Split('-').Last().ToInt()
                                  : default(int);


           // If response code is 200-7 or higher, there is insufficient information to validate the BOL (Appendix D of main EVA doc)
           if (responseSuccessful && responseCode <= AcceptedTrackingResponseStatusThreshold) 
               QueTrackingDataAsCheckCalls(data);
           

            SaveEvaTrackingInformation(data);

        }


        public EvaSynchronousResponse InitiateDispatch(DispatchRequest request)
        {
            var handler = new EvaServiceHandler(GetSettings());
            var response = handler.InitiateDispatch(request);

            if (handler.Errors.Any()) LogErrors(handler.Errors);

            return response;
        }

        public void ProcessDispatchResponse()
        {
            var handler = new EvaServiceHandler(GetSettings());
            var data = handler.ProcessDispatchUpdate(Request, Response);

            if (handler.Errors.Any()) LogErrors(handler.Errors);

            if (data.responseStatus == HttpStatusCode.OK.ToInt().GetString()) UpdateShipmentDispatchInformation(data);
            SaveDispatchInformation(data);
        }
    }
}
