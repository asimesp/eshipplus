﻿using System;

namespace LogisticsPlus.Eship.Smc.Eva
{
	[Serializable]
	public class EvaSynchronousResponse
	{
		public string transactionID { get; set; }
		public string responseStatus { get; set; }
		public string responseCode { get; set; }
		public string responseMessage { get; set; }
	}
}
