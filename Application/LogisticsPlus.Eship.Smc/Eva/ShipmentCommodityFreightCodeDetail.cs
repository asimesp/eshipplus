﻿using System;
using Newtonsoft.Json;

namespace LogisticsPlus.Eship.Smc.Eva
{
	[Serializable]
	public class ShipmentCommodityFreightCodeDetail
	{

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string name { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string value { get; set; }
	}
}
