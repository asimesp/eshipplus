﻿namespace LogisticsPlus.Eship.Smc.Eva
{
	public static class EventTypes
	{
		public const string CurrentCarrier = "CR";
		public const string CarrierDistressed = "DS";
		public const string Delivery = "DY";
		public const string Transit = "TR";
		public const string CarrierShipment = "IO";
		public const string CarrierStatus = "UD";
		public const string All = "AL";
	}
}
