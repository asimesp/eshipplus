﻿using System;
using Newtonsoft.Json;

namespace LogisticsPlus.Eship.Smc.Eva
{
	//NOTE: parameters names intentionally not set case sensitive from start.  Using JSON serializers so we need the name to remain this way to keep case sensitivity.

	[Serializable]
	public class TrackingData
	{
		public string transactionID { get; set; }
		public string accountToken { get; set; }
		public string referenceNumber { get; set; }
		public string referenceType { get; set; }
		public string scac { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string pro { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string bol { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string po { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string statusCode { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string statusEventType { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string statusDate { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string statusDescription { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string statusTime { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string statusCity { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string statusCountry { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string statusStateProvince { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string statusPostalCode { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string appointmentScheduled { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string appointmentDate { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string appointmentTime { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string appointmentType { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string pickupDate { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string pickupTime { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string deliveryDate { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string deliveryTime { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string etaDestinationDate { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string etaDestinationTime { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string ccxlDestinationDate { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string cxclDestinationTime { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string ccxlCalendarDays { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string interlineContactPhone { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string interlinePartnerName { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string interlineNotes { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string interlinePro { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string interlineScac { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string originTerminalCode { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string originTerminalPhone { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string originTerminalFax { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string originTerminalTollFree { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string originTerminalEmail { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string originTerminalContactName { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string originTerminalCity { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string originTerminalCountry { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string originTerminalStateProvince { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string originTerminalPostalCode { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationTerminalCode { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationTerminalPhone { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationTerminalFax { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationTerminalTollFree { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationTerminalContactName { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string destinationTerminalEmail { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationTerminalCity { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationTerminalCountry { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationTerminalStateProvince { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationTerminalPostalCode { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string equipmentID { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string equipmentType { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string weight { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string weightType { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string weightUnit { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string pieces { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string packagingType { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string hazardousMaterial { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string billToAccount { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string billToAddress1 { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string billToAddress2 { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string billToCity { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string billToCountry { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string billToName { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string billToPhone { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string billToStateProvince { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string billToPostalCode { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string paymentTerms { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationAccount { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationAddress1 { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationAddress2 { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationCity { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationCountry { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationName { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationPhone { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationStateProvince { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string destinationPostalCode { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string originAccount { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string originAddress1 { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string originAddress2 { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string originCity { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string originCountry { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string originName { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string originPhone { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string originStateProvince { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string originPostalCode { get; set; }

		public string responseStatus { get; set; }
		public string responseCode { get; set; }
		public string responseMessage { get; set; }

		public TrackingData()
		{
			transactionID = string.Empty;
			accountToken = string.Empty;
			referenceNumber = string.Empty;
			referenceType = string.Empty;
			scac = string.Empty;
			pro = string.Empty;
			bol = string.Empty;
			po = string.Empty;
			statusCode = string.Empty;
			statusEventType = string.Empty;
			statusDate = string.Empty;
			statusDescription = string.Empty;
			statusTime = string.Empty;
			statusCity = string.Empty;
			statusCountry = string.Empty;
			statusStateProvince = string.Empty;
			statusPostalCode = string.Empty;
			appointmentScheduled = false.GetString();
			appointmentDate = string.Empty;
			appointmentTime = string.Empty;
			appointmentType = string.Empty;
			pickupDate = string.Empty;
			pickupTime = string.Empty;
			deliveryDate = string.Empty;
			deliveryTime = string.Empty;
			etaDestinationDate = string.Empty;
			etaDestinationTime = string.Empty;
			ccxlDestinationDate = string.Empty;
			cxclDestinationTime = string.Empty;
			ccxlCalendarDays = string.Empty;
			interlineContactPhone = string.Empty;
			interlinePartnerName = string.Empty;
			interlineNotes = string.Empty;
			interlinePro = string.Empty;
			interlineScac = string.Empty;
			originTerminalCode = string.Empty;
			originTerminalPhone = string.Empty;
			originTerminalFax = string.Empty;
			originTerminalTollFree = string.Empty;
			originTerminalContactName = string.Empty;
			originTerminalCity = string.Empty;
			originTerminalCountry = string.Empty;
			originTerminalStateProvince = string.Empty;
			originTerminalPostalCode = string.Empty;
			destinationTerminalCode = string.Empty;
			destinationTerminalPhone = string.Empty;
			destinationTerminalFax = string.Empty;
			destinationTerminalTollFree = string.Empty;
			destinationTerminalContactName = string.Empty;
			destinationTerminalCity = string.Empty;
			destinationTerminalCountry = string.Empty;
			destinationTerminalStateProvince = string.Empty;
			destinationTerminalPostalCode = string.Empty;
			equipmentID = string.Empty;
			equipmentType = string.Empty;
			weight = 0.GetString();
			weightType = string.Empty;
			weightUnit = string.Empty;
			pieces = 0.GetString();
			packagingType = string.Empty;
			hazardousMaterial = false.GetString();
			billToAccount = string.Empty;
			billToAddress1 = string.Empty;
			billToAddress2 = string.Empty;
			billToCity = string.Empty;
			billToCountry = string.Empty;
			billToName = string.Empty;
			billToPhone = string.Empty;
			billToStateProvince = string.Empty;
			billToPostalCode = string.Empty;
			paymentTerms = string.Empty;
			destinationAccount = string.Empty;
			destinationAddress1 = string.Empty;
			destinationAddress2 = string.Empty;
			destinationCity = string.Empty;
			destinationCountry = string.Empty;
			destinationName = string.Empty;
			destinationPhone = string.Empty;
			destinationStateProvince = string.Empty;
			destinationPostalCode = string.Empty;
			originAccount = string.Empty;
			originAddress1 = string.Empty;
			originAddress2 = string.Empty;
			originCity = string.Empty;
			originCountry = string.Empty;
			originName = string.Empty;
			originPhone = string.Empty;
			originStateProvince = string.Empty;
			originPostalCode = string.Empty;
			responseStatus = string.Empty;
			responseCode = string.Empty;
			responseMessage = string.Empty;
		}
	}
}