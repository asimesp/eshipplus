﻿using System;

namespace LogisticsPlus.Eship.Smc.Eva
{
	[Serializable]
	public class Identifier
	{
		public string referenceNumber { get; set; }
		public string referenceType { get; set; }
	}
}
