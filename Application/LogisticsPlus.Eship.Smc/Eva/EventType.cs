﻿using System;
using Newtonsoft.Json;

namespace LogisticsPlus.Eship.Smc.Eva
{
	//NOTE: parameters names intentionally not set case sensitive from start.  Using JSON serializers so we need the name to remain this way to keep case sensitivity.

	[Serializable]
	public class EventType
	{

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string eventType { get; set; }
	}
}
