﻿using System;
using System.Collections.Generic;

namespace LogisticsPlus.Eship.Smc.Eva
{
	[Serializable]
	public class DispatchData
	{
        // TODO EVA DISPATCH: Add accountToken
		public string transactionID { get; set; }
		public string scac { get; set; }
		public string pickupNumber { get; set; }
		public string barcodeNumber { get; set; }
		public string dateAccepted { get; set; }
		public List<Identifier> identifiers { get; set; }
		public string responseStatus { get; set; }
		public string responseCode { get; set; }
		public string responseMessage { get; set; }

		public DispatchData()
		{
			transactionID = string.Empty;
			scac = string.Empty;
			pickupNumber = string.Empty;
			barcodeNumber = string.Empty;
			dateAccepted = string.Empty;
			identifiers = null;
			responseStatus = string.Empty;
			responseCode = string.Empty;
			responseMessage = string.Empty;
		}
	}
}
