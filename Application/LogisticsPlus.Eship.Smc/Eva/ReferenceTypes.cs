﻿namespace LogisticsPlus.Eship.Smc.Eva
{
	public static class ReferenceTypes
	{
		public const string CarrierProNumber = "CN";
		public const string BillOfLadingNumber = "BM";
		public const string PurchaseOrderNumber = "PO";
		public const string PickupNumber = "P8";
		public const string QuoteNumber = "Q1";
		public const string ShipmentId = "SI";
	}
}
