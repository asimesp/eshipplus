﻿namespace LogisticsPlus.Eship.Smc.Eva
{
	public static class Units
	{
        public const decimal FtIn = 12.00000000m;

		public struct Dimension
		{
			public const string Feet = "F";
			public const string Meters = "M";
		}

		public struct Weight
		{
			public const string Pounds = "L";
			public const string Kilograms = "K";
		}

        public static decimal InchesToFeet(this decimal value)
        {
            return value / FtIn;
        }
	}
}
