﻿namespace LogisticsPlus.Eship.Smc.Rates
{
	public class TariffDetails
	{
		public string TariffName { get; set; }
		public string Description { get; set; }
		public string EffectiveDate { get; set; }
		public string ProductNumber { get; set; }
		public string Release { get; set; }
		
		public string ReaderFriendlyTariff
		{
			get { return TariffNameFormatter.ReaderFriendlyTariff(TariffNameFormatter.BuildTariffNameDate(TariffName, EffectiveDate)); }
		}
	}
}
