﻿namespace LogisticsPlus.Eship.Smc.Rates
{
	public class RateRequestItem
	{
		public string NmfcClass { get; set; }
		public string Weight { get; set; }
		public string Length { get; set; }
		public string Width { get; set; }
		public string Height { get; set; }
	}
}
