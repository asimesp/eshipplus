﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Smc.SmcRatewareXL;

namespace LogisticsPlus.Eship.Smc.Rates
{
	public class TariffAnalysisResponse
	{
		public DiscountTierRateBasis RateBasis { get; set; }

		public string Error { get; set; }

		public bool HasError
		{
			get { return !string.IsNullOrEmpty(Error); }
		}

		public string Key { get; set; }
		public string TariffName { get; set; }
		public string TariffDate { get; set; }
		public string OriginalValue { get; set; }
		public string ActualWeight { get; set; }
		public string BilledWeight { get; set; }
		public string MinimumCharge { get; set; }
		public string DeficitRate { get; set; }
		public string DeficitWeight { get; set; }
		public string DeficitCharge { get; set; }
		public string OriginState { get; set; }
		public string OriginPostalCode { get; set; }
		public string OriginCountry { get; set; }
		public string DestinationState { get; set; }
		public string DestinationPostalCode { get; set; }
		public string DestinationCountry { get; set; }

		public List<TariffAnalysisDetail> Items { get; set; }

		public TariffAnalysisResponse()
		{
			Error = string.Empty;

			Key = string.Empty;
			TariffName = string.Empty;
			TariffDate = string.Empty;
			OriginalValue = string.Empty;
			ActualWeight = string.Empty;
			BilledWeight = string.Empty;
			MinimumCharge = string.Empty;
			DeficitCharge = string.Empty;
			DeficitRate = string.Empty;
			DeficitWeight = string.Empty;
			OriginState = string.Empty;
			DestinationState = string.Empty;
			OriginPostalCode = string.Empty;
			DestinationPostalCode = string.Empty;
			OriginCountry = string.Empty;
			DestinationCountry = string.Empty;

			Items = new List<TariffAnalysisDetail>();
		}


		public static TariffAnalysisResponse NewResponse(DensityRateShipmentResponse response)
		{
			return new TariffAnalysisResponse
			       	{
						Error = Rateware2.ErrorCodeDescription(response.errorCode),
						RateBasis = DiscountTierRateBasis.Density,
						Key = response.shipmentID,
			       		TariffName = response.tariffName,
			       		TariffDate = response.effectiveDate,
			       		OriginalValue = response.totalCharge,
			       		ActualWeight = response.actualWgt,
			       		BilledWeight = response.billedWgt,
			       		MinimumCharge = response.minimumCharge,
			       		DeficitRate = response.deficitWeight,
			       		DeficitWeight = response.deficitWeight,
			       		DeficitCharge = response.deficitCharge,
			       		OriginState = response.originState,
			       		OriginPostalCode = response.originPostalCode,
			       		OriginCountry = response.originCountry,
			       		DestinationState = response.destinationState,
			       		DestinationPostalCode = response.destinationPostalCode,
			       		DestinationCountry = response.destinationCountry,
			       		Items = response.details
			       			.Select(d => new TariffAnalysisDetail
			       			             	{
			       			             		FreightClass = string.Empty,
			       			             		Weight = d.weight,
			       			             		Length = d.length,
			       			             		Width = d.width,
			       			             		Height = d.height,
			       			             		Rate = d.rate,
			       			             		Charge = d.charge,
			       			             		Quantity = d.pieces,
			       			             	})
			       			.ToList()
			       	};
		}

		public static TariffAnalysisResponse NewResponse(LTLRateShipmentResponse response)
		{
			return new TariffAnalysisResponse
			       	{
						Error = Rateware2.ErrorCodeDescription(response.errorCode),
						RateBasis = DiscountTierRateBasis.FreightClass,
						Key = response.shipmentID,
			       		TariffName = response.tariffName,
			       		TariffDate = response.effectiveDate,
			       		OriginalValue = response.totalCharge,
			       		ActualWeight = response.actualWgt,
			       		BilledWeight = response.billedWgt,
			       		MinimumCharge = response.minimumCharge,
			       		DeficitRate = response.deficitRate,
			       		DeficitWeight = response.deficitWeight,
			       		DeficitCharge = response.deficitCharge,
			       		OriginState = response.originState,
			       		OriginPostalCode = response.originPostalCode,
			       		OriginCountry = response.originCountry,
			       		DestinationState = response.destinationState,
			       		DestinationPostalCode = response.destinationPostalCode,
			       		DestinationCountry = response.destinationCountry,
			       		Items = response.details.Select(d => new TariffAnalysisDetail
			       		                              	{
			       		                              		FreightClass = d.nmfcClass,
			       		                              		Weight = d.weight,
			       		                              		Length = string.Empty,
			       		                              		Width = string.Empty,
			       		                              		Height = string.Empty,
			       		                              		Rate = d.rate,
			       		                              		Charge = d.charge,
			       		                              		Quantity = string.Empty,
			       		                              	})
			       			.ToList()
			       	};
		}
	}
}
