﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Smc.SmcRatewareXL;

namespace LogisticsPlus.Eship.Smc.Rates
{
	public class TariffAnalysisRequest
	{
		public string Key { get; set; }

		public string OriginPostalCode { get; set; }
		public string OriginCountry { get; set; }
		public string DestinationPostalCode { get; set; }
		public string DestinationCountry { get; set; }
		public WeightBreak WeightBreak { get; set; }

		public List<TariffAnalysisDetail> Items { get; set; }

		public TariffAnalysisRequest()
		{
			Key = DateTime.Now.ToString("ffff");
			OriginCountry = string.Empty;
			OriginPostalCode = string.Empty;
			DestinationCountry = string.Empty;
			DestinationPostalCode = string.Empty;
			WeightBreak = WeightBreak.Ignore;		
			Items = new List<TariffAnalysisDetail>();
		}

		public DensityRateShipmentRequest ToDensityRateShipmentRequest(string serviceDateFormat, string tariff)
		{
			return new DensityRateShipmentRequest
			       	{
			       		destinationCountry = DestinationCountry,
			       		destinationPostalCode = DestinationPostalCode,
			       		originCountry = OriginCountry,
			       		originPostalCode = OriginPostalCode,
			       		shipmentID = Key,
			       		shipmentDateCCYYMMDD = TariffNameFormatter.ParseTariffDate(tariff).ToString(serviceDateFormat),
			       		details = Items
			       			.Select(i => new DensityRequestDetail
			       			             	{
			       			             		weight = i.Weight,
			       			             		length = i.Length,
			       			             		width = i.Width,
			       			             		height = i.Height,
			       			             		dimensionUnits = "I",
			       			             		weightUnits = "L",
			       			             		pieces = i.Quantity,
			       			             	})
			       			.ToArray(),
			       		detailType = "LWH",
			       		tariffName = TariffNameFormatter.ParseTariffName(tariff),
			       		stopAlternationWeight = WeightBreak.GetWeightBreakValue()
			       	};
		}

		public LTLRateShipmentRequest ToLTLRateShipmentRequest(string serviceDateFormat, string tariff)
		{
			return new LTLRateShipmentRequest
			       	{
			       		destinationCountry = DestinationCountry,
			       		destinationPostalCode = DestinationPostalCode,
			       		originCountry = OriginCountry,
			       		originPostalCode = OriginPostalCode,
			       		shipmentID = Key,
			       		shipmentDateCCYYMMDD = TariffNameFormatter.ParseTariffDate(tariff).ToString(serviceDateFormat),
			       		details = Items
			       			.Select(i => new LTLRequestDetail {nmfcClass = i.FreightClass, weight = i.Weight})
			       			.ToArray(),
			       		tariffName = TariffNameFormatter.ParseTariffName(tariff),
			       		stopAlternationWeight = WeightBreak.GetWeightBreakValue()
			       	};
		}
	}
}
