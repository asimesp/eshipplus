﻿using System;

namespace LogisticsPlus.Eship.Smc.Rates
{
	public class TransitDaysInfo
	{
		public string OriginTerminalCode { get; set; }
		public string DestinationTerminalCode { get; set; }
		public bool DirectPointTransit { get; set; }
		public int TransitDays { get; set; }
		public DateTime EstimatedDeliveryDate { get; set; }
		public string Key { get; set; }
	}
}
