﻿namespace LogisticsPlus.Eship.Smc.Rates
{
	public class TariffAnalysisDetail
	{
		public string FreightClass { get; set; }
		public string Weight { get; set; }
		public string Length { get; set; }
		public string Width { get; set; }
		public string Height { get; set; }
		public string Rate { get; set; }
		public string Charge { get; set; }
		public string Quantity { get; set; }

		public string Dimensions { get { return string.Format("{0}x{1}x{2}", Length, Width, Height); } }
	}
}
