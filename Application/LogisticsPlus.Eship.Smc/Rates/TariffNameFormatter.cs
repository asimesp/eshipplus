﻿using System;

namespace LogisticsPlus.Eship.Smc.Rates
{
	public class TariffNameFormatter
	{
		private const int TariffNameLength = 8;

		public static DateTime ParseTariffDate(string rateWareKeyName)
		{
			if (rateWareKeyName == null)
				throw new ArgumentNullException("rateWareKeyName");

			if (rateWareKeyName.Length != 17)
				throw new ArgumentOutOfRangeException(
					"rateWareKeyName",
					string.Format("The key '{0}' is not the expected length of 17.", rateWareKeyName));

			var year = int.Parse(rateWareKeyName.Substring(8, 4));
			var month = int.Parse(rateWareKeyName.Substring(12, 2));
			var day = int.Parse(rateWareKeyName.Substring(14, 2));

			return new DateTime(year, month, day);
		}

		public static string ParseTariffName(string rateWareKeyName)
		{
			return rateWareKeyName.Substring(0, TariffNameLength);
		}

		public static string BuildTariffNameDate(string name, string effectiveDate)
		{
			return string.Format("{0}{1}N", name, effectiveDate); // N required but ignored in processing, P for small package via SMC
		}

		public static string BuildTariffNameDate(string name, DateTime effectiveDate)
		{
			return BuildTariffNameDate(name, effectiveDate.ToString("yyyyMMdd"));
		}

		public static string ReaderFriendlyTariff(string tariff)
		{
			return string.IsNullOrEmpty(tariff) || tariff.Length < 16
			       	? string.Empty
			       	: string.Format("{0} {1}-{2}-{3}",
			       	                tariff.Substring(0, 8),
			       	                tariff.Substring(8, 4),
			       	                tariff.Substring(12, 2),
			       	                tariff.Substring(14, 2));
		}
	}
}
