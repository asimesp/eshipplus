﻿using System;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Smc.Rates
{
	public class TransitDaysRequest
	{
		public string Key { get; set; }
		public string DestinationCountry { get; set; }
		public string DestinationPostalCode { get; set; }
		public string OriginCountry { get; set; }
		public string OriginPostalCode { get; set; }
		public string ShipmentId { get; set; }
		public ServiceMode Mode { get; set; }
		public string Scac { get; set; }
		public bool IndirectPoint { get; set; }
		public DateTime EarlyPickup { get; set; }
		public DateTime LatePickup { get; set; }
		public string Smc3ServiceLevel { get; set; }
	}
}
