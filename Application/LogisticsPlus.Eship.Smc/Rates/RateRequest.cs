﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Smc.Rates
{
	public class RateRequest
	{
		public string DestinationCountry { get; set; }
		public string DestinationPostalCode { get; set; }
		public string OriginCountry { get; set; }
		public string OriginPostalCode { get; set; }
		public string ShipmentId { get; set; }
		public string LTLTariff { get; set; }
		public string StopAlternationWeight { get; set; }
		public ServiceMode Mode { get; set; }
		public string Scac { get; set; }
		public bool IndirectPoint { get; set; }
		public DiscountTierRateBasis RateBasis { get; set; }
		public DateTime EarlyPickup { get; set; }
		public DateTime LatePickup { get; set; }
		public string Smc3ServiceLevel { get; set; }

		public List<RateRequestItem> Items { get; set; }

		public RateRequest()
		{
			Items = new List<RateRequestItem>();
		}
	}
}
