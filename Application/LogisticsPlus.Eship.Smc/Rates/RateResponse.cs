﻿using System;

namespace LogisticsPlus.Eship.Smc.Rates
{
	public class RateResponse
	{
		public string ShipmentId { get; set; }
		public decimal LineHaulCharge { get; set; }
		public double BilledWeight { get; set; }
		public int TransitDays { get; set; }
		public DateTime EstimatedDeliveryDate { get; set; }
		public bool DirectPointRate { get; set; }
		public string OriginTerminalCode { get; set; }
		public string DestinationTerminalCode { get; set; }
	}
}
