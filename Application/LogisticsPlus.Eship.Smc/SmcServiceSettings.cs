﻿using System;

namespace LogisticsPlus.Eship.Smc
{
	[Serializable]
	public class SmcServiceSettings
	{
		public string UserId { get; set; }
		public string Password { get; set; }
		public string License { get; set; }
		public string Uri { get; set; }

		public bool IsSet
		{
			get
			{
				return !string.IsNullOrEmpty(UserId) &&
					   !string.IsNullOrEmpty(Password) &&
					   !string.IsNullOrEmpty(License) &&
					   !string.IsNullOrEmpty(Uri);
			}
		}

		public void Clear()
		{
			UserId = string.Empty;
			Password = string.Empty;
			License = string.Empty;
			Uri = string.Empty;
		}
	}
}
