﻿using System;

namespace LogisticsPlus.Eship.Smc.Carriers
{
	[Serializable]
	public class TerminalTransit
	{
		public Terminal From { get; set; }
		public Terminal To { get; set; }
		public int MinimumDays { get; set; }
		public int MinimumCalendarDays { get; set; }
		public int MaximumDays { get; set; }
		public int MaximumCalendarDays { get; set; }
		public bool Direct { get; set; }
		public string ServiceCode { get; set; }
		public string Method { get; set; }
		public DateTime DataReleaseDate { get; set; }
		public DateTime DataExpirationDate { get; set; }
		public string DataReleaseName { get; set; }
	}
}
