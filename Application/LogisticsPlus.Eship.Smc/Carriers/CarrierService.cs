﻿using System;

namespace LogisticsPlus.Eship.Smc.Carriers
{
	[Serializable]
	public class CarrierService
	{
		public string ServiceCode { get; set; }
		public string ServiceMethod { get; set; }
	}
}
