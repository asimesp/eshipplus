﻿using System;
using System.Collections.Generic;

namespace LogisticsPlus.Eship.Smc.Carriers
{
	[Serializable]
	public class Carrier
	{
		public string Name { get; set; }
		public string Scac { get; set; }

		public List<CarrierService> Services { get; set; }
	}
}
