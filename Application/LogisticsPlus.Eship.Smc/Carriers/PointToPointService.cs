﻿using System;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Smc.Carriers
{
	[Serializable]
	public class PointToPointService
	{
		public string OriginPostalCode { get; set; }
		public Country OriginCountry { get; set; }
		public string DestinationPostalCode { get; set; }
		public Country DestinationCountry { get; set; }
		public string OriginTerminalCode { get; set; }
		public string DestinationTerminalCode { get; set; }
		public string Scac { get; set; }
		public string CarrierName { get; set; }

		public int MinimumDays { get; set; }
		public int MinimumCalendarDays { get; set; }
		public int MaximumDays { get; set; }
		public int MaximumCalendarDays { get; set; }
		public bool Direct { get; set; }
		public string ServiceCode { get; set; }
		public string Method { get; set; }
		public DateTime DataReleaseDate { get; set; }
		public DateTime DataExpirationDate { get; set; }
		public string DataReleaseName { get; set; }
	}
}
