﻿using System;

namespace LogisticsPlus.Eship.Smc.Carriers
{
	[Serializable]
	public class Terminal
	{
		public string CarrierName { get; set; }
		public string Scac { get; set; }
		public string Method { get; set; }
		public string TerminalName { get; set; }
		public string CountryCode { get; set; }
		public string State { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string City { get; set; }
		public string PostalCode { get; set; }
		public string PostalCodePlus4 { get; set; }
		public string Code { get; set; }
		public string ContactName { get; set; }
		public string ContactTitle { get; set; }
		public string ContactPhone { get; set; }
		public string ContactFax { get; set; }
		public string ContactEmail { get; set; }
		public string ContactTollFree { get; set; }
	}
}
