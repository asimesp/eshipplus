using System.Configuration;

namespace LogisticsPlus.Eship.FaxMonitor
{
	internal static class Configuration
	{
		public static string RelayFaxLogFile
		{
			get { return GetSetting("RelayFax.LogFile"); }
		}


        public static string DefaultConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString; }
        }

		private static string GetSetting(string key)
		{
			return ConfigurationManager.AppSettings[key];
		}


	}
}