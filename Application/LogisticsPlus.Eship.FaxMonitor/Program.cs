using System;
using log4net;
using log4net.Config;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.FaxMonitor
{
	public static class Program
	{
		private static readonly ILog log = LogManager.GetLogger(typeof (Program));

		public static void Main()
		{
            DatabaseConnection.DatabaseType = DatabaseType.MsSql;
            DatabaseConnection.DefaultConnectionString = Configuration.DefaultConnectionString;
            
            XmlConfigurator.Configure();
			
            try
			{
				AdobeReaderTerminate.Execute(log);
				CheckFaxStatuses();
			}
			catch (Exception exception)
			{
				log.Fatal(exception.Message, exception);
			}
		}

		private static void CheckFaxStatuses()
		{
			try
			{
				IShipmentFaxSource faxSource = new RelayFaxShipmentFaxSource(Configuration.RelayFaxLogFile);
				var monitor = new ShipmentFaxStatusMonitor(faxSource);
				monitor.Notification += OnMonitorNotification;
				monitor.Error += OnError;
				monitor.FindAndUpdateShipments();
			}
			catch (Exception ex)
			{
				log.Error("An exception occurred while sending faxes.  " + ex);
			}
		}

		private static void OnError(object sender, NotificationEventArgs e)
		{
			log.Error(e.Message);
		}

		private static void OnMonitorNotification(object sender, NotificationEventArgs e)
		{
			log.Info(e.Message);
		}
	}
}