using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LogisticsPlus.Eship.FaxMonitor
{
	public class RelayFaxShipmentFaxSource : IShipmentFaxSource
	{
		private const string Separator = "----------";
		private const string Subject = "eShip PLUS Shipment Notification:";
		private const string TempFile = "C:\\Temp.RelayFax-Outbound.Log";
		private const string ErrorMessage = "Incorrect Log File";
		private const string Success = "Fax successfully sent";
		private const string Failed = "No more retries";


		private readonly string file;
		private string billOfLading;
		private string errorMessage;
		private List<ShipmentFax> results;

		public RelayFaxShipmentFaxSource(string file)
		{
			this.file = file;
		}

		public IEnumerable<ShipmentFax> ListFaxes()
		{
			results = new List<ShipmentFax>();

			string doc = ReadLogFile().Replace("\r", "");

			var splitArray = new string[1];
			splitArray[0] = Separator;

			//splits the log file into individual events and loops through them
			foreach (string loggedEvent in doc.Split(splitArray, StringSplitOptions.RemoveEmptyEntries))
				if (!string.IsNullOrEmpty(loggedEvent.Trim()))
					ParseEvent(loggedEvent);

			return results;
		}

		//string loggedEvent refers to a logged event surrounded by "----------"
		//string datedLine refers to the lines in a single logged event
		//string line refers to a line without the leading DateTime info
		//
		//if Carrier email setup is changed, reflect changes in this parse method

		//Pulls the information out of the log file
		private string ReadLogFile()
		{
			CopyFile();

			var stream = new StreamReader(TempFile);
			string doc = stream.ReadToEnd();
			stream.Close();

			File.Delete(TempFile);

			//this substring is used to remove the header information from the log file
			return doc.Substring(doc.LastIndexOf("-----------", StringComparison.Ordinal));
		}

		//RelayFax has a lock on log file.  Copying enables the parser to access the data.
		private void CopyFile()
		{
			if (File.Exists(TempFile))
				File.Delete(TempFile);
			new FileInfo(file).CopyTo(TempFile);
		}

		//breaks an event into individual lines to be processed
		private void ParseEvent(string loggedEvent)
		{
			int currentFax;
			//checks to ensure the right file is being used
			if (!int.TryParse(loggedEvent.Split(':')[3].Trim(), out currentFax))
				throw new FormatException(ErrorMessage);

			billOfLading = "";
			errorMessage = "Not Checked";

			foreach (string datedLine in loggedEvent.Trim().Split('\n'))
				//checks for junk lines
				if (datedLine != "-" && !datedLine.Contains("-----"))
				{
					//ensures datedLine contains ": " so line will not be null or empty
					if (!datedLine.Contains(currentFax + ": "))
						break;
					string line = datedLine.Substring(datedLine.LastIndexOf(currentFax + ": ", StringComparison.Ordinal));

					ParseLine(line);
				}
		}

		//extracts information from certain lines
		private void ParseLine(string line)
		{
			//gather bol number from the subject line
			if (line.Contains(Subject))
				ParseBillOfLadingNumber(line);

			//gather fax status from error line and link it to bol
			if (CanParseErrorMessage(line))
				ParseErrorMessage(line);

			if (ShouldSetErrorMessage(line))
				SetErrorMessage();
		}

		private void ParseBillOfLadingNumber(string line)
		{
			//removes ":" and leading/trailing white space from the bol
			billOfLading = line.Substring(line.LastIndexOf(':')).Replace(":", "").Trim();

			bool found = results.Any(fax => billOfLading == fax.BillOfLading);

			//checks to see if bol is already in results list

			//puts bol into results list if it is not already there
			if (!found)
			{
				var fax = new ShipmentFax {BillOfLading = billOfLading, ErrorMessage = "Not Checked"};
				results.Add(fax);
			}
		}

		//extracts error/success message from the error line
		private void ParseErrorMessage(string line)
		{
			errorMessage = line.Substring(line.LastIndexOf(':')).Replace(": ", "");
		}

		//sets fax.ErrorMessage to the last parsed errorMessage
		private void SetErrorMessage()
		{
			foreach (ShipmentFax fax in results)
				if (billOfLading == fax.BillOfLading)
				{
					fax.ErrorMessage = errorMessage;
					break;
				}
		}

		//checks to see if particular line contains the error message
		private bool CanParseErrorMessage(string line)
		{
			return !string.IsNullOrEmpty(billOfLading) &&
				(line.Contains(Success) ||
					(line.Contains("Fax modem on") && line.Contains("error:")));
		}

		//makes sure the fax has either passed or failed before setting the error message
		//example: status update happens durring first failed attempt, but later attempt is successful
		private bool ShouldSetErrorMessage(string line)
		{
			return (errorMessage == Success || line.Contains(Failed));
		}
	}
}