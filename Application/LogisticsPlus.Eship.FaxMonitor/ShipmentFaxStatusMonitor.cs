using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Processor.Searches.Connect;

namespace LogisticsPlus.Eship.FaxMonitor
{
	public class ShipmentFaxStatusMonitor
	{
		private readonly FaxTransmissionSearch _search = new FaxTransmissionSearch();
		private readonly IShipmentFaxSource _shipmentFaxSource;

		public ShipmentFaxStatusMonitor(IShipmentFaxSource shipmentFaxSource)
		{
			if (shipmentFaxSource == null)
				throw new ArgumentNullException("shipmentFaxSource");

			_shipmentFaxSource = shipmentFaxSource;
		}

		public event EventHandler<NotificationEventArgs> Notification;
		public event EventHandler<NotificationEventArgs> Error;

		private void NotifyInfo(string message, params object [] args)
		{
			if (Notification != null)
				Notification(this, new NotificationEventArgs(String.Format(message, args)));
		}

		private void NotifyError(string message, params object[] args)
		{
			if (Error != null)
				Error(this, new NotificationEventArgs(String.Format(message, args)));
		}

		public void FindAndUpdateShipments()
		{
			NotifyInfo("Getting fax information from service...");
			foreach (var fax in _shipmentFaxSource.ListFaxes())
			{
				NotifyInfo("Finding shipment {0}... ", fax.BillOfLading);
				try
				{
					var transmission = _search.FetchFaxTransmission(new Guid(fax.BillOfLading));
					if (transmission == null)
						NotifyInfo("NOT FOUND IN DATABASE!");
					else
					{
						NotifyInfo("Updating shipment {0}", transmission.TransmissionKey);

						transmission.Message = fax.ErrorMessage;
						if (transmission.ResponseDateTime == DateUtility.SystemEarliestDateTime)
							transmission.ResponseDateTime = DateTime.Now;
						transmission.LastStatusCheckDateTime = DateTime.Now;
						transmission.Status = fax.ErrorMessage.ToLower().Contains("successful")
												? FaxTransmissionStatus.Success
												: FaxTransmissionStatus.Failed;
						transmission.Save();
					}
				}
				catch (Exception e)
				{
					NotifyError("Exception Updating Shipment {0}: {1}", fax.BillOfLading, e.Message);
				}
			}

			NotifyInfo("Done.");
		}
	}
}