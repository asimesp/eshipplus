using System.Collections.Generic;

namespace LogisticsPlus.Eship.FaxMonitor
{
	public interface IShipmentFaxSource
	{
		IEnumerable<ShipmentFax> ListFaxes();
	}
}