namespace LogisticsPlus.Eship.FaxMonitor
{
	public class ShipmentFax
	{
		public string BillOfLading { get; set; }
		public string ErrorMessage { get; set; }
	}
}