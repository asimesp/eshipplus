using System;

namespace LogisticsPlus.Eship.FaxMonitor
{
	public class NotificationEventArgs : EventArgs
	{
		private readonly string message;

		public NotificationEventArgs(string message)
		{
			this.message = message;
		}

		public string Message
		{
			get { return message; }
		}
	}
}