﻿using System.Diagnostics;
using log4net;

namespace LogisticsPlus.Eship.FaxMonitor
{
	class AdobeReaderTerminate
	{
		private const string processName = "AcroRd32";
		internal static void Execute(ILog logger)
		{
			var toTerminate = Process.GetProcessesByName(processName);
			
			foreach (var process in toTerminate)
			{
				
				logger.InfoFormat("Terminating {0} PID: {1}. ", process.ProcessName, process.Id);

				process.Kill();
			}
		}
	}
}
