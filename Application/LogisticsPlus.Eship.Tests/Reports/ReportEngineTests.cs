﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Reports;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Reports
{
	[TestFixture]
	public class ReportEngineTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void Dump()
		{
			var c = new ReportCustomization
						{
							Charts = 
								new List<ChartConfiguration>
									{
                                        new ChartConfiguration()
                                        {
										Title = "CHART TITLE",
										XAxisDataColumn = "XAxisDataColumn",
										XAxisTitle = "XAxis Title",
										YAxisTitle = "YAxis Title",
                                        }
									},
							DataColumns = new List<ReportColumn>
							              	{
							              		new ReportColumn
							              			{
							              				Name = "Name",
                                                        Column = "ReportColumn",
                                                        Custom = false,
                                                        DataType = SqlDbType.NChar
							              			}
							              	},
							Parameters = new List<ParameterColumn>
							             	{
							             		new ParameterColumn
							             			{
							             				ReportColumnName = "ReportColumn",
                                                        DefaultValue = "",
                                                        Operator = Operator.Contains,
                                                        ReadOnly = false,
							             			}
							             	},
							SortColumns = new List<SortColumn>
							              	{
							              		new SortColumn
							              			{
							              				ReportColumnName = "ReportColumn",
                                                        Priority = 0,
                                                        Direction = SortDirection.Ascending
							              			}
							              	},
							Summaries = new List<Summary>
							            	{
							            		new Summary
							            			{
							            				Name = "SummaryName",
                                                        ReportColumnName = "ReportColumnName",
                                                        SortOrder = 0
							            			}
							            	}
						};

			Console.WriteLine(c.ToXml());
		}

		[Test]
		public void CanRunAndGenerateReportData()
		{
			var template = new ReportTemplate(GlobalTestInitializer.DefaultReportTemplate);
			if (!template.KeyLoaded) return;
			var configuration = new ReportConfiguration
									{
										Description = "Test",
										Name = "Test",
										ReportTemplate = template,
										TenantId = GlobalTestInitializer.DefaultTenantId,
										SerializedCustomization = template.DefaultCustomization,
										Visibility = ReportConfigurationVisibility.AllUsers
									};
			var engine = new ReportEngine();
			var data = engine.GenerateReport(configuration, GlobalTestInitializer.ActiveUser);
			DisplayReportData(data);
		}

		private void DisplayReportData(ReportData data)
		{
			if (data.HasTable)
			{
				var columns = data.Table.Columns.Cast<DataColumn>().Select(c => c.ColumnName).ToList();
				Console.WriteLine(data.Table.TableName);
				foreach (var c in columns) Console.Write("{0},", c);
				Console.WriteLine();
				foreach (DataRow row in data.Table.Rows)
				{
					foreach (var c in columns) Console.Write("{0},", row[c]);
					Console.WriteLine();
				}
			}

			if (data.HasSummaries)
			{
				Console.WriteLine();
				foreach (var summary in data.Summaries)
					Console.WriteLine("{0}: {1}", summary.Key, summary.Value);
			}
		}
	}
}
