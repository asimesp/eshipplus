﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Ionic.Zlib;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Extern.Ws;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Reports;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.Processor.Views.Registry;
using LogisticsPlus.Eship.Smc;
using LogisticsPlus.Eship.Smc.Eva;
using LogisticsPlus.Eship.Tests.ExcelLibManipulations;
using LogisticsPlus.Eship.WebApplication;
using NUnit.Framework;
using Newtonsoft.Json;
using OfficeOpenXml;
using CompressionLevel = Ionic.Zlib.CompressionLevel;


namespace LogisticsPlus.Eship.Tests
{
    [TestFixture]
    [Ignore("Misc Test. Run on demand only")]
    public class MiscTests
    {
        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }


        [Test]
        public void CanSendFromNewEmail()
        {
            var msg = new MailMessage();
            msg.To.Add("kyle.grygo@logisticsplus.net");
            msg.To.Add("chris.oyesiku@logisticsplus.net");

            msg.From = new MailAddress("lpls.monitor@logisticsplus.net");
            msg.Subject = "Test Email for lpls.monitor";
            msg.Body = "This is a test.";
            msg.IsBodyHtml = true;

            var client = new SmtpClient("smtp.office365.com")
                {
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Port = 25,
                    UseDefaultCredentials = false, 
                    EnableSsl = true,
                    Credentials = new NetworkCredential("lpls.monitor@logisticsplus.net", "LpMon7391$")
                };

            client.Send(msg);
        }

        [Test]
        public void RegexReplaceAllButLettersTest()
        {
            const string input = "NAD12346DBE-*?#$%";
            const string pattern = "([^0-9])";
            Console.WriteLine(Regex.Replace(input, pattern, string.Empty, RegexOptions.None));
        }

        [Test]
        public void PathCombinationTests()
        {
            const string a = "ABC";
            const string b = "214";

            Console.WriteLine(Path.Combine(a, b));

            Console.WriteLine("Empty Paths: {0}", Path.Combine(string.Empty, string.Empty));

            Assert.IsTrue(string.IsNullOrEmpty(Path.Combine(string.Empty, string.Empty)));
        }

        [Test]
        public void SendEmail()
        {
            for (var i = 0; i < 500; i++)
            {
                var msg = new MailMessage
                    {
                        From = new MailAddress("eship@lpinc.co"),
                        Subject = string.Format("Test hMail server - {0} - {1}", i + 1, Guid.NewGuid()),
                        Body = string.Format("This is a test email - {0}", DateTime.Now),
                        IsBodyHtml = true,
                    };

                msg.To.Add("kevin.gorny@logisticsplus.net");
                msg.To.Add("christopher.oyesiku@logisticsplus.net");
                //msg.To.Add("kgorny22@gmail.net");
                //msg.To.Add("coocomworldwide@yahoo.com");

                var client = new SmtpClient("mail.lpinc.co")
                    {
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        Port = 25,
                        Credentials = new NetworkCredential("eship@lpinc.co", "814Peach!")
                    };
                client.Send(msg);
            }
        }

        public static void CopyTo(Stream src, Stream dest)
        {
            byte[] bytes = new byte[4096];

            int cnt;

            while ((cnt = src.Read(bytes, 0, bytes.Length)) != 0)
            {
                dest.Write(bytes, 0, cnt);
            }
        }

        [Test]
        public void Z()
        {
            var tickets = new ServiceTicketSearch().FetchThem(GlobalTestInitializer.DefaultTenantId);
            Console.WriteLine("Count = {0}", tickets.Count);
            var groups = tickets.GroupBy(t => t.ExternalReference1);
            Console.WriteLine("Group Count = {0}", groups.Count());

            var an = new AutoNumberProcessor();
            var user = new User(GlobalTestInitializer.DefaultUserId);
            var customer = new CustomerSearch().FetchCustomerByNumber("10000", GlobalTestInitializer.DefaultTenantId);
            var criteria = new JobViewSearchCriteria
                {
                    ActiveUserId = GlobalTestInitializer.DefaultUserId,
                    Parameters = new List<ParameterColumn> { OperationsSearchFields.ExternalReference1.ToParameterColumn() },
                };
            criteria.Parameters[0].Operator = Operator.Equal;

            var jobSearch = new JobSearch();
            foreach (var g in groups)
            {
                criteria.Parameters[0].DefaultValue = g.First().ExternalReference1.Trim();
                var jobDto = jobSearch.FetchJobDashboardDtos(criteria, GlobalTestInitializer.DefaultTenantId).FirstOrDefault();
                if (jobDto != null)
                {
                    foreach (var ticket in g)
                    {
                        ticket.JobId = jobDto.Id;
                        ticket.JobStep = 0;
                        ticket.Save();
                    }
                }
                else
                {
                    var job = new Job
                        {
                            DateCreated = DateTime.Now,
                            JobNumber = an.NextNumber(AutoNumberCode.JobNumber, user).GetString(),
                            CreatedByUser = user,
                            Customer = customer,
                            Documents = new List<JobDocument>(),
                            ExternalReference1 = g.First().ExternalReference1,
                            Status = JobStatus.InProgress,
                            ExternalReference2 = string.Empty,
                            TenantId = GlobalTestInitializer.DefaultTenantId,
                        };
                    job.Save();

                    foreach (var i in g.ToList())
                    {
                        i.JobId = job.Id;
                        i.JobStep = 0;
                        i.Save();
                    }
                }
            }

        }

        [Test]
        public void Y()
        {
            // compression
            var value = "Remember that Zip returns a byte[], while Unzip returns a string. If you want a string from Zip you can Base64 encode it (for example by using Convert.ToBase64String(r1)) (the result of Zip is VERY binary! It isn't something you can print to the screen or write directly in an XML)";

            var compressString = GZipStream.CompressBuffer(new UTF8Encoding().GetBytes(value));
            Console.WriteLine("C: {0}", compressString);
            Console.WriteLine("D: {0}", new UTF8Encoding().GetString(GZipStream.UncompressBuffer(compressString)));


            byte[] arr;
            using (var ss = new MemoryStream(new UTF8Encoding().GetBytes(value)))
            using (var ds = new MemoryStream())
            {
                using (var compressor = new GZipStream(ds, CompressionMode.Compress, CompressionLevel.BestCompression))
                {
                    CopyTo(ss, compressor);

                }
                arr = ds.ToArray();
            }

            var b64 = Convert.ToBase64String(arr);
            Console.WriteLine(b64);

            ////decompression
            //arr = Convert.FromBase64String(b64);
            //using (var ss = new MemoryStream(arr))
            //using (var ds = new MemoryStream())
            //using (var decompressor = Ionic.Crc.CRC32  (ss, CompressionMode.Decompress))
            //{
            //	//decompressor.CopyTo(destStream);

            //	CopyTo(decompressor, ds);

            //	Console.WriteLine(new UTF8Encoding().GetString(ds.ToArray()));
            //}
        }

        [Test]
        public void X()
        {
            Console.WriteLine(HttpStatusCode.OK.ToInt());

            Console.WriteLine(JsonConvert.SerializeObject(new DispatchRequest { billToAccount = "my test" }));

            var request = WebRequest.Create("http://www.eshipplus.com/smc3/statusupdate.aspx");
            request.Method = SmcConstants.PostMethod;
            request.ContentType = SmcConstants.JsonContentType;

            var json = JsonConvert.SerializeObject(new TrackingData { transactionID = Guid.NewGuid().GetString(), pieces = null });
            Console.WriteLine(json);
            Console.WriteLine();
            using (var writer = new StreamWriter(request.GetRequestStream()))
                writer.Write(json);

            var response = request.GetResponse();
            string responseData;
            var responseStream = response.GetResponseStream();
            if (responseStream == null)
            {
                Console.WriteLine("Error reading response stream!");
                return;
            }
            using (var reader = new StreamReader(responseStream))
                responseData = reader.ReadToEnd();
            Console.WriteLine("Data: {0}", responseData);


            //foreach (var x in ProcessorUtilities.GetAll<DayOfWeek>()) Console.WriteLine("{0}: {1}", x.Key, x.Value);

            //Console.WriteLine("2016-02-28".ToDateTime().AddYears(1));

            //const string file = "C:\\Users\\christopher.oyesiku\\Desktop\\dump\\PeoplerequestGEHTS-20150730_035542.xlsx";

            ////var elm = new OpenDocXlsxDataDumpHelper(file, GlobalTestInitializer.TempFilePath + "eShipPlusZipTest\\");

            //WSBillingDetail w1 = new WSBillingDetail();
            //WSBillingDetail w2 = new WSBillingDetail();

            //Assert.IsTrue(w1 != w2);

            //elm.WriteData();

            //Console.WriteLine(DateTime.Now.ToString("yyyyMMddHHmmssffffff"));
            //Console.WriteLine(DateTime.Now.ToString("yyyyMMddHHmmssffffff"));
            //Console.WriteLine(DateTime.Now.ToString("yyyyMMddHHmmssffffff"));

            //var p = new PostalCode {Code = "M2J3Z6", Country = new Country(GlobalTestInitializer.DefaultCountryId) {PostalCodeValidation = "A####|#####|A#A#A#"}};
            //Console.WriteLine(p.IsValidPostalCodeFormat());


            //var msg = new MailMessage();
            //msg.To.Add("chris@softdevplus.com");
            //msg.From = new MailAddress("sales@riseandshinewindows.com");
            //msg.Subject = "Test";
            //msg.Body = "This is a test email";
            //msg.IsBodyHtml = true;
            //var client = new SmtpClient("smtpout.secureserver.net");
            //client.Credentials  = new NetworkCredential("sales@riseandshinewindows.com", "London2014");
            //client.Port = 25;
            //client.DeliveryMethod = SmtpDeliveryMethod.Network;

            //client.Send(msg);

            //TestRetrieval tr = new TestRetrieval();

            //tr.Echo();
            //tr.GetImage("1110080379");
            //Console.WriteLine(117.23216496449.ToInt());
            //int x;
            //Console.Write(Int32.TryParse(117.265498491651.ToString(), out x));

            //var x = "241562.pdf";
            //var idxp = x.LastIndexOf(".", StringComparison.Ordinal);
            //var ext = x.Substring(idxp);
            //var doc = x.Substring(0, idxp);
            //var guid = Guid.NewGuid().ToString();
            //var outf = string.Format("{0}_{1}{2}", doc, guid, ext);
            //Console.WriteLine(outf);

            //var idxu = outf.IndexOf("_", System.StringComparison.Ordinal);
            //idxp = outf.LastIndexOf(".", System.StringComparison.Ordinal);
            //var retGuid = outf.Substring(idxu + 1, idxp - idxu - 1);
            //Console.WriteLine(retGuid);

        }

        //this test is a proof of concept to see if we can detect infinite name resolution with custom columns in the report engine
        [Test]
        public void TestInfiniteLoopRecursionAlgorithm()
        {
            var configuration = new ReportConfiguration(116);
            var config = configuration.SerializedCustomization.FromXml<ReportCustomization>();
            var customCols = config.DataColumns.Where(c => c.Custom).ToList();
            var resolvedNames = new List<string>();

            var valid = true;
            foreach (var column in customCols)
            {
                resolvedNames.Add(ResolveColumnNameAndCheckForInfiniteLoop(column, new List<string>(), customCols, ref valid).WrapBraces());
                if (valid == false)
                    throw new Exception("Infinite Loop Detected");
            }
            Assert.IsTrue(valid);
            Console.WriteLine(string.Join(Environment.NewLine, resolvedNames.ToArray()));
        }

        //this version will return a bool of whether or not it infinite loops or not
        public bool ResolveColumnNameAndCheckForInfiniteLoop(ReportColumn column, List<string> previouslyResolvedNames, List<ReportColumn> columnsInReport)
        {
            var valid = true;
            if (previouslyResolvedNames.Contains(column.Name)) return false;

            var colunmsToCheck = columnsInReport.Where(reportColumn => column.Column.Contains(reportColumn.Name)).ToList();
            previouslyResolvedNames.Add(column.Name);

            foreach (var reportColumn in colunmsToCheck)
                if (!ResolveColumnNameAndCheckForInfiniteLoop(reportColumn, new List<string>(previouslyResolvedNames), columnsInReport))
                    valid = false;

            return valid;
        }

        //this version will fully resolve the name of a custom column and whether or not it infinite loops or not
        public string ResolveColumnNameAndCheckForInfiniteLoop(ReportColumn column, List<string> previouslyResolvedNames, List<ReportColumn> columnsInReport, ref bool valid)
        {
            if (!valid) return string.Empty;

            var resolvedName = column.Column;
            if (previouslyResolvedNames.Contains(column.Name))
            {
                valid = false;
                return string.Empty;
            }

            var colunmsToCheck = columnsInReport.Where(reportColumn => column.Column.Contains(reportColumn.Name)).ToList();
            previouslyResolvedNames.Add(column.Name);

            foreach (var reportColumn in colunmsToCheck)
                resolvedName = resolvedName.Replace(reportColumn.Name.WrapBraces(), ResolveColumnNameAndCheckForInfiniteLoop(reportColumn, new List<string>(previouslyResolvedNames), columnsInReport, ref valid).WrapBraces());

            return resolvedName;
        }




        //[Test]
        //public void ReadData()
        //{
        //	var arg = new FileInfo("C:\\Users\\christopher.oyesiku\\Desktop\\dump\\XENONMARGINSHARE.xlsx");
        //	var data = GetImportData(arg, true);
        //	var customer = new CustomerSearch().FetchCustomerByNumber("15453", 4);
        //	Assert.IsNotNull(customer);
        //	var vendor = new VendorSearch().FetchVendorByNumber("10000", 4);
        //	Assert.IsNotNull(vendor);
        //	var ab = new AccountBucketSearch().FetchAccountBucketByCode("DOR001", 4);
        //	Assert.IsNotNull(ab);
        //	var user = new UserSearch().FetchUserByUsernameAndAccessCode("Admin", "TENANT1");
        //	Assert.IsNotNull(user);
        //	var ccId = new ChargeCodeSearch().FetchChargeCodeIdByCode("PROFIT", 4);
        //	Assert.IsTrue(ccId != default(long));
        //	//return;
        //	var an = new AutoNumberProcessor();
        //	foreach(var line in data)
        //	{
        //		var ticket = new ServiceTicket
        //			{
        //				AccountBucket = ab,
        //				AccountBucketUnit = null,
        //				Assets = new List<ServiceTicketAsset>(),
        //				AuditedForInvoicing = true,
        //				BillReseller = false,
        //				Charges = new List<ServiceTicketCharge>
        //					{
        //						new ServiceTicketCharge
        //							{
        //								ChargeCodeId = ccId,
        //								Comment = string.Format("{0}_{1}", line[2], line[3]),
        //								Quantity = 1,
        //								ServiceTicket = null,
        //								TenantId = GlobalTestInitializer.DefaultTenantId,
        //								UnitBuy = 0.0m,
        //								UnitDiscount = 0.0m,
        //								UnitSell = line[4].ToDecimal(),
        //							}
        //					},
        //				Customer = customer,
        //				DateCreated = DateTime.Now,
        //				Documents = new List<ServiceTicketDocument>(),
        //				Equipments = new List<ServiceTicketEquipment>(),
        //				ExternalReference1 = line[5],
        //				ExternalReference2 = string.Empty,
        //				HidePrefix = false,
        //				Items = new List<ServiceTicketItem>
        //					{
        //						new ServiceTicketItem
        //							{
        //								Comment = string.Format("Transaction Id: {0}", line[2]),
        //								Description = string.Format("Container #: {0}", line[3]),
        //								TenantId = GlobalTestInitializer.DefaultTenantId,
        //								ServiceTicket = null,
        //							}
        //					},
        //				Notes = new List<ServiceTicketNote>(),
        //				OverrideCustomerLocation = customer.Locations.First(),
        //				Prefix = customer.Prefix,
        //				ResellerAddition = null,
        //				SalesRepresentative = null,
        //				SalesRepAddlEntityCommPercent = 0m,
        //				SalesRepAddlEntityName = string.Empty,
        //				SalesRepMaxCommValue = 0m,
        //				SalesRepMinCommValue = 0m,
        //				SalesRepresentativeCommissionPercent = 0m,
        //				ServiceTicketNumber = an.NextNumber(AutoNumberCode.ServiceTicketNumber, user).GetString(),
        //				Services = new List<ServiceTicketService>(),
        //				Status = ServiceTicketStatus.Open,
        //				TenantId = GlobalTestInitializer.DefaultTenantId,
        //				TicketDate = line[0].ToDateTime(),
        //				User = user,
        //				Vendors = new List<ServiceTicketVendor>
        //					{
        //						new ServiceTicketVendor
        //							{
        //								TenantId = GlobalTestInitializer.DefaultTenantId,
        //								Primary = true,
        //								ServiceTicket = null,
        //								Vendor = vendor,
        //							}
        //					},
        //			};
        //		ticket.Items[0].ServiceTicket = ticket;
        //		ticket.Charges[0].ServiceTicket = ticket;
        //		ticket.Vendors[0].ServiceTicket = ticket;

        //		ticket.Save();
        //		ticket.Items[0].Save();
        //		ticket.Charges[0].Save();
        //		ticket.Vendors[0].Save();
        //		new AuditLog
        //			{
        //				Description = string.Format("{0} Service Ticket: {1}", AuditLogConstants.AddedNew, ticket.ServiceTicketNumber),
        //				TenantId = GlobalTestInitializer.DefaultTenantId,
        //				User = user,
        //				EntityCode = ticket.EntityName(),
        //				EntityId = ticket.Id.ToString(),
        //				LogDateTime = DateTime.Now
        //			}.Log();
        //	}
        //}

        //internal static List<string[]> GetImportData(FileInfo arg, bool removeEmptyRow, bool removeHeader = true)
        //{
        //	List<string[]> lines;

        //	// reading from xlsx file
        //	using (var fileStream = arg.Open(FileMode.Open))
        //	{
        //		var pck = new ExcelPackage(fileStream);
        //		lines = GetLinesFromPackage(removeEmptyRow, removeHeader, pck);
        //	}

        //	return lines;
        //}


        //private static List<string[]> GetLinesFromPackage(bool removeEmptyRow, bool removeHeader, ExcelPackage pck)
        //{
        //	var ws = pck.Workbook.Worksheets.FirstOrDefault();
        //	if (ws == null)
        //		throw new NullReferenceException("No data sheet in excel file!");

        //	var lines = new List<string[]>();
        //	var startIndex = removeHeader ? 2 : 1;
        //	for (var r = startIndex; r <= ws.Dimension.End.Row; r++)
        //	{
        //		var row = new string[ws.Dimension.End.Column];
        //		for (var c = 1; c <= ws.Dimension.End.Column; c++)
        //			row[c - 1] = ws.Cells[r, c].Value.GetString();
        //		if (removeEmptyRow && !string.IsNullOrEmpty(row.SpaceJoin().Trim())) lines.Add(row);
        //	}
        //	return lines;
        //}
    }
}
