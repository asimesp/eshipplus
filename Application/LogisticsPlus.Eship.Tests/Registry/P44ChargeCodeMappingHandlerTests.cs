﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using LogisticsPlus.Eship.WebApplication.Members.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class P44ChargeCodeMappingHandlerTests
    {
		private P44ChargeCodeView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new P44ChargeCodeView();
			_view.Load();
		}

		[Test]
		public void CanHandleServiceSaveOrUpdate()
		{
			var all = new P44ChargeCodeMappingSearch().FetchAllP44ChargeCodeMappings(GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var p44ChargeCodeMapping = new P44ChargeCodeMapping(all[0].Id, false);
			_view.LockRecord(p44ChargeCodeMapping);
            p44ChargeCodeMapping.TakeSnapShot();
			long chargeCodeId = p44ChargeCodeMapping.ChargeCodeId;
            p44ChargeCodeMapping.ChargeCodeId = 24;
			_view.SaveRecord(p44ChargeCodeMapping);
            p44ChargeCodeMapping.TakeSnapShot();
            p44ChargeCodeMapping.Project44Code = Project44ChargeCode.ACCELERATED;
			_view.SaveRecord(p44ChargeCodeMapping);
			_view.UnLockRecord(p44ChargeCodeMapping);
		}

		[Test]
		public void CanHandleServiceSaveAndDelete()
		{
            var codes = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].ChargeCodes;
			if (codes.Count == 0) return;
			var p44ChargeCodeMapping = new P44ChargeCodeMapping()
			              	{
			              		Project44Code = Project44ChargeCode.ACCELERATED,
			              		ChargeCodeId = 38,
								TenantId = GlobalTestInitializer.DefaultTenantId
			              	};
			_view.SaveRecord(p44ChargeCodeMapping);
			Assert.IsTrue(p44ChargeCodeMapping.Id != default(long));
			_view.DeleteRecord(p44ChargeCodeMapping);
			Assert.IsTrue(new P44ChargeCodeMapping(p44ChargeCodeMapping.Id).Project44Code != Project44ChargeCode.ACCELERATED);
		}

		internal class P44ChargeCodeView : IP44ChargeCodeView
        {
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public Dictionary<int, string> ChargeCodes
			{
				set { Assert.IsTrue(value.Count > 0); }
			}

			public Dictionary<int, string> Categories
			{
				set { Assert.IsTrue(value.Count > 0); }
			}


		    public Dictionary<int, string> DispatchFlag
		    {
		        set { Assert.IsTrue(value.Count > 0); }
		    }

		    public Dictionary<int, string> Project44Codes
		    {
                set
                { }
		    }

            public List<Service> ServiceCodes
            {
                set
                { }
            }

            public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<P44ChargeCodeMapping>> Save;
			public event EventHandler<ViewEventArgs<P44ChargeCodeMapping>> Delete;
		    public event EventHandler<ViewEventArgs<List<P44ChargeCodeMapping>>> BatchImport;
		    public event EventHandler<ViewEventArgs<P44ChargeCodeMapping>> Lock;
			public event EventHandler<ViewEventArgs<P44ChargeCodeMapping>> UnLock;

            event EventHandler<ViewEventArgs<List<ParameterColumn>>> IP44ChargeCodeView.Search
            {
                add
                {
                    throw new NotImplementedException();
                }

                remove
                {
                    throw new NotImplementedException();
                }
            }

            public void FaildedLock(Lock @lock)
			{

			}

			public void DisplaySearchResult(List<ServiceViewSearchDto> service)
			{
				Assert.IsTrue(service.Count > 0);
			}

		    public void LogException(Exception ex)
		    {
		                
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
                messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void Load()
			{
				var handler = new P44ChargeCodeHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(P44ChargeCodeMapping p44ServiceMapping)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<P44ChargeCodeMapping>(p44ServiceMapping));
			}

			public void DeleteRecord(P44ChargeCodeMapping p44ServiceMapping)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<P44ChargeCodeMapping>(p44ServiceMapping));
			}

			public void LockRecord(P44ChargeCodeMapping code)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<P44ChargeCodeMapping>(code));
			}

			public void UnLockRecord(P44ChargeCodeMapping code)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<P44ChargeCodeMapping>(code));
			}

            public void DisplaySearchResult(List<P44ChargeCodeMapping> service)
            {
                throw new NotImplementedException();
            }
        }
	}
}
