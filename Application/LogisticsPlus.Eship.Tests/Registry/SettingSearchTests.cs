﻿using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class SettingSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchByCriteria()
		{
			var all = new SettingSearch().FetchSettings(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId); // fetch all
			Assert.IsTrue(all.Count > 0);
		}

		[Test]
		public void CanFetchByCode()
		{
			var search = new SettingSearch();
			var all = search.FetchSettings(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId); // fetch all
			if (all.Count == 0) return;
			var setting = search.FetchSettingByCode(all[0].Code, GlobalTestInitializer.DefaultTenantId);
			Assert.IsFalse(setting.IsNew);
		}
	}
}
