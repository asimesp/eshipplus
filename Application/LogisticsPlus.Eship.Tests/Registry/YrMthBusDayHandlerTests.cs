﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
    [TestFixture]
    public class YrMthBusDayHandlerTests
    {
        private YrYrMthBusDayCodeView _view;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _view = new YrYrMthBusDayCodeView();
            _view.Load();
        }

        [Test]
        public void CanHandleYrMthBusDaySaveOrUpdate()
        {
            var all = new YrMthBusDaySearch().FetchYrMthBusDays(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId);
            if (all.Count == 0) return;
           
            var yrMthBusDay = all[0];
            _view.LockRecord(yrMthBusDay);
            yrMthBusDay.TakeSnapShot();
            var january = yrMthBusDay.January;
            yrMthBusDay.January = january + 1 ;
            _view.SaveRecord(yrMthBusDay);
            yrMthBusDay.TakeSnapShot();
            yrMthBusDay.January = january;
            _view.SaveRecord(yrMthBusDay);
            _view.UnLockRecord(yrMthBusDay);
        }

        [Test]
        public void CanHandleYrMthBusDaySaveAndDelete()
        {
            var yrMthBusDay = new YrMthBusDay
            {
                TenantId = _view.ActiveUser.TenantId,
                January = 1,
                February = 1,
                March = 1,
                April = 1,
                May = 1,
                June = 1,
                July = 1,
                August = 1,
                September = 1,
                October = 1,
                November = 1,
                December = 1,
                Year = DateUtility.SystemEarliestDateTime.AddYears(2).Year.ToString(),
            };

            _view.SaveRecord(yrMthBusDay);
            _view.LockRecord(yrMthBusDay);
            Assert.IsTrue(yrMthBusDay.Id != default(long));
            _view.DeleteRecord(yrMthBusDay);
            Assert.IsTrue(!new ChargeCode(yrMthBusDay.Id, false).KeyLoaded);
        }

        internal class YrYrMthBusDayCodeView : IYrMthBusDayView
        {
            public User ActiveUser
            {
                get { return GlobalTestInitializer.ActiveUser; }
            }

            public event EventHandler Loading;
            public event EventHandler<ViewEventArgs<YrMthBusDay>> Save;
            public event EventHandler<ViewEventArgs<YrMthBusDay>> Delete;
            public event EventHandler<ViewEventArgs<YrMthBusDay>> Lock;
            public event EventHandler<ViewEventArgs<YrMthBusDay>> UnLock;
            public event EventHandler<ViewEventArgs<string>> Search;

            public void FailedLock(Lock @lock)
            {

            }

            public void DisplaySearchResult(List<YrMthBusDay> mthBusDay)
            {
                Assert.IsTrue(mthBusDay.Count > 0);
            }

            public void LogException(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            public void DisplayMessages(IEnumerable<ValidationMessage> messages)
            {
                messages.PrintMessages();
                Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
            }

            public void Load()
            {
                var handler = new YrMthBusDayHandler(this);
                handler.Initialize();
            }

            public void SaveRecord(YrMthBusDay yrMthBusDay)
            {
                if (Save != null)
                    Save(this, new ViewEventArgs<YrMthBusDay>(yrMthBusDay));
            }

            public void DeleteRecord(YrMthBusDay yrMthBusDay)
            {
                if (Delete != null)
                    Delete(this, new ViewEventArgs<YrMthBusDay>(yrMthBusDay));
            }

            public void LockRecord(YrMthBusDay code)
            {
                if (Lock != null)
                    Lock(this, new ViewEventArgs<YrMthBusDay>(code));
            }

            public void UnLockRecord(YrMthBusDay code)
            {
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<YrMthBusDay>(code));
            }
        }
    }
}