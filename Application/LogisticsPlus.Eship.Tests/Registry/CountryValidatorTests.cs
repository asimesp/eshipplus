﻿using System.Linq;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class CountryValidatorTests
	{
		private CountryValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new CountryValidator();
		}

		[Test]
		public void IsExistingAndUniqueTest()
		{
            var all = ProcessorVars.RegistryCache.Countries.Values.ToList();
			if (all.Count == 0) return;
			var country = all[0];
			Assert.IsTrue(_validator.IsUnique(country)); // this should be unique
			var newCountry = new Country(1000000);
			Assert.IsFalse(newCountry.KeyLoaded); // will not be found
			newCountry.Code = country.Code;
			Assert.IsFalse(_validator.IsUnique(newCountry)); // should be false
		}

		[Test]
		public void IsUniqueTest()
		{
			var country = new Country{Code = "T?", Name="T"}; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsUnique(country));
		}

		[Test]
		public void IsValidTest()
		{
			var country = new Country { Code = "T?", Name = "T" }; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsValid(country));
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var country = new Country();
			Assert.IsFalse(_validator.HasAllRequiredData(country));
		}
	}
}
