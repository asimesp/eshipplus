﻿using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class DocumentTemplateValidatorTests
	{
		private DocumentTemplateValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new DocumentTemplateValidator();
		}

		[Test]
		public void IsUniqueTest()
		{
			var documentTemplate = new DocumentTemplate
									{
										Code = "T?",
										Primary = true,
										Category = DocumentTemplateCategory.BillOfLading,
										TenantId = GlobalTestInitializer.DefaultTenantId,
										ServiceMode = ServiceMode.LessThanTruckload,
										WeightDecimals = DecimalPlacesUtility.DecimalPlaces()[0],
										DimensionDecimals = DecimalPlacesUtility.DecimalPlaces()[0],
										CurrencyDecimals = DecimalPlacesUtility.DecimalPlaces()[0]
									}; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsUnique(documentTemplate));
		}

		[Test]
		public void IsValidTest()
		{
			var all = new DocumentTemplateSearch().FetchDocumentTemplates(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var documentTemplate = new DocumentTemplate
									{
										Code = "T?",
										TemplatePath = "Path",
										Primary = false,
										Category = DocumentTemplateCategory.BillOfLading,
										TenantId = GlobalTestInitializer.DefaultTenantId,
										ServiceMode = ServiceMode.NotApplicable,
										WeightDecimals = DecimalPlacesUtility.DecimalPlaces()[0],
										DimensionDecimals = DecimalPlacesUtility.DecimalPlaces()[0],
										CurrencyDecimals = DecimalPlacesUtility.DecimalPlaces()[0]
									}; // T? will not be found therefore unique
			
			var isValid = _validator.IsValid(documentTemplate);
			_validator.Messages.PrintMessages();
			Assert.IsTrue(isValid);
			

			documentTemplate = new DocumentTemplate
			{
				Code = "T?",
				TemplatePath = "Path",
				Primary = true,
				Category = (DocumentTemplateCategory)100,
				TenantId = GlobalTestInitializer.DefaultTenantId,
				ServiceMode = ServiceMode.Rail,
				WeightDecimals = DecimalPlacesUtility.DecimalPlaces()[0],
				DimensionDecimals = DecimalPlacesUtility.DecimalPlaces()[0],
				CurrencyDecimals = DecimalPlacesUtility.DecimalPlaces()[0]
			}; // T? will not be found therefore unique
			Assert.IsFalse(_validator.IsValid(documentTemplate));

			documentTemplate = new DocumentTemplate
			{
				Code = "T?",
				TemplatePath = "Path",
				Primary = true,
				Category = DocumentTemplateCategory.BillOfLading,
				TenantId = GlobalTestInitializer.DefaultTenantId,
				ServiceMode = (ServiceMode)1000,
				WeightDecimals = DecimalPlacesUtility.DecimalPlaces()[0],
				DimensionDecimals = DecimalPlacesUtility.DecimalPlaces()[0],
				CurrencyDecimals = DecimalPlacesUtility.DecimalPlaces()[0]
			}; // T? will not be found therefore unique
			Assert.IsFalse(_validator.IsValid(documentTemplate));
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var tenant = new DocumentTemplate();
			Assert.IsFalse(_validator.HasAllRequiredData(tenant));
		}

		[Test]
		public void IsExistingAndUniqueTest()
		{
			var all = new DocumentTemplateSearch().FetchDocumentTemplates(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var code = all[0];
			Assert.IsTrue(_validator.IsUnique(code)); // this should be unique
			var newCode = new DocumentTemplate(1000000, false);
			Assert.IsFalse(newCode.KeyLoaded); // will not be found
			newCode.Code = code.Code;
			newCode.TenantId = code.TenantId;
			Assert.IsFalse(_validator.IsUnique(newCode)); // should be false
		}

		[Test]
		public void PassesPrimaryTest()
		{
			var all = new DocumentTemplateSearch().FetchDocumentTemplates(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var documentTemplate = all.FirstOrDefault(dt => dt.Primary);
			if (documentTemplate == null) return;
			var code = new DocumentTemplate
						{
							Code = documentTemplate.Code,
							Primary = documentTemplate.Primary,
							ServiceMode = documentTemplate.ServiceMode,
							Category = documentTemplate.Category,
							TenantId = documentTemplate.TenantId,
							WeightDecimals = DecimalPlacesUtility.DecimalPlaces()[0],
							DimensionDecimals = DecimalPlacesUtility.DecimalPlaces()[0],
							CurrencyDecimals = DecimalPlacesUtility.DecimalPlaces()[0]
						};
			Assert.IsFalse(_validator.PassesPrimary(code));
		}

		[Test]
		public void CanDeleteTest()
		{
			var all = new DocumentTemplateSearch().FetchDocumentTemplates(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var template = new DocumentTemplate(all[0].Id, false);
			Assert.IsTrue(template.KeyLoaded);
			_validator.Messages.Clear();
			_validator.CanDeleteDocumentTemplate(template);
			_validator.Messages.PrintMessages();
		}
	}
}
