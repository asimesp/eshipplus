﻿using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class InsuranceTypeValidatorTests
	{
		private InsuranceTypeValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new InsuranceTypeValidator();
		}

		[Test]
		public void IsUniqueTest()
		{
			var insuranceType = new InsuranceType{Code = "T?", Description= "T", TenantId = GlobalTestInitializer.DefaultTenantId}; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsUnique(insuranceType));
		}

		[Test]
		public void IsValidTest()
		{
			var insuranceType = new InsuranceType { Code = "T?", Description = "T", TenantId = GlobalTestInitializer.DefaultTenantId}; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsValid(insuranceType));
		}

		[Test]
		public void IsExistingAndUniqueTest()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].InsuranceTypes;
			if (all.Count == 0) return;
			var type = all[0];
			Assert.IsTrue(_validator.IsUnique(type)); // this should be unique
			var newType = new InsuranceType(1000000, false);
			Assert.IsFalse(newType.KeyLoaded); // will not be found
			newType.Code = type.Code;
			newType.TenantId = type.TenantId;
			Assert.IsFalse(_validator.IsUnique(newType)); // should be false
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var contactType = new InsuranceType();
			Assert.IsFalse(_validator.HasAllRequiredData(contactType));
		}

		[Test]
		public void CanDeleteTest()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].InsuranceTypes;
			if (all.Count == 0) return;
			var type = new InsuranceType(all[0].Id, false);
			Assert.IsTrue(type.KeyLoaded);
			_validator.Messages.Clear();
			var canDeleteTenant = _validator.CanDeleteInsuranceType(type);
			Assert.IsTrue(_validator.Messages.Count > 0);
			Assert.IsFalse(canDeleteTenant);
			_validator.Messages.PrintMessages();
		}
	}
}
