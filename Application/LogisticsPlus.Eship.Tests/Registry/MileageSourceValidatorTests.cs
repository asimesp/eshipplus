﻿using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class MileageSourceValidatorTests
	{
		private MileageSourceValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new MileageSourceValidator();
		}

		[Test]
		public void IsUniqueTest()
		{
			var mileageSource = new MileageSource { Code = "T?", Description = "T", TenantId = GlobalTestInitializer.DefaultTenantId}; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsUnique(mileageSource));
		}

		[Test]
		public void IsValidTest()
		{
			var mileageSource = new MileageSource { Code = "T?", Description = "T", TenantId = GlobalTestInitializer.DefaultTenantId }; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsValid(mileageSource));
		}

		[Test]
		public void PassesPrimaryTest()
		{
			var mileageSource = new MileageSource { Code = "T?", Description = "T", Primary = true, TenantId = GlobalTestInitializer.DefaultTenantId }; // T? will not be found therefore unique
			Assert.IsFalse(_validator.PassesPrimary(mileageSource));
		}

		[Test]
		public void IsExistingAndUniqueTest()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].MileageSources;
			if (all.Count == 0) return;
			var source = all[0];
			Assert.IsTrue(_validator.IsUnique(source)); // this should be unique
			var newSource = new MileageSource(1000000, false);
			Assert.IsFalse(newSource.KeyLoaded); // will not be found
			newSource.Code = source.Code;
			newSource.TenantId = source.TenantId;
			Assert.IsFalse(_validator.IsUnique(newSource)); // should be false
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var mileageSource = new MileageSource();
			Assert.IsFalse(_validator.HasAllRequiredData(mileageSource));
		}

		[Test]
		public void CanDeleteTest()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].MileageSources;
			if (all.Count == 0) return;
			var source = new MileageSource(all[0].Id, false);
			Assert.IsTrue(source.KeyLoaded);
			_validator.Messages.Clear();
			_validator.CanDeleteMileageSource(source);
			_validator.Messages.PrintMessages();
		}
	}
}
