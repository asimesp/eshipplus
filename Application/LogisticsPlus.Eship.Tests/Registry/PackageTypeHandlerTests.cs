﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class PackageTypeHandlerTests
	{
		private PackageTypeView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new PackageTypeView();
			_view.Load();
		}

		[Test]
		public void CanHandlePackageTypeSaveOrUpdate()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].PackageTypes;
			if (all.Count == 0) return;
			var packageType = all[0];
			_view.LockRecord(packageType);
			packageType.TakeSnapShot();
			var typeName = packageType.TypeName;
			packageType.TypeName = DateTime.Now.ToShortDateString();
			_view.SaveRecord(packageType);
			packageType.TakeSnapShot();
			packageType.TypeName = typeName;
			_view.SaveRecord(packageType);
			_view.UnLockRecord(packageType);
		}

		[Test]
		public void CanHandlePackageTypeSaveAndDelete()
		{
			var packageType = new PackageType
			                       	{
										TypeName = DateTime.Now.ToShortDateString(),
										Active = true,
										DefaultLength = 2,
										DefaultHeight = 4,
										DefaultWidth = 2,
										EditableDimensions = true,
										TenantId = _view.ActiveUser.TenantId,
										EdiOid = string.Empty
			                       	};
			_view.SaveRecord(packageType);
			Assert.IsTrue(packageType.Id != default(long));
			_view.DeleteRecord(packageType);
			Assert.IsTrue(!new PackageType(packageType.Id,false).KeyLoaded);

		}

		internal class PackageTypeView : IPackageTypeView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<PackageType>> Save;
			public event EventHandler<ViewEventArgs<PackageType>> Delete;
			public event EventHandler<ViewEventArgs<string>> Search;
		    public event EventHandler<ViewEventArgs<List<PackageType>>> BatchImport;
		    public event EventHandler<ViewEventArgs<PackageType>> Lock;
			public event EventHandler<ViewEventArgs<PackageType>> UnLock;

		    public Dictionary<int, string> Project44Codes
		    {
		        set
		        {
		        }
		    }

            public void FailedLock(Lock @lock)
			{

			}

			public void DisplaySearchResult(List<PackageType> packageType)
			{
				Assert.IsTrue(packageType.Count > 0);
			}

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				foreach (var message in messages)
					Console.WriteLine("Type: {0}, Message; {1}", message.Type, message.Message);
				Assert.IsTrue(messages.Where(m => m.Type != ValidateMessageType.Information).Count() == 0);
			}

			public void Load()
			{
				var handler = new PackageTypeHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(PackageType typeName)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<PackageType>(typeName));
			}

			public void DeleteRecord(PackageType typeName)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<PackageType>(typeName));
			}

			public void LockRecord(PackageType code)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<PackageType>(code));
			}

			public void UnLockRecord(PackageType code)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<PackageType>(code));
			}
		}
	}
}
