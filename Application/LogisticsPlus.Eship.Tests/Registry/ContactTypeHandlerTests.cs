﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class ContactTypeHandlerTests
	{
		private ContactTypeView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new ContactTypeView();
			_view.Load();
		}

		[Test]
		public void CanHandleContactTypeSaveOrUpdate()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].ContactTypes;
			if (all.Count == 0) return;
			var contactType = all[0];
			_view.LockRecord(contactType);
			contactType.TakeSnapShot();
			var code = contactType.Code;
			contactType.Code = DateTime.Now.ToShortDateString();
			_view.SaveRecord(contactType);
			contactType.TakeSnapShot();
			contactType.Code = code;
			_view.SaveRecord(contactType);
			_view.UnLockRecord(contactType);
		}

		[Test]
		public void CanHandleContactTypeSaveAndDelete()
		{
			var contactType = new ContactType
						{
							Code = DateTime.Now.ToShortDateString(),
							Description = "TEST",
							TenantId = _view.ActiveUser.TenantId,
						};
			_view.SaveRecord(contactType);
			_view.LockRecord(contactType);
			Assert.IsTrue(contactType.Id != default(long));
			_view.DeleteRecord(contactType);
			Assert.IsTrue(new ContactType(contactType.Id, false).Code != contactType.Code);

		}

		internal class ContactTypeView : IContactTypeView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public event EventHandler<ViewEventArgs<ContactType>> Save;
			public event EventHandler<ViewEventArgs<ContactType>> Delete;
			public event EventHandler<ViewEventArgs<string>> Search;
		    public event EventHandler<ViewEventArgs<List<ContactType>>> BatchImport;
		    public event EventHandler<ViewEventArgs<ContactType>> Lock;
			public event EventHandler<ViewEventArgs<ContactType>> UnLock;

			public void FailedLock(Lock @lock)
			{

			}

			public void DisplaySearchResult(List<ContactType> contactType)
			{
				Assert.IsTrue(contactType.Count > 0);
			}

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				Assert.IsTrue(messages.Where(m => m.Type != ValidateMessageType.Information).Count() == 0);
			}

			public void Load()
			{
				var handler = new ContactTypeHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(ContactType contactType)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<ContactType>(contactType));
			}

			public void DeleteRecord(ContactType contactType)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<ContactType>(contactType));
			}

			public void SearchRecord(string criteria)
			{
				if (Search != null)
					Search(this, new ViewEventArgs<string>(criteria));
			}

			public void LockRecord(ContactType code)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<ContactType>(code));
			}

			public void UnLockRecord(ContactType code)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<ContactType>(code));
			}
		}
	}
}
