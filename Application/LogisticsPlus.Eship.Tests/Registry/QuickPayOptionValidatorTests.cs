﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class QuickPayOptionValidatorTests
	{
		private QuickPayOptionValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new QuickPayOptionValidator();
		}

		[Test]
		public void IsUniqueTest()
		{
			var quickPayOption = new QuickPayOption {Code = "T?", DiscountPercent = .100m, TenantId = GlobalTestInitializer.DefaultTenantId}; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsUnique(quickPayOption));
		}

		[Test]
		public void IsValidTest()
		{
			var quickPayOption = new QuickPayOption { Code = "T?", DiscountPercent = .100m, TenantId = GlobalTestInitializer.DefaultTenantId }; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsValid(quickPayOption));
		}

		[Test]
		public void IsExistingAndUniqueTest()
		{
			var all = new QuickPayOptionSearch().FetchQuickPayOptions(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var option = all[0];
			Assert.IsTrue(_validator.IsUnique(option)); // this should be unique
			var newOption = new QuickPayOption(1000000, false);
			Assert.IsFalse(newOption.KeyLoaded); // will not be found
			newOption.Code = option.Code;
			newOption.TenantId = option.TenantId;
			Assert.IsFalse(_validator.IsUnique(newOption)); // should be false
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var tenant = new QuickPayOption();
			Assert.IsFalse(_validator.HasAllRequiredData(tenant));
		}

		[Test]
		public void CanDeleteTest()
		{
			var all = new QuickPayOptionSearch().FetchQuickPayOptions(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var option = new QuickPayOption(all[0].Id, false);
			Assert.IsTrue(option.KeyLoaded);
			_validator.Messages.Clear();
			_validator.CanDeleteQuickPayOption(option);
			_validator.Messages.PrintMessages();
		}
	}
}
