﻿using LogisticsPlus.Eship.Processor.Searches.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
    [TestFixture]
    public class DocumentPrefixMapSearchTests
    {
        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

        [Test]
        public void CanFetchByCriteria()
        {
            new DocumentPrefixMapSearch().FetchDocumentPrefixMaps(GlobalTestInitializer.DefaultTenantId); // fetch all

        }
    }
}
