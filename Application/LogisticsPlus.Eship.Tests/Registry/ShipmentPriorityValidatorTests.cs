﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class ShipmentPriorityValidatorTests
	{
		private ShipmentPriorityValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new ShipmentPriorityValidator();
		}

		[Test]
		public void PassesDefaultTest()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].ShipmentPriorities;
			if (all.Count == 0) return;
			var code = all.FirstOrDefault(s => s.Default);
			if (code == null) return;
			var priority = new ShipmentPriority {Code = code.Code, Default = true, TenantId = GlobalTestInitializer.DefaultTenantId};
			Assert.IsFalse(_validator.PassesDefault(priority));
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var priority = new ShipmentPriority();
			Assert.IsFalse(_validator.HasAllRequiredData(priority));
		}

		[Test]
		public void IsUniqueTest()
		{
			var priority = new ShipmentPriority { Code = "T?", Description = "T?", TenantId = GlobalTestInitializer.DefaultTenantId}; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsUnique(priority));
		}

		[Test]
		public void IsValidTest()
		{
			var priority = new ShipmentPriority
									{
										Code = DateTime.Now.ToString(),
										Description = DateTime.Now.ToString(),
										TenantId = GlobalTestInitializer.DefaultTenantId,
										Default = false,
										Active = true,
										PriorityColor = "FFFFFF"
									}; // T? will not be found therefore unique
			// hack in case there is no default in the database
			priority.Default = !_validator.PassesDefault(priority);

			Assert.IsTrue(_validator.IsValid(priority));
		}

		[Test]
		public void CannotMakeDefaultInactive()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].ShipmentPriorities;
			if (all.Count == 0) return;
			var priority = all.FirstOrDefault(p => p.Default);
			if (priority == null) return;
			priority.Active = false;
			Assert.IsTrue(_validator.AttemptToMakeDefaultInActive(priority));
		}

		[Test]
		public void IsExistingAndUniqueTest()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].ShipmentPriorities;
			if (all.Count == 0) return;
			var priority = all[0];
			Assert.IsTrue(_validator.IsUnique(priority)); // this should be unique
			var newPriority = new ShipmentPriority(1000000, false);
			Assert.IsFalse(newPriority.KeyLoaded); // will not be found
			newPriority.Code = priority.Code;
			newPriority.TenantId = priority.TenantId;
			Assert.IsFalse(_validator.IsUnique(newPriority)); // should be false
		}

		[Test]
		public void CanDeleteTest()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].ShipmentPriorities;
			if (all.Count == 0) return;
			var priority = new ShipmentPriority(all[0].Id, false);
			Assert.IsTrue(priority.KeyLoaded);
			_validator.Messages.Clear();
			_validator.CanDeleteShipmentPriority(priority);
			_validator.Messages.PrintMessages();
		}
	}
}
