﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class FailureCodeValidatorTests
	{
		private FailureCodeValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new FailureCodeValidator();
		}

		[Test]
		public void IsValidTest()
		{
			var failureCode = new FailureCode
			                  	{
			                  		Code = DateTime.Now.ToString("yyyyMMddff"),
			                  		TenantId = GlobalTestInitializer.DefaultTenantId,
			                  	};
			Assert.IsTrue(_validator.IsValid(failureCode));
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var failureCode = new FailureCode();
			Assert.IsFalse(_validator.HasAllRequiredData(failureCode));
		}

		[Test]
		public void IsExistingAndUniqueTest()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].FailureCodes;
			if (all.Count == 0) return;
			var failureCode = all[0];
			Assert.IsTrue(_validator.IsUnique(failureCode)); // this should be unique
			var newCode = new FailureCode(1000000, false);
			Assert.IsFalse(newCode.KeyLoaded); // will not be found
			newCode.Code = failureCode.Code;
			newCode.TenantId = failureCode.TenantId;
			Assert.IsFalse(_validator.IsUnique(newCode)); // should be false
		}

        [Test]
        public void CanDeleteTest()
        {
			var criteria = new ShipmentViewSearchCriteria { ActiveUserId = GlobalTestInitializer.DefaultUserId, };

            var all = new ShipmentSearch().FetchShipmentsDashboardDto(criteria, GlobalTestInitializer.DefaultTenantId);
            if(all.Count == 0)return;
            var shipment = new Shipment(all[0].Id);    
            shipment.LoadCollections();
            var shipmentVendor = shipment.Vendors.First(v => v.Primary);
            _validator.Messages.Clear();
            _validator.CanDeleteFailureCode(shipmentVendor.FailureCode);
            _validator.Messages.PrintMessages();
        }
	}
}
