﻿using System.Linq;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class PackageTypeSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchByCriteria()
		{
			var packageTypes = new PackageTypeSearch().FetchPackageTypes(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId); // fetch all
            if (packageTypes.Count < 1) return;
		    
			Assert.IsTrue(packageTypes.Count > 0);
		}

        [Test]
        public void CanFetchByTypeName()
        {
            var packageTypes = new PackageTypeSearch().FetchPackageTypes(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId); //fetch all package types 

            if (packageTypes.Count < 1) return;
            var typeName = packageTypes[0].TypeName;

            var packageTypeId = new PackageTypeSearch().FetchPackageTypeIdByTypeName(typeName, GlobalTestInitializer.DefaultTenantId);

            Assert.IsTrue(packageTypeId > default(long));
        }

		[Test]
		public void CanFetchByEdiOid()
		{
			var packageTypes = new PackageTypeSearch().FetchPackageTypes(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId); //fetch all package types 

			if (packageTypes.Count < 1) return;
			var pt = packageTypes.FirstOrDefault(p => !string.IsNullOrEmpty(p.EdiOid));
			if (pt == null) return;

			var packageTypeId = new PackageTypeSearch().FetchPackageTypeIdByEdiOid(pt.EdiOid, GlobalTestInitializer.DefaultTenantId);

			Assert.IsTrue(packageTypeId > default(long));
		}
	}
}
