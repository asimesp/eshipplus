﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class AccountBucketHandlerTests
	{
		private AccountBucketView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new AccountBucketView();
			_view.Load();
		}

		[Test]
		public void CanHandleAccountBucketSaveOrUpdate()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].AccountBuckets;
			if (all.Count == 0) return;
			var accountBucket = all[0];
            accountBucket.LoadAccountBucketUnits();
			_view.LockRecord(accountBucket);
			accountBucket.TakeSnapShot();
			var code = accountBucket.Code;
			accountBucket.Code = DateTime.Now.ToShortDateString();
			_view.SaveRecord(accountBucket);
			accountBucket.TakeSnapShot();
			accountBucket.Code = code;
			_view.SaveRecord(accountBucket);
			_view.UnLockRecord(accountBucket);
		}

		[Test]
		public void CanHandleAccountBucketSaveAndDelete()
		{
			var accountBucket = new AccountBucket
						{
							Code = DateTime.Now.ToShortDateString(),
							Description = "TEST",
							Active = true,
							TenantId = _view.ActiveUser.TenantId,
						};
		    accountBucket.AccountBucketUnits.Add(new AccountBucketUnit
		        {
		            AccountBucket = accountBucket,
		            TenantId = accountBucket.TenantId,
		            Name = DateTime.Now.ToString(),
		            Description = "TEST",
                    Active = true
		        });

			_view.SaveRecord(accountBucket);
			_view.LockRecord(accountBucket);
			Assert.IsTrue(accountBucket.Id != default(long));
			_view.DeleteRecord(accountBucket);
			Assert.IsTrue(!new AccountBucket(accountBucket.Id, false).KeyLoaded);

		}

		internal class AccountBucketView : IAccountBucketView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public event EventHandler<ViewEventArgs<AccountBucket>> Save;
			public event EventHandler<ViewEventArgs<AccountBucket>> Delete;
            public event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
		    public event EventHandler<ViewEventArgs<List<AccountBucket>>> BatchImport;
		    public event EventHandler<ViewEventArgs<AccountBucket>> Lock;
			public event EventHandler<ViewEventArgs<AccountBucket>> UnLock;

			public void FailedLock(Lock @lock)
			{

			}

			public void DisplaySearchResult(List<AccountBucket> accountBucket)
			{
				Assert.IsTrue(accountBucket.Count > 0);
			}

		    public void LogException(Exception ex)
		    {
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void Load()
			{
				var handler = new AccountBucketHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(AccountBucket accountBucket)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<AccountBucket>(accountBucket));
			}

			public void DeleteRecord(AccountBucket accountBucket)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<AccountBucket>(accountBucket));
			}

            public void SearchRecord(List<ParameterColumn> criteria)
			{
				if (Search != null)
                    Search(this, new ViewEventArgs<List<ParameterColumn>>(criteria));
			}

			public void LockRecord(AccountBucket code)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<AccountBucket>(code));
			}

			public void UnLockRecord(AccountBucket code)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<AccountBucket>(code));
			}
		}
	}
}
