﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class AccountBucketSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchAccountBuckets()
		{
            var columns = new List<ParameterColumn>();

            var field = RegistrySearchFields.Active;

            var column = field.ToParameterColumn();
            column.DefaultValue = true.ToString();
            column.Operator = Operator.Equal;
            columns.Add(column);

            var results = new AccountBucketSearch().FetchAccountBuckets(columns, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(results.Any());
		}

		[Test]
		public void CanFetchByCode()
		{
			var search = new AccountBucketSearch();
			var all = search.FetchAccountBuckets(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId); // fetch all
			if (all.Count == 0) return;
			var accountBucket = search.FetchAccountBucketByCode(all[0].Code, all[0].TenantId);
			Assert.IsNotNull(accountBucket);
		}
	}
}
