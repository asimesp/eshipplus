﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class FailCodeHandlerTests
	{
		private FailCodeView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new FailCodeView();
			_view.Load();
		}

		[Test]
		public void CanHandleFailureCodeSaveOrUpdate()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].FailureCodes;
			if (all.Count == 0) return;
			var failureCode = all[0];
			_view.LockRecord(failureCode);
			failureCode.TakeSnapShot();
			var code = failureCode.Code;
			failureCode.Category = FailureCodeCategory.Freight;
			_view.SaveRecord(failureCode);
			failureCode.TakeSnapShot();
			failureCode.Code = code;
			_view.SaveRecord(failureCode);
			_view.UnLockRecord(failureCode);
		}

        [Test]
        public void CanFailureCodeSaveAndDelete()
        {
            var failureCode = new FailureCode
            {
                TenantId = GlobalTestInitializer.DefaultTenantId,
                Code = DateTime.Now.ToString("yyyyMMddff"),
                Active = true,
                Category = FailureCodeCategory.Freight,
               
            };
            _view.SaveRecord(failureCode);
            Assert.IsTrue(failureCode.Id != default(long));
            _view.DeleteRecord(failureCode);
            Assert.IsTrue(!new ShipmentPriority(failureCode.Id, false).KeyLoaded);

        }

		internal class FailCodeView : IFailureCodeView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

		    public Dictionary<int, string> FailureCodeCategories
		    {
		        set { }
		    }

		    public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<FailureCode>> Save;
		    public event EventHandler<ViewEventArgs<FailureCode>> Delete;
		    public event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
            public event EventHandler<ViewEventArgs<List<FailureCode>>> BatchImport;
            public event EventHandler<ViewEventArgs<FailureCode>> Lock;
            public event EventHandler<ViewEventArgs<FailureCode>> UnLock;

		    public void DisplaySearchResult(List<FailureCode> failureCodes)
		    {
                Assert.IsTrue(failureCodes.Count > 0);
		    }

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.All(m => m.Type == ValidateMessageType.Information));
			}

		    public void FaildedLock(Lock @lock)
		    {
		    }

		    public void Load()
			{
				var handler = new FailureCodeHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(FailureCode failureCode)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<FailureCode>(failureCode));
			}

			public void LockRecord(FailureCode failureCode)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<FailureCode>(failureCode));
			}

			public void UnLockRecord(FailureCode failureCode)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<FailureCode>(failureCode));
			}
            public void DeleteRecord(FailureCode failureCode)
            {
                if (Delete != null)
                    Delete(this, new ViewEventArgs<FailureCode>(failureCode));
            }
		}
	}
}
