﻿using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class InsuranceTypesSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchByCriteria()
		{
			var code = new InsuranceTypeSearch().FetchInsuranceTypes(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId); // fetch all
			Assert.IsTrue(code.Count > 0);
		}
	}
}
