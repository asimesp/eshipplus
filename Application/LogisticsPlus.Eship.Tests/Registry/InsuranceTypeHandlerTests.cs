﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class InsuranceTypeHandlerTests
	{
		private InsuranceTypeView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new InsuranceTypeView();
			_view.Load();
		}

		[Test]
		public void CanHandleInsuranceTypeSaveOrUpdate()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].InsuranceTypes;
			if (all.Count == 0) return;
			var insuranceType = all[0];
			_view.LockRecord(insuranceType);
			insuranceType.TakeSnapShot();
			var code = insuranceType.Code;
			insuranceType.Code = DateTime.Now.ToShortDateString();
			_view.SaveRecord(insuranceType);
			insuranceType.TakeSnapShot();
			insuranceType.Code = code;
			_view.SaveRecord(insuranceType);
			_view.UnLockRecord(insuranceType);
		}

		[Test]
		public void CanHandlensuranceTypeSaveAndDelete()
		{
			var insuranceType = new InsuranceType
						{
							Code = DateTime.Now.ToShortDateString(),
							Description = "TEST",
							TenantId = _view.ActiveUser.TenantId,
						};
			_view.SaveRecord(insuranceType);
			Assert.IsTrue(insuranceType.Id != default(long));
			_view.DeleteRecord(insuranceType);
			Assert.IsTrue(!new InsuranceType(insuranceType.Id, false).KeyLoaded);

		}

		internal class InsuranceTypeView : IInsuranceTypeView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}


			public event EventHandler<ViewEventArgs<InsuranceType>> Save;
			public event EventHandler<ViewEventArgs<InsuranceType>> Delete;
			public event EventHandler<ViewEventArgs<string>> Search;
		    public event EventHandler<ViewEventArgs<List<InsuranceType>>> BatchImport;
		    public event EventHandler<ViewEventArgs<InsuranceType>> Lock;
			public event EventHandler<ViewEventArgs<InsuranceType>> UnLock;

			public void FailedLock(Lock @lock)
			{

			}


			public void DisplaySearchResult(List<InsuranceType> insuranceType)
			{
				Assert.IsTrue(insuranceType.Count > 0);
			}

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				Assert.IsTrue(messages.Where(m => m.Type != ValidateMessageType.Information).Count() == 0);
			}

			public void Load()
			{
				var handler = new InsuranceTypeHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(InsuranceType insuranceType)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<InsuranceType>(insuranceType));
			}

			public void DeleteRecord(InsuranceType insuranceType)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<InsuranceType>(insuranceType));
			}

			public void LockRecord(InsuranceType code)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<InsuranceType>(code));
			}

			public void UnLockRecord(InsuranceType code)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<InsuranceType>(code));
			}
		}
	}
}
