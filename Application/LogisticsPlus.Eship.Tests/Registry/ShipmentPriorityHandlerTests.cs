﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class ShipmentPriorityHandlerTests
	{
		private ShipmentPriorityView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new ShipmentPriorityView();
			_view.Load();
		}

		[Test]
		public void CanHandleShipmentPrioritySaveOrUpdate()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].ShipmentPriorities;
			if (all.Count == 0) return;
			var priority = all[0];
			_view.LockRecord(priority);
			priority.TakeSnapShot();
			var code = priority.Code;

			// hack in case there is not default in the database
			if (!new ShipmentPriorityValidator().PassesDefault(priority) && !priority.Default) priority.Default = true;

			priority.Code = DateTime.Now.ToShortDateString();
			_view.SaveRecord(priority);
			priority.TakeSnapShot();
			priority.Code = code;
			_view.SaveRecord(priority);
			_view.UnLockRecord(priority);
		}

		[Test]
		public void CanHandleShipmentPrioritySaveAndDelete()
		{
			var priority = new ShipmentPriority
			                       	{
			                       		Active = true,
			                       		Code = DateTime.Now.ToShortDateString(),
			                       		Default = false,
			                       		Description = "TEST ???",
			                       		TenantId = _view.ActiveUser.TenantId,
										PriorityColor = "000000"
			                       	};
			// hack in case there is no default in the database
			priority.Default = !new ShipmentPriorityValidator().PassesDefault(priority);
			_view.SaveRecord(priority);
			Assert.IsTrue(priority.Id != default(long));
			_view.DeleteRecord(priority);
			Assert.IsTrue(!new ShipmentPriority(priority.Id,false).KeyLoaded);

		}

		internal class ShipmentPriorityView : IShipmentPriorityView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public bool ForceDefault
			{
				get { return true; }
			}

			public event EventHandler<ViewEventArgs<ShipmentPriority>> Save;
			public event EventHandler<ViewEventArgs<ShipmentPriority>> Delete;
			public event EventHandler<ViewEventArgs<string>> Search;
			public event EventHandler<ViewEventArgs<List<ShipmentPriority>>> BatchImport;
			public event EventHandler<ViewEventArgs<ShipmentPriority>> Lock;
			public event EventHandler<ViewEventArgs<ShipmentPriority>> UnLock;

			public void FailedLock(Lock @lock)
			{
				
			}

			public void DisplaySearchResult(List<ShipmentPriority> shipmentPriorities)
			{
				Assert.IsTrue(shipmentPriorities.Count > 0);
			}

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void Load()
			{
				var handler = new ShipmentPriorityHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(ShipmentPriority code)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<ShipmentPriority>(code));
			}

			public void DeleteRecord(ShipmentPriority code)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<ShipmentPriority>(code));
			}

			public void SearchRecord(string criteria)
			{
				if (Search != null)
					Search(this, new ViewEventArgs<string>(criteria));
			}

			public void LockRecord(ShipmentPriority code)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<ShipmentPriority>(code));
			}

			public void UnLockRecord(ShipmentPriority code)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<ShipmentPriority>(code));
			}
		}
	}
}
