﻿using System;
using System.Diagnostics;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Views.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
    [TestFixture]
    public class PostalCodeSearchTests
    {
        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

        [Test]
        public void CanFetchByCodeAndCountryId()
        {
            new PostalCodeSearch().FetchPostalCodeByCode("16501", GlobalTestInitializer.DefaultCountryId);
        }

		[Test]
		public void CanFetchByCodeAndState()
		{
			new PostalCodeSearch().FetchPostalCodesByCodeAndState("16501", "PA");
		}


        [Test]
        public void SearchTime()
        {
            var watch = new Stopwatch();
            watch.Start();
            var codes = new PostalCodeSearch().FetchPostalCodes(new PostalCodeViewSearchCriteria());
            watch.Stop();

            Console.WriteLine("Time: {0}s, Record Count: {1}", watch.Elapsed.Seconds, codes.Count);

        }

        [Test]
        public void CanFetchPostalCodes()
        {
            var criteria = new PostalCodeViewSearchCriteria();

            var column = RegistrySearchFields.Code.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;

            criteria.Parameters.Add(column);
            var results = new PostalCodeSearch().FetchPostalCodes(criteria);
            Assert.IsTrue(results.Any());
        }

    }
}
