﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
    [TestFixture]
    public class UserDepartmentHandlerTests
    {
        private  UserDepartmentView  _view;
        
        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _view = new UserDepartmentView();
            _view.Load();
        }

        [Test]
        public void CanHandleSaveOrUpdate()
        {
            var all = new UserDepartmentSearch().FetchUserDepartments(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId);
            if (all.Count == 0) return;
            var userDepartment = all[0];
            _view.LockRecord(userDepartment);
            userDepartment.TakeSnapShot();
            var code = userDepartment.Code;
            userDepartment.Code = DateTime.Now.ToShortDateString().Substring(0, 5);
            _view.SaveRecord(userDepartment);
            userDepartment.TakeSnapShot();
            userDepartment.Code = code;
            _view.SaveRecord(userDepartment);
            _view.UnLockRecord(userDepartment);
        }

        [Test]
        public void CanHandleSaveAndDelete()
        {
            var userDepartment = new UserDepartment
            {
                TenantId = _view.ActiveUser.TenantId,
                Code = DateTime.Now.ToShortDateString().Substring(0, 5),
                Description = "TEST",
                Phone = "TEST",
                Fax = "TEST",
                Email = "test@test.com",
                Comments = "TEST",
            };

            _view.SaveRecord(userDepartment);
            Assert.IsTrue(userDepartment.Id != default(long));
            _view.DeleteRecord(userDepartment);
            Assert.IsTrue(!new Prefix(userDepartment.Id, false).KeyLoaded);
        }

        internal class UserDepartmentView : IUserDepartmentView
        {
            public User ActiveUser
            {
                get { return GlobalTestInitializer.ActiveUser; }
            }

            public event EventHandler<ViewEventArgs<UserDepartment>> Save;
            public event EventHandler<ViewEventArgs<UserDepartment>> Delete;
            public event EventHandler<ViewEventArgs<string>> Search;
            public event EventHandler<ViewEventArgs<List<UserDepartment>>> BatchImport;
            public event EventHandler<ViewEventArgs<UserDepartment>> Lock;
            public event EventHandler<ViewEventArgs<UserDepartment>> UnLock;

            public void FailedLock(Lock @lock)
            {

            }

            public void DisplaySearchResult(List<UserDepartment> userDepartments)
            {
                Assert.IsTrue(userDepartments.Count > 0);
            }

            public void LogException(Exception ex)
            {

            }

            public void DisplayMessages(IEnumerable<ValidationMessage> messages)
            {
                Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
            }

            public void Load()
            {
                var handler = new UserDepartmentHandler(this);
                handler.Initialize();
            }

            public void SaveRecord(UserDepartment userDepartment)
            {
                if (Save != null)
                    Save(this, new ViewEventArgs<UserDepartment>(userDepartment));
            }

            public void DeleteRecord(UserDepartment userDepartment)
            {
                if (Delete != null)
                    Delete(this, new ViewEventArgs<UserDepartment>(userDepartment));
            }

            public void LockRecord(UserDepartment userDepartment)
            {
                if (Lock != null)
                    Lock(this, new ViewEventArgs<UserDepartment>(userDepartment));
            }

            public void UnLockRecord(UserDepartment userDepartment)
            {
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<UserDepartment>(userDepartment));
            }
        }
    }
}
