﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class DocumentTagSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
        public void CanFetchDocumentTags()
        {
            var columns = new List<ParameterColumn>();

            var field = RegistrySearchFields.DocumentTags.FirstOrDefault(f => f.Name == "Code");
            if (field == null)
            {
                Console.WriteLine("RegistrySearchFields.DocumentTags no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }

            var column = field.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            columns.Add(column);

            var results = new DocumentTagSearch().FetchDocumentTags(columns, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(results.Any());

        }
	}
}
