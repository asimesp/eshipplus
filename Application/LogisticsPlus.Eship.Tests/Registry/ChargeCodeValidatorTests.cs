﻿using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class ChargeCodeValidatorTests
	{
		private ChargeCodeValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new ChargeCodeValidator();
		}

		[Test]
		public void IsUniqueTest()
		{
			var chargeCode = new ChargeCode
			                     {
			                         Code = "T?", 
                                     Description= "T", 
                                     TenantId = GlobalTestInitializer.DefaultTenantId
			                     }; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsUnique(chargeCode));
		}

		[Test]
		public void IsValidTest()
		{
			var chargeCode = new ChargeCode
			                     {
			                         Code = "T?", 
                                     Description = "T", 
                                     TenantId = GlobalTestInitializer.DefaultTenantId
			                     }; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsValid(chargeCode));

            chargeCode = new ChargeCode
            {
                Code = "T?",
                Description = "T",
                Category = (ChargeCodeCategory)1000,
                TenantId = GlobalTestInitializer.DefaultTenantId
            }; // T? will not be found therefore unique
            Assert.IsFalse(_validator.IsValid(chargeCode));
		}

		[Test]
		public void IsExistingAndUniqueTest()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].ChargeCodes;
			if (all.Count == 0) return;
			var contactType = all[0];
			Assert.IsTrue(_validator.IsUnique(contactType)); // this should be unique
			var newType = new ChargeCode(1000000, false);
			Assert.IsFalse(newType.KeyLoaded); // will not be found
			newType.Code = contactType.Code;
			newType.TenantId = contactType.TenantId;
			Assert.IsFalse(_validator.IsUnique(newType)); // should be false
		}

		[Test]
		public void RequiresTenantTest()
		{
			var chargeCode = new ChargeCode();
			Assert.IsFalse(_validator.HasAllRequiredData(chargeCode));
		}

		[Test]
		public void CanDeleteTest()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].ChargeCodes;
			if (all.Count == 0) return;
			var code = new ChargeCode(all[0].Id, false);
			Assert.IsTrue(code.KeyLoaded);
			_validator.Messages.Clear();
			var canDeleteTenant = _validator.CanDeleteChargeCode(code);
			Assert.IsTrue(_validator.Messages.Count > 0);
			Assert.IsFalse(canDeleteTenant);
			_validator.Messages.PrintMessages();
		}
	}
}
