﻿using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
    [TestFixture
    ]
    public class YrMthBusDayValidatorTests
    {
        private YrMthBusDayValidator _validator;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _validator = new YrMthBusDayValidator();
        }

        [Test]
        public void IsUniqueTest()
        {
            var all = new YrMthBusDaySearch().FetchYrMthBusDays(SearchUtilities.WildCard,GlobalTestInitializer.DefaultTenantId);
            if(!all.Any())return;

            var mthBusDay = new YrMthBusDay
                                {
                                    TenantId = GlobalTestInitializer.DefaultTenantId,
                                    January = 1,
                                    February = 1,
                                    March = 1,
                                    April = 1,
                                    May = 1,
                                    June = 1,
                                    July = 1,
                                    August = 1, 
                                    September = 1,
                                    October = 1,
                                    November = 1,
                                    December = 1,
                                    Year = all[0].Year
                                };
            Assert.IsTrue(_validator.DuplicateYear(mthBusDay));
        }

        [Test]
        public void IsValidTest()
        {
            var mthBusDay = new YrMthBusDay
            {
                TenantId = GlobalTestInitializer.DefaultTenantId,
                January = 1,
                February = 1,
                March = 1,
                April = 1,
                May = 1,
                June = 1,
                July = 1,
                August = 1,
                September = 1,
                October = 1,
                November = 1,
                December = 1,
                Year = DateUtility.SystemEarliestDateTime.AddYears(1).Year.ToString(),
            };

            var result = _validator.IsValid(mthBusDay);
            _validator.Messages.PrintMessages();
            Assert.IsTrue(result);
        }
    }
}