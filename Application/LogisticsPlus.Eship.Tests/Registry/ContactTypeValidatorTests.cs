﻿using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class ContactTypeValidatorTests
	{
		private ContactTypeValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new ContactTypeValidator();
		}

		[Test]
		public void IsUniqueTest()
		{
			var contactType = new ContactType{Code = "T?", Description= "T", TenantId = GlobalTestInitializer.DefaultTenantId}; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsUnique(contactType));
		}

		[Test]
		public void IsValidTest()
		{
			var contactType = new ContactType{Code = "T?", Description= "T", TenantId = GlobalTestInitializer.DefaultTenantId}; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsValid(contactType));
		}

		[Test]
		public void IsExistingAndUniqueTest()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].ContactTypes;
			if (all.Count == 0) return;
			var contactType = all[0];
			Assert.IsTrue(_validator.IsUnique(contactType)); // this should be unique
			var newType = new ContactType(1000000, false);
			Assert.IsFalse(newType.KeyLoaded); // will not be found
			newType.Code = contactType.Code;
			newType.TenantId = contactType.TenantId;
			Assert.IsFalse(_validator.IsUnique(newType)); // should be false
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var contactType = new ContactType();
			Assert.IsFalse(_validator.HasAllRequiredData(contactType));
		}

		[Test]
		public void CanDeleteTest()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].ContactTypes;
			if (all.Count == 0) return;
			var type = new ContactType(all[0].Id, false);
			Assert.IsTrue(type.KeyLoaded);
			_validator.Messages.Clear();
			var canDeleteTenant = _validator.CanDeleteContactType(type);
			Assert.IsTrue(_validator.Messages.Count > 0);
			Assert.IsFalse(canDeleteTenant);
			_validator.Messages.PrintMessages();
		}
	}
}
