﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class DocumentPrefixMapHandlerTests
	{
		private DocumentPrefixMapView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new DocumentPrefixMapView();
			_view.Load();
		}

		[Test]
		public void CanHandleDocumentPrefixMapSaveOrUpdate()
		{
			var all = new DocumentPrefixMapSearch().FetchDocumentPrefixMaps(GlobalTestInitializer.DefaultTenantId);
            var prefixes = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].Prefixes;
			if (all.Count == 0 || prefixes.Count < 2) return;
			var documentPrefixMap = new DocumentPrefixMap(all[0].Id, false);
			_view.LockRecord(documentPrefixMap);
			var prefixChange = prefixes.FirstOrDefault(p => p.Id != documentPrefixMap.PrefixId);
			if (prefixChange == null) return;
			var prefixId = documentPrefixMap.PrefixId;
			documentPrefixMap.TakeSnapShot();
			documentPrefixMap.PrefixId = prefixChange.Id;
			_view.SaveRecord(documentPrefixMap);
			documentPrefixMap.TakeSnapShot();
			documentPrefixMap.PrefixId = prefixId;
			_view.SaveRecord(documentPrefixMap);
			_view.UnLockRecord(documentPrefixMap);
		}

		[Test]
		public void CanHandleDocumentPrefixMapSaveAndDelete()
		{
			var all = new DocumentPrefixMapSearch().FetchDocumentPrefixMaps(GlobalTestInitializer.DefaultTenantId);
            var prefixes = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].Prefixes;
			var templates = new DocumentTemplateSearch().FetchDocumentTemplates(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId);

			if (prefixes.Count == 0 || templates.Count == 0 || all.Count == 0) return;

			var documentPrefixMap = new DocumentPrefixMap
			                        	{
			                        		PrefixId = prefixes[0].Id,
			                        		DocumentTemplateId = templates[0].Id,
											TenantId = _view.ActiveUser.TenantId,
			                        	};
			if (all.Count(m => m.PrefixId == documentPrefixMap.PrefixId && m.DocumentTemplateId == documentPrefixMap.DocumentTemplateId) > 0)
				return;

			_view.SaveRecord(documentPrefixMap);
			_view.LockRecord(documentPrefixMap);
			Assert.IsTrue(documentPrefixMap.Id != default(long));
			_view.DeleteRecord(documentPrefixMap);
			Assert.IsTrue(!new DocumentPrefixMap(documentPrefixMap.Id, false).KeyLoaded);
		}

		internal class DocumentPrefixMapView : IDocumentPrefixMapView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public List<Prefix> Prefixes
			{
				set { }
			}

			public List<DocumentTemplate> DocumentTemplates
			{
				set { }
			}

			public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<DocumentPrefixMap>> Save;
			public event EventHandler<ViewEventArgs<DocumentPrefixMap>> Delete;
			public event EventHandler<ViewEventArgs<DocumentPrefixMapViewSearchCriteria>> Search;
			public event EventHandler<ViewEventArgs<DocumentPrefixMap>> Lock;
			public event EventHandler<ViewEventArgs<DocumentPrefixMap>> UnLock;

			public void FailedLock(Lock @lock)
			{

			}

			public void DisplaySearchResult(List<DocumentPrefixMapViewSearchDto> documentPrefixMap)
			{
				Assert.IsTrue(documentPrefixMap.Count > 0);
			}

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void Load()
			{
				var handler = new DocumentPrefixMapHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(DocumentPrefixMap documentPrefixMap)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<DocumentPrefixMap>(documentPrefixMap));
			}

			public void DeleteRecord(DocumentPrefixMap documentPrefixMap)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<DocumentPrefixMap>(documentPrefixMap));
			}

			public void SearchRecord(DocumentPrefixMapViewSearchCriteria criteria)
			{
				if (Search != null)
					Search(this, new ViewEventArgs<DocumentPrefixMapViewSearchCriteria>(criteria));
			}

			public void LockRecord(DocumentPrefixMap documentPrefixMap)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<DocumentPrefixMap>(documentPrefixMap));
			}

			public void UnLockRecord(DocumentPrefixMap documentPrefixMap)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<DocumentPrefixMap>(documentPrefixMap));
			}
		}
	}
}
