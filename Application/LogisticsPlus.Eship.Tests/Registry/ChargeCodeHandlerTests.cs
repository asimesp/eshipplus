﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class ChargeCodeHandlerTests
	{
		private ChargeCodeView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new ChargeCodeView();
			_view.Load();
		}

		[Test]
		public void CanHandleChargeCodeSaveOrUpdate()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].ChargeCodes;
			if (all.Count == 0) return;
			var chargeCode = all[0];
			_view.LockRecord(chargeCode);
			chargeCode.TakeSnapShot();
			var code = chargeCode.Code;
			chargeCode.Code = DateTime.Now.ToShortDateString();
			_view.SaveRecord(chargeCode);
			chargeCode.TakeSnapShot();
			chargeCode.Code = code;
			_view.SaveRecord(chargeCode);
			_view.UnLockRecord(chargeCode);
		}

		[Test]
		public void CanHandleChargeCodeSaveAndDelete()
		{
			var chargeCode = new ChargeCode
						{
							Code = DateTime.Now.ToShortDateString(),
							Description = "Unit Test",
							Active = true,
							Category = ChargeCodeCategory.Freight,
							TenantId = _view.ActiveUser.TenantId,
                            Project44Code = Project44ChargeCode.ACC
						};

			_view.SaveRecord(chargeCode);
			_view.LockRecord(chargeCode);
			Assert.IsTrue(chargeCode.Id != default(long));
			_view.DeleteRecord(chargeCode);
			Assert.IsTrue(!new ChargeCode(chargeCode.Id, false).KeyLoaded);
		}

		internal class ChargeCodeView : IChargeCodeView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public Dictionary<int, string> Categories
			{
				set {  }
			}

		    public Dictionary<int, string> Project44Codes
		    {
		        set { }
		    }

            public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<ChargeCode>> Save;
			public event EventHandler<ViewEventArgs<ChargeCode>> Delete;
			public event EventHandler<ViewEventArgs<string>> Search;
		    public event EventHandler<ViewEventArgs<List<ChargeCode>>> BatchImport;
		    public event EventHandler<ViewEventArgs<ChargeCode>> Lock;
			public event EventHandler<ViewEventArgs<ChargeCode>> UnLock;

			public void FailedLock(Lock @lock)
			{

			}

			public void DisplaySearchResult(List<ChargeCode> chargeCode)
			{
				Assert.IsTrue(chargeCode.Count > 0);
			}

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Where(m => m.Type != ValidateMessageType.Information).Count() == 0);
			}

			public void Load()
			{
				var handler = new ChargeCodeHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(ChargeCode chargeCode)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<ChargeCode>(chargeCode));
			}

			public void DeleteRecord(ChargeCode chargeCode)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<ChargeCode>(chargeCode));
			}

			public void LockRecord(ChargeCode code)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<ChargeCode>(code));
			}

			public void UnLockRecord(ChargeCode code)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<ChargeCode>(code));
			}
		}
	}
}
