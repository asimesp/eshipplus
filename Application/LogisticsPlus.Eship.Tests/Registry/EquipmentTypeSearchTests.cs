﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class EquipmentTypeSearchTests
	{
		private readonly EquipmentTypeSearch _search = new EquipmentTypeSearch();

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

        [Test]
        public void CanFetchEquipmentTypes()
        {
            var columns = new List<ParameterColumn>();

            var field = RegistrySearchFields.EquipmentTypes.FirstOrDefault(f => f.Name == "TypeName");
            if (field == null)
            {
                Console.WriteLine("RegistrySearchFields.EquipmentTypes no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }

            var column = field.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            columns.Add(column);

            var results = _search.FetchEquipmentTypes(columns, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(results.Any());

        }

		[Test]
		public void CanTestIfEquipmentTypeExists()
		{
			var results = _search.FetchEquipmentTypes(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (!results.Any()) return;
			Assert.IsNotNull(_search.FetchEquipmentTypeByCode(results[0].Code, results[0].TenantId));

		}
	}
}
