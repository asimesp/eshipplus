﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class PrefixHandlerTests
	{
		private PrefixView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new PrefixView();
			_view.Load();
		}

		[Test]
		public void CanHandlePrefixSaveOrUpdate()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].Prefixes;
			if (all.Count == 0) return;
			var prefix = all[0];
			_view.LockRecord(prefix);
			prefix.TakeSnapShot();
			var code = prefix.Code;
			prefix.Code = DateTime.Now.ToShortDateString().Substring(0, 5);
			_view.SaveRecord(prefix);
			prefix.TakeSnapShot();
			prefix.Code = code;
			_view.SaveRecord(prefix);
			_view.UnLockRecord(prefix);
		}

		[Test]
		public void CanHandlePrefixSaveAndDelete()
		{
			var prefix = new Prefix
			             	{
								TenantId = _view.ActiveUser.TenantId,
			             		Code = DateTime.Now.ToShortDateString().Substring(0, 5),
			             		Description = "TEST",
			             		Active = true
			             	};

			_view.SaveRecord(prefix);
			Assert.IsTrue(prefix.Id != default(long));
			_view.DeleteRecord(prefix);
			Assert.IsTrue(!new Prefix(prefix.Id, false).KeyLoaded);
		}

		internal class PrefixView : IPrefixView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public event EventHandler<ViewEventArgs<Prefix>> Save;
			public event EventHandler<ViewEventArgs<Prefix>> Delete;
            public event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
		    public event EventHandler<ViewEventArgs<List<Prefix>>> BatchImport;
		    public event EventHandler<ViewEventArgs<Prefix>> Lock;
			public event EventHandler<ViewEventArgs<Prefix>> UnLock;

			public void FailedLock(Lock @lock)
			{

			}

			public void DisplaySearchResult(List<Prefix> prefix)
			{
				Assert.IsTrue(prefix.Count > 0);
			}

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void Load()
			{
				var handler = new PrefixHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(Prefix code)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<Prefix>(code));
			}

			public void DeleteRecord(Prefix code)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<Prefix>(code));
			}

			public void LockRecord(Prefix code)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<Prefix>(code));
			}

			public void UnLockRecord(Prefix code)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<Prefix>(code));
			}
		}
	}
}
