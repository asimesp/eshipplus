﻿using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class DocumentTemplateSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchByCriteria()
		{
			new DocumentTemplateSearch().FetchDocumentTemplates(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId); // fetch all
		}

		[Test]
		public void CanFetchCurrentPrimaryDocumentTemplate()
		{
			new DocumentTemplateSearch()
				.CurrentPrimaryDocumentTemplate(GlobalTestInitializer.DefaultTenantId, ServiceMode.LessThanTruckload, DocumentTemplateCategory.BillOfLading);
		}

		[Test]
		public void CanFetchByApplicableDocumentTemplate()
		{
			new DocumentTemplateSearch()
				.FetchApplicableDocumentTemplate(GlobalTestInitializer.DefaultTenantId, 0, ServiceMode.LessThanTruckload, DocumentTemplateCategory.BillOfLading);
		}
	}
}
