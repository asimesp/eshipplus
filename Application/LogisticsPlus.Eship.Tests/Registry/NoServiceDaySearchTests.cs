﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class NoServiceDaySearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

        [Test]
        public void CanFetchForFinder()
        {
            var columns = new List<ParameterColumn>();

            var column = RegistrySearchFields.Year.ToParameterColumn();
            column.DefaultValue = "2013";
            column.Operator = Operator.GreaterThanOrEqual;

            var field = RegistrySearchFields.NoServiceDays.FirstOrDefault(f => f.Name == "Description");
            if (field == null)
            {
                Console.WriteLine("RegistrySearchFields.NoServiceDays no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }

            column = field.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            columns.Add(column);

            var results = new NoServiceDaySearch().FetchNoServiceDays(columns, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(results.Any());
        }

		[Test]
		public void CanFetchByYear()
		{
			new NoServiceDaySearch().FetchYearNoServiceDays(DateTime.Now.Year.ToString(), GlobalTestInitializer.DefaultTenantId); // fetch all
		}
	}
}
