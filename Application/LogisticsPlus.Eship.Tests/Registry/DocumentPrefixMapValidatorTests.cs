﻿using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class DocumentPrefixMapValidatorTests
	{
		private DocumentPrefixMapValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new DocumentPrefixMapValidator();
		}

		[Test]
		public void IsUniqueTest()
		{
			var documentPrefixMap = new DocumentPrefixMap { PrefixId = 99999, DocumentTemplateId = 99999, TenantId = 99999}; // 99999 will not be found therefore unique
			Assert.IsTrue(_validator.IsUnique(documentPrefixMap));
		}
		
		[Test]
		public void IsExistingAndUniqueTest()
		{
			var all = new DocumentPrefixMapSearch().FetchDocumentPrefixMaps(GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var option = new DocumentPrefixMap(all[0].Id, false);
			Assert.IsTrue(_validator.IsUnique(option)); // this should be unique
			var newOption = new DocumentPrefixMap(1000000, false);
			Assert.IsFalse(newOption.KeyLoaded); // will not be found
			newOption.PrefixId = option.PrefixId;
			newOption.DocumentTemplateId = option.DocumentTemplateId;
			newOption.TenantId = option.TenantId;
			Assert.IsFalse(_validator.IsUnique(newOption)); // should be false
		}

		[Test]
		public void IsValidTest()
		{
			var documentPrefixMap = new DocumentPrefixMap { PrefixId = 99999, DocumentTemplateId = 99999, TenantId = 99999 }; // 99999 will not be found therefore unique
			Assert.IsTrue(_validator.IsValid(documentPrefixMap));
		}

		[Test]
		public void RequiresTenantTest()
		{
			var documentPrefixMap = new DocumentPrefixMap();
			Assert.IsFalse(_validator.HasAllRequiredData(documentPrefixMap));
		}
	}
}
