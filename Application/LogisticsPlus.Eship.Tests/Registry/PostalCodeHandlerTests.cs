﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class PostalCodeHandlerTests
	{
		private PostalCodeView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new PostalCodeView();
			_view.Load();
		}

		[Test]
		public void CanHandlePostalCodeSaveOrUpdate()
		{
			var all = new PostalCodeSearch().FetchPostalCodes(new PostalCodeViewSearchCriteria());
			if (all.Count == 0) return;
			var postalCode = new PostalCode(all[0].Id);
            postalCode.TakeSnapShot();
			var code = postalCode.Code;
			postalCode.Code = DateTime.Now.ToShortDateString();
			_view.SaveRecord(postalCode);
            postalCode.TakeSnapShot();
			postalCode.Code = code;
			_view.SaveRecord(postalCode);
		}

		[Test]
		public void CanHandlePostalCodeSaveAndDelete()
		{
			var code = new PostalCode
						{
							City = "Erie",
							CityAlias = "Erie",
							Code = "TEST02",
							CountryId = 1,
							Primary = true,
							State = "PA"
						};
			_view.SaveRecord(code);
			Assert.IsTrue(code.Id != default(long));
			_view.DeleteRecord(code);
			Assert.IsTrue(!new PostalCode(code.Id).KeyLoaded);

		}

		internal class PostalCodeView : IPostalCodeView
		{
			public AdminUser ActiveSuperUser
			{
				get { return GlobalTestInitializer.ActiveAdminUser; }
			}

			public bool ForcePrimary
			{
				get { return true; }
			}

			public List<Country> Countries { set { Assert.IsTrue(value.Count > 0); } }

			public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<PostalCode>> Save;
			public event EventHandler<ViewEventArgs<PostalCode>> Delete;
			public event EventHandler<ViewEventArgs<List<PostalCode>>> BatchImport;
			public event EventHandler<ViewEventArgs<PostalCodeViewSearchCriteria>> Search;
            public event EventHandler ExportAllPostalCodes;


			public void DisplaySearchResult(List<PostalCodeViewSearchDto> postalCodes)
			{
				Assert.IsTrue(postalCodes.Count > 0);
			}

            public void ExportPostalCodes(List<PostalCodeViewSearchDto> postalCodes)
            {
                
            }

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void Load()
			{
				var handler = new PostalCodeHandler(this);
				handler.Initialize();

				if (Loading != null)
					Loading(this, new EventArgs());
			}

			public void SaveRecord(PostalCode code)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<PostalCode>(code));
			}

			public void DeleteRecord(PostalCode code)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<PostalCode>(code));
			}

            public void SearchRecord(PostalCodeViewSearchCriteria criteria)
			{
				if (Search != null)
					Search(this, new ViewEventArgs<PostalCodeViewSearchCriteria>(criteria));
			}

			public void BatchImportRecord(List<PostalCode> postalCodes)
			{
				if(BatchImport!=null)
					BatchImport(this, new ViewEventArgs<List<PostalCode>>(postalCodes));
			}
		}
	}
}
