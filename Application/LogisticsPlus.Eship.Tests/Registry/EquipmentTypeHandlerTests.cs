﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class EquipmentTypeHandlerTests
	{
		private EquipmentTypeView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new EquipmentTypeView();
			_view.Load();
		}

		[Test]
		public void CanHandleEquipmentTypeSaveOrUpdate()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].EquipmentTypes;
			if (all.Count == 0) return;
			var equipmentType = all[0];
			_view.LockRecord(equipmentType);
			equipmentType.TakeSnapShot();
			var typeName = equipmentType.TypeName;
			equipmentType.TypeName = DateTime.Now.ToShortDateString();
			_view.SaveRecord(equipmentType);
			equipmentType.TakeSnapShot();
			equipmentType.TypeName = typeName;
			_view.SaveRecord(equipmentType);
			_view.UnLockRecord(equipmentType);
		}

		[Test]
		public void CanHandleEquipmentTypeSaveAndDelete()
		{
			var equipmentType = new EquipmentType
						{
							TenantId = _view.ActiveUser.TenantId,
							TypeName = DateTime.Now.ToShortDateString(),
							Code = DateTime.Now.ToString("ffff"),
							Active = true,
                            DatEquipmentType = DatEquipmentType.NotApplicable,
                            Group = EquipmentTypeGroup.MiscAccessorial
						};
			_view.SaveRecord(equipmentType);
			_view.LockRecord(equipmentType);
			Assert.IsTrue(equipmentType.Id != default(long));
			_view.DeleteRecord(equipmentType);
			Assert.IsTrue(!new EquipmentType(equipmentType.Id, false).KeyLoaded);
		}

		internal class EquipmentTypeView : IEquipmentTypeView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

		    public Dictionary<int, string> DatEquipmentTypes { set; private get; }

		    public Dictionary<int, string> Groups
			{
				set {  }
			}

			public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<EquipmentType>> Save;
			public event EventHandler<ViewEventArgs<EquipmentType>> Delete;
            public event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
			public event EventHandler<ViewEventArgs<EquipmentType>> Lock;
			public event EventHandler<ViewEventArgs<EquipmentType>> UnLock;
            public event EventHandler<ViewEventArgs<List<EquipmentType>>> BatchImport;

			public void FailedLock(Lock @lock)
			{

			}

			public void DisplaySearchResult(List<EquipmentType> equipmentType)
			{
				Assert.IsTrue(equipmentType.Count > 0);
			}

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void Load()
			{
				var handler = new EquipmentTypeHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(EquipmentType equipmentType)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<EquipmentType>(equipmentType));
			}

			public void DeleteRecord(EquipmentType equipmentType)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<EquipmentType>(equipmentType));
			}

			public void LockRecord(EquipmentType code)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<EquipmentType>(code));
			}

			public void UnLockRecord(EquipmentType code)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<EquipmentType>(code));
			}
		}
	}
}
