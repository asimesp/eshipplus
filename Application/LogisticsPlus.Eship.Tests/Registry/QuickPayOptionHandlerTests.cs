﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class QuickPayOptionHandlerTests
	{
		private QuickPayOptionView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new QuickPayOptionView();
			_view.Load();
		}

		[Test]
		public void CanHandleQuickPayOptionSaveOrUpdate()
		{
			var all = new QuickPayOptionSearch().FetchQuickPayOptions(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var quickPayOption = all[0];
			_view.LockRecord(quickPayOption);
			quickPayOption.TakeSnapShot();
			var code = quickPayOption.Code;
			quickPayOption.Code = DateTime.Now.ToShortDateString();
			_view.SaveRecord(quickPayOption);
			quickPayOption.TakeSnapShot();
			quickPayOption.Code = code;
			_view.SaveRecord(quickPayOption);
			_view.UnLockRecord(quickPayOption);
		}

		[Test]
		public void CanHandleQuickPayOptionSaveAndDelete()
		{
			var quickPayOption = new QuickPayOption
						{
							Code = DateTime.Now.ToShortDateString(),
							Description = "TEST",
							DiscountPercent = 5,
							Active = true,
							TenantId = _view.ActiveUser.TenantId,
						};
			_view.SaveRecord(quickPayOption);
			Assert.IsTrue(quickPayOption.Id != default(long));
			_view.DeleteRecord(quickPayOption);
			Assert.IsTrue(!new QuickPayOption(quickPayOption.Id, false).KeyLoaded);

		}

		internal class QuickPayOptionView : IQuickPayOptionView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public event EventHandler<ViewEventArgs<QuickPayOption>> Save;
			public event EventHandler<ViewEventArgs<QuickPayOption>> Delete;
            public event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
		    public event EventHandler<ViewEventArgs<List<QuickPayOption>>> BatchImport;
		    public event EventHandler<ViewEventArgs<QuickPayOption>> Lock;
			public event EventHandler<ViewEventArgs<QuickPayOption>> UnLock;


			public void FailedLock(Lock @lock)
			{

			}

			public void DisplaySearchResult(List<QuickPayOption> country)
			{
				Assert.IsTrue(country.Count > 0);
			}

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void Load()
			{
				var handler = new QuickPayOptionHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(QuickPayOption quickPayOption)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<QuickPayOption>(quickPayOption));
			}

			public void DeleteRecord(QuickPayOption quickPayOption)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<QuickPayOption>(quickPayOption));
			}

			public void LockRecord(QuickPayOption code)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<QuickPayOption>(code));
			}

			public void UnLockRecord(QuickPayOption code)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<QuickPayOption>(code));
			}
		}
	}
}
