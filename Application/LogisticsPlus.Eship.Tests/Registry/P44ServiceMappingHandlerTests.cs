﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using LogisticsPlus.Eship.WebApplication.Members.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class P44ServiceMappingHandlerTests
    {
		private P44ServiceMappingView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new P44ServiceMappingView();
			_view.Load();
		}

		[Test]
		public void CanHandleServiceSaveOrUpdate()
		{
			var all = new P44ServiceMappingSearch().FetchAllP44ServiceMappings(GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var p44ServiceMapping = new P44ServiceMapping(all[0].Id, false);
			_view.LockRecord(p44ServiceMapping);
            p44ServiceMapping.TakeSnapShot();
			long serviceId = p44ServiceMapping.ServiceId;
            p44ServiceMapping.ServiceId = 24;
			_view.SaveRecord(p44ServiceMapping);
            p44ServiceMapping.TakeSnapShot();
            p44ServiceMapping.Project44Code = Project44ServiceCode.ACCELERATED;
			_view.SaveRecord(p44ServiceMapping);
			_view.UnLockRecord(p44ServiceMapping);
		}

		[Test]
		public void CanHandleServiceSaveAndDelete()
		{
            var codes = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].ChargeCodes;
			if (codes.Count == 0) return;
			var p44ServiceMapping = new P44ServiceMapping()
			              	{
			              		Project44Code = Project44ServiceCode.ACCELERATED,
			              		ServiceId = 38,
								TenantId = GlobalTestInitializer.DefaultTenantId
			              	};
			_view.SaveRecord(p44ServiceMapping);
			Assert.IsTrue(p44ServiceMapping.Id != default(long));
			_view.DeleteRecord(p44ServiceMapping);
			Assert.IsTrue(new P44ServiceMapping(p44ServiceMapping.Id, false).Project44Code != p44ServiceMapping.Project44Code);
		}

		internal class P44ServiceMappingView : IP44ServiceView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public List<ChargeCode> ChargeCodes
			{
				set { Assert.IsTrue(value.Count > 0); }
			}

			public Dictionary<int, string> Categories
			{
				set { Assert.IsTrue(value.Count > 0); }
			}


		    public Dictionary<int, string> DispatchFlag
		    {
		        set { Assert.IsTrue(value.Count > 0); }
		    }

		    public Dictionary<int, string> Project44Codes
		    {
                set
                { }
		    }

            public List<Service> ServiceCodes
            {
                set
                { }
            }

            public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<P44ServiceMapping>> Save;
			public event EventHandler<ViewEventArgs<P44ServiceMapping>> Delete;
            public event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
		    public event EventHandler<ViewEventArgs<List<P44ServiceMapping>>> BatchImport;
		    public event EventHandler<ViewEventArgs<P44ServiceMapping>> Lock;
			public event EventHandler<ViewEventArgs<P44ServiceMapping>> UnLock;

			public void FaildedLock(Lock @lock)
			{

			}

			public void DisplaySearchResult(List<ServiceViewSearchDto> service)
			{
				Assert.IsTrue(service.Count > 0);
			}

            public void DisplaySearchResult(List<P44ServiceMapping> service)
            {
                throw new NotImplementedException();
            }

            public void LogException(Exception ex)
		    {
		                
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
                messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void Load()
			{
				var handler = new P44ServiceHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(P44ServiceMapping p44ServiceMapping)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<P44ServiceMapping>(p44ServiceMapping));
			}

			public void DeleteRecord(P44ServiceMapping p44ServiceMapping)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<P44ServiceMapping>(p44ServiceMapping));
			}

			public void LockRecord(P44ServiceMapping code)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<P44ServiceMapping>(code));
			}

			public void UnLockRecord(P44ServiceMapping code)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<P44ServiceMapping>(code));
			}
        }
	}
}
