﻿using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class PermissionDetailValidatorTest
	{
		private PermissionDetailValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new PermissionDetailValidator();
		}

		[Test]
		public void IsUniqueTest()
		{
			var detail = new PermissionDetail
				{
					Code = "T?",
					DisplayCode = "T",
					NavigationUrl = "www.google.com",
					Category = PermissionCategory.Operations,
					Type = PermissionType.System,
					TenantId = GlobalTestInitializer.DefaultTenantId
				}; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsUnique(detail));
		}

		[Test]
		public void IsValidTest()
		{
			var detail = new PermissionDetail
			{
				Code = "T?",
				DisplayCode = "T",
				NavigationUrl = "www.google.com",
				Category = PermissionCategory.Operations,
				Type = PermissionType.System,
				TenantId = GlobalTestInitializer.DefaultTenantId
			}; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsValid(detail));

			detail = new PermissionDetail
				{
					Code = "T?",
					DisplayCode = "T",
					NavigationUrl = "www.google.com",
					Category = (PermissionCategory) 1000,
					Type = PermissionType.System,
					TenantId = GlobalTestInitializer.DefaultTenantId
				}; // T? will not be found therefore unique
			Assert.IsFalse(_validator.IsValid(detail));
		}

		[Test]
		public void IsExistingAndUniqueTest()
		{
			var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].PermissionDetails;
			if (all.Count == 0) return;
			var details = all[0];
			Assert.IsTrue(_validator.IsUnique(details)); // this should be unique
			var newType = new PermissionDetail(1000000, false);
			Assert.IsFalse(newType.KeyLoaded); // will not be found
			newType.Code = details.Code;
			newType.TenantId = details.TenantId;
			Assert.IsFalse(_validator.IsUnique(newType)); // should be false
		}

		[Test]
		public void RequiresTenantTest()
		{
			var detail = new PermissionDetail();
			Assert.IsFalse(_validator.HasAllRequiredData(detail));
		}
	}
}
