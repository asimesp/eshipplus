﻿using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class PrefixValidatorTests
	{
		private PrefixValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new PrefixValidator();
		}

		[Test]
		public void IsUniqueTest()
		{
			var prefix = new Prefix { Code = "T?", Description = "T?", TenantId = GlobalTestInitializer.DefaultTenantId, Active = true }; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsUnique(prefix));
		}

		[Test]
		public void IsValidTest()
		{
			var prefix = new Prefix { Code = "T?", Description = "T?", TenantId = GlobalTestInitializer.DefaultTenantId, Active = true }; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsValid(prefix));
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var prefix = new Prefix();
			Assert.IsFalse(_validator.HasAllRequiredData(prefix));
		}

		[Test]
		public void IsExistingAndUniqueTest()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].Prefixes;
			if (all.Count == 0) return;
			var prefix = all[0];
			Assert.IsTrue(_validator.IsUnique(prefix)); // this should be unique
			var newPrefix = new Prefix(1000000, false);
			Assert.IsFalse(newPrefix.KeyLoaded); // will not be found
			newPrefix.Code = prefix.Code;
			newPrefix.TenantId = prefix.TenantId;
			Assert.IsFalse(_validator.IsUnique(newPrefix)); // should be false
		}

		[Test]
		public void CanDeleteTest()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].Prefixes;
			if (all.Count == 0) return;
			var prefix = new Prefix(all[0].Id, false);
			Assert.IsTrue(prefix.KeyLoaded);
			_validator.Messages.Clear();
			_validator.CanDeletePrefix(prefix);
		}
	}
}
