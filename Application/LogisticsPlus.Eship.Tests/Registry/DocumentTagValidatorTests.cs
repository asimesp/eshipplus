﻿using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class DocumentTagValidatorTests
	{
		private DocumentTagValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new DocumentTagValidator();
		}

		[Test]
		public void IsUniqueTest()
		{
			var documentTag = new DocumentTag { Code = "T?", Description = "T", TenantId = GlobalTestInitializer.DefaultTenantId}; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsUnique(documentTag));
		}

		[Test]
		public void IsValidTest()
		{
			var documentTag = new DocumentTag { Code = "T?", Description = "T", TenantId = GlobalTestInitializer.DefaultTenantId }; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsValid(documentTag));
		}

		[Test]
		public void IsExistingAndUniqueTest()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].DocumentTags;
			if (all.Count == 0) return;
			var tag = all[0];
			Assert.IsTrue(_validator.IsUnique(tag)); // this should be unique
			var newTag = new DocumentTag(1000000, false);
			Assert.IsFalse(newTag.KeyLoaded); // will not be found
			newTag.Code = tag.Code;
			newTag.TenantId = tag.TenantId;
			Assert.IsFalse(_validator.IsUnique(newTag)); // should be false
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var documentTag = new DocumentTag();
			Assert.IsFalse(_validator.HasAllRequiredData(documentTag));
		}

		[Test]
		public void CanDeleteTest()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].DocumentTags;
			if (all.Count == 0) return;
			var tag = new DocumentTag(all[0].Id, false);
			Assert.IsTrue(tag.KeyLoaded);
			_validator.Messages.Clear();
			_validator.CanDeleteDocumentTag(tag);
			_validator.Messages.PrintMessages();
		}
	}
}
