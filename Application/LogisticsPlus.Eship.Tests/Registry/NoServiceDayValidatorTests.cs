﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class NoServiceDayValidatorTests
	{
		private NoServiceDayValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new NoServiceDayValidator();
		}

		[Test]
		public void IsUniqueTest()
		{
			var noServiceDay = new NoServiceDay
			                   	{
			                   		DateOfNoService = Convert.ToDateTime("2011/01/12"), 
									Description = "Non-Holiday", 
									TenantId = GlobalTestInitializer.DefaultTenantId
			                   	};
			Assert.IsTrue(_validator.IsUnique(noServiceDay));
		}

		[Test]
		public void IsValidTest()
		{
			var noServiceDay = new NoServiceDay
			{
				DateOfNoService = Convert.ToDateTime("2011/01/12"),
				Description = "Non-Holiday",
				TenantId = GlobalTestInitializer.DefaultTenantId
			};
			Assert.IsTrue(_validator.IsValid(noServiceDay));
		}

		[Test]
		public void IsExistingAndUniqueTest()
		{
			var all = new NoServiceDaySearch().FetchNoServiceDays(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var day = all[0];
			Assert.IsTrue(_validator.IsUnique(day)); // this should be unique
			var newTag = new NoServiceDay(1000000, false);
			Assert.IsFalse(newTag.KeyLoaded); // will not be found
			newTag.DateOfNoService = day.DateOfNoService;
			newTag.TenantId = day.TenantId;
			Assert.IsFalse(_validator.IsUnique(newTag)); // should be false
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var noServiceDay = new NoServiceDay();
			Assert.IsFalse(_validator.HasAllRequiredData(noServiceDay));
		}

		[Test]
		public void CanDeleteTest()
		{
			var all = new NoServiceDaySearch().FetchNoServiceDays(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var day = new NoServiceDay(all[0].Id, false);
			Assert.IsTrue(day.KeyLoaded);
			_validator.Messages.Clear();
			_validator.CanDeleteNoServiceDay(day);
			_validator.Messages.PrintMessages();
		}
	}
}
