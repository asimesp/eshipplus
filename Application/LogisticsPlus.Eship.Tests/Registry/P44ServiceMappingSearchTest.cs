﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
    [TestFixture]
    public class P44ServiceMappingSearchTest
    {
        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

        [Test]
        public void CanFetchByParametersP44ServiceMappings()
        {
            var p44ServiceMappings = new P44ServiceMappingSearch().FetchP44ServiceMappings(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId); // all
            Assert.IsTrue(p44ServiceMappings.Count > 0);
        }

        [Test]
        public void CanFetchAllP44ServiceMappings()
        {
            var services = new P44ServiceMappingSearch().FetchAllP44ServiceMappings(GlobalTestInitializer.DefaultTenantId); // all
            Assert.IsTrue(services.Count > 0);
        }

        [Test]
        public void CanFetchP44ServiceMappingsWithParameterColumns()
        {
            var columns = new List<ParameterColumn>();

            var field = RegistrySearchFields.P44ChargeCodeMappings.FirstOrDefault(f => f.Name == "Project44Code");
            if (field == null)
            {
                Console.WriteLine("RegistrySearchFields.Services no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }

            var column = field.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;

            columns.Add(column);

            var services = new P44ServiceMappingSearch().FetchP44ServiceMappings(columns, GlobalTestInitializer.DefaultTenantId); // all
            Assert.IsTrue(services.Count > 0);
        }

        [Test]
        public void CanFetchP44ServiceMappingsByProject44Code()
        {
            new P44ServiceMappingSearch().FetchP44ServiceMappingIdByProject44Code(Project44ChargeCode.ACC.ToInt(), GlobalTestInitializer.DefaultTenantId);
        }
    }
}
