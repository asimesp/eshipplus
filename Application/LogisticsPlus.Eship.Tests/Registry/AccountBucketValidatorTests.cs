﻿using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class AccountBucketValidatorTests
	{
		private AccountBucketValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new AccountBucketValidator();
		}

		[Test]
		public void IsUniqueTest()
		{
			var accountBucket = new AccountBucket{Code = "T?", Description= "T", TenantId = GlobalTestInitializer.DefaultTenantId}; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsUnique(accountBucket));
		}

		[Test]
		public void IsExistingAndUniqueTest()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].AccountBuckets;
			if (all.Count == 0) return;
			var option = all[0];
			Assert.IsTrue(_validator.IsUnique(option)); // this should be unique
			var newOption = new AccountBucket(1000000, false);
			Assert.IsFalse(newOption.KeyLoaded); // will not be found
			newOption.Code = option.Code;
			newOption.TenantId = option.TenantId;
			Assert.IsFalse(_validator.IsUnique(newOption)); // should be false
		}

		[Test]
		public void IsValidTest()
		{
			var accountBucket = new AccountBucket { Code = "T?", Description = "T", TenantId = GlobalTestInitializer.DefaultTenantId }; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsValid(accountBucket));
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var accountBucket = new AccountBucket();
			Assert.IsFalse(_validator.HasAllRequiredData(accountBucket));
		}

		[Test]
		public void CanDeleteTest()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].AccountBuckets;
			if (all.Count == 0) return;
			var accountBucket = new AccountBucket(all[0].Id, false);
			Assert.IsTrue(accountBucket.KeyLoaded);
			_validator.Messages.Clear();
			_validator.CanDeleteAccountBucket(accountBucket);
			_validator.Messages.PrintMessages();
		}
	}
}
