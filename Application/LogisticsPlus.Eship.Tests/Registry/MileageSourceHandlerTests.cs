﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class MileageSourceHandlerTests
	{
		private MileageSourceView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new MileageSourceView();
			_view.Load();
		}

		[Test]
		public void CanHandleMileageSourceSaveOrUpdate()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].MileageSources;
			if (all.Count == 0) return;
			var mileageSource = all[0];
			_view.LockRecord(mileageSource);
			mileageSource.TakeSnapShot();
			var code = mileageSource.Code;
			mileageSource.Code = DateTime.Now.ToShortDateString();
			_view.SaveRecord(mileageSource);
			mileageSource.TakeSnapShot();
			mileageSource.Code = code;
			_view.SaveRecord(mileageSource);
			_view.UnLockRecord(mileageSource);
		}

		[Test]
		public void CanHandleMileageSourceSaveAndDelete()
		{
			var mileageSource = new MileageSource
						{
							Code = DateTime.Now.ToShortDateString(),
							Description = "TEST",
							Primary = false,
							TenantId = _view.ActiveUser.TenantId,
						};
			_view.SaveRecord(mileageSource);
			Assert.IsTrue(mileageSource.Id != default(long));
			_view.DeleteRecord(mileageSource);
			Assert.IsTrue(!new MileageSource(mileageSource.Id, false).KeyLoaded);

		}

		internal class MileageSourceView : IMileageSourceView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public bool ForcePrimary
			{
				get { return true; }
			}

			public Dictionary<int, string> MileageEngines
			{
				set { }
			}

			public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<MileageSource>> Save;
			public event EventHandler<ViewEventArgs<MileageSource>> Delete;
			public event EventHandler<ViewEventArgs<string>> Search;
		    public event EventHandler<ViewEventArgs<List<MileageSource>>> BatchImport;
		    public event EventHandler<ViewEventArgs<MileageSource>> Lock;
			public event EventHandler<ViewEventArgs<MileageSource>> UnLock;

			public void FailedLock(Lock @lock)
			{

			}

			public void DisplaySearchResult(List<MileageSource> mileageSources)
			{
				Assert.IsTrue(mileageSources.Count > 0);
			}

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				Assert.IsTrue(messages.Where(m => m.Type != ValidateMessageType.Information).Count() == 0);
			}

			public void Load()
			{
				var handler = new MileageSourceHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(MileageSource mileageSource)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<MileageSource>(mileageSource));
			}

			public void DeleteRecord(MileageSource mileageSource)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<MileageSource>(mileageSource));
			}

			public void LockRecord(MileageSource code)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<MileageSource>(code));
			}

			public void UnLockRecord(MileageSource code)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<MileageSource>(code));
			}
		}
	}
}
