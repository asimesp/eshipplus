﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class PrefixSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchPrefixes()
		{
            var columns = new List<ParameterColumn>();

            var field = RegistrySearchFields.QuickPayOptions.FirstOrDefault(f => f.Name == "Code");
            if (field == null)
            {
                Console.WriteLine("RegistrySearchFields.Prefixes no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }

            var column = field.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            columns.Add(column);

            var results = new PrefixSearch().FetchPrefixes(columns, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(results.Any());
		}

		[Test]
		public void CanFetchByBode()
		{
			var search = new PrefixSearch();
			var all = search.FetchPrefixes(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var prefix = search.FetchPrefixByCode(all[0].Code, all[0].TenantId);
			Assert.IsNotNull(prefix);
		}
	}
}
