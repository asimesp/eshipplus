﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class ServiceValidatorTests
	{
		private ServiceValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new ServiceValidator();
		}

		[Test]
		public void IsUniqueTest()
		{
			var service = new Service{Code = "T?", Description= "T", TenantId = GlobalTestInitializer.DefaultTenantId}; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsUnique(service));
		}

		[Test]
		public void IsExistingAndUniqueTest()
		{
			var all = new ServiceSearch().FetchServices(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var service = new Service(all[0].Id, false);
			Assert.IsTrue(_validator.IsUnique(service)); // this should be unique
			var newService = new Service(1000000, false);
			Assert.IsFalse(newService.KeyLoaded); // will not be found
			newService.Code = service.Code;
			newService.TenantId = service.TenantId;
			Assert.IsFalse(_validator.IsUnique(newService)); // should be false
		}

        [Test]
        public void IsValidTest()
        {
            var service = new Service
                              {
                                  Code = "T?",
                                  Description = "T",
                                  TenantId = GlobalTestInitializer.DefaultTenantId,
                                  ChargeCodeId = GlobalTestInitializer.DefaultChargeCodeId,
                                  Category = ServiceCategory.Freight,
                              }; // T? will not be found therefore unique
            Assert.IsTrue(_validator.IsValid(service));
        }

	    [Test]
		public void HasAllRequiredDataTest()
		{
			var service = new Service();
			Assert.IsFalse(_validator.HasAllRequiredData(service));

            service = new Service
                          {TenantId = GlobalTestInitializer.DefaultTenantId,
                              Code = "test",
                              Description = "test",
                              Category = (ServiceCategory)1000
                          };
            Assert.IsFalse(_validator.IsValid(service));
		}

		[Test]
		public void CanDeleteTest()
		{
			var all = new ServiceSearch().FetchServices(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var service = new Service(all[0].Id, false);
			Assert.IsTrue(service.KeyLoaded);
			_validator.Messages.Clear();
			var canDeleteTenant = _validator.CanDeleteService(service);
			Assert.IsTrue(_validator.Messages.Count > 0);
			Assert.IsFalse(canDeleteTenant);
			_validator.Messages.PrintMessages();
		}
	}
}

