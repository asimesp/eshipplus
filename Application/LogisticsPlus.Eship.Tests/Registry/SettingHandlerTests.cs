﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class SettingHandlerTests
	{
		private SettingView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new SettingView();
			_view.Load();
		}

		[Test]
		public void CanHandleSettingSaveOrUpdate()
		{
			var all = new SettingSearch().FetchSettings(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var setting = all[0];
			_view.LockRecord(setting);
			setting.TakeSnapShot();
			var code = setting.Code;
			setting.Code = SettingCode.SpecialistSupportEmail;
			_view.SaveRecord(setting);
			setting.TakeSnapShot();
			setting.Code = code;
			_view.SaveRecord(setting);
			_view.UnLockRecord(setting);
		}

		internal class SettingView : ISettingView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

		    public Dictionary<int, string> Codes
		    {
		        set { }
		    }

		    public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<Setting>> Save;
			public event EventHandler<ViewEventArgs<string>> Search;
		    public event EventHandler<ViewEventArgs<Setting>> Lock;
			public event EventHandler<ViewEventArgs<Setting>> UnLock;

			public void FailedLock(Lock @lock)
			{

			}

			public void DisplaySearchResult(List<Setting> setting)
			{
				Assert.IsTrue(setting.Count > 0);
			}

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void Load()
			{
				var handler = new SettingHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(Setting setting)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<Setting>(setting));
			}

			public void SearchRecord(string criteria)
			{
				if (Search != null)
					Search(this, new ViewEventArgs<string>(criteria));
			}

			public void LockRecord(Setting code)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<Setting>(code));
			}

			public void UnLockRecord(Setting code)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<Setting>(code));
			}
		}
	}
}
