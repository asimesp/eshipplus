﻿using System.Linq;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
    [TestFixture]
    public class YrMthBusDaySearchTests
    {
        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

        [Test]
        public void CanFetchByCriteria()
        {
            var results = new YrMthBusDaySearch().FetchYrMthBusDays(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId); // fetch all
            Assert.IsTrue(results.Any());
        }
    }
}