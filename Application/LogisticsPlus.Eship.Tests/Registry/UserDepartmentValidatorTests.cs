﻿using System;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
    public class UserDepartmentValidatorTests
	{
		private UserDepartmentValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _validator = new UserDepartmentValidator();
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
		    var userDepartment = new UserDepartment
		                             {
                                         TenantId = GlobalTestInitializer.DefaultTenantId,
                                         Code = DateTime.Now.ToString(),
                                         Description = DateTime.Now.ToString(),
		                             };
            Assert.IsTrue(_validator.IsValid(userDepartment));
		}

        [Test]
        public void IsExistingAndUniqueTest()
        {
            var all = new UserDepartmentSearch().FetchUserDepartments(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId);
            if (all.Count == 0) return;
            var userDepartment = all[0];
            Assert.IsTrue(_validator.IsValid(userDepartment)); // this should be unique
            var newUserDepartment = new UserDepartment(1000000, false);
            Assert.IsFalse(newUserDepartment.KeyLoaded); // will not be found
            newUserDepartment = new UserDepartment
                                        {
                                            Code = userDepartment.Code, 
                                            TenantId = userDepartment.TenantId,
                                            Description = DateTime.Now.ToString()
                                        };
            Assert.IsFalse(_validator.IsValid(newUserDepartment)); // should be false
        }

		[Test]
		public void CanDeleteTest()
		{
			var all = new UserDepartmentSearch().FetchUserDepartments(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var department = new UserDepartment(all[0].Id, false);
			Assert.IsTrue(department.KeyLoaded);
			_validator.Messages.Clear();
			_validator.CanDeleteDepartment(department);
		}
	}
}
