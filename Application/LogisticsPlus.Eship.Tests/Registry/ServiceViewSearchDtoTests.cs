﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class ServiceViewSearchDtoTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void RetrieveVendorServicesTests()
		{
			var all = new VendorSearch().FetchVendors(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			var vendor = all.FirstOrDefault(v => v.Services.Count > 0);
			if (vendor == null) return;
			new ServiceViewSearchDto().RetrieveVendorServices(vendor);
		}

		[Test]
		public void RetrievedAddressBookServices()
		{
			var criteria = new AddressBookViewSearchCriteria { CustomerId = 0, UserId = GlobalTestInitializer.DefaultUserId };
			var all = new AddressBookViewSearchDto()
				.FetchAddressBookDtos(GlobalTestInitializer.DefaultTenantId, criteria)
				.Select(i => new AddressBook(i.Id));
			var addressBook = all.FirstOrDefault(a => a.Services.Count > 0);
			if (addressBook == null) return;
			new ServiceViewSearchDto().RetrieveAddressBookServices(addressBook);
		}
	}
}
