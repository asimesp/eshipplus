﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class P44ServiceMappingValidatorTests
    {
		private P44ServiceValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new P44ServiceValidator();
		}

		[Test]
		public void IsUniqueTest()
		{
			var p44ServiceMapping = new P44ServiceMapping(){Project44Code = Project44ServiceCode.ACCELERATED, ServiceId= 2, TenantId = GlobalTestInitializer.DefaultTenantId}; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsUnique(p44ServiceMapping));
		}

		[Test]
		public void IsExistingAndUniqueTest()
		{
			var all = new P44ServiceMappingSearch().FetchP44ServiceMappings(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var p44ServiceMapping = new P44ServiceMapping(all[0].Id, false);
			Assert.IsTrue(_validator.IsUnique(p44ServiceMapping)); // this should be unique
			var newP44ServiceMapping = new P44ServiceMapping(1000000, false);
			Assert.IsFalse(newP44ServiceMapping.KeyLoaded); // will not be found
            newP44ServiceMapping.Project44Code = p44ServiceMapping.Project44Code;
            newP44ServiceMapping.TenantId = p44ServiceMapping.TenantId;
			Assert.IsFalse(_validator.IsUnique(newP44ServiceMapping)); // should be false
		}

        [Test]
        public void IsValidTest()
        {
            var p44ServiceMapping = new P44ServiceMapping
            {
                                  Project44Code = Project44ServiceCode.ACCELERATED,
                                  ServiceId = 2,
                                  TenantId = GlobalTestInitializer.DefaultTenantId
                              }; // T? will not be found therefore unique
            Assert.IsTrue(_validator.IsValid(p44ServiceMapping));
        }

	    [Test]
		public void HasAllRequiredDataTest()
		{
			var p44ServiceMapping = new P44ServiceMapping();
			Assert.IsFalse(_validator.HasAllRequiredData(p44ServiceMapping));

            p44ServiceMapping = new P44ServiceMapping
                          {TenantId = GlobalTestInitializer.DefaultTenantId,
                              Project44Code = Project44ServiceCode.ACCELERATED,
                              ServiceId = 4
                          };
            Assert.IsFalse(_validator.IsValid(p44ServiceMapping));
		}

		[Test]
		public void CanDeleteTest()
		{
			var all = new ServiceSearch().FetchServices(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var p44ServiceMapping = new P44ServiceMapping(all[0].Id, false);
			Assert.IsTrue(p44ServiceMapping.KeyLoaded);
			_validator.Messages.Clear();
		}
	}
}

