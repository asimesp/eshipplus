﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class ServiceHandlerTests
	{
		private ServiceView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new ServiceView();
			_view.Load();
		}

		[Test]
		public void CanHandleServiceSaveOrUpdate()
		{
			var all = new ServiceSearch().FetchServices(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var service = new Service(all[0].Id, false);
			_view.LockRecord(service);
			service.TakeSnapShot();
			string code = service.Code;
			service.Code = DateTime.Now.ToShortDateString();
			_view.SaveRecord(service);
			service.TakeSnapShot();
			service.Code = code;
			_view.SaveRecord(service);
			_view.UnLockRecord(service);
		}

		[Test]
		public void CanHandleServiceSaveAndDelete()
		{
            var codes = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].ChargeCodes;
			if (codes.Count == 0) return;
			var service = new Service
			              	{
			              		Code = DateTime.Now.ToShortDateString(),
			              		Description = "Test",
			              		Category = ServiceCategory.Service,
			              		ChargeCodeId = codes[0].Id,
								TenantId = GlobalTestInitializer.DefaultTenantId,
                                Project44Code = Project44ServiceCode.ACCELERATED
			              	};
			_view.SaveRecord(service);
			Assert.IsTrue(service.Id != default(long));
			_view.DeleteRecord(service);
			Assert.IsTrue(new Service(service.Id, false).Code != service.Code);
		}

		internal class ServiceView : IServiceView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public List<ChargeCode> ChargeCodes
			{
				set { Assert.IsTrue(value.Count > 0); }
			}

			public Dictionary<int, string> Categories
			{
				set { Assert.IsTrue(value.Count > 0); }
			}


		    public Dictionary<int, string> DispatchFlag
		    {
		        set { Assert.IsTrue(value.Count > 0); }
		    }

		    public Dictionary<int, string> Project44Codes
		    {
                set
                { }
		    }

            public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<Service>> Save;
			public event EventHandler<ViewEventArgs<Service>> Delete;
            public event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
		    public event EventHandler<ViewEventArgs<List<Service>>> BatchImport;
		    public event EventHandler<ViewEventArgs<Service>> Lock;
			public event EventHandler<ViewEventArgs<Service>> UnLock;

			public void FaildedLock(Lock @lock)
			{

			}

			public void DisplaySearchResult(List<ServiceViewSearchDto> service)
			{
				Assert.IsTrue(service.Count > 0);
			}

		    public void LogException(Exception ex)
		    {
		                
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
                messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void Load()
			{
				var handler = new ServiceHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(Service service)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<Service>(service));
			}

			public void DeleteRecord(Service service)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<Service>(service));
			}

			public void LockRecord(Service code)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<Service>(code));
			}

			public void UnLockRecord(Service code)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<Service>(code));
			}
		}
	}
}
