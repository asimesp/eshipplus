﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class DocumentTemplateHandlerTests
	{
		private DocumentTemplateView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new DocumentTemplateView();
			_view.Load();
		}

		[Test]
		public void CanHandleDocumentTemplateSaveOrUpdate()
		{
			var all = new DocumentTemplateSearch().FetchDocumentTemplates(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var documentTemplate = all[0];
			_view.LockRecord(documentTemplate);
			documentTemplate.TakeSnapShot();
			var code = documentTemplate.Code;
			documentTemplate.Code = DateTime.Now.ToShortDateString();
			_view.SaveRecord(documentTemplate);
			documentTemplate.TakeSnapShot();
			documentTemplate.Code = code;
			_view.SaveRecord(documentTemplate);
			_view.UnLockRecord(documentTemplate);
		}

		[Test]
		public void CanHandleDocumentTemplateSaveAndDelete()
		{
			var documentTemplate = new DocumentTemplate
			                       	{
			                       		TemplatePath = "UnitTestPath",
			                       		Code = DateTime.Now.ToShortDateString(),
			                       		Description = "UnitTestDescription",
			                       		Category = DocumentTemplateCategory.BillOfLading,
			                       		ServiceMode = ServiceMode.LessThanTruckload,
			                       		Primary = false,
										TenantId = _view.ActiveUser.TenantId,
										WeightDecimals = DecimalPlacesUtility.DecimalPlaces()[0],
										DimensionDecimals = DecimalPlacesUtility.DecimalPlaces()[0],
										CurrencyDecimals = DecimalPlacesUtility.DecimalPlaces()[0]
			                       	};
			_view.SaveRecord(documentTemplate);
			_view.LockRecord(documentTemplate);
			Assert.IsTrue(documentTemplate.Id != default(long));
			_view.DeleteRecord(documentTemplate);
			Assert.IsTrue(!new DocumentTemplate(documentTemplate.Id, false).KeyLoaded);
		}

		internal class DocumentTemplateView : IDocumentTemplateView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public bool ForceDefault
			{
				get { return false; }
			}

			public Dictionary<int, string> Categories
			{
				set { }
			}

			public Dictionary<int, string> ServiceModes
			{
				set { }
			}

			public List<string> DecimalPlaces
			{
				set {  }
			}

			public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<DocumentTemplate>> Save;
			public event EventHandler<ViewEventArgs<DocumentTemplate>> Delete;
			public event EventHandler<ViewEventArgs<string>> Search;
		    public event EventHandler<ViewEventArgs<DocumentTemplate>> Lock;
			public event EventHandler<ViewEventArgs<DocumentTemplate>> UnLock;

			public void FailedLock(Lock @lock)
			{

			}

			public void DisplaySearchResult(List<DocumentTemplate> documentTemplate)
			{
				Assert.IsTrue(documentTemplate.Count > 0);
			}

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void Load()
			{
				var handler = new DocumentTemplateHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(DocumentTemplate documentTemplate)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<DocumentTemplate>(documentTemplate));
			}

			public void DeleteRecord(DocumentTemplate documentTemplate)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<DocumentTemplate>(documentTemplate));
			}

			public void LockRecord(DocumentTemplate code)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<DocumentTemplate>(code));
			}

			public void UnLockRecord(DocumentTemplate code)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<DocumentTemplate>(code));
			}

            public bool TemplateFileHasBeenSaved()
            {
                return true;
            }
		}
	}
}
