﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class DocumentTagHandlerTests
	{
		private DocumentTagView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new DocumentTagView();
			_view.Load();
		}

		[Test]
		public void CanHandleDocumentTagSaveOrUpdate()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].DocumentTags;
			if (all.Count == 0) return;
			var documentTag = all[0];
			_view.LockRecord(documentTag);
			documentTag.TakeSnapShot();
			var code = documentTag.Code;
			documentTag.Code = DateTime.Now.ToShortDateString();
			_view.SaveRecord(documentTag);
			documentTag.TakeSnapShot();
			documentTag.Code = code;
			_view.SaveRecord(documentTag);
			_view.UnLockRecord(documentTag);
		}

		[Test]
		public void CanHandleDocumentTagSaveAndDelete()
		{
			var documentTag = new DocumentTag
						{
							Code = DateTime.Now.ToShortDateString(),
							Description = "TEST",
                            NonEmployeeVisible = true,
							TenantId = _view.ActiveUser.TenantId,
						};
			_view.SaveRecord(documentTag);
			_view.LockRecord(documentTag);
			Assert.IsTrue(documentTag.Id != default(long));
			_view.DeleteRecord(documentTag);
			Assert.IsTrue(new DocumentTag(documentTag.Id, false).Code != documentTag.Code);

		}

		internal class DocumentTagView : IDocumentTagView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public event EventHandler<ViewEventArgs<DocumentTag>> Save;
			public event EventHandler<ViewEventArgs<DocumentTag>> Delete;
            public event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
		    public event EventHandler<ViewEventArgs<List<DocumentTag>>> BatchImport;
		    public event EventHandler<ViewEventArgs<DocumentTag>> Lock;
			public event EventHandler<ViewEventArgs<DocumentTag>> UnLock;

			public void FailedLock(Lock @lock)
			{

			}

			public void DisplaySearchResult(List<DocumentTag> documentTag)
			{
				Assert.IsTrue(documentTag.Count > 0);
			}

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void Load()
			{
				var handler = new DocumentTagHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(DocumentTag documentTag)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<DocumentTag>(documentTag));
			}

			public void DeleteRecord(DocumentTag documentTag)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<DocumentTag>(documentTag));
			}

			public void LockRecord(DocumentTag code)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<DocumentTag>(code));
			}

			public void UnLockRecord(DocumentTag code)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<DocumentTag>(code));
			}
		}
	}
}
