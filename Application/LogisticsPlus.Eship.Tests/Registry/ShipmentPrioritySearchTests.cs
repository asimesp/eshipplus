﻿using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class ShipmentPrioritySearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchByCriteria()
		{
			var priorities = new ShipmentPrioritySearch().FetchShipmentPriorities(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(priorities.Count > 0);
		}

		[Test]
		public void CanFetchByCode()
		{
			var search = new ShipmentPrioritySearch();
			var all = search.FetchShipmentPriorities(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var priority = search.FetchShipmentPriorityByCode(all[0].Code, all[0].TenantId);
			Assert.IsNotNull(priority);
		}

		[Test]
		public void CanFetchCurrentDefault()
		{
			var priority = new ShipmentPrioritySearch().CurrentDefaultShipmentPriority(GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(priority != null);
		}
	}
}
