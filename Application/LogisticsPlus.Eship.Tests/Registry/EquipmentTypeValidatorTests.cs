﻿using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class EquipmentTypeValidatorTests
	{
		private EquipmentTypeValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new EquipmentTypeValidator();
		}

		[Test]
		public void IsUniqueTest()
		{
			var equipmentType = new EquipmentType
			{
				TypeName = "T?",
				Code = "C?",
				Active = true,
				Group = EquipmentTypeGroup.DryVan,
                DatEquipmentType = DatEquipmentType.NotApplicable,
                TenantId = GlobalTestInitializer.DefaultTenantId
			}; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsUnique(equipmentType));
		}

		[Test]
		public void IsValidTest()
		{
			var equipmentType = new EquipmentType
			{
				Code = "c?",
				TypeName = "T?",
				Active = true,
				Group = EquipmentTypeGroup.Specialized,
                DatEquipmentType = DatEquipmentType.NotApplicable,
				TenantId = GlobalTestInitializer.DefaultTenantId
			}; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsValid(equipmentType));

            equipmentType = new EquipmentType
            {
                TypeName = "T?",
				Code = "c?",
                Active = true,
				Group = (EquipmentTypeGroup)1001,
                DatEquipmentType = DatEquipmentType.LowboyorRGN,
                TenantId = GlobalTestInitializer.DefaultTenantId
            }; // T? will not be found therefore unique
		    Assert.IsFalse(_validator.IsValid(equipmentType));
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var tenant = new EquipmentType();
			Assert.IsFalse(_validator.HasAllRequiredData(tenant));
		}

		[Test]
		public void IsExistingAndUniqueTest()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].EquipmentTypes;
			if (all.Count == 0) return;
			var option = all[0];
			Assert.IsTrue(_validator.IsUnique(option)); // this should be unique
			var newOption = new EquipmentType(1000000, false);
			Assert.IsFalse(newOption.KeyLoaded); // will not be found
			newOption.Code = option.Code;
			newOption.TenantId = option.TenantId;
			Assert.IsFalse(_validator.IsUnique(newOption)); // should be false
		}

		[Test]
		public void CanDeleteTest()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].EquipmentTypes;
			if (all.Count == 0) return;
			var type = new EquipmentType(all[0].Id, false);
			Assert.IsTrue(type.KeyLoaded);
			_validator.Messages.Clear();
			var canDeleteTenant = _validator.CanDeleteEquipmentType(type);
			Assert.IsTrue(_validator.Messages.Count > 0);
			Assert.IsFalse(canDeleteTenant);
			_validator.Messages.PrintMessages();
		}
	}
}
