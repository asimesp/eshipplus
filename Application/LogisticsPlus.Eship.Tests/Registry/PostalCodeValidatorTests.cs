﻿using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using LogisticsPlus.Eship.Processor.Views.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class PostalCodeValidatorTests
	{
		private PostalCodeValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new PostalCodeValidator();
		}

		[Test]
		public void IsUniqueTest()
		{
			var code = new PostalCode{CountryId = 1, Code = "165??", CityAlias = "AliasTest"}; // 165?? will not be found therefore unique
			Assert.IsTrue(_validator.IsUnique(code));
		}

		[Test]
		public void IsExistingAndUniqueTest()
		{
			var all = new PostalCodeSearch().FetchPostalCodes(new PostalCodeViewSearchCriteria());
			if (all.Count == 0) return;
			var code = new PostalCode(all[0].Id);
			Assert.IsTrue(_validator.IsUnique(code)); // this should be unique
			var newCode = new PostalCode(1000000);
			Assert.IsFalse(newCode.KeyLoaded); // will not be found
			newCode.Code = code.Code;
			newCode.CityAlias = code.CityAlias;
			newCode.CountryId = code.CountryId;
			Assert.IsFalse(_validator.IsUnique(newCode)); // should be false
		}

		[Test]
		public void PassesPrimaryTest()
		{
			var code = new PostalCode {CountryId = 1, Code = "16501", Primary = true}; // code with 16501 expected to already be primary
			Assert.IsFalse(_validator.PassesPrimary(code));
		}

		[Test]
		public void IsValidTest()
		{
			var code = new PostalCode
			{
				CountryId = GlobalTestInitializer.DefaultCountryId,
				Code = "165??",
				City = "Erie",
				CityAlias = "Erie",
				State = "PA",
				Primary = true
			}; // 165?? will not be found therefore unique
			Assert.IsTrue(_validator.IsValid(code));
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var code = new PostalCode();
			Assert.IsFalse(_validator.HasAllRequiredData(code));
		}
	}
}
