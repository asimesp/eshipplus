﻿using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class MileageSourceSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchByCriteria()
		{
			var code = new MileageSourceSearch().FetchMileageSources(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId); // fetch all
			Assert.IsTrue(code.Count > 0);
		}

		[Test]
		public void CanFetchByCode()
		{
			var search = new MileageSourceSearch();
			var all = search.FetchMileageSources(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId); // fetch all
			if (all.Count == 0) return;
			var id = search.FetchMileageSourceIdByCode(all[0].Code, all[0].TenantId);
			Assert.IsTrue(id > default(long));
		}
	}
}
