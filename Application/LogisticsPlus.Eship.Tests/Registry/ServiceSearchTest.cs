﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class ServiceSearchTest
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchByParametersServices()
		{
			var services = new ServiceSearch().FetchServices(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId); // all
			Assert.IsTrue(services.Count > 0);
		}

		[Test]
		public void CanFetchAllServices()
		{
			var services = new ServiceSearch().FetchAllServices(GlobalTestInitializer.DefaultTenantId); // all
			Assert.IsTrue(services.Count > 0);
		}

        [Test]
        public void CanFetchServicesWithParameterColumns()
        {
            var columns = new List<ParameterColumn>();

            var field = RegistrySearchFields.Services.FirstOrDefault(f => f.Name == "Description");
            if (field == null)
            {
                Console.WriteLine("RegistrySearchFields.Services no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }

            var column = field.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;

            columns.Add(column);

            var services = new ServiceSearch().FetchServices(columns, GlobalTestInitializer.DefaultTenantId); // all
            Assert.IsTrue(services.Count > 0);
        }

		[Test]
		public void CanFetchServicesByCategory()
		{
			new ServiceSearch().FetchServicesByCategory(ServiceCategory.Freight, GlobalTestInitializer.DefaultTenantId);
		}

		[Test]
		public void CanFetchIdbyCode()
		{
			var search = new ServiceSearch();
			var all = search.FetchServices(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId); // all
			if (all.Count == 0) return;
			var s = search.FetchServiceIdByCode(all[0].Code, GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(s > default(long));
		}
	}
}
