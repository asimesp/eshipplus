﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
    [TestFixture]
    public class P44ChargeCodeMappingSearchTest
    {
        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

        [Test]
        public void CanFetchByParametersP44ChangeCodeMappings()
        {
            var p44ChargeCodeMappings = new P44ChargeCodeMappingSearch().FetchP44ChargeCodeMappings(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId); // all
            Assert.IsTrue(p44ChargeCodeMappings.Count > 0);
        }

        [Test]
        public void CanFetchAllP44ChargeCodeMappings()
        {
            var ChargeCodes = new P44ChargeCodeMappingSearch().FetchAllP44ChargeCodeMappings(GlobalTestInitializer.DefaultTenantId); // all
            Assert.IsTrue(ChargeCodes.Count > 0);
        }

        [Test]
        public void CanFetchP44ChargeCodeMappingsWithParameterColumns()
        {
            var columns = new List<ParameterColumn>();

            var field = RegistrySearchFields.P44ChargeCodeMappings.FirstOrDefault(f => f.Name == "Project44Code");
            if (field == null)
            {
                Console.WriteLine("RegistrySearchFields.ChargeCodes no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }

            var column = field.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;

            columns.Add(column);

            var ChargeCodes = new P44ChargeCodeMappingSearch().FetchP44ChargeCodeMappings(columns, GlobalTestInitializer.DefaultTenantId); // all
            Assert.IsTrue(ChargeCodes.Count > 0);
        }

        [Test]
        public void CanFetchP44ChargeCodeMappingsByProject44Code()
        {
            new P44ChargeCodeMappingSearch().FetchP44ChargeCodeMappingIdByProject44Code(Project44ChargeCode.ACC.ToInt(), GlobalTestInitializer.DefaultTenantId);
        }
    }
}
