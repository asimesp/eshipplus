﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class PermissionDetailHandlerTests
	{
		private PermissionDetailView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new PermissionDetailView();
			_view.Load();
		}

		[Test]
		public void CanHandlePermissionDetailSaveOrUpdate()
		{
			var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].PermissionDetails;
			if (all.Count == 0) return;
			var detail = all[0];
			_view.LockRecord(detail);
			detail.TakeSnapShot();
			var code = detail.Code;
			detail.Code = DateTime.Now.ToShortDateString();
			_view.SaveRecord(detail);
			detail.TakeSnapShot();
			detail.Code = code;
			_view.SaveRecord(detail);
			_view.UnLockRecord(detail);
		}

		[Test]
		public void CanHandlePermissionDetailSaveAndDelete()
		{
			var detail = new PermissionDetail
				{
					Code = DateTime.Now.ToShortDateString(),
					DisplayCode = "Unit Test",
					Category = PermissionCategory.Operations,
					Type = PermissionType.System,
					NavigationUrl = "www.google.com",
					TenantId = _view.ActiveUser.TenantId,
				};

			_view.SaveRecord(detail);
			_view.LockRecord(detail);
			Assert.IsTrue(detail.Id != default(long));
			_view.DeleteRecord(detail);
			Assert.IsTrue(!new PermissionDetail(detail.Id, false).KeyLoaded);
		}

		internal class PermissionDetailView : IPermissionDetailView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public Dictionary<int, string> Categories
			{
				set { }
			}

			public Dictionary<int, string> Types
			{
				set { }
			}

			public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<PermissionDetail>> Save;
			public event EventHandler<ViewEventArgs<PermissionDetail>> Delete;
			public event EventHandler<ViewEventArgs<string>> Search;
			public event EventHandler<ViewEventArgs<List<PermissionDetail>>> BatchImport;
			public event EventHandler<ViewEventArgs<PermissionDetail>> Lock;
			public event EventHandler<ViewEventArgs<PermissionDetail>> UnLock;

			public void FailedLock(Lock @lock)
			{

			}

			public void DisplaySearchResult(List<PermissionDetail> details)
			{
				Assert.IsTrue(details.Count > 0);
			}

			public void LogException(Exception ex)
			{

			}

			public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.All(m => m.Type == ValidateMessageType.Information));
			}

			public void Load()
			{
				var handler = new PermissionDetailHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(PermissionDetail detail)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<PermissionDetail>(detail));
			}

			public void DeleteRecord(PermissionDetail detail)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<PermissionDetail>(detail));
			}

			public void LockRecord(PermissionDetail detail)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<PermissionDetail>(detail));
			}

			public void UnLockRecord(PermissionDetail detail)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<PermissionDetail>(detail));
			}
		}
	}
}
