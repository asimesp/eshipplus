﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class CountryHandlerTests
	{
		private CountryView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new CountryView();
			_view.Load();
		}

		[Test]
		public void CanHandleCountrySaveOrUpdate()
		{
            var all = ProcessorVars.RegistryCache.Countries.Values.ToList();
			if (all.Count == 0) return;
			var country = all[0];
            country.TakeSnapShot();
			var code = country.Code;
			country.Code = DateTime.Now.ToShortDateString().Substring(0, 2);
			_view.SaveRecord(country);
            country.TakeSnapShot();
			country.Code = code;
			_view.SaveRecord(country);
		}

		[Test]
		public void CanHandleCountrySaveAndDelete()
		{
			var country = new Country
			              	{
			              		Code = DateTime.Now.ToShortDateString().Substring(0, 2),
			              		Name = "TEST",
			              		EmploysPostalCodes = true,
								PostalCodeValidation = string.Empty
			              	};
			_view.SaveRecord(country);
			Assert.IsTrue(country.Id != default(long));
			_view.DeleteRecord(country);
			Assert.IsTrue(new Country(country.Id).Code != country.Code);

		}

		internal class CountryView : ICountryView
		{
			public AdminUser ActiveSuperUser
			{
				get { return GlobalTestInitializer.ActiveAdminUser; }
			}

		    public Dictionary<int, string> Project44CountryCodes
		    {
		        set
		        {
		        }
		    }

            public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<Country>> Save;
			public event EventHandler<ViewEventArgs<Country>> Delete;
			public event EventHandler<ViewEventArgs<string>> Search;
		    public event EventHandler<ViewEventArgs<List<Country>>> BatchImport;

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplaySearchResult(List<Country> country)
			{
				Assert.IsTrue(country.Count > 0);
			}

			public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
				messages.PrintMessages();
			}

			public void Load()
			{
				var handler = new CountryHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(Country country)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<Country>(country));
			}

			public void DeleteRecord(Country country)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<Country>(country));
			}
		}
	}
}
