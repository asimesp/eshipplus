﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class P44ChargeCodeMappingValidatorTests
    {
		private P44ChargeCodeValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new P44ChargeCodeValidator();
		}

		[Test]
		public void IsUniqueTest()
		{
			var p44ChargeCodeMapping = new P44ChargeCodeMapping(){Project44Code = Project44ChargeCode.ACCELERATED, ChargeCodeId= 2, TenantId = GlobalTestInitializer.DefaultTenantId}; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsUnique(p44ChargeCodeMapping));
		}

		[Test]
		public void IsExistingAndUniqueTest()
		{
			var all = new P44ChargeCodeMappingSearch().FetchP44ChargeCodeMappings(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var p44ChargeCodeMapping = new P44ChargeCodeMapping(all[0].Id, false);
			Assert.IsTrue(_validator.IsUnique(p44ChargeCodeMapping)); // this should be unique
			var newP44ChargeCodeMapping = new P44ChargeCodeMapping(1000000, false);
			Assert.IsFalse(newP44ChargeCodeMapping.KeyLoaded); // will not be found
            newP44ChargeCodeMapping.Project44Code = p44ChargeCodeMapping.Project44Code;
            newP44ChargeCodeMapping.TenantId = p44ChargeCodeMapping.TenantId;
			Assert.IsFalse(_validator.IsUnique(newP44ChargeCodeMapping)); // should be false
		}

        [Test]
        public void IsValidTest()
        {
            var p44ChargeCodeMapping = new P44ChargeCodeMapping
            {
                                  Project44Code = Project44ChargeCode.ACCELERATED,
                                  ChargeCodeId = 2,
                                  TenantId = GlobalTestInitializer.DefaultTenantId
                              }; // T? will not be found therefore unique
            Assert.IsTrue(_validator.IsValid(p44ChargeCodeMapping));
        }

	    [Test]
		public void HasAllRequiredDataTest()
		{
			var p44ChargeCodeMapping = new P44ChargeCodeMapping();
			Assert.IsFalse(_validator.HasAllRequiredData(p44ChargeCodeMapping));

            p44ChargeCodeMapping = new P44ChargeCodeMapping
                          {TenantId = GlobalTestInitializer.DefaultTenantId,
                              Project44Code = Project44ChargeCode.ACCELERATED,
                              ChargeCodeId = 4
                          };
            Assert.IsFalse(_validator.IsValid(p44ChargeCodeMapping));
		}

		[Test]
		public void CanDeleteTest()
		{
			var all = new ServiceSearch().FetchServices(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var p44ChargeCodeMapping = new P44ChargeCodeMapping(all[0].Id, false);
			Assert.IsTrue(p44ChargeCodeMapping.KeyLoaded);
			_validator.Messages.Clear();
        }
	}
}

