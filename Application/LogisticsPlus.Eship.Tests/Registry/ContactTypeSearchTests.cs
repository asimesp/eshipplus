﻿using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class ContactTypeSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchByCriteria()
		{
			var code = new ContactTypeSearch().FetchContactTypes(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId); // fetch all
			Assert.IsTrue(code.Count > 0);
		}

		[Test]
		public void CanFetchByCode()
		{
			var codes = new ContactTypeSearch().FetchContactTypes(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId); // fetch all
			if (codes.Count > 1) return;
			
			var code = codes[0].Code;
			var results = new ContactTypeSearch().FetchContactTypeIdByCode(code, GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(results > 0);
		}
	}
}
