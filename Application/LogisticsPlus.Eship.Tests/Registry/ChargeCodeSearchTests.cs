﻿using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class ChargeCodeSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchByCriteria()
		{
			var code = new ChargeCodeSearch().FetchChargeCodes(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId); 
			Assert.IsTrue(code.Count > 0);
		}

		[Test]
		public void CanFetchByCode()
		{
			var search = new ChargeCodeSearch();
			var all = search.FetchChargeCodes(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var id = search.FetchChargeCodeIdByCode(all[0].Code, all[0].TenantId);
			Assert.IsTrue(id > default(long));
		}

        [Test]
        public void CanFectchForAsset()
        {
            var code = new ChargeCodeSearch().FetchActiveChargeCodesForAssets(GlobalTestInitializer.DefaultTenantId); 
            Assert.IsTrue(code.Count > 0);
        }

		[Test]
		public void CanFectchAllCodes()
		{
			var code = new ChargeCodeSearch().FetchChargeCodes(GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(code.Count > 0);
		}
	}
}
