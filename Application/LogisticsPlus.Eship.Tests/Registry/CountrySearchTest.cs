﻿using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class CountrySearchTest
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchCountries()
		{
			var code = new CountrySearch().FetchCountries(SearchUtilities.WildCard); // all
			Assert.IsTrue(code.Count > 0);
		}

		[Test]
		public void CanFetchCountriesThatRequirePostalCode()
		{
			var code = new CountrySearch().FetchCountriesThatRequirePostalCode(SearchUtilities.WildCard); // all
			Assert.IsTrue(code.Count > 0);
		}

		[Test]
		public void CanFetchCountryIdByCode()
		{
			var code = new CountrySearch().FetchCountryIdByCode("US");
			Assert.IsTrue(code != default(long));
		}
	}
}
