﻿using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class PackageTypeValidatorTests
	{
		private PackageTypeValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new PackageTypeValidator();
		}

		[Test]
		public void IsUniqueTest()
		{
			var packageType = new PackageType { TypeName = "T?" , TenantId = GlobalTestInitializer.DefaultTenantId}; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsUnique(packageType));
		}

		[Test]
		public void IsValidTest()
		{
			var packageType = new PackageType { TypeName = "T?", TenantId = GlobalTestInitializer.DefaultTenantId}; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsValid(packageType));
		}

		[Test]
		public void IsExistingAndUniqueTest()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].PackageTypes;
			if (all.Count == 0) return;
			var option = all[0];
			Assert.IsTrue(_validator.IsUnique(option)); // this should be unique
			var newOption = new PackageType(1000000, false);
			Assert.IsFalse(newOption.KeyLoaded); // will not be found
			newOption.TypeName = option.TypeName;
			newOption.TenantId = option.TenantId;
			Assert.IsFalse(_validator.IsUnique(newOption)); // should be false
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var tenant = new PackageType();
			Assert.IsFalse(_validator.HasAllRequiredData(tenant));
		}

		[Test]
		public void CanDeleteTest()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].PackageTypes;
			if (all.Count == 0) return;
			var type = new PackageType(all[0].Id, false);
			Assert.IsTrue(type.KeyLoaded);
			_validator.Messages.Clear();
			_validator.CanDeletePackageType(type);
			_validator.Messages.PrintMessages();
		}
	}
}
