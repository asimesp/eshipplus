﻿using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class SettingValidatorTests
	{
		private SettingValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new SettingValidator();
		}

		[Test]
		public void IsValidTest()
		{
			var all = new SettingSearch().FetchSettings(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId);
			var setting = all.Count > 0
			              	? all[0]
			              	: new Setting
			              	  	{
			              	  		Code = SettingCode.RecordLockLimit,
			              	  		TenantId = GlobalTestInitializer.DefaultTenantId,
			              	  	}; // T? will not be found therefore unique
			setting.Value = "IsValidTest";
			Assert.IsTrue(_validator.IsValid(setting));
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var tenant = new Setting();
			Assert.IsFalse(_validator.HasAllRequiredData(tenant));
		}

		[Test]
		public void IsExistingAndUniqueTest()
		{
			var all = new SettingSearch().FetchSettings(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var code = all[0];
			Assert.IsTrue(_validator.IsUnique(code)); // this should be unique
			var newCode = new Setting(1000000, false);
			Assert.IsFalse(newCode.KeyLoaded); // will not be found
			newCode.Code = code.Code;
			newCode.TenantId = code.TenantId;
			Assert.IsFalse(_validator.IsUnique(newCode)); // should be false
		}
	}
}
