﻿using System;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Validation.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class AccountBucketUnitValidatorTests
	{
		private AccountBucketUnitValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new AccountBucketUnitValidator();
		}

		[Test]
		public void IsValidTest()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].AccountBuckets;
            if(all.Count == 0 )return;
		    var accountBucket = all[0];		    

            var accountBucketUnit = new AccountBucketUnit
                                        {
                                            Name = DateTime.Now.ToShortDateString(),
                                            Description = DateTime.Now.ToShortDateString(),
                                            AccountBucket = accountBucket,
                                            TenantId = GlobalTestInitializer.DefaultTenantId,
                                            Active = true
                                        };
			Assert.IsTrue(_validator.IsValid(accountBucketUnit));
		}

        [Test]
        public void CanDeleteTest()
        {
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].AccountBuckets;
            if (all.Count == 0) return;

            var accountBucket = new AccountBucket(all[0].Id, false);
            var accountBucketUnits = accountBucket.AccountBucketUnits;
            if(accountBucketUnits.Count==0)return;
            var accountBucketUnit = accountBucketUnits[0];
            Assert.IsTrue(accountBucketUnit.KeyLoaded);
            _validator.Messages.Clear();
            _validator.CanDeleteAccountBucketUnit(accountBucketUnit);
            _validator.Messages.PrintMessages();
        }
	}
}
