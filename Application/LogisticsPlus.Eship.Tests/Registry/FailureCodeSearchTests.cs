﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
    [TestFixture]
    public class FailureCodeSearchTests
    {
        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

        [Test]
        public void CanFetchFailureCodesWithoutType()
        {
			var failureCodes = new FailureCodeSearch().FetchFailureCodes(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId); // all
            Assert.IsTrue(failureCodes.Count > 0);
        }

		[Test]
		public void CanFetchFailureCodeByCode()
		{
			var search = new FailureCodeSearch();
            var all = search.FetchFailureCodes(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var priority = search.FetchFailureCodeByCode(all[0].Code, all[0].TenantId);
			Assert.IsNotNull(priority);
		}
    }   
}
