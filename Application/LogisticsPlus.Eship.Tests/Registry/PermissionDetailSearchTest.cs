﻿using LogisticsPlus.Eship.Processor.Searches.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Registry
{
	[TestFixture]
	public class PermissionDetailSearchTest
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchByCriteria()
		{
			var code = new PermissionDetailSearch().FetchPermissionDetails(GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(code.Count > 0);
		}

		[Test]
		public void CanFetchByCode()
		{
			var search = new PermissionDetailSearch();
			var all = search.FetchPermissionDetails(GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var id = search.FetchPermissionDetailByCode(all[0].Code, all[0].TenantId);
			Assert.IsNotNull(id);
		}
	}
}
