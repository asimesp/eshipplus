﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Dat;
using LogisticsPlus.Eship.WebApplication.Utilities;
using NUnit.Framework;
using AssetType = LogisticsPlus.Eship.Dat.AssetType;
using EquipmentType = LogisticsPlus.Eship.Dat.EquipmentType;
using PostalCode = LogisticsPlus.Eship.Dat.PostalCode;
using Shipment = LogisticsPlus.Eship.Dat.Shipment;

namespace LogisticsPlus.Eship.Tests.Loadboard
{
    [TestFixture]
    [Ignore("Loadboard Tests. Run on demand only")]
    public class DatLoadboardTests
    {
        private Connexion _service;

        private string _alarmId;
        private string _alarmAssetId;
        private string _assetId;


        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();

            Connexion.Initialize("http://cnx.test.dat.com:9280/TfmiRequest", "https://cnx.test.dat.com/TfmiRequest");
            _service = new Connexion("lpi_cnx", "logistics");
        }

        [Test]
        public void CanLookupCapabilities()
        {
            var result = _service.LookupCapabilities(new LookupCapabilitiesRequest
                    {
                        lookupCapabilitiesOperation =
                            new LookupCapabilitiesOperation
                                {
                                    capability =
                                        new[]
                                            {
                                                CapabilityType.AlarmMatch, CapabilityType.LookupBasicCarrierInfo,
                                                CapabilityType.LookupContractRates, CapabilityType.LookupPremiumCarrierInfo,
                                                CapabilityType.LookupSpotRates, CapabilityType.ManagePostings,
                                                CapabilityType.Search
                                            }
                                }
                    });
            Assert.IsTrue(result.LookupCapabilitiesSuccessData != null && result.Error == null);
        }

        [Test]
        public void CanLookupAsset()
        {
            var result = _service.LookupAsset(new LookupAssetRequest { lookupAssetOperation = new LookupAssetOperation { Item = new QueryAllMyAssets() } });
            Assert.IsTrue(result.LookupAssetSuccessData != null && result.Error == null);
        }

        [Test]
        public void CanPostAsset()
        {
            var result = _service.PostAsset(GetPostAssetRequest());
            _assetId = result.PostAssetSuccessData.First().assetId;
            Assert.IsTrue(result.PostAssetSuccessData != null && result.PostAssetSuccessData.Any() && !result.Errors.Any());
        }

        [Test]
        public void CanUpdateAsset()
        {
            var request = new UpdateAssetRequest
                {
                    updateAssetOperation = new UpdateAssetOperation
                        {
                            Item = _assetId,
                            Item1 = new ShipmentUpdate
                                {
                                    comments = new[] {"TestNewComment"},
                                    count = 1,
                                    countSpecified = true,
                                    dimensions = new Dimensions
                                        {
                                            heightInches = 10,
                                            heightInchesSpecified = true,
                                            lengthFeet = 10,
                                            lengthFeetSpecified = true,
                                            volumeCubicFeet = 10,
                                            volumeCubicFeetSpecified = true,
                                            weightPounds = 100,
                                            weightPoundsSpecified = true
                                        },
                                    ltl = false,
                                    ltlSpecified = true,
                                    stops = 1,
                                    stopsSpecified = true,
                                    rate = new ShipmentRate
                                        {
                                            baseRateDollars = 12.35f,
                                            rateBasedOn = RateBasedOnType.Flat,
                                            rateMiles = 100,
                                            rateMilesSpecified = true
                                        }
                                },
                            ItemElementName = ItemChoiceType.assetId,
                        }
                };

            var result = _service.UpdateAsset(request);
            Assert.IsTrue(result.UpdateAssetSuccessData != null && result.Error == null);
        }

        [Test]
        public void ZCanDeleteAsset()
        {
            var request = new DeleteAssetRequest
            {
                deleteAssetOperation = new DeleteAssetOperation
                {
                    Item = new DeleteAssetsByAssetIds
                    {
                        assetIds = new[] { _assetId }
                    },
                }
            };

            var result = _service.DeleteAsset(request);
            Assert.IsTrue(result.DeleteAssetSuccessData != null && result.Error == null);
        }

        [Test]
        public void ZCanDeleteAllAssets()
        {
            // if request fails, will throw exception to signify failure
            _service.DeleteAllAssets();
        }

        [Test]
        public void CanCountAssets()
        {
            var request = new CountAssetsRequest
                {
                    countAssetsOperation = new CountAssetsOperation {criteria = new SearchCriteria
                        {
                            ageLimitMinutes = 3600,
                            ageLimitMinutesSpecified = true,
                            origin = new GeoCriteria
                                {
                                    Item = new SearchOpen()
                                },
                            destination = new GeoCriteria
                                {
                                    Item = new SearchRadius
                                        {
                                            place = new Place
                                                {
                                                    Item = new CityAndState
                                                        {
                                                            city = "Erie",
                                                            stateProvince = StateProvince.PA
                                                        }
                                                },
                                            radius = new Mileage
                                                {
                                                    miles = 100,
                                                    method = MileageType.Road
                                                }

                                        }
                                },
                                equipmentClasses = new[]{EquipmentClass.Flatbeds}
                        }}
                };
            var result = _service.CountAssets(request);
            Assert.IsTrue(result.Error == null);
            Console.WriteLine("Count of Assets: {0}", result.CountAssetsSuccessData.count);
        }

        [Test]
        public void CanLookupAlarm()
        {
            var result = _service.LookupAlarm(new LookupAlarmRequest { lookupAlarmOperation = new LookupAlarmOperation { Item = new QueryAllMyAlarms() } });
            Assert.IsTrue(result.LookupAlarmSuccessData != null && result.Error == null);
        }

        [Test]
        public void CanCreateAlarm()
        {
            var postResult = _service.PostAsset(GetPostAssetRequest());
            var request = new CreateAlarmRequest
                {
                    createAlarmOperation = new CreateAlarmOperation
                        {
                            assetId = postResult.PostAssetSuccessData.First().assetId,
                            criteria = new AlarmSearchCriteria
                                {
                                    ageLimitMinutes = 3600,
                                    ageLimitMinutesSpecified = true,
                                    destinationRadius = new Mileage
                                        {
                                            method = MileageType.Air,
                                            miles = 100
                                        },
                                    originRadius = new Mileage
                                        {
                                            miles = 100,
                                            method = MileageType.Air
                                        },
                                    
                                }
                        }
                };

            var result = _service.CreateAlarm(request);
            _alarmId = result.CreateAlarmSuccessData.alarmId;
            _alarmAssetId = postResult.PostAssetSuccessData.First().assetId;
            Assert.IsTrue(result.CreateAlarmSuccessData != null && result.Error == null);

            
        }

        [Test]
        public void CanUpdateAlarm()
        {
            var request = new UpdateAlarmRequest
                {
                    updateAlarmOperation = new UpdateAlarmOperation
                        {
                            alarmId = _alarmId,
                            destinationRadius = new Mileage
                                {
                                    method = MileageType.Air,
                                    miles = 100
                                },
                            originRadius = new Mileage
                                {
                                    method = MileageType.Air,
                                    miles = 100
                                }
                        }
                };

            var result = _service.UpdateAlarm(request);
            Assert.IsTrue(result.UpdateAlarmSuccessData != null && result.Error == null);
        }

        [Test]
        public void ZCanDeleteAlarm()
        {
            var request = new DeleteAlarmRequest
            {
               deleteAlarmOperation = new DeleteAlarmOperation
                   {
                       Item = new DeleteAlarmsByAlarmIds
                           {
                               alarmIds = new[] { _alarmId }
                           }
                   }
            };

            var result = _service.DeleteAlarm(request);
            Assert.IsTrue(result.DeleteAlarmSuccessData != null && result.Error == null);

            // remove above created asset, test shouldn't fail if this operation fails
            try
            {
                var deleteRequest = new DeleteAssetRequest
                {
                    deleteAssetOperation = new DeleteAssetOperation
                    {
                        Item = new DeleteAssetsByAssetIds
                        {
                            assetIds = new[] { _alarmAssetId }
                        },
                    }
                };

                _service.DeleteAsset(deleteRequest);
            }
            catch { }

        }


        [Test]
        public void CanSearchLoadboard()
        {
            var result = _service.Search(GetSearchRequest());
            Assert.IsTrue(result.CreateSearchSuccessData != null && result.Error == null);
            Console.WriteLine("Total Matches: {0}", result.CreateSearchSuccessData.totalMatches);
        }


        [Test]
        public void CanLookupCurrentRate()
        {
            var request = new LookupRateRequest
                {
                    lookupRateOperations = new []
                        {
                            new LookupRateOperation
                                {
                                    destination = new Place
                                        {
                                            Item = new NamedPostalCode
                                                {
                                                    city = "Erie",
                                                    stateProvince = StateProvince.PA,
                                                    postalCode = new PostalCode
                                                        {
                                                            code = "16501",
                                                            country = CountryCode.US
                                                        }
                                                }
                                        },
                                    origin = new Place
                                        {
                                            Item = new NamedPostalCode
                                                {
                                                    city = "Indianapolis",
                                                    stateProvince = StateProvince.IN,
                                                    postalCode = new PostalCode
                                                        {
                                                            code = "46202",
                                                            country = CountryCode.US
                                                        }
                                                }
                                        },
                                    equipment = RateEquipmentCategory.Flatbeds,
                                    includeContractRate = true,
                                    includeContractRateSpecified = true,
                                    includeMyRate = true,
                                    includeMyRateSpecified = true,
                                    includeSpotRate = true,
                                    includeSpotRateSpecified = true
                                }
                        }
                };

            var result = _service.LookupCurrentRate(request);
            Assert.IsTrue(result.LookupRateSuccessData != null && !result.Errors.Any());
        }

        [Test]
        public void CanLookupHistoricContract()
        {
            var request = new LookupHistoricContractRatesRequest
                {
                    lookupHistoricContractRatesOperation = new LookupHistoricContractRatesOperation
                        {
                            destination = new Place
                                {
                                    Item = new NamedPostalCode
                                        {
                                            city = "Erie",
                                            stateProvince = StateProvince.PA,
                                            postalCode = new PostalCode
                                                {
                                                    code = "16501",
                                                    country = CountryCode.US
                                                }
                                        }
                                },
                            origin = new Place
                                {
                                    Item = new NamedPostalCode
                                        {
                                            city = "Indianapolis",
                                            stateProvince = StateProvince.IN,
                                            postalCode = new PostalCode
                                                {
                                                    code = "46202",
                                                    country = CountryCode.US
                                                }
                                        }
                                },
                            equipment = RateEquipmentCategory.Flatbeds,
                        }
                };

            var result = _service.LookupHistoricContract(request);
            Assert.IsTrue(result.LookupHistoricContractRatesSuccessData != null && result.Error == null);
        }

        [Test]
        public void CanLookupHistoricSpot()
        {
            var request = new LookupHistoricSpotRatesRequest
                {
                    lookupHistoricSpotRatesOperation = new LookupHistoricSpotRatesOperation
                        {
                            destination = new Place
                                {
                                    Item = new NamedPostalCode
                                        {
                                            city = "Erie",
                                            stateProvince = StateProvince.PA,
                                            postalCode = new PostalCode
                                                {
                                                    code = "16501",
                                                    country = CountryCode.US
                                                }
                                        }
                                },
                            origin = new Place
                                {
                                    Item = new NamedPostalCode
                                        {
                                            city = "Indianapolis",
                                            stateProvince = StateProvince.IN,
                                            postalCode = new PostalCode
                                                {
                                                    code = "46202",
                                                    country = CountryCode.US
                                                }
                                        }
                                },
                            equipment = RateEquipmentCategory.Flatbeds,
                        }
                };

            var result = _service.LookupHistoricSpot(request);
            Assert.IsTrue(result.LookupHistoricSpotRatesSuccessData != null && result.Error == null);
        }

        [Test]
        public void CanLookupDoeFuelPrices()
        {
            var request = new LookupDoeFuelPricesRequest
                {
                    lookupDoeFuelPricesOperation = new LookupDoeFuelPricesOperation()
                };
            var result = _service.LookupDoeFuelPrices(request);
            Assert.IsTrue(result.LookupDoeFuelPricesSuccessData != null && result.Error == null);
        }

        [Test]
        [Ignore("Need to implement creation of a Carrier")]
        public void CanLookupDobCarriersByCarrierId()
        {
            var result = _service.LookupDobCarriersByCarrierId("CarrierId");
            Assert.IsTrue(result.LookupDobCarriersSuccessData != null && result.Error == null);
        }

        [Test]
        public void CanLookupDobEvents()
        {
            var result = _service.LookupDobEvents(DateTime.Now.AddDays(-7));
            Assert.IsTrue(result.LookupDobEventsSuccessData != null && result.Error == null);
        }

        [Test]
        public void CanLookupSignedCarriers()
        {
            var result = _service.LookupSignedCarriers();
            Assert.IsTrue(result.LookupDobSignedCarriersSuccessData != null && result.Error == null);
        }

        [Test]
        public void CanLookupCarrierByDotNumber()
        {
            var result = _service.LookupCarrierByDotNumber(1258500);
            Assert.IsTrue(result.LookupCarrierSuccessData != null && result.Error == null);
        }

        [Test]
        public void CanLookupCarrierByMcNumber()
        {
            var result = _service.LookupCarrierByMcNumber(177051);
            Assert.IsTrue(result.LookupCarrierSuccessData != null && result.Error == null);
        }

        [Test]
        public void CanLookupCarrierByUserId()
        {
            var result = _service.LookupCarrierByUserId(12);
            Assert.IsTrue(result.LookupCarrierSuccessData != null && result.Error == null);
        }

        [Test]
        public void CanLookupCarrier()
        {
            var request = new LookupCarrierRequest
                {
                    lookupCarrierOperation = new[]
                        {
                            new LookupCarrierOperation
                                {
                                    Item = 113594,
                                    ItemElementName = ItemChoiceType2.dotNumber
                                }
                        }
                };
            var result = _service.LookupCarrier(request);
            Assert.IsTrue(result.LookupCarrierSuccessData != null && result.Error == null);
        }
      
        private static PostAssetRequest GetPostAssetRequest()
        {
            var shipment = new Shipment
                {
                    destination = new Place
                        {
                            Item = new CityAndState
                                {
                                    city = "Canton",
                                    county = "Norfolk",
                                    stateProvince = StateProvince.MA
                                }
                        },
                    equipmentType = EquipmentType.Flatbed,
                    origin = new Place
                        {
                            Item = new CityAndState
                                {
                                    city = "Chicago",
                                    county = "Cook",
                                    stateProvince = StateProvince.IL
                                }
                        },
                    rate = new ShipmentRate
                            {
                                baseRateDollars = 1700,
                                rateBasedOn = RateBasedOnType.Flat,
                                rateMiles = 951,
                                rateMilesSpecified = true
                            },
                    truckStops = new TruckStops
                            {
                                enhancements = new TruckStopVideoEnhancement[0],
                                Item = new ClosestTruckStops(),
                                posterDisplayName = "12345"
                            }
                };

            var postAssetOperation = new PostAssetOperation
                {
                    availability =
                        new Availability
                            {
                                earliest = DateTime.Now.TimeToMinimum(),
                                earliestSpecified = true,
                                latest = DateTime.Now.TimeToMaximum(),
                                latestSpecified = true
                            },
                    comments = new[] {"Call Now!"},
                    count = 1,
                    countSpecified = true,
                    dimensions =
                        new Dimensions
                            {
                                heightInches = 48,
                                heightInchesSpecified = true,
                                lengthFeet = 30,
                                lengthFeetSpecified = true,
                                volumeCubicFeet = 0,
                                volumeCubicFeetSpecified = false,
                                weightPounds = 45000,
                                weightPoundsSpecified = true
                            },
                    includeAsset = true,
                    includeAssetSpecified = true,
                    Item = shipment,
                    ltl = true,
                    ltlSpecified = true,
                    postersReferenceId = Guid.NewGuid().ToString().Substring(0,8),
                    stops = 0,
                    stopsSpecified = true
                };

            return new PostAssetRequest { postAssetOperations = new[] { postAssetOperation } };
        }

        private static CreateSearchRequest GetSearchRequest()
        {
            return new CreateSearchRequest
            {
                createSearchOperation = new CreateSearchOperation
                {
                    criteria = new SearchCriteria
                    {
                        ageLimitMinutes = 3600,
                        ageLimitMinutesSpecified = true,
                        assetType = AssetType.Equipment,
                        availability = new Availability
                        {
                            earliest = DateTime.Now.AddHours(-10),
                            earliestSpecified = true,
                            latest = DateTime.Now.AddHours(5),
                            latestSpecified = true
                        },
                        destination = new GeoCriteria
                        {
                            Item = new SearchRadius
                            {
                                place = new Place
                                {
                                    Item = new NamedPostalCode
                                    {
                                        city = "ERIE",
                                        postalCode = new PostalCode
                                        {
                                            code = "16501",
                                            country = CountryCode.US,
                                        },
                                        stateProvince = StateProvince.PA,
                                    }
                                },
                                radius = new Mileage
                                    {
                                        method = MileageType.Road,
                                        miles = 10
                                    }
                            }
                        },
                        equipmentClasses = new[] { EquipmentClass.Flatbeds, EquipmentClass.DryBulk },
                        includeFulls = true,
                        includeFullsSpecified = true,
                        includeLtls = true,
                        includeLtlsSpecified = true,
                        origin = new GeoCriteria
                        {
                            Item = new SearchRadius
                            {
                                place = new Place
                                {
                                    Item = new NamedPostalCode
                                    {
                                        city = "INDIANAPOLIS",
                                        postalCode = new PostalCode
                                        {
                                            code = "46202",
                                            country = CountryCode.US,
                                        },
                                        stateProvince = StateProvince.IN,
                                    }
                                },
                                radius = new Mileage
                                {
                                    method = MileageType.Road,
                                    miles = 10
                                }
                            }
                        },
                    }
                }
            };
        }

        [Test]
        [Ignore("DO NOT RUN")]
        public void DatCertificationTests()
        {
            var loadOrder1 = new LoadOrder(30729);
            var loadOrder2 = new LoadOrder(20700);
            var loadOrder3 = new LoadOrder(20701);
            var loadOrder4 = new LoadOrder(20702);
            var loadOrder5 = new LoadOrder(20703);
            var loadOrder6 = new LoadOrder(20704);
            var loadOrder7 = new LoadOrder(20705);
            var loadOrder8 = new LoadOrder(20706);
            var loadOrder9 = new LoadOrder(20707);
            var loadOrder10 = new LoadOrder(20708);
            var loadOrder11 = new LoadOrder(20709);
            var loadOrder12 = new LoadOrder(20710);
            var loadOrder13 = new LoadOrder(20711);
            var loadOrder14 = new LoadOrder(20712);
            var loadOrder15 = new LoadOrder(20713);
            var loadOrder16 = new LoadOrder(20714);
            var loadOrder17 = new LoadOrder(20715);
            var loadOrder18 = new LoadOrder(30728);
            var loadOrder19 = new LoadOrder(20717);
            var loadOrder20 = new LoadOrder(20718);
            var loadOrder21 = new LoadOrder(20719);
            var loadOrder22 = new LoadOrder(30731);
            var loadOrder23 = new LoadOrder(20721);
            var loadOrder24 = new LoadOrder(20722);
            var loadOrder25 = new LoadOrder(20723);
            var loadOrder26 = new LoadOrder(20724);
            var loadOrder27 = new LoadOrder(20725);
            var loadOrder28 = new LoadOrder(20726);
            var loadOrder29 = new LoadOrder(20727);
            var loadOrder30 = new LoadOrder(20728);
            var loadOrder31 = new LoadOrder(30746);
            var loadOrder32 = new LoadOrder(30747);
            var loadOrder33 = new LoadOrder(30748);
            var loadOrder34 = new LoadOrder(30749);
            var loadOrder35 = new LoadOrder(30750);
            var loadOrder36 = new LoadOrder(30751);
            var loadOrder37 = new LoadOrder(30752);
            var loadOrder38 = new LoadOrder(30753);
            var loadOrder39 = new LoadOrder(30754);
            var loadOrder40 = new LoadOrder(30755);
            var loadOrder41 = new LoadOrder(30756);
            var loadOrder42 = new LoadOrder(30757);
            var loadOrder43 = new LoadOrder(30758);
            var loadOrder44 = new LoadOrder(30759);
            var loadOrder45 = new LoadOrder(30760);
            var loadOrder46 = new LoadOrder(30761);
            var loadOrder47 = new LoadOrder(30762);
            var loadOrder48 = new LoadOrder(30763);
            var loadOrder49 = new LoadOrder(30764);
            var loadOrder50 = new LoadOrder(30765);
            var loadOrder51 = new LoadOrder(30766);
            var loadOrder52 = new LoadOrder(30767);
            var loadOrder53 = new LoadOrder(30768);
            var loadOrder54 = new LoadOrder(30769);
            var loadOrder55 = new LoadOrder(30770);
            var loadOrder56 = new LoadOrder(30771);
            var loadOrder57 = new LoadOrder(30772);
            var loadOrder58 = new LoadOrder(30773);
            var loadOrder59 = new LoadOrder(30774);
            var loadOrder60 = new LoadOrder(30775);


            var requestRate = new ShipmentRate
            {
                baseRateDollars = 39.99f,
                rateBasedOn = RateBasedOnType.Flat,
                rateMiles = 0,
                rateMilesSpecified = false
            };
            

            var posting1 = _service.PostAsset(loadOrder1.ToPostAssetRequest(requestRate,EquipmentType.Flatbed,new List<string> { "Test Comment 1" }));
            if (posting1.HasError)
                Console.WriteLine(posting1.Errors.First().detailedMessage);
            var localAsset1 = new DatLoadboardAssetPosting()
                {
                    AssetId = posting1.PostAssetSuccessData.First().assetId,
                    ExpirationDate = DateTime.Now.AddDays(1),
                    DateCreated = DateTime.Now,
                    IdNumber = loadOrder1.LoadOrderNumber,
                    BaseRate = 0,
                    UserId = 12948,
                    SerializedComments = string.Empty,
                    HazardousMaterial = false,
                    RateType = RateBasedOnType.Flat,
                    Mileage = 0,
                    TenantId = 4
                };
            localAsset1.Save();

            var posting2 = _service.PostAsset(loadOrder2.ToPostAssetRequest(requestRate, EquipmentType.Flatbed, new List<string> { "Test Comment 1" }));
            if (posting2.HasError)
                Console.WriteLine(posting2.Errors.First().detailedMessage);
            var localAsset2 = new DatLoadboardAssetPosting()
            {
                AssetId = posting2.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder2.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset2.Save();

            var posting3 = _service.PostAsset(loadOrder3.ToPostAssetRequest(requestRate, EquipmentType.Flatbed, new List<string> { "Test Comment 1" }));
            if (posting3.HasError)
                Console.WriteLine(posting3.Errors.First().detailedMessage);
            var localAsset3 = new DatLoadboardAssetPosting()
            {
                AssetId = posting3.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder3.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset3.Save();

            var posting4 = _service.PostAsset(loadOrder4.ToPostAssetRequest(requestRate,EquipmentType.Flatbed,new List<string> { "Test Comment 1" }));
            if (posting4.HasError)
                Console.WriteLine(posting4.Errors.First().detailedMessage);
            var localAsset4 = new DatLoadboardAssetPosting()
            {
                AssetId = posting4.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder4.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset4.Save();

            var posting5 = _service.PostAsset(loadOrder5.ToPostAssetRequest(requestRate, EquipmentType.Flatbed, new List<string> { "Test Comment 1" }));
            if (posting5.HasError)
                Console.WriteLine(posting5.Errors.First().detailedMessage);
            var localAsset5 = new DatLoadboardAssetPosting()
            {
                AssetId = posting5.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder5.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset5.Save();

            var posting6 = _service.PostAsset(loadOrder6.ToPostAssetRequest(requestRate, EquipmentType.Container, new List<string> { "Test Comment 1" }));
            if (posting6.HasError)
                Console.WriteLine(posting6.Errors.First().detailedMessage);
            var localAsset6 = new DatLoadboardAssetPosting()
            {
                AssetId = posting6.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder6.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset6.Save();

            var posting7 = _service.PostAsset(loadOrder7.ToPostAssetRequest(requestRate, EquipmentType.Flatbed, new List<string> { "Test Comment 1" }));
            if (posting7.HasError)
                Console.WriteLine(posting7.Errors.First().detailedMessage);
            var localAsset7 = new DatLoadboardAssetPosting()
            {
                AssetId = posting7.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder7.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset7.Save();

            var posting8 = _service.PostAsset(loadOrder8.ToPostAssetRequest(requestRate, EquipmentType.Reefer, new List<string> { "Test Comment 1" }));
            if (posting8.HasError)
                Console.WriteLine(posting8.Errors.First().detailedMessage);
            var localAsset8 = new DatLoadboardAssetPosting()
            {
                AssetId = posting8.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder8.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset8.Save();

            var posting9 = _service.PostAsset(loadOrder9.ToPostAssetRequest(requestRate, EquipmentType.Van, new List<string> { "Test Comment 1" }));
            if (posting9.HasError)
                Console.WriteLine(posting9.Errors.First().detailedMessage);
            var localAsset9 = new DatLoadboardAssetPosting()
            {
                AssetId = posting9.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder9.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset9.Save();

            var posting10 = _service.PostAsset(loadOrder10.ToPostAssetRequest(requestRate, EquipmentType.Flatbed, new List<string> { "Test Comment 1" }));
            if (posting10.HasError)
                Console.WriteLine(posting10.Errors.First().detailedMessage);
            var localAsset10 = new DatLoadboardAssetPosting()
            {
                AssetId = posting10.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder10.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset10.Save();

            var posting11 = _service.PostAsset(loadOrder11.ToPostAssetRequest(requestRate, EquipmentType.Flatbed, new List<string> { "Test Comment 1" }));
            if (posting11.HasError)
                Console.WriteLine(posting11.Errors.First().detailedMessage);
            var localAsset11 = new DatLoadboardAssetPosting()
            {
                AssetId = posting11.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder11.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset11.Save();

            var posting12 = _service.PostAsset(loadOrder12.ToPostAssetRequest(requestRate, EquipmentType.Flatbed, new List<string> { "Test Comment 1" }));
            if (posting12.HasError)
                Console.WriteLine(posting12.Errors.First().detailedMessage);
            var localAsset12 = new DatLoadboardAssetPosting()
            {
                AssetId = posting12.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder12.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset12.Save();

            var posting13 = _service.PostAsset(loadOrder13.ToPostAssetRequest(requestRate, EquipmentType.DumpTrailer, new List<string> { "Test Comment 1" }));
            if (posting13.HasError)
                Console.WriteLine(posting13.Errors.First().detailedMessage);
            var localAsset13 = new DatLoadboardAssetPosting()
            {
                AssetId = posting13.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder13.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset13.Save();

            var posting14 = _service.PostAsset(loadOrder14.ToPostAssetRequest(requestRate, EquipmentType.Flatbed, new List<string> { "Test Comment 1" }));
            if (posting14.HasError)
                Console.WriteLine(posting14.Errors.First().detailedMessage);
            var localAsset14 = new DatLoadboardAssetPosting()
            {
                AssetId = posting14.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder14.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset14.Save();

            var posting15 = _service.PostAsset(loadOrder15.ToPostAssetRequest(requestRate, EquipmentType.Container, new List<string> { "Test Comment 1" }));
            if (posting15.HasError)
                Console.WriteLine(posting15.Errors.First().detailedMessage);
            var localAsset15 = new DatLoadboardAssetPosting()
            {
                AssetId = posting15.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder15.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset15.Save();

            Thread.Sleep(new TimeSpan(0,20,0));

            var posting16 = _service.PostAsset(loadOrder16.ToPostAssetRequest(requestRate, EquipmentType.Flatbed, new List<string> { "Test Comment 1" }));
            if (posting16.HasError)
                Console.WriteLine(posting16.Errors.First().detailedMessage);
            var localAsset16 = new DatLoadboardAssetPosting()
            {
                AssetId = posting16.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder16.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset16.Save();

            var posting17 = _service.PostAsset(loadOrder17.ToPostAssetRequest(requestRate, EquipmentType.Container, new List<string> { "Test Comment 1" }));
            if (posting17.HasError)
                Console.WriteLine(posting17.Errors.First().detailedMessage);
            var localAsset17 = new DatLoadboardAssetPosting()
            {
                AssetId = posting17.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder17.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset17.Save();

            var posting18 = _service.PostAsset(loadOrder18.ToPostAssetRequest(requestRate, EquipmentType.Flatbed, new List<string> { "Test Comment 1" }));
            if (posting18.HasError)
                Console.WriteLine(posting18.Errors.First().detailedMessage);
            var localAsset18 = new DatLoadboardAssetPosting()
            {
                AssetId = posting18.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder18.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset18.Save();

            var posting19 = _service.PostAsset(loadOrder19.ToPostAssetRequest(requestRate, EquipmentType.ContainerRefrigerated, new List<string> { "Test Comment 1" }));
            if (posting19.HasError)
                Console.WriteLine(posting19.Errors.First().detailedMessage);
            var localAsset19 = new DatLoadboardAssetPosting()
            {
                AssetId = posting19.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder19.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset19.Save();

            var posting20 = _service.PostAsset(loadOrder20.ToPostAssetRequest(requestRate, EquipmentType.Flatbed, new List<string> { "Test Comment 1" }));
            if (posting20.HasError)
                Console.WriteLine(posting20.Errors.First().detailedMessage);
            var localAsset20 = new DatLoadboardAssetPosting()
            {
                AssetId = posting20.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder20.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset20.Save();

            var posting21 = _service.PostAsset(loadOrder21.ToPostAssetRequest(requestRate, EquipmentType.Flatbed, new List<string> { "Test Comment 1" }));
            if (posting21.HasError)
                Console.WriteLine(posting21.Errors.First().detailedMessage);
            var localAsset21 = new DatLoadboardAssetPosting()
            {
                AssetId = posting21.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder21.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset21.Save();

            var posting22 = _service.PostAsset(loadOrder22.ToPostAssetRequest(requestRate, EquipmentType.Container, new List<string> { "Test Comment 1" }));
            if (posting22.HasError)
                Console.WriteLine(posting22.Errors.First().detailedMessage);
            var localAsset22 = new DatLoadboardAssetPosting()
            {
                AssetId = posting22.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder22.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset22.Save();

            var posting23 = _service.PostAsset(loadOrder23.ToPostAssetRequest(requestRate, EquipmentType.DumpTrailer, new List<string> { "Test Comment 1" }));
            if (posting23.HasError)
                Console.WriteLine(posting23.Errors.First().detailedMessage);
            var localAsset23 = new DatLoadboardAssetPosting()
            {
                AssetId = posting23.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder23.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset23.Save();

            var posting24 = _service.PostAsset(loadOrder24.ToPostAssetRequest(requestRate, EquipmentType.Container, new List<string> { "Test Comment 1" }));
            if (posting24.HasError)
                Console.WriteLine(posting24.Errors.First().detailedMessage);
            var localAsset24 = new DatLoadboardAssetPosting()
            {
                AssetId = posting24.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder24.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset24.Save();

            var posting25 = _service.PostAsset(loadOrder25.ToPostAssetRequest(requestRate, EquipmentType.Flatbed, new List<string> { "Test Comment 1" }));
            if (posting25.HasError)
                Console.WriteLine(posting25.Errors.First().detailedMessage);
            var localAsset25 = new DatLoadboardAssetPosting()
            {
                AssetId = posting25.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder25.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset25.Save();

            var posting26 = _service.PostAsset(loadOrder26.ToPostAssetRequest(requestRate, EquipmentType.StretchTrailer, new List<string> { "Test Comment 1" }));
            if (posting26.HasError)
                Console.WriteLine(posting26.Errors.First().detailedMessage);
            var localAsset26 = new DatLoadboardAssetPosting()
            {
                AssetId = posting26.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder26.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset26.Save();


            var posting27 = _service.PostAsset(loadOrder27.ToPostAssetRequest(requestRate, EquipmentType.Container, new List<string> { "Test Comment 1" }));
            if (posting27.HasError)
                Console.WriteLine(posting27.Errors.First().detailedMessage);
            var localAsset27 = new DatLoadboardAssetPosting()
            {
                AssetId = posting27.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder27.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset27.Save();

            var posting28 = _service.PostAsset(loadOrder28.ToPostAssetRequest(requestRate, EquipmentType.StretchTrailer, new List<string> { "Test Comment 1" }));
            if (posting28.HasError)
                Console.WriteLine(posting28.Errors.First().detailedMessage);
            var localAsset28 = new DatLoadboardAssetPosting()
            {
                AssetId = posting28.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder28.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset28.Save();

            var posting29 = _service.PostAsset(loadOrder29.ToPostAssetRequest(requestRate, EquipmentType.Container, new List<string> { "Test Comment 1" }));
            if (posting29.HasError)
                Console.WriteLine(posting29.Errors.First().detailedMessage);
            var localAsset29 = new DatLoadboardAssetPosting()
            {
                AssetId = posting29.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder29.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset29.Save();

            var posting30 = _service.PostAsset(loadOrder30.ToPostAssetRequest(requestRate, EquipmentType.Flatbed, new List<string> { "Test Comment 1" }));
            if (posting30.HasError)
                Console.WriteLine(posting30.Errors.First().detailedMessage);
            var localAsset30 = new DatLoadboardAssetPosting()
            {
                AssetId = posting30.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder30.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset30.Save();

            Thread.Sleep(new TimeSpan(0,20,0));

            var posting31 = _service.PostAsset(loadOrder31.ToPostAssetRequest(requestRate, EquipmentType.ReeferDouble, new List<string> { "Test Comment 1" }));
            if (posting31.HasError)
                Console.WriteLine(posting31.Errors.First().detailedMessage);
            var localAsset31 = new DatLoadboardAssetPosting()
            {
                AssetId = posting31.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder31.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset31.Save();

            var posting32 = _service.PostAsset(loadOrder32.ToPostAssetRequest(requestRate, EquipmentType.Flatbed, new List<string> { "Test Comment 1" }));
            if (posting32.HasError)
                Console.WriteLine(posting32.Errors.First().detailedMessage);
            var localAsset32 = new DatLoadboardAssetPosting()
            {
                AssetId = posting32.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder32.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset32.Save();

            var posting33 = _service.PostAsset(loadOrder33.ToPostAssetRequest(requestRate, EquipmentType.ReeferDouble, new List<string> { "Test Comment 1" }));
            if (posting33.HasError)
                Console.WriteLine(posting33.Errors.First().detailedMessage);
            var localAsset33 = new DatLoadboardAssetPosting()
            {
                AssetId = posting33.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder33.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset33.Save();

            var posting34 = _service.PostAsset(loadOrder34.ToPostAssetRequest(requestRate, EquipmentType.ReeferDouble, new List<string> { "Test Comment 1" }));
            if (posting34.HasError)
                Console.WriteLine(posting34.Errors.First().detailedMessage);
            var localAsset34 = new DatLoadboardAssetPosting()
            {
                AssetId = posting34.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder34.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset34.Save();

            var posting35 = _service.PostAsset(loadOrder35.ToPostAssetRequest(requestRate, EquipmentType.ReeferDouble, new List<string> { "Test Comment 1" }));
            if (posting35.HasError)
                Console.WriteLine(posting35.Errors.First().detailedMessage);
            var localAsset35 = new DatLoadboardAssetPosting()
            {
                AssetId = posting35.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder35.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset35.Save();

            var posting36 = _service.PostAsset(loadOrder36.ToPostAssetRequest(requestRate, EquipmentType.ReeferDouble, new List<string> { "Test Comment 1" }));
            if (posting36.HasError)
                Console.WriteLine(posting36.Errors.First().detailedMessage);
            var localAsset36 = new DatLoadboardAssetPosting()
            {
                AssetId = posting36.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder36.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset36.Save();

            var posting37 = _service.PostAsset(loadOrder37.ToPostAssetRequest(requestRate, EquipmentType.ReeferDouble, new List<string> { "Test Comment 1" }));
            if (posting37.HasError)
                Console.WriteLine(posting37.Errors.First().detailedMessage);
            var localAsset37 = new DatLoadboardAssetPosting()
            {
                AssetId = posting37.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder37.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset37.Save();

            var posting38 = _service.PostAsset(loadOrder38.ToPostAssetRequest(requestRate, EquipmentType.ReeferDouble, new List<string> { "Test Comment 1" }));
            if (posting38.HasError)
                Console.WriteLine(posting38.Errors.First().detailedMessage);
            var localAsset38 = new DatLoadboardAssetPosting()
            {
                AssetId = posting38.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder38.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset38.Save();

            var posting39 = _service.PostAsset(loadOrder39.ToPostAssetRequest(requestRate, EquipmentType.ReeferDouble, new List<string> { "Test Comment 1" }));
            if (posting39.HasError)
                Console.WriteLine(posting39.Errors.First().detailedMessage);
            var localAsset39 = new DatLoadboardAssetPosting()
            {
                AssetId = posting39.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder39.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset39.Save();

            var posting40 = _service.PostAsset(loadOrder40.ToPostAssetRequest(requestRate, EquipmentType.ReeferDouble, new List<string> { "Test Comment 1" }));
            if (posting40.HasError)
                Console.WriteLine(posting40.Errors.First().detailedMessage);
            var localAsset40 = new DatLoadboardAssetPosting()
            {
                AssetId = posting40.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder40.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset40.Save();

            var posting41 = _service.PostAsset(loadOrder41.ToPostAssetRequest(requestRate, EquipmentType.ReeferDouble, new List<string> { "Test Comment 1" }));
            if (posting41.HasError)
                Console.WriteLine(posting41.Errors.First().detailedMessage);
            var localAsset41 = new DatLoadboardAssetPosting()
            {
                AssetId = posting41.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder41.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset41.Save();

           var posting42 = _service.PostAsset(loadOrder42.ToPostAssetRequest(requestRate, EquipmentType.ReeferDouble, new List<string> { "Test Comment 1" }));
            if (posting42.HasError)
                Console.WriteLine(posting42.Errors.First().detailedMessage);
            var localAsset42 = new DatLoadboardAssetPosting()
            {
                AssetId = posting42.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder42.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset42.Save();

            var posting43 = _service.PostAsset(loadOrder43.ToPostAssetRequest(requestRate, EquipmentType.ReeferDouble, new List<string> { "Test Comment 1" }));
            if (posting43.HasError)
                Console.WriteLine(posting43.Errors.First().detailedMessage);
            var localAsset43 = new DatLoadboardAssetPosting()
            {
                AssetId = posting43.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder43.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset43.Save();

            var posting44 = _service.PostAsset(loadOrder44.ToPostAssetRequest(requestRate, EquipmentType.ReeferDouble, new List<string> { "Test Comment 1" }));
            if (posting44.HasError)
                Console.WriteLine(posting44.Errors.First().detailedMessage);
            var localAsset44 = new DatLoadboardAssetPosting()
            {
                AssetId = posting44.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder44.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset44.Save();

            var posting45 = _service.PostAsset(loadOrder45.ToPostAssetRequest(requestRate, EquipmentType.ReeferDouble, new List<string> { "Test Comment 1" }));
            if (posting45.HasError)
                Console.WriteLine(posting45.Errors.First().detailedMessage);
            var localAsset45 = new DatLoadboardAssetPosting()
            {
                AssetId = posting45.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder45.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset45.Save();

            Thread.Sleep(new TimeSpan(0,20,0));

            var posting46 = _service.PostAsset(loadOrder46.ToPostAssetRequest(requestRate, EquipmentType.Flatbed, new List<string> { "Test Comment 1" }));
            if (posting46.HasError)
                Console.WriteLine(posting46.Errors.First().detailedMessage);
            var localAsset46 = new DatLoadboardAssetPosting()
            {
                AssetId = posting46.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder46.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset46.Save();

            var posting47 = _service.PostAsset(loadOrder47.ToPostAssetRequest(requestRate, EquipmentType.Flatbed, new List<string> { "Test Comment 1" }));
            if (posting47.HasError)
                Console.WriteLine(posting47.Errors.First().detailedMessage);
            var localAsset47 = new DatLoadboardAssetPosting()
            {
                AssetId = posting47.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder47.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset47.Save();

            var posting48 = _service.PostAsset(loadOrder48.ToPostAssetRequest(requestRate, EquipmentType.Flatbed, new List<string> { "Test Comment 1" }));
            if (posting48.HasError)
                Console.WriteLine(posting48.Errors.First().detailedMessage);
            var localAsset48 = new DatLoadboardAssetPosting()
            {
                AssetId = posting48.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder48.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset48.Save();

            var posting49 = _service.PostAsset(loadOrder49.ToPostAssetRequest(requestRate, EquipmentType.Flatbed, new List<string> { "Test Comment 1" }));
            if (posting49.HasError)
                Console.WriteLine(posting49.Errors.First().detailedMessage);
            var localAsset49 = new DatLoadboardAssetPosting()
            {
                AssetId = posting49.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder49.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset49.Save();

            var posting50 = _service.PostAsset(loadOrder50.ToPostAssetRequest(requestRate, EquipmentType.Flatbed, new List<string> { "Test Comment 1" }));
            if (posting50.HasError)
                Console.WriteLine(posting50.Errors.First().detailedMessage);
            var localAsset50 = new DatLoadboardAssetPosting()
            {
                AssetId = posting50.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder50.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset50.Save();

            var posting51 = _service.PostAsset(loadOrder51.ToPostAssetRequest(requestRate, EquipmentType.Flatbed, new List<string> { "Test Comment 1" }));
            if (posting51.HasError)
                Console.WriteLine(posting51.Errors.First().detailedMessage);
            var localAsset51 = new DatLoadboardAssetPosting()
            {
                AssetId = posting51.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder51.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset51.Save();

            var posting52 = _service.PostAsset(loadOrder52.ToPostAssetRequest(requestRate, EquipmentType.Flatbed, new List<string> { "Test Comment 1" }));
            if (posting52.HasError)
                Console.WriteLine(posting52.Errors.First().detailedMessage);
            var localAsset52 = new DatLoadboardAssetPosting()
            {
                AssetId = posting52.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder52.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset52.Save();

            var posting53 = _service.PostAsset(loadOrder53.ToPostAssetRequest(requestRate, EquipmentType.Flatbed, new List<string> { "Test Comment 1" }));
            if (posting53.HasError)
                Console.WriteLine(posting53.Errors.First().detailedMessage);
            var localAsset53 = new DatLoadboardAssetPosting()
            {
                AssetId = posting53.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder53.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset53.Save();

            var posting54 = _service.PostAsset(loadOrder54.ToPostAssetRequest(requestRate, EquipmentType.Flatbed, new List<string> { "Test Comment 1" }));
            if (posting54.HasError)
                Console.WriteLine(posting54.Errors.First().detailedMessage);
            var localAsset54 = new DatLoadboardAssetPosting()
            {
                AssetId = posting54.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder54.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset54.Save();

            var posting55 = _service.PostAsset(loadOrder55.ToPostAssetRequest(requestRate, EquipmentType.Flatbed, new List<string> { "Test Comment 1" }));
            if (posting55.HasError)
                Console.WriteLine(posting55.Errors.First().detailedMessage);
            var localAsset55 = new DatLoadboardAssetPosting()
            {
                AssetId = posting55.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder55.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset55.Save();

            var posting56 = _service.PostAsset(loadOrder56.ToPostAssetRequest(requestRate, EquipmentType.Flatbed, new List<string> { "Test Comment 1" }));
            if (posting56.HasError)
                Console.WriteLine(posting56.Errors.First().detailedMessage);
            var localAsset56 = new DatLoadboardAssetPosting()
            {
                AssetId = posting56.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder56.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset56.Save();

            var posting57 = _service.PostAsset(loadOrder57.ToPostAssetRequest(requestRate, EquipmentType.Flatbed, new List<string> { "Test Comment 1" }));
            if (posting57.HasError)
                Console.WriteLine(posting57.Errors.First().detailedMessage);
            var localAsset57 = new DatLoadboardAssetPosting()
            {
                AssetId = posting57.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder57.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset57.Save();

            var posting58 = _service.PostAsset(loadOrder58.ToPostAssetRequest(requestRate, EquipmentType.Flatbed, new List<string> { "Test Comment 1" }));
            if (posting58.HasError)
                Console.WriteLine(posting58.Errors.First().detailedMessage);
            var localAsset58 = new DatLoadboardAssetPosting()
            {
                AssetId = posting58.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder58.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset58.Save();

            var posting59 = _service.PostAsset(loadOrder59.ToPostAssetRequest(requestRate, EquipmentType.Flatbed, new List<string> { "Test Comment 1" }));
            if (posting59.HasError)
                Console.WriteLine(posting59.Errors.First().detailedMessage);
            var localAsset59 = new DatLoadboardAssetPosting()
            {
                AssetId = posting59.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder59.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset59.Save();

            var posting60 = _service.PostAsset(loadOrder60.ToPostAssetRequest(requestRate, EquipmentType.Flatbed, new List<string> { "Test Comment 1" }));
            if (posting60.HasError)
                Console.WriteLine(posting60.Errors.First().detailedMessage);
            var localAsset60 = new DatLoadboardAssetPosting()
            {
                AssetId = posting60.PostAssetSuccessData.First().assetId,
                ExpirationDate = DateTime.Now.AddDays(1),
                DateCreated = DateTime.Now,
                IdNumber = loadOrder60.LoadOrderNumber,
                BaseRate = 0,
                UserId = 12948,
                SerializedComments = string.Empty,
                HazardousMaterial = false,
                RateType = RateBasedOnType.Flat,
                Mileage = 0,
                TenantId = 4
            };
            localAsset60.Save();


            Thread.Sleep(new TimeSpan(0,5,0));

            var requestRate3 = new ShipmentRate
            {
                baseRateDollars = 49.99f,
                rateBasedOn = RateBasedOnType.PerMile,
                rateMiles = 0,
                rateMilesSpecified = true
            };

            _service.UpdateAsset(loadOrder1.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting1.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder2.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting2.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder3.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting3.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder4.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting4.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder5.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting5.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder6.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting6.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder7.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting7.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder8.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting8.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder9.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting9.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder10.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting10.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder11.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting11.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder12.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting12.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder13.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting13.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder14.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting14.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder15.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting15.PostAssetSuccessData.First().assetId));

            Thread.Sleep(new TimeSpan(0, 5, 0));

            requestRate3.baseRateDollars = 35.99f;

            _service.UpdateAsset(loadOrder16.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting16.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder17.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting17.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder18.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting18.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder19.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting19.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder20.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting20.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder21.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting21.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder22.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting22.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder23.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting23.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder24.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting24.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder25.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting25.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder26.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting26.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder27.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting27.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder28.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting28.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder29.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting29.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder30.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting30.PostAssetSuccessData.First().assetId));

            Thread.Sleep(new TimeSpan(0, 5, 0));

            requestRate3.baseRateDollars = 43.75f;

            _service.UpdateAsset(loadOrder31.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting31.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder32.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting32.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder33.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting33.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder34.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting34.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder35.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting35.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder36.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting36.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder37.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting37.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder38.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting38.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder39.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting39.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder40.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting40.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder41.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting41.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder42.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting42.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder43.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting43.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder44.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting44.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder45.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting45.PostAssetSuccessData.First().assetId));
            Thread.Sleep(new TimeSpan(0,5,0));

            requestRate3.baseRateDollars = 20.99f;

            _service.UpdateAsset(loadOrder46.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting46.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder47.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting47.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder48.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting48.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder49.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting49.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder50.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting50.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder51.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting51.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder52.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting52.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder53.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting53.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder54.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting54.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder55.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting55.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder56.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting56.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder57.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting57.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder58.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting58.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder59.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting59.PostAssetSuccessData.First().assetId));
            _service.UpdateAsset(loadOrder60.ToUpdateAssetRequest(requestRate3, new List<string> { "Test Comment 1" },
                                                                 posting60.PostAssetSuccessData.First().assetId));

            Thread.Sleep(new TimeSpan(0, 5, 0));


            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting1.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset1.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting2.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset2.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting3.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset3.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting4.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset4.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting5.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset5.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting6.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset6.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting7.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset7.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting8.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset8.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting9.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset9.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting10.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset10.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting11.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset11.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting12.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset12.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting13.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset13.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting14.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset14.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting15.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset15.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting16.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset16.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting17.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset17.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting18.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset18.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting19.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset19.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting20.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset20.Delete();

            Thread.Sleep(new TimeSpan(0, 5, 0));

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting21.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset21.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting22.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset22.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting23.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset23.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting24.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset24.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting25.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset25.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting26.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset26.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting27.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset27.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting28.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset28.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting29.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset29.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting30.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset30.Delete();

            Thread.Sleep(new TimeSpan(0, 5, 0));

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting31.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset31.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting32.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset32.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting33.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset33.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting34.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset34.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting35.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset35.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting36.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset36.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting37.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset37.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting38.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset38.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting39.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset39.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting40.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset40.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting41.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset41.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting42.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset42.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting43.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset43.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting44.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset44.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting45.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset45.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting46.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset46.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting47.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset47.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting48.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset48.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting49.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset49.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting50.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset50.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting51.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset51.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting52.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset52.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting53.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset53.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting54.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset54.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting55.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset55.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting56.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset56.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting57.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset57.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting58.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset58.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting59.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset59.Delete();

            _service.DeleteAsset(new DeleteAssetRequest
            {
                deleteAssetOperation =
                    new DeleteAssetOperation
                    {
                        Item =
                            new DeleteAssetsByAssetIds
                            {
                                assetIds = new[] { posting60.PostAssetSuccessData.First().assetId }
                            }
                    }
            });

            localAsset60.Delete();

        }
    }
}
