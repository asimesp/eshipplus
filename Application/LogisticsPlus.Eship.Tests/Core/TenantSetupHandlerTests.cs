﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Core;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Administration;
using LogisticsPlus.Eship.Processor.Views.Core;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Core
{
	[TestFixture]
	public class TenantSetupHandlerTests
	{
		private TenantView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new TenantView();
			_view.Load();
		}

		[Test]
		public void CanLoadAndSaveExisting()
		{
			var tenant = new Tenant(GlobalTestInitializer.DefaultTenantId, true);
			var name = tenant.Name;
			tenant.Name += "TENANT NAME TEST";
			tenant.LoadCollections();
			_view.SaveRecord(tenant);
            tenant.TakeSnapShot();
			var afterChangeTenant1 = new Tenant(tenant.Id).Name;
			Assert.IsTrue(afterChangeTenant1 == tenant.Name);
			tenant.Name = name;
			tenant.LoadCollections();
			_view.SaveRecord(tenant);
			Assert.IsTrue(afterChangeTenant1 != tenant.Name);
		}

		[Test]
		public void CanCreateNewSaveAndDelete()
		{
			var tenant = new Tenant
			             	{
			             		Name = "testName",
			             		Code = DateTime.Now.ToString("yyyyMMddhhmmssffff"),
								TenantScac = string.Empty,
			             		LogoUrl = string.Empty,
			             		SMCCarrierConnectParameterFile = string.Empty,
			             		SMCRateWareParameterFile = string.Empty,
			             		UpsSmallPackParameterFile = string.Empty,
								MacroPointParameterFile = string.Empty,
			             		FedExSmallPackParameterFile = string.Empty,
								AutoRatingNoticeText = string.Empty,
								RatingNotice = string.Empty,
								TermsAndConditionsFile = string.Empty,
								BatchRatingAnalysisEndTime = TimeUtility.DefaultClose,
								BatchRatingAnalysisStartTime = TimeUtility.DefaultOpen,
                                PaymentGatewayLoginId = string.Empty,
                                PaymentGatewaySecret = string.Empty,
                                PaymentGatewayTransactionId = string.Empty,
                                PaymentGatewayType = PaymentGatewayType.NotApplicable,
								AutoNotificationSubjectPrefix = string.Empty,
			             	};
			var seeds = ProcessorUtilities.GetAll<AutoNumberCode>().Keys
				.Select(k => new AutoNumber { Code = k.ToEnum<AutoNumberCode>(), NextNumber = 10000 })
				.ToList();
			foreach (var seed in seeds) seed.Tenant = tenant;
			tenant.AutoNumbers.AddRange(seeds);

			_view.SaveRecord(tenant);
			Assert.IsTrue(tenant.Id != default(long));

            var criteria = new UserSearchCriteria
            {
                Parameters = new List<ParameterColumn>()
            };
			var users = new UserSearch().FetchUsers(criteria, tenant.Id);
			Assert.IsTrue(users.Count == 1);
			Assert.IsTrue(new User(users[0].Id).UserPermissions.Count == 3);
			_view.DeleteRecord(tenant);
			Assert.IsTrue(!new Tenant(tenant.Id, false).KeyLoaded);

            var userCriteria = new UserSearchCriteria
            {
                Parameters = new List<ParameterColumn>()
            };
			users = new UserSearch().FetchUsers(userCriteria, tenant.Id);
			Assert.IsTrue(users.Count == 0);
		}


		internal class TenantView : ITenantSetupView
		{
		    public AdminUser ActiveSuperUser
		    {
                get { return GlobalTestInitializer.ActiveAdminUser; }
		    }
 

			public Permission GroupPermission
			{
				get
				{
					return new Permission
					       	{
					       		Code = "Group",
								Description = string.Empty,
					       		Grant = true,
					       		Remove = true,
					       		Modify = true,
					       		ModifyApplicable = true,
					       		DeleteApplicable = true,
					       		Deny = false
					       	};
				}
			}

			public Permission UserPermission
			{
				get
				{
					return new Permission
					{
						Code = "User",
						Description = string.Empty,
						Grant = true,
						Remove = true,
						Modify = true,
						ModifyApplicable = true,
						DeleteApplicable = true,
						Deny = false
					};
				}
			}

			public Permission TenantAdminPermission
			{
				get
				{
					return new Permission
					{
						Code = "TenantAdmin",
						Description = string.Empty,
						Grant = true,
						Remove = true,
						Modify = true,
						ModifyApplicable = true,
						DeleteApplicable = true,
						Deny = false
					};
				}
			}

			public event EventHandler<ViewEventArgs<Tenant>> Save;
			public event EventHandler<ViewEventArgs<Tenant>> Delete;

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void SetId(long id)
			{

			}

			public void Load()
			{
				var handler = new TenantSetupHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(Tenant tenant)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<Tenant>(tenant));
			}

			public void DeleteRecord(Tenant tenant)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<Tenant>(tenant));
			}
		}
	}
}
