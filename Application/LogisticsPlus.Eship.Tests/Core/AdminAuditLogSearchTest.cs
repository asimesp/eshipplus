﻿using System;
using System.Diagnostics;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Views.Core;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Core
{
    [TestFixture]
    public class AdminAuditLogSearchTest
    {
        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

		[Test]
		public void CanFetchEntityAuditLog()
		{
			var x  = new AdminAuditLogSearch().FetchEntityAuditLogs(new User().EntityName(), GlobalTestInitializer.DefaultUserId);
			Console.WriteLine("X count: {0}", x.Count);
		}

        [Test]
        public void CanFetchAuditLogsWithCriteria()
        {
            var criteria = new AdminAuditLogSearchCriteria();
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            var logs = new AdminAuditLogSearch().FetchAdminAuditLogs(criteria);
            stopWatch.Stop();
            Console.WriteLine("Elaspsed time: {0} s", stopWatch.Elapsed.Seconds);
            Console.WriteLine("Records pulled: {0}", logs.Count);
        }
    }
}
