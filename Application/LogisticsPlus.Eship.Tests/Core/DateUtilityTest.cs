﻿using System;
using LogisticsPlus.Eship.Core;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Core
{
	[TestFixture]
	public class DateUtilityTest
	{
		[Test]
		public void CanParseKeywordDateWithoutTime()
		{
			const string keyword = DateUtility.KeywordToday;
			Assert.IsTrue(keyword.ParseKeywordDate() == DateTime.Today);
		}

		[Test]
		public void CanParseKeywordDateWithTime()
		{
			const string keyword = DateUtility.KeywordToday + "+01:00";
			Assert.IsTrue(keyword.ParseKeywordDate() == DateTime.Now.SetTime("01:00"));
		}

		[Test]
		public void CanParseKeywordDateWithAdditionalDaysWithoutTime()
		{
			const string keyword = DateUtility.KeywordToday + "+30";
			Assert.IsTrue(keyword.ParseKeywordDate() == DateTime.Today.AddDays(30));
		}

		[Test]
		public void CanParseKeywordDateWithAdditionalDaysWithTime()
		{
			const string keyword = DateUtility.KeywordToday + "+30" + "+01:00";
			Assert.IsTrue(keyword.ParseKeywordDate() == DateTime.Now.AddDays(30).SetTime("01:00"));
		}

		[Test]
		public void CanParseKeywordDateWithAdditionalMonthsWithoutTime()
		{
			const string keyword = DateUtility.KeywordToday + "+4m";
			Assert.IsTrue(keyword.ParseKeywordDate() == DateTime.Today.AddMonths(4));
		}

		[Test]
		public void CanParseKeywordDateWithAdditionalMonthsWithTime()
		{
			const string keyword = DateUtility.KeywordToday + "+3m" + "+04:00";
			Assert.IsTrue(keyword.ParseKeywordDate() == DateTime.Now.AddMonths(3).SetTime("04:00"));
		}
	}
}
