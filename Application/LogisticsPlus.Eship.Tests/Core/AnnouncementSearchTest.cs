﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Views.Core;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Core
{
[TestFixture]
   public class AnnouncementSearchTest
    {
        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

        [Test]
        public void CanFetchAnnouncements()
        {
            var criteria = new AnnouncementViewSearchCriteria();
            var field = CoreSearchFields.Announcements.FirstOrDefault(f => f.Name == "Name");
            if (field == null)
            {
                Console.WriteLine("CoreSearchFields.Announcements no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }

            var column = field.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;

            criteria.Parameters.Add(column);

            var results = new AnnouncementSearch().FetchAnnouncements(criteria, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(results.Any());
        }

        [Test]
        public void CanFetchTenantAnnouncements()
        {
            new AnnouncementSearch().FetchEnabledAnnouncements(GlobalTestInitializer.DefaultTenantId); // all           
        }
    }
}
