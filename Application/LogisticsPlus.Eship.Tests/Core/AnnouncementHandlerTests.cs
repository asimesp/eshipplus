﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Handlers.Core;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Core;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Core
{
    [TestFixture]
    public class AnnouncementHandlerTests
    {
        private AnnouncementView _view;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _view = new AnnouncementView();
            _view.Load();
        }

        [Test]
        public void CanHandleAnnouncementSaveOrUpdate()
        {
            var criteria = new AnnouncementViewSearchCriteria();
            var all = new AnnouncementSearch().FetchAnnouncements(criteria, GlobalTestInitializer.DefaultTenantId);
            if (all.Count == 0) return;
            var announcement = all[0];
            _view.LockRecord(announcement);
            announcement.TakeSnapShot();
            var name = announcement.Name;
            announcement.Name = "test900";
            _view.SaveRecord(announcement);
			announcement.TakeSnapShot();
            announcement.Name = name;
            _view.SaveRecord(announcement);
            _view.UnLockRecord(announcement);
        }

        [Test]
        public void CanHandleAnnouncementSaveAndDelete()
        {
            var announcement = new Announcement
                                   {
                                       Name = "TESTTEST",
                                       Type = AnnouncementType.General,
                                       Message = "save/delete test",
                                       TenantId = GlobalTestInitializer.DefaultTenantId,
                                       Enabled = true,
                                   };
            _view.SaveRecord(announcement);
            _view.LockRecord(announcement);
            Assert.IsTrue(announcement.Id != default(long));
            _view.DeleteRecord(announcement);
            Assert.IsTrue(!new Announcement(announcement.Id, false).KeyLoaded);

        }

        internal class AnnouncementView : IAnnouncementView
        {
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public Dictionary<int, string> AnnouncementTypes { get; set; }

            public event EventHandler Loading;
            public event EventHandler<ViewEventArgs<Announcement>> Save;
            public event EventHandler<ViewEventArgs<Announcement>> Delete;
            public event EventHandler<ViewEventArgs<Announcement>> Lock;
            public event EventHandler<ViewEventArgs<Announcement>> UnLock;
            public event EventHandler<ViewEventArgs<AnnouncementViewSearchCriteria>> Search;

        	public void FaildedLock(Lock @lock)
            {

            }

            public void DisplaySearchResult(List<Announcement> announcements)
            {
                Assert.IsTrue(announcements.Count > 0);
            }

            public void LogException(Exception ex)
            {
                    
            }

            public void DisplayMessages(IEnumerable<ValidationMessage> messages)
            {
                Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
            }

            public void Load()
            {
                var handler = new AnnouncementHandler(this);
                handler.Initialize();
            }

            public void SaveRecord(Announcement announcement)
            {
                if (Save != null)
                    Save(this, new ViewEventArgs<Announcement>(announcement));
            }

            public void DeleteRecord(Announcement announcment)
            {
                if (Delete != null)
                    Delete(this, new ViewEventArgs<Announcement>(announcment));
            }

            public void LockRecord(Announcement announcement)
            {
                if (Lock != null)
                    Lock(this, new ViewEventArgs<Announcement>(announcement));
            }

            public void UnLockRecord(Announcement announcement)
            {
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<Announcement>(announcement));
            }
        }
    }
}

