﻿using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor.Validation.Core;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Core
{
    [TestFixture]
    public class AnnouncementValidatorTest
    {
        private AnnouncementValidator _validator;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _validator = new AnnouncementValidator();
        }

        [Test]
        public void HasRequiredDataTest()
        {
            var announcement = new Announcement
                                   {
                                       Name = "teststuff", 
                                       TenantId = GlobalTestInitializer.DefaultTenantId,
                                       Message = "stuff",
                                       Type = AnnouncementType.Critical,
                                       Enabled = true,
                                   };
 
            Assert.IsTrue(_validator.HasAllRequiredData(announcement));
        }
    }
}

