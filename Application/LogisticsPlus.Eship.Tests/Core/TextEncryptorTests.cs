﻿using System;
using System.Security.Cryptography;
using LogisticsPlus.Eship.Core;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Core
{
	[TestFixture]
	public class TextEncyrptorTests
	{
		byte[] randomKey;
		byte[] randomIV;

		[SetUp]
		public void SetUp()
		{
			using (var crypto = new RijndaelManaged())
			{
				randomKey = crypto.Key;
				randomIV = crypto.IV;
			}
		}

		[Test]
		public void Works()
		{
			const string text = "ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz 1234567890.";

			var encryptor = new TextEncryptor(randomKey, randomIV);

			string encryptedText = encryptor.Encrypt(text);
			Assert.AreNotEqual(string.Empty, encryptedText);

			string decryptedText = encryptor.Decrypt(encryptedText);
			Assert.AreEqual(text, decryptedText);

			Console.WriteLine("Key: " + Convert.ToBase64String(randomKey));
			Console.WriteLine("IV: " + Convert.ToBase64String(randomIV));
			Console.WriteLine("Encypted Text: " + encryptedText);
		}

		[Test]
		public void HandlesBlankStringsOK()
		{
			var encryptor = new TextEncryptor(randomKey, randomIV);
			Assert.AreEqual(string.Empty, encryptor.Encrypt(string.Empty));
			Assert.AreEqual(string.Empty, encryptor.Decrypt(string.Empty));
		}

		[Test]
		public void NewEncryptionKeys()
		{
			using (var crypto = new RijndaelManaged())
			{
				crypto.GenerateKey();
				Console.WriteLine("Writer Key: {0}", Convert.ToBase64String(crypto.Key));
				crypto.GenerateIV();
				Console.WriteLine("Write IV: {0}", Convert.ToBase64String(crypto.IV));
			}
		}
	}
}
