﻿using System.Linq;
using LogisticsPlus.Eship.Core.Cache;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Core
{
	[TestFixture]
	public class CoreCacheInitializerTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanSuccessfullyInitialize()
		{
			CoreCache.Initialize();

			Assert.IsTrue(CoreCache.AccountBuckets.Any());
			Assert.IsTrue(CoreCache.AccountBucketUnits.Any());
			Assert.IsTrue(CoreCache.Assets.Any());
			Assert.IsTrue(CoreCache.ChargeCodes.Any());
			Assert.IsTrue(CoreCache.ContactTypes.Any());
			Assert.IsTrue(CoreCache.Countries.Any());
			Assert.IsTrue(CoreCache.DocumentTags.Any());
			Assert.IsTrue(CoreCache.EquipmentTypes.Any());
			Assert.IsTrue(CoreCache.FailureCodes.Any());
			Assert.IsTrue(CoreCache.InsuranceTypes.Any());
			Assert.IsTrue(CoreCache.MileageSources.Any());
            Assert.IsTrue(CoreCache.P44ChargeCodeMappings.Any());
            Assert.IsTrue(CoreCache.P44ChargeCodeMappings.Any());
            Assert.IsTrue(CoreCache.PackageTypes.Any());
			Assert.IsTrue(CoreCache.Prefixes.Any());
            Assert.IsTrue(CoreCache.QuickPayOptions.Any());
            Assert.IsTrue(CoreCache.Services.Any());
            Assert.IsTrue(CoreCache.ShipmentPriorities.Any());
			Assert.IsTrue(CoreCache.SmallPackagingMaps.Any());
			Assert.IsTrue(CoreCache.SmallPackageServiceMaps.Any());
		}
	}
}
