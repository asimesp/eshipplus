﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Core;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Core
{
    [TestFixture]
    public class EmailLogSearchTests
    {
        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

        [Test]
        public void CanFetchEmailLogs()
        {
            var logs = new EmailLogSearch().FetchEmailLogs(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(logs.Any());
        }

        [Test]
        public void CanFetchUnsentEmails()
        {
            var logs = new EmailLogSearch().FetchEmailLogs(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(logs.Any());

            var unsentLogs = new EmailLogSearch().FetchUnsentEmails();

            Assert.IsTrue((logs.All(l => l.Sent && !unsentLogs.Any())) || (logs.Any(l => !l.Sent) && unsentLogs.Any()));

        }
    }
}
