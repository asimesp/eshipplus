﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Views.Administration;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Core
{
	[TestFixture]
	public class LockTest
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanLockTest()
		{
            var criteria = new UserSearchCriteria
            {
                Parameters = new List<ParameterColumn>()
            };
			var users = new UserSearch().FetchUsers(criteria, GlobalTestInitializer.DefaultTenantId);
			if (users.Count < 2) return;

			var user1 = new User(users[0].Id);
			var user2 = new User(users[1].Id);

			var @lock = user1.ObtainLock(user1, user1.Id);
			Assert.IsNotNull(@lock);
			Assert.IsTrue(@lock.Locked);

			var @lock2 = user1.ObtainLock(user2, user1.Id);
			Assert.AreEqual(@lock2.Id, @lock.Id);

			Assert.IsTrue(@lock.IsUserLock(user1));
			Assert.IsFalse(@lock.LockIsExpired());

			@lock.Delete();

			@lock2 = user1.ObtainLock(user2, user1.Id);
			Assert.IsTrue(@lock2.Id != @lock.Id);

			@lock2.Delete();
		}
	}
}
