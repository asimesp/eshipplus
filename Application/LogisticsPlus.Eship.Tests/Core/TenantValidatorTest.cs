﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation.Core;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Core
{
    [TestFixture]
    public class TenantValidatorTest
    {
        private TenantValidator _validator;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _validator = new TenantValidator();
        }

        [Test]
        public void HasRequiredDataTest()
        {
        	var tenant = new Tenant
        	             	{
        	             		Name = "testName",
        	             		Code = "testVendorCode",
        	             		LogoUrl = "/testUrl.Test",
								TenantScac = "SCAC",
        	             		MailingLocation =
        	             			new TenantLocation
        	             				{
        	             					TenantId = GlobalTestInitializer.DefaultTenantId,
        	             					CountryId = GlobalTestInitializer.DefaultCountryId
        	             				},
        	             		BillingLocation =
        	             			new TenantLocation
        	             				{
        	             					TenantId = GlobalTestInitializer.DefaultTenantId,
        	             					CountryId = GlobalTestInitializer.DefaultCountryId
        	             				},
        	             		BatchRatingAnalysisEndTime = TimeUtility.Default,
        	             		BatchRatingAnalysisStartTime = TimeUtility.Default,
                                PaymentGatewayType = PaymentGatewayType.NotApplicable,
                                PaymentGatewayLoginId = string.Empty,
                                PaymentGatewaySecret = string.Empty,
                                PaymentGatewayTransactionId = string.Empty,
								AutoNotificationSubjectPrefix = string.Empty
        	             	};
        	var seeds = ProcessorUtilities.GetAll<AutoNumberCode>().Keys
        		.Select(k => new AutoNumber {Code = k.ToEnum<AutoNumberCode>(), NextNumber = 10000})
        		.ToList();
			foreach (var seed in seeds) seed.Tenant = tenant;
        	tenant.AutoNumbers.AddRange(seeds);

        	var valid = _validator.HasAllRequiredData(tenant);
			_validator.Messages.PrintMessages();
        	Assert.IsTrue(valid);
        }

		[Test]
		public void IsUniqueTest()
		{
			var code = new Tenant { Code = "165??" }; // 165?? will not be found therefore unique
			Assert.IsTrue(_validator.IsUnique(code));
		}

		[Test]
		public void CanDeleteTest()
		{
			var tenant = new Tenant(GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(tenant.KeyLoaded);
			_validator.Messages.Clear();
			var canDeleteTenant = _validator.CanDeleteTenant(tenant);
			Assert.IsTrue(_validator.Messages.Count > 0);
			Assert.IsFalse(canDeleteTenant);
			_validator.Messages.PrintMessages();
		}

		[Test]
		public void IsExistingAndUniqueTest()
		{
			var all = new TenantSearch().FetchTenants(new List<ParameterColumn>());
			if (all.Count == 0) return;
			var code = all[0];
			Assert.IsTrue(_validator.IsUnique(code)); // this should be unique
			var newCode = new Tenant(1000000, false);
			Assert.IsFalse(newCode.KeyLoaded); // will not be found
			newCode.Code = code.Code;
			Assert.IsFalse(_validator.IsUnique(newCode)); // should be false
		}

		[Test]
		public void IsValidTest()
		{
			Assert.IsFalse(_validator.IsValid(new Tenant {Code = string.Empty}));
		}
    }
}

