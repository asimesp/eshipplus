﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Validation.Core;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Core
{
    [TestFixture]
    public class DeveloperAccessRequestValidatorTest
    {
        private DeveloperAccessRequestValidator _validator;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _validator = new DeveloperAccessRequestValidator();
        }

        [Test]
        public void HasRequiredDataTest()
        {
            var request = new DeveloperAccessRequest
                              {
                                TenantId = GlobalTestInitializer.DefaultTenantId,
                                ContactName = DateTime.Now.ToShortDateString(),
                                ContactEmail = "test@test.com",
                                DateCreated = DateTime.Now,
                                Customer = new Customer(GlobalTestInitializer.DefaultCustomerId)
                              };

            Assert.IsTrue(_validator.IsValid(request));
        }
    }
}

