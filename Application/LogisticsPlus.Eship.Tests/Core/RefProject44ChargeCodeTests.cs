﻿using System;
using System.Text;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Project44;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Core
{
    [TestFixture]
    public class RefProject44ChargeCodeTests
    {
        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

        [Test]
        public void CanSave()
        {
            StringBuilder insertQueries=new StringBuilder();
            var project44ChargeCodes = Enum.GetValues(typeof(Project44ChargeCode));
            int i = 0;
            foreach (Project44ChargeCode p44Code in project44ChargeCodes)
            {
                var description = Project44Constants.GetChargeCodeDescription(p44Code);
                insertQueries.Append($"{i} {description}" + Environment.NewLine);
                i++;
            }

            var result = insertQueries.ToString();
        }
    }
}
