﻿using System;
using LogisticsPlus.Eship.Core;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Core
{
	[TestFixture]
	public class UtitliesTest
	{
		[Test]
		public void CanGetEntityName()
		{
			var name = new TenantLocation(default(long)).EntityName();
			Assert.IsTrue(!string.IsNullOrEmpty(name));

			Console.WriteLine(name);
		}
	}
}
