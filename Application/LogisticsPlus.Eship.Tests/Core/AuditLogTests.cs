﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Core
{
	[TestFixture]
	public class AuditLogTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanSave()
		{
			var user = new User(2);
			var auditLog = new AuditLog
			           	{
			           		Description = "Test",
			           		TenantId = user.TenantId,
			           		User = user,
			           		EntityCode = this.EntityName(),
			           		EntityId = 0.ToString(),
			           		LogDateTime = DateTime.Now
			           	};
			auditLog.Log();
		}
	}
}
