﻿using System;
using System.Text;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Project44;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Core
{
    [TestFixture]
    public class RefProject44ServiceCodeTests
    {
        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

        [Test]
        public void CanSave()
        {
            StringBuilder insertQueries = new StringBuilder();
            var project44ServiceCode = Enum.GetValues(typeof(Project44ServiceCode));
            int i = 0;
            foreach (Project44ChargeCode p44Code in project44ServiceCode)
            {
                var description = Project44Constants.GetChargeCodeDescription(p44Code);
                insertQueries.Append($"{i} {description}" + Environment.NewLine);
                i++;
            }

            var result = insertQueries.ToString();

            /*StringBuilder insertQueries = new StringBuilder();
            var project44ChargeCodes = Enum.GetValues(typeof(Project44ServiceCode));
            foreach (Project44ServiceCode p44Code in project44ChargeCodes)
            {
                var description = Project44Constants.GetServiceCodeDescription(p44Code);
                insertQueries.Append(@"INSERT INTO RefProject44ServiceCode(Project44ServiceCodeIndex,Project44ServiceCodeText,Project44ServiceCodeDescription)
                                       VALUES(" + p44Code.ToInt() + ",'" + p44Code.ToString() + "','" + description + "');" + Environment.NewLine
                                     );
            }

            var result = insertQueries.ToString();*/
        }
    }
}
