﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Core;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Core
{
    [TestFixture]
    public class DeveloperAccessRequestSearchTest
    {
        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

        [Test]
        public void CanFetchRequests()
        {
            var columns = new List<ParameterColumn>();

            var column = CoreSearchFields.CustomerNumber.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            columns.Add(column);

            var field = CoreSearchFields.DeveloperAccessRequests.FirstOrDefault(f => f.Name == "ContactName");
            if (field == null)
            {
                Console.WriteLine("CoreSearchFields.DeveloperAccessRequests no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }

            column = field.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            columns.Add(column);

            var results = new DeveloperAccessRequestSearch().FetchRequests(columns, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(results.Any());
        }

        [Test]
        public void CanFetchRequest()
        {
            new DeveloperAccessRequestSearch().FetchRequest(GlobalTestInitializer.DefaultCustomerId, GlobalTestInitializer.DefaultTenantId);
        }
    }
}
