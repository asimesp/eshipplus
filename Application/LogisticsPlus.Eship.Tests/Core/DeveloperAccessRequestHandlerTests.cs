﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Handlers.Core;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Core;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Core
{
    [TestFixture]
    public class DeveloperAccessRequestHandlerTests
    {
        private DeveloperAccessRequestView _view;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _view = new DeveloperAccessRequestView();
            _view.Load();
        }

        [Test]
        public void CanHandleSave()
        {
            var columns = new List<ParameterColumn>();

            var column = CoreSearchFields.CustomerNumber.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            columns.Add(column);
            var all = new DeveloperAccessRequestSearch().FetchRequests(columns, GlobalTestInitializer.DefaultTenantId);

            if (all.Count == 0) return;
            var accessRequest = all[0];
            var name = accessRequest.ContactName;
            accessRequest.ContactName = DateTime.Now.ToShortDateString();
            _view.SaveRecord(accessRequest);
            accessRequest.ContactName = name;
            _view.SaveRecord(accessRequest);
        }

        internal class DeveloperAccessRequestView : IDeveloperAccessRequestView
        {
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

            public List<string> SupportAreas
            {
                set {  }
            }

            public event EventHandler<ViewEventArgs<DeveloperAccessRequest>> Save;
            public event EventHandler Loading;

            public void LogException(Exception ex)
            {
                    
            }

            public void DisplayMessages(IEnumerable<ValidationMessage> messages)
            {
                Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
            }

            public void Load()
            {
                var handler = new DeveloperAccessRequestHandler(this);
                handler.Initialize();
            }

            public void SaveRecord(DeveloperAccessRequest developerAccessRequest)
            {
                if (Save != null)
                    Save(this, new ViewEventArgs<DeveloperAccessRequest>(developerAccessRequest));
            }
        }
    }
}

