﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Searches.Core;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Core
{
	[TestFixture]
	public class TenantSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

        [Test]
        public void CanFetchTenantsWithParameterColumns()
        {
            var columns = new List<ParameterColumn>();
            var field = AdminSearchFields.Tenants.FirstOrDefault(f => f.Name == "Name");
            if (field == null)
            {
                Console.WriteLine("AdminSearchFields.Tenants no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }
            columns.Add(new ParameterColumn { DefaultValue = SearchUtilities.WildCard, ReportColumnName = field.DisplayName, Operator = Operator.Contains });

            var tenants = new TenantSearch().FetchTenants(columns);

            Assert.IsTrue(tenants.Any());
        }

		[Test]
		public void CanFetchTenantByCode()
		{
			var tenant = new TenantSearch().FetchTenantByCode("TENANT1");

			Assert.IsTrue(tenant.Id != 0);
		}
	}
}
