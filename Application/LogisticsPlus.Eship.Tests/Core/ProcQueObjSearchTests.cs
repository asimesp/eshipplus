﻿using System.Linq;
using LogisticsPlus.Eship.Processor.Searches.Core;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Core
{
	[TestFixture]
	public class ProcQueObjSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanSave()
		{
			var objs = new ProcQueObjSearch().FetchQue();
			//Assert.IsTrue(objs.Any()); TODO: undo this when we have data in the test database!
		}
	}
}
