﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Core;
using LogisticsPlus.Eship.Processor.Searches.Core;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Core
{
	[TestFixture]
	public class LocksCleanUpManagerTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();

			ProcessorVars.RecordLockLimit.Clear();
			foreach(var tenant in new TenantSearch().FetchTenants(new List<ParameterColumn>()).Distinct())
			{
				// 7776000 seconds = 90 days.
				ProcessorVars.RecordLockLimit.Add(tenant.Id, 7776000);
			}
		}

		[Test]
		public void CanPurgeExpiredLocks()
		{
			new LocksCleanUpManager().PurgeExpiredTenantLocks();
		}
	}
}
