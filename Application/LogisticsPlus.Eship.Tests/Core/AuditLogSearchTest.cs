﻿using System;
using System.Diagnostics;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Views.Core;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Core
{
    [TestFixture]
    public class AuditLogSearchTest
    {
        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

        [Test]
        public void CanFetchAuditLog()
        {
            var criteria = new AuditLogViewSearchCriteria(); 

            var field = CoreSearchFields.AuditLogs.FirstOrDefault(f => f.Name == "EntityCode");
            if (field != null) criteria.Parameters.Add(new ParameterColumn { DefaultValue = "User", ReportColumnName = field.DisplayName, Operator = Operator.Equal });
            
        	new AuditLogSearch().FetchAuditLogs(criteria,  GlobalTestInitializer.DefaultTenantId);
        }

		[Test]
		public void CanFetchEntityAuditLog()
		{
			var user = GlobalTestInitializer.ActiveUser;
			var logs = new AuditLogSearch().FetchEntityAuditLogs(user.EntityName(), user.Id, user.TenantId);
			Assert.IsTrue(logs.Count > 0);
		}
    }
}
