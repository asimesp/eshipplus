﻿using System;
using LogisticsPlus.Eship.Core;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Core
{
	[TestFixture]
	public class TimeUtilityTests
	{
		[Test]
		public void PrintTimesTest()
		{
			foreach (var t in TimeUtility.BuildTimeList())
				Console.WriteLine(t);
		}

		[Test]
		public void PrintTimesTestWithLimit()
		{
			foreach (var t in TimeUtility.BuildTimeList(new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second)))
				Console.WriteLine(t);
		}

		[Test]
		public void SetTimeTest()
		{
			var d = DateTime.Now;
			const string timeString = "15:45";

			d = d.SetTime(timeString);

			Assert.IsTrue(d.ToString("HH:mm") == timeString);
		}

		[Test]
		public void IsValidTimeStringTest()
		{
			const string t1 = "15:45";
			const string t2 = "XX:45";

			Assert.IsTrue(t1.IsValidTimeString());
			Assert.IsFalse(t2.IsValidTimeString());
			Assert.IsTrue(t1.IsValidFreeFormTimeString());
			Assert.IsFalse(t2.IsValidFreeFormTimeString());
		}
	}
}
