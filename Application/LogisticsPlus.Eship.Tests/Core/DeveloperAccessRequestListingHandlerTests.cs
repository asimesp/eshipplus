﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Core;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Core;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Core
{
    [TestFixture]
    public class DeveloperAccessRequestListingHandlerTests
    {
        private DeveloperAccessRequestListingView _view;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _view = new DeveloperAccessRequestListingView();
            _view.Load();
        }

        [Test]
        public void CanHandleSaveOrUpdate()
        {
            var columns = new List<ParameterColumn>();

            var column = CoreSearchFields.CustomerNumber.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            columns.Add(column);
            var all = new DeveloperAccessRequestSearch().FetchRequests(columns, GlobalTestInitializer.DefaultTenantId);
            if (all.Count == 0) return;
            var accessRequest = all[0];
            _view.LockRecord(accessRequest);
            accessRequest.TakeSnapShot();
            var name = accessRequest.ContactName;
            accessRequest.ContactName = DateTime.Now.ToShortDateString();
            _view.SaveRecord(accessRequest);
            accessRequest.TakeSnapShot();
            accessRequest.ContactName = name;
            _view.SaveRecord(accessRequest);
            _view.UnLockRecord(accessRequest);
        }

        [Test]
        public void CanHandleSaveOrDelete()
        {
            var request = new DeveloperAccessRequest
                              {
                                  TenantId = GlobalTestInitializer.DefaultTenantId,
                                  ContactName = DateTime.Now.ToShortDateString(),
                                  ContactEmail = "test@test.com",
                                  DateCreated = DateTime.Now,
                                  Customer = new Customer(GlobalTestInitializer.DefaultCustomerId),
                              };
            _view.SaveRecord(request);
            Assert.IsTrue(request.Id != default(long));
            _view.DeleteRecord(request);
            Assert.IsTrue(!new DeveloperAccessRequest(request.Id, false).KeyLoaded);
        }


        internal class DeveloperAccessRequestListingView : IDeveloperAccessRequestListingView
        {
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

            public event EventHandler Loading;
            
            public event EventHandler<ViewEventArgs<DeveloperAccessRequest>> Save;
            public event EventHandler<ViewEventArgs<DeveloperAccessRequest>> Delete;
            public event EventHandler<ViewEventArgs<DeveloperAccessRequest>> Lock;
            public event EventHandler<ViewEventArgs<DeveloperAccessRequest>> UnLock;
            public event EventHandler<ViewEventArgs<DeveloperAccessRequest>> LoadAuditLog;

            public void DisplaySearchResult(List<DeveloperAccessRequest> developerAccessRequests)
            {
                    
            }

            public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
            {
                    
            }

            public void LogException(Exception ex)
            {
                    
            }

            public void DisplayMessages(IEnumerable<ValidationMessage> messages)
            {
                Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
            }

            public void DisplayCustomer(Customer customer){}

            public void FailedLock(Lock @lock){}
           
            public void Set(DeveloperAccessRequest request)
            {
                    
            }

            public void Load()
            {
                var handler = new DeveloperAccessRequestListingHandler(this);
                handler.Initialize();
            }

            public void SaveRecord(DeveloperAccessRequest developerAccessRequest)
            {
                if (Save != null)
                    Save(this, new ViewEventArgs<DeveloperAccessRequest>(developerAccessRequest));
            }


            public void DeleteRecord(DeveloperAccessRequest developerAccessRequest)
            {
                if (Delete != null)
                    Delete(this, new ViewEventArgs<DeveloperAccessRequest>(developerAccessRequest));
            }

            public void LockRecord(DeveloperAccessRequest developerAccessRequest)
            {
                if (Lock != null)
                    Lock(this, new ViewEventArgs<DeveloperAccessRequest>(developerAccessRequest));
            }

            public void UnLockRecord(DeveloperAccessRequest developerAccessRequest)
            {
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<DeveloperAccessRequest>(developerAccessRequest));
            }
        }
    }
}

