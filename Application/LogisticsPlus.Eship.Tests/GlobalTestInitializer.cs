﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Cache;
using LogisticsPlus.Eship.MacroPoint;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Cache;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Connect;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Project44;
using LogisticsPlus.Eship.Smc;
using LogisticsPlus.UpsPlugin;
using LogisticssPlus.FedExPlugin.Services;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Tests
{
	public static class GlobalTestInitializer
	{
		public static bool IsInitialized { get; set; }

		public static long DefaultAssetId { get { return 22; } }

		public static long DefaultTenantId { get { return 4; } }

		public static long DefaultUserId { get { return 2; } }
        
        public static long DefaultGroupId { get { return 1; } }

        public static long DefaultCustomerId { get { return 4; } }

        public static long DefaultEquipmentTypeId { get { return 197; } }

		public static long DefaultMileageSourceId { get { return 776; } }

        public static long DefaultPostedInvoiceWithApplicableMiscReceiptsId { get { return 121793; } }

		public static long DefaultReportTemplate { get { return 12; } }

        public static long DefaultShipmentId { get { return 542; } }

		public static string DefaultLoadNumber { get { return "56539"; } }

		public static long DefaultLoadOrderId { get { return 24; } }

		public static long DefaulCustomerTLTenderingProfileId { get { return 37; } }

		public static long DefaulCustomerTLTenderingProfileLaneId { get { return 130; } }

        public static string ExternalReference1 { get { return " "; } }

		public static string ExternalReference2 { get { return " "; } }

		public static string DefaultSalesRepNumber { get { return "003"; } }

        public static string DefaultJobNumber { get { return "10002"; } }
        
        public static long DefaultVendorRatingId { get { return 521; } }

		public static int DeleteBatchRateDataAfterDays { get { return 7; } }

		public static long DefaultVendorWithTerminalId { get { return 2496; } }

		public static long DefaultShipmentChargeShipmentId { get { return 542; } }

		public static long DefaultServiceTicketChargeSearchServiceTicketId { get { return 158; } }

		public static long DefaultUserIdThatHasShipAs { get { return 1103; } }

        public static long DefaultShipmentIdForWeightBreakTest { get { return 163667; } }

        public static long DefaultShipmentIdForVendorFloorTest { get { return 163696; } }

        public static long DefaultVendorRatingIdForWeightBreakTest { get { return 521; } }

        public static long DefaultVendorRatingIdForVendorFloorTest { get { return 131; } }

		public static long DefaultChargeCodeId
		{
			get { return 23; }
		}

	    public static string DefaultTenantCode
	    {
            get { return "TENANT1"; }
	    }

	    public static string DefaultHost
	    {
	        get { return "wwwtest.myvan.descartes.com"; }
	    }

	    public static string DefaultFtpUsername
	    {
	        get { return "coyesiku"; }
	    }

	    public static string DefaultFtpPassword
	    {
	        get { return "CBgJJ-F8"; }
	    }


	    public static string DefaultSftpHost
	    {
	        get { return "data.logisticsplus.net"; }
	    }

	    public static string DefaultSFtpUsername
	    {
	        get { return "dev.Test"; }
	    }

	    public static string DefaultSFtpPassword
	    {
	        get { return "LpDt#1406@Peach"; }
	    }

        public static long DefaultCountryId
		{
			get { return 1; }
		}

        public static long DefaultAdminUserId { get { return 4; } }

		public static long DefaultVendorBillId { get { return 25571; } }

        public static long DefaultMacroPointOrderId { get { return 1; } }

        public static long DefaultPackageTypeId { get { return 258; } }

		public static string TempFilePath { get { return "C:\\Users\\christopher.oyesiku\\Desktop\\dump\\"; } }


		public static void Initialize()
		{
			DatabaseConnection.DatabaseType = DatabaseType.MsSql;
            DatabaseConnection.DefaultConnectionString = "Server=devserver01\\Sql2012_2;database=PacmanDevelopment;user=developmentUser;password=devlp6s";
            DatabaseConnection.SubstituteParameters = false;

		    AccountingSearchFields.Initialize();
		    AdminSearchFields.Initialize();
		    BusinessIntelligenceSearchFields.Initialize();
			ConnectSearchFields.Initialize();
		    CoreSearchFields.Initialize();
		    OperationsSearchFields.Initialize();
            RegistrySearchFields.Initialize();
            RatingSearchFields.Initialize();

			// Initialize cache
			CoreCache.Initialize();
			ProcessorVars.RegistryCache[DefaultTenantId] = new CachedCollectionItem(DefaultTenantId);

		    IsInitialized = true;
		}

		private static User _activeUser;
		public static User ActiveUser
		{
			get { return _activeUser ?? (_activeUser = new User(DefaultUserId)); }
		}

	    private static AdminUser _activeAdminUser;
	    public static AdminUser ActiveAdminUser
	    {
	        get { return _activeAdminUser ?? (_activeAdminUser = new AdminUser(DefaultAdminUserId)); }
	    }

	    private static SmcServiceSettings _ratewareSettings;
		public static SmcServiceSettings RatewareSettings
		{
			get
			{
				return _ratewareSettings ??
				       (_ratewareSettings =
				        new SmcServiceSettings
				        	{
				        		UserId = "edi@logisticsplus.net",
								//Uri = "http://50.58.179.135/AdminManager/services/RateWareXL",
								Uri = "http://applications.smc3.com/AdminManager/services/RateWareXL",
								//Uri = "http://applications-lou2.smc3.com/AdminManager/services/RateWareXL",
								Password = "kVwpz14K",
								License = "aub75Gp0OoiO"
				        	});
			}
		}

		private static SmcServiceSettings _carrierConnectSettings;
		private static SmcServiceSettings CarrierConnectSettings
		{
			get
			{
				return _carrierConnectSettings ??
					   (_carrierConnectSettings =
						new SmcServiceSettings
						{
							UserId = "edi@logisticsplus.net",
							Uri = "http://demo.smc3.com/AdminManager/services/CarrierConnectWS",
							Password = "kVwpz14K",
							License = "I2V305ThKb73"
						});
			}
		}

		private static SmcServiceSettings _carrierConnectSettings2;
		public static SmcServiceSettings CarrierConnectSettings2
		{
			get
			{
				return _carrierConnectSettings2 ??
					   (_carrierConnectSettings2 =
						new SmcServiceSettings
						{
							UserId = "edi@logisticsplus.net",
							//Uri = "http://199.244.253.197/AdminManager/services/CarrierConnectXL", // carrier connect XL
							Uri = "http://ccxl.smc3.com/AdminManager/services/CarrierConnectXL", // carrier connect XL
							Password = "kVwpz14K",
							License = "UxZXHmGs1u6x" // carrier connect XL
						});
			}
		}

		private static ServiceParams _fedExServiceParams;
		public static ServiceParams FedExServiceParams
		{
			get
			{
				return _fedExServiceParams ??
				       (_fedExServiceParams = new ServiceParams
				                         	{
				                         		AccountNumber = "510087828",
				                         		Key = "9UEf8RsV2CQlLGDv",
				                         		LocalCountry = "US",
				                         		MeterNumber = "100007880",
				                         		Password = "VE13hWtldicjVz3igJt00gP8L",
				                         		TransactionId = "Test",
				                         		Url = "https://gatewaybeta.fedex.com:443/web-services"
				                         	});
			}
		}

	    private static MacroPointSettings _macroPointSettings;
	    public static MacroPointSettings MacroPointSettings
	    {
	        get
	        {
		        return _macroPointSettings ??
		               (_macroPointSettings = new MacroPointSettings
			               {
				               UserId = "5035246",
				               Password = "SuJ1t@$Htsb",
				               Key = "5035246",
				               PriceBreaks = new List<TrackingPriceBreak>
					               {
						               new TrackingPriceBreak
							               {
								               Category = "Critical",
								               Price = "5",
								               TrackDurationHours = TrackDuration.OneDay.ToInt().GetString(),
								               TrackInternvalMinutes = TrackInterval.OneHour.ToInt().GetString()
							               },
					               }
			               });
	        }
	    }

		private static UpsServiceParams _upsServiceParams;

		public static UpsServiceParams UpsServiceParams
		{
			get
			{
				return _upsServiceParams ??
				       (_upsServiceParams = new UpsServiceParams
				                            	{
				                            		AccessKey = "1C586A40E29E7134",
				                            		Username = "ckoutzarov",
				                            		Password = "Log1st1cs",
				                            		LabelFormat = UpsLabelFormat.Gif,
				                            		LabelHeight = 0,
				                            		LabelWidth = 0,
				                            		ValidateAddressesWhenShipping = true,
				                            		RateServiceUrl = "https://wwwcie.ups.com/webservices/Rate",
				                            		ShipServiceUrl = "https://wwwcie.ups.com/webservices/Ship",
				                            		VoidServiceUrl = "https://wwwcie.ups.com/webservices/Void",
				                            		TimeInTransitServiceUrl = "https://wwwcie.ups.com/webservices/TimeInTransit",
				                            		ShipperName = "Logistics Plus, Inc",
				                            		ShipperAttentionName = "Logistics Plus, Inc",
				                            		ShipperTaxIdentificationNumber = string.Empty,
				                            		ShipperAccountNumber = "8W6W24",
				                            		ShipperPhone = "8144617643",
				                            		ShipperFax = string.Empty,
				                            		ShipperEmailAddress = string.Empty,
				                            		ShipperAddressLine1 = "1406 Peach Street",
				                            		ShipperAddressLine2 = string.Empty,
				                            		ShipperCity = "Erie",
				                            		ShipperCountryCode = "US",
				                            		ShipperPostalCode = "16501",
				                            		ShipperStateProvinceCode = "PA"
				                            	});
			}
		}

	    public static Project44ServiceSettings Project44Settings
	    {
            get
            {
                return new Project44ServiceSettings
                {
                    //Hostname = "https://test.p-44.com",
                    //Username = "logisticsplus_testing@p-44.com",
                    //Password = "project44!",
                    Hostname = "https://cloud.p-44.com",
                    Username = "logisticsplus.prod@p-44.com",
                    Password = "Logisticsplus2018!",
                    PrimaryLocationId = "a62b8e6f-100a-495a-95f9-7ee1f39573ed"
                };
            }
	    }
	}
}
