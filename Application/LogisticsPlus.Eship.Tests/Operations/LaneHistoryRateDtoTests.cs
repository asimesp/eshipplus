﻿using System;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
	[TestFixture]
	public class LaneHistoryRateDtoTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}
		
		[Test]
		[Ignore("Test is being refactored as functionality is changing")]
		public void FetchHistoricalAndContractRates()
		{
			var criteria = new LaneHistorySearchCriteria
			               	{
			               		OriginCountryId = GlobalTestInitializer.DefaultCountryId,
			               		OriginPostalCode = "16501",
			               		DestinationCountryId = GlobalTestInitializer.DefaultCountryId,
			               		DestinationPostalCode = "44445",
								DesiredPickupDate = DateTime.Now
			               	};
			new LaneHistoryRateDto().VendorLaneHistory(GlobalTestInitializer.DefaultTenantId, criteria);
		}
	}
}
