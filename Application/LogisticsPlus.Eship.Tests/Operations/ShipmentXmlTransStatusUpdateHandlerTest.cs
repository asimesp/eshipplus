﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
	public class ShipmentXmlTransStatusUpdateHandlerTest
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanSaveExistingShipment()
		{
			var handler = new ShipmentUpdateHandler();
			var criteria = new ShipmentViewSearchCriteria { ActiveUserId = GlobalTestInitializer.DefaultUserId, };
			var all = new ShipmentSearch().FetchShipmentsDashboardDto(criteria, GlobalTestInitializer.DefaultTenantId);

			if (all.Count == 0) return;
			var shipment = new Shipment(all[0].Id);
			//shipment.LoadCollections();

			var x = shipment.CheckCalls.Any(); // hack to load check calls before processing.

			var proSave = shipment.Vendors[0].ProNumber;
			shipment.Vendors[0].TakeSnapShot();
			shipment.Vendors[0].ProNumber = "TestTestTest";

			Exception ex;
			handler.UpdateShipmentTrackingInfo(shipment, out ex);
			if (ex != null) Console.WriteLine(ex);

			shipment.Vendors[0].TakeSnapShot();
			shipment.Vendors[0].ProNumber = proSave;
			handler.UpdateShipmentTrackingInfo(shipment, out ex);
			if (ex != null) Console.WriteLine(ex);
		}

        [Test]
        public void CanSaveShipmentCharge()
        {
            var criteria = new ShipmentViewSearchCriteria { ActiveUserId = GlobalTestInitializer.DefaultUserId, };
            var shipments = new ShipmentSearch().FetchShipmentsDashboardDto(criteria, GlobalTestInitializer.DefaultTenantId);
            if (!shipments.Any()) return;
            var shipment = new Shipment(shipments[0].Id);

            var chargeCodes = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].ChargeCodes;
            if (!chargeCodes.Any()) return;
            var chargeCode = chargeCodes[0];

            var charge = new ShipmentCharge
            {
                ChargeCode = chargeCode,
                Comment = string.Empty,
                Quantity = 1,
                Shipment = shipment,
                TenantId = GlobalTestInitializer.DefaultTenantId,
                UnitBuy = default(decimal),
                UnitSell = default(decimal),
                UnitDiscount = default(decimal),
            };

            Exception ex;
            var errMsgs = new ShipmentUpdateHandler().SaveShipmentCharge(charge, out ex, GlobalTestInitializer.ActiveUser);
            Assert.IsTrue(charge.Id != default(long));
            Assert.IsTrue(!errMsgs.Any());
            charge.Delete();
        }
	}
}
