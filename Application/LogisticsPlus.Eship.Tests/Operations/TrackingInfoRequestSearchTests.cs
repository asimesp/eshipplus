﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
	[TestFixture]
	public class TrackingInfoRequestSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchShipments()
		{
			var criteria = new ShipmentViewSearchCriteria { ActiveUserId = GlobalTestInitializer.DefaultUserId, };
		    var shipments = new ShipmentSearch().FetchShipmentsDashboardDto(criteria, GlobalTestInitializer.DefaultTenantId);

            if (!shipments.Any()) return;

		    var scac = shipments.Select(s=>new Shipment(s.Id)).First(s => s.Vendors.Any()).Vendors[0].Vendor.Scac;

			new TrackingInfoRequestDto().FetchTrackingInfoRequests(scac, DateTime.Now.AddMonths(-6), GlobalTestInitializer.DefaultTenantId);
		}
	}
}
