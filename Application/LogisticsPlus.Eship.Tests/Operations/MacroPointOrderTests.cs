﻿using LogisticsPlus.Eship.Core.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
	[TestFixture]
	public class MacroPointOrderTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanRetrieveCorrespondingShipment()
		{
			var order = new MacroPointOrder(GlobalTestInitializer.DefaultMacroPointOrderId);
			if (!order.KeyLoaded) return;
			Assert.IsNotNull(order.RetrieveCorrespondingShipment());
		}

		[Test]
		public void CanLoadByOrderIdAndTenantId()
		{
			var order = new MacroPointOrder(GlobalTestInitializer.DefaultMacroPointOrderId);
			if (!order.KeyLoaded) return;
			var order2 = new MacroPointOrder(order.OrderId, order.TenantId);
			Assert.IsTrue(order.Id == order2.Id);
		}
	}
}
