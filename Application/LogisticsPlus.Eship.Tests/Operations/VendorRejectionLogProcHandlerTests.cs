﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
	[TestFixture]
	public class VendorRejectionLogProcHandlerTests
	{
		private VendorRejectionLogView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new VendorRejectionLogView();
		}

		[Test]
		public void CanLogVendorRejection()
		{
			var allVendors = new VendorSearch().FetchVendors(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			var vendor = allVendors.FirstOrDefault();
			if (vendor == null) return;
			var log = new VendorRejectionLog
			          	{
			          		UserId = GlobalTestInitializer.DefaultUserId,
			          		VendorId = vendor.Id,
			          		TenantId = GlobalTestInitializer.DefaultTenantId,
							CustomerId = GlobalTestInitializer.DefaultCustomerId,
			          		OriginPostalCode = "16501",
			          		OriginCity = "Erie",
			          		OriginState = "PA",
			          		OriginCountryCode = "US",
			          		DestinationPostalCode = "16502",
			          		DestinationCountryCode = "US",
			          		DestinationCity = "Erie",
			          		DestinationState = "PA",
			          		ShipmentNumber = "00000",
			          		ShipmentDateCreated = DateTime.Now,
			          		DateCreated = DateTime.Now,
							ServiceMode = ServiceMode.NotApplicable
			          	};
			_view.LogRejection(log);
			Assert.IsTrue(!log.IsNew);

			// clean up log from database!
			log.Delete();
			Assert.IsTrue(!new VendorRejectionLog(log.Id).KeyLoaded);
		}
	}

	internal class VendorRejectionLogView : IVendorRejectionLogProcView
	{
		private readonly User _user;

		public User ActiveUser
		{
			get { return _user; }
		}

		public event EventHandler<ViewEventArgs<VendorRejectionLog>> LogVendorRejection;

		public void DisplayMessages(IEnumerable<ValidationMessage> messages)
		{
			messages.PrintMessages();
			Assert.IsTrue(!messages.Any(m => m.Type == ValidateMessageType.Error));
		}

		public void LogException(Exception ex)
		{
			Console.WriteLine(ex.ToString());
		}

		public VendorRejectionLogView()
		{
			_user = GlobalTestInitializer.ActiveUser;

			new VendorRejectionLogProcHandler(this).Initialize();
		}

		public void LogRejection(VendorRejectionLog log)
		{
			if (LogVendorRejection != null)
				LogVendorRejection(this, new ViewEventArgs<VendorRejectionLog>(log));
		}
	}
}
