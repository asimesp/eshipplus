﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.MacroPoint;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
    [TestFixture]
    public class MacroPointValidatorTests
    {
        private MacroPointOrderValidator _validator;
        private MacroPointOrder _order;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _validator = new MacroPointOrderValidator();

            if (_order != null) return;

            var tenantId = GlobalTestInitializer.DefaultTenantId;

            _order = new MacroPointOrder
                {
                    IdNumber = "00000",
                    User = GlobalTestInitializer.ActiveUser,
                    TenantId = tenantId,
                    TrackDurationHours = TrackDuration.OneDay.ToInt(),
                    TrackIntervalMinutes = TrackInterval.FourHours.ToInt(),
                    TrackCost = 5,
                    OrderId = "012345",
                    TrackingStatusCode = OrderTrackingStatusCodes.RequestingInstallation.ToInt().GetString(),
                    TrackingStatusDescription = OrderTrackingStatusCodes.RequestingInstallation.FormattedString(),
					Notes = string.Empty,
					EmailCopiesOfUpdatesTo = string.Empty,
                    NumberType = NumberType.Mobile.GetString(),
                    Number = "555-555-5555",
                    DateStopRequested = DateUtility.SystemEarliestDateTime,
                    StartTrackDateTime = DateTime.Now,
                    DateCreated = DateTime.Now,
                };
        }

        [Test]
        public void IsValidTest()
        {
            Assert.IsTrue(_validator.IsValid(_order));
        }

        [Test]
        public void HasRequiredDataTest()
        {
            var hasAllRequiredData = _validator.HasAllRequiredData(_order);
            if (!hasAllRequiredData) _validator.Messages.PrintMessages();
            Assert.IsTrue(hasAllRequiredData);
        }
        
        [Test]
        public void DuplicateIdNumberTest()
        {
            var existingOrder = new MacroPointOrder(GlobalTestInitializer.DefaultMacroPointOrderId);
            Assert.IsTrue(existingOrder.KeyLoaded);

            var number = _order.IdNumber;
            _order.IdNumber = existingOrder.IdNumber;
            Assert.IsTrue(_validator.DuplicateIdNumber(_order));
            _order.IdNumber = number;
        }

        [Test]
        public void DuplicateOrderIdTest()
        {
            var existingOrder = new MacroPointOrder(GlobalTestInitializer.DefaultMacroPointOrderId);
            Assert.IsTrue(existingOrder.KeyLoaded);

            var orderId = _order.OrderId;
            _order.OrderId = existingOrder.OrderId;
            Assert.IsTrue(_validator.DuplicateOrderId(_order));
            _order.OrderId = orderId;
        }
    }
}
