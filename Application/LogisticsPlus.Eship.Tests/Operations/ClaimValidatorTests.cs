﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.Processor.Views.Administration;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
	[TestFixture]
	public class ClaimValidatorTests
	{
		private ClaimValidator _validator;
		private Claim _claim;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new ClaimValidator();

			if (_claim != null) return;

			var tenantId = GlobalTestInitializer.DefaultTenantId;
			var customer = new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), tenantId).FirstOrDefault() ?? new Customer();
			var vendor = new VendorSearch().FetchVendors(new List<ParameterColumn>(), tenantId).FirstOrDefault() ?? new Vendor();

            var userCriteria = new UserSearchCriteria
            {
                Parameters = new List<ParameterColumn>()
            };
			var userDto = new UserSearch().FetchUsers(userCriteria, tenantId).FirstOrDefault();
            var user = userDto == null ? new User() : new User(userDto.Id);
            var docTag = ProcessorVars.RegistryCache[tenantId].DocumentTags.FirstOrDefault() ?? new DocumentTag();

			var shipmentCriteria = new ShipmentViewSearchCriteria { ActiveUserId = GlobalTestInitializer.DefaultUserId, };

			var shipment = new ShipmentSearch().FetchShipmentsDashboardDto(shipmentCriteria, tenantId).FirstOrDefault() ?? new ShipmentDashboardDto();

			var criteria = new ServiceTicketViewSearchCriteria
			{
				ActiveUserId = GlobalTestInitializer.DefaultUserId,
			};

			var dto = new ServiceTicketSearch().FetchServiceTicketDashboardDtos(criteria, tenantId).FirstOrDefault();
			var serviceTicket = dto == null ? new ServiceTicket() : new ServiceTicket(dto.Id);


			_claim = new Claim
							{
								ClaimNumber = "00000",
								ClaimDetail = "",
								Customer = customer,
								DateCreated = DateTime.Now,
								Status = ClaimStatus.Open,
								User = user,
								TenantId = tenantId,
								Vendors = new List<ClaimVendor>(),
								Shipments = new List<ClaimShipment>(),
								Notes = new List<ClaimNote>(),
								ServiceTickets = new List<ClaimServiceTicket>(),
                                ClaimantReferenceNumber = string.Empty,
                                AmountClaimed = default(long),
                                AmountClaimedType = AmountClaimedType.NotApplicable,
                                ClaimType = ClaimType.NotApplicable,
                                IsRepairable = false,
                                EstimatedRepairCost = default(long),
                                DateLpAcctToPayCarrier = DateTime.Now,
                                Acknowledged = DateTime.Now,
                                CheckAmount = 100.9.ToDecimal(),
                                CheckNumber = "122333333"

							};


			_claim.Vendors.Add(new ClaimVendor
			{
				Claim = _claim,
				Vendor = vendor,
				TenantId = tenantId,
                ProNumber = string.Empty,
                FreightBillNumber = string.Empty
			});

			_claim.Notes.Add(new ClaimNote
			{
				Archived = false,
				Classified = false,
				Message = "Note Test Message",
				Claim = _claim,
				TenantId = tenantId,
				Type = NoteType.Normal,
				User = user,
                SendReminderOn = DateUtility.SystemEarliestDateTime,
                DateCreated = DateTime.Now,
                SendReminderEmails = string.Empty
			});

			_claim.Shipments.Add(new ClaimShipment
			{
				Claim = _claim,
				ShipmentId = shipment.Id,
				TenantId = tenantId
			});

			_claim.ServiceTickets.Add(new ClaimServiceTicket
			{
				Claim = _claim,
				ServiceTicketId = serviceTicket.Id,
				TenantId = tenantId
			});

			_claim.Documents.Add(new ClaimDocument
			{
				TenantId = tenantId,
				Claim = _claim,
				Name = "Document",
				Description = "Description",
				DocumentTagId = docTag.Id,
				LocationPath = "test"
			});
		}

		[Test]
		public void IsValidTest()
		{
			Assert.IsTrue(_validator.IsValid(_claim));
		}

		[Test]
		public void HasRequiredDataTest()
		{
			var hasAllRequiredData = _validator.HasAllRequiredData(_claim);
			if (!hasAllRequiredData) _validator.Messages.PrintMessages();
			Assert.IsTrue(hasAllRequiredData);
		}

		[Test]
		public void DuplicateClaimNumberTest()
		{
			var criteria = new ClaimViewSearchCriteria {ActiveUserId = GlobalTestInitializer.DefaultUserId};

			var all = new ClaimSearch().FetchClaimDtos(criteria, GlobalTestInitializer.DefaultTenantId);

			if (all.Count == 0) return;

			var number = _claim.ClaimNumber;
			_claim.ClaimNumber = all[0].ClaimNumber;
			Assert.IsTrue(_validator.DuplicateClaimNumber(_claim));
			_claim.ClaimNumber = number;
		}

		[Test]
		public void DuplicateClaimServiceTicketTest()
		{
			var serviceTicket = new ClaimServiceTicket();
			var criteria = new ClaimViewSearchCriteria {ActiveUserId = GlobalTestInitializer.DefaultUserId};
			Assert.IsFalse(_validator.ClaimServiceTicketExists(serviceTicket));
			var all = new ClaimSearch().FetchClaimDtos(criteria, GlobalTestInitializer.DefaultTenantId)
				.Select(c => new Claim(c.Id))
				.Where(c => c.ServiceTickets.Count > 0)
				.ToList();
			if (all.Count == 0) return;
			serviceTicket = all[0].ServiceTickets[0];
			Assert.IsTrue(_validator.ClaimServiceTicketExists(serviceTicket));
		}

		[Test]
		public void DuplicateClaimShipmentsTest()
		{
			var shipment = new ClaimShipment();
			var criteria = new ClaimViewSearchCriteria {ActiveUserId = GlobalTestInitializer.DefaultUserId};
			Assert.IsFalse(_validator.ClaimShipmentExists(shipment));
			var all = new ClaimSearch().FetchClaimDtos(criteria, GlobalTestInitializer.DefaultTenantId)
				.Select(c => new Claim(c.Id))
				.Where(c => c.Shipments.Count > 0)
				.ToList();
			if (all.Count == 0) return;
			shipment = all[0].Shipments[0];
			Assert.IsTrue(_validator.ClaimShipmentExists(shipment));
		}

        [Test]
        public void DuplicateClaimVendorsTest()
        {
            var claimVendor = new ClaimVendor();
            var criteria = new ClaimViewSearchCriteria { ActiveUserId = GlobalTestInitializer.DefaultUserId };
            Assert.IsFalse(_validator.ClaimVendorExists(claimVendor));
            var all = new ClaimSearch().FetchClaimDtos(criteria, GlobalTestInitializer.DefaultTenantId)
                .Select(c => new Claim(c.Id))
                .Where(c => c.Vendors.Count > 0)
                .ToList();
            if (all.Count == 0) return;
            claimVendor = all[0].Vendors[0];
            Assert.IsTrue(_validator.ClaimVendorExists(claimVendor));
        }
	}
}