﻿using System;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
    [TestFixture]
    public class  LibraryItemValidatorTests
    {
        private LibraryItemValidator _validator;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _validator = new LibraryItemValidator();
        }

        [Test]
        public void HasAllRequiredDataTest()
        {
            var customer = new Customer(GlobalTestInitializer.DefaultCustomerId);
            var packages = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].PackageTypes;
            if(packages.Count == 0)return;
            var package = packages[0];

            var libraryItem = new LibraryItem
                                  {
                                      TenantId = GlobalTestInitializer.DefaultTenantId,
                                      Customer = customer,
                                      Description = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff"),
                                      PieceCount = 1,
                                      PackageTypeId = package.Id,
                                      Quantity = 1,
                                      Weight = 1,
                                      Length = 1,
                                      Width = 1,
                                      Height = 1,
                                      NMFCCode = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff"),
                                      HTSCode = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff"),
                                      Comment = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff")
                                  };

        	Assert.IsTrue(_validator.IsValid(libraryItem));
        }
    }
}
