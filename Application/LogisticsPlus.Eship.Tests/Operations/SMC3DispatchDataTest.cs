﻿using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Smc.Eva;
using NUnit.Framework;
using System;

namespace LogisticsPlus.Eship.Tests.Operations
{
    [TestFixture]
	public class SMC3DispatchDataTest
    {
		
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
        [Ignore("Test is Disabled")]
        public void CanSMCvalid()
		{
            var validator = new SMC3DispatchDataValidator();
            var data = new SMC3DispatchData();
            validator.IsValid(data);
        }

        [Test]
        [Ignore("Test is Disabled")]
        public void CanSaveSMC3TrackingData()
        {
            var tdp = new Smc3EvaDataProcessor();
            var data = new DispatchData();
            Exception ex;
            tdp.SaveDispatchData(data, out ex).PrintMessages();
        }
    }
}