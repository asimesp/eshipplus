﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.Processor.Views.Administration;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
    [TestFixture]
    public class ShipmentHandlerTests
    {
        private ShipmentView _view;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _view = new ShipmentView();
            _view.Load();
        }

        [Test]
        public void CanSaveOrUpdate()
        {
			var criteria = new ShipmentViewSearchCriteria { ActiveUserId = GlobalTestInitializer.DefaultUserId, };
            var all = new ShipmentSearch().FetchShipmentsDashboardDto(criteria, GlobalTestInitializer.DefaultTenantId);

            if (all.Count == 0) return;
            var shipment = new Shipment(all[0].Id);
            shipment.LoadCollections();
            _view.LockRecord(shipment);
            shipment.TakeSnapShot();       	
            var number = shipment.PurchaseOrderNumber;
            shipment.PurchaseOrderNumber = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff");
            _view.SaveRecord(shipment);
            shipment.TakeSnapShot();
            shipment.PurchaseOrderNumber = number;
            _view.SaveRecord(shipment);
            _view.UnLockRecord(shipment);
        }

        [Test]
        public void CanSaveAndDelete()
        {
            var shipment = GetShipment();
            _view.SaveRecord(shipment);
            Assert.IsTrue(shipment.Id != default(long));
            _view.DeleteRecord(shipment);
            Assert.IsTrue(!new Shipment(shipment.Id, false).KeyLoaded);

        }

        private static Shipment GetShipment()
        {
            var tenantId = GlobalTestInitializer.DefaultTenantId;
            var customer = new CustomerSearch().FetchCustomers( new CustomerViewSearchCriteria(), tenantId).FirstOrDefault() ?? new Customer();
            var vendor = new VendorSearch().FetchVendors(new List<ParameterColumn>(), tenantId).FirstOrDefault() ?? new Vendor();
            var country = ProcessorVars.RegistryCache.Countries.Values.FirstOrDefault() ?? new Country();

            var criteria = new UserSearchCriteria
            {
                Parameters = new List<ParameterColumn>()
            };
			var userDto = new UserSearch().FetchUsers(criteria, tenantId).FirstOrDefault();
            var user = userDto == null ? new User() : new User(userDto.Id);
            var equipment = ProcessorVars.RegistryCache[tenantId].EquipmentTypes.FirstOrDefault() ??
                            new EquipmentType();
            var packageType = ProcessorVars.RegistryCache[tenantId].PackageTypes.FirstOrDefault() ??
                              new PackageType();
            var service = new ServiceSearch().FetchServices(new List<ParameterColumn>(), tenantId).FirstOrDefault() ??
                          new ServiceViewSearchDto();
            var chargeCode = ProcessorVars.RegistryCache[tenantId].ChargeCodes.FirstOrDefault() ?? new ChargeCode();
            var failureCode = ProcessorVars.RegistryCache[tenantId].FailureCodes.FirstOrDefault() ?? new FailureCode();
            var accountBucket = ProcessorVars.RegistryCache[tenantId].AccountBuckets.FirstOrDefault() ??
                                new AccountBucket();
            var documentTag = ProcessorVars.RegistryCache[tenantId].DocumentTags.FirstOrDefault() ??
                                new DocumentTag();



            var assets = ProcessorVars.RegistryCache[tenantId].Assets;

            var timeList = TimeUtility.BuildTimeList();

            var shipment = new Shipment
            {
                ShipmentNumber = "00000",
                Customer = customer,
                PrefixId = default(long),
                HidePrefix = false,
                ServiceMode = ServiceMode.LessThanTruckload,
                Notes = new List<ShipmentNote>(),
                CustomerReferences = new List<ShipmentReference>(),
                ShipperBol = string.Empty,

                GeneralBolComments = string.Empty,
                CriticalBolComments = string.Empty,
                MiscField1 = string.Empty,
                MiscField2 = string.Empty,

				DriverName = string.Empty,
				DriverPhoneNumber = string.Empty,
				DriverTrailerNumber = string.Empty,

                AccountBucketUnitId = 0,

                EarlyDelivery = timeList[0],
                EarlyPickup = timeList[0],

                DateCreated = DateTime.Now,
                EstimatedDeliveryDate = DateTime.Now,
                ActualDeliveryDate = DateTime.Now,
                DesiredPickupDate = DateTime.Now,
                ActualPickupDate = DateTime.Now,

                Charges = new List<ShipmentCharge>(),
                Vendors = new List<ShipmentVendor>(),
                Stops = new List<ShipmentLocation>(),
                Status = ShipmentStatus.Scheduled,

                User = user,
                Equipments = new List<ShipmentEquipment>(),
                HazardousMaterial = true,
                HazardousMaterialContactEmail = "test@test.com",
                HazardousMaterialContactMobile = "111-111-1111",
                HazardousMaterialContactName = "Haz Mat. Contact",
                HazardousMaterialContactPhone = "111-222-3333",
                Items = new List<ShipmentItem>(),
                LateDelivery = timeList[10],
                LatePickup = timeList[10],
                PurchaseOrderNumber = "PO#",
                Services = new List<ShipmentService>(),
                ShipperReference = "Ref#",
                TenantId = tenantId,
                OriginalRateValue = 0,
                VendorDiscountPercentage = 0,
                VendorFreightFloor = 0,
                VendorFuelPercent = 0,
                CustomerFreightMarkupValue = 0,
                CustomerFreightMarkupPercent = 0,
                UseLowerCustomerFreightMarkup = false,
                BilledWeight = 0,
                ApplyDiscount = false,
                LineHaulProfitAdjustmentRatio = 0,
                FuelProfitAdjustmentRatio = 0,
                AccessorialProfitAdjustmentRatio = 0,
                ServiceProfitAdjustmentRatio = 0,
                ResellerAdditionalFreightMarkup = 0,
                ResellerAdditionalFreightMarkupIsPercent = false,
                ResellerAdditionalFuelMarkup = 0,
                ResellerAdditionalFuelMarkupIsPercent = false,
                ResellerAdditionalAccessorialMarkup = 0,
                ResellerAdditionalAccessorialMarkupIsPercent = false,
                DirectPointRate = false,
                SmallPackType = string.Empty,
                VendorRatingOverrideAddress = string.Empty,
                SmallPackageEngine = SmallPackageEngine.None,
                AutoRatingAccessorials = new List<ShipmentAutoRatingAccessorial>(),
                Mileage = 1, 
                ShipmentCoordinatorId = GlobalTestInitializer.DefaultUserId,
                CarrierCoordinatorId = GlobalTestInitializer.DefaultUserId,
				SalesRepAddlEntityName = string.Empty,
                GuaranteedDeliveryServiceTime = string.Empty,
                IsGuaranteedDeliveryService = false,
                GuaranteedDeliveryServiceRateType = RateType.Flat,
		        GuaranteedDeliveryServiceRate = default(decimal),
                InDispute = false,
                InDisputeReason = InDisputeReason.NotApplicable,
                Project44QuoteNumber = string.Empty,
                IsExpeditedService = false,
                ExpeditedServiceRate = 0,
                ExpeditedServiceTime = string.Empty
            };

            shipment.Origin = new ShipmentLocation
            {
                Shipment = shipment,
                City = "Erie",
                Contacts = new List<ShipmentContact>(),
                Country = country,
                Description = "Description",
                PostalCode = "16504",
                State = "Pa",
                StopOrder = 0,
                Street1 = "123 St.",
                Street2 = "Street 2",
                TenantId = tenantId,
				Direction = string.Empty,
				GeneralInfo = string.Empty,
				SpecialInstructions = string.Empty,
				AppointmentDateTime = DateUtility.SystemEarliestDateTime
            };

            shipment.Destination = new ShipmentLocation
            {
                Shipment = shipment,
                City = "Erie",
                Contacts = new List<ShipmentContact>(),
                Country = country,
                Description = "Description",
                PostalCode = "16504",
                State = "Pa",
                StopOrder = 2,
                Street1 = "123 St.",
                Street2 = "Street 2",
                TenantId = tenantId,
				Direction = string.Empty,
				GeneralInfo = string.Empty,
				SpecialInstructions = string.Empty,
				AppointmentDateTime = DateUtility.SystemEarliestDateTime
            };

            shipment.Stops.Add(new ShipmentLocation
            {
                Shipment = shipment,
                City = "Erie",
                Contacts = new List<ShipmentContact>(),
                Country = country,
                Description = "Description",
                PostalCode = "16504",
                State = "Pa",
                StopOrder = 1,
                Street1 = "123 St.",
                Street2 = "Street 2",
                TenantId = tenantId,
				Direction = string.Empty,
				GeneralInfo = string.Empty,
				SpecialInstructions = string.Empty,
				AppointmentDateTime = DateUtility.SystemEarliestDateTime
            });

            shipment.OriginTerminal = new LTLTerminalInfo
            {
                Shipment = shipment,
                TenantId = tenantId,
                Code = DateTime.Now.ToShortDateString(),
                Name = DateTime.Now.ToShortDateString(),
                Phone = DateTime.Now.ToShortDateString(),
                TollFree = DateTime.Now.ToShortDateString(),
                Fax = DateTime.Now.ToShortDateString(),
                Email = "test@test.com",
                ContactName = DateTime.Now.ToShortDateString(),
                ContactTitle = "president",
                Street1 = DateTime.Now.ToShortDateString(),
                Street2 = DateTime.Now.ToShortDateString(),
                City = DateTime.Now.ToShortDateString(),
                State = DateTime.Now.ToShortDateString(),
                Country = country,
                PostalCode = "123456789"
            };

            shipment.CustomerReferences.Add(new ShipmentReference
            {
                Shipment = shipment,
                Name = "Ref name",
                Required = true,
                TenantId = tenantId,
                Value = "Ref Val"
            });
            shipment.Equipments.Add(new ShipmentEquipment
            {
                Shipment = shipment,
                EquipmentType = equipment,
                TenantId = tenantId
            });

            shipment.Items.Add(new ShipmentItem
            {
                Shipment = shipment,
                Comment = "Item Comm.",
                Delivery = 2,
                Description = "Item Desc.",
                ActualFreightClass = 92.5,
                ActualHeight = 96,
                ActualLength = 96,
                ActualWidth = 96,
                ActualWeight = 1000,
                PackageType = packageType,
                Pickup = 0,
                PieceCount = 1,
                Quantity = 1,
                TenantId = tenantId,
				HazardousMaterial = true,
            });

            shipment.Services.Add(new ShipmentService
            {
                Shipment = shipment,
                ServiceId = service.Id,
                TenantId = tenantId
            });

            shipment.Vendors.Add(new ShipmentVendor
            {
                Shipment = shipment,
                Vendor = vendor,
                TenantId = tenantId,
                Primary = true,
                FailureCodeId = failureCode.Id,
                FailureComments = "test",
                ProNumber = "text"
            });

            shipment.Charges.Add(new ShipmentCharge
            {
                ChargeCode = chargeCode,
                Comment = string.Empty,
                Quantity = 1,
                Shipment = shipment,
                TenantId = tenantId,
                UnitBuy = 5.0000m,
                UnitSell = 10.0000m,
                UnitDiscount = 2.0000m
            });

            shipment.Notes.Add(new ShipmentNote
            {
                Archived = false,
                Classified = false,
                Message = "Note Test Message",
                Shipment = shipment,
                TenantId = tenantId,
                Type = NoteType.Normal,
                User = user
            });

            shipment.QuickPayOptions.Add(new ShipmentQuickPayOption
            {
                Description = "Test Message",
                DiscountPercent = 1m,
                Shipment = shipment,
                TenantId = tenantId
            });

            shipment.AccountBuckets.Add(new ShipmentAccountBucket
            {
                AccountBucketId = accountBucket.Id,
                Primary = true,
                Shipment = shipment,
                TenantId = tenantId,
            });

            var assetTractor = assets.FirstOrDefault(c => c.AssetType == AssetType.IndependentTractor || c.AssetType == AssetType.OwnerOpTractor || c.AssetType == AssetType.CompanyTractor);
            var assetTrailer = assets.FirstOrDefault(c => c.AssetType == AssetType.Trailer);
        	shipment.Assets.Add(new ShipmentAsset
        	                    	{
        	                    		Shipment = shipment,
        	                    		DriverAssetId = assets.First(c => c.AssetType == AssetType.Driver).Id,
        	                    		TractorAssetId = assetTractor == null ? default(long) : assetTractor.Id,
        	                    		TrailerAssetId = assetTrailer == null ? default(long) : assetTrailer.Id,
        	                    		MilesRun = 1m,
        	                    		TenantId = tenantId,
        	                    		Primary = true
        	                    	});

            shipment.Documents.Add(new ShipmentDocument
            {
                Shipment = shipment,
                Description = "Test Description",
                TenantId = tenantId,
                DocumentTag = documentTag,
                LocationPath = "Test Location Path",
                Name = "Test Name"
            });
            shipment.AutoRatingAccessorials.Add(new ShipmentAutoRatingAccessorial
            {
                BuyCeiling = 500,
                BuyFloor = 100,
                SellCeiling = 1000,
                SellFloor = 200,
                MarkupPercent = 10,
                MarkupValue = 50,
                Rate = 50,
                RateType = RateType.Flat,
                Shipment = shipment,
                TenantId = tenantId,
                ServiceDescription = "My Description",
                UseLowerSell = false,
            }
                );

            shipment.CheckCalls.Add(new CheckCall
                                        {
                                            TenantId = GlobalTestInitializer.DefaultTenantId,
                                            Shipment = shipment,
                                            User = new User(GlobalTestInitializer.DefaultUserId),
                                            CallDate = DateTime.Now,
											EventDate = DateTime.Now,
                                            CallNotes = DateTime.Now.ToShortDateString(),
											EdiStatusCode = string.Empty,
                                        }
                );


            return shipment;
        }

        internal class ShipmentView : IShipmentView
        {
            public User ActiveUser
            {
                get { return GlobalTestInitializer.ActiveUser; }
            }

            public bool RefundOrVoidMiscReceipts { get; private set; }

            public bool SaveOriginToAddressBook
        	{
				get { return false; }
        	}

        	public bool SaveDestinationToAddressBook
        	{
				get { return false; }
        	}

        	public List<Country> Countries
            {
                set { }
            }
            public List<ContactType> ContactTypes
            {
                set { }
            }
            public List<PackageType> PackageTypes
            {
                set { }
            }
            public List<ServiceViewSearchDto> Services
            {
                set { }
            }
            public List<EquipmentType> EquipmentTypes
            {
                set { }
            }
            public List<ChargeCode> ChargeCodes
            {
                set { }
            }
            public List<MileageSource> MileageSources
            {
                set { }
            }
            public List<FailureCode> FailureCodes { set { } }
            public List<DocumentTag> DocumentTags { set { } }
        	public List<ShipmentPriority> ShipmentPriorities{set { }}
            public Dictionary<int, string> DateUnits { set; private get; }
            public Dictionary<int, string> InDisputeReasons { set; private get; }

            public List<Asset> Assets
            {
                set { }
            }

            public Dictionary<int, string> NoteTypes
            {
                set { }
            }
            public Dictionary<int, string> ServiceModes
            {
                set { }
            }
        	public Dictionary<int, string> MileageEngines
        	{
        		set {  }
        	}

        	public event EventHandler Loading;
            public event EventHandler<ViewEventArgs<Shipment>> Save;
            public event EventHandler<ViewEventArgs<Shipment>> Delete;
            public event EventHandler<ViewEventArgs<Shipment>> Lock;
            public event EventHandler<ViewEventArgs<Shipment>> UnLock;
            public event EventHandler<ViewEventArgs<Shipment>> LoadAuditLog;
            public event EventHandler<ViewEventArgs<string>> CustomerSearch;
            public event EventHandler<ViewEventArgs<string>> VendorSearch;
            public event EventHandler<ViewEventArgs<ShoppedRate>> SaveShoppedRate;
	        public event EventHandler<ViewEventArgs<SMC3TrackRequestResponse>> SaveSmc3TrackRequestResponse;
            public event EventHandler<ViewEventArgs<Project44TrackingResponse>> SaveProject44TrackRequestResponse;

            public void DisplayVendor(Vendor vendor)
            {
            }

	        public void DisplayCustomer(Customer customer, bool updateSalesRepAndResellers)
	        {
	        }

	        public void LogException(Exception ex)
			{
			}

            public void DisplayMessages(IEnumerable<ValidationMessage> messages)
            {
                messages.PrintMessages();
                Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
            }

            public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
            {
            }

            public void FailedLock(Lock @lock)
            {
            }

            public void Set(Shipment shipment)
            {
            }

            public void Set(ShoppedRate shoppedRate)
            {
            }

            public void Load()
            {
                var handler = new ShipmentHandler(this);
                handler.Initialize();
            }

            public void SaveRecord(Shipment shipment)
            {
                if (Save != null)
                    Save(this, new ViewEventArgs<Shipment>(shipment));
            }

            public void DeleteRecord(Shipment shipment)
            {
                if (Delete != null)
                    Delete(this, new ViewEventArgs<Shipment>(shipment));
            }

            public void LockRecord(Shipment shipment)
            {
                if (Lock != null)
                    Lock(this, new ViewEventArgs<Shipment>(shipment));
            }

            public void UnLockRecord(Shipment shipment)
            {
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<Shipment>(shipment));
            }
        }
    }
}
