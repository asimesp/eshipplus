﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
	[TestFixture]
	public class ShipmentSearchTests
	{
		private ShipmentViewSearchCriteria _criteria;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();

			_criteria = new ShipmentViewSearchCriteria {ActiveUserId = GlobalTestInitializer.DefaultUserId,};
		}

		[Test]
		public void CanFetchShipmentByShipmentNumber()
		{
			var search = new ShipmentSearch();
			var shipments = search.FetchShipmentsDashboardDto(_criteria, GlobalTestInitializer.DefaultTenantId);
			var number = shipments[0].ShipmentNumber;

			search.FetchShipmentByShipmentNumber(number, GlobalTestInitializer.DefaultTenantId);
		}

		[Test]
		public void CanFetchShipmentByPrimaryVendorProAndScac()
		{
			var search = new ShipmentSearch();
			var shipments = search.FetchShipmentsDashboardDto(_criteria, GlobalTestInitializer.DefaultTenantId);
			var shipment = shipments.FirstOrDefault();
			if (shipment == null) return;
			var s = new Shipment(shipment.Id);
			var sv = s.Vendors.FirstOrDefault(v => v.Primary);
			if (sv == null) return;

			search.FetchShipmentByPrimaryVendorProAndScac(sv.ProNumber, sv.Vendor.Scac, GlobalTestInitializer.DefaultTenantId);
		}

		[Test]
		public void CanTestExistingShipment()
		{
			var search = new ShipmentSearch();
			var shipments = search.FetchShipmentsDashboardDto(_criteria, GlobalTestInitializer.DefaultTenantId);
			var number = shipments[0].ShipmentNumber;

			Assert.IsTrue(search.ShipmentExists(number, GlobalTestInitializer.DefaultTenantId));
		}

		[Test]
		public void CanFetchShipmentsReadyToInvoice()
		{
			new ShipmentSearch().FetchShipmentsToBeInvoiced(_criteria, GlobalTestInitializer.DefaultTenantId);
		}

		[Test]
		public void CanFetchShipmentsReadyToInvoiceWithShipmentReference()
		{
			var field = OperationsSearchFields.ShipmentReferenceName;
			if (field != null) _criteria.Parameters.Add(new ParameterColumn { DefaultValue = string.Empty, ReportColumnName = field.DisplayName, Operator = Operator.Contains });
			field = OperationsSearchFields.ShipmentReferenceValue;
			if (field != null) _criteria.Parameters.Add(new ParameterColumn { DefaultValue = string.Empty, ReportColumnName = field.DisplayName, Operator = Operator.Contains });

			var results = new ShipmentSearch().FetchShipmentsToBeInvoiced(_criteria, GlobalTestInitializer.DefaultTenantId);
			Console.WriteLine(results.Count);
			Assert.IsTrue(results.Any());
		}

		[Test]
		public void CanFetchShipmentNotBolDelivered()
		{
			new ShipmentSearch().FetchShipmentNotBolDelivered(new DateTime(2012, 01, 01), GlobalTestInitializer.DefaultCustomerId, GlobalTestInitializer.DefaultTenantId, false);
			new ShipmentSearch().FetchShipmentNotBolDelivered(new DateTime(2012, 01, 01), GlobalTestInitializer.DefaultCustomerId, GlobalTestInitializer.DefaultTenantId, true);
		}

		[Test]
		public void CanFetchShipmentDocumentsNotDelivered()
		{
			var tagIds =
                ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].DocumentTags
					.Select(t => t.Id)
					.Take(3)
					.ToList();

			new ShipmentSearch().FetchShipmentDocumentsNotDelivered(new DateTime(2012, 01, 01), GlobalTestInitializer.DefaultCustomerId, GlobalTestInitializer.DefaultTenantId, false, false, tagIds);
			new ShipmentSearch().FetchShipmentDocumentsNotDelivered(new DateTime(2012, 01, 01), GlobalTestInitializer.DefaultCustomerId, GlobalTestInitializer.DefaultTenantId, true, false, tagIds);
			new ShipmentSearch().FetchShipmentDocumentsNotDelivered(new DateTime(2012, 01, 01), GlobalTestInitializer.DefaultCustomerId, GlobalTestInitializer.DefaultTenantId, true, true, new List<long>());
		}

		[Test]
		public void CanFetchShipmentNotShipmentStatementDelivered()
		{
			new ShipmentSearch().FetchShipmentNotShipmentStatementDelivered(new DateTime(2012, 01, 01), GlobalTestInitializer.DefaultCustomerId, GlobalTestInitializer.DefaultTenantId);
		}

		[Test]
		public void CanFetchShipmentsDashboardDto()
		{
			var field = OperationsSearchFields.Shipments.FirstOrDefault(f => f.Name == "OriginCountryName");
			if (field != null) _criteria.Parameters.Add(new ParameterColumn {DefaultValue = "U", ReportColumnName = field.DisplayName, Operator = Operator.Contains});
			field = OperationsSearchFields.Shipments.FirstOrDefault(f => f.Name == "DestinationCountryName");
			if (field != null) _criteria.Parameters.Add(new ParameterColumn { DefaultValue = "Ca", ReportColumnName = field.DisplayName, Operator = Operator.Contains });

			new ShipmentSearch().FetchShipmentsDashboardDto(_criteria, GlobalTestInitializer.DefaultTenantId);
		}

		[Test]
		public void CanFetchShipmentsDashboardDtoWithShipmentReference()
		{
			var field = OperationsSearchFields.ShipmentReferenceName;
			if (field != null) _criteria.Parameters.Add(new ParameterColumn { DefaultValue = string.Empty, ReportColumnName = field.DisplayName, Operator = Operator.Contains });
			field = OperationsSearchFields.ShipmentReferenceValue;
			if (field != null) _criteria.Parameters.Add(new ParameterColumn { DefaultValue = string.Empty, ReportColumnName = field.DisplayName, Operator = Operator.Contains });

			var results = new ShipmentSearch().FetchShipmentsDashboardDto(_criteria, GlobalTestInitializer.DefaultTenantId);
			Console.WriteLine(results.Count);
			Assert.IsTrue(results.Any());
		}

        [Test]
        public void CanFetchShipmentPickupDashboardDto()
        {
            var criteria = new PendingLtlPickupViewSearchCriteria{ActiveUserId = GlobalTestInitializer.DefaultUserId};
            
            var field = OperationsSearchFields.PendingLtlPickups.FirstOrDefault(f => f.Name == "OriginCountryName");
            if (field != null) criteria.Parameters.Add(new ParameterColumn { DefaultValue = "U", ReportColumnName = field.DisplayName, Operator = Operator.Contains });
            field = OperationsSearchFields.PendingLtlPickups.FirstOrDefault(f => f.Name == "DestinationCountryName");
            
            if (field != null) criteria.Parameters.Add(new ParameterColumn { DefaultValue = "Ca", ReportColumnName = field.DisplayName, Operator = Operator.Contains });
            var results = new ShipmentSearch().FetchShipmentPickupDashboardDto(criteria, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(results.Any());
        }

		[Test]
		public void CanFetchLoadsShipmentsDashboardDto()
		{
			var field = OperationsSearchFields.Shipments.FirstOrDefault(f => f.Name == "OriginCountryName");
			if (field != null) _criteria.Parameters.Add(new ParameterColumn { DefaultValue = "U", ReportColumnName = field.DisplayName, Operator = Operator.Contains });
			field = OperationsSearchFields.Shipments.FirstOrDefault(f => f.Name == "DestinationCountryName");
			if (field != null) _criteria.Parameters.Add(new ParameterColumn { DefaultValue = "Ca", ReportColumnName = field.DisplayName, Operator = Operator.Contains });

			new ShipmentSearch().FetchLoadsShipmentsDashboardDto(_criteria, GlobalTestInitializer.DefaultTenantId, true);
		}

		[Test]
		public void CanFetchLoadsShipmentsDashboardDtoWithShipmentReference()
		{
			var field = OperationsSearchFields.ShipmentReferenceName;
			if (field != null) _criteria.Parameters.Add(new ParameterColumn { DefaultValue = string.Empty, ReportColumnName = field.DisplayName, Operator = Operator.Contains });
			field = OperationsSearchFields.ShipmentReferenceValue;
			if (field != null) _criteria.Parameters.Add(new ParameterColumn { DefaultValue = string.Empty, ReportColumnName = field.DisplayName, Operator = Operator.Contains });

			var results = new ShipmentSearch().FetchLoadsShipmentsDashboardDto(_criteria, GlobalTestInitializer.DefaultTenantId, true);
			Console.WriteLine(results.Count);
			Assert.IsTrue(results.Any());
		}

        [Test]
        public void CanFetchLoadsShipmentsDashboardDtoForDispatch()
        {
            var field = OperationsSearchFields.Shipments.FirstOrDefault(f => f.Name == "OriginCountryName");
            if (field != null) _criteria.Parameters.Add(new ParameterColumn { DefaultValue = "U", ReportColumnName = field.DisplayName, Operator = Operator.Contains });
            field = OperationsSearchFields.Shipments.FirstOrDefault(f => f.Name == "DestinationCountryName");
            if (field != null) _criteria.Parameters.Add(new ParameterColumn { DefaultValue = "Ca", ReportColumnName = field.DisplayName, Operator = Operator.Contains });

            new ShipmentSearch().FetchLoadsShipmentsDashboardDtoForDispatch(_criteria, GlobalTestInitializer.DefaultTenantId, true);
        }

		[Test]
		public void CanFetchLoadsShipmentsDashboardDtoForDispatchWithShipmentReference()
		{
			var field = OperationsSearchFields.ShipmentReferenceName;
			if (field != null) _criteria.Parameters.Add(new ParameterColumn { DefaultValue = string.Empty, ReportColumnName = field.DisplayName, Operator = Operator.Contains });
			field = OperationsSearchFields.ShipmentReferenceValue;
			if (field != null) _criteria.Parameters.Add(new ParameterColumn { DefaultValue = string.Empty, ReportColumnName = field.DisplayName, Operator = Operator.Contains });

			var results = new ShipmentSearch().FetchLoadsShipmentsDashboardDtoForDispatch(_criteria, GlobalTestInitializer.DefaultTenantId, true);
			Console.WriteLine(results.Count);
			Assert.IsTrue(results.Any());
		}

		[Test]
		public void CanFetchShipmentsDashboardDtoWithNoFilter()
		{
			_criteria.Parameters= new List<ParameterColumn>();
			new ShipmentSearch().FetchShipmentsDashboardDto(_criteria, GlobalTestInitializer.DefaultTenantId);
		}

        [Test]
        public void CanFetchFedExSmallPackShipmentsNotDeliveredInvoicedOrVoid()
        {
            var s = new ShipmentSearch().FetchSmallPackShipmentsForTracking(SmallPackageEngine.FedEx);
			Assert.IsTrue(s.Any());
        }

		[Test]
        [Ignore]
		public void CanFetchRetrieveNonExpiredShipmentDocRtrvLogs()
		{
			var s = new ShipmentSearch().RetrieveNonExpiredShipmentDocRtrvLogs();
			Assert.IsTrue(s.Any());
		}

		[Test]
		public void CanRetrieveDispatchableShipmentsForTracking()
		{
			var s = new ShipmentSearch().RetrieveDispatchableShipmentsForTracking(GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(s.Any());
		}

		[Test]
		public void CanCheckShipmentDocRtrvLogExistsForShipment()
		{
			Assert.IsFalse(new ShipmentSearch().ShipmentDocRtrvLogExistsForShipment("1235", GlobalTestInitializer.DefaultTenantId));
			Assert.IsTrue(new ShipmentSearch().ShipmentDocRtrvLogExistsForShipment("56837", GlobalTestInitializer.DefaultTenantId));
		}

        [Test]
        public void CanFetchShipmentPickupDashboardDtoByUnauthoriziedUser()
        {
            var criteria = new PendingLtlPickupViewSearchCriteria { ActiveUserId = GlobalTestInitializer.DefaultUserId };

            var originPostalCode = "";
            var shipmentNumber = "";
            var purchaseOrderNumber = "";
            var vendorProNumber = "";
            if (!string.IsNullOrEmpty(originPostalCode))
            {
                criteria.Parameters.Add(new ParameterColumn()
                {
                    Operator = Operator.Equal,
                    DefaultValue = originPostalCode,
                    ReportColumnName = OperationsSearchFields.OriginPostalCode.DisplayName
                });
            }

            if (!string.IsNullOrEmpty(shipmentNumber))
            {
                criteria.Parameters.Add(new ParameterColumn()
                {
                    Operator = Operator.Equal,
                    ReportColumnName = OperationsSearchFields.ShipmentNumber.DisplayName,
                    DefaultValue = shipmentNumber
                });
            }

            if (!string.IsNullOrEmpty(purchaseOrderNumber))
            {
                criteria.Parameters.Add(new ParameterColumn()
                {
                    Operator = Operator.Equal,
                    ReportColumnName = OperationsSearchFields.PurchaseOrderNumber.DisplayName,
                    DefaultValue = purchaseOrderNumber
                });
            }

            if (!string.IsNullOrEmpty(vendorProNumber))
            {
                criteria.Parameters.Add(new ParameterColumn()
                {
                    Operator = Operator.Equal,
                    ReportColumnName = OperationsSearchFields.VendorProNumber.DisplayName,
                    DefaultValue = vendorProNumber
                });
            }

            var results = new ShipmentSearch().FetchShipmentPickupDashboardDto(criteria, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(results.Any());
        }
    }
}
