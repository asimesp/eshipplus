﻿using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
	[TestFixture]
	public class ClaimSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchClaimDtos()
		{
			var criteria = new ClaimViewSearchCriteria {ActiveUserId = GlobalTestInitializer.DefaultUserId,};
			new ClaimSearch().FetchClaimDtos(criteria, GlobalTestInitializer.DefaultTenantId);
		}

        [Test]
        public void CanFetchClaimByClaimNumber()
        {
            var search = new ClaimSearch();
            var claims = search.FetchClaimDtos(new ClaimViewSearchCriteria {ActiveUserId = GlobalTestInitializer.DefaultUserId}, GlobalTestInitializer.DefaultTenantId);
            var number = claims[0].ClaimNumber;

            search.FetchClaimByClaimNumber(number, GlobalTestInitializer.DefaultTenantId);
        }
	}
}
