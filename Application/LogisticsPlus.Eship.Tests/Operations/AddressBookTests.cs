﻿using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
	[TestFixture]
	public class AddressBookTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanLoadServiceCollections()
		{
			var criteria = new AddressBookViewSearchCriteria { CustomerId = 0, UserId = GlobalTestInitializer.DefaultUserId };
			var all = new AddressBookViewSearchDto().FetchAddressBookDtos(GlobalTestInitializer.DefaultTenantId, criteria);
			var addressBook = new AddressBook(all[0].Id);
			if (addressBook.Services.Count < 1) return;
			addressBook.TenantId = GlobalTestInitializer.DefaultTenantId;
			addressBook.LoadCollections();
			Assert.IsTrue(addressBook.Services.Count > 0);
		}

		[Test]
		public void CanLoadContactCollections()
		{
			var criteria = new AddressBookViewSearchCriteria { CustomerId = 0, UserId = GlobalTestInitializer.DefaultUserId };
			var all = new AddressBookViewSearchDto().FetchAddressBookDtos(GlobalTestInitializer.DefaultTenantId, criteria);
			var addressBook = new AddressBook(all[0].Id);
			if (addressBook.Contacts.Count < 1) return;
			addressBook.TenantId = GlobalTestInitializer.DefaultTenantId;
			addressBook.LoadCollections();
			Assert.IsTrue(addressBook.Contacts.Count > 0);
		}

		[Test]
		public void CanFetchAddressBookDtoSingleCriteriaAndDefaultsSearch()
		{
			var all = new AddressBookViewSearchDto()
				.FetchAddressBookDtoSingleCriteriaAndDefaultsSearch(GlobalTestInitializer.DefaultTenantId,
				                                                    GlobalTestInitializer.DefaultUserId,
				                                                    GlobalTestInitializer.DefaultCustomerId, "a", Operator.Contains);
			Assert.IsTrue(all.Any());
		}

	}
}
