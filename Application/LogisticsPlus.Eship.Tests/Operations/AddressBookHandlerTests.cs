﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
	[TestFixture]
	public class AddressBookHandlerTests
	{
		private AddressBookView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new AddressBookView();
			_view.Load();
		}

		[Test]
		public void CanSaveAndDelete()
		{
			var addressBook = GetAddressBook();
			_view.SaveRecord(addressBook);
			Assert.IsTrue(addressBook.Id != default(long));
			_view.DeleteRecord(addressBook);
			Assert.IsTrue(!new AddressBook(addressBook.Id, false).KeyLoaded);

		}

		[Test]
		public void CanSaveOrUpdate()
		{
			var criteria = new AddressBookViewSearchCriteria {CustomerId = 0, UserId = GlobalTestInitializer.DefaultUserId};
			var all = new AddressBookViewSearchDto().FetchAddressBookDtos(GlobalTestInitializer.DefaultTenantId, criteria);
			if (all.Count == 0) return;
			var addressBook = new AddressBook(all[0].Id);
			addressBook.LoadCollections();
			_view.LockRecord(addressBook);
			addressBook.TakeSnapShot();
			var city = addressBook.City;
			addressBook.City = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff");
			_view.SaveRecord(addressBook);
			addressBook.TakeSnapShot();
			addressBook.City = city;
			_view.SaveRecord(addressBook);
			_view.UnLockRecord(addressBook);
		}


		private AddressBook GetAddressBook()
		{
			var tenantId = GlobalTestInitializer.DefaultTenantId;
			var customer = new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), tenantId).FirstOrDefault();
            var contactType = ProcessorVars.RegistryCache[tenantId].ContactTypes[0];
			var addressBook = new AddressBook
			                  	{
			                  		TenantId = tenantId,
			                  		CountryId = GlobalTestInitializer.DefaultCountryId,
			                  		Customer = customer,
			                  		Description = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff"),
			                  		OriginSpecialInstruction = string.Empty,
			                  		DestinationSpecialInstruction = string.Empty,
									Direction = string.Empty,
									GeneralInfo = string.Empty,
			                  		City = string.Empty,
			                  		State = string.Empty,
			                  		Street1 = string.Empty,
			                  		Street2 = string.Empty,
			                  		PostalCode = string.Empty,
			                  	};
			addressBook.Contacts = new List<AddressBookContact>
			                       	{
			                       		new AddressBookContact
			                       			{
			                       				TenantId = addressBook.TenantId,
			                       				AddressBook = addressBook,
			                       				ContactType = contactType,
			                       				Name = "Test1",
			                       				Primary = true,
			                       			},
			                       		new AddressBookContact
			                       			{
			                       				TenantId = addressBook.TenantId,
			                       				AddressBook = addressBook,
			                       				ContactType = contactType,
			                       				Name = "Test2",
			                       				Primary = false,
			                       			}
			                       	};
			addressBook.Services = new List<AddressBookService>();
			return addressBook;
		}

		internal class AddressBookView : IAddressBookView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public List<Country> Countries
			{
				set { }
			}

			public List<ContactType> ContactTypes
			{
				set { }
			}

			public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<AddressBook>> Save;
			public event EventHandler<ViewEventArgs<AddressBook>> Delete;
			public event EventHandler<ViewEventArgs<AddressBook>> Lock;
			public event EventHandler<ViewEventArgs<AddressBook>> UnLock;
			public event EventHandler<ViewEventArgs<AddressBook>> LoadAuditLog;
			public event EventHandler<ViewEventArgs<string>> CustomerSearch;
			public event EventHandler<ViewEventArgs<List<AddressBook>>> BatchImport;

			public void DisplayCustomer(Customer customer)
			{
				
			}


		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
			{

			}

			public void Load()
			{
				var handler = new AddressBookHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(AddressBook addressBook)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<AddressBook>(addressBook));
			}

			public void DeleteRecord(AddressBook vendor)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<AddressBook>(vendor));
			}

			public void LockRecord(AddressBook vendor)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<AddressBook>(vendor));
			}

			public void UnLockRecord(AddressBook vendor)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<AddressBook>(vendor));
			}

			public void FailedLock(Lock @lock)
			{

			}

			public void Set(AddressBook addressBook)
			{

			}
		}
	}
}
