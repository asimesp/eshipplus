﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
    [TestFixture]
    public class VendorPreferredLanesHandlerTests
    {
        private CarrierPreferredLanesView _view;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _view = new CarrierPreferredLanesView();
            _view.Load();
        }

        [Test]
        public void CanHandleMileageSourceSaveOrUpdate()
        {
            var all = new VendorPreferredLaneSearch().FetchVendorPreferredLanes(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
            if (!all.Any()) return;
            var vendorPreferredLane = new VendorPreferredLane(all[0].Id);
            _view.LockRecord(vendorPreferredLane);
            vendorPreferredLane.TakeSnapShot();
            var originalOriginPostalCode = vendorPreferredLane.OriginPostalCode;
            vendorPreferredLane.OriginPostalCode = "test";
            _view.SaveRecord(vendorPreferredLane);
            vendorPreferredLane.TakeSnapShot();
            vendorPreferredLane.OriginPostalCode = originalOriginPostalCode;
            _view.SaveRecord(vendorPreferredLane);
            _view.UnLockRecord(vendorPreferredLane);
        }

        [Test]
        public void CanHandleMileageSourceSaveAndDelete()
        {
            var allVendors = new VendorSearch().FetchVendors(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId).OrderBy(vt => vt.Id);
            if (!allVendors.Any()) return;

            var vendor = allVendors.First();

            var vendorPreferredLane = new VendorPreferredLane
            {
                TenantId = GlobalTestInitializer.DefaultTenantId,
                Vendor = vendor,
                OriginCity = string.Empty,
                OriginState = string.Empty,
                OriginPostalCode =  string.Empty,
                OriginCountryId = GlobalTestInitializer.DefaultCountryId,
                DestinationCity = string.Empty,
                DestinationState = string.Empty,
                DestinationPostalCode = string.Empty,
                DestinationCountryId = GlobalTestInitializer.DefaultCountryId,
            };
            _view.SaveRecord(vendorPreferredLane);
            Assert.IsTrue(vendorPreferredLane.Id != default(long));
            _view.DeleteRecord(vendorPreferredLane);
            Assert.IsTrue(!new VendorTerminal(vendorPreferredLane.Id, false).KeyLoaded);

        }

        internal class CarrierPreferredLanesView : ICarrierPreferredLaneView
        {
            public User ActiveUser
            {
                get { return GlobalTestInitializer.ActiveUser; }
            }

            public List<Country> Countries
            {
                set { }
            }

            public event EventHandler Loading;
            public event EventHandler<ViewEventArgs<VendorPreferredLane>> Save;
            public event EventHandler<ViewEventArgs<VendorPreferredLane>> Delete;
            public event EventHandler<ViewEventArgs<VendorPreferredLane>> Lock;
            public event EventHandler<ViewEventArgs<VendorPreferredLane>> UnLock;
            public event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
            public event EventHandler<ViewEventArgs<string>> VendorSearch;
            public event EventHandler<ViewEventArgs<List<VendorPreferredLane>>> BatchImport;


            public void LogException(Exception ex) { }

            public void DisplaySearchResult(List<VendorPreferredLaneViewSearchDto> vendorPreferredLanes)
            {
                Assert.IsTrue(vendorPreferredLanes.Count > 0);
            }

            public void DisplayMessages(IEnumerable<ValidationMessage> messages)
            {
                Assert.IsTrue(messages.Any(m => m.Type != ValidateMessageType.Information));
            }

            public void DisplayVendor(Vendor vendor) { }

            public void FailedLock(Lock @lock) { }


            public void Load()
            {
                var handler = new CarrierPreferredLanesHandler(this);
                handler.Initialize();
            }

            public void SaveRecord(VendorPreferredLane vendorPreferredLane)
            {
                if (Save != null)
                    Save(this, new ViewEventArgs<VendorPreferredLane>(vendorPreferredLane));
            }

            public void DeleteRecord(VendorPreferredLane vendorPreferredLane)
            {
                if (Delete != null)
                    Delete(this, new ViewEventArgs<VendorPreferredLane>(vendorPreferredLane));
            }

            public void LockRecord(VendorPreferredLane vendorPreferredLane)
            {
                if (Lock != null)
                    Lock(this, new ViewEventArgs<VendorPreferredLane>(vendorPreferredLane));
            }

            public void UnLockRecord(VendorPreferredLane vendorPreferredLane)
            {
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<VendorPreferredLane>(vendorPreferredLane));
            }
        }
    }
}

