﻿using System.Linq;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
	[TestFixture]
	class TruckloadBidSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchTruckloadBidByLoadOrderId()
		{
			var bid = new TruckloadBidSearch().FetchTruckloadBidByLoadOrderId(GlobalTestInitializer.DefaultLoadOrderId,
			                                                                  GlobalTestInitializer.DefaultTenantId);
			if (bid == null) return;
			Assert.IsTrue(bid!=null);
		}

		[Test]
		public void CanFetchAvailableTruckloadBids()
		{
			var bids = new TruckloadBidSearch().FetchAvailableTruckloadBids(GlobalTestInitializer.DefaultTenantId);
			if (!bids.Any()) return;
			Assert.IsTrue(bids.Any());
		}

		[Test]
		public void CanFetchTruckloadBidByProfileId()
		{
			var bids = new TruckloadBidSearch().FetchAvailableTruckloadBids(GlobalTestInitializer.DefaultTenantId);
			if (!bids.Any()) return;

			var bid = new TruckloadBidSearch().FetchTruckloadBidByProfileId(bids.First().TLTenderingProfileId,
			                                                                GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(bid != null);
		}

		[Test]
		public void CanFetchTruckloadBidByStatus()
		{
			var bids = new TruckloadBidSearch().FetchTruckloadBidByStatus(TruckloadBidStatus.Processing.ToInt(),GlobalTestInitializer.DefaultTenantId);
			if (!bids.Any()) return;
			Assert.IsTrue(bids.Any());
		}

		[Test]
		public void CanFetchTruckloadBidByLoadOrderIdAndStatus()
		{
			var bid = new TruckloadBidSearch().FetchTruckloadBidByLoadOrderIdAndStatus(GlobalTestInitializer.DefaultLoadOrderId,TruckloadBidStatus.Processing.ToInt(),
																			  GlobalTestInitializer.DefaultTenantId);
			if (bid == null) return;
			Assert.IsTrue(bid != null);
		}
	}
}
