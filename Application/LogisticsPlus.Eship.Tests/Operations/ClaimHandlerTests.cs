﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.Processor.Views.Administration;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
	[TestFixture]
	public class ClaimHandlerTests
	{
		private ClaimView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new ClaimView();
			_view.Load();
		}

		[Test]
		public void CanSaveOrUpdate()
		{
			var criteria = new ClaimViewSearchCriteria {ActiveUserId = GlobalTestInitializer.DefaultUserId};
			var all = new ClaimSearch().FetchClaimDtos(criteria, GlobalTestInitializer.DefaultTenantId);

			if (all.Count == 0) return;
			var serviceTicket = new Claim(all[0].Id);
			serviceTicket.LoadCollections();
			_view.LockRecord(serviceTicket);
			serviceTicket.TakeSnapShot();
			_view.SaveRecord(serviceTicket);
			_view.UnLockRecord(serviceTicket);
		}

		[Test]

		public void CanSaveAndDelete()
		{
			var claim = GetClaim();
			_view.SaveRecord(claim);
			Assert.IsTrue(claim.Id != default(long));
			_view.DeleteRecord(claim);
			Assert.IsTrue(!new Claim(claim.Id, false).KeyLoaded);

		}

		private static Claim GetClaim()
		{
			var tenantId = GlobalTestInitializer.DefaultTenantId;
			var customer = new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), tenantId).FirstOrDefault() ?? new Customer();
			var vendor = new VendorSearch().FetchVendors(new List<ParameterColumn>(), tenantId).FirstOrDefault() ?? new Vendor();

            var criteria = new UserSearchCriteria
            {
                Parameters = new List<ParameterColumn>()
            };
            var userDto = new UserSearch().FetchUsers(criteria, tenantId).FirstOrDefault();
            var user = userDto == null ? new User() : new User(userDto.Id);
            var docTag = ProcessorVars.RegistryCache[tenantId].DocumentTags.FirstOrDefault() ?? new DocumentTag();

			var shipmentCriteria = new ShipmentViewSearchCriteria { ActiveUserId = GlobalTestInitializer.DefaultUserId, };

			var shipment = new ShipmentSearch().FetchShipmentsDashboardDto(shipmentCriteria, tenantId).FirstOrDefault() ?? new ShipmentDashboardDto();

			var userCriteria = new ServiceTicketViewSearchCriteria
			{
				ActiveUserId = GlobalTestInitializer.DefaultUserId,
			};

			var dto = new ServiceTicketSearch().FetchServiceTicketDashboardDtos(userCriteria, tenantId).FirstOrDefault();
			var serviceTicket = dto == null ? new ServiceTicket() : new ServiceTicket(dto.Id);


			var claim = new Claim
			{
				ClaimNumber = "00000",
				ClaimDetail = "",
                ClaimDate = DateTime.Now,
				Customer = customer,
				DateCreated = DateTime.Now,
				Status = ClaimStatus.Open,
				User = user,
				TenantId = tenantId,
				Vendors = new List<ClaimVendor>(),
				Shipments = new List<ClaimShipment>(),
				Notes = new List<ClaimNote>(),
				ServiceTickets = new List<ClaimServiceTicket>(),
                ClaimantReferenceNumber = string.Empty,
                AmountClaimed = default(long),
                AmountClaimedType = AmountClaimedType.NotApplicable,
                ClaimType = ClaimType.NotApplicable,
                IsRepairable = false,
                EstimatedRepairCost = default(long),
                DateLpAcctToPayCarrier = DateTime.Now,
                Acknowledged = DateTime.Now,
                CheckAmount = 100.9.ToDecimal(),
                CheckNumber = "122333333"
            };

			claim.Vendors.Add(new ClaimVendor
			{
				Claim = claim,
				Vendor = vendor,
				TenantId = tenantId,
                ProNumber = string.Empty,
                FreightBillNumber = string.Empty
			});

			claim.Notes.Add(new ClaimNote
			{
				Archived = false,
				Classified = false,
				Message = "Note Test Message",
				Claim = claim,
				TenantId = tenantId,
				Type = NoteType.Normal,
				User = user,
				DateCreated = DateTime.Now,
				SendReminderOn = DateUtility.SystemLatestDateTime,
				SendReminderEmails = string.Empty,
			});

			claim.Shipments.Add(new ClaimShipment
			{
				Claim = claim,
				ShipmentId = shipment.Id,
				TenantId = tenantId
			});

			claim.ServiceTickets.Add(new ClaimServiceTicket
			{
				Claim = claim,
				ServiceTicketId = serviceTicket.Id,
				TenantId = tenantId
			});

			claim.Documents.Add(new ClaimDocument
			{
				TenantId = tenantId,
				Claim = claim,
				Name = "Document",
				Description = "Description",
				DocumentTagId = docTag.Id,
				LocationPath = "test"
			});

			return claim;
		}


		internal class ClaimView : IClaimView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public List<DocumentTag> DocumentTags
			{
				set { }
			}

			public Dictionary<int, string> NoteTypes
			{
				set { }
			}

            public Dictionary<int, string> ClaimTypes
            {
                set { }
            }

            public Dictionary<int, string> AmountClaimedTypes
            {
                set { }
            }

			public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<Claim>> Save;
			public event EventHandler<ViewEventArgs<Claim>> Delete;
			public event EventHandler<ViewEventArgs<Claim>> Lock;
			public event EventHandler<ViewEventArgs<Claim>> UnLock;
			public event EventHandler<ViewEventArgs<Claim>> LoadAuditLog;
			public event EventHandler<ViewEventArgs<string>> CustomerSearch;
			public event EventHandler<ViewEventArgs<string>> VendorSearch;

			public void DisplayVendor(Vendor vendor)
			{
			}

			public void DisplayCustomer(Customer customer)
			{
			}

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
			{
			}

			public void FailedLock(Lock @lock)
			{
			}

			public void Set(Claim serviceTicket)
			{
			}

			public void Load()
			{
				var handler = new ClaimHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(Claim claim)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<Claim>(claim));
			}

			public void DeleteRecord(Claim claim)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<Claim>(claim));
			}

			public void LockRecord(Claim claim)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<Claim>(claim));
			}

			public void UnLockRecord(Claim claim)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<Claim>(claim));
			}
		}
	}
}
