﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
    public class LibraryItemHandlerTests
    {
        private  LibraryItemView _view;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _view = new LibraryItemView();
            _view.Load();
        }

        [Test]
        public void CanSaveOrUpdate()
        {
            var criteria = new LibraryItemSearchCriteria
            {
                CustomerId = GlobalTestInitializer.DefaultCustomerId,
                ActiveUserId = GlobalTestInitializer.DefaultUserId
            };

            var field = OperationsSearchFields.LibraryItemsFinder.FirstOrDefault(f => f.Name == "Description");
            if (field == null)
            {
                Console.WriteLine("OperationsSearchFields.LibraryItems no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }

            var column = field.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            criteria.Parameters.Add(column);

            var all = new LibraryItemSearch()
                .FetchLibraryItems(criteria, GlobalTestInitializer.DefaultTenantId);

            if (all.Count == 0) return;

            var libraryItem = all[0];

            _view.LockRecord(libraryItem);
            libraryItem.TakeSnapShot();
            var description = libraryItem.Description;
            libraryItem.Description = DateTime.Now.ToShortDateString();
            _view.SaveRecord(libraryItem);
            libraryItem.TakeSnapShot();
            libraryItem.Description = description;
            _view.SaveRecord(libraryItem);
            _view.UnLockRecord(libraryItem);
        }

        [Test]
        public void CanSaveAndDelete()
        {
            var customer = new Customer(GlobalTestInitializer.DefaultCustomerId);

            var packageTypes = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].PackageTypes;

            if (packageTypes.Count < 1) return;
            

            var libraryItem = new LibraryItem
                        {   
                            TenantId = _view.ActiveUser.TenantId,
                            Customer = customer,
                            Description = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff"), 
                            PieceCount = 1,
                            PackageTypeId = packageTypes[0].Id,
                            Quantity = 1,
                            Weight = 1,
                            Length = 1,
                            Width = 1,
                            Height = 1,
                            FreightClass = 1,
                            NMFCCode = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff"),
                            HTSCode = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff"),
                            Comment = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff")

                        };
            _view.SaveRecord(libraryItem);
            _view.LockRecord(libraryItem);
            Assert.IsTrue(libraryItem.Id != default(long));
            _view.DeleteRecord(libraryItem);
            Assert.IsTrue(!new LibraryItem(libraryItem.Id, false).KeyLoaded);

        }

        internal class LibraryItemView : ILibraryItemView
        {
            public User ActiveUser
            {
                get { return GlobalTestInitializer.ActiveUser; }
            }

            public List<PackageType> PackageTypes
            {
                set {}
            }

            public event EventHandler Loading;
            public event EventHandler<ViewEventArgs<LibraryItem>> Save;
            public event EventHandler<ViewEventArgs<LibraryItem>> Delete;
            public event EventHandler<ViewEventArgs<LibraryItemSearchCriteria>> Search;
            public event EventHandler<ViewEventArgs<LibraryItem>> LoadAuditLog;
            public event EventHandler<ViewEventArgs<List<LibraryItem>>> BatchImport;
            public event EventHandler<ViewEventArgs<string>> CustomerSearch;
            public event EventHandler<ViewEventArgs<LibraryItem>> Lock;
            public event EventHandler<ViewEventArgs<LibraryItem>> UnLock;

            public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
            {
                
            }

            public void DisplayCustomer(Customer customer)
            {
                    
            }

            public void FailedLock(Lock @lock)
            {

            }

            public void DisplaySearchResult(List<LibraryItem> libraryItem)
            {
                Assert.IsTrue(libraryItem.Count > 0);
            }

            public void LogException(Exception ex)
            {
                    
            }

            public void DisplayMessages(IEnumerable<ValidationMessage> messages)
            {
                messages.PrintMessages();
                Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
            }

            public void Load()
            {
                var handler = new LibraryItemHandler(this);
                handler.Initialize();
            }

            public void SaveRecord(LibraryItem libraryItem)
            {
                if (Save != null)
                    Save(this, new ViewEventArgs<LibraryItem>(libraryItem));
            }

            public void DeleteRecord(LibraryItem libraryItem)
            {
                if (Delete != null)
                    Delete(this, new ViewEventArgs<LibraryItem>(libraryItem));
            }

            public void SearchRecord(LibraryItemSearchCriteria criteria)
            {
                if (Search != null)
                    Search(this, new ViewEventArgs<LibraryItemSearchCriteria>(criteria));
            }

            public void LockRecord(LibraryItem libraryItem)
            {
                if (Lock != null)
                    Lock(this, new ViewEventArgs<LibraryItem>(libraryItem));
            }

            public void UnLockRecord(LibraryItem libraryItem)
            {
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<LibraryItem>(libraryItem));
            }
        }
    }
}
