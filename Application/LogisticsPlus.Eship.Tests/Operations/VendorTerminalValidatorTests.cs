﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
    [TestFixture]
    public class VendorTerminalValidatorTests
    {
        private VendorTerminalValidator _validator;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _validator = new VendorTerminalValidator();
        }

        [Test]
        public void IsValidTest()
        {
            var allVendors = new VendorSearch().FetchVendors(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
            if(!allVendors.Any())return;

            var vendor = allVendors.FirstOrDefault();
            var terminal = new VendorTerminal
                               {
                                   Name = DateTime.Now.ToShortDateString(),
                                   DateCreated = DateTime.Now,
                                   TenantId = GlobalTestInitializer.DefaultTenantId,
                                   Vendor = vendor,
                                   CountryId = GlobalTestInitializer.DefaultCountryId
                               };
            Assert.IsTrue(_validator.IsValid(terminal));
        }
    }
}
