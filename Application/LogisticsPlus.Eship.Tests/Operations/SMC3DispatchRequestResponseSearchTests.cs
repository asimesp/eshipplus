﻿using LogisticsPlus.Eship.Processor.Searches.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
	[TestFixture]
	public class SMC3DispatchRequestResponseSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		[Ignore("Specify valid transaction id")]
		public void CanFetchDispatchRequestResponseByTransactionId()
		{
			var search = new SMC3DispatchRequestResponseSearch();
			const string transacitonId = "TEST";
			search.FetchDispatchRequestResponseByTransactionId(transacitonId);
		}

		[Test]
		[Ignore("Specify Shipment Id")]
		public void FetchDispatchRequestResponseByShipmentId()
		{
			var search = new SMC3DispatchRequestResponseSearch();
			const long shipmentId = default(long);
			search.FetchTrackRequestResponseByShipmentId(shipmentId);
		}
	}
}
