﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
	[TestFixture]
	public class JobValidatorTests
	{
		private JobValidator _validator;
		private JobViewSearchCriteria _criteria;
		private Job _job;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new JobValidator();
			_criteria = new JobViewSearchCriteria { ActiveUserId = GlobalTestInitializer.DefaultUserId };
			
			var tenantId = GlobalTestInitializer.DefaultTenantId;
			var user = new User(GlobalTestInitializer.DefaultUserId);
			var customer = new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), tenantId).FirstOrDefault() ?? new Customer();

			_job = new Job(100)
				{
					JobNumber = "00000",
					DateCreated = DateTime.Now,
					CreatedByUser = user,
					Customer = customer,
					Status = JobStatus.InProgress,
					ExternalReference1 = "Test Reference 1",
					ExternalReference2 = "Test Reference 2",
					TenantId = tenantId
				};
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var hasAllRequiredData = _validator.HasAllRequiredData(_job);
			if (!hasAllRequiredData) _validator.Messages.PrintMessages();
			Assert.IsTrue(hasAllRequiredData);
		}

		[Test]
		public void DuplicateJobNumberTest()
		{
			var all = new JobSearch().FetchJobDashboardDtos(_criteria, GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var job = new Job { JobNumber = all[0].JobNumber, TenantId = GlobalTestInitializer.DefaultTenantId };
			Assert.IsTrue(_validator.DuplicateJobNumber(job));
		}

		[Test]
		public void IsValidTest()
		{
			Assert.IsTrue(_validator.IsValid(_job));
		}

		[Test]
		public void CanDeleteTest()
		{
			_validator.Messages.Clear();
			_validator.CanDeleteJob(_job);
			_validator.Messages.PrintMessages();
		}
	}
}
