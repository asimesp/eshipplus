﻿using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
	[TestFixture]
	public class ServiceTicketSearchTests
	{
		private ServiceTicketViewSearchCriteria _criteria;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();

			_criteria = new ServiceTicketViewSearchCriteria
			            	{
			            		ActiveUserId = GlobalTestInitializer.DefaultUserId,
			            	};
		}

		[Test]
		public void CanFetchServiceTicketbyServiceTicketNumber()
		{
			var search = new ServiceTicketSearch();
			var serviceTickets = search.FetchServiceTicketDashboardDtos(_criteria, GlobalTestInitializer.DefaultTenantId);
			var number = serviceTickets[0].ServiceTicketNumber;

			search.FetchServiceTicketByServiceTicketNumber(number, GlobalTestInitializer.DefaultTenantId);
		}

		[Test]
		public void CanTestExistingServiceTicket()
		{
			var search = new ServiceTicketSearch();
			var serviceTickets = search.FetchServiceTicketDashboardDtos(_criteria, GlobalTestInitializer.DefaultTenantId);
			var number = serviceTickets[0].ServiceTicketNumber;

			Assert.IsTrue(search.ServiceTicketExists(number, GlobalTestInitializer.DefaultTenantId));
		}

		[Test]
		public void CanFetchServiceTicketDashboardDtos()
		{
			var field = OperationsSearchFields.ServiceTickets.FirstOrDefault(f => f.Name == "ServiceTicketNumber");
			if (field != null) _criteria.Parameters.Add(new ParameterColumn { DefaultValue = "1", ReportColumnName = field.DisplayName, Operator = Operator.Contains });

			var tickets = new ServiceTicketSearch().FetchServiceTicketDashboardDtos(_criteria, GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(tickets.Any());
		}

		[Test]
		public void CanFetchServiceTicketByExternalReference1()
		{
			var tickets = new ServiceTicketSearch().FetchServiceTicketByExternalReference1(GlobalTestInitializer.ExternalReference1, GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(tickets.Any());
		}

		[Test]
		public void CanFetchServiceTicketByExternalReference2()
		{
			var tickets = new ServiceTicketSearch().FetchServiceTicketByExternalReference2(GlobalTestInitializer.ExternalReference2, GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(tickets.Any());
		}

		[Test]
		public void CanFetchServiceTicketsReadyToBeInvoiced()
		{
			new ServiceTicketSearch().FetchServiceTicketReadyForInvoicing(new ServiceTicketViewSearchCriteria(),
			                                                              GlobalTestInitializer.DefaultTenantId);
		}
	}
}