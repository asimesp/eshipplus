﻿using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
    [TestFixture]
    class ShoppedRateViewSearchDtoTests
    {
        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

        [Test]
        public void CanFetchShoppedRateDtos()
        {
            var criteria = new ShoppedRateSearchCriteria {ActiveUserId = GlobalTestInitializer.ActiveUser.Id};

            var field = OperationsSearchFields.ShoppedRates.FirstOrDefault(f => f.Name == "OriginCountryName");
            if (field != null) criteria.Parameters.Add(new ParameterColumn { DefaultValue = "U", ReportColumnName = field.DisplayName, Operator = Operator.Contains });
            field = OperationsSearchFields.Shipments.FirstOrDefault(f => f.Name == "DestinationCountryName");
            if (field != null) criteria.Parameters.Add(new ParameterColumn { DefaultValue = "Ca", ReportColumnName = field.DisplayName, Operator = Operator.Contains });

            new ShoppedRateViewSearchDto().FetchShoppedRateDtos(criteria, GlobalTestInitializer.DefaultTenantId);
            
        }
    }
}
