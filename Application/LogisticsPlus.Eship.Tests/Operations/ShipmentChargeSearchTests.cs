﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
	[TestFixture]
	public class ShipmentChargeSearchTests
	{
		

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			
		}

		[Test]
		public void CanFetchUnbilledChargesByShipmentId()
		{
			var search = new ShipmentChargeSearch();
			var shipmentCharges = search.HasUnBilledCharges(GlobalTestInitializer.DefaultShipmentChargeShipmentId);

			Assert.IsTrue(shipmentCharges);
			
		}
	}
}
