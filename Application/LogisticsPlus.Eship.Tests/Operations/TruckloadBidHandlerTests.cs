﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;


namespace LogisticsPlus.Eship.Tests.Operations
{
	[TestFixture]
	class TruckloadBidHandlerTests
	{
		private TruckloadBidView _view;
		
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new TruckloadBidView();
			_view.Load();
		}

		[Test]
		public void CanSaveAndDelete()
		{
			var truckLoadBid = GetTruckloadBid();
			var loadOrder = new LoadOrder(GlobalTestInitializer.DefaultLoadOrderId)
				{
					ServiceMode = ServiceMode.Truckload,
					Status = LoadOrderStatus.Offered
				};

			// Modifying existing defaulttest load to satisfy truckload bid criteria.
			truckLoadBid.LoadOrder = loadOrder;

			_view.SaveRecord(truckLoadBid, loadOrder);
			Assert.IsTrue(truckLoadBid.Id != default(long));

			_view.DeleteRecord(truckLoadBid);
			Assert.IsTrue(!new TruckloadBid(truckLoadBid.Id, false).KeyLoaded);
		}

		[Test]
		public void CanSaveOrUpdate()
		{
			var tenantId = GlobalTestInitializer.DefaultTenantId;
			var tlProfileLane = new CustomerTLTenderingProfileLane(GlobalTestInitializer.DefaulCustomerTLTenderingProfileLaneId);
			var loadOrder = new LoadOrder(GlobalTestInitializer.DefaultLoadOrderId);
			loadOrder.ServiceMode = ServiceMode.Truckload;
			loadOrder.Status = LoadOrderStatus.Offered;

			var bid = new TruckloadBidSearch().FetchTruckloadBidByLoadOrderId(GlobalTestInitializer.DefaultLoadOrderId, tenantId);

			if (bid == null) return;
			var truckLoadBid = bid;
			truckLoadBid.LoadOrder = loadOrder;
			_view.LockRecord(truckLoadBid);
			truckLoadBid.TakeSnapShot();
			truckLoadBid.SecondPreferredVendorId = tlProfileLane.SecondPreferredVendorId;
			truckLoadBid.TimeLoadTendered2 = DateTime.Now;
			truckLoadBid.ExpirationTime2 = DateTime.Now;
			_view.SaveRecord(truckLoadBid, loadOrder);
			_view.UnLockRecord(truckLoadBid, loadOrder);
		}

	

		private TruckloadBid GetTruckloadBid()
		{
			var tenantId = GlobalTestInitializer.DefaultTenantId;
			var tlProfileLane = new CustomerTLTenderingProfileLane(GlobalTestInitializer.DefaulCustomerTLTenderingProfileLaneId);
			var bid = new TruckloadBid
				{
					LoadOrderId = GlobalTestInitializer.DefaultLoadOrderId,
					TLLaneId = GlobalTestInitializer.DefaulCustomerTLTenderingProfileLaneId,
					TLTenderingProfileId = GlobalTestInitializer.DefaulCustomerTLTenderingProfileId,
					FirstPreferredVendorId = tlProfileLane.FirstPreferredVendorId,
					SecondPreferredVendorId = tlProfileLane.SecondPreferredVendorId,
					ThirdPreferredVendorId = tlProfileLane.ThirdPreferredVendorId,
					DateCreated = DateTime.Now,
					TimeLoadTendered1 = DateTime.Now,
					TimeLoadTendered2 = DateUtility.SystemEarliestDateTime,
					TimeLoadTendered3 = DateUtility.SystemEarliestDateTime,
					ExpirationTime1 = DateTime.Now,
					ExpirationTime2 = DateUtility.SystemEarliestDateTime,
					ExpirationTime3 = DateUtility.SystemEarliestDateTime,
					TenantId = tenantId,
					UserId = GlobalTestInitializer.DefaultUserId
				};

			return bid;
		}
		

		internal class TruckloadBidView : ITruckloadBidView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}



			public event EventHandler<ViewEventArgs<TruckloadBid, LoadOrder>> TruckloadBidSave;
			public event EventHandler<ViewEventArgs<TruckloadBid>> TruckloadBidDelete;
			public event EventHandler<ViewEventArgs<TruckloadBid>> TruckloadBidLock;
			public event EventHandler<ViewEventArgs<TruckloadBid>> TruckloadBidUnLock;
			public event EventHandler<ViewEventArgs<TruckloadBid>> TruckloadBidLoadAuditLog;
			public List<ValidationMessage> TruckloadBidMsgs { get; set; }
			public void Load()
			{
				var handler = new TruckloadBidHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(TruckloadBid bid, LoadOrder loadOrder)
			{
				if (TruckloadBidSave != null)
					TruckloadBidSave(this, new ViewEventArgs<TruckloadBid, LoadOrder>(bid, loadOrder));
			}

			public void DeleteRecord(TruckloadBid tlBid)
			{
				if (TruckloadBidDelete != null)
					TruckloadBidDelete(this, new ViewEventArgs<TruckloadBid>(tlBid));
			}

			public void LockRecord(TruckloadBid tlBid)
			{
				if (TruckloadBidLock != null)
					TruckloadBidLock(this, new ViewEventArgs<TruckloadBid>(tlBid));
			}

			public void UnLockRecord(TruckloadBid tlBid, LoadOrder loadOrder)
			{
				if (TruckloadBidUnLock != null)
					TruckloadBidUnLock(this, new ViewEventArgs<TruckloadBid>(tlBid));
			}

			public void LoadAuditLogs(TruckloadBid tlBid)
			{
				if (TruckloadBidLoadAuditLog != null)
					TruckloadBidLoadAuditLog(this, new ViewEventArgs<TruckloadBid>(tlBid));
			}
			public void LogException(Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}

			public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				var loadOrderMessages = messages.Select(validationMessage => new ValidationMessage
				{
					Type = validationMessage.Type,
					Message = string.Format("Truckload Bid {0}", validationMessage.Message)
				}).ToList();


				loadOrderMessages.PrintMessages();
				
			}

			public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs)
			{
				
			}

			public void FailedLock(Lock @lock)
			{
				
			}

			
		}
	}
}
