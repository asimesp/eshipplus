﻿using LogisticsPlus.Eship.Processor.Searches.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{

	[TestFixture]
	public class ServiceTicketChargeSearchTests
	{


		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();

		}

		[Test]
		public void CanFetchUnbilledChargesByServiceTicketId()
		{
			var search = new ServiceTicketChargeSearch();
			var shipmentCharges = search.HasUnBilledCharges(GlobalTestInitializer.DefaultServiceTicketChargeSearchServiceTicketId);

			Assert.IsTrue(shipmentCharges);

		}
	}
}
