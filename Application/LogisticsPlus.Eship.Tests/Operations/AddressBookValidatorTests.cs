﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
    [TestFixture]
    public class AddressBookValidatorTests
    {
        private AddressBookValidator _validator;
    	private AddressBook _addressBook;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _validator = new AddressBookValidator();
        	var tenantId = GlobalTestInitializer.DefaultTenantId;
        	var customer = new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), tenantId).FirstOrDefault();
            var contactType = ProcessorVars.RegistryCache[tenantId].ContactTypes[0];
        	_addressBook = new AddressBook
        	               	{
        	               		TenantId = tenantId,
        	               		CountryId = GlobalTestInitializer.DefaultCountryId,
								Customer = customer,
                                Description = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff")
        	               	};
        	_addressBook.Contacts = new List<AddressBookContact>
        	                        	{
        	                        		new AddressBookContact
        	                        			{
        	                        				TenantId = _addressBook.TenantId,
        	                        				AddressBook = _addressBook,
        	                        				ContactType = contactType,
        	                        				Name = "Test1",
        	                        				Primary = true,
        	                        			},
        	                        		new AddressBookContact
        	                        			{
        	                        				TenantId = _addressBook.TenantId,
        	                        				AddressBook = _addressBook,
        	                        				ContactType = contactType,
        	                        				Name = "Test2",
        	                        				Primary = false,
        	                        			}
        	                        	};
        }

        [Test]
        public void HasAllRequiredDataTest()
        {
			var hasAllRequiredData = _validator.HasAllRequiredData(_addressBook);
			if (!hasAllRequiredData) _validator.Messages.PrintMessages();
			Assert.IsTrue(hasAllRequiredData);
        }

        [Test]
        public void AddressBookServiceExistsTest()
        {
			var criteria = new AddressBookViewSearchCriteria { CustomerId = 0, UserId = GlobalTestInitializer.DefaultUserId };
        	var all = new AddressBookViewSearchDto()
        		.FetchAddressBookDtos(GlobalTestInitializer.DefaultTenantId, criteria)
        		.Select(i => new AddressBook(i.Id))
        		.ToList();
            if (all.Count == 0) return;
            var addressBook = all.FirstOrDefault(g => g.Services.Count > 0);
            if (addressBook == null) return;
            var service = addressBook.Services[0];

            var newService = new AddressBookService
                                {
                                    TenantId = service.TenantId,
                                    ServiceId = service.ServiceId,
                                    AddressBookId = service.AddressBookId
                                };

            Assert.IsTrue(_validator.AddressBookServiceExists(newService));
        }
    }
}
