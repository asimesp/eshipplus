﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
    [TestFixture]
    public class LibraryItemSearchTests
    {
        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

        [Test]
        public void CanFetchLibraryItemsWithParameterColumns()
        {

            var criteria = new LibraryItemSearchCriteria
            {
                CustomerId = GlobalTestInitializer.DefaultCustomerId,
                ActiveUserId = GlobalTestInitializer.DefaultUserId
            };

            var field = OperationsSearchFields.LibraryItemsFinder.FirstOrDefault(f => f.Name == "Description");
            if (field == null)
            {
                Console.WriteLine("OperationsSearchFields.LibraryItems no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }

            var column = field.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            criteria.Parameters.Add(column);

            var results = new LibraryItemSearch()
                .FetchLibraryItems(criteria, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(results.Any());


        }

        [Test]
        public void CanFetchLibraryItemDtosWithParameterColumns()
        {
            var criteria = new LibraryItemSearchCriteria
            {
                CustomerId = GlobalTestInitializer.DefaultCustomerId,
                ActiveUserId = GlobalTestInitializer.DefaultUserId
            };

            var field = OperationsSearchFields.LibraryItemsFinder.FirstOrDefault(f => f.Name == "Description");
            if (field == null)
            {
                Console.WriteLine("OperationsSearchFields.LibraryItems no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }

            var column = field.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            criteria.Parameters.Add(column);

            var results = new LibraryItemSearch()
                .FetchLibraryItemDtos(criteria, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(results.Any());
        }
    }
}
