﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
    [TestFixture]
    public class VendorTerminalHandlerTest
    {
        private VendorTerminalView _view;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _view = new VendorTerminalView();
            _view.Load();
        }

        [Test]
        public void CanHandleMileageSourceSaveOrUpdate()
        {
            var allVendors = new VendorSearch().FetchVendors(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
            var vendor = allVendors.FirstOrDefault();
            if (vendor == null) return;
           
            var criteria = new VendorTerminalSearchCriteria
            {
                VendorId = vendor.Id
            };

            var field = OperationsSearchFields.VendorTerminals.FirstOrDefault(f => f.Name == "Name");
            if (field == null)
            {
                Console.WriteLine("OperationsSearchFields.VendorTerminals no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }

            var column = field.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            criteria.Parameters.Add(column);
           
            var all = new VendorTerminalSearch().FetchVendorTerminals(criteria, GlobalTestInitializer.DefaultTenantId);
            if (!all.Any()) return;

            var vendorTerminal = all[0];
            _view.LockRecord(vendorTerminal);
            vendorTerminal.TakeSnapShot();
            var name = vendorTerminal.Name;
            vendorTerminal.Name = DateTime.Now.ToShortDateString();
            _view.SaveRecord(vendorTerminal);
            vendorTerminal.TakeSnapShot();
            vendorTerminal.Name = name;
            _view.SaveRecord(vendorTerminal);
            _view.UnLockRecord(vendorTerminal);
        }

        [Test]
        public void CanHandleMileageSourceSaveAndDelete()
        {
            var allVendors = new VendorSearch().FetchVendors(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId).OrderBy(vt => vt.Id);
            if (!allVendors.Any()) return;

            var vendor = allVendors.First();

            var vendorTerminal = new VendorTerminal
            {
                Name = DateTime.Now.ToShortDateString(),
                DateCreated = DateTime.Now,
                TenantId = GlobalTestInitializer.DefaultTenantId,
                Vendor = vendor,
                CountryId = GlobalTestInitializer.DefaultCountryId
            };
            _view.SaveRecord(vendorTerminal);
            Assert.IsTrue(vendorTerminal.Id != default(long));
            _view.DeleteRecord(vendorTerminal);
            Assert.IsTrue(!new VendorTerminal(vendorTerminal.Id, false).KeyLoaded);

        }

        internal class VendorTerminalView : ICarrierTerminalView
        {
            public User ActiveUser
            {
                get { return GlobalTestInitializer.ActiveUser; }
            }

            public List<Country> Countries
            {
                set { }
            }

            public event EventHandler Loading;
            public event EventHandler<ViewEventArgs<VendorTerminal>> Save;
            public event EventHandler<ViewEventArgs<VendorTerminal>> Delete;
            public event EventHandler<ViewEventArgs<VendorTerminal>> Lock;
            public event EventHandler<ViewEventArgs<VendorTerminal>> UnLock;
            public event EventHandler<ViewEventArgs<VendorTerminalSearchCriteria>> Search;
            public event EventHandler<ViewEventArgs<string>> VendorSearch;
            public event EventHandler<ViewEventArgs<List<VendorTerminal>>> BatchImport;


            public void LogException(Exception ex)
            {

            }

            public void DisplaySearchResult(List<VendorTerminal> vendorTerminals)
            {
                Assert.IsTrue(vendorTerminals.Count > 0);
            }

            public void DisplayMessages(IEnumerable<ValidationMessage> messages)
            {
                Assert.IsTrue(messages.Any(m => m.Type != ValidateMessageType.Information));
            }

            public void DisplayVendor(Vendor vendor)
            {

            }

            public void FailedLock(Lock @lock)
            {

            }

            public void Load()
            {
                var handler = new CarrierTerminalHandler(this);
                handler.Initialize();
            }

            public void SaveRecord(VendorTerminal vendorTerminal)
            {
                if (Save != null)
                    Save(this, new ViewEventArgs<VendorTerminal>(vendorTerminal));
            }

            public void DeleteRecord(VendorTerminal vendorTerminal)
            {
                if (Delete != null)
                    Delete(this, new ViewEventArgs<VendorTerminal>(vendorTerminal));
            }

            public void LockRecord(VendorTerminal vendorTerminal)
            {
                if (Lock != null)
                    Lock(this, new ViewEventArgs<VendorTerminal>(vendorTerminal));
            }

            public void UnLockRecord(VendorTerminal vendorTerminal)
            {
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<VendorTerminal>(vendorTerminal));
            }
        }
    }
}
