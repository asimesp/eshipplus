﻿using System.Linq;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
    [TestFixture]
    public class DatLoadboardAssetPostingSearchTests
    {
        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

        [Test]
        public void CanFetchAllDatLoadboardAssetPostings()
        {
            var postings = new DatLoadboardAssetPostingSearch().FetchAllDatLoadboardAssetPostings(GlobalTestInitializer.DefaultTenantId);
	        if (!postings.Any()) return;
            Assert.IsTrue(postings.Any());
        }

        [Test]
        public void CanFetchDatLoadboardAssetPostingByIdNumber()
        {
            var postings = new DatLoadboardAssetPostingSearch().FetchAllDatLoadboardAssetPostings(GlobalTestInitializer.DefaultTenantId);
            if (!postings.Any()) return;

            var posting = new DatLoadboardAssetPostingSearch().FetchDatLoadboardAssetPostingByIdNumber(postings.First().IdNumber, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(posting != null);
        }
    }
}
