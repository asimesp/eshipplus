﻿using System.Linq;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Views.Connect;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{

    [TestFixture]
    public class MacroPointOrderViewSearchDtoTests
    {
        private MacroPointOrder _macroPointOrder;
	    private readonly MacroPointOrderSearch _search = new MacroPointOrderSearch();

	    [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _macroPointOrder = new MacroPointOrder(GlobalTestInitializer.DefaultMacroPointOrderId);
        }

        [Test]
        public void CanFetchMacroPointViewSeachDtoByOrderId()
        {
            var order = _search.FetchMacroPointOrderByOrderId(_macroPointOrder.OrderId, GlobalTestInitializer.DefaultTenantId);
			Assert.IsNotNull(order);
        }

        [Test]
        public void CanFetchMacroPointViewSeachDtoByIdNumber()
        {
            var order = _search.FetchMacroPointOrderByIdNumber(_macroPointOrder.IdNumber, GlobalTestInitializer.DefaultTenantId);
			Assert.IsNotNull(order);
        }

		[Test]
		public void CanFetchMacroPointOrderByIdNumAndOrderId()
		{
			var ids = _search.FetchTenantIdsByIdNumAndOrderId(_macroPointOrder.OrderId, _macroPointOrder.IdNumber);
			Assert.IsTrue(ids.Any());
		}

		[Test]
		public void CanTestMacroPointOrderForIdNumberExist()
		{
			Assert.IsTrue(_search.MacroPointOrderForIdNumberExists(_macroPointOrder.IdNumber, GlobalTestInitializer.DefaultTenantId));
		}

        [Test]
        public void CanFetchMacroPointOrders()
        {
            var searchCriteria = new MacroPointOrderSearchCriteria();
            var orders = _search.FetchMacroPointOrders(searchCriteria, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(orders != null && orders.Count() > default(int));
        }
    }
}
