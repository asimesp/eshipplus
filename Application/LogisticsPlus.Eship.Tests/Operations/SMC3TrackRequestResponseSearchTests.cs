﻿using LogisticsPlus.Eship.Processor.Searches.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
    [TestFixture]
	public class SMC3TrackRequestResponseSearchTests
    {
		
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
        [Ignore("Specify valid transaction id")]
        public void CanFetchTrackRequestResponseByTransactionId()
		{
            var search = new SMC3TrackRequestResponseSearch();
		    const string transacitonId = "TEST";
		    search.FetchTrackRequestResponseByTransactionId(transacitonId);
		}

        [Test]
        [Ignore("Specify Shipment Id")]
        public void FetchTrackRequestResponseByShipmentId()
        {
            var search = new SMC3TrackRequestResponseSearch();
            const long shipmentId = default(long);
            search.FetchTrackRequestResponseByShipmentId(shipmentId);
        }
    }
}