﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
    [TestFixture]
    public class LoadOrderValidatorTests
    {
        private LoadOrderValidator _validator;
        private LoadOrder _loadOrder;
        private ShipmentViewSearchCriteria _criteria;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _validator = new LoadOrderValidator();

            if (_loadOrder != null) return;

            var tenantId = GlobalTestInitializer.DefaultTenantId;
            var customer = new Customer(GlobalTestInitializer.DefaultCustomerId);
            var vendor = new VendorSearch().FetchVendors(new List<ParameterColumn>(), tenantId).FirstOrDefault() ?? new Vendor();
            var country = new Country(GlobalTestInitializer.DefaultCountryId);
            var user = new User(GlobalTestInitializer.DefaultUserId);
            var equipment = ProcessorVars.RegistryCache[tenantId].EquipmentTypes.FirstOrDefault() ?? new EquipmentType();
            var packageType = ProcessorVars.RegistryCache[tenantId].PackageTypes.FirstOrDefault() ?? new PackageType();
            var service = new ServiceSearch().FetchServices(new List<ParameterColumn>(), tenantId).FirstOrDefault() ?? new ServiceViewSearchDto();
            var chargeCode = new ChargeCode(GlobalTestInitializer.DefaultChargeCodeId);
            var docTag = ProcessorVars.RegistryCache[tenantId].DocumentTags.FirstOrDefault() ?? new DocumentTag();
            var accountBucket = ProcessorVars.RegistryCache[tenantId].AccountBuckets.FirstOrDefault() ?? new AccountBucket();

            var timeList = TimeUtility.BuildTimeList();
            _loadOrder = new LoadOrder
            {
                LoadOrderNumber = "00000",
				Description = DateTime.Now.ToString(),
                Customer = customer,
                PrefixId = default(long),
                HidePrefix = false,
                ServiceMode = ServiceMode.LessThanTruckload,
                Notes = new List<LoadOrderNote>(),
                CustomerReferences = new List<LoadOrderReference>(),
                DesiredPickupDate = DateTime.Now,
                AccountBuckets = new List<LoadOrderAccountBucket>(),
                EarlyDelivery = timeList[0],
                EarlyPickup = timeList[0],
                EstimatedDeliveryDate = DateTime.Now,
                Charges = new List<LoadOrderCharge>(),
                Vendors = new List<LoadOrderVendor>(),
                Stops = new List<LoadOrderLocation>(),
                Status = LoadOrderStatus.Accepted,
                DateCreated = DateTime.Now,
                User = user,
                Equipments = new List<LoadOrderEquipment>(),
                HazardousMaterial = true,
                HazardousMaterialContactEmail = "test@test.com",
                HazardousMaterialContactMobile = "111-111-1111",
                HazardousMaterialContactName = "Haz Mat. Contact",
                HazardousMaterialContactPhone = "111-222-3333",
                Items = new List<LoadOrderItem>(),
                LateDelivery = timeList[10],
                LatePickup = timeList[10],
                PurchaseOrderNumber = "PO#",
                Services = new List<LoadOrderService>(),
                ShipperReference = "Ref#",
                TenantId = tenantId,
                Mileage = 1,
                CarrierCoordinatorId = GlobalTestInitializer.DefaultUserId,

            };

            _loadOrder.Origin = new LoadOrderLocation
            {
                LoadOrder = _loadOrder,
                City = "Erie",
                Contacts = new List<LoadOrderContact>(),
                Country = country,
                Description = "Description",
                PostalCode = "16504",
                State = "Pa",
                StopOrder = 0,
                Street1 = "123 St.",
                Street2 = "Street 2",
                TenantId = tenantId,
                AppointmentDateTime = DateTime.Now
            };

            _loadOrder.Destination = new LoadOrderLocation
            {
                LoadOrder = _loadOrder,
                City = "Erie",
                Contacts = new List<LoadOrderContact>(),
                Country = country,
                Description = "Description",
                PostalCode = "16504",
                State = "Pa",
                StopOrder = 2,
                Street1 = "123 St.",
                Street2 = "Street 2",
                TenantId = tenantId,
                AppointmentDateTime = DateTime.Now
            };

            _loadOrder.Stops.Add(new LoadOrderLocation(1)
            {
                LoadOrder = _loadOrder,
                City = "Erie",
                Contacts = new List<LoadOrderContact>(),
                Country = country,
                Description = "Description",
                PostalCode = "16504",
                State = "Pa",
                StopOrder = 1,
                Street1 = "123 St.",
                Street2 = "Street 2",
                TenantId = tenantId,
                AppointmentDateTime = DateTime.Now
            });


            _loadOrder.AccountBuckets.Add(new LoadOrderAccountBucket { TenantId = tenantId, LoadOrder = _loadOrder, AccountBucket = accountBucket, Primary = true });
            _loadOrder.CustomerReferences.Add(new LoadOrderReference
            {
                LoadOrder = _loadOrder,
                Name = "Ref name",
                Required = true,
                TenantId = tenantId,
                Value = "Ref Val"
            });

            _loadOrder.Equipments.Add(new LoadOrderEquipment { LoadOrder = _loadOrder, EquipmentType = equipment, TenantId = tenantId });

            _loadOrder.Items.Add(new LoadOrderItem
            {
                LoadOrder = _loadOrder,
                Comment = "Item Comm.",
                Delivery = 2,
                Description = "Item Desc.",
                FreightClass = 92.5,
                Height = 96,
                Length = 96,
                Width = 96,
                Weight = 1000,
                PackageType = packageType,
                Pickup = 0,
                PieceCount = 1,
                Quantity = 1,
                TenantId = tenantId,
				HazardousMaterial = true,
            });

            _loadOrder.Services.Add(new LoadOrderService { LoadOrder = _loadOrder, ServiceId = service.Id, TenantId = tenantId });

            _loadOrder.Vendors.Add(new LoadOrderVendor
            {
                LoadOrder = _loadOrder,
                Vendor = vendor,
                TenantId = tenantId,
                Primary = true,
            });

            _loadOrder.Charges.Add(new LoadOrderCharge
            {
                ChargeCode = chargeCode,
                Comment = string.Empty,
                Quantity = 1,
                LoadOrder = _loadOrder,
                TenantId = tenantId,
                UnitBuy = 5.0000m,
                UnitSell = 10.0000m,
                UnitDiscount = 2.0000m
            });

            _loadOrder.Notes.Add(new LoadOrderNote
            {
                Archived = false,
                Classified = false,
                Message = "Note Test Message",
                LoadOrder = _loadOrder,
                TenantId = tenantId,
                Type = NoteType.Normal,
                User = user
            });

            _loadOrder.Documents = new List<LoadOrderDocument>
                                       {
                                           new LoadOrderDocument
                                               {
                                                   TenantId = tenantId,
                                                   LoadOrder = _loadOrder,
                                                   Name = "Document",
                                                   Description = "Description",
                                                   DocumentTagId = docTag.Id,
                                                   LocationPath = "test"
                                               }
                                       };

            _criteria = new ShipmentViewSearchCriteria { ActiveUserId = GlobalTestInitializer.DefaultUserId, };
        }

        [Test]
        public void IsValidTest()
        {
            _validator.Messages.Clear();
            var isValid = _validator.IsValid(_loadOrder);
            _validator.Messages.PrintMessages();
            Assert.IsTrue(isValid);
        }

        [Test]
        public void DuplicateNumberTest()
        {
            _validator.Messages.Clear();

            var shipment = new Shipment(GlobalTestInitializer.DefaultShipmentId);
            var number = _loadOrder.LoadOrderNumber;
            _loadOrder.LoadOrderNumber = shipment.ShipmentNumber;
            var duplicate = _validator.DuplicateLoadNumber(_loadOrder);
            _loadOrder.LoadOrderNumber = number;
            Assert.IsTrue(duplicate);
        }

        [Test]
        public void CanDeleteTest()
        {
            _validator.Messages.Clear();
            var isValid = _validator.CanDeleteLoadOrder(_loadOrder);
            _validator.Messages.PrintMessages();
            Assert.IsFalse(isValid);

			var load = new LoadOrderSearch().FetchLoadOrderByLoadNumber(GlobalTestInitializer.DefaultLoadNumber, GlobalTestInitializer.DefaultTenantId);
			Assert.IsNotNull(load);
        	isValid = _validator.CanDeleteLoadOrder(load);
			_validator.Messages.PrintMessages();
        	Assert.IsFalse(isValid);
        }

        [Test]
        public void DuplicateLoadOrderServiceTest()
        {
            var service = new LoadOrderService();

            Assert.IsFalse(_validator.LoadOrderServiceExists(service));

            var all = new LoadOrderSearch().FetchLoadsDashboardDto(_criteria, GlobalTestInitializer.DefaultTenantId)
                .Select(q => new LoadOrder(q.Id))
                .Where(c => c.Services.Count > 0)
                .ToList();
            if (!all.Any()) return;
            service = all[0].Services[0];
            Assert.IsTrue(_validator.LoadOrderServiceExists(service));
        }

        [Test]
        public void DuplicateLoadOrderEquipmentTest()
        {
            var equipment = new LoadOrderEquipment();

            Assert.IsFalse(_validator.LoadOrderEquipmentExists(equipment));
            var all = new LoadOrderSearch().FetchLoadsDashboardDto(_criteria, GlobalTestInitializer.DefaultTenantId)
                .Select(l => new LoadOrder(l.Id))
                .Where(c => c.Equipments.Count > 0)
                .ToList();
            if (!all.Any()) return;
            equipment = all[0].Equipments[0];
            Assert.IsTrue(_validator.LoadOrderEquipmentExists(equipment));
        }

    }
}