﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Dat;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
    [TestFixture]
    public class DatLoadboardAssetPostingValidatorTests
    {
        private DatLoadboardAssetPostingValidator _validator;
        private DatLoadboardAssetPosting _posting;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _validator = new DatLoadboardAssetPostingValidator();

            if (_posting != null) return;

            _posting = new DatLoadboardAssetPosting
                {
                    DateCreated = DateTime.Now,
                    ExpirationDate = DateTime.Now.AddDays(1),
                    BaseRate = 12,
                    AssetId = DateTime.Now.FormattedShortDate(),
                    HazardousMaterial = true,
                    IdNumber = DateTime.Now.FormattedShortDate(),
                    Mileage = 100,
                    TenantId = GlobalTestInitializer.DefaultTenantId,
                    RateType = RateBasedOnType.Flat,
                    UserId = GlobalTestInitializer.DefaultUserId,
                };



        }

        [Test]
        public void IsValidTest()
        {
            Assert.IsTrue(_validator.IsValid(_posting));
        }

        [Test]
        public void HasRequiredDataTest()
        {
            var hasAllRequiredData = _validator.HasAllRequiredData(_posting);
            if (!hasAllRequiredData) _validator.Messages.PrintMessages();
            Assert.IsTrue(hasAllRequiredData);
        }

        [Test]
        public void DuplicateIdNumberTest()
        {
            var posting = new DatLoadboardAssetPostingSearch().FetchAllDatLoadboardAssetPostings(GlobalTestInitializer.DefaultTenantId).FirstOrDefault();
            if (posting == null) return;

            var number = _posting.IdNumber;
            _posting.IdNumber = posting.IdNumber;
            Assert.IsTrue(_validator.DuplicateIdNumber(_posting));
            _posting.IdNumber = number;
        }

        [Test]
        public void DuplicateOrderIdTest()
        {
            var posting = new DatLoadboardAssetPostingSearch().FetchAllDatLoadboardAssetPostings(GlobalTestInitializer.DefaultTenantId).FirstOrDefault();
            if (posting == null) return;

            var orderId = _posting.AssetId;
            _posting.AssetId = posting.AssetId;
            Assert.IsTrue(_validator.DuplicateAssetId(_posting));
            _posting.AssetId = orderId;
        }
    }
}
