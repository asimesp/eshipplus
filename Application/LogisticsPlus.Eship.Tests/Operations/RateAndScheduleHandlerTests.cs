﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.PaymentGateway;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.Processor.Views.Administration;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
	[TestFixture]
	public class RateAndScheduleHandlerTests
	{
		private RateAndScheduleView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new RateAndScheduleView();
			_view.Load();
		}

		[Test]
		public void CanSaveShipment()
		{
			_view.SaveRecord(GetShipment());
		}

		[Test]
		public void CanSaveQuotedLoad()
		{
            var loadOrder = GetLoadOrder();
            loadOrder.Status = LoadOrderStatus.Quoted;
            _view.SaveLoadRecord(loadOrder);
		}

		[Test]
		public void CanSaveOfferedLoad()
		{
		    var loadOrder = GetLoadOrder();
            loadOrder.Status = LoadOrderStatus.Offered;
			loadOrder.Charges = new List<LoadOrderCharge>();
			_view.SaveLoadRecord(loadOrder);
		}


        private static LoadOrder GetLoadOrder()
        {
            var tenantId = GlobalTestInitializer.DefaultTenantId;
            var customer = new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), tenantId).FirstOrDefault() ?? new Customer();
            var vendor = new VendorSearch().FetchVendors(new List<ParameterColumn>(), tenantId).FirstOrDefault() ?? new Vendor();
            var country = ProcessorVars.RegistryCache.Countries.Values.FirstOrDefault() ?? new Country();

            var criteria = new UserSearchCriteria
            {
                Parameters = new List<ParameterColumn>()
            };
			var userDto = new UserSearch().FetchUsers(criteria, tenantId).FirstOrDefault();
            var user = userDto == null ? new User() : new User(userDto.Id);
            var equipment = ProcessorVars.RegistryCache[tenantId].EquipmentTypes.FirstOrDefault() ??
                            new EquipmentType();
            var packageType = ProcessorVars.RegistryCache[tenantId].PackageTypes.FirstOrDefault() ??
                              new PackageType();
            var service = new ServiceSearch().FetchServices(new List<ParameterColumn>(), tenantId).FirstOrDefault() ??
                          new ServiceViewSearchDto();
            var chargeCode = ProcessorVars.RegistryCache[tenantId].ChargeCodes.FirstOrDefault() ?? new ChargeCode();
            var accountBucket = ProcessorVars.RegistryCache[tenantId].AccountBuckets.FirstOrDefault() ??
                                new AccountBucket();
            var documentTag = ProcessorVars.RegistryCache[tenantId].DocumentTags.FirstOrDefault() ??
                                new DocumentTag();

            var timeList = TimeUtility.BuildTimeList();

            var loadOrder = new LoadOrder
            {
                LoadOrderNumber = "00000",
				Description = DateTime.Now.ToString(),
                Customer = customer,
                PrefixId = default(long),
                HidePrefix = false,
                ServiceMode = ServiceMode.LessThanTruckload,
                Notes = new List<LoadOrderNote>(),
                CustomerReferences = new List<LoadOrderReference>(),
                ShipperBol = string.Empty,

                GeneralBolComments = string.Empty,
                CriticalBolComments = string.Empty,
                MiscField1 = string.Empty,
                MiscField2 = string.Empty,

				DriverName = string.Empty,
				DriverPhoneNumber = string.Empty,
				DriverTrailerNumber = string.Empty,

                AccountBucketUnitId = 0,

                EarlyDelivery = timeList[0],
                EarlyPickup = timeList[0],

                DateCreated = DateTime.Now,
                EstimatedDeliveryDate = DateTime.Now,
                DesiredPickupDate = DateTime.Now,

                Charges = new List<LoadOrderCharge>(),
                Vendors = new List<LoadOrderVendor>(),
                Stops = new List<LoadOrderLocation>(),
                Status = LoadOrderStatus.Accepted,

                User = user,
                Equipments = new List<LoadOrderEquipment>(),
                HazardousMaterial = true,
                HazardousMaterialContactEmail = "test@test.com",
                HazardousMaterialContactMobile = "111-111-1111",
                HazardousMaterialContactName = "Haz Mat. Contact",
                HazardousMaterialContactPhone = "111-222-3333",
                Items = new List<LoadOrderItem>(),
                LateDelivery = timeList[10],
                LatePickup = timeList[10],
                PurchaseOrderNumber = "PO#",
                Services = new List<LoadOrderService>(),
                ShipperReference = "Ref#",
                TenantId = tenantId,
                Mileage = 1,
                CarrierCoordinatorId = GlobalTestInitializer.DefaultUserId,

				SalesRepAddlEntityName = string.Empty
            };

            loadOrder.Origin = new LoadOrderLocation
            {
                LoadOrder = loadOrder,
                City = "Erie",
                Contacts = new List<LoadOrderContact>(),
                Country = country,
                Description = "Description",
                PostalCode = "16504",
                State = "Pa",
                StopOrder = 0,
                Street1 = "123 St.",
                Street2 = "Street 2",
                TenantId = tenantId,
                Direction = string.Empty,
                GeneralInfo = string.Empty,
                SpecialInstructions = string.Empty,
                AppointmentDateTime = DateTime.Now
            };

            loadOrder.Destination = new LoadOrderLocation
            {
                LoadOrder = loadOrder,
                City = "Erie",
                Contacts = new List<LoadOrderContact>(),
                Country = country,
                Description = "Description",
                PostalCode = "16504",
                State = "Pa",
                StopOrder = 2,
                Street1 = "123 St.",
                Street2 = "Street 2",
                TenantId = tenantId,
                Direction = string.Empty,
                GeneralInfo = string.Empty,
                SpecialInstructions = string.Empty,
                AppointmentDateTime = DateTime.Now
            };

            loadOrder.Stops.Add(new LoadOrderLocation
            {
                LoadOrder = loadOrder,
                City = "Erie",
                Contacts = new List<LoadOrderContact>(),
                Country = country,
                Description = "Description",
                PostalCode = "16504",
                State = "Pa",
                StopOrder = 1,
                Street1 = "123 St.",
                Street2 = "Street 2",
                TenantId = tenantId,
                Direction = string.Empty,
                GeneralInfo = string.Empty,
                SpecialInstructions = string.Empty,
                AppointmentDateTime = DateTime.Now
            });

            loadOrder.CustomerReferences.Add(new LoadOrderReference
            {
                LoadOrder = loadOrder,
                Name = "Ref name",
                Required = true,
                TenantId = tenantId,
                Value = "Ref Val"
            });
            loadOrder.Equipments.Add(new LoadOrderEquipment
            {
                LoadOrder = loadOrder,
                EquipmentType = equipment,
                TenantId = tenantId
            });

            loadOrder.Items.Add(new LoadOrderItem
            {
                LoadOrder = loadOrder,
                Comment = "Item Comm.",
                Delivery = 2,
                Description = "Item Desc.",
                FreightClass = 92.5,
                Height = 96,
                Length = 96,
                Width = 96,
                Weight = 1000,
                PackageType = packageType,
                Pickup = 0,
                PieceCount = 1,
                Quantity = 1,
                TenantId = tenantId,
                NMFCCode = string.Empty,
                HTSCode = string.Empty,
				HazardousMaterial = true,

            });

            loadOrder.Services.Add(new LoadOrderService
            {
                LoadOrder = loadOrder,
                ServiceId = service.Id,
                TenantId = tenantId
            });

            loadOrder.Vendors.Add(new LoadOrderVendor
            {
                LoadOrder = loadOrder,
                Vendor = vendor,
                TenantId = tenantId,
                Primary = true,
                ProNumber = "text"
            });

            loadOrder.Charges.Add(new LoadOrderCharge
            {
                ChargeCode = chargeCode,
                Comment = string.Empty,
                Quantity = 1,
                LoadOrder = loadOrder,
                TenantId = tenantId,
                UnitBuy = 5.0000m,
                UnitSell = 10.0000m,
                UnitDiscount = 2.0000m
            });

            loadOrder.Notes.Add(new LoadOrderNote
            {
                Archived = false,
                Classified = false,
                Message = "Note Test Message",
                LoadOrder = loadOrder,
                TenantId = tenantId,
                Type = NoteType.Normal,
                User = user
            });

            loadOrder.AccountBuckets = new List<LoadOrderAccountBucket>
                                           {
                                               new LoadOrderAccountBucket
                                                   {
                                                       AccountBucketId = accountBucket.Id,
                                                       Primary = true,
                                                       LoadOrder = loadOrder,
                                                       TenantId = tenantId,
                                                   }
                                           };

            loadOrder.Documents = new List<LoadOrderDocument>
                                      {
                                          new LoadOrderDocument
                                              {
                                                  LoadOrder = loadOrder,
                                                  Description = "Test Description",
                                                  TenantId = tenantId,
                                                  DocumentTag = documentTag,
                                                  LocationPath = "Test Location Path",
                                                  Name = "Test Name"
                                              }
                                      };

            return loadOrder;
        }

        private static Shipment GetShipment()
        {
            var tenantId = GlobalTestInitializer.DefaultTenantId;
            var customer = new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), tenantId).FirstOrDefault() ?? new Customer();
            var vendor = new VendorSearch().FetchVendors(new List<ParameterColumn>(), tenantId).FirstOrDefault() ?? new Vendor();
            var country = ProcessorVars.RegistryCache.Countries.Values.FirstOrDefault() ?? new Country();

            var criteria = new UserSearchCriteria
            {
                Parameters = new List<ParameterColumn>()
            };
			var userDto = new UserSearch().FetchUsers(criteria, tenantId).FirstOrDefault();
            var user = userDto == null ? new User() : new User(userDto.Id);

            var packageType = ProcessorVars.RegistryCache[tenantId].PackageTypes.FirstOrDefault() ??
                              new PackageType();
            var service = new ServiceSearch().FetchServices(new List<ParameterColumn>(), tenantId).FirstOrDefault() ??
                          new ServiceViewSearchDto();
            var chargeCode = ProcessorVars.RegistryCache[tenantId].ChargeCodes.FirstOrDefault() ?? new ChargeCode();
            var failureCode = ProcessorVars.RegistryCache[tenantId].FailureCodes.FirstOrDefault() ?? new FailureCode();
            var accountBucket = ProcessorVars.RegistryCache[tenantId].AccountBuckets.FirstOrDefault() ??
                                new AccountBucket();
            var documentTag = ProcessorVars.RegistryCache[tenantId].DocumentTags.FirstOrDefault() ??
                                new DocumentTag();



            var timeList = TimeUtility.BuildTimeList();

            var shipment = new Shipment
            {
                ShipmentNumber = "00000",
                Customer = customer,
                PrefixId = default(long),
                HidePrefix = false,
                ServiceMode = ServiceMode.LessThanTruckload,
                Notes = new List<ShipmentNote>(),
                CustomerReferences = new List<ShipmentReference>(),
                ShipperBol = string.Empty,

                GeneralBolComments = string.Empty,
                CriticalBolComments = string.Empty,
                VendorRatingOverrideAddress = string.Empty,

                EarlyDelivery = timeList[0],
                EarlyPickup = timeList[0],

                DateCreated = DateTime.Now,
                EstimatedDeliveryDate = DateTime.Now,
                ActualDeliveryDate = DateTime.Now,
                DesiredPickupDate = DateTime.Now,
                ActualPickupDate = DateTime.Now,

                Charges = new List<ShipmentCharge>(),
                Vendors = new List<ShipmentVendor>(),
                Stops = new List<ShipmentLocation>(),
                Status = ShipmentStatus.Scheduled,

                User = user,
                Equipments = new List<ShipmentEquipment>(),
                HazardousMaterial = true,
                HazardousMaterialContactEmail = "test@test.com",
                HazardousMaterialContactMobile = "111-111-1111",
                HazardousMaterialContactName = "Haz Mat. Contact",
                HazardousMaterialContactPhone = "111-222-3333",
                Items = new List<ShipmentItem>(),
                LateDelivery = timeList[10],
                LatePickup = timeList[10],
                PurchaseOrderNumber = "PO#",
                Services = new List<ShipmentService>(),
                ShipperReference = "Ref#",
                TenantId = tenantId,
                OriginalRateValue = 0,
                VendorDiscountPercentage = 0,
                VendorFreightFloor = 0,
                VendorFuelPercent = 0,
                CustomerFreightMarkupValue = 0,
                CustomerFreightMarkupPercent = 0,
                UseLowerCustomerFreightMarkup = false,
                BilledWeight = 0,
                ApplyDiscount = false,
                LineHaulProfitAdjustmentRatio = 0,
                FuelProfitAdjustmentRatio = 0,
                AccessorialProfitAdjustmentRatio = 0,
                ServiceProfitAdjustmentRatio = 0,
                ResellerAdditionalFreightMarkup = 0,
                ResellerAdditionalFreightMarkupIsPercent = false,
                ResellerAdditionalFuelMarkup = 0,
                ResellerAdditionalFuelMarkupIsPercent = false,
                ResellerAdditionalAccessorialMarkup = 0,
                ResellerAdditionalAccessorialMarkupIsPercent = false,
                DirectPointRate = false,
                SmallPackType = string.Empty,
                SmallPackageEngine = SmallPackageEngine.None,
                AutoRatingAccessorials = new List<ShipmentAutoRatingAccessorial>(),
                EmptyMileage = 0m,
                MiscField1 = string.Empty,
                MiscField2 = string.Empty,
				DriverName = string.Empty,
				DriverPhoneNumber = string.Empty,
				DriverTrailerNumber = string.Empty,
				SalesRepAddlEntityName = string.Empty,
                GuaranteedDeliveryServiceTime = string.Empty,
                IsGuaranteedDeliveryService = false,
                GuaranteedDeliveryServiceRateType = RateType.Flat,
		        GuaranteedDeliveryServiceRate = default(decimal),
				Project44QuoteNumber = string.Empty,
                ExpeditedServiceTime = string.Empty
            };

            shipment.Origin = new ShipmentLocation
            {
                Shipment = shipment,
                City = "Erie",
                Contacts = new List<ShipmentContact>(),
                Country = country,
                Description = "Description",
                PostalCode = "16504",
                State = "Pa",
                StopOrder = 0,
                Street1 = "123 St.",
                Street2 = "Street 2",
                TenantId = tenantId,
                Direction = string.Empty,
                GeneralInfo = string.Empty,
                SpecialInstructions = string.Empty,
                AppointmentDateTime = DateTime.Now
            };

            shipment.Destination = new ShipmentLocation
            {
                Shipment = shipment,
                City = "Erie",
                Contacts = new List<ShipmentContact>(),
                Country = country,
                Description = "Description",
                PostalCode = "16504",
                State = "Pa",
                StopOrder = 2,
                Street1 = "123 St.",
                Street2 = "Street 2",
                TenantId = tenantId,
                Direction = string.Empty,
                GeneralInfo = string.Empty,
                SpecialInstructions = string.Empty,
                AppointmentDateTime = DateTime.Now
            };

            shipment.OriginTerminal = new LTLTerminalInfo
            {
                Shipment = shipment,
                TenantId = tenantId,
                Code = DateTime.Now.ToShortDateString(),
                Name = DateTime.Now.ToShortDateString(),
                Phone = DateTime.Now.ToShortDateString(),
                TollFree = DateTime.Now.ToShortDateString(),
                Fax = DateTime.Now.ToShortDateString(),
                Email = "test@test.com",
                ContactName = DateTime.Now.ToShortDateString(),
                ContactTitle = "president",
                Street1 = DateTime.Now.ToShortDateString(),
                Street2 = DateTime.Now.ToShortDateString(),
                City = DateTime.Now.ToShortDateString(),
                State = DateTime.Now.ToShortDateString(),
                Country = country,
                PostalCode = "123456789",
            };

            shipment.CustomerReferences.Add(new ShipmentReference
            {
                Shipment = shipment,
                Name = "Ref name",
                Required = true,
                TenantId = tenantId,
                Value = "Ref Val"
            });


            shipment.Items.Add(new ShipmentItem
            {
                Shipment = shipment,
                Comment = "Item Comm.",
                Delivery = 2,
                Description = "Item Desc.",
                ActualFreightClass = 92.5,
                ActualHeight = 96,
                ActualLength = 96,
                ActualWidth = 96,
                ActualWeight = 1000,
                PackageType = packageType,
                Pickup = 0,
                PieceCount = 1,
                Quantity = 1,
                TenantId = tenantId,
				HazardousMaterial = true,
            });

            shipment.Services.Add(new ShipmentService
            {
                Shipment = shipment,
                ServiceId = service.Id,
                TenantId = tenantId
            });

            shipment.Vendors.Add(new ShipmentVendor
            {
                Shipment = shipment,
                Vendor = vendor,
                TenantId = tenantId,
                Primary = true,
                FailureCodeId = failureCode.Id,
                FailureComments = "test",
                ProNumber = "text"
            });

            shipment.Charges.Add(new ShipmentCharge
            {
                ChargeCode = chargeCode,
                Comment = string.Empty,
                Quantity = 1,
                Shipment = shipment,
                TenantId = tenantId,
                UnitBuy = 5.0000m,
                UnitSell = 10.0000m,
                UnitDiscount = 2.0000m
            });

            shipment.AccountBuckets.Add(new ShipmentAccountBucket
            {
                AccountBucketId = accountBucket.Id,
                Primary = true,
                Shipment = shipment,
                TenantId = tenantId,
            });

            shipment.Documents.Add(new ShipmentDocument
            {
                Shipment = shipment,
                Description = "Test Description",
                TenantId = tenantId,
                DocumentTag = documentTag,
                LocationPath = "Test Location Path",
                Name = "Test Name"
            });

            shipment.AutoRatingAccessorials.Add(new ShipmentAutoRatingAccessorial
            {
                BuyCeiling = 500,
                BuyFloor = 100,
                SellCeiling = 1000,
                SellFloor = 200,
                MarkupPercent = 10,
                MarkupValue = 50,
                Rate = 50,
                RateType = RateType.Flat,
                Shipment = shipment,
                TenantId = tenantId,
                ServiceDescription = "My Description",
                UseLowerSell = false,
            }
                );


            return shipment;
        }



		internal class RateAndScheduleView : IRateAndScheduleView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}
			public bool SaveOriginToAddressBook
			{
				get { return false;	 }
			}
			public bool SaveOriginWithServicesToAddressBook
			{
				get { return false; }
			}
			public bool SaveDestinationToAddressBook
			{
				get { return false; }
			}
			public bool SaveDestinationWithServicesToAddressBook
			{
				get { return false; }
			}

			public List<Country> Countries
			{
				set { }
			}
			public List<ContactType> ContactTypes
			{
				set { }
			}
			public List<EquipmentType> EquipmentTypes
			{
				set { }
			}
			public List<PackageType> PackageTypes
			{
				set { }
			}
			public List<ServiceViewSearchDto> Services
			{
				set { }
			}

			public event EventHandler Loading;
            public event EventHandler<ViewEventArgs<Shipment, PaymentInformation>> SaveShipment;
            public event EventHandler<ViewEventArgs<LoadOrder, PaymentInformation>> SaveLoadOrder;
			public event EventHandler<ViewEventArgs<string>> CustomerSearch;
			public event EventHandler<ViewEventArgs<ShoppedRate>> SaveShoppedRate;


		    public void DisplayCustomer(Customer customer)
			{
			}

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void Set(Shipment shipment)
			{
				var view = new ShipmentHandlerTests.ShipmentView();
				view.Load();
				view.DeleteRecord(shipment);
			}

		    public void Set(LoadOrder loadOrder)
		    {
                var view = new LoadOrderHandlerTests.LoadOrderView();
                view.Load();
                view.DeleteRecord(loadOrder); 
		    }

			public void Set(ShoppedRate shoppedRate)
			{
			}



			public void Load()
			{
				var handler = new RateAndScheduleHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(Shipment shipment)
			{
				if (SaveShipment != null)
					SaveShipment(this, new ViewEventArgs<Shipment, PaymentInformation>(shipment, null));
			}

            public void SaveLoadRecord(LoadOrder loadOrder)
			{
				if (SaveLoadOrder != null)
					SaveLoadOrder(this, new ViewEventArgs<LoadOrder, PaymentInformation>(loadOrder, null));
			}
		}
	}
}
