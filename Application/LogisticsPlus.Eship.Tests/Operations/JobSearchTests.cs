﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
	public class JobSearchTests
	{
		private JobViewSearchCriteria _criteria;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_criteria = new JobViewSearchCriteria { ActiveUserId = GlobalTestInitializer.DefaultUserId, };
		}

		
		[Test]
		public void CanFetchJobDashboardDtos()
		{
			_criteria.Parameters.Add(new ParameterColumn
			{
				DefaultValue = "10033",
				ReportColumnName = OperationsSearchFields.JobNumber.DisplayName,
				Operator = Operator.Contains
			});
			var jobs = new JobSearch().FetchJobDashboardDtos(_criteria, GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(jobs.Any());
		}

        [Test]
        public void CanFetchJobByJobNumber()
        {
            var job = new JobSearch().FetchJobByJobNumber(GlobalTestInitializer.DefaultJobNumber, GlobalTestInitializer.DefaultTenantId);
            Assert.IsNotNull(job);
        }

        [Test]
		public void CanFetchJobComponents()
		{
            var job = new JobSearch().FetchJobByJobNumber(GlobalTestInitializer.DefaultJobNumber, GlobalTestInitializer.DefaultTenantId);
            Assert.IsNotNull(job, "Default Job mast be Not null");
			
			var jobs = new JobSearch().FetchJobComponents(job.Id, GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(jobs.Any());
		}
		
	}
}
