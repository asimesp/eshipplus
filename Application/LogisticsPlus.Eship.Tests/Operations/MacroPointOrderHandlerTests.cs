﻿using System;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
    [TestFixture]
    public class MacroPointOrderHandlerTests
    {
        private MacroPointOrderUpdateHandler _handler;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _handler = new MacroPointOrderUpdateHandler();
        }

        [Test]
        public void CanSaveOrUpdate()
        {
            var order = new MacroPointOrder(GlobalTestInitializer.DefaultMacroPointOrderId);
            Assert.IsTrue(order.KeyLoaded);

            order.TakeSnapShot();
            var number = order.IdNumber;
            order.IdNumber = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff");
            Exception ex;
            _handler.SaveMacroPointOrder(order, out ex);
            order.TakeSnapShot();
            order.IdNumber = number;
            _handler.SaveMacroPointOrder(order, out ex);

            Assert.IsTrue(ex == null);
        }
    }
}
