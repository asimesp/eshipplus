﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.Processor.Views.Administration;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
    [TestFixture]
    public class ServiceTicketValidatorTests
    {
		private ServiceTicketValidator _validator;
        private ServiceTicket _serviceTicket;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new ServiceTicketValidator();

            if (_serviceTicket != null) return;

            var tenantId = GlobalTestInitializer.DefaultTenantId;
			var customer = new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), tenantId).FirstOrDefault() ?? new Customer();
			var vendor = new VendorSearch().FetchVendors(new List<ParameterColumn>(), tenantId).FirstOrDefault() ?? new Vendor();

            var criteria = new UserSearchCriteria
            {
                Parameters = new List<ParameterColumn>()
            };
			var userDto = new UserSearch().FetchUsers(criteria, tenantId).FirstOrDefault();
            var user = userDto == null ? new User() : new User(userDto.Id);
            var equipment = ProcessorVars.RegistryCache[tenantId].EquipmentTypes.FirstOrDefault() ??
							new EquipmentType();
			var service = new ServiceSearch().FetchServices(new List<ParameterColumn>(), tenantId).FirstOrDefault() ??
						  new ServiceViewSearchDto();
            var chargeCode = ProcessorVars.RegistryCache[tenantId].ChargeCodes.FirstOrDefault() ?? new ChargeCode();
            var accountBucket = ProcessorVars.RegistryCache[tenantId].AccountBuckets.FirstOrDefault() ??
								new AccountBucket();
            var assets = ProcessorVars.RegistryCache[tenantId].Assets;
			var docTag = ProcessorVars.RegistryCache[tenantId].DocumentTags.FirstOrDefault() ?? new DocumentTag();

			_serviceTicket = new ServiceTicket
							{
								ServiceTicketNumber = "00000",
								Customer = customer,
								PrefixId = default(long),
								AccountBucket = accountBucket,
								HidePrefix = false,
								DateCreated = DateTime.Now,
								Status = ServiceTicketStatus.Open,
								User = user,
								TenantId = tenantId,

								ExternalReference1 = string.Empty,
								ExternalReference2 = string.Empty,

								Equipments = new List<ServiceTicketEquipment>(),
								Items = new List<ServiceTicketItem>(),
								Services = new List<ServiceTicketService>(),
								Vendors = new List<ServiceTicketVendor>(),
								Charges = new List<ServiceTicketCharge>(),
								Notes = new List<ServiceTicketNote>(),
								Assets = new List<ServiceTicketAsset>(),
							};


			_serviceTicket.Equipments.Add(new ServiceTicketEquipment
			{
				ServiceTicket = _serviceTicket,
				EquipmentType = equipment,
				TenantId = tenantId
			});

			_serviceTicket.Items.Add(new ServiceTicketItem
			{
				ServiceTicket = _serviceTicket,
				Comment = "Item Comm.",
				Description = "Item Desc.",
				TenantId = tenantId
			});

			_serviceTicket.Services.Add(new ServiceTicketService
			{
				ServiceTicket = _serviceTicket,
				ServiceId = service.Id,
				TenantId = tenantId
			});

			_serviceTicket.Vendors.Add(new ServiceTicketVendor
			{
				ServiceTicket = _serviceTicket,
				Vendor = vendor,
				TenantId = tenantId,
				Primary = true,
			});

			_serviceTicket.Charges.Add(new ServiceTicketCharge
			{
				ChargeCode = chargeCode,
				Comment = string.Empty,
				Quantity = 1,
				ServiceTicket = _serviceTicket,
				TenantId = tenantId,
				UnitBuy = 5.0000m,
				UnitSell = 10.0000m,
				UnitDiscount = 2.0000m
			});

			_serviceTicket.Notes.Add(new ServiceTicketNote
			{
				Archived = false,
				Classified = false,
				Message = "Note Test Message",
				ServiceTicket = _serviceTicket,
				TenantId = tenantId,
				Type = NoteType.Normal,
				User = user
			});
			_serviceTicket.Documents.Add(new ServiceTicketDocument
			{
				TenantId = tenantId,
				ServiceTicket = _serviceTicket,
				Name = "Document",
				Description = "Description",
				DocumentTagId = docTag.Id,
				LocationPath = "test"
			});

            var assetTractor = assets.FirstOrDefault(c => c.AssetType == AssetType.IndependentTractor || c.AssetType == AssetType.OwnerOpTractor || c.AssetType == AssetType.CompanyTractor);
            var assetTrailer = assets.FirstOrDefault(c => c.AssetType == AssetType.Trailer);
        	_serviceTicket.Assets.Add(new ServiceTicketAsset
        	                          	{
        	                          		ServiceTicket = _serviceTicket,
        	                          		DriverAssetId = assets.First(c => c.AssetType == AssetType.Driver).Id,
        	                          		TractorAssetId = assetTractor == null ? default(long) : assetTractor.Id,
        	                          		TrailerAssetId = assetTrailer == null ? default(long) : assetTrailer.Id,
        	                          		MilesRun = 1m,
        	                          		TenantId = tenantId,
        	                          		Primary = true
        	                          	});
        }

        [Test]
        public void IsValidTest()
        {
            Assert.IsTrue(_validator.IsValid(_serviceTicket));
        }

        [Test]
        public void HasRequiredDataTest()
        {
            var hasAllRequiredData = _validator.HasAllRequiredData(_serviceTicket);
            if (!hasAllRequiredData) _validator.Messages.PrintMessages();
            Assert.IsTrue(hasAllRequiredData);
        }

        [Test]
        public void DuplicateServiceTicketNumberTest()
        {
			var criteria = new ServiceTicketViewSearchCriteria
            {
                ActiveUserId = GlobalTestInitializer.DefaultUserId
            };

			var all = new ServiceTicketSearch().FetchServiceTicketDashboardDtos(criteria, GlobalTestInitializer.DefaultTenantId);

            if (all.Count == 0) return;

            var number = _serviceTicket.ServiceTicketNumber;
			_serviceTicket.ServiceTicketNumber = all[0].ServiceTicketNumber;
            Assert.IsTrue(_validator.DuplicateServiceTicketNumber(_serviceTicket));
			_serviceTicket.ServiceTicketNumber = number;
        }

        [Test]
        public void CanDeleteTest()
        {
            _validator.Messages.Clear();
            _validator.CanDeleteServiceTicket(_serviceTicket);
            _validator.Messages.PrintMessages();
        }

        [Test]
        public void DuplicateServiceTicketServiceTest()
        {
            var service = new ServiceTicketService();
			var criteria = new ServiceTicketViewSearchCriteria
                            {
                                ActiveUserId = GlobalTestInitializer.DefaultUserId
                            };
            Assert.IsFalse(_validator.ServiceTicketServiceExists(service));
			var all = new ServiceTicketSearch().FetchServiceTicketDashboardDtos(criteria, GlobalTestInitializer.DefaultTenantId)
                .Select(s => new ServiceTicket(s.Id))
                .Where(c => c.Services.Count > 0)
                .ToList();
            if (all.Count == 0) return;
            service = all[0].Services[0];
            Assert.IsTrue(_validator.ServiceTicketServiceExists(service));
        }

        [Test]
        public void DuplicateServiceTicketEquipmentTest()
        {
			var equipment = new ServiceTicketEquipment();
			var criteria = new ServiceTicketViewSearchCriteria
                            {
                                ActiveUserId = GlobalTestInitializer.DefaultUserId
                            };
            Assert.IsFalse(_validator.ServiceTicketEquipmentExists(equipment));
			var all = new ServiceTicketSearch().FetchServiceTicketDashboardDtos(criteria, GlobalTestInitializer.DefaultTenantId)
				.Select(q => new ServiceTicket(q.Id))
                .Where(c => c.Equipments.Count > 0)
                .ToList();
            if (all.Count == 0) return;
            equipment = all[0].Equipments[0];
			Assert.IsTrue(_validator.ServiceTicketEquipmentExists(equipment));
        }
    }
}
