﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
	[TestFixture]
	public class LoadOrderSearchTests
	{
	    private LoadOrderSearch _search;

		private ShipmentViewSearchCriteria _criteria;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _search = new LoadOrderSearch();
			_criteria = new ShipmentViewSearchCriteria {ActiveUserId = GlobalTestInitializer.DefaultUserId,};
		}

		[Test]
		public void CanFetchLoadOrderByShipmentNumber()
		{
			var loadOrders = _search.FetchLoadsDashboardDto(_criteria, GlobalTestInitializer.DefaultTenantId);
            if (!loadOrders.Any()) return;
		    
            var load = loadOrders.First();
            var result = _search.FetchLoadOrderByLoadNumber(load.ShipmentNumber, GlobalTestInitializer.DefaultTenantId);

            Assert.IsNotNull(result);
		}

		[Test]
		public void CanFetchLoadOrderByShipperBol()
		{
			var result = _search.FetchLoadOrderByShipperBol("PRE74SUF", GlobalTestInitializer.DefaultTenantId);

			Assert.IsNotNull(result);
		}

		[Test]
		public void CanFetchShipmentsDashboardDto()
		{
            _criteria.Parameters.Add(new ParameterColumn
            {
                DefaultValue = DateTime.Now.AddYears(-1).ToString(),
                ReportColumnName = OperationsSearchFields.DateCreated.DisplayName,
                Operator = Operator.GreaterThanOrEqual
            });

			var results = _search.FetchLoadsDashboardDto(_criteria, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(results.Any());
		}

		[Test]
		public void CanFetchShipmentsDashboardDtoWithShipmentRefernce()
		{
			var field = OperationsSearchFields.ShipmentReferenceName;
			if (field != null) _criteria.Parameters.Add(new ParameterColumn { DefaultValue = string.Empty, ReportColumnName = field.DisplayName, Operator = Operator.Contains });
			field = OperationsSearchFields.ShipmentReferenceValue;
			if (field != null) _criteria.Parameters.Add(new ParameterColumn { DefaultValue = string.Empty, ReportColumnName = field.DisplayName, Operator = Operator.Contains });

			var results = _search.FetchLoadsDashboardDto(_criteria, GlobalTestInitializer.DefaultTenantId);
			Console.WriteLine(results.Count);
			Assert.IsTrue(results.Any());
		}

		[Test]
		public void CanFetchShipmentsDashboardDtoWithNoFilter()
		{
			_criteria.Parameters= new List<ParameterColumn>();
		    var results = _search.FetchLoadsDashboardDto(_criteria, GlobalTestInitializer.DefaultTenantId);

            Assert.IsTrue(results.Any());
		}


		[Test]
		public void CanFetchAvailableBookingRequests()
		{
			new LoadOrderSearch().FetchAvailableLoads(GlobalTestInitializer.DefaultTenantId);
		}
	}
}
