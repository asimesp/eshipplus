﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
    [TestFixture]
    public class VendorPreferredLaneTests
    {
        private VendorPreferredLaneValidator _validator;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _validator = new VendorPreferredLaneValidator();
        }

        [Test]
        public void IsValidTest()
        {
            var allVendors = new VendorSearch().FetchVendors(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
            if (!allVendors.Any()) return;

            var vendor = allVendors.FirstOrDefault();
            var vendorPreferredLane = new VendorPreferredLane
                                          {
                                              OriginCity = DateTime.Now.ToShortDateString(),
                                              OriginState = DateTime.Now.ToShortDateString(),
                                              OriginPostalCode = DateTime.Now.ToShortDateString(),
                                              OriginCountryId = GlobalTestInitializer.DefaultCountryId,
                                              DestinationCity = DateTime.Now.ToShortDateString(),
                                              DestinationState = DateTime.Now.ToShortDateString(),
                                              DestinationPostalCode = DateTime.Now.ToShortDateString(),
                                              DestinationCountryId = GlobalTestInitializer.DefaultCountryId,
                                              Vendor = vendor,
                                              TenantId = GlobalTestInitializer.DefaultTenantId
                                          };
           
            Assert.IsTrue(_validator.IsValid(vendorPreferredLane));
        }
    }
}
