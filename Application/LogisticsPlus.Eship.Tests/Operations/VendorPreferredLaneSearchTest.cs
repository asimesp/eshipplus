﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
    [TestFixture]
    public class VendorPreferredLaneSearchTest
    {
        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

        [Test]
        public void CanFetchVendorPreferredLanes()
        {

            var column = OperationsSearchFields.VendorName.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;

            var results = new VendorPreferredLaneSearch().FetchVendorPreferredLanes(new List<ParameterColumn> { column }, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(results.Any());
        }
    }
}
