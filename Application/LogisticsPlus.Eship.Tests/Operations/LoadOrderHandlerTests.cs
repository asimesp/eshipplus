﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.Processor.Views.Administration;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
    [TestFixture]
    public class LoadOrderHandlerTests
    {
        private LoadOrderView _view;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _view = new LoadOrderView();
            _view.Load();
        }

        [Test]
        public void CanSaveOrUpdate()
        {
			var criteria = new ShipmentViewSearchCriteria { ActiveUserId = GlobalTestInitializer.DefaultUserId, };
            var all = new LoadOrderSearch().FetchLoadsDashboardDto(criteria, GlobalTestInitializer.DefaultTenantId);
	       
            if (all.Count == 0) return;
            var loadOrder = new LoadOrder(all[0].Id);

	        var customerPurchaseOrder =
		        new CustomerPurchaseOrderSearch().FetchCustomerPurchaseOrders(new PurchaseOrderViewSearchCriteria()
			        {
				        CustomerId = loadOrder.Customer.Id
			        },
		                                                                      GlobalTestInitializer.DefaultTenantId)
		                                         .FirstOrDefault(
			                                         p =>
			                                         p.ExpirationDate > DateTime.Now && p.CurrentUses < p.MaximumUses &&
			                                         loadOrder.Destination.PostalCode == p.ValidPostalCode);
            loadOrder.LoadCollections();
	        loadOrder.Items[0].HazardousMaterial = true;

            _view.LockRecord(loadOrder);
            loadOrder.TakeSnapShot();

            // Set all emails to avoid junk/test data
            loadOrder.Destination.Contacts.ForEach(c =>
            {
                c.TakeSnapShot();
                c.Email = "test@test.com";
            });
            loadOrder.Origin.Contacts.ForEach(c =>
            {
                c.TakeSnapShot();
                c.Email = "test@test.com";
            });
            loadOrder.HazardousMaterialContactEmail = "test@test.com";
            var number = loadOrder.PurchaseOrderNumber;
            loadOrder.PurchaseOrderNumber = customerPurchaseOrder != null? customerPurchaseOrder.PurchaseOrderNumber: string.Empty;

            _view.SaveRecord(loadOrder);
            loadOrder.TakeSnapShot();
            loadOrder.PurchaseOrderNumber = number;

            _view.SaveRecord(loadOrder);
            _view.UnLockRecord(loadOrder);
        }

        [Test]
        public void CanSaveAndDelete()
        {
            var loadOrder = GetLoadOrder();
            _view.SaveRecord(loadOrder);
            Assert.IsTrue(loadOrder.Id != default(long));
            _view.DeleteRecord(loadOrder);
            Assert.IsTrue(!new LoadOrder(loadOrder.Id, false).KeyLoaded);

        }

        [Test]
        public void CanSaveAndConvert()
        {
            var loadOrder = GetLoadOrder();
            loadOrder.Status = LoadOrderStatus.Offered;
            
            _view.SaveRecord(loadOrder);
            _view.LockRecord(loadOrder);

            loadOrder.Status = LoadOrderStatus.Shipment;
            
            var shipment = loadOrder.ToShipment();
            var user = new User(GlobalTestInitializer.DefaultUserId);
            shipment.User = user;

            _view.SaveRecordAndConvertToShipment(loadOrder, shipment);

            Assert.IsTrue(shipment.Id != default(long));

            var shipmentView = new ShipmentView();
            shipmentView.Load();
            shipmentView.DeleteRecord(shipment);
            
            Assert.IsTrue(!new Shipment(shipment.Id, false).KeyLoaded);

            _view.DeleteRecord(loadOrder);
            Assert.IsTrue(!new LoadOrder(loadOrder.Id, false).KeyLoaded);
        }

        private static LoadOrder GetLoadOrder()
        {
            var tenantId = GlobalTestInitializer.DefaultTenantId;
            var customer = new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), tenantId).FirstOrDefault() ?? new Customer();
            var vendor = new VendorSearch().FetchVendors(new List<ParameterColumn>(), tenantId).FirstOrDefault() ?? new Vendor();
            var country = ProcessorVars.RegistryCache.Countries.Values.FirstOrDefault() ?? new Country();

            var criteria = new UserSearchCriteria
            {
                Parameters = new List<ParameterColumn>()
            };
			var userDto = new UserSearch().FetchUsers(criteria, tenantId).FirstOrDefault();
            var user = userDto == null ? new User() : new User(userDto.Id);
            var equipment = ProcessorVars.RegistryCache[tenantId].EquipmentTypes.FirstOrDefault() ??
                            new EquipmentType();
            var packageType = ProcessorVars.RegistryCache[tenantId].PackageTypes.FirstOrDefault() ??
                              new PackageType();
            var service = new ServiceSearch().FetchServices(new List<ParameterColumn>(), tenantId).FirstOrDefault() ??
                          new ServiceViewSearchDto();
            var chargeCode = ProcessorVars.RegistryCache[tenantId].ChargeCodes.FirstOrDefault() ?? new ChargeCode();
            var accountBucket = ProcessorVars.RegistryCache[tenantId].AccountBuckets.FirstOrDefault() ??
                                new AccountBucket();
            var documentTag = ProcessorVars.RegistryCache[tenantId].DocumentTags.FirstOrDefault() ??
                                new DocumentTag();

            var timeList = TimeUtility.BuildTimeList();

            var loadOrder = new LoadOrder
            {
                LoadOrderNumber = "00000",
				Description = string.Empty,
                Customer = customer,
                PrefixId = default(long),
                HidePrefix = false,
                ServiceMode = ServiceMode.LessThanTruckload,
                Notes = new List<LoadOrderNote>(),
                CustomerReferences = new List<LoadOrderReference>(),
                ShipperBol = string.Empty,

                GeneralBolComments = string.Empty,
                CriticalBolComments = string.Empty,
                MiscField1 = string.Empty,
                MiscField2 = string.Empty,

				DriverName = string.Empty,
				DriverPhoneNumber = string.Empty,
				DriverTrailerNumber = string.Empty,

                AccountBucketUnitId = 0,

                EarlyDelivery = timeList[0],
                EarlyPickup = timeList[0],

                DateCreated = DateTime.Now,
                EstimatedDeliveryDate = DateTime.Now,
                DesiredPickupDate = DateTime.Now,

                Charges = new List<LoadOrderCharge>(),
                Vendors = new List<LoadOrderVendor>(),
                Stops = new List<LoadOrderLocation>(),
                Status = LoadOrderStatus.Accepted,

                User = user,
                Equipments = new List<LoadOrderEquipment>(),
                HazardousMaterial = true,
                HazardousMaterialContactEmail = "test@test.com",
                HazardousMaterialContactMobile = "111-111-1111",
                HazardousMaterialContactName = "Haz Mat. Contact",
                HazardousMaterialContactPhone = "111-222-3333",
                Items = new List<LoadOrderItem>(),
                LateDelivery = timeList[10],
                LatePickup = timeList[10],
                PurchaseOrderNumber = "PO#",
                Services = new List<LoadOrderService>(),
                ShipperReference = "Ref#",
                TenantId = tenantId,
                Mileage = 1, 
                CarrierCoordinatorId = GlobalTestInitializer.DefaultUserId,

				SalesRepAddlEntityName = string.Empty,
            };

            loadOrder.Origin = new LoadOrderLocation
            {
                LoadOrder = loadOrder,
                City = "Erie",
                Contacts = new List<LoadOrderContact>(),
                Country = country,
                Description = "Description",
                PostalCode = "16504",
                State = "Pa",
                StopOrder = 0,
                Street1 = "123 St.",
                Street2 = "Street 2",
                TenantId = tenantId,
				Direction = string.Empty,
				GeneralInfo = string.Empty,
				SpecialInstructions = string.Empty,
                AppointmentDateTime = DateTime.Now
            };

            loadOrder.Destination = new LoadOrderLocation
            {
                LoadOrder = loadOrder,
                City = "Erie",
                Contacts = new List<LoadOrderContact>(),
                Country = country,
                Description = "Description",
                PostalCode = "16504",
                State = "Pa",
                StopOrder = 2,
                Street1 = "123 St.",
                Street2 = "Street 2",
                TenantId = tenantId,
				Direction = string.Empty,
				GeneralInfo = string.Empty,
				SpecialInstructions = string.Empty,
                AppointmentDateTime = DateTime.Now
            };

            loadOrder.Stops.Add(new LoadOrderLocation
            {
                LoadOrder = loadOrder,
                City = "Erie",
                Contacts = new List<LoadOrderContact>(),
                Country = country,
                Description = "Description",
                PostalCode = "16504",
                State = "Pa",
                StopOrder = 1,
                Street1 = "123 St.",
                Street2 = "Street 2",
                TenantId = tenantId,
				Direction = string.Empty,
				GeneralInfo = string.Empty,
				SpecialInstructions = string.Empty,
                AppointmentDateTime = DateTime.Now
            });

            loadOrder.CustomerReferences.Add(new LoadOrderReference
            {
                LoadOrder = loadOrder,
                Name = "Ref name",
                Required = true,
                TenantId = tenantId,
                Value = "Ref Val"
            });
            loadOrder.Equipments.Add(new LoadOrderEquipment
            {
                LoadOrder = loadOrder,
                EquipmentType = equipment,
                TenantId = tenantId
            });

            loadOrder.Items.Add(new LoadOrderItem
            {
                LoadOrder = loadOrder,
                Comment = "Item Comm.",
                Delivery = 2,
                Description = "Item Desc.",
                FreightClass = 92.5,
                Height = 96,
                Length = 96,
                Width = 96,
                Weight = 1000,
                PackageType = packageType,
                Pickup = 0,
                PieceCount = 1,
                Quantity = 1,
                TenantId = tenantId,
                NMFCCode = string.Empty,
                HTSCode = string.Empty,
				HazardousMaterial = true,
                                
            });

            loadOrder.Services.Add(new LoadOrderService
            {
                LoadOrder = loadOrder,
                ServiceId = service.Id,
                TenantId = tenantId
            });

            loadOrder.Vendors.Add(new LoadOrderVendor
            {
                LoadOrder = loadOrder,
                Vendor = vendor,
                TenantId = tenantId,
                Primary = true,
                ProNumber = "text"
            });

            loadOrder.Charges.Add(new LoadOrderCharge
            {
                ChargeCode = chargeCode,
                Comment = string.Empty,
                Quantity = 1,
                LoadOrder = loadOrder,
                TenantId = tenantId,
                UnitBuy = 5.0000m,
                UnitSell = 10.0000m,
                UnitDiscount = 2.0000m
            });

            loadOrder.Notes.Add(new LoadOrderNote
            {
                Archived = false,
                Classified = false,
                Message = "Note Test Message",
                LoadOrder = loadOrder,
                TenantId = tenantId,
                Type = NoteType.Normal,
                User = user
            });

            loadOrder.AccountBuckets = new List<LoadOrderAccountBucket>
                                           {
                                               new LoadOrderAccountBucket
                                                   {
                                                       AccountBucketId = accountBucket.Id,
                                                       Primary = true,
                                                       LoadOrder = loadOrder,
                                                       TenantId = tenantId,
                                                   }
                                           };

            loadOrder.Documents = new List<LoadOrderDocument>
                                      {
                                          new LoadOrderDocument
                                              {
                                                  LoadOrder = loadOrder,
                                                  Description = "Test Description",
                                                  TenantId = tenantId,
                                                  DocumentTag = documentTag,
                                                  LocationPath = "Test Location Path",
                                                  Name = "Test Name"
                                              }
                                      };

            return loadOrder;
        }




        internal class LoadOrderView : ILoadOrderView
        {
            public User ActiveUser
            {
                get { return GlobalTestInitializer.ActiveUser; }
            }

            public bool RefundOrVoidMiscReceipts { get; private set; }

            public bool SaveOriginToAddressBook
        	{
				get { return false; }
        	}

        	public bool SaveDestinationToAddressBook
        	{
				get { return false; }
        	}

        	public List<Country> Countries
            {
                set { }
            }
            public List<ContactType> ContactTypes
            {
                set { }
            }
            public List<PackageType> PackageTypes
            {
                set { }
            }
            public List<ServiceViewSearchDto> Services
            {
                set { }
            }
            public List<EquipmentType> EquipmentTypes
            {
                set { }
            }
            public List<ChargeCode> ChargeCodes
            {
                set { }
            }
            public List<MileageSource> MileageSources
            {
                set { }
            }
            public List<DocumentTag> DocumentTags
            {
                set { }
            }
        	
            public List<ShipmentPriority> ShipmentPriorities
            {
                set { }
            }

            public Dictionary<int, string> DateUnits
            {
                set{ }
            }

            public Dictionary<int, string> NoteTypes
            {
                set { }
            }
            public Dictionary<int, string> ServiceModes
            {
                set { }
            }

			public event EventHandler Loading;
            public event EventHandler<ViewEventArgs<LoadOrder>> Save;
            public event EventHandler<ViewEventArgs<LoadOrder>> Delete;
            public event EventHandler<ViewEventArgs<LoadOrder>> Lock;
            public event EventHandler<ViewEventArgs<LoadOrder>> UnLock;
            public event EventHandler<ViewEventArgs<LoadOrder>> LoadAuditLog;
            public event EventHandler<ViewEventArgs<string>> CustomerSearch;
            public event EventHandler<ViewEventArgs<string>> VendorSearch;
        	public event EventHandler<ViewEventArgs<LoadOrder, Shipment>> SaveConvertToShipment;

            public void FinalizeConvertToShipment(Shipment shipment)
        	{
                DisplayMessages(new[]
                                    {
                                        ValidationMessage.Information("succesfully converted to shipment and saved")
                                    });
        	}

        	public void DisplayVendor(Vendor vendor)
            {
            }

	        public void DisplayCustomer(Customer customer, bool updateSalesRepAndResellers)
	        {
	        }

			public void LogException(Exception ex)
			{
                Console.WriteLine(ex.ToString());
			}

            public void DisplayMessages(IEnumerable<ValidationMessage> messages)
            {
                var loadOrderMessages = messages.Select(validationMessage => new ValidationMessage
                {
                    Type = validationMessage.Type,
                    Message = string.Format("Load Order {0}", validationMessage.Message)
                }).ToList();


                loadOrderMessages.PrintMessages();
                Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
            }

            public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
            {
            }

            public void FailedLock(Lock @lock)
            {
            }

            public void Set(LoadOrder loadOrder)
            {
            }

            public void Load()
            {
                var handler = new LoadOrderHandler(this);
                handler.Initialize();
            }

            public void SaveRecord(LoadOrder loadOrder)
            {
                if (Save != null)
                    Save(this, new ViewEventArgs<LoadOrder>(loadOrder));
            }

            public void DeleteRecord(LoadOrder loadOrder)
            {
                if (Delete != null)
                    Delete(this, new ViewEventArgs<LoadOrder>(loadOrder));
            }

            public void SaveRecordAndConvertToShipment(LoadOrder loadOrder, Shipment shipment)
            {
                if (SaveConvertToShipment != null)
                    SaveConvertToShipment(this, new ViewEventArgs<LoadOrder, Shipment>(loadOrder, shipment));
            }

            public void LockRecord(LoadOrder loadOrder)
            {
                if (Lock != null)
                    Lock(this, new ViewEventArgs<LoadOrder>(loadOrder));
            }

            public void UnLockRecord(LoadOrder loadOrder)
            {
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<LoadOrder>(loadOrder));
            }
        }

        internal class ShipmentView : IShipmentView
        {
            public User ActiveUser
            {
                get { return GlobalTestInitializer.ActiveUser; }
            }

            public bool RefundOrVoidMiscReceipts { get; private set; }

            public bool SaveOriginToAddressBook
            {
                get { return false; }
            }

            public bool SaveDestinationToAddressBook
            {
                get { return false; }
            }

            public List<Country> Countries
            {
                set { }
            }
            public List<ContactType> ContactTypes
            {
                set { }
            }
            public List<PackageType> PackageTypes
            {
                set { }
            }
            public List<ServiceViewSearchDto> Services
            {
                set { }
            }
            public List<EquipmentType> EquipmentTypes
            {
                set { }
            }
            public List<ChargeCode> ChargeCodes
            {
                set { }
            }
            public List<MileageSource> MileageSources
            {
                set { }
            }
            public List<FailureCode> FailureCodes { set { } }
            public List<DocumentTag> DocumentTags { set { } }
            public List<ShipmentPriority> ShipmentPriorities { set { } }
            public Dictionary<int, string> DateUnits { set; private get; }
            public Dictionary<int, string> InDisputeReasons { set; private get; }

            public List<Asset> Assets
            {
                set { }
            }

            public Dictionary<int, string> NoteTypes
            {
                set { }
            }
            public Dictionary<int, string> ServiceModes
            {
                set { }
            }
            public Dictionary<int, string> MileageEngines
            {
                set { }
            }

            public event EventHandler Loading;
            public event EventHandler<ViewEventArgs<Shipment>> Save;
            public event EventHandler<ViewEventArgs<Shipment>> Delete;
            public event EventHandler<ViewEventArgs<Shipment>> Lock;
            public event EventHandler<ViewEventArgs<Shipment>> UnLock;
            public event EventHandler<ViewEventArgs<Shipment>> LoadAuditLog;
            public event EventHandler<ViewEventArgs<string>> CustomerSearch;
            public event EventHandler<ViewEventArgs<string>> VendorSearch;
            public event EventHandler<ViewEventArgs<ShoppedRate>> SaveShoppedRate;
	        public event EventHandler<ViewEventArgs<SMC3TrackRequestResponse>> SaveSmc3TrackRequestResponse;
            public event EventHandler<ViewEventArgs<Project44TrackingResponse>> SaveProject44TrackRequestResponse;

            public void DisplayVendor(Vendor vendor)
            {
            }

	        public void DisplayCustomer(Customer customer, bool updateSalesRepAndResellers)
	        {
	        }

	        public void LogException(Exception ex)
            {
            }

            public void DisplayMessages(IEnumerable<ValidationMessage> messages)
            {
                var shipmentMessages = messages.Select(validationMessage => new ValidationMessage
                                {
                                    Type = validationMessage.Type,
                                    Message = string.Format("Converted Shipment {0}",validationMessage.Message)
                                }).ToList();

                shipmentMessages.PrintMessages();
            }

            public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
            {
            }

            public void FailedLock(Lock @lock)
            {
            }

            public void Set(Shipment shipment)
            {
            }

            public void Set(ShoppedRate shoppedRate)
            {
            }

            public void Load()
            {
                var handler = new ShipmentHandler(this);
                handler.Initialize();
            }

            public void SaveRecord(Shipment shipment)
            {
                if (Save != null)
                    Save(this, new ViewEventArgs<Shipment>(shipment));
            }

            public void DeleteRecord(Shipment shipment)
            {
                if (Delete != null)
                    Delete(this, new ViewEventArgs<Shipment>(shipment));
            }

            public void LockRecord(Shipment shipment)
            {
                if (Lock != null)
                    Lock(this, new ViewEventArgs<Shipment>(shipment));
            }

            public void UnLockRecord(Shipment shipment)
            {
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<Shipment>(shipment));
            }
        }
    }
}
