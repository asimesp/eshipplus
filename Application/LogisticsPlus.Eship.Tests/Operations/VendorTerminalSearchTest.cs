﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
    [TestFixture]
    public class VendorTerminalSearchTest
    {
        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

        [Test]
        public void CanFetchVendorTerminals()
        {
            var criteria = new VendorTerminalSearchCriteria
                               {
                                   VendorId = GlobalTestInitializer.DefaultVendorWithTerminalId
                               };

            var field = OperationsSearchFields.VendorTerminals.FirstOrDefault(f => f.Name == "Name");
            if (field == null)
            {
                Console.WriteLine("OperationsSearchFields.VendorTerminals no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }

            var column = field.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            criteria.Parameters.Add(column);

            var countryColumn = OperationsSearchFields.NonAliasedCountryCode.ToParameterColumn();
            countryColumn.DefaultValue = SearchUtilities.WildCard;
            countryColumn.Operator = Operator.Contains;
            criteria.Parameters.Add(countryColumn);

            var results = new VendorTerminalSearch().FetchVendorTerminals(criteria, GlobalTestInitializer.DefaultTenantId);

            Assert.IsTrue(results.Any());
        }
    }
}
