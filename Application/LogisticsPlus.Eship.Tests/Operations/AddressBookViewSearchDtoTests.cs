﻿using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
	[TestFixture]
	public class AddressBookViewSearchDtoTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
        public void CanFetchAddressBooksForFinderWithParamterColumns()
        {
            var criteria = new AddressBookViewSearchCriteria
                               {
                                   UserId = GlobalTestInitializer.DefaultUserId,
                                   CustomerId = default(long),
                               };

            var addressBooks = new AddressBookViewSearchDto()
                .FetchAddressBookDtos(GlobalTestInitializer.DefaultTenantId, criteria);
            Assert.IsTrue(addressBooks.Count > 0);
        }
	}
}
