﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
	class TruckloadBidValidatorTests
	{
		private TruckloadBidValidator _validator;
		private TruckloadBid _truckLoadBid;

		[SetUp]
		public void Initialize()
		{
			var tenantId = GlobalTestInitializer.DefaultTenantId;

			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new TruckloadBidValidator();

			_truckLoadBid = new TruckloadBid
			{

				LoadOrderId = GlobalTestInitializer.DefaultLoadOrderId,
				TLLaneId = GlobalTestInitializer.DefaulCustomerTLTenderingProfileLaneId,
				TLTenderingProfileId = GlobalTestInitializer.DefaulCustomerTLTenderingProfileId,
				FirstPreferredVendorId = 218,
				SecondPreferredVendorId = 39,
				ThirdPreferredVendorId = 1,
				TimeLoadTendered1 = DateTime.Now,
				ExpirationTime1 = DateTime.Now,
				TimeLoadTendered2 = DateUtility.SystemEarliestDateTime,
				ExpirationTime2 = DateUtility.SystemEarliestDateTime,
				TimeLoadTendered3 = DateUtility.SystemEarliestDateTime,
				ExpirationTime3 = DateUtility.SystemEarliestDateTime,
				TenantId = tenantId,
				UserId = GlobalTestInitializer.DefaultUserId
			};

		}

		[Test]
		public void IsValidTest()
		{
			_truckLoadBid.LoadOrder.Status = LoadOrderStatus.Offered;
			_truckLoadBid.LoadOrder.ServiceMode = ServiceMode.Truckload;
			Assert.IsTrue(_validator.IsValid(_truckLoadBid));
		}

		[Test]
		public void HasRequiredDataTest()
		{
			_truckLoadBid.LoadOrder.Status = LoadOrderStatus.Offered;
			_truckLoadBid.LoadOrder.ServiceMode = ServiceMode.Truckload;
			var hasAllRequiredData = _validator.HasAllRequiredData(_truckLoadBid);
			if (!hasAllRequiredData) _validator.Messages.PrintMessages();
			Assert.IsTrue(hasAllRequiredData);
		}
	}
}
