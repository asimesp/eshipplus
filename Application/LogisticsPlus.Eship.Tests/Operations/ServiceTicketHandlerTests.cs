﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.Processor.Views.Administration;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
	[TestFixture]
	public class ServiceTicketHandlerTests
	{
		private ServiceTicketView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new ServiceTicketView();
			_view.Load();
		}

		[Test]
		public void CanSaveOrUpdate()
		{
			var criteria = new ServiceTicketViewSearchCriteria
							{
								ActiveUserId = GlobalTestInitializer.DefaultUserId
							};
			var all = new ServiceTicketSearch().FetchServiceTicketDashboardDtos(criteria, GlobalTestInitializer.DefaultTenantId);

			if (all.Count == 0) return;
			var serviceTicket = new ServiceTicket(all[0].Id);
			serviceTicket.LoadCollections();
			_view.LockRecord(serviceTicket);
			serviceTicket.TakeSnapShot();
			_view.SaveRecord(serviceTicket);
			_view.UnLockRecord(serviceTicket);
		}

		[Test]
		public void CanSaveAndDelete()
		{
			var serviceTicket = GetServiceTicket();
			_view.SaveRecord(serviceTicket);
			Assert.IsTrue(serviceTicket.Id != default(long));
			_view.DeleteRecord(serviceTicket);
			Assert.IsTrue(!new ServiceTicket(serviceTicket.Id, false).KeyLoaded);

		}

		private static ServiceTicket GetServiceTicket()
		{
			var tenantId = GlobalTestInitializer.DefaultTenantId;
			var customer = new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), tenantId).FirstOrDefault() ?? new Customer();
			var vendor = new VendorSearch().FetchVendors(new List<ParameterColumn>(), tenantId).FirstOrDefault() ?? new Vendor();

            var criteria = new UserSearchCriteria
            {
                Parameters = new List<ParameterColumn>()
            };
			var userDto = new UserSearch().FetchUsers(criteria, tenantId).FirstOrDefault();
		    var user = userDto == null ? new User() : new User(userDto.Id);
            var equipment = ProcessorVars.RegistryCache[tenantId].EquipmentTypes.FirstOrDefault() ??
							new EquipmentType();
			var service = new ServiceSearch().FetchServices(new List<ParameterColumn>(), tenantId).FirstOrDefault() ??
						  new ServiceViewSearchDto();
            var chargeCode = ProcessorVars.RegistryCache[tenantId].ChargeCodes.FirstOrDefault() ?? new ChargeCode();
            var accountBucket = ProcessorVars.RegistryCache[tenantId].AccountBuckets.FirstOrDefault() ??
								new AccountBucket();
            var assets = ProcessorVars.RegistryCache[tenantId].Assets;
			var docTag = ProcessorVars.RegistryCache[tenantId].DocumentTags.FirstOrDefault() ?? new DocumentTag();

			var serviceTicket = new ServiceTicket
							{
								ServiceTicketNumber = "00000",
								Customer = customer,
								PrefixId = default(long),
								AccountBucket = accountBucket,
								HidePrefix = false,
								DateCreated = DateTime.Now,
								TicketDate = DateTime.Now,
								Status = ServiceTicketStatus.Open,
								User = user,
								TenantId = tenantId,

								ExternalReference1 = string.Empty,
								ExternalReference2 = string.Empty,

								Equipments = new List<ServiceTicketEquipment>(),
								Items = new List<ServiceTicketItem>(),
								Services = new List<ServiceTicketService>(),
								Vendors = new List<ServiceTicketVendor>(),
								Charges = new List<ServiceTicketCharge>(),
								Notes = new List<ServiceTicketNote>(),
								Assets = new List<ServiceTicketAsset>(),

								SalesRepAddlEntityName = string.Empty
							};


			serviceTicket.Equipments.Add(new ServiceTicketEquipment
			{
				ServiceTicket = serviceTicket,
				EquipmentType = equipment,
				TenantId = tenantId
			});

			serviceTicket.Items.Add(new ServiceTicketItem
			{
				ServiceTicket = serviceTicket,
				Comment = "Item Comm.",
				Description = "Item Desc.",
				TenantId = tenantId
			});

			serviceTicket.Services.Add(new ServiceTicketService
			{
				ServiceTicket = serviceTicket,
				ServiceId = service.Id,
				TenantId = tenantId
			});

			serviceTicket.Vendors.Add(new ServiceTicketVendor
			{
				ServiceTicket = serviceTicket,
				Vendor = vendor,
				TenantId = tenantId,
				Primary = true,
			});

			serviceTicket.Charges.Add(new ServiceTicketCharge
			{
				ChargeCode = chargeCode,
				Comment = string.Empty,
				Quantity = 1,
				ServiceTicket = serviceTicket,
				TenantId = tenantId,
				UnitBuy = 5.0000m,
				UnitSell = 10.0000m,
				UnitDiscount = 2.0000m
			});

			serviceTicket.Notes.Add(new ServiceTicketNote
			{
				Archived = false,
				Classified = false,
				Message = "Note Test Message",
				ServiceTicket = serviceTicket,
				TenantId = tenantId,
				Type = NoteType.Normal,
				User = user
			});

			serviceTicket.Documents.Add(new ServiceTicketDocument
			{
				TenantId = tenantId,
				ServiceTicket = serviceTicket,
				Name = "Document",
				Description = "Description",
				DocumentTagId = docTag.Id,
				LocationPath = "test"
			});

            var assetTractor = assets.FirstOrDefault(c => c.AssetType == AssetType.IndependentTractor || c.AssetType == AssetType.OwnerOpTractor || c.AssetType == AssetType.CompanyTractor);
            var assetTrailer = assets.FirstOrDefault(c => c.AssetType == AssetType.Trailer);
			serviceTicket.Assets.Add(new ServiceTicketAsset
			                         	{
			                         		ServiceTicket = serviceTicket,
			                         		DriverAssetId = assets.First(c => c.AssetType == AssetType.Driver).Id,
			                         		TractorAssetId = assetTractor == null ? default(long) : assetTractor.Id,
			                         		TrailerAssetId = assetTrailer == null ? default(long) : assetTrailer.Id,
			                         		MilesRun = 1m,
			                         		TenantId = tenantId,
			                         		Primary = true
			                         	});

			return serviceTicket;
		}




		internal class ServiceTicketView : IServiceTicketView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public List<ChargeCode> ChargeCodes
			{
				set { }
			}

			public List<ServiceViewSearchDto> Services
			{
				set { }
			}

		    public List<Asset> Assets
		    {
		        set { }
		    }

		    public List<EquipmentType> EquipmentTypes
			{
				set { }
			}
			public Dictionary<int, string> NoteTypes
			{
				set { }
			}
			public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<ServiceTicket>> Save;
			public event EventHandler<ViewEventArgs<ServiceTicket>> Delete;
			public event EventHandler<ViewEventArgs<ServiceTicket>> Lock;
			public event EventHandler<ViewEventArgs<ServiceTicket>> UnLock;
			public event EventHandler<ViewEventArgs<ServiceTicket>> LoadAuditLog;
			public event EventHandler<ViewEventArgs<string>> CustomerSearch;
			public event EventHandler<ViewEventArgs<string>> VendorSearch;
			public event EventHandler<ViewEventArgs<string>> PrefixSearch;
			public event EventHandler<ViewEventArgs<string>> AccountBucketSearch;
			public event EventHandler<ViewEventArgs<List<ServiceTicket>>> BatchImport;

			public void DisplayPrefix(Prefix prefix)
			{

			}

			public void DisplayAccountBucket(AccountBucket accountBucket)
			{

			}

			public void DisplayVendor(Vendor vendor)
			{
			}

		    public void LogException(Exception ex)
		    {
		            
		    }

			public void DisplayCustomer(Customer customer, bool updateSalesRepAndResellers)
			{
			}

			public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
			{
			}

			public void FailedLock(Lock @lock)
			{
			}

			public void Set(ServiceTicket serviceTicket)
			{
			}

			public void Load()
			{
				var handler = new ServiceTicketHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(ServiceTicket serviceTicket)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<ServiceTicket>(serviceTicket));
			}

			public void DeleteRecord(ServiceTicket serviceTicket)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<ServiceTicket>(serviceTicket));
			}

			public void LockRecord(ServiceTicket serviceTicket)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<ServiceTicket>(serviceTicket));
			}

			public void UnLockRecord(ServiceTicket serviceTicket)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<ServiceTicket>(serviceTicket));
			}
		}
	}
}
