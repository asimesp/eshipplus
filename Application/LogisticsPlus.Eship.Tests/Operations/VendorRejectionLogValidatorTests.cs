﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
	[TestFixture]
	public class VendorRejectionLogValidatorTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanValidateRejectionLog()
		{
			var allVendors = new VendorSearch().FetchVendors(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			var vendor = allVendors.FirstOrDefault();
			if (vendor == null) return;
			var log = new VendorRejectionLog
			          	{
			          		UserId = GlobalTestInitializer.DefaultUserId,
			          		VendorId = vendor.Id,
			          		TenantId = GlobalTestInitializer.DefaultTenantId,
							CustomerId = GlobalTestInitializer.DefaultCustomerId,
			          		OriginPostalCode = string.Empty,
			          		OriginCity = string.Empty,
			          		OriginState = string.Empty,
			          		OriginCountryCode = string.Empty,
			          		DestinationPostalCode = string.Empty,
			          		DestinationCountryCode = string.Empty,
			          		DestinationCity = string.Empty,
			          		DestinationState = string.Empty,
			          		ShipmentNumber = "12345",
			          		ShipmentDateCreated = DateTime.Now,
			          		DateCreated = DateTime.Now,
							ServiceMode = ServiceMode.NotApplicable
			          	};
			Assert.IsTrue(new VendorRejectionLogValidator().IsValid(log));
		}
	}
}
