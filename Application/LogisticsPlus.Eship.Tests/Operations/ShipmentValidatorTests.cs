﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.Processor.Views.Administration;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Operations
{
	[TestFixture]
	public class ShipmentValidatorTests
	{
		private ShipmentValidator _validator;
		private Shipment _shipment;
		private ShipmentViewSearchCriteria _criteria;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new ShipmentValidator();

			if (_shipment != null) return;

			var tenantId = GlobalTestInitializer.DefaultTenantId;
			var customer = new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), tenantId).FirstOrDefault() ?? new Customer();
			var vendor = new VendorSearch().FetchVendors(new List<ParameterColumn>(), tenantId).FirstOrDefault() ?? new Vendor();
            var country = ProcessorVars.RegistryCache.Countries.Values.FirstOrDefault() ?? new Country();

            var criteria = new UserSearchCriteria
            {
                Parameters = new List<ParameterColumn>()
            };
			var userDto = new UserSearch().FetchUsers(criteria, tenantId).FirstOrDefault();
            var user = userDto == null ? new User() : new User(userDto.Id);
            var equipment = ProcessorVars.RegistryCache[tenantId].EquipmentTypes.FirstOrDefault() ?? new EquipmentType();
            var packageType = ProcessorVars.RegistryCache[tenantId].PackageTypes.FirstOrDefault() ?? new PackageType();
			var service = new ServiceSearch().FetchServices(new List<ParameterColumn>(), tenantId).FirstOrDefault() ?? new ServiceViewSearchDto();
            var chargeCode = ProcessorVars.RegistryCache[tenantId].ChargeCodes.FirstOrDefault() ?? new ChargeCode();
            var assets = ProcessorVars.RegistryCache[tenantId].Assets;
            var docTag = ProcessorVars.RegistryCache[tenantId].DocumentTags.FirstOrDefault() ?? new DocumentTag();
            var failureCode = ProcessorVars.RegistryCache[tenantId].FailureCodes.FirstOrDefault() ?? new FailureCode();
            var accountBucket = ProcessorVars.RegistryCache[tenantId].AccountBuckets.FirstOrDefault() ?? new AccountBucket();
            
			var timeList = TimeUtility.BuildTimeList();
			_shipment = new Shipment(10000)
								{
									ShipmentNumber = "00000",
									Customer = customer,
									PrefixId = default(long),
									HidePrefix = false,
									ServiceMode = ServiceMode.LessThanTruckload,
									Notes = new List<ShipmentNote>(),
									CustomerReferences = new List<ShipmentReference>(),
									DesiredPickupDate = DateTime.Now,
									ActualPickupDate = DateTime.Now,
									ActualDeliveryDate = DateTime.Now,
									AccountBuckets = new List<ShipmentAccountBucket>(),
									EarlyDelivery = timeList[0],
									EarlyPickup = timeList[0],
									EstimatedDeliveryDate = DateTime.Now,
									Charges = new List<ShipmentCharge>(),
									Vendors = new List<ShipmentVendor>(),
									Stops = new List<ShipmentLocation>(),
									Status = ShipmentStatus.InTransit,
									DateCreated = DateTime.Now,
									User = user,
									Equipments = new List<ShipmentEquipment>(),
									HazardousMaterial = true,
									HazardousMaterialContactEmail = "test@test.com",
									HazardousMaterialContactMobile = "111-111-1111",
									HazardousMaterialContactName = "Haz Mat. Contact",
									HazardousMaterialContactPhone = "111-222-3333",
									Items = new List<ShipmentItem>(),
									LateDelivery = timeList[10],
									LatePickup = timeList[10],
									PurchaseOrderNumber = "PO#",
									Services = new List<ShipmentService>(),
									ShipperReference = "Ref#",
									TenantId = tenantId,
									AutoRatingAccessorials = new List<ShipmentAutoRatingAccessorial>(),
									VendorRatingOverrideAddress = string.Empty,
									Mileage = 1,
                                    CarrierCoordinatorId = GlobalTestInitializer.DefaultUserId,
                                    ShipmentCoordinatorId = GlobalTestInitializer.DefaultUserId,
                                    GuaranteedDeliveryServiceTime = string.Empty,
                                    IsGuaranteedDeliveryService = false,
                                    GuaranteedDeliveryServiceRateType = RateType.Flat,
		                            GuaranteedDeliveryServiceRate = default(decimal),
                                    InDispute = false,
                                    InDisputeReason = InDisputeReason.NotApplicable
								};
			_shipment.Origin = new ShipmentLocation(1)
								{
									Shipment = _shipment,
									City = "Erie",
									Contacts = new List<ShipmentContact>(),
									Country = country,
									Description = "Description",
									PostalCode = "16504",
									State = "Pa",
									StopOrder = 0,
									Street1 = "123 St.",
									Street2 = "Street 2",
									TenantId = tenantId,
									AppointmentDateTime = DateUtility.SystemEarliestDateTime
								};
			_shipment.Destination = new ShipmentLocation(1)
								{
									Shipment = _shipment,
									City = "Erie",
									Contacts = new List<ShipmentContact>(),
									Country = country,
									Description = "Description",
									PostalCode = "16504",
									State = "Pa",
									StopOrder = 2,
									Street1 = "123 St.",
									Street2 = "Street 2",
									TenantId = tenantId,
									AppointmentDateTime = DateUtility.SystemEarliestDateTime
								};
			_shipment.Stops.Add(new ShipmentLocation(1)
								{
									Shipment = _shipment,
									City = "Erie",
									Contacts = new List<ShipmentContact>(),
									Country = country,
									Description = "Description",
									PostalCode = "16504",
									State = "Pa",
									StopOrder = 1,
									Street1 = "123 St.",
									Street2 = "Street 2",
									TenantId = tenantId,
									AppointmentDateTime = DateUtility.SystemEarliestDateTime
								});
			_shipment.OriginTerminal = new LTLTerminalInfo
										 {
											 Shipment = _shipment,
											 TenantId = tenantId,
											 Code = "code",
											 Name = DateTime.Now.ToShortDateString(),
											 Phone = DateTime.Now.ToShortDateString(),
											 TollFree = DateTime.Now.ToShortDateString(),
											 Fax = DateTime.Now.ToShortDateString(),
											 Email = "test@test.com",
											 ContactName = DateTime.Now.ToShortDateString(),
											 ContactTitle = "president",
											 Street1 = DateTime.Now.ToShortDateString(),
											 Street2 = DateTime.Now.ToShortDateString(),
											 City = DateTime.Now.ToShortDateString(),
											 State = DateTime.Now.ToShortDateString(),
											 Country = country,
											 PostalCode = "123456789"
										 };
			_shipment.DestinationTerminal = new LTLTerminalInfo
										{
											Shipment = _shipment,
											TenantId = tenantId,
											Code = "code",
											Name = DateTime.Now.ToShortDateString(),
											Phone = DateTime.Now.ToShortDateString(),
											TollFree = DateTime.Now.ToShortDateString(),
											Fax = DateTime.Now.ToShortDateString(),
											Email = "test@test.com",
											ContactName = DateTime.Now.ToShortDateString(),
											ContactTitle = "president",
											Street1 = DateTime.Now.ToShortDateString(),
											Street2 = DateTime.Now.ToShortDateString(),
											City = DateTime.Now.ToShortDateString(),
											State = DateTime.Now.ToShortDateString(),
											Country = country,
											PostalCode = "123456789"
										};
			_shipment.AccountBuckets.Add(new ShipmentAccountBucket { TenantId = tenantId, Shipment = _shipment, AccountBucket = accountBucket, Primary = true });
			_shipment.CustomerReferences.Add(new ShipmentReference
								{
									Shipment = _shipment,
									Name = "Ref name",
									Required = true,
									TenantId = tenantId,
									Value = "Ref Val"
								});
			_shipment.Equipments.Add(new ShipmentEquipment { Shipment = _shipment, EquipmentType = equipment, TenantId = tenantId });
			_shipment.Items.Add(new ShipmentItem
								{
									Shipment = _shipment,
									Comment = "Item Comm.",
									Delivery = 2,
									Description = "Item Desc.",
									RatedFreightClass = 92.5,
									ActualFreightClass = 92.5,
									ActualHeight = 96,
									ActualLength = 96,
									ActualWidth = 96,
									ActualWeight = 1000,
									PackageType = packageType,
									Pickup = 0,
									PieceCount = 1,
									Quantity = 1,
									TenantId = tenantId,
									HazardousMaterial = true,
								});
			_shipment.Services.Add(new ShipmentService { Shipment = _shipment, ServiceId = service.Id, TenantId = tenantId });
			_shipment.Vendors.Add(new ShipmentVendor
								{
									Shipment = _shipment,
									Vendor = vendor,
									TenantId = tenantId,
									Primary = true,
									FailureCode = failureCode,
									FailureComments = "Test"
								});
			_shipment.Charges.Add(new ShipmentCharge
								{
									ChargeCode = chargeCode,
									Comment = string.Empty,
									Quantity = 1,
									Shipment = _shipment,
									TenantId = tenantId,
									UnitBuy = 5.0000m,
									UnitSell = 10.0000m,
									UnitDiscount = 2.0000m
								});
			_shipment.Notes.Add(new ShipmentNote
								{
									Archived = false,
									Classified = false,
									Message = "Note Test Message",
									Shipment = _shipment,
									TenantId = tenantId,
									Type = NoteType.Normal,
									User = user
								});

			var assetTractor = assets.FirstOrDefault(c => c.AssetType == AssetType.IndependentTractor || c.AssetType == AssetType.OwnerOpTractor || c.AssetType == AssetType.CompanyTractor);
			var assetTrailer = assets.FirstOrDefault(c => c.AssetType == AssetType.Trailer);
			_shipment.Assets.Add(new ShipmentAsset
			                     	{
			                     		Shipment = _shipment,
			                     		DriverAssetId = assets.First(c => c.AssetType == AssetType.Driver).Id,
			                     		TractorAssetId = assetTractor == null ? default(long) : assetTractor.Id,
			                     		TrailerAssetId = assetTrailer == null ? default(long) : assetTrailer.Id,
			                     		MilesRun = 1m,
			                     		TenantId = tenantId,
			                     		Primary = true
			                     	});

			_shipment.Documents.Add(new ShipmentDocument
								{
									TenantId = tenantId,
									Shipment = _shipment,
									Name = "Document",
									Description = "Description",
									DocumentTagId = docTag.Id,
									LocationPath = "test"
								});

			_shipment.QuickPayOptions.Add(new ShipmentQuickPayOption
								{
									TenantId = tenantId,
									Shipment = _shipment,
									Description = "Test",
									DiscountPercent = 99,
								});
			_shipment.AutoRatingAccessorials.Add(new ShipmentAutoRatingAccessorial
													{
														BuyCeiling = 500,
														BuyFloor = 100,
														SellCeiling = 1000,
														SellFloor = 200,
														MarkupPercent = 10,
														MarkupValue = 50,
														Rate = 50,
														RateType = RateType.Flat,
														Shipment = _shipment,
														TenantId = tenantId,
														ServiceDescription = "My Description",
														UseLowerSell = false,
													}
				);
			_shipment.CheckCalls.Add(new CheckCall
										 {
											 TenantId = GlobalTestInitializer.DefaultTenantId,
											 Shipment = _shipment,
											 User = _shipment.User,
											 CallDate = DateTime.Now,
											 EventDate = DateTime.Now,
											 CallNotes = DateTime.Now.ToShortDateString(),
											 EdiStatusCode = "TE"
										 });

			_criteria = new ShipmentViewSearchCriteria { ActiveUserId = GlobalTestInitializer.DefaultUserId, };
		}

		[Test]
		public void IsValidTest()
		{
			Assert.IsTrue(_validator.IsValid(_shipment));
		}

		[Test]
		public void HasRequiredDataTest()
		{
			var hasAllRequiredData = _validator.HasAllRequiredData(_shipment);
			if (!hasAllRequiredData) _validator.Messages.PrintMessages();
			Assert.IsTrue(hasAllRequiredData);
		}

		[Test]
		public void DuplicateShipmentNumberTest()
		{
			var all = new ShipmentSearch().FetchShipmentsDashboardDto(_criteria, GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var number = _shipment.ShipmentNumber;
			_shipment.ShipmentNumber = all[0].ShipmentNumber;
			Assert.IsTrue(_validator.DuplicateShipmentNumber(_shipment));
			_shipment.ShipmentNumber = number;
		}

		[Test]
		public void DuplicateShipmentLoadNumberTest()
		{
			var shipment = new ShipmentSearch().FetchShipmentByShipmentNumber(GlobalTestInitializer.DefaultLoadNumber, GlobalTestInitializer.DefaultTenantId);
			Assert.IsNotNull(shipment);
			Assert.IsTrue(_validator.DuplicateShipmentNumber(new Shipment {ShipmentNumber = shipment.ShipmentNumber, TenantId = shipment.TenantId}));
		}

		[Test]
		public void CanDeleteTest()
		{
			_validator.Messages.Clear();
			_validator.CanDeleteShipment(_shipment);
			_validator.Messages.PrintMessages();
		}

		[Test]
		public void DuplicateShipmentServiceTest()
		{
			var service = new ShipmentService();

			Assert.IsFalse(_validator.ShipmentServiceExists(service));
			var all = new ShipmentSearch().FetchShipmentsDashboardDto(_criteria, GlobalTestInitializer.DefaultTenantId)
				.Select(q => new Shipment(q.Id))
				.Where(c => c.Services.Count > 0)
				.ToList();
			if (all.Count == 0) return;
			service = all[0].Services[0];
			Assert.IsTrue(_validator.ShipmentServiceExists(service));
		}

		[Test]
		public void DuplicateShipmentEquipmentTest()
		{
			var equipment = new ShipmentEquipment();

			Assert.IsFalse(_validator.ShipmentEquipmentExists(equipment));
			var all = new ShipmentSearch().FetchShipmentsDashboardDto(_criteria, GlobalTestInitializer.DefaultTenantId)
				.Select(q => new Shipment(q.Id))
				.Where(c => c.Equipments.Count > 0)
				.ToList();
			if (all.Count == 0) return;
			equipment = all[0].Equipments[0];
			Assert.IsTrue(_validator.ShipmentEquipmentExists(equipment));
		}
	}
}
