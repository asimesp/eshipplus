﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Ionic.Zlib;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Extern.Ws;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Reports;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.Processor.Views.Registry;
using LogisticsPlus.Eship.Smc;
using LogisticsPlus.Eship.Smc.Eva;
using LogisticsPlus.Eship.Tests.ExcelLibManipulations;
using LogisticsPlus.Eship.WebApplication;
using LogisticsPlus.Eship.WebApplication.Utilities;
using NUnit.Framework;
using Newtonsoft.Json;
using OfficeOpenXml;
using CompressionLevel = Ionic.Zlib.CompressionLevel;

namespace LogisticsPlus.Eship.Tests
{
    [TestFixture]
    [Ignore("Misc Test. Run on demand only")]
    public class FtpClientTest
    {
        private FtpClient client = null;



        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();

        }


        [Test]
        public void CanConnectUsingFTPS()
        {
            client = new FtpClient(GlobalTestInitializer.DefaultHost, GlobalTestInitializer.DefaultFtpUsername,
                GlobalTestInitializer.DefaultFtpPassword);
            client.IsSslftp = true;
            var fileContent = new byte[] {0x20, 0x20, 0x30, 0x20, 0x10, 0x10};//Try chnaging the fileContent if test fails
            var filename = DateTime.Now.ToString();
            Assert.IsNotNull(client.UploadFile(fileContent, filename));
        }

        [Test]
        public void CanConnectUsingSFTP()
        {
            client = new FtpClient(GlobalTestInitializer.DefaultSftpHost, GlobalTestInitializer.DefaultSFtpUsername,
                GlobalTestInitializer.DefaultSFtpPassword);
            client.IsSSHFtp = true;

            var listDirectory = client.ListDirectory(true);
            Assert.IsTrue(listDirectory.Any());
           
        }
    }
}

