﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.MacroPoint;
using LogisticsPlus.Eship.Processor;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.MacroPoint
{
	[TestFixture]
	[Ignore("Misc Test. Run on demand only")]
	public class MacroPointSamplesTests
	{
		const string FutureUse = "Future Use";

		[SetUp]
		public void Setup()
		{
			GlobalTestInitializer.Initialize();
		}

		[Test]
		public void SerializeMessage()
		{
			var d = new Message
			{
				OrderID = "12345",
				Text = "Message Text"
			};
			Console.WriteLine(d.ToXml(true, true));
		}

		[Test]
		public void SerializeMessageResult()
		{
			var d = new MessageResult
			{
				Errors = new List<Error>
					{
						new Error
							{
								Code = ErrorCodes.UnexpectedErrorOccurredAdministratorNotified.ToInt().GetString(),
								Message = "Error Message Text"
							}
					}
			};
			Console.WriteLine(d.ToXml(true, true));
		}

		[Test]
		public void SerializeOrder()
		{
			var d = new Order
				{
					Number = new Number
						{
							Type = NumberType.Mobile.GetString(),
							Value = "9999999999"
						},
					TrackStartDateTime = DateTime.Now.FormattedShortDateAlt() + " " + DateTime.Now.ToString("HH") + ":00 ET",
					Notifications = new List<Notification>
						{
							new Notification
								{
									IDNumber = "12345",
									PartnerMPID = "Macro Point Partner ID",
									TrackDurationInHours = TrackDuration.OneWeek.ToInt().GetString(),
									TrackIntervalInMinutes = TrackInterval.FourHours.ToInt().GetString(),
								}
						},
					Carrier = new Carrier
						{
							Name = FutureUse,
							Dispatcher = new Dispatcher
								{
									Name = FutureUse,
									Email = FutureUse,
									Phone = FutureUse
								}
						},
					TripSheet = new TripSheet
						{
							Stops = new List<Stop>
								{
									new Stop
										{
											Address = new Address
												{
													City = FutureUse,
													Line1 = FutureUse,
													Line2 = FutureUse,
													PostalCode = FutureUse,
													StateOrProvince = FutureUse,
												},
											Name = FutureUse,
											EndDateTime = FutureUse,
											Latitude = FutureUse,
											Longitude = FutureUse,
											StartDateTime = FutureUse,
											StopID = FutureUse,
											StopType = FutureUse,
										}
								}
						}
				};
			Console.WriteLine(d.ToXml(true, true));
		}

		[Test]
		public void SerializeOrderResult()
		{
			var d = new OrderResult
			{
				OrderID = "12345",
				Status = OrderResultStatus.Success.GetString(),
				Errors = new List<Error>
					{
						new Error
							{
								Code = ErrorCodes.UnexpectedErrorOccurredAdministratorNotified.ToInt().GetString(),
								Message = "Error Message Text"
							}
					},

			};
			Console.WriteLine(d.ToXml(true, true));
		}

        [Test]
        public void SerializeUpdateLocationResult()
        {
            var d = new UpdateLocationResult
            {
                Errors = new List<Error>
					{
						new Error
							{
								Code = ErrorCodes.UnexpectedErrorOccurredAdministratorNotified.ToInt().GetString(),
								Message = "Error Message Text"
							}
					},

            };
            Console.WriteLine(d.ToXml(false, true));
        }

		[Test]
		public void SerializeAuthencation()
		{
			var auth = new MacroPointSettings
				{
					UserId = string.Empty,
					Password = string.Empty,
					Key = string.Empty,
					PriceBreaks = new List<TrackingPriceBreak>
						{
							new TrackingPriceBreak{Category = "Critical", Price = "5", TrackDurationHours = TrackDuration.OneDay.ToInt().GetString(), TrackInternvalMinutes = TrackInterval.OneHour.ToInt().GetString()},
							new TrackingPriceBreak{Category = "Critical", Price = "10", TrackDurationHours = TrackDuration.TwoDays.ToInt().GetString(), TrackInternvalMinutes = TrackInterval.OneHour.ToInt().GetString()},
							new TrackingPriceBreak{Category = "Critical", Price = "15", TrackDurationHours = TrackDuration.ThreeDays.ToInt().GetString(), TrackInternvalMinutes = TrackInterval.OneHour.ToInt().GetString()},
							new TrackingPriceBreak{Category = "Critical", Price = "20", TrackDurationHours = TrackDuration.FourDays.ToInt().GetString(), TrackInternvalMinutes = TrackInterval.OneHour.ToInt().GetString()},
							new TrackingPriceBreak{Category = "Critical", Price = "25", TrackDurationHours = TrackDuration.FiveDays.ToInt().GetString(), TrackInternvalMinutes = TrackInterval.OneHour.ToInt().GetString()},
							new TrackingPriceBreak{Category = "Critical", Price = "30", TrackDurationHours = TrackDuration.SixDays.ToInt().GetString(), TrackInternvalMinutes = TrackInterval.OneHour.ToInt().GetString()},
							new TrackingPriceBreak{Category = "Critical", Price = "35", TrackDurationHours = TrackDuration.OneWeek.ToInt().GetString(), TrackInternvalMinutes = TrackInterval.OneHour.ToInt().GetString()},

							new TrackingPriceBreak{Category = "Standard", Price = "5", TrackDurationHours = TrackDuration.OneDay.ToInt().GetString(), TrackInternvalMinutes = TrackInterval.FourHours.ToInt().GetString()},
							new TrackingPriceBreak{Category = "Standard", Price = "5", TrackDurationHours = TrackDuration.TwoDays.ToInt().GetString(), TrackInternvalMinutes = TrackInterval.FourHours.ToInt().GetString()},
							new TrackingPriceBreak{Category = "Standard", Price = "5", TrackDurationHours = TrackDuration.ThreeDays.ToInt().GetString(), TrackInternvalMinutes = TrackInterval.FourHours.ToInt().GetString()},
							new TrackingPriceBreak{Category = "Standard", Price = "5", TrackDurationHours = TrackDuration.FourDays.ToInt().GetString(), TrackInternvalMinutes = TrackInterval.FourHours.ToInt().GetString()},
							new TrackingPriceBreak{Category = "Standard", Price = "5", TrackDurationHours = TrackDuration.FiveDays.ToInt().GetString(), TrackInternvalMinutes = TrackInterval.FourHours.ToInt().GetString()},
							new TrackingPriceBreak{Category = "Standard", Price = "10", TrackDurationHours = TrackDuration.SixDays.ToInt().GetString(), TrackInternvalMinutes = TrackInterval.FourHours.ToInt().GetString()},
							new TrackingPriceBreak{Category = "Standard", Price = "10", TrackDurationHours = TrackDuration.OneWeek.ToInt().GetString(), TrackInternvalMinutes = TrackInterval.FourHours.ToInt().GetString()},
							
						}
				};
			Console.WriteLine(auth.ToXml());
		}
	}
}
