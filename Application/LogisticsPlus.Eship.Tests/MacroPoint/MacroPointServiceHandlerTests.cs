﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Extern.Edi.Segments;
using LogisticsPlus.Eship.MacroPoint;
using LogisticsPlus.Eship.Processor;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.MacroPoint
{
    //IMPORTANT NOTE: Do not run the first two tests (CanSubmitOrder, CanStopOrder) unless you have the uri in the MacropointServiceHandler set to
    //submit to "http://localhost:53038/Services/MacroPointServiceProxy.aspx" rather than the actual MacroPoint uri address

    [TestFixture]
	[Ignore("Run on demand only. Must test against valid Uri")]
    public class MacroPointServiceHandlerTests
    {
        private const string FutureUse = "Future Use";
        private const string EventCallNotes = "Stop Name";
        private const string Latitude = "Latitude";
        private const string Longitude = "Longitude";
        private const string Uncertainty = "Uncertainty";
        private const string Street1 = "Street1";
        private const string Street2 = "Street2";
        private const string Neighborhood = "Neighborhood";
        private const string City = "City";
        private const string StateOrProvince = "State";
        private const string PostalCode = "Postal";
        private const string Country = "Country";

	    private MacroPointServiceHandler _handler;
	    private Order _order;
	    private MacroPointOrder _macroPointOrder;
	    private TrackingPriceBreak _priceBreak;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            
           _handler = new MacroPointServiceHandler(GlobalTestInitializer.MacroPointSettings, GlobalTestInitializer.ActiveUser);
            _order = new Order
				{
					Number = new Number
						{
							Type = NumberType.Mobile.GetString(),
							Value = "9999999999"
						},
					TrackStartDateTime = DateTime.Now.FormattedShortDateAlt() + " " + DateTime.Now.ToString("HH") + ":00 ET",
					Notifications = new List<Notification>
					    {
							new Notification
								{
									IDNumber = "12345",
									PartnerMPID = "Macro Point Partner ID",
									TrackDurationInHours = TrackDuration.OneWeek.ToInt().GetString(),
									TrackIntervalInMinutes = TrackInterval.FourHours.ToInt().GetString(),
								}
						},
					Carrier = new Carrier
						{
							Name = FutureUse,
							Dispatcher = new Dispatcher
								{
									Name = FutureUse,
									Email = FutureUse,
									Phone = FutureUse
								}
						},
					TripSheet = new TripSheet
						{
							Stops = new List<Stop>
							    {
									new Stop
										{
											Address = new Address
												{
													City = FutureUse,
													Line1 = FutureUse,
													Line2 = FutureUse,
													PostalCode = FutureUse,
													StateOrProvince = FutureUse,
												},
											Name = FutureUse,
											EndDateTime = FutureUse,
											Latitude = FutureUse,
											Longitude = FutureUse,
											StartDateTime = FutureUse,
											StopID = FutureUse,
											StopType = FutureUse,
										}
								}
						}
				};

            _priceBreak = GlobalTestInitializer.MacroPointSettings.PriceBreaks.FirstOrDefault();
        }

        [Test]
        public void CanSubmitOrder()
        {
            _macroPointOrder = _handler.SubmitOrder(_order, _priceBreak);
            Assert.IsTrue(_macroPointOrder.OrderId != String.Empty);
        }

        [Test]
        public void CanStopOrder()
        {
           Assert.IsTrue(_handler.StopOrder(_macroPointOrder));
        }

        [Test]
        public void CanUpdateOrderStatus()
        {
            _macroPointOrder = new MacroPointOrder(GlobalTestInitializer.DefaultMacroPointOrderId);

            var request = new HttpRequest(
                String.Empty,
                "https://CustomerSpecifiedURL",
                string.Format("ID={0}&Code={1}&Message={2}",
                              _macroPointOrder.OrderId,
                              OrderTrackingStatusCodes.TrackingNow.ToInt().GetString(),
                              OrderTrackingStatusCodes.TrackingNow.GetString()));

            var response = new HttpResponse(new StringWriter());

            _macroPointOrder = _handler.UpdateOrderStatus(request, response);

            Assert.IsTrue(_macroPointOrder.TrackingStatusCode == OrderTrackingStatusCodes.TrackingNow.ToInt().GetString());
            Assert.IsTrue(_macroPointOrder.TrackingStatusDescription == OrderTrackingStatusCodes.TrackingNow.GetString());
            Assert.IsTrue(response.StatusCode == HttpStatusCode.OK.ToInt());
        }

        [Test]
        public void CanProcessEventUpdate()
        {
            _macroPointOrder = new MacroPointOrder(GlobalTestInitializer.DefaultMacroPointOrderId);
            var dateTimeNow = DateTime.Now.ToUniversalTime();
            var formattedDateTimeNow = dateTimeNow.FormattedShortDateAlt() + " " + dateTimeNow.ToString("HH") + ":00Z";

            var request = new HttpRequest(
                String.Empty,
                "https://CustomerSpecifiedURL",
                string.Format("ID={0}&Stop={1}&Event={2}&EventDateTime={3}",
                              _macroPointOrder.OrderId,
                              EventCallNotes,
                              ShipStatMsgCode.AA,
                              formattedDateTimeNow));
            var response = new HttpResponse(new StringWriter());

            var checkCall = _handler.ProcessEventUpdate(request, response);

            Assert.IsTrue(checkCall.CallNotes == EventCallNotes);
            Assert.IsTrue(checkCall.EdiStatusCode == ShipStatMsgCode.AA.GetString());
            Assert.IsTrue(checkCall.ShipmentId == _macroPointOrder.RetrieveCorrespondingShipment().Id);
            Assert.IsTrue(checkCall.EventDate == formattedDateTimeNow.ToDateTime());
            Assert.IsTrue(checkCall.TenantId == GlobalTestInitializer.ActiveUser.TenantId);
            Assert.IsTrue(response.StatusCode == HttpStatusCode.OK.ToInt());
        }

        [Test]
        public void CanProcessLocationNotification()
        {
            _macroPointOrder = new MacroPointOrder(GlobalTestInitializer.DefaultMacroPointOrderId);
            var dateTimeNow = DateTime.Now.ToUniversalTime();
            var formattedDateTimeNow = dateTimeNow.FormattedShortDateAlt() + " " + dateTimeNow.ToString("HH") + ":00Z";

            var request = new HttpRequest(
                String.Empty,
                "https://CustomerSpecifiedURL",
                string.Format(
                    "ID={0}&Latitude={1}&Longitude={2}&Uncertainty={3}&Street1={4}&Street2={5}&Neighborhood={6}&City={7}&State={8}&Postal={9}&Country={10}&LocationDateTimeUTC={11}",
                    _macroPointOrder.OrderId,
                    Latitude,
                    Longitude,
                    Uncertainty,
                    Street1,
                    Street2,
                    Neighborhood,
                    City,
                    StateOrProvince,
                    PostalCode,
                    Country,
                    formattedDateTimeNow));
            var response = new HttpResponse(new StringWriter());

            var checkCall = _handler.ProcessLocationNotification(request, response).First();

            Assert.IsTrue(checkCall.EdiStatusCode == String.Empty);
            Assert.IsTrue(checkCall.ShipmentId == _macroPointOrder.RetrieveCorrespondingShipment().Id);
            Assert.IsTrue(checkCall.EventDate == formattedDateTimeNow.ToDateTime()); 
            Assert.IsTrue(checkCall.TenantId == GlobalTestInitializer.ActiveUser.TenantId);
            Assert.IsTrue(response.StatusCode == HttpStatusCode.OK.ToInt());
        }
    }
}
