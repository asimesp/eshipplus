﻿using System;
using System.Linq;
using LogisticsPlus.Eship.SmallPacks;
using LogisticssPlus.FedExPlugin.Services;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.SmallPacks
{
	[TestFixture]
	public class HandlersTests
	{
		[Test]
		public void PrintPackageTypes()
		{
			var types = SmallPacksFactory.RetrievePackageTypes();
			foreach (var key in types.Keys)
				foreach (var k in types[key].Keys)
					Console.WriteLine("{0} {1} - {2}", key, k, types[key][k]);
		}

		[Test]
		public void PrintServiceOptions()
		{
			var options = SmallPacksFactory.RetrieveServiceOptions();
			foreach (var key in options.Keys)
				Console.WriteLine("{0} - {1}", options[key], key);
		}

        [Test]
        [Ignore("Update tracking number.")]
        public void FedExPluginCanGetTrackingDetails()
        {
            var pluginHandler = new FedExPluginHandler(new ServiceParams().DefaultProductionConfiguration(), null, null);

            var details = pluginHandler.GetTrackingInformation("898439500110", new DateTime(2014, 7, 21, 0, 0, 0), 3);

            //we will get a TrackingNumberUniqueIdentifier only if we received tracking data from webservice
            Assert.IsTrue(!string.IsNullOrEmpty(details.First().TrackingNumberUniqueIdentifier));
        }
	}
}
