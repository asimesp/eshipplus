﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Cms;
using LogisticsPlus.Eship.Processor.Searches.Cms;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Cms
{
	[TestFixture]
	class ContentTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanSaveAndDeleteRecord()
		{
			var content = new Content
			              	{
								ContentKey = new Guid("c001ac5e-d319-422d-a847-4ba8f225143b"),
			              		FullText =
			              			"Here is the full text test in the ContentTests. I decided to put more than just the word Test since it's nvarchar(max).",
			              		IdentifierCode = "TestCode",
			              		DateCreated = DateTime.Now,
			              		Current = true,
			              		ShortName = "Test",
			              		ShortText = "Test"
			              	};
			content.Save();

			Assert.IsTrue(content.Id != default(long));
			var id = content.Id;
			content.Delete();
			var newContent = new Content(id) { DateCreated = DateUtility.SystemEarliestDateTime };
			Assert.IsTrue(!newContent.KeyLoaded);
		}

		[Test]
		public void TestGetContentByCode()
		{
			var contentList = new ContentSearch().FetchContentByIdentifierCode("ContactUs");
			Assert.IsTrue(contentList.Count > 0);
		}

		[Test]
		public void TestUpdate()
		{
			var content = new Content(1);
			Assert.IsTrue(content.KeyLoaded);

			var saveName = content.ShortName;
			content.ShortName = DateTime.Now.ToString();

			content.Save();

			var content2 = new Content(content.Id);

			Assert.IsTrue(content.ShortName == content2.ShortName);

			content.ShortName = saveName;

			content.Save();

			content2 = new Content(content.Id);

			Assert.IsTrue(content.ShortName == content2.ShortName);
		}
	}
}
