﻿using System.Linq;
using LogisticsPlus.Eship.Processor.Dto.CMS;
using LogisticsPlus.Eship.Processor.Searches.Cms;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Cms
{
	[TestFixture]
	class ContentSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchContent()
		{
			var contents = new ContentSearch().FetchAllContent();
			Assert.IsTrue(contents.Count > default(long));
		}

		[Test]
		public void CanFetchContentByCode()
		{
			var contents = new ContentSearch().FetchAllContent();
			if (!contents.Any()) return;
			var contentsByCode = new ContentSearch().FetchContentByIdentifierCode(contents[0].IdentifierCode);
			Assert.IsTrue(contentsByCode.Count > default(long));
		}

		[Test]
		public void CanFetchCurrentContentByCode()
		{
			var content = new ContentSearch().FetchAllContent().FirstOrDefault(c => c.Current);
			if (content == null) return;
			var currentContent = new ContentSearch().FetchCurrentContentByIdentifierCode(content.IdentifierCode);
			Assert.IsNotNull(currentContent);
		}
		
		[Test]
		public void CanFetchContentByKey()
		{
			var content = new ContentSearch().FetchAllContent().FirstOrDefault();
			if (content == null) return;
			var contentByKey = new ContentSearch().FetchContentByContentKey(content.ContentKey.ToString());
			Assert.IsNotNull(contentByKey);
			Assert.IsTrue(contentByKey.Id == content.Id);
		}

        [Test]
        public void CanFetchContentDto()
        {
            var contentSummaries = new CmsContentDto().FetchContentDtos();
            Assert.IsTrue(contentSummaries.Count > default(long));
        }
	}
}
