﻿using System;
using LogisticsPlus.Eship.Core.Cms;
using LogisticsPlus.Eship.Processor.Validation.Cms;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Cms
{
	[TestFixture]
	class ContentValidatorTests
	{
		private ContentValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new ContentValidator();
		}

		[Test]
		public void IsNotValidTest()
		{
			var content = new Content();
			Assert.IsFalse(_validator.IsValid(content));
		}

		[Test]
		public void IsValidTest()
		{
			var content = new Content
							{
								ContentKey = new Guid(),
								FullText = DateTime.Now.ToString(),
								IdentifierCode = DateTime.Now.ToString(),
								ShortName = DateTime.Now.ToString(),
								ShortText = string.Empty,
								DateCreated = DateTime.Now
							};
			Assert.IsTrue(_validator.IsValid(content));
		}

	}
}
