﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Cms;
using LogisticsPlus.Eship.Processor.Handlers.Cms;
using LogisticsPlus.Eship.Processor.Searches.Cms;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Cms;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Cms
{
	[TestFixture]
	class NewsHandlerTests
	{
		private NewsView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new NewsView();
			_view.Load();
		}

		[Test]
		public void CanSaveAndDeleteContent()
		{
			var news = new News
			{
				NewsKey = Guid.NewGuid(),
				Title = "Test",
				ShortText = "Test",
				FullText = "This is some text",
				DateCreated = DateTime.Now,
				ExpirationDate = DateTime.Now.AddDays(1),
				Hidden = false,
				Priority = 0
			};
			_view.NewsSave(news);
			Assert.IsFalse(news.IsNew);
            _view.NewsDelete(news);
			var news2 = new News(news.Id);
			Assert.IsFalse(news2.KeyLoaded);
		}

		[Test]
		public void CanUpdateContent()
		{
			var news = new NewsSearch().FetchNonHiddenNews().FirstOrDefault();
			if (news == null) return;
			news.TakeSnapShot();
			var saveShortText = news.ShortText;
			news.ShortText = DateTime.Now.ToString();
			_view.NewsSave(news);
			var newNews = new News(news.Id);
			Assert.IsTrue(newNews.ShortText == news.ShortText);
			news.ShortText = saveShortText;
			news.Save();
		}

		internal class NewsView : INewsView
		{
			public AdminUser ActiveSuperUser { get { return new AdminUser(GlobalTestInitializer.DefaultAdminUserId); } }

			public event EventHandler<ViewEventArgs<News>> Save;
			public event EventHandler<ViewEventArgs<News>> Delete;

			public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void LogException(Exception ex)
			{
				Console.WriteLine(ex);
			}

			public void Set(News news)
			{
			}

			public void Load()
			{
				var handler = new NewsHandler(this);
				handler.Initialize();
			}

			public void NewsSave(News news)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<News>(news));
			}

			public void NewsDelete(News news)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<News>(news));
			}
		}
	}
}
