﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Cms;
using LogisticsPlus.Eship.Processor.Searches.Cms;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Cms
{
	[TestFixture]
	class NewsTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanSaveAndDeleteRecord()
		{
			var news = new News
			{
				NewsKey = new Guid("c001ac5e-d319-422d-a847-4ba8f225143b"),
				Title = "Test",
				ShortText = "Test",
				FullText = "Test",
				DateCreated = DateTime.Now,
				ExpirationDate = DateTime.Now.AddDays(3),
				Hidden = true,
				Priority = 0,
			};
			news.Save();

			Assert.IsTrue(news.Id != default(long));
			var id = news.Id;
			news.Delete();
			var newContent = new News(id) { DateCreated = DateUtility.SystemEarliestDateTime };
			Assert.IsTrue(!newContent.KeyLoaded);
		}

		[Test]
		public void CanSaveAndUpdate()
		{
			var all = new NewsSearch().FetchAllNews();
			if (!all.Any()) return;
			var news = new News(all.First().Id);

			Assert.IsTrue(news.KeyLoaded);

			var saveName = news.ShortText;
			news.ShortText = DateTime.Now.ToString();

			news.Save();

			var news2 = new News(news.Id);

			Assert.IsTrue(news.ShortText == news2.ShortText);

			news.ShortText = saveName;

			news.Save();

			news2 = new News(news.Id);

			Assert.IsTrue(news.ShortText == news2.ShortText);
		}

	}
}
