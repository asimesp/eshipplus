﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Cms;
using LogisticsPlus.Eship.Processor.Handlers.Cms;
using LogisticsPlus.Eship.Processor.Searches.Cms;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Cms;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Cms
{
	[TestFixture]
	public class ContentHandlerTests
	{

		private ContentView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new ContentView();
			_view.Load();
		}

		[Test]
		public void CanSaveAndDeleteContent()
		{
			var content = new Content
							{
								ContentKey = Guid.NewGuid(),
								FullText = "This is some text",
								IdentifierCode = "Test",
								DateCreated = DateTime.Now,
								Current = false,
								ShortName = "Test",
								ShortText = "Test"
							};
			_view.ContentSave(content);
			var content2 = new Content(content.Id);
			Assert.IsTrue(content2.KeyLoaded);
			_view.ContentDelete(content2);
			content2 = new Content(content.Id);
			Assert.IsFalse(content2.KeyLoaded);
		}

		[Test]
		public void CanUpdateContent()
		{
			var content = new ContentSearch().FetchAllContent().FirstOrDefault();
			if (content == null) return;
			content.TakeSnapShot();
			var saveShortName = content.ShortName;
			content.ShortName = DateTime.Now.ToString();
			_view.ContentSave(content);
			var newContent = new Content(content.Id);
			Assert.IsTrue(newContent.ShortName == content.ShortName);
			content.ShortName = saveShortName;
			content.Save();
		}


		internal class ContentView : IContentView
		{
			public AdminUser ActiveSuperUser { get { return new AdminUser(GlobalTestInitializer.DefaultAdminUserId); } }

			public event EventHandler<ViewEventArgs<Content>> Save;
			public event EventHandler<ViewEventArgs<Content>> Delete;

			public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void LogException(Exception ex)
			{
				Console.WriteLine(ex);
			}

			public void Set(Content content)
			{
			}

			public void Load()
			{
				var handler = new ContentHandler(this);
				handler.Initialize();
			}

			public void ContentSave(Content content)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<Content>(content));
			}

			public void ContentDelete(Content content)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<Content>(content));
			}
		}
	}
}
