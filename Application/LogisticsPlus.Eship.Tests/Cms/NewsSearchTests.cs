﻿using System.Linq;
using NUnit.Framework;
using LogisticsPlus.Eship.Processor.Searches.Cms;

namespace LogisticsPlus.Eship.Tests.Cms
{
	[TestFixture]
	class NewsSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchNews()
		{
			var news = new NewsSearch().FetchNonHiddenNews();
			Assert.IsTrue(news.Count > default(long));
		}

		[Test]
		public void CanFetchNewsByNewsKey()
		{
			var news = new NewsSearch().FetchNonHiddenNews().FirstOrDefault();
			if (news == null) return;
			var newsByKey = new NewsSearch().FetchNewsByNewsKey(news.NewsKey.ToString());
			Assert.IsTrue(newsByKey.Id == news.Id);
		}
	}
}
