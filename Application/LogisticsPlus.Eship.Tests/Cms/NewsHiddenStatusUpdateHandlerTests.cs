﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Processor.Handlers.Cms;
using LogisticsPlus.Eship.Processor.Searches.Cms;
using LogisticsPlus.Eship.Processor.Validation;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Cms
{
	[TestFixture]
	class NewsHiddenStatusUpdateHandlerTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanSetandSaveNewsArchivedStatus()
		{
			var news = new NewsSearch().FetchAllNews().FirstOrDefault();
			if (news == null) return;
			var handler = new NewsHiddenStatusUpdateHandler(GlobalTestInitializer.ActiveAdminUser);
			Exception ex;
			var msg = handler.SetAndSaveNewsArchivedStatus(news.Id, !news.Hidden, out ex);
			Assert.IsNull(ex);
			Assert.IsTrue(msg.Type == ValidateMessageType.Information);
		}
	}
}
