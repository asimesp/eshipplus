﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Processor.Handlers.Cms;
using LogisticsPlus.Eship.Processor.Searches.Cms;
using LogisticsPlus.Eship.Processor.Validation;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Cms
{
	[TestFixture]
	public class CurrentContentUpdateHandlerTests
	{

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanSetandSaveContentCurrentStatus()
		{
			var content = new ContentSearch().FetchAllContent().FirstOrDefault();
			if (content == null) return;
			var handler = new CurrentContentUpdateHandler(GlobalTestInitializer.ActiveAdminUser);
			Exception ex;
			var msg = handler.SetandSaveContentCurrentStatus(content.Id, true, out ex);
			Assert.IsNull(ex);
			Assert.IsTrue(msg.Type == ValidateMessageType.Information);
		}
	}
}
