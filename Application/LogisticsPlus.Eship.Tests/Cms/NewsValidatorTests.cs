﻿using System;
using LogisticsPlus.Eship.Core.Cms;
using LogisticsPlus.Eship.Processor.Validation.Cms;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Cms
{
	[TestFixture]
	class NewsValidatorTests
	{
		private NewsValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new NewsValidator();
		}
		[Test]
		public void IsNotValidTest()
		{
			var news = new News();
			Assert.IsFalse(_validator.IsValid(news));
		}

		[Test]
		public void IsValidTest()
		{
			var news = new News()
			{
				NewsKey = new Guid(),
				FullText = DateTime.Now.ToString(),
				Title = DateTime.Now.ToString(),
				ShortText = string.Empty,
				DateCreated = DateTime.Now,
				ExpirationDate = DateTime.Now.AddDays(3),
				Hidden = false,
				Priority = 0,
			};
			Assert.IsTrue(_validator.IsValid(news));
		}

	}
}
