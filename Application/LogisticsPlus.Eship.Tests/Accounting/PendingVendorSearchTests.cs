﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class PendingVendorSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchAllPendingVendors()
		{
			var results = new PendingVendorSearch().FetchPendingVendors(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(results.Any());
		}

		[Test]
		public void CanFetchByVendorNumber()
		{
			var all = new PendingVendorSearch().FetchPendingVendors(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			new PendingVendorSearch().FetchPendingVendorByNumber(all[0].VendorNumber, GlobalTestInitializer.DefaultTenantId);
		}

		[Test]
		public void CanFetchPendingVendors()
		{
			var columns = new List<ParameterColumn>();

			var column = AccountingSearchFields.NonAliasEquipmentTypeName.ToParameterColumn();
			column.DefaultValue = SearchUtilities.WildCard;
			column.Operator = Operator.Contains;
			columns.Add(column);

			var field = AccountingSearchFields.PendingVendors.FirstOrDefault(f => f.Name == "VendorNumber");
			if (field == null)
			{
				Console.WriteLine("AccountingSearchFields.PendingVendors no longer contains the search field this test relies on");
				Assert.IsNotNull(field); //fail the test, print the message
			}
			column = field.ToParameterColumn();
			column.DefaultValue = SearchUtilities.WildCard;
			column.Operator = Operator.Contains;
			columns.Add(column);

			var results = new PendingVendorSearch().FetchPendingVendors(columns, GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(results.Any());
		}
	}
}
