﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class SalesRepresentativeHandlerTests
	{
		private SalesRepresentativeView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new SalesRepresentativeView();
			_view.Load();
		}

		[Test]
		public void CanHandleSalesRepresentativeSaveOrUpdate()
		{
            var columns = new List<ParameterColumn>();
            var column = AccountingSearchFields.NonAliasedCountryCode.ToParameterColumn();

            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            columns.Add(column);

            var all = new SalesRepresentativeSearch().FetchSalesRepresentatives(columns, GlobalTestInitializer.DefaultTenantId);
            if (all.Count == 0) return;
            var salesRepresentative = all[0];
			salesRepresentative.LoadCollections();
            _view.LockRecord(salesRepresentative);
            salesRepresentative.TakeSnapShot();
            var number = salesRepresentative.SalesRepresentativeNumber;
            salesRepresentative.SalesRepresentativeNumber = DateTime.Now.ToShortDateString();
            _view.SaveRecord(salesRepresentative);
            salesRepresentative.TakeSnapShot();
            salesRepresentative.SalesRepresentativeNumber = number;
            _view.SaveRecord(salesRepresentative);
            _view.UnLockRecord(salesRepresentative);
		}

		[Test]
		public void CanHandleSalesRepresentativeSaveAndDelete()
		{
			var salesRepresentative = new SalesRepresentative
						{
							Name = DateTime.Now.ToShortDateString(),
							SalesRepresentativeNumber = "99999999999999999999",
							DateCreated = DateTime.Now,
							CountryId = GlobalTestInitializer.DefaultCountryId,
							TenantId = _view.ActiveUser.TenantId,
							AdditionalEntityName = DateTime.Now.ToString(),
							City = DateTime.Now.ToString(),
							CompanyName = DateTime.Now.ToString(),
							Email = string.Empty,
							Street1 = string.Empty,
							Street2 = string.Empty,
							State = string.Empty,
							Fax = string.Empty,
							Phone = "111-111-1111",
							Mobile = string.Empty,
							PostalCode = "16501",
						};
		    salesRepresentative.SalesRepCommTiers = new List<SalesRepCommTier>
		        {
		            new SalesRepCommTier
		                {
		                    SalesRepresentative = salesRepresentative,
		                    TenantId = GlobalTestInitializer.DefaultTenantId,
		                    ServiceMode = ServiceMode.NotApplicable,
		                    CeilingValue = default(decimal),
		                    CommissionPercent = default(decimal),
		                    EffectiveDate = DateTime.Now,
		                    ExpirationDate = DateTime.Now,
		                    FloorValue = default(decimal),
		                }
		        };
			_view.SaveRecord(salesRepresentative);
			_view.LockRecord(salesRepresentative);
			Assert.IsTrue(salesRepresentative.Id != default(long));
			_view.DeleteRecord(salesRepresentative);
			Assert.IsTrue(!new SalesRepresentative(salesRepresentative.Id, false).KeyLoaded);

		}

		internal class SalesRepresentativeView : ISalesRepresentativeView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public List<Country> Countries
			{
				set { Assert.IsTrue(value.Count > 0); }
			}

		    public Dictionary<int, string> ServiceModes { set; private get; }

		    public event EventHandler Loading;
            public event EventHandler<ViewEventArgs<SalesRepresentative>> LoadAuditLog;
		    public event EventHandler<ViewEventArgs<SalesRepresentative>> Save;
			public event EventHandler<ViewEventArgs<SalesRepresentative>> Delete;
			public event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
		    public event EventHandler<ViewEventArgs<List<SalesRepresentative>>> BatchImport;
		    public event EventHandler<ViewEventArgs<SalesRepresentative>> Lock;
			public event EventHandler<ViewEventArgs<SalesRepresentative>> UnLock;
		    public event EventHandler<ViewEventArgs<string>> CustomerSearch;

		    public void DisplayCustomer(Customer customer)
		    {
		        
            }

		    public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
		    {
		        
		    }

		    public void FailedLock(Lock @lock)
			{

			}

		    public void Set(SalesRepresentative salesRep)
		    {
		        
		    }

		    public void DisplaySearchResult(List<SalesRepresentative> SalesRepresentative)
			{
				Assert.IsTrue(SalesRepresentative.Count > 0);
			}

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void Load()
			{
				var handler = new SalesRepresentativeHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(SalesRepresentative salesRepresentative)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<SalesRepresentative>(salesRepresentative));
			}

			public void DeleteRecord(SalesRepresentative salesRepresentative)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<SalesRepresentative>(salesRepresentative));
			}

			public void SearchRecord(List<ParameterColumn> columns )
			{
				if (Search != null)
                    Search(this, new ViewEventArgs<List<ParameterColumn>>(columns));
			}

			public void LockRecord(SalesRepresentative salesRepresentative)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<SalesRepresentative>(salesRepresentative));
			}

			public void UnLockRecord(SalesRepresentative salesRepresentative)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<SalesRepresentative>(salesRepresentative));
			}
		}
	}
}
