﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	public class TierSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchByWildCard()
		{
			var tiers = new TierSearch().FetchTiers(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(tiers.Count > 0);
		}

		[Test]
		public void CanSearchByTierNumber()
		{
			var all = new TierSearch().FetchTiers(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var tier = new TierSearch().FetchTierByTierNumber(all[0].TierNumber, GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(tier.Id != 0);
		}

        [Test]
        public void CanFetchTiers()
        {
            var columns = new List<ParameterColumn>();
            var field = AccountingSearchFields.Tiers.FirstOrDefault(f => f.Name == "Name");
            if (field == null)
            {
                Console.WriteLine("AccountingSearchFields.Tiers no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }
            columns.Add(new ParameterColumn { DefaultValue = SearchUtilities.WildCard, ReportColumnName = field.DisplayName, Operator = Operator.Contains });

            var tiers = new TierSearch().FetchTiers(columns, GlobalTestInitializer.DefaultTenantId);

            Assert.IsTrue(tiers.Any());
        }
	}
}
