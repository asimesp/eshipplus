﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class InvoicePaidAmountUpdateHandlerTests
	{
		private InvoicePaidAmountUpdateView _paidAmountUpdateView;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_paidAmountUpdateView = new InvoicePaidAmountUpdateView();
			_paidAmountUpdateView.Load();
		}

		[Test]
		public void CanUpdate()
		{
			var criteria = new InvoiceViewSearchCriteria { ActiveUserId = GlobalTestInitializer.DefaultUserId, };
			var all = new InvoiceSearch().FetchInvoiceDashboardDtos(criteria, GlobalTestInitializer.DefaultTenantId);

			if (all.Count == 0) return;
		    var invoices = new List<Invoice>();
		    var invoice = new Invoice(all[0].Id);

            invoice.LoadCollections();

		    var originalValue = invoice.PaidAmount;

		    invoice.PaidAmount = default(int);
			invoices.Add(invoice);
			_paidAmountUpdateView.SaveRecord(invoices);
		    invoice.PaidAmount = originalValue;
            _paidAmountUpdateView.SaveRecord(invoices);
		}

		internal class InvoicePaidAmountUpdateView : IInvoicePaidAmountUpdatesView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public event EventHandler<ViewEventArgs<List<Invoice>>> Save;

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

		    public void DisplayUpdatedRecords(List<InvoicePaidAmountUpdateDto> updatedInvoices)
		    {
		            
		    }

		    public void Load()
			{
				var handler = new InvoicePaidAmountUpdateHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(List<Invoice> invoices)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<List<Invoice>>(invoices));
			}
		}
	}
}
