﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class InvoiceControlAccountUpdatesHandlerTests
	{
		private InvoiceControlAccountUpdatesView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new InvoiceControlAccountUpdatesView();
			_view.Load();
		}

		[Test]
		public void CanUpdate()
		{
			var all = new InvoiceSearch().FetchInvoicesNotPostedWithoutControlAccounts(GlobalTestInitializer.DefaultTenantId);

			if (all.Count == 0) return;
			var invoices = new List<Invoice>();
			var invoice = new Invoice(all[0].InvoiceId);

			invoice.LoadCollections();

			var originalValue = invoice.CustomerControlAccountNumber;

			invoice.CustomerControlAccountNumber = "CustomerControlAccount";
			invoices.Add(invoice);
			_view.SaveRecord(invoices);
			invoice.CustomerControlAccountNumber = originalValue;
			_view.SaveRecord(invoices);
		}

		internal class InvoiceControlAccountUpdatesView : IInvoiceControlAccountUpdatesView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public event EventHandler RetrieveRecordsToUpdate;
			public event EventHandler<ViewEventArgs<List<Invoice>>> Save;

			public void LogException(Exception ex)
			{

			}

			public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void DisplayRecordsToUpdate(List<InvoiceControlAccountUpdateDto> recordsToUpdate)
			{
				
			}

			public void DisplayUpdateMessages(List<ValidationMessage> messages)
			{

			}

			public void Load()
			{
				new InvoiceControlAccountUpdatesHandler(this).Initialize();
			}

			public void SaveRecord(List<Invoice> invoices)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<List<Invoice>>(invoices));
			}
		}
	}
}
