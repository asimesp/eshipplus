﻿using System;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class AccountingFunctionsTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CustomerOutstandingBalanceTests()
		{
			Console.WriteLine(new AccountingFunctions().OutstandingCustomerBalance(GlobalTestInitializer.DefaultCustomerId));
		}
	}
}
