﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class CustomerPurchaseOrderValidatorTests
	{
		private CustomerPurchaseOrderValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new CustomerPurchaseOrderValidator();
		}

		[Test]
		public void IsExistingAndUniqueTest()
		{
            var criteria = new PurchaseOrderViewSearchCriteria
            {
                CustomerId = GlobalTestInitializer.DefaultCustomerId
            };

            var field = AccountingSearchFields.CustomerPurchaseOrders.FirstOrDefault(f => f.Name == "PurchaseOrderNumber");
            if (field == null)
            {
                Console.WriteLine("AccountingSearchFields.CustomerPurchaseOrders no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }

            var column = field.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            criteria.Parameters.Add(column);

            var all = new CustomerPurchaseOrderSearch().FetchCustomerPurchaseOrders(criteria, GlobalTestInitializer.DefaultTenantId);
            if(!all.Any())return;

            var customerPurchaseOrder = all.First();
            
            Assert.IsTrue(_validator.IsUnique(customerPurchaseOrder)); // this should be unique
            
            var newOption = new CustomerPurchaseOrder(1000000, false);
            Assert.IsFalse(newOption.KeyLoaded); // will not be found
            newOption.PurchaseOrderNumber = customerPurchaseOrder.PurchaseOrderNumber;
            newOption.TenantId = customerPurchaseOrder.TenantId;
		    newOption.Customer = customerPurchaseOrder.Customer;
            Assert.IsFalse(_validator.IsUnique(newOption)); // should be false
		}

        [Test]
        public void IsValidTest()
        {
            var customerPurchaseOrder = new CustomerPurchaseOrder
                                        {TenantId = GlobalTestInitializer.DefaultTenantId,
                                         Customer = new Customer(GlobalTestInitializer.DefaultCustomerId),
                                         PurchaseOrderNumber = "asdf1234asdf",
                                         ValidPostalCode = "24523",
                                         MaximumUses = 4,
                                         CurrentUses = 2,
                                         ExpirationDate = DateTime.Now.AddDays(20),
                                         CountryId = GlobalTestInitializer.DefaultCountryId,
										 ApplyOnOrigin = true,
										 ApplyOnDestination = false
                                        };
            Assert.IsTrue(_validator.IsValid(customerPurchaseOrder));
        }
	}
}
