﻿using LogisticsPlus.Eship.Processor.Searches.Operations;
using NUnit.Framework;


namespace LogisticsPlus.Eship.Tests.Accounting
{
    [TestFixture]
    public class AssetLogSearchTests
    {
        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize(); 
        }

        [Test]
        public void CanFetchAssetLogs()
        {
        	new AssetLogSearch().FetchAssetLogs(GlobalTestInitializer.DefaultTenantId, GlobalTestInitializer.DefaultAssetId);
        }

		[Test]
		public void CanFetchLastAndPreviousAssetLogs()
		{
			var search = new AssetLogSearch();
			var log = search.FetchLastLogEntry(GlobalTestInitializer.DefaultAssetId, GlobalTestInitializer.DefaultTenantId);
			Assert.IsNotNull(log);
			log = search.FetchPreviousLog(log.Id, log.AssetId, GlobalTestInitializer.DefaultTenantId);
			Assert.IsNotNull(log);
		}
    }
}
