﻿using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class InvoiceTests
	{
		private InvoiceViewSearchCriteria _criteria;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			var start = DateUtility.SystemEarliestDateTime.TimeToMinimum();
			var end = DateUtility.SystemLatestDateTime.TimeToMinimum();

			_criteria = new InvoiceViewSearchCriteria {ActiveUserId = GlobalTestInitializer.DefaultUserId,};
		}

		[Test]
		public void CanLoadCollections()
		{
			var all = new InvoiceSearch().FetchInvoiceDashboardDtos(_criteria, GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var invoice = new Invoice(all[0].Id, false);
			invoice.LoadCollections();
		}

		[Test]
		public void CanRetrieveAssociatedRecords()
		{
			var all = new InvoiceSearch().FetchInvoiceDashboardDtos(_criteria, GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var invoice = new Invoice(all[0].Id, false);
			invoice.RetrieveAssociatedServiceTickets();
			invoice.RetrieveAssociatedShipments();
			invoice.RetrieveInvoicePrintLogs();
		}
	}
}