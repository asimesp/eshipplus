﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class AverageWeeklyFuelHandlerTests
	{
		private AverageWeeklyFuelView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new AverageWeeklyFuelView();
			_view.Load();
		}

		[Test]
		public void CanHandleAverageWeeklyFuelSaveOrUpdate()
		{
			var all = new AverageWeeklyFuelSearch().FetchAverageWeeklyFuels(string.Empty, DateTime.Now.AddDays(-10), DateTime.Now, GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;

			var averageWeeklyFuel = new AverageWeeklyFuel(all[0].Id, false);

			_view.LockRecord(averageWeeklyFuel);
			averageWeeklyFuel.TakeSnapShot();

			var effectiveDate = averageWeeklyFuel.EffectiveDate;
            averageWeeklyFuel.EffectiveDate = DateTime.Now;
			_view.SaveRecord(averageWeeklyFuel);
			averageWeeklyFuel.TakeSnapShot();
            averageWeeklyFuel.EffectiveDate = effectiveDate;
			_view.SaveRecord(averageWeeklyFuel);
			_view.UnLockRecord(averageWeeklyFuel);
		}

		[Test]
		public void CanHandleAverageWeeklyFuelSaveAndDelete()
		{
			var averageWeeklyFuel = new AverageWeeklyFuel
						{
							DateCreated = DateTime.Now,
							EffectiveDate = DateTime.Now.AddDays(30),
							ChargeCodeId = 23,
							EastCoastCost = new decimal(10.00),
							NewEnglandCost = new decimal(10.00),
							CentralAtlanticCost = new decimal(10.00),
							LowerAtlanticCost = new decimal(10.00),
							MidwestCost = new decimal(10.00),
							GulfCoastCost = new decimal(10.00),
							RockyMountainCost = new decimal(10.00),
							WestCoastCost = new decimal(10.00),
							WestCoastLessCaliforniaCost = new decimal(10.00),
							CaliforniaCost = new decimal(10.00),
							TenantId = _view.ActiveUser.TenantId,
						};
			_view.SaveRecord(averageWeeklyFuel);
			_view.LockRecord(averageWeeklyFuel);
			Assert.IsTrue(averageWeeklyFuel.Id != default(long));
			_view.DeleteRecord(averageWeeklyFuel);
			Assert.IsTrue(!new AverageWeeklyFuel(averageWeeklyFuel.Id, false).KeyLoaded);
		}

		internal class AverageWeeklyFuelView : IAverageWeeklyFuelView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public List<ChargeCode> ChargeCodes
			{
				set { Assert.IsTrue(value.Count>0); }
			}

			public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<AverageWeeklyFuel>> Save;
			public event EventHandler<ViewEventArgs<AverageWeeklyFuel>> Delete;
			public event EventHandler<ViewEventArgs<AverageWeeklyFuelViewSearchCriteria>> Search;
		    public event EventHandler<ViewEventArgs<List<AverageWeeklyFuel>>> BatchImport;
		    public event EventHandler<ViewEventArgs<AverageWeeklyFuel>> Lock;
			public event EventHandler<ViewEventArgs<AverageWeeklyFuel>> UnLock;

			public void FailedLock(Lock @lock)
			{

			}

			public void DisplaySearchResult(List<AverageWeeklyFuelViewSearchDto> AverageWeeklyFuel)
			{
				Assert.IsTrue(AverageWeeklyFuel.Count > 0);
			}

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Where(m => m.Type != ValidateMessageType.Information).Count() == 0);
			}

			public void Load()
			{
				var handler = new AverageWeeklyFuelHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(AverageWeeklyFuel averageWeeklyFuel)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<AverageWeeklyFuel>(averageWeeklyFuel));
			}

			public void DeleteRecord(AverageWeeklyFuel averageWeeklyFuel)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<AverageWeeklyFuel>(averageWeeklyFuel));
			}

			public void SearchRecord(AverageWeeklyFuelViewSearchCriteria criteria)
			{
				if (Search != null)
					Search(this, new ViewEventArgs<AverageWeeklyFuelViewSearchCriteria>(criteria));
			}

			public void LockRecord(AverageWeeklyFuel averageWeeklyFuel)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<AverageWeeklyFuel>(averageWeeklyFuel));
			}

			public void UnLockRecord(AverageWeeklyFuel averageWeeklyFuel)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<AverageWeeklyFuel>(averageWeeklyFuel));
			}
		}
	}
}
