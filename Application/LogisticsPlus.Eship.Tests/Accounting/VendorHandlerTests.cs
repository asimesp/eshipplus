﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class VendorHandlerTests
	{
		private VendorView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new VendorView();
			_view.Load();
		}

		[Test]
		public void CanSaveOrUpdate()
		{
			var all = new VendorSearch().FetchVendors(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var vendor = all[0];
			vendor.LoadCollections();
			_view.LockRecord(vendor);
			vendor.TakeSnapShot();
			var name = vendor.Name;
			vendor.Name = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff");
			VendorLocation remitTo = null;
			if (!vendor.Locations.Any(r => r.MainRemitToLocation && r.RemitToLocation) && vendor.Locations.Any(r => r.RemitToLocation))
				remitTo = vendor.Locations.First(r => r.RemitToLocation);
			if (remitTo != null) remitTo.MainRemitToLocation = true;
			_view.SaveRecord(vendor);
			vendor.TakeSnapShot();
			vendor.Name = name;
			_view.SaveRecord(vendor);
			_view.UnLockRecord(vendor);
		}

		[Test]
		public void CanSaveAndDelete()
		{
			var vendor = GetVendor();
			_view.SaveRecord(vendor);
			Assert.IsTrue(vendor.Id != default(long));
			_view.DeleteRecord(vendor);
			Assert.IsTrue(!new Vendor(vendor.Id, false).KeyLoaded);
		}

		public Vendor GetVendor()
		{
			var tenantId = GlobalTestInitializer.DefaultTenantId;
			var vendor = new Vendor
			             	{
			             		Name = DateTime.Now.ToShortDateString(),
			             		VendorNumber = "-1",
			             		TrackingUrl = string.Empty,
			             		Notation = string.Empty,
								LogoUrl = string.Empty,
			             		Active = true,
			             		Communication = null,
			             		TSACertified = true,
								CTPATNumber =  string.Empty,
								PIPNumber = string.Empty,
			             		Scac = "FAKE",
			             		FederalIDNumber = string.Empty,
			             		BrokerReferenceNumber = string.Empty,
			             		BrokerReferenceNumberType = BrokerReferenceNumberType.EmployerIdentificationNumber,
			             		DateCreated = DateTime.Now,
								LastAuditDate = DateTime.Now,
			             		MC = string.Empty,
			             		DOT = string.Empty,
			             		HandlesAir = true,
			             		HandlesLessThanTruckload = true,
			             		HandlesPartialTruckload = true,
			             		HandlesRail = true,
			             		HandlesSmallPack = true,
			             		HandlesTruckload = true,
			             		IsCarrier = true,
			             		IsAgent = true,
			             		IsBroker = true,
								Notes = string.Empty,
								CustomField1 = string.Empty,
								CustomField2 = string.Empty,
								CustomField3 = string.Empty,
								CustomField4 = string.Empty,
			             		Insurances = new List<VendorInsurance>(),
								VendorPackageCustomMappings = new List<VendorPackageCustomMapping>(),
			             		Locations = new List<VendorLocation>
			             		            	{
			             		            		new VendorLocation
			             		            			{
			             		            				TenantId = GlobalTestInitializer.DefaultTenantId,
			             		            				CountryId = GlobalTestInitializer.DefaultCountryId,
			             		            				Primary = true
			             		            			},
			             		            		new VendorLocation
			             		            			{
			             		            				TenantId = tenantId,
			             		            				CountryId = GlobalTestInitializer.DefaultCountryId
			             		            			}
			             		            	},
			             		Services = new List<VendorService>(),
			             		Equipments = new List<VendorEquipment>(),
								NoServiceDays = new List<VendorNoServiceDay>(),
								Documents = new List<VendorDocument>(),
			             		Ratings = null,
			             		TenantId = tenantId,
			             	};
			foreach (var location in vendor.Locations)
			{
				location.DateCreated = DateTime.Now;
				location.Vendor = vendor;
				location.Contacts = new List<VendorContact>
				                    	{
				                    		new VendorContact
				                    			{
				                    				TenantId = location.TenantId,
				                    				VendorLocation = location,
				                    				ContactType = ProcessorVars.RegistryCache[tenantId].ContactTypes[0],
													Primary = true,
				                    			}
				                    	};
			}
			vendor.Insurances.Add(new VendorInsurance
			                      	{
			                      		TenantId = tenantId,
			                      		Vendor = vendor,
                                        InsuranceType = ProcessorVars.RegistryCache[tenantId].InsuranceTypes[0],
			                      		CoverageAmount = 10000m,
			                      		CertificateHolder = "Holder",
			                      		EffectiveDate = DateTime.Now.AddDays(-7),
			                      		ExpirationDate = DateTime.Now,
			                      		PolicyNumber = "19834X901823",
			                      		SpecialInstruction = string.Empty,
										CarrierName = "CarrierName"
			                      	});
			vendor.VendorPackageCustomMappings.Add(new VendorPackageCustomMapping
				{
					TenantId = tenantId,
					Vendor = vendor,
					PackageType = ProcessorVars.RegistryCache[tenantId].PackageTypes[0],
					VendorCode = "PLT",
				});
			vendor.Services.Add(new VendorService
			                    	{
										ServiceId = new ServiceSearch().FetchServices(new List<ParameterColumn>(), tenantId)[0].Id,
			                    		TenantId = tenantId,
			                    		Vendor = vendor
			                    	});
			vendor.Equipments.Add(new VendorEquipment
			                      	{
                                        EquipmentTypeId = ProcessorVars.RegistryCache[tenantId].EquipmentTypes[0].Id,
			                      		TenantId = tenantId,
			                      		Vendor = vendor
			                      	});

		    var columns = new List<ParameterColumn>();
		    var column = RegistrySearchFields.Year.ToParameterColumn();
		    column.DefaultValue = "2012";
            column.Operator = Operator.Equal;
            columns.Add(column);
			vendor.NoServiceDays.Add(new VendorNoServiceDay
									{
										NoServiceDayId = new NoServiceDaySearch().FetchNoServiceDays(columns, tenantId)[0].Id,
										TenantId = tenantId,
										Vendor = vendor
									});
			return vendor;
		}

		internal class VendorView : IVendorView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public List<Country> Countries
			{
				set { }
			}

			public List<InsuranceType> InsuranceTypes
			{
				set { }
			}

			public List<ContactType> ContactTypes
			{
				set {  }
			}

			public List<DocumentTag> DocumentTags
			{
				set { }
			}

			public Dictionary<int, string> BrokerReferenceTypes
			{
				set { }
			}

		    public Dictionary<int, string> DateUnits { set{ } }

		    public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<Vendor>> Save;
			public event EventHandler<ViewEventArgs<Vendor>> Delete;
			public event EventHandler<ViewEventArgs<Vendor>> Lock;
			public event EventHandler<ViewEventArgs<Vendor>> UnLock;
			public event EventHandler<ViewEventArgs<string>> VendorServiceRepSearch;
			public event EventHandler<ViewEventArgs<Vendor>> LoadAuditLog;

			public void DisplayVendorServiceRep(User user)
			{
			}

			public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
			{

			}

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void FailedLock(Lock @lock)
			{

			}

		    public void Set(Vendor vendor)
		    {
		        
		    }

		    public void SetId(long id)
			{

			}

			public void Load()
			{
				var handler = new VendorHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(Vendor vendor)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<Vendor>(vendor));
			}

			public void DeleteRecord(Vendor vendor)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<Vendor>(vendor));
			}

			public void LockRecord(Vendor vendor)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<Vendor>(vendor));
			}

			public void UnLockRecord(Vendor vendor)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<Vendor>(vendor));
			}
		}
	}
}
