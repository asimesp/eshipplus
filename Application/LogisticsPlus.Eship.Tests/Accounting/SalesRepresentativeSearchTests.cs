﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class SalesRepresentativeSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchSalesRepresentatives()
		{
		    var columns = new List<ParameterColumn>();
            var column = AccountingSearchFields.NonAliasedCountryCode.ToParameterColumn();

            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            columns.Add(column);

            var field = AccountingSearchFields.SalesRepresentatives.FirstOrDefault(f => f.Name == "Name");
            if (field == null)
            {
                Console.WriteLine("AccountingSearchFields.SalesRepresentatives no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }

            var column2 = field.ToParameterColumn();
            column2.DefaultValue = SearchUtilities.WildCard;
            column2.Operator = Operator.Contains;
            columns.Add(column2);

            var results = new SalesRepresentativeSearch().FetchSalesRepresentatives(columns, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(results.Any());
		}

        [Test]
        public void CanFetchByNumber()
        {
            var salesRepresentative = new SalesRepresentativeSearch().FetchSalesRepresentativesByNumber
                (GlobalTestInitializer.DefaultSalesRepNumber, GlobalTestInitializer.DefaultTenantId); // fetch all
            Assert.IsTrue(salesRepresentative != null);
        }

		[Test]
		public void CanFetchSaleRepresentativeByCriteriaForNameOrCompanyName()
		{
			var results = new SalesRepresentativeSearch().FetchSaleRepresentativeByCriteriaForNameOrCompanyName(GlobalTestInitializer.DefaultTenantId, SearchUtilities.WildCard, Operator.Contains);
			Assert.IsTrue(results.Any());
		}
	}
}
