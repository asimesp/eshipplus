﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class PendingPendingVendorValidatorTests
	{
		private PendingVendorValidator _validator;
		private PendingVendor _pendingVendor;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new PendingVendorValidator();

			if (_pendingVendor != null) return;

			var tenantId = GlobalTestInitializer.DefaultTenantId;
			_pendingVendor = new PendingVendor
			{
				Name = DateTime.Now.ToShortDateString(),
				VendorNumber = "-123456",
				TrackingUrl = string.Empty,
				Notation = string.Empty,
				Approved = true,
				Rejected = false,
				TSACertified = true,
				Scac = "FAKE",
				FederalIDNumber = string.Empty,
				CTPATNumber = string.Empty,
				PIPNumber = string.Empty,
				BrokerReferenceNumber = string.Empty,
				BrokerReferenceNumberType = BrokerReferenceNumberType.EmployerIdentificationNumber,
				DateCreated = DateTime.Now,
				MC = string.Empty,
				DOT = string.Empty,
				HandlesAir = true,
				HandlesLessThanTruckload = true,
				HandlesPartialTruckload = true,
				HandlesRail = true,
				HandlesSmallPack = true,
				HandlesTruckload = true,
				IsCarrier = true,
				IsAgent = true,
				IsBroker = true,
				Insurances = new List<PendingVendorInsurance>(),
				Locations = new List<PendingVendorLocation>
			          		            	{
			          		            		new PendingVendorLocation
			          		            			{
			          		            				TenantId = GlobalTestInitializer.DefaultTenantId,
			          		            				CountryId = GlobalTestInitializer.DefaultCountryId,
			          		            				Primary = true
			          		            			},
			          		            		new PendingVendorLocation
			          		            			{
			          		            				TenantId = tenantId,
			          		            				CountryId = GlobalTestInitializer.DefaultCountryId
			          		            			}
			          		            	},
				Services = new List<PendingVendorService>(),
				Equipments = new List<PendingVendorEquipment>(),
				TenantId = tenantId,
                CreatedByUserId = GlobalTestInitializer.DefaultUserId
			};
			foreach (var location in _pendingVendor.Locations)
			{
				location.DateCreated = DateTime.Now;
				location.PendingVendor = _pendingVendor;
				location.Contacts = new List<PendingVendorContact>
				                    	{
				                    		new PendingVendorContact
				                    			{
				                    				TenantId = location.TenantId,
				                    				PendingVendorLocation = location,
				                    				ContactType = ProcessorVars.RegistryCache[tenantId].ContactTypes[0],
													Primary = true,
				                    			}
				                    	};
			}
			_pendingVendor.Services.Add(new PendingVendorService
			{
				ServiceId = new ServiceSearch().FetchServices(new List<ParameterColumn>(), tenantId)[0].Id,
				TenantId = tenantId,
				PendingVendor = _pendingVendor
			});
			_pendingVendor.Insurances.Add(new PendingVendorInsurance
			{
				TenantId = tenantId,
				PendingVendor = _pendingVendor,
				InsuranceType = ProcessorVars.RegistryCache[tenantId].InsuranceTypes[0],
				CoverageAmount = 10000m,
				CertificateHolder = "Holder",
				EffectiveDate = DateTime.Now.AddDays(-7),
				ExpirationDate = DateTime.Now,
				PolicyNumber = "19834X901823",
				SpecialInstruction = string.Empty,
				CarrierName = "CarrierName"
			});
			_pendingVendor.Equipments.Add(new PendingVendorEquipment
			{
				EquipmentTypeId = ProcessorVars.RegistryCache[tenantId].EquipmentTypes[0].Id,
				TenantId = tenantId,
				PendingVendor = _pendingVendor
			});
		}

		[Test]
		public void IsValidTest()
		{
			Assert.IsTrue(_validator.IsValid(_pendingVendor));
		}

		[Test]
		public void HasRequiredDataTest()
		{
			var hasAllRequiredData = _validator.HasAllRequiredData(_pendingVendor);
			if (!hasAllRequiredData) _validator.Messages.PrintMessages();
			Assert.IsTrue(hasAllRequiredData);
		}

		[Test]
		public void DuplicatePendingVendorNumberTest()
		{
			var all = new PendingVendorSearch().FetchPendingVendors(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var number = _pendingVendor.VendorNumber;
			_pendingVendor.VendorNumber = all[0].VendorNumber;
			Assert.IsTrue(_validator.DuplicatePendingVendorNumber(_pendingVendor));
			
			_pendingVendor.VendorNumber = number;
		}

        [Test]
        public void DuplicatePendingVendorMcTest()
        {
            var all = new PendingVendorSearch().FetchPendingVendors(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
            if (all.Count == 0) return;
            var pendingVendorWithMc = all.FirstOrDefault(p => !string.IsNullOrEmpty(p.MC));
            
            var mc = _pendingVendor.MC;
            _pendingVendor.MC = pendingVendorWithMc == null ? all[0].MC : pendingVendorWithMc.MC;
            Assert.IsTrue((pendingVendorWithMc == null && !_validator.DuplicatePendingVendorMc(_pendingVendor))|| _validator.DuplicatePendingVendorMc(_pendingVendor));

            _pendingVendor.MC = mc;
        }

        [Test]
        public void DuplicatePendingVendorDotTest()
        {
            var all = new PendingVendorSearch().FetchPendingVendors(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
            if (all.Count == 0) return;
            var pendingVendorWithDot = all.FirstOrDefault(p => !string.IsNullOrEmpty(p.DOT));
            var dot = _pendingVendor.DOT;
            _pendingVendor.DOT = pendingVendorWithDot == null ? all[0].DOT : pendingVendorWithDot.DOT;
            Assert.IsTrue((pendingVendorWithDot == null && !_validator.DuplicatePendingVendorMc(_pendingVendor)) || _validator.DuplicatePendingVendorDot(_pendingVendor));

            _pendingVendor.DOT = dot;
        }

		[Test]
		public void DuplicatePendingVendorServiceTest()
		{
			var vs = new PendingVendorService();
			Assert.IsFalse(_validator.PendingVendorServiceExists(vs));
			var all = new PendingVendorSearch().FetchPendingVendors(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId)
				.Where(v => v.Services.Count > 0)
				.ToList();
			if (!all.Any()) return;
			vs = all[0].Services[0];
			Assert.IsTrue(_validator.PendingVendorServiceExists(vs));
		}

		[Test]
		public void DuplicatePendingVendorEquipmentTest()
		{
			var ve = new PendingVendorEquipment();
			Assert.IsFalse(_validator.PendingVendorEquipmentExists(ve));
			var all = new PendingVendorSearch().FetchPendingVendors(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId)
				.Where(v => v.Equipments.Count > 0)
				.ToList();
			if (!all.Any()) return;
			ve = all[0].Equipments[0];
			Assert.IsTrue(_validator.PendingVendorEquipmentExists(ve));
		}
	}
}
