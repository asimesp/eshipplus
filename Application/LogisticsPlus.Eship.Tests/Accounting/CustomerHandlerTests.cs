﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	public class CustomerHandlerTests
	{
		private CustomerView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new CustomerView();
			_view.Load();
		}

		[Test]
		public void CanSaveOrUpdate()
		{
            var all = new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var customer = all[0];
			customer.LoadCollections();
			_view.LockRecord(customer);
			customer.TakeSnapShot();
			var name = customer.Name;
			customer.Name = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff");
			CustomerLocation billTo = null;
			if (!customer.Locations.Any(r => r.MainBillToLocation && r.BillToLocation) && customer.Locations.Any(r => r.BillToLocation))
				billTo = customer.Locations.First(r => r.BillToLocation);
			if (billTo != null) billTo.MainBillToLocation = true;
			_view.SaveRecord(customer);
			customer.TakeSnapShot();
			customer.Name = name;
			_view.SaveRecord(customer);
			_view.UnLockRecord(customer);
		}

		[Test]
		public void CanSaveAndDelete()
		{
			var customer = GetCustomer();
			_view.SaveRecord(customer);
			Assert.IsTrue(customer.Id != default(long));
			_view.DeleteRecord(customer);
			Assert.IsTrue(!new Customer(customer.Id, false).KeyLoaded);

		}

		private Customer GetCustomer()
		{
			var tenantId = GlobalTestInitializer.DefaultTenantId;
			var customer = new Customer
			               	{
			               		Name = DateTime.Now.ToShortDateString(),
			               		CustomerNumber = "-1",
			               		LogoUrl = string.Empty,
                                DefaultAccountBucket = ProcessorVars.RegistryCache[tenantId].AccountBuckets[0],
			               		Active = true,
                                AllowLocationContactNotification = false,
			               		AdditionalBillOfLadingText = string.Empty,
			               		Communication = null,
			               		CreditLimit = 100M,
			               		CustomFields = new List<CustomField>(),
			               		CustomChargeCodeMap = new List<CustomerChargeCodeMap>(),
								Documents = new List<CustomerDocument>(),
			               		CustomVendorSelectionMessage = string.Empty,
			               		CustomerType = CustomerType.DirectShipperReceiver,
			               		DateCreated = DateTime.Now,
			               		EnableShipperBillOfLading = false,
			               		HidePrefix = true,
			               		InvalidPurchaseOrderNumberMessage = string.Empty,
			               		InvoiceRequiresCarrierProNumber = true,
			               		InvoiceRequiresShipperReference = true,
			               		InvoiceTerms = 15,
								Notes = string.Empty,
								CustomField1 = string.Empty,
								CustomField2 = string.Empty,
								CustomField3 = string.Empty,
								CustomField4 = string.Empty,
                                AuditInstructions = "Test Adutit Instuctions",
			               		Locations = new List<CustomerLocation>
			               		            	{
			               		            		new CustomerLocation
			               		            			{
			               		            				TenantId = GlobalTestInitializer.DefaultTenantId,
			               		            				CountryId = GlobalTestInitializer.DefaultCountryId,
			               		            				Primary = true
			               		            			},
			               		            		new CustomerLocation
			               		            			{
			               		            				TenantId = tenantId,
			               		            				CountryId = GlobalTestInitializer.DefaultCountryId
			               		            			}
			               		            	},
			               		MileageSource = null,
			               		Prefix = null,
			               		Rating = null,
			               		RequiredInvoiceDocumentTags = new List<RequiredInvoiceDocumentTag>(),
			               		SalesRepresentative = null,
			               		ServiceRepresentatives = new List<CustomerServiceRepresentative>(),
			               		ShipmentRequiresNMFC = true,
			               		ShipmentRequiresPurchaseOrderNumber = true,
			               		ShipperBillOfLadingPrefix = string.Empty,
			               		ShipperBillOfLadingSeed = 1,
			               		ShipperBillOfLadingSuffix = string.Empty,
			               		TenantId = tenantId,
								Tier = new TierSearch().FetchTiers(new List<ParameterColumn>(), tenantId)[0],
			               		ValidatePurchaseOrderNumber = false,
                                CanPayByCreditCard = false,
                                IsCashOnly = false,
                                PaymentGatewayKey = string.Empty
			               	};
			foreach (var location in customer.Locations)
			{
				location.DateCreated = DateTime.Now;
				location.Customer = customer;
				location.Contacts = new List<CustomerContact>
				                    	{
				                    		new CustomerContact
				                    			{
				                    				TenantId = location.TenantId,
				                    				CustomerLocation = location,
				                    				ContactType = ProcessorVars.RegistryCache[tenantId].ContactTypes[0],
													Name = "test",
													Primary = true,
				                    			}
				                    	};
			}
			customer.CustomFields.Add(new CustomField {Customer = customer, TenantId = tenantId, Name = "Test Customer Field"});
			customer.CustomChargeCodeMap.Add(new CustomerChargeCodeMap
				{
					ChargeCodeId = 23,
					Customer = customer,
					ExternalCode = "Test",
					ExternalDescription = "Test",
					TenantId = tenantId
				});
		
			customer.RequiredInvoiceDocumentTags.Add(new RequiredInvoiceDocumentTag
			                                         	{
			                                         		TenantId = tenantId,
			                                         		Customer = customer,
			                                         		DocumentTag = ProcessorVars.RegistryCache[tenantId].DocumentTags[0]
			                                         	});
			customer.ServiceRepresentatives.Add(new CustomerServiceRepresentative
			                                    	{
			                                    		Customer = customer,
			                                    		TenantId = tenantId,
														User = new User(GlobalTestInitializer.DefaultUserId)
			                                    	});
			return customer;
		}

		internal class CustomerView : ICustomerView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}
			public List<Country> Countries
			{
				set {}
			}
			public List<ContactType> ContactTypes
			{
				set { }
			}
			public List<MileageSource> MileageSources
			{
				set { }
			}

			public List<DocumentTag> DocumentTags
			{
				set { }
			}

		    public Dictionary<int, string> CustomerTypes
			{
				set { }
			}

			public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<Customer>> Save;
			public event EventHandler<ViewEventArgs<Customer>> Delete;
			public event EventHandler<ViewEventArgs<Customer>> Lock;
			public event EventHandler<ViewEventArgs<Customer>> UnLock;
			public event EventHandler<ViewEventArgs<Customer>> LoadAuditLog;
		    public event EventHandler<ViewEventArgs<string>> SalesRepSearch;
			public event EventHandler<ViewEventArgs<string>> TierSearch;
			public event EventHandler<ViewEventArgs<string>> PrefixSearch;
			public event EventHandler<ViewEventArgs<string>> AccountBucketSearch;
		    public event EventHandler<ViewEventArgs<List<UserShipAs>>> AddCustomerUsers;
		    public event EventHandler<ViewEventArgs<Customer>> CreateCustomerInPaymentGateway;
		    public event EventHandler<ViewEventArgs<Customer>> DeleteCustomerInPaymentGateway;

		    public void DisplayAccountBucket(AccountBucket accountBucket)
			{
			}

		    public void LogException(Exception ex)
		    {
		                
		    }

		    public void DisplayPrefix(Prefix prefix)
			{
			}

			public void DisplayTier(Tier tier)
			{
				
			}

			public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
			{

			}

		    public void DisplaySalesRep(SalesRepresentative salesRepresentative)
		    {
		        
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void FailedLock(Lock @lock)
			{

			}

			public void SetId(long id)
			{

			}

			public void Load()
			{
				var handler = new CustomerHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(Customer customer)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<Customer>(customer));
			}

			public void DeleteRecord(Customer customer)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<Customer>(customer));
			}

			public void LockRecord(Customer customer)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<Customer>(customer));
			}

			public void UnLockRecord(Customer customer)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<Customer>(customer));
			}
		}
	}
}
