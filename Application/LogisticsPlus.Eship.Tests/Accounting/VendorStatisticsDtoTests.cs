﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Dto.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class VendorStatisticsDtoTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanRetrieveVendorStatistics()
		{
			var tenantId = GlobalTestInitializer.DefaultTenantId;
            var vendorId = new VendorSearch().FetchVendors(new List<ParameterColumn>(), tenantId).First().Id;

			new VendorStatisticsDto().RetrieveVendorStatistics(vendorId, tenantId);
		}
	}
}
