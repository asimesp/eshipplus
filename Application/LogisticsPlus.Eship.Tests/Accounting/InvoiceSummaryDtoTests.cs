﻿using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class InvoiceSummaryDtoTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchInvoiceSummary()
		{
			// safe number from db = '10956'
			new InvoiceSummaryDto().FetchInvoices("10956", DetailReferenceType.Shipment, GlobalTestInitializer.DefaultTenantId);
		}

		[Test]
		public void CanFetchInvoiceSummarySupplementalsAndCredits()
		{
			// safe id from db = 74
			new InvoiceSummaryDto().FetchInvoiceSupplementalsAndCredits(74, GlobalTestInitializer.DefaultTenantId);
		}
	}
}
