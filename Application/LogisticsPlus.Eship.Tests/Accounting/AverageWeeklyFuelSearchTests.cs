﻿using System;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class AverageWeeklyFuelSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchByCriteria()
		{
			var code = new AverageWeeklyFuelSearch().FetchAverageWeeklyFuels(string.Empty, DateTime.Now.AddYears(-3), DateTime.Now.AddDays(+10), GlobalTestInitializer.DefaultTenantId); // fetch all
			Assert.IsTrue(code.Count > 0);
		}
	}
}
