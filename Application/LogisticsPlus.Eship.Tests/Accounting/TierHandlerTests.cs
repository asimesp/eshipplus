﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	public class TierHandlerTests
	{
		private TierView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new TierView();
			_view.Load();
		}

		[Test]
		public void CanSaveOrUpdate()
		{
			var all = new TierSearch().FetchTiers(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var tier = all[0];
			_view.LockRecord(tier);
			tier.TakeSnapShot();
			var name = tier.Name;
			tier.Name = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff");
			_view.SaveRecord(tier);
			tier.TakeSnapShot();
			tier.Name = name;
			_view.SaveRecord(tier);
			_view.UnLockRecord(tier);
		}

		[Test]
		public void CanSaveAndDelete()
		{
			var tier = new Tier
			           	{
			           		Name = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff"),
			           		TierNumber = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff"),
			           		TenantId = _view.ActiveUser.TenantId,
			           		TollFreeContactNumber = "1-800-564-7857",
                            SupportTollFree = "1-800-000-TEST",
			           		DateCreated = DateTime.Now,
			           		TierSupportEmails = string.Empty,
			           		AdditionalBillOfLadingText = string.Empty,
			           		LTLNotificationEmails = string.Empty,
			           		FTLNotificationEmails = string.Empty,
			           		SPNotificationEmails = string.Empty,
			           		RailNotificationEmails = string.Empty,
			           		AirNotificationEmails = string.Empty,
							GeneralNotificationEmails = string.Empty,
							PendingVendorNotificationEmails = string.Empty,
			           		LogoUrl = string.Empty,
							AccountPayableDisputeNotificationEmails = string.Empty
			           	};
			_view.SaveRecord(tier);
			Assert.IsTrue(tier.Id != default(long));
			_view.DeleteRecord(tier);
			Assert.IsTrue(!new Tier(tier.Id, false).KeyLoaded);

		}

		internal class TierView : ITierView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public event EventHandler<ViewEventArgs<Tier>> Save;
			public event EventHandler<ViewEventArgs<Tier>> Delete;
			public event EventHandler<ViewEventArgs<Tier>> Lock;
			public event EventHandler<ViewEventArgs<Tier>> UnLock;
			public event EventHandler<ViewEventArgs<Tier>> LoadAuditLog;

			public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
			{

			}

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void FailedLock(Lock @lock)
			{

			}

		    public void Set(Tier tier)
		    {
		        
		    }


			public void Load()
			{
				new TierHandler(this).Initialize();
			}

			public void SaveRecord(Tier tier)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<Tier>(tier));
			}

			public void DeleteRecord(Tier tier)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<Tier>(tier));
			}

			public void LockRecord(Tier tier)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<Tier>(tier));
			}

			public void UnLockRecord(Tier tier)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<Tier>(tier));
			}
		}
	}
}
