﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	public class InvoiceSearchTests
	{
		private InvoiceViewSearchCriteria _criteria;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_criteria = new InvoiceViewSearchCriteria {ActiveUserId = GlobalTestInitializer.DefaultUserId,};
		}

		[Test]
		public void CanFetchId()
		{
			var all = new InvoiceSearch().FetchInvoiceDashboardDtos(_criteria, GlobalTestInitializer.DefaultTenantId);
			
			if (all.Count == 0) return;
			
			new InvoiceSearch().FetchInvoiceIdByInvoiceNumber(all[0].InvoiceNumber, GlobalTestInitializer.DefaultChargeCodeId);
        }

		[Test]
		public void CanFetchInvoiceDashboardDtos()
		{

			_criteria.Parameters.Add(new ParameterColumn
			                         	{
			                         		DefaultValue = DateTime.Now.AddDays(-180).ToString(),
			                         		ReportColumnName = AccountingSearchFields.DueDate.DisplayName,
			                         		Operator = Operator.GreaterThanOrEqual
			                         	});

			var invoices = new InvoiceSearch().FetchInvoiceDashboardDtos(_criteria, GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(invoices.Any());
		}

		[Test]
		public void CanFetchInvoicesNotPostedWithoutControlAccounts()
		{
			new InvoiceSearch().FetchInvoicesNotPostedWithoutControlAccounts(GlobalTestInitializer.DefaultTenantId);
		}

		[Test]
		public void CanFetchInvoiceNotDocDelivered()
		{
			new InvoiceSearch().FetchInvoiceNotDocDelivered(new DateTime(2013, 01, 01), GlobalTestInitializer.DefaultCustomerId,  GlobalTestInitializer.DefaultTenantId);
		}

		[Test]
		public void CanFetchInvoiceTotalCreditAdjustments()
		{
			var search = new InvoiceSearch();
			search.FetchInvoiceTotalCreditAdjustments(332, GlobalTestInitializer.DefaultTenantId);
			search.FetchInvoiceTotalCreditAdjustments(0, GlobalTestInitializer.DefaultTenantId);
		}

		[Test]
		public void CanFetchInvoiceTotalSupplementalAdjustments()
		{
			var search = new InvoiceSearch();
			search.FetchInvoiceTotalSupplementalAdjustments(724, GlobalTestInitializer.DefaultTenantId);
			search.FetchInvoiceTotalSupplementalAdjustments(0, GlobalTestInitializer.DefaultTenantId);
		}

        [Test]
        public void CanFetchUnpaidPostedInvoiceDtosForShipment()
        {
            var search = new InvoiceSearch();

            var invoices = search.FetchInvoiceDashboardDtos(new InvoiceViewSearchCriteria { ActiveUserId = GlobalTestInitializer.DefaultUserId }, GlobalTestInitializer.DefaultTenantId);

            var invoice = invoices.FirstOrDefault(i => !string.IsNullOrEmpty(i.Shipments) && i.AmountDue > i.PaidAmount) ?? new InvoiceDashboardDto();

            if (invoice.Id == default(long)) return;

            var shipmentNumber = invoice.Shipments.IndexOf(",", StringComparison.Ordinal) > 0
                                     ? invoice.Shipments.Substring(0, invoice.Shipments.IndexOf(",", StringComparison.Ordinal))
                                     : invoice.Shipments;

            var foundInvoices = search.FetchUnpaidPostedInvoiceDtosForShipment(shipmentNumber,
                                                                        GlobalTestInitializer.DefaultUserId,
                                                                        GlobalTestInitializer.DefaultTenantId);

            Assert.IsTrue(foundInvoices.Any());
        }
	}
}