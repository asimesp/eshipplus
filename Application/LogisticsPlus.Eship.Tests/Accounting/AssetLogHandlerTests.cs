﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
    [TestFixture]
    public class AssetLogHandlerTests
    {
        private AssetLogView _view;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _view = new AssetLogView();
            _view.Load();
        }

        [Test]
        public void CanSaveAndUpdate()
        {
            var assets = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].Assets;
            if(assets.Count == 0)return;
            var asset = assets[0];
            var all = new AssetLogSearch().FetchAssetLogs(GlobalTestInitializer.DefaultTenantId, asset.Id);
            if(all.Count == 0 )return;

        	var log = new AssetLog(all[0].Id);

            _view.LockRecord(log);
            log.TakeSnapShot();
            var comment = log.Comment;
            log.Comment = DateTime.Now.ToShortDateString();
            _view.SaveRecord(log);
            log.TakeSnapShot();
            log.Comment = comment;
            _view.SaveRecord(log);
            _view.UnLockRecord(log);

        }

        [Test]
        public void CanSaveAndDelete()
        {

            var assets = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].Assets;
            if (assets.Count == 0) return;
            var asset = assets[0];

            var criteria = new ShipmentViewSearchCriteria
                               {
                                   Parameters = new List<ParameterColumn>()
                               };
            var shipments =  new ShipmentSearch().FetchShipmentsDashboardDto(criteria, GlobalTestInitializer.DefaultTenantId);
            if (shipments.Count == 0) return;
            var shipment = shipments[0];

            var assetLog = new AssetLog
                               {
                                   DateCreated = DateTime.Now,
                                   LogDate = DateTime.Now,
                                   Comment = DateTime.Now.ToShortDateString(),
                                   MileageEngine = MileageEngine.LongitudeLatitude,
                                   ShipmentId = shipment.Id,
                                   AssetId = asset.Id,
                                   Street1 = DateTime.Now.ToShortDateString(),
                                   Street2 = DateTime.Now.ToShortDateString(),
                                   City = "Erie",
                                   State = "PA",
                                   PostalCode = "16502",
                                   CountryId = GlobalTestInitializer.DefaultCountryId
                               };

            _view.SaveRecord(assetLog);
            _view.LockRecord(assetLog);
            Assert.IsTrue(assetLog.Id != default(long));
            _view.DeleteRecord(assetLog);
            Assert.IsTrue(!new LibraryItem(assetLog.Id, false).KeyLoaded);
        }

        internal class AssetLogView : IAssetLogView
        {
            public User ActiveUser
            {
                get { return GlobalTestInitializer.ActiveUser; }
            }

            public List<Country> Countries
            {
                set {    }
            }

            public Dictionary<int, string> MileageEngines
            {
                set {    }
            }

            public Dictionary<int, string> MileageSources
            {
                set {    }
            }

            public event EventHandler   Loading;

            public event EventHandler<ViewEventArgs<AssetLog>> Save;
            public event EventHandler<ViewEventArgs<AssetLog>> Delete;
            public event EventHandler<ViewEventArgs<AssetLog>> Lock;
            public event EventHandler<ViewEventArgs<AssetLog>> UnLock;
            public event EventHandler<ViewEventArgs<long>> Search;
            public event EventHandler<ViewEventArgs<string>> AssetSearch;

            public void DisplayAsset(Asset asset)
            {
                    
            }

            public void FailedLock(Lock @lock){}

            public void DisplaySearchResult(List<AssetLogDto> assetLogs)
            {
                Assert.IsTrue(assetLogs.Count > 0);
            }

            public void LogException(Exception ex){}

            public void DisplayMessages(IEnumerable<ValidationMessage> messages)
            {
                messages.PrintMessages();
                Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
            }

            public void Load()
            {
                var handler = new AssetLogHandler(this);
                handler.Initialize();
            }

            public void SaveRecord(AssetLog assetLog)
            {
                if (Save != null)
                    Save(this, new ViewEventArgs<AssetLog>(assetLog));
            }

            public void DeleteRecord(AssetLog assetLog)
            {
                if (Delete != null)
                    Delete(this, new ViewEventArgs<AssetLog>(assetLog));
            }

            public void LockRecord(AssetLog assetLog)
            {
                if (Lock != null)
                    Lock(this, new ViewEventArgs<AssetLog>(assetLog));
            }

            public void UnLockRecord(AssetLog assetLog)
            {
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<AssetLog>(assetLog));
            }
        }
    }
}
