﻿using System;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class AssetValidatorTests
	{
		private AssetValidator _validator;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _validator = new AssetValidator();
        }

		[Test]
		public void IsValidTest()
		{
			var asset = new Asset
			            	{
			            		DateCreated = DateTime.Now,
			            		AssetType = AssetType.Driver,
			            		Description = "test",
			            		TenantId = GlobalTestInitializer.DefaultTenantId,
								AssetNumber = "99999",
			            	};
			var isValid = _validator.IsValid(asset);
			_validator.Messages.PrintMessages();
			Assert.IsTrue(isValid);
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var asset = new Asset();
			Assert.IsFalse(_validator.HasAllRequiredData(asset));
		}

		[Test]
		public void DuplicateAssetNumberTest()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].Assets;
			if (all.Count == 0) return;
			var asset = new Asset
			            	{
			            		Active = true,
			            		AssetType = AssetType.Driver,
			            		DateCreated = DateTime.Now,
			            		TenantId = GlobalTestInitializer.DefaultTenantId,
			            		AssetNumber = all[0].AssetNumber
			            	};
			Assert.IsTrue(_validator.DuplicateAssetNumber(asset));
		}
	}
}
