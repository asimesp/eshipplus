﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class CustomerControlAccountHandlerTests
	{
		private CustomerControlAccountView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new CustomerControlAccountView();
			_view.Load();
		}

		[Test]
		public void CanSaveOrUpdate()
		{
            var criteria = new CustomerControlAccountSearchCriteria
            {
                CustomerId = GlobalTestInitializer.DefaultCustomerId
            };

            var column = AccountingSearchFields.NonAliasedCountryCode.ToParameterColumn();

            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            criteria.Parameters.Add(column);

			var all = new CustomerControlAccountSearch()
				.FetchCustomerControlAccounts(criteria, GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var account = all[0];
			_view.LockRecord(account);
			account.TakeSnapShot();
			var name = account.GenericCategory;
			account.GenericCategory = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff");
			_view.SaveRecord(account);
			account.TakeSnapShot();
			account.GenericCategory = name;
			_view.SaveRecord(account);
			_view.UnLockRecord(account);
		}

		[Test]
		public void CanSaveAndDelete()
		{
			var account = GetAccount();
			_view.SaveRecord(account);
            _view.LockRecord(account);
			Assert.IsTrue(account.Id != default(long));
			_view.DeleteRecord(account);
			Assert.IsTrue(!new CustomerControlAccount(account.Id, false).KeyLoaded);
		}


		private static CustomerControlAccount GetAccount()
		{
			return new CustomerControlAccount
			       	{
			       		TenantId = GlobalTestInitializer.DefaultTenantId,
			       		Customer = new Customer(GlobalTestInitializer.DefaultCustomerId),
			       		AccountNumber = "asdf1234asdf",
			       		GenericCategory = "Category",
			       		Street1 = "123 Street",
			       		Street2 = "Apt 1",
			       		City = "City",
			       		State = "State",
			       		PostalCode = "12345",
						Description = "Some Account",
			       		CountryId = GlobalTestInitializer.DefaultCountryId,
			       	};
		}

		internal class CustomerControlAccountView : ICustomerControlAccountView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public List<Country> Countries
			{
				set{}
			}

			public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<CustomerControlAccount>> Save;
			public event EventHandler<ViewEventArgs<CustomerControlAccount>> Delete;
			public event EventHandler<ViewEventArgs<CustomerControlAccount>> Lock;
			public event EventHandler<ViewEventArgs<CustomerControlAccount>> UnLock;
            public event EventHandler<ViewEventArgs<CustomerControlAccountSearchCriteria>> Search;
			public event EventHandler<ViewEventArgs<List<CustomerControlAccount>>> BatchImport;
			public event EventHandler<ViewEventArgs<string>> CustomerSearch;

			public void DisplaySearchResult(List<CustomerControlAccount> customerControlAccounts)
			{
				
			}

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void DisplayCustomer(Customer customer)
			{
			}

			public void FailedLock(Lock @lock)
			{
			}


			public void SaveRecord(CustomerControlAccount account)
			{
				if (Save != null) Save(this, new ViewEventArgs<CustomerControlAccount>(account));
			}

			public void DeleteRecord(CustomerControlAccount account)
			{
				if (Delete != null) Delete(this, new ViewEventArgs<CustomerControlAccount>(account));
			}

			public void LockRecord(CustomerControlAccount account)
			{
				if (Lock != null) Lock(this, new ViewEventArgs<CustomerControlAccount>(account));
			}

			public void UnLockRecord(CustomerControlAccount account)
			{
				if (UnLock != null) UnLock(this, new ViewEventArgs<CustomerControlAccount>(account));
			}

			public void Load()
			{
				new CustomerControlAccountHandler(this).Initialize();
			}
		}
	}
}
