﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class CustomerControlAccountSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

        [Test]
        public void CanFetchCustomerControlAccounts()
        {
            var criteria = new CustomerControlAccountSearchCriteria
                               {
                                   CustomerId = GlobalTestInitializer.DefaultCustomerId
                               };

            var column = AccountingSearchFields.NonAliasedCountryCode.ToParameterColumn();

            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            criteria.Parameters.Add(column);

            var field = AccountingSearchFields.CustomerControlAccounts.FirstOrDefault(f => f.Name == "Description");
            if (field == null)
            {
                Console.WriteLine("AccountingSearchFields.CustomerControlAccounts no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }

            var column2 = field.ToParameterColumn();
            column2.DefaultValue = SearchUtilities.WildCard;
            column2.Operator = Operator.Contains;
            criteria.Parameters.Add(column2);

            var results = new CustomerControlAccountSearch().FetchCustomerControlAccounts(criteria, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(results.Any());

        }
	}
}
