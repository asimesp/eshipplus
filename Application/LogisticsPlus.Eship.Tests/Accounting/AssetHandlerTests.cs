﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class AssetHandlerTests
	{
		private AssetView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new AssetView();
			_view.Load();
		}

		[Test]
		public void CanHandleAssetSaveOrUpdate()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].Assets;
			if (all.Count == 0) return;

			var asset = all[0];
			_view.LockRecord(asset);
			asset.TakeSnapShot();
			var description = asset.Description;
			asset.Description = DateTime.Now.ToShortDateString();
			_view.SaveRecord(asset);
			asset.TakeSnapShot();
			asset.Description = description;
			_view.SaveRecord(asset);
			_view.UnLockRecord(asset);
		}

		[Test]
		public void CanHandleAssetSaveAndDelete()
		{
			var asset = new Asset
			            	{
			            		TenantId = GlobalTestInitializer.DefaultTenantId,
			            		AssetNumber = "99999",
			            		DateCreated = DateTime.Now,
			            		AssetType = AssetType.Driver,
			            		Active = true,
			            		Description = string.Empty
			            	};
			_view.SaveRecord(asset);
			_view.LockRecord(asset);
			Assert.IsTrue(asset.Id != default(long));
			_view.DeleteRecord(asset);
			Assert.IsTrue(!new Asset(asset.Id, false).KeyLoaded);

		}

		internal class AssetView : IAssetView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

		    public Dictionary<int, string> SearchAssetTypes
		    {
		        set {}
		    }

			public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<Asset>> Save;
            public event EventHandler<ViewEventArgs<Asset>> Delete;
            public event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
            public event EventHandler<ViewEventArgs<List<Asset>>> BatchImport;
            public event EventHandler<ViewEventArgs<Asset>> Lock;
            public event EventHandler<ViewEventArgs<Asset>> UnLock;

			public void FailedLock(Lock @lock)
			{

			}

            public void DisplaySearchResult(List<Asset> assets)
			{
				Assert.IsTrue(assets.Count > 0);
			}

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void Load()
			{
				var handler = new AssetHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(Asset asset)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<Asset>(asset));
			}

			public void DeleteRecord(Asset asset)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<Asset>(asset));
			}

            public void SearchRecord(List<ParameterColumn> criteria)
			{
				if (Search != null)
					Search(this, new ViewEventArgs<List<ParameterColumn>>(criteria));
			}

			public void LockRecord(Asset asset)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<Asset>(asset));
			}

			public void UnLockRecord(Asset asset)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<Asset>(asset));
			}
		}
	}
}
