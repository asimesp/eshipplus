﻿using System;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	public class VendorBillSearchTests
	{
		private VendorBillViewSearchCriteria _criteria;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_criteria = new VendorBillViewSearchCriteria {ActiveUserId = GlobalTestInitializer.DefaultUserId};
		}

		[Test]
	    public void CanFetchVendorBillDashboardDtos()
		{
			_criteria.Parameters.Add(new ParameterColumn
			                         	{
			                         		DefaultValue = DateTime.Now.AddMonths(-12).ToString(),
			                         		ReportColumnName = AccountingSearchFields.DateCreated.DisplayName,
			                         		Operator = Operator.GreaterThanOrEqual
			                         	});

	        var bills = new VendorBillSearch().FetchVendorBillDashboardDtos(_criteria, GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(bills.Count > 0);
	    }

		[Test]
		public void CanFetchVendorBillDashboardByDocumentNumber()
		{

			new VendorBillSearch().FetchVendorBillByVendorBillNumber(SearchUtilities.WildCard, default(long), GlobalTestInitializer.DefaultTenantId);
		}

		[Test]
		public void CanFetchInvoiceTotalCreditAdjustments()
		{
			var search = new VendorBillSearch();
			search.FetchInvoiceTotalCreditAdjustments(27016, GlobalTestInitializer.DefaultTenantId);
			search.FetchInvoiceTotalCreditAdjustments(0, GlobalTestInitializer.DefaultTenantId);
		}

		[Test]
		public void CanFetchVendorBillDocuments()
		{
			var search = new VendorBillSearch();
			var documents = search.FetchAssociatedDocuments(57725);

			Assert.IsTrue(documents.Count > 0);
		}
	}
}