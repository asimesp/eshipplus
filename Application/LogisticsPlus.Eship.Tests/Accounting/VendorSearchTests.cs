﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
    [TestFixture]
    public class VendorSearchTests
    {
        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

        [Test]
        public void CanFetchAllVendors()
        {
            var results = new VendorSearch().FetchVendors(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(results.Any());
        }

        [Test]
        public void CanFetchVendorsForLTLRating()
        {
            new VendorSearch().FetchVendorsForLTLRating(GlobalTestInitializer.DefaultTenantId);
        }

        [Test]
        public void CanFetchByVendorNumber()
        {
            var all = new VendorSearch().FetchVendors(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
            if (all.Count == 0) return;
            new VendorSearch().FetchVendorByNumber(all[0].VendorNumber, GlobalTestInitializer.DefaultTenantId);
        }

        [Test]
        public void CanFetchVendorsWithCommunication()
        {
            var columns = new List<ParameterColumn>();

            var field = AccountingSearchFields.VendorCommunications.FirstOrDefault(f => f.Name == "VendorNumber");
            if (field == null)
            {
                Console.WriteLine("AccountingSearchFields.VendorCommunications no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }

            var column = field.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            columns.Add(column);

            var results = new VendorSearch().FetchVendorsWithCommunication(columns, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(results.Any());

        }

        [Test]
        public void CanFetchVendorCommunicationsWithEdiOrFtpEnabled()
        {
            new VendorSearch().FetchVendorCommunicationsWithEdiOrFtp();
        }

		[Test]
		public void CanFetchVendorCommunicationsWithImgRtrvEnabled()
		{
			new VendorSearch().FetchVendorCommunicationsWithImgRtrv();
		}

        [Test]
        public void CandFetchEmailsForAvailableLoads()
        {
            var equipment = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].EquipmentTypes;
            var equipmentTypeIds = new List<long>();
            for (var i = 0; i < 5; i++)
            {
                equipmentTypeIds.Add(equipment[i].Id);
            }

            var criteria = new AvailableLoadsSearchCriteria
                               {
                                   IgnoreLanes = true,
                                   CheckTerminals = true,
                                   IncludeAgents = false,
                                   IncludeBrokers = false,
                                   IncludeCarrier = true,
                                   ContactTypeId = new Tenant(GlobalTestInitializer.DefaultTenantId).AvailableLoadsContactTypeId,
                                   DestinationCity = string.Empty,
                                   DestinationState = "PA",
                                   DestinationPostalCode = string.Empty,
                                   DestinationCountryId = GlobalTestInitializer.DefaultCountryId,
                                   OriginCity = string.Empty,
                                   OriginState = "OH",
                                   OriginPostalCode = string.Empty,
                                   OriginCountryId = GlobalTestInitializer.DefaultCountryId,
                                   EquipmentTypeIds = equipmentTypeIds,
                               };

            Assert.IsTrue(new VendorSearch().FetchVendorEmailsForAvailableLoads(criteria, GlobalTestInitializer.DefaultTenantId).Count > 0);
        }

        [Test]
        public void CanFetchVendors()
        {
            var columns = new List<ParameterColumn>(); 

            var column = AccountingSearchFields.NonAliasedVendorTerminalCity.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            columns.Add(column);

            column = AccountingSearchFields.NonAliasedVendorTerminalCountryCode.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            columns.Add(column);

            column = AccountingSearchFields.NonAliasEquipmentTypeName.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            columns.Add(column);

            var field = AccountingSearchFields.Vendors.FirstOrDefault(f => f.Name == "VendorNumber");
            if (field == null)
            {
                Console.WriteLine("AccountingSearchFields.Vendors no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }
            column = field.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            columns.Add(column);

            var results = new VendorSearch().FetchVendors(columns, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(results.Any());
        }
    }
}
