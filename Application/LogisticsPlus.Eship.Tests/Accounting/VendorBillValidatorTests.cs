﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class VendorBillValidatorTests
	{
		private VendorBillValidator _validator;
		private VendorBill _invoice;
		private VendorBillViewSearchCriteria _criteria;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new VendorBillValidator();

			_criteria = new VendorBillViewSearchCriteria {ActiveUserId = GlobalTestInitializer.DefaultUserId};

			var tenantId = GlobalTestInitializer.DefaultTenantId;
            var chargeCode = ProcessorVars.RegistryCache[tenantId].ChargeCodes.FirstOrDefault() ?? new ChargeCode();
            var accountBucket = ProcessorVars.RegistryCache[tenantId].AccountBuckets.FirstOrDefault() ?? new AccountBucket();
			var user = new User(GlobalTestInitializer.DefaultUserId);
			var vendor = new VendorSearch().FetchVendors(new List<ParameterColumn>(), tenantId).FirstOrDefault() ?? new Vendor();

			_invoice = new VendorBill(10000)
			{
				DocumentNumber = "00000",
				TenantId = tenantId,
				DateCreated = DateTime.Now,
				DocumentDate = DateTime.Now,
				VendorLocation = vendor.Locations.FirstOrDefault(l => l.Primary),
				BillType = InvoiceType.Invoice,
				Details = new List<VendorBillDetail>(),
				Posted = false,
				Exported = false,
				ExportDate = DateTime.Today,
				PostDate = DateTime.Today,
				User = user,
				ApplyToDocumentId = 0

			};

			_invoice.Details.Add(new VendorBillDetail
			{
				TenantId = GlobalTestInitializer.DefaultTenantId,
				UnitBuy = 1,
				VendorBill = _invoice,
				Note = "TEST",
				ReferenceNumber = "1234",
				ReferenceType = DetailReferenceType.Shipment,
				Quantity = 1,
				ChargeCode = chargeCode,
				AccountBucket = accountBucket
			});
		}

		[Test]
		public void DuplicateVendorBillNumberTest()
		{
			var all = new VendorBillSearch().FetchVendorBillDashboardDtos(_criteria, GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var bill = new VendorBill(all[0].Id);
			var newBill = new VendorBill{DocumentNumber = bill.DocumentNumber, VendorLocation = bill.VendorLocation, TenantId = bill.TenantId};
			Assert.IsTrue(_validator.DuplicateVendorBillNumber(newBill));
			newBill.VendorLocation = null;
			Assert.IsTrue(_validator.DuplicateVendorBillNumber(newBill));
			_validator.Messages.PrintMessages();
			if (bill.VendorLocation.Vendor.Locations.Count > 1)
			{
				newBill.VendorLocation = bill.VendorLocation.Vendor.Locations.First(i => i.Id != bill.VendorLocation.Id);
				Assert.IsTrue(_validator.DuplicateVendorBillNumber(newBill));
			}
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var hasAllRequiredData = _validator.HasAllRequiredData(_invoice);
			if (!hasAllRequiredData) _validator.Messages.PrintMessages();
			Assert.IsTrue(hasAllRequiredData);
		}

		[Test]
		public void IsValidTest()
		{
			Assert.IsTrue(_validator.IsValid(_invoice));
		}

		[Test]
		public void CanDeleteTest()
		{

			_validator.Messages.Clear();
			_validator.CanDeleteVendorBill(new VendorBill(GlobalTestInitializer.DefaultVendorBillId, false));
			_validator.Messages.PrintMessages();
		}
	}
}
