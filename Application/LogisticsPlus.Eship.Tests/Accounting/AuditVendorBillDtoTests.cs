﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
    [TestFixture]
    public class AuditVendorBillDtoTests
    {
        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

        [Test]
        public void CanFetchVendorBillDtoForAudit()
        {
        	var criteria = new ShipmentViewSearchCriteria {ActiveUserId = GlobalTestInitializer.DefaultUserId,};

			var shipment = new ShipmentSearch().FetchShipmentsDashboardDto(criteria, GlobalTestInitializer.DefaultTenantId).First();

			new AuditVendorBillDto().FetchAuditVendorBillDtos(shipment.ShipmentNumber, DetailReferenceType.Shipment, GlobalTestInitializer.DefaultTenantId);
        }
    }
}
