﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
    [TestFixture]
    public class InvoiceValidatorTests
    {
        private InvoiceValidator _validator;
        private Invoice _invoice;
        private InvoiceViewSearchCriteria _criteria;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _validator = new InvoiceValidator();

			_criteria = new InvoiceViewSearchCriteria { ActiveUserId = GlobalTestInitializer.DefaultUserId, };

            var tenantId = GlobalTestInitializer.DefaultTenantId;
            var chargeCode = ProcessorVars.RegistryCache[tenantId].ChargeCodes.FirstOrDefault() ?? new ChargeCode();
        	var user = new User(GlobalTestInitializer.DefaultUserId);
            var accountBucket = ProcessorVars.RegistryCache[tenantId].AccountBuckets.FirstOrDefault() ?? new AccountBucket();
            var customer = new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), tenantId).FirstOrDefault() ?? new Customer();

            _invoice = new Invoice(10000)
            {
                InvoiceNumber = "00000",
                TenantId = tenantId,
                InvoiceDate = DateTime.Now,
                CustomerLocation = customer.Locations.FirstOrDefault(l => l.Primary),
                DueDate = DateTime.Now,
                SpecialInstruction = "TEST",
                InvoiceType = InvoiceType.Invoice,
                Details = new List<InvoiceDetail>(),
                Posted = false,
                Exported = false,
                ExportDate = DateTime.Today,
                PostDate = DateTime.Today,
                PaidAmount = 0,
                User = user,
				CustomerControlAccountNumber = string.Empty,
            };

            _invoice.Details.Add(new InvoiceDetail
            {
                TenantId = GlobalTestInitializer.DefaultTenantId,
                UnitSell = 2,
                UnitDiscount = 1,
                Invoice = _invoice,
                Comment = "TEST",
                ReferenceNumber = "1234",
                ReferenceType = DetailReferenceType.Shipment,
                Quantity = 1,
                ChargeCode = chargeCode,
				AccountBucket = accountBucket
            });
        }

        [Test]
        public void DuplicateInvoiceNumberTest()
        {
            var all = new InvoiceSearch().FetchInvoiceDashboardDtos(_criteria, GlobalTestInitializer.DefaultTenantId);
            if (all.Count == 0) return;
            var invoice = new Invoice { InvoiceNumber = all[0].InvoiceNumber, TenantId = GlobalTestInitializer.DefaultTenantId };
            Assert.IsTrue(_validator.DuplicateInvoiceNumber(invoice));
        }

        [Test]
        public void HasAllRequiredDataTest()
        {
            var hasAllRequiredData = _validator.HasAllRequiredData(_invoice);
            if (!hasAllRequiredData) _validator.Messages.PrintMessages();
            Assert.IsTrue(hasAllRequiredData);
        }

		[Test]
		public void CanTestInvoiceProcessingRequirement()
		{
			_invoice.CustomerLocation.Customer.InvoiceRequiresControlAccount = true;
			_invoice.Posted = true;
			var valid = _validator.MeetsInvoicingRequirement(_invoice);
			if (!valid) _validator.Messages.PrintMessages();
			Assert.IsFalse(valid);

			_invoice.CustomerLocation.Customer.InvoiceRequiresShipperReference = true;
			_invoice.CustomerLocation.Customer.InvoiceRequiresCarrierProNumber = true;
			var shipment = new Shipment {ShipmentNumber = "12345", ShipperReference = string.Empty};
			var shipmentVendor = new ShipmentVendor {Shipment = shipment, ProNumber = string.Empty};
			shipment.Vendors = new List<ShipmentVendor> {shipmentVendor};
			valid = _validator.MeetsInvoicingRequirement(_invoice, new List<Shipment> {shipment});
			if (!valid) _validator.Messages.PrintMessages();
			Assert.IsFalse(valid);

			shipmentVendor.Primary = true;
			valid = _validator.MeetsInvoicingRequirement(_invoice, new List<Shipment> { shipment });
			if (!valid) _validator.Messages.PrintMessages();
			Assert.IsFalse(valid);

			_invoice.CustomerLocation.Customer.InvoiceRequiresControlAccount = false;
			_invoice.Posted = false;
			shipment.ShipperReference = "test";
			shipmentVendor.ProNumber = "pro";
			valid = _validator.MeetsInvoicingRequirement(_invoice, new List<Shipment> { shipment });
			if (!valid) _validator.Messages.PrintMessages();
			Assert.IsTrue(valid);

			valid = _validator.MeetsInvoicingRequirement(_invoice);
			if (!valid) _validator.Messages.PrintMessages();
			Assert.IsTrue(valid);
		}

        [Test]
        public void IsValidTest()
        {
            Assert.IsTrue(_validator.IsValid(_invoice));
        }

        [Test]
        public void InvoiceNumberInvalidTest()
        {
            _invoice.InvoiceNumber = Eship.Core.CoreUtilities.Suffix.Credit;
            var isValid = _validator.IsValid(_invoice);
            _validator.Messages.PrintMessages();
            Assert.IsFalse(isValid);
        }

        [Test]
        public void CanDeleteTest()
        {
            _validator.Messages.Clear();
            _validator.CanDeleteInvoice(_invoice);
            _validator.Messages.PrintMessages();
        }

        [Test]
        public void TotalAmountDueTest()
        {
            foreach (var detail in _invoice.Details)
            {
                detail.UnitSell = 0;
            }

            Assert.IsFalse(_validator.IsValid(_invoice));
        }
    }
}
