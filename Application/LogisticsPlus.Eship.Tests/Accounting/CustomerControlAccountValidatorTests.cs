﻿using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class CustomerControlAccountValidatorTests
	{
		private CustomerControlAccountValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new CustomerControlAccountValidator();
		}

		[Test]
		public void IsValidTest()
		{
			var account = new CustomerControlAccount
			{
				TenantId = GlobalTestInitializer.DefaultTenantId,
				Customer = new Customer(GlobalTestInitializer.DefaultCustomerId),
				AccountNumber = "asdf1234asdf",
				GenericCategory = "Category",
				Street1 = "123 Street",
				Street2 = "Apt 1",
				City = "City",
				State = "State",
				PostalCode = "12345",
				Description = "Some Account",
				CountryId = GlobalTestInitializer.DefaultCountryId,                
			};
			Assert.IsTrue(_validator.IsValid(account));
		}
	}
}
