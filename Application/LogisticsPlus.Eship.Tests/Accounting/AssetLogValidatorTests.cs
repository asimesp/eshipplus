﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
    [TestFixture]
    public class AssetLogValidatorTests
    {
        private AssetLogValidator _validator;


        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _validator = new AssetLogValidator();
        }

        [Test]
        public void HasRequiredDataTest()
        {
            var assets = new AssetSearch().FetchAssets(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
            var asset = assets[0];
            if(asset.Id == default(long))return;

            var criteria = new ShipmentViewSearchCriteria {ActiveUserId = GlobalTestInitializer.DefaultUserId,};
			var shipments = new ShipmentSearch().FetchShipmentsDashboardDto(criteria, GlobalTestInitializer.DefaultTenantId);
            var shipment = shipments[0];
            if (shipment.Id == default(long)) return;

            var log = new AssetLog
                          {
                              DateCreated = DateTime.Now,
                              LogDate = DateTime.Now,
                              Comment = DateTime.Now.ToShortDateString(),
                              MileageEngine = MileageEngine.LongitudeLatitude,
                              ShipmentId = shipment.Id,
                              AssetId = asset.Id,
                              Street1 = DateTime.Now.ToShortDateString(),
                              Street2 = DateTime.Now.ToShortDateString(),
                              City = DateTime.Now.ToShortDateString(),
                              State = "PA",
                              PostalCode = "16502",
                              CountryId = GlobalTestInitializer.DefaultCountryId, 
                              TenantId = GlobalTestInitializer.DefaultTenantId
                          };
            Assert.IsTrue(_validator.IsValid(log));
        }
    }
}
