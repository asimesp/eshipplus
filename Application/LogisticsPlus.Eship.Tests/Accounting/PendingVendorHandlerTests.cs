﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class PendingVendorHandlerTests
	{
		private PendingVendorView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new PendingVendorView();
			_view.Load();
		}

		[Test]
		public void CanSaveOrUpdate()
		{
			var all = new PendingVendorSearch().FetchPendingVendors(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var pendingVendor = all[0];
			pendingVendor.LoadCollections();
			_view.LockRecord(pendingVendor);
			pendingVendor.TakeSnapShot();
			var name = pendingVendor.Name;
			pendingVendor.Name = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff");
			PendingVendorLocation remitTo = null;
			if (!pendingVendor.Locations.Any(r => r.MainRemitToLocation && r.RemitToLocation) && pendingVendor.Locations.Any(r => r.RemitToLocation))
				remitTo = pendingVendor.Locations.First(r => r.RemitToLocation);
			if (remitTo != null) remitTo.MainRemitToLocation = true;
			_view.SaveRecord(pendingVendor);
			pendingVendor.TakeSnapShot();
			pendingVendor.Name = name;
			_view.SaveRecord(pendingVendor);
			_view.UnLockRecord(pendingVendor);
		}

		[Test]
		public void CanSaveAndDelete()
		{
			var pendingVendor = GetPendingVendor();
			_view.SaveRecord(pendingVendor);
			Assert.IsTrue(pendingVendor.Id != default(long));
			_view.DeleteRecord(pendingVendor);
			Assert.IsTrue(!new PendingVendor(pendingVendor.Id, false).KeyLoaded);
		}

		public PendingVendor GetPendingVendor()
		{
			var tenantId = GlobalTestInitializer.DefaultTenantId;
			var pendingVendor = new PendingVendor
			{
				Name = DateTime.Now.ToShortDateString(),
				VendorNumber = "-1",
				TrackingUrl = string.Empty,
				Notation = string.Empty,
				Approved = true,
				Rejected = false,
				TSACertified = true,
				CTPATNumber = string.Empty,
				PIPNumber = string.Empty,
				Scac = "FAKE",
				FederalIDNumber = string.Empty,
				BrokerReferenceNumber = string.Empty,
				BrokerReferenceNumberType = BrokerReferenceNumberType.EmployerIdentificationNumber,
				DateCreated = DateTime.Now,
				MC = string.Empty,
				DOT = string.Empty,
				HandlesAir = true,
				HandlesLessThanTruckload = true,
				HandlesPartialTruckload = true,
				HandlesRail = true,
				HandlesSmallPack = true,
				HandlesTruckload = true,
				IsCarrier = true,
				IsAgent = true,
				IsBroker = true,
				Notes = string.Empty,
				Insurances = new List<PendingVendorInsurance>(),
				Locations = new List<PendingVendorLocation>
			             		            	{
			             		            		new PendingVendorLocation
			             		            			{
			             		            				TenantId = GlobalTestInitializer.DefaultTenantId,
			             		            				CountryId = GlobalTestInitializer.DefaultCountryId,
			             		            				Primary = true
			             		            			},
			             		            		new PendingVendorLocation
			             		            			{
			             		            				TenantId = tenantId,
			             		            				CountryId = GlobalTestInitializer.DefaultCountryId
			             		            			}
			             		            	},
				Services = new List<PendingVendorService>(),
				Equipments = new List<PendingVendorEquipment>(),
				Documents = new List<PendingVendorDocument>(),
				TenantId = tenantId,
                CreatedByUserId = GlobalTestInitializer.DefaultUserId
            };
			foreach (var location in pendingVendor.Locations)
			{
				location.DateCreated = DateTime.Now;
				location.PendingVendor = pendingVendor;
				location.Contacts = new List<PendingVendorContact>
				                    	{
				                    		new PendingVendorContact
				                    			{
				                    				TenantId = location.TenantId,
				                    				PendingVendorLocation = location,
				                    				ContactType = ProcessorVars.RegistryCache[tenantId].ContactTypes[0],
													Primary = true,
				                    			}
				                    	};
			}
			pendingVendor.Insurances.Add(new PendingVendorInsurance
			{
				TenantId = tenantId,
				PendingVendor = pendingVendor,
				InsuranceType = ProcessorVars.RegistryCache[tenantId].InsuranceTypes[0],
				CoverageAmount = 10000m,
				CertificateHolder = "Holder",
				EffectiveDate = DateTime.Now.AddDays(-7),
				ExpirationDate = DateTime.Now,
				PolicyNumber = "19834X901823",
				SpecialInstruction = string.Empty,
				CarrierName = "CarrierName"
			});
			pendingVendor.Services.Add(new PendingVendorService
			{
				ServiceId = new ServiceSearch().FetchServices(new List<ParameterColumn>(), tenantId)[0].Id,
				TenantId = tenantId,
				PendingVendor = pendingVendor
			});
			pendingVendor.Equipments.Add(new PendingVendorEquipment
			{
				EquipmentTypeId = ProcessorVars.RegistryCache[tenantId].EquipmentTypes[0].Id,
				TenantId = tenantId,
				PendingVendor = pendingVendor
			});

			var columns = new List<ParameterColumn>();
			var column = RegistrySearchFields.Year.ToParameterColumn();
			column.DefaultValue = "2012";
			column.Operator = Operator.Equal;
			columns.Add(column);
			
			return pendingVendor;
		}

		internal class PendingVendorView : IPendingVendorView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public bool CreateAndSaveVendor { get; private set; }

			
			public Dictionary<int, string> BrokerReferenceTypes
			{
				set { }
			}

			public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<PendingVendor>> Save;
			public event EventHandler<ViewEventArgs<PendingVendor>> Delete;
			public event EventHandler<ViewEventArgs<PendingVendor>> Lock;
			public event EventHandler<ViewEventArgs<PendingVendor>> UnLock;
			public event EventHandler<ViewEventArgs<string>> PendingVendorServiceRepSearch;
			public event EventHandler<ViewEventArgs<PendingVendor>> LoadAuditLog;
			
			public event EventHandler<ViewEventArgs<Vendor>> SaveVendor;

			public string CopyFileForVendor(Vendor vendor, string sourceFileVirtualPath)
			{
				return string.Empty;
			}

			public void Set(Vendor vendor)
			{
			}

			public void DisplayPendingVendorServiceRep(User user)
			{
			}

			public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
			{

			}

			public void LogException(Exception ex)
			{

			}

			public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void FailedLock(Lock @lock)
			{

			}

			public void Set(PendingVendor pendingVendor)
			{

			}

			public void SetId(long id)
			{

			}

			public void Load()
			{
				var handler = new PendingVendorHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(PendingVendor pendingVendor)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<PendingVendor>(pendingVendor));
			}

			public void DeleteRecord(PendingVendor pendingVendor)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<PendingVendor>(pendingVendor));
			}

			public void LockRecord(PendingVendor pendingVendor)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<PendingVendor>(pendingVendor));
			}

			public void UnLockRecord(PendingVendor pendingVendor)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<PendingVendor>(pendingVendor));
			}
		}
	}
}
