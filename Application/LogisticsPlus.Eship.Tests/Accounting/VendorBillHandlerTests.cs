﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class VendorBillHandlerTests
	{
		private VendorBillView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new VendorBillView();
			_view.Load();
		}

		[Test]
		public void CanSaveOrUpdate()
		{
			var criteria = new VendorBillViewSearchCriteria { ActiveUserId = GlobalTestInitializer.DefaultUserId };
			var all = new VendorBillSearch().FetchVendorBillDashboardDtos(criteria,
																		  GlobalTestInitializer.DefaultTenantId);

			if (all.Count == 0) return;
			var bill = new VendorBill(all[0].Id);
			bill.LoadCollections();
			_view.LockRecord(bill);
			bill.TakeSnapShot();
			_view.SaveRecord(bill);
			_view.UnLockRecord(bill);
		}

		[Test]
		public void CanSaveAndDelete()
		{
			var bill = GetVendorBill();
			_view.SaveRecord(bill);
			Assert.IsTrue(bill.Id != default(long));
			_view.DeleteRecord(bill);
			Assert.IsTrue(!new VendorBill(bill.Id, false).KeyLoaded);
		}

		[Test]
		public void CanBatchUpdateAndDelete()
		{
			var vendor = new VendorSearch().FetchVendors(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId).FirstOrDefault();
			if (vendor == null) return;
			var vendorLocation = vendor.Locations.FirstOrDefault(l => l.RemitToLocation && l.MainRemitToLocation) ??
								 vendor.Locations.FirstOrDefault(l => l.Primary);

			var columns = new List<ParameterColumn>();
			var column = RegistrySearchFields.Active.ToParameterColumn();
			column.DefaultValue = true.ToString();
			column.Operator = Operator.Equal;
			columns.Add(column);

			var accountBucket =
                ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].AccountBuckets.
					FirstOrDefault();
			if (accountBucket == null) return;

            var chargeCode = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].ChargeCodes.FirstOrDefault();
			if (chargeCode == null) return;

		    var shipments = new ShipmentSearch().FetchShipmentsDashboardDto(new ShipmentViewSearchCriteria()
		        {
		            ActiveUserId = GlobalTestInitializer.DefaultUserId
		        },GlobalTestInitializer.DefaultTenantId);

			var bills = new List<VendorBill>();

			var bill1 = new VendorBill
							{
								TenantId = GlobalTestInitializer.DefaultTenantId,
								User = new User(GlobalTestInitializer.DefaultUserId),
								PostDate = DateUtility.SystemEarliestDateTime,
								ExportDate = DateUtility.SystemEarliestDateTime,
								DateCreated = DateTime.Now,
								VendorLocation = vendorLocation,
								DocumentDate = DateTime.Now,
								DocumentNumber = DateTime.Now.ToString(),
								BillType = InvoiceType.Invoice,
							};
			var detail1 = new VendorBillDetail
			{
				ReferenceNumber = shipments[0].ShipmentNumber,
				ReferenceType = DetailReferenceType.Shipment,
				Quantity = 1,
				UnitBuy = 10,
				AccountBucket = accountBucket,
				ChargeCode = chargeCode,
				Note = string.Empty,
				VendorBill = bill1,
				TenantId = bill1.TenantId
			};
			bill1.Details.Add(detail1);
			bills.Add(bill1);


			var bill2 = new VendorBill
			{
				TenantId = GlobalTestInitializer.DefaultTenantId,
				User = new User(GlobalTestInitializer.DefaultUserId),
				PostDate = DateUtility.SystemEarliestDateTime,
				ExportDate = DateUtility.SystemEarliestDateTime,
				DateCreated = DateTime.Now,
				VendorLocation = vendorLocation,
				DocumentDate = DateTime.Now,
				DocumentNumber = DateTime.Now.AddDays(1).ToString(),
				BillType = InvoiceType.Invoice,
			};
			var detail2 = new VendorBillDetail
			{
				ReferenceNumber = shipments[1].ShipmentNumber,
				ReferenceType = DetailReferenceType.Shipment,
				Quantity = 2,
				UnitBuy = 12,
				AccountBucket = accountBucket,
				ChargeCode = chargeCode,
				Note = string.Empty,
				VendorBill = bill2,
				TenantId = bill2.TenantId
			};
			bill2.Details.Add(detail2);
			bills.Add(bill2);

			_view.BatchImport(bills);
			Assert.IsTrue(!bill1.IsNew);
			Assert.IsTrue(!bill2.IsNew);

			var key1 = bill1.Id;
			var key2 = bill2.Id;

			foreach (var vendorBill in bills)
			{
				_view.DeleteRecord(vendorBill);
			}

			Assert.IsTrue(!new VendorBill(key1, false).KeyLoaded);
			Assert.IsTrue(!new VendorBill(key2, false).KeyLoaded);

		}

		private static VendorBill GetVendorBill()
		{
			var tenantId = GlobalTestInitializer.DefaultTenantId;
			var vendor = new VendorSearch().FetchVendors(new List<ParameterColumn>(), tenantId).FirstOrDefault() ?? new Vendor();
			var user = new User(GlobalTestInitializer.DefaultUserId);
            var chargeCode = ProcessorVars.RegistryCache[tenantId].ChargeCodes.FirstOrDefault() ?? new ChargeCode();
            var accountBucket = ProcessorVars.RegistryCache[tenantId].AccountBuckets.FirstOrDefault() ?? new AccountBucket();

			var shipmentCriteria = new ShipmentViewSearchCriteria { ActiveUserId = GlobalTestInitializer.DefaultUserId, };

			var shipment = new ShipmentSearch().FetchShipmentsDashboardDto(shipmentCriteria, tenantId).FirstOrDefault() ?? new ShipmentDashboardDto();

			var bill = new VendorBill
						{
							DocumentNumber = "00000",
							TenantId = tenantId,
							DateCreated = DateTime.Now,
							VendorLocation = vendor.Locations.FirstOrDefault(l => l.Primary),
							DocumentDate = DateTime.Now,
							BillType = InvoiceType.Invoice,
							Details = new List<VendorBillDetail>(),
							Posted = false,
							Exported = false,
							ExportDate = DateTime.Today,
							PostDate = DateTime.Today,
							User = user,
							ApplyToDocumentId = default(long)
						};

			bill.Details.Add(new VendorBillDetail
								{
									TenantId = GlobalTestInitializer.DefaultTenantId,
									UnitBuy = 1,
									VendorBill = bill,
									Note = "TEST",
									ReferenceNumber = shipment.ShipmentNumber,
									ReferenceType = DetailReferenceType.Shipment,
									Quantity = 1,
									ChargeCode = chargeCode,
									AccountBucket = accountBucket,
								});

			return bill;
		}

		internal class VendorBillView : IVendorBillView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public List<ChargeCode> ChargeCodes
			{
				set { }
			}

			public List<AccountBucket> AccountBuckets
			{
				set { }
			}

			public Dictionary<int, string> VendorBillDetailReferenceType
			{
				set { }
			}

			public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<VendorBill>> Save;
			public event EventHandler<ViewEventArgs<VendorBill>> Delete;
			public event EventHandler<ViewEventArgs<VendorBill>> Lock;
			public event EventHandler<ViewEventArgs<VendorBill>> UnLock;
			public event EventHandler<ViewEventArgs<VendorBill>> Search;
			public event EventHandler<ViewEventArgs<VendorBill>> LoadAuditLog;
			public event EventHandler<ViewEventArgs<string>> VendorSearch;
			public event EventHandler<ViewEventArgs<List<VendorBill>>> BatchSave;


			public void DisplaySearchResult(List<VendorBill> invoices)
			{

			}

			public void DisplayVendor(Vendor vendor)
			{

			}

			public void LogException(Exception ex)
			{

			}

			public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
			{
			}

			public void FailedLock(Lock @lock)
			{
			}

			public void Set(VendorBill invoice)
			{

			}


			public void Load()
			{
				var handler = new VendorBillHandler(this);
				handler.Initialize();
			}

			public void BatchImport(List<VendorBill> bills)
			{
				if (BatchSave != null)
					BatchSave(this, new ViewEventArgs<List<VendorBill>>(bills));
			}

			public void SaveRecord(VendorBill invoice)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<VendorBill>(invoice));
			}

			public void DeleteRecord(VendorBill invoice)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<VendorBill>(invoice));
			}

			public void LockRecord(VendorBill invoice)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<VendorBill>(invoice));
			}

			public void UnLockRecord(VendorBill invoice)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<VendorBill>(invoice));
			}
		}
	}
}
