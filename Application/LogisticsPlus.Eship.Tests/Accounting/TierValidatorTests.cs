﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class TierValidatorTests
	{
		private TierValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new TierValidator();
		}

		[Test]
		public void DuplicateTierNumberTest()
		{
			var all = new TierSearch().FetchTiers(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var tier = new Tier {Name = all[0].Name, TierNumber = all[0].TierNumber, TenantId = all[0].TenantId};
			Assert.IsTrue(_validator.DuplicateTierNumber(tier));
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var tier = new Tier();
			Assert.IsFalse(_validator.HasAllRequiredData(tier));
		}

		[Test]
		public void IsValidTest()
		{
			var tier = new Tier
			           	{
			           		Name = DateTime.Now.ToString(),
			           		TierNumber = DateTime.Now.ToLongDateString(),
			           		TenantId = GlobalTestInitializer.DefaultTenantId,
			           		TollFreeContactNumber = "1-800-564-7857",
			           		DateCreated = DateTime.Now,
			           		TierSupportEmails = string.Empty,
			           		GeneralNotificationEmails = string.Empty,
							PendingVendorNotificationEmails = string.Empty,
			           		LTLNotificationEmails = string.Empty,
			           		FTLNotificationEmails = string.Empty,
			           		SPNotificationEmails = string.Empty,
			           		RailNotificationEmails = string.Empty,
			           		AirNotificationEmails = string.Empty,
			           		LogoUrl = string.Empty,
			           		AdditionalBillOfLadingText = string.Empty,
			           	};
			Assert.IsTrue(_validator.IsValid(tier));
		}

		[Test]
		public void CanDeleteTest()
		{
			var all = new TierSearch().FetchTiers(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var type = new Tier(all[0].Id, false);
			Assert.IsTrue(type.KeyLoaded);
			_validator.Messages.Clear();
			var canDeleteTenant = _validator.CanDeleteTier(type);
			Assert.IsTrue(_validator.Messages.Count > 0);
			Assert.IsFalse(canDeleteTenant);
			_validator.Messages.PrintMessages();
		}
	}
}
