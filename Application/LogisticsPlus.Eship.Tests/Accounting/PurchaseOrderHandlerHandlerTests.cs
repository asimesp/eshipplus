﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
    [TestFixture]
    public class CustomerPurchaseOrderHandlerTests
    {
        private CustomerPurchaseOrderView _view;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _view = new CustomerPurchaseOrderView();
            _view.Load();
        }

        [Test]
        public void CanHandleSaveOrUpdate()
        {
            var criteria = new PurchaseOrderViewSearchCriteria
            {
                CustomerId = GlobalTestInitializer.DefaultCustomerId
            };

            var field = AccountingSearchFields.CustomerPurchaseOrders.FirstOrDefault(f => f.Name == "PurchaseOrderNumber");
            if (field == null)
            {
                Console.WriteLine("AccountingSearchFields.CustomerPurchaseOrders no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }

            var column = field.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            criteria.Parameters.Add(column);

            var all = new CustomerPurchaseOrderSearch().FetchCustomerPurchaseOrders(criteria, GlobalTestInitializer.DefaultTenantId);
            if (!all.Any()) return;
            var customerPurchaseOrder = all[0];
            _view.LockRecord(customerPurchaseOrder);
            customerPurchaseOrder.TakeSnapShot();
            var number = customerPurchaseOrder.PurchaseOrderNumber;
            customerPurchaseOrder.PurchaseOrderNumber = DateTime.Now.ToShortDateString();
            _view.SaveRecord(customerPurchaseOrder);
            customerPurchaseOrder.TakeSnapShot();
            customerPurchaseOrder.PurchaseOrderNumber = number;
            _view.SaveRecord(customerPurchaseOrder);
            _view.UnLockRecord(customerPurchaseOrder);
        }

        [Test]
        public void CanHandleSaveAndDelete()
        {
            var customerPurchaseOrder = new CustomerPurchaseOrder
                        {
                           TenantId = GlobalTestInitializer.DefaultTenantId,
                           Customer = new Customer(GlobalTestInitializer.DefaultCustomerId),
                           PurchaseOrderNumber = "asdfffffsdssss", 
                           ValidPostalCode = "16506", 
                           ApplyOnOrigin = true,
                           ApplyOnDestination = false,
                           MaximumUses = 2,
                           CurrentUses = 1,
                           ExpirationDate = DateTime.Now.AddDays(20),
                           CountryId = GlobalTestInitializer.DefaultCountryId
                        };

            _view.SaveRecord(customerPurchaseOrder);
            _view.LockRecord(customerPurchaseOrder);
            Assert.IsTrue(customerPurchaseOrder.Id != default(long));
            _view.DeleteRecord(customerPurchaseOrder);
            Assert.IsTrue(!new CustomerPurchaseOrder(customerPurchaseOrder.Id, false).KeyLoaded);

        }

        internal class CustomerPurchaseOrderView : ICustomerPurchaseOrderView
        {
            public User ActiveUser
            {
                get { return GlobalTestInitializer.ActiveUser; }
            }

            public List<Country> Countries
            {
                set { Assert.IsTrue(value.Count > 0); }
            }

            public event EventHandler Loading;
            public event EventHandler<ViewEventArgs<CustomerPurchaseOrder>> Save;
            public event EventHandler<ViewEventArgs<CustomerPurchaseOrder>> Delete;
            public event EventHandler<ViewEventArgs<CustomerPurchaseOrder>> Lock;
            public event EventHandler<ViewEventArgs<CustomerPurchaseOrder>> UnLock;
            public event EventHandler<ViewEventArgs<PurchaseOrderViewSearchCriteria>> Search;
            public event EventHandler<ViewEventArgs<List<CustomerPurchaseOrder>>> BatchImport;
            public event EventHandler<ViewEventArgs<string>> CustomerSearch;

            public void DisplaySearchResult(List<CustomerPurchaseOrder> customerPurchaseOrders)
            {
                Assert.IsTrue(customerPurchaseOrders.Count > 0);
            }

            public void LogException(Exception ex)
            {
                    
            }

            public void DisplayMessages(IEnumerable<ValidationMessage> messages)
            {
                messages.PrintMessages();
                Assert.IsTrue(messages.Where(m => m.Type != ValidateMessageType.Information).Count() == 0);
            }

            public void DisplayCustomer(Customer customer)
            {
              
            }

            public void FailedLock(Lock @lock)
            {
                
            }

            public void Load()
            {
                var handler = new CustomerPurchaseOrderHandler(this);
                handler.Initialize();
            }

            public void SaveRecord(CustomerPurchaseOrder customerPurchaseOrder)
            {
                if (Save != null)
                    Save(this, new ViewEventArgs<CustomerPurchaseOrder>(customerPurchaseOrder));
            }

            public void DeleteRecord(CustomerPurchaseOrder customerPurchaseOrder)
            {
                if (Delete != null)
                    Delete(this, new ViewEventArgs<CustomerPurchaseOrder>(customerPurchaseOrder));
            }

            public void SearchRecord(string criteria, long tenantId, DateTime expirationDate)
            {
                var searchCriteria = new PurchaseOrderViewSearchCriteria();
                if (Search != null)
                    Search(this, new ViewEventArgs<PurchaseOrderViewSearchCriteria>(searchCriteria));
            }

            public void LockRecord(CustomerPurchaseOrder customerPurchaseOrder)
            {
                if (Lock != null)
                    Lock(this, new ViewEventArgs<CustomerPurchaseOrder>(customerPurchaseOrder));
            }

            public void UnLockRecord(CustomerPurchaseOrder customerPurchaseOrder)
            {
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<CustomerPurchaseOrder>(customerPurchaseOrder));
            }
        }
    }
}
