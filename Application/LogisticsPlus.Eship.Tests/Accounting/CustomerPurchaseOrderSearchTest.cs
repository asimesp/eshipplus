﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
    [TestFixture]
    public class CustomerPurchaseOrderTests
    {
        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

        [Test]
        public void CanFetchPurchaseOrders()
        {
            var criteria = new PurchaseOrderViewSearchCriteria
            {
                CustomerId = GlobalTestInitializer.DefaultCustomerId
            };

            var field = AccountingSearchFields.CustomerPurchaseOrders.FirstOrDefault(f => f.Name == "PurchaseOrderNumber");
            if (field == null)
            {
                Console.WriteLine("AccountingSearchFields.CustomerPurchaseOrders no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }

            var column = field.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            criteria.Parameters.Add(column);

            var countryColumn = OperationsSearchFields.NonAliasedCountryCode.ToParameterColumn();
            countryColumn.DefaultValue = SearchUtilities.WildCard;
            countryColumn.Operator = Operator.Contains;
            criteria.Parameters.Add(countryColumn);

            var results = new CustomerPurchaseOrderSearch().FetchCustomerPurchaseOrders(criteria, GlobalTestInitializer.DefaultTenantId);

            Assert.IsTrue(results.Any());
        }

        [Test]
        public void CanFetchPurchaseOrdersForValidation()
        {
           var criteria = new PurchaseOrderViewSearchCriteria
            {
                CustomerId = GlobalTestInitializer.DefaultCustomerId
            };

            var field = AccountingSearchFields.CustomerPurchaseOrders.FirstOrDefault(f => f.Name == "PurchaseOrderNumber");
            if (field == null)
            {
                Console.WriteLine("AccountingSearchFields.CustomerPurchaseOrders no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }

            var column = field.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            criteria.Parameters.Add(column);

            var purchaseOrders = new CustomerPurchaseOrderSearch().FetchCustomerPurchaseOrders(criteria, GlobalTestInitializer.DefaultTenantId);
            if(!purchaseOrders.Any())return;

            var purchaseOrder = purchaseOrders.First();

           
            var result = new CustomerPurchaseOrderSearch()
                .FetchCustomerPurchaseOrder(GlobalTestInitializer.DefaultTenantId, purchaseOrder.Customer.Id, purchaseOrder.PurchaseOrderNumber);
            Assert.IsNotNull(result);

        }
    }
}
