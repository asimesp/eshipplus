﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class CommissionPaymentValidatorTests
	{
		private CommissionsToBePaidValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new CommissionsToBePaidValidator();
		}

		[Test]
		public void IsValidTest()
		{

			var tenantId = GlobalTestInitializer.DefaultTenantId;
            var customer = new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), GlobalTestInitializer.DefaultTenantId).First(c => c.SalesRepresentative != null);

			var shipmentCriteria = new ShipmentViewSearchCriteria { ActiveUserId = GlobalTestInitializer.DefaultUserId, };

			var shipment = new ShipmentSearch().FetchShipmentsDashboardDto(shipmentCriteria, tenantId).FirstOrDefault() ?? new ShipmentDashboardDto();

			var commission = new CommissionPayment
							{
								DateCreated = DateTime.Now,
								PaymentDate =  DateTime.Now,
								AmountPaid = 0,
								Type = CommissionPaymentType.Payment,
								ReferenceNumber = shipment.ShipmentNumber,
								SalesRepresentativeId = customer.SalesRepresentative.Id,
                                AdditionalEntityCommission = false,
								TenantId = tenantId,
							};

			var isValid = _validator.IsValid(commission);
			_validator.Messages.PrintMessages();
			Assert.IsTrue(isValid);
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var commission = new CommissionPayment();
			Assert.IsFalse(_validator.HasAllRequiredData(commission));
		}
	}
}
