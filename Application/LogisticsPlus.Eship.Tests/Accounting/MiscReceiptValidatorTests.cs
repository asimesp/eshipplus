﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
    [TestFixture]
    public class MiscReceiptValidatorTests
    {
        private MiscReceiptValidator _validator;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _validator = new MiscReceiptValidator();
        }

        [Test]
        public void IsValidTest()
        {
            var miscReceipt = new MiscReceipt
            {
                ShipmentId = GlobalTestInitializer.DefaultShipmentId,
                LoadOrderId = default(long),
                UserId = GlobalTestInitializer.DefaultUserId,
                CustomerId = GlobalTestInitializer.DefaultCustomerId,
                AmountPaid = 10.99m,
                PaymentGatewayType = PaymentGatewayType.NotApplicable,
                GatewayTransactionId = DateTime.Now.ToString(),
                PaymentDate = DateTime.Now,
                Reversal = false,
                TenantId = GlobalTestInitializer.DefaultTenantId,
                MiscReceiptApplications = new List<MiscReceiptApplication>(),
                PaymentProfileId = string.Empty
            };
            var isValid = _validator.IsValid(miscReceipt);
            _validator.Messages.PrintMessages();
            Assert.IsTrue(isValid);
        }

        [Test]
        public void HasAllRequiredDataTest()
        {
            var miscReceipt = new MiscReceipt();
            Assert.IsFalse(_validator.HasAllRequiredData(miscReceipt));
        }
    }
}
