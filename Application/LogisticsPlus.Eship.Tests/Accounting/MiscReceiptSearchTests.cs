﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
    [TestFixture]
    public class MiscReceiptSearchTests
    {
        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

        [Test]
        public void CanFetchMiscReceipts()
        {
            var miscReceipts = new MiscReceiptSearch().FetchMiscReceipts(new MiscReciptViewSearchCriteria(), GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(miscReceipts.Count > 0);
        }

        [Test]
        public void CanGetMiscReceiptMaximumRefundableAmount()
        {
            var search = new MiscReceiptSearch();
            var miscReceiptDtos = search.FetchMiscReceipts(new MiscReciptViewSearchCriteria(), GlobalTestInitializer.DefaultTenantId);
            var miscReceiptDto = miscReceiptDtos.FirstOrDefault(m => m.OriginalMiscReceiptId != default(long)) ?? new MiscReceiptViewSearchDto();
            if (miscReceiptDto.Id == default (long)) return;

            var miscReceipt = new MiscReceipt(miscReceiptDto.OriginalMiscReceiptId);
            var result = search.GetMiscReceiptAmountLeftToBeAppliedOrRefunded(miscReceipt.Id, miscReceipt.TenantId);
            Console.WriteLine("Amount Left To Be Applied Or Refunded Receipt:{0}", result);
        }

        [Test]
        public void CanFetchMiscReceiptsApplicableToPostedInvoice()
        {
            Assert.IsTrue(new MiscReceiptSearch().FetchMiscReceiptsApplicableToPostedInvoice(GlobalTestInitializer.DefaultPostedInvoiceWithApplicableMiscReceiptsId, GlobalTestInitializer.DefaultTenantId).Any());
        }

        [Test]
        public void CanGetMiscReceiptsAmountPaidToLoadOrder()
        {
            var search = new MiscReceiptSearch();
            var miscReceiptDtos = search.FetchMiscReceipts(new MiscReciptViewSearchCriteria(), GlobalTestInitializer.DefaultTenantId);
            var miscReceiptDto = miscReceiptDtos.FirstOrDefault(m => m.LoadOrderId != default(long)) ?? new MiscReceiptViewSearchDto();
            if (miscReceiptDto.Id == default(long)) return;

            var result = search.GetMiscReceiptsAmountPaidToLoadOrder(miscReceiptDto.LoadOrderId, GlobalTestInitializer.DefaultTenantId);
            Console.WriteLine("Amount Paid To Load Order {0}: {1}", miscReceiptDto.LoadOrderNumber, result);
        }

        [Test]
        public void CanFetchMiscReceiptsRelatedToLoadOrder()
        {
            var search = new MiscReceiptSearch();
            var miscReceiptDtos = search.FetchMiscReceipts(new MiscReciptViewSearchCriteria(), GlobalTestInitializer.DefaultTenantId);
            var miscReceiptDto = miscReceiptDtos.FirstOrDefault(m => m.LoadOrderId != default(long)) ?? new MiscReceiptViewSearchDto();
            if (miscReceiptDto.Id == default(long)) return;

            var results = search.FetchMiscReceiptsRelatedToLoadOrder(miscReceiptDto.LoadOrderId, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(results.Any());
        }

        [Test]
        public void CanGetMiscReceiptsAmountPaidToShipment()
        {
            var search = new MiscReceiptSearch();
            var miscReceiptDtos = search.FetchMiscReceipts(new MiscReciptViewSearchCriteria(), GlobalTestInitializer.DefaultTenantId);
            var miscReceiptDto = miscReceiptDtos.FirstOrDefault(m => m.ShipmentId != default(long)) ?? new MiscReceiptViewSearchDto();
            if (miscReceiptDto.Id == default(long)) return;

            var result = search.GetMiscReceiptsAmountPaidToShipment(miscReceiptDto.ShipmentId, GlobalTestInitializer.DefaultTenantId);
            Console.WriteLine("Amount Paid To Shipment {0}: {1}", miscReceiptDto.ShipmentNumber, result);
        }

        [Test]
        public void CanFetchMiscReceiptsRelatedToShipment()
        {
            var search = new MiscReceiptSearch();
            var miscReceiptDtos = search.FetchMiscReceipts(new MiscReciptViewSearchCriteria(), GlobalTestInitializer.DefaultTenantId);
            var miscReceiptDto = miscReceiptDtos.FirstOrDefault(m => m.ShipmentId != default(long)) ?? new MiscReceiptViewSearchDto();
            if (miscReceiptDto.Id == default(long)) return;

            var results = search.FetchMiscReceiptsRelatedToShipment(miscReceiptDto.ShipmentId, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(results.Any());
        }

        [Test]
        public void CanFetchMiscReceiptsUsingPaymentProfile()
        {
            var search = new MiscReceiptSearch();
            var miscReceiptDtos = search.FetchMiscReceipts(new MiscReciptViewSearchCriteria(), GlobalTestInitializer.DefaultTenantId);
            var miscReceiptDto = miscReceiptDtos.FirstOrDefault(m => !string.IsNullOrEmpty(m.PaymentProfileId)) ?? new MiscReceiptViewSearchDto();
            if (miscReceiptDto.Id == default(long)) return;

            var results = search.FetchMiscReceiptsUsingPaymentProfile(miscReceiptDto.PaymentProfileId, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(results.Any());
        }
    }
}
