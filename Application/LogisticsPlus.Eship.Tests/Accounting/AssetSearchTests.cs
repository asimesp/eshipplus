﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
    [TestFixture]
    public class AssetSearchTests
    {
        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

        [Test]
        public void CanFetchAssets()
        {
            var assets = new AssetSearch().FetchAssets(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(assets.Count > 0);
        }

        [Test]
        public void CanFetchAssetsByAssetNumber()
        {
            var search = new AssetSearch();
            var assets = search.FetchAssets(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
            if(assets.Count == 0)return;

            var asset = assets[0];
            search.FetchAssetByAssetNumber(asset.AssetNumber, GlobalTestInitializer.DefaultTenantId);
        }

        [Test]
        public void CanFetchAssetsForFinder()
        {
            var columns = new List<ParameterColumn>();

            var field = OperationsSearchFields.Description;

            var column = field.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            columns.Add(column);

            var results = new AssetSearch().FetchAssets(columns, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(results.Any());

        }
    }
}
