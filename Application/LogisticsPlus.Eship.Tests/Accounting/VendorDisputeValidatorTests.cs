﻿using System;
using System.Linq;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.Core.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
    [TestFixture]
    public class VendorDisputeValidatorTests
    {
        private VendorDisputeValidator _validator;
        private VendorDispute _dispute;
        private VendorDisputeViewSearchCriteria _criteria;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _validator = new VendorDisputeValidator();

            _criteria = new VendorDisputeViewSearchCriteria { ActiveUserId = GlobalTestInitializer.DefaultUserId };

            var tenantId = GlobalTestInitializer.DefaultTenantId;
            var user = new User(GlobalTestInitializer.DefaultUserId);
            var vendor = new VendorSearch().FetchVendors(new List<ParameterColumn>(), tenantId).FirstOrDefault() ?? new Vendor();

            _dispute = new VendorDispute(10000)
            {
                CreatedByUser = user,
                ResolvedByUser = user,
                TenantId = tenantId,
                DateCreated = DateTime.Now,
                ResolutionDate = DateTime.Today,
                DisputerReferenceNumber = "123456789",
                IsResolved = true,
                ResolutionComments = "Resolution comments",
				DisputeComments = "Don't like it!",
                Vendor = vendor,
				VendorDisputeDetails = new List<VendorDisputeDetail>()
            };

            _dispute.VendorDisputeDetails.Add(new VendorDisputeDetail
            {
                TenantId = GlobalTestInitializer.DefaultTenantId,
                ChargeLineId = 1,
                ChargeLineIdType = 0,
                VendorDispute = _dispute,
                OriginalChargeAmount = (decimal)100.1234,
                RevisedChargeAmount = (decimal)100.1234
            });
        }

        [Test]
        public void DuplicateDisputerReferenceNumberTest()
        {
            var all = new VendorDisputeSearch().FetchVendorDisputes(_criteria, GlobalTestInitializer.DefaultTenantId);
            if (all.Count == 0) return;

            var vendorDispute = new VendorDispute(all[0].Id);
            var newVendorDispute = new VendorDispute
            {
                DisputerReferenceNumber = vendorDispute.DisputerReferenceNumber,
                VendorDisputeDetails = vendorDispute.VendorDisputeDetails,
                TenantId = vendorDispute.TenantId
            };

            Assert.IsTrue(_validator.DuplicateDisputerReferenceNumber(newVendorDispute));
            newVendorDispute.VendorDisputeDetails = null;
            Assert.IsTrue(_validator.DuplicateDisputerReferenceNumber(newVendorDispute));
            _validator.Messages.PrintMessages();

            if (vendorDispute.VendorDisputeDetails.Count > 1)
            {
                newVendorDispute.VendorDisputeDetails.Add(vendorDispute.VendorDisputeDetails[0]);
                Assert.IsTrue(_validator.DuplicateDisputerReferenceNumber(newVendorDispute));
            }
        }

		[Test]
		public void DuplicateVendorDisputeforCharges()
		{
			var all = new VendorDisputeSearch().FetchVendorDisputes(_criteria, GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var unResolved = all.Select(p => p).FirstOrDefault(d => d.IsResolved == false);
			var vendorDispute = new VendorDispute(unResolved.Id);

			var details = vendorDispute.VendorDisputeDetails.FirstOrDefault();
			var disputeDetails = new VendorDisputeDetail()
				{
					ChargeLineIdType = details.ChargeLineIdType,
					OriginalChargeAmount = details.OriginalChargeAmount,
					ChargeLineId = details.ChargeLineId,
					TenantId = details.TenantId

				};
			Assert.IsTrue(_validator.DuplicateDisputeCharge(disputeDetails));
				
		}

        [Test]
        public void HasAllRequiredDataTest()
        {
            var hasAllRequiredData = _validator.HasAllRequiredData(_dispute);
            if (!hasAllRequiredData) _validator.Messages.PrintMessages();
            Assert.IsTrue(hasAllRequiredData);
        }

        [Test]
        public void IsValidTest()
        {
            Assert.IsTrue(_validator.IsValid(_dispute));
        }
    }
}