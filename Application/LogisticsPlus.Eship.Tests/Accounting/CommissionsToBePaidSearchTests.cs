﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	public class CommissionsToBePaidSearchTests
	{
		private CommissionsToBePaidViewSearchCriteria _criteria;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchByWildCard()
		{
			_criteria = new CommissionsToBePaidViewSearchCriteria
			{
				ActiveUserId = GlobalTestInitializer.DefaultUserId,
				Parameters = new List<ParameterColumn>
									{
										new SearchField{Name = "ShipmentDateCreated", DataType = SqlDbType.DateTime}.ToParameterColumn()
									},
				OnlyCommsWithNoPayment = true,
			};
			_criteria.Parameters[0].Operator = Operator.Between;
			_criteria.Parameters[0].DefaultValue = DateTime.Now.AddMonths(-60).FormattedShortDateAlt();
			_criteria.Parameters[0].DefaultValue2 = DateTime.Now.FormattedShortDateAlt();
		
			var dtos = new CommissionsToBePaidSearch().FetchCommissionsToBePaid(_criteria, GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(dtos.Any());
		}
	}
}
