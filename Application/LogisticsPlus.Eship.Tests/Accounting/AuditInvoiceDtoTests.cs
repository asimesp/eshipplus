﻿using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class AuditInvoiceDtoTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchInvoiceDtoForAudit()
		{
			//SAFE REFERENCE NUMBER (type shipment) TO USE FOR TEST: '10956'
			new AuditInvoiceDto().FetchAuditInvoiceDtos("10956", DetailReferenceType.Shipment, GlobalTestInitializer.DefaultTenantId);
		}
	}
}
