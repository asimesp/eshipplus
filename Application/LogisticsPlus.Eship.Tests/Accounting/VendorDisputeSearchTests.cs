﻿using System;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
    public class VendorDisputeSearchTests
    {
        private VendorDisputeViewSearchCriteria _criteria;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _criteria = new VendorDisputeViewSearchCriteria { ActiveUserId = GlobalTestInitializer.DefaultUserId };
        }

        [Test]
        public void CanFetchVendorDisputes()
        {
            var vendorDisputes = new VendorDisputeSearch().FetchVendorDisputes(_criteria, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(vendorDisputes.Count > 0);
        }

        [Test]
        public void CanFetchVendorBillDashboardByDocumentNumber()
        {
            new VendorDisputeSearch().FetchVendorDisputeByDisputerReferenceNumber(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId);
        }
    }
}