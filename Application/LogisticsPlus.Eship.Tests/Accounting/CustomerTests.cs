﻿using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class CustomerTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanLoadWithExceptions()
		{
            var all = new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var customer = all[0];
			customer.LoadCollections();
		}
	}
}
