﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class VendorValidatorTests
	{
		private VendorValidator _validator;
		private Vendor _vendor;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new VendorValidator();

			if (_vendor != null) return;

			var tenantId = GlobalTestInitializer.DefaultTenantId;
			_vendor = new Vendor
						{
							Name = DateTime.Now.ToShortDateString(),
							VendorNumber = "-1",
							TrackingUrl = string.Empty,
							Notation = string.Empty,
							LogoUrl = string.Empty,
							Active = true,
							Communication = null,
							TSACertified = true,
							Scac = "FAKE",
							FederalIDNumber = string.Empty,
							CTPATNumber = string.Empty,
							PIPNumber = string.Empty,
							BrokerReferenceNumber = string.Empty,
							BrokerReferenceNumberType = BrokerReferenceNumberType.EmployerIdentificationNumber,
							DateCreated = DateTime.Now,
							LastAuditDate = DateTime.Now,
							MC = string.Empty,
							DOT = string.Empty,
							HandlesAir = true,
							HandlesLessThanTruckload = true,
							HandlesPartialTruckload = true,
							HandlesRail = true,
							HandlesSmallPack = true,
							HandlesTruckload = true,
							IsCarrier = true,
							IsAgent = true,
							IsBroker = true,
							Insurances = new List<VendorInsurance>(),
							VendorPackageCustomMappings = new List<VendorPackageCustomMapping>(),
							Locations = new List<VendorLocation>
			          		            	{
			          		            		new VendorLocation
			          		            			{
			          		            				TenantId = GlobalTestInitializer.DefaultTenantId,
			          		            				CountryId = GlobalTestInitializer.DefaultCountryId,
			          		            				Primary = true
			          		            			},
			          		            		new VendorLocation
			          		            			{
			          		            				TenantId = tenantId,
			          		            				CountryId = GlobalTestInitializer.DefaultCountryId
			          		            			}
			          		            	},
							Services = new List<VendorService>(),
							Equipments = new List<VendorEquipment>(),
							NoServiceDays = new List<VendorNoServiceDay>(),
							Ratings = null,
							TenantId = tenantId,
						};
			foreach (var location in _vendor.Locations)
			{
				location.DateCreated = DateTime.Now;
				location.Vendor = _vendor;
				location.Contacts = new List<VendorContact>
				                    	{
				                    		new VendorContact
				                    			{
				                    				TenantId = location.TenantId,
				                    				VendorLocation = location,
				                    				ContactType = ProcessorVars.RegistryCache[tenantId].ContactTypes[0],
													Primary = true,
				                    			}
				                    	};
			}
			_vendor.Services.Add(new VendorService
									{
										ServiceId = new ServiceSearch().FetchServices(new List<ParameterColumn>(), tenantId)[0].Id,
										TenantId = tenantId,
										Vendor = _vendor
									});
			_vendor.Insurances.Add(new VendorInsurance
			                       	{
			                       		TenantId = tenantId,
			                       		Vendor = _vendor,
                                        InsuranceType = ProcessorVars.RegistryCache[tenantId].InsuranceTypes[0],
			                       		CoverageAmount = 10000m,
			                       		CertificateHolder = "Holder",
			                       		EffectiveDate = DateTime.Now.AddDays(-7),
			                       		ExpirationDate = DateTime.Now,
			                       		PolicyNumber = "19834X901823",
			                       		SpecialInstruction = string.Empty,
			                       		CarrierName = "CarrierName"
			                       	});
			_vendor.VendorPackageCustomMappings.Add(new VendorPackageCustomMapping
				{
					TenantId = tenantId,
					Vendor = _vendor,
					PackageType = ProcessorVars.RegistryCache[tenantId].PackageTypes[0],
					VendorCode = "PLT",
				});
			_vendor.Equipments.Add(new VendorEquipment
									{
                                        EquipmentTypeId = ProcessorVars.RegistryCache[tenantId].EquipmentTypes[0].Id,
										TenantId = tenantId,
										Vendor = _vendor
									});
		}

		[Test]
		public void IsValidTest()
		{
			Assert.IsTrue(_validator.IsValid(_vendor));
		}

		[Test]
		public void HasRequiredDataTest()
		{
			var hasAllRequiredData = _validator.HasAllRequiredData(_vendor);
			if (!hasAllRequiredData) _validator.Messages.PrintMessages();
			Assert.IsTrue(hasAllRequiredData);
		}

		[Test]
		public void DuplicateVendorNumberTest()
		{
            var all = new VendorSearch().FetchVendors(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var number = _vendor.VendorNumber;
			_vendor.VendorNumber = all[0].VendorNumber;
			Assert.IsTrue(_validator.DuplicateVendorNumber(_vendor));
            _vendor.VendorNumber = number;
		}

		[Test]
		public void DuplicateVendorLocationNumberTest()
		{
			var all = new VendorSearch().FetchVendors(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var number = _vendor.Locations[0].LocationNumber;
			_vendor.Locations[0].LocationNumber = all[0].Locations[0].LocationNumber;
			Assert.IsTrue(_validator.DuplicateVendorLocationNumber(_vendor.Locations[0]));
			_vendor.Locations[0].LocationNumber = number;
		}

        [Test]
        public void DuplicateVendorMcTest()
        {
            var all = new VendorSearch().FetchVendors(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
            if (all.Count == 0) return;
            var vendorWithMc = all.FirstOrDefault(p => !string.IsNullOrEmpty(p.MC));
            var mc = _vendor.MC;
            _vendor.MC = vendorWithMc == null ? all[0].MC : vendorWithMc.MC;
            Assert.IsTrue((vendorWithMc == null && !_validator.DuplicateVendorMc(_vendor)) || _validator.DuplicateVendorMc(_vendor));
            _vendor.MC = mc;
        }

        [Test]
        public void DuplicateVendorDotTest()
        {
            var all = new VendorSearch().FetchVendors(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
            if (all.Count == 0) return;
            var vendorWithDot = all.FirstOrDefault(p => !string.IsNullOrEmpty(p.DOT) && p.Id != _vendor.Id);
            var dot = _vendor.DOT;
            _vendor.DOT = vendorWithDot == null ? all[0].DOT : vendorWithDot.DOT;
            Assert.IsTrue((vendorWithDot == null && !_validator.DuplicateVendorDot(_vendor)) || _validator.DuplicateVendorDot(_vendor));
            _vendor.DOT = dot;
        }

		[Test]
		public void DuplicateVendorServiceTest()
		{
			var vs = new VendorService();
			Assert.IsFalse(_validator.VendorServiceExists(vs));
            var all = new VendorSearch().FetchVendors(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId)
				.Where(v => v.Services.Count > 0)
				.ToList();
			if (!all.Any()) return;
			vs = all[0].Services[0];
			Assert.IsTrue(_validator.VendorServiceExists(vs));
		}

		[Test]
		public void DuplicateVendorEquipmentTest()
		{
			var ve = new VendorEquipment();
			Assert.IsFalse(_validator.VendorEquipmentExists(ve));
            var all = new VendorSearch().FetchVendors(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId)
				.Where(v => v.Equipments.Count > 0)
				.ToList();
			if (!all.Any()) return;
			ve = all[0].Equipments[0];
			Assert.IsTrue(_validator.VendorEquipmentExists(ve));
		}

		[Test]
		public void CanDeleteTest()
		{
            var all = new VendorSearch().FetchVendors(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var vendor = new Vendor(all[0].Id, false);
			Assert.IsTrue(vendor.KeyLoaded);
			_validator.Messages.Clear();
			_validator.CanDeleteVendor(vendor);
			_validator.Messages.PrintMessages();
		}

		[Test]
		public void CanDeleteVendorLocationTest()
		{
            var all = new VendorSearch().FetchVendors(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var vendor = all.FirstOrDefault(v => v.Locations.Any());
			if (vendor == null) return;
			_validator.Messages.Clear();
			_validator.CanDeleteVendorLocation(vendor.Locations[0]);
			_validator.Messages.PrintMessages();
		}
	}
}
