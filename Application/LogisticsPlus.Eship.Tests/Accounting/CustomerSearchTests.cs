﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class CustomerSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchByWildCard()
		{
            var customers = new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(customers.Count > 0);
		}

		[Test]
		public void CanFetchByCustomerNumber()
		{
            var all = new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var customer = new CustomerSearch().FetchCustomerByNumber(all[0].CustomerNumber,
			                                                          GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(customer.Id != 0);
		}

		[Test]
		public void CanFetchCustomersWithRating()
		{
            var columns = new List<ParameterColumn>();

            var field = AccountingSearchFields.CustomerNumber;
            if (field != null) columns.Add(new ParameterColumn { DefaultValue = SearchUtilities.WildCard, ReportColumnName = field.DisplayName, Operator = Operator.Contains });

			var customers = new CustomerSearch().FetchCustomersWithRating(columns, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(customers.Any());
		}

		[Test]
		public void CanFetchCustomersWithCommunication()
		{
		    var columns = new List<ParameterColumn>();

            var field = AccountingSearchFields.CustomerNumber;
            if (field != null) columns.Add(new ParameterColumn { DefaultValue = SearchUtilities.WildCard, ReportColumnName = field.DisplayName, Operator = Operator.Contains });

            var customers = new CustomerSearch().FetchCustomersWithCommunication(columns, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(customers.Any());
		}

		[Test]
		public void CanFetchCustomers()
		{
            new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), GlobalTestInitializer.DefaultTenantId);
		}

		[Test]
		public void CanFetchCustomer()
		{
			new CustomerSearch().FetchCustomerByCommunicationConnectGuid(new Guid("9DD08C2C-0753-45F6-8D5B-8553F635E0E2"),
			                                                             GlobalTestInitializer.DefaultTenantId);
		}

		[Test]
		public void CanFetchCustomerCommunicationsWithEdiOrFtpEnabled()
		{
			new CustomerSearch().FetchCustomerCommunicationsWithEdiOrFtp();
		}

		[Test]
		public void CanFetchCustomerCommunicationsWithDocDelivery()
		{
			new CustomerSearch().FetchCustomerCommunicationsWithDocDelivery();
		}

        [Test]
        public void CanFetchActiveCustomersWithSearchCriteria()
        {
            var criteria = new CustomerViewSearchCriteria {OnlyActiveCustomers = true};
            var field = AccountingSearchFields.CustomerNumber;
            if (field != null) criteria.Parameters.Add(new ParameterColumn { DefaultValue = SearchUtilities.WildCard, ReportColumnName = field.DisplayName, Operator = Operator.Contains });

            var customers = new CustomerSearch().FetchCustomers(criteria, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(customers.Any());
        }

        [Test]
        public void CanFetchAnyCustomersWithSearchCriteria()
        {
            var criteria = new CustomerViewSearchCriteria { OnlyActiveCustomers = false };
            var field = AccountingSearchFields.CustomerNumber;
            if (field != null) criteria.Parameters.Add(new ParameterColumn { DefaultValue = SearchUtilities.WildCard, ReportColumnName = field.DisplayName, Operator = Operator.Contains });

            var customers = new CustomerSearch().FetchCustomers(criteria, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(customers.Any());
        }

        [Test]
        public void CanFetchCustomerViewSearchDtos()
        {
            var customerDtos = new CustomerSearch().FetchCustomerViewSearchDtos(new CustomerViewSearchCriteria(), GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(customerDtos.Count > 0);
        }
	}
}
