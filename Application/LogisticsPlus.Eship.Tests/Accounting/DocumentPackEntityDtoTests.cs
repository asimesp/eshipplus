﻿using System;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class DocumentPackEntityDtoTests
	{
		private DocumentPackEntityDtoSearchCriteria _criteria;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();

			_criteria = new DocumentPackEntityDtoSearchCriteria {ActiveUserId = GlobalTestInitializer.DefaultUserId};
		}

		[Test]
		public void CanFetchForInvoices()
		{
			var dto = new DocumentPackEntityDto();
			_criteria.Type = DocumentPackEntityType.Invoice;
			dto.RetrieveEntities(_criteria, GlobalTestInitializer.DefaultUserId, GlobalTestInitializer.DefaultTenantId);
		}

		[Test]
		public void CanFetchForShipments()
		{
			var dto = new DocumentPackEntityDto();
			_criteria.Type = DocumentPackEntityType.Shipment;
			dto.RetrieveEntities(_criteria, GlobalTestInitializer.DefaultUserId, GlobalTestInitializer.DefaultTenantId);
		}

		[Test]
		public void CanFetchForShipmentsWithShipmentReference()
		{
			var dto = new DocumentPackEntityDto();
			_criteria.Type = DocumentPackEntityType.Shipment;
			var field = OperationsSearchFields.ShipmentReferenceName;
			if (field != null) _criteria.Parameters.Add(new ParameterColumn { DefaultValue = string.Empty, ReportColumnName = field.DisplayName, Operator = Operator.Contains });
			field = OperationsSearchFields.ShipmentReferenceValue;
			if (field != null) _criteria.Parameters.Add(new ParameterColumn { DefaultValue = string.Empty, ReportColumnName = field.DisplayName, Operator = Operator.Contains });
			var results =  dto.RetrieveEntities(_criteria, GlobalTestInitializer.DefaultUserId, GlobalTestInitializer.DefaultTenantId);
			Console.WriteLine(results.Count);
		}
	}
}
