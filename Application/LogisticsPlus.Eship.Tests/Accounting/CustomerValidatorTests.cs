﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class CustomerValidatorTests
	{
		private CustomerValidator _validator;
		private Customer _customer;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new CustomerValidator();

			if(_customer != null) return;

			var tenantId = GlobalTestInitializer.DefaultTenantId;
			_customer = new Customer
							{
								Name = DateTime.Now.ToShortDateString(),
								CustomerNumber = "-1",
								LogoUrl = string.Empty,
                                DefaultAccountBucket = ProcessorVars.RegistryCache[tenantId].AccountBuckets[0],
								Active = true,
								AdditionalBillOfLadingText = string.Empty,
								Communication = null,
								CreditLimit = 10000,
								CustomFields = new List<CustomField>(),
								CustomChargeCodeMap = new List<CustomerChargeCodeMap>(),
								CustomVendorSelectionMessage = string.Empty,
								CustomerType = CustomerType.DirectShipperReceiver,
								DateCreated = DateTime.Now,
								EnableShipperBillOfLading = false,
								HidePrefix = true,
								InvalidPurchaseOrderNumberMessage = string.Empty,
								InvoiceRequiresCarrierProNumber = true,
								InvoiceRequiresShipperReference = true,
								InvoiceTerms = 15,
								Locations = new List<CustomerLocation>
			               		            	{
			               		            		new CustomerLocation
			               		            			{
			               		            				TenantId = GlobalTestInitializer.DefaultTenantId,
			               		            				CountryId = GlobalTestInitializer.DefaultCountryId,
															Primary = true
			               		            			},
			               		            		new CustomerLocation
			               		            			{
			               		            				TenantId = tenantId,
			               		            				CountryId = GlobalTestInitializer.DefaultCountryId
			               		            			}
			               		            	},
								MileageSource = null,
								Prefix = null,
								Rating = null,
								RequiredInvoiceDocumentTags = new List<RequiredInvoiceDocumentTag>(),
								SalesRepresentative = null,
								ServiceRepresentatives = new List<CustomerServiceRepresentative>(),
								ShipmentRequiresNMFC = true,
								ShipmentRequiresPurchaseOrderNumber = true,
								ShipperBillOfLadingPrefix = string.Empty,
								ShipperBillOfLadingSeed = 1,
								ShipperBillOfLadingSuffix = string.Empty,
								TenantId = tenantId,
								Tier = new TierSearch().FetchTiers(new List<ParameterColumn>(), tenantId)[0],
								ValidatePurchaseOrderNumber = false,
                                AuditInstructions = "Test Audit Instruction",
                                PaymentGatewayKey = string.Empty,
                                CanPayByCreditCard = false,
                                IsCashOnly = false
							};
			foreach (var location in _customer.Locations)
			{
				location.DateCreated = DateTime.Now;
				location.Customer = _customer;
				location.Contacts = new List<CustomerContact>
				                    	{
				                    		new CustomerContact
				                    			{
				                    				TenantId = location.TenantId,
				                    				CustomerLocation = location,
				                    				ContactType = ProcessorVars.RegistryCache[tenantId].ContactTypes[0],
													Name = "Test",
													Primary = true,
				                    			}
				                    	};
			}
			_customer.CustomFields.Add(new CustomField {Customer = _customer, TenantId = tenantId, Name = "Test Customer Field"});
			_customer.CustomChargeCodeMap.Add(new CustomerChargeCodeMap
			{
				ChargeCodeId = 23,
				CustomerId = _customer.Id,
				ExternalCode = "Test",
				ExternalDescription = "Test",
				TenantId = tenantId
			});
		
			_customer.RequiredInvoiceDocumentTags.Add(new RequiredInvoiceDocumentTag
			                                         	{
			                                         		TenantId = tenantId,
			                                         		Customer = _customer,
                                                            DocumentTag = ProcessorVars.RegistryCache[tenantId].DocumentTags[0]                                                            
			                                         	});
			_customer.ServiceRepresentatives.Add(new CustomerServiceRepresentative
			                                    	{
			                                    		Customer = _customer,
			                                    		TenantId = tenantId,
														User = new User(GlobalTestInitializer.DefaultUserId)
			                                    	});
		}

		[Test]
		public void IsValidTest()
		{
			Assert.IsTrue(_validator.IsValid(_customer));
		}

		[Test]
		public void HasRequiredDataTest()
		{
			var hasAllRequiredData = _validator.HasAllRequiredData(_customer);
			if (!hasAllRequiredData) _validator.Messages.PrintMessages();
			Assert.IsTrue(hasAllRequiredData);
		}

		[Test]
		public void DuplicateCustomerNumberTest()
		{
            var all = new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var number = _customer.CustomerNumber;
			_customer.CustomerNumber = all[0].CustomerNumber;
			Assert.IsTrue(_validator.DuplicateCustomerNumber(_customer));
			_customer.CustomerNumber = number;
		}

		[Test]
		public void DuplicateCustomerLocationNumberTest()
		{
			var all = new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var number = _customer.Locations.First().LocationNumber;
			_customer.Locations[0].LocationNumber = all[0].Locations[0].LocationNumber;
			Assert.IsTrue(_validator.DuplicateCustomerLocationNumber(_customer.Locations[0]));
			_customer.Locations[0].LocationNumber = number;
		}

		[Test]
		public void DuplicateCustomerServiceRepresentativeTest()
		{
			var csr = new CustomerServiceRepresentative();
			Assert.IsFalse(_validator.CustomerServiceRepresentativeExists(csr));
            var all = new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), GlobalTestInitializer.DefaultTenantId)
				.Where(c => c.ServiceRepresentatives.Count > 0)
				.ToList();
			if (!all.Any()) return;
			csr = all[0].ServiceRepresentatives[0];
			Assert.IsTrue(_validator.CustomerServiceRepresentativeExists(csr));
		}

		[Test]
		public void DuplicateRequiredInvoiceDocumentTagTest()
		{
			var tag = new RequiredInvoiceDocumentTag();
			Assert.IsFalse(_validator.CustomerRequiredInvoiceDocumentTagExist(tag));
            var all = new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), GlobalTestInitializer.DefaultTenantId)
				.Where(c => c.RequiredInvoiceDocumentTags.Count > 0)
				.ToList();
			if (!all.Any()) return;
			tag = all[0].RequiredInvoiceDocumentTags[0];
			Assert.IsTrue(_validator.CustomerRequiredInvoiceDocumentTagExist(tag));
		}

		[Test]
		public void CanDeleteTest()
		{
            var all = new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var customer = new Customer(all[0].Id, false);
			Assert.IsTrue(customer.KeyLoaded);
			_validator.Messages.Clear();
			_validator.CanDeleteCustomer(customer);
			_validator.Messages.PrintMessages();
		}

		[Test]
		public void CanDeleteCustomerLocationTest()
		{
			var all = new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var customer = all.FirstOrDefault(c => c.Locations.Any());
			if (customer == null) return;
			_validator.Messages.Clear();
			_validator.CanDeleteCustomerLocation(customer.Locations[0]);
			_validator.Messages.PrintMessages();
		}
	}
}
