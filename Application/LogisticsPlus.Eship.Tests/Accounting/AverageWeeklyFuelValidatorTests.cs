﻿using System;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class AverageWeeklyFuelValidatorTests
	{
		private AverageWeeklyFuelValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new AverageWeeklyFuelValidator();
		}

		[Test]
		public void IsExistingTest()
		{
			var all = new AverageWeeklyFuelSearch().FetchAverageWeeklyFuels(string.Empty, DateTime.Now.AddDays(-10), DateTime.Now, GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var option = new AverageWeeklyFuel(all[0].Id,false);
			Assert.IsTrue(option.Id > 0); 
		}

		[Test]
		public void IsValidTest()
		{
			var averageWeeklyFuel = new AverageWeeklyFuel
									{
										DateCreated = DateTime.Now,
										EffectiveDate = DateTime.Now.AddDays(30),
										ChargeCodeId = 1,
										EastCoastCost = new decimal(10.00),
										NewEnglandCost = new decimal(10.00),
										CentralAtlanticCost = new decimal(10.00),
										LowerAtlanticCost = new decimal(10.00),
										MidwestCost = new decimal(10.00),
										GulfCoastCost = new decimal(10.00),
										RockyMountainCost = new decimal(10.00),
										WestCoastCost = new decimal(10.00),
										WestCoastLessCaliforniaCost = new decimal(10.00),
										CaliforniaCost = new decimal(10.00),
										TenantId = GlobalTestInitializer.DefaultTenantId
									}; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsValid(averageWeeklyFuel));
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var averageWeeklyFuel = new AverageWeeklyFuel();
			Assert.IsFalse(_validator.HasAllRequiredData(averageWeeklyFuel));
		}
	}
}
