﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class SalesRepresentativeValidatorTests
	{
		private SalesRepresentativeValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new SalesRepresentativeValidator();
		}

		[Test]
		public void IsUniqueTest()
		{
			var salesRepresentative = new SalesRepresentative{SalesRepresentativeNumber = "99999?", TenantId = GlobalTestInitializer.DefaultTenantId}; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsUnique(salesRepresentative));
		}

		[Test]
		public void IsExistingAndUniqueTest()
		{
            var columns = new List<ParameterColumn>();
            var column = AccountingSearchFields.NonAliasedCountryCode.ToParameterColumn();

            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            columns.Add(column);

            var all = new SalesRepresentativeSearch().FetchSalesRepresentatives(columns, GlobalTestInitializer.DefaultTenantId);
            if (all.Count == 0) return;
            var option = all[0];
            Assert.IsTrue(_validator.IsUnique(option)); // this should be unique
            var newOption = new SalesRepresentative(1000000, false);
            Assert.IsFalse(newOption.KeyLoaded); // will not be found
            newOption.SalesRepresentativeNumber = option.SalesRepresentativeNumber;
            newOption.TenantId = option.TenantId;
            Assert.IsFalse(_validator.IsUnique(newOption)); // should be false
		}

		[Test]
		public void IsValidTest()
		{
			var salesRepresentative = new SalesRepresentative
			                          	{
			                          		Name = "T?", 
											SalesRepresentativeNumber = "99999999999",
											TenantId = GlobalTestInitializer.DefaultTenantId,
											CountryId = GlobalTestInitializer.DefaultCountryId,
                                            SalesRepCommTiers = new List<SalesRepCommTier>
                                                {
                                                    new SalesRepCommTier
                                                        {
                                                            SalesRepresentative = new SalesRepresentative(),
                                                            TenantId = GlobalTestInitializer.DefaultTenantId,
                                                            ServiceMode = ServiceMode.NotApplicable,
                                                            CeilingValue = default(decimal),
                                                            CommissionPercent = default(decimal),
                                                            EffectiveDate = DateTime.Now,
                                                            ExpirationDate = DateTime.Now,
                                                            FloorValue =  default(decimal),
                                                        }
                                                }
			                          	}; // T? will not be found therefore unique
			Assert.IsTrue(_validator.IsValid(salesRepresentative));
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var salesRepresentative = new SalesRepresentative();
			Assert.IsFalse(_validator.HasAllRequiredData(salesRepresentative));
		}

		[Test]
		public void CanDeleteTest()
		{
            var columns = new List<ParameterColumn>();
            var column = AccountingSearchFields.NonAliasedCountryCode.ToParameterColumn();

            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            columns.Add(column);

            var all = new SalesRepresentativeSearch().FetchSalesRepresentatives(columns, GlobalTestInitializer.DefaultTenantId);
            if (all.Count == 0) return;
            var representative = new SalesRepresentative(all[1].Id, false);
            Assert.IsTrue(representative.KeyLoaded);
            _validator.Messages.Clear();
            var canDeleteRep = _validator.CanDeleteSalesRepresentative(representative);
            Assert.IsTrue(_validator.Messages.Count > 0);
            Assert.IsFalse(canDeleteRep);
            _validator.Messages.PrintMessages();
		}
	}
}
