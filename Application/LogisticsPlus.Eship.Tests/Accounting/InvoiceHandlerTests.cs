﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
	[TestFixture]
	public class InvoiceHandlerTests
	{
		private InvoiceView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new InvoiceView();
			_view.Load();
		}

		[Test]
		public void CanSaveOrUpdate()
		{
			var criteria = new InvoiceViewSearchCriteria { ActiveUserId = GlobalTestInitializer.DefaultUserId, };

			var all = new InvoiceSearch().FetchInvoiceDashboardDtos(criteria, GlobalTestInitializer.DefaultTenantId);

			if (all.Count == 0) return;
			var invoice = new Invoice(all[0].Id);
			invoice.LoadCollections();
			_view.LockRecord(invoice);
			invoice.TakeSnapShot();
			_view.SaveRecord(invoice);
			_view.UnLockRecord(invoice);
		}

		[Test]
		public void CanSaveAndDelete()
		{
			var invoice = GetInvoice();
			_view.SaveRecord(invoice);
			Assert.IsTrue(invoice.Id != default(long));
			_view.DeleteRecord(invoice);
			Assert.IsTrue(!new Invoice(invoice.Id, false).KeyLoaded);
		}

		[Test]
		public void CanSaveLogPrintAndDelete()
		{
			var invoice = GetInvoice();
			_view.SaveRecord(invoice);
			Assert.IsTrue(invoice.Id != default(long));
			Exception ex;
			new InvoicePrintLogger().LogPrint(invoice, GlobalTestInitializer.ActiveUser, out ex);
			Assert.IsNull(ex);
			_view.DeleteRecord(invoice);
			Assert.IsTrue(!new Invoice(invoice.Id, false).KeyLoaded);
		}

		private static Invoice GetInvoice()
		{
			var tenantId = GlobalTestInitializer.DefaultTenantId;
			var customer = new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), tenantId).FirstOrDefault() ?? new Customer();
			var user = new User(GlobalTestInitializer.DefaultUserId);
            var chargeCode = ProcessorVars.RegistryCache[tenantId].ChargeCodes.FirstOrDefault() ?? new ChargeCode();
            var accountBucket = ProcessorVars.RegistryCache[tenantId].AccountBuckets.FirstOrDefault() ?? new AccountBucket();

			var shipmentCriteria = new ShipmentViewSearchCriteria { ActiveUserId = GlobalTestInitializer.DefaultUserId, };
			var shipment = new ShipmentSearch().FetchShipmentsToBeInvoiced(shipmentCriteria, tenantId).FirstOrDefault() ?? new ShipmentDashboardDto();



			var invoice = new Invoice
			              	{
			              		InvoiceNumber = "00000",
			              		TenantId = tenantId,
			              		InvoiceDate = DateTime.Now,
			              		CustomerLocation = customer.Locations.FirstOrDefault(l => l.Primary),
			              		DueDate = DateTime.Now,
			              		SpecialInstruction = "TEST",
			              		InvoiceType = InvoiceType.Invoice,
			              		Details = new List<InvoiceDetail>(),
			              		Posted = false,
			              		Exported = false,
			              		ExportDate = DateTime.Today,
			              		PostDate = DateTime.Today,
			              		PaidAmount = 0,
			              		User = user,
			              		CustomerControlAccountNumber = string.Empty,
			              	};
			invoice.AppendNumberSuffix();
			invoice.Details.Add(new InvoiceDetail
			                    	{
			                    		TenantId = GlobalTestInitializer.DefaultTenantId,
			                    		UnitSell = 10,
			                    		UnitDiscount = 1,
			                    		Invoice = invoice,
			                    		Comment = "TEST",
			                    		ReferenceNumber = shipment.ShipmentNumber,
			                    		ReferenceType = DetailReferenceType.Shipment,
			                    		Quantity = 1,
			                    		ChargeCode = chargeCode,
			                    		AccountBucket = accountBucket
			                    	});

			return invoice;
		}

		internal class InvoiceView : IInvoiceView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public List<ChargeCode> ChargeCodes
			{
				set {  }
			}

			public List<AccountBucket> AccountBuckets
			{
				set {  }
			}

			public Dictionary<int, string> InvoiceDetailReferenceType
			{
				set {  }
			}

			public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<Invoice>> Save;
			public event EventHandler<ViewEventArgs<Invoice>> Delete;
			public event EventHandler<ViewEventArgs<Invoice>> Lock;
			public event EventHandler<ViewEventArgs<Invoice>> UnLock;
			public event EventHandler<ViewEventArgs<Invoice>> Search;
			public event EventHandler<ViewEventArgs<string>> PrefixSearch;
			public event EventHandler<ViewEventArgs<Invoice>> LoadAuditLog;
			public event EventHandler<ViewEventArgs<string>> CustomerSearch;
		    public event EventHandler<ViewEventArgs<long>> LoadAndLockMiscReceiptsApplicableToPostedInvoice;
		    public event EventHandler<ViewEventArgs<Invoice, List<MiscReceiptApplication>>> SaveMiscReceiptApplicationsAndInvoice;
		    public event EventHandler<ViewEventArgs<Invoice, List<MiscReceipt>>> UnlockInvoiceAndMiscReceipts;

		    public void DisplayPrefix(Prefix prefix)
			{
				
			}

			public void DisplayCustomer(Customer customer)
			{
				
			}

		    public void DisplayMiscReceipts(List<MiscReceiptViewSearchDto> miscReceipts)
		    {
		        
		    }

		    public void LogException(Exception ex)
		    {
		    	Console.WriteLine("Exception: {0}", ex);
		    }

		    public void DisplaySearchResult(List<Invoice> invoices)
			{
				
			}

			public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
			{
			}

		    public void FailedMiscReceiptLocks()
		    {
		    }

		    public void FailedLock(Lock @lock)
			{
			}

			public void Set(Invoice invoice)
			{
				
			}


			public void Load()
			{
				var handler = new InvoiceHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(Invoice invoice)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<Invoice>(invoice));
			}

			public void DeleteRecord(Invoice invoice)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<Invoice>(invoice));
			}

			public void LockRecord(Invoice invoice)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<Invoice>(invoice));
			}

			public void UnLockRecord(Invoice invoice)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<Invoice>(invoice));
			}
		}
	}
}