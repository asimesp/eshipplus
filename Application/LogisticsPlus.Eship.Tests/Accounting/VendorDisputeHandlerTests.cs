﻿using System;
using System.Linq;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Accounting
{
    public class VendorDisputeHandlerTests
    {
        private VendorDisputeView _view;
        private static Shipment _shipment;
        private static ServiceTicket _serviceTicket;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _view = new VendorDisputeView();

            _shipment = GetShipment();
            _serviceTicket = GetServiceTicket();

            _view.Load();
        }

        [Test]
        public void CanSaveOrUpdate()
        {
            var criteria = new VendorDisputeViewSearchCriteria { ActiveUserId = GlobalTestInitializer.DefaultUserId };
            var all = new VendorDisputeSearch().FetchVendorDisputes(criteria, GlobalTestInitializer.DefaultTenantId);

            if (all.Count == 0) return;
            var vendorDispute = new VendorDispute(all[0].Id);
			vendorDispute.LoadCollection();
	        var disputeComments = vendorDispute.DisputeComments;
            _view.LockRecord(vendorDispute);
            vendorDispute.TakeSnapShot();
	        vendorDispute.DisputeComments = DateTime.Now.FormattedString();
            _view.SaveRecord(vendorDispute);
	        vendorDispute.DisputeComments = disputeComments;
			_view.SaveRecord(vendorDispute);
            _view.UnLockRecord(vendorDispute);
        }

        [Test]
        public void CanResolve()
        {
            var vendorDispute = GetVendorDispute();
            _view.LockRecord(vendorDispute);

            vendorDispute.TakeSnapShot();
            vendorDispute.IsResolved = true;
            vendorDispute.ResolutionComments = "Resolution Comments";
            vendorDispute.DisputeComments = "Dispute Comments";
            vendorDispute.ResolutionDate = DateTime.Now;
            vendorDispute.ResolvedByUser = vendorDispute.CreatedByUser;

            _view.ResolveRecord(vendorDispute);
            Assert.IsTrue(vendorDispute.IsResolved);

            Assert.IsTrue(vendorDispute.VendorDisputeDetails[0].RevisedChargeAmount == _shipment.Charges[0].UnitBuy);
            Assert.IsTrue(vendorDispute.VendorDisputeDetails[1].RevisedChargeAmount == _shipment.Charges[1].UnitBuy);
            Assert.IsTrue(vendorDispute.VendorDisputeDetails[2].RevisedChargeAmount == _shipment.Charges[2].UnitBuy);

            Assert.IsTrue(vendorDispute.VendorDisputeDetails[3].RevisedChargeAmount == _serviceTicket.Charges[0].UnitBuy);

            _view.DeleteRecord(vendorDispute);
            Assert.IsTrue(!new VendorDispute(vendorDispute.Id, false).KeyLoaded);
            _view.UnLockRecord(vendorDispute);
        }

        [Test]
        public void CanSaveAndDelete()
        {
            var vendorDispute = GetVendorDispute();
            _view.SaveRecord(vendorDispute);
            Assert.IsTrue(vendorDispute.Id != default(long));
            _view.DeleteRecord(vendorDispute);
            Assert.IsTrue(!new VendorDispute(vendorDispute.Id, false).KeyLoaded);
        }

        private static VendorDispute GetVendorDispute()
        {
            var tenantId = GlobalTestInitializer.DefaultTenantId;
            var vendor = new VendorSearch().FetchVendors(new List<ParameterColumn>(), tenantId).FirstOrDefault() ?? new Vendor();
            var user = new User(GlobalTestInitializer.DefaultUserId);

            var vendorDispute = new VendorDispute
            {
                CreatedByUser = user,
                TenantId = tenantId,
                DateCreated = DateTime.Now,
                ResolutionDate = System.Data.SqlTypes.SqlDateTime.MinValue.ToDateTime(),
                DisputerReferenceNumber = "123456789",
                ResolutionComments = string.Empty,
				DisputeComments = "Don't like the charge!",
                Vendor = vendor,
                VendorDisputeDetails = new List<VendorDisputeDetail>()
            };

            vendorDispute.VendorDisputeDetails.Add(new VendorDisputeDetail
            {
                TenantId = GlobalTestInitializer.DefaultTenantId,
                ChargeLineId = _shipment.Charges[0].Id,
                ChargeLineIdType = ChargeLineType.Shipment,
                VendorDispute = vendorDispute,
                DisputedId = vendorDispute.Id,
                OriginalChargeAmount = (decimal)123.1234,
                RevisedChargeAmount = (decimal)456.4567
            });

            vendorDispute.VendorDisputeDetails.Add(new VendorDisputeDetail
            {
                TenantId = GlobalTestInitializer.DefaultTenantId,
                ChargeLineId = _shipment.Charges[1].Id,
                ChargeLineIdType = ChargeLineType.Shipment,
                VendorDispute = vendorDispute,
                DisputedId = vendorDispute.Id,
                OriginalChargeAmount = (decimal)456.4567,
                RevisedChargeAmount = (decimal)890.8901
            });

            vendorDispute.VendorDisputeDetails.Add(new VendorDisputeDetail
            {
                TenantId = GlobalTestInitializer.DefaultTenantId,
                ChargeLineId = _shipment.Charges[2].Id,
                ChargeLineIdType = ChargeLineType.Shipment,
                VendorDispute = vendorDispute,
                DisputedId = vendorDispute.Id,
                OriginalChargeAmount = (decimal)123.1234,
                RevisedChargeAmount = (decimal)456.4567
            });

            vendorDispute.VendorDisputeDetails.Add(new VendorDisputeDetail
            {
                TenantId = GlobalTestInitializer.DefaultTenantId,
                ChargeLineId = _serviceTicket.Charges[0].Id,
                ChargeLineIdType = ChargeLineType.ServiceTicket,
                VendorDispute = vendorDispute,
                DisputedId = vendorDispute.Id,
                OriginalChargeAmount = (decimal)123.1234,
                RevisedChargeAmount = (decimal)456.4567
            });

            return vendorDispute;
        }

        private static Shipment GetShipment()
        {
            var criteria = new ShipmentViewSearchCriteria { ActiveUserId = GlobalTestInitializer.DefaultUserId, };
            var all = new ShipmentSearch().FetchShipmentsDashboardDto(criteria, GlobalTestInitializer.DefaultTenantId);

            if (all.Count == 0) throw new Exception("No shipments found.");

            var shipment = new Shipment(all[0].Id);

            var vendor = new VendorSearch().FetchVendors(new List<ParameterColumn>(), shipment.TenantId).FirstOrDefault() ?? new Vendor();
            var chargeCode = ProcessorVars.RegistryCache[shipment.TenantId].ChargeCodes.FirstOrDefault() ?? new ChargeCode();

            shipment.LoadCollections();

            shipment.Charges.Add(new ShipmentCharge
            {
                ChargeCode = chargeCode,
                Comment = string.Empty,
                Quantity = 1,
                Shipment = shipment,
                TenantId = shipment.TenantId,
                UnitBuy = 1.0000m,
                UnitSell = 2.0000m,
                UnitDiscount = 2.0000m,
                Vendor = vendor,
            });

            shipment.Charges.Add(new ShipmentCharge
            {
                ChargeCode = chargeCode,
                Comment = string.Empty,
                Quantity = 1,
                Shipment = shipment,
                TenantId = shipment.TenantId,
                UnitBuy = 2.0000m,
                UnitSell = 4.0000m,
                UnitDiscount = 2.0000m,
                Vendor = vendor,
            });

            shipment.Charges.Add(new ShipmentCharge
            {
                ChargeCode = chargeCode,
                Comment = string.Empty,
                Quantity = 1,
                Shipment = shipment,
                TenantId = shipment.TenantId,
                UnitBuy = 3.0000m,
                UnitSell = 6.0000m,
                UnitDiscount = 2.0000m,
                Vendor = vendor,
            });

            return shipment;
        }

        private static ServiceTicket GetServiceTicket()
        {
            var criteria = new ServiceTicketViewSearchCriteria
            {
                ActiveUserId = GlobalTestInitializer.DefaultUserId
            };
            var all = new ServiceTicketSearch().FetchServiceTicketDashboardDtos(criteria, GlobalTestInitializer.DefaultTenantId);

            if (all.Count == 0) throw new Exception("No service tickets found.");

            var serviceTicket = new ServiceTicket(all[0].Id);

            var vendor = new VendorSearch().FetchVendors(new List<ParameterColumn>(), serviceTicket.TenantId).FirstOrDefault() ?? new Vendor();
            var chargeCode = ProcessorVars.RegistryCache[serviceTicket.TenantId].ChargeCodes.FirstOrDefault() ?? new ChargeCode();

            serviceTicket.LoadCollections();

            serviceTicket.Charges.Add(new ServiceTicketCharge
            {
                ChargeCode = chargeCode,
                Comment = string.Empty,
                Quantity = 1,
                ServiceTicket = serviceTicket,
                TenantId = serviceTicket.TenantId,
                UnitBuy = 1.0000m,
                UnitSell = 2.0000m,
                UnitDiscount = 2.0000m,
                Vendor = vendor,
            });

            return serviceTicket;
        }

        internal class VendorDisputeView : IVendorDisputeView
        {
            public User ActiveUser
            {
                get { return GlobalTestInitializer.ActiveUser; }
            }

            public event EventHandler<ViewEventArgs<VendorDispute>> Delete;
            public event EventHandler Loading;
            public event EventHandler<ViewEventArgs<VendorDispute>> Lock;
            public event EventHandler<ViewEventArgs<VendorDispute>> Save;
            public event EventHandler<ViewEventArgs<VendorDispute>> Resolve;
            public event EventHandler<ViewEventArgs<VendorDispute>> UnLock;
	        public event EventHandler<ViewEventArgs<VendorDispute>> LoadAuditLog;

	        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
            {
                messages.PrintMessages();
                Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
            }

            public void FailedLock(Lock @lock) { }
	        public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
	        {
		       
	        }

	        public void LogException(Exception ex) { }

            public void Set(VendorDispute vendorDispute) { }

	        public void OnVendorDisputeResolvedComplete(VendorDispute vendorDispute)
	        {
		        
	        }

	        public void Load()
            {
                var handler = new VendorDisputeHandler(this);
                handler.Initialize();
            }

            public void SaveRecord(VendorDispute invoice)
            {
                if (Save != null)
                    Save(this, new ViewEventArgs<VendorDispute>(invoice));
            }

            public void ResolveRecord(VendorDispute invoice)
            {
                if (Resolve != null)
                    Resolve(this, new ViewEventArgs<VendorDispute>(invoice));
            }

            public void DeleteRecord(VendorDispute invoice)
            {
                if (Delete != null)
                    Delete(this, new ViewEventArgs<VendorDispute>(invoice));
            }

            public void LockRecord(VendorDispute invoice)
            {
                if (Lock != null)
                    Lock(this, new ViewEventArgs<VendorDispute>(invoice));
            }

            public void UnLockRecord(VendorDispute invoice)
            {
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<VendorDispute>(invoice));
            }
        }
    }
}