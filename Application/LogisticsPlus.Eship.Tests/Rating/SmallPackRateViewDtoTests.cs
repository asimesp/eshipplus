﻿using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Dto.Rating;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
	[TestFixture]
	public class SmallPackRateViewDtoTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanRetrieveCustomerRatingSmallPackRates()
		{
			new SmallPackRateViewDto().RetrieveCustomerRatingSmallPackRates(new CustomerRating(2, false));
		}
	}
}
