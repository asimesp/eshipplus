﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
	[TestFixture]
	public class ResellerAdditionSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchResellers()
		{
            var resellerAddtions = new ResellerAdditionSearch().FetchResellerAdditions
									(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(resellerAddtions.Count > 0);
		}

        [Test]
        public void CanFetchResellersWithParameterColumns()
        {
            var columns = new List<ParameterColumn>();
            var column = RatingSearchFields.CustomerNumber.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;

            columns.Add(column);

            var resellerAddtions = new ResellerAdditionSearch().FetchResellerAdditions
                                    (columns, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(resellerAddtions.Count > 0);
        }
	}
}
