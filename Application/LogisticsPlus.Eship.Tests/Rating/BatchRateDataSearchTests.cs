﻿using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
	[TestFixture]
	public class BatchRateDataSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchBatchRateDataSet()
		{
			new BatchRateDataSearch().FetchBatchRateDataSet(ServiceMode.LessThanTruckload, GlobalTestInitializer.DefaultTenantId);
		}

		[Test]
		public void CanFetchIncompleteBatchRateDataSetIds()
		{
			new BatchRateDataSearch().FetchIncompleteBatchRateDataSetIds(GlobalTestInitializer.DefaultTenantId);
		}
	}
}
