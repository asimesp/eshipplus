﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Rating;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
	public class RegionHandlerTests
	{
		private RegionView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new RegionView();
			_view.Load();
		}

		[Test]
		public void CanSaveOrUpdate()
		{
			var all = new RegionSearch().FetchRegions(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var region = all[0];
			region.LoadCollections();
			_view.LockRecord(region);
			region.TakeSnapShot();
			var name = region.Name;
			region.Name = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff");
			_view.SaveRecord(region);
			region.TakeSnapShot();
			region.Name = name;
			_view.SaveRecord(region);
			_view.UnLockRecord(region);
		}

		[Test]
		public void CanSaveAndDelete()
		{
			var region = GetRegion();
			_view.SaveRecord(region);
			Assert.IsTrue(region.Id != default(long));
			_view.DeleteRecord(region);
			Assert.IsTrue(!new Region(region.Id, false).KeyLoaded);

		}

		private Region GetRegion()
		{
			var tenantId = GlobalTestInitializer.DefaultTenantId;
			var region = new Region { Name = "Unit Test Region", TenantId = tenantId, Areas = new List<Area>() };
			region.Areas.Add(new Area
										{
											Region = region,
											TenantId = tenantId,
											PostalCode = "16501",
											CountryId = 1,
											UseSubRegion = false,
										});
			return region;
		}

		internal class RegionView : IRegionView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public List<Country> Countries
			{
				set {}
			}

			public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<Region>> Save;
			public event EventHandler<ViewEventArgs<Region>> Delete;
			public event EventHandler<ViewEventArgs<Region>> Lock;
			public event EventHandler<ViewEventArgs<Region>> UnLock;
			public event EventHandler<ViewEventArgs<Region>> LoadAuditLog;

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
			{

			}

			public void FailedLock(Lock @lock)
			{

			}

			public void Set(Region region)
			{

			}

			public void Load()
			{
				var handler = new RegionHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(Region region)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<Region>(region));
			}

			public void DeleteRecord(Region region)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<Region>(region));
			}

			public void LockRecord(Region region)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<Region>(region));
			}

			public void UnLockRecord(Region region)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<Region>(region));
			}
		}
	}
}
