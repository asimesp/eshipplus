﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Rating;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
    [TestFixture]
    class BatchRateDataHandlerTests
    {
        private BatchRateDataView _view;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _view = new BatchRateDataView();
            _view.Load();
        }

        [Test]
        public void CanSaveAndDelete()
        {
            var data = GetBatchRateData();
            _view.SaveRecord(data);
            Assert.IsTrue(data.Id != default(long));
            _view.DeleteRecord(data);
            Assert.IsTrue(!new BatchRateData(data.Id, false).KeyLoaded);
        }

        [Test]
        public void CanRefreshVendorRatingProfiles()
        {
            var list = new VendorRatingSearch().FetchVendorRatings(new VendorRatingViewSearchCriteria(), GlobalTestInitializer.DefaultTenantId);
            Assert.IsNotNull(list);
            Assert.IsNotEmpty(list);
        }

        private BatchRateData GetBatchRateData()
        {
            var tenantId = GlobalTestInitializer.DefaultTenantId;
            var data = new BatchRateData
                           {
                               Name = "TestData",
							   GroupCode = string.Empty,
                               LaneData = "Test Lane Data",
                               AnalysisEffectiveDate = DateTime.Now,
                               ResultData = "Test Result Data",
                               DateCreated = DateTime.Now.AddDays(-4),
                               DateCompleted = DateTime.Now,
                               ServiceMode = ServiceMode.LessThanTruckload,
                               LTLIndirectPointEnabled = false,
                               SmallPackageEngine = SmallPackageEngine.None,
                               SmallPackageEngineType = string.Empty,
                               ChargeCodeId = GlobalTestInitializer.DefaultChargeCodeId,
                               SubmittedByUserId = GlobalTestInitializer.DefaultUserId,
                               TenantId = tenantId,
                               VendorRatingId = GlobalTestInitializer.DefaultVendorRatingId,
                           };
            
            return data;
        }

        internal class BatchRateDataView : IBatchRateDataView
        {
            public User ActiveUser
            {
                get { return GlobalTestInitializer.ActiveUser; }
            }

            public List<ChargeCode> ChargeCodes
            {
                set { }
            }

            public List<PackageType> PackageTypes
            {
                set { }
            }

            public List<VendorRating> VendorRatingProfiles
            {
                set { }
            }

            public Dictionary<int, string> SmallPackEngines
            {
                set { }
            }

            public Dictionary<SmallPackageEngine, List<string>> SmallPackTypes
            {
                set { }
            }

            public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<List<BatchRateData>>> Save;
			public event EventHandler<ViewEventArgs<List<BatchRateData>>> Delete;
            public event EventHandler<ViewEventArgs<ServiceMode>> Search;
            public event EventHandler<ViewEventArgs<VendorRatingViewSearchCriteria>> RefreshVendorRatingProfiles;

            public void DisplayBatchRateDataSet(List<BatchRateData> data) { }

            public void DisplayMessages(IEnumerable<ValidationMessage> messages)
            {
                messages.PrintMessages();
                Assert.IsTrue(!messages.Any(m => m.Type == ValidateMessageType.Error));
            }

            public void LogException(Exception ex)
            {
            	DisplayMessages(new[] {ValidationMessage.Error(ex.Message)});
            }

            public void Load()
            {
                var handler = new BatchRateDataHandler(this);
                handler.Initialize();
            }

            public void SaveRecord(BatchRateData data)
            {
				if (Save != null)
					Save(this, new ViewEventArgs<List<BatchRateData>>(new List<BatchRateData> {data}));
            }

            public void DeleteRecord(BatchRateData data)
            {
				if (Delete != null)
					Delete(this, new ViewEventArgs<List<BatchRateData>>(new List<BatchRateData> {data}));
            }
        }
    }
}
