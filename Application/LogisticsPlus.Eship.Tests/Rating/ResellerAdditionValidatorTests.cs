﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Validation.Rating;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
	[TestFixture]
	public class ResellerAdditionValidatorTests
	{
        private ResellerAdditionValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _validator = new ResellerAdditionValidator();
		}

		[Test]
		public void IsValidTest()
		{
		    var resellerAddtion = new ResellerAddition
		                              {
                                          Name = "Test",
                                          ResellerCustomerAccount = new Customer(GlobalTestInitializer.DefaultCustomerId),
                                          LineHaulType = ValuePercentageType.Percentage,
                                          LineHaulPercentage = 10,
                                          LineHaulValue = 10,
                                          UseLineHaulMinimum = true,
                                          FuelType = ValuePercentageType.Percentage,
                                          FuelPercentage = 10,
                                          FuelValue = 10,
                                          UseFuelMinimum = true,
                                          AccessorialPercentage = 10,
                                          AccessorialType = ValuePercentageType.Percentage,
                                          AccessorialValue = 10,
                                          UseAccessorialMinimum = false,
                                          ServiceType = ValuePercentageType.Both,
                                          ServicePercentage = 10,
                                          ServiceValue = 10,
                                          UseServiceMinimum = false 
		                              };

           
           Assert.IsTrue(_validator.IsValid(resellerAddtion));
		}

        [Test]
        public void IsExistingAndUniqueTest()
        {
			var all = new ResellerAdditionSearch().FetchResellerAdditions(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
            if (all.Count == 0) return;
            var resellerAddition = all[0];
            Assert.IsTrue(_validator.IsUnique(resellerAddition)); // this should be unique
            var newReseller = new ResellerAddition(1000000, false);
            Assert.IsFalse(newReseller.KeyLoaded); // will not be found
            newReseller.Name = resellerAddition.Name;
            newReseller.TenantId = resellerAddition.TenantId;
            Assert.IsFalse(_validator.IsUnique(newReseller)); // should be false
        }

		[Test]
		public void CanDeleteTest()
		{
            var all = new ResellerAdditionSearch().FetchResellerAdditions(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var addition = new ResellerAddition(all[0].Id, false);
			Assert.IsTrue(addition.KeyLoaded);
			_validator.Messages.Clear();
			_validator.CanDeleteResellerAddition(addition);
			_validator.Messages.PrintMessages();
		}
	}
}
