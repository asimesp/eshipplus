﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Views.Rating;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
	[TestFixture]
	class CustomerTLTenderingProfileSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchCustomerTLTenderProfiles()
		{
			var criteria = new CustomerTLTenderingProfileViewSearchCriteria();
			criteria.Parameters = new List<ParameterColumn>();
			var profiles = new CustomerTLTenderProfileSearch().FetchCustomerTLTenderProfiles(criteria,
			                                                                                 GlobalTestInitializer
				                                                                                 .DefaultTenantId);
			if (!profiles.Any()) return;
			Assert.IsTrue(profiles.Any());
		}

		[Test]
		public void CanFetchCustomerTenderingProfileByCustomerId()
		{
			var profile =
				new CustomerTLTenderProfileSearch().FetchCustomerTenderingProfileByCustomerId(
					GlobalTestInitializer.DefaultCustomerId, GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(profile != null);
		}
	}
}
