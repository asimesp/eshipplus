﻿using LogisticsPlus.Eship.Processor.Searches.Rating;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
	[TestFixture]
	public class SmallPackageServiceMapSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchByTenant()
		{
			var smallPackagingMaps = new SmallPackageServiceMapSearch().FetchSmallPackageServiceMaps(GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(smallPackagingMaps.Count > 0);
		}
	}
}
