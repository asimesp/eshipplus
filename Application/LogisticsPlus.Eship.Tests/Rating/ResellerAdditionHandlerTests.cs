﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Rating;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
    public class ResellerAdditionHandlerTests
    {
        private ResellerAdditionView _view;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _view = new ResellerAdditionView();
            _view.Load();
        }

        [Test]
        public void CanSaveOrUpdate()
        {
            var all = new ResellerAdditionSearch().FetchResellerAdditions
				(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
            if (all.Count == 0) return;
            var resellerAddition = all[0];
            _view.LockRecord(resellerAddition);
            resellerAddition.TakeSnapShot();
            var name = resellerAddition.Name;
            resellerAddition.Name = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff");
            _view.SaveRecord(resellerAddition);
            resellerAddition.TakeSnapShot();
            resellerAddition.Name = name;
            _view.SaveRecord(resellerAddition);
            _view.UnLockRecord(resellerAddition);
        }

        [Test]
        public void CanHandleSaveAndDelete()
        {
            var resellerAddition = new ResellerAddition
            {
                Name = "Test",
                ResellerCustomerAccount = new Customer(GlobalTestInitializer.DefaultCustomerId),
                TenantId = _view.ActiveUser.TenantId,
                LineHaulType = ValuePercentageType.Percentage,
                LineHaulPercentage = 10,
                LineHaulValue = 10,
                UseLineHaulMinimum = true,
                FuelType = ValuePercentageType.Percentage,
                FuelPercentage = 10,
                FuelValue = 10,
                UseFuelMinimum = false,
                AccessorialType = ValuePercentageType.Value,
                AccessorialPercentage = 10,
                AccessorialValue = 10,
                UseAccessorialMinimum = true,
                ServiceType = ValuePercentageType.Value,
                ServicePercentage = 10,
                ServiceValue = 10,
                UseServiceMinimum = false
            };


            _view.SaveRecord(resellerAddition);
            Assert.IsTrue(resellerAddition.Id != default(long));
            _view.DeleteRecord(resellerAddition);
            Assert.IsTrue(!new ResellerAddition(resellerAddition.Id, false).KeyLoaded);

        }



        internal class ResellerAdditionView : IResellerAdditionView
        {
            public User ActiveUser
            {
                get { return GlobalTestInitializer.ActiveUser; }
            }

            public Dictionary<int, string> ValuePercentagTypes
            {
				set{}
            }

            public event EventHandler Loading;
            public event EventHandler<ViewEventArgs<ResellerAddition>> LoadAuditLog;

            public event EventHandler<ViewEventArgs<ResellerAddition>> Save;
            public event EventHandler<ViewEventArgs<ResellerAddition>> Delete;
            public event EventHandler<ViewEventArgs<ResellerAddition>> Lock;
            public event EventHandler<ViewEventArgs<ResellerAddition>> UnLock;
        	public event EventHandler<ViewEventArgs<string>> CustomerSearch;

			public void Set(ResellerAddition addition)
        	{
        	}

        	public void Load()
            {
                var handler = new ResellerAdditionHandler(this);
                handler.Initialize();
            }

            public void LogException(Exception ex)
            {
                    
            }

            public void DisplayMessages(IEnumerable<ValidationMessage> messages)
            {
                Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
            }

            public void DisplayCustomer(Customer customer)
            {
            }

            public void FailedLock(Lock @lock)
            {
            }

            public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
            {
            }

            public void SaveRecord(ResellerAddition resellerAddition)
            {
                if (Save != null)
                    Save(this, new ViewEventArgs<ResellerAddition>(resellerAddition));
            }

            public void DeleteRecord(ResellerAddition resellerAddition)
            {
                if (Delete != null)
                    Delete(this, new ViewEventArgs<ResellerAddition>(resellerAddition));
            }

            public void LockRecord(ResellerAddition resellerAddition)
            {
                if (Lock != null)
                    Lock(this, new ViewEventArgs<ResellerAddition>(resellerAddition));
            }

            public void UnLockRecord(ResellerAddition resellerAddition)
            {
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<ResellerAddition>(resellerAddition));
            }
        }
    }
}
