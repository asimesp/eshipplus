﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Validation.Rating;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
	[TestFixture]
	public class FuelTableValidatorTests
	{
		private FuelTableValidator _validator;
		private FuelTable _fuelTable;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new FuelTableValidator();

			if (_fuelTable != null) return;

			var tenantId = GlobalTestInitializer.DefaultTenantId;
			_fuelTable = new FuelTable {Name = DateTime.Now.ToString(), TenantId = tenantId, FuelIndices = new List<FuelIndex>()};
			_fuelTable.FuelIndices.Add(new FuelIndex
			                           	{
			                           		FuelTable = _fuelTable,
			                           		TenantId = tenantId,
			                           		LowerBound = 0.0000m,
			                           		UpperBound = 1.0000m,
			                           		Surcharge = 20.0000m
			                           	});
		}

		[Test]
		public void IsValidTest()
		{
			Assert.IsTrue(_validator.IsValid(_fuelTable));
		}

		[Test]
		public void HasRequiredDataTest()
		{
			var hasAllRequiredData = _validator.HasAllRequiredData(_fuelTable);
			if (!hasAllRequiredData) _validator.Messages.PrintMessages();
			Assert.IsTrue(hasAllRequiredData);
		}

		[Test]
		public void DuplicateGroupNameTest()
		{
			var all = new FuelTableSearch().FetchFuelTables(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var fuelTable = new FuelTable {Name = all[0].Name, TenantId = all[0].TenantId};
			Assert.IsTrue(_validator.DuplicateFuelTableName(fuelTable));
		}

		[Test]
		public void CanDeleteTest()
		{
			var all = new FuelTableSearch().FetchFuelTables(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var fuelTable = new FuelTable(all[0].Id, false);
			Assert.IsTrue(fuelTable.KeyLoaded);
			_validator.Messages.Clear();
			_validator.CanDeleteFuelTable(fuelTable);
			_validator.Messages.PrintMessages();
		}
	}
}
