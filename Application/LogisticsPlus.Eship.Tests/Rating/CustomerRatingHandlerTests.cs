﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Rating;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.Processor.Views.Rating;
using LogisticsPlus.Eship.SmallPacks;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
	[TestFixture]
	public class CustomerRatingHandlerTests
	{
		private CustomerRatingView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new CustomerRatingView();
			_view.Load();
		}

		[Test] 
		public void CanSaveOrUpdate()
		{
             var columns = new List<ParameterColumn>();

            var field = AccountingSearchFields.CustomerNumber;
            if (field != null) columns.Add(new ParameterColumn { DefaultValue = SearchUtilities.WildCard, ReportColumnName = field.DisplayName, Operator = Operator.Contains });

			var all = new CustomerSearch().FetchCustomersWithRating(columns, GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var rating = all[0].Rating;
			rating.LoadCollections();
			_view.LockRecord(rating);
			rating.TakeSnapShot();
			var percentage = rating.LineHaulProfitCeilingPercentage;
			rating.LineHaulProfitCeilingPercentage = 0;
			_view.SaveRecord(rating);
			rating.TakeSnapShot();
			rating.LineHaulProfitCeilingPercentage = percentage;
			_view.SaveRecord(rating);
			_view.UnLockRecord(rating);
		}

		[Test]
		public void CanSaveAndDelete()
		{
			var rating = GetRating();
			_view.SaveRecord(rating);
			Assert.IsTrue(rating.Id != default(long));
			_view.DeleteRecord(rating);
			Assert.IsTrue(!new CustomerRating(rating.Id, false).KeyLoaded);
		}

		

		private CustomerRating GetRating()
		{
			var tenantId = GlobalTestInitializer.DefaultTenantId;


            var columns = new List<ParameterColumn>();

            var field = AccountingSearchFields.CustomerNumber;
            if (field != null) columns.Add(new ParameterColumn { DefaultValue = SearchUtilities.WildCard, ReportColumnName = field.DisplayName, Operator = Operator.Contains });
			var customersWithRating = new CustomerSearch().FetchCustomersWithRating(columns, tenantId);

            var criteria = new CustomerViewSearchCriteria();
            var customers = new CustomerSearch().FetchCustomers(criteria, tenantId);
			var customer = customers.FirstOrDefault(c => !customersWithRating.Select(cr => cr.Id).Contains(c.Id));

			var rating = new CustomerRating
			             	{
			             		TenantId = tenantId,
			             		InsuranceEnabled = true,
			             		InsuranceChargeCodeId = GlobalTestInitializer.DefaultChargeCodeId,
			             		InsurancePurchaseFloor = 40.0000m,
			             		LineHaulProfitCeilingPercentage = 40.0000m,
			             		LineHaulProfitCeilingType = ValuePercentageType.Both,
			             		LineHaulProfitCeilingValue = 50.0000m,
			             		LineHaulProfitFloorPercentage = 20.0000m,
			             		LineHaulProfitFloorType = ValuePercentageType.Both,
			             		LineHaulProfitFloorValue = 20.000m,
			             		NoLineHaulProfit = false,
			             		UseLineHaulMinimumCeiling = false,
			             		UseLineHaulMinimumFloor = false,
			             		FuelProfitCeilingPercentage = 40.0000m,
			             		FuelProfitCeilingType = ValuePercentageType.Both,
			             		FuelProfitCeilingValue = 50.0000m,
			             		FuelProfitFloorPercentage = 20.0000m,
			             		FuelProfitFloorType = ValuePercentageType.Both,
			             		FuelProfitFloorValue = 20.000m,
			             		NoFuelProfit = false,
			             		UseFuelMinimumCeiling = false,
			             		UseFuelMinimumFloor = false,
			             		AccessorialProfitCeilingPercentage = 40.0000m,
			             		AccessorialProfitCeilingType = ValuePercentageType.Both,
			             		AccessorialProfitCeilingValue = 50.0000m,
			             		AccessorialProfitFloorPercentage = 20.0000m,
			             		AccessorialProfitFloorType = ValuePercentageType.Both,
			             		AccessorialProfitFloorValue = 20.000m,
			             		NoAccessorialProfit = false,
			             		UseAccessorialMinimumCeiling = false,
			             		UseAccessorialMinimumFloor = false,
			             		ServiceProfitCeilingPercentage = 40.0000m,
			             		ServiceProfitCeilingType = ValuePercentageType.Both,
			             		ServiceProfitCeilingValue = 50.0000m,
			             		ServiceProfitFloorPercentage = 20.0000m,
			             		ServiceProfitFloorType = ValuePercentageType.Both,
			             		ServiceProfitFloorValue = 20.000m,
			             		NoServiceProfit = false,
			             		UseServiceMinimumCeiling = false,
			             		UseServiceMinimumFloor = false,
			             		ResellerAddition = new ResellerAdditionSearch().FetchResellerAdditions(new List<ParameterColumn>(), tenantId).FirstOrDefault(),
			             		BillReseller = false,
								TLFreightChargeCodeId = GlobalTestInitializer.DefaultChargeCodeId,
							TLFuelChargeCodeId = GlobalTestInitializer.DefaultChargeCodeId,
							TLFuelRate = 10.000m,
							TLFuelRateType = RateType.Flat,
				                 Project44AccountGroup = string.Empty,
                                Customer = customer,
			             		LTLSellRates = new List<LTLSellRate>(),
								TLSellRates = new List<TLSellRate>(),
			             		SmallPackRates = new List<SmallPackRate>(),
			             		CustomerServiceMarkups = new List<CustomerServiceMarkup>(),
			             	};
			rating.Customer.Rating = rating;

			rating.LTLSellRates.Add(new LTLSellRate
			                        	{
			                        		TenantId = tenantId,
			                        		CustomerRating = rating,
			                        		VendorRating =
												new VendorRatingSearch().FetchVendorRatings(new VendorRatingViewSearchCriteria(), tenantId).FirstOrDefault(),
			                        		LTLIndirectPointEnabled = false,
			                        		EffectiveDate = DateTime.Now,
			                        		Active = true,
			                        		MarkupPercent = 40.0000m,
			                        		MarkupValue = 50.0000m,
			                        		UseMinimum = false,
                                            OverrideMarkupPercentVendorFloor = 10.0000m,
                                            OverrideMarkupValueVendorFloor = 20.0000m
			                        	});
			rating.TLSellRates.Add(new TLSellRate
				{
					TenantId = tenantId,
					CustomerRating = rating,
					DestinationCity = string.Empty,
					DestinationCountryId = GlobalTestInitializer.DefaultCountryId,
					DestinationPostalCode = string.Empty,
					DestinationState = string.Empty,
					EffectiveDate = DateTime.Now,
					OriginCity = string.Empty,
					OriginState = string.Empty,
					OriginPostalCode = string.Empty,
					OriginCountryId = GlobalTestInitializer.DefaultCountryId,
					EquipmentType = null,
					TariffType = TLTariffType.OneWay,
					MaximumWeight = 10,
					MinimumCharge = 10,
					MinimumWeight = 9,
					Mileage = 100,
				});
			rating.SmallPackRates.Add(new SmallPackRate
			                          	{
			                          		TenantId = tenantId,
			                          		CustomerRating = rating,
			                          		SmallPackageEngine = SmallPackageEngine.FedEx,
			                          		SmallPackType = SmallPacksFactory.RetrieveServiceTypes()[SmallPackageEngine.FedEx][0],
			                          		MarkupPercent = 40.0000m,
			                          		MarkupValue = 50.0000m,
			                          		UseMinimum = false,
			                          		EffectiveDate = DateTime.Now,
			                          		Active = true,
			                          		Vendor = new VendorSearch().FetchVendors(new List<ParameterColumn>(), tenantId).FirstOrDefault(),
                                            ChargeCode = ProcessorVars.RegistryCache[tenantId].ChargeCodes.FirstOrDefault() ?? new ChargeCode()
			                          	});
			var dto = new ServiceSearch().FetchServices(new List<ParameterColumn>(), tenantId).FirstOrDefault();
			rating.CustomerServiceMarkups.Add(new CustomerServiceMarkup
			                                  	{
			                                  		TenantId = tenantId,
			                                  		CustomerRating = rating,
			                                  		ServiceId = dto == null ? default(long) : dto.Id,
			                                  		MarkupPercent = 40.0000m,
			                                  		MarkupValue = 50.0000m,
			                                  		UseMinimum = false,
			                                  		EffectiveDate = DateTime.Now,
			                                  		Active = true,
			                                  		ServiceChargeCeiling = 500.0000m,
			                                  		ServiceChargeFloor = 0.0000m,
			                                  	});
			return rating;
		}

		internal class CustomerRatingView : ICustomerRatingView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}
			public List<Country> Countries
			{
				set { }
			}

			public List<ResellerAddition> ResellerAdditions
			{
				set { }
			}
			public List<VendorRating> VendorRatings
			{
				set { }
			}

		    public Dictionary<int, string> RateType { set{} }

		    public Dictionary<int, string> SmallPackEngines
			{
				set { }
			}
			public Dictionary<SmallPackageEngine, List<string>> SmallPackTypes
			{
				set { }
			}

		    public Dictionary<int, string> TariffType { set{} }

		    public Dictionary<int, string> ValuePercentageTypes
			{
				set { }
			}

			public Dictionary<int, string> WeightBreaks { set { } }

			public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<CustomerRating>> Save;
			public event EventHandler<ViewEventArgs<CustomerRating>> Delete;
			public event EventHandler<ViewEventArgs<CustomerRating>> Lock;
			public event EventHandler<ViewEventArgs<CustomerRating>> UnLock;
			public event EventHandler<ViewEventArgs<CustomerRating>> LoadAuditLog;
			public event EventHandler<ViewEventArgs<string>> VendorSearch;
			public event EventHandler<ViewEventArgs<string>> CustomerSearch;

			public void DisplayVendor(Vendor vendor)
			{
			}

			public void DisplayCustomer(Customer customer)
			{
			}

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
			{
			}

			public void FailedLock(Lock @lock)
			{
			}

			public void Set(CustomerRating rating)
			{
			}

			public void Load()
			{
				var handler = new CustomerRatingHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(CustomerRating rating)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<CustomerRating>(rating));
			}

			public void DeleteRecord(CustomerRating rating)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<CustomerRating>(rating));
			}

			public void LockRecord(CustomerRating rating)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<CustomerRating>(rating));
			}

			public void UnLockRecord(CustomerRating rating)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<CustomerRating>(rating));
			}
		}
	}
}
