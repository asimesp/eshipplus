﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Rating;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
	public class FuelTableHandlerTests
	{
		private FuelTableView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new FuelTableView();
			_view.Load();
		}

		[Test]
		public void CanSaveOrUpdate()
		{
			var all = new FuelTableSearch().FetchFuelTables(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var fuelTable = all[0];
			fuelTable.LoadCollections();
			_view.LockRecord(fuelTable);
			fuelTable.TakeSnapShot();
			var name = fuelTable.Name;
			fuelTable.Name = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff");
			_view.SaveRecord(fuelTable);
			fuelTable.TakeSnapShot();
			fuelTable.Name = name;
			_view.SaveRecord(fuelTable);
			_view.UnLockRecord(fuelTable);
		}

		[Test]
		public void CanSaveAndDelete()
		{
			var fuelTable = GetFuelTable();
			_view.SaveRecord(fuelTable);
			Assert.IsTrue(fuelTable.Id != default(long));
			_view.DeleteRecord(fuelTable);
			Assert.IsTrue(!new FuelTable(fuelTable.Id, false).KeyLoaded);
		}

		private FuelTable GetFuelTable()
		{
			var tenantId = GlobalTestInitializer.DefaultTenantId;
			var fuelTable = new FuelTable { Name = DateTime.Now.ToString(), TenantId = tenantId, FuelIndices = new List<FuelIndex>() };
			fuelTable.FuelIndices.Add(new FuelIndex
			                          	{
			                          		FuelTable = fuelTable,
			                          		TenantId = tenantId,
			                          		LowerBound = 0.0000m,
			                          		UpperBound = 1.0000m,
			                          		Surcharge = 20.0000m
			                          	});
			return fuelTable;
		}

		internal class FuelTableView : IFuelTableView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public event EventHandler<ViewEventArgs<FuelTable>> Save;
			public event EventHandler<ViewEventArgs<FuelTable>> Delete;
			public event EventHandler<ViewEventArgs<FuelTable>> Lock;
			public event EventHandler<ViewEventArgs<FuelTable>> UnLock;
			public event EventHandler<ViewEventArgs<FuelTable>> LoadAuditLog;

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Where(m => m.Type != ValidateMessageType.Information).Count() == 0);
			}

			public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
			{
				
			}

			public void FailedLock(Lock @lock)
			{
				
			}

			public void Set(FuelTable fuelTable)
			{
				
			}

			public void Load()
			{
				var handler = new FuelTableHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(FuelTable fuelTable)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<FuelTable>(fuelTable));
			}

			public void DeleteRecord(FuelTable fuelTable)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<FuelTable>(fuelTable));
			}

			public void LockRecord(FuelTable fuelTable)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<FuelTable>(fuelTable));
			}

			public void UnLockRecord(FuelTable fuelTable)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<FuelTable>(fuelTable));
			}
		}
	}
}
