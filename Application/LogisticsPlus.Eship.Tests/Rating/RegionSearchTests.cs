﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
	[TestFixture]
	public class RegionSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchByWildCard()
		{
			var regions = new RegionSearch().FetchRegions(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(regions.Count > 0);
		}

        [Test]
        public void CanFetchRegions()
        {
            var columns = new List<ParameterColumn>();

            var field = RatingSearchFields.Regions.FirstOrDefault(f => f.Name == "Name");
            if (field == null)
            {
                Console.WriteLine("RatingSearchFields.Regions no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }

            var column = field.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            columns.Add(column);

            var results = new RegionSearch().FetchRegions(columns, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(results.Any());
        }


		[Test]
		public void CanFetchByName()
		{
			var all = new RegionSearch().FetchRegions(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var id = new RegionSearch().FetchRegionIdByName(all[0].Name, all[0].TenantId);
			Assert.IsTrue(id != default(long));
		}
	}
}
