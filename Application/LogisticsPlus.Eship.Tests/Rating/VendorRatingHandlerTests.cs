﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Rating;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;
using LogisticsPlus.Eship.Smc;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
	[TestFixture]
	public class VendorRatingHandlerTests
	{
		private VendorRatingView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new VendorRatingView();
			_view.Load();
		}

		[Test]
		public void CanSaveOrUpdate()
		{
			var all = new VendorRatingSearch().FetchVendorRatings(new VendorRatingViewSearchCriteria(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var rating = all[0];
			rating.LoadCollections();
			_view.LockRecord(rating);
			rating.TakeSnapShot();
			var name = rating.Name;
			rating.Name = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff");
			_view.SaveRecord(rating);
			rating.TakeSnapShot();
			rating.Name = name;
			_view.SaveRecord(rating);
			_view.UnLockRecord(rating);
		}

		[Test]
		public void CanSaveAndDelete()
		{
			var rating = GetRating();
			_view.SaveRecord(rating);
			Assert.IsTrue(rating.Id != default(long));
			_view.DeleteRecord(rating);
			Assert.IsTrue(!new VendorRating(rating.Id, false).KeyLoaded);
		}

		private VendorRating GetRating()
		{
			var tenantId = GlobalTestInitializer.DefaultTenantId;
		    var rating
		        = new VendorRating
		        {
		            CurrentLTLFuelMarkup = 20.0000m,
		            DisplayName = "Display2",
		            FuelChargeCodeId = GlobalTestInitializer.DefaultChargeCodeId,
		            FuelIndexRegion = FuelIndexRegion.National,
		            FuelMarkupCeiling = 25.0000m,
		            FuelMarkupFloor = 15.0000m,
		            FuelTableId = default(long),
		            ExpirationDate = DateTime.Now,
		            HasAdditionalCharges = true,
		            HasLTLCubicFootCapacityRules = true,
		            HasLTLOverLengthRules = true,
		            LTLAccessorials = new List<LTLAccessorial>(),
		            LTLAdditionalCharges = new List<LTLAdditionalCharge>(),
		            LTLCubicFootCapacityRules = new List<LTLCubicFootCapacityRule>(),
		            LTLDiscountTiers = new List<DiscountTier>(),
		            LTLOverLengthRules = new List<LTLOverLengthRule>(),
		            LTLPackageSpecificRates = new List<LTLPackageSpecificRate>(),
		            LTLTariff = "ABF5040120060403N",
		            LTLTruckHeight = 96,
		            LTLTruckLength = 144,
		            LTLTruckWidth = 96,
		            LTLTruckWeight = 10000,
		            LTLMinPickupWeight = 0,
		            LTLTruckUnitWeight = 5000,
		            Name = "Name2",
		            TenantId = tenantId,
		            Vendor = new VendorSearch().FetchVendors(new List<ParameterColumn>(), tenantId)[0],
		            OverrideAddress = string.Empty,
		            EnableLTLPackageSpecificRates = true,
		            LTLPackageSpecificRatePackageTypeId = GlobalTestInitializer.DefaultPackageTypeId,
		            LTLPackageSpecificFreightChargeCodeId = GlobalTestInitializer.DefaultChargeCodeId,
		            CubicCapacityPenaltyWidth = 96,
		            CubicCapacityPenaltyHeight = 96,
		            MaxItemHeight = 0,
		            MaxItemWidth = 0,
		            MaxItemLength = 0,
		            IndividualItemLimitsApply = false,
		            MaxPackageQuantityApplies = false,
		            MaxPackageQuantity = 0,
		            RatingDetailNotes = string.Empty,
		            CriticalBOLNotes = string.Empty,
		            Smc3ServiceLevel = Smc3ServiceLevel.ServiceLevels[0],
		            Project44Profile = false,
                    Project44TradingPartnerCode = "Test"
		        };
			rating.LTLAccessorials.Add(new LTLAccessorial
			                           	{
			                           		CeilingValue = 200.0000m,
			                           		EffectiveDate = DateTime.Now,
			                           		FloorValue = 20.0000m,
			                           		Rate = 100.0000m,
			                           		RateType = RateType.PerHundredWeight,
											ServiceId = new ServiceSearch().FetchServices(new List<ParameterColumn>(), tenantId)[0].Id,
			                           		TenantId = tenantId,
			                           		VendorRating = rating
			                           	});
			rating.LTLAdditionalCharges.Add(new LTLAdditionalCharge
			                                	{
			                                		AdditionalChargeIndices = new List<AdditionalChargeIndex>(),
			                                		ChargeCodeId = GlobalTestInitializer.DefaultChargeCodeId,
			                                		EffectiveDate = DateTime.Now,
			                                		Name = "Name",
			                                		TenantId = tenantId,
			                                		VendorRating = rating
			                                	});
			rating.LTLAdditionalCharges[0].AdditionalChargeIndices.Add(new AdditionalChargeIndex
			                                                           	{
			                                                           		ApplyOnDestination = true,
			                                                           		ApplyOnOrigin = false,
			                                                           		ChargeCeiling = 20.0000m,
			                                                           		ChargeFloor = 15.0000m,
			                                                           		LTLAdditionalCharge = rating.LTLAdditionalCharges[0],
			                                                           		PostalCode = "16504",
																			CountryId = GlobalTestInitializer.DefaultCountryId,
			                                                           		Rate = 18.0000m,
			                                                           		RateType = RateType.LineHaulPercentage,
			                                                           		TenantId = tenantId,
																			UsePostalCode = true
			                                                           	});
			rating.LTLCubicFootCapacityRules.Add(new LTLCubicFootCapacityRule
			                                     	{
			                                     		AppliedFreightClass = 50,
			                                     		ApplyFAK = true,
														ApplyDiscount = true,
			                                     		AveragePoundPerCubicFootApplies = true,
			                                     		AveragePoundPerCubicFootLimit = 5,
			                                     		EffectiveDate = DateTime.Now,
			                                     		LowerBound = 0,
			                                     		UpperBound = 750,
			                                     		PenaltyPoundPerCubicFoot = 5,
			                                     		TenantId = tenantId,
			                                     		VendorRating = rating
			                                     	});
			rating.LTLDiscountTiers.Add(new DiscountTier
			                            	{
			                            		CeilingValue = 120.0000m,
												DestinationRegion = new RegionSearch().FetchRegions(new List<ParameterColumn>(), tenantId)[0],
			                            		DiscountPercent = 84.0000m,
			                            		EffectiveDate = DateTime.Now,
			                            		FAK100 = 100,
			                            		FAK110 = 110,
			                            		FAK125 = 125,
			                            		FAK150 = 150,
			                            		FAK175 = 175,
			                            		FAK200 = 200,
			                            		FAK250 = 250,
			                            		FAK300 = 300,
			                            		FAK400 = 400,
			                            		FAK50 = 50,
			                            		FAK500 = 500,
			                            		FAK55 = 55,
			                            		FAK60 = 60,
			                            		FAK65 = 65,
			                            		FAK70 = 70,
			                            		FAK775 = 77.5,
			                            		FAK85 = 85,
			                            		FAK925 = 92.5,
			                            		FloorValue = 80.0000m,
			                            		FreightChargeCodeId = GlobalTestInitializer.DefaultChargeCodeId,
												OriginRegion = new RegionSearch().FetchRegions(new List<ParameterColumn>(), tenantId)[0],
			                            		RateBasis = DiscountTierRateBasis.FreightClass,
												WeightBreak = WeightBreak.Ignore,
			                            		TenantId = tenantId,
			                            		TierPriority = 1,
			                            		VendorRating = rating,
			                            	});
			rating.LTLOverLengthRules.Add(new LTLOverLengthRule
			                              	{
			                              		Charge = 20.0000m,
			                              		ChargeCodeId = GlobalTestInitializer.DefaultChargeCodeId,
			                              		EffectiveDate = DateTime.Now,
												LowerLengthBound = 144.0001m,
			                              		TenantId = tenantId,
			                              		UpperLengthBound = 200.0000m,
			                              		VendorRating = rating
			                              	});
			rating.LTLPcfToFcConvTab.Add(new LTLPcfToFcConversion
			{
				EffectiveDate = DateTime.Now,
				TenantId = GlobalTestInitializer.DefaultTenantId,
				VendorRating = rating,
				ConversionTableData =
					@"lower	Upper	Fc
0.0000	1.9999	50
2.0000	4.9999	125"
			});
            var region = new RegionSearch().FetchRegions(new List<ParameterColumn>(), tenantId)[0];
            rating.LTLPackageSpecificRates.Add(new LTLPackageSpecificRate
            {
                TenantId = tenantId,
                VendorRating = rating,
                EffectiveDate = DateTime.Now,
                Rate = 10,
                DestinationRegionId = region.Id,
                OriginRegionId = region.Id,
                PackageQuantity = 2,
                RatePriority = 1
            });

            rating.LTLGuaranteedCharges.Add(new LTLGuaranteedCharge
            {
                TenantId = tenantId,
                VendorRating = rating,
                ChargeCodeId = GlobalTestInitializer.DefaultChargeCodeId,
                Description = DateTime.Now.ToString(),
                EffectiveDate = DateTime.Now,
                FloorValue = 115.25m,
                CeilingValue = 670.91m,
                Rate = 15,
                Time = "12:45",
                RateType = RateType.LineHaulPercentage,
                CriticalNotes = DateTime.Now.ToString(),
            });

			return rating;
		}

		internal class VendorRatingView : IVendorRatingView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}
			public SmcServiceSettings RatewareSettings
			{
				get { return GlobalTestInitializer.RatewareSettings;}
			}
			public SmcServiceSettings CarrierConnectSettings
			{
				get { return GlobalTestInitializer.CarrierConnectSettings2; }
			}
			public List<Country> Countries
			{
				set {}
			}
			public List<ChargeCode> ChargeCodes
			{
				set{}
			}
			public List<FuelTable> FuelTables
			{
				set { }
			}
			public List<Region> Regions
			{
				set { }
			}
			public List<ServiceViewSearchDto>  Services
			{
				set{ }
			}

		    public List<PackageType> PackageTypes
		    {
		        set{ }
		    }

			public Dictionary<string, string> Smc3ServiceLevels { set {} }

			public Dictionary<string, string> LTLTariffs
			{
				set { }
			}
			public Dictionary<int, string> FuelIndexRegions
			{
				set { }
			}
			
			public Dictionary<int, string> DiscountTierRateBasis
			{
				set { }
			}
			public Dictionary<int, string> LTLAccessorialsRateTypes
			{
				set { }
			}
			public Dictionary<int, string> LTLAdditionalChargeIndexRateTypes
			{
				set { }
			}

		    public Dictionary<int, string> LTLGuaranteedChargeRateTypes
		    {
		        set { }
		    }

		    public Dictionary<int, string> DaysOfWeek
			{
				set { }
			}
			public Dictionary<int, string> WeightBreaks
			{
				set {}
			}

			public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<VendorRating>> Save;
			public event EventHandler<ViewEventArgs<VendorRating>> Delete;
			public event EventHandler<ViewEventArgs<VendorRating>> Lock;
			public event EventHandler<ViewEventArgs<VendorRating>> UnLock;
			public event EventHandler<ViewEventArgs<VendorRating>> LoadAuditLog;
			public event EventHandler<ViewEventArgs<string>> VendorSearch;

			public void DisplayVendor(Vendor vendor)
			{
			}

		    public void LogException(Exception ex)
		    {
		            Console.WriteLine(ex.ToString());
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)	
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type == ValidateMessageType.Error) == 0);
			}

			public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
			{
			}

			public void FailedLock(Lock @lock)
			{
			}

			public void Set(VendorRating vendorRating)
			{
			}

			public void Load()
			{
				var handler = new VendorRatingHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(VendorRating rating)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<VendorRating>(rating));
			}

			public void DeleteRecord(VendorRating rating)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<VendorRating>(rating));
			}

			public void LockRecord(VendorRating rating)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<VendorRating>(rating));
			}

			public void UnLockRecord(VendorRating rating)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<VendorRating>(rating));
			}
		}
	}
}
