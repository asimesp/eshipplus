﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Dto.Rating;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
    [TestFixture]
    public class MatchingRegionPostalCodeWithinRegionDtosTests
    {
        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

        [Test]
        public void MatchingRegionPostalCodeWithinRegionDtosTestsTest()
        {
            var region = new RegionSearch().FetchRegions(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId)
                                           .FirstOrDefault(r =>r.Areas.Any(a => a.UseSubRegion && a.SubRegion.Areas.Any(ar => !ar.UseSubRegion)));
            var subregionArea = region == null
                                 ? new Area()
                                 : region.Areas.First(a => a.UseSubRegion)
                                         .SubRegion.Areas.First(a => !a.UseSubRegion);

            var dto = new MatchingRegionPostalCodeWithinRegionDto(GlobalTestInitializer.DefaultTenantId, subregionArea.CountryId, region == null ? default(long) : region.Id, subregionArea.PostalCode);
            
            // If region that met criteria to be selected in first line was found, then this is always true
            Assert.IsTrue(dto.RegionIds.Any());
        }
    }
}
