﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
	[TestFixture]
	public class VendorRatingSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchApplicableAdditionalChargeIndex()
		{
			new VendorRatingSearch().ApplicableAdditionalChargeIndex(
				GlobalTestInitializer.DefaultTenantId,
				288,
				GlobalTestInitializer.DefaultCountryId,
				"16501",
				GlobalTestInitializer.DefaultCountryId,
				"16504");
		}

        [Test]
        public void CanFetchVendorRatings()
        {
            var criteria = new VendorRatingViewSearchCriteria();

            var field = RatingSearchFields.VendorRatings.FirstOrDefault(f => f.Name == "VendorName");
            if (field == null)
            {
                Console.WriteLine("Vendor rating search fields no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }
            criteria.Parameters.Add(new ParameterColumn { DefaultValue = SearchUtilities.WildCard, ReportColumnName = field.DisplayName, Operator = Operator.Contains });
            
            var ratings = new VendorRatingSearch().FetchVendorRatings(criteria, GlobalTestInitializer.DefaultTenantId);

            Assert.IsTrue(ratings.Any());
        }
	}
}
