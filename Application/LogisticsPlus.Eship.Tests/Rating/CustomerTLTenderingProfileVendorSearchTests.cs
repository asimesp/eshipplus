﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
	[TestFixture]
	class CustomerTLTenderingProfileVendorSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchTLProfileVendorByVendorId()
		{
			var columns = new List<ParameterColumn>();
			var vendors = new VendorSearch().FetchVendors(columns, GlobalTestInitializer.DefaultTenantId);
			if (!vendors.Any()) return;

			var profiles = new CustomerTLTenderingProfileVendorSearch().FetchTLProfileVendorByVendorId(vendors.First().Id);
			if (!profiles.Any()) return;
			Assert.IsTrue(profiles.Any());
		}
	}
}
