﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Rating;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
    public class SmallPackageServiceMapHandlerTests
    {
        private SmallPackageServiceMapView _view;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _view = new SmallPackageServiceMapView();
            _view.Load();
        }

        [Test]
        public void CanSaveOrUpdate()
        {
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].SmallPackageServiceMaps;

            if (all.Count == 0) return;

            var smallPackageServiceMap = all[0];
            _view.LockRecord(smallPackageServiceMap);
            smallPackageServiceMap.TakeSnapShot();

            var smallPackEngineService = smallPackageServiceMap.SmallPackEngineService;
            smallPackageServiceMap.SmallPackEngineService = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff");
            _view.SaveRecord(smallPackageServiceMap);
            smallPackageServiceMap.TakeSnapShot();
            smallPackageServiceMap.SmallPackEngineService = smallPackEngineService;
            _view.SaveRecord(smallPackageServiceMap);
            _view.UnLockRecord(smallPackageServiceMap);
        }

        [Test]
        public void CanHandleSaveAndDelete()
        {
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].SmallPackageServiceMaps;

            if (all.Count == 0) return;

            var smallPackageServiceMap = new SmallPackageServiceMap
            {
                TenantId = GlobalTestInitializer.DefaultTenantId,
                SmallPackageEngine = SmallPackageEngine.Ups,
                ServiceId = all[0].ServiceId,
                SmallPackEngineService = "TestPackageType1000"
            };


            _view.SaveRecord(smallPackageServiceMap);
            Assert.IsTrue(smallPackageServiceMap.Id != default(long));
            _view.DeleteRecord(smallPackageServiceMap);
            Assert.IsTrue(!new SmallPackageServiceMap(smallPackageServiceMap.Id, false).KeyLoaded);

        }



        internal class SmallPackageServiceMapView : ISmallPackageServiceMapView
        {
            public User ActiveUser
            {
                get { return GlobalTestInitializer.ActiveUser; }
            }

            public Dictionary<int, string> SmallPackageEngines
            {
                set { }
            }

            public Dictionary<string, string> SmallPackageEngineServices
            {
                set { }
            }

            public List<ServiceViewSearchDto> Services
            {
                set { }
            }

            public event EventHandler Loading;
            public event EventHandler Search;
            public event EventHandler<ViewEventArgs<SmallPackageServiceMap>> Save;
            public event EventHandler<ViewEventArgs<SmallPackageServiceMap>> Delete;
            public event EventHandler<ViewEventArgs<SmallPackageServiceMap>> Lock;
            public event EventHandler<ViewEventArgs<SmallPackageServiceMap>> UnLock;

            public void DisplaySearchResult(List<SmallPackageServiceMap> smallPackageServiceMaps)
            {
                Assert.IsTrue(smallPackageServiceMaps.Count > 0);
            }

            public void LogException(Exception ex)
            {
                    
            }

            public void DisplayMessages(IEnumerable<ValidationMessage> messages)
            {

            }

            public void FailedLock(Lock @lock)
            {

            }

            public void Load()
            {
                var handler = new SmallPackageServiceMapHandler(this);
                handler.Initialize();
            }
            public void SaveRecord(SmallPackageServiceMap smallPackageServiceMap)
            {
                if (Save != null)
                    Save(this, new ViewEventArgs<SmallPackageServiceMap>(smallPackageServiceMap));
            }

            public void DeleteRecord(SmallPackageServiceMap smallPackageServiceMap)
            {
                if (Delete != null)
                    Delete(this, new ViewEventArgs<SmallPackageServiceMap>(smallPackageServiceMap));
            }

            public void LockRecord(SmallPackageServiceMap smallPackageServiceMap)
            {
                if (Lock != null)
                    Lock(this, new ViewEventArgs<SmallPackageServiceMap>(smallPackageServiceMap));
            }

            public void UnLockRecord(SmallPackageServiceMap smallPackageServiceMap)
            {
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<SmallPackageServiceMap>(smallPackageServiceMap));
            }
        }
    }
}

