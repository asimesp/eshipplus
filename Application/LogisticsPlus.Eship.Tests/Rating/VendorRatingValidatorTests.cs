﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation.Rating;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
	[TestFixture]
	public class VendorRatingValidatorTests
	{
		private VendorRatingValidator _validator;
		private VendorRating _rating;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new VendorRatingValidator();

			var tenantId = GlobalTestInitializer.DefaultTenantId;
			_rating = new VendorRating
			          	{
			          		CurrentLTLFuelMarkup = 20.0000m,
			          		DisplayName = "Display",
			          		FuelChargeCodeId = GlobalTestInitializer.DefaultChargeCodeId,
			          		FuelIndexRegion = FuelIndexRegion.National,
			          		FuelMarkupCeiling = 25.0000m,
			          		FuelMarkupFloor = 15.0000m,
			          		FuelTableId = default(long),
			          		HasAdditionalCharges = true,
			          		HasLTLCubicFootCapacityRules = true,
			          		HasLTLOverLengthRules = true,
			          		LTLAccessorials = new List<LTLAccessorial>(),
			          		LTLAdditionalCharges = new List<LTLAdditionalCharge>(),
			          		LTLCubicFootCapacityRules = new List<LTLCubicFootCapacityRule>(),
			          		LTLDiscountTiers = new List<DiscountTier>(),
			          		LTLOverLengthRules = new List<LTLOverLengthRule>(),
                            LTLPackageSpecificRates = new List<LTLPackageSpecificRate>(),
			          		LTLTariff = "ABF5040120060403N",
			          		LTLTruckHeight = 96,
			          		LTLTruckLength = 144,
			          		LTLTruckWidth = 96,
			          		LTLTruckWeight = 10000,
							LTLMinPickupWeight = 0,
			          		LTLTruckUnitWeight = 5000,
			          		Name = "Name2",
			          		TenantId = tenantId,
							Vendor = new VendorSearch().FetchVendors(new List<ParameterColumn>(), tenantId)[0],
							OverrideAddress = string.Empty,
							EnableLTLPcfToFcConversion = true,
							LTLMaxCfForPcfConv = 750.0000m,
                            EnableLTLPackageSpecificRates = true,
                            LTLPackageSpecificRatePackageTypeId = GlobalTestInitializer.DefaultPackageTypeId,
                            LTLPackageSpecificFreightChargeCodeId = GlobalTestInitializer.DefaultChargeCodeId,
							CubicCapacityPenaltyHeight = 96,
							CubicCapacityPenaltyWidth = 96,
                            MaxItemHeight = 0,
                            MaxItemWidth = 0,
                            MaxItemLength = 0,
                            IndividualItemLimitsApply = false,
                            MaxPackageQuantityApplies = false,
                            MaxPackageQuantity = 0,
                            RatingDetailNotes = string.Empty,
							CriticalBOLNotes = string.Empty,
							Smc3ServiceLevel = string.Empty,
			          	};
			_rating.LTLAccessorials.Add(new LTLAccessorial
			                            	{
			                            		CeilingValue = 200.0000m,
			                            		EffectiveDate = DateTime.Now,
			                            		FloorValue = 20.0000m,
			                            		Rate = 100.0000m,
			                            		RateType = RateType.PerHundredWeight,
												ServiceId = new ServiceSearch().FetchServices(new List<ParameterColumn>(), tenantId)[0].Id,
			                            		TenantId = tenantId,
			                            		VendorRating = _rating
			                            	});
			_rating.LTLAdditionalCharges.Add(new LTLAdditionalCharge
			                                 	{
			                                 		AdditionalChargeIndices = new List<AdditionalChargeIndex>(),
			                                 		ChargeCodeId = GlobalTestInitializer.DefaultChargeCodeId,
			                                 		EffectiveDate = DateTime.Now,
			                                 		Name = "Name",
			                                 		TenantId = tenantId,
			                                 		VendorRating = _rating
			                                 	});
			_rating.LTLAdditionalCharges[0].AdditionalChargeIndices.Add(new AdditionalChargeIndex
			                                                            	{
			                                                            		ApplyOnDestination = true,
			                                                            		ApplyOnOrigin = false,
			                                                            		ChargeCeiling = 20.0000m,
			                                                            		ChargeFloor = 15.0000m,
			                                                            		LTLAdditionalCharge = _rating.LTLAdditionalCharges[0],
			                                                            		PostalCode = "16504",
																				CountryId = GlobalTestInitializer.DefaultCountryId,
			                                                            		Rate = 18.0000m,
			                                                            		RateType = RateType.LineHaulPercentage,
			                                                            		TenantId = tenantId,
																				UsePostalCode = true,
			                                                            	});
			_rating.LTLCubicFootCapacityRules.Add(new LTLCubicFootCapacityRule
			                                      	{
			                                      		AppliedFreightClass = 50,
			                                      		ApplyFAK = true,
														ApplyDiscount = true,
			                                      		AveragePoundPerCubicFootApplies = true,
			                                      		AveragePoundPerCubicFootLimit = 5,
			                                      		EffectiveDate = DateTime.Now,
			                                      		LowerBound = 0,
			                                      		UpperBound = 750,
			                                      		PenaltyPoundPerCubicFoot = 5,
			                                      		TenantId = tenantId,
			                                      		VendorRating = _rating
			                                      	});
			_rating.LTLDiscountTiers.Add(new DiscountTier
			                             	{
			                             		CeilingValue = 120.0000m,
												DestinationRegion = new RegionSearch().FetchRegions(new List<ParameterColumn>(), tenantId)[0],
			                             		DiscountPercent = 84.0000m,
			                             		EffectiveDate = DateTime.Now,
			                             		FAK100 = 100,
			                             		FAK110 = 110,
			                             		FAK125 = 125,
			                             		FAK150 = 150,
			                             		FAK175 = 175,
			                             		FAK200 = 200,
			                             		FAK250 = 250,
			                             		FAK300 = 300,
			                             		FAK400 = 400,
			                             		FAK50 = 50,
			                             		FAK500 = 500,
			                             		FAK55 = 55,
			                             		FAK60 = 60,
			                             		FAK65 = 65,
			                             		FAK70 = 70,
			                             		FAK775 = 77.5,
			                             		FAK85 = 85,
			                             		FAK925 = 92.5,
			                             		FloorValue = 80.0000m,
			                             		FreightChargeCodeId = GlobalTestInitializer.DefaultChargeCodeId,
												OriginRegion = new RegionSearch().FetchRegions(new List<ParameterColumn>(), tenantId)[0],
			                             		RateBasis = DiscountTierRateBasis.FreightClass,
												WeightBreak = WeightBreak.Ignore,
			                             		TenantId = tenantId,
			                             		TierPriority = 1,
			                             		VendorRating = _rating,
			                             	});
			_rating.LTLOverLengthRules.Add(new LTLOverLengthRule
			                               	{
												Charge = 20.0000m,
												ChargeCodeId = GlobalTestInitializer.DefaultChargeCodeId,
												EffectiveDate = DateTime.Now,
												LowerLengthBound = 144.0001m,
												TenantId = tenantId,
												UpperLengthBound = 200.0000m,
												VendorRating = _rating
			                               	});
			_rating.LTLPcfToFcConvTab.Add(new LTLPcfToFcConversion
			                              	{
			                              		EffectiveDate = DateTime.Now,
			                              		TenantId = GlobalTestInitializer.DefaultTenantId,
			                              		VendorRating = _rating,
			                              		ConversionTableData =
			                              			@"lower	Upper	Fc
0.0000	1.9999	50
2.0000	4.9999	125"
			                              	});
		    var region = new RegionSearch().FetchRegions(new List<ParameterColumn>(), tenantId)[0];
            _rating.LTLPackageSpecificRates.Add(new LTLPackageSpecificRate
                {
                    TenantId = tenantId,
                    VendorRating = _rating,
                    EffectiveDate = DateTime.Now,
                    Rate = 10,
                    DestinationRegionId = region.Id,
                    OriginRegionId = region.Id,
                    PackageQuantity = 2,
                    RatePriority = 1
                });

            _rating.LTLGuaranteedCharges.Add(new LTLGuaranteedCharge
                {
                    TenantId = tenantId,
                    VendorRating = _rating,
                    ChargeCodeId = GlobalTestInitializer.DefaultChargeCodeId,
                    Description = DateTime.Now.ToString(),
                    EffectiveDate = DateTime.Now,
                    FloorValue = 115.25m,
                    CeilingValue = 670.91m,
                    Rate = 15,
                    Time = "12:45",
                    RateType = RateType.LineHaulPercentage,
                    CriticalNotes = DateTime.Now.ToString(),
                });
		}

		[Test]
		public void IsValidTest()
		{
			var isValid = _validator.IsValid(_rating);
			_validator.Messages.PrintMessages();
			Assert.IsTrue(isValid);
		}

		[Test]
		public void HasRequiredDataTest()
		{
			var hasAllRequiredData = _validator.HasAllRequiredData(_rating);
			if (!hasAllRequiredData) _validator.Messages.PrintMessages();
			Assert.IsTrue(hasAllRequiredData);
		}

		[Test]
		public void DuplicateRatingNameTest()
		{
			var all = new VendorRatingSearch().FetchVendorRatings(new VendorRatingViewSearchCriteria(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var rating = new VendorRating { Name = all[0].Name, TenantId = all[0].TenantId };
			Assert.IsTrue(_validator.DuplicateVendorRatingnName(rating));
		}

		[Test]
		public void CanDeleteTest()
		{
            var all = new VendorRatingSearch().FetchVendorRatings(new VendorRatingViewSearchCriteria(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var rating = new VendorRating(all[0].Id, false);
			Assert.IsTrue(rating.KeyLoaded);
			_validator.Messages.Clear();
			_validator.CanDeleteVendorRating(rating);
			_validator.Messages.PrintMessages();
		}
	}
}
