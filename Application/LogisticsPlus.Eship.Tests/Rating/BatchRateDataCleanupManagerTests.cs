﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Rating;
using LogisticsPlus.Eship.Processor.Searches.Core;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
	[TestFixture]
	public class BatchRateDataCleanupManagerTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			ProcessorVars.DeleteCompletedRateAnalysisRecordAfterDays.Clear();
			foreach (var tenant in new TenantSearch().FetchTenants(new List<ParameterColumn>()).Distinct())
			{
				ProcessorVars.DeleteCompletedRateAnalysisRecordAfterDays.Add(tenant.Id, 7);
			}
		}

		[Test]
		public void CanCompletedBatchRateData()
		{
			new BatchRateDataCleanUpManager().PurgeCompletedReadyForDeleteAfter();
		}
	}
}
