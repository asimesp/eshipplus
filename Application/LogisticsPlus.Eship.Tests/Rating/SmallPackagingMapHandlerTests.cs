﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Rating;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
    public class SmallPackagingMapHandlerTests
    {
        private SmallPackagingMapView _view;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _view = new SmallPackagingMapView();
            _view.Load();
        }

        [Test]
        public void CanSaveOrUpdate()
        {
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].SmallPackagingMaps;

            if (all.Count == 0) return;

            var smallPackagingMap = all[0];
            _view.LockRecord(smallPackagingMap);
            smallPackagingMap.TakeSnapShot();

            var smallPackageType = smallPackagingMap.SmallPackEngineType;
            smallPackagingMap.SmallPackEngineType = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff");
            _view.SaveRecord(smallPackagingMap);
            smallPackagingMap.TakeSnapShot();
            smallPackagingMap.SmallPackEngineType = smallPackageType;
            _view.SaveRecord(smallPackagingMap);
            _view.UnLockRecord(smallPackagingMap);
        }

        [Test]
        public void CanHandleSaveAndDelete()
        {
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].SmallPackagingMaps;
			if (!all.Any()) return;
        	var smallPackagingMap = new SmallPackagingMap
        	                        	{
        	                        		TenantId = GlobalTestInitializer.DefaultTenantId,
        	                        		SmallPackageEngine = SmallPackageEngine.Ups,
        	                        		PackageTypeId = all[0].PackageTypeId,
        	                        		SmallPackEngineType = "TestPackageType1000"
        	                        	};


            _view.SaveRecord(smallPackagingMap);
            Assert.IsTrue(smallPackagingMap.Id != default(long));
            _view.DeleteRecord(smallPackagingMap);
            Assert.IsTrue(!new SmallPackagingMap(smallPackagingMap.Id, false).KeyLoaded);

        }

        internal class SmallPackagingMapView : ISmallPackagingMapView
        {
            public User ActiveUser
            {
                get { return GlobalTestInitializer.ActiveUser; }
            }

            public Dictionary<int, string> SmallPackageEngines
            {
                set {}
            }

            public Dictionary<SmallPackageEngine, Dictionary<string, string>> SmallPackageEnginesTypes
            {
                set { }
            }

            public List<PackageType> PackageTypes
            {
                set { }
            }

            public event EventHandler Loading;
            public event EventHandler<ViewEventArgs<SmallPackagingMap>> Save;
            public event EventHandler<ViewEventArgs<SmallPackagingMap>> Delete;
            public event EventHandler<ViewEventArgs<SmallPackagingMap>> Lock;
            public event EventHandler<ViewEventArgs<SmallPackagingMap>> UnLock;
            public event EventHandler<ViewEventArgs<string>> Search;


            public void DisplaySearchResult(List<SmallPackagingMap> smallPackagingMap)
            {
                Assert.IsTrue(smallPackagingMap.Count > 0);
            }

            public void LogException(Exception ex)
            {
                    
            }

            public void DisplayMessages(IEnumerable<ValidationMessage> messages)
            {
               messages.PrintMessages();
			}
            
            public void FailedLock(Lock @lock)
            {
             
            }

            public void Load()
            {
                var handler = new SmallPackagingMapHandler(this);
                handler.Initialize();
            }

            public void SaveRecord(SmallPackagingMap smallPackagingMap)
            {
                if (Save != null)
                    Save(this, new ViewEventArgs<SmallPackagingMap>(smallPackagingMap));
            }

            public void DeleteRecord(SmallPackagingMap smallPackagingMap)
            {
                if (Delete != null)
                    Delete(this, new ViewEventArgs<SmallPackagingMap>(smallPackagingMap));
            }

            public void LockRecord(SmallPackagingMap smallPackagingMap)
            {
                if (Lock != null)
                    Lock(this, new ViewEventArgs<SmallPackagingMap>(smallPackagingMap));
            }

            public void UnLockRecord(SmallPackagingMap smallPackagingMap)
            {
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<SmallPackagingMap>(smallPackagingMap));
            }
        }
    }
}

