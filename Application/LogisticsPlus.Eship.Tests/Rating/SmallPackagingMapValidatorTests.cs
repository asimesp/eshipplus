﻿using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Validation.Rating;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
	[TestFixture]
	public class SmallPackagingMapValidatorTests
	{
        private SmallPackagingMapValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _validator = new SmallPackagingMapValidator();
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].SmallPackagingMaps;
            if (all.Count == 0) return;

		    var smallPackagingMap = new SmallPackagingMap
		                              {
                                          TenantId = GlobalTestInitializer.DefaultTenantId,
                                          SmallPackageEngine = SmallPackageEngine.FedEx,
                                          PackageTypeId = all[0].PackageTypeId,
                                          SmallPackEngineType = "TestPackageType1000"
                                      };

           Assert.IsTrue(_validator.HasAllRequiredData(smallPackagingMap));
		}

        [Test]
        public void IsExistingAndUniqueTest()
        {
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].SmallPackagingMaps;
            if (all.Count == 0) return;

            var smallPackagingMap = all[0];
            Assert.IsTrue(_validator.IsUnique(smallPackagingMap)); // this should be unique
           
            var newSmallPackagingMap = new SmallPackagingMap(1000000, false);
            Assert.IsFalse(newSmallPackagingMap.KeyLoaded); // will not be found

            newSmallPackagingMap.PackageTypeId = smallPackagingMap.PackageTypeId;
            newSmallPackagingMap.SmallPackageEngine = smallPackagingMap.SmallPackageEngine;
            newSmallPackagingMap.TenantId = smallPackagingMap.TenantId;

            Assert.IsFalse(_validator.IsUnique(newSmallPackagingMap)); // should be false
        }
	}
}
