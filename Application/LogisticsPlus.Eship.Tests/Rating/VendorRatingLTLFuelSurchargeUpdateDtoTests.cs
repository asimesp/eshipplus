﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Processor.Dto.Rating;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
	[TestFixture]
	public class VendorRatingLTLFuelSurchargeUpdateDtoTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanRetrieveVendorRatingLTLFuelSurchargeListForUpdate()
		{
			var list = new VendorRatingLTLFuelSurchargeUpdateDto().RetrieveListForUpdate(DateTime.Now, GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(list.Any());
		}
	}
}
