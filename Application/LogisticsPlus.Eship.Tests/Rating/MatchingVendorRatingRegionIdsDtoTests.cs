﻿using LogisticsPlus.Eship.Processor.Dto.Rating;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
	[TestFixture]
	public class MatchingVendorRatingRegionIdsDtoTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void MatchingVendorRatingRegionIdsDtoTest()
		{
			var criteria = new MatchingRatingRegionIdCriteria
			               	{
			               		OriginPostalCode = "16509",
			               		DestinationPostalCode = "16509",
			               		OriginCountryId = GlobalTestInitializer.DefaultCountryId,
			               		DestinationCountryId = GlobalTestInitializer.DefaultCountryId,
			               		RatingId = 0
			               	};

			new MatchingVendorRatingRegionIdsDto(GlobalTestInitializer.DefaultTenantId, criteria);
		}
	}
}
