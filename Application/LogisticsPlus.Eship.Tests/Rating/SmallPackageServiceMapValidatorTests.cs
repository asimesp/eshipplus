﻿using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Validation.Rating;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
	[TestFixture]
	public class SmallPackageServiceMapValidatorTests
	{
        private SmallPackageServiceMapValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _validator = new SmallPackageServiceMapValidator();
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].SmallPackageServiceMaps;
            if (all.Count == 0) return;

		    var smallPackageServiceMap = new SmallPackageServiceMap
		                              {
                                          TenantId = GlobalTestInitializer.DefaultTenantId,
                                          SmallPackageEngine = SmallPackageEngine.FedEx,
                                          ServiceId = all[0].ServiceId,
                                          SmallPackEngineService = "TestServiceType1000"
                                      };

           Assert.IsTrue(_validator.HasAllRequiredData(smallPackageServiceMap));
		}

        [Test]
        public void IsExistingAndUniqueTest()
        {
            var all = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].SmallPackageServiceMaps;
            if (all.Count == 0) return;

            var smallPackageServiceMap = all[0];
            Assert.IsTrue(_validator.IsUnique(smallPackageServiceMap)); // this should be unique
           
            var newSmallPackageServiceMap = new SmallPackageServiceMap(1000000, false);
            Assert.IsFalse(newSmallPackageServiceMap.KeyLoaded); // will not be found

            newSmallPackageServiceMap.ServiceId = smallPackageServiceMap.ServiceId;
            newSmallPackageServiceMap.SmallPackageEngine = smallPackageServiceMap.SmallPackageEngine;
            newSmallPackageServiceMap.TenantId = smallPackageServiceMap.TenantId;

            Assert.IsFalse(_validator.IsUnique(newSmallPackageServiceMap)); // should be false
        }
	}
}
