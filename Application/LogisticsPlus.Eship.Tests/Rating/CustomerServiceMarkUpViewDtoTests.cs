﻿using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Dto.Rating;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
	[TestFixture]
	public class CustomerServiceMarkUpViewDtoTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanRetrieveCustomerRatingSmallPackRates()
		{
			new CustomerServiceMarkUpViewDto().RetrieveCustomerRatingCustomerServiceMarkUps(new CustomerRating(2, false));
		}
	}
}
