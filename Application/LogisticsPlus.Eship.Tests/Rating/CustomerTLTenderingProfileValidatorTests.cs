﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
	[TestFixture]
	public class CustomerTLTenderingProfileValidatorTests
	{
		private CustomerTLTenderingProfileValidator _validator;
		private CustomerTLTenderingProfile _truckLoadProfile;

		

		[SetUp]
		public void Initialize()
		{
			var tenantId = GlobalTestInitializer.DefaultTenantId;

			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new CustomerTLTenderingProfileValidator();

			_truckLoadProfile = new CustomerTLTenderingProfile
				{
					
					TenantId = tenantId,
					Customer = new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), tenantId).FirstOrDefault() ?? new Customer(),
					Lanes = new List<CustomerTLTenderingProfileLane>(),
					Vendors = new List<CustomerTLTenderingProfileVendor>(),
					MaxWaitTime = 30


				};
			_truckLoadProfile.Vendors.Add(new CustomerTLTenderingProfileVendor
				{
					VendorId = 1,
					CommunicationType = (NotificationMethod) 1,
					TLTenderingProfile = _truckLoadProfile,
					TenantId = tenantId

				}
				);
			
			_truckLoadProfile.Lanes.Add(new CustomerTLTenderingProfileLane
			{
				TenantId = tenantId,
				OriginCity = "Cleveland",
				OriginState = "Ohio",
				OriginPostalCode = "44114",
				OriginCountryId= new Country(new CountrySearch().FetchCountryIdByCode("US")).Id,
				DestinationCity = "Erie",
				DestinationState = "Ohio",
				DestinationPostalCode = "16501",
				DestinationCountryId = new Country(new CountrySearch().FetchCountryIdByCode("US")).Id,
				TLTenderingProfile = _truckLoadProfile,
			    OriginCountry = new Country(new CountrySearch().FetchCountryIdByCode("US")),
				DestinationCountry = new Country(new CountrySearch().FetchCountryIdByCode("US")),
				FirstPreferredVendor = new Vendor(new VendorSearch().FetchVendorByNumber("121",tenantId).Id),
				SecondPreferredVendor = new Vendor(new VendorSearch().FetchVendorByNumber("10000", tenantId).Id),
				ThirdPreferredVendor = new Vendor(new VendorSearch().FetchVendorByNumber("1", tenantId).Id),
			
				
			}
				);
		}

		[Test]
		public void IsValidTest()
		{
			Assert.IsTrue(_validator.IsValid(_truckLoadProfile));
		}

		[Test]
		public void HasRequiredDataTest()
		{
			var hasAllRequiredData = _validator.HasAllRequiredData(_truckLoadProfile);
			if (!hasAllRequiredData) _validator.Messages.PrintMessages();
			Assert.IsTrue(hasAllRequiredData);
		}

	}
}
