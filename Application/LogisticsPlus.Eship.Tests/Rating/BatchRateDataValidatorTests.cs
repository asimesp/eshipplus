﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Validation.Rating;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
	[TestFixture]
	public class BatchRateDataValidatorTests
	{
		private BatchRateData _data;
		private BatchRateDataValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new BatchRateDataValidator();
			_data = new BatchRateData
			        	{
			        		AnalysisEffectiveDate = DateTime.Now,
			        		ChargeCodeId = GlobalTestInitializer.DefaultChargeCodeId,
			        		DateCompleted = DateUtility.SystemEarliestDateTime,
			        		DateCreated = DateTime.Now,
			        		LTLIndirectPointEnabled = false,
			        		LaneData = "TEST DATA BUT NOT VALID FORMAT",
			        		Name = "tesT",
							GroupCode = string.Empty,
			        		PackageTypeId = default(long),
			        		ResultData = string.Empty,
			        		ServiceMode = ServiceMode.LessThanTruckload,
			        		SmallPackageEngine = SmallPackageEngine.None,
			        		SmallPackageEngineType = string.Empty,
			        		SubmittedByUserId = GlobalTestInitializer.DefaultUserId,
			        		TenantId = GlobalTestInitializer.DefaultTenantId,
			        		VendorRatingId = default(long)
			        	};
		}

		[Test]
		public void IsLTLValid()
		{
			_data.ServiceMode = ServiceMode.LessThanTruckload;
			_data.VendorRatingId = 1; // bogus
			var isValid = _validator.IsValid(_data);
			_validator.Messages.PrintMessages();
			Assert.IsTrue(isValid);
		}

		[Test]
		public void IsTLValid()
		{
			_data.ServiceMode = ServiceMode.Truckload;
			_data.VendorRatingId = 1; // bogus
			var isValid = _validator.IsValid(_data);
			_validator.Messages.PrintMessages();
			Assert.IsTrue(isValid);
		}

		[Test]
		public void IsSmallPackValid()
		{
			_data.ServiceMode = ServiceMode.SmallPackage;
			_data.SmallPackageEngineType = "Type";
			_data.SmallPackageEngine = SmallPackageEngine.FedEx;
			_data.PackageTypeId = 1; // bogus
			var isValid = _validator.IsValid(_data);
			_validator.Messages.PrintMessages();
			Assert.IsTrue(isValid);
		}
	}
}
