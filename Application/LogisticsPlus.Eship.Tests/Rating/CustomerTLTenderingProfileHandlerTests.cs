﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.Processor.Views.Rating;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
	[TestFixture]
	public class CustomerTLTenderingProfileHandlerTests
	{
		private CustomerTLTenderingProfileView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new CustomerTLTenderingProfileView();
			_view.Load();
		}

		[Test]
		public void CanSaveAndDelete()
		{
			var truckLoadProfile = GetCustomerTLTenderingProfile();
			_view.SaveRecord(truckLoadProfile);
			Assert.IsTrue(truckLoadProfile.Id != default(long));
			_view.DeleteRecord(truckLoadProfile);
			Assert.IsTrue(!new CustomerTLTenderingProfile(truckLoadProfile.Id, false).KeyLoaded);
		}

		[Test] 
		public void CanSaveOrUpdate()
		{
			var columns = new List<ParameterColumn>();
             var criteria = new CustomerTLTenderingProfileViewSearchCriteria {Parameters = columns};
		
            columns.Add(new ParameterColumn { DefaultValue = SearchUtilities.WildCard, Operator = Operator.Contains });

			var all = new CustomerTLTenderProfileSearch().FetchCustomerTLTenderProfiles(criteria, GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var profile = all[0];
			profile.LoadCollections();
			_view.LockRecord(profile);
			profile.TakeSnapShot();
			_view.SaveRecord(profile);
			profile.MaxWaitTime = 12;
			profile.TakeSnapShot();
			_view.SaveRecord(profile);
			_view.UnLockRecord(profile);
		}

		private CustomerTLTenderingProfile GetCustomerTLTenderingProfile()
		{
			var tenantId = GlobalTestInitializer.DefaultTenantId;
			var truckLoadProfile = new CustomerTLTenderingProfile
				{
					TenantId = tenantId,
					Customer =
						new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), tenantId).FirstOrDefault() ?? new Customer(),
					Lanes = new List<CustomerTLTenderingProfileLane>(),
					Vendors = new List<CustomerTLTenderingProfileVendor>(),
					MaxWaitTime = 30
				};

			truckLoadProfile.Vendors.Add(new CustomerTLTenderingProfileVendor
				{
					VendorId = 1,
					CommunicationType = (NotificationMethod) 2,
					TLTenderingProfile = truckLoadProfile,
					TenantId = tenantId
				}
				);

			truckLoadProfile.Lanes.Add(new CustomerTLTenderingProfileLane
				{
					TenantId = tenantId,
					OriginCity = "Cleveland",
					OriginState = "Ohio",
					OriginPostalCode = "44114",
					OriginCountryId = new Country(new CountrySearch().FetchCountryIdByCode("US")).Id,
					DestinationCity = "Erie",
					DestinationState = "Ohio",
					DestinationPostalCode = "16501",
					DestinationCountryId = new Country(new CountrySearch().FetchCountryIdByCode("US")).Id,
					TLTenderingProfile = truckLoadProfile,
					OriginCountry = new Country(new CountrySearch().FetchCountryIdByCode("US")),
					DestinationCountry = new Country(new CountrySearch().FetchCountryIdByCode("US")),
					FirstPreferredVendor = new Vendor(new VendorSearch().FetchVendorByNumber("121", tenantId).Id),
					SecondPreferredVendor = new Vendor(new VendorSearch().FetchVendorByNumber("10000", tenantId).Id),
					ThirdPreferredVendor = new Vendor(new VendorSearch().FetchVendorByNumber("1", tenantId).Id)
				}
				);

			return truckLoadProfile;
		}

		internal class CustomerTLTenderingProfileView : ICustomerTLTenderingProfile
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<CustomerTLTenderingProfile>> TLProfileSave;
			public event EventHandler<ViewEventArgs<CustomerTLTenderingProfile>> TLProfileDelete;
			public event EventHandler<ViewEventArgs<CustomerTLTenderingProfile>> TLProfileLock;
			public event EventHandler<ViewEventArgs<CustomerTLTenderingProfile>> TLProfileUnLock;
			public event EventHandler<ViewEventArgs<CustomerTLTenderingProfile>> TLProfileLoadAuditLog;
			public event EventHandler<ViewEventArgs<long>> TLProfileCanDeleteLane;
			public event EventHandler<ViewEventArgs<string>> CustomerSearch;
			public event EventHandler<ViewEventArgs<string>> VendorSearch;


			public void DisplayVendorForLane(Vendor vendor)
			{
				
			}

			public void LogException(Exception ex)
			{
			}

			public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}


			public void Set(CustomerTLTenderingProfile profile)
			{
			}

			public void DisplayCustomer(Customer customer)
			{
			}


			public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs)
			{
				//auditLogs.Load(logs);
			}

			public void FailedLock(Lock @lock)
			{
			}


			public void Load()
			{
				var handler = new CustomerTLTenderingProfileHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(CustomerTLTenderingProfile truckLoadProfile)
			{
				if (TLProfileSave != null)
					TLProfileSave(this, new ViewEventArgs<CustomerTLTenderingProfile>(truckLoadProfile));
			}

			public void DeleteRecord(CustomerTLTenderingProfile truckLoadProfile)
			{
				if (TLProfileDelete != null)
					TLProfileDelete(this, new ViewEventArgs<CustomerTLTenderingProfile>(truckLoadProfile));
			}

			public void LockRecord(CustomerTLTenderingProfile truckLoadProfile)
			{
				if (TLProfileLock != null)
					TLProfileLock(this, new ViewEventArgs<CustomerTLTenderingProfile>(truckLoadProfile));
			}

			public void UnLockRecord(CustomerTLTenderingProfile truckLoadProfile)
			{
				if (TLProfileUnLock != null)
					TLProfileUnLock(this, new ViewEventArgs<CustomerTLTenderingProfile>(truckLoadProfile));
			}

			public void LoadAuditLogs(CustomerTLTenderingProfile truckLoadProfile)
			{
				if (TLProfileLoadAuditLog != null)
					TLProfileLoadAuditLog(this, new ViewEventArgs<CustomerTLTenderingProfile>(truckLoadProfile));
			}
		}
	}
}

