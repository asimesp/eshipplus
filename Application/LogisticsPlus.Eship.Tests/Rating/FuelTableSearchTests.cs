﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
	[TestFixture]
	public class FuelTableSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchByWildCard()
		{
			var fuelTables = new FuelTableSearch().FetchFuelTables(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(fuelTables.Count > 0);
		}

        [Test]
        public void CanFetchEquipmentTypes()
        {
            var columns = new List<ParameterColumn>();

            var field = RegistrySearchFields.FuelTables.FirstOrDefault(f => f.Name == "Name");
            if (field == null)
            {
                Console.WriteLine("RegistrySearchFields.FuelTables no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }

            var column = field.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            columns.Add(column);

            var results = new FuelTableSearch().FetchFuelTables(columns, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(results.Any());

        }
	}
}
