﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
	[TestFixture]
	public class CustomerRatingTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanDeepCopy()
		{
			var customers = new CustomerSearch().FetchCustomersWithRating(new List<ParameterColumn>(),GlobalTestInitializer.DefaultTenantId);
			if (!customers.Any()) return;
			var r1 = customers[0].Rating;
			var r2 = new CustomerRating(r1.Id);

			Assert.IsTrue(r1.LTLSellRates[0].EffectiveDate == r2.LTLSellRates[0].EffectiveDate);
			r2.LTLSellRates[0].EffectiveDate = DateTime.Now;
			Assert.IsFalse(r1.LTLSellRates[0].EffectiveDate == r2.LTLSellRates[0].EffectiveDate);
		}
	}
}
