﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation.Rating;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.SmallPacks;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
	[TestFixture]
	public class CustomerRatingValidtorTests
	{
		private CustomerRatingValidator _validator;
		private CustomerRating _rating;
		private CustomerRating _ratingDuplicates;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new CustomerRatingValidator();

            var columns = new List<ParameterColumn>();

            var field = AccountingSearchFields.CustomerNumber;
            if (field != null) columns.Add(new ParameterColumn { DefaultValue = SearchUtilities.WildCard, ReportColumnName = field.DisplayName, Operator = Operator.Contains });


			var tenantId = GlobalTestInitializer.DefaultTenantId;
			var customersWithRating = new CustomerSearch().FetchCustomersWithRating(columns, tenantId);

		    var criteria = new CustomerViewSearchCriteria();
            var customers = new CustomerSearch().FetchCustomers(criteria, tenantId);
			var customer = customers.FirstOrDefault(c => !customersWithRating.Select(cr => cr.Id).Contains(c.Id));
		

			_rating = new CustomerRating
			          	{
			          		TenantId = tenantId,
			          		InsuranceEnabled = true,
			          		InsuranceChargeCodeId = GlobalTestInitializer.DefaultChargeCodeId,
			          		InsurancePurchaseFloor = 40.0000m,
			          		LineHaulProfitCeilingPercentage = 40.0000m,
			          		LineHaulProfitCeilingType = ValuePercentageType.Both,
			          		LineHaulProfitCeilingValue = 50.0000m,
			          		LineHaulProfitFloorPercentage = 20.0000m,
			          		LineHaulProfitFloorType = ValuePercentageType.Both,
			          		LineHaulProfitFloorValue = 20.000m,
			          		NoLineHaulProfit = false,
			          		UseLineHaulMinimumCeiling = false,
			          		UseLineHaulMinimumFloor = false,
			          		FuelProfitCeilingPercentage = 40.0000m,
			          		FuelProfitCeilingType = ValuePercentageType.Both,
			          		FuelProfitCeilingValue = 50.0000m,
			          		FuelProfitFloorPercentage = 20.0000m,
			          		FuelProfitFloorType = ValuePercentageType.Both,
			          		FuelProfitFloorValue = 20.000m,
			          		NoFuelProfit = false,
			          		UseFuelMinimumCeiling = false,
			          		UseFuelMinimumFloor = false,
			          		AccessorialProfitCeilingPercentage = 40.0000m,
			          		AccessorialProfitCeilingType = ValuePercentageType.Both,
			          		AccessorialProfitCeilingValue = 50.0000m,
			          		AccessorialProfitFloorPercentage = 20.0000m,
			          		AccessorialProfitFloorType = ValuePercentageType.Both,
			          		AccessorialProfitFloorValue = 20.000m,
			          		NoAccessorialProfit = false,
			          		UseAccessorialMinimumCeiling = false,
			          		UseAccessorialMinimumFloor = false,
			          		ServiceProfitCeilingPercentage = 40.0000m,
			          		ServiceProfitCeilingType = ValuePercentageType.Both,
			          		ServiceProfitCeilingValue = 50.0000m,
			          		ServiceProfitFloorPercentage = 20.0000m,
			          		ServiceProfitFloorType = ValuePercentageType.Both,
			          		ServiceProfitFloorValue = 20.000m,
			          		NoServiceProfit = false,
			          		UseServiceMinimumCeiling = false,
			          		UseServiceMinimumFloor = false,
							ResellerAddition = new ResellerAdditionSearch().FetchResellerAdditions(new List<ParameterColumn>(), tenantId).FirstOrDefault(),
			          		BillReseller = false,
							Customer = customer,
							TLFreightChargeCodeId = GlobalTestInitializer.DefaultChargeCodeId,
							TLFuelChargeCodeId = GlobalTestInitializer.DefaultChargeCodeId,
							TLFuelRate = 10.000m,
                            Project44AccountGroup = string.Empty,
							TLFuelRateType = RateType.Flat,
			          		LTLSellRates = new List<LTLSellRate>(),
							TLSellRates = new List<TLSellRate>(),
			          		SmallPackRates = new List<SmallPackRate>(),
			          		CustomerServiceMarkups = new List<CustomerServiceMarkup>(),
			          	};
			_rating.Customer.Rating = _rating;
			_rating.LTLSellRates.Add(new LTLSellRate
			                         	{
			                         		TenantId = tenantId,
			                         		CustomerRating = _rating,
			                         		VendorRating =
												new VendorRatingSearch().FetchVendorRatings(new VendorRatingViewSearchCriteria(), tenantId).FirstOrDefault(),
			                         		LTLIndirectPointEnabled = false,
			                         		EffectiveDate = DateTime.Now,
			                         		Active = true,
			                         		MarkupPercent = 40.0000m,
			                         		MarkupValue = 50.0000m,
			                         		UseMinimum = false,
                                            OverrideMarkupPercentVendorFloor = 10.0000m,
                                            OverrideMarkupValueVendorFloor = 20.0000m
			                         	});
			_rating.TLSellRates.Add(new TLSellRate
				{
					TenantId = tenantId,
					CustomerRating = _rating,
					DestinationCity = string.Empty,
					DestinationCountryId = GlobalTestInitializer.DefaultCountryId,
					DestinationPostalCode = string.Empty,
					DestinationState = string.Empty,
					EffectiveDate = DateTime.Now,
					OriginCity = string.Empty,
					OriginState = string.Empty,
					OriginPostalCode = string.Empty,
					OriginCountryId = GlobalTestInitializer.DefaultCountryId,
					EquipmentType = null,
					TariffType = TLTariffType.OneWay,
					MaximumWeight = 10,
					MinimumCharge = 10,
					MinimumWeight = 9,
					Mileage = 100,
				});
			_rating.SmallPackRates.Add(new SmallPackRate
			                           	{
			                           		TenantId = tenantId,
			                           		CustomerRating = _rating,
                                            VendorId = GlobalTestInitializer.DefaultVendorWithTerminalId,
			                           		SmallPackageEngine = SmallPackageEngine.FedEx,
			                           		SmallPackType = SmallPacksFactory.RetrieveServiceTypes()[SmallPackageEngine.FedEx][0],
			                           		MarkupPercent = 40.0000m,
			                           		MarkupValue = 50.0000m,
			                           		UseMinimum = false,
			                           		EffectiveDate = DateTime.Now,
			                           		Active = true,
                                            ChargeCode = ProcessorVars.RegistryCache[tenantId].ChargeCodes.FirstOrDefault() ?? new ChargeCode()
			                           	});
			var dto = new ServiceSearch().FetchServices(new List<ParameterColumn>(), tenantId).FirstOrDefault();
			_rating.CustomerServiceMarkups.Add(new CustomerServiceMarkup
			                                   	{
			                                   		TenantId = tenantId,
													CustomerRating = _rating,
													ServiceId = dto == null ? default(long) : dto.Id,
													MarkupPercent = 40.0000m,
													MarkupValue = 50.0000m,
													UseMinimum = false,
													EffectiveDate = DateTime.Now,
													Active = true,
													ServiceChargeCeiling = 500.0000m,
													ServiceChargeFloor = 0.0000m,                                                    
			                                   	});

			// Setup specific to CustomerRatingDuplicateImportsTest
			_ratingDuplicates = new CustomerRating();
			_ratingDuplicates.Copy(_rating);
			_ratingDuplicates.Customer.Rating = _ratingDuplicates;
			_ratingDuplicates.LTLSellRates.Add(new LTLSellRate
			{
				TenantId = tenantId,
				CustomerRating = _ratingDuplicates,
				VendorRating =
					new VendorRatingSearch().FetchVendorRatings(new VendorRatingViewSearchCriteria(), tenantId).FirstOrDefault(),
				LTLIndirectPointEnabled = false,
				EffectiveDate = DateTime.Now.Date,
				Active = true,
				MarkupPercent = 40.0000m,
				MarkupValue = 50.0000m,
				UseMinimum = false
			});
			_ratingDuplicates.LTLSellRates.Add(new LTLSellRate
			{
				TenantId = tenantId,
				CustomerRating = _ratingDuplicates,
				VendorRating =
					new VendorRatingSearch().FetchVendorRatings(new VendorRatingViewSearchCriteria(), tenantId).FirstOrDefault(),
				LTLIndirectPointEnabled = false,
				EffectiveDate = DateTime.Now.Date,
				Active = true,
				MarkupPercent = 40.0000m,
				MarkupValue = 50.0000m,
				UseMinimum = false
			});
			_ratingDuplicates.TLSellRates.Add(new TLSellRate
			{
				TenantId = tenantId,
				CustomerRating = _ratingDuplicates,
				DestinationCity = string.Empty,
				DestinationCountryId = GlobalTestInitializer.DefaultCountryId,
				DestinationPostalCode = string.Empty,
				DestinationState = string.Empty,
				EffectiveDate = DateTime.Now.Date,
				OriginCity = string.Empty,
				OriginState = string.Empty,
				OriginPostalCode = string.Empty,
				OriginCountryId = GlobalTestInitializer.DefaultCountryId,
				EquipmentType = null,
				TariffType = TLTariffType.OneWay,
				MaximumWeight = 10,
				MinimumCharge = 10,
				MinimumWeight = 9,
				Mileage = 100,
			});
			_ratingDuplicates.TLSellRates.Add(new TLSellRate
			{
				TenantId = tenantId,
				CustomerRating = _ratingDuplicates,
				DestinationCity = string.Empty,
				DestinationCountryId = GlobalTestInitializer.DefaultCountryId,
				DestinationPostalCode = string.Empty,
				DestinationState = string.Empty,
				EffectiveDate = DateTime.Now.Date,
				OriginCity = string.Empty,
				OriginState = string.Empty,
				OriginPostalCode = string.Empty,
				OriginCountryId = GlobalTestInitializer.DefaultCountryId,
				EquipmentType = null,
				TariffType = TLTariffType.OneWay,
				MaximumWeight = 10,
				MinimumCharge = 10,
				MinimumWeight = 9,
				Mileage = 100,
			});
			_ratingDuplicates.SmallPackRates.Add(new SmallPackRate
			{
				TenantId = tenantId,
				CustomerRating = _ratingDuplicates,
				VendorId = GlobalTestInitializer.DefaultVendorWithTerminalId,
				SmallPackageEngine = SmallPackageEngine.FedEx,
				SmallPackType = SmallPacksFactory.RetrieveServiceTypes()[SmallPackageEngine.FedEx][0],
				MarkupPercent = 40.0000m,
				MarkupValue = 50.0000m,
				UseMinimum = false,
				EffectiveDate = DateTime.Now.Date,
				Active = true,
				ChargeCode = ProcessorVars.RegistryCache[tenantId].ChargeCodes.FirstOrDefault() ?? new ChargeCode()
			});
			_ratingDuplicates.SmallPackRates.Add(new SmallPackRate
			{
				TenantId = tenantId,
				CustomerRating = _ratingDuplicates,
				VendorId = GlobalTestInitializer.DefaultVendorWithTerminalId,
				SmallPackageEngine = SmallPackageEngine.FedEx,
				SmallPackType = SmallPacksFactory.RetrieveServiceTypes()[SmallPackageEngine.FedEx][0],
				MarkupPercent = 40.0000m,
				MarkupValue = 50.0000m,
				UseMinimum = false,
				EffectiveDate = DateTime.Now.Date,
				Active = true,
				ChargeCode = ProcessorVars.RegistryCache[tenantId].ChargeCodes.FirstOrDefault() ?? new ChargeCode()
			});
			var dtoDuplicate = new ServiceSearch().FetchServices(new List<ParameterColumn>(), tenantId).FirstOrDefault();
			_ratingDuplicates.CustomerServiceMarkups.Add(new CustomerServiceMarkup
			{
				TenantId = tenantId,
				CustomerRating = _ratingDuplicates,
				ServiceId = dtoDuplicate == null ? default(long) : dtoDuplicate.Id,
				MarkupPercent = 40.0000m,
				MarkupValue = 50.0000m,
				UseMinimum = false,
				EffectiveDate = DateTime.Now.Date,
				Active = true,
				ServiceChargeCeiling = 500.0000m,
				ServiceChargeFloor = 0.0000m,
			});
			_ratingDuplicates.CustomerServiceMarkups.Add(new CustomerServiceMarkup
			{
				TenantId = tenantId,
				CustomerRating = _ratingDuplicates,
				ServiceId = dtoDuplicate == null ? default(long) : dtoDuplicate.Id,
				MarkupPercent = 40.0000m,
				MarkupValue = 50.0000m,
				UseMinimum = false,
				EffectiveDate = DateTime.Now.Date,
				Active = true,
				ServiceChargeCeiling = 500.0000m,
				ServiceChargeFloor = 0.0000m,
			});



		}

		[Test]
		public void IsValidTest()
		{
			Assert.IsTrue(_validator.IsValid(_rating));
		}

		[Test]
		public void HasRequiredDataTest()
		{
			var hasAllRequiredData = _validator.HasAllRequiredData(_rating);
			if (!hasAllRequiredData) _validator.Messages.PrintMessages();
			Assert.IsTrue(hasAllRequiredData);
		}

		[Test]
		public void CustomerRatingDuplicateImportsTest()
		{
			var noDuplicateImports = _validator.HasAllRequiredData(_ratingDuplicates);
			if (!noDuplicateImports) _validator.Messages.PrintMessages();
			Assert.IsFalse(noDuplicateImports);
			
		}


	}
}
