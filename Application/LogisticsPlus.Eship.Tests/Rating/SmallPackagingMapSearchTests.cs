﻿using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
	[TestFixture]
	public class SmallPackagingMapSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchByTenant()
		{
			new SmallPackagingMapSearch().FetchSmallPackagingMaps(SearchUtilities.WildCard, GlobalTestInitializer.DefaultTenantId);
		}
	}
}
