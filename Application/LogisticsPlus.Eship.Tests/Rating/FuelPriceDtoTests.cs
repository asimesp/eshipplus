﻿using System;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Rating;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rating
{
	[TestFixture]
	public class FuelPriceDtoTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanGetFuelPrice()
		{
			var dto = new FuelPriceDto();
			dto.GetFuelPrice(FuelIndexRegion.WestCoastLessCalifornia.ToInt(), DateTime.Now, GlobalTestInitializer.DefaultTenantId);
		}
	}
}
