﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Validation.Rating;
using NUnit.Framework;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Tests.Rating
{
	[TestFixture]
	public class RegionValidatorTests
	{
		private RegionValidator _validator;
		private Region _region;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new RegionValidator();

			if (_region != null) return;

			var tenantId = GlobalTestInitializer.DefaultTenantId;
			_region = new Region
						{
							Name = "Test Fuel Table",
							TenantId = tenantId,
							Areas = new List<Area>(),
						};
			_region.Areas.Add(new Area
										{
											Region = _region,
											SubRegion = _region,
											TenantId = tenantId,
											PostalCode = "16501",
											CountryId = 1,
											UseSubRegion = false,
										});
		}

		[Test]
		public void IsValidTest()
		{
			Assert.IsTrue(_validator.IsValid(_region));
		}

		[Test]
		public void HasRequiredDataTest()
		{
			var hasAllRequiredData = _validator.HasAllRequiredData(_region);
			if (!hasAllRequiredData) _validator.Messages.PrintMessages();
			Assert.IsTrue(hasAllRequiredData);
		}

		[Test]
		public void DuplicateGroupNameTest()
		{
			var all = new RegionSearch().FetchRegions(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var region = new Region
							{
								Name = all[0].Name,
								TenantId = all[0].TenantId,
								Areas = all[0].Areas
							};
			Assert.IsTrue(_validator.DuplicateRegionName(region));
		}

		[Test]
		public void EndlessRecursiveSubRegionNestingTests()
		{
			new EndlessRecursiveHelper().PerformTest(_validator);
		}

		[Test]
		public void CanDeleteTest()
		{
			var all = new RegionSearch().FetchRegions(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var region = new Region(all[0].Id, false);
			Assert.IsTrue(region.KeyLoaded);
			_validator.Messages.Clear();
			var canDeleteTenant = _validator.CanDeleteRegion(region);
			//Assert.IsTrue(_validator.Messages.Count > 0);
			//Assert.IsFalse(canDeleteTenant);
			_validator.Messages.PrintMessages();
		}

		internal class EndlessRecursiveHelper : EntityBase
		{
			internal void PerformTest(RegionValidator validator)
			{
				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();
				try
				{
					validator.Connection = Connection;
					validator.Transaction = Transaction;

					var tenantId = GlobalTestInitializer.DefaultTenantId;
					var regionA = new Region
									{
										TenantId = tenantId,
										Name = "EndlessRecursiveSubRegionNestingTests Region A",
										Areas = new List<Area>(),
										Connection = Connection,
										Transaction =  Transaction,
									};
					var regionB = new Region
									{
										TenantId = tenantId,
										Name = "EndlessRecursiveSubRegionNestingTests Region B",
										Areas = new List<Area>(),
										Connection = Connection,
										Transaction = Transaction,
									};

					// save regions
					regionA.Save();
					regionB.Save();

					// add area to A with sub region to B
					var areaA = new Area
									{
										UseSubRegion = true,
										SubRegion = regionB,
										Region = regionA,
										TenantId = tenantId,
										PostalCode = string.Empty,
										Connection = Connection,
										Transaction = Transaction,
									};
					areaA.Save();
					regionA.Areas.Add(areaA);

					// try for endless recursive nesting
					var areaB = new Area
									{
										UseSubRegion = true,
										SubRegion = regionA,
										Region = regionB,
										TenantId = tenantId,
										PostalCode = string.Empty,
										Connection = Connection,
										Transaction = Transaction,
									};
					regionB.Areas.Add(areaB);

					Assert.IsFalse(validator.IsValid(regionB));
					validator.Messages.PrintMessages();

					areaA.Delete();
					regionA.Delete();
					regionB.Delete();

					CommitTransaction();
				}
				catch (Exception e)
				{
					RollBackTransaction();
					Console.WriteLine(e.ToString());
				}
			}
		}
	}
}
