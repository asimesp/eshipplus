﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Project44;
using Newtonsoft.Json;
using NUnit.Framework;
using P44SDK.V4.Model;

namespace LogisticsPlus.Eship.Tests.Rates
{
	[TestFixture]
	[Ignore("Run on demand")]
    public class Project44Tests
	{
	    private Project44Wrapper p44Wrapper { get; set; }


	    [SetUp]
	    public void Initialize()
	    {
	        if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();


	        var p44Settings = new Project44ServiceSettings
	        {
                //Hostname = "https://test.p-44.com",
                //Username = "logisticsplus_testing@p-44.com",
                //   Password = "project44!",
                //   PrimaryLocationId = "a62b8e6f-100a-495a-95f9-7ee1f39573ed"
	            Hostname = "https://cloud.p-44.com",
	            Username = "logisticsplus.prod@p-44.com",
	            Password = "Logisticsplus2018!",
	            PrimaryLocationId = "a62b8e6f-100a-495a-95f9-7ee1f39573ed"
            };

            p44Wrapper = new Project44Wrapper(p44Settings);
        }


	    [Test]
        public void CanGetCapacityProviderAccountGroups()
	    {
	        var groups = p44Wrapper.GetCapacityProviderGroups();

            Console.WriteLine(JsonConvert.SerializeObject(groups));
	    }


	    [Test]
        public void CanRateForLane()
	    {
	        var p44Request = new LaneRateRequest
	        {
	            OriginPostalCode = "16335",
	            OriginCountry = Address.CountryEnum.US,
	            DestinationPostalCode = "48309",
	            DestinationCountry = Address.CountryEnum.US,
	            FreightClass = 85,
	            Height = 48,
	            Length = 40,
	            Weight = 1000,
	            Width = 48,
	            Scac = "DAFG"
	        };

	        var laneRateResponse = p44Wrapper.GetRates(p44Request);
            Console.WriteLine(JsonConvert.SerializeObject(laneRateResponse));
	    }

	    [Test]
	    public void CanRateShipment()
	    {
	        var shipment = new Shipment
	        {
	            DesiredPickupDate = DateTime.Today.AddDays(7),
	            EarlyPickup = "08:00",
	            LatePickup = "17:00",
	            Origin = new ShipmentLocation
	            {
	                Street1 = "abc",
	                Street2 = "def",
	                City = "",
	                State = "",
	                PostalCode = "17748",
	                CountryId = GlobalTestInitializer.DefaultCountryId
	            },
	            Destination = new ShipmentLocation
	            {
	                Street1 = "abc",
	                Street2 = "def",
	                City = "",
	                State = "",
	                PostalCode = "28147",
	                CountryId = GlobalTestInitializer.DefaultCountryId
	            },
	            Items = new List<ShipmentItem>
	            {
	                new ShipmentItem
	                {
	                    ActualFreightClass = 85,
	                    ActualLength = 48,
	                    ActualWidth = 40,
	                    ActualHeight = 40,
	                    ActualWeight = 1000,
	                    PackageTypeId = GlobalTestInitializer.DefaultPackageTypeId,
	                    Quantity = 1,
	                    PieceCount = 1,
	                    Description = "some item"
	                }
	            }
	        };

	        var scacs = new List<string> { "RDWY"/*, "WARD", "ABFS", "PYLE"*/ };

	        var rates = p44Wrapper.GetRates(shipment, scacs, "Default");
	        Console.WriteLine(JsonConvert.SerializeObject(rates));
	    }

	    [Test]
	    public void CanDispatchShipmentAndCancelDispatch()
	    {
	        var shipment = new Shipment
	        {
                ShipmentNumber = "TEST123",
	            DesiredPickupDate = DateTime.Today.AddDays(7),
	            EarlyPickup = "08:00",
	            LatePickup = "17:00",
                Vendors = new List<ShipmentVendor>
                {
                    new ShipmentVendor()
                    {
                        Primary = true,
                        VendorId = new VendorSearch().FetchVendorByNumber("445", 4).Id,
                        ProNumber = "TEST123"
                    }
                },
	            Origin = new ShipmentLocation
	            {
	                Street1 = "abc",
	                Street2 = "def",
	                City = "ERIE",
	                State = "PA",
	                PostalCode = "16508",
	                CountryId = GlobalTestInitializer.DefaultCountryId,
                    Description = "test",
                    SpecialInstructions = "test",
                    Contacts = new List<ShipmentContact>
                    {
                        new ShipmentContact
                        {
                            Primary = true,
                            Comment = "test",
                            Name = "test",
                            Email = "test@test.com",
                            Phone = "1234567890",
                            Mobile = "1234567890",
                            Fax = "1234567890"
                        }
                    }
	            },
	            Destination = new ShipmentLocation
	            {
	                Street1 = "abc",
	                Street2 = "def",
	                City = "Schenectady",
	                State = "NY",
	                PostalCode = "12345",
	                CountryId = GlobalTestInitializer.DefaultCountryId,
	                Description = "test",
	                SpecialInstructions = "test",
                    Contacts = new List<ShipmentContact>
	                {
	                    new ShipmentContact
	                    {
	                        Primary = true,
                            Comment = "test",
	                        Name = "test",
	                        Email = "test@test.com",
	                        Phone = "1234567890",
	                        Mobile = "1234567890",
	                        Fax = "1234567890"
	                    }
	                }
                },
	            Items = new List<ShipmentItem>
	            {
	                new ShipmentItem
	                {
	                    RatedFreightClass = 77.5,
	                    ActualLength = 200,
	                    ActualWidth = 45,
	                    ActualHeight = 45,
	                    ActualWeight = 1500,
	                    PackageTypeId = GlobalTestInitializer.DefaultPackageTypeId,
	                    Quantity = 1,
	                    PieceCount = 1,
	                    Description = "some item"
	                }
	            }
	        };

	        var dispatchResponse = p44Wrapper.DispatchShipment(shipment, "EXLA");

            Console.WriteLine("Dispatch Response:");
	        Console.WriteLine(JsonConvert.SerializeObject(dispatchResponse));
	        Console.WriteLine(string.Empty);
	        Console.WriteLine(string.Empty);
	        Console.WriteLine(string.Empty);


	        var cancelDispatchResponse = p44Wrapper.CancelDispatchedShipment(shipment, dispatchResponse.ShipmentIdentifiers.First(i => i.Type == LtlShipmentIdentifier.TypeEnum.PICKUP).Value);

            Console.WriteLine("Cancel Dispatch Response:");
	        Console.WriteLine(JsonConvert.SerializeObject(cancelDispatchResponse));
	        Console.WriteLine(string.Empty);
	        Console.WriteLine(string.Empty);
	        Console.WriteLine(string.Empty);


        }


        [Test]
	    public void RateShipmentForOverlengthCharges()
	    {
	        var shipment = new Shipment
	        {
	            DesiredPickupDate = DateTime.Today.AddDays(7),
	            EarlyPickup = "08:00",
	            LatePickup = "17:00",
	            Origin = new ShipmentLocation
	            {
	                Street1 = "abc",
	                Street2 = "def",
	                City = "ERIE",
	                State = "PA",
	                PostalCode = "16508",
	                CountryId = GlobalTestInitializer.DefaultCountryId
	            },
	            Destination = new ShipmentLocation
	            {
	                Street1 = "abc",
	                Street2 = "def",
	                City = "Schenectady",
	                State = "NY",
	                PostalCode = "12345",
	                CountryId = GlobalTestInitializer.DefaultCountryId
	            },
	            Items = new List<ShipmentItem>
	            {
	                new ShipmentItem
	                {
	                    RatedFreightClass = 77.5,
	                    ActualLength = 200,
	                    ActualWidth = 45,
	                    ActualHeight = 45,
	                    ActualWeight = 1500,
	                    PackageTypeId = GlobalTestInitializer.DefaultPackageTypeId,
	                    Quantity = 1,
	                    PieceCount = 1,
	                    Description = "some item"
	                }
	            }
	        };

	        var scacs = new List<string> {"EXLA", "WARD", "ABFS", "PYLE"};

	        var rates = p44Wrapper.GetRates(shipment, scacs, "Default");
	        Console.WriteLine(JsonConvert.SerializeObject(rates));
	    }

	    [Test]
	    public void RateShipmentForCfcDensityCharges()
	    {
	        var shipment = new Shipment
	        {
	            DesiredPickupDate = DateTime.Today.AddDays(7),
	            EarlyPickup = "08:00",
	            LatePickup = "17:00",
	            Origin = new ShipmentLocation
	            {
	                Street1 = "abc",
	                Street2 = "def",
	                City = "ERIE",
	                State = "PA",
	                PostalCode = "16508",
	                CountryId = GlobalTestInitializer.DefaultCountryId
	            },
	            Destination = new ShipmentLocation
	            {
	                Street1 = "abc",
	                Street2 = "def",
	                City = "Schenectady",
	                State = "NY",
	                PostalCode = "12345",
	                CountryId = GlobalTestInitializer.DefaultCountryId
	            },
	            Items = new List<ShipmentItem>
	            {
	                new ShipmentItem
	                {
	                    RatedFreightClass = 77.5,
	                    ActualLength = 100,
	                    ActualWidth = 80,
	                    ActualHeight = 96,
	                    ActualWeight = 5000,
	                    PackageTypeId = GlobalTestInitializer.DefaultPackageTypeId,
	                    Quantity = 2,
	                    PieceCount = 1,
	                    Description = "some item"
	                }
	            }
	        };

	        var scacs = new List<string> { "EXLA", "WARD", "ABFS", "PYLE" };

	        var rates = p44Wrapper.GetRates(shipment, scacs, "Default");
	        Console.WriteLine(JsonConvert.SerializeObject(rates));
        }

	    [Test]
        [Ignore("Configure carrier SCACs and destination zip")]
	    public void RateShipmentForHighCostDeliveryRegionCharges()
	    {
	        var shipment = new Shipment
	        {
	            DesiredPickupDate = DateTime.Today.AddDays(7),
	            EarlyPickup = "08:00",
	            LatePickup = "17:00",
	            Origin = new ShipmentLocation
	            {
	                Street1 = "abc",
	                Street2 = "def",
	                City = "ERIE",
	                State = "PA",
	                PostalCode = "16508",
	                CountryId = GlobalTestInitializer.DefaultCountryId
	            },
	            Destination = new ShipmentLocation
	            {
	                Street1 = "abc",
	                Street2 = "def",
	                City = "Schenectady",
	                State = "NY",
	                PostalCode = "12345",
	                CountryId = GlobalTestInitializer.DefaultCountryId
	            },
	            Items = new List<ShipmentItem>
	            {
	                new ShipmentItem
	                {
	                    RatedFreightClass = 77.5,
	                    ActualLength = 48,
	                    ActualWidth = 40,
	                    ActualHeight = 48,
	                    ActualWeight = 1500,
	                    PackageTypeId = GlobalTestInitializer.DefaultPackageTypeId,
	                    Quantity = 1,
	                    PieceCount = 1,
	                    Description = "some item"
	                }
	            }
	        };

	        var scacs = new List<string> { "EXLA", "WARD", "ABFS", "PYLE" };

	        var rates = p44Wrapper.GetRates(shipment, scacs, "Default");
	        Console.WriteLine(JsonConvert.SerializeObject(rates));
        }

	    [Test]
	    public void RateShipmentForLinearFootRules()
	    {
	        var shipment = new Shipment
	        {
	            DesiredPickupDate = DateTime.Today.AddDays(7),
	            EarlyPickup = "08:00",
	            LatePickup = "17:00",
	            Origin = new ShipmentLocation
	            {
	                Street1 = "abc",
	                Street2 = "def",
	                City = "ERIE",
	                State = "PA",
	                PostalCode = "16508",
	                CountryId = GlobalTestInitializer.DefaultCountryId
	            },
	            Destination = new ShipmentLocation
	            {
	                Street1 = "abc",
	                Street2 = "def",
	                City = "Schenectady",
	                State = "NY",
	                PostalCode = "12345",
	                CountryId = GlobalTestInitializer.DefaultCountryId
	            },
	            Items = new List<ShipmentItem>
	            {
	                new ShipmentItem
	                {
	                    RatedFreightClass = 77.5,
	                    ActualLength = 999,
	                    ActualWidth = 999,
	                    ActualHeight = 999,
	                    ActualWeight = 1,
	                    PackageTypeId = GlobalTestInitializer.DefaultPackageTypeId,
	                    Quantity = 1,
	                    PieceCount = 1,
	                    Description = "some item"
	                }
	            }
	        };

	        var scacs = new List<string> { "EXLA", "WARD", "ABFS", "PYLE" };

	        var rates = p44Wrapper.GetRates(shipment, scacs, "Default");
	        Console.WriteLine(JsonConvert.SerializeObject(rates));
        }
    }
}
