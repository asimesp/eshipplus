﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Rates;
using LogisticsPlus.UpsPlugin;
using LogisticssPlus.FedExPlugin.ShipWebService;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rates
{
	[TestFixture]
	public class VendorRateAnalysisTests
	{
		private VendorRateAnalysisLTL _rateAnalysisLTL;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_rateAnalysisLTL = new VendorRateAnalysisLTL(GlobalTestInitializer.RatewareSettings, GlobalTestInitializer.CarrierConnectSettings2);
		}

		[Test]
		public void CanGetLTLRates()
		{
			var vendorRating = new VendorRating(GlobalTestInitializer.DefaultVendorRatingId);
			Assert.IsTrue(vendorRating.KeyLoaded);

			var lanes = new List<Lane>
                            {
                                new Lane
                                    {
										LineId = "ID",
                                        OriginPostalCode = "16501",
                                        OriginCountryId = GlobalTestInitializer.DefaultCountryId,
                                        DestinationPostalCode = "16502",
                                        DestinationCountryId = GlobalTestInitializer.DefaultCountryId,
                                        ActualFreightClass = 55,
                                        ActualWeight = 150,
                                        Length = 50,
                                        Width = 12,
                                        Height = 55,
                                    }
                            };

			var watch = new Stopwatch();
			watch.Start();
			Console.WriteLine("Start Time: {0}", DateTime.Now);

			Assert.IsTrue(_rateAnalysisLTL.RunSmc3Analysis(vendorRating, DateTime.Now, lanes, false));

			watch.Stop();
			Console.WriteLine("End Time: {0} Elasped Time: {1}s", DateTime.Now, watch.Elapsed.Seconds);
			Console.WriteLine();
			Console.WriteLine("Rates:");
			Console.WriteLine();


			foreach (var lane in lanes)
			{
				Console.WriteLine("Billed Weight: {0}", lane.BilledWeight.ToString());
				Console.WriteLine("OriginalValue: {0}", lane.OriginalValue);
				Console.WriteLine("Floor: {0}", lane.FreightCostFloor);
				Console.WriteLine("Ceiling: {0}", lane.FreightCostCeiling);
				Console.WriteLine("Discount Percent: {0}", lane.DiscountPercent);
				Console.WriteLine("Fuel Percent: {0}", lane.FuelPercent);
				Console.WriteLine("Freight Cost: {0}", lane.FreightCost);
				Console.WriteLine("Fuel Cost: {0}", lane.FuelCost);
				Console.WriteLine("Current Transit Days: {0}", lane.CurrentTransitDays);
				Console.WriteLine("Comment: {0}", lane.Comment);
			}
		}

		[Test]
		public void CanGetFedexSmallPackRates()
		{
            var pmaps = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].SmallPackagingMaps;
			if (pmaps.Count == 0) return;
			var p = pmaps.FirstOrDefault(pm => pm.PackageType.TypeName.ToLower().Contains("your")) ?? pmaps[0];

			var lane = new Lane
					{
						LineId = "ID",
						OriginPostalCode = "16501",
						OriginCountryId = GlobalTestInitializer.DefaultCountryId,
						DestinationPostalCode = "16502",
						DestinationCountryId = GlobalTestInitializer.DefaultCountryId,
						ActualFreightClass = 10,
						ActualWeight = 10,
						Length = 10,
						Width = 10,
						Height = 10,
					};



			var rateAnalyzer = new VendorRateAnalysisSmallPack(GlobalTestInitializer.FedExServiceParams, GlobalTestInitializer.UpsServiceParams, pmaps);
			var watch = new Stopwatch();
			watch.Start();
			Console.WriteLine("Start Time: {0}", DateTime.Now);


			var type = ServiceType.FEDEX_GROUND.ToString();
			rateAnalyzer.RunAnalysis(SmallPackageEngine.FedEx, type, p.PackageType, new[] { lane }.ToList(), DateTime.Now);

			watch.Stop();
			Console.WriteLine("End Time: {0} Elasped Time: {1}s", DateTime.Now, watch.Elapsed.Seconds);
			Console.WriteLine();
			Console.WriteLine("Rates:");
			Console.WriteLine();

			Console.WriteLine("Billed Weight: {0}", lane.BilledWeight);
			Console.WriteLine("OriginalValue: {0}", lane.OriginalValue);
			Console.WriteLine("Discount Percent: {0}", lane.DiscountPercent);
			Console.WriteLine("Fuel Percent: {0}", lane.FuelPercent);
			Console.WriteLine("Current Transit Days: {0}", lane.CurrentTransitDays);
			Console.WriteLine("Message/Comment: {0}", lane.Comment);

		}

		[Test]
		public void CanGetUpsSmallPackRates()
		{
            var pmaps = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].SmallPackagingMaps;
			if (pmaps.Count == 0) return;

			var p = pmaps.FirstOrDefault(pm => pm.PackageType.TypeName.ToLower().Contains("your")) ?? pmaps[0];

			var lane = new Lane
						{
							LineId = "ID",
							OriginPostalCode = "16502",
							OriginCountryId = GlobalTestInitializer.DefaultCountryId,
							DestinationPostalCode = "44106",
							DestinationCountryId = GlobalTestInitializer.DefaultCountryId,
							ActualFreightClass = 10,
							ActualWeight = 10,
							Length = 10,
							Width = 10,
							Height = 10,
						};

			var rateAnalyzer = new VendorRateAnalysisSmallPack(GlobalTestInitializer.FedExServiceParams, GlobalTestInitializer.UpsServiceParams, pmaps);
			var watch = new Stopwatch();
			watch.Start();
			Console.WriteLine("Start Time: {0}", DateTime.Now);


			var type = UpsServiceType.Ground.ToString();
			rateAnalyzer.RunAnalysis(SmallPackageEngine.FedEx, type, p.PackageType, new[] { lane }.ToList(), DateTime.Now);

			watch.Stop();
			Console.WriteLine("End Time: {0} Elasped Time: {1}s", DateTime.Now, watch.Elapsed.Seconds);
			Console.WriteLine();
			Console.WriteLine("Rates:");
			Console.WriteLine();

			Console.WriteLine("Billed Weight: {0}", lane.BilledWeight);
			Console.WriteLine("OriginalValue: {0}", lane.OriginalValue);
			Console.WriteLine("Discount Percent: {0}", lane.DiscountPercent);
			Console.WriteLine("Fuel Percent: {0}", lane.FuelPercent);
			Console.WriteLine("Current Transit Days: {0}", lane.CurrentTransitDays);
			Console.WriteLine("Message/Comment: {0}", lane.Comment);

		}


	}
}
