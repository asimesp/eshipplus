﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Smc;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rates
{
	[TestFixture]
	public class CarrierConnectTests
	{
		private CarrierConnect _carrierConnect;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();

			var csearch = new CountrySearch();
			var countryMap = new List<Country>
				{
					new Country(csearch.FetchCountryIdByCode(IataCodes.Country.Canada)),
					new Country(csearch.FetchCountryIdByCode(IataCodes.Country.Mexico)),
					new Country(csearch.FetchCountryIdByCode(IataCodes.Country.Usa)),
				};
			_carrierConnect = new CarrierConnect(GlobalTestInitializer.CarrierConnectSettings2, countryMap);
		}

		[Test]
		public void CanGetAvailableCarriers()
		{
			var carriers = _carrierConnect.GetAvailableCarriers(DateTime.Now);
			Assert.IsTrue(carriers.Any());
			Console.WriteLine("Carrier Count: {0}", carriers.Count);
			Console.WriteLine();
			foreach (var carrier in carriers)
			{
				Console.WriteLine("\tScac:{0} \tName:{1}", carrier.Scac, carrier.Name);
				foreach (var service in carrier.Services)
					Console.WriteLine("\t\tCode:{0} \tMethod:{1}", service.ServiceCode, service.ServiceMethod);
			}
		}

		[Test]
		public void CanGetCarrierTerminals()
		{
			var terminals = _carrierConnect.GetCarrierTerminals("EXLA", DateTime.Now);
			Assert.IsTrue(terminals.Any());
			Console.WriteLine("Carrier Count: {0}", terminals.Count);
			Console.WriteLine();
		}

		[Test]
		public void CanGetCarrierTerminalTransits()
		{
			var terminals = _carrierConnect.GetCarrierTerminals("EXLA", DateTime.Now);
			if (!terminals.Any()) return;
			var @from = terminals.First();
			var transits = _carrierConnect.GetTerminalTransits(@from, terminals);

			Console.WriteLine("Carrier Count: {0}", terminals.Count);
			Console.WriteLine();

			foreach (var t in transits)
				Console.WriteLine("Origin: {0}-{1} Destination: {2}-{3} Minimum Days: {4} Direct: {5} Method: {6}",
				                  t.From.Code, t.From.PostalCode, t.To.Code, t.To.PostalCode, t.MinimumDays, t.Direct, t.Method);
		}

		[Test]
		public void CanGetPointToPointService()
		{
			var country = new Country(GlobalTestInitializer.DefaultCountryId);
			var services = _carrierConnect.GetPointToPointService("16501", country, "77083", country);
			if (!services.Any()) return;

			Console.WriteLine("Service Count: {0}", services.Count);
			Console.WriteLine();

			foreach (var s in services)
				Console.WriteLine(
					"Origin: {0} {1} Destination: {2} {3} Minimum Days: {4} Direct: {5} Method: {6} Scac: {7} Carrier: {8}",
					s.OriginPostalCode, s.OriginTerminalCode, s.DestinationPostalCode, s.DestinationTerminalCode, s.MinimumDays,
					s.Direct, s.Method, s.Scac, s.CarrierName);
		}

	}
}
