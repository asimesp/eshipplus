﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Calculations;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Rates;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Smc;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rates
{
	[TestFixture]
	public class RaterTests
	{
		private Rater2 _rater2;

		private CarrierConnect _carrierConnect;


		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();

			var tenantId = GlobalTestInitializer.DefaultTenantId;
            var pmaps = ProcessorVars.RegistryCache[tenantId].SmallPackagingMaps;
            var smaps = ProcessorVars.RegistryCache[tenantId].SmallPackageServiceMaps;

			_rater2 = new Rater2(GlobalTestInitializer.RatewareSettings,
							   GlobalTestInitializer.CarrierConnectSettings2,
							   GlobalTestInitializer.FedExServiceParams,
							   GlobalTestInitializer.UpsServiceParams,
							   pmaps, smaps, GlobalTestInitializer.Project44Settings);


			var csearch = new CountrySearch();
			var countryMap = new List<Country>
				{
					new Country(csearch.FetchCountryIdByCode(IataCodes.Country.Canada)),
					new Country(csearch.FetchCountryIdByCode(IataCodes.Country.Mexico)),
					new Country(csearch.FetchCountryIdByCode(IataCodes.Country.Usa)),
				};
			_carrierConnect = new CarrierConnect(GlobalTestInitializer.CarrierConnectSettings2, countryMap);
		}

		[Test]
		public void CanRateQuote2()
		{
			var shipment = new Shipment(185783);

		    shipment.Customer = new CustomerSearch().FetchCustomerByNumber("12913", 4);

		    var service = new ServiceSearch().FetchServiceIdByCode("LGD", 4);

		    //var one = shipment.Customer.Rating.LTLSellRates[0];
		    //one.VendorRating.Vendor.Scac = "RIST";
		    //one.VendorRating.Project44Profile = true;
		    //shipment.Customer.Rating.LTLSellRates = new List<LTLSellRate> {one};

			shipment.Origin.PostalCode = "16501";
		    shipment.Origin.State = "PA";
		    shipment.Origin.City = "Erie";
			shipment.Destination.PostalCode = "10001";
		    shipment.Destination.State = "NY";
		    shipment.Destination.City = "New York";
			//shipment.Destination.Country = new Country(new CountrySearch().FetchCountryIdByCode(IataCodes.Country.Canada));
			shipment.Items[0].ActualFreightClass = 50;
		    shipment.Items[0].ActualWeight = 200;
			shipment.Services = new List<ShipmentService>(); // no services
			shipment.DesiredPickupDate = DateTime.Now.AddDays(1);

            shipment.Services = new List<ShipmentService>
            {
                new ShipmentService{ Shipment =  shipment, TenantId = shipment.TenantId, ServiceId = service}
            };

			var watch = new Stopwatch();
			watch.Start();
			Console.WriteLine("Start Time: {0}", DateTime.Now);

			
			var rates = _rater2.GetRates(shipment);

			watch.Stop();
			Console.WriteLine("End Time: {0} Elasped Time: {1}s", DateTime.Now, watch.Elapsed.Seconds);
			Console.WriteLine();
			Console.WriteLine("Rates:");
			Console.WriteLine();

			foreach (var rate in rates)
			{
				Console.WriteLine("Mode: {0}\tEst. Del: {1}", rate.Mode.FormattedString(), rate.EstimatedDeliveryDate);
				var vendor = rate.Vendor ?? (rate.LTLSellRate != null ? rate.LTLSellRate.VendorRating.Vendor : null);
				Console.WriteLine("Vendor: {0}", vendor == null ? string.Empty : vendor.VendorNumber + " - " + vendor.Name + " - " + vendor.Scac);
				Console.WriteLine("LTL Sell Rate: {0}", rate.LTLSellRate == null ? string.Empty : rate.LTLSellRate.VendorRating.Name);
				Console.WriteLine("LTL Sell Rate Markup %: {0}", rate.LTLSellRate == null ? string.Empty : rate.LTLSellRate.MarkupPercent.ToString("f4"));
				Console.WriteLine("LTL Sell Rate Markup Val.: {0}", rate.LTLSellRate == null ? string.Empty : rate.LTLSellRate.MarkupValue.ToString("f4"));
				Console.WriteLine("Discount Tier: {0}", rate.DiscountTier == null ? string.Empty : rate.DiscountTier.OriginRegion.Name + " - " +
					rate.DiscountTier.DestinationRegion.Name);
				Console.WriteLine("Discount Tier Discount: {0}", rate.DiscountTier == null ? string.Empty : rate.DiscountTier.DiscountPercent.ToString("f4"));
				Console.WriteLine("Small Pack Type: {0}", rate.SmallPackServiceType);
				Console.WriteLine("Small Pack Engine: {0}", rate.SmallPackEngine);
				Console.WriteLine("Original Rate Value: {0}", rate.OriginalRateValue.ToString("f4"));
				Console.WriteLine("Fuel Markup %: {0}", rate.FuelMarkupPercent.ToString("f4"));
				Console.WriteLine("Direct Point Rate: {0}", rate.DirectPointRate);
				Console.WriteLine("Apply Discount: {0}", rate.ApplyDiscount);
				Console.WriteLine("Transit Days: {0}", rate.TransitDays);
				Console.WriteLine("Bill Weight: {0}", rate.BilledWeight.ToString("f4"));
				Console.WriteLine("Freight Profit Adj.: {0}", rate.FreightProfitAdjustment.ToString("f4"));
				Console.WriteLine("Fuel Profit Adj.: {0}", rate.FuelProfitAdjustment.ToString("f4"));
				Console.WriteLine("Accessorial Profit Adj.: {0}", rate.AccessorialProfitAdjustment.ToString("f4"));
				Console.WriteLine("Service Profit Adj.: {0}", rate.ServiceProfitAdjustment.ToString("f4"));
				Console.WriteLine("No Freight Profit: {0}", rate.NoFreightProfit);
				Console.WriteLine("No Fuel Profit: {0}", rate.NoFuelProfit);
				Console.WriteLine("No Accessorial Profit: {0}", rate.NoAccessorialProfit);
				Console.WriteLine("No Service Profit: {0}", rate.NoServiceProfit);
				Console.WriteLine("Reseller Freight Markup: {0}", rate.ResellerFreightMarkup.ToString("f4"));
				Console.WriteLine("Reseller Freight Markup Is %: {0}", rate.ResellerFreightMarkupIsPercent);
				Console.WriteLine("Reseller Fuel Markup: {0}", rate.ResellerFuelMarkup.ToString("f4"));
				Console.WriteLine("Reseller Fuel Markup Is %: {0}", rate.ResellerFuelMarkupIsPercent);
				Console.WriteLine("Reseller Accessorial Markup: {0}", rate.ResellerAccessorialMarkup.ToString("f4"));
				Console.WriteLine("Reseller Accessorial Markup Is %: {0}", rate.ResellerAccessorialMarkupIsPercent);
				Console.WriteLine("Reseller Service Markup: {0}", rate.ResellerServiceMarkup.ToString("f4"));
				Console.WriteLine("Reseller Service Markup Is %: {0}", rate.ResellerServiceMarkupIsPercent);
				Console.WriteLine();
				Console.WriteLine("Charges:");
				Console.WriteLine();
				Console.WriteLine("Qty\t\tUnit Buy\tUnit Sell\tUnit Disc.\tCharge Code\tCharge Desc.");
				foreach (var c in rate.Charges)
				{
					Console.WriteLine("{0}\t\t{1}\t{2}\t{3}\t{4}\t{5}",
						c.Quantity, c.UnitBuy.ToString("f4"), c.UnitSell.ToString("f4"), c.UnitDiscount.ToString("f4"), c.ChargeCode.Code, c.ChargeCode.Description);
				}
			}
		}

        [Test]
        public void CanRateQuoteWithWeightBreakOverrides()
        {
            // This shipment used customer # 10000
            var shipment = new Shipment(GlobalTestInitializer.DefaultShipmentIdForWeightBreakTest)
                {
                    Origin = {PostalCode = "44622"}, 
                    Destination = {PostalCode = "40511"}
                };

            shipment.Items[0].ActualFreightClass = 77.5;
            shipment.Services = new List<ShipmentService>(); // no services
            shipment.DesiredPickupDate = DateTime.Now;

            var rates = _rater2.GetRates(shipment);

            foreach (var rate in rates)
            {
                var vendor = rate.Vendor ?? (rate.LTLSellRate != null ? rate.LTLSellRate.VendorRating.Vendor : null);

                // Let us only examine "ESTES EXPRESS LINES" for this Customer Rating.
                if (rate.LTLSellRate == null || rate.LTLSellRate.VendorRatingId != GlobalTestInitializer.DefaultVendorRatingIdForWeightBreakTest) continue;

                Console.WriteLine("Vendor: {0}", vendor == null ? string.Empty : vendor.VendorNumber + " - " + vendor.Name + " - " + vendor.Scac);
                Console.WriteLine("LTL Sell Rate: {0}", rate.LTLSellRate == null ? string.Empty : rate.LTLSellRate.VendorRating.Name);
                Console.WriteLine("LTL Default Sell Rate Markup %: {0}", rate.LTLSellRate.MarkupPercent.ToString("f4"));
                Console.WriteLine("LTL Default Sell Rate Markup Val.: {0}", rate.LTLSellRate.MarkupValue.ToString("f4"));

                Console.WriteLine("LTL Final Sell Rate Markup %: {0}", rate.MarkupPercent.ToString("f4"));
                Console.WriteLine("LTL Final Sell Rate Markup Val.: {0}", rate.MarkupValue.ToString("f4"));

                Console.WriteLine("Rate Used WeightBreak Override Markup: {0}",
                                  (rate.MarkupValue != rate.LTLSellRate.MarkupValue
                                   && rate.MarkupPercent != rate.LTLSellRate.MarkupPercent).GetString());
                Console.WriteLine("Total actual shipment weight: {0}", rate.TotalActualShipmentWeight.ToString("f4"));
                Console.WriteLine("Weight Break Category Used (if any): {0}", rate.TotalActualShipmentWeight.GetBilledWeightBreak().GetString());

                Assert.IsTrue(rate.MarkupValue != rate.LTLSellRate.MarkupValue
                                   && rate.MarkupPercent != rate.LTLSellRate.MarkupPercent);
            }
        }

        [Test]
        public void CanRateQuoteWithVendorFloorOverrides()
        {
            // This shipment used customer # 10000
            var shipment = new Shipment(GlobalTestInitializer.DefaultShipmentIdForVendorFloorTest)
            {
                Origin = { PostalCode = "44001" },
                Destination = { PostalCode = "16335" }
            };

            shipment.Items[0].ActualFreightClass = 77.5;
            shipment.Services = new List<ShipmentService>(); // no services
            shipment.DesiredPickupDate = DateTime.Now;

            var rates = _rater2.GetRates(shipment);

            foreach (var rate in rates)
            {
                var vendor = rate.Vendor ?? (rate.LTLSellRate != null ? rate.LTLSellRate.VendorRating.Vendor : null);

                // Let us only examine "A Duie Pyle Inc" for this Customer Rating.
                if (rate.LTLSellRate == null || rate.LTLSellRate.VendorRatingId != GlobalTestInitializer.DefaultVendorRatingIdForVendorFloorTest) continue;

                Console.WriteLine("Vendor: {0}", vendor == null ? string.Empty : vendor.VendorNumber + " - " + vendor.Name + " - " + vendor.Scac);
                Console.WriteLine("LTL Sell Rate: {0}", rate.LTLSellRate == null ? string.Empty : rate.LTLSellRate.VendorRating.Name);
                Console.WriteLine("LTL Default Sell Rate Markup %: {0}", rate.LTLSellRate.MarkupPercent.ToString("f4"));
                Console.WriteLine("LTL Default Sell Rate Markup Val.: {0}", rate.LTLSellRate.MarkupValue.ToString("f4"));
                Console.WriteLine("LTL Vendor Floor Rate Markup %: {0}", rate.LTLSellRate.OverrideMarkupPercentVendorFloor.ToString("f4"));
                Console.WriteLine("LTL Vendor Floor Rate Markup Val.: {0}", rate.LTLSellRate.OverrideMarkupValueVendorFloor.ToString("f4"));
                Console.WriteLine("LTL Vendor Floor Override Enabled: {0}", rate.LTLSellRate.OverrideMarkupVendorFloorEnabled.GetString());
                Console.WriteLine("LTL Final Sell Rate Markup %: {0}", rate.MarkupPercent.ToString("f4"));
                Console.WriteLine("LTL Final Sell Rate Markup Val.: {0}", rate.MarkupValue.ToString("f4"));

                Console.WriteLine("Rate Used Vendor Floor Override Markup: {0}",
                                  (rate.MarkupValue == rate.LTLSellRate.OverrideMarkupValueVendorFloor
                                   && rate.MarkupPercent == rate.LTLSellRate.OverrideMarkupPercentVendorFloor).GetString());

                Assert.IsTrue(rate.MarkupValue == rate.LTLSellRate.OverrideMarkupValueVendorFloor
                                   && rate.MarkupPercent == rate.LTLSellRate.OverrideMarkupPercentVendorFloor);
            }
        }


		[Test]
		public void CanRateQuotePackageSpecificRates()
		{
			var shipment = new Shipment(75795);

			var watch = new Stopwatch();
			watch.Start();
			Console.WriteLine("Start Time: {0}", DateTime.Now);

			shipment.DesiredPickupDate = DateTime.Now;
			shipment.Origin.PostalCode = "53110";
			shipment.Destination.PostalCode = "N3S7P5";
			shipment.Destination.Country = new Country(new CountrySearch().FetchCountryIdByCode(IataCodes.Country.Canada));
			shipment.Items[0].ActualFreightClass = 77.5;
			shipment.Items[0].Quantity = 10;
			shipment.Items[0].ActualLength = 48;
			shipment.Items[0].ActualWidth = 48;
			shipment.Items[0].ActualHeight = 48;
			shipment.Items[0].ActualWeight = 500;
			shipment.Items[0].IsStackable = true;

			shipment.Customer = new CustomerSearch().FetchCustomerByNumber("11328", GlobalTestInitializer.DefaultTenantId);
			shipment.Customer.Rating.LTLSellRates[0].VendorRating.Vendor.Scac = "EXLA";

			//shipment.Services = new List<ShipmentService>();
			
			var rates = _rater2.GetRates(shipment);

			watch.Stop();
			Console.WriteLine("End Time: {0} Elasped Time: {1}s", DateTime.Now, watch.Elapsed.Seconds);
			Console.WriteLine();
			Console.WriteLine("Rates:");
			Console.WriteLine();

			foreach (var rate in rates)
			{
				Console.WriteLine("Mode: {0}\tEst. Del: {1}", rate.Mode.FormattedString(), rate.EstimatedDeliveryDate);
				Console.WriteLine("Package Specific: {0}", rate.IsLTLPackageSpecificRate);
				var vendor = rate.Vendor ?? (rate.LTLSellRate != null ? rate.LTLSellRate.VendorRating.Vendor : null);
				Console.WriteLine("Vendor: {0}", vendor == null ? string.Empty : vendor.VendorNumber + " - " + vendor.Name);
				Console.WriteLine("LTL Sell Rate: {0}", rate.LTLSellRate == null ? string.Empty : rate.LTLSellRate.VendorRating.Name);
				Console.WriteLine("LTL Sell Rate Markup %: {0}", rate.LTLSellRate == null ? string.Empty : rate.LTLSellRate.MarkupPercent.ToString("f4"));
				Console.WriteLine("LTL Sell Rate Markup Val.: {0}", rate.LTLSellRate == null ? string.Empty : rate.LTLSellRate.MarkupValue.ToString("f4"));
				Console.WriteLine("Discount Tier: {0}", rate.DiscountTier == null ? string.Empty : rate.DiscountTier.OriginRegion.Name + " - " +
					rate.DiscountTier.DestinationRegion.Name);
				Console.WriteLine("Discount Tier Discount: {0}", rate.DiscountTier == null ? string.Empty : rate.DiscountTier.DiscountPercent.ToString("f4"));
				Console.WriteLine("Small Pack Type: {0}", rate.SmallPackServiceType);
				Console.WriteLine("Small Pack Engine: {0}", rate.SmallPackEngine);
				Console.WriteLine("Original Rate Value: {0}", rate.OriginalRateValue.ToString("f4"));
				Console.WriteLine("Fuel Markup %: {0}", rate.FuelMarkupPercent.ToString("f4"));
				Console.WriteLine("Direct Point Rate: {0}", rate.DirectPointRate);
				Console.WriteLine("Apply Discount: {0}", rate.ApplyDiscount);
				Console.WriteLine("Transit Days: {0}", rate.TransitDays);
				Console.WriteLine("Bill Weight: {0}", rate.BilledWeight.ToString("f4"));
				Console.WriteLine("Freight Profit Adj.: {0}", rate.FreightProfitAdjustment.ToString("f4"));
				Console.WriteLine("Fuel Profit Adj.: {0}", rate.FuelProfitAdjustment.ToString("f4"));
				Console.WriteLine("Accessorial Profit Adj.: {0}", rate.AccessorialProfitAdjustment.ToString("f4"));
				Console.WriteLine("Service Profit Adj.: {0}", rate.ServiceProfitAdjustment.ToString("f4"));
				Console.WriteLine("No Freight Profit: {0}", rate.NoFreightProfit);
				Console.WriteLine("No Fuel Profit: {0}", rate.NoFuelProfit);
				Console.WriteLine("No Accessorial Profit: {0}", rate.NoAccessorialProfit);
				Console.WriteLine("No Service Profit: {0}", rate.NoServiceProfit);
				Console.WriteLine("Reseller Freight Markup: {0}", rate.ResellerFreightMarkup.ToString("f4"));
				Console.WriteLine("Reseller Freight Markup Is %: {0}", rate.ResellerFreightMarkupIsPercent);
				Console.WriteLine("Reseller Fuel Markup: {0}", rate.ResellerFuelMarkup.ToString("f4"));
				Console.WriteLine("Reseller Fuel Markup Is %: {0}", rate.ResellerFuelMarkupIsPercent);
				Console.WriteLine("Reseller Accessorial Markup: {0}", rate.ResellerAccessorialMarkup.ToString("f4"));
				Console.WriteLine("Reseller Accessorial Markup Is %: {0}", rate.ResellerAccessorialMarkupIsPercent);
				Console.WriteLine("Reseller Service Markup: {0}", rate.ResellerServiceMarkup.ToString("f4"));
				Console.WriteLine("Reseller Service Markup Is %: {0}", rate.ResellerServiceMarkupIsPercent);
				Console.WriteLine();
				Console.WriteLine("Charges:");
				Console.WriteLine();
				Console.WriteLine("Qty\t\tUnit Buy\tUnit Sell\tUnit Disc.\tCharge Code\tCharge Desc.");
				foreach (var c in rate.Charges)
				{
					Console.WriteLine("{0}\t\t{1}\t{2}\t{3}\t{4}\t{5}",
						c.Quantity, c.UnitBuy.ToString("f4"), c.UnitSell.ToString("f4"), c.UnitDiscount.ToString("f4"), c.ChargeCode.Code, c.ChargeCode.Description);
				}
			}
		}

		[Test]
		public void CanRateTruckload()
		{
			var shipment = new Shipment(75795);

			var watch = new Stopwatch();
			watch.Start();
			Console.WriteLine("Start Time: {0}", DateTime.Now);

			shipment.DesiredPickupDate = DateTime.Now;
			shipment.EstimatedDeliveryDate = DateTime.Now.AddDays(7);
			shipment.Origin.City = "Erie";
			shipment.Origin.State = "PA";
			shipment.Origin.PostalCode = string.Empty;
			shipment.Destination.City = "Baltimore";
			shipment.Destination.State = "MD";
			shipment.Destination.PostalCode = string.Empty;
			shipment.Customer = new Customer(GlobalTestInitializer.DefaultCustomerId)
			{
				RequiredMileageSourceId = GlobalTestInitializer.DefaultMileageSourceId
			};
			shipment.Items[0].Quantity = 1;
			shipment.Items[0].ActualWeight = 15000;
			shipment.Items[0].IsStackable = true;
			shipment.Equipments.Add(new ShipmentEquipment
				{
					EquipmentTypeId = GlobalTestInitializer.DefaultEquipmentTypeId,
					ShipmentId = shipment.Id,
					TenantId = shipment.TenantId
				});

			var rates = _rater2.GetRates(shipment);
			
			watch.Stop();
			Console.WriteLine("End Time: {0} Elasped Time: {1}s", DateTime.Now, watch.Elapsed.Seconds);
			Console.WriteLine();
			Console.WriteLine("Rates:");
			Console.WriteLine();

			foreach (var rate in rates)
			{
				Console.WriteLine("Mode: {0}\tEst. Del: {1}", rate.Mode.FormattedString(), rate.EstimatedDeliveryDate);
				Console.WriteLine("Mileage: {0}", rate.Mileage.ToString("f4"));
				Console.WriteLine("Mileage Source: {0}", new MileageSource(rate.MileageSourceId).FormattedString());
				Console.WriteLine("Bill Weight: {0}", rate.BilledWeight.ToString("f4"));
				Console.WriteLine("Freight Profit Adj.: {0}", rate.FreightProfitAdjustment.ToString("f4"));
				Console.WriteLine("Fuel Profit Adj.: {0}", rate.FuelProfitAdjustment.ToString("f4"));
				Console.WriteLine("Accessorial Profit Adj.: {0}", rate.AccessorialProfitAdjustment.ToString("f4"));
				Console.WriteLine("Service Profit Adj.: {0}", rate.ServiceProfitAdjustment.ToString("f4"));
				Console.WriteLine("No Freight Profit: {0}", rate.NoFreightProfit);
				Console.WriteLine("No Fuel Profit: {0}", rate.NoFuelProfit);
				Console.WriteLine("No Accessorial Profit: {0}", rate.NoAccessorialProfit);
				Console.WriteLine("No Service Profit: {0}", rate.NoServiceProfit);
				Console.WriteLine("Reseller Freight Markup: {0}", rate.ResellerFreightMarkup.ToString("f4"));
				Console.WriteLine("Reseller Freight Markup Is %: {0}", rate.ResellerFreightMarkupIsPercent);
				Console.WriteLine("Reseller Fuel Markup: {0}", rate.ResellerFuelMarkup.ToString("f4"));
				Console.WriteLine("Reseller Fuel Markup Is %: {0}", rate.ResellerFuelMarkupIsPercent);
				Console.WriteLine("Reseller Accessorial Markup: {0}", rate.ResellerAccessorialMarkup.ToString("f4"));
				Console.WriteLine("Reseller Accessorial Markup Is %: {0}", rate.ResellerAccessorialMarkupIsPercent);
				Console.WriteLine("Reseller Service Markup: {0}", rate.ResellerServiceMarkup.ToString("f4"));
				Console.WriteLine("Reseller Service Markup Is %: {0}", rate.ResellerServiceMarkupIsPercent);
				Console.WriteLine();
				Console.WriteLine("Charges:");
				Console.WriteLine();
				Console.WriteLine("Qty\t\tUnit Sell\tCharge Code\tCharge Desc.\tComment");
				foreach (var c in rate.Charges)
				{
					Console.WriteLine("{0}\t\t{1}\t{2}\t{3}\t{4}",
						c.Quantity, c.UnitSell.ToString("f4"), c.ChargeCode.Code, c.ChargeCode.Description, c.Comment);
				}
			}
		}

        [Test]
        public void CanRateTruckloadRoundTrip()
        {
            var shipment = new Shipment(75795);

            var watch = new Stopwatch();
            watch.Start();
            Console.WriteLine("Start Time: {0}", DateTime.Now);

	        
            shipment.DesiredPickupDate = DateTime.Now;
            shipment.EstimatedDeliveryDate = DateTime.Now.AddDays(7);
            shipment.Origin.City = "GROVE CITY";
            shipment.Origin.State = "PA";
            shipment.Origin.PostalCode = string.Empty;
            shipment.Origin.StopOrder = 0;
            shipment.Origin.CountryId = 1;
            shipment.Destination.City = "GROVE CITY";
            shipment.Destination.State = "PA";
            shipment.Destination.PostalCode = string.Empty;
            shipment.Destination.StopOrder = 2;
            shipment.Destination.CountryId = 1;
            shipment.Customer = new Customer(GlobalTestInitializer.DefaultCustomerId)
	            {
		            RequiredMileageSourceId = GlobalTestInitializer.DefaultMileageSourceId
	            };

	        shipment.Items[0].Quantity = 1;
            shipment.Items[0].ActualWeight = 15000;
            shipment.Items[0].IsStackable = true;
            shipment.Equipments.Add(new ShipmentEquipment
            {
                EquipmentTypeId = 213,
                ShipmentId = shipment.Id,
                TenantId = shipment.TenantId
            });

            shipment.Stops.Add(new ShipmentLocation
                {
                    City = "Barstow",
                    State="CA",
                    PostalCode = string.Empty,
                    StopOrder = 1,
                    CountryId=1
                });

            var rates = _rater2.GetRates(shipment);

            watch.Stop();
            Console.WriteLine("End Time: {0} Elasped Time: {1}s", DateTime.Now, watch.Elapsed.Seconds);
            Console.WriteLine();
            Console.WriteLine("Rates:");
            Console.WriteLine();

            foreach (var rate in rates)
            {
                Console.WriteLine("Mode: {0}\tEst. Del: {1}", rate.Mode.FormattedString(), rate.EstimatedDeliveryDate);
                Console.WriteLine("Mileage: {0}", rate.Mileage.ToString("f4"));
                Console.WriteLine("Mileage Source: {0}", new MileageSource(rate.MileageSourceId).FormattedString());
                Console.WriteLine("Bill Weight: {0}", rate.BilledWeight.ToString("f4"));
                Console.WriteLine("Freight Profit Adj.: {0}", rate.FreightProfitAdjustment.ToString("f4"));
                Console.WriteLine("Fuel Profit Adj.: {0}", rate.FuelProfitAdjustment.ToString("f4"));
                Console.WriteLine("Accessorial Profit Adj.: {0}", rate.AccessorialProfitAdjustment.ToString("f4"));
                Console.WriteLine("Service Profit Adj.: {0}", rate.ServiceProfitAdjustment.ToString("f4"));
                Console.WriteLine("No Freight Profit: {0}", rate.NoFreightProfit);
                Console.WriteLine("No Fuel Profit: {0}", rate.NoFuelProfit);
                Console.WriteLine("No Accessorial Profit: {0}", rate.NoAccessorialProfit);
                Console.WriteLine("No Service Profit: {0}", rate.NoServiceProfit);
                Console.WriteLine("Reseller Freight Markup: {0}", rate.ResellerFreightMarkup.ToString("f4"));
                Console.WriteLine("Reseller Freight Markup Is %: {0}", rate.ResellerFreightMarkupIsPercent);
                Console.WriteLine("Reseller Fuel Markup: {0}", rate.ResellerFuelMarkup.ToString("f4"));
                Console.WriteLine("Reseller Fuel Markup Is %: {0}", rate.ResellerFuelMarkupIsPercent);
                Console.WriteLine("Reseller Accessorial Markup: {0}", rate.ResellerAccessorialMarkup.ToString("f4"));
                Console.WriteLine("Reseller Accessorial Markup Is %: {0}", rate.ResellerAccessorialMarkupIsPercent);
                Console.WriteLine("Reseller Service Markup: {0}", rate.ResellerServiceMarkup.ToString("f4"));
                Console.WriteLine("Reseller Service Markup Is %: {0}", rate.ResellerServiceMarkupIsPercent);
                Console.WriteLine();
                Console.WriteLine("Charges:");
                Console.WriteLine();
                Console.WriteLine("Qty\t\tUnit Sell\tCharge Code\tCharge Desc.\tComment");
                foreach (var c in rate.Charges)
                {
                    Console.WriteLine("{0}\t\t{1}\t{2}\t{3}\t{4}",
                        c.Quantity, c.UnitSell.ToString("f4"), c.ChargeCode.Code, c.ChargeCode.Description, c.Comment);
                }
				Console.WriteLine();
				Console.WriteLine();

            }
        }

        [Test]
        public void CanRateTruckloadMultiStop()
        {
            var shipment = new Shipment(75795);

            var watch = new Stopwatch();
            watch.Start();
            Console.WriteLine("Start Time: {0}", DateTime.Now);

            shipment.DesiredPickupDate = DateTime.Now;
            shipment.EstimatedDeliveryDate = DateTime.Now.AddDays(7);
            shipment.Origin.City = "Erie";
            shipment.Origin.State = "PA";
            shipment.Origin.PostalCode = string.Empty;
            shipment.Origin.StopOrder = 0;
            shipment.Origin.CountryId = 1;
            shipment.Destination.City = "Erie";
            shipment.Destination.State = "PA";
            shipment.Destination.PostalCode = string.Empty;
            shipment.Destination.StopOrder = 4;
            shipment.Destination.CountryId = 1;
            shipment.Customer = new Customer(GlobalTestInitializer.DefaultCustomerId)
            {
                RequiredMileageSourceId = GlobalTestInitializer.DefaultMileageSourceId
            };
            shipment.Items[0].Quantity = 1;
            shipment.Items[0].ActualWeight = 15000;
            shipment.Items[0].IsStackable = true;
            shipment.Equipments.Add(new ShipmentEquipment
            {
                EquipmentTypeId = 202, // FLATBED
                ShipmentId = shipment.Id,
                TenantId = shipment.TenantId
            });

            shipment.Stops.Add(new ShipmentLocation
            {
                City = "",
                State = "",
                PostalCode = "76052",
                StopOrder = 1,
                CountryId = 1
            });
            shipment.Stops.Add(new ShipmentLocation
            {
                City = "DENVER",
                State = "CO",
                PostalCode = string.Empty,
                StopOrder = 2,
                CountryId = 1
            });
            shipment.Stops.Add(new ShipmentLocation
            {
                City = "Kansas City",
                State = "MO",
                PostalCode = string.Empty,
                StopOrder = 3,
                CountryId = 1
            });
            var rates = _rater2.GetRates(shipment);

            watch.Stop();
            Console.WriteLine("End Time: {0} Elasped Time: {1}s", DateTime.Now, watch.Elapsed.Seconds);
            Console.WriteLine();
            Console.WriteLine("Rates:");
            Console.WriteLine();

            foreach (var rate in rates)
            {
                Console.WriteLine("Mode: {0}\tEst. Del: {1}", rate.Mode.FormattedString(), rate.EstimatedDeliveryDate);
                Console.WriteLine("Mileage: {0}", rate.Mileage.ToString("f4"));
                Console.WriteLine("Mileage Source: {0}", new MileageSource(rate.MileageSourceId).FormattedString());
                Console.WriteLine("Bill Weight: {0}", rate.BilledWeight.ToString("f4"));
                Console.WriteLine("Freight Profit Adj.: {0}", rate.FreightProfitAdjustment.ToString("f4"));
                Console.WriteLine("Fuel Profit Adj.: {0}", rate.FuelProfitAdjustment.ToString("f4"));
                Console.WriteLine("Accessorial Profit Adj.: {0}", rate.AccessorialProfitAdjustment.ToString("f4"));
                Console.WriteLine("Service Profit Adj.: {0}", rate.ServiceProfitAdjustment.ToString("f4"));
                Console.WriteLine("No Freight Profit: {0}", rate.NoFreightProfit);
                Console.WriteLine("No Fuel Profit: {0}", rate.NoFuelProfit);
                Console.WriteLine("No Accessorial Profit: {0}", rate.NoAccessorialProfit);
                Console.WriteLine("No Service Profit: {0}", rate.NoServiceProfit);
                Console.WriteLine("Reseller Freight Markup: {0}", rate.ResellerFreightMarkup.ToString("f4"));
                Console.WriteLine("Reseller Freight Markup Is %: {0}", rate.ResellerFreightMarkupIsPercent);
                Console.WriteLine("Reseller Fuel Markup: {0}", rate.ResellerFuelMarkup.ToString("f4"));
                Console.WriteLine("Reseller Fuel Markup Is %: {0}", rate.ResellerFuelMarkupIsPercent);
                Console.WriteLine("Reseller Accessorial Markup: {0}", rate.ResellerAccessorialMarkup.ToString("f4"));
                Console.WriteLine("Reseller Accessorial Markup Is %: {0}", rate.ResellerAccessorialMarkupIsPercent);
                Console.WriteLine("Reseller Service Markup: {0}", rate.ResellerServiceMarkup.ToString("f4"));
                Console.WriteLine("Reseller Service Markup Is %: {0}", rate.ResellerServiceMarkupIsPercent);
                Console.WriteLine();
                Console.WriteLine("Charges:");
                Console.WriteLine();
                Console.WriteLine("Qty\t\tUnit Sell\tCharge Code\tCharge Desc.\tComment");
                foreach (var c in rate.Charges)
                {
                    Console.WriteLine("{0}\t\t{1}\t{2}\t{3}\t{4}",
                        c.Quantity, c.UnitSell.ToString("f4"), c.ChargeCode.Code, c.ChargeCode.Description, c.Comment);
                }
                Console.WriteLine();
                Console.WriteLine();

            }
        }

		[Test]
		public void CanRateGetTerminalInfo()
		{
			var shipment = new Shipment(75795);

			var watch = new Stopwatch();
			watch.Start();
			Console.WriteLine("Start Time: {0}", DateTime.Now);

			var rates = _rater2.GetRates(shipment);
			shipment.Items[0].ActualFreightClass = 77.5;

			watch.Stop();
			Console.WriteLine("End Time: {0} Elasped Time (Rates): {1}s", DateTime.Now, watch.Elapsed.Seconds);
			Console.WriteLine();
			Console.WriteLine("Terminal Info:");
			Console.WriteLine();

			foreach (var rate in rates)
			{
				if (rate.DiscountTier == null)
				{
					Console.WriteLine("No Discount Tier!!!");
					continue;
				}


				Console.WriteLine("Mode: {0}", rate.Mode.FormattedString());
				var vendor = rate.DiscountTier.VendorRating.Vendor;
				Console.WriteLine("Vendor: {0}", vendor == null ? string.Empty : vendor.VendorNumber + " - " + vendor.Name);
				Console.WriteLine("Origin: {0}", rate.OriginTerminalCode);
				Console.WriteLine("Destination: {0}", rate.DestinationTerminalCode);
				Console.WriteLine();
				Console.WriteLine("Terminal Details");
				Console.WriteLine();

				watch.Start();
				Console.WriteLine("Start Time: {0}", DateTime.Now);
				var ltlTerminalsInfo = _carrierConnect.GetLTLTerminalInfo(rate.DiscountTier.VendorRating.Vendor.Scac, new[] { rate.OriginTerminalCode, rate.DestinationTerminalCode });
				watch.Stop();
				Console.WriteLine("End Time: {0} Elasped Time (CCXL): {1}s", DateTime.Now, watch.Elapsed.Seconds);
				Console.WriteLine();


				var cnt = 1;
				foreach (var t in ltlTerminalsInfo)
				{
					Console.WriteLine("{0} Terminal - {1}", cnt++, t.Code);
					Console.WriteLine("\tName: {0}", t.Name);
					Console.WriteLine("\tPhone: {0}", t.Phone);
					Console.WriteLine("\tToll Free: {0}", t.TollFree);
					Console.WriteLine("\tFax: {0}", t.Fax);
					Console.WriteLine("\tEmail: {0}", t.Email);
					Console.WriteLine("\tContact Name: {0}", t.ContactName);
					Console.WriteLine("\tContact Tile: {0}", t.ContactTitle);
					Console.WriteLine("\tStreet 1: {0}", t.Street1);
					Console.WriteLine("\tStreet 2: {0}", t.Street2);
					Console.WriteLine("\tCity: {0}", t.City);
					Console.WriteLine("\tState: {0}", t.State);
					Console.WriteLine("\tCountry: {0}-{1}", t.Country.Code, t.Country.Name);
					Console.WriteLine();
				}
			}
		}

		[Test]
		public void CanSetTerminalInfo()
		{
			var shipment = new Shipment(75795) {DesiredPickupDate = DateTime.Now};
			var watch = new Stopwatch();

			Console.WriteLine();
			Console.WriteLine("Terminal Info:");
			Console.WriteLine();

			watch.Start();
			Console.WriteLine("Start Time: {0}", DateTime.Now);
			var success = _carrierConnect.SetPrimaryVendorLTLTerminalInfo(shipment);
			watch.Stop();
			Console.WriteLine("End Time: {0} Elasped Time (CCXL): {1}s - Success: {2}", DateTime.Now, watch.Elapsed.Seconds, success);
			Console.WriteLine();

			Assert.IsNotNull(shipment.OriginTerminal);

			Console.WriteLine("Origin Terminal - {0}", shipment.OriginTerminal.Code);
			Console.WriteLine("\tName: {0}", shipment.OriginTerminal.Name);
			Console.WriteLine("\tPhone: {0}", shipment.OriginTerminal.Phone);
			Console.WriteLine("\tToll Free: {0}", shipment.OriginTerminal.TollFree);
			Console.WriteLine("\tFax: {0}", shipment.OriginTerminal.Fax);
			Console.WriteLine("\tEmail: {0}", shipment.OriginTerminal.Email);
			Console.WriteLine("\tContact Name: {0}", shipment.OriginTerminal.ContactName);
			Console.WriteLine("\tContact Tile: {0}", shipment.OriginTerminal.ContactTitle);
			Console.WriteLine("\tStreet 1: {0}", shipment.OriginTerminal.Street1);
			Console.WriteLine("\tStreet 2: {0}", shipment.OriginTerminal.Street2);
			Console.WriteLine("\tCity: {0}", shipment.OriginTerminal.City);
			Console.WriteLine("\tState: {0}", shipment.OriginTerminal.State);
			Console.WriteLine("\tCountry: {0}-{1}", shipment.OriginTerminal.Country.Code, shipment.OriginTerminal.Country.Name);
			Console.WriteLine();

			Assert.IsNotNull(shipment.DestinationTerminal);

			Console.WriteLine("Destination Terminal - {0}", shipment.DestinationTerminal.Code);
			Console.WriteLine("\tName: {0}", shipment.DestinationTerminal.Name);
			Console.WriteLine("\tPhone: {0}", shipment.DestinationTerminal.Phone);
			Console.WriteLine("\tToll Free: {0}", shipment.DestinationTerminal.TollFree);
			Console.WriteLine("\tFax: {0}", shipment.DestinationTerminal.Fax);
			Console.WriteLine("\tEmail: {0}", shipment.DestinationTerminal.Email);
			Console.WriteLine("\tContact Name: {0}", shipment.DestinationTerminal.ContactName);
			Console.WriteLine("\tContact Tile: {0}", shipment.DestinationTerminal.ContactTitle);
			Console.WriteLine("\tStreet 1: {0}", shipment.DestinationTerminal.Street1);
			Console.WriteLine("\tStreet 2: {0}", shipment.DestinationTerminal.Street2);
			Console.WriteLine("\tCity: {0}", shipment.DestinationTerminal.City);
			Console.WriteLine("\tState: {0}", shipment.DestinationTerminal.State);
			Console.WriteLine("\tCountry: {0}-{1}", shipment.DestinationTerminal.Country.Code, shipment.DestinationTerminal.Country.Name);
			Console.WriteLine();
		}

		[Test]
		public void CanRateQuote2CubicFoot()
		{
			var shipment = new Shipment(75795);

			var watch = new Stopwatch();
			watch.Start();
			Console.WriteLine("Start Time: {0}", DateTime.Now);

			shipment.Customer = new CustomerSearch().FetchCustomerByNumber("10000", GlobalTestInitializer.DefaultTenantId);

			shipment.DesiredPickupDate = DateTime.Now;
			shipment.EarlyPickup = "00:00";
			shipment.LatePickup = "23:45";
			shipment.Origin.PostalCode = "16501";
			shipment.Destination.PostalCode = "12345";
			//shipment.Destination.Country = new Country(new CountrySearch().FetchCountryIdByCode(IataCodes.Country.Canada));
			shipment.Items = new List<ShipmentItem>
				{
					new ShipmentItem {ActualLength = 48, ActualWidth = 40, ActualHeight = 69, Quantity = 3, ActualWeight = 1500, ActualFreightClass = 85},
					new ShipmentItem {ActualLength = 48, ActualWidth = 40, ActualHeight = 58, Quantity = 1, ActualWeight = 295, ActualFreightClass = 85},
					new ShipmentItem {ActualLength = 48, ActualWidth = 40, ActualHeight = 43, Quantity = 2, ActualWeight = 600, ActualFreightClass = 85},
				};
			//shipment.Items = new List<ShipmentItem>
			//	{
			//		new ShipmentItem {ActualLength = 40, ActualWidth = 20, ActualHeight = 48, Quantity = 17, ActualWeight = 2000, IsStackable = true, ActualFreightClass = 50},
			//		new ShipmentItem {ActualLength = 50, ActualWidth = 20, ActualHeight = 40, Quantity = 3, ActualWeight = 100, IsStackable = true, ActualFreightClass = 50},
			//		new ShipmentItem {ActualLength = 55, ActualWidth = 20, ActualHeight = 48, Quantity = 1, ActualWeight = 50, IsStackable = true, ActualFreightClass = 50},
			//	};
			var rates = _rater2.GetRates(shipment);

			watch.Stop();
			Console.WriteLine("End Time: {0} Elasped Time: {1}s", DateTime.Now, watch.Elapsed.TotalMilliseconds);
			Console.WriteLine();
			Console.WriteLine("Rates:");
			Console.WriteLine();

			foreach (var rate in rates)
			{
				Rater2.UpdateShipmentItemRatedCalculations(shipment, rate.CFCRule, rate.DiscountTier);

				Console.WriteLine("Mode: {0}\tEst. Del: {1}", rate.Mode.FormattedString(), rate.EstimatedDeliveryDate);
				var vendor = rate.Vendor ?? (rate.LTLSellRate != null ? rate.LTLSellRate.VendorRating.Vendor : null);
				Console.WriteLine("Vendor: {0}", vendor == null ? string.Empty : vendor.VendorNumber + " - " + vendor.Name);
				Console.WriteLine("LTL Sell Rate: {0}", rate.LTLSellRate == null ? string.Empty : rate.LTLSellRate.VendorRating.Name);
				Console.WriteLine("LTL Sell Rate Markup %: {0}", rate.LTLSellRate == null ? string.Empty : rate.LTLSellRate.MarkupPercent.ToString("f4"));
				Console.WriteLine("LTL Sell Rate Markup Val.: {0}", rate.LTLSellRate == null ? string.Empty : rate.LTLSellRate.MarkupValue.ToString("f4"));
				Console.WriteLine("Discount Tier: {0}", rate.DiscountTier == null ? string.Empty : rate.DiscountTier.OriginRegion.Name + " - " +
					rate.DiscountTier.DestinationRegion.Name);
				Console.WriteLine("Discount Tier Discount: {0}", rate.DiscountTier == null ? string.Empty : rate.DiscountTier.DiscountPercent.ToString("f4"));
				Console.WriteLine("Small Pack Type: {0}", rate.SmallPackServiceType);
				Console.WriteLine("Small Pack Engine: {0}", rate.SmallPackEngine);
				Console.WriteLine("CFC Rule: {0}", rate.CFCRule != null);
				Console.WriteLine("Actual Cubic Feet: {0}", shipment.Items.TotalActualCubitFeet().ToString("f4"));
				Console.WriteLine("Rated Cubic Feet: {0}", rate.RatedCubicFeet.ToString("f4"));
				Console.WriteLine("Actual Weight: {0}", shipment.Items.TotalActualWeight().ToString("f4"));
				Console.WriteLine("Rated Weight: {0}", rate.RatedWeight.ToString("f4"));
				Console.WriteLine("Bill Weight: {0}", rate.BilledWeight.ToString("f4"));
				Console.WriteLine("Original Rate Value: {0}", rate.OriginalRateValue.ToString("f4"));
				Console.WriteLine("Fuel Markup %: {0}", rate.FuelMarkupPercent.ToString("f4"));
				Console.WriteLine("Direct Point Rate: {0}", rate.DirectPointRate);
				Console.WriteLine("Apply Discount: {0}", rate.ApplyDiscount);
				Console.WriteLine("Transit Days: {0}", rate.TransitDays);
				Console.WriteLine("Freight Profit Adj.: {0}", rate.FreightProfitAdjustment.ToString("f4"));
				Console.WriteLine("Fuel Profit Adj.: {0}", rate.FuelProfitAdjustment.ToString("f4"));
				Console.WriteLine("Accessorial Profit Adj.: {0}", rate.AccessorialProfitAdjustment.ToString("f4"));
				Console.WriteLine("Service Profit Adj.: {0}", rate.ServiceProfitAdjustment.ToString("f4"));
				Console.WriteLine("No Freight Profit: {0}", rate.NoFreightProfit);
				Console.WriteLine("No Fuel Profit: {0}", rate.NoFuelProfit);
				Console.WriteLine("No Accessorial Profit: {0}", rate.NoAccessorialProfit);
				Console.WriteLine("No Service Profit: {0}", rate.NoServiceProfit);
				Console.WriteLine("Reseller Freight Markup: {0}", rate.ResellerFreightMarkup.ToString("f4"));
				Console.WriteLine("Reseller Freight Markup Is %: {0}", rate.ResellerFreightMarkupIsPercent);
				Console.WriteLine("Reseller Fuel Markup: {0}", rate.ResellerFuelMarkup.ToString("f4"));
				Console.WriteLine("Reseller Fuel Markup Is %: {0}", rate.ResellerFuelMarkupIsPercent);
				Console.WriteLine("Reseller Accessorial Markup: {0}", rate.ResellerAccessorialMarkup.ToString("f4"));
				Console.WriteLine("Reseller Accessorial Markup Is %: {0}", rate.ResellerAccessorialMarkupIsPercent);
				Console.WriteLine("Reseller Service Markup: {0}", rate.ResellerServiceMarkup.ToString("f4"));
				Console.WriteLine("Reseller Service Markup Is %: {0}", rate.ResellerServiceMarkupIsPercent);
				Console.WriteLine();
				Console.WriteLine("Charges:");
				Console.WriteLine();
				Console.WriteLine("Qty\t\tUnit Buy\tUnit Sell\tUnit Disc.\tCharge Code\tCharge Desc.");
				foreach (var c in rate.Charges)
				{
					Console.WriteLine("{0}\t\t{1}\t{2}\t{3}\t{4}\t{5}",
						c.Quantity, c.UnitBuy.ToString("f4"), c.UnitSell.ToString("f4"), c.UnitDiscount.ToString("f4"), c.ChargeCode.Code, c.ChargeCode.Description);
				}
			}
		}

		[Test]
		public void UpdateShipmentItemRatedCalculationsTest()
		{
			var shipment = new Shipment(75795);

			var watch = new Stopwatch();
			watch.Start();
			Console.WriteLine("Start Time: {0}", DateTime.Now);

			shipment.DesiredPickupDate = DateTime.Now;
			shipment.Origin.PostalCode = "16501";
			shipment.Destination.PostalCode = "12345";
			//shipment.Destination.Country = new Country(new CountrySearch().FetchCountryIdByCode(IataCodes.Country.Canada));
			shipment.Items = new List<ShipmentItem>
				{
					new ShipmentItem {ActualLength = 48, ActualWidth = 40, ActualHeight = 69, Quantity = 3, ActualWeight = 1500, ActualFreightClass = 85},
					new ShipmentItem {ActualLength = 48, ActualWidth = 40, ActualHeight = 58, Quantity = 1, ActualWeight = 295, ActualFreightClass = 85},
					new ShipmentItem {ActualLength = 48, ActualWidth = 40, ActualHeight = 43, Quantity = 2, ActualWeight = 600, ActualFreightClass = 85},
				};
			var rates = _rater2.GetRates(shipment);

			watch.Stop();
			Console.WriteLine("End Time: {0} Elasped Time: {1}s", DateTime.Now, watch.Elapsed.Seconds);
			Console.WriteLine();
			Console.WriteLine("Rates:");
			Console.WriteLine();

			foreach (var rate in rates.Where(r => r.CFCRule != null && r.LTLSellRate.VendorRating.Vendor.VendorNumber == "342"))
			//foreach (var rate in rates.Where(r=>r.CFCRule != null))
			{
				Rater2.UpdateShipmentItemRatedCalculations(shipment, rate.CFCRule, rate.DiscountTier);

				Console.WriteLine();

				var vendor = rate.Vendor ?? (rate.LTLSellRate != null ? rate.LTLSellRate.VendorRating.Vendor : null);
				Console.WriteLine("Vendor: {0}", vendor == null ? string.Empty : vendor.VendorNumber + " - " + vendor.Name);
				Console.WriteLine("Discount Tier: {0}", rate.DiscountTier == null ? string.Empty : rate.DiscountTier.OriginRegion.Name + " - " +
					rate.DiscountTier.DestinationRegion.Name);
				Console.WriteLine("Shipment Item Count: {0}", shipment.Items.Count);
				Console.WriteLine("Shipment Items Actual CF: {0}", shipment.Items.TotalActualCubitFeet());
				Console.WriteLine("Shipment Items Rated CF: {0}", rate.RatedCubicFeet);
				Console.WriteLine("Dim\tAct\tRated");
				foreach (var item in shipment.Items)
				{
					
					Console.WriteLine("Fc:\t{0}\t{1}", item.ActualFreightClass, item.RatedFreightClass);
					Console.WriteLine("Qty:\t{0}", item.Quantity);
					Console.WriteLine();
				}
			}
		}
	}
}
