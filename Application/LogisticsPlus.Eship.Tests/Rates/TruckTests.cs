using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor.Rates;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Rates
{
	[TestFixture]
	public class TruckTests
	{
		private VendorRating _rating;

		[SetUp]
		public void SetUp()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_rating = new VendorRating
			          	{
			          		LTLTruckHeight = 96,
			          		LTLTruckLength = 144,
			          		LTLTruckUnitWeight = 5000,
			          		LTLTruckWeight = 10000,
							LTLMinPickupWeight = 500,
			          		LTLTruckWidth = 96,
                            MaxPackageQuantityApplies = true,
                            MaxItemWidth = 48,
                            MaxItemLength = 48,
                            MaxItemHeight = 48,
                            MaxPackageQuantity = 6,
                            IndividualItemLimitsApply = true
			          	};
		}

		[Test]
		public void CanTruckSupportShipment()
		{
			var shipment = new Shipment{Items = new List<ShipmentItem>()};

			shipment.Items.Add(new ShipmentItem());
			shipment.Items[0].ActualWeight = 5000;
			shipment.Items[0].ActualHeight = 80;
			shipment.Items[0].ActualWidth = 48;
			shipment.Items[0].ActualLength = 96;
			shipment.Items[0].IsStackable = false;
			shipment.Items[0].Quantity = 1;

			shipment.Items.Add(new ShipmentItem());
			shipment.Items[1].ActualWeight = 5000;
			shipment.Items[1].ActualHeight = 30;
			shipment.Items[1].ActualWidth = 48;
			shipment.Items[1].ActualLength = 48;
			shipment.Items[1].IsStackable = true;
			shipment.Items[1].Quantity = 4;

			Assert.IsTrue(_rating.CanHoldWeightOf(shipment), "Weight");

			Assert.IsTrue(_rating.FitsLinearFootage(shipment), "Linear Foot");
		}

		[Test]
		public void CanTruckSupportVolumeSingleShipmentItemGroupNotStackable()
		{
			var shipment = new Shipment { Items = new List<ShipmentItem>() };

			shipment.Items.Add(new ShipmentItem());
			shipment.Items[0].ActualWeight = 5000;
			shipment.Items[0].ActualHeight = 77;
			shipment.Items[0].ActualWidth = 48;
			shipment.Items[0].ActualLength = 48;
			shipment.Items[0].IsStackable = false;
			shipment.Items[0].Quantity = 6;


			Assert.IsTrue(_rating.FitsLinearFootage(shipment), "Linear Foot");
		}

		[Test]
		public void CanTruckSupportVolumeMultiShipmentItemGroupNotStackable()
		{
			var shipment = new Shipment { Items = new List<ShipmentItem>() };

			shipment.Items.Add(new ShipmentItem());
			shipment.Items[0].ActualWeight = 5000;
			shipment.Items[0].ActualHeight = 77;
			shipment.Items[0].ActualWidth = 36;
			shipment.Items[0].ActualLength = 48;
			shipment.Items[0].IsStackable = false;
			shipment.Items[0].Quantity = 4;

			shipment.Items.Add(new ShipmentItem());
			shipment.Items[1].ActualWeight = 5000;
			shipment.Items[1].ActualHeight = 77;
			shipment.Items[1].ActualWidth = 24;
			shipment.Items[1].ActualLength = 24;
			shipment.Items[1].IsStackable = false;
			shipment.Items[1].Quantity = 4;


			Assert.IsTrue(_rating.FitsLinearFootage(shipment), "Linear Foot");
		}

		[Test]
		public void CanTruckSupportVolumeSingleShipmentItemOverlength()
		{
			var shipment = new Shipment { Items = new List<ShipmentItem>() };

			shipment.Items.Add(new ShipmentItem());
			shipment.Items[0].ActualWeight = 5000;
			shipment.Items[0].ActualHeight = 14;
			shipment.Items[0].ActualWidth = 23;
			shipment.Items[0].ActualLength = 245;
			shipment.Items[0].IsStackable = false;
			shipment.Items[0].Quantity = 1;

			_rating.LTLOverLengthRules.Add(new LTLOverLengthRule
				{
					UpperLengthBound = 250,
					LowerLengthBound = _rating.LTLTruckLength + 1
				});


			Assert.IsTrue(_rating.FitsLinearFootage(shipment), "Linear Foot");

			_rating.LTLOverLengthRules.Clear();
		}

        [Test]
        public void CanTruckSupportShipmentItems()
        {
            var shipment = new Shipment { Items = new List<ShipmentItem>() };

            shipment.Items.Add(new ShipmentItem());
            shipment.Items[0].ActualWeight = 1000;
            shipment.Items[0].ActualHeight = 48;
            shipment.Items[0].ActualWidth = 48;
            shipment.Items[0].ActualLength = 48;
            shipment.Items[0].IsStackable = false;
            shipment.Items[0].Quantity = 3;

            shipment.Items.Add(new ShipmentItem());
            shipment.Items[1].ActualWeight = 1000;
            shipment.Items[1].ActualHeight = 22;
            shipment.Items[1].ActualWidth = 22;
            shipment.Items[1].ActualLength = 48;
            shipment.Items[1].IsStackable = false;
            shipment.Items[1].Quantity = 3;

            Assert.IsTrue(_rating.ItemsWithinLimits(shipment), "Items Within Limits");
        }

		[Test]
		public void CanTruckSupportVolumeMultiShipmentItemGroupStackable()
		{
			var shipment = new Shipment { Items = new List<ShipmentItem>() };

			shipment.Items.Add(new ShipmentItem());
			shipment.Items[0].ActualWeight = 5000;
			shipment.Items[0].ActualHeight = 34;
			shipment.Items[0].ActualWidth = 36;
			shipment.Items[0].ActualLength = 48;
			shipment.Items[0].IsStackable = true;
			shipment.Items[0].Quantity = 4;

			shipment.Items.Add(new ShipmentItem());
			shipment.Items[1].ActualWeight = 5000;
			shipment.Items[1].ActualHeight = 26;
			shipment.Items[1].ActualWidth = 24;
			shipment.Items[1].ActualLength = 24;
			shipment.Items[1].IsStackable = true;
			shipment.Items[1].Quantity = 4;


			Assert.IsTrue(_rating.FitsLinearFootage(shipment), "Linear Foot");
		}

		[Test]
		public void CanTruckSupportVolumeMultiShipmentItemMixed1()
		{
			var shipment = new Shipment { Items = new List<ShipmentItem>() };

			shipment.Items.Add(new ShipmentItem());
			shipment.Items[0].ActualWeight = 5000;
			shipment.Items[0].ActualHeight = 34;
			shipment.Items[0].ActualWidth = 60;
			shipment.Items[0].ActualLength = 24;
			shipment.Items[0].IsStackable = false;
			shipment.Items[0].Quantity = 1;

			shipment.Items.Add(new ShipmentItem());
			shipment.Items[1].ActualWeight = 2000;
			shipment.Items[1].ActualHeight = 48;
			shipment.Items[1].ActualWidth = 48;
			shipment.Items[1].ActualLength = 48;
			shipment.Items[1].IsStackable = true;
			shipment.Items[1].Quantity = 6;

			shipment.Items.Add(new ShipmentItem());
			shipment.Items[2].ActualWeight = 3000;
			shipment.Items[2].ActualHeight = 77;
			shipment.Items[2].ActualWidth = 48;
			shipment.Items[2].ActualLength = 60;
			shipment.Items[2].IsStackable = false;
			shipment.Items[2].Quantity = 1;


			Assert.IsTrue(_rating.FitsLinearFootage(shipment), "Linear Foot");
		}

		[Test]
		public void CanTruckSupportVolumeMultiShipmentItemMixed2()
		{
			var shipment = new Shipment { Items = new List<ShipmentItem>() };

			shipment.Items.Add(new ShipmentItem());
			shipment.Items[0].ActualWeight = 2680;
			shipment.Items[0].ActualHeight = 24;
			shipment.Items[0].ActualWidth = 48;
			shipment.Items[0].ActualLength = 43;
			shipment.Items[0].IsStackable = false;
			shipment.Items[0].Quantity = 3;

			shipment.Items.Add(new ShipmentItem());
			shipment.Items[1].ActualWeight = 2000;
			shipment.Items[1].ActualHeight = 24;
			shipment.Items[1].ActualWidth = 48;
			shipment.Items[1].ActualLength = 144;
			shipment.Items[1].IsStackable = false;
			shipment.Items[1].Quantity = 1;


			Assert.IsTrue(_rating.FitsLinearFootage(shipment), "Linear Foot");
		}

		[Test]
		public void CanTruckSupportVolumeMultiShipmentItemMixed3()
		{
			var shipment = new Shipment { Items = new List<ShipmentItem>() };
			var i = 0;

			shipment.Items.Add(new ShipmentItem());
			shipment.Items[i].ActualWeight = 465;
			shipment.Items[i].ActualHeight = 40;
			shipment.Items[i].ActualWidth = 31;
			shipment.Items[i].ActualLength = 60;
			shipment.Items[i].IsStackable = false;
			shipment.Items[i].Quantity = 1;

			i++;
			shipment.Items.Add(new ShipmentItem());
			shipment.Items[i].ActualWeight = 465;
			shipment.Items[i].ActualHeight = 35;
			shipment.Items[i].ActualWidth = 31;
			shipment.Items[i].ActualLength = 33;
			shipment.Items[i].IsStackable = false;
			shipment.Items[i].Quantity = 2;

			i++;
			shipment.Items.Add(new ShipmentItem());
			shipment.Items[i].ActualWeight = 600;
			shipment.Items[i].ActualHeight = 20;
			shipment.Items[i].ActualWidth = 30;
			shipment.Items[i].ActualLength = 72;
			shipment.Items[i].IsStackable = false;
			shipment.Items[i].Quantity = 1;

			i++;
			shipment.Items.Add(new ShipmentItem());
			shipment.Items[i].ActualWeight = 5;
			shipment.Items[i].ActualHeight = 5;
			shipment.Items[i].ActualWidth = 5;
			shipment.Items[i].ActualLength = 5;
			shipment.Items[i].IsStackable = false;
			shipment.Items[i].Quantity = 1;

			i++;
			shipment.Items.Add(new ShipmentItem());
			shipment.Items[i].ActualWeight = 140;
			shipment.Items[i].ActualHeight = 30;
			shipment.Items[i].ActualWidth = 31;
			shipment.Items[i].ActualLength = 40;
			shipment.Items[i].IsStackable = false;
			shipment.Items[i].Quantity = 1;

			i++;
			shipment.Items.Add(new ShipmentItem());
			shipment.Items[i].ActualWeight = 29;
			shipment.Items[i].ActualHeight = 8;
			shipment.Items[i].ActualWidth = 12;
			shipment.Items[i].ActualLength = 48;
			shipment.Items[i].IsStackable = false;
			shipment.Items[i].Quantity = 1;

			i++;
			shipment.Items.Add(new ShipmentItem());
			shipment.Items[i].ActualWeight = 29;
			shipment.Items[i].ActualHeight = 12;
			shipment.Items[i].ActualWidth = 10;
			shipment.Items[i].ActualLength = 30;
			shipment.Items[i].IsStackable = false;
			shipment.Items[i].Quantity = 1;


			Assert.IsTrue(_rating.FitsLinearFootage(shipment), "Linear Foot");
		}

		[Test]
		public void InferQuantityMultiplierAfterStacks()
		{
			var shipment = new Shipment { Items = new List<ShipmentItem>() };
			const int i = 0;

			shipment.Items.Add(new ShipmentItem());
			shipment.Items[i].ActualWeight = 240;
			shipment.Items[i].ActualHeight = 48;
			shipment.Items[i].ActualWidth = 48;
			shipment.Items[i].ActualLength = 48;
			shipment.Items[i].IsStackable = true;
			shipment.Items[i].Quantity = 12;

			Console.WriteLine("Truck 2 Stacks: {0}", shipment.Items[0].InferQuantityMultiplierAfterStacks2(_rating));
		}

		[Test]
		public void CanConfigureRatingItemsTest()
		{
			var r = new VendorRating(279);
			var shipment = new Shipment
			{
				Items = new List<ShipmentItem>
						{
							new ShipmentItem {ActualLength = 48, ActualWidth = 40, ActualHeight = 69, Quantity = 3},
							new ShipmentItem {ActualLength = 48, ActualWidth = 40, ActualHeight = 58, Quantity = 1},
							new ShipmentItem {ActualLength = 48, ActualWidth = 40, ActualHeight = 43, Quantity = 2},
						}
			};
			var boxes = r.ConfigureRatingItems(shipment);
			DisplayBoxes(boxes, 1);

			shipment = new Shipment
			{
				Items = new List<ShipmentItem>
						{
							new ShipmentItem {ActualLength = 48, ActualWidth = 20, ActualHeight = 20, Quantity = 8, IsStackable = true},
						}
			};
			boxes = r.ConfigureRatingItems(shipment);
			DisplayBoxes(boxes, 2);

			shipment = new Shipment
			{
				Items = new List<ShipmentItem>
						{
							new ShipmentItem {ActualLength = 48, ActualWidth = 20, ActualHeight = 20, Quantity = 12, IsStackable = true},
						}
			};
			boxes = r.ConfigureRatingItems(shipment);
			DisplayBoxes(boxes, 3);

			shipment = new Shipment
			{
				Items = new List<ShipmentItem>
						{
							new ShipmentItem {ActualLength = 48, ActualWidth = 20, ActualHeight = 20, Quantity = 20, IsStackable = true},
						}
			};
			boxes = r.ConfigureRatingItems(shipment);
			DisplayBoxes(boxes, 4);

			shipment = new Shipment
			{
				Items = new List<ShipmentItem>
						{
							new ShipmentItem {ActualLength = 48, ActualWidth = 20, ActualHeight = 20, Quantity = 17, IsStackable = true},
							new ShipmentItem {ActualLength = 50, ActualWidth = 20, ActualHeight = 20, Quantity = 3, IsStackable = true},
							new ShipmentItem {ActualLength = 55, ActualWidth = 20, ActualHeight = 48, Quantity = 1, IsStackable = true},
						}
			};
			boxes = r.ConfigureRatingItems(shipment);
			DisplayBoxes(boxes, 5);
		}

		[Test]
		public void CanRetrieveRatingItemHierachyTest()
		{
			var r = new VendorRating(279);
			var shipment = new Shipment
				{
					Items = new List<ShipmentItem>
						{
							new ShipmentItem {ActualLength = 48, ActualWidth = 40, ActualHeight = 69, Quantity = 3},
							new ShipmentItem {ActualLength = 48, ActualWidth = 40, ActualHeight = 58, Quantity = 1},
							new ShipmentItem {ActualLength = 48, ActualWidth = 40, ActualHeight = 43, Quantity = 2},
						}
				};
			var boxes = r.ConfigureRatingItems(shipment);
			DisplayBoxes(boxes.SelectMany(b => b.RetrieveComponentHierarchy()), 1);


			shipment = new Shipment
				{
					Items = new List<ShipmentItem>
						{
							new ShipmentItem {ActualLength = 48, ActualWidth = 20, ActualHeight = 20, Quantity = 8, IsStackable = true},
						}
				};
			boxes = r.ConfigureRatingItems(shipment);
			DisplayBoxes(boxes.SelectMany(b => b.RetrieveComponentHierarchy()), 2);

			shipment = new Shipment
				{
					Items = new List<ShipmentItem>
						{
							new ShipmentItem {ActualLength = 48, ActualWidth = 20, ActualHeight = 20, Quantity = 12, IsStackable = true},
						}
				};
			boxes = r.ConfigureRatingItems(shipment);
			DisplayBoxes(boxes.SelectMany(b => b.RetrieveComponentHierarchy()), 3);

			shipment = new Shipment
				{
					Items = new List<ShipmentItem>
						{
							new ShipmentItem {ActualLength = 48, ActualWidth = 20, ActualHeight = 20, Quantity = 20, IsStackable = true},
						}
				};
			boxes = r.ConfigureRatingItems(shipment);
			DisplayBoxes(boxes.SelectMany(b => b.RetrieveComponentHierarchy()), 4);

			shipment = new Shipment
				{
					Items = new List<ShipmentItem>
						{
							new ShipmentItem {ActualLength = 48, ActualWidth = 20, ActualHeight = 20, Quantity = 17, IsStackable = true},
							new ShipmentItem {ActualLength = 50, ActualWidth = 20, ActualHeight = 20, Quantity = 3, IsStackable = true},
							new ShipmentItem {ActualLength = 55, ActualWidth = 20, ActualHeight = 48, Quantity = 1, IsStackable = true},
						}
				};
			boxes = r.ConfigureRatingItems(shipment);
			DisplayBoxes(boxes.SelectMany(b => b.RetrieveComponentHierarchy()), 5);
		}


		private static void DisplayBoxes(IEnumerable<RatingItem> boxes, int round)
		{
			Console.WriteLine();
			Console.WriteLine("Round {0}", round);
			foreach (var box in boxes)
				Console.WriteLine(
					"L:{0:n4} W:{1:n4} H{2:n4} RL:{3:n4} RW:{4:n4} RH:{5:n4} RCF:{6:n4}\tComponents:{7}\tStacked:{8}\tStackable:{9}",
					box.Length, box.Width, box.Height, box.RatedLength, box.RatedWidth, box.RatedHeight, box.RatingCubicFeet,
					box.HasComponents, box.Stacked, box.Stackable);
		}
	}
}