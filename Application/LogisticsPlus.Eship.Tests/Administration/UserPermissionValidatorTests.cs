﻿using System;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Validation.Administration;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Administration
{
	public class UserPermissionValidatorTests
	{
		private UserPermissionValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new UserPermissionValidator();
		}

		[Test]
		public void DuplicateGroupCodeTest()
		{
			var user = new User(GlobalTestInitializer.DefaultUserId); 
			if(user.UserPermissions.Count == 0) return;
			var permission = new UserPermission
			                 	{
			                 		Code = user.UserPermissions[0].Code,
			                 		User = user,
			                 		TenantId = user.TenantId
			                 	};
			Assert.IsTrue(_validator.DuplicateUserPermission(permission));
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var permission = new UserPermission();
			Assert.IsFalse(_validator.HasAllRequiredData(permission));
		}

		[Test]
		public void IsValidTest()
		{
			var permission = new UserPermission
			                 	{
			                 		Code = DateTime.Now.ToString(),
			                 		Description = string.Empty,
			                 		TenantId = GlobalTestInitializer.DefaultTenantId,
			                 		User = GlobalTestInitializer.ActiveUser,
			                 		Modify = true
			                 	};
			Assert.IsTrue(_validator.IsValid(permission));
		}
	}
}
