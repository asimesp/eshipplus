﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Administration;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Administration;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Administration
{
	[TestFixture]
	public class UserHandlerTests
	{
		private UserView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new UserView();
			_view.Load();
		}

		[Test]
		public void CanUserSaveOrUpdate()
		{
            var criteria = new UserSearchCriteria
            {
                Parameters = new List<ParameterColumn>()
            };
			var all = new UserSearch().FetchUsers(criteria, GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var user = new User(all[0].Id);
			user.LoadCollections();
			_view.LockRecord(user);
			user.TakeSnapShot();
			var username = user.Username;
			user.Username = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff");
			_view.SaveRecord(user);
			user.TakeSnapShot();
			user.Username = username;
			_view.SaveRecord(user);
			_view.UnLockRecord(user);
		}

		[Test]
		public void CanUserSaveAndDelete()
		{
			var user = new User
			           	{
			           		Username = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff"),
			           		Password = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff"),
			           		FirstName = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff"),
			           		LastName = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff"),
			           		TenantId = _view.ActiveUser.TenantId,
			           		UserPermissions = new List<UserPermission>
			           		                  	{
			           		                  		new UserPermission
			           		                  			{
			           		                  				Code = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff"),
			           		                  				Description = "TEST",
			           		                  				Grant = true,
			           		                  				TenantId = _view.ActiveUser.TenantId,
			           		                  			}
			           		                  	},
			           		DefaultCustomer = new Customer(GlobalTestInitializer.DefaultCustomerId),
			           		CountryId = GlobalTestInitializer.DefaultCountryId,
							AlwaysShowRatingNotice = false,
							QlikUserId = string.Empty,
                            QlikUserDirectory = string.Empty,
                            DatLoadboardPassword = string.Empty,
                            DatLoadboardUsername = string.Empty,
							AdUserName = string.Empty,
			           	};
			user.UserPermissions[0].User = user;
			user.LoadCollections();
			_view.SaveRecord(user);
			Assert.IsTrue(user.Id != default(long));
			_view.DeleteRecord(user);
			Assert.IsTrue(!new User(user.Id, false).KeyLoaded);
		}

		[Test]
		public void CanUserSaveAndDeleteWithSearchProfile()
		{
			var user = new User
			           	{
			           		Username = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff"),
			           		Password = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff"),
			           		FirstName = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff"),
			           		LastName = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff"),
			           		TenantId = _view.ActiveUser.TenantId,
			           		UserPermissions = new List<UserPermission>
			           		                  	{
			           		                  		new UserPermission
			           		                  			{
			           		                  				Code = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff"),
			           		                  				Description = "TEST",
			           		                  				Grant = true,
			           		                  				TenantId = _view.ActiveUser.TenantId,
			           		                  			}
			           		                  	},
			           		DefaultCustomer = new Customer(GlobalTestInitializer.DefaultCustomerId),
			           		CountryId = GlobalTestInitializer.DefaultCountryId,
			           		AlwaysShowRatingNotice = false,
							QlikUserId = string.Empty,
                            QlikUserDirectory = string.Empty,
                            DatLoadboardUsername = string.Empty,
                            DatLoadboardPassword = string.Empty,
							AdUserName = string.Empty
			           	};
			user.UserPermissions[0].User = user;
			user.LoadCollections();
			_view.SaveRecord(user);
			Assert.IsTrue(user.Id != default(long));
			var profile = new SearchProfileDefault {User = user, TenantId = user.TenantId};
			profile.Save();
			Assert.IsTrue(!profile.IsNew);
			_view.DeleteRecord(user);
			Assert.IsTrue(!new User(user.Id, false).KeyLoaded);
		}

		internal class UserView : IUserView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public List<Country> Countries
			{
				set { }
			}

			public List<UserDepartment> Departments
			{
				set {  }
			}

			public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<User>> Save;
			public event EventHandler<ViewEventArgs<User>> Delete;
			public event EventHandler<ViewEventArgs<User>> Lock;
			public event EventHandler<ViewEventArgs<User>> UnLock;
			public event EventHandler<ViewEventArgs<User>> LoadAuditLog;
			public event EventHandler<ViewEventArgs<List<Group>>> UpdateGroupUsers;
			public event EventHandler<ViewEventArgs<List<GroupUser>>> RemoveGroupUsers;
		    public event EventHandler<ViewEventArgs<List<User>>> BatchImport;
		    public event EventHandler<ViewEventArgs<string>> CustomerSearch;
		    public event EventHandler<ViewEventArgs<string>> VendorSearch;

			public void DisplayCustomer(Customer customer)
			{
			
			}
            
            public void DisplayVendor(Vendor vendor)
			{
			
			}

			public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
			{

			}

		    public void LogException(Exception ex)
		    {
		    	Console.WriteLine(ex);
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void FailedLock(Lock @lock)
			{

			}

			public void Set(User user)
			{
				
			}

			public void Load()
			{
				var handler = new UserHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(User user)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<User>(user));
			}

			public void DeleteRecord(User user)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<User>(user));
			}

			public void LockRecord(User user)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<User>(user));
			}

			public void UnLockRecord(User user)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<User>(user));
			}
		}
	}
}
