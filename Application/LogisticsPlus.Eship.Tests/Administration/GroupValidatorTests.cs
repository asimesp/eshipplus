﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Validation.Administration;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Administration
{
	[TestFixture]
	public class GroupValidatorTests
	{
		private GroupValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new GroupValidator();
		}

		[Test]
		public void DuplicateGroupNameTest()
		{
			var all = new GroupSearch().FetchGroups(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var group = new Group {Name = all[0].Name, Description = all[0].Description, TenantId = all[0].TenantId};
			Assert.IsTrue(_validator.DuplicateGroupName(group));
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var group = new Group();
			Assert.IsFalse(_validator.HasAllRequiredData(group));
		}

		[Test]
		public void IsValidTest()
		{
			var group = new Group
			            	{
			            		Name = DateTime.Now.ToString(),
			            		Description = string.Empty,
								TenantId = GlobalTestInitializer.DefaultTenantId,
			            		Permissions = new List<GroupPermission> {new GroupPermission()}
			            	};
			Assert.IsTrue(_validator.IsValid(group));
		}

	    [Test]
	    public void CanDeleteTest()
	    {
	        var group = new Group(GlobalTestInitializer.DefaultGroupId);
	        Assert.IsTrue(group.KeyLoaded);
	        _validator.Messages.Clear();
	        var deleteGroup = _validator.CanDeleteGroup(group);
	        Assert.IsTrue(_validator.Messages.Count > 0);
	        Assert.IsFalse(deleteGroup);
	        _validator.Messages.PrintMessages();
	    }
    }
}
