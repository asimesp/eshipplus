﻿using LogisticsPlus.Eship.Core.Administration;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Administration
{
	[TestFixture]
	public class AdminUserTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanLoadWithoutExceptions()
		{
		    var adminUser = new AdminUser(GlobalTestInitializer.DefaultUserId);

            adminUser.LoadCollections();
		}
	}
}
