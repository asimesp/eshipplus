﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Administration
{
	[TestFixture]
	public class GroupSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchByWildCard()
		{
			var code = new GroupSearch().FetchGroups(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(code.Count > 0);
		}

        [Test]
        public void CanFetchGroupsWithUserInfo()
        {
            var columns = new List<ParameterColumn>();

            for (var i = 0; i < 3; i++)
            {
                columns.Add(AdminSearchFields.Username.ToParameterColumn());
            }

            columns[0].DefaultValue = "rick";
            columns[0].Operator = Operator.Contains;

            columns[1].DefaultValue = "chris";
            columns[1].Operator = Operator.Contains;

            columns[2].DefaultValue = "deb";
            columns[2].Operator = Operator.Contains;

            var field = AdminSearchFields.Groups.FirstOrDefault(f => f.Name == "Name");
            if (field == null)
            {
                Console.WriteLine("Admin search fields no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }

            var column = field.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            columns.Add(column);

            var results = new GroupSearch().FetchGroups(columns, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(results.Any());
        }

        [Test]
        public void CanFetchGroupsWithoutUserInfo()
        {
            var columns = new List<ParameterColumn>();

            var field = AdminSearchFields.Groups.FirstOrDefault(f => f.Name == "Name");
            if (field == null)
            {
                Console.WriteLine("Admin search fields no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }

            var column = field.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            columns.Add(column);

            var results = new GroupSearch().FetchGroups(columns, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(results.Any());
        }
	}
}
