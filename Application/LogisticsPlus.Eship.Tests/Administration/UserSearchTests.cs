﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Views.Administration;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Administration
{
    [TestFixture]
    public class UserSearchTests
    {
        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

		[Test]
		public void CanFetchEmployeesOnly()
		{
			var columns = new List<ParameterColumn> {AdminSearchFields.TenantEmployee.ToParameterColumn()};
			columns[0].DefaultValue = true.GetString();
			columns[0].Operator = Operator.Equal;

		    var field = AdminSearchFields.Users.FirstOrDefault(f => f.Name == "FirstName");
            if (field == null)
            {
                Console.WriteLine("AdminSearchFields.Users  no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }

		    var column = field.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            columns.Add(column);

            var criteria = new UserSearchCriteria
            {
                Parameters = columns,
                SortBy = null
            };
            var users = new UserSearch().FetchUsers(criteria, GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(users.Any());
			Assert.IsTrue(users.All(u => u.TenantEmployee));
		}

        [Test]
        public void CanFetchWithCountryData()
        {
            var column = AdminSearchFields.CountryCode.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
        	var columns = new List<ParameterColumn> {column};

            var criteria = new UserSearchCriteria
            {
                Parameters = columns,
                SortBy = null
            };
        	var users = new UserSearch().FetchUsers(criteria, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(users.Any());
        }

        [Test]
        public void CanFetchByUserNameAndTenantCode()
        {
            User user = new UserSearch().FetchUserByUsernameAndAccessCode(GlobalTestInitializer.ActiveUser.Username,
                                                                         GlobalTestInitializer.DefaultTenantCode);
        	Assert.IsTrue(user != null);

        }

		[Test]
		public void CannotFetchByInvalidByUserName()
		{
			User user = new UserSearch().FetchUserByUsernameAndAccessCode(SearchUtilities.WildCard,
																		 GlobalTestInitializer.DefaultTenantCode);
			Assert.IsTrue(user == null);

		}

		[Test]
		public void CanFetchUsersByCriteriaWithSearchDefaults()
		{
			var users = new UserSearch()
				.FetchUsersByCriteriaWithSearchDefaults(GlobalTestInitializer.DefaultTenantId, SearchUtilities.WildCard,
				                                        Operator.Contains);
			Assert.IsTrue(users.Any());
		}

		[Test]
		public void CannotFetchByInvalidByTenantCode()
		{
			User user = new UserSearch().FetchUserByUsernameAndAccessCode(GlobalTestInitializer.ActiveUser.Username, string.Empty);
			Assert.IsTrue(user == null);

		}
	}
}
