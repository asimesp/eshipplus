﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Administration;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Administration;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Administration
{
	[TestFixture]
	public class UserProfileHandlerTests
	{
		private UserView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new UserView();
			_view.Load();
		}

		[Test]
		public void CanGroupSaveAndDelete()
		{
		    var all = new UserSearch().FetchUsers("%", GlobalTestInitializer.DefaultCountryId,GlobalTestInitializer.DefaultTenantId);
            if (all.Count == 0) return;
		    var user = all[0];
            user.LoadCollections();
            _view.LockRecord(user);
            user.TakeSnapShot();
		    var name = user.FirstName;
		    user.FirstName = DateTime.Now.ToShortDateString();
            _view.SaveRecord(user);
            user.TakeSnapShot();
		    user.FirstName = name;
            _view.SaveRecord(user);
            _view.UnLockRecord(user);
		}

		internal class UserView : IUserView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public List<Country> Countries
			{
				set { Assert.IsTrue(value.Count > 0); }
			}

			public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<User>> Save;
			public event EventHandler<ViewEventArgs<User>> Delete;
			public event EventHandler<ViewEventArgs<User>> Lock;
			public event EventHandler<ViewEventArgs<User>> UnLock;
			public event EventHandler<ViewEventArgs<User>> LoadAuditLog;
			public event EventHandler<ViewEventArgs<List<Group>>> UpdateGroupUsers;
			public event EventHandler<ViewEventArgs<List<GroupUser>>> RemoveGroupUsers;
			public event EventHandler<ViewEventArgs<string>> CustomerSearch;

			public void DisplayCustomer(Customer customer)
			{
			
			}

			public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
			{

			}

			public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void FailedLock(Lock @lock)
			{

			}

			public void Set(User user)
			{
				
			}

			public void Load()
			{
				var handler = new UserProfileHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(User user)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<User>(user));
			}

			public void LockRecord(User user)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<User>(user));
			}

			public void UnLockRecord(User user)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<User>(user));
			}
		}
	}
}
