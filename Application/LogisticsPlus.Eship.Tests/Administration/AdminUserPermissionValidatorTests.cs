﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Validation.Administration;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Administration
{
	public class AdminUserPermissionValidatorTests
	{
		private AdminUserPermissionValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _validator = new AdminUserPermissionValidator();
		}

		[Test]
		public void DuplicateGroupCodeTest()
		{
            var all = new AdminUserSearch().FetchAdminUsers(new List<ParameterColumn>());
			if (all.Count == 0) return;
			var adminUser = all.FirstOrDefault(u => u.AdminUserPermissions.Count > 0);
			if (adminUser == null) return;
			var adminUserPermission = new AdminUserPermission
			                 	{
			                 		Code = adminUser.AdminUserPermissions[0].Code,
			                 		AdminUser = adminUser,
			                 	};
			Assert.IsTrue(_validator.DuplicateUserPermission(adminUserPermission));
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var permission = new AdminUserPermission();
			Assert.IsFalse(_validator.HasAllRequiredData(permission));
		}

		[Test]
		public void IsValidTest()
		{
			var adminUserPermission = new AdminUserPermission
			                 	{
			                 		Code = DateTime.Now.ToString(),
			                 		Description = string.Empty,
			                 		AdminUser = new AdminUser(GlobalTestInitializer.DefaultAdminUserId),
			                 		Modify = true
			                 	};
			Assert.IsTrue(_validator.IsValid(adminUserPermission));
		}
	}
}
