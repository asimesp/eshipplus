﻿using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace LogisticsPlus.Eship.Tests.Administration
{
    [TestFixture]
    public class ChargeTests
    {
        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

        [Test]
        public void CanLoadWithoutExceptions()
        {
            var user = new User(12948);

            var cs = new ChargeSearch();
            var getInvoiceChargeGroups = cs.FetchChargeGroups(
                new ChargeViewSearchCriteria { Parameters = new List<ParameterColumn>() },
                GlobalTestInitializer.DefaultTenantId,
                user.VendorPortalVendorId,
                user.CanSeeAllVendorsInVendorPortal);

            var wrongItems = getInvoiceChargeGroups.Where(t => t.VendorId != user.VendorPortalVendorId)
                .ToList();

            if (wrongItems.Any())
            {
                Assert.Fail("Search returned wrong items");
            }

            if (getInvoiceChargeGroups.Count > 0)
            {
                var firstElementInGroup = getInvoiceChargeGroups.First();
                var charge = cs.FetchChargesByTypeAndNumber(firstElementInGroup.Number, firstElementInGroup.ChargeType.ToEnum<ChargeType>());
                cs.FetchChargesByTypeAndId(charge.First().Id.ToString(), charge.First().ChargeType.ToString(), charge.First().ChargeType);
            }

        }
    }
}
