﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Administration;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Administration;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Administration
{
    [TestFixture]
    public class AdminUserHandlerTests
    {
        private AdminUserView _view;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _view = new AdminUserView();
            _view.Load();
        }

        [Test]
        public void CanUserSaveOrUpdate()
        {
            var all = new AdminUserSearch().FetchAdminUsers(new List<ParameterColumn>());
            if (all.Count == 0) return;
            var adminUser = all.Last();
            adminUser.LoadCollections();
            adminUser.TakeSnapShot();
            var username = adminUser.Username;
            adminUser.Username = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff");
            _view.SaveRecord(adminUser);
            adminUser.TakeSnapShot();
            adminUser.Username = username;
            _view.SaveRecord(adminUser);
            Assert.IsTrue(adminUser.Username == username);
        }

        [Test]
        public void CanUserSaveAndDelete()
        {
            var adminUser = new AdminUser
                        {
                            Username = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff"),
                            Password = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff"),
                            FirstName = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff"),
                            LastName = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff"),
                            Email = "test@test.com",
                            Notes = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff"),
                            AdminUserPermissions = new List<AdminUserPermission>
			           		                  	{
			           		                  		new AdminUserPermission
			           		                  			{
			           		                  				Code = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff"),
			           		                  				Description = "TEST",
			           		                  				Grant = true,
			           		                  			}
			           		                  	},
                        };
            adminUser.AdminUserPermissions[0].AdminUser = adminUser;
            adminUser.LoadCollections();
            _view.SaveRecord(adminUser);
            Assert.IsTrue(adminUser.Id != default(long));
            var key = adminUser.Id;
            _view.DeleteRecord(adminUser);
            Assert.IsFalse(new AdminUser(key).KeyLoaded);

        }


        internal class AdminUserView : IAdminUserView
        {

            public AdminUser ActiveSuperUser
            {
                get { return GlobalTestInitializer.ActiveAdminUser; }
            }

            public event EventHandler<ViewEventArgs<AdminUser>> Save;
            public event EventHandler<ViewEventArgs<AdminUser>> Delete;
            public event EventHandler<ViewEventArgs<AdminUser>> LoadAuditLog;

            public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
            {
            }

            public void Set(AdminUser adminUser)
            {
            }

            public void LogException(Exception ex)
            {
                Console.WriteLine(ex);
            }

            public void DisplayMessages(IEnumerable<ValidationMessage> messages)
            {
                messages.PrintMessages();
                Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
            }

            public void Load()
            {
                var handler = new AdminUserHandler(this);
                handler.Initialize();
            }

            public void SaveRecord(AdminUser adminUser)
            {
                if (Save != null)
                    Save(this, new ViewEventArgs<AdminUser>(adminUser));
            }

            public void DeleteRecord(AdminUser adminUser)
            {
                if (Delete != null)
                    Delete(this, new ViewEventArgs<AdminUser>(adminUser));
            }
        }
    }
}
