﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Validation.Administration;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Administration
{
	[TestFixture]
	public class GroupPermissionValidatorTests
	{
		private GroupPermissionValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new GroupPermissionValidator();
		}

		[Test]
		public void DuplicateGroupCodeTest()
		{
			var all = new GroupSearch().FetchGroups(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var group = all.FirstOrDefault(g => g.Permissions.Count > 0);
			if (group == null) return;
			var permission = new GroupPermission
			                 	{
			                 		Code = group.Permissions[0].Code,
			                 		Group = group,
			                 		TenantId = group.TenantId
			                 	};
			Assert.IsTrue(_validator.DuplicateGroupPermission(permission));
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var permission = new GroupPermission();
			Assert.IsFalse(_validator.HasAllRequiredData(permission));
		}

		[Test]
		public void IsValidTest()
		{
			var permission = new GroupPermission
			                 	{
			                 		Code = DateTime.Now.ToString(),
			                 		Description = string.Empty,
			                 		TenantId = GlobalTestInitializer.DefaultTenantId,
									Group = new Group(GlobalTestInitializer.DefaultGroupId, false),
			                 		Modify = true
			                 	};
			Assert.IsTrue(_validator.IsValid(permission));
		}
	}
}
