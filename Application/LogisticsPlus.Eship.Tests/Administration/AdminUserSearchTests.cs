﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Administration
{
    [TestFixture]
    public class AdminUserSearchTests
    {
        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

		[Test]
		public void CanFetchByUserName()
		{
		    var adminUser = new AdminUser(GlobalTestInitializer.DefaultAdminUserId);
			var user = new AdminUserSearch().FetchUserByUsername(adminUser.Username);
			Assert.IsTrue(user != null);
		}

        [Test]
        public void CanFetchAdminUsers()
        {
            var columns = new List<ParameterColumn>();
            var column = AdminSearchFields.FirstName.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            columns.Add(column);
            var adminUsers = new AdminUserSearch().FetchAdminUsers(columns);
            Assert.IsTrue(adminUsers.Any());

        }
    }
}
