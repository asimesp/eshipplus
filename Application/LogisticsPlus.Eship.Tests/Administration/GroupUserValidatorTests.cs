﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Validation.Administration;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Administration
{
	[TestFixture]
	public class GroupUserValidatorTests
	{
		private GroupUserValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new GroupUserValidator();
		}

		[Test]
		public void GroupUserExistsTest()
		{
			var all = new GroupSearch().FetchGroups(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var group = all.FirstOrDefault(g => g.GroupUsers.Count > 0);
			if (group == null) return;
			var groupUser = new GroupUser
			                	{
			                		UserId = group.GroupUsers[0].UserId,
			                		GroupId = group.Id,
			                		TenantId = group.TenantId
			                	};
			Assert.IsTrue(_validator.GroupUserExists(groupUser));
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var permission = new GroupUser();
			Assert.IsFalse(_validator.HasAllRequiredData(permission));
		}
	}
}
