﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Administration;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Administration;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Administration
{
	[TestFixture]
	public class TenantAdminHandlerTests
	{
		private TenantView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new TenantView();
			_view.Load();
		}

		[Test]
		public void CanTenantSaveOrUpdate()
		{
			var all = new TenantSearch().FetchTenants(new List<ParameterColumn>());
			if (all.Count == 0) return;
			var tenant = all[0];
			tenant.LoadCollections();
			_view.LockRecord(tenant);
			tenant.TakeSnapShot();
			var name = tenant.Name;
			tenant.Name = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff");
			_view.SaveRecord(tenant);
			tenant.TakeSnapShot();
			tenant.Name = name;
			_view.SaveRecord(tenant);
			_view.UnLockRecord(tenant);
		}

		internal class TenantView : ITenantAdminView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public List<string> TimeList
			{
				set { }
			}

		    public List<ChargeCode> ChargeCodes
		    {
		        set{}
		    } 

			public List<Country> Countries
			{
				set {}
			}

			public List<ContactType> ContactTypes
			{
				set { }
			}

			public List<PackageType> PackageTypes
			{
				set { }
			}
			

			public List<DocumentTag> DocumentTags
			{
				set { }
			}

			public Dictionary<int, string> MileageEngines
			{
				set { }
			}

		    public Dictionary<int, string> PaymentGatewayTypes { set; private get; }

		    public event EventHandler Loading;

			public event EventHandler<ViewEventArgs<Tenant>> Lock;
			public event EventHandler<ViewEventArgs<Tenant>> UnLock;
			public event EventHandler<ViewEventArgs<Tenant>> LoadAuditLog;
			public event EventHandler<ViewEventArgs<Tenant>> Save;
			public event EventHandler<ViewEventArgs<string>> DefaultSystemUserSearch;

			public void DisplayDefaultSystemUser(User user)
			{
			}

			public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
			{
				
			}

			public void FailedLock(Lock @lock)
			{
				
			}

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages == null || messages.Count(m => m.Type != ValidateMessageType.Information) != 0);
			}

			public void SetId(long id)
			{

			}

			public void Load()
			{
				var handler = new TenantAdminHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(Tenant tenant)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<Tenant>(tenant));
			}

			public void LockRecord(Tenant tenant)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<Tenant>(tenant));
			}

			public void UnLockRecord(Tenant tenant)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<Tenant>(tenant));
			}
		}
	}
}
