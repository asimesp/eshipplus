﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Administration;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Administration;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Administration
{
	[TestFixture]
	public class GroupHandlerTests
	{
		private GroupView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new GroupView();
			_view.Load();
		}

		[Test]
		public void CanGroupSaveOrUpdate()
		{
			var all = new GroupSearch().FetchGroups(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var @group = all[0];
			@group.LoadCollections();
			_view.LockRecord(@group);
			@group.TakeSnapShot();
			var name = @group.Name;
			@group.Name = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff");
			_view.SaveRecord(@group);
			@group.TakeSnapShot();
			@group.Name = name;
			_view.SaveRecord(@group);
			_view.UnLockRecord(@group);
		}

		[Test]
		public void CanGroupSaveAndDelete()
		{
			var @group = new Group
			             	{
								Name = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff"),
			             		Description = "TEST",
								TenantId = _view.ActiveUser.TenantId,
			             		Permissions = new List<GroupPermission>
			             		              	{
			             		              		new GroupPermission
			             		              			{
			             		              				Code = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff"),
			             		              				Description = "TEST",
			             		              				Grant = true,
			             		              				TenantId = _view.ActiveUser.TenantId,
			             		              			}
			             		              	},
			             		GroupUsers = new List<GroupUser>()
			             	};
			@group.Permissions[0].Group = @group;
			_view.SaveRecord(@group);
			Assert.IsTrue(@group.Id != default(long));
			_view.DeleteRecord(@group);
			Assert.IsTrue(!new Group(@group.Id, false).KeyLoaded);

		}

		internal class GroupView : IGroupView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public event EventHandler<ViewEventArgs<Group>> Save;
			public event EventHandler<ViewEventArgs<Group>> Delete;
			public event EventHandler<ViewEventArgs<Group>> Lock;
			public event EventHandler<ViewEventArgs<Group>> UnLock;
			public event EventHandler<ViewEventArgs<Group>> LoadAuditLog;

			public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
			{
				
			}

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Where(m => m.Type != ValidateMessageType.Information).Count() == 0);
			}

			public void FailedLock(Lock @lock)
			{

			}

		    public void Set(Group @group)
		    {
		            
		    }

		    public void Set(long id)
			{
				
			}

			public void Load()
			{
				var handler = new GroupHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(Group @group)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<Group>(@group));
			}

			public void DeleteRecord(Group @group)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<Group>(@group));
			}

			public void LockRecord(Group @group)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<Group>(@group));
			}

			public void UnLockRecord(Group @group)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<Group>(@group));
			}
		}
	}
}
