﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Validation.Administration;
using LogisticsPlus.Eship.Processor.Views.Administration;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Administration
{
	[TestFixture]
	public class UserValidatorTests
	{
		private UserValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new UserValidator();
		}

		[Test]
		public void DuplicateUserNameTest()
		{
            var criteria = new UserSearchCriteria
            {
                Parameters = new List<ParameterColumn>()
            };
			var all = new UserSearch().FetchUsers(criteria, GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var u1 = new User(all[0].Id);
			var user = new User
			           	{
			           		Username = u1.Username,
			           		FirstName = u1.FirstName,
			           		LastName = u1.LastName,
			           		Password = u1.Password,
			           		TenantId = u1.TenantId
			           	};

			Assert.IsTrue(_validator.DuplicateUserName(user));
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var user = new User();
			Assert.IsFalse(_validator.HasAllRequiredData(user));
		}

		[Test]
		public void IsValidTest()
		{
			var user = new User
			{
				Username = DateTime.Now.ToString(),
				FirstName = DateTime.Now.ToString(),
				LastName = DateTime.Now.ToString(),
				Password = DateTime.Now.ToString(),
				TenantId = GlobalTestInitializer.DefaultTenantId,
				CountryId = GlobalTestInitializer.DefaultCountryId,
				DefaultCustomer = new Customer(GlobalTestInitializer.DefaultCustomerId)
			};
			Assert.IsTrue(_validator.IsValid(user));
		}

		[Test]
		public void DuplicateUserShipAsTest()
		{
			var usa = new UserShipAs();
			Assert.IsFalse(_validator.UserShipAsExists(usa));
			usa = new User(GlobalTestInitializer.DefaultUserIdThatHasShipAs).UserShipAs[0];
			Assert.IsTrue(_validator.UserShipAsExists(usa));
		}

		[Test]
		public void CanDeleteTest()
		{
            var user = new User(GlobalTestInitializer.DefaultUserId);
			Assert.IsTrue(user.KeyLoaded);
			_validator.Messages.Clear();
			var deleteUser = _validator.CanDeleteUser(user);
			Assert.IsTrue(_validator.Messages.Count > 0);
			Assert.IsFalse(deleteUser);
			_validator.Messages.PrintMessages();
		}
	}
}
