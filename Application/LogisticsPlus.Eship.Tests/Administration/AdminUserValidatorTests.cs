﻿using System;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Validation.Administration;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Administration
{
    [TestFixture]
    public class AdminUserValidatorTests
    {
        private AdminUserValidator _validator;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _validator = new AdminUserValidator();
        }

        [Test]
        public void DuplicateUserNameTest()
        {
            var defaultAdminUser = new AdminUser(GlobalTestInitializer.DefaultAdminUserId);

            var adminUser = new AdminUser
            {
                Username = defaultAdminUser.Username,
                FirstName = defaultAdminUser.FirstName,
                LastName = defaultAdminUser.LastName,
                Password = defaultAdminUser.Password,
                Email = DateTime.Now.ToShortDateString(),
                Notes = string.Empty,
                Enabled = true
            };

            Assert.IsFalse(_validator.UsernameIsUnique(adminUser));
        }

        [Test]
        public void IsNotValidTest()
        {
            var adminUser = new AdminUser{Username = string.Empty};
            Assert.IsFalse(_validator.IsValid(adminUser));
        }

        [Test]
        public void IsValidTest()
        {
            var adminUser = new AdminUser
            {
                Username = DateTime.Now.ToString(),
                FirstName = DateTime.Now.ToString(),
                LastName = DateTime.Now.ToString(),
                Password = DateTime.Now.ToString(),
                Notes = string.Empty,
                Email = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
                Enabled = true,
            };
            _validator.IsValid(adminUser);
            _validator.Messages.ForEach(m => Console.WriteLine(m.Message));
        }
    }
}
