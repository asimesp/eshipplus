﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Views.Administration;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Administration
{
	[TestFixture]
	public class UserTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanLoadWithoutExceptions()
		{
            var criteria = new UserSearchCriteria
            {
                Parameters = new List<ParameterColumn>()
            };
            var all = new UserSearch().FetchUsers(criteria, GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var user = new User(all[0].Id);
			user.LoadCollections();
			user.RetrieveUsersGroups();
			user.RetrieveCustomersThatUserRepresents();
			user.RetrieveShipAsCustomers();
		}
	}
}
