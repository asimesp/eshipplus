﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Handlers.Administration;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Administration;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Administration
{
    [TestFixture]
    public class ResetTenantUserHandlerTests
    {
        private ResetTenantUserView _view;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _view = new ResetTenantUserView();
            _view.Load();
        }

        [Test]
        public void CanResetTenantFailedLogins()
        {
            var user = new User(GlobalTestInitializer.DefaultUserId);
            var criteria = new TenantUserResetCriteria
                               {
                                   ResetPassword = true,
                                   UserName = user.Username,
                                   TenantCode = user.Tenant.Code
                               };
            var originalPassword = user.Password;
            user.FailedLoginAttempts = 1;
            user.Save();
            _view.TenantFailedLoginsReset(criteria);
            user = new User(GlobalTestInitializer.DefaultUserId);
            Assert.IsTrue(user.FailedLoginAttempts == default(int));
            Assert.IsTrue(user.Password != originalPassword);
            user.Password = originalPassword;
            user.Save();
            Assert.IsTrue(user.Password == originalPassword);

        }


        internal class ResetTenantUserView : IResetTenantUserView
        {
            public AdminUser ActiveSuperUser
            {
                get { return new AdminUser(GlobalTestInitializer.DefaultAdminUserId); }
            }

            public event EventHandler<ViewEventArgs<TenantUserResetCriteria>> Reset;

            public void DisplayMessages(IEnumerable<ValidationMessage> messages)
            {
                messages.PrintMessages();
                Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
            }

            public void LogException(Exception ex)
            {
                Console.WriteLine(ex);
            }

            public void Load()
            {
                var handler = new ResetTenantUserHandler(this);
                handler.Initialize();
            }

            public void TenantFailedLoginsReset(TenantUserResetCriteria criteria)
            {
                if (Reset != null)
                    Reset(this, new ViewEventArgs<TenantUserResetCriteria>(criteria));
            }
        }
    }
}
