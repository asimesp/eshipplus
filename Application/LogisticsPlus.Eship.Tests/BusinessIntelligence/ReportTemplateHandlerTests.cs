﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.BusinessIntelligence
{
    [TestFixture]
    class ReportTemplateHandlerTests
    {
        private ReportTemplatesView _view;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _view = new ReportTemplatesView();
            _view.Load();
        }

        [Test]
        public void CanSaveOrUpdate()
        {
            var all = new ReportTemplateSearch().FetchReportTemplates(new List<ParameterColumn>());
            if (all.Count == 0) return;

            var reportTemplate = all[0];
            reportTemplate.TakeSnapShot();
            var name = reportTemplate.Name;
            reportTemplate.Name = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff");
            _view.SaveRecord(reportTemplate);
            reportTemplate.TakeSnapShot();
            reportTemplate.Name = name;
            _view.SaveRecord(reportTemplate);
        }

        [Test]
        public void CanSaveAndDelete()
        {
            var template = new ReportTemplate
                               {
                                   Name = DateTime.Now.ToString(),
                                   CanFilterByCustomerGroup = true,
                                   CanFilterByVendorGroup = true,
                                   Query = DateTime.Now.ToString(),
                                   DefaultCustomization = DateTime.Now.ToString(),
								   Description = string.Empty,
                               };

            _view.SaveRecord(template);
            Assert.IsTrue(template.Id != default(long));
            _view.DeleteRecord(template);
        }

        internal class ReportTemplatesView : IReportTemplateView
        {
            public AdminUser ActiveSuperUser
            {
                get { return GlobalTestInitializer.ActiveAdminUser; }
            }

            public event EventHandler<ViewEventArgs<ReportTemplate>> Save;
            public event EventHandler<ViewEventArgs<ReportTemplate>> Delete;

            public void LogException(Exception ex)
            {
                    
            }

            public void DisplayMessages(IEnumerable<ValidationMessage> messages)
            {
                messages.PrintMessages();
                Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
            }

            public void Set(ReportTemplate reportTemplate)
            {
                    
            }

            public void Load()
            {
                var handler = new ReportTemplateHandler(this);
                handler.Initialize();
            }

            public void SaveRecord(ReportTemplate reportTemplate)
            {
                if (Save != null)
                    Save(this, new ViewEventArgs<ReportTemplate>(reportTemplate));
            }

            public void DeleteRecord(ReportTemplate reportTemplate)
            {
                if (Delete != null)
                    Delete(this, new ViewEventArgs<ReportTemplate>(reportTemplate));
            }

            public void DisplayTemplate(ReportTemplate reportTemplate)
            {

            }
        }
    }
}

