﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.BusinessIntelligence
{
    class QlikUserHandlerTests
    {
        private QlikUsersView _view;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _view = new QlikUsersView();
            _view.Load();
        }

        [Test]
        public void CanSaveOrUpdate()
        {
            var all = new QlikUserSearch().FetchAllQlikUsers();
            if (all.Count == 0) return;

            var qlikUser = all[0];
            qlikUser.LoadQlikUserAttributes();
            qlikUser.TakeSnapShot();
            var name = qlikUser.Name;
            qlikUser.Name = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff");
            _view.SaveRecord(qlikUser);
            qlikUser.TakeSnapShot();
            qlikUser.Name = name;
            _view.SaveRecord(qlikUser);
        }

        [Test]
        public void CanSaveAndDelete()
        {
            var qlikUser = new QlikUser
            {
                UserId = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff"),
                Name = DateTime.Now.ToString(),
                QlikUserAttributes = new List<QlikUserAttribute>()
            };

            qlikUser.QlikUserAttributes = new List<QlikUserAttribute>
                {
                    new QlikUserAttribute
                        {
                            QlikUser = qlikUser,
                            Type = "Type",
                            Value = "Value"
                        }
                };

            _view.SaveRecord(qlikUser);
            Assert.IsTrue(!qlikUser.IsNew);
            _view.DeleteRecord(qlikUser);
        }

        internal class QlikUsersView : IQlikUserView
        {
            public AdminUser ActiveSuperUser
            {
                get { return GlobalTestInitializer.ActiveAdminUser; }
            }

            public event EventHandler Search;
            public event EventHandler<ViewEventArgs<QlikUser>> Save;
            public event EventHandler<ViewEventArgs<QlikUser>> Delete;
            public event EventHandler<ViewEventArgs<List<QlikUser>>> BatchImport;

            public void LogException(Exception ex)
            {

            }

            public void DisplaySearchResult(List<QlikUser> qlikUsers)
            {
                
            }

            public void DisplayMessages(IEnumerable<ValidationMessage> messages)
            {
                messages.PrintMessages();
                Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
            }

            public void Set(QlikUser qlikUser)
            {

            }

            public void Load()
            {
                var handler = new QlikUserHandler(this);
                handler.Initialize();
            }

            public void SaveRecord(QlikUser qlikUser)
            {
                if (Save != null)
                    Save(this, new ViewEventArgs<QlikUser>(qlikUser));
            }

            public void DeleteRecord(QlikUser qlikUser)
            {
                if (Delete != null)
                    Delete(this, new ViewEventArgs<QlikUser>(qlikUser));
            }

            public void DisplayTemplate(QlikUser qlikUser)
            {

            }
        }
    }
}
