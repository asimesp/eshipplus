﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.BusinessIntelligence
{
    [TestFixture]
   public class VendorGroupSearchTests
    {

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

        [Test]
        public void CanFetchVendorGroupsWithVendorFields()
        {
            var columns = new List<ParameterColumn>();

            var column = BusinessIntelligenceSearchFields.NonAliasedVendorName.ToParameterColumn();
            column.DefaultValue = "Logistics Plus Vendor";
            column.Operator = Operator.Equal;
            columns.Add(column);

            column = BusinessIntelligenceSearchFields.VendorNumber.ToParameterColumn();
            column.DefaultValue = "10000";
            column.Operator = Operator.Equal;
            columns.Add(column);

            var field = BusinessIntelligenceSearchFields.VendorGroups.FirstOrDefault(f => f.Name == "Name");
            if (field == null)
            {
                Console.WriteLine("Business Intelligence search fields no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }

            var column2 = field.ToParameterColumn();
            column2.DefaultValue = SearchUtilities.WildCard;
            column2.Operator = Operator.Contains;
            columns.Add(column2);

            var results = new VendorGroupSearch().FetchVendorGroups(columns, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(results.Any());
        }
    }
}
