﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation.BusinessIntelligence;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.BusinessIntelligence
{
    [TestFixture]
    public class CustomerGroupValidatorTests
    {
        private CustomerGroupValidator _validator;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _validator = new CustomerGroupValidator();
        }

        [Test]
        public void HasAllRequiredDataTest()
        {
            var customerGroup = new CustomerGroup();
            Assert.IsFalse(_validator.HasAllRequiredData(customerGroup));
        }

        [Test]
        public void IsUniqueTest()
        {
			var all = new CustomerGroupSearch().FetchCustomerGroups(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
            if (all.Count == 0) return;

            var customerGroup = all[0];
            Assert.IsTrue(_validator.IsUnique(customerGroup)); // this should be unique

            var newCustomerGroup = new CustomerGroup(1000000, false);
            Assert.IsFalse(newCustomerGroup.KeyLoaded); // will not be found

            newCustomerGroup.TenantId = customerGroup.TenantId;
            newCustomerGroup.Name = customerGroup.Name;
            newCustomerGroup.Description = customerGroup.Description;


            Assert.IsFalse(_validator.IsUnique(newCustomerGroup)); // should be false
        }

        [Test]
        public void MappingIsUniqueTest()
        {
            var all = new CustomerGroupSearch().FetchCustomerGroups(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
            if (all.Count == 0) return;
        	var group = all.FirstOrDefault(g => g.CustomerGroupMaps.Count > 0);
			if (group == null) return;
        	var map = group.CustomerGroupMaps[0];

        	var newCustomerGroupMaps = new CustomerGroupMap
                                           {
                                               TenantId = map.TenantId,
                                               CustomerGroup = map.CustomerGroup,
                                               Customer = map.Customer
                                           };

            Assert.IsTrue(_validator.CustomerGroupMapExists(newCustomerGroupMaps)); // should be false
        }

		[Test]
		public void CanDeleteTest()
		{
			var all = new CustomerGroupSearch().FetchCustomerGroups(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var @group = new CustomerGroup(all[0].Id, false);
			Assert.IsTrue(@group.KeyLoaded);
			_validator.Messages.Clear();
			_validator.CanDeleteCustomerGroup(@group);
			_validator.Messages.PrintMessages();
		}

    }
}
