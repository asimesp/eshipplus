﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Validation.BusinessIntelligence;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.BusinessIntelligence
{
    [TestFixture]
    public class BackgroundReportRunSearchTests
    {

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();

        }

        [Test]
        public void CanFetchBackgroundReportsForUser()
        {
            var results =
                new BackgroundReportRunSearch().FetchBackgroundReportRunRecordsForUser(GlobalTestInitializer.DefaultUserId);
            if (results.Any()) Console.WriteLine("{0} {1}", results[0].ReportName.GetString(), results[0].User.Username);
        }

        [Test]
        public void CanFetchExpiredBackgroundReports()
        {
            var results =
                new BackgroundReportRunSearch().FetchExpiredBackgroundReportRunRecords();
            if (results.Any()) Console.WriteLine("{0} {1}", results[0].ReportName.GetString(), results[0].User.Username);
        }

        [Test]
        public void CanFetchReportsToRun()
        {
            var results =
                new BackgroundReportRunSearch().FetchBackgroundReportsToRun();
            if (results.Any()) Console.WriteLine("{0} {1}", results[0].ReportName.GetString(), results[0].User.Username);
        }
    }
}
