﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.BusinessIntelligence
{
	[TestFixture]
	class ReportScheduleHandlerTests
	{
		private ReportScheduleView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new ReportScheduleView();
			_view.Load();
		}

		[Test]
		public void CanSaveOrUpdate()
		{
            var criteria = new ReportScheduleViewSearchCriteria { ActiveUserId = GlobalTestInitializer.DefaultUserId, };

		    var all = new ReportScheduleSearch().FetchReportSchedules(criteria, GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;

			var reportSchedule = all[0];
			_view.LockRecord(reportSchedule);
			reportSchedule.TakeSnapShot();

			var startDate = reportSchedule.Start;
			reportSchedule.Start = DateTime.Now.AddDays(30);
			_view.SaveRecord(reportSchedule);
			reportSchedule.TakeSnapShot();
			reportSchedule.Start = startDate;
			_view.SaveRecord(reportSchedule);
			_view.UnLockRecord(reportSchedule);
		}

		[Test]
		public void CanSaveAndDelete()
		{
			var configuration = new ReportConfigurationSearch().FetchReportConfigurations(new List<ParameterColumn>(), GlobalTestInitializer.DefaultUserId, GlobalTestInitializer.DefaultTenantId).First();

			var schedule = new ReportSchedule
							   {
								   Enabled = true,
								   Time = "12:00",
								   Start = DateTime.Today,
								   End = DateTime.Today,
								   LastRun = DateTime.Today,
								   ScheduleInterval = ScheduleInterval.Yearly,
								   ReportConfiguration = configuration,
								   NotifyEmails = string.Empty,
								   Notify = false,
								   ExcludeFriday = false,
								   ExcludeMonday = false,
								   ExcludeSaturday = false,
								   ExcludeSunday = false,
								   ExcludeThursday = false,
								   ExcludeTuesday = false,
								   ExcludeWednesday = false,
								   TenantId = GlobalTestInitializer.DefaultTenantId,
								   UserId = GlobalTestInitializer.DefaultUserId,
								   FtpDefaultFolder = string.Empty,
								   FtpEnabled = false,
								   FtpPassword = "ftp password",
								   FtpUrl = "ftp url",
								   FtpUsername = "username",
								   SecureFtp = false
							   };


			_view.SaveRecord(schedule);
			Assert.IsTrue(schedule.Id != default(long));
			_view.DeleteRecord(schedule);
		}

		internal class ReportScheduleView : IReportSchedulerView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public Dictionary<int, string> ScheduleIntervals
			{
				set { }
			}

			public Dictionary<int, string> ExportFileExtension
			{
				set {  }
			}

			public List<string> Time
			{
				set { }
			}


			public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<ReportSchedule>> Lock;
			public event EventHandler<ViewEventArgs<ReportSchedule>> UnLock;
			public event EventHandler<ViewEventArgs<ReportSchedule>> Search;

			public event EventHandler<ViewEventArgs<ReportSchedule>> Save;
			public event EventHandler<ViewEventArgs<ReportSchedule>> Delete;
			public event EventHandler<ViewEventArgs<ReportSchedule>> LoadAuditLog;

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs)
			{
			}

			public void FailedLock(Lock @lock)
			{
			}

			public void Set(ReportSchedule reportConfiguration)
			{
			}

			public void Set(ReportTemplate reportTemplate)
			{

			}

			public void Load()
			{
				var handler = new ReportSchedulerHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(ReportSchedule reportSchedule)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<ReportSchedule>(reportSchedule));
			}

			public void DeleteRecord(ReportSchedule reportSchedule)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<ReportSchedule>(reportSchedule));
			}

			public void LockRecord(ReportSchedule reportSchedule)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<ReportSchedule>(reportSchedule));
			}

			public void UnLockRecord(ReportSchedule reportSchedule)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<ReportSchedule>(reportSchedule));
			}
		}
	}
}

