﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.BusinessIntelligence
{
    [TestFixture]
    class CustomerGroupHandlerTests
    {
        private CustomerGroupView _view;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _view = new CustomerGroupView();
            _view.Load();
        }

        [Test]
        public void CanCustomerGroupSaveOrUpdate()
        {
			var all = new CustomerGroupSearch().FetchCustomerGroups(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
            if (all.Count == 0) return;
            var customerGroup = all[0];
            var customerGroupMaps = customerGroup.CustomerGroupMaps;
            _view.LockRecord(customerGroup);
            customerGroup.TakeSnapShot();
            var name = customerGroup.Name;
            customerGroup.Name = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff");
            _view.SaveRecord(customerGroup);
            customerGroup.TakeSnapShot();
            customerGroup.Name = name;
            customerGroup.CustomerGroupMaps = customerGroupMaps;
            _view.SaveRecord(customerGroup);
            _view.UnLockRecord(customerGroup);
        }

        [Test]
        public void CanGroupSaveAndDelete()
        {
			var customers = new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), GlobalTestInitializer.DefaultTenantId);
        	if (customers.Count == 0) return;

        	var customerGroup = new CustomerGroup
        	                    	{
        	                    		TenantId = _view.ActiveUser.TenantId,
        	                    		Name = DateTime.Now.ToString(),
        	                    		Description = DateTime.Now.ToString(),
        	                    		CustomerGroupMaps = new List<CustomerGroupMap>()
        	                    	};
        	customerGroup.CustomerGroupMaps.Add(new CustomerGroupMap
        	                                    	{
        	                                    		CustomerGroup = customerGroup,
        	                                    		Customer = customers[0],
        	                                    		TenantId = GlobalTestInitializer.DefaultTenantId,
        	                                    	});

        	_view.SaveRecord(customerGroup);
            Assert.IsTrue(customerGroup.Id != default(long));
            _view.DeleteRecord(customerGroup);
        }

        internal class CustomerGroupView : ICustomerGroupView
        {
            public User ActiveUser
            {
                get { return GlobalTestInitializer.ActiveUser; }
            }

            public event EventHandler Loading;
            public event EventHandler<ViewEventArgs<CustomerGroup>> Save;
            public event EventHandler<ViewEventArgs<CustomerGroup>> Delete;
            public event EventHandler<ViewEventArgs<CustomerGroup>> Lock;
            public event EventHandler<ViewEventArgs<CustomerGroup>> UnLock;
            public event EventHandler<ViewEventArgs<CustomerGroup>> LoadAuditLog;

			public void Set(CustomerGroup @group)
        	{
        		
        	}

            public void LogException(Exception ex)
            {
                    
            }

            public void DisplayMessages(IEnumerable<ValidationMessage> messages)
            {
                messages.PrintMessages();
                Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
            }

            public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
            {
                
            }

            public void FailedLock(Lock @lock)
            {

            }


            public void Load()
            {
                var handler = new CustomerGroupHandler(this);
                handler.Initialize();
            }

            public void SaveRecord(CustomerGroup customerGroup)
            {
                if (Save != null)
                    Save(this, new ViewEventArgs<CustomerGroup>(customerGroup));
            }

            public void DeleteRecord(CustomerGroup customerGroup)
            {
                if (Delete != null)
                    Delete(this, new ViewEventArgs<CustomerGroup>(customerGroup));
            }

            public void LockRecord(CustomerGroup customerGroup)
            {
                if (Lock != null)
                    Lock(this, new ViewEventArgs<CustomerGroup>(customerGroup));
            }

            public void UnLockRecord(CustomerGroup customerGroup)
            {
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<CustomerGroup>(customerGroup));
            }
        }
    }
}
