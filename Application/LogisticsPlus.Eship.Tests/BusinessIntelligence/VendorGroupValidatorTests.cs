﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation.BusinessIntelligence;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.BusinessIntelligence
{
    [TestFixture]
    public class VendorGroupValidatorTests
    {
        private VendorGroupValidator _validator;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _validator = new VendorGroupValidator();
        }

        [Test]
        public void HasAllRequiredDataTest()
        {
            var vendorGroup = new VendorGroup();
            Assert.IsFalse(_validator.HasAllRequiredData(vendorGroup));
        }

        [Test]
        public void IsUniqueTest()
        {
			var all = new VendorGroupSearch().FetchVendorGroups(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
            if (all.Count == 0) return;

            var vendorGroup = all[0];
            Assert.IsTrue(_validator.IsUnique(vendorGroup)); // this should be unique

            var newVendorGroup = new VendorGroup(1000000, false);
            Assert.IsFalse(newVendorGroup.KeyLoaded); // will not be found

            newVendorGroup.TenantId = vendorGroup.TenantId;
            newVendorGroup.Name = vendorGroup.Name;
            newVendorGroup.Description = vendorGroup.Description;


            Assert.IsFalse(_validator.IsUnique(newVendorGroup)); // should be false
        }

        [Test]
        public void MappingIsUniqueTest()
        {
			var all = new VendorGroupSearch().FetchVendorGroups(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
            if (all.Count == 0) return;
        	var group = all.FirstOrDefault(g => g.VendorGroupMaps.Count > 0);
			if (group == null) return;
        	var map = group.VendorGroupMaps[0];

        	var newVendorGroupMaps = new VendorGroupMap
                                           {
                                               TenantId = map.TenantId,
                                               VendorGroup = map.VendorGroup,
                                               Vendor = map.Vendor
                                           };

            Assert.IsTrue(_validator.VendorGroupMapExists(newVendorGroupMaps)); // should be false
        }

		[Test]
		public void CanDeleteTest()
		{
			var all = new VendorGroupSearch().FetchVendorGroups(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var @group = new VendorGroup(all[0].Id, false);
			Assert.IsTrue(@group.KeyLoaded);
			_validator.Messages.Clear();
			_validator.CanDeleteVendorGroup(@group);
			_validator.Messages.PrintMessages();
		}
    }
}
