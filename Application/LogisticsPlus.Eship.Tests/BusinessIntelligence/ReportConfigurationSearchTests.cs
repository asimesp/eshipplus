﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.BusinessIntelligence
{
    [TestFixture]
   public class ReportConfigurationSearchTests
    {

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

		[Test]
		public void CanFetchByUser()
		{
			new ReportConfigurationSearch().FetchReportConfigurationsVisibleToUser(GlobalTestInitializer.DefaultUserId, GlobalTestInitializer.ActiveUser.TenantId);
		}

        [Test]
        public void CanFetchWithUsername()
        {
            var columns = new List<ParameterColumn>();

            for (var i = 0; i < 3; i++)
            {
                columns.Add(BusinessIntelligenceSearchFields.Username.ToParameterColumn());
            }

            columns[0].DefaultValue = "kyle";
            columns[0].Operator = Operator.Contains;

            columns[1].DefaultValue = "chris";
            columns[1].Operator = Operator.Contains;

            columns[2].DefaultValue = "deb";
            columns[2].Operator = Operator.Contains;

            var field = BusinessIntelligenceSearchFields.ReportConfigurations.FirstOrDefault(f => f.Name == "Name");
            if (field == null)
            {
                Console.WriteLine("BusinessInteligenceSearchFields.ReportConfigurations no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }

            var column = field.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            columns.Add(column);

            var results = new ReportConfigurationSearch().FetchReportConfigurations(columns, GlobalTestInitializer.DefaultUserId, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(results.Any());

        }

    }
}
