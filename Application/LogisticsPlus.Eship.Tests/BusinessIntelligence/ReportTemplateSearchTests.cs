﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.BusinessIntelligence
{
    [TestFixture]
    public class ReportTemplateSearchTests
    {

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

        [Test]
        public void CanFetchWithParameterColumns()
        {
            var columns = new List<ParameterColumn>();
            var field = BusinessIntelligenceSearchFields.ReportTemplates.FirstOrDefault(f => f.Name == "Name");
            if (field == null)
            {
                Console.WriteLine("BusinessIntelligenceSearchFields.ReportTemplates no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }
            columns.Add(new ParameterColumn { DefaultValue = SearchUtilities.WildCard, ReportColumnName = field.DisplayName, Operator = Operator.Contains });

            var templates = new ReportTemplateSearch().FetchReportTemplates(columns);

            Assert.IsTrue(templates.Any());
        }
    }
}
