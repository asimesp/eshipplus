﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.BusinessIntelligence
{
	[TestFixture]
	class VendorGroupHandlerTests
	{
		private VendorGroupView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new VendorGroupView();
			_view.Load();
		}

		[Test]
		public void CanGroupSaveOrUpdate()
		{
			var all = new VendorGroupSearch().FetchVendorGroups(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var vendorGroup = all[0];
			var vendorGroupMaps = vendorGroup.VendorGroupMaps;
			_view.LockRecord(vendorGroup);
			vendorGroup.TakeSnapShot();
			var name = vendorGroup.Name;
			vendorGroup.Name = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff");
			_view.SaveRecord(vendorGroup);
			vendorGroup.TakeSnapShot();
			vendorGroup.Name = name;
			vendorGroup.VendorGroupMaps = vendorGroupMaps;
			_view.SaveRecord(vendorGroup);
			_view.UnLockRecord(vendorGroup);
		}

		[Test]
		public void CanGroupSaveAndDelete()
		{
			var vendors = new VendorSearch().FetchVendors(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (vendors.Count == 0) return;

			var vendorGroup = new VendorGroup
								{
									TenantId = _view.ActiveUser.TenantId,
									Name = DateTime.Now.ToString(),
									Description = DateTime.Now.ToString(),
									VendorGroupMaps = new List<VendorGroupMap>()
								};
			vendorGroup.VendorGroupMaps.Add(new VendorGroupMap
												{
													VendorGroup = vendorGroup,
													Vendor = vendors[0],
													TenantId = GlobalTestInitializer.DefaultTenantId,
												});

			_view.SaveRecord(vendorGroup);
			Assert.IsTrue(vendorGroup.Id != default(long));
			_view.DeleteRecord(vendorGroup);
		}

		internal class VendorGroupView : IVendorGroupView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<VendorGroup>> Save;
			public event EventHandler<ViewEventArgs<VendorGroup>> Delete;
			public event EventHandler<ViewEventArgs<VendorGroup>> Lock;
			public event EventHandler<ViewEventArgs<VendorGroup>> UnLock;
			public event EventHandler<ViewEventArgs<VendorGroup>> LoadAuditLog;

			public void Set(VendorGroup @group)
			{

			}

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
			{
			}

			public void FailedLock(Lock @lock)
			{

			}

			public void Load()
			{
				var handler = new VendorGroupHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(VendorGroup vendorGroup)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<VendorGroup>(vendorGroup));
			}

			public void DeleteRecord(VendorGroup vendorGroup)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<VendorGroup>(vendorGroup));
			}

			public void LockRecord(VendorGroup vendorGroup)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<VendorGroup>(vendorGroup));
			}

			public void UnLockRecord(VendorGroup vendorGroup)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<VendorGroup>(vendorGroup));
			}
		}
	}
}
