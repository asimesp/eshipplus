﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation.BusinessIntelligence;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.BusinessIntelligence
{
    [TestFixture]
    public class ReportTemplateValidatorTests
    {
        private ReportTemplateValidator _validator;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _validator = new ReportTemplateValidator();
        }

        [Test]
        public void HasAllRequiredDataTest()
        {
            var reportTemplate = new ReportTemplate
                                     {
                                         Name = DateTime.Now.ToString(),
                                         Description = DateTime.Now.ToString(),
                                         CanFilterByCustomerGroup = true,
                                         CanFilterByVendorGroup = false,
                                         Query = DateTime.Now.ToString(),
                                         DefaultCustomization = DateTime.Now.ToString()
                                     };

            Assert.IsTrue(_validator.HasAllRequiredData(reportTemplate));
        }

		[Test]
		public void CanDeleteTest()
		{
			var configs = new ReportConfigurationSearch().FetchReportConfigurations(new List<ParameterColumn>(), GlobalTestInitializer.DefaultUserId, GlobalTestInitializer.DefaultTenantId);
			if (configs.All(i => i.ReportTemplate == null)) return;
			var template = configs.First(i => i.ReportTemplate != null).ReportTemplate;
			_validator.Messages.Clear();
			var deleteTemplate = _validator.CanDeleteTemplate(template);
			Assert.IsTrue(_validator.Messages.Count > 0);
			Assert.IsFalse(deleteTemplate);
			_validator.Messages.PrintMessages();
		}
    }
}
