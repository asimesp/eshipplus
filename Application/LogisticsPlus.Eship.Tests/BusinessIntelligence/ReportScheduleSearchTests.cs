﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.BusinessIntelligence
{
    [TestFixture]
   public class ReportScheduleSearchTests
    {

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();

        }

        [Test]
        public void CanFetchByWildCard()
        {
            var columns = new List<ParameterColumn>();

            for (var i = 0; i < 3; i++)
            {
                columns.Add(BusinessIntelligenceSearchFields.ReportScheduleUsername.ToParameterColumn());
            }

            columns[0].DefaultValue = "rick";
            columns[0].Operator = Operator.Contains;

            columns[1].DefaultValue = "chris";
            columns[1].Operator = Operator.Contains;

            columns[2].DefaultValue = "deb";
            columns[2].Operator = Operator.Contains;

            var field = BusinessIntelligenceSearchFields.ReportConfigurations.FirstOrDefault(f => f.Name == "Name");
            if (field == null)
            {
                Console.WriteLine("BusinessInteligenceSearchFields.ReportConfigurations no longer contains the search field this test relies on");
                Assert.IsNotNull(field); //fail the test, print the message
            }

            var column = field.ToParameterColumn();
            column.DefaultValue = SearchUtilities.WildCard;
            column.Operator = Operator.Contains;
            columns.Add(column);

            var reports = new ReportScheduleViewSearchCriteria()
                {
                    Parameters = columns
                };

            var results = new ReportScheduleSearch().FetchReportSchedules(reports, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(results.Any());
        }

		[Test]
		public void FetchReportScheduleForRun()
		{
			var results = new ReportScheduleSearch().FetchReportScheduleForRun(DateTime.Now);
			if(results.Any()) Console.WriteLine("{0} {1}", results[0].ScheduleInterval.GetString(), results[0].ReportConfiguration.Name);
		}
    }
}
