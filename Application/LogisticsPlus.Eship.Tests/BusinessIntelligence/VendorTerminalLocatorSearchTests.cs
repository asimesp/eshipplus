﻿using LogisticsPlus.Eship.Processor.Dto.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.BusinessIntelligence
{
    [TestFixture]
    public class VendorTerminalLocatorSearchTests
    {

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

        [Test]
        public void CanFetchTerminals()
        {
            var criteria = new VendorTerminalLocatorSearchCriteria
                               {
                                   PostalCode = "16502",
                                   Radius = 10,
                                   CountryId = GlobalTestInitializer.DefaultCountryId,
								   UseKilometers = false
                               };

		   new VendorTerminalLocatorDto().FetchVendorTerminals(criteria, GlobalTestInitializer.DefaultTenantId);
           
        }
    }
}
