﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Views.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.BusinessIntelligence
{
	[TestFixture]
	public class VendorPerformanceTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchVendorPerformanceSummaryTest()
		{
			var criteria = new VendorPerformanceSummaryCriteria
			               	{
			               		TenantId = GlobalTestInitializer.DefaultTenantId,
								UserId = GlobalTestInitializer.DefaultUserId,
			               		CustomerNumber = new Customer(GlobalTestInitializer.DefaultCustomerId).CustomerNumber,
								VendorNumber = string.Empty,
			               		StartShipmentDateCreated = DateUtility.SystemEarliestDateTime,
			               		EndShipmentDateCreated = DateTime.Now,
			               		Mode = ServiceMode.NotApplicable
			               	};
			var summaries = new VendorPerformanceSummaryDto().Fetch(criteria);
			const string format = "{0}\t{1}\t{2}\t{3:n0}\t{4:n0}\t{5:n2}\t{6:n2}\t{7:n2}\t{8:n2}\t{9:c2}\t{10:n2}";
			foreach (var s in summaries)
				Console.WriteLine(format, s.Name, s.VendorNumber, s.Scac, s.TotalShipments, s.TotalNonExcludedShipments,
				                  s.PercentOnTimeOverall, s.PercentOnTimePickup, s.PercentOnTimeDelivery, s.PercentAcceptedTenders,
				                  s.TotalAmountPaid, s.TotalMileage);
		}

        [Test]
        public void CanRetrieveVendorStatisticsPerformanceShipmentDtos()
        {
            var shipmentsToGrabVendorFrom = new ShipmentSearch().FetchShipmentsDashboardDto(new ShipmentViewSearchCriteria{ActiveUserId = GlobalTestInitializer.DefaultUserId}, GlobalTestInitializer.DefaultTenantId);
            var vendor = shipmentsToGrabVendorFrom.Any() ? new Vendor(shipmentsToGrabVendorFrom.First().VendorId) : new Vendor();
            var shipments = new VendorStatisticsPerformanceShipmentDto().RetrieveVendorStatisticsPerformanceShipmentDtos(
                    GlobalTestInitializer.DefaultTenantId, vendor.VendorNumber, DateUtility.SystemEarliestDateTime,
                    DateTime.Now, GlobalTestInitializer.DefaultUserId);
            
            Assert.IsTrue(shipments.Any());
        }
	}
}
