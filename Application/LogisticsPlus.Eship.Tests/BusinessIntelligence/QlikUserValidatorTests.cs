﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation.BusinessIntelligence;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.BusinessIntelligence
{
    [TestFixture]
    public class QlikUserValidatorTests
    {
        private QlikUserValidator _validator;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _validator = new QlikUserValidator();
        }

        [Test]
        public void HasAllRequiredDataTest()
        {
            var qlikUser = new QlikUser
            {
                UserId = DateTime.Now.ToString("{0: yyyyMMdd HHmmss}"),
                Name = DateTime.Now.ToString(),
            };

            Assert.IsTrue(_validator.HasAllRequiredData(qlikUser));
        }

        [Test]
        public void IsDuplicateUserId()
        {
            var qlikUsers = new QlikUserSearch().FetchAllQlikUsers();
            if (!qlikUsers.Any()) return;
            var qlikUser = new QlikUser
                {
                    UserId = qlikUsers[0].UserId,
                    Name = qlikUsers[0].Name
                };
            _validator.Messages.Clear();
            var duplicateUserId = _validator.DuplicateUserId(qlikUser);
            Assert.IsTrue(_validator.Messages.Count > 0);
            Assert.IsTrue(duplicateUserId);
            _validator.Messages.PrintMessages();
        }
    }
}
