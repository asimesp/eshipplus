﻿using System.Linq;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.BusinessIntelligence
{
        [TestFixture]
        public class QlikUserSearchTests
        {

            [SetUp]
            public void Initialize()
            {
                if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            }

            [Test]
            public void CanFetchAllQlikUsers()
            {
                var qlikUsers = new QlikUserSearch().FetchAllQlikUsers();
                Assert.IsTrue(qlikUsers.Any());
            }

            [Test]
            public void CanFetchAllDistinctQlikUserAttributeTypes()
            {
                var qlikUserAttributeTypes = new QlikUserSearch().FetchAllDistinctQlikUserAttributeTypes(SearchUtilities.WildCard);
                Assert.IsTrue(qlikUserAttributeTypes.Any());
            }

            [Test]
            public void CanFetchAllDistinctQlikUserAttributeValues()
            {
                var qlikUserAttributeValues = new QlikUserSearch().FetchAllDistinctQlikUserAttributeValues(SearchUtilities.WildCard,"AccessCode");
                Assert.IsTrue(qlikUserAttributeValues.Any());
            }
    }
}
