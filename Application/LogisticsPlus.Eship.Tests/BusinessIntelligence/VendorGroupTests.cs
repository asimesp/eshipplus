﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.BusinessIntelligence
{
	[TestFixture]
	public class VendorGroupTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanRetreiveVendorGroupCustomers()
		{
			var all = new VendorGroupSearch().FetchVendorGroups(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			all[0].RetrieveGroupVendors();
		}
	}
}
