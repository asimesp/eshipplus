﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation.BusinessIntelligence;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.BusinessIntelligence
{
	[TestFixture]
	public class ReportScheduleValidatorTests
	{
		private ReportScheduleValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new ReportScheduleValidator();
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var configuration = new ReportConfigurationSearch().FetchReportConfigurations(new List<ParameterColumn>(), GlobalTestInitializer.DefaultUserId, GlobalTestInitializer.DefaultTenantId).First();

			var schedule = new ReportSchedule
			{
				Enabled = true,
				Time = "12:00",
				Start = DateTime.Today,
				End = DateTime.Today,
				LastRun = DateTime.Today,
				ScheduleInterval = ScheduleInterval.Yearly,
				ReportConfiguration = configuration,
				NotifyEmails = string.Empty,
				Notify = false,
				ExcludeFriday = false,
				ExcludeMonday = false,
				ExcludeSaturday = false,
				ExcludeSunday = false,
				ExcludeThursday = false,
				ExcludeTuesday = false,
				ExcludeWednesday = false,
				TenantId = GlobalTestInitializer.DefaultTenantId,
				UserId = GlobalTestInitializer.DefaultUserId,
				Extension = ReportExportExtension.txt,
				FtpDefaultFolder = string.Empty,
				FtpEnabled = false,
				FtpPassword = "ftp password",
				FtpUrl = "ftp url",
				FtpUsername = "username",
				SecureFtp = false
			};

			Assert.IsTrue(_validator.HasAllRequiredData(schedule));
		}
	}
}
