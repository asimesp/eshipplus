﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation.BusinessIntelligence;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.BusinessIntelligence
{
	[TestFixture]
	public class ReportConfigurationValidatorTests
	{
		private ReportConfigurationValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new ReportConfigurationValidator();
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var reportTemplate = new ReportTemplateSearch().FetchReportTemplates(new List<ParameterColumn>()).First();

			var reportConfiguration = new ReportConfiguration
									 {
										 Name = DateTime.Now.ToString(),
										 Description = DateTime.Now.ToString(),
										 ReportTemplate = reportTemplate,
										 SelectUsers = new List<ReportCustomizationUser>(),
									     SelectGroups = new List<ReportCustomizationGroup>(),
                                         Visibility = ReportConfigurationVisibility.AllUsers,
										 SerializedCustomization = " ",
										 TenantId = GlobalTestInitializer.DefaultTenantId, 
										 User = GlobalTestInitializer.ActiveUser
									 };

			Assert.IsTrue(_validator.HasAllRequiredData(reportConfiguration));

			reportConfiguration.QlikConfiguration = true;
			reportConfiguration.ReportTemplate = null;
			

			Assert.IsFalse(_validator.HasAllRequiredData(reportConfiguration));

			reportConfiguration.QlikSheets = new List<QlikSheet> {new QlikSheet {Name = "Test", Link = "My Link test"}};

			Assert.IsTrue(_validator.HasAllRequiredData(reportConfiguration));
		}
	}
}
