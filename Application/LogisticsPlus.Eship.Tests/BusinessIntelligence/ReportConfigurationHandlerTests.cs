﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.BusinessIntelligence
{
	[TestFixture]
	class ReportConfigurationHandlerTests
	{
		private ReportConfigurationView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new ReportConfigurationView();
			_view.Load();
		}

		[Test]
		public void CanSaveOrUpdate()
		{
			var all = new ReportConfigurationSearch().FetchReportConfigurations(new List<ParameterColumn>(), GlobalTestInitializer.DefaultUserId, GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;

			var reportConfiguration = all[0];
			reportConfiguration.LoadCollections();
			_view.LockRecord(reportConfiguration);
			reportConfiguration.TakeSnapShot();
			
			var name = reportConfiguration.Name;
			reportConfiguration.Name = DateTime.Now.ToString("yyyyMMdd_hhmmss_ffff");
			_view.SaveRecord(reportConfiguration);
			reportConfiguration.TakeSnapShot();
			reportConfiguration.Name = name;
			_view.SaveRecord(reportConfiguration);
			_view.UnLockRecord(reportConfiguration);
		}

		[Test]
		public void CanSaveAndDelete()
		{
			//TODO: add use save delete, add sheet save delete

			var template = new ReportTemplateSearch().FetchReportTemplates(new List<ParameterColumn>()).First();

			var configuration = new ReportConfiguration
							   {
								   Name = DateTime.Now.ToString(),
								   Description = "TEST",
								   ReportTemplate = template,
								   TenantId = GlobalTestInitializer.DefaultTenantId,
								   SelectUsers = new List<ReportCustomizationUser>(),
							       SelectGroups = new List<ReportCustomizationGroup>(),
                                   QlikConfiguration = false,
								   QlikSheets = new List<QlikSheet>(),
								   SerializedCustomization = "",
								   Visibility = ReportConfigurationVisibility.AllUsers,
								   User = GlobalTestInitializer.ActiveUser,
                                   LastRun = DateUtility.SystemEarliestDateTime
							   };

			configuration.LoadCollections();

			_view.SaveRecord(configuration);
			Assert.IsTrue(configuration.Id != default(long));
			_view.DeleteRecord(configuration);
		}

		internal class ReportConfigurationView : IReportConfigurationView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public Dictionary<int, string> SqlDbTypes
			{
				set { }
			}

			public Dictionary<int, string> ReportConfigurationVisibilities
			{
				set { }
			}

			public Dictionary<int, string> Operators
			{
				set { }
			}

			public Dictionary<int, string> ReportChartTypes
			{
				set { }
			}

			public Dictionary<int, string> SortDirections
			{
				set { }
			}


            public Dictionary<int, string> ChartDataSourceTypes
            {
                set { }
            }
			public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<ReportConfiguration>> Lock;
			public event EventHandler<ViewEventArgs<ReportConfiguration>> UnLock;
			public event EventHandler<ViewEventArgs<ReportConfiguration>> Search;

			public event EventHandler<ViewEventArgs<ReportConfiguration>> Save;
			public event EventHandler<ViewEventArgs<ReportConfiguration>> Delete;
			public event EventHandler<ViewEventArgs<ReportConfiguration>> LoadAuditLog;

		    public void LogException(Exception ex)
		    {
		            
		    }

		    public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs)
			{
			}

			public void FailedLock(Lock @lock)
			{
			}

			public void Set(ReportConfiguration reportConfiguration)
			{
			}

			public void Set(ReportTemplate reportTemplate)
			{

			}

			public void Load()
			{
				var handler = new ReportConfigurationHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(ReportConfiguration reportConfiguration)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<ReportConfiguration>(reportConfiguration));
			}

			public void DeleteRecord(ReportConfiguration reportConfiguration)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<ReportConfiguration>(reportConfiguration));
			}

			public void LockRecord(ReportConfiguration reportConfiguration)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<ReportConfiguration>(reportConfiguration));
			}

			public void UnLockRecord(ReportConfiguration reportConfiguration)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<ReportConfiguration>(reportConfiguration));
			}
		}
	}
}

