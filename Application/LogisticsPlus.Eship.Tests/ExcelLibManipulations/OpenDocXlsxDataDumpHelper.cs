﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Ionic.Zip;
using LogisticsPlus.Eship.Processor;

namespace LogisticsPlus.Eship.Tests.ExcelLibManipulations
{
	public class OpenDocXlsxDataDumpHelper
	{
		private readonly string _filename;
		private readonly string _tempFolder;

		public OpenDocXlsxDataDumpHelper(string fullyQualifiedFileName, string fullyQualifiedTempFolder)
		{
			_filename = fullyQualifiedFileName;
			_tempFolder = fullyQualifiedTempFolder;
		}

		/// <summary>
		/// Write table data out to prebuilt sheet with table headers already present in last data row of sheet 1
		/// </summary>
		/// <param name="table"></param>
		public void WriteData(DataTable table = null)
		{
			var info = new FileInfo(_filename);
			var newFile = Path.Combine(_tempFolder, info.Name.Replace(info.Extension, string.Empty) + ".zip");

			if (File.Exists(newFile)) File.Delete(newFile);

			info.CopyTo(newFile);

			using (var zip = new ZipFile(newFile))
			{
				// find data worksheet
				var sheet1 = zip.Entries.First(i => i.FileName.Contains("worksheets/sheet1.xml"));
				var entries = zip.SelectEntries(string.Format("name={0}", sheet1.FileName));
				var zipEntry = entries.First();
				zipEntry.Extract(_tempFolder, ExtractExistingFileAction.OverwriteSilently);

				// load sheet content
				string content;
				var sheetFile = Path.Combine(_tempFolder, "xl\\worksheets\\sheet1.xml");
				using (var stream = new FileStream(sheetFile, FileMode.Open))
				using (var reader = new StreamReader(stream))
					content = reader.ReadToEnd();

				// find last row.  Data will be written after last row!!!
				var rowStart = content.LastIndexOf("<row", StringComparison.Ordinal);
				var rowEnd = content.IndexOf("</row>", rowStart, StringComparison.Ordinal) + "</row>".Length;

				var rowTemplate = content.Substring(rowStart, rowEnd - rowStart);

				var contentHeaderSection = content.Substring(0, rowEnd);
				var contentFooterSection = content.Substring(contentHeaderSection.Length);


				

				var startIdx = rowTemplate.IndexOf(" r=\"", StringComparison.Ordinal);
				var endIdx = rowTemplate.IndexOf("\" ", startIdx, StringComparison.Ordinal);
				var rowCntSub = rowTemplate.Substring(startIdx, endIdx - startIdx + "\" ".Length);

				var rowNumber = rowCntSub.Replace("r=\"", string.Empty).Replace("\"", string.Empty).Trim().ToInt();

				var rowh = "<row r=\"{0}\" spans=\"1:{1}\">";
				var col = "<c r=\"{0}{1}\" t=\"inlineStr\"><is><t>{2}</t></is></c>";
				var rowf = "</row>";

				// write data
				var rows = new List<string>();

				var rowCnt = 5; // row count
				var colCnt = 13; // column count

				for (var i = 0; i < rowCnt; i++)
				{
					var row = string.Format(rowh, ++rowNumber, rowCnt);

					var cols = string.Empty;
					for (var k = 0; k < colCnt; k++)
					{
						// get column prefix
						var divisible = k;
						var reduce = divisible > 25; // pre-first boundary break; account for excel crazy number scheme!
						var charStack = new Stack<char>();
						do
						{
							var remainder = divisible % 26;
							divisible = (divisible / 26);

							charStack.Push((char)(remainder + 'A'));

						} while (divisible > 0);
						if (reduce) charStack.Push((char)(charStack.Pop() - 1));

						// build column
						cols += string.Format(col, new string(charStack.ToArray()), rowNumber, DateTime.Now.AddDays(i).ToString("yyyyMMddmmHHssffff"));
					}

					row += cols + rowf;

					rows.Add(row);
				}

				

				content = contentHeaderSection + rows.ToArray().NewLineJoin() + contentFooterSection;

				// update entry
				zip.UpdateEntry(zipEntry.FileName, content);
				zip.Save();

				// delete temp folder
				File.Delete(sheetFile);
				var dir = _tempFolder + "xl\\";
				var files = Directory.GetFiles(dir, "*.*", SearchOption.AllDirectories);
				foreach (var file in files) File.Delete(file);
				Directory.Delete(dir, true);

			}

			//return file and clean up
			var nInfo = new FileInfo(newFile);
			nInfo.CopyTo(_filename, true);
			nInfo.Delete();
		}
	}
}
