﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using LogisticsPlus.Eship.Fax;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Core.Connect;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Fax
{
    [TestFixture]
    [Ignore("Fax Tests. Run on demand only")]
    public class FaxTests
    {
        private FaxHandler _handler;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _handler = new FaxHandler(new FaxSettings
                {
                    RingCentralUser = "+13106261343",
					RingCentralUserPassword = "1406LogPlus",
					RingCentralBaseUrl = "https://platform.devtest.ringcentral.com",
					RingCentralAppKey = "jH2dyHsUSOOC-PlrBKtgYw",
					RingCentralUserExtension = "101",
					RingCentralAppSecret = "44M678BoQRSxv4Y0xj6ROgl03iVqsESsiCWjxJlm1snQ",
					InterfaxUser = "LPInterfax",
					InterfaxUserPassword = "LP1nterfax",
                });
        }

        [Test]
        public void CanSendFax()
        {
			//var file = new FileInfo("C:\\Projects\\11024.pdf");
			var file = new FileInfo("C:\\Users\\christopher.oyesiku\\Desktop\\dump\\eShipPlus_TMS_4.2.2.9_Rate_And_Schedule_P" +
			                        "reauthorization.pdf");
            Assert.IsTrue(file.Exists);
            var fax = new FaxWrapper
                {
                    CoverPage = new CoverPage { Text = string.Empty, Type = CoverPageType.None },
                    RecipientFaxNumbers = new List<string> { "18142863590" },
                    Resolution = Resolution.High,
                    SendTime = string.Empty,
                    Attachments = new List<FaxDocument>
						{
							new FaxDocument
								{
									Name = file.Name,
									Content = new byte[file.Length]
								}
						}
                };
            file.OpenRead().Read(fax.Attachments[0].Content, 0, file.Length.ToInt());

            var faxId = _handler.Send(fax, ServiceProvider.RingCentral);
            Console.WriteLine(_handler.LastMessage);
            Assert.IsTrue(!string.IsNullOrEmpty(faxId));
        }

		[Test]
		public void CanSendInterfaxFax()
		{
			var file = new FileInfo("C:\\Users\\Zach.Demmel\\Desktop\\dump\\Bill_of_Lading_Template.pdf");
			Assert.IsTrue(file.Exists);
			var fax = new FaxWrapper
				{
					CoverPage = new CoverPage { Text = string.Empty, Type = CoverPageType.None },
					RecipientFaxNumbers = new List<string> { "18142863590" },
					Resolution = Resolution.High,
					SendTime = string.Empty,
					Attachments = new List<FaxDocument>
						{
							new FaxDocument
								{
									Name = file.Name,
									Content = new byte[file.Length]
								}
						}
				};
			file.OpenRead().Read(fax.Attachments[0].Content, 0, file.Length.ToInt());

			
			var faxId = _handler.Send(fax, ServiceProvider.Interfax);
			Console.WriteLine(faxId);
			Console.WriteLine(_handler.LastMessage);
			Assert.IsTrue(!string.IsNullOrEmpty(faxId));
			var faxIdInt = faxId.ToInt();
			Assert.IsTrue(faxIdInt > 0);
			 
		}

        [Test]
        public void CanRetrieveFaxEmails()
        {
            Exception ex;
            var status = _handler.RetrieveFaxStatus(out ex, "1403442004", ServiceProvider.RingCentral);
            if (ex != null) Console.WriteLine(ex.ToString());
            if (status == null) return;
            Console.WriteLine();
            Console.WriteLine("Fax Number: {0}", status.FaxNumber);
            Console.WriteLine("Send Result: {0}", status.SendResult.ToString());
            Console.WriteLine(string.Empty);
        }

		[Test]
		public void CanRetrieveInterfaxFaxEmails()
		{
			Exception ex;
			var status = _handler.RetrieveFaxStatus(out ex, "643422707", ServiceProvider.Interfax);
			if (ex != null) Console.Write(ex.ToString());
			if (status == null) return;
			Console.WriteLine();
			Console.WriteLine("Fax Number: {0}", status.FaxNumber);
			Console.WriteLine("Send Result: {0}", status.SendResult.ToString());
			Console.WriteLine(string.Empty);
		}

        [Test]
		[Ignore("Test was designed for the sole purpose of passing eShipPlus for production in the Ring Central system")]
        public void CanCheatTestForRingCentral()
        {
            // set 1
            Exception ex;
            for (var i = 0; i < 60; i++)
            {
                var status = _handler.RetrieveFaxStatus(out ex, "1403442004", ServiceProvider.RingCentral);

                if (ex != null)
                {
                    Console.WriteLine(ex.ToString());
                    return;
                }
                Thread.Sleep(1100);
            }
        }
    }
}
