﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.Tests
{
	internal static class TestUtilities
	{
		internal static void PrintMessages(this IEnumerable<ValidationMessage> messages)
		{
			foreach (var message in messages)
				Console.WriteLine("Type: {0}, Message; {1}", message.Type, message.Message);
		}
	}
}
