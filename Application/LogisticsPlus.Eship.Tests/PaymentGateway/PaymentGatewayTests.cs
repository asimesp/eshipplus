﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.PaymentGateway;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.PaymentGateway
{

    [TestFixture]
    [Ignore("Payment Gateway tests. Run on demand only")]
    public class PaymentGatewayTests
    {
        private PaymentGatewayService _gateway;

        [SetUp]
        public void Setup()
        {
            GlobalTestInitializer.Initialize();

            var tenant = new Tenant(GlobalTestInitializer.DefaultTenantId);

            _gateway = new PaymentGatewayService(tenant.PaymentGatewayType,
                                                    new PaymentGatewayCredentials
                                                    {
                                                        LoginId = tenant.PaymentGatewayLoginId,
                                                        Secret = tenant.PaymentGatewaySecret,
                                                        TransactionId = tenant.PaymentGatewayTransactionId
                                                    },
                                                    true);
        }

        [Test]
        public void CanGetTransactionStatus()
        {
            var response = _gateway.GetTransactionStatus("2251030730");
            Console.WriteLine("Approved:{0} {4}Error Message:{1} {4}Transaction Id:{2} {4}Transaction Status:{3}", response.Approved, response.ErrorMessage, response.TransactionId, response.TransactionStatus.FormattedString(), Environment.NewLine);
        }

        [Test]
        public void CanRefundTransaction()
        {
            var response = _gateway.Refund("2251084783", .10m);
            Console.WriteLine("Approved:{0} {4}Error Message:{1} {4}Transaction Id:{2} {4}Transaction Status:{3}", response.Approved, response.ErrorMessage, response.TransactionId, response.TransactionStatus.FormattedString(), Environment.NewLine);
        }

        [Test]
        public void CanVoidTransaction()
        {
            var response = _gateway.Void("2251084783");
            Console.WriteLine("Approved:{0} {4}Error Message:{1} {4}Transaction Id:{2} {4}Transaction Status:{3}", response.Approved, response.ErrorMessage, response.TransactionId, response.TransactionStatus.FormattedString(), Environment.NewLine);
        }

        [Test]
        public void CanAuthorizeAndCaptureTransaction()
        {
            var response = _gateway.AuthorizeAndCapture(new PaymentInformation
                {
                    CardNumber = "4007000000027",
                    SecurityCode = "123",
                    ExpMonth = 01,
                    ExpYear = 20,
                    Description = string.Format("Test Transaction - {0}", DateTime.Now),
                    ChargeAmount = .75m,
                    BillingAddress = "Billing Address",
                    BillingCity = "Billing City",
                    BillingCountry = "United States",
                    BillingFirstName = "First Name",
                    BillingLastName = "Last Name",
                    BillingState = "PA",
                    BillingZip = "16501",
                    CustomerPaymentGatewayKey = new Customer(GlobalTestInitializer.DefaultCustomerId).PaymentGatewayKey,
                    CustomerPaymentProfileId = string.Empty,
                    CustomerNumber = new Customer(GlobalTestInitializer.DefaultCustomerId).CustomerNumber,
                    CheckNumber = string.Empty
                });
            Console.WriteLine("Approved:{0} {4}Error Message:{1} {4}Transaction Id:{2} {4}Transaction Status:{3}", response.Approved, response.ErrorMessage, response.TransactionId, response.TransactionStatus.FormattedString(), Environment.NewLine);
        }

        [Test]
        public void CanCreateAndDeletePaymentProfile()
        {
            var profileId = _gateway.AddPaymentProfile(new PaymentInformation
                {
                    CardNumber = "4007000000027",
                    SecurityCode = "123",
                    ExpMonth = 01,
                    ExpYear = 20,
                    BillingAddress = "Billing Address",
                    BillingCity = "Billing City",
                    BillingCountry = "United States",
                    BillingFirstName = "First Name",
                    BillingLastName = "Last Name",
                    BillingState = "PA",
                    BillingZip = "16501",
                    CustomerPaymentGatewayKey = new Customer(GlobalTestInitializer.DefaultCustomerId).PaymentGatewayKey,
                    CustomerPaymentProfileId = string.Empty,
                    CustomerNumber = new Customer(GlobalTestInitializer.DefaultCustomerId).CustomerNumber,
                    CheckNumber = string.Empty
                });

            Console.WriteLine("Profile ID:{0}", profileId);
            Console.WriteLine("Succeeded: {0}",_gateway.RemovePaymentProfile(new Customer(GlobalTestInitializer.DefaultCustomerId).PaymentGatewayKey, profileId));
        }
    }
}
