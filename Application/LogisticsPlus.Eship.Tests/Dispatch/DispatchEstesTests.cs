﻿using System;
using LogisticsPlus.Eship.CarrierIntegrations;
using LogisticsPlus.Eship.CarrierIntegrations.Dispatch;
using LogisticsPlus.Eship.Core.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Dispatch
{
    [TestFixture]
    [Ignore("Misc Test. Run on demand only")]
    public class DispatchEstesTests
    {
	    private	EstesDispatch _dispatch;

	    [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_dispatch = new EstesDispatch(new CarrierAuth { Username = "KSWARTWOOD", Password = "erie15" });
        }

        [Test]
        public void Echo()
        {
	        
			Console.WriteLine(_dispatch.Echo());
        }

		[Test]
		public void CanCheckDispatchedShimpent()
		{
			var response = _dispatch.CheckDispatchedShimpent(new Shipment { ShipmentNumber = "NAD330226" });
			Console.WriteLine(response);
		}

		[Test]
		public void CanDispatchShipment()
		{
			var shipment = new Shipment(153475);
			//var shipment = new Shipment(441025);
			var response = _dispatch.DispatchShipment(shipment, GlobalTestInitializer.ActiveUser);

			Console.WriteLine("Pickup #: {0}", response.PickupNumber);
			if(response.HasErrors)
				foreach (var err in response.Errors)
					Console.WriteLine("Error: {0}", err);
		}
    }
}
