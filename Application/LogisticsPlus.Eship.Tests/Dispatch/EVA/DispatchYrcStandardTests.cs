﻿using System;
using System.Collections.Generic;
using System.Net;
using LogisticsPlus.Eship.CarrierIntegrations.Dispatch.EVA;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Smc.Eva;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Dispatch.EVA
{
    [TestFixture]
    [Ignore("Ensure Vendor Communication ID still points to VCOM for Vendor with SCAC of \"RDWY\" and that it is in test mode.")]
    public class DispatchYrcStandardTests
    {
        private YrcDispatch _yrcStandardDispatch;


        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _yrcStandardDispatch = new YrcDispatch(new EvaSettings { BaseUri = "http://demoplatform.smc3.com/industryplatform" });
        }

        [Test]
        public void CanCreatePickup()
        {
            var dispatchResponse = _yrcStandardDispatch.DispatchShipment(GetShipment(), new VendorCommunication(237));
            Console.WriteLine("Success: {0}", (dispatchResponse.responseStatus == HttpStatusCode.OK.ToString()).ToString());
            Console.WriteLine("Response Code: {0}", dispatchResponse.responseCode);
            Console.WriteLine("Response Message: {0}", dispatchResponse.responseMessage);


        }

        private Shipment GetShipment()
        {
            var countryId = new CountrySearch().FetchCountryIdByCode("US");
            return new Shipment
                {
                    ShipmentNumber = "TESTING",
                    Origin = new ShipmentLocation
                        {
                            Contacts = new List<ShipmentContact>
                                {
                                    new ShipmentContact
                                        {
                                            Name = "TestName",
                                            Phone = "(123) 456-7890",
                                            Primary = true
                                        }
                                },
                            Street1 = "TestStreet1",
                            Street2 = "TestStreet2",
                            PostalCode = "16501",
                            City = "Erie",
                            State = "PA",
                            CountryId = countryId,
                            Description = "TestDesc",
                            SpecialInstructions = "TestSpecialInstructions"
                        },
                    Destination = new ShipmentLocation
                        {
                            Contacts = new List<ShipmentContact>
                                {
                                    new ShipmentContact
                                        {
                                            Name = "TestName",
                                            Phone = "(890) 123-4567",
                                            Primary = true
                                        }
                                },
                            Street1 = "TestStreet1",
                            Street2 = "TestStreet2",
                            PostalCode = "12345",
                            City = "Schenectady",
                            State = "NY",
                            CountryId = countryId,
                            Description = "TestDesc",
                            SpecialInstructions = "TestSpecialInstructions"
                        },
                    Items = new List<ShipmentItem>
                        {
                            new ShipmentItem
                                {
                                    PackageTypeId = 258, // PALLET(S)
                                    Description = "TestDesc",
                                    Quantity = 1,
                                    ActualLength = 48,
                                    ActualHeight = 48,
                                    ActualWidth = 48,
                                    ActualFreightClass = 77.5,
                                    ActualWeight = 250,
                                }
                        },
                    EstimatedDeliveryDate = new DateTime(2017, 6, 28, 0, 0, 0),
                    EarlyDelivery = "08:00",
                    LateDelivery = "12:00",
                    DesiredPickupDate = new DateTime(2017, 6, 28, 0, 0, 0),
                    EarlyPickup = "08:00",
                    LatePickup = "12:00",
                    User = new User()
                        {
                            FirstName = "John",
                            LastName = "Smith",
                            Phone = "(814) 123-4567",
                            Email = "jsmith@test.com"
                        },
                    Vendors = new List<ShipmentVendor>
                        {
                            new ShipmentVendor
                                {
                                    VendorId = 216, // YRC Freight
                                    Primary = true
                                }
                        }
                };
        }
    }
}
