﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using LogisticsPlus.Eship.CarrierIntegrations;
using LogisticsPlus.Eship.CarrierIntegrations.Dispatch;
using LogisticsPlus.Eship.CarrierIntegrations.Dispatch.Ward;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticssPlus.FedExPlugin;
using NUnit.Framework;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.Tests.Dispatch
{
    [TestFixture]
    [Ignore("Misc Test. Run on demand only")]
    public class DispatchWardTests
    {
        private WardDispatch _wardDispatch;


        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_wardDispatch = new WardDispatch(new CarrierAuth { Username = "kswartwood", Password = "erie16" });
        }

        [Test]
        public void TestObjectsSerializing()
        {
            var obj = new WardPickupReques
            {
                Body = new WardResponseBody()
                {
                    Request = new WardPickupRequestBody()
                    {
                        ShipperInformation = new WardShipperInformation()
                        {
                            DriverNote1 = "x",
                            DriverNote2 = "x",
                            DriverNote3 = "x",
                            PickupDate = "x",
                            RequestOrigin = "x",
                            RequestorContactEmail = "x",
                            RequestorContactName = "x",
                            RequestorContactTelephone = "x",
                            RequestorRole = "x",
                            RequestorUser = "x",
                            ShipperAddress1 = "x",
                            ShipperAddress2 = "x",
                            ShipperCity = "x",
                            ShipperCloseTime = "x",
                            ShipperCode = "x",
                            ShipperContactEmail = "x",
                            ShipperContactName = "x",
                            ShipperContactTelephone = "x",
                            ShipperName = "x",
                            ShipperReadyTime = "x",
                            ShipperRestriction = "x",
                            ShipperState = "x",
                            ShipperZipcode = "x",
                            ThirdParty = "x",
                            ThirdPartyContactEmail = "x",
                            ThirdPartyContactName = "x",
                            ThirdPartyContactTelephone = "x",
                            ThirdPartyName = "x",
                            WardAssuredContactEmail = "x",
                            WardAssuredContactName = "x",
                            WardAssuredContactTelephone = "x"
                        },
                        Shipment = new WardShipment()
                        {
                            RequestOrigin = "x",
                            ConsigneeAddress1 = "x",
                            ConsigneeAddress2 = "x",
                            ConsigneeCity = "x",
                            ConsigneeCode = "x",
                            ConsigneeName = "x",
                            ConsigneeState = "x",
                            ConsigneeZipcode = "x",
                            DeliveryAppntDate = "x",
                            DeliveryAppntFlag = "x",
                            Freezable = "x",
                            FullValue = "x",
                            FullValueInsuredAmount = "",
                            Hazardous = "x",
                            NonStandardSize = "x",
                            NonStandardSizeDescription = "x",
                            PackageCode = "x",
                            PickupShipmentInstruction1 = "x",
                            PickupShipmentInstruction2 = "x",
                            PickupShipmentInstruction3 = "x",
                            PickupShipmentInstruction4 = "x",
                            Pieces = "",
                            RequestorReference = "x",
                            ShipperRoutingSCAC = "x",
                            WardAssured03PM = "x",
                            WardAssured12PM = "x",
                            WardAssuredTimeDefinite = "x",
                            WardAssuredTimeDefiniteEnd = "",
                            WardAssuredTimeDefiniteStart = "",
                            Weight = ""
                        }
                    }
                }
            };

            var res = new WardPickupRequesResponse()
            {
                Body = new WardCreateResponseBody()
                {
                    CreateResponse = new WardCreateResultResponse()
                    {
                        CreateResult = new WardPickupRequestResponseBody()
                        {
                            Message = "x",
                            PickupConfirmation = "x",
                            PickupTerminal = "x",
                            WardEmail = "x",
                            WardTelephone = "x"
                        }
                    }
                }
            };

            var var1 = obj.ToXml();
            var var2 = res.ToXml();
            Console.WriteLine(var1);
            Console.WriteLine(var2);
        }

        [Test]
        public void CanCreatePickup()
        {
            Console.WriteLine("Pickup Number: {0}", _wardDispatch.DispatchShipment(GetShipment(), new User(2764)).ToXML());
        }

        private Shipment GetShipment()
        {
            return new Shipment
            {
                ShipmentNumber = "TESTING",
                Origin = new ShipmentLocation
                {
                    Contacts = new List<ShipmentContact>
                    {
                        new ShipmentContact
                        {
                            Name = "TestName",
                            Phone = "(123) 456-7890",
                            Primary = true
                        }
                    },
                    Street1 = "TestStreet1",
                    Street2 = "TestStreet2",
                    PostalCode = "16501",
                    City = "Erie",
                    State = "PA",
                    Description = "TestDesc",
                    SpecialInstructions = "TestSpecialInstructions"
                },
                Destination = new ShipmentLocation
                {
                    PostalCode = "12345"
                },
                Items = new List<ShipmentItem>
                {
                    new ShipmentItem
                    {
                        Description = "TestDesc",
                        Quantity = 2,
                        ActualWeight = 250,
                    }
                },
                EstimatedDeliveryDate = new DateTime(2016, 4, 22, 0, 0, 0),
                EarlyDelivery = "08:00",
                LateDelivery = "12:00",
                DesiredPickupDate = new DateTime(2016, 4, 21, 0, 0, 0),
                EarlyPickup = "08:00",
                LatePickup = "12:00",
            };
        }
    }
}
