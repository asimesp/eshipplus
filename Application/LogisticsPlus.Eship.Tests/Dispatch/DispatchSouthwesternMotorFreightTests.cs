﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.CarrierIntegrations;
using LogisticsPlus.Eship.CarrierIntegrations.Dispatch;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Dispatch
{
    [TestFixture]
    [Ignore("Misc Test. Run on demand only.")]
    public class DispatchSouthwesternMotorFreightTests
    {
        private SouthwesternMotorTransportDispatch _southwesternMotorTransportDispatch;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _southwesternMotorTransportDispatch = new SouthwesternMotorTransportDispatch(new CarrierAuth { Username = "LOPLjs622", Password = "erie15" });
        }

        [Test]
        public void CanCreatePickup()
        {
            Console.WriteLine("Pickup Number: {0}", _southwesternMotorTransportDispatch.DispatchShipment(GetShipment(), new User(2764)).PickupNumber);
        }

        [Test]
        public void CanDeletePickup()
        {
            Console.WriteLine("Success: {0}", _southwesternMotorTransportDispatch.CancelDispatchedShipment("SAA9999"));
        }

        [Test]
        public void CanUpdatePickup()
        {
            Console.WriteLine("New Pickup Number: {0}", _southwesternMotorTransportDispatch.UpdateDispatchedShipment(GetShipment(), new User(2764), "SAA9999"));
        }

        [Test]
        public void CanCheckDispatchedShimpent()
        {
            Console.WriteLine("Pro Number: {0}", _southwesternMotorTransportDispatch.CheckDispatchedShimpent(new Shipment(153473) { ShipmentNumber = "329225" }));
        }


        private static Shipment GetShipment()
        {
            return new Shipment
                {
                    ShipmentNumber = "TESTING - DO NOT PICKUP",
                    Origin = new ShipmentLocation
                        {
                            Contacts = new List<ShipmentContact>
                                {
                                    new ShipmentContact
                                        {
                                            Name = "TEST NAME",
                                            Phone = "(123) 456-7890",
                                            Primary = true
                                        }
                                },
                            Street1 = "TEST ST",
                            Street2 = string.Empty,
                            PostalCode = "78503",
                            City = "MCALLEN",
                            State = "TX",
                            Description = "TESTShippingCompany",
                            SpecialInstructions = "TESTOriginSpecialInstructions"
                        },
                    Destination = new ShipmentLocation
                        {
                            PostalCode = "78045",
                            Description = "TESTConsigneeCompany",
                            City = "Laredo",
                            State = "TX",
                            Street1 = "TEST ST",
                            Street2 = string.Empty,
                            SpecialInstructions = "TESTDestinationSpecialInstructions",
                            Contacts = new List<ShipmentContact>
                                {
                                    new ShipmentContact
                                        {
                                            Name = "TEST NAME",
                                            Phone = "(123) 456-7890",
                                            Primary = true
                                        }
                                },
                        },
                    Items = new List<ShipmentItem>
                        {
                            new ShipmentItem
                                {
                                    Description = "TESTShipmentItem",
                                    Quantity = 2,
                                    ActualWeight = 250,
                                    ActualLength = 48,
                                    ActualHeight = 48,
                                    ActualWidth = 40,
                                }
                        },
                    EstimatedDeliveryDate = new DateTime(2016, 6, 7, 0, 0, 0),
                    EarlyDelivery = "08:00",
                    LateDelivery = "15:00",
                    DesiredPickupDate = new DateTime(2016, 6, 7, 0, 0, 0),
                    EarlyPickup = "08:00",
                    LatePickup = "15:00",
                    Services = new List<ShipmentService>
                        {
                            new ShipmentService
                                {
                                    Service = new Service
                                        {
                                            Description = "Liftgate Pickup",
                                            Code = "LIFT",
                                            ApplicableAtPickup = true,
                                            ApplicableAtDelivery = false,
                                            Category = ServiceCategory.Accessorial,
                                            DispatchFlag = ServiceDispatchFlag.LiftGate,
                                        }
                                },
                            new ShipmentService
                                {
                                    Service = new Service
                                        {
                                            Description = "Inside Pickup",
                                            Code = "INSIDE",
                                            ApplicableAtPickup = true,
                                            ApplicableAtDelivery = false,
                                            Category = ServiceCategory.Accessorial,
                                            DispatchFlag = ServiceDispatchFlag.Inside,
                                        }
                                },
                            new ShipmentService
                                {
                                    Service = new Service
                                        {
                                            Description = "Inside Delivery",
                                            Code = "INSIDE",
                                            ApplicableAtPickup = false,
                                            ApplicableAtDelivery = true,
                                            Category = ServiceCategory.Accessorial,
                                            DispatchFlag = ServiceDispatchFlag.Inside,
                                        }
                                },
                            new ShipmentService
                                {
                                    Service = new Service
                                        {
                                            Description = "Non-Accesorial",
                                            Code = "NONE",
                                            ApplicableAtPickup = true,
                                            ApplicableAtDelivery = true,
                                            Category = ServiceCategory.Service,
                                            DispatchFlag = ServiceDispatchFlag.None,
                                        }
                                }
                        }
                };
        }
    }
}
