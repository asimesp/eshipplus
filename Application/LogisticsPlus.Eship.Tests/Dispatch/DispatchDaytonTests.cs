﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.CarrierIntegrations;
using LogisticsPlus.Eship.CarrierIntegrations.Dispatch;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Dispatch
{
    [TestFixture]
    [Ignore("Misc Test. Run on demand only")]
    public class DispatchDaytonTests
    {
        private DaytonDispatch _daytonDispatch;


        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _daytonDispatch = new DaytonDispatch(new CarrierAuth { Username = "KATHYF", Password = "erie15" });
        }

        [Test]
        public void CanCreatePickup()
        {
            Console.WriteLine("Pickup Number: {0}", _daytonDispatch.DispatchShipment(GetShipment(), new User(2764)));
        }

        [Test]
        public void CanDeletePickup()
        {
            Console.WriteLine("Success: {0}", _daytonDispatch.CancelDispatchedShipment("DAY-123456"));
        }

        [Test]
        public void CanUpdatePickup()
        {
            Console.WriteLine("New Pickup Number: {0}", _daytonDispatch.UpdateDispatchedShipment(GetShipment(), new User(2764), "DAY-123456"));
        }

        [Test]
        public void CanCheckDispatchedShimpent()
        {
            Console.WriteLine("Pro Number: {0}", _daytonDispatch.CheckDispatchedShimpent(new Shipment(153473) { ShipmentNumber = "329225"}).ProNumber);
        }


        private Shipment GetShipment()
        {
            return new Shipment
                {
                    ShipmentNumber = "TESTING",
                    Origin = new ShipmentLocation
                        {
                            Contacts = new List<ShipmentContact>
                                {
                                    new ShipmentContact
                                        {
                                            Name = "TestName",
                                            Phone = "(123) 456-7890",
                                            Primary = true
                                        }
                                },
                            Street1 = "TestStreet1",
                            Street2 = "TestStreet2",
                            PostalCode = "16501",
                            City = "Erie",
                            State = "PA",
                            Description = "TestDesc",
                            SpecialInstructions = "TestSpecialInstructions"
                        },
                    Destination = new ShipmentLocation
                        {
                            PostalCode = "12345"
                        },
                    Items = new List<ShipmentItem>
                        {
                            new ShipmentItem
                                {
                                    Description = "TestDesc",
                                    Quantity = 2,
                                    ActualWeight = 250,
                                }
                        },
                    EstimatedDeliveryDate = new DateTime(2016, 4, 22, 0, 0, 0),
                    EarlyDelivery = "08:00",
                    LateDelivery = "12:00",
                    DesiredPickupDate = new DateTime(2016, 4, 21, 0, 0, 0),
                    EarlyPickup = "08:00",
                    LatePickup = "12:00",
                };
        }
    }
}
