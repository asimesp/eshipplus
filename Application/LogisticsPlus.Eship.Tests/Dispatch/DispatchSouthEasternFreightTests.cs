﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.CarrierIntegrations;
using LogisticsPlus.Eship.CarrierIntegrations.Dispatch;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Dispatch
{
    [TestFixture]
    [Ignore("Misc Test. Run on demand only.")]
    public class DispatchSouthEasternFreightTests
    {
        private SouthEasternFreightDispatch _southEasternFreightDispatch;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _southEasternFreightDispatch = new SouthEasternFreightDispatch(new CarrierAuth { Username = "logisticsplus", Password = "erie15|999802192" });
        }

        [Test]
        public void CanCreatePickup()
        {
            Console.WriteLine("Pickup Number: {0}", _southEasternFreightDispatch.DispatchShipment(GetShipment(), new User(2764), true).PickupNumber);
        }

        [Test]
        public void CanDeletePickup()
        {
            Console.WriteLine("Success: {0}", _southEasternFreightDispatch.CancelDispatchedShipment("SAA9999"));
        }

        [Test]
        public void CanUpdatePickup()
        {
            Console.WriteLine("New Pickup Number: {0}", _southEasternFreightDispatch.UpdateDispatchedShipment(GetShipment(), new User(2764), "SAA9999"));
        }

        [Test]
        public void CanCheckDispatchedShimpent()
        {
            Console.WriteLine("Pro Number: {0}", _southEasternFreightDispatch.CheckDispatchedShimpent(new Shipment(153473) { ShipmentNumber = "329225" }));
        }


        private static Shipment GetShipment()
        {
            return new Shipment
            {
                ShipmentNumber = "TESTING",
                IsGuaranteedDeliveryService = true,
                GuaranteedDeliveryServiceTime = "12:30",
                Origin = new ShipmentLocation
                {
                    Contacts = new List<ShipmentContact>
                                {
                                    new ShipmentContact
                                        {
                                            Name = "TestName",
                                            Phone = "(123) 456-7890",
                                            Primary = true
                                        }
                                },
                    Street1 = "1406 Peach St.",
                    Street2 = string.Empty,
                    PostalCode = "16501",
                    City = "Erie",
                    State = "PA",
                    Description = "ShippingCompany",
                    SpecialInstructions = "OriginSpecialInstructions"
                },
                Destination = new ShipmentLocation
                {
                    PostalCode = "12345",
                    Description = "ConsigneeCompany",
                    City = "SCHENECTADY",
                    State = "NY",
                    Street1 = "123 Test St.",
                    Street2 = string.Empty,
                    SpecialInstructions = "DestinationSpecialInstructions",
                    Contacts = new List<ShipmentContact>
                                {
                                    new ShipmentContact
                                        {
                                            Name = "TestName",
                                            Phone = "(123) 456-7890",
                                            Primary = true
                                        }
                                },
                },
                Items = new List<ShipmentItem>
                        {
                            new ShipmentItem
                                {
                                    Description = "ShipmentItem",
                                    Quantity = 2,
                                    ActualWeight = 250,
                                    ActualLength = 48,
                                    ActualHeight = 48,
                                    ActualWidth = 40,
                                }
                        },
                EstimatedDeliveryDate = new DateTime(2016, 6, 2, 0, 0, 0),
                EarlyDelivery = "08:00",
                LateDelivery = "15:00",
                DesiredPickupDate = new DateTime(2016, 6, 2, 0, 0, 0),
                EarlyPickup = "08:00",
                LatePickup = "15:00",
                Services = new List<ShipmentService>
                        {
                            new ShipmentService
                                {
                                    Service = new Service
                                        {
                                            Description = "Liftgate Pickup",
                                            Code = "LIFT",
                                            ApplicableAtPickup = true,
                                            ApplicableAtDelivery = false,
                                            Category = ServiceCategory.Accessorial,
                                            DispatchFlag = ServiceDispatchFlag.LiftGate,
                                        }
                                },
                            new ShipmentService
                                {
                                    Service = new Service
                                        {
                                            Description = "Inside Pickup",
                                            Code = "INSIDE",
                                            ApplicableAtPickup = true,
                                            ApplicableAtDelivery = false,
                                            Category = ServiceCategory.Accessorial,
                                            DispatchFlag = ServiceDispatchFlag.Inside,
                                        }
                                },
                            new ShipmentService
                                {
                                    Service = new Service
                                        {
                                            Description = "Inside Delivery",
                                            Code = "INSIDE",
                                            ApplicableAtPickup = false,
                                            ApplicableAtDelivery = true,
                                            Category = ServiceCategory.Accessorial,
                                            DispatchFlag = ServiceDispatchFlag.Inside,
                                        }
                                },
                            new ShipmentService
                                {
                                    Service = new Service
                                        {
                                            Description = "Non-Accesorial",
                                            Code = "NONE",
                                            ApplicableAtPickup = true,
                                            ApplicableAtDelivery = true,
                                            Category = ServiceCategory.Service,
                                            DispatchFlag = ServiceDispatchFlag.None,
                                        }
                                }
                        }
            };
        }
    }
}
