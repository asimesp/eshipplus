﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.CarrierIntegrations;
using LogisticsPlus.Eship.CarrierIntegrations.Dispatch;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Dispatch
{
    [TestFixture]
    [Ignore("Misc Test. Run on demand only.")]
    public class DispatchUpsTests
    {
        private UpsDispatch _upsDispatch;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _upsDispatch = new UpsDispatch(new CarrierAuth { Username = "kswartwood", Password = "Erie2015|ED0D700745A11E48" });
        }

        [Test]
        public void CanCreatePickup()
        {
            var response = _upsDispatch.DispatchShipment(GetShipment(), new User(2764));
            Console.WriteLine("Pickup Number: {0}", response.PickupNumber);
            Console.WriteLine("Error: {0}", response.Errors.ToArray().CommaJoin());
        }

        [Test]
        public void CanDeletePickup()
        {
            Console.WriteLine("Success: {0}", _upsDispatch.CancelDispatchedShipment("888"));
        }

        [Test]
        public void CanUpdatePickup()
        {
            Console.WriteLine("New Pickup Number: {0}", _upsDispatch.UpdateDispatchedShipment(GetShipment(), new User(2764), "888"));
        }

        [Test]
        public void CanCheckDispatchedShimpent()
        {
            Console.WriteLine("Pro Number: {0}", _upsDispatch.CheckDispatchedShimpent(new Shipment(153473) { ShipmentNumber = "329225" }));
        }


        private static Shipment GetShipment()
        {
            return new Shipment
            {
                ShipmentNumber = "TESTING",
                Origin = new ShipmentLocation
                {
                    Contacts = new List<ShipmentContact>
                                {
                                    new ShipmentContact
                                        {
                                            Name = "TESTING",
                                            Phone = "(123) 456-7890",
                                            Primary = true
                                        }
                                },
                    Street1 = "2923 BOONES CREEK RD",
                    Street2 = string.Empty,
                    PostalCode = "37615",
                    City = "JOHNSON CITY",
                    State = "TN",
                    Description = "TESTINGTESTING",
                    SpecialInstructions = string.Empty,
                    Country = new Country(GlobalTestInitializer.DefaultCountryId)
                },
                Destination = new ShipmentLocation
                {
                    PostalCode = "78045",
                    Description = "TESTINGTESTING",
                    City = "LAREDO",
                    State = "TX",
                    Street1 = "4519 MODERN LANE",
                    Street2 = string.Empty,
                    SpecialInstructions = string.Empty,
                    Contacts = new List<ShipmentContact>
                                {
                                    new ShipmentContact
                                        {
                                            Name = "TESTING",
                                            Phone = "(123) 456-7890",
                                            Primary = true
                                        }
                                },
                    Country = new Country(GlobalTestInitializer.DefaultCountryId)
                },
                Items = new List<ShipmentItem>
                        {
                            new ShipmentItem
                                {
                                    Description = "TEST ITEM",
                                    Quantity = 1,
                                    ActualWeight = 350,
                                    ActualLength = 40,
                                    ActualHeight = 52,
                                    ActualWidth = 42,
                                    PieceCount = 1,
                                    ActualFreightClass = 85,
                                    Comment = string.Empty
                                }
                        },
                EstimatedDeliveryDate = new DateTime(2016, 7, 21, 0, 0, 0),
                EarlyDelivery = string.Empty,
                LateDelivery = string.Empty,
                DesiredPickupDate = new DateTime(2016, 7, 18, 0, 0, 0),
                EarlyPickup = "10:00",
                LatePickup = "15:00",
            };
        }
    }
}
