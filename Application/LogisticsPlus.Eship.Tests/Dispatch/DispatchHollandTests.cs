﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using LogisticsPlus.Eship.CarrierIntegrations;
using LogisticsPlus.Eship.CarrierIntegrations.Dispatch;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Dispatch
{
    [TestFixture]
    [Ignore("Misc Test. Run on demand only")]
    public class DispatchHollandTests
    {
        private HollandDispatch _hollandDispatch;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _hollandDispatch = new HollandDispatch(new CarrierAuth { Username = "unionstation", Password = "erie2015" });
        }

        [Test]
        public void CanCreatePickup()
        {
            try
            {
                var reply = _hollandDispatch.DispatchShipment(GetShipment(), new User(2764));
                Console.WriteLine("Pickup Number: {0}", reply.PickupNumber);
                if (reply.Errors != null && reply.Errors.Any())
                    foreach (var error in reply.Errors)
                        Console.WriteLine("Error: {0}", error);
            }
            catch (WebException webex)
            {
                using (var data = webex.Response.GetResponseStream())
                    if (data != null)
                        using (var reader = new StreamReader(data))
                            Console.WriteLine(reader.ReadToEnd());
            }
        }

        [Test]
        public void CanDeletePickup()
        {
            Console.WriteLine("Success: {0}", _hollandDispatch.CancelDispatchedShipment("DAY-123456"));
        }

        [Test]
        public void CanUpdatePickup()
        {
            Console.WriteLine("New Pickup Number: {0}", _hollandDispatch.UpdateDispatchedShipment(GetShipment(), new User(2764), "DAY-123456"));
        }

        [Test]
        public void CanCheckDispatchedShimpent()
        {
            Console.WriteLine("Pro Number: {0}", _hollandDispatch.CheckDispatchedShipment(new Shipment(153473) { ShipmentNumber = "329225" }));
        }


        private Shipment GetShipment()
        {
            return new Shipment
            {
                ShipmentNumber = "TESTING",
                Origin = new ShipmentLocation
                {
                    Description = "ITW SHAKEPROOF AUTO PROD",
                    Street1 = "1201 st charles rd",
                    Street2 = string.Empty,
                    City = "Elgin",
                    State = "IL",
                    PostalCode = "60120",
                    SpecialInstructions = "TestSpecialInstructions",
                    Contacts = new List<ShipmentContact>
                                {
                                    new ShipmentContact
                                        {
                                            Name = "TestName",
                                            Phone = "1234567890",
                                            Primary = true
                                        }
                                },
                },
                Destination = new ShipmentLocation
                {
                    Description = "VAN ROB VOLUNTEER",
                    Street1 = "1021 VOLUNTEER PARKWAY",
                    Street2 = string.Empty,
                    City="Manchester",
                    State="TN",
                    PostalCode = "37355",
                    Country = new Country(GlobalTestInitializer.DefaultCountryId),
                },
                Items = new List<ShipmentItem>
                        {
                            new ShipmentItem
                                {
                                    Description = "Steel Screws",
                                    Quantity = 5,
                                    ActualWeight = 5413,
                                    ActualFreightClass = 70,
                                    ActualLength = 31,
                                    ActualHeight = 30,
                                    ActualWidth = 30
                                },
                          
                        },
                EstimatedDeliveryDate = new DateTime(2016, 4, 29, 0, 0, 0),
                EarlyDelivery = "13:00",
                LateDelivery = "18:00",
                DesiredPickupDate = new DateTime(2016, 5, 2, 0, 0, 0),
                EarlyPickup = "00:00",
                LatePickup = "00:00",
            };
        }
    }
}
