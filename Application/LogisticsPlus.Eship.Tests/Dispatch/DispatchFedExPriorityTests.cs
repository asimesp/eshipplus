﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.CarrierIntegrations;
using LogisticsPlus.Eship.CarrierIntegrations.Dispatch;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Dispatch
{
    [TestFixture]
    [Ignore("Misc Test. Run on demand only")]
    public class DispatchFedExPriorityTests
    {
        private FedExPriorityDispatch _fedExPriorityDispatch;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _fedExPriorityDispatch = new FedExPriorityDispatch(new CarrierAuth
            {
                Username = string.Format("{0}|{1}", GlobalTestInitializer.FedExServiceParams.AccountNumber, GlobalTestInitializer.FedExServiceParams.MeterNumber),
                Password = string.Format("{0}|{1}", GlobalTestInitializer.FedExServiceParams.Password, GlobalTestInitializer.FedExServiceParams.Key)
            });
        }

        [Test]
        public void CanCreatePickup()
        {
            var reply = _fedExPriorityDispatch.DispatchShipment(GetShipment(), new User(2764));
            Console.WriteLine("Pickup Number: {0}", reply.PickupNumber);

            if (reply.Errors != null && reply.Errors.Any())
                foreach (var error in reply.Errors)
                    Console.WriteLine("Error: {0}", error);
        }

        [Test]
        public void CanDeletePickup()
        {
            Console.WriteLine("Success: {0}", _fedExPriorityDispatch.CancelDispatchedShipment("DAY-123456"));
        }

        [Test]
        public void CanUpdatePickup()
        {
            Console.WriteLine("New Pickup Number: {0}", _fedExPriorityDispatch.UpdateDispatchedShipment(GetShipment(), new User(2764), "DAY-123456"));
        }

        [Test]
        public void CanCheckDispatchedShimpent()
        {
            Console.WriteLine("Pro Number: {0}", _fedExPriorityDispatch.CheckDispatchedShipment(new Shipment(153473) { ShipmentNumber = "329225" }));
        }


        private Shipment GetShipment()
        {
            return new Shipment
            {
                ShipmentNumber = "TESTING",
                Origin = new ShipmentLocation
                {
                    Contacts = new List<ShipmentContact>
                                {
                                    new ShipmentContact
                                        {
                                            Name = "TestName",
                                            Phone = "(123) 456-7890",
                                            Primary = true
                                        }
                                },
                    Street1 = "TestStreet1",
                    Street2 = "TestStreet2",
                    PostalCode = "16501",
                    City = "Erie",
                    State = "PA",
                    Description = "TestDesc",
                    SpecialInstructions = "TestSpecialInstructions",
                    CountryId = GlobalTestInitializer.DefaultCountryId
                },
                Destination = new ShipmentLocation
                {
                    Description = "TestDescDest",
                    Street1 = "TestStreet1Dest",
                    Street2 = "TestStreet2Dest",
                    City = "HILLSBORO",
                    State = "OR",
                    PostalCode = "97124",
                    CountryId = GlobalTestInitializer.DefaultCountryId,
                },
                Items = new List<ShipmentItem>
                        {
                            new ShipmentItem
                                {
                                    Description = "TestDesc",
                                    Quantity = 2,
                                    ActualWeight = 250,
                                },
                            new ShipmentItem
                                {
                                    Description = "TestDesc2",
                                    Quantity = 1,
                                    ActualWeight = 120,
                                }
                        },
                EstimatedDeliveryDate = new DateTime(2016, 4, 27, 0, 0, 0),
                EarlyDelivery = "08:00",
                LateDelivery = "12:00",
                DesiredPickupDate = new DateTime(2016, 4, 28, 0, 0, 0),
                EarlyPickup = "08:00",
                LatePickup = "12:00",
                Services = new List<ShipmentService>
                        {
                            new ShipmentService
                                {
                                    Service = new Service
                                        {
                                            Description = "Liftgate Pickup",
                                            Code = "LIFT",
                                            ApplicableAtPickup = true,
                                            ApplicableAtDelivery = false,
                                            Category = ServiceCategory.Accessorial,
                                            DispatchFlag = ServiceDispatchFlag.LiftGate,
                                        }
                                },
                            new ShipmentService
                                {
                                    Service = new Service
                                        {
                                            Description = "Inside Pickup",
                                            Code = "INSIDE",
                                            ApplicableAtPickup = true,
                                            ApplicableAtDelivery = false,
                                            Category = ServiceCategory.Accessorial,
                                            DispatchFlag = ServiceDispatchFlag.Inside,
                                        }
                                },
                            new ShipmentService
                                {
                                    Service = new Service
                                        {
                                            Description = "Inside Delivery",
                                            Code = "INSIDE",
                                            ApplicableAtPickup = false,
                                            ApplicableAtDelivery = true,
                                            Category = ServiceCategory.Accessorial,
                                            DispatchFlag = ServiceDispatchFlag.Inside,
                                        }
                                },
                            new ShipmentService
                                {
                                    Service = new Service
                                        {
                                            Description = "Non-Accesorial",
                                            Code = "NONE",
                                            ApplicableAtPickup = true,
                                            ApplicableAtDelivery = true,
                                            Category = ServiceCategory.Accessorial,
                                            DispatchFlag = ServiceDispatchFlag.None,
                                        }
                                }
                        }
            };
        }

    }
}
