﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Handlers;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests
{
    [TestFixture]
    public class LoginHandlerTests
    {
        private LoginView _view;


        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _view = new LoginView();
            _view.Load();
        }

        [Test]
        public void CanAuthenticateUser()
        {
            _view.Authenticate();
        }

        [Test]
        public void CanRetrievePassword()
        {
            _view.Authenticate();
        }

        [Test]
        public void CanResetPasswordOnLogin()
        {
            GlobalTestInitializer.ActiveUser.ForcePasswordReset = true;
            GlobalTestInitializer.ActiveUser.Save();

            var password = _view.Password;
            var newPassword = DateTime.Now.ToShortDateString();
            _view.FakePasswordReset(newPassword);
            var user = new User(GlobalTestInitializer.ActiveUser.Id);
            Assert.IsTrue(password != user.Password);
            Assert.IsTrue(newPassword == user.Password);

            GlobalTestInitializer.ActiveUser.ForcePasswordReset = false;
            GlobalTestInitializer.ActiveUser.Password = password;
            GlobalTestInitializer.ActiveUser.Save();
        }

        internal class LoginView : ILoginView
        {
            public string UserName
            {
                get { return GlobalTestInitializer.ActiveUser.Username; }
            }

            public string Password
            {
                get { return GlobalTestInitializer.ActiveUser.Password; }
            }

            public string Domain
            {
                get { return WebApplication.WebApplicationSettings.Domain; }
            }

            public AuthenticationType AuthenticationType
            {
				get { return AuthenticationType.ApplicationAuthentication; }
            }

            public string AccessCode
            {
                get { return GlobalTestInitializer.ActiveUser.Tenant.Code; }
            }

			public bool AdminAuthentication { get { return false; } }

            public event EventHandler AuthenticateUser;
            public event EventHandler RetrieveUserPassword;
            public event EventHandler<ViewEventArgs<string>> PasswordChange;

            public void ProcessPasswordReset()
            {

            }

            public void Load()
            {
                var handler = new LoginHandler(this);
                handler.Initialize();
            }

            public void NotifyInActiveAccount()
            {
            }

            public void NotifyDisabledUser()
            {

            }

            public void NotifyUserNotFound()
            {

            }

            public void NotifyInvalidPassword()
            {

            }

            public void NotifyLoginAttemptsExceeded()
            {

            }

            public void NotifySuccesfulLogin(User user)
            {
                Assert.IsTrue(user != null);
            }

        	public void NotifySuccesfulLogin(AdminUser user)
        	{
				Assert.IsTrue(user != null);
        	}

        	public void ProcessPasswordRetrieval(string password, string userEmail)
            {
                Assert.IsTrue(!string.IsNullOrEmpty(password));
                Assert.IsTrue(!string.IsNullOrEmpty(userEmail));
            }

            public void DisplayMessages(IEnumerable<ValidationMessage> messages)
            {
                messages.PrintMessages();
                Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
            }

            public void Authenticate()
            {
                if (AuthenticateUser != null)
                    AuthenticateUser(this, new EventArgs());
            }

            public void RetrievePassword()
            {
                if (RetrieveUserPassword != null)
                    RetrieveUserPassword(this, new EventArgs());
            }

            public void FakePasswordReset(string newPassword)
            {
                if (PasswordChange != null)
                    PasswordChange(this, new ViewEventArgs<string>(newPassword));
            }
        }
    }
}
