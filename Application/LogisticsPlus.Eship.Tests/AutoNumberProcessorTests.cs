﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Views.Administration;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests
{
	[TestFixture]
	public class AutoNumberProcessorTests
	{
		private AutoNumberProcessor _processor;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_processor = new AutoNumberProcessor();
		}

		[Test]
		public void CanFetchNextNumber()
		{
			var tenant = new Tenant(GlobalTestInitializer.DefaultTenantId, false);
			Assert.IsTrue(tenant.KeyLoaded);
			foreach (var autoNumber in tenant.AutoNumbers)
			{
				var value = _processor.NextNumber(GlobalTestInitializer.DefaultTenantId, autoNumber.Code,
												  GlobalTestInitializer.ActiveUser);
				Assert.IsTrue(value > default(long));
			}
		}

		[Test]
		public void LockOnFetchNextNumberWorking()
		{
			var locks = new List<Lock>();
			var tenant = new Tenant(GlobalTestInitializer.DefaultTenantId, false);

            var criteria = new UserSearchCriteria
            {
                Parameters = new List<ParameterColumn>()
            };
			var userDto = new UserSearch().FetchUsers(criteria, GlobalTestInitializer.DefaultTenantId)
				.First(u => u.Id != GlobalTestInitializer.ActiveUser.Id);
			if (userDto == null) return;
		    var user = new User(userDto.Id);
			Assert.IsTrue(tenant.KeyLoaded);
			tenant.LoadCollections();
			foreach(var autoNumber in tenant.AutoNumbers)
			{
				var @lock = autoNumber.ObtainLock(GlobalTestInitializer.ActiveUser, autoNumber.Id);
				if (!@lock.IsUserLock(GlobalTestInitializer.ActiveUser))
				{
					foreach (var l in locks) l.Delete();
					return;
				}
				locks.Add(@lock);
			}
			foreach (var autoNumber in tenant.AutoNumbers)
			{
                var value = _processor.NextNumber(GlobalTestInitializer.DefaultTenantId, autoNumber.Code, user);
				Assert.IsTrue(value == default(long));
			}
			foreach(var l in locks) l.Delete();
		}
	}
}
