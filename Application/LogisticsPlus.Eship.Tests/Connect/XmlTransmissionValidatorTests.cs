﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Processor.Validation.Connect;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Connect
{
	[TestFixture]
	public class XmlTransmissionValidatorTests
	{
		private XmlTransmissionValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new XmlTransmissionValidator();
		}

		[Test]
		public void IsValidTest()
		{
			var transmission = new XmlTransmission
			                   	{
			                   		TenantId = GlobalTestInitializer.DefaultTenantId,
			                   		AcknowledgementDateTime = DateUtility.SystemEarliestDateTime,
			                   		AcknowledgementMessage = string.Empty,
			                   		CommunicationReferenceId = 1,
			                   		// cannot be default(long)
			                   		CommunicationReferenceType = CommunicationReferenceType.Customer,
			                   		DocumentType = "210",
			                   		// cannot be string.empty 210 = Invoice
			                   		NotificationMethod = NotificationMethod.Edi,
			                   		ReferenceNumber = "12345",
			                   		// cannot be string.empty
			                   		ReferenceNumberType = XmlTransmissionReferenceNumberType.Invoice,
			                   		TransmissionDateTime = DateTime.Now,
			                   		TransmissionKey = Guid.NewGuid(),
			                   		Xml = "Some Test",
			                   		// cannot be string.empty
			                   		User = GlobalTestInitializer.ActiveUser
			                   	};

			var isValid = _validator.IsValid(transmission);
			_validator.Messages.PrintMessages();
			Assert.IsTrue(isValid);
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var transmission = new XmlTransmission();
			Assert.IsFalse(_validator.HasAllRequiredData(transmission));
		}
	}
}
