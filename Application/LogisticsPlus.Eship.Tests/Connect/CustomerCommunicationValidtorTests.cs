﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation.Connect;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Connect
{
	[TestFixture]
	public class CustomerCommunicationValidtorTests
	{
	    private List<ParameterColumn> _columns;
	    private CustomerCommunicationValidator _validator;
	    private CustomerCommunication _communication;

	    [SetUp]
		public void Initialize()
		{
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _validator = new CustomerCommunicationValidator();

            _columns = new List<ParameterColumn>();

            var field = AccountingSearchFields.CustomerNumber;
            if (field != null) _columns.Add(new ParameterColumn { DefaultValue = SearchUtilities.WildCard, ReportColumnName = field.DisplayName, Operator = Operator.Contains });


            var tenantId = GlobalTestInitializer.DefaultTenantId;
            var customersWithCommunication = new CustomerSearch().FetchCustomersWithCommunication(_columns, tenantId);
            var customers = new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), tenantId);
            var customer = customers.FirstOrDefault(c => !customersWithCommunication.Select(cr => cr.Id).Contains(c.Id));

            _communication = new CustomerCommunication
                                {
                                    ConnectGuid = new Guid(),
                                    EnableWebServiceAPIAccess = true,
                                    Customer = customer,

                                    EdiEnabled = false,
                                    EdiCode = "Test",
                                    EdiVANUrl = "Test",
                                    EdiVANUsername = "Test",
                                    EdiVANPassword = "Test",
                                    EdiVANDefaultFolder = "Test",

                                    FtpEnabled = false,
                                    FtpUrl = "Test",
                                    FtpUsername = "Test",
                                    FtpPassword = "test",
                                    FtpDefaultFolder = "test",

									FtpDocDeliveryEnabled = false,
                                    DocDeliveryFtpUrl = "Test",
                                    DocDeliveryFtpUsername = "Test",
                                    DocDeliveryFtpPassword = "test",
                                    DocDeliveryFtpDefaultFolder = "test",

									EmailDocDeliveryEnabled = false,

                                    StartDocDeliveriesFrom = DateTime.Now,
									SendAveryLabel = false,
									SendStandardLabel = false,

                                    TenantId = tenantId,
                                };

            _communication.Notifications.Add(new CustomerNotification
                                {
                                    Milestone = Milestone.NewShipmentRequest,
                                    NotificationMethod = NotificationMethod.Fax,
                                    Fax = "888-888-8888",
                                    Email = "email@email.com",
                                    Enabled = true,

                                    CustomerCommunication = _communication,
                                    TenantId = tenantId,
                                });

         
		}

		[Test]
		public void IsValidTest()
		{
			Assert.IsTrue(_validator.IsValid(_communication));
		}

		[Test]
		public void HasRequiredDataTest()
		{
			var hasAllRequiredData = _validator.HasAllRequiredData(_communication);
			if (!hasAllRequiredData) _validator.Messages.PrintMessages();
			Assert.IsTrue(hasAllRequiredData);
		}

		[Test]
		public void CanDeleteTest()
        {
            var all = new CustomerSearch().FetchCustomersWithCommunication(_columns, GlobalTestInitializer.DefaultTenantId);
            if (all.Count == 0) return;
            var communication = new CustomerCommunication(new Customer(all[0].Id).Communication.Id, false);
            Assert.IsTrue(communication.KeyLoaded);
            _validator.Messages.Clear();
            _validator.CanDeleteCustomerCommunication(communication);
            _validator.Messages.PrintMessages();
		}


		[Test]
		public void DuplicateRequiredInvoiceDocumentTagTest()
		{
            var tag = new DocDeliveryDocTag();
            Assert.IsFalse(_validator.CustomerCommunicationDocDeliveryDocumentTagExist(tag));
            var all =
                new CustomerSearch().FetchCustomersWithCommunication(_columns, GlobalTestInitializer.DefaultTenantId)
                    .Select(c => new Customer(c.Id).Communication)
                    .Where(c => c.DocDeliveryDocTags.Any())
                    .ToList();
            if (!all.Any()) return;
            tag = all[0].DocDeliveryDocTags[0];
            Assert.IsTrue(_validator.CustomerCommunicationDocDeliveryDocumentTagExist(tag));
		}
	}
}
