﻿using System;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Processor.Validation.Connect;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Connect
{
	[TestFixture]
	public class XmlConnectValidatorTests
	{
		private XmlConnectValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new XmlConnectValidator();
		}

		[Test]
		public void IsValidTest()
		{
			var connect = new XmlConnect
				{
					TenantId = GlobalTestInitializer.DefaultTenantId,
					TotalPackages = 1,
					TotalPieces = 1,
					TotalStops = 2,
					TotalWeight = 1000m,
					ShipmentIdNumber = DateTime.Now.ToString("ffff"),
					ExpirationDate = DateTime.Now,
					EquipmentDescriptionCode = DateTime.Now.ToString("HH"),
					CustomerNumber = DateTime.Now.ToString("hhmmss"),
					DateCreated = DateTime.Now,
					DestinationCity = string.Empty,
					DestinationCountryCode = string.Empty,
					DestinationCountryName = string.Empty,
					DestinationPostalCode = string.Empty,
					DestinationState = string.Empty,
					DestinationStreet1 = string.Empty,
					DestinationStreet2 = string.Empty,
					Direction = Direction.Inbound,
					ControlNumber =  string.Empty,
					DocumentType = EdiDocumentType.EDI204,
					OriginCity = string.Empty,
					OriginCountryCode = string.Empty,
					OriginCountryName = string.Empty,
					OriginPostalCode = string.Empty,
					OriginState = string.Empty,
					OriginStreet1 = string.Empty,
					OriginStreet2 = string.Empty,
					PurchaseOrderNumber = string.Empty,
					ReceiptDate = DateTime.Now,
					ShipperReference = string.Empty,
					VendorNumber = DateTime.Now.ToString("hhmmss"),
                    VendorPro = string.Empty,
					VendorScac = DateTime.Now.ToString("ffff"),
					Xml = "some content!!!!",
					Status = XmlConnectStatus.Rejected,
					StatusMessage = "Test Rejection!"
				};

			var isValid = _validator.IsValid(connect);
			_validator.Messages.PrintMessages();
			Assert.IsTrue(isValid);
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var connect = new XmlConnect();
			Assert.IsFalse(_validator.HasAllRequiredData(connect));
		}
	}
}
