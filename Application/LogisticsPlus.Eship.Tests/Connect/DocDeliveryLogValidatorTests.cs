﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Validation.Connect;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Connect
{
    [TestFixture]
    public class DocDeliveryLogValidatorTests
    {
        private DocDeliveryLogValidator _validator;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
            _validator = new DocDeliveryLogValidator();
        }

        [Test]
        public void IsValid()
        {
            var tags = ProcessorVars.RegistryCache[GlobalTestInitializer.DefaultTenantId].DocumentTags;
            if (!tags.Any()) return;

            var text = DateTime.Now.ToString();

            var log = new DocDeliveryLog
                          {
                              TenantId = GlobalTestInitializer.DefaultTenantId,
                              EntityId = GlobalTestInitializer.DefaultShipmentId,
                              EntityType = DocDeliveryEntityType.ShipmentDocument,
                              LocationPath = text,
                              DocumentName = text,
                              LogDateTime = DateTime.Now,
                              DeliveryWasSuccessful = false,
                              FailedDeliveryMessage = text,
                              DocumentTagId = tags.First().Id,
							  UserId = GlobalTestInitializer.DefaultUserId
                          };
            var isValid = _validator.IsValid(log);
            _validator.Messages.PrintMessages();
            Assert.IsTrue(isValid);
        }

        [Test]
        public void IsNotValid()
        {
            var log = new DocDeliveryLog
                          {
                              DeliveryWasSuccessful = false,
                              EntityType = DocDeliveryEntityType.ShipmentDocument,
                          };

            var isValid = _validator.IsValid(log);
            _validator.Messages.PrintMessages();
            Assert.IsFalse(isValid);
        }
    }
}