﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Processor.Validation.Connect;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Connect
{
	[TestFixture]
	public class FaxTransmissionValidatorTests
	{
		private FaxTransmissionValidator _validator;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new FaxTransmissionValidator();
		}

		[Test]
		public void IsValidTest()
		{
			var transmission = new FaxTransmission
							{
								Resolved = false,
								Message = string.Empty,
								ResolutionComment = "Resolution",
								Status = FaxTransmissionStatus.Failed,
								SendDateTime = DateTime.Now,
								ResponseDateTime = DateUtility.SystemEarliestDateTime,
								LastStatusCheckDateTime = DateUtility.SystemEarliestDateTime,
								TenantId = GlobalTestInitializer.DefaultTenantId,
								ShipmentNumber = "10000",
								TransmissionKey = Guid.NewGuid(),    
								ServiceProvider = ServiceProvider.None
							};


			var isValid = _validator.IsValid(transmission);
			_validator.Messages.PrintMessages();
			Assert.IsTrue(isValid);
		}

		[Test]
		public void HasAllRequiredDataTest()
		{
			var transmission = new FaxTransmission();
			Assert.IsFalse(_validator.HasAllRequiredData(transmission));
		}
	}
}
