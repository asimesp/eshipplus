﻿using System.Linq;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Processor.Searches.Connect;
using LogisticsPlus.Eship.Processor.Views.Connect;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Connect
{
	[TestFixture]
	public class XmlConnectSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchXmlConnectRecordsTest()
		{
			var searchCriteria = new XmlConnectSearchCriteria{ActiveUserId = GlobalTestInitializer.DefaultUserId};
			new XmlConnectSearch().FetchXmlConnectRecords(searchCriteria, GlobalTestInitializer.DefaultTenantId);
		}

		[Test]
		public void CanFetchFetchLoadTenderByShipmentIdNumberTest()
		{
			var xc = new XmlConnectSearch().FetchInboundLoadTenderByShipmentIdNumber("56213", GlobalTestInitializer.DefaultTenantId,
			                                                         GlobalTestInitializer.DefaultUserId);
			Assert.IsNotNull(xc);
		}

        [Test]
        [Ignore("When outbound load tenders are made, this test will need a valid shipment number and vendor number")]
        public void CanFetchFetchOutboundLoadTenderResponseByShipmentIdNumberTest()
        {
            var xc = new XmlConnectSearch().FetchOutboundLoadTenderResponseByShipmentIdNumber("", GlobalTestInitializer.DefaultTenantId,
                                                                     GlobalTestInitializer.DefaultUserId, "");
            Assert.IsNotNull(xc);
        }

        [Test]
        public void CanFetchFetchOutBoundLoadTenderByVendShipIdNumAndDocTypeTest()
        {
            var xc = new XmlConnectSearch().FetchOutBoundLoadTenderByVendShipIdNumAndDocType("10000", "56763", GlobalTestInitializer.DefaultTenantId,
                                                                     GlobalTestInitializer.DefaultUserId);
            Assert.IsNotNull(xc);
        }

		[Test]
		public void CanFetchXmlConnectBySetControlNumberAndDocumentTypeTest()
		{
			var records = new XmlConnectSearch()
				.FetchOutBoundXmlConnectBySetCtrlNumAndDocType("10792", EdiDocumentType.EDI990, GlobalTestInitializer.DefaultTenantId, GlobalTestInitializer.DefaultUserId);
			Assert.IsTrue(records.Any());
		}

	}
}