﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation.Connect;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Connect
{
	[TestFixture]
	public class VendorCommunicationValidtorTests
	{
		private VendorCommunicationValidator _validator;
		private VendorCommunication _communication;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_validator = new VendorCommunicationValidator();

			var tenantId = GlobalTestInitializer.DefaultTenantId;
		    var columns = new List<ParameterColumn>();

			var vendorsWithCommunication = new VendorSearch().FetchVendorsWithCommunication(columns, tenantId);
			var vendors = new VendorSearch().FetchVendors(new List<ParameterColumn>(), tenantId);
			var vendor = vendors.FirstOrDefault(c => !vendorsWithCommunication.Select(cr => cr.Id).Contains(c.Id));

			_communication = new VendorCommunication
								{
									ConnectGuid = new Guid(),
									EnableWebServiceAPIAccess = true,
									Vendor = vendor,

									EdiEnabled = false,
									EdiCode = "Test",
									EdiVANUrl = "Test",
									EdiVANUsername = "Test",
									EdiVANPassword = "Test",
									EdiVANDefaultFolder = "Test",

									FtpEnabled = false,
									FtpUrl = "Test",
									FtpUsername = "Test",
									FtpPassword = "test",
									FtpDefaultFolder = "test",

									TenantId = tenantId,
								};

			_communication.Vendor.Communication = _communication;
			_communication.Notifications.Add(new VendorNotification
								{
									Milestone = Milestone.NewShipmentRequest,
									NotificationMethod = NotificationMethod.Fax,
									Fax = "888-888-8888",
									Email = "email@email.com",
									Enabled = true,
									VendorCommunication = _communication,
									TenantId = tenantId,
								});
		}

		[Test]
		public void IsValidTest()
		{
			Assert.IsTrue(_validator.IsValid(_communication));
		}

		[Test]
		public void HasRequiredDataTest()
		{
			var hasAllRequiredData = _validator.HasAllRequiredData(_communication);
			if (!hasAllRequiredData) _validator.Messages.PrintMessages();
			Assert.IsTrue(hasAllRequiredData);
		}

		[Test]
		public void CanDeleteTest()
		{
            var columns = new List<ParameterColumn>();

            var vendorsWithCommunication = new VendorSearch().FetchVendorsWithCommunication(columns, GlobalTestInitializer.DefaultTenantId);
            if (vendorsWithCommunication.Count == 0) return;
            var communication = new VendorCommunication(vendorsWithCommunication[0].Communication.Id, false);
			Assert.IsTrue(communication.KeyLoaded);
			_validator.Messages.Clear();
			_validator.CanDeleteVendorCommunication(communication);
			_validator.Messages.PrintMessages();
		}
	}
}
