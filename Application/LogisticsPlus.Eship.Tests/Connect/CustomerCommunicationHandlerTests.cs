﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Connect;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.Processor.Views.Connect;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Connect
{
	[TestFixture]
	public class CustomerCommunicationHandlerTests
	{
		private CustomerCommunicationView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new CustomerCommunicationView();
			_view.Load();
		}

		[Test]
		public void CanSaveOrUpdate()
		{
            var columns = new List<ParameterColumn>();

            var field = AccountingSearchFields.CustomerNumber;
            if (field != null) columns.Add(new ParameterColumn { DefaultValue = SearchUtilities.WildCard, ReportColumnName = field.DisplayName, Operator = Operator.Contains });

            var all = new CustomerSearch().FetchCustomersWithCommunication(columns, GlobalTestInitializer.DefaultTenantId);
            if (all.Count == 0) return;
            var communication = new Customer(all[0].Id).Communication;
            communication.LoadCollections();
            _view.LockRecord(communication);
            communication.TakeSnapShot();
            var code = communication.EdiVANPassword;
            communication.EdiVANPassword = "0";
            _view.SaveRecord(communication);
            communication.TakeSnapShot();
            communication.EdiVANPassword = code;
            _view.SaveRecord(communication);
            _view.UnLockRecord(communication);
		}

		[Test]
		public void CanSaveAndDelete()
		{
			var rating = GetCommunication();
			_view.SaveRecord(rating);
			Assert.IsTrue(rating.Id != default(long));
			_view.DeleteRecord(rating);
			Assert.IsTrue(!new CustomerCommunication(rating.Id, false).KeyLoaded);
		}

		

        private CustomerCommunication GetCommunication()
        {
            var tenantId = GlobalTestInitializer.DefaultTenantId;

            var columns = new List<ParameterColumn>();

            var field = AccountingSearchFields.CustomerNumber;
            if (field != null) columns.Add(new ParameterColumn { DefaultValue = SearchUtilities.WildCard, ReportColumnName = field.DisplayName, Operator = Operator.Contains });


            var customersWithCommunication = new CustomerSearch().FetchCustomersWithCommunication(columns, tenantId);
            var customers = new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), tenantId);
            var customer = customers.FirstOrDefault(c => !customersWithCommunication.Select(cr => cr.Id).Contains(c.Id));

            var customerCommunication = new CustomerCommunication
                                            {
                                                TenantId = tenantId,
                                                Customer = customer,
                                                EdiEnabled = false,
                                                EdiCode = string.Empty,
                                                FtpEnabled = false,
                                                ConnectGuid = Guid.NewGuid(),
                                                EdiVANDefaultFolder = string.Empty,
                                                EdiVANPassword = string.Empty,
                                                EdiVANUrl = string.Empty,
                                                EdiVANUsername = string.Empty,
                                                EdiVANEnvelopePath = string.Empty,
                                                FtpDefaultFolder = string.Empty,
                                                FtpPassword = string.Empty,
                                                FtpUrl = string.Empty,
                                                FtpUsername = string.Empty,
                                                StopVendorNotifications = false,
												EmailDocDeliveryEnabled = false,
												FtpDocDeliveryEnabled = false,
                                                DocDeliveryFtpUrl = string.Empty,
                                                DocDeliveryFtpUsername = string.Empty,
                                                DocDeliveryFtpPassword = string.Empty,
                                                DocDeliveryFtpDefaultFolder = string.Empty,
                                                StartDocDeliveriesFrom = DateTime.Now,
												SendAveryLabel = false,
												SendStandardLabel = false
                                            };
            customerCommunication.Customer.Communication = customerCommunication;


            customerCommunication.Notifications.Add(new CustomerNotification
                            {
                                TenantId = tenantId,
                                CustomerCommunication = customerCommunication,
                                Fax = "888-888-8888",
                                Milestone = Milestone.NewShipmentRequest,
                                NotificationMethod = NotificationMethod.Fax
                            });

            customerCommunication.DocDeliveryDocTags.Add(new DocDeliveryDocTag
                                                            {
                                                                TenantId = tenantId,
                                                                CustomerCommunication = customerCommunication,
                                                                DocumentTag = ProcessorVars.RegistryCache[tenantId].DocumentTags.FirstOrDefault()
                                                            });

            return customerCommunication;

        }

		internal class CustomerCommunicationView : ICustomerCommunicationView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public List<DocumentTag> DocumentTags
			{
				set { }
			}

			public Dictionary<int, string> Milestones
			{
				set {  }
			}

			public Dictionary<int, string> NotificationMethods
			{
				set {  }
			}

			public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<CustomerCommunication>> Save;
			public event EventHandler<ViewEventArgs<CustomerCommunication>> Delete;
			public event EventHandler<ViewEventArgs<CustomerCommunication>> Lock;
			public event EventHandler<ViewEventArgs<CustomerCommunication>> UnLock;
			public event EventHandler<ViewEventArgs<CustomerCommunication>> LoadAuditLog;
			public event EventHandler<ViewEventArgs<string>> CustomerSearch;

			public void DisplayVendor(Vendor vendor)
			{
			}

			public void DisplayCustomer(Customer customer)
			{
			}

			public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void LogException(Exception ex)
			{		
			}

			public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
			{
			}

			public void FailedLock(Lock @lock)
			{
			}

			public void Set(CustomerCommunication communication)
			{
			}

			public void Load()
			{
				var handler = new CustomerCommunicationHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(CustomerCommunication rating)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<CustomerCommunication>(rating));
			}

			public void DeleteRecord(CustomerCommunication rating)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<CustomerCommunication>(rating));
			}

			public void LockRecord(CustomerCommunication rating)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<CustomerCommunication>(rating));
			}

			public void UnLockRecord(CustomerCommunication rating)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<CustomerCommunication>(rating));
			}
		}
	}
}
