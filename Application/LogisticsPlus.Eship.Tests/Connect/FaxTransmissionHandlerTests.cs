﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Processor.Dto.Connect;
using LogisticsPlus.Eship.Processor.Handlers.Connect;
using LogisticsPlus.Eship.Processor.Searches.Connect;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Connect;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Connect
{
	[TestFixture]
	public class FaxTransmissionHandlerTests
	{
		private FaxTransmissionView _view;

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new FaxTransmissionView();
			_view.Load();
		}

		[Test]
		public void CanHandleTransmissionSaveOrUpdate()
		{
			var criteria =new FaxTransmissionSearchCriteria();
			var all = new FaxTransmissionSearch().FetchFaxTransmissions(criteria, GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;

			var transmission = new FaxTransmission(all[0].Id, false);
			_view.LockRecord(transmission);
			transmission.TakeSnapShot();
			var message = transmission.Message;
			transmission.Message = DateTime.Now.ToShortDateString();
			_view.SaveRecord(transmission);
			transmission.TakeSnapShot();
			transmission.Message = message;
			_view.SaveRecord(transmission);
			_view.UnLockRecord(transmission);
		}

		internal class FaxTransmissionView : IFaxTransmissionView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public event EventHandler<ViewEventArgs<FaxTransmission>> Save;
			public event EventHandler<ViewEventArgs<FaxTransmission>> Delete;
			public event EventHandler<ViewEventArgs<FaxTransmission>> LoadAuditLog;
			public event EventHandler<ViewEventArgs<FaxTransmissionSearchCriteria>> Search;
			public event EventHandler<ViewEventArgs<FaxTransmission>> Lock;
			public event EventHandler<ViewEventArgs<FaxTransmission>> UnLock;

			public void FailedLock(Lock @lock)
			{

			}

			public void DisplaySearchResult(List<FaxTransmissionViewSearchDto> assets)
			{
				Assert.IsTrue(assets.Count > 0);
			}

			public void LogException(Exception ex)
			{
			}

			public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void Load()
			{
				var handler = new FaxTransmissionHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(FaxTransmission transmission)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<FaxTransmission>(transmission));
			}

			public void SearchRecord(FaxTransmissionSearchCriteria criteria)
			{
				if (Search != null)
					Search(this, new ViewEventArgs<FaxTransmissionSearchCriteria>(criteria));
			}

			public void LockRecord(FaxTransmission transmission)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<FaxTransmission>(transmission));
			}

			public void UnLockRecord(FaxTransmission transmission)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<FaxTransmission>(transmission));
			}
		}
	}
}
