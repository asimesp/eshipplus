﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Processor.Searches.Connect;
using LogisticsPlus.Eship.Processor.Views.Connect;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Connect
{
	[TestFixture]
	public class FaxTransmissionSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchFaxTransmissions()
		{
			var searchCriteria = new FaxTransmissionSearchCriteria();
			var trans = new FaxTransmissionSearch().FetchFaxTransmissions(searchCriteria, GlobalTestInitializer.DefaultTenantId);
			Assert.IsTrue(trans.Any());
			Console.WriteLine(trans.Count);
		}

		[Test]
		public void CanFetchAllFaxTransmissionsWithNoResponse()
		{
			var trans = new FaxTransmissionSearch().FetchAllFaxTransmissionWithNoResponse();
			Assert.IsTrue(trans.Any());
			Console.WriteLine(trans.Count);
		}

		[Test]
		public void CanFetchFaxTransmissionByKey()
		{
			new FaxTransmissionSearch().FetchFaxTransmission(Guid.Empty);
		}
	}
}
