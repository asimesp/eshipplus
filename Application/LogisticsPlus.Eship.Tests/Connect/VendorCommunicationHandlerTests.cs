﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Connect;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Connect;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Connect
{
	[TestFixture]
	public class VendorCommunicationHandlerTests
	{
		private VendorCommunicationView _view;

		[Test]
		public void CanSaveOrUpdate()
		{
			var all = new VendorSearch().FetchVendorsWithCommunication(new List<ParameterColumn>(), GlobalTestInitializer.DefaultTenantId);
			if (all.Count == 0) return;
			var communication = all[0].Communication;
			communication.LoadCollections();
			_view.LockRecord(communication);
			communication.TakeSnapShot();
			var code = communication.EdiVANPassword;
			communication.EdiVANPassword = "0";
			_view.SaveRecord(communication);
			communication.TakeSnapShot();
			communication.EdiVANPassword = code;
			_view.SaveRecord(communication);
			_view.UnLockRecord(communication);
		}

		[Test]
		public void CanSaveAndDelete()
		{
			var rating = GetCommunication();
			_view.SaveRecord(rating);
			Assert.IsTrue(rating.Id != default(long));
			_view.DeleteRecord(rating);
			Assert.IsTrue(!new VendorCommunication(rating.Id, false).KeyLoaded);
		}

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_view = new VendorCommunicationView();
			_view.Load();
		}

		private VendorCommunication GetCommunication()
		{
			var tenantId = GlobalTestInitializer.DefaultTenantId;

			var vendorsWithCommunication = new VendorSearch().FetchVendorsWithCommunication(new List<ParameterColumn>(), tenantId);
			var vendors = new VendorSearch().FetchVendors(new List<ParameterColumn>(), tenantId);
			var vendor = vendors.FirstOrDefault(c => !vendorsWithCommunication.Select(cr => cr.Id).Contains(c.Id));

			var vendorCommunication = new VendorCommunication
			{
				ConnectGuid = new Guid(),
				EnableWebServiceAPIAccess = true,
				Vendor = vendor,

				EdiEnabled = false,
				EdiCode = "Test",
				EdiVANUrl = "Test",
				EdiVANUsername = "Test",
				EdiVANPassword = "Test",
				EdiVANDefaultFolder = "Test",
				EdiVANEnvelopePath = string.Empty,

				FtpEnabled = false,
				FtpUrl = "Test",
				FtpUsername = "Test",
				FtpPassword = "test",
				FtpDefaultFolder = "test",

				TenantId = tenantId,

				ImgRtrvEnabled = false,
				ShipmentDispatchEnabled = false,
				CarrierIntegrationEngine = string.Empty,
				CarrierIntegrationPassword = string.Empty,
				CarrierIntegrationUsername = string.Empty,

				SMC3ProductionAccountToken = string.Empty,
				SMC3TestAccountToken = string.Empty,

                Project44Enabled = false,
                Project44TrackingEnabled = false,
                Project44DispatchEnabled = false
			};

			vendorCommunication.Vendor.Communication = vendorCommunication;
			vendorCommunication.Notifications.Add(new VendorNotification
			{
				Milestone = Milestone.NewShipmentRequest,
				NotificationMethod = NotificationMethod.Fax,
				Fax = "888-888-8888",
				Email = "email@email.com",
				Enabled = true,
				VendorCommunication = vendorCommunication,
				TenantId = tenantId,
			});
			return vendorCommunication;
		}

		internal class VendorCommunicationView : IVendorCommunicationView
		{
			public User ActiveUser
			{
				get { return GlobalTestInitializer.ActiveUser; }
			}

			public Dictionary<int, string> Milestones
			{
				set {  }
			}

			public Dictionary<int, string> NotificationMethods
			{
				set {  }
			}

			public event EventHandler Loading;
			public event EventHandler<ViewEventArgs<VendorCommunication>> Save;
			public event EventHandler<ViewEventArgs<VendorCommunication>> Delete;
			public event EventHandler<ViewEventArgs<VendorCommunication>> Lock;
			public event EventHandler<ViewEventArgs<VendorCommunication>> UnLock;
			public event EventHandler<ViewEventArgs<VendorCommunication>> LoadAuditLog;
			public event EventHandler<ViewEventArgs<string>> VendorSearch;

			public void DisplayVendor(Vendor vendor)
			{
			}

			public void DisplayMessages(IEnumerable<ValidationMessage> messages)
			{
				messages.PrintMessages();
				Assert.IsTrue(messages.Count(m => m.Type != ValidateMessageType.Information) == 0);
			}

			public void LogException(Exception ex)
			{
			}

			public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
			{
			}

			public void FailedLock(Lock @lock)
			{
			}

			public void Set(VendorCommunication communication)
			{
			}

			public void Load()
			{
				var handler = new VendorCommunicationHandler(this);
				handler.Initialize();
			}

			public void SaveRecord(VendorCommunication rating)
			{
				if (Save != null)
					Save(this, new ViewEventArgs<VendorCommunication>(rating));
			}

			public void DeleteRecord(VendorCommunication rating)
			{
				if (Delete != null)
					Delete(this, new ViewEventArgs<VendorCommunication>(rating));
			}

			public void LockRecord(VendorCommunication rating)
			{
				if (Lock != null)
					Lock(this, new ViewEventArgs<VendorCommunication>(rating));
			}

			public void UnLockRecord(VendorCommunication rating)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<VendorCommunication>(rating));
			}
		}
	}
}
