﻿using System.Linq;
using LogisticsPlus.Eship.Processor.Searches.Connect;
using LogisticsPlus.Eship.Processor.Views.Connect;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Connect
{
    [TestFixture]
    public class DocDeliveryLogSearchTests
    {
        private DocDeliveryLogSearchCriteria _criteria;

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();

            _criteria = new DocDeliveryLogSearchCriteria();
        }

        [Test]
        public void CanFetchShipmentsReadyToInvoice()
        {
            var logs = new DocDeliveryLogSearch().FetchDocDeliveryLogs(_criteria, GlobalTestInitializer.DefaultTenantId);
            Assert.IsTrue(logs.Any());
        }
    
    
    }
}