﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Connect;
using LogisticsPlus.Eship.Processor.Views.Connect;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Connect
{
	[TestFixture]
	public class XmlTransmissionSearchTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void CanFetchXmlTransmissions()
		{
			var searchCriteria = new XmlTransmissionViewSearchCriteria
									{
										Criteria = SearchUtilities.WildCard,
										SendOkay = -1,
										Direction = -1,
										StartAcknowledgementDateTime = DateUtility.SystemEarliestDateTime,
										EndAcknowledgementDateTime = DateUtility.SystemLatestDateTime,
										StartSendDateTime = DateUtility.SystemEarliestDateTime,
										EndSendDateTime = DateUtility.SystemLatestDateTime,
									};
			new XmlTransmissionSearch().FetchXmlTransmissions(searchCriteria, GlobalTestInitializer.DefaultTenantId);
		}

		[Test]
		public void CanFetchXmlTransmissionByKey()
		{
			new XmlTransmissionSearch().FetchXmlTransmission(Guid.Empty);
		}
	}
}