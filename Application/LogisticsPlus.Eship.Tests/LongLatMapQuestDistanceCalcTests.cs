﻿using System;
using LogisticsPlus.Eship.MapQuestMilesPlugin;
using LogisticsPlus.Eship.PluginBase.Mileage;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests
{
	[TestFixture]
	public class LongLatMapQuestDistanceCalcTests
	{
		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
		}

		[Test]
		public void GetDistance()
		{
			var search = new PostalCodeSearch();
			var p1 = search.FetchPostalCodeByCode("16501", GlobalTestInitializer.DefaultCountryId);
			var p2 = search.FetchPostalCodeByCode("16502", GlobalTestInitializer.DefaultCountryId);

			var calculation = new LongLatDistanceCalc();
			var initialized = calculation.Initialize();
			if(!initialized)
				Console.WriteLine(calculation.LastErrorMessage);
			Assert.IsTrue(initialized);


			Console.WriteLine("With Long-Lat:");
			var pt1 = new Point {Type = PointType.LongitudeLatitude, Longitude = p1.Longitude, Latitude = p1.Latitude};
			var pt2 = new Point {Type = PointType.LongitudeLatitude, Longitude = p2.Longitude, Latitude = p2.Latitude};
			var distance = calculation.GetMiles(pt1, pt2);
			Console.WriteLine("Distance: {0}\tMessage: {1}", distance, calculation.LastErrorMessage);
			distance = calculation.GetKilometers(pt1, pt2);
			Console.WriteLine("Distance: {0}\tMessage: {1}", distance, calculation.LastErrorMessage);

			Console.WriteLine("\n\nWith Custom Type:");
			pt1 = new Point { Type = PointType.Address, PostalCode = p1.Code, Country = p1.CountryId };
			pt2 = new Point { Type = PointType.Address, PostalCode = p2.Code, Country = p2.CountryId };
			distance = calculation.GetMiles(pt1, pt2);
			Console.WriteLine("Distance: {0}\tMessage: {1}", distance, calculation.LastErrorMessage);
			distance = calculation.GetKilometers(pt1, pt2);
			Console.WriteLine("Distance: {0}\tMessage: {1}", distance, calculation.LastErrorMessage);
		}

		[Test]
		public void GetDistanceBadData()
		{
			var calculation = new LongLatDistanceCalc();
			calculation.Initialize();

			var pt1 = new Point { Type = PointType.Address };
			var pt2 = new Point { Type = PointType.Address };

			var distance = calculation.GetMiles(pt1, pt2);
			Console.WriteLine("Distance: {0}\tMessage: {1}", distance, calculation.LastErrorMessage);
		}

		[Test]
		public void GetDistanceMapQuest()
		{
			var search = new PostalCodeSearch();
			var p1 = search.FetchPostalCodeByCode("16501", GlobalTestInitializer.DefaultCountryId);
			var p2 = search.FetchPostalCodeByCode("44001", GlobalTestInitializer.DefaultCountryId);

			var longLat = new LongLatDistanceCalc();
			var mapQuestMiles = new MapQuestMiles();
			var initialized = longLat.Initialize();
			mapQuestMiles.Initialize();
			if (!initialized)
				Console.WriteLine(longLat.LastErrorMessage);
			Assert.IsTrue(initialized);


			Console.WriteLine("With Long-Lat:");
			var pt1 = new Point { Type = PointType.LongitudeLatitude, Longitude = p1.Longitude, Latitude = p1.Latitude };
			var pt2 = new Point { Type = PointType.LongitudeLatitude, Longitude = p2.Longitude, Latitude = p2.Latitude };
			var distance = longLat.GetMiles(pt1, pt2);
			Console.WriteLine("Distance: {0}\tMessage: {1}", distance, longLat.LastErrorMessage);
			distance = longLat.GetKilometers(pt1, pt2);
			Console.WriteLine("Distance: {0}\tMessage: {1}", distance, longLat.LastErrorMessage);

			Console.WriteLine("\n\nWith MapQuest Type:");
			pt1 = new Point { Type = PointType.Address, PostalCode = p1.Code, Country = p1.CountryCode, City = p1.City, State = p1.State };
			pt2 = new Point { Type = PointType.Address, PostalCode = p2.Code, Country = p2.CountryCode, City = p2.City, State = p2.State };
			distance = mapQuestMiles.GetMiles(pt1, pt2);
			Console.WriteLine("Distance: {0}\tMessage: {1}", distance, mapQuestMiles.LastErrorMessage);
			distance = mapQuestMiles.GetKilometers(pt1, pt2);
            Console.WriteLine("Distance: {0}\tMessage: {1}", distance, mapQuestMiles.LastErrorMessage);
		}
	}
}
