﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Extern.Edi;
using LogisticsPlus.Eship.Extern.Edi.Segments;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using NUnit.Framework;
using Contact = LogisticsPlus.Eship.Extern.Edi.Segments.Contact;
using ShipmentStatus = LogisticsPlus.Eship.Extern.Edi.Segments.ShipmentStatus;

namespace LogisticsPlus.Eship.Tests.Edi
{
	[TestFixture]
	[Ignore("Misc Test. Run on demand only")]
	public class ExternEdiSamplesTests
	{
		const string FutureUse = "Future Use";
		private Shipment _shipment;

		[SetUp]
		public void Setup()
		{
			GlobalTestInitializer.Initialize();
			_shipment = new ShipmentSearch().FetchShipmentByShipmentNumber("56750", GlobalTestInitializer.DefaultTenantId);
		}

		[Test]
		public void SerializeEdi204()
		{
			Assert.IsNotNull(_shipment);

			var controlNumber = DateTime.Now.ToString("yyyyMMddHHmmss");
			
			var oids = _shipment.Items
			                   .Select(i => new OrderIdentificationDetail
				                   {
					                   DimensionUnitQualifier = DimsUnitQualifier.I,
					                   Height = i.ActualHeight,
					                   Width = i.ActualWidth,
					                   Length = i.ActualLength,
					                   Weight = i.ActualWeight,
					                   Quantity = i.Quantity,
					                   ReferenceIdentification = i.Id.GetString(),
					                   WeightUnitCode = WghtUnitQualifier.L,
					                   NumberOfIndividualUnitsShipped = i.PieceCount,
					                   NMFCCode = i.NMFCCode,
					                   CommodityCode = FutureUse,
					                   ItemDescription = i.Description,
					                   PackageTypeCode = i.PackageType.EdiOid,
									   FreightClass = i.ActualFreightClass.ToInt() == default(int) ? string.Empty : i.ActualFreightClass.GetString() // blank freight class is zero so to int will not break code
				                   })
			                   .ToList();

			var refNums = new List<ReferenceNumber>
				{
					new ReferenceNumber
						{
							Description = "PO",
							ReferenceIdentification = _shipment.PurchaseOrderNumber,
							ReferenceIdentificationQualifier = RefIdQualifier.PO,
						},
					new ReferenceNumber
						{
							Description = "Shipper Ref",
							ReferenceIdentification = _shipment.ShipperReference,
							ReferenceIdentificationQualifier = RefIdQualifier.SHR,
						},
				};
			refNums.AddRange(_shipment.CustomerReferences.Select(r => new ReferenceNumber
				{
					ReferenceIdentification = r.Value,
					Description = r.Name,
					ReferenceIdentificationQualifier = RefIdQualifier.CUS
				}));
			var d = new Edi204
				{
					TransactionSetHeader = new TransactionSetHeader
						{
							TransactionSetControlNumber = controlNumber,
							TransactionSetIdentifierCode = TransSetIdentifierCode.LTR
						},

					Loads = new List<Load>
						{
							new Load
								{
									BeginningSegment = new BeginningSegmentShipmentInfo
										{
											CustomsDocumentHandlingCode = FutureUse,
											PaymentMethodCode = ShipMethodOfPay.TP,
											ShipmentIdNumber = _shipment.ShipmentNumber,
											ShipmentMethodOfPayment = ShipMethodOfPay.TP.GetString(),
											ShipmentQualifier = ShipmentQualifier.G.GetString(),
											ShipmentWeightCode = WghtQualifier.N.GetString(),
											StandardCarrierAlphaCode = new Tenant(GlobalTestInitializer.DefaultTenantId).TenantScac,
											StandardPointLocationCode = FutureUse,
											TariffServiceCode = FutureUse,
											TotalEquipment = 1,
											TransportationTermsCode = FutureUse,
											WeightUnitQualifier = WghtUnitQualifier.L
										},
									EquipmentDetail = new EquipmentDetail
										{
											EquipmentDescriptionCode = _shipment.Equipments.Any() ? _shipment.Equipments.First().EquipmentType.Code : "EquipCode",
											EquipmentNumber = FutureUse,
										},
									Remarks = new Remarks
										{
											FreeFormMessage = _shipment.MiscField1,
											FreeFormMessage2 = _shipment.MiscField2
										},
									LoadReferenceNumbers = refNums,
									SetPurpose = new SetPurpose
										{
											ApplicationType = SetPurposeAppType.LT,
											TransactionSetPurpose = SetPurposeTransType.OR
										},
									Stops = new List<Edi204Stop>
										{
											new Edi204Stop
												{
													LocationContact = new Contact
														{
															CommunicationNumber =
																_shipment.Origin.Contacts.Any() ? _shipment.Origin.Contacts[0].Phone : "111-111-1111",
															CommunicationNumberQualifier = CommNumberQualifier.TE,
															ContactFunctionCode = ContactFuncCode.GC,
															ContactInquiryReference = FutureUse
														},
													DateTime = DateTime.Now.FormattedLongDateAlt(),
													GeographicLocation = new GeographicLocation
														{
															StateOrProvinceCode = _shipment.Origin.State,
															CityName = _shipment.Origin.City,
															CountryCode = _shipment.Origin.Country.Code,
															LocationIdentifier = FutureUse,
															PostalCode = _shipment.Origin.PostalCode
														},
													LocationName = new Name
														{
															EntityName = _shipment.Origin.Description,
															EntityIdentifierCode = EntityIdCode.SH,
															IdentificationCode = FutureUse,
															IdentificationCodeQualifier = FutureUse
														},
													LocationStreetInformation = new AddressInformation
														{
															AddressInfo1 = _shipment.Origin.Street1,
															AddressInfo2 = _shipment.Origin.Street2
														},

													StopOffDetails = new StopOffDetails
														{
															SpecialInstructions = _shipment.Origin.SpecialInstructions,
															StopReasonCode = StopReasonCode.CL,
															StopSequenceNumber = 1
														},
													OrderIdentificationDetails = oids,
													IsHazardousMaterial = _shipment.HazardousMaterial,
													HazardousMaterialContact = new Contact
														{
															CommunicationNumber = _shipment.HazardousMaterialContactPhone,
															CommunicationNumberQualifier = CommNumberQualifier.TE,
															ContactFunctionCode = ContactFuncCode.HZ,
															ContactInquiryReference = FutureUse,
															Name = _shipment.HazardousMaterialContactName
														}
												},
											new Edi204Stop
												{
													LocationContact = new Contact
														{
															CommunicationNumber =
																_shipment.Destination.Contacts.Any() ? _shipment.Destination.Contacts[0].Phone : "222-222-2222",
															CommunicationNumberQualifier = CommNumberQualifier.TE,
															ContactFunctionCode = ContactFuncCode.GC,
															ContactInquiryReference = FutureUse
														},
													DateTime = DateTime.Now.AddDays(1).FormattedLongDateAlt(),
													GeographicLocation = new GeographicLocation
														{
															StateOrProvinceCode = _shipment.Destination.State,
															CityName = _shipment.Destination.City,
															CountryCode = _shipment.Destination.Country.Code,
															LocationIdentifier = FutureUse,
															PostalCode = _shipment.Destination.PostalCode
														},
													LocationName = new Name
														{
															EntityName = _shipment.Destination.Description,
															EntityIdentifierCode = EntityIdCode.SH,
															IdentificationCode = FutureUse,
															IdentificationCodeQualifier = FutureUse
														},
													LocationStreetInformation = new AddressInformation
														{
															AddressInfo1 = _shipment.Destination.Street1,
															AddressInfo2 = _shipment.Destination.Street2
														},

													StopOffDetails = new StopOffDetails
														{
															SpecialInstructions = _shipment.Destination.SpecialInstructions,
															StopReasonCode = StopReasonCode.CU,
															StopSequenceNumber = 2
														},
													OrderIdentificationDetails = oids,
													IsHazardousMaterial = _shipment.HazardousMaterial,
													HazardousMaterialContact = new Contact
														{
															CommunicationNumber = _shipment.HazardousMaterialContactPhone,
															CommunicationNumberQualifier = CommNumberQualifier.TE,
															ContactFunctionCode = ContactFuncCode.HZ,
															ContactInquiryReference = FutureUse,
															Name = _shipment.HazardousMaterialContactName
														}
												},
										},
									ShipmentWeightAndTrackingData = new ShipmentWghtPkgAndQtyData
										{
											LadingPackages = _shipment.Items.Sum(i => i.Quantity),
											LadingPieces = _shipment.Items.Sum(i => i.PieceCount),
											Weight = _shipment.Items.Sum(i => i.ActualWeight),
											WeightUnitCode = WghtUnitQualifier.L
										},
									ExpirationDate = DateTime.Now.AddHours(2).FormattedLongDateAlt(),
									HandlingRequirements = _shipment.Services.Any()
									? _shipment.Services
									                                .Select(s => new HandlingRequirement
										                                {
											                                SpecialHandlingCode = FutureUse,
											                                SpecialHandlingDescription = FutureUse,
											                                SpecialServicesCode = s.Service.Code
										                                })
									                                .ToList()
										: new List<HandlingRequirement>
											{
												new HandlingRequirement
													{
														SpecialHandlingCode = FutureUse,
														SpecialHandlingDescription = FutureUse,
														SpecialServicesCode = "CODE"
													}
											}
								}
						},
					TransactionSetTrailer = new TransactionSetTrailer
						{
							TransactionSetControlNumber = controlNumber,
							NumberOfIncludedSegments = 1
						}
				};
			Console.WriteLine(d.ToXml());
		}

		[Test]
		public void DeserializeEdi204()
		{
			var s = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
			s +=
				"<Edi204 xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><TransactionSetHeader><TransactionSetIdentifierCode>LTR</TransactionSetIdentifierCode><TransactionSetControlNumber>000097643</TransactionSetControlNumber></TransactionSetHeader><Loads><Load><BeginningSegment><StandardPointLocationCode /><WeightUnitQualifier>L</WeightUnitQualifier><TotalEquipment>0</TotalEquipment><ShipmentWeightCode></ShipmentWeightCode><CustomsDocumentHandlingCode></CustomsDocumentHandlingCode><PaymentMethodCode>TP</PaymentMethodCode><TariffServiceCode /><StandardCarrierAlphaCode>LGPL</StandardCarrierAlphaCode><ShipmentIdNumber>00011864</ShipmentIdNumber><ShipmentMethodOfPayment>CC</ShipmentMethodOfPayment><ShipmentQualifier></ShipmentQualifier><TransportationTermsCode /></BeginningSegment><SetPurpose><TransactionSetPurpose>OR</TransactionSetPurpose><ApplicationType>LT</ApplicationType></SetPurpose><ExpirationDate>2014-11-13</ExpirationDate><LoadReferenceNumbers><ReferenceNumber><ReferenceIdentificationQualifier>SBL</ReferenceIdentificationQualifier><ReferenceIdentification>00011864</ReferenceIdentification><Description /></ReferenceNumber></LoadReferenceNumbers><HandlingRequirements><HandlingRequirement><SpecialHandlingCode /><SpecialServicesCode /><SpecialHandlingDescription /></HandlingRequirement></HandlingRequirements><Remarks><FreeFormMessage>98.9</FreeFormMessage><FreeFormMessage2></FreeFormMessage2></Remarks><EquipmentDetail><EquipmentNumber /><EquipmentDescriptionCode>CV</EquipmentDescriptionCode></EquipmentDetail><Stops><Edi204Stop><StopOffDetails><StopSequenceNumber>1</StopSequenceNumber><StopReasonCode>CL</StopReasonCode><SpecialInstructions></SpecialInstructions></StopOffDetails><DateTime>2014-11-26T08:26</DateTime><LocationName><EntityIdentifierCode>SF</EntityIdentifierCode><EntityName>SHL</EntityName><IdentificationCodeQualifier /><IdentificationCode /></LocationName><LocationStreetInformation><AddressInfo1>940 WASHBURN SWITCH ROAD</AddressInfo1><AddressInfo2></AddressInfo2></LocationStreetInformation><GeographicLocation><CityName>SHELBY</CityName><StateOrProvinceCode>NC</StateOrProvinceCode><PostalCode>28150</PostalCode><CountryCode>US</CountryCode><LocationIdentifier /></GeographicLocation><LocationContact><ContactFunctionCode>GC</ContactFunctionCode><CommunicationNumberQualifier>TE</CommunicationNumberQualifier><CommunicationNumber></CommunicationNumber><ContactInquiryReference /></LocationContact><OrderIdentificationDetails><OrderIdentificationDetail><ReferenceIdentification>20141121-0007</ReferenceIdentification><Quantity>2</Quantity><NumberOfIndividualUnitsShipped>0</NumberOfIndividualUnitsShipped><WeightUnitCode>L</WeightUnitCode><Weight>35000</Weight><Length>0</Length><Width>0</Width><Height>0</Height><DimensionUnitQualifier>I</DimensionUnitQualifier><ItemDescription>PPG Goods</ItemDescription><CommodityCode /><PackageTypeCode>PC</PackageTypeCode></OrderIdentificationDetail></OrderIdentificationDetails><IsHazardousMaterial>false</IsHazardousMaterial></Edi204Stop><Edi204Stop><StopOffDetails><StopSequenceNumber>2</StopSequenceNumber><StopReasonCode>CU</StopReasonCode><SpecialInstructions></SpecialInstructions></StopOffDetails><DateTime>2014-11-26T13:43</DateTime><LocationName><EntityIdentifierCode>ST</EntityIdentifierCode><EntityName>LEX</EntityName><IdentificationCodeQualifier /><IdentificationCode /></LocationName><LocationStreetInformation><AddressInfo1>P. O. BOX 949 473 NEW JERSEY CHURCH ROAD</AddressInfo1><AddressInfo2></AddressInfo2></LocationStreetInformation><GeographicLocation><CityName>LEXINGTON</CityName><StateOrProvinceCode>NC</StateOrProvinceCode><PostalCode>27292</PostalCode><CountryCode>US</CountryCode><LocationIdentifier /></GeographicLocation><LocationContact><ContactFunctionCode>GC</ContactFunctionCode><CommunicationNumberQualifier>TE</CommunicationNumberQualifier><CommunicationNumber></CommunicationNumber><ContactInquiryReference /></LocationContact><OrderIdentificationDetails><OrderIdentificationDetail><ReferenceIdentification>20141121-0007</ReferenceIdentification><Quantity>2</Quantity><NumberOfIndividualUnitsShipped>0</NumberOfIndividualUnitsShipped><WeightUnitCode>L</WeightUnitCode><Weight>35000</Weight><Length>0</Length><Width>0</Width><Height>0</Height><DimensionUnitQualifier>I</DimensionUnitQualifier><ItemDescription>PPG Goods</ItemDescription><CommodityCode /><PackageTypeCode>PC</PackageTypeCode></OrderIdentificationDetail></OrderIdentificationDetails><IsHazardousMaterial>false</IsHazardousMaterial></Edi204Stop></Stops><ShipmentWeightAndTrackingData><WeightUnitCode>L</WeightUnitCode><Weight>35000</Weight><LadingPieces>1</LadingPieces><LadingPackages>1</LadingPackages></ShipmentWeightAndTrackingData></Load></Loads><TransactionSetTrailer><NumberOfIncludedSegments>1</NumberOfIncludedSegments><TransactionSetControlNumber>000097643</TransactionSetControlNumber></TransactionSetTrailer></Edi204>";

			var edi204 = s.FromXml<Edi204>();

			Assert.IsNotNull(edi204);

			Console.WriteLine("s:{0}\tdt:{1}",edi204.Loads[0].Stops[0].DateTime, edi204.Loads[0].Stops[0].DateTime.ToDateTime());
		}

		[Test]
		public void SerializeEdi210()
		{
			Assert.IsNotNull(_shipment);

			var controlNumber = DateTime.Now.ToString("yyyyMMddHHmmss");

			// stops and items info
			var originStopTime = _shipment.Origin.AppointmentDateTime == DateUtility.SystemEarliestDateTime
									 ? _shipment.DesiredPickupDate
									 : _shipment.Origin.AppointmentDateTime;
			var destStopTime = _shipment.Destination.AppointmentDateTime == DateUtility.SystemEarliestDateTime
								   ? _shipment.EstimatedDeliveryDate
								   : _shipment.Destination.AppointmentDateTime;

			var allStops = _shipment.Stops;
			allStops.Insert(0, _shipment.Origin);
			allStops.Add(_shipment.Destination);
			var pItems = _shipment.Items.GroupBy(u => u.Pickup).ToDictionary(u => u.Key, u => u.Select(x => x));
			var dItems = _shipment.Items.GroupBy(u => u.Delivery).ToDictionary(u => u.Key, u => u.Select(x => x));

			var ediStopsData = allStops
				.SelectMany(s => pItems
									 .Where(di => di.Key == s.StopOrder)
									 .Select(x => new
									 {
										 Stop = s,
										 IsShipper = s.StopOrder == _shipment.Origin.StopOrder,
										 IsConsignee = s.StopOrder == _shipment.Destination.StopOrder,
										 IsPickup = true,
										 Item = x.Value
									 }))
				.ToList();
			ediStopsData.AddRange(
				allStops
					.SelectMany(s => dItems
										 .Where(di => di.Key == s.StopOrder)
										 .Select(x => new
										 {
											 Stop = s,
											 IsShipper = s.StopOrder == _shipment.Origin.StopOrder,
											 IsConsignee = s.StopOrder == _shipment.Destination.StopOrder,
											 IsPickup = false,
											 Item = x.Value
										 })
										 .ToList()));

			// references
			var refNums = new List<ReferenceNumber>
				{
					new ReferenceNumber
						{
							Description = "PO",
							ReferenceIdentification = _shipment.PurchaseOrderNumber,
							ReferenceIdentificationQualifier = RefIdQualifier.PO,
						},
					new ReferenceNumber
						{
							Description = "Shipper Ref",
							ReferenceIdentification = _shipment.ShipperReference,
							ReferenceIdentificationQualifier = RefIdQualifier.SHR,
						},
				};
			refNums.AddRange(_shipment.CustomerReferences.Select(r => new ReferenceNumber
			{
				ReferenceIdentification = r.Value,
				Description = r.Name,
				ReferenceIdentificationQualifier = RefIdQualifier.CUS
			}));

			var sv = _shipment.Vendors.First(v => v.Primary);
			var chg = _shipment.Charges.First();
			var d = new Edi210
				{
					TransactionSetHeader = new TransactionSetHeader
						{
							TransactionSetControlNumber = controlNumber,
							TransactionSetIdentifierCode = TransSetIdentifierCode.CSI
						},
					Invoices = new List<FreightInvoice>
						{
							new FreightInvoice
								{
									BeginningSegment = new BeginningSegmentFreightInv
										{
											CorrectionIndicator = CorrectionIndicator.NA,
											Date = DateTime.Now.AddDays(30).FormattedLongDateAlt(),
											PickupDateTimeQualifier = FreightInvDateQualifier.ACP,
											PickupDate = DateTime.Now.AddDays(-2).FormattedLongDateAlt(),
											DeliveryDateTimeQualifier = FreightInvDateQualifier.ACD,
											DeliveryDate = DateTime.Now.AddDays(-1).FormattedLongDateAlt(),
											InvoiceDate = DateTime.Now.FormattedLongDateAlt(),
											InvoiceNumber = DateTime.Now.ToString("yyyyffff"),
											NetAmountDue = 1000.0000m,
											ShipmentIdNumber = _shipment.ShipmentNumber,
											ShipmentMethodOfPay = ShipMethodOfPay.TP,
											ShipmentQualifier = ShipmentQualifier.G,
											StandardCarrierAlphaCode = sv.Vendor.Scac,
											TariffServiceCode = FutureUse,
											TransportationTermsCode = FutureUse,
											WeightUnitCode = WghtUnitQualifier.L,
											Equipment = new EquipmentDetail
												{
													EquipmentDescriptionCode = "Equip. Code",
													EquipmentNumber = FutureUse
												}
										},


									ReferenceNumbers = refNums,

									Remarks = new Remarks
										{
											FreeFormMessage = "Msg 1",
											FreeFormMessage2 = "Msg 2"
										},

									FreightInvoiceDetails = new List<FreightInvoiceDetail>
										{
											new FreightInvoiceDetail
												{
													Charge = chg.AmountDue,
													ChargeCode = chg.ChargeCode.Code,
													ChargeCodeDescription = chg.ChargeCode.Description,
													LadingQuantity = _shipment.Items.Sum(i => i.Quantity),
													Remarks = new Remarks
														{
															FreeFormMessage = chg.Comment,
															FreeFormMessage2 = string.Empty,
														},
													Weight = _shipment.Items.Sum(i => i.ActualWeight),
													WeightUnitCode = WghtUnitQualifier.L,
													LadingPieces = _shipment.Items.Sum(i => i.PieceCount)
												}
										},
									Stops = ediStopsData
										.Select(s =>
											{
												var locContact = s.Stop.Contacts.FirstOrDefault(c => c.Primary) ?? s.Stop.Contacts.FirstOrDefault();
												return new Edi204Stop
													{
														DateTime = (s.IsShipper ? originStopTime : s.IsConsignee ? destStopTime : s.Stop.AppointmentDateTime).FormattedLongDateAlt(),
														HazardousMaterialContact = new Contact
															{
																CommunicationNumber = _shipment.HazardousMaterialContactPhone,
																CommunicationNumberQualifier = CommNumberQualifier.TE,
																ContactFunctionCode = ContactFuncCode.HZ,
																ContactInquiryReference = string.Empty,
																Name = _shipment.HazardousMaterialContactName
															},
														IsHazardousMaterial = _shipment.HazardousMaterial,
														LocationContact = locContact == null
															                  ? null
															                  : new Contact
																                  {
																	                  CommunicationNumber = locContact.Phone,
																	                  CommunicationNumberQualifier = CommNumberQualifier.TE,
																	                  ContactFunctionCode = ContactFuncCode.GC,
																	                  ContactInquiryReference = string.Empty,
																	                  Name = locContact.Name
																                  },
														GeographicLocation = new GeographicLocation
															{
																CityName = s.Stop.City,
																CountryCode = s.Stop.Country == null ? string.Empty : s.Stop.Country.Code,
																LocationIdentifier = string.Empty,
																LocationQualifier = string.Empty,
																PostalCode = s.Stop.PostalCode,
																StateOrProvinceCode = s.Stop.State
															},
														LocationName = new Name
															{
																EntityIdentifierCode = s.IsShipper
																	                       ? EntityIdCode.SH
																	                       : s.IsConsignee
																		                         ? EntityIdCode.CN
																		                         : s.IsPickup ? EntityIdCode.SF : EntityIdCode.ST,
																EntityName = s.Stop.Description,
																IdentificationCode = string.Empty,
																IdentificationCodeQualifier = string.Empty
															},
														LocationStreetInformation = new AddressInformation
															{
																AddressInfo1 = s.Stop.Street1,
																AddressInfo2 = s.Stop.Street2
															},
														OrderIdentificationDetails = s.Item
														                              .Select(i => new OrderIdentificationDetail
															                              {
																                              DimensionUnitQualifier = DimsUnitQualifier.I,
																                              Height = i.ActualHeight,
																							  Length = i.ActualLength,
																                              Quantity = i.Quantity,
																                              ReferenceIdentification = i.Description,
																							  Weight = i.ActualWeight,
																                              WeightUnitCode = WghtUnitQualifier.L,
																							  Width = i.ActualWidth,
																                              NumberOfIndividualUnitsShipped = i.PieceCount,
																                              NMFCCode = i.NMFCCode,
																                              CommodityCode = string.Empty,
																                              ItemDescription = i.Description,
																                              PackageTypeCode = i.PackageType.EdiOid,
																							  FreightClass = i.ActualFreightClass.ToInt() == default(int) ? string.Empty : i.ActualFreightClass.GetString() // blank freight class is zero so to int will not break code
															                              })
														                              .ToList(),
														StopOffDetails = new StopOffDetails
															{
																SpecialInstructions = s.Stop.SpecialInstructions,
																StopReasonCode =
																	s.IsShipper
																		? StopReasonCode.CL
																		: s.IsConsignee ? StopReasonCode.UL : s.IsPickup ? StopReasonCode.PL : StopReasonCode.PU,
																StopSequenceNumber = s.Stop.StopOrder
															}
													};
											})
										.ToList()

								}
						},
					TransactionSetTrailer = new TransactionSetTrailer
						{
							TransactionSetControlNumber = controlNumber,
							NumberOfIncludedSegments = 1
						}

				};
			Console.WriteLine(d.ToXml());
		}

		[Test]
		public void SerializeEdi214()
		{
			Assert.IsNotNull(_shipment);

			var controlNumber = DateTime.Now.ToString("yyyyMMddHHmmss");

			var sv = _shipment.Vendors.First(v => v.Primary);
			var d = new Edi214
				{
					TransactionSetHeader = new TransactionSetHeader
						{
							TransactionSetControlNumber = controlNumber,
							TransactionSetIdentifierCode = TransSetIdentifierCode.SSM
						},
					Statuses = new List<ShipmentStatus>
						{
							new ShipmentStatus
								{
									BeginningSegment = new BeginningSegmentShipStatMsg
										{
											ReferenceIdentification = "Pro Number",
											ReferenceIdentificationQualifier = RefIdQualifier.CN,
											ShipmentIdentificationNumber = _shipment.ShipmentNumber,
											StandardCarrierAlphaCode = sv.Vendor.Scac
										},

									Remarks = new Remarks
										{
											FreeFormMessage = "Notes",
											FreeFormMessage2 = "Msg Line 2"
										},
									Location = new EdiLocation
										{
											GeographicLocation = new GeographicLocation
												{
													StateOrProvinceCode = "PA",
													CityName = "Erie",
													CountryCode = "US",
													LocationIdentifier = FutureUse,
													PostalCode = "16501"
												},
											LocationName = new Name
												{
													EntityName = "Logistics Plus",
													EntityIdentifierCode = EntityIdCode.SH,
													IdentificationCode = FutureUse,
													IdentificationCodeQualifier = FutureUse
												},
											LocationStreetInformation = new AddressInformation
												{
													AddressInfo1 = "1406 Peach St.",
													AddressInfo2 = string.Empty
												}
										},
									EquipmentDetail = new EquipmentDetail
										{
											EquipmentDescriptionCode = "Equip Code",
											EquipmentNumber = "Equip Number"
										},
									DateTime = DateTime.Now.AddDays(-1).FormattedLongDateAlt(),
									ShipmentStatusReasonCode = ShipStatReasonCode.NS.GetString(),
									ShipmentStatusCode = ShipStatMsgCode.AF,
									ShipmentWghtPkgAndQtyData = new ShipmentWghtPkgAndQtyData
										{
											LadingPackages = _shipment.Items.Sum(i => i.Quantity),
											LadingPieces = _shipment.Items.Sum(i => i.PieceCount),
											Weight = _shipment.Items.Sum(i => i.ActualWeight),
											WeightUnitCode = WghtUnitQualifier.L
										},

								}
						},
					TransactionSetTrailer = new TransactionSetTrailer
						{
							TransactionSetControlNumber = controlNumber,
							NumberOfIncludedSegments = 1
						}

				};
			Console.WriteLine(d.ToXml());
		}

		[Test]
		public void Deserialize214()
		{
			var s = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
			s +=
				"<Edi214><TransactionSetHeader><TransactionSetIdentifierCode>SSM</TransactionSetIdentifierCode><TransactionSetControlNumber>000010444</TransactionSetControlNumber></TransactionSetHeader><Statuses><ShipmentStatus><BeginningSegment><ShipmentIdentificationNumber>NS</ShipmentIdentificationNumber><StandardCarrierAlphaCode>HMES</StandardCarrierAlphaCode><ReferenceIdentification>1034325772</ReferenceIdentification><ReferenceIdentificationQualifier>CN</ReferenceIdentificationQualifier></BeginningSegment><Location><LocationName><EntityIdentifierCode>EL</EntityIdentifierCode><EntityName /></LocationName><LocationStreetInformation><AddressInfo1 /><AddressInfo2 /></LocationStreetInformation><GeographicLocation><CityName>MEADVILLE</CityName><StateOrProvinceCode>PA</StateOrProvinceCode><PostalCode /><CountryCode></CountryCode></GeographicLocation></Location><ShipmentStatusCode>AF</ShipmentStatusCode><ShipmentStatusReasonCode>NS</ShipmentStatusReasonCode><DateTime>2014-11-11T16:21</DateTime><Remarks><FreeFormMessage></FreeFormMessage><FreeFormMessage2></FreeFormMessage2></Remarks><EquipmentDetail><EquipmentNumber></EquipmentNumber><EquipmentDescriptionCode></EquipmentDescriptionCode></EquipmentDetail><ShipmentWghtPkgAndQtyData><WeightUnitCode>L</WeightUnitCode><Weight>3112</Weight><LadingPieces>2</LadingPieces><LadingPackages>0</LadingPackages></ShipmentWghtPkgAndQtyData></ShipmentStatus><ShipmentStatus><BeginningSegment><ShipmentIdentificationNumber>NS</ShipmentIdentificationNumber><StandardCarrierAlphaCode>HMES</StandardCarrierAlphaCode><ReferenceIdentification>1034325772</ReferenceIdentification><ReferenceIdentificationQualifier>CN</ReferenceIdentificationQualifier></BeginningSegment><Location><LocationName><EntityIdentifierCode>EL</EntityIdentifierCode><EntityName /></LocationName><LocationStreetInformation><AddressInfo1 /><AddressInfo2 /></LocationStreetInformation><GeographicLocation><CityName>CANTON</CityName><StateOrProvinceCode>OH</StateOrProvinceCode><PostalCode /><CountryCode></CountryCode></GeographicLocation></Location><ShipmentStatusCode>AG</ShipmentStatusCode><ShipmentStatusReasonCode>NS</ShipmentStatusReasonCode><DateTime>2014-11-12T10:00</DateTime><Remarks><FreeFormMessage></FreeFormMessage><FreeFormMessage2></FreeFormMessage2></Remarks><EquipmentDetail><EquipmentNumber></EquipmentNumber><EquipmentDescriptionCode></EquipmentDescriptionCode></EquipmentDetail><ShipmentWghtPkgAndQtyData><WeightUnitCode>L</WeightUnitCode><Weight>3112</Weight><LadingPieces>2</LadingPieces><LadingPackages>0</LadingPackages></ShipmentWghtPkgAndQtyData></ShipmentStatus></Statuses><TransactionSetTrailer><NumberOfIncludedSegments>2</NumberOfIncludedSegments><TransactionSetControlNumber>000010444</TransactionSetControlNumber></TransactionSetTrailer></Edi214>";

			var edi214 = s.FromXml<Edi214>();
			Assert.IsNotNull(edi214);
		}

		[Test]
		public void SerializeEdi990()
		{
			Assert.IsNotNull(_shipment);

			var controlNumber = DateTime.Now.ToString("yyyyMMddHHmmss");

			var sv = _shipment.Vendors.First(v => v.Primary);
			var d = new Edi990
				{
					TransactionSetHeader = new TransactionSetHeader
						{
							TransactionSetControlNumber = controlNumber,
							TransactionSetIdentifierCode = TransSetIdentifierCode.RLT
						},
					Responses = new List<LoadResponse>
						{
							new LoadResponse
								{
									BeginningSegment = new BeginningSegmentBooking
										{
											Date = DateTime.Now.FormattedLongDateAlt(),
											ReservationActionCode = ReservationCode.A,
											ShipmentIdentificationNumber = DateTime.Now.ToString("yyyyffff"),
											StandardCarrierAlphaCode = sv.Vendor.Scac
										},
									ReferenceIdentification = new ReferenceNumber
										{
											Description = FutureUse,
											ReferenceIdentification = _shipment.ShipmentNumber,
											ReferenceIdentificationQualifier = RefIdQualifier.SBL,
										},
									ResponseToSetControlNumber = "1234567890"
								}
						},
					TransactionSetTrailer = new TransactionSetTrailer
						{
							TransactionSetControlNumber = controlNumber,
							NumberOfIncludedSegments = 1
						}

				};
			Console.WriteLine(d.ToXml());
		}

		[Test]
		public void SerializeEdi997()
		{
			Assert.IsNotNull(_shipment);

			var controlNumber = DateTime.Now.ToString("yyyyMMddHHmmss");

			var d = new Edi997
						{
							TransactionSetHeader = new TransactionSetHeader
													{
														TransactionSetControlNumber = controlNumber,
														TransactionSetIdentifierCode = TransSetIdentifierCode.ACK
													},
							FunctionalIdentifierCode = FuncIdentifierCode.IM,
							SetAcknowledgementCode = AcknowledgementCode.A,
							SetControlNumber = DateTime.Now.ToString("ffff"),
							SetIdentifierCode = TransSetIdentifierCode.SSM,
							SetSyntaxErrorCode = SyntaxErrorCode.NA,
							TransactionSetTrailer = new TransactionSetTrailer
														{
															TransactionSetControlNumber = controlNumber,
															NumberOfIncludedSegments = 1
														}

						};
			Console.WriteLine(d.ToXml());
		}
	}
}
