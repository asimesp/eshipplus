﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LogisticsPlus.Eship.CarrierIntegrations;
using LogisticsPlus.Eship.CarrierIntegrations.ImgRtrv;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.ImageRetrieval
{
    [TestFixture]
    [Ignore("Misc Test. Run on demand only")]
    public class ImgRtrvEstesTests
    {
	    private EstesImgRtrv _estesImgRtrv;
	    private ImgRtrvCriteria _criteria;

	    [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_estesImgRtrv = new EstesImgRtrv(new CarrierAuth { Username = "KSWARTWOOD", Password = "erie15" });
			_criteria = new ImgRtrvCriteria { ProNumber = "0491653600", UniqueRequestKey = "NAD214447" };
        }

        [Test]
        public void Echo()
        {
	        
			Console.WriteLine(_estesImgRtrv.Echo(_criteria));
        }

		[Test]
		public void GetImage()
		{
			var docs = _estesImgRtrv.GetImages(new List<ImgRtrvCriteria> {_criteria});

			var path = Path.GetTempPath();
			foreach (var doc in docs)
			{
				var file = Path.Combine(path, doc.Filename);
				using(var stream = new FileStream(file, FileMode.Create))
				using (var writer = new BinaryWriter(stream))
					writer.Write(doc.FileContent);
				System.Diagnostics.Process.Start(file);
			}
		}

		[Test]
		public void GetImageWithExclude()
		{
			_criteria.ExcludeDocTypes = new List<ImgRtrvDocType> {ImgRtrvDocType.BOL};
			var docs = _estesImgRtrv.GetImages(new List<ImgRtrvCriteria> {_criteria});
			_criteria.ExcludeDocTypes = null;

			Assert.IsTrue(docs.All(d => d.Type != ImgRtrvDocType.BOL));

			var path = Path.GetTempPath();
			foreach (var doc in docs)
			{
				var file = Path.Combine(path, doc.Filename);
				using (var stream = new FileStream(file, FileMode.Create))
				using (var writer = new BinaryWriter(stream))
					writer.Write(doc.FileContent);
				System.Diagnostics.Process.Start(file);
			}
		}
    }
}
