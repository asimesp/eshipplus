﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using LogisticsPlus.Eship.CarrierIntegrations;
using LogisticsPlus.Eship.CarrierIntegrations.ImgRtrv;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.ImageRetrieval
{
    [TestFixture]
    [Ignore("Misc Test. Run on demand only")]
    public class ImgRtrvDaytonFreightTests
    {
        private DaytonFreightImgRtrv _daytonFreightImgRtrv;
        private ImgRtrvCriteria _criteria;

        [SetUp]
        public void Initialize()
        { 
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
			_daytonFreightImgRtrv = new DaytonFreightImgRtrv(new CarrierAuth { Username = "KATHYF", Password = "erie15" });
            _criteria = new ImgRtrvCriteria { ProNumber = "364439002", UniqueRequestKey = "NAD12345" };
        }


        [Test]
        public void GetImage()
        {
            var docs = _daytonFreightImgRtrv.GetImages(new List<ImgRtrvCriteria> { _criteria });

            var path = Path.GetTempPath();
            foreach (var doc in docs)
            {
                var file = Path.Combine(path, doc.Filename);
                using (var stream = new FileStream(file, FileMode.Create))
                using (var writer = new BinaryWriter(stream))
                    writer.Write(doc.FileContent);
                System.Diagnostics.Process.Start(file);
            }
        }

        [Test]
        public void GetImageWithExclude()
        {
            _criteria.ExcludeDocTypes = new List<ImgRtrvDocType> { ImgRtrvDocType.BOL };
            var docs = _daytonFreightImgRtrv.GetImages(new List<ImgRtrvCriteria> { _criteria });
            _criteria.ExcludeDocTypes = null;

            Assert.IsTrue(docs.All(d => d.Type != ImgRtrvDocType.BOL));

            var path = Path.GetTempPath();
            foreach (var doc in docs)
            {
                var file = Path.Combine(path, doc.Filename);
                using (var stream = new FileStream(file, FileMode.Create))
                using (var writer = new BinaryWriter(stream))
                    writer.Write(doc.FileContent);
                System.Diagnostics.Process.Start(file);
            }
        }
    }
}
