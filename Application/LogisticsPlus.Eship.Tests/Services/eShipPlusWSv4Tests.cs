﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Tests.eShipPlusWSv4;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Services
{
    [TestFixture]
    [Ignore("You must set the correct path for the webservice before testing")]
    public class eShipPlusWSv4Tests
    {
        private const string ServerPath1 = "http://dev.logisticsplus.net/EShipV4/Services/eShipPlusWSv4.asmx";
        private const string ServerPath2 = "http://lpdev.eshipplus.com/Services/eShipPlusWSv4.asmx";
        private const string ServerPath3 = "http://www.eshipplus.com/Services/eShipPlusWSv4.asmx";
        private const string LocalPath = "http://localhost:58058/Services/eShipPlusWSv4.asmx";

        private static eShipPlusWSv4.eShipPlusWSv4 Service
        {
            get
            {
                var token = new AuthenticationToken
                {
                    AccessKey = new Guid("edbeb659-0d40-4dbf-b584-fa30ae4cbb89"),
                    Password = "$$c134n",
                    Username = "soclean.api",
                    AccessCode = "TENANT1"
                };
                return new eShipPlusWSv4.eShipPlusWSv4 { Url = LocalPath, AuthenticationTokenValue = token };
            }
        }

        private static eShipPlusWSv4.eShipPlusWSv4 ServiceForOverrideTests
        {
            get
            {
                var token = new AuthenticationToken
                {
                    AccessKey = new Guid("9DD08C2C-0753-45F6-8D5B-8553F635E0E2"),
                    Password = "admintest",
                    Username = "chris",
                    AccessCode = "TENANT1"
                };
                return new eShipPlusWSv4.eShipPlusWSv4 { Url = LocalPath, AuthenticationTokenValue = token };
            }
        }

        [Test]
        public void CanGetAccessorialsServices()
        {
            var services = Service.GetAccessorialServices();

            if (services.Count() == 1 && services[0].Messages.Any())
            {
                Console.WriteLine("\nMessage(s):");
                foreach (var message in services[0].Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }
            else
            {
                Console.WriteLine("\n\nServices: ...");
                foreach (var s in services)
                    Console.WriteLine("Service Code: {0}, \t Description: {1}, \t Billing Code: {2}",
                                      s.ServiceCode, s.ServiceDescription, s.BillingCode);
            }
        }

        [Test]
        public void InvalidAccessKeyTest()
        {
            var service = Service;
            service.AuthenticationTokenValue.AccessKey = Guid.Empty;
            var freightClasses = service.GetLTLFreightClasses();

            if (freightClasses.Count() == 1 && freightClasses[0].Messages.Any())
            {
                Console.WriteLine("\nMessage(s):");
                foreach (var message in freightClasses[0].Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }
            else
            {
                Console.WriteLine("\n\nFreight Classes: ...");
                foreach (var fc in freightClasses)
                    Console.WriteLine("Freight Class: {0}", fc.FreightClass);
            }
        }

        [Test]
        public void CanGetFreightClasses()
        {
            var freightClasses = Service.GetLTLFreightClasses();

            if (freightClasses.Count() == 1 && freightClasses[0].Messages.Any())
            {
                Console.WriteLine("\nMessage(s):");
                foreach (var message in freightClasses[0].Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }
            else
            {
                Console.WriteLine("\n\nFreight Classes: ...");
                foreach (var fc in freightClasses)
                    Console.WriteLine("Freight Class: {0}", fc.FreightClass);
            }
        }

        [Test]
        public void CanGetCountries()
        {
            var countries = Service.GetCountries();

            if (countries.Count() == 1 && countries[0].Messages.Any())
            {
                Console.WriteLine("\nMessage(s):");
                foreach (var message in countries[0].Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }
            else
            {
                Console.WriteLine("\n\nCountries: ...");
                foreach (var s in countries)
                    Console.WriteLine("Code: {0}, \t Name: {1}, \t Uses Postal Code: {2}",
                                      s.Code, s.Name, s.UsesPostalCode);
            }
        }

        [Test]
        public void CanGetItemPackaging()
        {
            var packaging = Service.GetItemPackaging();

            if (packaging.Count() == 1 && packaging[0].Messages.Any())
            {
                Console.WriteLine("\nMessage(s):");
                foreach (var message in packaging[0].Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }
            else
            {
                Console.WriteLine("\n\nPackaging: ...");
                foreach (var p in packaging)
                    Console.WriteLine("Key: {0}, \t Name: {1}, \t Default Length: {2}, \t Default Width: {3}, \t Default Height: {4}",
                                      p.Key, p.PackageName, p.DefaultLength, p.DefaultWidth, p.DefaultHeight);
            }
        }

        [Test]
        public void CanGetTimes()
        {
            var times = Service.GetTimes();

            if (times.Count() == 1 && times[0].Messages.Any())
            {
                Console.WriteLine("\nMessage(s):");
                foreach (var message in times[0].Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }
            else
            {
                Console.WriteLine("\n\nTimes: ...");
                foreach (var t in times)
                    Console.WriteLine("Time: {0}", t.Time);
            }
        }

        [Test]
        public void CanGetAllOptions()
        {
            var options = Service.GetInputOptions();

            if (options.Times != null && options.Times.Count() == 1 && options.Times[0].Messages.Any())
            {
                Console.WriteLine("\nTime Message(s):");
                foreach (var message in options.Times[0].Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }

            if (options.AccessorialServices != null && options.AccessorialServices.Count() == 1 && options.AccessorialServices[0].Messages.Any())
            {
                Console.WriteLine("\nAccessorial Message(s):");
                foreach (var message in options.AccessorialServices[0].Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }

            if (options.LTLFreightClasses != null && options.LTLFreightClasses.Count() == 1 && options.LTLFreightClasses[0].Messages.Any())
            {
                Console.WriteLine("\nFreight Class Message(s):");
                foreach (var message in options.LTLFreightClasses[0].Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }

            if (options.Countries != null && options.Countries.Count() == 1 && options.Countries[0].Messages.Any())
            {
                Console.WriteLine("\nCountries Message(s):");
                foreach (var message in options.Countries[0].Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }

            if (options.ItemPackaging != null && options.ItemPackaging.Count() == 1 && options.ItemPackaging[0].Messages.Any())
            {
                Console.WriteLine("\nItem packaging Message(s):");
                foreach (var message in options.ItemPackaging[0].Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }


        }

        [Test]
        public void CanGetCustomerReferences()
        {
            var references = Service.GetCustomerCustomReferences();

            if (references.Count() == 1 && references[0].Messages.Any())
            {
                Console.WriteLine("\nMessage(s):");
                foreach (var message in references[0].Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }
            else
            {
                Console.WriteLine("\n\nTimes: ...");
                foreach (var r in references)
                    Console.WriteLine("Name: {0}, \t Required: {1}", r.Name, r.Required);
            }
        }

        [Test]
        public void CanGetAddPONumber()
        {
            var ret = Service.AddPONumber(GetPurchaseOrder());

            if (ret.Messages == null || !ret.Messages.Any()) return;

            Console.WriteLine("\nMessage(s):");
            foreach (var message in ret.Messages)
                Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
        }

        [Test]
        public void CanRetrievePONumber()
        {
            var ret = Service.RetrievePONumber(GetPurchaseOrder().PONumber);

            if (ret.Messages != null)
            {
                Console.WriteLine("\nMessage(s):");
                foreach (var message in ret.Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }
            else
            {
                Console.WriteLine("PO Number: {0}", ret.PONumber);
            }
        }

        [Test]
        public void CanUpdatePONumber()
        {
            var po = GetPurchaseOrder();
            po.ValidPostalCode = "16504";
            po.Validity = PONumberValidity.Destination;
            var ret = Service.UpdatePONumber(po);

            if (ret.Messages == null || !ret.Messages.Any()) return;

            Console.WriteLine("\nMessage(s):");
            foreach (var message in ret.Messages)
                Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
        }

        [Test]
        public void CanDeletePONumber()
        {
            var po = GetPurchaseOrder();
            var ret = Service.DeletePONumber(po.PONumber);

            if (ret.Messages == null || !ret.Messages.Any()) return;

            Console.WriteLine("\nMessage(s):");
            foreach (var message in ret.Messages)
                Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
        }

        [Test]
        public void CanGetAddPONumbersAndDelete()
        {
            var pos = new[] { GetPurchaseOrder(), GetPurchaseOrder() };
            pos[1].PONumber = "54321";
            pos[0].PONumber += "A";

            var ret = Service.AddPONumbers(pos);

            if (ret.Messages == null || !ret.Messages.Any()) return;

            Console.WriteLine("\nMessage(s):");
            foreach (var message in ret.Messages)
                Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);

            ret = Service.DeletePONumber(pos[1].PONumber);
            foreach (var message in ret.Messages)
                Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);

            ret = Service.DeletePONumber(pos[0].PONumber);
            foreach (var message in ret.Messages)
                Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
        }

        [Test]
        public void CanRetrieveBookingRequestStatus()
        {
            var ret = Service.GetBookingRequestStatus("56530");

            if (ret.Messages != null)
            {
                Console.WriteLine("\nMessage(s):");
                foreach (var message in ret.Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }
            else
            {
                Console.WriteLine("PO Number: {0} \t Status: {1} \t Shipments: {2}", ret.BookingNumber,
                                  ret.Status, ret.Shipments.Select(s => s.ShipmentNumber).ToArray().CommaJoin());
            }
        }

        [Test]
        public void CanRetrieveShipmentStatus()
        {
            var ret = Service.GetShipmentStatus("56587");

            if (ret.Messages != null)
            {
                Console.WriteLine("\nMessage(s):");
                foreach (var message in ret.Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }
            else
            {
                Console.WriteLine("Shipment Number: {0} \t Status: {1} \t Vendor Scac: {2}", ret.ShipmentNumber, ret.Status, ret.VendorScac);
            }
        }

        [Test]
        public void CanCancelBookingRequest()
        {
            var ret = Service.RequestBookingRequestCancellation("56643");

            if (ret.Messages != null)
            {
                Console.WriteLine("\nMessage(s):");
                foreach (var message in ret.Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }
        }

        [Test]
        public void CanCancelShipment()
        {
            var ret = Service.RequestShipmentCancellation("56644");

            if (ret.Messages != null)
            {
                Console.WriteLine("\nMessage(s):");
                foreach (var message in ret.Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }
        }

        [Test]
        public void CanGetInvoice()
        {
            var ret = Service.GetInvoice("10766");

            if (ret.Messages != null)
            {
                Console.WriteLine("\nMessage(s):");
                foreach (var message in ret.Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }
            else
            {
                Console.WriteLine("\nInvioce Details:");
                Console.WriteLine("Invoice #: \t {0}", ret.InvoiceNumber);
                Console.WriteLine("Invoice Type: \t {0}", ret.Type);
                Console.WriteLine("Address #: \t {0}",

                                  new[]
				                  	{
				                  		ret.Billing.Street, ret.Billing.City, ret.Billing.State, ret.Billing.PostalCode,
				                  		ret.Billing.Country.Name
				                  	}.CommaJoin());
                Console.WriteLine("Cust. Control Acct.: \t {0}", ret.CustomerControlAccount);
                Console.WriteLine("Invoice Date: \t {0}", ret.Date);
                Console.WriteLine("Invoice Due Date: \t {0}", ret.DueDate);
                Console.WriteLine("Total Due: \t {0:c2}", ret.TotalAmountDue);
                Console.WriteLine("Freight Charges: \t {0:c2}", ret.TotalFreightCharges);
                Console.WriteLine("Fuel Charges: \t {0:c2}", ret.TotalFuelCharges);
                Console.WriteLine("Accessorial Charges: \t {0:c2}", ret.TotalAccessorialCharges);
                Console.WriteLine("Service Charges: \t {0:c2}", ret.TotalServiceCharges);
                Console.WriteLine("Invoice Terms: \t {0}", ret.Terms);

                var i = 0;
                foreach (var detail in ret.BillingDetails)
                {
                    Console.WriteLine("Line {0}: Ref #: {1} Type: {2} Code: {3} Desc: {4} Category: {5} Amount: {6:c2}", ++i,
                                      detail.ReferenceNumber, detail.ReferenceType, detail.BillingCode, detail.Description,
                                      detail.Category, detail.AmountDue);
                }
            }
        }

        [Test]
        public void CanGetInvoiceByDate()
        {
            var ret = Service.GetInvoicesByDate(DateTime.Today.AddDays(-60), DateTime.Now);

            if (ret.Count() == 1 && ret[0].Messages != null)
            {
                Console.WriteLine("\nMessage(s):");
                foreach (var message in ret[0].Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }
            else
            {
                foreach (var inv in ret)
                {
                    Console.WriteLine("\nInvioce Details:");

                    Console.WriteLine("Invoice #: \t {0}", inv.InvoiceNumber);
                    Console.WriteLine("Invoice Type.: \t {0}", inv.InvoiceType);
                    Console.WriteLine("Original Invoice #: \t {0}", inv.OriginalInvoiceNumber);
                    Console.WriteLine("Address #: \t {0}",

                                      new[]
					                  	{
					                  		inv.LocationStreet, inv.LocationCity, inv.LocationState,
					                  		inv.LocationPostalCode, inv.LocationCountryName
					                  	}.CommaJoin());


                    Console.WriteLine("Invoice Date: \t {0}", inv.Date);
                    Console.WriteLine("Invoice Due Date: \t {0}", inv.DueDate);
                    Console.WriteLine("Total Due: \t {0:c2}", inv.TotalAmount);
                    Console.WriteLine("Instructions: \t {0}", inv.SpecialInstruction);
                }
            }
        }

        [Test]
        public void CanGetInvoiceByDueDate()
        {
            var ret = Service.GetInvoicesByDueDate(DateTime.Today.AddDays(-60), DateTime.Now);

            if (ret.Count() == 1 && ret[0].Messages != null)
            {
                Console.WriteLine("\nMessage(s):");
                foreach (var message in ret[0].Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }
            else
            {
                foreach (var inv in ret)
                {
                    Console.WriteLine("\nInvioce Details:");

                    Console.WriteLine("Invoice #: \t {0}", inv.InvoiceNumber);
                    Console.WriteLine("Invoice Type.: \t {0}", inv.InvoiceType);
                    Console.WriteLine("Original Invoice #: \t {0}", inv.OriginalInvoiceNumber);
                    Console.WriteLine("Address #: \t {0}",

                                      new[]
					                  	{
					                  		inv.LocationStreet, inv.LocationCity, inv.LocationState,
					                  		inv.LocationPostalCode, inv.LocationCountryName
					                  	}.CommaJoin());


                    Console.WriteLine("Invoice Date: \t {0}", inv.Date);
                    Console.WriteLine("Invoice Due Date: \t {0}", inv.DueDate);
                    Console.WriteLine("Total Due: \t {0:c2}", inv.TotalAmount);
                    Console.WriteLine("Instructions: \t {0}", inv.SpecialInstruction);
                }
            }
        }

        [Test]
        public void CanGetInvoiceForShipment()
        {
            var ret = Service.GetInvoicesForShipment("10804");

            if (ret.Count() == 1 && ret[0].Messages != null)
            {
                Console.WriteLine("\nMessage(s):");
                foreach (var message in ret[0].Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }
            else
            {
                foreach (var inv in ret)
                {
                    Console.WriteLine("\nInvioce Details:");

                    Console.WriteLine("Invoice #: \t {0}", inv.InvoiceNumber);
                    Console.WriteLine("Invoice Type.: \t {0}", inv.InvoiceType);
                    Console.WriteLine("Original Invoice #: \t {0}", inv.OriginalInvoiceNumber);
                    Console.WriteLine("Address #: \t {0}",
                                      new[]
					                  	{
					                  		inv.LocationStreet, inv.LocationCity, inv.LocationState,
					                  		inv.LocationPostalCode, inv.LocationCountryName
					                  	}.CommaJoin());


                    Console.WriteLine("Invoice Date: \t {0}", inv.Date);
                    Console.WriteLine("Invoice Due Date: \t {0}", inv.DueDate);
                    Console.WriteLine("Total Due: \t {0:c2}", inv.TotalAmount);
                    Console.WriteLine("Instructions: \t {0}", inv.SpecialInstruction);
                }
            }
        }

        [Test]
        public void CanRateShipment()
        {
            var sh = GetShipmentForRating();
            var shipment = Service.Rate(sh);

            Console.WriteLine("\n\nRates ...");
            if (shipment.Messages != null && shipment.Messages.Any())
            {
                Console.WriteLine("\nMessage:");
                foreach (var message in shipment.Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }
            if (shipment.AvailableRates != null && shipment.AvailableRates.Any())
            {
                Console.WriteLine("\n\nRates:");
                foreach (var rate in shipment.AvailableRates)
                {
                    Console.WriteLine(
                        "Freight: {2:F2}, Fuel: {3:F2}, Accessorials: {4:F2}, Service: {5:F2}, Transit: {6}, Carrier Key: {0} \tCarrier Name: {1} \tCarrier SCAC: {7}",
                        rate.CarrierKey, rate.CarrierName, rate.FreightCharges, rate.FuelCharges, rate.AccessorialCharges,
                        rate.ServiceCharges, rate.TransitTime, rate.CarrierScac);
                    var i = 0;
                    foreach (var detail in rate.BillingDetails)
                    {
                        Console.WriteLine("Line {0}: Ref #: {1} Type: {2} Code: {3} Desc: {4} Category: {5} Amount: {6:c2}", ++i,
                                          detail.ReferenceNumber, detail.ReferenceType, detail.BillingCode, detail.Description,
                                          detail.Category, detail.AmountDue);
                    }
                    Console.WriteLine();
                    Console.WriteLine("Guaranteed Service Options: {0}", rate.GuarOptions.Count());
                    foreach (var chg in rate.GuarOptions)
                    {
                        Console.WriteLine("\t Ref #: {0} Desc: {1} Code: {2} Amount: {3:c2} Category: {4} Ref Type: {5}",
                                          chg.ReferenceNumber, chg.Description, chg.BillingCode, chg.AmountDue, chg.Category,
                                          chg.ReferenceType);
                    }
                    Console.WriteLine();
                }
            }
        }

        [Test]
        public void CanSubmitBookingRequest()
        {
            var references = Service.GetCustomerCustomReferences();
            if (references.Count() == 1 && references[0].Messages != null &&
                (references[0].Messages.Any(m => m.Type == MessageType.Error) || references[0].Messages.Any(m => m.Value.StartsWith("No custom"))))
                references = null;

            var request = GetBookingRequest(references);
            var bookingRequest = Service.SubmitBookingRequest(request);

            Console.WriteLine("\n\nBooking Request Submission [Number: {0}]...", bookingRequest.SubmittedRequestNumber);
            if (bookingRequest.Messages != null && bookingRequest.Messages.Any())
            {
                Console.WriteLine("\nMessage:");
                foreach (var message in bookingRequest.Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }
        }

        [Test]
        public void CanBookShipment()
        {
            var sh = GetShipmentForRating();
            var shipment = Service.Rate(sh);

            Console.WriteLine("\n\nRates ...");
            if (shipment.Messages != null && shipment.Messages.Any())
            {
                Console.WriteLine("\nMessage:");
                foreach (var message in shipment.Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }
            if (shipment.AvailableRates != null && shipment.AvailableRates.Any())
            {
                Console.WriteLine("\n\nRates:");
                foreach (var rate in shipment.AvailableRates)
                {
                    Console.WriteLine(
                        "Freight: {2:F2}, Fuel: {3:F2}, Accessorials: {4:F2}, Service: {5:F2}, Transit: {6}, Carrier Key: {0} \tCarrier Name: {1}",
                        rate.CarrierKey, rate.CarrierName, rate.FreightCharges, rate.FuelCharges, rate.AccessorialCharges,
                        rate.ServiceCharges, rate.TransitTime);
                    var i = 0;
                    foreach (var detail in rate.BillingDetails)
                    {
                        Console.WriteLine("Line {0}: Ref #: {1} Type: {2} Code: {3} Desc: {4} Category: {5} Amount: {6:c2}", ++i,
                                          detail.ReferenceNumber, detail.ReferenceType, detail.BillingCode, detail.Description,
                                          detail.Category, detail.AmountDue);
                    }
                    Console.WriteLine();
                }
            }

            if (shipment.AvailableRates == null || !shipment.AvailableRates.Any()) return;

            UpdateShipmentForBooking(shipment);

            shipment.SelectedRate = shipment.AvailableRates[0];

            var s2 = Service.Book(shipment);
            if (s2.Messages != null && s2.Messages.Any())
            {
                Console.WriteLine("\nMessage:");
                foreach (var message in s2.Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }

            if (s2.BookedRate != null)
            {
                var rate = s2.BookedRate;
                Console.WriteLine("Booking Shipment Number: {0}, \t Type: {1}", s2.BookingReferenceNumber, s2.BookingReferenceNumberType);
                Console.WriteLine(
                        "Freight: {2:F2}, Fuel: {3:F2}, Accessorials: {4:F2}, Service: {5:F2}, Transit: {6}, Carrier Key: {0} \tCarrier Name: {1}",
                        rate.CarrierKey, rate.CarrierName, rate.FreightCharges, rate.FuelCharges, rate.AccessorialCharges,
                        rate.ServiceCharges, rate.TransitTime);
                var i = 0;
                foreach (var detail in rate.BillingDetails)
                {
                    Console.WriteLine("Line {0}: Ref #: {1} Type: {2} Code: {3} Desc: {4} Category: {5} Amount: {6:c2}", ++i,
                                      detail.ReferenceNumber, detail.ReferenceType, detail.BillingCode, detail.Description,
                                      detail.Category, detail.AmountDue);
                }
                Console.WriteLine();
                Console.WriteLine("Has Return Confirmations: {0}", s2.ReturnConfirmations != null && s2.ReturnConfirmations.Any());
                Console.WriteLine();


            }
        }

        [Test]
        [Ignore("Only test when attempting to verify override markups through the WebService")]
        public void CanBookShipmentWithVendorFloorOverrides()
        {
            var sh = GetShipmentForRatingWithVendorFloorOverrides();
            var shipment = ServiceForOverrideTests.Rate(sh);

            Console.WriteLine("\n\nRates ...");
            if (shipment.Messages != null && shipment.Messages.Any())
            {
                Console.WriteLine("\nMessage:");
                foreach (var message in shipment.Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }

            if (shipment.AvailableRates == null || !shipment.AvailableRates.Any()) return;

            UpdateShipmentForBooking(shipment);

            // Let's examine A Duie Pyle
            shipment.SelectedRate = shipment.AvailableRates.First(ar => ar.CarrierKey == "1");

            if (shipment.SelectedRate == null)
            {
                Console.WriteLine("Could not find rate for A Duie Pyle ...");
                return;
            }

            var s2 = ServiceForOverrideTests.Book(shipment);
            if (s2.Messages != null && s2.Messages.Any())
            {
                Console.WriteLine("\nMessage:");
                foreach (var message in s2.Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }

            if (s2.BookedRate != null)
            {
                Console.WriteLine("Booking Shipment Number: {0}, \t Type: {1}", s2.BookingReferenceNumber, s2.BookingReferenceNumberType);
                Console.WriteLine("Verify that this shipment's CustomerFreightMarkupPercent and CustomerFreightMarkupValue match the Vendor Floor overrides for A Duie Pyle on the Customer Rating for Customer #10000");
            }
        }

        [Test]
        [Ignore("Only test when attempting to verify override markups through the WebService")]
        public void CanBookShipmentWithWeightBreakOverrides()
        {
            var sh = GetShipmentForRatingWithWeightBreakOverrides();
            var shipment = ServiceForOverrideTests.Rate(sh);

            Console.WriteLine("\n\nRates ...");
            if (shipment.Messages != null && shipment.Messages.Any())
            {
                Console.WriteLine("\nMessage:");
                foreach (var message in shipment.Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }

            if (shipment.AvailableRates == null || !shipment.AvailableRates.Any()) return;

            UpdateShipmentForBooking(shipment);

            // Let's examine A Duie Pyle
            shipment.SelectedRate = shipment.AvailableRates.First(ar => ar.CarrierKey == "1");

            if (shipment.SelectedRate == null)
            {
                Console.WriteLine("Could not find rate for A Duie Pyle ...");
                return;
            }

            var s2 = ServiceForOverrideTests.Book(shipment);
            if (s2.Messages != null && s2.Messages.Any())
            {
                Console.WriteLine("\nMessage:");
                foreach (var message in s2.Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }

            if (s2.BookedRate != null)
            {
                Console.WriteLine("Booking Shipment Number: {0}, \t Type: {1}", s2.BookingReferenceNumber, s2.BookingReferenceNumberType);
                Console.WriteLine("Verify that this shipment's CustomerFreightMarkupPercent and CustomerFreightMarkupValue match the M1M overrides for A Duie Pyle on the Customer Rating for Customer #10000");
            }
        }

        [Test]
        public void CanBookGuarShipment()
        {
            var sh = GetShipmentForRating();
            var shipment = Service.Rate(sh);

            Console.WriteLine("\n\nRates ...");
            if (shipment.Messages != null && shipment.Messages.Any())
            {
                Console.WriteLine("\nMessage:");
                foreach (var message in shipment.Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }
            if (shipment.AvailableRates != null && shipment.AvailableRates.Any())
            {
                Console.WriteLine("\n\nRates:");
                foreach (var rate in shipment.AvailableRates)
                {
                    Console.WriteLine(
                        "Freight: {2:F2}, Fuel: {3:F2}, Accessorials: {4:F2}, Service: {5:F2}, Transit: {6}, Carrier Key: {0} \tCarrier Name: {1}",
                        rate.CarrierKey, rate.CarrierName, rate.FreightCharges, rate.FuelCharges, rate.AccessorialCharges,
                        rate.ServiceCharges, rate.TransitTime);
                    var i = 0;
                    foreach (var detail in rate.BillingDetails)
                    {
                        Console.WriteLine("Line {0}: Ref #: {1} Type: {2} Code: {3} Desc: {4} Category: {5} Amount: {6:c2}", ++i,
                                          detail.ReferenceNumber, detail.ReferenceType, detail.BillingCode, detail.Description,
                                          detail.Category, detail.AmountDue);
                    }
                    Console.WriteLine();
                    Console.WriteLine("Guaranteed Service Options: {0}", rate.GuarOptions.Count());
                    foreach (var chg in rate.GuarOptions)
                    {
                        Console.WriteLine("\t Ref #: {0} Desc: {1} Code: {2} Amount: {3:c2} Category: {4} Ref Type: {5}",
                                          chg.ReferenceNumber, chg.Description, chg.BillingCode, chg.AmountDue, chg.Category,
                                          chg.ReferenceType);
                    }
                    Console.WriteLine();
                }
            }

            if (shipment.AvailableRates == null || !shipment.AvailableRates.Any(r => r.GuarOptions.Any())) return;

            UpdateShipmentForBooking(shipment);

            shipment.SelectedRate = shipment.AvailableRates.First(r => r.GuarOptions.Any());
            shipment.SelectedRate.SelectedGuarOption = shipment.SelectedRate.GuarOptions[0];

            var s2 = Service.Book(shipment);
            if (s2.Messages != null && s2.Messages.Any())
            {
                Console.WriteLine("\nMessage:");
                foreach (var message in s2.Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }

            if (s2.BookedRate != null)
            {
                var rate = s2.BookedRate;
                Console.WriteLine("Booking Shipment Number: {0}, \t Type: {1}", s2.BookingReferenceNumber, s2.BookingReferenceNumberType);
                Console.WriteLine(
                        "Freight: {2:F2}, Fuel: {3:F2}, Accessorials: {4:F2}, Service: {5:F2}, Transit: {6}, Carrier Key: {0} \tCarrier Name: {1}",
                        rate.CarrierKey, rate.CarrierName, rate.FreightCharges, rate.FuelCharges, rate.AccessorialCharges,
                        rate.ServiceCharges, rate.TransitTime);
                var i = 0;
                foreach (var detail in rate.BillingDetails)
                {
                    Console.WriteLine("Line {0}: Ref #: {1} Type: {2} Code: {3} Desc: {4} Category: {5} Amount: {6:c2}", ++i,
                                      detail.ReferenceNumber, detail.ReferenceType, detail.BillingCode, detail.Description,
                                      detail.Category, detail.AmountDue);
                }
                Console.WriteLine();
                Console.WriteLine("Has Return Confirmations: {0}", s2.ReturnConfirmations != null && s2.ReturnConfirmations.Any());
                Console.WriteLine();


            }
        }

        [Test]
        public void CanSaveQuote()
        {
            var sh = GetShipmentForRating();
            var quote = Service.Rate(sh);

            Console.WriteLine("\n\nRates ...");
            if (quote.Messages != null && quote.Messages.Any())
            {
                Console.WriteLine("\nMessage:");
                foreach (var message in quote.Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }
            if (quote.AvailableRates != null && quote.AvailableRates.Any())
            {
                Console.WriteLine("\n\nRates:");
                foreach (var rate in quote.AvailableRates)
                {
                    Console.WriteLine(
                        "Freight: {2:F2}, Fuel: {3:F2}, Accessorials: {4:F2}, Service: {5:F2}, Transit: {6}, Carrier Key: {0} \tCarrier Name: {1}",
                        rate.CarrierKey, rate.CarrierName, rate.FreightCharges, rate.FuelCharges, rate.AccessorialCharges,
                        rate.ServiceCharges, rate.TransitTime);
                    var i = 0;
                    foreach (var detail in rate.BillingDetails)
                    {
                        Console.WriteLine("Line {0}: Ref #: {1} Type: {2} Code: {3} Desc: {4} Category: {5} Amount: {6:c2}", ++i,
                                          detail.ReferenceNumber, detail.ReferenceType, detail.BillingCode, detail.Description,
                                          detail.Category, detail.AmountDue);
                    }
                    Console.WriteLine();
                }
            }

            if (quote.AvailableRates == null || !quote.AvailableRates.Any()) return;

            UpdateShipmentForBooking(quote);

            quote.SelectedRate = quote.AvailableRates[0];

            var quotedLoad = Service.SaveQuote(quote);

            if (quotedLoad.Messages != null && quotedLoad.Messages.Any())
            {
                Console.WriteLine("\nMessage:");
                foreach (var message in quotedLoad.Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }

            Console.WriteLine();
            Console.WriteLine("Has Return Confirmations: {0}", quotedLoad.ReturnConfirmations != null && quotedLoad.ReturnConfirmations.Any());
            Console.WriteLine();
        }

        [Test]
        [Ignore("Specify a valid quote/load order number before running.")]
        public void CanConvertQuoteToShipment()
        {
            const string quoteNumber = "58583";

            var newShipment = Service.ConvertQuoteToShipment(quoteNumber);

            if (newShipment.Messages != null && newShipment.Messages.Any())
            {
                Console.WriteLine("\nMessage:");
                foreach (var message in newShipment.Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }

            Console.WriteLine();
            Console.WriteLine("Has Return Confirmations: {0}", newShipment.ReturnConfirmations != null && newShipment.ReturnConfirmations.Any());
            Console.WriteLine();
        }

        [Test]
        public void CanGetShipmentDocument()
        {
            var ret = Service.GetShipmentDocument("58990", 27018);

            if (ret.Messages != null)
            {
                Console.WriteLine("\nMessage(s):");
                foreach (var message in ret.Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }
            else
            {
                var bytes = Convert.FromBase64String(ret.EncodedDocumentBytes);
                Console.WriteLine("\nDocuments:");
                Console.WriteLine("Key: \t {0}", ret.DocumentKey);
                Console.WriteLine("Name: \t {0}", ret.DocumentFileName);
               Console.WriteLine("Byte Length: \t {0}", bytes.Length);
            }
        }


        private static WSCustomerPONumber GetPurchaseOrder()
        {
            return new WSCustomerPONumber
            {
                AdditionalPurchaseOrderNotes = "Test",
                Country = new WSCountry { Code = "US" },
                CurrentUses = 0,
                ExpirationDate = DateTime.Now,
                MaximumUses = 10,
                PONumber = "12345",
                ValidPostalCode = "16501",
                Validity = PONumberValidity.Origin
            };
        }

        private static WSShipment2 GetShipmentForRating()
        {
            var shipment = new WSShipment2();

            var service1 = new WSAccessorialService
            {
                ServiceCode = "RESD",
                ServiceDescription = "Private Residence Delivery",
                BillingCode = "RESD"
            };
            var service2 = new WSAccessorialService
            {
                ServiceCode = "LGD",
                ServiceDescription = "Lift-Gate Service Delivery",
                BillingCode = "LGD"
            };

            var packaging = new WSItemPackage { Key = 258, PackageName = "Pallets" };
            var fc = new WSFreightClass { FreightClass = 77.5 };
            var item = new WSItem2
            {
                DeclaredValue = 100m,
                Description = "Test Type",
                FreightClass = fc,
                Stackable = false,
                Length = 48,
                Width = 48,
                Height = 48,
                NationalMotorFreightClassification = string.Empty,
                Packaging = packaging,
                PackagingQuantity = 1,
                SaidToContain = 1,
                Weight = 310.5m,
                Comment = string.Empty,
                HarmonizedTariffSchedule = string.Empty
            };

            var country = new WSCountry { Code = "US", Name = "United States", UsesPostalCode = true };
            var country2 = new WSCountry { Code = "CA", Name = "Canada", UsesPostalCode = true };

            //shipment.Origin = new WSLocation2 { PostalCode = "M2J3Z6", Country = country2 };
            shipment.Origin = new WSLocation2 { PostalCode = "91761", Country = country };
            shipment.Destination = new WSLocation2 { PostalCode = "93033", Country = country };
            shipment.EarliestPickup = new WSTime2 { Time = "08:00" };
            shipment.LatestPickup = new WSTime2 { Time = "17:00" };
            shipment.ShipmentDate = DateTime.Now;

            //shipment.Accessorials = new[] { service1, service2 };
            shipment.Items = new[] { item };

            shipment.DeclineAdditionalInsuranceIfApplicable = true;

            return shipment;
        }

        private static WSShipment2 GetShipmentForRatingWithVendorFloorOverrides()
        {
            var shipment = new WSShipment2();

            var service1 = new WSAccessorialService
            {
                ServiceCode = "RESD",
                ServiceDescription = "Private Residence Delivery",
                BillingCode = "RESD"
            };
            var service2 = new WSAccessorialService
            {
                ServiceCode = "LGD",
                ServiceDescription = "Lift-Gate Service Delivery",
                BillingCode = "LGD"
            };

            var packaging = new WSItemPackage { Key = 258, PackageName = "Pallets" };
            var fc = new WSFreightClass { FreightClass = 55 };
            var item = new WSItem2
            {
                DeclaredValue = 100m,
                Description = "Test Type",
                FreightClass = fc,
                Stackable = false,
                Length = 90,
                Width = 90,
                Height = 90,
                NationalMotorFreightClassification = string.Empty,
                Packaging = packaging,
                PackagingQuantity = 1,
                SaidToContain = 1,
                Weight = 200,
                Comment = string.Empty,
                HarmonizedTariffSchedule = string.Empty
            };

            var country = new WSCountry { Code = "US", Name = "United States", UsesPostalCode = true };

            shipment.Origin = new WSLocation2 { PostalCode = "44001", Country = country };
            shipment.Destination = new WSLocation2 { PostalCode = "16335", Country = country };
            shipment.EarliestPickup = new WSTime2 { Time = "08:00" };
            shipment.LatestPickup = new WSTime2 { Time = "17:00" };
            shipment.ShipmentDate = DateTime.Now;

            shipment.Items = new[] { item };

            shipment.DeclineAdditionalInsuranceIfApplicable = true;

            return shipment;
        }

        private static WSShipment2 GetShipmentForRatingWithWeightBreakOverrides()
        {
            var shipment = new WSShipment2();

            var packaging = new WSItemPackage { Key = 258, PackageName = "Pallets" };
            var fc = new WSFreightClass { FreightClass = 77.5 };
            var item = new WSItem2
            {
                DeclaredValue = 100m,
                Description = "Test Type",
                FreightClass = fc,
                Stackable = false,
                Length = 48,
                Width = 48,
                Height = 40,
                NationalMotorFreightClassification = string.Empty,
                Packaging = packaging,
                PackagingQuantity = 1,
                SaidToContain = 1,
                Weight = 1000,
                Comment = string.Empty,
                HarmonizedTariffSchedule = string.Empty
            };

            var country = new WSCountry { Code = "US", Name = "United States", UsesPostalCode = true };

            shipment.Origin = new WSLocation2 { PostalCode = "16508", Country = country };
            shipment.Destination = new WSLocation2 { PostalCode = "12345", Country = country };
            shipment.EarliestPickup = new WSTime2 { Time = "08:00" };
            shipment.LatestPickup = new WSTime2 { Time = "17:00" };
            shipment.ShipmentDate = DateTime.Now;

            shipment.Items = new[] { item };

            shipment.DeclineAdditionalInsuranceIfApplicable = true;

            return shipment;
        }

        private static void UpdateShipmentForBooking(WSShipment2 shipment)
        {
            shipment.Origin.Description = "Origin Name";
            shipment.Origin.Street = "123 Origin Street";
            shipment.Origin.StreetExtra = "123 Origin Street Extra";
            shipment.Origin.Phone = "111-111-1111";
            shipment.Origin.Contact = "John Doe";
            shipment.Origin.City = "City";
            shipment.Origin.State = "State";

            shipment.Destination.Description = "Destination Name";
            shipment.Destination.Street = "456 Destination Street";
            shipment.Destination.StreetExtra = "456 Destination Street Extra";
            shipment.Destination.Phone = "222-222-2222";
            shipment.Destination.Contact = "Jane Doe";
            shipment.Destination.City = "City";
            shipment.Destination.State = "State";

            shipment.ReferenceNumber = "20975";
            shipment.PurchaseOrder = "13181";

            if (shipment.CustomReferences != null)
                for (var i = 0; i < shipment.CustomReferences.Length; i++)
                    shipment.CustomReferences[i].Value = (i + 1).ToString();
        }

        private static WSBookingRequest GetBookingRequest(WSReference[] references)
        {
            var request = new WSBookingRequest();

            var service1 = new WSAccessorialService
            {
                ServiceCode = "RESD",
                ServiceDescription = "Private Residence Delivery",
                BillingCode = "RESD"
            };
            var service2 = new WSAccessorialService
            {
                ServiceCode = "LGD",
                ServiceDescription = "Lift-Gate Service Delivery",
                BillingCode = "LGD"
            };

            var packaging = new WSItemPackage { Key = 258, PackageName = "Pallets" };
            var fc = new WSFreightClass { FreightClass = 77.5 };
            var item = new WSItem2
            {
                DeclaredValue = 100m,
                Description = "Test Type",
                FreightClass = fc,
                Stackable = false,
                Length = 48,
                Width = 48,
                Height = 40,
                NationalMotorFreightClassification = string.Empty,
                Packaging = packaging,
                PackagingQuantity = 1,
                SaidToContain = 1,
                Weight = 1000,
                Comment = string.Empty,
                HarmonizedTariffSchedule = string.Empty
            };

            var country = new WSCountry { Code = "US", Name = "United States", UsesPostalCode = true };
            var country2 = new WSCountry { Code = "CA", Name = "Canada", UsesPostalCode = true };

            request.Origin = new WSLocation2 { PostalCode = "M2J3Z6", Country = country2 };
            request.Destination = new WSLocation2 { PostalCode = "44445", Country = country };
            request.EarliestPickup = new WSTime2 { Time = "08:00" };
            request.LatestPickup = new WSTime2 { Time = "17:00" };
            request.DesiredShipmentDate = DateTime.Now;

            request.Accessorials = new[] { service1, service2 };
            request.Items = new[] { item, item };

            request.DeclineAdditionalInsuranceIfApplicable = true;

            request.Origin.Description = "Origin Name";
            request.Origin.Street = "123 Origin Street";
            request.Origin.Phone = "111-111-1111";
            request.Origin.Contact = "John Doe";
            request.Origin.City = "City";
            request.Origin.State = "State";

            request.Destination.Description = "Destination Name";
            request.Destination.Street = "456 Destination Street";
            request.Destination.Phone = "222-222-2222";
            request.Destination.Contact = "Jane Doe";
            request.Destination.City = "City";
            request.Destination.State = "State";

            request.ReferenceNumber = "20975";
            request.PurchaseOrder = "13181";

            request.CustomReferences = references;
            if (request.CustomReferences != null)
                for (var i = 0; i < request.CustomReferences.Length; i++)
                    request.CustomReferences[i].Value = (i + 1).ToString();

            return request;
        }
    }
}