﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Tests.eShipPlusWS;
using NUnit.Framework;
using WSCredentials = LogisticsPlus.Eship.Tests.eShipPlusWS.WSCredentials;
using WSItem = LogisticsPlus.Eship.Tests.eShipPlusWS.WSItem;
using WSLocation = LogisticsPlus.Eship.Tests.eShipPlusWS.WSLocation;
using WSOption = LogisticsPlus.Eship.Tests.eShipPlusWS.WSOption;
using WSRate = LogisticsPlus.Eship.Tests.eShipPlusWS.WSRate;
using WSShipment = LogisticsPlus.Eship.Tests.eShipPlusWS.WSShipment;
using WSTime = LogisticsPlus.Eship.Tests.eShipPlusWS.WSTime;

namespace LogisticsPlus.Eship.Tests.Services
{
	[TestFixture]
	[Ignore("You must set the correct path for the webservice before testing")]
	public class eShipPlusWSTests
	{
		private const string ServerPath = "http://lp6s/EShipV4/Services/eShipPlusWS.asmx";
		private const string LocalPath = "http://localhost:55216/Services/eShipPlusWS.asmx";

		private static WSCredentials ServiceCredentials
		{
			get
			{
				return new WSCredentials
				       	{
                            AccessKey = new Guid("49385a37-c159-4183-ba5b-7b4659db69bc"),
							Password = "admintest",
							Username = "chris",
							AccessCode = "TENANT1"
				       	};
			}
		}

        private static WSCredentials ServiceCredentialsForMarkupOverrideTests
        {
            get
            {
                return new WSCredentials
                {
                    AccessKey = new Guid("9DD08C2C-0753-45F6-8D5B-8553F635E0E2"),
                    Password = "admintest",
                    Username = "chris",
                    AccessCode = "TENANT1"
                };
            }
        }

		private static eShipPlusWS.eShipPlusWS LocalService
		{
			get { return new eShipPlusWS.eShipPlusWS {Url = LocalPath}; }
		}

		private static eShipPlusWS.eShipPlusWS ServerService
		{
			get { return new eShipPlusWS.eShipPlusWS {Url = ServerPath}; }
		}

		private static WSShipment GenericRateShipment
		{
			get
			{
				var shipment = new WSShipment();

				var service1 = new WSOption { Key = "RESD", Value = "Private Residence Delivery" };
                var service2 = new WSOption { Key = "LGD", Value = "Lift-Gate Service Delivery" };

                var country = new WSOption { Key = "US", Value = "United States" };
                var country2 = new WSOption { Key = "CA", Value = "Canada" };

                shipment.Credentials = ServiceCredentials;
                shipment.AdditionalInsuranceRequested = false;
                shipment.Origin = new WSLocation { PostalCode = "M2J3Z6", Country = country2 };
                shipment.Destination = new WSLocation { PostalCode = "44445", Country = country };
                shipment.EarliestPickup = new WSTime { Hour = 08, Minute = 00, Second = 00, TimeUnit = TimeUnit.AM };
                shipment.LatestPickup = new WSTime { Hour = 04, Minute = 00, Second = 00, TimeUnit = TimeUnit.PM };
                shipment.ShipmentDate = DateTime.Now;

                shipment.RequestedModes = new[] { Mode.LTL };
                shipment.RequestedServices = new[] { service1, service2 };
                shipment.Items = new[] { GenericItem, GenericItem };

                return shipment;
			}
		}

        private static WSShipment GenericRateShipmentForVendorFloorTest
        {
            get
            {
                var shipment = new WSShipment();

                var service1 = new WSOption { Key = "RESD", Value = "Private Residence Delivery" };
                var service2 = new WSOption { Key = "LGD", Value = "Lift-Gate Service Delivery" };

                var country = new WSOption { Key = "US", Value = "United States" };

                shipment.Credentials = ServiceCredentialsForMarkupOverrideTests;
                shipment.AdditionalInsuranceRequested = false;
                shipment.Origin = new WSLocation { PostalCode = "16508", Country = country };
                shipment.Destination = new WSLocation { PostalCode = "12345", Country = country };
                shipment.EarliestPickup = new WSTime { Hour = 08, Minute = 00, Second = 00, TimeUnit = TimeUnit.AM };
                shipment.LatestPickup = new WSTime { Hour = 04, Minute = 00, Second = 00, TimeUnit = TimeUnit.PM };
                shipment.ShipmentDate = DateTime.Now;

                shipment.RequestedModes = new[] { Mode.LTL };
                shipment.RequestedServices = new[] { service1, service2 };
                shipment.Items = new[] { GenericItemForVendorFloor };

                return shipment;
            }
        }

        private static WSShipment GenericRateShipmentForWeightBreakTest
        {
            get
            {
                var shipment = new WSShipment();

                var service1 = new WSOption { Key = "RESD", Value = "Private Residence Delivery" };
                var service2 = new WSOption { Key = "LGD", Value = "Lift-Gate Service Delivery" };

                var country = new WSOption { Key = "US", Value = "United States" };

                shipment.Credentials = ServiceCredentialsForMarkupOverrideTests;
                shipment.AdditionalInsuranceRequested = false;
                shipment.Origin = new WSLocation { PostalCode = "16508", Country = country };
                shipment.Destination = new WSLocation { PostalCode = "12345", Country = country };
                shipment.EarliestPickup = new WSTime { Hour = 08, Minute = 00, Second = 00, TimeUnit = TimeUnit.AM };
                shipment.LatestPickup = new WSTime { Hour = 04, Minute = 00, Second = 00, TimeUnit = TimeUnit.PM };
                shipment.ShipmentDate = DateTime.Now;

                shipment.RequestedModes = new[] { Mode.LTL };
                shipment.RequestedServices = new[] { service1, service2 };
                shipment.Items = new[] { GenericItem };

                return shipment;
            }
        }


		private static WSItem GenericItem
		{
			get
			{
				var packaging = new WSOption { Key = "258", Value = "Pallets" };
				var fc = new WSOption { Key = "77.5", Value = "77.5" };
				var commodityType = new WSOption { Key = "1", Value = "General Merchandise" };
				return new WSItem
				{
					CommodityType = commodityType,
					DeclaredValue = 100m,
					Description = "Test Type",
					DimsUnit = DimensionUnit.In,
					FreightClass = fc,
					IsStackable = false,
					Length = 48,
					Width = 48,
					Height = 40,
					NationalMotorFreightClassification = string.Empty,
					Packaging = packaging,
					Quantity = 1,
					SaidToContain = 1,
					Weight = 1000,
					WeightUnit = WeightUnit.Lb,
					
				};
			}
		}

        private static WSItem GenericItemForVendorFloor
        {
            get
            {
                var packaging = new WSOption { Key = "258", Value = "Pallets" };
                var fc = new WSOption { Key = "55", Value = "55" };
                var commodityType = new WSOption { Key = "1", Value = "General Merchandise" };
                return new WSItem
                {
                    CommodityType = commodityType,
                    DeclaredValue = 100m,
                    Description = "Test Type",
                    DimsUnit = DimensionUnit.In,
                    FreightClass = fc,
                    IsStackable = false,
                    Length = 90,
                    Width = 90,
                    Height = 90,
                    NationalMotorFreightClassification = string.Empty,
                    Packaging = packaging,
                    Quantity = 1,
                    SaidToContain = 1,
                    Weight = 200,
                    WeightUnit = WeightUnit.Lb,

                };
            }
        }

		[Test]
		public void CanGetCountries()
		{
			var countries = LocalService.GetCountries(ServiceCredentials);

			Console.WriteLine("\n\nCountries ...");
			if (countries.Messages != null && countries.Messages.Any())
			{
				Console.WriteLine("\nMessage:");
				foreach (var message in countries.Messages)
					Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
			}
			if (countries.Options != null && countries.Options.Any())
			{
				Console.WriteLine("\n\nCountries:");
				foreach (var option in countries.Options)
					Console.WriteLine("Key: {0}, \t Value: {1}", option.Key, option.Value);
			}
		}

		[Test]
		public void CanGetCountriesThatRequirePostalCode()
		{
			var countries = LocalService.GetCountriesRequiringPostalCodes(ServiceCredentials);

			Console.WriteLine("\n\nCountries Requiring Postal Codes ...");
			if (countries.Messages != null && countries.Messages.Any())
			{
				Console.WriteLine("\nMessage:");
				foreach (var message in countries.Messages)
					Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
			}
			if (countries.Options != null && countries.Options.Any())
			{
				Console.WriteLine("\n\nCountries Requiring Postal Codes:");
				foreach (var option in countries.Options)
					Console.WriteLine("Key: {0}, \t Value: {1}", option.Key, option.Value);
			}
		}

		[Test]
		public void CanGetCommodityTypes()
		{
			var commodityTypes = LocalService.GetCommodityTypes(ServiceCredentials);

			Console.WriteLine("\n\nCommodity types ...");
			if (commodityTypes.Messages != null && commodityTypes.Messages.Any())
			{
				Console.WriteLine("\nMessage:");
				foreach (var message in commodityTypes.Messages)
					Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
			}
			if (commodityTypes.Options != null && commodityTypes.Options.Any())
			{
				Console.WriteLine("\n\nTypes:");
				foreach (var option in commodityTypes.Options)
					Console.WriteLine("Key: {0}, \t Value: {1}", option.Key, option.Value);
			}
		}

		[Test]
		public void CanGetCities()
		{
			var cities = LocalService.GetCities(ServiceCredentials, "US");

			Console.WriteLine("\n\nCities ...");
			if (cities.Messages != null && cities.Messages.Any())
			{
				Console.WriteLine("\nMessage:");
				foreach (var message in cities.Messages)
					Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
			}
			if (cities.Options != null && cities.Options.Any())
			{
				Console.WriteLine("\n\nCities:");
				foreach (var option in cities.Options)
					Console.WriteLine("Key: {0}, \t Value: {1}", option.Key, option.Value);
			}
		}

		[Test]
		public void CanGetAvailableServices()
		{
			var services = LocalService.GetAvailableServices(ServiceCredentials, new[] { Mode.SmallPack });

			Console.WriteLine("\n\nShipping Services ...");
			if (services.Messages != null && services.Messages.Any())
			{
				Console.WriteLine("\nMessage:");
				foreach (var message in services.Messages)
					Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
			}
			if (services.Options != null && services.Options.Any())
			{
				Console.WriteLine("\n\nServices:");
				foreach (var option in services.Options)
					Console.WriteLine("Key: {0}, \t Value: {1}", option.Key, option.Value);
			}
		}

		[Test]
		public void CanGetItemPackages()
		{
			var packageReturn = LocalService.GetItemPackaging(ServiceCredentials, new[] { Mode.LTL });

			Console.WriteLine("\n\nItem Packaging ...");
			if (packageReturn.Messages != null && packageReturn.Messages.Any())
			{
				Console.WriteLine("\nMessage:");
				foreach (var message in packageReturn.Messages)
					Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
			}
			if (packageReturn.Options != null && packageReturn.Options.Any())
			{
				Console.WriteLine("\n\nItem Packages:");
				foreach (var option in packageReturn.Options)
					Console.WriteLine("Key: {0}, \t Value: {1}", option.Key, option.Value);
			}
		}

		[Test]
		public void CanGetLTLFreightClasses()
		{
			var freightClasses = LocalService.GetLTLFreightClasses(ServiceCredentials);

			Console.WriteLine("\n\nLTL Freight Classes ...");
			if (freightClasses.Messages != null && freightClasses.Messages.Any())
			{
				Console.WriteLine("\nMessage:");
				foreach (var message in freightClasses.Messages)
					Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
			}
			if (freightClasses.Options != null && freightClasses.Options.Any())
			{
				Console.WriteLine("\n\nLTL Freight Classes:");
				foreach (var option in freightClasses.Options)
					Console.WriteLine("Key: {0}, \t Value: {1}", option.Key, option.Value);
			}
		}

		[Test]
		public void CanGetShipmentStatus()
		{
			var shipments = new[] { "10500", "10662", "912020001", "912010004", "912010003", "912010002", "912010001", "1003230002", "FAKE", "11111" };
			var statusReturn = LocalService.GetShipmentStatus(ServiceCredentials, shipments);

			Console.WriteLine("\n\nShipment Status(es) ...");
			if (statusReturn.Messages != null && statusReturn.Messages.Any())
			{
				Console.WriteLine("\nMessage:");
				foreach (var message in statusReturn.Messages)
					Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
			}
			if (statusReturn.Statuses != null && statusReturn.Statuses.Any())
			{
				Console.WriteLine("\n\nStatus(es):");
				foreach (var s in statusReturn.Statuses)
					Console.WriteLine("Shipment: {0}, \t Status: {1}, \t Pro: {2}, \t Delivery Date: {3}",
						s.BillOfLading, s.Status, s.ProNumber, s.DeliveryDate);
			}
		}

		[Test]
		public void CanGetLTLShipmentRate()
		{
			var shippingRates = LocalService.GetShippingRates(GenericRateShipment);

			Console.WriteLine("\n\nShipping Rates [Generic Rate Shipment] ...");
			if (shippingRates.Messages != null && shippingRates.Messages.Any())
			{
				Console.WriteLine("\nMessage:");
				foreach (var message in shippingRates.Messages)
					Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
			}
			if (shippingRates.Shipment != null && shippingRates.Shipment.AvailableRates.Any())
			{
				Console.WriteLine("\n\nRates:");
				foreach (WSRate rate in shippingRates.Shipment.AvailableRates)
					Console.WriteLine(
						"Freight: {2:F2}, Fuel: {3:F2}, Accessorials: {4:F2}, Transit: {5}, Carrier Key: {0} \tCarrier Name: {1}",
						rate.CarrierKey, rate.CarrierName, rate.Freight, rate.Fuel, rate.Accessorials, rate.TransitTime);
			}
		}

		[Test]
		public void CanGetMultiLTLShipmentRate()
		{
			var shipment = GenericRateShipment;
			shipment.RequestedServices[0].Key = "who cares";

			var multiShippingRates = LocalService.GetMultipleShippingRates(new[] { shipment, GenericRateShipment });

			foreach (var shippingRates in multiShippingRates)
			{
				Console.WriteLine("\n\nShipping Rates [Generic Rate Shipment] ...");
				if (shippingRates.Messages != null && shippingRates.Messages.Any())
				{
					Console.WriteLine("\nMessage:");
					foreach (var message in shippingRates.Messages)
						Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
				}
				if (shippingRates.Shipment != null && shippingRates.Shipment.AvailableRates.Any())
				{
					Console.WriteLine("\n\nRates:");
					foreach (WSRate rate in shippingRates.Shipment.AvailableRates)
						Console.WriteLine(
							"Freight: {2:F2}, Fuel: {3:F2}, Accessorials: {4:F2}, Transit: {5}, Carrier Key: {0} \tCarrier Name: {1}",
							rate.CarrierKey, rate.CarrierName, rate.Freight, rate.Fuel, rate.Accessorials, rate.TransitTime);
				}
				Console.WriteLine("\n\n------------------------------------------------------------\n\n");
			}
		}

		[Test]
		public void CanBookLTLShipment()
		{
			var rates = LocalService.GetShippingRates(GenericRateShipment);
			if (rates.Messages != null && rates.Messages.Any())
			{
				Console.WriteLine("\n\nShipping Rates [Generic Rate Shipment] ...");
				Console.WriteLine("\nMessage:");
				foreach (var message in rates.Messages)
					Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
				return;
			}
			var shipment = rates.Shipment;
			shipment.SelectedRate = shipment.AvailableRates[0];

			//shipment.Origin.City = "O Fallon";
			//shipment.Origin.State = "MO";
			//shipment.Origin.Name = "Permian Plastics LLC.";
			//shipment.Origin.Street = "1477 Hoff Industrial Dr.";
			//shipment.Origin.Phone = "636-978-4655";
			//shipment.Origin.Fax = "636-978-0557";
			//shipment.Origin.Contact = "Permian Plastics LLC.";
			//shipment.Origin.SpecialInstructions = "Origin special instructions";
			//shipment.Origin.PostalCode = "63366";
			//shipment.Origin.Country.Key = "US";
			//shipment.Origin.Country.Value = "United States";

			//shipment.Destination.City = "New Berlin";
			//shipment.Destination.State = "WI";
			//shipment.Destination.Name = "Wisconsin Engraving Co.";
			//shipment.Destination.Street = "2435 South 170";
			//shipment.Destination.Phone = "262-786-4521";
			//shipment.Destination.Contact = "Receiving";
			//shipment.Destination.SpecialInstructions = "Destination Special Instructions";
			//shipment.Destination.PostalCode = "53151";
			//shipment.Destination.Country.Key = "US";
			//shipment.Destination.Country.Value = "United States";

			//shipment.ReferenceNumber = "20975";
			//shipment.PurchaseOrder = "13181";
			//shipment.ByPassPurchaseOrderValidation = false;
			//shipment.ShipmentDate = new DateTime(2012,05,21,12,45,00,000);
		    
			//shipment.EarliestPickup.Hour = 12;
			//shipment.EarliestPickup.Minute = 30;
			//shipment.EarliestPickup.Second = 0;
			//shipment.EarliestPickup.TimeUnit = TimeUnit.PM;

			//shipment.LatestPickup.Hour = 3;
			//shipment.LatestPickup.Minute = 30;
			//shipment.LatestPickup.Second = 0;
			//shipment.LatestPickup.TimeUnit = TimeUnit.PM;

			//var items = new List<WSItem>();

			//var item = new WSItem
			//               {
			//                   Weight = 610,
			//                   WeightUnit = WeightUnit.Lb,
			//                   Quantity = 1,
			//                   Height = 1,
			//                   Width = 1,
			//                   Length = 1,
			//                   DimsUnit = (DimensionUnit) WSEnums.DimensionUnit.In,
			//                   IsStackable = false,
			//                   DeclaredValue = 0,
			//                   Description = "General Merchandise",
			//                   SaidToContain = 0,
			//               };

			//item.Packaging = new WSOption
			//                     {
			//                         Key = "1",
			//                         Value = "Pallets"
			//                     };
			//item.FreightClass = new WSOption
			//                        {
			//                            Key = "50",
			//                            Value = "50"
			//                        };
			//item.CommodityType = new WSOption
			//                         {
			//                             Key = "1",
			//                             Value = "General Merchandise"
			//                         };


			//items.Add(item);
			//shipment.Items = items.ToArray();

			//shipment.RequestedModes = new[] {(Mode) WSEnums.Mode.LTL};

			//shipment.SelectedRate.CarrierKey = 478;
			//shipment.SelectedRate.CarrierName = "Dayton Freight";
			//shipment.SelectedRate.Freight = (Decimal)74.5756460000;
			//shipment.SelectedRate.Fuel = (Decimal) 12.70313970000000;
			//shipment.SelectedRate.Accessorials = 0;
			//shipment.SelectedRate.BillWeight = 610;
			//shipment.SelectedRate.TransitTime = 1;
			//shipment.SelectedRate.Mode = Mode.LTL;
			//shipment.SelectedRate.AdditionalInsuranceAvailable = false;
			//shipment.SelectedRate.AdditionalInsuranceCost = 0;

			shipment.Origin.Name = "Origin Name";
			shipment.Origin.Street = "123 Origin Street";
			shipment.Origin.Phone = "111-111-1111";
			shipment.Origin.Contact = "John Doe";
			shipment.Origin.City = "City";
			shipment.Origin.State = "State";
			shipment.Destination.Name = "Destination Name";
			shipment.Destination.Street = "456 Destination Street";
			shipment.Destination.Phone = "222-222-2222";
			shipment.Destination.Contact = "Jane Doe";
			shipment.Destination.City = "City";
			shipment.Destination.State = "State";

			var bookedShipment = LocalService.BookShipment(shipment);
			Console.WriteLine("\n\nBooked Shipment [Generic Rate Shipment] ...");
			if (bookedShipment.Messages != null && bookedShipment.Messages.Any())
			{
				Console.WriteLine("\nMessage:");
				foreach (var message in bookedShipment.Messages)
					Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
			}
			if (bookedShipment.Shipment != null)
			{
				Console.WriteLine("\n\nBill of Lading (# {0}):\n\n", bookedShipment.Shipment.BookedShipmentNumber);

				if (bookedShipment.Shipment.ReturnConfirmations != null && bookedShipment.Shipment.ReturnConfirmations.Length > 0)
					Console.WriteLine(bookedShipment.Shipment.ReturnConfirmations[0].Html);
			}
		}

        [Test]
        [Ignore("Only test when attempting to verify override markups through the WebService")]
        public void CanBookLTLShipmentWithVendorFloorOverrides()
        {
            var rates = LocalService.GetShippingRates(GenericRateShipmentForVendorFloorTest);
            if (rates.Messages != null && rates.Messages.Any())
            {
                Console.WriteLine("\n\nShipping Rates [Generic Rate Shipment] ...");
                Console.WriteLine("\nMessage:");
                foreach (var message in rates.Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                return;
            }
            var shipment = rates.Shipment;

            // grab the rate for A Duie Pyle
            shipment.SelectedRate = shipment.AvailableRates.FirstOrDefault(ar => ar.CarrierKey == 1);

            if (shipment.SelectedRate == null)
            {
                Console.WriteLine("Could not find a rate for A Duie Pyle ...");
                return;
            }

            shipment.Origin.Name = "Origin Name";
            shipment.Origin.Street = "123 Origin Street";
            shipment.Origin.Phone = "111-111-1111";
            shipment.Origin.Contact = "John Doe";
            shipment.Origin.City = "City";
            shipment.Origin.State = "State";
            shipment.Destination.Name = "Destination Name";
            shipment.Destination.Street = "456 Destination Street";
            shipment.Destination.Phone = "222-222-2222";
            shipment.Destination.Contact = "Jane Doe";
            shipment.Destination.City = "City";
            shipment.Destination.State = "State";

            var bookedShipment = LocalService.BookShipment(shipment);
            Console.WriteLine("\n\nBooked Shipment [Generic Rate Shipment] ...");
            if (bookedShipment.Messages != null && bookedShipment.Messages.Any())
            {
                Console.WriteLine("\nMessage:");
                foreach (var message in bookedShipment.Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }
            if (bookedShipment.Shipment != null)
            {
                Console.WriteLine("\n\nBill of Lading (# {0}):\n\n", bookedShipment.Shipment.BookedShipmentNumber);
                Console.WriteLine("Verify that this shipment's CustomerFreightMarkupPercent and CustomerFreightMarkupValue match the Vendor Floor overrides for A Duie Pyle on the Customer Rating for Customer #10000");

                if (bookedShipment.Shipment.ReturnConfirmations != null && bookedShipment.Shipment.ReturnConfirmations.Length > 0)
                    Console.WriteLine(bookedShipment.Shipment.ReturnConfirmations[0].Html);
            }
        }

        [Test]
        [Ignore("Only test when attempting to verify override markups through the WebService")]
        public void CanBookLTLShipmentWithWeightBreakOverrides()
        {
            var rates = LocalService.GetShippingRates(GenericRateShipmentForWeightBreakTest);
            if (rates.Messages != null && rates.Messages.Any())
            {
                Console.WriteLine("\n\nShipping Rates [Generic Rate Shipment] ...");
                Console.WriteLine("\nMessage:");
                foreach (var message in rates.Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                return;
            }
            var shipment = rates.Shipment;

            // grab the rate for A Duie Pyle
            shipment.SelectedRate = shipment.AvailableRates.FirstOrDefault(ar => ar.CarrierKey == 1);

            if (shipment.SelectedRate == null)
            {
                Console.WriteLine("Could not find a rate for A Duie Pyle ...");
                return;
            }

            shipment.Origin.Name = "Origin Name";
            shipment.Origin.Street = "123 Origin Street";
            shipment.Origin.Phone = "111-111-1111";
            shipment.Origin.Contact = "John Doe";
            shipment.Origin.City = "City";
            shipment.Origin.State = "State";
            shipment.Destination.Name = "Destination Name";
            shipment.Destination.Street = "456 Destination Street";
            shipment.Destination.Phone = "222-222-2222";
            shipment.Destination.Contact = "Jane Doe";
            shipment.Destination.City = "City";
            shipment.Destination.State = "State";

            var bookedShipment = LocalService.BookShipment(shipment);
            Console.WriteLine("\n\nBooked Shipment [Generic Rate Shipment] ...");
            if (bookedShipment.Messages != null && bookedShipment.Messages.Any())
            {
                Console.WriteLine("\nMessage:");
                foreach (var message in bookedShipment.Messages)
                    Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
            }
            if (bookedShipment.Shipment != null)
            {
                Console.WriteLine("\n\nBill of Lading (# {0}):\n\n", bookedShipment.Shipment.BookedShipmentNumber);
                Console.WriteLine("Verify that this shipment's CustomerFreightMarkupPercent and CustomerFreightMarkupValue match the M1M overrides for A Duie Pyle on the Customer Rating for Customer #10000");

                if (bookedShipment.Shipment.ReturnConfirmations != null && bookedShipment.Shipment.ReturnConfirmations.Length > 0)
                    Console.WriteLine(bookedShipment.Shipment.ReturnConfirmations[0].Html);
            }
        }

		[Test]
		public void CanBookMultipleLTLShipment()
		{
			var rateShipment = GenericRateShipment;
			rateShipment.RequestedServices[0].Key = "who cares";

			var multiRates = LocalService.GetMultipleShippingRates(new[] {rateShipment, GenericRateShipment});
			var multiShipments = new List<WSShipment>();

			foreach (var rates in multiRates)
			{
				if (rates.Messages != null && rates.Messages.Any())
				{
					Console.WriteLine("\n\nShipping Rates [Generic Rate Shipment] ...");
					Console.WriteLine("\nMessage:");
					foreach (var message in rates.Messages)
						Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
					continue;
				}
				var shipment = rates.Shipment;
				shipment.SelectedRate = shipment.AvailableRates[0];
				shipment.Origin.Name = "Origin Name";
				shipment.Origin.Street = "123 Origin Street";
				shipment.Origin.Phone = "111-111-1111";
				shipment.Origin.Contact = "John Doe";
				shipment.Destination.Name = "Destination Name";
				shipment.Destination.Street = "456 Destination Street";
				shipment.Destination.Phone = "222-222-2222";
				shipment.Destination.Contact = "Jane Doe";

				multiShipments.Add(shipment);
			}

			var multiBookedShipment = LocalService.BookMultipleShipments(multiShipments.ToArray());

			foreach (var bookedShipment in multiBookedShipment)
			{
				Console.WriteLine("\n\nBooked Shipment [Generic Rate Shipment] ...");
				if (bookedShipment.Messages != null && bookedShipment.Messages.Any())
				{
					Console.WriteLine("\nMessage:");
					foreach (var message in bookedShipment.Messages)
						Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
				}
				if (bookedShipment.Shipment != null && bookedShipment.Shipment.ReturnConfirmations != null &&
				    bookedShipment.Shipment.ReturnConfirmations.Length > 0)
				{
					Console.WriteLine("\n\nBill of Lading:\n\n");

					Console.WriteLine(bookedShipment.Shipment.ReturnConfirmations[0].Html);
				}
				Console.WriteLine("\n\n------------------------------------------------------------\n\n");
			}
		}
	}
}