﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Extern.Ws;
using LogisticsPlus.Eship.Processor;
using System.Text;
using System.IO;
using System.Net;
using LogisticsPlus.Eship.WebApplication.Services.Rest.RequestObjects;
using LogisticsPlus.Eship.WebApplication.Utilities;
using NUnit.Framework;
using Newtonsoft.Json;

namespace LogisticsPlus.Eship.Tests.Services
{
    [TestFixture]
	[Ignore]
    public class RESTAPITests
    {
        private const string LocalPath = "http://localhost:58058/Services/Rest/";

        [Test]
        public void BookShipmentTest()
        {
            try
            {
                var link = string.Format("{0}BookShipment.aspx", LocalPath);
                var uri = new Uri(link);

                var request = WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("eShipPlusAuth", GetAuthData());

                var json = JsonConvert.SerializeObject(GetShipment());

                using (var writer = new StreamWriter(request.GetRequestStream()))
                    writer.Write(json);

                var response = request.GetResponse();

                string responseData;

                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                {
                    return;
                }

                using (var reader = new StreamReader(responseStream))
                    responseData = reader.ReadToEnd();

                var result = JsonConvert.DeserializeObject<WSShipment2>(responseData);

                Console.WriteLine("Response:");

                if (result.Messages != null && result.Messages.Any())
                {
                    Console.WriteLine("\nMessage:");
                    foreach (var message in result.Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }
            }
            catch (WebException ex)
            {
                ProcessWebException(ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [Test]
        public void GetAccessorialServicesTest()
        {
            try
            {
                var link = string.Format("{0}GetAccessorialServices.aspx", LocalPath);
                var uri = new Uri(link);

                var request = WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("eShipPlusAuth", GetAuthData());

                var json = JsonConvert.SerializeObject(GetAuthentication());

                using (var writer = new StreamWriter(request.GetRequestStream()))
                    writer.Write(json);

                var response = request.GetResponse();

                string responseData;

                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                {
                    return;
                }

                using (var reader = new StreamReader(responseStream))
                    responseData = reader.ReadToEnd();

                var result = JsonConvert.DeserializeObject<WSAccessorialService[]>(responseData);

                Console.WriteLine("Response:");

                if (result.Count() == 1 && result[0].Messages.Any())
                {
                    Console.WriteLine("\nMessage(s):");
                    foreach (var message in result[0].Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }
                else
                {
                    Console.WriteLine("\n\nServices: ...");
                    foreach (var s in result)
                        Console.WriteLine("Service Code: {0}, \t Description: {1}, \t Billing Code: {2}",
                                          s.ServiceCode, s.ServiceDescription, s.BillingCode);
                }
            }
            catch (WebException ex)
            {
                ProcessWebException(ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [Test]
        public void GetLTLFreightClassesTest()
        {
            try
            {
                var link = string.Format("{0}GetLTLFreightClasses.aspx", LocalPath);
                var uri = new Uri(link);

                var request = WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("eShipPlusAuth", GetAuthData());

                var json = JsonConvert.SerializeObject(GetAuthentication());

                using (var writer = new StreamWriter(request.GetRequestStream()))
                    writer.Write(json);

                var response = request.GetResponse();

                string responseData;

                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                {
                    return;
                }

                using (var reader = new StreamReader(responseStream))
                    responseData = reader.ReadToEnd();

                var result = JsonConvert.DeserializeObject<WSFreightClass[]>(responseData);

                Console.WriteLine("Response:");

                if (result.Count() == 1 && result[0].Messages.Any())
                {
                    Console.WriteLine("\nMessage(s):");
                    foreach (var message in result[0].Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }
                else
                {
                    Console.WriteLine("\n\nFreight Classes: ...");
                    foreach (var fc in result)
                        Console.WriteLine("Freight Class: {0}", fc.FreightClass);
                }
            }
            catch (WebException ex)
            {
                ProcessWebException(ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [Test]
        public void GetItemPackagingTest()
        {
            try
            {
                var link = string.Format("{0}GetItemPackaging.aspx", LocalPath);
                var uri = new Uri(link);

                var request = WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("eShipPlusAuth", GetAuthData());

                var json = JsonConvert.SerializeObject(GetAuthentication());

                using (var writer = new StreamWriter(request.GetRequestStream()))
                    writer.Write(json);

                var response = request.GetResponse();

                string responseData;

                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                {
                    return;
                }

                using (var reader = new StreamReader(responseStream))
                    responseData = reader.ReadToEnd();

                var result = JsonConvert.DeserializeObject<WSItemPackage[]>(responseData);

                Console.WriteLine("Response:");

                if (result.Count() == 1 && result[0].Messages.Any())
                {
                    Console.WriteLine("\nMessage(s):");
                    foreach (var message in result[0].Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }
                else
                {
                    Console.WriteLine("\n\nPackaging: ...");
                    foreach (var p in result)
                        Console.WriteLine("Key: {0}, \t Name: {1}, \t Default Length: {2}, \t Default Width: {3}, \t Default Height: {4}",
                                          p.Key, p.PackageName, p.DefaultLength, p.DefaultWidth, p.DefaultHeight);
                }

            }
            catch (WebException ex)
            {
                ProcessWebException(ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [Test]
        public void GetCountriesTest()
        {
            try
            {
                var link = string.Format("{0}GetCountries.aspx", LocalPath);
                var uri = new Uri(link);

                var request = WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("eShipPlusAuth", GetAuthData());

                var json = JsonConvert.SerializeObject(GetAuthentication());

                using (var writer = new StreamWriter(request.GetRequestStream()))
                    writer.Write(json);

                var response = request.GetResponse();

                string responseData;

                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                {
                    return;
                }

                using (var reader = new StreamReader(responseStream))
                    responseData = reader.ReadToEnd();

                var result = JsonConvert.DeserializeObject<WSCountry[]>(responseData);

                Console.WriteLine("Response:");

                if (result.Count() == 1 && result[0].Messages.Any())
                {
                    Console.WriteLine("\nMessage(s):");
                    foreach (var message in result[0].Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }
                else
                {
                    Console.WriteLine("\n\nCountries: ...");
                    foreach (var s in result)
                        Console.WriteLine("Code: {0}, \t Name: {1}, \t Uses Postal Code: {2}",
                                          s.Code, s.Name, s.UsesPostalCode);
                }
            }
            catch (WebException ex)
            {
                ProcessWebException(ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [Test]
        public void GetTimesTest()
        {
            try
            {
                var link = string.Format("{0}GetTimes.aspx", LocalPath);
                var uri = new Uri(link);

                var request = WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("eShipPlusAuth", GetAuthData());

                var json = JsonConvert.SerializeObject(GetAuthentication());

                using (var writer = new StreamWriter(request.GetRequestStream()))
                    writer.Write(json);

                var response = request.GetResponse();

                string responseData;

                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                {
                    return;
                }

                using (var reader = new StreamReader(responseStream))
                    responseData = reader.ReadToEnd();

                var result = JsonConvert.DeserializeObject<WSTime2[]>(responseData);

                Console.WriteLine("Response:");

                if (result.Count() == 1 && result[0].Messages.Any())
                {
                    Console.WriteLine("\nMessage(s):");
                    foreach (var message in result[0].Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }
                else
                {
                    Console.WriteLine("\n\nTimes: ...");
                    foreach (var t in result)
                        Console.WriteLine("Time: {0}", t.Time);
                }

            }
            catch (WebException ex)
            {
                ProcessWebException(ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [Test]
        public void GetCustomerCustomReferencesTest()
        {
            try
            {
                var link = string.Format("{0}GetCustomerCustomReferences.aspx", LocalPath);
                var uri = new Uri(link);

                var request = WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("eShipPlusAuth", GetAuthData());

                var json = JsonConvert.SerializeObject(GetAuthentication());

                using (var writer = new StreamWriter(request.GetRequestStream()))
                    writer.Write(json);

                var response = request.GetResponse();

                string responseData;

                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                {
                    return;
                }

                using (var reader = new StreamReader(responseStream))
                    responseData = reader.ReadToEnd();

                var result = JsonConvert.DeserializeObject<WSReference[]>(responseData);

                Console.WriteLine("Response:");

                if (result.Count() == 1 && result[0].Messages.Any())
                {
                    Console.WriteLine("\nMessage(s):");
                    foreach (var message in result[0].Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }
                else
                {
                    Console.WriteLine("\n\nTimes: ...");
                    foreach (var r in result)
                        Console.WriteLine("Name: {0}, \t Required: {1}", r.Name, r.Required);
                }
            }
            catch (WebException ex)
            {
                ProcessWebException(ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [Test]
        public void RetrievePONumberTest()
        {
            try
            {
                var link = string.Format("{0}RetrievePONumber.aspx", LocalPath);
                var uri = new Uri(link);

                var request = WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("eShipPlusAuth", GetAuthData());

                var json = JsonConvert.SerializeObject("12345");

                using (var writer = new StreamWriter(request.GetRequestStream()))
                    writer.Write(json);

                var response = request.GetResponse();

                string responseData;

                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                {
                    return;
                }

                using (var reader = new StreamReader(responseStream))
                    responseData = reader.ReadToEnd();

                var result = JsonConvert.DeserializeObject<WSCustomerPONumber>(responseData);

                Console.WriteLine("Response:");

                if (result.Messages != null)
                {
                    Console.WriteLine("\nMessage(s):");
                    foreach (var message in result.Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }
                else
                {
                    Console.WriteLine("PO Number: {0}", result.PONumber);
                }
            }
            catch (WebException ex)
            {
                ProcessWebException(ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [Test]
        public void AddPONumberTest()
        {
            try
            {
                var link = string.Format("{0}AddPONumber.aspx", LocalPath);
                var uri = new Uri(link);

                var request = WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("eShipPlusAuth", GetAuthData());

                var json = JsonConvert.SerializeObject(GetPurchaseOrder());

                using (var writer = new StreamWriter(request.GetRequestStream()))
                    writer.Write(json);

                var response = request.GetResponse();

                string responseData;

                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                {
                    return;
                }

                using (var reader = new StreamReader(responseStream))
                    responseData = reader.ReadToEnd();

                var result = JsonConvert.DeserializeObject<WSReturn>(responseData);

                Console.WriteLine("Response:");

                if (result.Messages != null)
                {
                    Console.WriteLine("\nMessage(s):");
                    foreach (var message in result.Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }
            }
            catch (WebException ex)
            {
                ProcessWebException(ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [Test]
        public void UpdatePONumberTest()
        {
            try
            {
                var link = string.Format("{0}UpdatePONumber.aspx", LocalPath);
                var uri = new Uri(link);

                var request = WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("eShipPlusAuth", GetAuthData());

                var json = JsonConvert.SerializeObject(GetPurchaseOrder());

                using (var writer = new StreamWriter(request.GetRequestStream()))
                    writer.Write(json);

                var response = request.GetResponse();

                string responseData;

                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                {
                    return;
                }

                using (var reader = new StreamReader(responseStream))
                    responseData = reader.ReadToEnd();

                var result = JsonConvert.DeserializeObject<WSReturn>(responseData);

                Console.WriteLine("Response:");

                if (result.Messages != null)
                {
                    Console.WriteLine("\nMessage(s):");
                    foreach (var message in result.Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }
            }
            catch (WebException ex)
            {
                ProcessWebException(ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [Test]
        public void AddPONumbersTest()
        {
            try
            {
                var link = string.Format("{0}AddPONumbers.aspx", LocalPath);
                var uri = new Uri(link);

                var request = WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("eShipPlusAuth", GetAuthData());

                var json = JsonConvert.SerializeObject(GetPurchaseOrders());

                using (var writer = new StreamWriter(request.GetRequestStream()))
                    writer.Write(json);

                var response = request.GetResponse();

                string responseData;

                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                {
                    return;
                }

                using (var reader = new StreamReader(responseStream))
                    responseData = reader.ReadToEnd();

                var result = JsonConvert.DeserializeObject<WSReturn>(responseData);

                Console.WriteLine("Response:");

                if (result.Messages != null)
                {
                    Console.WriteLine("\nMessage(s):");
                    foreach (var message in result.Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }
            }
            catch (WebException ex)
            {
                ProcessWebException(ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [Test]
        public void DeletePONumberTest()
        {
            try
            {
                var link = string.Format("{0}DeletePONumber.aspx", LocalPath);
                var uri = new Uri(link);

                var request = WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("eShipPlusAuth", GetAuthData());

                var json = JsonConvert.SerializeObject(GetPurchaseOrder());

                using (var writer = new StreamWriter(request.GetRequestStream()))
                    writer.Write(json);

                var response = request.GetResponse();

                string responseData;

                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                {
                    return;
                }

                using (var reader = new StreamReader(responseStream))
                    responseData = reader.ReadToEnd();

                var result = JsonConvert.DeserializeObject<WSReturn>(responseData);

                Console.WriteLine("Response:");

                if (result.Messages != null)
                {
                    Console.WriteLine("\nMessage(s):");
                    foreach (var message in result.Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }
            }
            catch (WebException ex)
            {
                ProcessWebException(ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [Test]
        public void GetBookingRequestStatusTest()
        {
            try
            {
                var link = string.Format("{0}GetBookingRequestStatus.aspx", LocalPath);
                var uri = new Uri(link);

                var request = WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("eShipPlusAuth", GetAuthData());

                var json = JsonConvert.SerializeObject("58721");

                using (var writer = new StreamWriter(request.GetRequestStream()))
                    writer.Write(json);

                var response = request.GetResponse();

                string responseData;

                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                {
                    return;
                }

                using (var reader = new StreamReader(responseStream))
                    responseData = reader.ReadToEnd();

                var result = JsonConvert.DeserializeObject<WSBookingRequestStatus>(responseData);

                Console.WriteLine("Response:");

                if (result.Messages != null)
                {
                    Console.WriteLine("\nMessage(s):");
                    foreach (var message in result.Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }
                else
                {
                    Console.WriteLine("PO Number: {0} \t Status: {1} \t Shipments: {2}", result.BookingNumber,
                                      result.Status, result.Shipments.Select(s => s.ShipmentNumber).ToArray().CommaJoin());
                }
            }
            catch (WebException ex)
            {
                ProcessWebException(ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [Test]
        public void GetShipmentStatusTest()
        {
            try
            {
                var link = string.Format("{0}GetShipmentStatus.aspx", LocalPath);
                var uri = new Uri(link);

                var request = WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("eShipPlusAuth", GetAuthData());

                var json = JsonConvert.SerializeObject("56587");

                using (var writer = new StreamWriter(request.GetRequestStream()))
                    writer.Write(json);

                var response = request.GetResponse();

                string responseData;

                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                {
                    return;
                }

                using (var reader = new StreamReader(responseStream))
                    responseData = reader.ReadToEnd();

                var result = JsonConvert.DeserializeObject<WSShipmentStatus>(responseData);

                Console.WriteLine("Response:");

                if (result.Messages != null)
                {
                    Console.WriteLine("\nMessage(s):");
                    foreach (var message in result.Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }
                else
                {
                    Console.WriteLine("Shipment Number: {0} \t Status: {1} \t Vendor Scac: {2}", result.ShipmentNumber, result.Status, result.VendorScac);
                }
            }
            catch (WebException ex)
            {
                ProcessWebException(ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [Test]
        public void GetShipmentStatusCondensedTest()
        {
            try
            {
                var link = string.Format("{0}GetShipmentStatusCondensed.aspx", LocalPath);
                var uri = new Uri(link);

                var request = WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("eShipPlusAuth", GetAuthData());

                var json = JsonConvert.SerializeObject("56587");

                using (var writer = new StreamWriter(request.GetRequestStream()))
                    writer.Write(json);

                var response = request.GetResponse();

                string responseData;

                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                {
                    return;
                }

                using (var reader = new StreamReader(responseStream))
                    responseData = reader.ReadToEnd();

                var result = JsonConvert.DeserializeObject<WSShipmentStatus>(responseData);

                Console.WriteLine("Response:");

                if (result.Messages != null)
                {
                    Console.WriteLine("\nMessage(s):");
                    foreach (var message in result.Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }
                else
                {
                    Console.WriteLine("Shipment Number: {0} \t Status: {1} \t Vendor Scac: {2}", result.ShipmentNumber, result.Status, result.VendorScac);
                }
            }
            catch (WebException ex)
            {
                ProcessWebException(ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        [Test]
        public void GetInputOptionsTest()
        {
            try
            {
                var link = string.Format("{0}GetInputOptions.aspx", LocalPath);
                var uri = new Uri(link);

                var request = WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("eShipPlusAuth", GetAuthData());

                var json = JsonConvert.SerializeObject(GetAuthentication());

                using (var writer = new StreamWriter(request.GetRequestStream()))
                    writer.Write(json);

                var response = request.GetResponse();

                string responseData;

                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                {
                    return;
                }

                using (var reader = new StreamReader(responseStream))
                    responseData = reader.ReadToEnd();

                var result = JsonConvert.DeserializeObject<WSOptions>(responseData);

                Console.WriteLine("Response:");

                if (result.Times != null && result.Times.Count() == 1 && result.Times[0].Messages.Any())
                {
                    Console.WriteLine("\nTime Message(s):");
                    foreach (var message in result.Times[0].Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }

                if (result.AccessorialServices != null && result.AccessorialServices.Count() == 1 && result.AccessorialServices[0].Messages.Any())
                {
                    Console.WriteLine("\nAccessorial Message(s):");
                    foreach (var message in result.AccessorialServices[0].Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }

                if (result.LTLFreightClasses != null && result.LTLFreightClasses.Count() == 1 && result.LTLFreightClasses[0].Messages.Any())
                {
                    Console.WriteLine("\nFreight Class Message(s):");
                    foreach (var message in result.LTLFreightClasses[0].Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }

                if (result.Countries != null && result.Countries.Count() == 1 && result.Countries[0].Messages.Any())
                {
                    Console.WriteLine("\nCountries Message(s):");
                    foreach (var message in result.Countries[0].Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }

                if (result.ItemPackaging != null && result.ItemPackaging.Count() == 1 && result.ItemPackaging[0].Messages.Any())
                {
                    Console.WriteLine("\nItem packaging Message(s):");
                    foreach (var message in result.ItemPackaging[0].Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }
            }
            catch (WebException ex)
            {
                ProcessWebException(ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [Test]
        public void RequestShipmentCancellationTest()
        {
            try
            {
                var link = string.Format("{0}RequestShipmentCancellation.aspx", LocalPath);
                var uri = new Uri(link);

                var request = WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("eShipPlusAuth", GetAuthData());

                var json = JsonConvert.SerializeObject("56644");

                using (var writer = new StreamWriter(request.GetRequestStream()))
                    writer.Write(json);

                var response = request.GetResponse();

                string responseData;

                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                {
                    return;
                }

                using (var reader = new StreamReader(responseStream))
                    responseData = reader.ReadToEnd();

                var result = JsonConvert.DeserializeObject<WSReturn>(responseData);

                Console.WriteLine("Response:");

                if (result.Messages != null)
                {
                    Console.WriteLine("\nMessage(s):");
                    foreach (var message in result.Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }
            }
            catch (WebException ex)
            {
                ProcessWebException(ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [Test]
        public void RequestBookingRequestCancellationTest()
        {
            try
            {
                var link = string.Format("{0}RequestBookingRequestCancellation.aspx", LocalPath);
                var uri = new Uri(link);

                var request = WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("eShipPlusAuth", GetAuthData());

                var json = JsonConvert.SerializeObject("56643");

                using (var writer = new StreamWriter(request.GetRequestStream()))
                    writer.Write(json);

                var response = request.GetResponse();

                string responseData;

                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                {
                    return;
                }

                using (var reader = new StreamReader(responseStream))
                    responseData = reader.ReadToEnd();

                var result = JsonConvert.DeserializeObject<WSReturn>(responseData);

                Console.WriteLine("Response:");

                if (result.Messages != null)
                {
                    Console.WriteLine("\nMessage(s):");
                    foreach (var message in result.Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }
            }
            catch (WebException ex)
            {
                ProcessWebException(ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [Test]
        public void GetInvoiceTest()
        {
            try
            {
                var link = string.Format("{0}GetInvoice.aspx", LocalPath);
                var uri = new Uri(link);

                var request = WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("eShipPlusAuth", GetAuthData());

                var json = JsonConvert.SerializeObject("10766");

                using (var writer = new StreamWriter(request.GetRequestStream()))
                    writer.Write(json);

                var response = request.GetResponse();

                string responseData;

                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                {
                    return;
                }

                using (var reader = new StreamReader(responseStream))
                    responseData = reader.ReadToEnd();

                var result = JsonConvert.DeserializeObject<WSInvoice>(responseData);

                Console.WriteLine("Response:");

                if (result.Messages != null)
                {
                    Console.WriteLine("\nMessage(s):");
                    foreach (var message in result.Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }
                else
                {
                    Console.WriteLine("\nInvioce Details:");
                    Console.WriteLine("Invoice #: \t {0}", result.InvoiceNumber);
                    Console.WriteLine("Invoice Type: \t {0}", result.Type);
                    Console.WriteLine("Address #: \t {0}",

                                      new[]
										  {
										  result.Billing.Street, result.Billing.City, result.Billing.State, result.Billing.PostalCode,
										  result.Billing.Country.Name
										  }.CommaJoin());
                    Console.WriteLine("Cust. Control Acct.: \t {0}", result.CustomerControlAccount);
                    Console.WriteLine("Invoice Date: \t {0}", result.Date);
                    Console.WriteLine("Invoice Due Date: \t {0}", result.DueDate);
                    Console.WriteLine("Total Due: \t {0:c2}", result.TotalAmountDue);
                    Console.WriteLine("Freight Charges: \t {0:c2}", result.TotalFreightCharges);
                    Console.WriteLine("Fuel Charges: \t {0:c2}", result.TotalFuelCharges);
                    Console.WriteLine("Accessorial Charges: \t {0:c2}", result.TotalAccessorialCharges);
                    Console.WriteLine("Service Charges: \t {0:c2}", result.TotalServiceCharges);
                    Console.WriteLine("Invoice Terms: \t {0}", result.Terms);

                    var i = 0;
                    foreach (var detail in result.BillingDetails)
                    {
                        Console.WriteLine("Line {0}: Ref #: {1} Type: {2} Code: {3} Desc: {4} Category: {5} Amount: {6:c2}", ++i,
                                          detail.ReferenceNumber, detail.ReferenceType, detail.BillingCode, detail.Description,
                                          detail.Category, detail.AmountDue);
                    }
                }
            }
            catch (WebException ex)
            {
                ProcessWebException(ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [Test]
        public void GetInvoicesByDueDateTest()
        {
            try
            {
                var link = string.Format("{0}GetInvoicesByDueDate.aspx", LocalPath);
                var uri = new Uri(link);

                var request = WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("eShipPlusAuth", GetAuthData());

                var json = JsonConvert.SerializeObject(new { Startdate = DateTime.Today.AddDays(-60), EndDate = DateTime.Now });

                using (var writer = new StreamWriter(request.GetRequestStream()))
                    writer.Write(json);

                var response = request.GetResponse();

                string responseData;

                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                {
                    return;
                }

                using (var reader = new StreamReader(responseStream))
                    responseData = reader.ReadToEnd();

                var result = JsonConvert.DeserializeObject<WSInvoiceSummary[]>(responseData);

                Console.WriteLine("Response:");

                if (result.Count() == 1 && result[0].Messages != null)
                {
                    Console.WriteLine("\nMessage(s):");
                    foreach (var message in result[0].Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }
                else
                {
                    foreach (var inv in result)
                    {
                        Console.WriteLine("\nInvioce Details:");

                        Console.WriteLine("Invoice #: \t {0}", inv.InvoiceNumber);
                        Console.WriteLine("Invoice Type.: \t {0}", inv.InvoiceType);
                        Console.WriteLine("Original Invoice #: \t {0}", inv.OriginalInvoiceNumber);
                        Console.WriteLine("Address #: \t {0}",

                                          new[]
											  {
											  inv.LocationStreet, inv.LocationCity, inv.LocationState,
											  inv.LocationPostalCode, inv.LocationCountryName
											  }.CommaJoin());


                        Console.WriteLine("Invoice Date: \t {0}", inv.Date);
                        Console.WriteLine("Invoice Due Date: \t {0}", inv.DueDate);
                        Console.WriteLine("Total Due: \t {0:c2}", inv.TotalAmount);
                        Console.WriteLine("Instructions: \t {0}", inv.SpecialInstruction);
                    }
                }
            }
            catch (WebException ex)
            {
                ProcessWebException(ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [Test]
        public void GetInvoicesByDateTest()
        {
            try
            {
                var link = string.Format("{0}GetInvoicesByDate.aspx", LocalPath);
                var uri = new Uri(link);

                var request = WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("eShipPlusAuth", GetAuthData());

                var json = JsonConvert.SerializeObject(new { Startdate = DateTime.Today.AddDays(-60), EndDate = DateTime.Now });

                using (var writer = new StreamWriter(request.GetRequestStream()))
                    writer.Write(json);

                var response = request.GetResponse();

                string responseData;

                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                {
                    return;
                }

                using (var reader = new StreamReader(responseStream))
                    responseData = reader.ReadToEnd();

                var result = JsonConvert.DeserializeObject<WSInvoiceSummary[]>(responseData);

                Console.WriteLine("Response:");

                if (result.Count() == 1 && result[0].Messages != null)
                {
                    Console.WriteLine("\nMessage(s):");
                    foreach (var message in result[0].Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }
                else
                {
                    foreach (var inv in result)
                    {
                        Console.WriteLine("\nInvioce Details:");

                        Console.WriteLine("Invoice #: \t {0}", inv.InvoiceNumber);
                        Console.WriteLine("Invoice Type.: \t {0}", inv.InvoiceType);
                        Console.WriteLine("Original Invoice #: \t {0}", inv.OriginalInvoiceNumber);
                        Console.WriteLine("Address #: \t {0}",

                                          new[]
											  {
											  inv.LocationStreet, inv.LocationCity, inv.LocationState,
											  inv.LocationPostalCode, inv.LocationCountryName
											  }.CommaJoin());


                        Console.WriteLine("Invoice Date: \t {0}", inv.Date);
                        Console.WriteLine("Invoice Due Date: \t {0}", inv.DueDate);
                        Console.WriteLine("Total Due: \t {0:c2}", inv.TotalAmount);
                        Console.WriteLine("Instructions: \t {0}", inv.SpecialInstruction);
                    }
                }
            }
            catch (WebException ex)
            {
                ProcessWebException(ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [Test]
        public void GetInvoicesForShipmentTest()
        {
            try
            {
                var link = string.Format("{0}GetInvoicesForShipment.aspx", LocalPath);
                var uri = new Uri(link);

                var request = WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("eShipPlusAuth", GetAuthData());

                var json = JsonConvert.SerializeObject("10804");

                using (var writer = new StreamWriter(request.GetRequestStream()))
                    writer.Write(json);

                var response = request.GetResponse();

                string responseData;

                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                {
                    return;
                }

                using (var reader = new StreamReader(responseStream))
                    responseData = reader.ReadToEnd();

                var result = JsonConvert.DeserializeObject<WSInvoiceSummary[]>(responseData);

                Console.WriteLine("Response:");

                if (result.Count() == 1 && result[0].Messages != null)
                {
                    Console.WriteLine("\nMessage(s):");
                    foreach (var message in result[0].Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }
                else
                {
                    foreach (var inv in result)
                    {
                        Console.WriteLine("\nInvioce Details:");

                        Console.WriteLine("Invoice #: \t {0}", inv.InvoiceNumber);
                        Console.WriteLine("Invoice Type.: \t {0}", inv.InvoiceType);
                        Console.WriteLine("Original Invoice #: \t {0}", inv.OriginalInvoiceNumber);
                        Console.WriteLine("Address #: \t {0}",
                                          new[]
											  {
											  inv.LocationStreet, inv.LocationCity, inv.LocationState,
											  inv.LocationPostalCode, inv.LocationCountryName
											  }.CommaJoin());


                        Console.WriteLine("Invoice Date: \t {0}", inv.Date);
                        Console.WriteLine("Invoice Due Date: \t {0}", inv.DueDate);
                        Console.WriteLine("Total Due: \t {0:c2}", inv.TotalAmount);
                        Console.WriteLine("Instructions: \t {0}", inv.SpecialInstruction);
                    }
                }
            }
            catch (WebException ex)
            {
                ProcessWebException(ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [Test]
        public void SubmitBookingRequestTest()
        {
            try
            {
                var link = string.Format("{0}SubmitBookingRequest.aspx", LocalPath);
                var uri = new Uri(link);

                var request = WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("eShipPlusAuth", GetAuthData());

                var json = JsonConvert.SerializeObject(GetBookingRequest());

                using (var writer = new StreamWriter(request.GetRequestStream()))
                    writer.Write(json);

                var response = request.GetResponse();

                string responseData;

                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                {
                    return;
                }

                using (var reader = new StreamReader(responseStream))
                    responseData = reader.ReadToEnd();

                var result = JsonConvert.DeserializeObject<WSBookingRequest>(responseData);

                Console.WriteLine("Response:");

                Console.WriteLine("\n\nBooking Request Submission [Number: {0}]...", result.SubmittedRequestNumber);

                if (result.Messages != null && result.Messages.Any())
                {
                    Console.WriteLine("\nMessage:");
                    foreach (var message in result.Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }
            }
            catch (WebException ex)
            {
                ProcessWebException(ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [Test]
        public void RateShipmentTest()
        {
            try
            {
                var link = string.Format("{0}RateShipment.aspx", LocalPath);
                var uri = new Uri(link);

                var request = WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("eShipPlusAuth", GetAuthData());

                var json = JsonConvert.SerializeObject(GetShipment());

                using (var writer = new StreamWriter(request.GetRequestStream()))
                    writer.Write(json);

                var response = request.GetResponse();

                string responseData;

                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                {
                    return;
                }

                using (var reader = new StreamReader(responseStream))
                    responseData = reader.ReadToEnd();

                var result = JsonConvert.DeserializeObject<WSShipment2>(responseData);

                Console.WriteLine("Response:");

                if (result.Messages != null && result.Messages.Any())
                {
                    Console.WriteLine("\nMessage:");
                    foreach (var message in result.Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }
                if (result.AvailableRates != null && result.AvailableRates.Any())
                {
                    Console.WriteLine("\n\nRates:");
                    foreach (var rate in result.AvailableRates)
                    {
                        Console.WriteLine(
                            "Freight: {2:F2}, Fuel: {3:F2}, Accessorials: {4:F2}, Service: {5:F2}, Transit: {6}, Carrier Key: {0} \tCarrier Name: {1} \tCarrier SCAC: {7}",
                            rate.CarrierKey, rate.CarrierName, rate.FreightCharges, rate.FuelCharges, rate.AccessorialCharges,
                            rate.ServiceCharges, rate.TransitTime, rate.CarrierScac);
                        var i = 0;
                        foreach (var detail in rate.BillingDetails)
                        {
                            Console.WriteLine("Line {0}: Ref #: {1} Type: {2} Code: {3} Desc: {4} Category: {5} Amount: {6:c2}", ++i,
                                detail.ReferenceNumber, detail.ReferenceType, detail.BillingCode, detail.Description,
                                detail.Category, detail.AmountDue);
                        }
                        Console.WriteLine();
                        Console.WriteLine("Guaranteed Service Options: {0}", rate.GuarOptions.Count());
                        foreach (var chg in rate.GuarOptions)
                        {
                            Console.WriteLine("\t Ref #: {0} Desc: {1} Code: {2} Amount: {3:c2} Category: {4} Ref Type: {5}",
                                chg.ReferenceNumber, chg.Description, chg.BillingCode, chg.AmountDue, chg.Category,
                                chg.ReferenceType);
                        }
                        Console.WriteLine();
                    }
                }

            }
            catch (WebException ex)
            {
                ProcessWebException(ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [Test]
        public void SaveQuoteTest()
        {
            try
            {
                var link = string.Format("{0}SaveQuote.aspx", LocalPath);
                var uri = new Uri(link);

                var request = WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("eShipPlusAuth", GetAuthData());

                var json = JsonConvert.SerializeObject(GetShipment());

                using (var writer = new StreamWriter(request.GetRequestStream()))
                    writer.Write(json);

                var response = request.GetResponse();

                string responseData;

                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                {
                    return;
                }

                using (var reader = new StreamReader(responseStream))
                    responseData = reader.ReadToEnd();

                var result = JsonConvert.DeserializeObject<WSShipment2>(responseData);

                Console.WriteLine("Response:");

                if (result.Messages != null && result.Messages.Any())
                {
                    Console.WriteLine("\nMessage:");
                    foreach (var message in result.Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }

                Console.WriteLine("\nHas Return Confirmations: {0}", result.ReturnConfirmations != null && result.ReturnConfirmations.Any());
            }
            catch (WebException ex)
            {
                ProcessWebException(ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [Test]
        public void ConvertQuoteToShipmentTest()
        {
            try
            {
                var link = string.Format("{0}ConvertQuoteToShipment.aspx", LocalPath);
                var uri = new Uri(link);

                var request = WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("eShipPlusAuth", GetAuthData());

                var json = JsonConvert.SerializeObject("58583");

                using (var writer = new StreamWriter(request.GetRequestStream()))
                    writer.Write(json);

                var response = request.GetResponse();

                string responseData;

                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                {
                    return;
                }

                using (var reader = new StreamReader(responseStream))
                    responseData = reader.ReadToEnd();

                var result = JsonConvert.DeserializeObject<WSShipment2>(responseData);

                Console.WriteLine("Response:");

                if (result.Messages != null && result.Messages.Any())
                {
                    Console.WriteLine("\nMessage:");
                    foreach (var message in result.Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }

                Console.WriteLine("\nHas Return Confirmations: {0}", result.ReturnConfirmations != null && result.ReturnConfirmations.Any());
            }
            catch (WebException ex)
            {
                ProcessWebException(ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [Test]
        public void UpdateProNumberTest()
        {
            try
            {
                var link = string.Format("{0}UpdateVendorProNumber.aspx", LocalPath);
                var uri = new Uri(link);

                var request = WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("eShipPlusAuth", GetAuthData());

                var json = JsonConvert.SerializeObject(GetVendorProNumber());

                using (var writer = new StreamWriter(request.GetRequestStream()))
                    writer.Write(json);

                var response = request.GetResponse();

                string responseData;

                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                {
                    return;
                }

                using (var reader = new StreamReader(responseStream))
                    responseData = reader.ReadToEnd();

                var result = JsonConvert.DeserializeObject<WSVendorProNumber>(responseData);

                Console.WriteLine("Response:");

                if (result.Messages != null && result.Messages.Any())
                {
                    Console.WriteLine("\nMessage:");
                    foreach (var message in result.Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }
            }
            catch (WebException ex)
            {
                ProcessWebException(ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        
        [Test]
        public void FetchShipmentDocumentTest()
        {
            try
            {
                var link = string.Format("{0}GetShipmentDocument.aspx", LocalPath);
                var uri = new Uri(link);

                var request = WebRequest.Create(uri);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers.Add("eShipPlusAuth", GetAuthData());

                var json = JsonConvert.SerializeObject(new GetDocumentRequest { DocumentId = 27018 , ReferenceNumber = "58990"});

                using (var writer = new StreamWriter(request.GetRequestStream()))
                    writer.Write(json);

                var response = request.GetResponse();

                string responseData;

                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                {
                    return;
                }

                using (var reader = new StreamReader(responseStream))
                    responseData = reader.ReadToEnd();

                var result = JsonConvert.DeserializeObject<WSShipmentDocument>(responseData);

                Console.WriteLine("Response:");

                if (result.Messages != null)
                {
                    Console.WriteLine("\nMessage(s):");
                    foreach (var message in result.Messages)
                        Console.WriteLine("Type: {0}, Message: {1}", message.Type, message.Value);
                }
                else
                {
                    var bytes = Convert.FromBase64String(result.EncodedDocumentBytes);
                    Console.WriteLine("\nDocuments:");
                    Console.WriteLine("Id: \t {0}", result.DocumentKey);
                    Console.WriteLine("Name: \t {0}", result.DocumentFileName);
                    Console.WriteLine("Byte Length: \t {0}", bytes.Length);
                }
            }
            catch (WebException ex)
            {
                ProcessWebException(ex);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        private void ProcessWebException(WebException ex)
        {
            var errorResponse = (HttpWebResponse)ex.Response;
            var responseStream = errorResponse.GetResponseStream();
            if (responseStream != null)
            {
                string xml;
                using (var reader = new StreamReader(responseStream))
                    xml = reader.ReadToEnd();
                Console.WriteLine("HTTP Web Exception Occured. Response from stream: ");
                Console.WriteLine(xml);
            }
        }

        private string GetAuthData()
        {
            var authenticationObj = new WSAuthentication
            {
                UserName = "chris",
                Password = "admintest",
                AccessKey = new Guid("9DD08C2C-0753-45F6-8D5B-8553F635E0E2"),
                AccessCode = "TENANT1"
            };

            var bytes = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(authenticationObj));
            var authData = Convert.ToBase64String(bytes);

            return authData;
        }

        private WSAuthentication GetAuthentication()
        {
            return new WSAuthentication
            {
                UserName = "chris",
                Password = "admintest",
                AccessKey = new Guid("9DD08C2C-0753-45F6-8D5B-8553F635E0E2"),
                AccessCode = "TENANT1"
            };
        }

        private WSCustomerPONumber GetPurchaseOrder()
        {
            return new WSCustomerPONumber
            {
                AdditionalPurchaseOrderNotes = "Test",
                Country = new WSCountry { Code = "US" },
                CurrentUses = 0,
                ExpirationDate = DateTime.Now,
                MaximumUses = 10,
                PONumber = "12345",
                ValidPostalCode = "16501",
                Validity = WSEnums.PONumberValidity.Origin,
            };
        }

        private WSCustomerPONumber[] GetPurchaseOrders()
        {
            return new List<WSCustomerPONumber>
		    {
			    new WSCustomerPONumber
			    {
				    AdditionalPurchaseOrderNotes = "Test",
				    Country = new WSCountry {Code = "US"},
				    CurrentUses = 0,
				    ExpirationDate = DateTime.Now,
				    MaximumUses = 10,
				    PONumber = "123457",
				    ValidPostalCode = "16501",
				    Validity = WSEnums.PONumberValidity.Origin,
			    },
			    new WSCustomerPONumber
			    {
				    AdditionalPurchaseOrderNotes = "Test1",
				    Country = new WSCountry {Code = "US"},
				    CurrentUses = 0,
				    ExpirationDate = DateTime.Now,
				    MaximumUses = 10,
				    PONumber = "123456",
				    ValidPostalCode = "16501",
				    Validity = WSEnums.PONumberValidity.Origin,
			    }
		    }.ToArray();
        }

        private WSShipment2 GetShipment()
        {
            var shipment = new WSShipment2();

            var packaging = new WSItemPackage { Key = 258, PackageName = "Pallets" };
            var fc = new WSFreightClass { FreightClass = 77.5 };
            var item = new WSItem2
            {
                DeclaredValue = 100m,
                Description = "Test Type",
                FreightClass = fc,
                Stackable = false,
                Length = 48,
                Width = 48,
                Height = 40,
                NationalMotorFreightClassification = string.Empty,
                Packaging = packaging,
                PackagingQuantity = 1,
                SaidToContain = 1,
                Weight = 1000,
                Comment = string.Empty,
                HarmonizedTariffSchedule = string.Empty
            };

            var country = new WSCountry { Code = "US", Name = "United States", UsesPostalCode = true };

            shipment.Origin = new WSLocation2 { PostalCode = "16508", Country = country };
            shipment.Destination = new WSLocation2 { PostalCode = "12345", Country = country };
            shipment.EarliestPickup = new WSTime2 { Time = "08:00" };
            shipment.LatestPickup = new WSTime2 { Time = "17:00" };
            shipment.ShipmentDate = DateTime.Now;
            shipment.Items = new[] { item };
            shipment.DeclineAdditionalInsuranceIfApplicable = true;

            return shipment;
        }

        private WSBookingRequest GetBookingRequest()
        {
            var request = new WSBookingRequest();

            var service1 = new WSAccessorialService
            {
                ServiceCode = "RESD",
                ServiceDescription = "Private Residence Delivery",
                BillingCode = "RESD"
            };
            var service2 = new WSAccessorialService
            {
                ServiceCode = "LGD",
                ServiceDescription = "Lift-Gate Service Delivery",
                BillingCode = "LGD"
            };

            var packaging = new WSItemPackage { Key = 258, PackageName = "Pallets" };
            var fc = new WSFreightClass { FreightClass = 77.5 };
            var item = new WSItem2
            {
                DeclaredValue = 100m,
                Description = "Test Type",
                FreightClass = fc,
                Stackable = false,
                Length = 48,
                Width = 48,
                Height = 40,
                NationalMotorFreightClassification = string.Empty,
                Packaging = packaging,
                PackagingQuantity = 1,
                SaidToContain = 1,
                Weight = 1000,
                Comment = string.Empty,
                HarmonizedTariffSchedule = string.Empty
            };

            var country = new WSCountry { Code = "US", Name = "United States", UsesPostalCode = true };
            var country2 = new WSCountry { Code = "CA", Name = "Canada", UsesPostalCode = true };

            request.Origin = new WSLocation2 { PostalCode = "M2J3Z6", Country = country2 };
            request.Destination = new WSLocation2 { PostalCode = "44445", Country = country };
            request.EarliestPickup = new WSTime2 { Time = "08:00" };
            request.LatestPickup = new WSTime2 { Time = "17:00" };
            request.DesiredShipmentDate = DateTime.Now;

            request.Accessorials = new[] { service1, service2 };
            request.Items = new[] { item, item };

            request.DeclineAdditionalInsuranceIfApplicable = true;

            request.Origin.Description = "Origin Name";
            request.Origin.Street = "123 Origin Street";
            request.Origin.Phone = "111-111-1111";
            request.Origin.Contact = "John Doe";
            request.Origin.City = "City";
            request.Origin.State = "State";

            request.Destination.Description = "Destination Name";
            request.Destination.Street = "456 Destination Street";
            request.Destination.Phone = "222-222-2222";
            request.Destination.Contact = "Jane Doe";
            request.Destination.City = "City";
            request.Destination.State = "State";

            request.ReferenceNumber = "20975";
            request.PurchaseOrder = "13181";

            request.CustomReferences = null;

            return request;
        }

        private WSVendorProNumber GetVendorProNumber()
        {
            var pro = new WSVendorProNumber
                {
                    ReferenceNumber = "58902",
                    VendorScac = "ABFL",
                    VendorProNumber = "TEST PRO 123"
                };

            return pro;
        }

    }
}
