﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Xml.Serialization;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Rates;
using LogisticsPlus.Eship.Tests.banyanWs;
using NUnit.Framework;
using Contact = LogisticsPlus.Eship.Tests.banyanWs.Contact;

namespace LogisticsPlus.Eship.Tests.Services
{
	[TestFixture]
	[Ignore("Run on demand only")]
	public class RatingApiTests
	{
		private FullLoad_Request _request;
		private ConnectivityPlatform _service;
		private Rater2 _rater2;
		private Shipment _shipment;
		private P44RateRequst _p44;

		private readonly List<string[]> _points = new List<string[]>
			{
				new[] {"OH", "44622", "KY", "40511"},
				new[] {"KY", "41042", "PA", "17044"},
				new[] {"IA", "50010", "OH", "43502"},
				new[] {"IN", "46168", "OH", "44004"},
				new[] {"MO", "64658", "IL", "60007"},
				new[] {"OH", "44408", "NY", "14760"},
				new[] {"GA", "30121", "MA", "01238"},
				new[] {"OK", "73118", "CA", "92071"},
				new[] {"OH", "45204", "PA", "16335"},
				new[] {"PA", "19020", "TX", "77069"},
				new[] {"PA", "19365", "PA", "16701"},
				new[] {"ID", "83402", "MN", "56560"},
				new[] {"NY", "14094", "PA", "16502"},
				new[] {"IN", "46235", "IL", "60007"},
				new[] {"OH", "44011", "IL", "60106"},
				new[] {"PA", "19503", "RI", "02865"},
				new[] {"PA", "15112", "MI", "48180"},
				new[] {"CT", "06470", "TX", "78624"},
				new[] {"MO", "63010", "MI", "48174"},
				new[] {"TX", "79936", "TX", "78045"},
			};

		[SetUp]
		public void Initialize()
		{
			if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();

			var tenantId = GlobalTestInitializer.DefaultTenantId;
			var pmaps = ProcessorVars.RegistryCache[tenantId].SmallPackagingMaps;
			var smaps = ProcessorVars.RegistryCache[tenantId].SmallPackageServiceMaps;

			_rater2 = new Rater2(GlobalTestInitializer.RatewareSettings,
							   GlobalTestInitializer.CarrierConnectSettings2,
							   GlobalTestInitializer.FedExServiceParams,
							   GlobalTestInitializer.UpsServiceParams,
							   pmaps, smaps);


			_shipment = new Shipment(75795)
				{
					Items = new List<ShipmentItem>
						{
							new ShipmentItem
								{
									ActualFreightClass = 77.5,
									ActualLength = 58,
									ActualWidth = 42,
									ActualHeight = 24,
									PackageTypeId = GlobalTestInitializer.DefaultPackageTypeId,
									Description = "Stuff",
									ActualWeight = 1400
								}
						},
					Services = new List<ShipmentService>(),
					DesiredPickupDate = DateTime.Now,
				};

			_service = new ConnectivityPlatform();
			_request = new FullLoad_Request
				{
					AuthenticationData = new AuthenticationData
						{
							Username = "!WSRate OnlyLP",
							Password = "LogisticsPlus"
						},
					BillTo = new BillTo
						{
							AddressInfo = new Address
								{
									Address1 = "",
									City = "",
									CountryCode = "US",
									State = "PA",
									Zipcode = "16501"
								},
							ContactInfo = new Contact
								{
									ContactName = "Christopher Oyesiku",
									Email = "christopher.oyesiku@logisticsplus.net",
									Phone = "814.461.7643",
								},
							Name = "Emile Zafirov",
							PayType = PayType.Prepaid,
							ShipType = ShipType.ThirdParty,
						},
					Consignee = new Consignee
						{
							AddressInfo = new Address
								{
									Address1 = "",
									City = "",
									CountryCode = "US",
									State = "OH",
									Zipcode = "44125"
								},
							ContactInfo = new Contact
								{
									ContactName = "Christopher Oyesiku",
									Email = "christopher.oyesiku@logisticsplus.net",
									Phone = "814.461.7643",
								},
						},
					Shipper = new Shipper
						{
							AddressInfo = new Address
								{
									Address1 = "1406 Peach St.",
									City = "Erie",
									CountryCode = "US",
									State = "PA",
									Zipcode = "16501"
								},
							ContactInfo = new Contact
								{
									ContactName = "Christopher Oyesiku",
									Email = "christopher.oyesiku@logisticsplus.net",
									Phone = "814.461.7643",
								},
						},
					Loadinfo = new Load { CustomerPO = "Test1234", LoadID = new Random(1000).Next() },
					Products = new[]
						{
							new Product
								{
									Class = FreightClass.c775,
									Length = 58,
									Width = 42,
									Height = 24,
									PackageType = PackageType.Pallets,
									Description = "Stuff",
									Weight = 1400
								}
							//new Product
							//	{
							//		Class = FreightClass.c100,
							//		Length = 48,
							//		Width = 48,
							//		Height = 48,
							//		PackageType = PackageType.Pallets,
							//		Description = "Stuff 2",
							//		Weight = 1000
							//	}
						},
					RateServices = new[]
						{
							new RateServices
								{
									ServiceCode = ServiceCode.LTL,
								}
						}
				};

			_p44 = new P44RateRequst
				{
					Origin = new P44Location
						{
							//City = "Erie",
							Country = "US",
							//State = "PA",
							PostalCode = "16501",
						},
					Destination = new P44Location
						{
							//City = "Cleveland",
							Country = "US",
							//State = "OH",
							PostalCode = "90001"
						},
					ReturnMultiple = true,
					ShipDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm"),
					ShipTimeEnd = "17:00",
					ShipTimeStart = "08:00",
					Items = new List<P44Item>
						{
							new P44Item
								{
									FreightClass = "77.5",
									Length = 58.GetString(),
									Width = 42.GetString(),
									Height = 24.GetString(),
									Weight = 201.GetString(),
									PalletCount = 1.GetString()
								}
						}
				};
		}

		[Test]
		public void BlockingRateTest()
		{
			foreach (var p in _points)
			{
				_request.Shipper.AddressInfo.Zipcode = p[1];
				_request.Consignee.AddressInfo.Zipcode = p[3];

				Console.WriteLine("Points: {0} {1} to {2} {3}", p[1], p[0], p[3], p[2]);

				var watch = new Stopwatch();
				watch.Start();
				Console.WriteLine("Start Time: {0}", DateTime.Now);

				var results = _service.ImportForQuote_Sync(_request);


				Console.WriteLine("End Time: {0} Elasped Time: {1}s", DateTime.Now, watch.Elapsed.Seconds);
				Console.WriteLine();
				Console.WriteLine("Rates:");
				Console.WriteLine();

				if (!string.IsNullOrEmpty(results.ErrorMessage))
				{
					Console.WriteLine(results.ErrorMessage);
					return;
				}



				foreach (var q in results.Quotes)
				{
					Console.WriteLine();
					Console.WriteLine("Carrier: {0}-{1}", q.SCAC, q.CarrierName);
					Console.WriteLine("Quote Id: {0}", q.QuoteID);
					Console.WriteLine("Quote Number: {0}", q.QuoteNumber);
					Console.WriteLine("Transit: {0}", q.TransitTime);
					Console.WriteLine("Customer Price: {0:c2}", q.CustomerPrice.NetPrice);
					Console.WriteLine("Carrier Price: {0:c2}", q.CarrierPrice.NetPrice);
					Console.WriteLine("Raw Price: {0:c2}", q.RawPrice.NetPrice);

				}
				Console.WriteLine();
				Console.WriteLine();
			}
		}

		[Ignore("Blocking test with sleep does the job")]
		[Test]
		public void NonBlockingRateTest()
		{
			var watch = new Stopwatch();
			watch.Start();
			Console.WriteLine("Start Time: {0}", DateTime.Now);

			var results = _service.ImportForQuote(_request);
			Console.WriteLine("Initial Reponse Time: {0}", DateTime.Now);
			var quotes = (results.Quotes ?? new Quote[0]).ToDictionary(i => i.QuoteID, i => i);
			var checkAgain = true;
			var loopControl = 0;
			do
			{
				var qr = _service
					.GetQuotes(new GetQuotes_Request
						{
							AuthenticationData = _request.AuthenticationData,
							LoadID = results.Load.Loadinfo.LoadID.GetValueOrDefault()
						});
				Console.WriteLine("Quote Check {0} Reponse Time: {1}", loopControl + 1, DateTime.Now);
				if (qr.RatingCompleted) checkAgain = false;
				if (qr.Error != null)
				{
					Console.WriteLine("Error: {0}", qr.Error.Message);
				}
				else
				{
					foreach (var q in qr.Quotes.Where(q => !quotes.ContainsKey(q.QuoteID)))
						quotes.Add(q.QuoteID, q);
				}
				if (checkAgain && loopControl++ > 1000) checkAgain = false;
			} while (checkAgain);

			Console.WriteLine("End Time: {0} Elasped Time: {1}s", DateTime.Now, watch.Elapsed.Seconds);
			Console.WriteLine();
			Console.WriteLine("Rates:");
			Console.WriteLine();

			if (!string.IsNullOrEmpty(results.ErrorMessage))
			{
				Console.WriteLine(results.ErrorMessage);
				return;
			}



			foreach (var q in quotes.Values)
			{
				Console.WriteLine();
				Console.WriteLine("Carrier: {0}-{1}", q.SCAC, q.CarrierName);
				Console.WriteLine("Quote Id: {0}", q.QuoteID);
				Console.WriteLine("Quote Number: {0}", q.QuoteNumber);
				Console.WriteLine("Transit: {0}", q.TransitTime);
				Console.WriteLine("Customer Price: {0:c2}", q.CustomerPrice.NetPrice);
				Console.WriteLine("Carrier Price: {0:c2}", q.CarrierPrice.NetPrice);
				Console.WriteLine("Raw Price: {0:c2}", q.RawPrice.NetPrice);

			}
		}

		[Test]
		public void NonBlockingWithSleep250RateTest()
		{
			foreach (var p in _points)
			{
				_request.Shipper.AddressInfo.Zipcode = p[1];
				_request.Consignee.AddressInfo.Zipcode = p[3];

				Console.WriteLine("Points: {0} {1} to {2} {3}", p[1], p[0], p[3], p[2]);

				var watch = new Stopwatch();
				watch.Start();
				Console.WriteLine("Start Time: {0}", DateTime.Now);

				var results = _service.ImportForQuote(_request);
				Console.WriteLine("Initial Reponse Time: {0}", DateTime.Now);
				var quotes = (results.Quotes ?? new Quote[0]).ToDictionary(i => i.QuoteID, i => i);
				var checkAgain = true;
				var loopControl = 0;
				do
				{
					var qr = _service
						.GetQuotes(new GetQuotes_Request
							{
								AuthenticationData = _request.AuthenticationData,
								LoadID = results.Load.Loadinfo.LoadID.GetValueOrDefault()
							});
					Console.WriteLine("Quote Check: {0} Rate count: {1} Reponse Time: {2}", loopControl + 1, quotes.Count, DateTime.Now);
					if (qr.RatingCompleted) checkAgain = false;
					if (qr.Error != null)
					{
						Console.WriteLine("Error: {0}", qr.Error.Message);
					}
					else
					{
						foreach (var q in qr.Quotes.Where(q => !quotes.ContainsKey(q.QuoteID)))
							quotes.Add(q.QuoteID, q);
					}
					if (checkAgain && loopControl++ > 1000) checkAgain = false;
					if (checkAgain) Thread.Sleep(250);
				} while (checkAgain);

				Console.WriteLine("End Time: {0} Elasped Time: {1}s", DateTime.Now, watch.Elapsed.Seconds);
				Console.WriteLine();
				Console.WriteLine("Rates:");
				Console.WriteLine();

				if (!string.IsNullOrEmpty(results.ErrorMessage))
				{
					Console.WriteLine(results.ErrorMessage);
					return;
				}

				foreach (var q in quotes.Values)
				{
					Console.WriteLine();
					Console.WriteLine("Carrier: {0}-{1}", q.SCAC, q.CarrierName);
					Console.WriteLine("Quote Id: {0}", q.QuoteID);
					Console.WriteLine("Quote Number: {0}", q.QuoteNumber);
					Console.WriteLine("Transit: {0}", q.TransitTime);
					Console.WriteLine("Customer Price: {0:c2}", q.CustomerPrice.NetPrice);
					Console.WriteLine("Carrier Price: {0:c2}", q.CarrierPrice.NetPrice);
					Console.WriteLine("Raw Price: {0:c2}", q.RawPrice.NetPrice);

				}
				Console.WriteLine();
				Console.WriteLine();
			}
		}

		[Test]
		public void SmcTest()
		{
			foreach (var p in _points)
			{
				_shipment.Origin.PostalCode = p[1];
				_shipment.Destination.PostalCode = p[3];

				Console.WriteLine("Points: {0} {1} to {2} {3}", p[1], p[0], p[3], p[2]);

				var watch = new Stopwatch();
				watch.Start();
				Console.WriteLine("Start Time: {0}", DateTime.Now);


				var rates = _rater2.GetRates(_shipment);

				watch.Stop();
				Console.WriteLine("End Time: {0} Elasped Time: {1}s", DateTime.Now, watch.Elapsed.Seconds);
				Console.WriteLine();
				var scacs = rates
					.Select(r =>
						{
							var vendor = r.Vendor ?? (r.LTLSellRate != null ? r.LTLSellRate.VendorRating.Vendor : null);
							return vendor == null ? string.Empty : vendor.Scac;
						})
					.Distinct();
				Console.WriteLine("Carrier Count: {0}", scacs.Count());
				Console.WriteLine("Carrier Scacs: {0}", scacs.ToArray().CommaJoin());
				Console.WriteLine();
				Console.WriteLine();
			}
		}

		[Test]
		public void P44Test()
		{
			foreach (var p in _points)
			{
				_p44.Origin.PostalCode = p[1];
				_p44.Destination.PostalCode = p[3];

				Console.WriteLine("----------------------------------------------------------------------");
				Console.WriteLine();
				Console.WriteLine("Points: {0} {1} to {2} {3}", p[1], p[0], p[3], p[2]);

				//var x = new P44RateQuotes
				//	{
				//		RateQuotes = new List<P44RateQuote>
				//			{
				//				new P44RateQuote
				//					{
				//						Mode = string.Empty,
				//						Vendor = "RDNY",
				//						Scac = string.Empty,
				//						CarrierNote = string.Empty,
				//						Errors = new List<P44Error>
				//							{
				//								new P44Error
				//									{

				//									}
				//							},
				//						QuoteNumber = string.Empty,
				//						RateDetail = new P44RateDetail
				//							{
				//								RateAdjustments = new List<P44RateAdjustment>
				//									{
				//										new P44RateAdjustment
				//											{
				//												Amount = 1,
				//												Description = string.Empty,
				//												DescriptionCode = string.Empty,
				//												Rate = 0
				//											},
				//											new P44RateAdjustment
				//											{
				//												Amount = 1,
				//												Description = string.Empty,
				//												DescriptionCode = string.Empty,
				//												Rate = 0
				//											}
				//									}
				//							},
				//						TransitTime = 1.GetString()
				//					},
				//					new P44RateQuote
				//					{
				//						Mode = string.Empty,
				//						Vendor = "RDNY",
				//						Scac = string.Empty,
				//						CarrierNote = string.Empty,
				//						Errors = new List<P44Error>(),
				//						QuoteNumber = string.Empty,
				//						RateDetail = new P44RateDetail(),
				//						TransitTime = 1.GetString()
				//					}
				//			}
				//	};
				//Console.WriteLine(x.ToXml());
				//return;


				var watch = new Stopwatch();
				watch.Start();
				Console.WriteLine("Start Time: {0}", DateTime.Now);

				var data = _p44.ToXml(true, true);
				//Console.WriteLine();
				//Console.WriteLine(data);
				//Console.WriteLine();
				//Console.WriteLine();

				var url = string.Format("http://test.p-44.com/xml/xmlrate/quote.xml?p44Login={0}&p44Password={1}&xml={2}",
				                        "christopher.oyesiku@logisticsplus.net", "admintest", data);
				var wr = (HttpWebRequest) WebRequest.Create(url);
				wr.Method = "POST";

				var webResponse = wr.GetResponse();
				var retData = new StreamReader(webResponse.GetResponseStream()).ReadToEnd();
				//Console.Write(retData);
				//Console.WriteLine();
				//Console.WriteLine();

				var rates = retData.FromXml<P44RateQuotes>();

				//Console.WriteLine(rates.ToXml());
				//Console.WriteLine();
				//Console.WriteLine();

				watch.Stop();

				Console.WriteLine();
				Console.WriteLine();
				Console.WriteLine("End Time: {0} Elasped Time: {1}s", DateTime.Now, watch.Elapsed.Seconds);
				Console.WriteLine();
				Console.WriteLine("Rates:");
				Console.WriteLine();
				Console.WriteLine("Carrier Count: {0}", rates.RateQuotes.Count);
				Console.WriteLine("Carrier Scacs: {0}", rates.RateQuotes.Select(i=>i.Scac).ToArray().CommaJoin());
				Console.WriteLine();

				foreach (var q in rates.RateQuotes)
				{
					if (q.Errors.Any())
					{
						Console.WriteLine();
						Console.WriteLine("Errors: {0}", q.Errors.ToXml());
						Console.WriteLine();
					}

					Console.WriteLine();
					Console.WriteLine("Carrier: {0}-{1} \t Mode: {2}", q.Scac, q.Vendor, q.Mode);
					Console.WriteLine("Quote Service Type: {0}", q.ServiceType);
					Console.WriteLine("Quote Number: {0}", q.QuoteNumber);
					Console.WriteLine("Transit: {0}", q.TransitTime);
					foreach (var d in q.RateDetail.RateAdjustments)
					{
						Console.WriteLine("Amount: {0:c2}", d.Amount);
						Console.WriteLine("Rate: {0:c2}", d.Rate);
						Console.WriteLine("Code: {0}", d.DescriptionCode);
						Console.WriteLine("Description: {0}", d.Description);
					}

				}
			}
		}


		[Serializable]
		[XmlRoot("RateRequest")]
		public class P44RateRequst
		{
			[XmlElement("shipDate")]
			public string ShipDate { get; set; }

			[XmlElement("shipTimeStart")]
			public string ShipTimeStart { get; set; }

			[XmlElement("shipTimeEnd")]
			public string ShipTimeEnd { get; set; }

			[XmlElement("returnMultiple")]
			public bool ReturnMultiple { get; set; }

			[XmlElement("origin")]
			public P44Location Origin { get; set; }

			[XmlElement("destination")]
			public P44Location Destination { get; set; }

			[XmlArray("items")]
			public List<P44Item> Items { get; set; }
		}

		[Serializable]
		public class P44Location
		{
			[XmlElement("country")]
			public string Country { get; set; }

			//[XmlElement("stateName")]
			//public string State { get; set; }

			//[XmlElement("city")]
			//public string City { get; set; }

			[XmlElement("postalCode")]
			public string PostalCode { get; set; }
		}

		[Serializable]
		[XmlType("item")]
		public class P44Item
		{
			[XmlElement("freightClass")]
			public string FreightClass { get; set; }

			[XmlElement("weight")]
			public string Weight { get; set; }

			[XmlElement("weightUnit")]
			public string WeightUnit { get { return "lb"; } set { } }

			[XmlElement("length")]
			public string Length { get; set; }

			[XmlElement("width")]
			public string Width { get; set; }

			[XmlElement("height")]
			public string Height { get; set; }

			[XmlElement("dimUnit")]
			public string DimUnit { get { return "in"; } set { } }

			[XmlElement("palletCount")]
			public string PalletCount { get; set; }
		}

		[Serializable]
		[XmlRoot("RateQuotes")]
		public class P44RateQuotes
		{
			[XmlElement("rateQuote")]
			public List<P44RateQuote> RateQuotes { get; set; } 
		}

		[Serializable]
		[XmlType("rateQuote")]
		public class P44RateQuote
		{
			[XmlElement("mode", IsNullable = true)]
			public string Mode { get; set; }

			[XmlElement("scac", IsNullable = true)]
			public string Scac { get; set; }

			[XmlElement("vendor", IsNullable = true)]
			public string Vendor { get; set; }

			[XmlArray("errors", IsNullable = true)]
			public List<P44Error> Errors { get; set; }

			[XmlElement("carrierNote", IsNullable = true)]
			public string CarrierNote { get; set; }

			[XmlElement("quoteNumber", IsNullable = true)]
			public string QuoteNumber { get; set; }

			[XmlElement("rateDetail", IsNullable = true)]
			public P44RateDetail RateDetail { get; set; }

			[XmlElement("serviceType", IsNullable = true)]
			public string ServiceType { get; set; }

			[XmlElement("totalPallets", IsNullable = true)]
			public string TotalPallets { get; set; }

			[XmlElement("totalPieces", IsNullable = true)]
			public string TotalPieces { get; set; }

			[XmlElement("totalWeight", IsNullable = true)]
			public string TotalWeight { get; set; }

			[XmlElement("transitTime", IsNullable = true)]
			public string TransitTime { get; set; }
		}

		[Serializable]
		[XmlType("error")]
		public class P44Error
		{
			[XmlElement("errorCode", IsNullable = true)]
			public string ErrorCode { get; set; }

			[XmlElement("errorMessage", IsNullable = true)]
			public string ErrorMessage { get; set; }

			[XmlElement("vendorErrorCode", IsNullable = true)]
			public string VendorErrorCode { get; set; }

			[XmlElement("vendorErrorMessage", IsNullable = true)]
			public string VendorErrorMessage { get; set; }

			[XmlElement("message", IsNullable = true)]
			public string Message { get; set; }
		}

		[Serializable]
		[XmlType("rateDetail")]
		public class P44RateDetail
		{
			[XmlArray("rateAdjustments")]
			public List<P44RateAdjustment> RateAdjustments { get; set; }

			[XmlElement("total")]
			public decimal Total { get; set; }
		}

		[Serializable]
		[XmlType("rateAdjustment")]
		public class P44RateAdjustment
		{
			[XmlElement("description")]
			public string Description { get; set; }

			[XmlElement("descriptionCode")]
			public string DescriptionCode { get; set; }

			[XmlElement("amount")]
			public decimal Amount { get; set; }

			[XmlElement("rate")]
			public decimal Rate { get; set; }
		}
	}
}
