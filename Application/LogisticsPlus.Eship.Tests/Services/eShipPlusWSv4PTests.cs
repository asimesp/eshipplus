﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Processor.Searches.Connect;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Views.Connect;
using LogisticsPlus.Eship.Tests.eShipPlusWSv4P;
using NUnit.Framework;

namespace LogisticsPlus.Eship.Tests.Services
{
    [TestFixture]
    [Ignore("Run on demand only")]
    public class eShipPlusWSv4PTests
    {
        private const string ServerPath = "http://lp6s/EShipV4/Services/eShipPlusWSv4P.asmx";
        private const string LocalPath = "http://localhost:55216/Services/eShipPlusWSv4P.asmx";
        private const string Key = "9dd08c2c-0753-45f6-8d5b-8553f635e0e2";

        private static eShipPlusWSv4P.eShipPlusWSv4P Service
        {
            get
            {
            	return new eShipPlusWSv4P.eShipPlusWSv4P { Url = ServerPath };
            }
        }

        [SetUp]
        public void Initialize()
        {
            if (!GlobalTestInitializer.IsInitialized) GlobalTestInitializer.Initialize();
        }

        [Test]
        public void CanUpdateFaxTransmission()
        {
			var criteria = new FaxTransmissionSearchCriteria();
            var trans = new FaxTransmissionSearch().FetchFaxTransmissions(criteria, GlobalTestInitializer.DefaultTenantId);
            var key = trans.Any() ? trans[0].TransmissionKey : Guid.Empty;

            var @return = Service.UpdateFaxTransmission(key.ToString(), DateTime.Now.ToString(), FaxTransmissionStatus.Failed);
            foreach (var msg in @return.Messages)
                Console.WriteLine("Type: {0} \t\t Message: {1}", msg.Type, msg.Value);
        }

        [Test]
        public void CanUpdateFaxTransmissions()
        {
			var criteria = new FaxTransmissionSearchCriteria();
            var trans = new FaxTransmissionSearch().FetchFaxTransmissions(criteria, GlobalTestInitializer.DefaultTenantId);
            var key = trans.Any() ? trans[0].TransmissionKey : Guid.Empty;
            var t = new WSFaxTransmissionUpdate { Key = key.ToString(), Message = DateTime.Now.ToString(), Status = FaxTransmissionStatus.Failed };

            var returns = Service.UpdateFaxTransmissions(new[] { t });
            foreach (var @return in returns)
                foreach (var msg in @return.Messages)
                    Console.WriteLine("Type: {0} \t\t Message: {1}", msg.Type, msg.Value);
        }

        [Test]
        public void CanTrackShipments()
        {
            var credentials = new WSCredentials
                                  {
                                      AccessCode = GlobalTestInitializer.ActiveUser.Tenant.Code,
                                      AccessKey = new Guid(Key),
                                      Password = GlobalTestInitializer.ActiveUser.Password,
                                      Username = GlobalTestInitializer.ActiveUser.Username
                                  };

            var infoRequestReturn = Service.GetTrackingInfoRequests(credentials, "FXFE", DateTime.Now.AddMonths(-2));

            if (infoRequestReturn.Messages.Any())
                foreach (var message in infoRequestReturn.Messages)
                    Console.WriteLine("Type: {0}\t\t Message: {1}", message.Type, message.Value);

            if (infoRequestReturn.TrackingInfoRequests == null) return;

            Console.WriteLine("Return Count: {0}", infoRequestReturn.TrackingInfoRequests.Count());

            foreach (var request in infoRequestReturn.TrackingInfoRequests)
            {
                var ps = request.GetType().GetProperties().Where(p => p.CanRead);
                Console.WriteLine();
                foreach (var p in ps)
                    Console.Write("{0}: {1}\t", p.Name, p.GetValue(request, null));

            }

            var shipmentToUpate = infoRequestReturn.TrackingInfoRequests.FirstOrDefault();
            if(shipmentToUpate==null)return;

            //get actual shipment info so that we can replace original values when we are done testing.
            var actualShipment = new ShipmentSearch().FetchShipmentByShipmentNumber(shipmentToUpate.BillOfLadingNumber,
                                                                                 GlobalTestInitializer.DefaultTenantId);
            var actualPickUpdate = actualShipment.ActualPickupDate;
            var actualDeliveryDate = actualShipment.ActualDeliveryDate;
            var actualVendor = actualShipment.Vendors.FirstOrDefault(v => v.Vendor.Scac == shipmentToUpate.Scac);
            if(actualVendor == null)return;
            var actualPro = actualVendor.ProNumber;

            //create new tracking info object to return to the webservice
            var newTrackingInfo = new WSShipmentTrackingInfo
                                  {
                                      ShipmentNumber = shipmentToUpate.BillOfLadingNumber,
                                      Scac = shipmentToUpate.Scac,
                                      PickUpDate = DateTime.Now.AddDays(-1),
                                      PickupDatePresent = true,
                                      DeliveryDate = DateTime.Now,
                                      DeliveryDatePresent = true,
                                      ProNumber = DateTime.Now.ToString(),
                                      ProNumberIsPresent = true
                                  };
            //return the tracking info to the webservice
            var infoUpdateRequestReturn = Service.UpdateTrackingInfoRequests(credentials, newTrackingInfo);
            foreach (var msg in infoUpdateRequestReturn.Messages)
                Console.WriteLine("Type: {0} \t\t Message: {1}", msg.Type, msg.Value);

            //return the original values to the webservice
            newTrackingInfo.PickUpDate = actualPickUpdate;
            newTrackingInfo.DeliveryDate = actualDeliveryDate;
            newTrackingInfo.ProNumber = actualPro;
           
            infoUpdateRequestReturn = Service.UpdateTrackingInfoRequests(credentials, newTrackingInfo);
            foreach (var msg in infoUpdateRequestReturn.Messages)
                Console.WriteLine("Type: {0} \t\t Message: {1}", msg.Type, msg.Value);
        }

        [Test]
        public void CanFetchAvailableLoads()
        {
            var credentials = new WSCredentials
            {
                AccessCode = GlobalTestInitializer.ActiveUser.Tenant.Code,
                AccessKey = new Guid(Key),
                Password = GlobalTestInitializer.ActiveUser.Password,
                Username = GlobalTestInitializer.ActiveUser.Username
            };

            var loads = Service.GetAvailableLoads(credentials);
            foreach (var msg in loads.Messages)
                    Console.WriteLine("Type: {0} \t\t Message: {1}", msg.Type, msg.Value);
        }
    }
}
