﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Core
{
	public static class CoreUtilities
	{
		private const char Hash = '#';
		private const char Question = '?';
		private const char Star = '*';

	    public struct Suffix
		{
			public const string Invoice = "V";
			public const string Credit = "C";
			public const string Supplemental = "B";
		}


		public static char[] DataSeperators()
		{
			return new[] { '\t', ',', ';', ':', '|' };
		}

		public static char[] Seperators()
		{
			var chars = DataSeperators().ToList();
			chars.AddRange(Environment.NewLine.ToArray());
			return chars.ToArray();
		}


		internal static int ToInt(this object value)
		{
			var x = default(int);
			return value == null
					? x
					: value.GetType().IsEnum
						? (int)value
						: Int32.TryParse(value.ToString(), out x) ? x : default(int);
		}

		internal static decimal ToDecimal(this object value)
		{
			var x = default(decimal);
			return value == null ? x : Decimal.TryParse(value.ToString(), out x) ? x : default(decimal);
		}

		internal static double ToDouble(this object value)
		{
			var x = default(double);
			return value == null ? x : Double.TryParse(value.ToString(), out x) ? x : default(double);
		}


		public static string EntityName(this object entity)
		{
			return entity.GetType().Name.FormattedString();
		}


		public static string FormattedString(this object value)
		{
			return value == null ? String.Empty : Regex.Replace(value.ToString(), @"([a-z])([A-Z])", @"$1 $2", RegexOptions.None);
		}

        public static string FormattedString(this AccountBucket accountBucket)
        {
            return accountBucket == null ? String.Empty : String.Format("{0} - {1}", accountBucket.Code, accountBucket.Description);
        }

		public static string FormattedString(this ChargeCode code)
		{
			return code == null ? String.Empty : String.Format(" {0} ({1})", code.Description, code.Code);
		}

		public static string FormattedString(this DocumentTag tag)
		{
			return tag == null ? String.Empty : String.Format(" {0} ({1})", tag.Description, tag.Code);
		}

		public static string FormattedString(this MileageSource source)
		{
			return source == null ? String.Empty : String.Format(" {0} ({1})", source.Description, source.Code);
		}

		public static string FormattedString(this FailureCode code)
		{
			return code == null ? String.Empty : String.Format(" {0} ({1})", code.Description, code.Code);
		}

		public static string FormattedString(this ShipmentPriority priority)
		{
			return priority == null ? String.Empty : String.Format(" {0} ({1})", priority.Description, priority.Code);
		}

		public static string FormattedString(this InsuranceType type)
		{
			return type == null ? String.Empty : String.Format(" {0} ({1})", type.Description, type.Code);
		}

		public static string FormattedString(this EquipmentType type)
		{
			return type == null ? String.Empty : String.Format(" {0} ({1})", type.TypeName, type.Code);
		}

		public static string FormattedString(this UserDepartment dept)
		{
			return dept == null ? String.Empty : String.Format(" {0} ({1})", dept.Description, dept.Code);
		}

		public static string FormattedString(this Service service)
		{
			return service == null ? String.Empty : String.Format(" {0} ({1})", service.Description, service.Code);
		}


		public static decimal TotalActualWeight(this IEnumerable<ShipmentItem> items)
		{
			return items.Sum(i => i.ActualWeight);
		}

		public static decimal TotalWeight(this IEnumerable<LoadOrderItem> items)
		{
			return items.Sum(i => i.Weight);
		}


		public static bool AlertVendorInsurance(this Vendor vendor, int alertThresholdDays)
		{
			return !vendor.Insurances.Any() || vendor.Insurances.Any(i => i.ExpirationDate < DateTime.Now.AddDays(alertThresholdDays));
		}

		public static string VendorInsuranceAlertText(this Vendor vendor, int alertThresholdDays)
		{
			return !vendor.Insurances.Any()
			       	? "No Insurance on file"
			       	: vendor.Insurances.Any(i => i.ExpirationDate < DateTime.Now.AddDays(alertThresholdDays))
			       	  	? "Expired Insurance"
			       	  	: String.Empty;
		}


		public static bool CreditAlert(this decimal credit, decimal outstandingBalance, decimal alertThreshold)
		{
			return (credit - outstandingBalance) <= alertThreshold;
		}
	

		public static ShipmentStatus InferShipmentStatus(this Shipment shipment, bool ignoreVoidAndInvoiced)
		{
			if (!ignoreVoidAndInvoiced)
			{
				if (shipment.Status == ShipmentStatus.Void) return ShipmentStatus.Void;
				if (shipment.Status == ShipmentStatus.Invoiced) return ShipmentStatus.Invoiced;
			}

			return shipment.ActualDeliveryDate > DateUtility.SystemEarliestDateTime
			       	? ShipmentStatus.Delivered
			       	: shipment.ActualPickupDate > DateUtility.SystemEarliestDateTime
			       	  	? ShipmentStatus.InTransit
			       	  	: ShipmentStatus.Scheduled;
		}


		public static string ApplicableEmails(this Tier tier, ServiceMode mode = ServiceMode.NotApplicable)
		{
			switch(mode)
			{
				case ServiceMode.LessThanTruckload:
					return tier.LTLNotificationEmails;
				case ServiceMode.Air:
					return tier.AirNotificationEmails;
				case ServiceMode.Truckload:
					return tier.FTLNotificationEmails;
				case ServiceMode.Rail:
					return tier.RailNotificationEmails;
				case ServiceMode.SmallPackage:
					return tier.SPNotificationEmails;
				default:
					return tier.GeneralNotificationEmails;
			}
		}


		public static string GetWeightBreakValue(this WeightBreak weightBreak)
		{
			switch (weightBreak)
			{
				case WeightBreak.M5C:
					return "500";
				case WeightBreak.M1M:
					return "1000";
				case WeightBreak.M2M:
					return "2000";
				case WeightBreak.M5M:
					return "5000";
				case WeightBreak.M10M:
					return "10000";
				case WeightBreak.M20M:
					return "20000";
				case WeightBreak.M30M:
					return "30000";
				case WeightBreak.M40M:
					return "40000";
				default:
					return String.Empty;
			}
		}


		public static void AppendNumberSuffix(this Invoice invoice)
		{
			switch(invoice.InvoiceType)
			{
				case InvoiceType.Supplemental:
					invoice.InvoiceNumber += Suffix.Supplemental;
					break;
				case InvoiceType.Credit:
					invoice.InvoiceNumber += Suffix.Credit;
					break;
				case InvoiceType.Invoice:
					invoice.InvoiceNumber += Suffix.Invoice;
					break;
			}
		}

		public static bool NumberIsEqualToSuffix(this Invoice invoice)
		{
			return invoice.InvoiceNumber == Suffix.Supplemental ||
			       invoice.InvoiceNumber == Suffix.Credit ||
			       invoice.InvoiceNumber == Suffix.Invoice;
		}


		public static bool PickupIsLate(this Shipment shipment)
		{
			var customer = shipment.Customer ?? new Customer();
			var tenant = shipment.Tenant ?? new Tenant();
			var baseDate = shipment.ActualPickupDate != DateUtility.SystemEarliestDateTime
			                  	? shipment.ActualPickupDate
			                  	: DateTime.Now;
			DateTime compareDate;
			if (customer.CustomerToleranceEnabled && shipment.ServiceMode == ServiceMode.Truckload)
				compareDate = shipment.DesiredPickupDate.SetTime(shipment.LatePickup).AddMinutes(customer.TruckloadPickupTolerance);
			else if (!customer.CustomerToleranceEnabled && shipment.ServiceMode == ServiceMode.Truckload)
				compareDate = shipment.DesiredPickupDate.SetTime(shipment.LatePickup).AddMinutes(tenant.TruckloadPickupTolerance);
			else if (shipment.ServiceMode == ServiceMode.LessThanTruckload)
				compareDate = shipment.DesiredPickupDate.SetToEndOfDay();
			else
				compareDate = shipment.DesiredPickupDate.SetTime(shipment.LatePickup);

			return baseDate > compareDate;

		}

		public static bool DeliveryIsLate(this Shipment shipment)
		{
			var baseDate = shipment.ActualDeliveryDate != DateUtility.SystemEarliestDateTime
								? shipment.ActualDeliveryDate
								: DateTime.Now;
			DateTime compareDate;
			var customer = shipment.Customer ?? new Customer();
			var tenant = shipment.Tenant ?? new Tenant();
			if (customer.CustomerToleranceEnabled && shipment.ServiceMode == ServiceMode.Truckload)
				compareDate = shipment.EstimatedDeliveryDate.SetTime(shipment.LateDelivery).AddMinutes(customer.TruckloadDeliveryTolerance);
			else if (!customer.CustomerToleranceEnabled && shipment.ServiceMode == ServiceMode.Truckload)
				compareDate = shipment.EstimatedDeliveryDate.SetTime(shipment.LateDelivery).AddMinutes(tenant.TruckloadDeliveryTolerance);
			else if (shipment.ServiceMode == ServiceMode.LessThanTruckload)
				compareDate = shipment.EstimatedDeliveryDate.SetToEndOfDay();
			else
				compareDate = shipment.EstimatedDeliveryDate.SetTime(shipment.LateDelivery);

			return baseDate > compareDate;

		}

		public static bool ShipmentAccountBucketCanBeChanged(this Shipment shipment)
		{
			return shipment.Status != ShipmentStatus.Invoiced && shipment.Charges.All(c => c.VendorBillId == default(long));
		}

		public static List<Customer> FilterCustomersForUser(this User user, List<Customer> customers)
		{
			if (user.TenantEmployee) return customers;
			var ucIds = user.UserShipAs
							.Select(usa => usa.CustomerId)
							.ToList();
			ucIds.Add(user.DefaultCustomer.Id);
			return customers.Where(c => ucIds.Contains(c.Id)).ToList();
		}


		public static SalesRepCommTier ApplicableTier(this SalesRepresentative rep, DateTime applicableDate,
		                                              ServiceMode mode = ServiceMode.NotApplicable, Customer customer = null)
		{
			// customer and mode specific
			var tier = rep.SalesRepCommTiers
			              .Where(t => customer != null && t.CustomerId == customer.Id)
			              .Where(t => t.EffectiveDate.TimeToMinimum() <= applicableDate.TimeToMinimum() &&
			                          t.ExpirationDate.TimeToMaximum() >= applicableDate.TimeToMaximum())
			              .Where(t => t.ServiceMode == mode)
			              .OrderByDescending(t => t.EffectiveDate)
			              .FirstOrDefault();

			if (tier != null) return tier;

			// customer specific filter (supercedes mode only filter)
			tier = rep.SalesRepCommTiers
			          .Where(t => customer != null && t.CustomerId == customer.Id)
			          .Where(t => t.EffectiveDate.TimeToMinimum() <= applicableDate.TimeToMinimum() &&
			                      t.ExpirationDate.TimeToMaximum() >= applicableDate.TimeToMaximum())
			          .Where(t => t.ServiceMode == ServiceMode.NotApplicable)
			          .OrderByDescending(t => t.EffectiveDate)
			          .FirstOrDefault();

			if (tier != null) return tier;

			// mode specific filter
			tier = rep.SalesRepCommTiers
			          .Where(t => t.Customer == null)
			          .Where(t => t.EffectiveDate.TimeToMinimum() <= applicableDate.TimeToMinimum() &&
			                      t.ExpirationDate.TimeToMaximum() >= applicableDate.TimeToMaximum())
			          .Where(t => t.ServiceMode == mode)
			          .OrderByDescending(t => t.EffectiveDate)
			          .FirstOrDefault();

			if (tier != null) return tier;

			// non specific (all filter if nothing above matches) - exclude customer and mode
			return rep.SalesRepCommTiers
			          .Where(t => t.Customer == null)
			          .Where(t => t.EffectiveDate.TimeToMinimum() <= applicableDate.TimeToMinimum() &&
			                      t.ExpirationDate.TimeToMaximum() >= applicableDate.TimeToMaximum())
			          .Where(t => t.ServiceMode == ServiceMode.NotApplicable)
			          .OrderByDescending(t => t.EffectiveDate)
			          .FirstOrDefault();
		}



		public static bool IsValidPostalCodeFormat(this CustomerPurchaseOrder purchaseOrder, string postalCode, bool wildcardIsValidInPostalCode = false)
		{
			var containsWildCard = purchaseOrder.ValidPostalCode.Contains(Hash) || purchaseOrder.ValidPostalCode.Contains(Star) ||
			                       purchaseOrder.ValidPostalCode.Contains(Question);

			return containsWildCard
				       ? purchaseOrder.ValidPostalCode.IsValidPostalCodeFormat(postalCode, wildcardIsValidInPostalCode)
				       : purchaseOrder.ValidPostalCode.ToLower() == postalCode.ToLower();
		}

	    public static bool IsValidPostalCodeFormat(this PostalCode postalCode, bool wildcardIsValidInPostalCode = false)
		{
			if (string.IsNullOrEmpty(postalCode.Code)) return true;
			if (string.IsNullOrEmpty(postalCode.Country.PostalCodeValidation)) return true;

			return postalCode.Country.IsValidPostalCodeFormat(postalCode.Code, wildcardIsValidInPostalCode);
		}

		public static bool IsValidPostalCodeFormat(this Country country, string postalCode, bool wildcardIsValidInPostalCode = false)
		{
			if (string.IsNullOrEmpty(country.PostalCodeValidation)) return true;

			return country.PostalCodeValidation.IsValidPostalCodeFormat(postalCode, wildcardIsValidInPostalCode);
		}

		public static bool IsValidPostalCodeFormat(this string formatPattern, string postalCode, bool wildcardIsValidInPostalCode = false)
		{
			var codeArr = postalCode.ToArray();
			var formats = formatPattern
				.Split(new[] {'|'}, StringSplitOptions.RemoveEmptyEntries)
				.Select(i => i.ToArray())
				.ToList();
			if (formats.All(i => i.Length != codeArr.Length)) return false;
			return formats
				.Where(f => f.Length == codeArr.Length)
				.Any(f =>
				{
					var match1 = true;
					for (var i1 = 0; i1 < codeArr.Length; i1++)
						switch (f[i1])
						{
							case Hash:
							    if (!char.IsDigit(codeArr[i1]))
							    {
							        if (wildcardIsValidInPostalCode)
							        {
							            if (codeArr[i1] != '#') match1 = false;
                                    }
							        else match1 = false;
                                }
							    break;
							case Question:
							    if (!char.IsLetter(codeArr[i1]))
							    {
							        if (wildcardIsValidInPostalCode)
							        {
							            if (codeArr[i1] != '?') match1 = false;
							        }
                                    else match1 = false;
                                }
                                break;
							case Star:
							    if (!char.IsDigit(codeArr[i1]) && !char.IsLetter(codeArr[i1]))
                                {
                                    if (wildcardIsValidInPostalCode)
                                    {
                                        if (codeArr[i1] != '#' && codeArr[i1] != '?' && codeArr[i1] != '*') match1 = false;
                                    }
                                    else match1 = false;
                                }
                                break;
							default:
							    if (!char.IsDigit(codeArr[i1]) && !char.IsLetter(codeArr[i1])) match1 = false;
							    break;
						}
					return match1;
				});
		}


		public static T Copy<T>(this T source) where T : ObjectBase, new()
		{
			var newObj = new T();
			newObj.Copy(source);
			return newObj;
		}

		public static List<T> Copy<T>(this List<T> source) where T : ObjectBase, new()
		{
			return source.Select(i => i.Copy()).ToList();
		}


		public static string AppendVendorRatingCriticalCommentToShipment(this VendorRating rating, string value)
		{

			return rating == null || String.IsNullOrEmpty(rating.CriticalBOLNotes)
				       ? value
				       : String.IsNullOrEmpty(value)
					         ? rating.CriticalBOLNotes
					         : String.Format("{0}|{1}", value, rating.CriticalBOLNotes);
		}


		public static WeightBreak GetBilledWeightBreak(this decimal billedWeight)
		{
			var weightBreak = new WeightBreak();

			if (billedWeight > 0 && billedWeight < 500) weightBreak = WeightBreak.L5C;
			if (billedWeight >= 500 && billedWeight < 1000) weightBreak = WeightBreak.M5C;
			if (billedWeight >= 1000 && billedWeight < 2000) weightBreak = WeightBreak.M1M;
			if (billedWeight >= 2000 && billedWeight < 5000) weightBreak = WeightBreak.M2M;
			if (billedWeight >= 5000 && billedWeight < 10000) weightBreak = WeightBreak.M5M;
			if (billedWeight >= 10000 && billedWeight < 20000) weightBreak = WeightBreak.M10M;
			if (billedWeight >= 20000 && billedWeight < 30000) weightBreak = WeightBreak.M20M;
			if (billedWeight >= 30000 && billedWeight < 40000) weightBreak = WeightBreak.M30M;
			if (billedWeight >= 40000) weightBreak = WeightBreak.M40M;

			return weightBreak;
		}


		public static string StripNonDigits(this string value)
		{
			return string.IsNullOrEmpty(value) ? string.Empty : Regex.Replace(value, "([^0-9])", string.Empty, RegexOptions.None);
		}

	    public static string StripSpacesFromEmails(this string emailString)
	    {
	        if (string.IsNullOrEmpty(emailString)) return string.Empty;
	        return emailString.Replace(" ", string.Empty);
	    }
	}
}
