﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Administration;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core
{
	[Serializable]
	[Entity("AuditLog", ReadOnly = false, Source = EntitySource.TableView)]
	public class AuditLog : TenantBase
	{
		private long _userId;

		private User _user;

	    [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[Property("EntityCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string EntityCode { get; set; }

		[Property("EntityId", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string EntityId { get; set; }

		[Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Description { get; set; }

		[Property("LogDateTime", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime LogDateTime { get; set; }

	    [Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long UserId
		{
			get
			{
				if (_user != null) _userId = _user.Id;
				return _userId;
			}
			set
			{
				_userId = value;
				if (_user != null && value != _user.Id) _user = null;
			}
		}

		public User User
		{
			get { return _user ?? (_user = new User(_userId)); }
			set
			{
				_user = value;
				_userId = value == null ? default(long) : value.Id;
			}
		}

		public void Log()
		{
			Insert();
		}

        public AuditLog(long id)
        {
            Id = id;
            if (Id == default(long)) return;
            KeyLoaded = Load();
        }

		public AuditLog(DbDataReader reader)
		{
			Load(reader);
		}
        
        public AuditLog()
        {
            
        }
	}
}
