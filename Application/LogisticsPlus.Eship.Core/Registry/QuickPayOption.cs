﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Cache;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Registry
{
	[Serializable]
	[Entity("QuickPayOption", ReadOnly = false, Source = EntitySource.TableView)]
	public class QuickPayOption : TenantBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("Code")]
		[Property("Code", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Code { get; set; }

		[EnableSnapShot("Description")]
		[Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Description { get; set; }

		[EnableSnapShot("DiscountPercent", Description = "Discount Percentage")]
		[Property("DiscountPercent", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal DiscountPercent { get; set; }

		[EnableSnapShot("Active")]
		[Property("Active", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Active { get; set; }

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public QuickPayOption()
		{
		}

		public QuickPayOption(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			if (CoreCache.QuickPayOptions.ContainsKey(Id))
			{
				Copy(CoreCache.QuickPayOptions[Id]);
				KeyLoaded = true;
			}
			else KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public QuickPayOption(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long)) base.Delete();
		}
	}
}
