﻿namespace LogisticsPlus.Eship.Core.Registry
{
	public enum EquipmentTypeGroup
	{
        // DO NOT REORDER. SEE APPLICABLE JOINS ON DATABASE REFEQUIPMENTTYPEGROUP
        MiscAccessorial = 0,
		DryVan,
		Flatbed,
		Refrigerated,
		Specialized,
		TankerOrBulk = 5,
		Rail,
		CargoVanExpedite,
		StraightTruckExpedite,
		ContainerDrayage,
	}
}
