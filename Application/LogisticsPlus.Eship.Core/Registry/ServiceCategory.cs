﻿namespace LogisticsPlus.Eship.Core.Registry
{
	public enum ServiceCategory
	{
		Freight = 0,
		Service,
		Accessorial
	}
}
