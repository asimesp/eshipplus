﻿namespace LogisticsPlus.Eship.Core.Registry
{
	public enum ChargeCodeCategory
	{
        // DO NOT REORDER. SEE APPLICABLE JOINS ON DATABASE REFCHARGECODECATEGORY
        Freight = 0,
		Fuel,
		Accessorial,
		Service
	}
}
