﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Registry
{
	[Serializable]
	[Entity("DocumentTemplate", ReadOnly = false, Source = EntitySource.TableView)]
	public class DocumentTemplate : TenantBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("TemplatePath", Description = "Template Path")]
		[Property("TemplatePath", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string TemplatePath { get; set; }

		[EnableSnapShot("Code")]
		[Property("Code", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Code { get; set; }

		[EnableSnapShot("Description")]
		[Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Description { get; set; }

		[EnableSnapShot("Category")]
		[Property("Category", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public DocumentTemplateCategory Category { get; set; }

		[EnableSnapShot("ServiceMode", Description = "Service Mode")]
		[Property("ServiceMode", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ServiceMode ServiceMode { get; set; }

		[EnableSnapShot("Primary", Description = "Default Template Designation")]
		[Property("Primary", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Primary { get; set; }

		[EnableSnapShot("WeightDecimals", Description = "Weights rounded to decimal places")]
		[Property("WeightDecimals", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string WeightDecimals { get; set; }

		[EnableSnapShot("DimensionDecimals", Description = "Dimensions rounded to decimal places")]
		[Property("DimensionDecimals", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DimensionDecimals { get; set; }

		[EnableSnapShot("CurrencyDecimals", Description = "Currency rounded to decimal places")]
		[Property("CurrencyDecimals", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CurrencyDecimals { get; set; }

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public DocumentTemplate() { }

		public DocumentTemplate(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public DocumentTemplate(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long)) base.Delete();
		}
	}
}
