﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Cache;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Registry
{
	[Serializable]
	[Entity("ShipmentPriority", ReadOnly = false, Source = EntitySource.TableView)]
	public class ShipmentPriority : TenantBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("Code")]
		[Property("Code", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Code { get; set; }

		[EnableSnapShot("Description")]
		[Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Description { get; set; }

		[EnableSnapShot("Default")]
		[Property("Default", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Default { get; set; }

		[EnableSnapShot("Active")]
		[Property("Active", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Active { get; set; }

		[EnableSnapShot("PriorityColor", Description = "Priority Color")]
		[Property("PriorityColor", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string PriorityColor { get; set; }

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public ShipmentPriority()
		{
		}

		public ShipmentPriority(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			if (CoreCache.ShipmentPriorities.ContainsKey(Id))
			{
				Copy(CoreCache.ShipmentPriorities[Id]);
				KeyLoaded = true;
			}
			else KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public ShipmentPriority(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long)) base.Delete();
		}
	}
}
