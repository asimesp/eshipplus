﻿namespace LogisticsPlus.Eship.Core.Registry
{
    public enum ServiceDispatchFlag
    {
        // NOTE: Do not Re-arrange this!!!
        None = 0,
        LiftGate,
        BlindShipment,
        BrokerSelectOption,
        CashOnDelivery,
        DangerousGoods = 5,
        Detention,
        DryIce,
        Exhibition,
        ExtraLabor,
        ExtremeLength = 10,
        HoldAtLocation,
        Holiday,
        Inside,
        LimitedAccess,
        MarkingOrTagging = 15,
        Port,
        PreDeliveryNotification,
        ProtectionFromFreezing,
        Saturday,
        SortAndSegregate = 20,
        Storage,
        Sunday
    }
}
