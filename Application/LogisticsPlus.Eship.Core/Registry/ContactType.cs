﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Cache;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Registry
{
	[Serializable]
	[Entity("ContactType", ReadOnly = false, Source = EntitySource.TableView)]
	public class ContactType : TenantBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("Code")]
		[Property("Code", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Code { get; set; }

		[EnableSnapShot("Description")]
		[Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Description { get; set; }

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public ContactType() { }

		public ContactType(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			if (CoreCache.ContactTypes.ContainsKey(Id))
			{
				Copy(CoreCache.ContactTypes[Id]);
				KeyLoaded = true;
			}
			else KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public ContactType(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long)) base.Delete();
		}
	}
}
