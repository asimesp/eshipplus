﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Cache;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Registry
{
    [Serializable]
    [Entity("FailureCode", ReadOnly = false, Source = EntitySource.TableView)]
   public class FailureCode:TenantBase
    {
        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; set; }

        [EnableSnapShot("Code")]
        [Property("Code", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Code { get; set; }

        [EnableSnapShot("Description")]
        [Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Description { get; set; }

        [EnableSnapShot("Active")]
        [Property("Active", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool Active { get; set; }

        [EnableSnapShot("Category")]
        [Property("Category", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public FailureCodeCategory Category { get; set; }

        [EnableSnapShot("ExcludeOnScoreCard")]
        [Property("ExcludeOnScoreCard", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool ExcludeOnScoreCard { get; set; }

        public bool IsNew
        {
            get { return Id == default(long); }
        }

        public FailureCode() {}

		public FailureCode(long id) : this (id, false)
		{
			
		}

		public FailureCode(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			if (CoreCache.FailureCodes.ContainsKey(Id))
			{
				Copy(CoreCache.FailureCodes[Id]);
				KeyLoaded = true;
			}
			else KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public FailureCode(DbDataReader reader)
		{
			Load(reader);
		}

        public void Save()
        {
            if (Id == default(long)) Insert();
            else Update();
        }

        public new void Delete()
        {
            base.Delete();
        }
    }
}
