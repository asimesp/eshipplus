﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Cache;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Registry
{
	[Serializable]
    [Entity("AccountBucketUnit", ReadOnly = false, Source = EntitySource.TableView)]
    public class AccountBucketUnit : TenantBase
    {
        private long _accountBucketId;

        private AccountBucket _accountBucket;

        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; private set; }

        [EnableSnapShot("Name")]
        [Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Name { get; set; }

        [EnableSnapShot("Description")]
        [Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Description { get; set; }

        [EnableSnapShot("Active")]
        [Property("Active", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool Active { get; set; }

        [EnableSnapShot("AccountBucketId", Description = "Account Bucket Reference")]
        [Property("AccountBucketId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long AccountBucketId
        {
            get
            {
                if (_accountBucket != null) _accountBucketId = _accountBucket.Id;
                return _accountBucketId;
            }
            set
            {
                _accountBucketId = value;
                if (_accountBucket != null && value != _accountBucket.Id) _accountBucket = null;
            }
        }

        public AccountBucket AccountBucket
        {
            get { return _accountBucket ?? (_accountBucket = new AccountBucket(_accountBucketId)); }
            set
            {
                _accountBucket = value;
                _accountBucketId = value == null ? default(long) : value.Id;
            }
        }

        public bool IsNew
		{
			get { return Id == default(long); }
		}

		public AccountBucketUnit() { }

		public AccountBucketUnit(long id) : this(id, false){}

		public AccountBucketUnit(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			if (CoreCache.AccountBucketUnits.ContainsKey(Id))
			{
				Copy(CoreCache.AccountBucketUnits[Id]);
				KeyLoaded = true;
			}
			else KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public AccountBucketUnit(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long)) base.Delete();
		}
    }
}
