﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Registry
{
	[Serializable]
	[Entity("Setting", ReadOnly = false, Source = EntitySource.TableView)]
	public class Setting : TenantBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("Code")]
		[Property("Code", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public SettingCode Code { get; set; }

		[EnableSnapShot("Value")]
		[Property("Value", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Value { get; set; }

        public bool IsNew
		{
			get { return Id == default(long); }
		}

		public Setting() { }

		public Setting(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public Setting(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long)) base.Delete();
		}
	}
}
