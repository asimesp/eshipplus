﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Cache;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Registry
{
	[Serializable]
	[Entity("EquipmentType", ReadOnly = false, Source = EntitySource.TableView)]
	public class EquipmentType : TenantBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("Code", Description = "Code")]
		[Property("Code", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Code { get; set; }

		[EnableSnapShot("TypeName", Description = "Name")]
		[Property("TypeName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string TypeName { get; set; }

		[EnableSnapShot("Active")]
		[Property("Active", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Active { get; set; }

		[EnableSnapShot("Group")]
		[Property("Group", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public EquipmentTypeGroup Group { get; set; }

        [EnableSnapShot("DatEquipmentType", Description = "DAT Equipment Type")]
        [Property("DatEquipmentType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public DatEquipmentType DatEquipmentType { get; set; }


		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public EquipmentType() { }

		public EquipmentType(long id) : this(id, false)
		{
			
		}

		public EquipmentType(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			if (CoreCache.EquipmentTypes.ContainsKey(Id))
			{
				Copy(CoreCache.EquipmentTypes[Id]);
				KeyLoaded = true;
			}
			else KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public EquipmentType(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long)) base.Delete();
		}
	}
}
