﻿namespace LogisticsPlus.Eship.Core.Registry
{
    public enum FailureCodeCategory
    {
        Freight = 0,
        Service,
        SalesRepresentative,
        User
    }
}
