﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Registry
{
	[Serializable]
	[Entity("DocumentPrefixMap", ReadOnly = false, Source = EntitySource.TableView)]
	public class DocumentPrefixMap : TenantBase
	{
		private long _prefixId;
		private long _documentTemplateId;

		private Prefix _prefix;
		private DocumentTemplate _documentTemplate;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("PrefixId", Description = "Prefix Id")]
		[Property("PrefixId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long PrefixId
		{
			get { return _prefixId; }
			set
			{
				_prefixId = value;
				if (_prefix != null && value != _prefix.Id) _prefix = null;
			}
		}

		[EnableSnapShot("DocumentTemplateId", Description = "Document Template Id")]
		[Property("DocumentTemplateId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long DocumentTemplateId
		{
			get { return _documentTemplateId; }
			set
			{
				_documentTemplateId = value;
				if (_documentTemplate != null && value != _documentTemplate.Id) _documentTemplate = null;
			}
		}
		
		public Prefix Prefix
		{
			get { return _prefixId == default(long) ? null : _prefix ?? (_prefix = new Prefix(_prefixId, false)); }
			set
			{
				_prefix = value;
				_prefixId = value == null ? default(long) : value.Id;
			}
		}

		public DocumentTemplate DocumentTemplate
		{
			get { return _documentTemplateId == default(long) ? null : _documentTemplate ?? (_documentTemplate = new DocumentTemplate(_documentTemplateId, false)); }
			set
			{
				_documentTemplate = value;
				_documentTemplateId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public DocumentPrefixMap() { }

		public DocumentPrefixMap(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public DocumentPrefixMap(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long)) base.Delete();
		}
	}
}
