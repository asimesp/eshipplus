﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Cache;
using LogisticsPlus.Eship.Core.Operations;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Registry
{
	[Serializable]
	[Entity("MileageSource", ReadOnly = false, Source = EntitySource.TableView)]
	public class MileageSource : TenantBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("Code")]
		[Property("Code", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Code { get; set; }

		[EnableSnapShot("Description")]
		[Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Description { get; set; }

		[EnableSnapShot("Primary")]
		[Property("Primary", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Primary { get; set; }

		[EnableSnapShot("MileageEngine", Description = "Mileage Engine Reference")]
		[Property("MileageEngine", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public MileageEngine MileageEngine { get; set; }

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public MileageSource() { }

		public MileageSource(long id)
			: this(id, false)
		{

		}

		public MileageSource(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			if (CoreCache.MileageSources.ContainsKey(Id))
			{
				Copy(CoreCache.MileageSources[Id]);
				KeyLoaded = true;
			}
			else KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public MileageSource(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long)) base.Delete();
		}
	}
}
