﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Cache;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Registry
{
    [Serializable]
    [Entity("P44ChargeCodeMapping", ReadOnly = false, Source = EntitySource.TableView)]
    public class P44ChargeCodeMapping : TenantBase
    {
        private long _chargeCodeId;

        private ChargeCode _chargeCode;

        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; protected set; }

        [EnableSnapShot("Project44Code")]
        [Property("Project44Code", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public Project44ChargeCode Project44Code { get; set; }

        [EnableSnapShot("ChargeCodeId", Description = "Charge Code Reference")]
        [Property("ChargeCodeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long ChargeCodeId
        {
            get { return _chargeCodeId; }
            set
            {
                _chargeCodeId = value;
                if (_chargeCode != null && value != _chargeCode.Id) _chargeCode = null;
            }
        }

        public ChargeCode ChargeCode
        {
            get { return _chargeCodeId == default(long) ? null : _chargeCode ?? (_chargeCode = new ChargeCode(_chargeCodeId, false)); }
            set
            {
                _chargeCode = value;
                _chargeCodeId = value == null ? default(long) : value.Id;
            }
        }

        public bool IsNew
        {
            get { return Id == default(long); }
        }

        public P44ChargeCodeMapping() { }

        public P44ChargeCodeMapping(long id, bool takeSnapshot = false)
        {
            Id = id;
            if (Id == default(long)) return;
            if (CoreCache.P44ChargeCodeMappings.ContainsKey(Id))
            {
                Copy(CoreCache.P44ChargeCodeMappings[Id]);
                KeyLoaded = true;
            }
            else KeyLoaded = Load();
            if (takeSnapshot) TakeSnapShot();
        }

        public P44ChargeCodeMapping(DbDataReader reader)
        {
            Load(reader);
        }

        public void Save()
        {
            if (Id == default(long)) Insert();
            else Update();
        }

        public new void Delete()
        {
            if (Id != default(long)) base.Delete();
        }
    }
}
