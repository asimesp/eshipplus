﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Cache;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Registry
{
	[Serializable]
	[Entity("ChargeCode", ReadOnly = false, Source = EntitySource.TableView)]
	public class ChargeCode : TenantBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("Code")]
		[Property("Code", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Code { get; set; }

		[EnableSnapShot("Description")]
		[Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Description { get; set; }

		[EnableSnapShot("Active")]
		[Property("Active", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Active { get; set; }

		[EnableSnapShot("SurpressOnCarrierRateAgreement", Description = "Surpress On Carrier Rate Agreement")]
		[Property("SurpressOnCarrierRateAgreement", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool SurpressOnCarrierRateAgreement { get; set; }

		[EnableSnapShot("Category")]
		[Property("Category", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ChargeCodeCategory Category { get; set; }

	    [EnableSnapShot("Project44Code")]
	    [Property("Project44Code", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
	    public Project44ChargeCode Project44Code { get; set; }

        public bool IsNew
		{
			get { return Id == default(long); }
		}

		public ChargeCode() { }

		public ChargeCode(long id) : this(id, false)
		{
		}

		public ChargeCode(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			if (CoreCache.ChargeCodes.ContainsKey(Id))
			{
				Copy(CoreCache.ChargeCodes[Id]);
				KeyLoaded = true;
			}
			else KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public ChargeCode(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long)) base.Delete();
		}
	}
}
