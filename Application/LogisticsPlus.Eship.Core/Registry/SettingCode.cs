﻿namespace LogisticsPlus.Eship.Core.Registry
{
	public enum SettingCode
	{
		SpecialistSupportEmail = 0,
		SalesTeamEmail,
		RecordLockLimit,
		AdvancedCustomerLTLScheduleLimit,
		CustomerCreditAlertThreshold,
		VendorInsuranceAlertThresholdDays = 5,
		MonitorEmail,
		DeveloperAccessRequestSupportEmail,
		TenantAdminNotificationEmail,
		DeleteCompletedRateAnalysisRecordAfterDays,
	}
}
