﻿namespace LogisticsPlus.Eship.Core.Registry
{
	public static class IataCodes
	{
		public static class Country
		{
			public const string Usa = "US";
			public const string Mexico = "MX";
			public const string Canada = "CA";
		}
	}
}
