﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Cache;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Registry
{
	[Serializable]
	[Entity("Country", ReadOnly =  false, Source =  EntitySource.TableView)]
	public class Country : ObjectBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

        [EnableSnapShot("Code", Description = "Country Code")]
		[Property("Code", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Code { get; set; }

        [EnableSnapShot("Name", Description = "Country Name")]
		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

        [EnableSnapShot("EmploysPostalCodes", Description = "Country Employs Postal Codes")]
		[Property("EmploysPostalCodes", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool EmploysPostalCodes { get; set; }

        [EnableSnapShot("SortWeight", Description = "Country Sort Weight")]
        [Property("SortWeight", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int SortWeight { get; set; }

        [EnableSnapShot("PostalCodeValidation", Description = "Country Postal Code Validation")]
        [Property("PostalCodeValidation", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string PostalCodeValidation { get; set; }

	    [EnableSnapShot("Project44CountryCode", Description = "Project 44 Country Code")]
	    [Property("Project44CountryCode", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
	    public Project44CountryCode Project44CountryCode { get; set; }

        public bool IsNew
		{
			get { return Id == default(long); }
		}

		public Country()
        {}

		public Country(long id)
            : this(id, false)
		{
		}

        public Country(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			if (CoreCache.Countries.ContainsKey(Id))
			{
				Copy(CoreCache.Countries[Id]);
				KeyLoaded = true;
			}
			else KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public Country(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long)) base.Delete();
		}
	}
}
