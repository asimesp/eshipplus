﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Cache;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Registry
{
    [Serializable]
    [Entity("P44ServiceMapping", ReadOnly = false, Source = EntitySource.TableView)]
    public class P44ServiceMapping : TenantBase
    {
        private long _serviceId;

        private Service _service;

        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; protected set; }

        [EnableSnapShot("Project44Code")]
        [Property("Project44Code", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public Project44ServiceCode Project44Code { get; set; }

        [EnableSnapShot("ServiceId", Description = "Service Reference")]
        [Property("ServiceId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long ServiceId
        {
            get { return _serviceId; }
            set
            {
                _serviceId = value;
                if (_service != null && value != _service.Id) _service = null;
            }
        }

        public Service Service
        {
            get { return _serviceId == default(long) ? null : _service ?? (_service = new Service(_serviceId, false)); }
            set
            {
                _service = value;
                _serviceId = value == null ? default(long) : value.Id;
            }
        }

        public bool IsNew
        {
            get { return Id == default(long); }
        }

        public P44ServiceMapping() { }

        public P44ServiceMapping(long id, bool takeSnapshot = false)
        {
            Id = id;
            if (Id == default(long)) return;
            if (CoreCache.P44ServiceMappings.ContainsKey(Id))
            {
                Copy(CoreCache.P44ServiceMappings[Id]);
                KeyLoaded = true;
            }
            else KeyLoaded = Load();
            if (takeSnapshot) TakeSnapShot();
        }

        public P44ServiceMapping(DbDataReader reader)
        {
            Load(reader);
        }

        public void Save()
        {
            if (Id == default(long)) Insert();
            else Update();
        }

        public new void Delete()
        {
            if (Id != default(long)) base.Delete();
        }
    }
}
