﻿namespace LogisticsPlus.Eship.Core.Registry
{
	public enum ServiceMode
	{
        // DO NOT REORDER. SEE APPLICABLE JOINS ON DATABASE REFSERVICEMODE
		LessThanTruckload = 0,
		Truckload,
		Air,
		Rail,
		SmallPackage,
		NotApplicable = 5,
	}
}
