﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Registry
{
	[Serializable]
	[Entity("PostalCode", ReadOnly = false, Source = EntitySource.TableView)]
	public class PostalCode : ObjectBase
	{
		private long _countryId;

		private Country _country;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("Code")]
		[Property("Code", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Code { get; set; }

		[EnableSnapShot("City")]
		[Property("City", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string City { get; set; }

		[EnableSnapShot("CityAlias", Description = "City Alias")]
		[Property("CityAlias", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CityAlias { get; set; }

		[EnableSnapShot("State")]
		[Property("State", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string State { get; set; }

		[EnableSnapShot("CountryId", Description = "Country Reference")]
		[Property("CountryId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long CountryId
		{
			get { return _countryId; }
			set
			{
				_countryId = value;
				if (_country != null && value != _country.Id) _country = null;
			}
		}

		[EnableSnapShot("Primary")]
		[Property("Primary", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Primary { get; set; }

		[EnableSnapShot("Latitude")]
		[Property("Latitude", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
		public double Latitude { get; set; }

		[EnableSnapShot("Longitude")]
		[Property("Longitude", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
		public double Longitude { get; set; }

		public bool IsNew
		{
			get { return Id == default(long); }
		}
		
		public Country Country
		{
			get { return _countryId == default(long) ? null : _country ?? (_country = new Country(_countryId)); }
			set
			{
				_country = value;
				_countryId = value == null ? default(long) : value.Id;
			}
		}

		public PostalCode()
		{
		}

		public PostalCode(long id)
            : this(id, false)
		{
		}

        public PostalCode(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public PostalCode(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
