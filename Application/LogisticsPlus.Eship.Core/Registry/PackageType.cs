﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Cache;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Registry
{
	[Serializable]
	[Entity("PackageType", ReadOnly = false, Source = EntitySource.TableView)]
	public class PackageType : TenantBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("TypeName", Description = "name")]
		[Property("TypeName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string TypeName { get; set; }

		[EnableSnapShot("EdiOid", Description = "EDI Order Identification Detail Code")]
		[Property("EdiOid", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string EdiOid { get; set; }

		[EnableSnapShot("Active")]
		[Property("Active", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Active { get; set; }

		[EnableSnapShot("DefaultLength", Description = "Default Length")]
		[Property("DefaultLength", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal DefaultLength { get; set; }

		[EnableSnapShot("DefaultHeight", Description = "Default Height")]
		[Property("DefaultHeight", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal DefaultHeight { get; set; }

		[EnableSnapShot("DefaultWidth" , Description = "Default Width")]
		[Property("DefaultWidth", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal DefaultWidth { get; set; }

		[EnableSnapShot("EditableDimensions", Description = "Editable Dimensions")]
		[Property("EditableDimensions", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool EditableDimensions { get; set; }

		[EnableSnapShot("SortWeight", Description = "Sort Weight")]
		[Property("SortWeight", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int SortWeight { get; set; }

	    [EnableSnapShot("Project44Code", Description = "Project 44 Code")]
	    [Property("Project44Code", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
	    public int Project44Code { get; set; }

        public bool IsNew
		{
			get { return Id == default(long); }
		}

		public PackageType()
		{
		}

		public PackageType(long id) : this(id, false)
		{
		}

		public PackageType(long id, bool takeSnapshot)
		{

			Id = id;
			if (Id == default(long)) return;
			if (CoreCache.PackageTypes.ContainsKey(Id))
			{
				Copy(CoreCache.PackageTypes[Id]);
				KeyLoaded = true;
			}
			else KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public PackageType(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long)) base.Delete();
		}
	}
}
