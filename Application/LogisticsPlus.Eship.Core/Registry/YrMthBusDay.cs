﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Registry
{
	[Serializable]
    [Entity("YrMthBusDay", ReadOnly = false, Source = EntitySource.TableView)]
    public class YrMthBusDay : TenantBase
    {
        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; private set; }

        [EnableSnapShot("January")]
        [Property("January", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int January { get; set; }

        [EnableSnapShot("February")]
        [Property("February", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int February { get; set; }

        [EnableSnapShot("March")]
        [Property("March", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int March { get; set; }

        [EnableSnapShot("April")]
        [Property("April", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int April { get; set; }

        [EnableSnapShot("May")]
        [Property("May", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int May { get; set; }

        [EnableSnapShot("June")]
        [Property("June", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int June { get; set; }

        [EnableSnapShot("July")]
        [Property("July", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int July { get; set; }

        [EnableSnapShot("August")]
        [Property("August", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int August { get; set; }

        [EnableSnapShot("September")]
        [Property("September", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int September { get; set; }

        [EnableSnapShot("October")]
        [Property("October", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int October { get; set; }

        [EnableSnapShot("November")]
        [Property("November", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int November { get; set; }

        [EnableSnapShot("December")]
        [Property("December", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int December { get; set; }

        [EnableSnapShot("Year")]
        [Property("Year", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Year { get; set; }

        public bool IsNew
		{
			get { return Id == default(long); }
		}

		public YrMthBusDay() { }

		public YrMthBusDay(long id) : this(id, false)
		{
		}

		public YrMthBusDay(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

        public YrMthBusDay(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long)) base.Delete();
		}
    }
}