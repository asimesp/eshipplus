﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Cache;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Registry
{
	[Serializable]
	[Entity("AccountBucket", ReadOnly = false, Source = EntitySource.TableView)]
	public class AccountBucket : TenantBase
	{
        private List<AccountBucketUnit> _accountBucketUnits;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }
		
		[EnableSnapShot("Code")]
		[Property("Code", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Code { get; set; }

		[EnableSnapShot("Description")]
		[Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Description { get; set; }

		[EnableSnapShot("Active")]
		[Property("Active", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Active { get; set; }


        public List<AccountBucketUnit> AccountBucketUnits
        {
            get
            {
                if (_accountBucketUnits == null) LoadAccountBucketUnits();
                return _accountBucketUnits;
            }
            set { _accountBucketUnits = value; }
        }


		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public AccountBucket() { }

		public AccountBucket(long id) : this(id, false){}

		public AccountBucket(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			if (CoreCache.AccountBuckets.ContainsKey(Id))
			{
				Copy(CoreCache.AccountBuckets[Id]);
				KeyLoaded = true;
			}
			else KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public AccountBucket(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long)) base.Delete();
		}

        public void LoadAccountBucketUnits()
        {
            _accountBucketUnits = new List<AccountBucketUnit>();
            const string query = "SELECT * FROM AccountBucketUnit WHERE TenantId = @TenantId and AccountBucketId = @AccountBucketId";
            var parameters = new Dictionary<string, object> { { "TenantId", TenantId }, { "AccountBucketId", Id } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _accountBucketUnits.Add(new AccountBucketUnit(reader));
            Connection.Close();
        }
	}
}
