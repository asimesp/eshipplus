﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Registry
{
	[Serializable]
	[Entity("NoServiceDay", ReadOnly = false, Source = EntitySource.TableView)]
	public class NoServiceDay : TenantBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("DateOfNoService")]
		[Property("DateOfNoService", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateOfNoService { get; set; }

		[EnableSnapShot("Description")]
		[Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Description { get; set; }

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public NoServiceDay() { }

		public NoServiceDay(long id) : this(id, false)
		{
			
		}

		public NoServiceDay(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public NoServiceDay(DbDataReader reader)
		{
			Load(reader);
		}


		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long)) base.Delete();
		}
	}
}
