﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Cache;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Registry
{
	[Serializable]
	[Entity("Service", ReadOnly = false, Source = EntitySource.TableView)]
	public class Service : TenantBase
	{
		private long _chargeCodeId;

		private ChargeCode _chargeCode;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("Code")]
		[Property("Code", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Code { get; set; }

		[EnableSnapShot("Description")]
		[Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Description { get; set; }

		[EnableSnapShot("Category")]
		[Property("Category", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ServiceCategory Category { get; set; }

	    [EnableSnapShot("Project44Code")]
	    [Property("Project44Code", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
	    public Project44ServiceCode Project44Code { get; set; }

        [EnableSnapShot("DispatchFlag", Description = "Dispatch Flag")]
		[Property("DispatchFlag", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ServiceDispatchFlag DispatchFlag { get; set; }

		[EnableSnapShot("ApplicableAtPickup", Description = "Applicable At Pickup")]
		[Property("ApplicableAtPickup", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ApplicableAtPickup { get; set; }

		[EnableSnapShot("ApplicableAtDelivery", Description = "Applicable At Delivery")]
		[Property("ApplicableAtDelivery", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ApplicableAtDelivery { get; set; }

		[EnableSnapShot("ChargeCodeId", Description = "Charge Code Reference")]
		[Property("ChargeCodeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ChargeCodeId
		{
			get { return _chargeCodeId; }
			set
			{
				_chargeCodeId = value;
				if (_chargeCode != null && value != _chargeCode.Id) _chargeCode = null;
			}
		}

		public ChargeCode ChargeCode
		{
			get { return _chargeCodeId == default(long) ? null : _chargeCode ?? (_chargeCode = new ChargeCode(_chargeCodeId, false)); }
			set
			{
				_chargeCode = value;
				_chargeCodeId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public Service() { }

		public Service(long id) : this(id, false)
		{
			
		}

		public Service(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			if (CoreCache.Services.ContainsKey(Id))
			{
				Copy(CoreCache.Services[Id]);
				KeyLoaded = true;
			}
			else KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public Service(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long)) base.Delete();
		}
	}
}
