﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Registry
{
	[Serializable]
    [Entity("UserDepartment", ReadOnly = false, Source = EntitySource.TableView)]
    public class UserDepartment : TenantBase
    {
        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; private set; }

        [EnableSnapShot("Code")]
        [Property("Code", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Code { get; set; }

        [EnableSnapShot("Description")]
        [Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Description { get; set; }

        [EnableSnapShot("Phone")]
        [Property("Phone", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Phone { get; set; }

        [EnableSnapShot("Fax")]
        [Property("Fax", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Fax { get; set; }

        [EnableSnapShot("Email")]
        [Property("Email", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Email { get; set; }

        [EnableSnapShot("Comments")]
        [Property("Comments", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Comments { get; set; }

        public bool IsNew
        {
            get { return Id == default(long); }
        }

        public UserDepartment()
        {
        }

        public UserDepartment(long id)
            : this(id, false)
        {

        }

        public UserDepartment(long id, bool takeSnapshot)
        {
            Id = id;
            if (Id == default(long)) return;
            KeyLoaded = Load();
            if (takeSnapshot) TakeSnapShot();
        }

        public UserDepartment(DbDataReader reader)
        {
            Load(reader);
        }

        public void Save()
        {
            if (Id == default(long)) Insert();
            else Update();
        }

        public new void Delete()
        {
            if (Id != default(long)) base.Delete();
        }
    }
}
