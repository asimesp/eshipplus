﻿namespace LogisticsPlus.Eship.Core.Registry
{
	public enum DocumentTemplateCategory
	{
		BillOfLading = 0,
		RateConfirmation,
		Quote,
		ShipmentInvoice,
		ServiceTicketInvoice,
		MiscellaneousInvoice = 5,
		StatementInvoice,
        LoadOrder,
        ShipmentStatement,
        LoadTender,
        ProLabel4x6inch,
        ProLabelAvery3x4inch
    }
}
