﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core
{
	[Serializable]
	[Entity("TenantLocation", ReadOnly = false, Source = EntitySource.TableView)]
	public class TenantLocation : Location
	{
		private List<TenantContact> _contacts;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		public bool IsNew { get { return Id == default(long); } }

		public List<TenantContact>  Contacts
		{
			get
			{
				if (_contacts == null) LoadContacts();
				return _contacts;
			}
			set { _contacts = value; }
		}

		public TenantLocation()
		{
			
		}

		public TenantLocation(long id) : this(id, false)
		{
		}

		public TenantLocation(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if(takeSnapshot) TakeSnapShot();
		}

		public TenantLocation(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

		public void LoadCollections()
		{
			LoadContacts();
		}

		private void LoadContacts()
		{
			_contacts = new List<TenantContact>();
			const string query = "Select * from TenantContact where TenantLocationId = @TenantLocationId and TenantId = @TenantId";
			IDictionary<string, object> parameters = new Dictionary<string, object> { { "TenantLocationId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_contacts.Add(new TenantContact(reader));
			Connection.Close();
		}
	}
}
