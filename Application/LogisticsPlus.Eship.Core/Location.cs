﻿using System;
using System.Data;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core
{
	[Serializable]
	[Entity("Location", ReadOnly = false, Source = EntitySource.TableView)]
	public class Location : TenantBase
	{
		private long _countryId;

		private Country _country;

		[EnableSnapShot("Street1", Description = "Address Line 1")]
		[Property("Street1", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Street1 { get; set; }

		[EnableSnapShot("Street2", Description = "Address Line 2")]
		[Property("Street2", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Street2 { get; set; }

		[EnableSnapShot("City")]
		[Property("City", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string City { get; set; }

		[EnableSnapShot("State")]
		[Property("State", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string State { get; set; }

		[EnableSnapShot("CountryId", Description = "Country Reference")]
		[Property("CountryId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long CountryId
		{
			get { return _countryId; }
			set
			{
				_countryId = value;
				if (_country != null && value != _country.Id) _country = null;
			}
		}

		[EnableSnapShot("PostalCode", Description = "Postal Code")]
		[Property("PostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string PostalCode { get; set; }

		public string FullAddress
		{
			get
			{
				return string.Format("{0} {1} {2} {3} {4}", CombinedStreet, City, State, PostalCode,
				                     Country == null ? string.Empty : Country.Name);
			}
		}

		public string CombinedStreet
		{
			get { return string.Format("{0}{1}{2}", Street1, string.IsNullOrEmpty(Street2) ? string.Empty : ", ", Street2); }
		}

		public Country Country
		{
			get { return _countryId == default(long) ? null : _country ?? (_country = new Country(_countryId)); }
			set
			{
				_country = value;
				_countryId = value == null ? default(long) : value.Id;
			}
		}
	}
}
