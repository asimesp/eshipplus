﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Cms
{
	[Serializable]
	[Entity("News", ReadOnly = false, Source = EntitySource.TableView, SourceName = "CmsNews")]
	public class News : ObjectBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("NewsKey")]
		[Property("NewsKey", AutoValueOnInsert = false, DataType = SqlDbType.UniqueIdentifier, Key = false)]
		public Guid NewsKey { get; set; }
		
		[EnableSnapShot("Title")]
		[Property("Title", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Title { get; set; }
		
		[EnableSnapShot("ShortText", Description = "Short Text")]
		[Property("ShortText", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ShortText { get; set; }
		
		[EnableSnapShot("FullText", Description = "Full Text")]
		[Property("FullText", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string FullText { get; set; }
	
		[EnableSnapShot("DateCreated", Description = "Date Created")]
		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }
		
		[EnableSnapShot("ExpirationDate", Description = "Expiration Date")]
		[Property("ExpirationDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime ExpirationDate { get; set; }

		[EnableSnapShot("Hidden")]
		[Property("Hidden", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Hidden { get; set; }

		[EnableSnapShot("Priority")]
		[Property("Priority", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int Priority { get; set; }

		public bool IsNew { get { return Id == default(long); } }

		public News()
		{
			Id = default(long);
		}

		public News(long id):this(id, false)
		{
			
		}

		public News(long id, bool takeSnapShot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if(takeSnapShot) TakeSnapShot();
		}

		public News(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
