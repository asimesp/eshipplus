﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Cms
{
	[Serializable]
	[Entity("Content", ReadOnly = false, Source = EntitySource.TableView, SourceName = "CmsContent")]
	public class Content: ObjectBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("ContentKey",Description = "Content Key")]
		[Property("ContentKey", AutoValueOnInsert = false, DataType = SqlDbType.UniqueIdentifier, Key = false)]
		public Guid ContentKey { get; set; }
		
		[EnableSnapShot("Current")]
		[Property("Current", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Current { get; set; }
		
		[EnableSnapShot("DateCreated", Description = "Date Created")]
		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[EnableSnapShot("FullText", Description = "Full Text")]
		[Property("FullText", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string FullText { get; set; }

		[EnableSnapShot("IdentifierCode", Description = "Identifier Code")]
		[Property("IdentifierCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string IdentifierCode { get; set; }

		[EnableSnapShot("ShortName", Description = "Short Name")]
		[Property("ShortName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ShortName { get; set; }

		[EnableSnapShot("ShortText", Description = "Short Text")]
		[Property("ShortText", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ShortText { get; set; }

		public bool IsNew { get { return Id == default(long); } }

		public Content()
		{
			Id = default(long);
			ContentKey = Guid.Empty;
		}

		public Content(long id): this(id, false)
		{
			
		}

		public Content(long id, bool takeSnapShot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if(takeSnapShot) TakeSnapShot();
		}

		public Content(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
