﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Core.Cache
{
	public static class CoreCache
	{
		private static readonly object LockObj = new object();

		// Preloaded collections (A-Z)
		public static Dictionary<long, AccountBucket> AccountBuckets { get; private set; }
		public static Dictionary<long, AccountBucketUnit> AccountBucketUnits { get; private set; }
		public static Dictionary<long, Asset> Assets { get; private set; }
		public static Dictionary<long, ChargeCode> ChargeCodes { get; private set; }
		public static Dictionary<long, ContactType> ContactTypes { get; private set; }
		public static Dictionary<long, Country> Countries { get; private set; }
		public static Dictionary<long, DocumentTag> DocumentTags { get; private set; }
		public static Dictionary<long, EquipmentType> EquipmentTypes { get; private set; }
		public static Dictionary<long, FailureCode> FailureCodes { get; private set; }
		public static Dictionary<long, InsuranceType> InsuranceTypes { get; private set; }
		public static Dictionary<long, MileageSource> MileageSources { get; private set; }
		public static Dictionary<long, QuickPayOption> QuickPayOptions { get; private set; }
		public static Dictionary<long, PackageType> PackageTypes { get; private set; }
		public static Dictionary<long, PermissionDetail> PermissionDetails { get; private set; }
		public static Dictionary<long, Prefix> Prefixes { get; private set; }
		public static Dictionary<long, Service> Services { get; private set; }
        public static Dictionary<long, P44ChargeCodeMapping> P44ChargeCodeMappings { get; private set; }
        public static Dictionary<long, P44ServiceMapping> P44ServiceMappings { get; private set; }
        public static Dictionary<long, ShipmentPriority> ShipmentPriorities { get; private set; }
		public static Dictionary<long, SmallPackagingMap> SmallPackagingMaps { get; private set; }
		public static Dictionary<long, SmallPackageServiceMap> SmallPackageServiceMaps { get; private set; }


		// loaded as used collections (A-Z)
		public static Dictionary<long, CustomerRating> CustomerRatings { get; set; }


		public static void Initialize()
		{
			// preloaded
			var initializer = new CoreCacheInitializer();

			AccountBuckets = initializer.FetchAccountBuckets().ToDictionary(i => i.Id, i => i);
			AccountBucketUnits = initializer.FetchAccountBucketUnits().ToDictionary(i => i.Id, i => i);
			Assets = initializer.FetchAssets().ToDictionary(i => i.Id, i => i);
			ChargeCodes = initializer.FetchChargeCodes().ToDictionary(i => i.Id, i => i);
			ContactTypes = initializer.FetchContactTypes().ToDictionary(i => i.Id, i => i);
			Countries = initializer.FetchCountries().ToDictionary(i => i.Id, i => i);
			DocumentTags = initializer.FetchDocumentTags().ToDictionary(i => i.Id, i => i);
			EquipmentTypes = initializer.FetchEquipmentTypes().ToDictionary(i => i.Id, i => i);
			FailureCodes = initializer.FetchFailureCodes().ToDictionary(i => i.Id, i => i);
			InsuranceTypes = initializer.FetchInsuranceTypes().ToDictionary(i => i.Id, i => i);
			MileageSources = initializer.FetchMileageSources().ToDictionary(i => i.Id, i => i);
			QuickPayOptions = initializer.FetchQuickPayOptions().ToDictionary(i => i.Id, i => i);
			PackageTypes = initializer.FetchPackageTypes().ToDictionary(i => i.Id, i => i);
			PermissionDetails = initializer.FetchPermissionDetails().ToDictionary(i => i.Id, i => i);
			Prefixes = initializer.FetchPrefixes().ToDictionary(i => i.Id, i => i);
			Services = initializer.FetchServices().ToDictionary(i => i.Id, i => i);
            P44ChargeCodeMappings = initializer.FetchP44ChargeCodeMappings().ToDictionary(i => i.Id, i => i);
            P44ServiceMappings = initializer.FetchP44ServiceMappings().ToDictionary(i => i.Id, i => i);
            ShipmentPriorities = initializer.FetchShipmentPrioritys().ToDictionary(i => i.Id, i => i);
			SmallPackagingMaps = initializer.FetchSmallPackagingMaps().ToDictionary(i => i.Id, i => i);
			SmallPackageServiceMaps = initializer.FetchSmallPackageServiceMaps().ToDictionary(i => i.Id, i => i);

			// loaded as used
			CustomerRatings = new Dictionary<long, CustomerRating>();
		}


		// preloaded collections (A-Z)

		public static void UpdateCache(AccountBucket item, bool drop = false)
		{
			if(AccountBuckets.ContainsKey(item.Id))
				lock (LockObj)
				{
					if (drop) AccountBuckets.Remove(item.Id);
					else AccountBuckets[item.Id] = item;
				}
			else if (item.Id != default(long) && !drop) AccountBuckets.Add(item.Id, item);
		}

		public static void UpdateCache(AccountBucketUnit item, bool drop = false)
		{
			if (AccountBucketUnits.ContainsKey(item.Id))
				lock (LockObj)
				{
					if (drop) AccountBucketUnits.Remove(item.Id);
					else AccountBucketUnits[item.Id] = item;
				}
			else if (item.Id != default(long) && !drop) AccountBucketUnits.Add(item.Id, item);
		}

		public static void UpdateCache(Asset item, bool drop = false)
		{
			if (Assets.ContainsKey(item.Id))
				lock (LockObj)
				{
					if (drop) Assets.Remove(item.Id);
					else Assets[item.Id] = item;
				}
			else if (item.Id != default(long) && !drop) Assets.Add(item.Id, item);
		}

		public static void UpdateCache(ChargeCode item, bool drop = false)
		{
			if (ChargeCodes.ContainsKey(item.Id))
				lock (LockObj)
				{
					if (drop) ChargeCodes.Remove(item.Id);
					else
					{
						ChargeCodes[item.Id] = item;

						// updates
						foreach (var rating in CustomerRatings.Values.ToList())
						{
							if (rating.InsuranceChargeCodeId == item.Id) rating.InsuranceChargeCode = item;
							foreach (var spr in rating.SmallPackRates.Where(spr => spr.ChargeCodeId == item.Id))
								spr.ChargeCode = item;
							foreach (var sr in rating.LTLSellRates.ToList())
							{
								if (sr.VendorRating.FuelChargeCodeId == item.Id) sr.VendorRating.FuelChargeCode = item;
								if (sr.VendorRating.LTLPackageSpecificFreightChargeCodeId == item.Id) sr.VendorRating.LTLPackageSpecificFreightChargeCode = item;
								foreach (var dt in sr.VendorRating.LTLDiscountTiers.Where(t => t.FreightChargeCodeId == item.Id))
									dt.FreightChargeCode = item;
								foreach (var addlChg in sr.VendorRating.LTLAdditionalCharges.Where(t => t.ChargeCodeId == item.Id))
									addlChg.ChargeCode = item;
								foreach (var over in sr.VendorRating.LTLOverLengthRules.Where(t => t.ChargeCodeId == item.Id))
									over.ChargeCode = item;
							}
						}

						foreach (var serv in Services.Values.Where(serv => serv.ChargeCodeId == item.Id))
							serv.ChargeCode = item;
					}
				}
			else if (item.Id != default(long) && !drop) ChargeCodes.Add(item.Id, item);
		}

		public static void UpdateCache(ContactType item, bool drop = false)
		{
			if (ContactTypes.ContainsKey(item.Id))
				lock (LockObj)
				{
					if (drop) ContactTypes.Remove(item.Id);
					else ContactTypes[item.Id] = item;
				}
			else if (item.Id != default(long) && !drop) ContactTypes.Add(item.Id, item);
		}

		public static void UpdateCache(Country item, bool drop = false)
		{
			if (Countries.ContainsKey(item.Id))
				lock (LockObj)
				{
					if (drop) Countries.Remove(item.Id);
					else Countries[item.Id] = item;
				}
			else if (item.Id != default(long) && !drop) Countries.Add(item.Id, item);
		}

		public static void UpdateCache(DocumentTag item, bool drop = false)
		{
			if (DocumentTags.ContainsKey(item.Id))
				lock (LockObj)
				{
					if (drop) DocumentTags.Remove(item.Id);
					else DocumentTags[item.Id] = item;
				}
			else if (item.Id != default(long) && !drop) DocumentTags.Add(item.Id, item);
		}

		public static void UpdateCache(EquipmentType item, bool drop = false)
		{
			if (EquipmentTypes.ContainsKey(item.Id))
				lock (LockObj)
				{
					if (drop) EquipmentTypes.Remove(item.Id);
					else EquipmentTypes[item.Id] = item;
				}
			else if (item.Id != default(long) && !drop) EquipmentTypes.Add(item.Id, item);
		}

		public static void UpdateCache(FailureCode item, bool drop = false)
		{
			if (FailureCodes.ContainsKey(item.Id))
				lock (LockObj)
				{
					if (drop) FailureCodes.Remove(item.Id);
					else FailureCodes[item.Id] = item;
				}
			else if (item.Id != default(long) && !drop) FailureCodes.Add(item.Id, item);
		}

		public static void UpdateCache(InsuranceType item, bool drop = false)
		{
			if (InsuranceTypes.ContainsKey(item.Id))
				lock (LockObj)
				{
					if (drop) InsuranceTypes.Remove(item.Id);
					else InsuranceTypes[item.Id] = item;
				}
			else if (item.Id != default(long) && !drop) InsuranceTypes.Add(item.Id, item);
		}

		public static void UpdateCache(MileageSource item, bool drop = false)
		{
			if (MileageSources.ContainsKey(item.Id))
				lock (LockObj)
				{
					if (drop) MileageSources.Remove(item.Id);
					else MileageSources[item.Id] = item;
				}
			else if (item.Id != default(long) && !drop) MileageSources.Add(item.Id, item);
		}

		public static void UpdateCache(QuickPayOption item, bool drop = false)
		{
			if (QuickPayOptions.ContainsKey(item.Id))
				lock (LockObj)
				{
					if (drop) QuickPayOptions.Remove(item.Id);
					else QuickPayOptions[item.Id] = item;
				}
			else if (item.Id != default(long) && !drop) QuickPayOptions.Add(item.Id, item);
		}

		public static void UpdateCache(PackageType item, bool drop = false)
		{
			if (PackageTypes.ContainsKey(item.Id))
				lock (LockObj)
				{
					if (drop) PackageTypes.Remove(item.Id);
					else
					{
						PackageTypes[item.Id] = item;

						// updates
						foreach (var sr in CustomerRatings.Values.SelectMany(rating => rating.LTLSellRates.Where(sr => sr.VendorRating.LTLPackageSpecificRatePackageTypeId == item.Id)))
						{
							sr.VendorRating.LTLPackageSpecificRatePackageType = item;
						}
					}
				}
			else if (item.Id != default(long) && !drop) PackageTypes.Add(item.Id, item);
		}

		public static void UpdateCache(PermissionDetail item, bool drop = false)
		{
			if (PermissionDetails.ContainsKey(item.Id))
				lock (LockObj)
				{
					if (drop) PermissionDetails.Remove(item.Id);
					else PermissionDetails[item.Id] = item;
				}
			else if (item.Id != default(long) && !drop) PermissionDetails.Add(item.Id, item);
		}

		public static void UpdateCache(Prefix item, bool drop = false)
		{
			if (Prefixes.ContainsKey(item.Id))
				lock (LockObj)
				{
					if (drop) Prefixes.Remove(item.Id);
					else Prefixes[item.Id] = item;
				}
			else if (item.Id != default(long) && !drop) Prefixes.Add(item.Id, item);
		}

		public static void UpdateCache(Service item, bool drop = false)
		{
			if (Services.ContainsKey(item.Id))
				lock (LockObj)
				{
					if (drop) Services.Remove(item.Id);
					else
					{
						Services[item.Id] = item;

						// updates
						foreach (var rating in CustomerRatings.Values)
						{
							foreach (var serv in rating.CustomerServiceMarkups.Where(spr => spr.ServiceId == item.Id))
								serv.Service = item;

							foreach (var acc in rating.LTLSellRates.SelectMany(sr => sr.VendorRating.LTLAccessorials.Where(t => t.ServiceId == item.Id)))
								acc.Service = item;
						}
					}
				}
			else if (item.Id != default(long) && !drop) Services.Add(item.Id, item);
		}

        public static void UpdateCache(P44ChargeCodeMapping item, bool drop = false)
        {
            if (P44ChargeCodeMappings.ContainsKey(item.Id))
                lock (LockObj)
                {
                    if (drop) P44ChargeCodeMappings.Remove(item.Id);
                    else
                    {
                        P44ChargeCodeMappings[item.Id] = item;
                    }
                }
            else if (item.Id != default(long) && !drop) P44ChargeCodeMappings.Add(item.Id, item);
        }

        public static void UpdateCache(P44ServiceMapping item, bool drop = false)
        {
            if (P44ServiceMappings.ContainsKey(item.Id))
                lock (LockObj)
                {
                    if (drop) P44ServiceMappings.Remove(item.Id);
                    else
                    {
                        P44ServiceMappings[item.Id] = item;
                    }
                }
            else if (item.Id != default(long) && !drop) P44ServiceMappings.Add(item.Id, item);
        }

        public static void UpdateCache(ShipmentPriority item, bool drop = false)
		{
			if (ShipmentPriorities.ContainsKey(item.Id))
				lock (LockObj)
				{
					if (drop) ShipmentPriorities.Remove(item.Id);
					else ShipmentPriorities[item.Id] = item;
				}
			else if (item.Id != default(long) && !drop) ShipmentPriorities.Add(item.Id, item);
		}

		public static void UpdateCache(SmallPackagingMap item, bool drop = false)
		{
			if (SmallPackagingMaps.ContainsKey(item.Id))
				lock (LockObj)
				{
					if (drop) SmallPackagingMaps.Remove(item.Id);
					else SmallPackagingMaps[item.Id] = item;
				}
			else if (item.Id != default(long) && !drop) SmallPackagingMaps.Add(item.Id, item);
		}

		public static void UpdateCache(SmallPackageServiceMap item, bool drop = false)
		{
			if (SmallPackageServiceMaps.ContainsKey(item.Id))
				lock (LockObj)
				{
					if (drop) SmallPackageServiceMaps.Remove(item.Id);
					else SmallPackageServiceMaps[item.Id] = item;
				}
			else if (item.Id != default(long) && !drop) SmallPackageServiceMaps.Add(item.Id, item);
		}


		// loaded as used (A-Z)
		public static void UpdateCache(CustomerRating item, bool drop = false)
		{
			if (CustomerRatings.ContainsKey(item.Id))
				lock (LockObj)
				{
					if (drop) CustomerRatings.Remove(item.Id);
					else CustomerRatings[item.Id] = item;
				}
			else if (item.Id != default(long) && !drop) CustomerRatings.Add(item.Id, item);
		}
	}
}
