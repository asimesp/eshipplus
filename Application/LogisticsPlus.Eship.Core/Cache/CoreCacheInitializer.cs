﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Core.Cache
{
	internal class CoreCacheInitializer : EntityBase
	{
		public List<AccountBucket> FetchAccountBuckets()
		{
			var objs = new List<AccountBucket>();
			const string query = @"SELECT * FROM AccountBucket";
			using (var reader = GetReader(query, null))
				while (reader.Read())
					objs.Add(new AccountBucket(reader));
			Connection.Close();
			return objs;
		}

		public List<AccountBucketUnit> FetchAccountBucketUnits()
		{
			var objs = new List<AccountBucketUnit>();
			const string query = @"SELECT * FROM AccountBucketUnit";
			using (var reader = GetReader(query, null))
				while (reader.Read())
					objs.Add(new AccountBucketUnit(reader));
			Connection.Close();
			return objs;
		}

		public List<Asset> FetchAssets()
		{
			var objs = new List<Asset>();
			const string query = @"SELECT * FROM Asset";
			using (var reader = GetReader(query, null))
				while (reader.Read())
					objs.Add(new Asset(reader));
			Connection.Close();
			return objs;
		}

		public List<ChargeCode> FetchChargeCodes()
		{
			var objs = new List<ChargeCode>();
			const string query = @"SELECT * FROM ChargeCode";
			using (var reader = GetReader(query, null))
				while (reader.Read())
					objs.Add(new ChargeCode(reader));
			Connection.Close();
			return objs;
		}

		public List<ContactType> FetchContactTypes()
		{
			var objs = new List<ContactType>();
			const string query = @"SELECT * FROM ContactType";
			using (var reader = GetReader(query, null))
				while (reader.Read())
					objs.Add(new ContactType(reader));
			Connection.Close();
			return objs;
		}

		public List<Country> FetchCountries()
		{
			var objs = new List<Country>();
            const string query = @"SELECT * FROM Country order by [Name] asc";
			using (var reader = GetReader(query, null))
				while (reader.Read())
					objs.Add(new Country(reader));
			Connection.Close();
			return objs;
		}

		public List<DocumentTag> FetchDocumentTags()
		{
			var objs = new List<DocumentTag>();
			const string query = @"SELECT * FROM DocumentTag";
			using (var reader = GetReader(query, null))
				while (reader.Read())
					objs.Add(new DocumentTag(reader));
			Connection.Close();
			return objs;
		}

		public List<EquipmentType> FetchEquipmentTypes()
		{
			var objs = new List<EquipmentType>();
			const string query = @"SELECT * FROM EquipmentType";
			using (var reader = GetReader(query, null))
				while (reader.Read())
					objs.Add(new EquipmentType(reader));
			Connection.Close();
			return objs;
		}

		public List<FailureCode> FetchFailureCodes()
		{
			var objs = new List<FailureCode>();
			const string query = @"SELECT * FROM FailureCode";
			using (var reader = GetReader(query, null))
				while (reader.Read())
					objs.Add(new FailureCode(reader));
			Connection.Close();
			return objs;
		}

		public List<InsuranceType> FetchInsuranceTypes()
		{
			var objs = new List<InsuranceType>();
			const string query = @"SELECT * FROM InsuranceType";
			using (var reader = GetReader(query, null))
				while (reader.Read())
					objs.Add(new InsuranceType(reader));
			Connection.Close();
			return objs;
		}

		public List<MileageSource> FetchMileageSources()
		{
			var objs = new List<MileageSource>();
            const string query = @"SELECT * FROM MileageSource Order by Code ASC";
			using (var reader = GetReader(query, null))
				while (reader.Read())
					objs.Add(new MileageSource(reader));
			Connection.Close();
			return objs;
		}

		public List<QuickPayOption> FetchQuickPayOptions()
		{
			var objs = new List<QuickPayOption>();
			const string query = @"SELECT * FROM QuickPayOption";
			using (var reader = GetReader(query, null))
				while (reader.Read())
					objs.Add(new QuickPayOption(reader));
			Connection.Close();
			return objs;
		}

		public List<PackageType> FetchPackageTypes()
		{
			var objs = new List<PackageType>();
			const string query = @"SELECT * FROM PackageType";
			using (var reader = GetReader(query, null))
				while (reader.Read())
					objs.Add(new PackageType(reader));
			Connection.Close();
			return objs;
		}

		public List<PermissionDetail> FetchPermissionDetails()
		{
			var objs = new List<PermissionDetail>();
			const string query = @"SELECT * FROM PermissionDetail";
			using (var reader = GetReader(query, null))
				while (reader.Read())
					objs.Add(new PermissionDetail(reader));
			Connection.Close();
			return objs;
		}

		public List<Prefix> FetchPrefixes()
		{
			var objs = new List<Prefix>();
			const string query = @"SELECT * FROM Prefix";
			using (var reader = GetReader(query, null))
				while (reader.Read())
					objs.Add(new Prefix(reader));
			Connection.Close();
			return objs;
		}

		public List<P44ChargeCodeMapping> FetchP44ChargeCodeMappings()
		{
			var objs = new List<P44ChargeCodeMapping>();
            const string query = @"SELECT * FROM [P44ChargeCodeMapping] ORDER BY Id ASC";
			using (var reader = GetReader(query, null))
				while (reader.Read())
					objs.Add(new P44ChargeCodeMapping(reader));
			Connection.Close();
			return objs;
		}

        public List<P44ServiceMapping> FetchP44ServiceMappings()
        {
            var objs = new List<P44ServiceMapping>();
            const string query = @"SELECT * FROM [P44ServiceMapping] ORDER BY Id ASC";
            using (var reader = GetReader(query, null))
                while (reader.Read())
                    objs.Add(new P44ServiceMapping(reader));
            Connection.Close();
            return objs;
        }

        public List<Service> FetchServices()
        {
            var objs = new List<Service>();
            const string query = @"SELECT * FROM [Service] ORDER BY Code ASC";
            using (var reader = GetReader(query, null))
                while (reader.Read())
                    objs.Add(new Service(reader));
            Connection.Close();
            return objs;
        }

        public List<ShipmentPriority> FetchShipmentPrioritys()
		{
			var objs = new List<ShipmentPriority>();
			const string query = @"SELECT * FROM ShipmentPriority";
			using (var reader = GetReader(query, null))
				while (reader.Read())
					objs.Add(new ShipmentPriority(reader));
			Connection.Close();
			return objs;
		}

		public List<SmallPackagingMap> FetchSmallPackagingMaps()
		{
			var objs = new List<SmallPackagingMap>();
            const string query = @"SELECT * FROM SmallPackagingMap ORDER BY SmallPackageEngine ASC, SmallPackEngineType ASC";
			using (var reader = GetReader(query, null))
				while (reader.Read())
					objs.Add(new SmallPackagingMap(reader));
			Connection.Close();
			return objs;
		}

		public List<SmallPackageServiceMap> FetchSmallPackageServiceMaps()
		{
			var objs = new List<SmallPackageServiceMap>();
            const string query = @"SELECT * FROM SmallPackageServiceMap ORDER BY SmallPackageEngine ASC, SmallPackEngineService ASC";
			using (var reader = GetReader(query, null))
				while (reader.Read())
					objs.Add(new SmallPackageServiceMap(reader));
			Connection.Close();
			return objs;
		}
	}
}
