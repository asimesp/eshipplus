﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;

namespace LogisticsPlus.Eship.Core
{
    public static class EntityConversionUtility
    {
        public static Claim Duplicate(this Claim claim)
        {
            //Claim
            var newClaim = new Claim
            {
                ClaimNumber = string.Empty,
                ClaimantReferenceNumber = claim.ClaimantReferenceNumber,
                ClaimType = claim.ClaimType,
                AmountClaimed = claim.AmountClaimed,
                AmountClaimedType = claim.AmountClaimedType,
                EstimatedRepairCost = claim.EstimatedRepairCost,
                IsRepairable = claim.IsRepairable,
                ClaimDetail = claim.ClaimDetail,
                Customer = claim.Customer,
                DateCreated = DateTime.Now,
                Status = claim.Status,
                TenantId = claim.TenantId,
                Vendors = new List<ClaimVendor>(),
                Documents = new List<ClaimDocument>(),
                Notes = new List<ClaimNote>(),
                Shipments = new List<ClaimShipment>(),
                ServiceTickets = new List<ClaimServiceTicket>(),
                ClaimDate = DateTime.Now,
                DateLpAcctToPayCarrier = claim.DateLpAcctToPayCarrier,
                Acknowledged = claim.Acknowledged,
                CheckNumber = claim.CheckNumber,
                CheckAmount = claim.CheckAmount
            };

            foreach (var vendor in claim.Vendors)
                newClaim.Vendors.Add(new ClaimVendor
                {
                    Claim = newClaim,
                    VendorId = vendor.VendorId,
                    TenantId = vendor.TenantId,
                    ProNumber = vendor.ProNumber,
                    FreightBillNumber = vendor.FreightBillNumber
                }
            );
            //should note user be user?
            foreach (var note in claim.Notes)
                newClaim.Notes.Add(new ClaimNote
                {
                    Claim = newClaim,
                    Archived = note.Archived,
                    Type = note.Type,
                    Classified = note.Classified,
                    Message = note.Message,
                    User = note.User,
                    TenantId = note.TenantId,
                    SendReminder = note.SendReminder,
                    SendReminderOn = note.SendReminderOn,
                    SendReminderEmails = note.SendReminderEmails,
                    DateCreated = DateTime.Now
                }
            );
            foreach (var shipment in claim.Shipments)
                newClaim.Shipments.Add(new ClaimShipment
                {
                    Claim = newClaim,
                    ShipmentId = shipment.ShipmentId,
                    TenantId = shipment.TenantId
                }
            );

            return newClaim;
        }

        public static ReportConfiguration Duplicate(this ReportConfiguration configuration)
        {
            var c = new ReportConfiguration
                        {
                            Name = configuration.Name,
                            Description = configuration.Description,
                            Visibility = configuration.Visibility,
                            SerializedCustomization = configuration.SerializedCustomization,
                            ReportTemplateId = configuration.ReportTemplateId,
                            TenantId = configuration.TenantId,
                            CustomerGroupId = configuration.CustomerGroupId,
                            VendorGroupId = configuration.VendorGroupId,
                            ReadonlyCustomerGroupFilter = configuration.ReadonlyCustomerGroupFilter,
                            ReadonlyVendorGroupFilter = configuration.ReadonlyVendorGroupFilter,
                            SelectUsers = new List<ReportCustomizationUser>(),
                            SelectGroups = new List<ReportCustomizationGroup>(),
                QlikConfiguration = configuration.QlikConfiguration
                        };
            c.SelectUsers.AddRange(configuration.SelectUsers.Select(u => new ReportCustomizationUser
                                                                            {
                                                                                TenantId = u.TenantId,
                                                                                ReportConfiguration = c,
                                                                                UserId = u.UserId
                                                                            }));
            c.SelectGroups.AddRange(configuration.SelectGroups.Select(g => new ReportCustomizationGroup
            {
                ReportConfiguration = c,
                GroupId = g.GroupId,
                TenantId = g.TenantId
            }));

            c.QlikSheets.AddRange(configuration.QlikSheets.Select(s => new QlikSheet
                {
                    DisplayOrder = s.DisplayOrder,
                    Link = s.Link,
                    Name = s.Name,
                    ReportConfiguration = c,
                    TenantId = configuration.TenantId,
                }));
            return c;
        }

        public static Shipment Duplicate(this Shipment shipment, bool roundTrip = false)
        {
			var newShipment = new Shipment
                                {
                                    ShipmentNumber = string.Empty,
                                    Customer = shipment.Customer,
									CareOfAddressFormatEnabled = shipment.CareOfAddressFormatEnabled,
                                    DeclineInsurance = shipment.DeclineInsurance,
                                    Prefix = shipment.Prefix,
                                    HidePrefix = shipment.HidePrefix,
                                    ServiceMode = shipment.ServiceMode,
                                    IsPartialTruckload = shipment.IsPartialTruckload,
                                    PurchaseOrderNumber = shipment.PurchaseOrderNumber,
                                    ShipperReference = shipment.ShipperReference,
                                    ShipmentPriority = shipment.ShipmentPriority,
                                    JobId = default(long),

                                    //dates
                                    DateCreated = DateTime.Now,
                                    DesiredPickupDate = DateTime.Now,
                                    EstimatedDeliveryDate = DateTime.Now,
                                    ActualPickupDate = DateUtility.SystemEarliestDateTime,
                                    ActualDeliveryDate = DateUtility.SystemEarliestDateTime,

                                    EarlyDelivery = shipment.EarlyDelivery,
                                    LateDelivery = shipment.LateDelivery,
                                    EarlyPickup = shipment.EarlyPickup,
                                    LatePickup = shipment.LatePickup,

                                    Mileage = shipment.Mileage,
                                    MileageSourceId = shipment.MileageSourceId,
                                    MiscField1 = shipment.MiscField1,
                                    MiscField2 = shipment.MiscField2,

									DriverName =  shipment.DriverName,
									DriverPhoneNumber = shipment.DriverPhoneNumber,
									DriverTrailerNumber = shipment.DriverTrailerNumber,

									CreatedInError = false,
                                    HazardousMaterial = shipment.HazardousMaterial,
                                    HazardousMaterialContactName = shipment.HazardousMaterialContactName,
                                    HazardousMaterialContactPhone = shipment.HazardousMaterialContactPhone,
                                    HazardousMaterialContactMobile = shipment.HazardousMaterialContactMobile,
                                    HazardousMaterialContactEmail = shipment.HazardousMaterialContactEmail,
                                    Status = ShipmentStatus.Scheduled,
                                    TenantId = shipment.TenantId,
                                    EnableQuickPay = shipment.EnableQuickPay,
                                    GeneralBolComments = shipment.GeneralBolComments,
                                    CriticalBolComments = shipment.CriticalBolComments,
                                    ShipperBol = string.Empty,
                                    Project44QuoteNumber = shipment.Project44QuoteNumber,
                                    IsExpeditedService = false,
                                    ExpeditedServiceTime = string.Empty,

                                    //Lists
                                    Services = new List<ShipmentService>(),
                                    Items = new List<ShipmentItem>(),
                                    Stops = new List<ShipmentLocation>(),
                                    Notes = new List<ShipmentNote>(),
                                    Equipments = new List<ShipmentEquipment>(),
                                    QuickPayOptions = new List<ShipmentQuickPayOption>(),
                                    CustomerReferences = new List<ShipmentReference>(),
                                    Charges = new List<ShipmentCharge>(),
                                    Documents = new List<ShipmentDocument>(),
                                    Vendors = new List<ShipmentVendor>(),
                                    Assets = new List<ShipmentAsset>(),
                                    AccountBuckets = new List<ShipmentAccountBucket>(),
                                };

            //Shipment Origin
            if (shipment.Origin != null)
            {
                newShipment.Origin = new ShipmentLocation
                                {
                                    Shipment = newShipment,
                                    Description = shipment.Origin.Description,
                                    Street1 = shipment.Origin.Street1,
                                    Street2 = shipment.Origin.Street2,
                                    City = shipment.Origin.City,
                                    State = shipment.Origin.State,
                                    PostalCode = shipment.Origin.PostalCode,
                                    CountryId = shipment.Origin.CountryId,
                                    TenantId = shipment.Origin.TenantId,
                                    StopOrder = shipment.Origin.StopOrder,
                                    Contacts = new List<ShipmentContact>(),
                                    SpecialInstructions = shipment.Origin.SpecialInstructions,
                                    Direction = shipment.Origin.Direction,
                                    GeneralInfo = shipment.Origin.GeneralInfo,
									AppointmentDateTime = DateUtility.SystemEarliestDateTime
                                };
                foreach (var contact in shipment.Origin.Contacts)
                    newShipment.Origin.Contacts.Add(new ShipmentContact
                                {
                                    Location = newShipment.Origin,
                                    Comment = contact.Comment,
                                    ContactTypeId = contact.ContactTypeId,
                                    Email = contact.Email,
                                    Fax = contact.Fax,
                                    Phone = contact.Phone,
                                    Mobile = contact.Mobile,
                                    Name = contact.Name,
                                    Primary = contact.Primary,
                                    TenantId = contact.TenantId
                                });
            }

            //Shipment Destination
            if (shipment.Destination != null)
            {
                newShipment.Destination = new ShipmentLocation
                                {
                                    Shipment = newShipment,
                                    Description = shipment.Destination.Description,
                                    Street1 = shipment.Destination.Street1,
                                    Street2 = shipment.Destination.Street2,
                                    City = shipment.Destination.City,
                                    State = shipment.Destination.State,
                                    PostalCode = shipment.Destination.PostalCode,
                                    CountryId = shipment.Destination.CountryId,
                                    TenantId = shipment.Destination.TenantId,
                                    Contacts = new List<ShipmentContact>(),
                                    StopOrder = shipment.Destination.StopOrder,
                                    SpecialInstructions = shipment.Destination.SpecialInstructions,
                                    Direction = shipment.Destination.Direction,
                                    GeneralInfo = shipment.Destination.GeneralInfo,
									AppointmentDateTime = DateUtility.SystemEarliestDateTime
                                };
                foreach (var contact in shipment.Destination.Contacts)
                    newShipment.Destination.Contacts.Add(new ShipmentContact
                                {
                                    Location = newShipment.Destination,
                                    Comment = contact.Comment,
                                    ContactTypeId = contact.ContactTypeId,
                                    Email = contact.Email,
                                    Fax = contact.Fax,
                                    Phone = contact.Phone,
                                    Mobile = contact.Mobile,
                                    Name = contact.Name,
                                    Primary = contact.Primary,
                                    TenantId = contact.TenantId
                                });
            }

            //Shipment Stops
            foreach (var stop in shipment.Stops)
            {
            	var location = new ShipmentLocation
            	               	{
            	               		Shipment = newShipment,
            	               		Description = stop.Description,
            	               		Street1 = stop.Street1,
            	               		Street2 = stop.Street2,
            	               		City = stop.City,
            	               		State = stop.State,
            	               		PostalCode = stop.PostalCode,
            	               		CountryId = stop.CountryId,
            	               		Contacts = new List<ShipmentContact>(),
            	               		StopOrder = stop.StopOrder,
            	               		SpecialInstructions = stop.SpecialInstructions,
            	               		GeneralInfo = stop.GeneralInfo,
            	               		Direction = stop.Direction,
            	               		TenantId = stop.TenantId,
            	               		AppointmentDateTime = DateUtility.SystemEarliestDateTime
            	               	};
				foreach (var contact in stop.Contacts)
					location.Contacts.Add(new ShipmentContact
					                      	{
					                      		Location = location,
					                      		Comment = contact.Comment,
					                      		ContactTypeId = contact.ContactTypeId,
					                      		Email = contact.Email,
					                      		Fax = contact.Fax,
					                      		Phone = contact.Phone,
					                      		Mobile = contact.Mobile,
					                      		Name = contact.Name,
					                      		Primary = contact.Primary,
					                      		TenantId = contact.TenantId
					                      	});
                newShipment.Stops.Add(location);
            }

			

            //Shipment Items
			foreach (var item in shipment.Items)
			{
				var shipmentItem = new ShipmentItem
				                   	{
				                   		Shipment = newShipment,
				                   		Pickup = item.Pickup,
				                   		Delivery = item.Delivery,
				                   		ActualWeight = item.ActualWeight,
				                   		ActualLength = item.ActualLength,
				                   		ActualWidth = item.ActualWidth,
				                   		ActualHeight = item.ActualHeight,
				                   		Quantity = item.Quantity,
				                   		PieceCount = item.PieceCount,
				                   		Description = item.Description,
				                   		Comment = item.Comment,
				                   		ActualFreightClass = item.ActualFreightClass,
				                   		RatedFreightClass = item.ActualFreightClass,
				                   		PackageTypeId = item.PackageTypeId,
				                   		NMFCCode = item.NMFCCode,
				                   		HTSCode = item.HTSCode,
				                   		TenantId = item.TenantId,
				                   		Value = item.Value,
				                   		IsStackable = item.IsStackable,
                                        HazardousMaterial = item.HazardousMaterial
				                   	};
				newShipment.Items.Add(shipmentItem);
			}

			if (roundTrip)
			{
				// reverse origin destination
				var origin = newShipment.Origin;
				newShipment.Origin = newShipment.Destination;
				newShipment.Destination = origin;

				// reverse stops
				if (shipment.Stops.Count > 0)
				{
					var i = newShipment.Stops.Max(s => s.StopOrder);
					foreach (var stop in newShipment.Stops)
					stop.StopOrder = i--;
				}
				
				
				// reverse item pickups/deliveries
				foreach (var item in newShipment.Items)
				{
					var delivery = item.Delivery;
					item.Delivery = item.Pickup;
					item.Pickup = delivery;
				}
			}

        	//Shipment Services
            foreach (var service in shipment.Services)
                newShipment.Services.Add(new ShipmentService
                                            {
                                                Shipment = newShipment,
                                                ServiceId = service.ServiceId,
                                                TenantId = service.TenantId
                                            }
                    );

            //Shipment Equipment
            foreach (var equipment in shipment.Equipments)
                newShipment.Equipments.Add(new ShipmentEquipment
                                            {
                                                Shipment = newShipment,
                                                EquipmentTypeId = equipment.EquipmentTypeId,
                                                TenantId = equipment.TenantId
                                            }
                    );

            //Shipment References
            foreach (var reference in shipment.CustomerReferences)
                newShipment.CustomerReferences.Add(new ShipmentReference
                {
                    Shipment = newShipment,
                    DisplayOnOrigin = reference.DisplayOnOrigin,
                    DisplayOnDestination = reference.DisplayOnDestination,
                    Required = reference.Required,
                    Value = string.Empty,
                    Name = reference.Name,
                    TenantId = reference.TenantId
                }
                    );

            //Shipment Vendors
            foreach (var vendor in shipment.Vendors)
                newShipment.Vendors.Add(new ShipmentVendor
                {
                    Shipment = newShipment,
                    TenantId = vendor.TenantId,
                    Primary = vendor.Primary,
                    Vendor = vendor.Vendor,
                    FailureCode = null,
                    ProNumber = string.Empty,
                    FailureComments = string.Empty
                }
                    );

            //Shipment Quick Pay Options
            foreach (var quickPayOption in shipment.QuickPayOptions)
                newShipment.QuickPayOptions.Add(new ShipmentQuickPayOption
                {
                    TenantId = shipment.TenantId,
                    Shipment = newShipment,
                    Description = quickPayOption.Description,
                    DiscountPercent = quickPayOption.DiscountPercent
                });

            //Shipment Charges
            foreach (var charge in shipment.Charges)
                newShipment.Charges.Add(new ShipmentCharge
                {
                    TenantId = shipment.TenantId,
                    Shipment = newShipment,
                    ChargeCode = charge.ChargeCode,
                    Quantity = charge.Quantity,
                    UnitBuy = charge.UnitBuy,
                    UnitSell = charge.UnitSell,
                    UnitDiscount = charge.UnitDiscount,
                    Comment = charge.Comment,
                    VendorId = charge.VendorId
                });

            //Shipment AccountBuckets
            foreach (var accountBucket in shipment.AccountBuckets)
                newShipment.AccountBuckets.Add(new ShipmentAccountBucket
                    {
                        Shipment = newShipment,
                        TenantId = shipment.TenantId,
                        Primary = accountBucket.Primary,
                        AccountBucket = accountBucket.AccountBucket,
                    });

            // Shipment assets
            foreach (var asset in shipment.Assets)
                newShipment.Assets.Add(new ShipmentAsset
                {
                    Shipment = newShipment,
                    TenantId = shipment.TenantId,
                    DriverAssetId = asset.DriverAssetId,
                    TractorAssetId = asset.TractorAssetId,
                    TrailerAssetId = asset.TrailerAssetId,
                    MilesRun = asset.MilesRun,
                    Primary = asset.Primary
                });

            return newShipment;
        }

        public static LoadOrder Duplicate(this LoadOrder loadOrder, bool roundTrip = false)
        {
            var newLoadOrder = new LoadOrder
            {
                LoadOrderNumber = string.Empty,
				Description =  loadOrder.Description,
                Customer = loadOrder.Customer,
                DeclineInsurance = loadOrder.DeclineInsurance,
                Prefix = loadOrder.Prefix,
                HidePrefix = loadOrder.HidePrefix,
                ServiceMode = loadOrder.ServiceMode,
                IsPartialTruckload = loadOrder.IsPartialTruckload,
                PurchaseOrderNumber = loadOrder.PurchaseOrderNumber,
                ShipperReference = loadOrder.ShipperReference,
                ShipmentPriority = loadOrder.ShipmentPriority,
                JobId = default(long),

                //dates
                DateCreated = DateTime.Now,
                DesiredPickupDate = DateTime.Now,
                EstimatedDeliveryDate = DateTime.Now,

                EarlyDelivery = loadOrder.EarlyDelivery,
                LateDelivery = loadOrder.LateDelivery,
                EarlyPickup = loadOrder.EarlyPickup,
                LatePickup = loadOrder.LatePickup,

                Mileage = loadOrder.Mileage,
                MileageSourceId = loadOrder.MileageSourceId,
                MiscField1 = loadOrder.MiscField1,
                MiscField2 = loadOrder.MiscField2,

				DriverName = loadOrder.DriverName,
				DriverPhoneNumber = loadOrder.DriverPhoneNumber,
				DriverTrailerNumber = loadOrder.DriverTrailerNumber,

                HazardousMaterial = loadOrder.HazardousMaterial,
                HazardousMaterialContactName = loadOrder.HazardousMaterialContactName,
                HazardousMaterialContactPhone = loadOrder.HazardousMaterialContactPhone,
                HazardousMaterialContactMobile = loadOrder.HazardousMaterialContactMobile,
                HazardousMaterialContactEmail = loadOrder.HazardousMaterialContactEmail,
                Status = LoadOrderStatus.Offered,
                TenantId = loadOrder.TenantId,
                GeneralBolComments = loadOrder.GeneralBolComments,
                CriticalBolComments = loadOrder.CriticalBolComments,
                ShipperBol = string.Empty,

                //Lists
                Services = new List<LoadOrderService>(),
                Items = new List<LoadOrderItem>(),
                Stops = new List<LoadOrderLocation>(),
                Notes = new List<LoadOrderNote>(),
                Equipments = new List<LoadOrderEquipment>(),
                CustomerReferences = new List<LoadOrderReference>(),
                Charges = new List<LoadOrderCharge>(),
                Documents = new List<LoadOrderDocument>(),
                Vendors = new List<LoadOrderVendor>(),
                AccountBuckets = new List<LoadOrderAccountBucket>(),
            };

            // Origin
            if (loadOrder.Origin != null)
            {
                newLoadOrder.Origin = new LoadOrderLocation
                {
                    LoadOrder = newLoadOrder,
                    Description = loadOrder.Origin.Description,
                    Street1 = loadOrder.Origin.Street1,
                    Street2 = loadOrder.Origin.Street2,
                    City = loadOrder.Origin.City,
                    State = loadOrder.Origin.State,
                    PostalCode = loadOrder.Origin.PostalCode,
                    CountryId = loadOrder.Origin.CountryId,
                    TenantId = loadOrder.Origin.TenantId,
                    StopOrder = loadOrder.Origin.StopOrder,
                    Contacts = new List<LoadOrderContact>(),
                    SpecialInstructions = loadOrder.Origin.SpecialInstructions,
                    Direction = loadOrder.Origin.Direction,
                    GeneralInfo = loadOrder.Origin.GeneralInfo,
                    AppointmentDateTime = loadOrder.Origin.AppointmentDateTime
                };
                foreach (var contact in loadOrder.Origin.Contacts)
                    newLoadOrder.Origin.Contacts.Add(new LoadOrderContact
                    {
                        Location = newLoadOrder.Origin,
                        Comment = contact.Comment,
                        ContactTypeId = contact.ContactTypeId,
                        Email = contact.Email,
                        Fax = contact.Fax,
                        Phone = contact.Phone,
                        Mobile = contact.Mobile,
                        Name = contact.Name,
                        Primary = contact.Primary,
                        TenantId = contact.TenantId
                    });
            }

            // Destination
            if (loadOrder.Destination != null)
            {
                newLoadOrder.Destination = new LoadOrderLocation
                {
                    LoadOrder = newLoadOrder,
                    Description = loadOrder.Destination.Description,
                    Street1 = loadOrder.Destination.Street1,
                    Street2 = loadOrder.Destination.Street2,
                    City = loadOrder.Destination.City,
                    State = loadOrder.Destination.State,
                    PostalCode = loadOrder.Destination.PostalCode,
                    CountryId = loadOrder.Destination.CountryId,
                    TenantId = loadOrder.Destination.TenantId,
                    Contacts = new List<LoadOrderContact>(),
                    StopOrder = loadOrder.Destination.StopOrder,
                    SpecialInstructions = loadOrder.Destination.SpecialInstructions,
                    Direction = loadOrder.Destination.Direction,
                    GeneralInfo = loadOrder.Destination.GeneralInfo,
                    AppointmentDateTime = loadOrder.Destination.AppointmentDateTime
                };
                foreach (var contact in loadOrder.Destination.Contacts)
                    newLoadOrder.Destination.Contacts.Add(new LoadOrderContact
                    {
                        Location = newLoadOrder.Destination,
                        Comment = contact.Comment,
                        ContactTypeId = contact.ContactTypeId,
                        Email = contact.Email,
                        Fax = contact.Fax,
                        Phone = contact.Phone,
                        Mobile = contact.Mobile,
                        Name = contact.Name,
                        Primary = contact.Primary,
                        TenantId = contact.TenantId
                    });
            }

            // Stops
            foreach (var stop in loadOrder.Stops)
            {
            	var location = new LoadOrderLocation
            	               	{
            	               		LoadOrder = newLoadOrder,
            	               		Description = stop.Description,
            	               		Street1 = stop.Street1,
            	               		Street2 = stop.Street2,
            	               		City = stop.City,
            	               		State = stop.State,
            	               		PostalCode = stop.PostalCode,
            	               		CountryId = stop.CountryId,
            	               		Contacts = new List<LoadOrderContact>(),
            	               		StopOrder = stop.StopOrder,
            	               		SpecialInstructions = stop.SpecialInstructions,
            	               		GeneralInfo = stop.GeneralInfo,
            	               		Direction = stop.Direction,
            	               		TenantId = stop.TenantId,
            	               		AppointmentDateTime = stop.AppointmentDateTime
            	               	};
				foreach (var contact in stop.Contacts)
					location.Contacts.Add(new LoadOrderContact
					                      	{
					                      		Location = location,
					                      		Comment = contact.Comment,
					                      		ContactTypeId = contact.ContactTypeId,
					                      		Email = contact.Email,
					                      		Fax = contact.Fax,
					                      		Phone = contact.Phone,
					                      		Mobile = contact.Mobile,
					                      		Name = contact.Name,
					                      		Primary = contact.Primary,
					                      		TenantId = contact.TenantId
					                      	});
                newLoadOrder.Stops.Add(location);
            }

            // Items
            foreach (var item in loadOrder.Items)
            {
                var shipmentItem = new LoadOrderItem
                {
                    LoadOrder = newLoadOrder,
                    Pickup = item.Pickup,
                    Delivery = item.Delivery,
                    Weight = item.Weight,
                    Length = item.Length,
                    Width = item.Width,
                    Height = item.Height,
                    Quantity = item.Quantity,
                    PieceCount = item.PieceCount,
                    Description = item.Description,
                    Comment = item.Comment,
                    FreightClass = item.FreightClass,
                    PackageTypeId = item.PackageTypeId,
                    NMFCCode = item.NMFCCode,
                    HTSCode = item.HTSCode,
                    TenantId = item.TenantId,
                    Value = item.Value,
                    IsStackable = item.IsStackable,
                    HazardousMaterial = item.HazardousMaterial,
                };
                newLoadOrder.Items.Add(shipmentItem);
            }

			if (roundTrip)
			{
				// reverse origin destination
				var origin = newLoadOrder.Origin;
				newLoadOrder.Origin = newLoadOrder.Destination;
				newLoadOrder.Destination = origin;

				// reverse stops
				if (loadOrder.Stops.Count > 0)
				{
					var i = newLoadOrder.Stops.Max(s => s.StopOrder);
					foreach (var stop in newLoadOrder.Stops)
						stop.StopOrder = i--;
				}


				// reverse item pickups/deliveries
				foreach (var item in newLoadOrder.Items)
				{
					var delivery = item.Delivery;
					item.Delivery = item.Pickup;
					item.Pickup = delivery;
				}
			}

            // Services
            foreach (var service in loadOrder.Services)
                newLoadOrder.Services.Add(new LoadOrderService
                {
                    LoadOrder = newLoadOrder,
                    ServiceId = service.ServiceId,
                    TenantId = service.TenantId
                }
                    );

            // Equipment
            foreach (var equipment in loadOrder.Equipments)
                newLoadOrder.Equipments.Add(new LoadOrderEquipment
                {
                    LoadOrder = newLoadOrder,
                    EquipmentTypeId = equipment.EquipmentTypeId,
                    TenantId = equipment.TenantId
                }
                    );

            // References
            foreach (var reference in loadOrder.CustomerReferences)
                newLoadOrder.CustomerReferences.Add(new LoadOrderReference
                {
                    LoadOrder = newLoadOrder,
                    DisplayOnOrigin = reference.DisplayOnOrigin,
                    DisplayOnDestination = reference.DisplayOnDestination,
                    Required = reference.Required,
                    Value = string.Empty,
                    Name = reference.Name,
                    TenantId = reference.TenantId
                }
                    );

            // Vendors
			foreach (var vendor in loadOrder.Vendors)
				newLoadOrder.Vendors.Add(new LoadOrderVendor
				                         	{
				                         		LoadOrder = newLoadOrder,
				                         		TenantId = vendor.TenantId,
				                         		Primary = vendor.Primary,
				                         		Vendor = vendor.Vendor,
				                         		ProNumber = string.Empty,
				                         	}
					);

            // Charges
            foreach (var charge in loadOrder.Charges)
                newLoadOrder.Charges.Add(new LoadOrderCharge
                {
                    TenantId = loadOrder.TenantId,
                    LoadOrder = newLoadOrder,
                    ChargeCode = charge.ChargeCode,
                    Quantity = charge.Quantity,
                    UnitBuy = charge.UnitBuy,
                    UnitSell = charge.UnitSell,
                    UnitDiscount = charge.UnitDiscount,
                    Comment = charge.Comment,
                    VendorId = charge.VendorId
                });

            // AccountBuckets
            foreach (var accountBucket in loadOrder.AccountBuckets)
                newLoadOrder.AccountBuckets.Add(new LoadOrderAccountBucket
                {
                    LoadOrder = newLoadOrder,
                    TenantId = loadOrder.TenantId,
                    Primary = accountBucket.Primary,
                    AccountBucket = accountBucket.AccountBucket,
                });

            return newLoadOrder;
        }

        public static ServiceTicket Duplicate(this ServiceTicket serviceTicket)
        {
            var newServiceTicket = new ServiceTicket
            {
                ServiceTicketNumber = string.Empty,
                Customer = serviceTicket.Customer,
                AccountBucket = serviceTicket.AccountBucket,
                Prefix = serviceTicket.Prefix,
                HidePrefix = serviceTicket.HidePrefix,
                DateCreated = DateTime.Now,
                TicketDate = DateUtility.SystemEarliestDateTime,
                Status = ServiceTicketStatus.Open,
                TenantId = serviceTicket.TenantId,

                JobId = default(long),
                ExternalReference1 = serviceTicket.ExternalReference1,
                ExternalReference2 = serviceTicket.ExternalReference2,

                Services = new List<ServiceTicketService>(),
                Items = new List<ServiceTicketItem>(),
                Equipments = new List<ServiceTicketEquipment>(),
                Charges = new List<ServiceTicketCharge>(),
                Vendors = new List<ServiceTicketVendor>(),
                Assets = new List<ServiceTicketAsset>(),
                Documents = new List<ServiceTicketDocument>(),
            };

            //Service Ticket Items
            foreach (var item in serviceTicket.Items)
                newServiceTicket.Items.Add(new ServiceTicketItem
                    {
                        ServiceTicket = newServiceTicket,
                        Description = item.Description,
                        Comment = item.Comment,
                        TenantId = item.TenantId,

                    }
            );

            //Service Ticket Services
            foreach (var service in serviceTicket.Services)
                newServiceTicket.Services.Add(new ServiceTicketService
                    {
                        ServiceTicket = newServiceTicket,
                        ServiceId = service.ServiceId,
                        TenantId = service.TenantId
                    }
            );

            //Service Ticket Equipment
            foreach (var equipment in serviceTicket.Equipments)
                newServiceTicket.Equipments.Add(new ServiceTicketEquipment
                    {
                        ServiceTicket = newServiceTicket,
                        EquipmentTypeId = equipment.EquipmentTypeId,
                        TenantId = equipment.TenantId
                    }
            );

            //Shipment Vendors
            foreach (var vendor in serviceTicket.Vendors)
                newServiceTicket.Vendors.Add(new ServiceTicketVendor
                    {
                        ServiceTicket = newServiceTicket,
                        TenantId = vendor.TenantId,
                        Primary = vendor.Primary,
                        Vendor = vendor.Vendor,
                    }
            );

            //Shipment Charges
            foreach (var charge in serviceTicket.Charges)
                newServiceTicket.Charges.Add(new ServiceTicketCharge
                    {
                        TenantId = serviceTicket.TenantId,
                        ServiceTicket = newServiceTicket,
                        ChargeCode = charge.ChargeCode,
                        Quantity = charge.Quantity,
                        UnitBuy = charge.UnitBuy,
                        UnitSell = charge.UnitSell,
                        UnitDiscount = charge.UnitDiscount,
                        Comment = charge.Comment,
                        VendorId = charge.VendorId
                    }
            );

            // Shipment assets
            foreach (var asset in serviceTicket.Assets)
                newServiceTicket.Assets.Add(new ServiceTicketAsset
                {
                    ServiceTicket = newServiceTicket,
                    TenantId = serviceTicket.TenantId,
                    DriverAssetId = asset.DriverAssetId,
                    TractorAssetId = asset.TractorAssetId,
                    TrailerAssetId = asset.TrailerAssetId,
                    MilesRun = asset.MilesRun,
                    Primary = asset.Primary
                });


            return newServiceTicket;
        }

        public static ReportSchedule Duplicate(this ReportSchedule reportSchedule)
        {
            var newReportSchedule = new ReportSchedule
                {
                    ReportConfigurationId = reportSchedule.ReportConfigurationId,
                    UserId = reportSchedule.UserId,
                    ScheduleInterval = reportSchedule.ScheduleInterval,
                    Start = reportSchedule.Start,
                    End = reportSchedule.End,
                    LastRun = DateUtility.SystemEarliestDateTime,
                    Enabled = reportSchedule.Enabled,
                    Time = reportSchedule.Time,
                    ExcludeSunday = reportSchedule.ExcludeSunday,
                    ExcludeMonday = reportSchedule.ExcludeMonday,
                    ExcludeTuesday = reportSchedule.ExcludeTuesday,
                    ExcludeWednesday = reportSchedule.ExcludeWednesday,
                    ExcludeThursday = reportSchedule.ExcludeThursday,
                    ExcludeFriday = reportSchedule.ExcludeFriday,
                    ExcludeSaturday = reportSchedule.ExcludeSaturday,
                    Notify = reportSchedule.Notify,
                    NotifyEmails = reportSchedule.NotifyEmails,
                    FtpEnabled = reportSchedule.FtpEnabled,
                    FtpUrl = reportSchedule.FtpUrl,
                    SecureFtp = reportSchedule.SecureFtp,
                    FtpUsername = reportSchedule.FtpUsername,
                    FtpPassword = reportSchedule.FtpPassword,
                    FtpDefaultFolder = reportSchedule.FtpDefaultFolder,
                    Extension = reportSchedule.Extension,
                    CustomerGroupId = reportSchedule.CustomerGroupId,
                    VendorGroupId = reportSchedule.VendorGroupId
                };

            return newReportSchedule;
        }

        public static Shipment ToShipment(this LoadOrder loadOrder)
        {
	        var customer = loadOrder.Customer ?? new Customer();
	        var shipment = new Shipment
            {
                ShipmentNumber = loadOrder.LoadOrderNumber,
                Customer = customer,
                CareOfAddressFormatEnabled = customer.CareOfAddressFormatEnabled,
                DeclineInsurance = loadOrder.DeclineInsurance,
                Prefix = loadOrder.Prefix,
                HidePrefix = loadOrder.HidePrefix,
                ServiceMode = loadOrder.ServiceMode,
                IsPartialTruckload = loadOrder.IsPartialTruckload,
                PurchaseOrderNumber = loadOrder.PurchaseOrderNumber,
                ShipperReference = loadOrder.ShipperReference,
                ShipmentPriority = loadOrder.ShipmentPriority,
                CarrierCoordinator = loadOrder.CarrierCoordinator,
                ShipmentCoordinator = loadOrder.LoadOrderCoordinator,
                SmallPackType =  string.Empty,
                VendorRatingOverrideAddress = string.Empty,
                GuaranteedDeliveryServiceTime = string.Empty,
                GuaranteedDeliveryServiceRateType = RateType.Flat,
		        GuaranteedDeliveryServiceRate = default(decimal),
                IsExpeditedService = false,
                ExpeditedServiceTime = string.Empty,
                ExpeditedServiceRate = default(decimal),

                //dates
                DateCreated = DateTime.Now,
                DesiredPickupDate = loadOrder.DesiredPickupDate,
                EstimatedDeliveryDate = loadOrder.EstimatedDeliveryDate,

                ActualPickupDate = DateUtility.SystemEarliestDateTime,
                ActualDeliveryDate = DateUtility.SystemEarliestDateTime,

                EarlyDelivery = loadOrder.EarlyDelivery,
                LateDelivery = loadOrder.LateDelivery,
                EarlyPickup = loadOrder.EarlyPickup,
                LatePickup = loadOrder.LatePickup,

                Mileage = loadOrder.Mileage,
				EmptyMileage = loadOrder.EmptyMileage,
                MileageSourceId = loadOrder.MileageSourceId,
                MiscField1 = loadOrder.MiscField1,
                MiscField2 = loadOrder.MiscField2,

				DriverName =  loadOrder.DriverName,
				DriverPhoneNumber = loadOrder.DriverPhoneNumber,
				DriverTrailerNumber = loadOrder.DriverTrailerNumber,
                
                CreatedInError = false,
                HazardousMaterial = loadOrder.HazardousMaterial,
                HazardousMaterialContactName = loadOrder.HazardousMaterialContactName,
                HazardousMaterialContactPhone = loadOrder.HazardousMaterialContactPhone,
                HazardousMaterialContactMobile = loadOrder.HazardousMaterialContactMobile,
                HazardousMaterialContactEmail = loadOrder.HazardousMaterialContactEmail,
                Status = ShipmentStatus.Scheduled,
                TenantId = loadOrder.TenantId,
                EnableQuickPay = false,
                GeneralBolComments = loadOrder.GeneralBolComments,
                CriticalBolComments = loadOrder.CriticalBolComments,
                ShipperBol = loadOrder.ShipperBol,
                Project44QuoteNumber = string.Empty,

				AccountBucketUnitId = loadOrder.AccountBucketUnitId,
				BillReseller = loadOrder.BillReseller,
				ResellerAdditionId = loadOrder.ResellerAdditionId,
				LinearFootRuleBypassed = loadOrder.LinearFootRuleBypassed,
				SalesRepresentativeId =  loadOrder.SalesRepresentativeId,
				SalesRepresentativeCommissionPercent = loadOrder.SalesRepresentativeCommissionPercent,
				SalesRepAddlEntityCommPercent = loadOrder.SalesRepAddlEntityCommPercent,
				SalesRepAddlEntityName = loadOrder.SalesRepAddlEntityName,
				SalesRepMaxCommValue = loadOrder.SalesRepMaxCommValue,
				SalesRepMinCommValue = loadOrder.SalesRepMinCommValue,
				

                //Lists
                Services = new List<ShipmentService>(),
                Items = new List<ShipmentItem>(),
                Stops = new List<ShipmentLocation>(),
                Notes = new List<ShipmentNote>(),
                Equipments = new List<ShipmentEquipment>(),
                QuickPayOptions = new List<ShipmentQuickPayOption>(),
                CustomerReferences = new List<ShipmentReference>(),
                Charges = new List<ShipmentCharge>(),
                Documents = new List<ShipmentDocument>(),
                Vendors = new List<ShipmentVendor>(),
                Assets = new List<ShipmentAsset>(),
                AccountBuckets = new List<ShipmentAccountBucket>(),
            };

            //Shipment Origin
            if (loadOrder.Origin != null)
            {
            	shipment.Origin = new ShipmentLocation
            	                  	{
            	                  		Shipment = shipment,
            	                  		Description = loadOrder.Origin.Description,
            	                  		Street1 = loadOrder.Origin.Street1,
            	                  		Street2 = loadOrder.Origin.Street2,
            	                  		City = loadOrder.Origin.City,
            	                  		State = loadOrder.Origin.State,
            	                  		PostalCode = loadOrder.Origin.PostalCode,
            	                  		CountryId = loadOrder.Origin.CountryId,
            	                  		TenantId = loadOrder.Origin.TenantId,
            	                  		StopOrder = loadOrder.Origin.StopOrder,
            	                  		Contacts = new List<ShipmentContact>(),
            	                  		SpecialInstructions = loadOrder.Origin.SpecialInstructions,
            	                  		Direction = loadOrder.Origin.Direction,
            	                  		GeneralInfo = loadOrder.Origin.GeneralInfo,
										AppointmentDateTime = loadOrder.Origin.AppointmentDateTime
            	                  	};
				foreach (var contact in loadOrder.Origin.Contacts)
					shipment.Origin.Contacts.Add(new ShipmentContact
					                             	{
					                             		Location = shipment.Origin,
					                             		Comment = contact.Comment,
					                             		ContactTypeId = contact.ContactTypeId,
					                             		Email = contact.Email,
					                             		Fax = contact.Fax,
					                             		Phone = contact.Phone,
					                             		Mobile = contact.Mobile,
					                             		Name = contact.Name,
					                             		Primary = contact.Primary,
					                             		TenantId = contact.TenantId
					                             	});
            }

            //Shipment Destination
            if (loadOrder.Destination != null)
            {
            	shipment.Destination = new ShipmentLocation
            	                       	{
            	                       		Shipment = shipment,
            	                       		Description = loadOrder.Destination.Description,
            	                       		Street1 = loadOrder.Destination.Street1,
            	                       		Street2 = loadOrder.Destination.Street2,
            	                       		City = loadOrder.Destination.City,
            	                       		State = loadOrder.Destination.State,
            	                       		PostalCode = loadOrder.Destination.PostalCode,
            	                       		CountryId = loadOrder.Destination.CountryId,
            	                       		TenantId = loadOrder.Destination.TenantId,
            	                       		Contacts = new List<ShipmentContact>(),
            	                       		StopOrder = loadOrder.Destination.StopOrder,
            	                       		SpecialInstructions = loadOrder.Destination.SpecialInstructions,
            	                       		Direction = loadOrder.Destination.Direction,
            	                       		GeneralInfo = loadOrder.Destination.GeneralInfo,
											AppointmentDateTime = loadOrder.Destination.AppointmentDateTime
            	                       	};
				foreach (var contact in loadOrder.Destination.Contacts)
					shipment.Destination.Contacts.Add(new ShipmentContact
					                                  	{
					                                  		Location = shipment.Destination,
					                                  		Comment = contact.Comment,
					                                  		ContactTypeId = contact.ContactTypeId,
					                                  		Email = contact.Email,
					                                  		Fax = contact.Fax,
					                                  		Phone = contact.Phone,
					                                  		Mobile = contact.Mobile,
					                                  		Name = contact.Name,
					                                  		Primary = contact.Primary,
					                                  		TenantId = contact.TenantId
					                                  	});
            }

            //Shipment Stops
            foreach (var stop in loadOrder.Stops)
            {
                var location = new ShipmentLocation
                {
                    Shipment = shipment,
                    Description = stop.Description,
                    Street1 = stop.Street1,
                    Street2 = stop.Street2,
                    City = stop.City,
                    State = stop.State,
                    PostalCode = stop.PostalCode,
                    CountryId = stop.CountryId,
                    Contacts = new List<ShipmentContact>(),
                    StopOrder = stop.StopOrder,
                    SpecialInstructions = stop.SpecialInstructions,
                    GeneralInfo = stop.GeneralInfo,
                    Direction = stop.Direction,
                    TenantId = stop.TenantId,
					AppointmentDateTime = stop.AppointmentDateTime
                };
                foreach (var contact in stop.Contacts)
                    location.Contacts.Add(new ShipmentContact
                    {
                        Location = location,
                        Comment = contact.Comment,
                        ContactTypeId = contact.ContactTypeId,
                        Email = contact.Email,
                        Fax = contact.Fax,
                        Phone = contact.Phone,
                        Mobile = contact.Mobile,
                        Name = contact.Name,
                        Primary = contact.Primary,
                        TenantId = contact.TenantId
                    });
                shipment.Stops.Add(location);
            }

            //Shipment Items
            foreach (var item in loadOrder.Items)
            {
                var shipmentItem = new ShipmentItem
                {
                    Shipment = shipment,
                    Pickup = item.Pickup,
                    Delivery = item.Delivery,
                    ActualWeight = item.Weight,
                    ActualLength = item.Length,
                    ActualWidth = item.Width,
                    ActualHeight = item.Height,
                    Quantity = item.Quantity,
                    PieceCount = item.PieceCount,
                    Description = item.Description,
                    Comment = item.Comment,
                    ActualFreightClass = item.FreightClass,
                    RatedFreightClass = item.FreightClass,
                    PackageTypeId = item.PackageTypeId,
                    NMFCCode = item.NMFCCode,
                    HTSCode = item.HTSCode,
                    TenantId = item.TenantId,
                    Value = item.Value,
                    IsStackable = item.IsStackable,
                    HazardousMaterial = item.HazardousMaterial,
                };
                shipment.Items.Add(shipmentItem);
            }

            //Shipment Services
			foreach (var service in loadOrder.Services)
				shipment.Services.Add(new ShipmentService
				                      	{
				                      		Shipment = shipment,
				                      		ServiceId = service.ServiceId,
				                      		TenantId = service.TenantId
				                      	}
					);

            //Shipment Equipment
			foreach (var equipment in loadOrder.Equipments)
				shipment.Equipments.Add(new ShipmentEquipment
				                        	{
				                        		Shipment = shipment,
				                        		EquipmentTypeId = equipment.EquipmentTypeId,
				                        		TenantId = equipment.TenantId
				                        	}
					);

            //Shipment References
			foreach (var reference in loadOrder.CustomerReferences)
				shipment.CustomerReferences.Add(new ShipmentReference
				                                	{
				                                		Shipment = shipment,
				                                		DisplayOnOrigin = reference.DisplayOnOrigin,
				                                		DisplayOnDestination = reference.DisplayOnDestination,
				                                		Required = reference.Required,
				                                		Value = reference.Value,
				                                		Name = reference.Name,
				                                		TenantId = reference.TenantId
				                                	}
					);

            //Shipment Vendors
			foreach (var vendor in loadOrder.Vendors)
				shipment.Vendors.Add(new ShipmentVendor
				                     	{
				                     		Shipment = shipment,
				                     		TenantId = vendor.TenantId,
				                     		Primary = vendor.Primary,
				                     		Vendor = vendor.Vendor,
				                     		FailureCode = null,
				                     		ProNumber = vendor.ProNumber,
				                     		FailureComments = string.Empty
				                     	}
					);

            //Shipment Charges
            foreach (var charge in loadOrder.Charges)
                shipment.Charges.Add(new ShipmentCharge
                {
                    TenantId = loadOrder.TenantId,
                    Shipment = shipment,
                    ChargeCode = charge.ChargeCode,
                    Quantity = charge.Quantity,
                    UnitBuy = charge.UnitBuy,
                    UnitSell = charge.UnitSell,
                    UnitDiscount = charge.UnitDiscount,
                    Comment = charge.Comment,
                    VendorId = charge.VendorId
                });

            //Shipment AccountBuckets
			foreach (var accountBucket in loadOrder.AccountBuckets)
				shipment.AccountBuckets.Add(new ShipmentAccountBucket
				                            	{
				                            		Shipment = shipment,
				                            		TenantId = loadOrder.TenantId,
				                            		Primary = accountBucket.Primary,
				                            		AccountBucket = accountBucket.AccountBucket,
				                            	});

            return shipment;
        }

        public static LoadOrder ToLoadOrder(this Shipment shipment, bool roundTrip = false)
        {
            var customer = shipment.Customer ?? new Customer();
	        var loadOrder = new LoadOrder
            {
                LoadOrderNumber = string.Empty,
                Customer = customer,
                DeclineInsurance = shipment.DeclineInsurance,
                Prefix = shipment.Prefix,
                HidePrefix = shipment.HidePrefix,
                ServiceMode = shipment.ServiceMode,
                IsPartialTruckload = shipment.IsPartialTruckload,
                PurchaseOrderNumber = shipment.PurchaseOrderNumber,
                ShipperReference = shipment.ShipperReference,
                ShipmentPriority = shipment.ShipmentPriority,
                CarrierCoordinator = shipment.CarrierCoordinator,
                LoadOrderCoordinator = shipment.ShipmentCoordinator,

                //dates
                DateCreated = DateTime.Now,
                DesiredPickupDate = shipment.DesiredPickupDate,
                EstimatedDeliveryDate = shipment.EstimatedDeliveryDate,

                EarlyDelivery = shipment.EarlyDelivery,
                LateDelivery = shipment.LateDelivery,
                EarlyPickup = shipment.EarlyPickup,
                LatePickup = shipment.LatePickup,

                Mileage = shipment.Mileage,
				EmptyMileage = shipment.EmptyMileage,
                MileageSourceId = shipment.MileageSourceId,
                MiscField1 = shipment.MiscField1,
                MiscField2 = shipment.MiscField2,

				DriverName = shipment.DriverName,
				DriverPhoneNumber = shipment.DriverPhoneNumber,
				DriverTrailerNumber = shipment.DriverTrailerNumber,
                
                HazardousMaterial = shipment.HazardousMaterial,
                HazardousMaterialContactName = shipment.HazardousMaterialContactName,
                HazardousMaterialContactPhone = shipment.HazardousMaterialContactPhone,
                HazardousMaterialContactMobile = shipment.HazardousMaterialContactMobile,
                HazardousMaterialContactEmail = shipment.HazardousMaterialContactEmail,
                Status = LoadOrderStatus.Offered,
                TenantId = shipment.TenantId,
                GeneralBolComments = shipment.GeneralBolComments,
                CriticalBolComments = shipment.CriticalBolComments,
                ShipperBol = shipment.ShipperBol,

				AccountBucketUnitId = shipment.AccountBucketUnitId,
				BillReseller = shipment.BillReseller,
				ResellerAdditionId = shipment.ResellerAdditionId,
				LinearFootRuleBypassed = shipment.LinearFootRuleBypassed,
				SalesRepresentativeId =  shipment.SalesRepresentativeId,
				SalesRepresentativeCommissionPercent = shipment.SalesRepresentativeCommissionPercent,
				SalesRepAddlEntityCommPercent = shipment.SalesRepAddlEntityCommPercent,
				SalesRepAddlEntityName = shipment.SalesRepAddlEntityName,
				SalesRepMaxCommValue = shipment.SalesRepMaxCommValue,
				SalesRepMinCommValue = shipment.SalesRepMinCommValue,
				

                //Lists
                Services = new List<LoadOrderService>(),
                Items = new List<LoadOrderItem>(),
                Stops = new List<LoadOrderLocation>(),
                Notes = new List<LoadOrderNote>(),
                Equipments = new List<LoadOrderEquipment>(),
                CustomerReferences = new List<LoadOrderReference>(),
                Charges = new List<LoadOrderCharge>(),
                Documents = new List<LoadOrderDocument>(),
                Vendors = new List<LoadOrderVendor>(),
                AccountBuckets = new List<LoadOrderAccountBucket>(),
            };

            //Shipment Origin
            if (shipment.Origin != null)
            {
            	loadOrder.Origin = new LoadOrderLocation
            	                  	{
            	                  		LoadOrder = loadOrder,
            	                  		Description = shipment.Origin.Description,
            	                  		Street1 = shipment.Origin.Street1,
            	                  		Street2 = shipment.Origin.Street2,
            	                  		City = shipment.Origin.City,
            	                  		State = shipment.Origin.State,
            	                  		PostalCode = shipment.Origin.PostalCode,
            	                  		CountryId = shipment.Origin.CountryId,
            	                  		TenantId = shipment.Origin.TenantId,
            	                  		StopOrder = shipment.Origin.StopOrder,
            	                  		Contacts = new List<LoadOrderContact>(),
            	                  		SpecialInstructions = shipment.Origin.SpecialInstructions,
            	                  		Direction = shipment.Origin.Direction,
            	                  		GeneralInfo = shipment.Origin.GeneralInfo,
										AppointmentDateTime = shipment.Origin.AppointmentDateTime
            	                  	};
				foreach (var contact in shipment.Origin.Contacts)
					loadOrder.Origin.Contacts.Add(new LoadOrderContact
					                             	{
					                             		Location = loadOrder.Origin,
					                             		Comment = contact.Comment,
					                             		ContactTypeId = contact.ContactTypeId,
					                             		Email = contact.Email,
					                             		Fax = contact.Fax,
					                             		Phone = contact.Phone,
					                             		Mobile = contact.Mobile,
					                             		Name = contact.Name,
					                             		Primary = contact.Primary,
					                             		TenantId = contact.TenantId
					                             	});
            }

            //Shipment Destination
            if (shipment.Destination != null)
            {
            	loadOrder.Destination = new LoadOrderLocation
            	                       	{
            	                       		LoadOrder = loadOrder,
            	                       		Description = shipment.Destination.Description,
            	                       		Street1 = shipment.Destination.Street1,
            	                       		Street2 = shipment.Destination.Street2,
            	                       		City = shipment.Destination.City,
            	                       		State = shipment.Destination.State,
            	                       		PostalCode = shipment.Destination.PostalCode,
            	                       		CountryId = shipment.Destination.CountryId,
            	                       		TenantId = shipment.Destination.TenantId,
            	                       		Contacts = new List<LoadOrderContact>(),
            	                       		StopOrder = shipment.Destination.StopOrder,
            	                       		SpecialInstructions = shipment.Destination.SpecialInstructions,
            	                       		Direction = shipment.Destination.Direction,
            	                       		GeneralInfo = shipment.Destination.GeneralInfo,
											AppointmentDateTime = shipment.Destination.AppointmentDateTime
            	                       	};
				foreach (var contact in shipment.Destination.Contacts)
					loadOrder.Destination.Contacts.Add(new LoadOrderContact
					                                  	{
					                                  		Location = loadOrder.Destination,
					                                  		Comment = contact.Comment,
					                                  		ContactTypeId = contact.ContactTypeId,
					                                  		Email = contact.Email,
					                                  		Fax = contact.Fax,
					                                  		Phone = contact.Phone,
					                                  		Mobile = contact.Mobile,
					                                  		Name = contact.Name,
					                                  		Primary = contact.Primary,
					                                  		TenantId = contact.TenantId
					                                  	});
            }

            //Shipment Stops
            foreach (var stop in shipment.Stops)
            {
                var location = new LoadOrderLocation
                {
                    LoadOrder = loadOrder,
                    Description = stop.Description,
                    Street1 = stop.Street1,
                    Street2 = stop.Street2,
                    City = stop.City,
                    State = stop.State,
                    PostalCode = stop.PostalCode,
                    CountryId = stop.CountryId,
                    Contacts = new List<LoadOrderContact>(),
                    StopOrder = stop.StopOrder,
                    SpecialInstructions = stop.SpecialInstructions,
                    GeneralInfo = stop.GeneralInfo,
                    Direction = stop.Direction,
                    TenantId = stop.TenantId,
					AppointmentDateTime = stop.AppointmentDateTime
                };
                foreach (var contact in stop.Contacts)
                    location.Contacts.Add(new LoadOrderContact
                    {
                        Location = location,
                        Comment = contact.Comment,
                        ContactTypeId = contact.ContactTypeId,
                        Email = contact.Email,
                        Fax = contact.Fax,
                        Phone = contact.Phone,
                        Mobile = contact.Mobile,
                        Name = contact.Name,
                        Primary = contact.Primary,
                        TenantId = contact.TenantId
                    });
                loadOrder.Stops.Add(location);
            }

            //Shipment Items
            foreach (var item in shipment.Items)
            {
                var shipmentItem = new LoadOrderItem
                {
                    LoadOrder = loadOrder,
                    Pickup = item.Pickup,
                    Delivery = item.Delivery,
                    Weight = item.ActualWeight,
                    Length = item.ActualLength,
                    Width = item.ActualWidth,
                    Height = item.ActualHeight,
                    Quantity = item.Quantity,
                    PieceCount = item.PieceCount,
                    Description = item.Description,
                    Comment = item.Comment,
                    FreightClass = item.ActualFreightClass,
                    PackageTypeId = item.PackageTypeId,
                    NMFCCode = item.NMFCCode,
                    HTSCode = item.HTSCode,
                    TenantId = item.TenantId,
                    Value = item.Value,
                    IsStackable = item.IsStackable,
                    HazardousMaterial = item.HazardousMaterial
                };
                loadOrder.Items.Add(shipmentItem);
            }

			if (roundTrip)
			{
				// reverse origin destination
				var origin = loadOrder.Origin;
				loadOrder.Origin = loadOrder.Destination;
				loadOrder.Destination = origin;

				// reverse stops
				if (loadOrder.Stops.Count > 0)
				{
					var i = loadOrder.Stops.Max(s => s.StopOrder);
					foreach (var stop in loadOrder.Stops)
						stop.StopOrder = i--;
				}


				// reverse item pickups/deliveries
				foreach (var item in loadOrder.Items)
				{
					var delivery = item.Delivery;
					item.Delivery = item.Pickup;
					item.Pickup = delivery;
				}
			}
            //Shipment Services
			foreach (var service in shipment.Services)
				loadOrder.Services.Add(new LoadOrderService
				                      	{
				                      		LoadOrder = loadOrder,
				                      		ServiceId = service.ServiceId,
				                      		TenantId = service.TenantId
				                      	}
					);

            //Shipment Equipment
			foreach (var equipment in shipment.Equipments)
				loadOrder.Equipments.Add(new LoadOrderEquipment
				                        	{
				                        		LoadOrder = loadOrder,
				                        		EquipmentTypeId = equipment.EquipmentTypeId,
				                        		TenantId = equipment.TenantId
				                        	}
					);

            //Shipment References
			foreach (var reference in shipment.CustomerReferences)
				loadOrder.CustomerReferences.Add(new LoadOrderReference
				                                	{
				                                		LoadOrder = loadOrder,
				                                		DisplayOnOrigin = reference.DisplayOnOrigin,
				                                		DisplayOnDestination = reference.DisplayOnDestination,
				                                		Required = reference.Required,
				                                		Value = reference.Value,
				                                		Name = reference.Name,
				                                		TenantId = reference.TenantId
				                                	}
					);

            //Shipment Vendors
			foreach (var vendor in shipment.Vendors)
				loadOrder.Vendors.Add(new LoadOrderVendor
				                     	{
				                     		LoadOrder = loadOrder,
				                     		TenantId = vendor.TenantId,
				                     		Primary = vendor.Primary,
				                     		Vendor = vendor.Vendor,
				                     		ProNumber = string.Empty,
				                     	}
					);

            //Shipment Charges
            foreach (var charge in shipment.Charges)
                loadOrder.Charges.Add(new LoadOrderCharge
                {
                    TenantId = shipment.TenantId,
                    LoadOrder = loadOrder,
                    ChargeCode = charge.ChargeCode,
                    Quantity = charge.Quantity,
                    UnitBuy = charge.UnitBuy,
                    UnitSell = charge.UnitSell,
                    UnitDiscount = charge.UnitDiscount,
                    Comment = charge.Comment,
                    VendorId = charge.VendorId
                });

            //Shipment AccountBuckets
			foreach (var accountBucket in shipment.AccountBuckets)
				loadOrder.AccountBuckets.Add(new LoadOrderAccountBucket
				                            	{
				                            		LoadOrder = loadOrder,
				                            		TenantId = shipment.TenantId,
				                            		Primary = accountBucket.Primary,
				                            		AccountBucket = accountBucket.AccountBucket,
				                            	});

            return loadOrder;
        }


		public static Vendor ToVendor(this PendingVendor pendingVendor)
		{
			var vendor = new Vendor
				{
					Active = true,
					BrokerReferenceNumber = pendingVendor.BrokerReferenceNumber,
					BrokerReferenceNumberType = pendingVendor.BrokerReferenceNumberType,
					CTPATCertified = pendingVendor.CTPATCertified,
					CTPATNumber = pendingVendor.CTPATNumber,
					Communication = null,
					DOT = pendingVendor.DOT,
					DateCreated = DateTime.Now,
					Documents = new List<VendorDocument>(),
					Equipments = new List<VendorEquipment>(),
					ExcludeFromAvailableLoadsBlast = true,
					FederalIDNumber = pendingVendor.FederalIDNumber,
					HandlesAir = pendingVendor.HandlesAir,
					HandlesLessThanTruckload = pendingVendor.HandlesLessThanTruckload,
					HandlesPartialTruckload = pendingVendor.HandlesPartialTruckload,
					HandlesRail = pendingVendor.HandlesRail,
					HandlesSmallPack = pendingVendor.HandlesSmallPack,
					HandlesTruckload = pendingVendor.HandlesTruckload,
					Insurances = new List<VendorInsurance>(),
					IsAgent = pendingVendor.IsAgent,
					IsBroker = pendingVendor.IsBroker,
					IsCarrier = pendingVendor.IsCarrier,
					LastAuditDate = DateUtility.SystemEarliestDateTime,
					LastAuditedBy = null,
					Locations = new List<VendorLocation>(),
					MC = pendingVendor.MC,
					Name = pendingVendor.Name,
					NoServiceDays = new List<VendorNoServiceDay>(),
					Notation = pendingVendor.Notation,
					LogoUrl = string.Empty,
					Notes = pendingVendor.Notes,
					CustomField1 = string.Empty,
					CustomField2 = string.Empty,
					CustomField3 = string.Empty,
					CustomField4 = string.Empty,
					PIPCertified = pendingVendor.PIPCertified,
					PIPNumber = pendingVendor.PIPNumber,
					Ratings = new List<VendorRating>(),
					VendorPackageCustomMappings = new List<VendorPackageCustomMapping>(),
					Scac = pendingVendor.Scac,
					Services = new List<VendorService>(),
					SmartWayCertified = pendingVendor.SmartWayCertified,
					TSACertified = pendingVendor.TSACertified,
					Tenant = pendingVendor.Tenant,
					TrackingUrl = pendingVendor.TrackingUrl,
					VendorNumber = pendingVendor.VendorNumber,
					VendorServiceRep = null,
				};
			
			// equipment type
			vendor.Equipments = pendingVendor
				.Equipments
				.Select(e => new VendorEquipment
					{
						Vendor = vendor,
						EquipmentType = e.EquipmentType,
						Tenant = e.Tenant,
					})
				.ToList();

			// insurance
			vendor.Insurances = pendingVendor
				.Insurances
				.Select(i => new VendorInsurance
					{
						Vendor = vendor,
						InsuranceType = i.InsuranceType,
						CarrierName = i.CarrierName,
						CertificateHolder = i.CertificateHolder,
						CoverageAmount = i.CoverageAmount,
						EffectiveDate = i.EffectiveDate,
						ExpirationDate = i.ExpirationDate,
						PolicyNumber = i.PolicyNumber,
						Required = i.Required,
						SpecialInstruction = i.SpecialInstruction,
						Tenant = i.Tenant,
					})
				.ToList();

			// location
			var vendorContacts = new List<VendorContact>();
			vendor.Locations = pendingVendor
				.Locations
				.Select(l =>
					{
						var location = new VendorLocation
							{
								Vendor = vendor,
								Tenant = pendingVendor.Tenant,
								City = l.City,
								Contacts = vendorContacts,
								Country = l.Country,
								DateCreated = l.DateCreated,
								LocationNumber = l.LocationNumber,
								MainRemitToLocation = l.MainRemitToLocation,
								PostalCode = l.PostalCode,
								Primary = l.Primary,
								RemitToLocation = l.RemitToLocation,
								State = l.State,
								Street1 = l.Street1,
								Street2 = l.Street2,
							};
						location.Contacts = l.Contacts
						                     .Select(c => new VendorContact
							                     {
								                     VendorLocation = location,
								                     Tenant = location.Tenant,
								                     Comment = c.Comment,
								                     ContactType = c.ContactType,
								                     Email = c.Email,
								                     Fax = c.Fax,
								                     Mobile = c.Mobile,
								                     Name = c.Name,
								                     Phone = c.Phone,
								                     Primary = c.Primary,
							                     })
						                     .ToList();
						return location;
					})
				.ToList();

			// Services
			vendor.Services = pendingVendor
				.Services
				.Select(s => new VendorService
					{
						Vendor = vendor,
						Tenant = s.Tenant,
						Service = s.Service,
					})
				.ToList();

			return vendor;
		}


	    public static XmlTransmission Duplicate(this XmlTransmission transmission, User user)
        {
            var newTransmission = new XmlTransmission
                                    {
                                        ReferenceNumber = transmission.ReferenceNumber,
                                        ReferenceNumberType = transmission.ReferenceNumberType,
                                        TransmissionDateTime = DateTime.Now,
                                        CommunicationReferenceId = transmission.CommunicationReferenceId,
                                        CommunicationReferenceType = transmission.CommunicationReferenceType,
                                        DocumentType = transmission.DocumentType,
                                        NotificationMethod = transmission.NotificationMethod,
                                        AcknowledgementDateTime = DateUtility.SystemEarliestDateTime,
                                        AcknowledgementMessage = string.Empty,
                                        TenantId = transmission.TenantId,
                                        Xml = transmission.Xml,
                                        TransmissionKey = Guid.NewGuid(),
                                        Direction = transmission.Direction,
                                        User = user,
                                    };
            return newTransmission;
        }
    }
}