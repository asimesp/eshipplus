﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("AverageWeeklyFuel", ReadOnly = false, Source = EntitySource.TableView)]
	public class AverageWeeklyFuel : TenantBase
	{
		private long _chargeCodeId;

		private ChargeCode _chargeCode;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("DateCreated", Description = "Date Created")]
		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[EnableSnapShot("EffectiveDate", Description = "Effective Date")]
		[Property("EffectiveDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime EffectiveDate { get; set; }

		[EnableSnapShot("ChargeCodeId", Description = "Charge Code Reference")]
		[Property("ChargeCodeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ChargeCodeId
		{
			get { return _chargeCodeId; }
			set
			{
				_chargeCodeId = value;
				if (_chargeCode != null && value != _chargeCode.Id) _chargeCode = null;
			}
		}

		[EnableSnapShot("EastCoastCost", Description = "Cost on East Coast")]
		[Property("EastCoastCost", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal EastCoastCost { get; set; }

		[EnableSnapShot("NewEnglandCost", Description = "Cost in New England")]
		[Property("NewEnglandCost", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal NewEnglandCost { get; set; }

		[EnableSnapShot("CentralAtlanticCost", Description = "Cost in Central Atlantic")]
		[Property("CentralAtlanticCost", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal CentralAtlanticCost { get; set; }

		[EnableSnapShot("LowerAtlanticCost", Description = "Cost in Lower Atlantic")]
		[Property("LowerAtlanticCost", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal LowerAtlanticCost { get; set; }

		[EnableSnapShot("MidwestCost", Description = "Cost in Midwest")]
		[Property("MidwestCost", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal MidwestCost { get; set; }

		[EnableSnapShot("GulfCoastCost", Description = "Cost on Gulf Coast")]
		[Property("GulfCoastCost", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal GulfCoastCost { get; set; }

		[EnableSnapShot("RockyMountainCost", Description = "Cost in Rocky Mountain")]
		[Property("RockyMountainCost", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal RockyMountainCost { get; set; }

		[EnableSnapShot("WestCoastCost", Description = "Cost on West Coast")]
		[Property("WestCoastCost", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal WestCoastCost { get; set; }

		[EnableSnapShot("WestCoastLessCaliforniaCost", Description = "Cost on West Coast less California")]
		[Property("WestCoastLessCaliforniaCost", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal WestCoastLessCaliforniaCost { get; set; }

		[EnableSnapShot("CaliforniaCost", Description = "Cost in CaliforniaCost")]
		[Property("CaliforniaCost", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal CaliforniaCost { get; set; }

		[EnableSnapShot("NationalCost", Description = "Cost in NationalCost")]
		[Property("NationalCost", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal NationalCost { get; set; }

		public ChargeCode ChargeCode
		{
			get { return _chargeCodeId == default(long) ? null : _chargeCode ?? (_chargeCode = new ChargeCode(_chargeCodeId, false)); }
			set
			{
				_chargeCode = value;
				_chargeCodeId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public AverageWeeklyFuel()
		{
		}

		public AverageWeeklyFuel(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public AverageWeeklyFuel(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long)) base.Delete();
		}
	}
}
