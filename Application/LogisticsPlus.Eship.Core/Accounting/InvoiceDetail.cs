﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("InvoiceDetail", ReadOnly = false, Source =  EntitySource.TableView)]
	public class InvoiceDetail : TenantBase
	{
		private long _chargeCodeId;
		private long _accountBucketId;
		private long _invoiceId;

		private ChargeCode _chargeCode;
		private AccountBucket _accountBucket;
		private Invoice _invoice;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("Quantity")]
		[Property("Quantity", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int Quantity { get; set; }

		[EnableSnapShot("UnitSell", Description = "Sell Per Quantity Unit")]
		[Property("UnitSell", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal UnitSell { get; set; }

		[EnableSnapShot("UnitDiscount", Description = "Discount Per Quantity Unit")]
		[Property("UnitDiscount", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal UnitDiscount { get; set; }

		[EnableSnapShot("ReferenceNumber", Description = "Reference Number")]
		[Property("ReferenceNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ReferenceNumber { get; set; }

		[EnableSnapShot("ReferenceType", Description = "Reference Number Type")]
		[Property("ReferenceType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public DetailReferenceType ReferenceType { get; set; }

		[EnableSnapShot("Comment")]
		[Property("Comment", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Comment { get; set; }

		[EnableSnapShot("ChargeCodeId", Description = "Charge Code Reference")]
		[Property("ChargeCodeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ChargeCodeId
		{
			get { return _chargeCodeId; }
			set
			{
				_chargeCodeId = value;
				if (_chargeCode != null && value != _chargeCode.Id) _chargeCode = null;
			}
		}

		[EnableSnapShot("AccountBucketId", Description = "Account Bucket Reference")]
		[Property("AccountBucketId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long AccountBucketId
		{
			get { return _accountBucketId; }
			set
			{
				_accountBucketId = value;
				if (_accountBucket != null && value != _accountBucket.Id) _accountBucket = null;
			}
		}

		[EnableSnapShot("InvoiceId", Description = "Invoice Reference")]
		[Property("InvoiceId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long InvoiceId
		{
			get
			{
				if (_invoice != null) _invoiceId = _invoice.Id;
				return _invoiceId ;
			}
			set
			{
				_invoiceId = value;
				if (_invoice != null && value != _invoice.Id) _invoice = null;
			}
		}

		public decimal FinalSell
		{
			get { return UnitSell * (Quantity < 1 ? 1 : Quantity); }
		}
		public decimal AmountDue
		{
			get { return (UnitSell - UnitDiscount)* (Quantity < 1 ? 1 : Quantity); }
		}

		public ChargeCode ChargeCode
		{
			get { return _chargeCodeId == default(long) ? null : _chargeCode ?? (_chargeCode = new ChargeCode(_chargeCodeId, false)); }
			set
			{
				_chargeCode = value;
				_chargeCodeId = value == null ? default(long) : value.Id;
			}
		}

		public AccountBucket AccountBucket
		{
			get { return _accountBucketId == default(long) ? null : _accountBucket ?? (_accountBucket = new AccountBucket(_accountBucketId, false)); }
			set
			{
				_accountBucket = value;
				_accountBucketId = value == null ? default(long) : value.Id;
			}
		}

		public Invoice Invoice
		{
			get { return _invoice ?? (_invoice = new Invoice(_invoiceId)); }
			set
			{
				_invoice = value;
				InvoiceId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public InvoiceDetail() { }

		public InvoiceDetail(long id) : this(id, false) { }

		public InvoiceDetail(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public InvoiceDetail(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
