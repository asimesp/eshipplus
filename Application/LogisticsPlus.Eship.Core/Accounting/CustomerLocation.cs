﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("CustomerLocation", ReadOnly = false, Source =  EntitySource.TableView)]
	public class CustomerLocation : Location
	{
		private long _customerId;

		private Customer _customer;

		private List<CustomerContact> _contacts;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("LocationNumber", Description = "Location Reference Number")]
		[Property("LocationNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LocationNumber { get; set; }

		[EnableSnapShot("BillToLocation", Description = "Is a Bill To Location")]
		[Property("BillToLocation", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool BillToLocation { get; set; }

        [EnableSnapShot("MainBillToLocation", Description = "Is Main Bill To Location")]
        [Property("MainBillToLocation", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool MainBillToLocation { get; set; }

		[EnableSnapShot("Primary", Description = "Location Primary Designation")]
		[Property("Primary", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Primary { get; set; }

		[EnableSnapShot("DateCreated", Description = "Date Created")]
		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[EnableSnapShot("CustomerId", Description = "Customer Reference")]
		[Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long CustomerId
		{
			get
			{
				if (_customer != null) _customerId = _customer.Id;
				return _customerId;
			}
			set
			{
				_customerId = value;
				if (_customer != null && value != _customer.Id) _customer = null;
			}
		}

		public Customer Customer
		{
			get { return _customer ?? (_customer = new Customer(_customerId)); }
			set
			{
				_customer = value;
				_customerId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public List<CustomerContact> Contacts
		{
			get
			{
				if (_contacts == null) LoadContacts();
				return _contacts;
			}
			set { _contacts = value; }
		}

		public CustomerLocation()
		{
			
		}

		public CustomerLocation(long id) : this(id, false)
		{
		}

		public CustomerLocation(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public CustomerLocation(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

		public void LoadCollections()
		{
			LoadContacts();
		}

		private void LoadContacts()
		{
			_contacts = new List<CustomerContact>();
			const string query = "Select * from CustomerContact where CustomerLocationId = @CustomerLocationId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "CustomerLocationId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_contacts.Add(new CustomerContact(reader));
			Connection.Close();
		}
	}
}
