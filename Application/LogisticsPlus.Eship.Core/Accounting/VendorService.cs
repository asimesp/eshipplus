﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("VendorService", ReadOnly = false, Source = EntitySource.TableView)]
	public class VendorService : TenantBase
	{
		private long _serviceId;
		private long _vendorId;

		private Vendor _vendor;
		private Service _service;

		[EnableSnapShot("ServiceId", Description = "Service Reference")]
		[Property("ServiceId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long ServiceId
		{
			get
			{
				if (_service != null) _serviceId = _service.Id;
				return _serviceId;
			}
			set
			{
				_serviceId = value;
				if (_service != null && value != _service.Id) _service = null;
			}
		}

		[EnableSnapShot("VendorId", Description = "Vendor Reference")]
		[Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long VendorId
		{
			get
			{
				if (_vendor != null) _vendorId = _vendor.Id;
				return _vendorId;
			}
			set
			{
				_vendorId = value;
				if (_vendor != null && value != _vendor.Id) _vendor = null;
			}
		}

		public Vendor Vendor
		{
			get { return _vendor ?? (_vendor = new Vendor(_vendorId)); }
			set
			{
				_vendor = value;
				_vendorId = value == null ? default(long) : value.Id;
			}
		}
		public Service Service
		{
			get { return _service ?? (_service = new Service(_serviceId)); }
			set
			{
				_service = value;
				_serviceId = value == null ? default(long) : value.Id;
			}
		}

		public VendorService()
		{
		}

		public VendorService(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			Insert();
		}

		public new void Delete()
		{
			base.Delete();
		}
	}
}
