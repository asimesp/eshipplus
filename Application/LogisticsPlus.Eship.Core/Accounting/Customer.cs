﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("Customer", ReadOnly = false, Source = EntitySource.TableView)]
	public class Customer : TenantBase
	{
		private long _defaultAccountBucketId;
		private long _requiredMileageSourceId;
		private long _prefixId;
		private long _ratingId;

		private Tier _tier;
		private AccountBucket _accountBucket;
		private SalesRepresentative _salesRepresentative;
		private MileageSource _mileageSource;
		private Prefix _prefix;
		private CustomerRating _rating;
		private CustomerCommunication _communication;

		private List<CustomerServiceRepresentative> _serviceRepresentatives;
		private List<RequiredInvoiceDocumentTag> _requiredInvoiceDocumentTags;
		private List<CustomerLocation> _locations;
		private List<CustomField> _customFields;
		private List<CustomerChargeCodeMap> _customerChargeCodeMap;
		private List<CustomerDocument> _documents;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("CustomerNumber", Description = "Customer Number")]
		[Property("CustomerNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomerNumber { get; set; }

		[EnableSnapShot("Name")]
		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

		[EnableSnapShot("DefaultAccountBucketId", Description = "Default Account Bucket Reference")]
		[Property("DefaultAccountBucketId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long DefaultAccountBucketId
		{
			get { return _defaultAccountBucketId; }
			set
			{
				_defaultAccountBucketId = value;
				if (_accountBucket != null && _accountBucket.Id != value) _accountBucket = null;
			}
		}

		[EnableSnapShot("PrefixId", Description = "Prefix Reference")]
		[Property("PrefixId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long PrefixId
		{
			get { return _prefixId; }
			set
			{
				_prefixId = value;
				if (_prefix != null && value != _prefix.Id) _prefix = null;
			}
		}

		[EnableSnapShot("HidePrefix", Description = "Hide Prefix")]
		[Property("HidePrefix", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool HidePrefix { get; set; }

		[EnableSnapShot("Active", Description = "Active Status")]
		[Property("Active", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Active { get; set; }

		[EnableSnapShot("RequiredMileageSourceId", Description = "Mileage Source Reference")]
		[Property("RequiredMileageSourceId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long RequiredMileageSourceId
		{
			get { return _requiredMileageSourceId; }
			set
			{
				_requiredMileageSourceId = value;
				if (_mileageSource != null && _mileageSource.Id != value) _mileageSource = null;
			}
		}

		[EnableSnapShot("CustomerType", Description = "Customer Type")]
		[Property("CustomerType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public CustomerType CustomerType { get; set; }

		[EnableSnapShot("AdditionalBillOfLadingText", Description = "Additional Bill of Lading Text")]
		[Property("AdditionalBillOfLadingText", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string AdditionalBillOfLadingText { get; set; }

		[EnableSnapShot("InvoiceTerms", Description = "Invoice Terms")]
		[Property("InvoiceTerms", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int InvoiceTerms { get; set; }

		[EnableSnapShot("InvoiceRequiresCarrierProNumber", Description = "Invoice Pro Number Requirement")]
		[Property("InvoiceRequiresCarrierProNumber", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool InvoiceRequiresCarrierProNumber { get; set; }

		[EnableSnapShot("InvoiceRequiresShipperReference", Description = "Invoice Shipper Reference Requirement")]
		[Property("InvoiceRequiresShipperReference", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool InvoiceRequiresShipperReference { get; set; }

		[EnableSnapShot("InvoiceRequiresControlAccount", Description = "Invoice Customer Control Account Requirement")]
		[Property("InvoiceRequiresControlAccount", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool InvoiceRequiresControlAccount { get; set; }

        [EnableSnapShot("CanInvoiceNonDeliveredShipment", Description = "Can Invoice Non Delivered Shipment")]
        [Property("CanInvoiceNonDeliveredShipment", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool CanInvoiceNonDeliveredShipment { get; set; }

        [EnableSnapShot("ShipmentRequiresPurchaseOrderNumber", Description = "Shipment Purchase Order Requirement")]
		[Property("ShipmentRequiresPurchaseOrderNumber", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ShipmentRequiresPurchaseOrderNumber { get; set; }

		[EnableSnapShot("ValidatePurchaseOrderNumber", Description = "Purchase Order Validation Requirement")]
		[Property("ValidatePurchaseOrderNumber", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ValidatePurchaseOrderNumber { get; set; }

		[EnableSnapShot("InvalidPurchaseOrderNumberMessage", Description = "Invalid Purchase Order Number Message")]
		[Property("InvalidPurchaseOrderNumberMessage", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string InvalidPurchaseOrderNumberMessage { get; set; }

		[EnableSnapShot("CustomVendorSelectionMessage", Description = "Custom Message at Vendor Selection")]
		[Property("CustomVendorSelectionMessage", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomVendorSelectionMessage { get; set; }

		[EnableSnapShot("ShipmentRequiresNMFC", Description = "Shipment NMFC Requirement")]
		[Property("ShipmentRequiresNMFC", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ShipmentRequiresNMFC { get; set; }

		[EnableSnapShot("DateCreated", Description = "Date Created")]
		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[EnableSnapShot("LogoUrl", Description = "Logo Url")]
		[Property("LogoUrl", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LogoUrl { get; set; }

		[EnableSnapShot("ShipperBillOfLadingSeed", Description = "Shipper Bill of Lading Seed")]
		[Property("ShipperBillOfLadingSeed", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ShipperBillOfLadingSeed { get; set; }

		[EnableSnapShot("EnableShipperBillOfLading", Description = "Shipper Bill of Lading Enabled")]
		[Property("EnableShipperBillOfLading", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool EnableShipperBillOfLading { get; set; }

		[EnableSnapShot("ShipperBillOfLadingPrefix", Description = "Shipper Bill of Lading Prefix")]
		[Property("ShipperBillOfLadingPrefix", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ShipperBillOfLadingPrefix { get; set; }

		[EnableSnapShot("ShipperBillOfLadingSuffix", Description = "Shipper Bill of Lading Suffix")]
		[Property("ShipperBillOfLadingSuffix", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ShipperBillOfLadingSuffix { get; set; }

		[EnableSnapShot("CreditLimit", Description = "Credit Limit")]
		[Property("CreditLimit", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal CreditLimit { get; set; }

		[EnableSnapShot("Notes", Description = "Quick Reference Notes")]
		[Property("Notes", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Notes { get; set; }

		[EnableSnapShot("CustomField1")]
		[Property("CustomField1", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomField1 { get; set; }

		[EnableSnapShot("CustomField2")]
		[Property("CustomField2", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomField2 { get; set; }

		[EnableSnapShot("CustomField3")]
		[Property("CustomField3", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomField3 { get; set; }

		[EnableSnapShot("CustomField4")]
		[Property("CustomField4", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomField4 { get; set; }

	    [EnableSnapShot("AuditInstructions", Description = "Audit Instructions")]
	    [Property("AuditInstructions", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
	    public string AuditInstructions { get; set; }

		[EnableSnapShot("CareOfAddressFormatEnabled", Description = "Care Of Address Format Enabled")]
		[Property("CareOfAddressFormatEnabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool CareOfAddressFormatEnabled { get; set; }

		[EnableSnapShot("CustomerToleranceEnabled", Description = "Customer Tolerance Enabled")]
		[Property("CustomerToleranceEnabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool CustomerToleranceEnabled { get; set; }

		[EnableSnapShot("TruckloadPickupTolerance", Description = "Truckload Pickup Tolerance Minutes")]
		[Property("TruckloadPickupTolerance", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int TruckloadPickupTolerance { get; set; }

		[EnableSnapShot("TruckloadDeliveryTolerance", Description = "Truckload Delivery Tolerance Minutes")]
		[Property("TruckloadDeliveryTolerance", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int TruckloadDeliveryTolerance { get; set; }

		[EnableSnapShot("RatingId", Description = "Rating Settings Reference")]
		[Property("RatingId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long RatingId
		{
			get { return _ratingId; }
			set
			{
				_ratingId = value;
				if (_rating != null && value != _rating.Id) _rating = null;
			}
		}

		[EnableSnapShot("CommunicationId", Description = "Communication Settings Reference")]
		[Property("CommunicationId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		private long CommunicationId { get; set; }

		[EnableSnapShot("SalesRepresentativeId", Description = "Sales Representative Reference")]
		[Property("SalesRepresentativeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		private long SalesRepresentativeId { get; set; }

	    [EnableSnapShot("TierId", Description = "Tier Reference")]
		[Property("TierId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		private long TierId { get; set; }

        [EnableSnapShot("PaymentGatewayKey", Description = "Payment Gateway Key")]
        [Property("PaymentGatewayKey", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string PaymentGatewayKey { get; set; }

        [EnableSnapShot("IsCashOnly", Description = "Is Cash Only")]
        [Property("IsCashOnly", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool IsCashOnly { get; set; }

        [EnableSnapShot("CanPayByCreditCard", Description = "Can Pay By Credit Card")]
        [Property("CanPayByCreditCard", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool CanPayByCreditCard { get; set; }

		[EnableSnapShot("RateAndScheduleInDemo", Description = "Rate And Schedule In Demo")]
		[Property("RateAndScheduleInDemo", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool RateAndScheduleInDemo { get; set; }

        [EnableSnapShot("PurgeExpiredQuotes", Description = "Purge Expired Quotes")]
        [Property("PurgeExpiredQuotes", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool PurgeExpiredQuotes { get; set; }

	    [EnableSnapShot("AllowLocationContactNotification", Description = "Allow Location Contact Notification")]
	    [Property("AllowLocationContactNotification", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
	    public bool AllowLocationContactNotification { get; set; }

        public Tier Tier
		{
			get { return TierId == default(long) ? null : _tier ?? (_tier = new Tier(TierId)); }
			set
			{
				_tier = value;
				TierId = value == null ? default(long) : value.Id;
			}
		}
		public AccountBucket DefaultAccountBucket
		{
			get
			{
				return _defaultAccountBucketId == default(long)
						? null
						: _accountBucket ?? (_accountBucket = new AccountBucket(_defaultAccountBucketId, false));
			}
			set
			{
				_accountBucket = value;
				_defaultAccountBucketId = value == null ? default(long) : value.Id;
			}
		}
		public Prefix Prefix
		{
			get { return _prefixId == default(long) ? null : _prefix ?? (_prefix = new Prefix(_prefixId, false)); }
			set
			{
				_prefix = value;
				_prefixId = value == null ? default(long) : value.Id;
			}
		}
		public SalesRepresentative SalesRepresentative
		{
			get
			{
				return SalesRepresentativeId == default(long)
						? null
						: _salesRepresentative ?? (_salesRepresentative = new SalesRepresentative(SalesRepresentativeId, false));
			}
			set
			{
				_salesRepresentative = value;
				SalesRepresentativeId = value == null ? default(long) : value.Id;
			}
		}
		public MileageSource MileageSource
		{
			get
			{
				return _requiredMileageSourceId == default(long)
						? null
						: _mileageSource ?? (_mileageSource = new MileageSource(_requiredMileageSourceId));
			}
			set
			{
				_mileageSource = value;
				_requiredMileageSourceId = value == null ? default(long) : value.Id;
			}
		}
		public CustomerCommunication Communication
		{
			get { return CommunicationId == default(long) ? null : _communication ?? (_communication = new CustomerCommunication(CommunicationId)); }
			set
			{
				_communication = value;
				CommunicationId = value == null ? default(long) : value.Id;
			}
		}
		public CustomerRating Rating
		{
			get { return _ratingId == default(long) ? null : _rating ?? (_rating = new CustomerRating(_ratingId)); }
			set
			{
				_rating = value;
				_ratingId = value == null ? default(long) : value.Id;
			}
		}

		public List<RequiredInvoiceDocumentTag> RequiredInvoiceDocumentTags
		{
			get
			{
				if (_requiredInvoiceDocumentTags == null) LoadRequiredInvoiceDocumentTags();
				return _requiredInvoiceDocumentTags;
			}
			set { _requiredInvoiceDocumentTags = value; }
		}
		public List<CustomerLocation> Locations
		{
			get
			{
				if (_locations == null) LoadLocations();
				return _locations;
			}
			set { _locations = value; }
		}
		public List<CustomerServiceRepresentative> ServiceRepresentatives
		{
			get
			{
				if (_serviceRepresentatives == null) LoadServiceRepresentatives();
				return _serviceRepresentatives;
			}
			set { _serviceRepresentatives = value; }
		}
		public List<CustomField> CustomFields
		{
			get
			{
				if (_customFields == null) LoadCustomFields();
				return _customFields;
			}
			set { _customFields = value; }
		}

		public List<CustomerChargeCodeMap> CustomChargeCodeMap
		{
			get
			{
				if (_customerChargeCodeMap == null) LoadCustomerChargeCodeMap();
				return _customerChargeCodeMap;
			}
			set { _customerChargeCodeMap = value; }
		}
		public List<CustomerDocument> Documents
		{
			get
			{
				if (_documents == null) LoadDocuments();
				return _documents;
			}
			set { _documents = value; }
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public Customer()
		{
			
		}

		public Customer(long id) : this(id, false)
		{
		}

		public Customer(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public Customer(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

		public void LoadCollections()
		{
			LoadCustomFields();
			LoadCustomerChargeCodeMap();
			LoadServiceRepresentatives();
			LoadLocations();
			LoadRequiredInvoiceDocumentTags();
			LoadDocuments();
		}

		public List<DocumentTag> RetrieveRequiredInvoiceDocumentTags()
		{
			var users = new List<DocumentTag>();
			const string query = @"select DocumentTag.* from DocumentTag, RequiredInvoiceDocumentTag where 
				RequiredInvoiceDocumentTag.DocumentTagId = DocumentTag.Id and RequiredInvoiceDocumentTag.CustomerId = @CustomerId
				and RequiredInvoiceDocumentTag.TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "TenantId", TenantId }, { "CustomerId", Id } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					users.Add(new DocumentTag(reader));
			Connection.Close();
			return users;
		}

		public List<User> RetrieveServiceRepresentatives()
		{
			var users = new List<User>();
			const string query = @"select [User].* from [User], CustomerServiceRepresentative where 
				CustomerServiceRepresentative.UserId = [User].[Id] and CustomerServiceRepresentative.CustomerId = @CustomerId
				and CustomerServiceRepresentative.TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "TenantId", TenantId }, { "CustomerId", Id } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					users.Add(new User(reader));
			Connection.Close();
			return users;
		}

		public List<UserShipAs> RetrieveUserShipAs()
		{
			var shipAs = new List<UserShipAs>();
			const string query = "Select UserShipAs.* from UserShipAs where UserShipAs.TenantId = @TenantId	and UserShipAs.CustomerId = @CustomerId";
			var parameters = new Dictionary<string, object> { { "TenantId", TenantId }, { "CustomerId", Id } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					shipAs.Add(new UserShipAs(reader));
			Connection.Close();
			return shipAs;
		}

        public List<User> RetrieveNonEmployeeUsers()
        {
            var users = new List<User>();
            const string query = @"Select * 
                                    From [User] 
                                    Where [User].TenantId = @TenantId 
		                                    And ([User].CustomerId = @CustomerId Or (Select Count(*) From UserShipAs Where UserShipAs.UserId = [User].Id And CustomerId = @CustomerId) > 0) 
		                                    And [User].TenantEmployee = 0";
            var parameters = new Dictionary<string, object> { { "TenantId", TenantId }, { "CustomerId", Id } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    users.Add(new User(reader));
            Connection.Close();
            return users;
        }
		
		private void LoadCustomFields()
		{
			_customFields = new List<CustomField>();
			const string query = "Select * from CustomField where CustomerId = @CustomerId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> {{"CustomerId", Id}, {"TenantId", TenantId}};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_customFields.Add(new CustomField(reader));
			Connection.Close();
		}

		private void LoadCustomerChargeCodeMap()
		{
			_customerChargeCodeMap = new List<CustomerChargeCodeMap>();
			const string query = "Select * from CustomerChargeCodeMap where CustomerId = @CustomerId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "CustomerId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_customerChargeCodeMap.Add(new CustomerChargeCodeMap(reader));
			Connection.Close();
		}

		private void LoadServiceRepresentatives()
		{
			_serviceRepresentatives = new List<CustomerServiceRepresentative>();
			const string query = "Select * from CustomerServiceRepresentative where CustomerId = @CustomerId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "CustomerId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_serviceRepresentatives.Add(new CustomerServiceRepresentative(reader));
			Connection.Close();
		}

		private void LoadLocations()
		{
			_locations = new List<CustomerLocation>();
			const string query =
				"Select * from CustomerLocation where CustomerId = @CustomerId and TenantId = @TenantId Order by LocationNumber ASC";
			var parameters = new Dictionary<string, object> { { "CustomerId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_locations.Add(new CustomerLocation(reader));
			Connection.Close();
		}

		private void LoadRequiredInvoiceDocumentTags()
		{
			_requiredInvoiceDocumentTags = new List<RequiredInvoiceDocumentTag>();
			const string query = "Select * from RequiredInvoiceDocumentTag where CustomerId = @CustomerId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "CustomerId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_requiredInvoiceDocumentTags.Add(new RequiredInvoiceDocumentTag(reader));
			Connection.Close();
		}

		private void LoadDocuments()
		{
			_documents = new List<CustomerDocument>();
			const string query = @"SELECT * FROM CustomerDocument WHERE CustomerId = @CustomerId AND TenantId = @TenantId;";
			var parameters = new Dictionary<string, object> { { "CustomerId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_documents.Add(new CustomerDocument(reader));
			Connection.Close();
		}
	}
}
