﻿namespace LogisticsPlus.Eship.Core.Accounting
{
    // DO NOT RE-ORDER. SEE APPLICABLE JOINS ON DATABASE REFCOMMISSIONPAYMENTTYPE
    public enum CommissionPaymentType
	{
		Payment = 0,
		Reversal
	}
}
