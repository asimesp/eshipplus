﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("RequiredInvoiceDocumentTag", ReadOnly = false, Source = EntitySource.TableView)]
	public class RequiredInvoiceDocumentTag : TenantBase
	{
		private long _documentTagId;
		private long _customerId;

		private Customer _customer;
		private DocumentTag _documentTag;

		[Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long CustomerId
		{
			get
			{
				if (_customer != null) _customerId = _customer.Id;
				return _customerId;
			}
			set
			{
				_customerId = value;
				if (_customer != null && value != _customer.Id) _customer = null;
			}
		}

		[Property("DocumentTagId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long DocumentTagId
		{
			get
			{
				if (_documentTag != null) _documentTagId = _documentTag.Id;
				return _documentTagId;
			}
			set
			{
				_documentTagId = value;
				if (_documentTag != null && value != _documentTag.Id) _documentTag = null;
			}
		}

		public Customer Customer
		{
			get { return _customer ?? (_customer = new Customer(_customerId)); }
			set
			{
				_customer = value;
				_customerId = value == null ? default(long) : value.Id;
			}
		}
		public DocumentTag DocumentTag
		{
			get { return _documentTag ?? (_documentTag = new DocumentTag(_documentTagId)); }
			set
			{
				_documentTag = value;
				_documentTagId = value == null ? default(long) : value.Id;
			}
		}

		public RequiredInvoiceDocumentTag(){}

		public RequiredInvoiceDocumentTag(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			Insert();
		}

		public new void Delete()
		{
			base.Delete();
		}
	}
}
