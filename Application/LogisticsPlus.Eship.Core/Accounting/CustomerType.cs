﻿namespace LogisticsPlus.Eship.Core.Accounting
{
	public enum CustomerType
	{
        // DO NOT RE-ORDER. SEE APPLICABLE JOINS ON DATABASE REFCUSTOMERTYPE
        Reseller = 0,
		DirectShipperReceiver,
		BrokerCustomer
	}
}
