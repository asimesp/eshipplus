﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("InvoicePrintLog", ReadOnly = false, Source = EntitySource.TableView)]
	public class InvoicePrintLog : TenantBase
	{
		private long _invoiceId;

		private Invoice _invoice;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[Property("InvoiceId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long InvoiceId
		{
			get
			{
				if (_invoice != null) _invoiceId = _invoice.Id;
				return _invoiceId;
			}
			set
			{
				_invoiceId = value;
				if (_invoice != null && value != _invoice.Id) _invoice = null;
			}
		}

		[Property("LogDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime LogDate { get; set; }

		[Property("UserLogon", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string UserLogon { get; set; }

		[Property("UserFirstName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string UserFirstName { get; set; }

		[Property("UserLastName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string UserLastName { get; set; }

		[Property("UserDefaultCustomerNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string UserDefaultCustomerNumber { get; set; }

		[Property("UserDefaultCustomerName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string UserDefaultCustomerName { get; set; }

		[Property("TenantEmployee", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool TenantEmployee { get; set; }

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public Invoice Invoice
		{
			get { return _invoice ?? (_invoice = new Invoice(_invoiceId)); }
			set
			{
				_invoice = value;
				InvoiceId = value == null ? default(long) : value.Id;
			}
		}

		public InvoicePrintLog() {}

		public InvoicePrintLog(long id)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
		}

		public InvoicePrintLog(DbDataReader reader)
		{
			Load(reader);
		}

		public void Log()
		{
			Insert();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
