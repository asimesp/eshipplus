﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("PendingVendorLocation", ReadOnly = false, Source = EntitySource.TableView)]
	public class PendingVendorLocation: Location
	{
		private long _pendingVendorId;

		private PendingVendor _pendingVendor;

		private List<PendingVendorContact> _contacts;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("LocationNumber", Description = "Location Reference Number")]
		[Property("LocationNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LocationNumber { get; set; }

		[EnableSnapShot("RemitToLocation", Description = "Is a Remit To Location")]
		[Property("RemitToLocation", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool RemitToLocation { get; set; }

        [EnableSnapShot("MainRemitToLocation", Description = "Is Main Remit To Location")]
        [Property("MainRemitToLocation", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool MainRemitToLocation { get; set; }

		[EnableSnapShot("Primary", Description = "Location Primary Designation")]
		[Property("Primary", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Primary { get; set; }

		[EnableSnapShot("DateCreated", Description = "Date Created")]
		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[EnableSnapShot("PendingVendorId", Description = "Pending Vendor Reference")]
		[Property("PendingVendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long PendingVendorId
		{
			get
			{
				if (_pendingVendor != null) _pendingVendorId = _pendingVendor.Id;
				return _pendingVendorId;
			}
			set
			{
				_pendingVendorId = value;
				if (_pendingVendor != null && value != _pendingVendor.Id) _pendingVendor = null;
			}
		}

		public PendingVendor PendingVendor
		{
			get { return _pendingVendor ?? (_pendingVendor = new PendingVendor(_pendingVendorId)); }
			set
			{
				_pendingVendor = value;
				_pendingVendorId = value == null ? default(long) : value.Id;
			}
		}

		public List<PendingVendorContact> Contacts
		{
			get
			{
				if (_contacts == null) LoadContacts();
				return _contacts;
			}
			set { _contacts = value; }
		}

		public bool IsNew { get { return Id == default(long); } }

		public PendingVendorLocation()
		{
			
		}

		public PendingVendorLocation(long id) : this(id, false)
		{
		}

		public PendingVendorLocation(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public PendingVendorLocation(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

		public void LoadCollections()
		{
			LoadContacts();
		}

		private void LoadContacts()
		{
			_contacts = new List<PendingVendorContact>();
			const string query = "Select * from PendingVendorContact where PendingVendorLocationId = @PendingVendorLocationId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "PendingVendorLocationId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_contacts.Add(new PendingVendorContact(reader));
			Connection.Close();
		}
	}
}