﻿namespace LogisticsPlus.Eship.Core.Accounting
{
    public enum ChargeType
	{
        // Full copy of DetailReferenceType
        // DO NOT RE-ORDER. SEE APPLICABLE JOINS ON DATABASE REFDETAILREFERENCETYPE
        Shipment = 0,
        ServiceTicket,
        Miscellaneous,
        LoadOrder
    }
}
