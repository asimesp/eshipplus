﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("CustomerContact", ReadOnly = false, Source = EntitySource.TableView)]
	public class CustomerContact : Contact
	{
		private long _customerLocationId;

		private CustomerLocation _customerLocation;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key =  true)]
		public long Id { get; private set; }

		[Property("CustomerLocationId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long CustomerLocationId
		{
			get
			{
				if (_customerLocation != null) _customerLocationId = _customerLocation.Id;
				return _customerLocationId;
			}
			set
			{
				_customerLocationId = value;
				if (_customerLocation != null && value != _customerLocation.Id) _customerLocation = null;
			}
		}

		public CustomerLocation CustomerLocation
		{
			get { return _customerLocation ?? (_customerLocation = new CustomerLocation(_customerLocationId)); }
			set
			{
				_customerLocation = value;
				_customerLocationId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public CustomerContact()
		{
			
		}

		public CustomerContact(long id) : this(id, false)
		{
		}

		public CustomerContact(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public CustomerContact(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
