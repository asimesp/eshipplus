﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("PendingVendorEquipment", ReadOnly = false, Source = EntitySource.TableView)]
	public class PendingVendorEquipment : TenantBase
	{
		private long _pendingVendorId;
		private long _equipmentTypeId;

		private EquipmentType _equipmentType;
		private PendingVendor _pendingVendor;

		[EnableSnapShot("EquipmentTypeId", Description = "Equipment Type Reference")]
		[Property("EquipmentTypeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long EquipmentTypeId
		{
			get
			{
				if (_equipmentType != null) _equipmentTypeId = _equipmentType.Id;
				return _equipmentTypeId;
			}
			set
			{
				_equipmentTypeId = value;
				if (_equipmentType != null && value != _equipmentType.Id) _equipmentType = null;
			}
		}

		[EnableSnapShot("PendingVendorId", Description = "Pending Vendor Reference")]
		[Property("PendingVendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long PendingVendorId
		{
			get
			{
				if (_pendingVendor != null) _pendingVendorId = _pendingVendor.Id;
				return _pendingVendorId;
			}
			set
			{
				_pendingVendorId = value;
				if (_pendingVendor != null && value != _pendingVendor.Id) _pendingVendor = null;
			}
		}

		public PendingVendor PendingVendor
		{
			get { return _pendingVendor ?? (_pendingVendor = new PendingVendor(_pendingVendorId)); }
			set
			{
				_pendingVendor = value;
				_pendingVendorId = value == null ? default(long) : value.Id;
			}
		}
		public EquipmentType EquipmentType
		{
			get { return _equipmentType ?? (_equipmentType = new EquipmentType(_equipmentTypeId)); }
			set
			{
				_equipmentType = value;
				_equipmentTypeId = value == null ? default(long) : value.Id;
			}
		}

		public PendingVendorEquipment()
		{
		}

		public PendingVendorEquipment(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			Insert();
		}

		public new void Delete()
		{
			base.Delete();
		}
	}
}
