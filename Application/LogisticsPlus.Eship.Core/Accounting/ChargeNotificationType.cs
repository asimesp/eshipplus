﻿namespace LogisticsPlus.Eship.Core.Accounting
{
    public enum ChargeNotificationType
    {
        // DO NOT RE-ORDER
        Confirm = 0,
        Dispute
    }
}
