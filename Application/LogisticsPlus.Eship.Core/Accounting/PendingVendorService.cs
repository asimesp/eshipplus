﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("PendingVendorService", ReadOnly = false, Source = EntitySource.TableView)]
	public class PendingVendorService : TenantBase
	{
		private long _serviceId;
		private long _pendingVendorId;

		private PendingVendor _pendingVendor;
		private Service _service;

		[EnableSnapShot("ServiceId", Description = "Service Reference")]
		[Property("ServiceId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long ServiceId
		{
			get
			{
				if (_service != null) _serviceId = _service.Id;
				return _serviceId;
			}
			set
			{
				_serviceId = value;
				if (_service != null && value != _service.Id) _service = null;
			}
		}

		[EnableSnapShot("PendingVendorId", Description = "Pending Vendor Reference")]
		[Property("PendingVendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long PendingVendorId
		{
			get
			{
				if (_pendingVendor != null) _pendingVendorId = _pendingVendor.Id;
				return _pendingVendorId;
			}
			set
			{
				_pendingVendorId = value;
				if (_pendingVendor != null && value != _pendingVendor.Id) _pendingVendor = null;
			}
		}

		public PendingVendor PendingVendor
		{
			get { return _pendingVendor ?? (_pendingVendor = new PendingVendor(_pendingVendorId)); }
			set
			{
				_pendingVendor = value;
				_pendingVendorId = value == null ? default(long) : value.Id;
			}
		}
		public Service Service
		{
			get { return _service ?? (_service = new Service(_serviceId)); }
			set
			{
				_service = value;
				_serviceId = value == null ? default(long) : value.Id;
			}
		}

		public PendingVendorService()
		{
		}

		public PendingVendorService(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			Insert();
		}

		public new void Delete()
		{
			base.Delete();
		}
	}
}
