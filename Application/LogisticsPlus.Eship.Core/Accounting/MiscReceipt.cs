﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
    [Serializable]
    [Entity("MiscReceipt", ReadOnly = false, Source = EntitySource.TableView)]
    public class MiscReceipt : TenantBase
    {
        private long _customerId;
        private long _loadOrderId;
        private long _originalMiscReceiptId;
        private long _shipmentId;
        private long _userId;

        private Customer _customer;
        private LoadOrder _loadOrder;
        private MiscReceipt _originalMiscReceipt;
        private Shipment _shipment;
        private User _user;

        private List<MiscReceiptApplication> _miscReceiptApplications;


        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; private set; }

        [EnableSnapShot("ShipmentId", Description = "Shipment Reference")]
        [Property("ShipmentId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long ShipmentId
        {
            get { return _shipmentId; }
            set
            {
                _shipmentId = value;
                if (_shipment != null && value != _shipment.Id) _shipment = null;
            }
        }

        [EnableSnapShot("LoadOrderId", Description = "Load Order Reference")]
        [Property("LoadOrderId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long LoadOrderId
        {
            get { return _loadOrderId; }
            set
            {
                _loadOrderId = value;
                if (_loadOrder != null && value != _loadOrder.Id) _loadOrder = null;
            }
        }

        [EnableSnapShot("UserId", Description = "User Reference")]
        [Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long UserId
        {
            get { return _userId; }
            set
            {
                _userId = value;
                if (_user != null && value != _user.Id) _user = null;
            }
        }

        [EnableSnapShot("CustomerId", Description = "Customer Reference")]
        [Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long CustomerId
        {
            get { return _customerId; }
            set
            {
                _customerId = value;
                if (_customer != null && value != _customer.Id) _customer = null;
            }
        }

        [EnableSnapShot("AmountPaid", Description = "Amount Paid")]
        [Property("AmountPaid", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal AmountPaid { get; set; }

        [EnableSnapShot("PaymentDate", Description = "Payment Date")]
        [Property("PaymentDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime PaymentDate { get; set; }

        [EnableSnapShot("GatewayTransactionId", Description = "Gateway Transaction Id")]
        [Property("GatewayTransactionId", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string GatewayTransactionId { get; set; }

        [EnableSnapShot("OriginalMiscReceiptId", Description = "Original Misc Receipt Reference")]
        [Property("OriginalMiscReceiptId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long OriginalMiscReceiptId
        {
            get { return _originalMiscReceiptId; }
            set
            {
                _originalMiscReceiptId = value;
                if (_originalMiscReceipt != null && value != _originalMiscReceipt.Id) _originalMiscReceipt = null;
            }
        }

        [EnableSnapShot("PaymentGatewayType", Description = "Payment Gateway Type")]
        [Property("PaymentGatewayType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public PaymentGatewayType PaymentGatewayType { get; set; }

        [EnableSnapShot("Reversal")]
        [Property("Reversal", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool Reversal { get; set; }

        [EnableSnapShot("NameOnCard", Description = "Name On Card")]
        [Property("NameOnCard", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string NameOnCard { get; set; }

        [EnableSnapShot("PaymentProfileId", Description = "Payment Profile Id")]
        [Property("PaymentProfileId", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string PaymentProfileId { get; set; }


        public Customer Customer
        {
            get { return _customerId == default(long) ? null : _customer ?? (_customer = new Customer(_customerId, false)); }
            set
            {
                _customer = value;
                _customerId = value == null ? default(long) : value.Id;
            }
        }
        public LoadOrder LoadOrder
        {
            get { return _loadOrderId == default(long) ? null : _loadOrder ?? (_loadOrder = new LoadOrder(_loadOrderId, false)); }
            set
            {
                _loadOrder = value;
                _loadOrderId = value == null ? default(long) : value.Id;
            }
        }

        public MiscReceipt OriginalMiscReceipt
        {
            get { return _originalMiscReceiptId == default(long) ? null : _originalMiscReceipt ?? (_originalMiscReceipt = new MiscReceipt(_originalMiscReceiptId, false)); }
            set
            {
                _originalMiscReceipt = value;
                _originalMiscReceiptId = value == null ? default(long) : value.Id;
            }
        }
        public Shipment Shipment
        {
            get { return _shipmentId == default(long) ? null : _shipment ?? (_shipment = new Shipment(_shipmentId, false)); }
            set
            {
                _shipment = value;
                _shipmentId = value == null ? default(long) : value.Id;
            }
        }
        public User User
        {
            get { return _userId == default(long) ? null : _user ?? (_user = new User(_userId, false)); }
            set
            {
                _user = value;
                _userId = value == null ? default(long) : value.Id;
            }
        }


        public List<MiscReceiptApplication> MiscReceiptApplications
        {
            get
            {
                if (_miscReceiptApplications == null) LoadMiscReceiptApplications();
                return _miscReceiptApplications;
            }
            set { _miscReceiptApplications = value; }
        }


        public bool IsNew
		{
			get { return Id == default(long); }
		}

		public MiscReceipt()
		{
			
		}

		public MiscReceipt(long id) : this(id, false)
		{
		}

		public MiscReceipt(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

        public MiscReceipt(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}


        public void LoadCollections()
        {
            LoadMiscReceiptApplications();
        }


        private void LoadMiscReceiptApplications()
        {
            _miscReceiptApplications = new List<MiscReceiptApplication>();
            const string query = @"SELECT * FROM MiscReceiptApplication WHERE MiscReceiptId = @MiscReceiptId";
            var parameters = new Dictionary<string, object> { { "MiscReceiptId", Id } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _miscReceiptApplications.Add(new MiscReceiptApplication(reader));
            Connection.Close();
        }
    }
}
