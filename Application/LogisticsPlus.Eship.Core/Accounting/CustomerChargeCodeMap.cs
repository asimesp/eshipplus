﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("CustomerChargeCodeMap", ReadOnly = false, Source = EntitySource.TableView)]
	public class CustomerChargeCodeMap:TenantBase
	{
		private long _chargeCodeId;
		private long _customerId;

		private Customer _customer;
		private ChargeCode _chargeCode;



		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("ExternalCode", Description = "External Code")]
		[Property("ExternalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ExternalCode { get; set; }

		[EnableSnapShot("ExternalDescription", Description = "External Description")]
		[Property("ExternalDescription", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ExternalDescription { get; set; }

		[EnableSnapShot("ChargeCodeId", Description = "Charge Code Reference")]
		[Property("ChargeCodeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ChargeCodeId
		{
			get { return _chargeCodeId; }
			set
			{
				_chargeCodeId = value;
				if (_chargeCode != null && value != _chargeCode.Id) _chargeCode = null;
			}
		}

		[EnableSnapShot("CustomerId", Description = "Customer Reference")]
		[Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long CustomerId
		{
			get
			{
				if (_customer != null) _customerId = _customer.Id;
				return _customerId;
			}
			set
			{
				_customerId = value;
				if (_customer != null && value != _customer.Id) _customer = null;
			}
		}

		public Customer Customer
		{
			get { return _customer ?? (_customer = new Customer(_customerId)); }
			set
			{
				_customer = value;
				_customerId = value == null ? default(long) : value.Id;
			}
		}

		public ChargeCode ChargeCode
		{
			get { return _chargeCode ?? (_chargeCode = new ChargeCode(_chargeCodeId)); }
			set
			{
				_chargeCode = value;
				_chargeCodeId = value == null ? default(long) : value.Id;
			}
		}


		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public CustomerChargeCodeMap()
		{

		}

		public CustomerChargeCodeMap(long id) : this(id, false)
		{
		}

		public CustomerChargeCodeMap(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public CustomerChargeCodeMap(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}


	}
}
