﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("VendorBillDetail", ReadOnly = false, Source = EntitySource.TableView)]
	public class VendorBillDetail : TenantBase
	{
		private long _chargeCodeId;
		private long _accountBucketId;
		private long _vendorBillId;

		private ChargeCode _chargeCode;
		private VendorBill _vendorBill;
		private AccountBucket _accountBucket;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("Quantity")]
		[Property("Quantity", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int Quantity { get; set; }

		[EnableSnapShot("UnitBuy", Description = "Buy Per Quantity Unit")]
		[Property("UnitBuy", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal UnitBuy { get; set; }

		[EnableSnapShot("ReferenceNumber", Description = "Reference Number")]
		[Property("ReferenceNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ReferenceNumber { get; set; }

		[EnableSnapShot("ReferenceType", Description = "Reference Number Type")]
		[Property("ReferenceType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public DetailReferenceType ReferenceType { get; set; }

		[EnableSnapShot("Note")]
		[Property("Note", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Note { get; set; }

		[EnableSnapShot("ChargeCodeId", Description = "Charge Code Reference")]
		[Property("ChargeCodeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ChargeCodeId
		{
			get { return _chargeCodeId; }
			set
			{
				_chargeCodeId = value;
				if (_chargeCode != null && value != _chargeCode.Id) _chargeCode = null;
			}
		}

		[EnableSnapShot("AccountBucketId", Description = "Account Bucket Reference")]
		[Property("AccountBucketId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long AccountBucketId
		{
			get { return _accountBucketId; }
			set
			{
				_accountBucketId = value;
				if (_accountBucket != null && value != _accountBucket.Id) _accountBucket = null;
			}
		}

		[EnableSnapShot("VendorBillId", Description = "Vendor Bill Reference")]
		[Property("VendorBillId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long VendorBillId
		{
			get
			{
				if (_vendorBill != null) _vendorBillId = _vendorBill.Id;
				return _vendorBillId;
			}
			set
			{
				_vendorBillId = value;
				if (_vendorBill != null && value != _vendorBill.Id) _vendorBill = null;
			}
		}


		public decimal TotalAmount
		{
			get { return UnitBuy*(Quantity < 1 ? 1 : Quantity); }
		}

		public ChargeCode ChargeCode
		{
			get { return _chargeCodeId == default(long) ? null : _chargeCode ?? (_chargeCode = new ChargeCode(_chargeCodeId, false)); }
			set
			{
				_chargeCode = value;
				_chargeCodeId = value == null ? default(long) : value.Id;
			}
		}
		public VendorBill VendorBill
		{
			get { return _vendorBill ?? (_vendorBill = new VendorBill(_vendorBillId)); }
			set
			{
				_vendorBill = value;
				_vendorBillId = value == null ? default(long) : value.Id;
			}
		}
		public AccountBucket AccountBucket
		{
			get { return _accountBucketId == default(long) ? null : _accountBucket ?? (_accountBucket = new AccountBucket(_accountBucketId, false)); }
			set
			{
				_accountBucket = value;
				_accountBucketId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public VendorBillDetail() { }

		public VendorBillDetail(long id) : this(id, false) { }

		public VendorBillDetail(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public VendorBillDetail(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
