﻿namespace LogisticsPlus.Eship.Core.Accounting
{
	public enum BrokerReferenceNumberType
	{
        // DO NOT RE-ORDER. SEE APPLICABLE JOINS ON DATABASE REFBROKERREFERENCENUMBERTYPE
		EmployerIdentificationNumber = 0,
		SocialSecurityNumber,
		Foreign
	}
}
