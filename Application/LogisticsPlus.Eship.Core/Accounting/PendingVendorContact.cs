﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("PendingVendorContact", ReadOnly = false, Source = EntitySource.TableView)]
	public class PendingVendorContact : Contact
	{
		private long _pendingVendorLocationId;

		private PendingVendorLocation _pendingVendorLocation;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[Property("PendingVendorLocationId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long PendingVendorLocationId
		{
			get
			{
				if (_pendingVendorLocation != null) _pendingVendorLocationId = _pendingVendorLocation.Id;
				return _pendingVendorLocationId;
			}
			set
			{
				_pendingVendorLocationId = value;
				if (_pendingVendorLocation != null && value != _pendingVendorLocation.Id) _pendingVendorLocation = null;
			}
		}

		public PendingVendorLocation PendingVendorLocation
		{
			get { return _pendingVendorLocation ?? (_pendingVendorLocation = new PendingVendorLocation(_pendingVendorLocationId)); }
			set
			{
				_pendingVendorLocation = value;
				_pendingVendorLocationId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew { get { return Id == default(long); } }

		public PendingVendorContact()
		{

		}

		public PendingVendorContact(long id)
			: this(id, false)
		{
		}

		public PendingVendorContact(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public PendingVendorContact(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
