﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("VendorPackageCustomMapping", ReadOnly = false, Source = EntitySource.TableView)]
	public class VendorPackageCustomMapping : TenantBase
	{
		private long _vendorId;
		private long _packageTypeId;

		private PackageType _packageType;
		private Vendor _vendor;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("VendorCode", Description = "Vendor Code")]
		[Property("VendorCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string VendorCode { get; set; }

		[EnableSnapShot("PackageTypeId", Description = "Package Type Reference")]
		[Property("PackageTypeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long PackageTypeId
		{
			get { return _packageTypeId; }
			set
			{
				_packageTypeId = value;
				if (_packageType != null && value != _packageType.Id) _packageType = null;
			}
		}

		[EnableSnapShot("VendorId", Description = "Vendor Reference")]
		[Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long VendorId
		{
			get
			{
				if (_vendor != null) _vendorId = _vendor.Id;
				return _vendorId;
			}
			set
			{
				_vendorId = value;
				if (_vendor != null && value != _vendor.Id) _vendor = null;
			}
		}

		public Vendor Vendor
		{
			get { return _vendor ?? (_vendor = new Vendor(_vendorId)); }
			set
			{
				_vendor = value;
				_vendorId = value == null ? default(long) : value.Id;
			}
		}
		public PackageType PackageType
		{
			get { return _packageTypeId == default(long) ? null : _packageType ?? (_packageType = new PackageType(_packageTypeId, false)); }
			set
			{
				_packageType = value;
				_packageTypeId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public VendorPackageCustomMapping()
		{
			
		}

		public VendorPackageCustomMapping(long id) : this(id, false)
		{
		}

		public VendorPackageCustomMapping(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public VendorPackageCustomMapping(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
