﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
    [Serializable]
    [Entity("SalesRepCommTier", ReadOnly = false, Source = EntitySource.TableView)]
    public class SalesRepCommTier : TenantBase
    {
        private long _salesRepresentativeId;
        private long _customerId;

        private SalesRepresentative _salesRepresentative;
        private Customer _customer;


        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; protected set; }

        [EnableSnapShot("SalesRepresentativeId", Description = "Sales Representative Reference")]
        [Property("SalesRepresentativeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long SalesRepresentativeId
        {
            get
            {
                if (_salesRepresentative != null) _salesRepresentativeId = _salesRepresentative.Id;
                return _salesRepresentativeId;
            }
            set
            {
                _salesRepresentativeId = value;
                if (_salesRepresentative != null && _salesRepresentative.Id != value) _salesRepresentative = null;
            }
        }

        [EnableSnapShot("CustomerId", Description = "Customer Reference")]
        [Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long CustomerId
        {
            get { return _customerId; }
            set
            {
                _customerId = value;
                if (_customer != null && value != _customer.Id) _customer = null;
            }
        }
        
        [EnableSnapShot("CommissionPercent", Description = "Commission Percent")]
        [Property("CommissionPercent", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal CommissionPercent { get; set; }

        [EnableSnapShot("CeilingValue", Description = "Ceiling Value")]
        [Property("CeilingValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal CeilingValue { get; set; }

        [EnableSnapShot("FloorValue", Description = "Floor Value")]
        [Property("FloorValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal FloorValue { get; set; }

        [EnableSnapShot("EffectiveDate", Description = "Effective Date")]
        [Property("EffectiveDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime EffectiveDate { get; set; }

        [EnableSnapShot("ExpirationDate", Description = "Expiration Date")]
        [Property("ExpirationDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime ExpirationDate { get; set; }

        [EnableSnapShot("ServiceMode", Description = "Service Mode")]
        [Property("ServiceMode", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public ServiceMode ServiceMode { get; set; }


        public SalesRepresentative SalesRepresentative
        {
            get
            {
                return _salesRepresentative ?? (_salesRepresentative = new SalesRepresentative(_salesRepresentativeId, false));
            }
            set
            {
                _salesRepresentative = value;
                _salesRepresentativeId = value == null ? default(long) : value.Id;
            }
        }
        public Customer Customer
        {
            get { return _customerId == default(long) ? null : _customer ?? (_customer = new Customer(_customerId, false)); }
            set
            {
                _customer = value;
                _customerId = value == null ? default(long) : value.Id;
            }
        }

        public bool IsNew
        {
            get { return Id == default(long); }
        }

        public SalesRepCommTier() { }

        public SalesRepCommTier(long id) : this(id, false) { }

        public SalesRepCommTier(long id, bool takeSnapshot)
        {
            Id = id;
            if (Id == default(long)) return;
            KeyLoaded = Load();
            if (takeSnapshot) TakeSnapShot();
        }

        public SalesRepCommTier(DbDataReader reader)
        {
            Load(reader);
        }


        public void Save()
        {
            if (Id == default(long)) Insert();
            else Update();
        }

        public new void Delete()
        {
            if (Id != default(long))
                base.Delete();
        }

    }
}
