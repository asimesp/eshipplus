﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("VendorEquipment", ReadOnly = false, Source = EntitySource.TableView)]
	public class VendorEquipment : TenantBase
	{
		private long _vendorId;
		private long _equipmentTypeId;

		private EquipmentType _equipmentType;
		private Vendor _vendor;

		[EnableSnapShot("EquipmentTypeId", Description = "Equipment Type Reference")]
		[Property("EquipmentTypeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long EquipmentTypeId
		{
			get
			{
				if (_equipmentType != null) _equipmentTypeId = _equipmentType.Id;
				return _equipmentTypeId;
			}
			set
			{
				_equipmentTypeId = value;
				if (_equipmentType != null && value != _equipmentType.Id) _equipmentType = null;
			}
		}

		[EnableSnapShot("VendorId", Description = "Vendor Reference")]
		[Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long VendorId
		{
			get
			{
				if (_vendor != null) _vendorId = _vendor.Id;
				return _vendorId;
			}
			set
			{
				_vendorId = value;
				if (_vendor != null && value != _vendor.Id) _vendor = null;
			}
		}

		public Vendor Vendor
		{
			get { return _vendor ?? (_vendor = new Vendor(_vendorId)); }
			set
			{
				_vendor = value;
				_vendorId = value == null ? default(long) : value.Id;
			}
		}
		public EquipmentType EquipmentType
		{
			get { return _equipmentType ?? (_equipmentType = new EquipmentType(_equipmentTypeId)); }
			set
			{
				_equipmentType = value;
				_equipmentTypeId = value == null ? default(long) : value.Id;
			}
		}

		public VendorEquipment()
		{
		}

		public VendorEquipment(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			Insert();
		}

		public new void Delete()
		{
			base.Delete();
		}
	}
}
