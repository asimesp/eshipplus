﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("VendorBill", ReadOnly = false, Source = EntitySource.TableView)]
	public class VendorBill : TenantBase
	{
		private long _applyToDocumentId;
		private long _vendorLocationId;
		private long _userId;

		private VendorLocation _vendorLocation;
		private VendorBill _applyToDocument;

		private User _user;
		
		private List<VendorBillDetail> _details;
		private List<ServiceTicketDocument> _serviceTicketDocuments;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("DocumentNumber", Description = "Document Number")]
		[Property("DocumentNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DocumentNumber { get; set; }

		[EnableSnapShot("DateCreated", Description = "Date Created")]
		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[EnableSnapShot("DocumentDate", Description = "Document Date")]
		[Property("DocumentDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DocumentDate { get; set; }

		[EnableSnapShot("BillType", Description = "Bill Type")]
		[Property("BillType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public InvoiceType BillType { get; set; }

		[EnableSnapShot("Posted")]
		[Property("Posted", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Posted { get; set; }

		[EnableSnapShot("Exported")]
		[Property("Exported", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Exported { get; set; }

		[EnableSnapShot("PostDate", Description = "Post Date")]
		[Property("PostDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime PostDate { get; set; }

		[EnableSnapShot("ExportDate", Description = "Export Date")]
		[Property("ExportDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime ExportDate { get; set; }

		[EnableSnapShot("ApplyToDocumentId", Description = "Apply To Document Reference")]
		[Property("ApplyToDocumentId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ApplyToDocumentId
		{
			get
			{
				if (_applyToDocument != null) _applyToDocumentId = _applyToDocument.Id;
				return _applyToDocumentId;
			}
			set
			{
				_applyToDocumentId = value;
				if (_applyToDocument != null && value != _applyToDocument.Id) _applyToDocument = null;
			}
		}

		[EnableSnapShot("VendorLocationId", Description = "Vendor Location Reference")]
		[Property("VendorLocationId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long VendorLocationId
		{
			get
			{
				if (_vendorLocation != null) _vendorLocationId = _vendorLocation.Id;
				return _vendorLocationId;
			}
			set
			{
				_vendorLocationId = value;
				if (_vendorLocation != null && value != _vendorLocation.Id) _vendorLocation = null;
			}
		}

		[EnableSnapShot("UserId", Description = "User Reference")]
		[Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long UserId
		{
			get { return _userId; }
			set
			{
				_userId = value;
				if (_user != null && value != _user.Id) _user = null;
			}
		}

		public VendorBill ApplyToDocument
		{
			get
			{
				return _applyToDocumentId == default(long)
				       	? null
				       	: _applyToDocument ?? (_applyToDocument = new VendorBill(ApplyToDocumentId));
			}
			set
			{
				_applyToDocument = value;
				_applyToDocumentId = value == null ? default(long) : value.Id;
			}
		}
		public VendorLocation VendorLocation
		{
			get
			{
				return _vendorLocationId == default(long) ? null : _vendorLocation ?? (_vendorLocation = new VendorLocation(_vendorLocationId));
			}
			set
			{
				_vendorLocation = value;
				_vendorLocationId = value == null ? default(long) : value.Id;
			}
		}
		public User User
		{
			get { return _userId == default(long) ? null : _user ?? (_user = new User(_userId, false)); }
			set
			{
				_user = value;
				_userId = value == null ? default(long) : value.Id;
			}
		}

		public List<VendorBillDetail> Details
		{
			get
			{
				if (_details == null) LoadDetails();
				return _details;
			}
			set { _details = value; }
		}

		public List<ServiceTicketDocument> ServiceTicketDocuments
		{
			get
			{
				if (_serviceTicketDocuments == null) LoadServiceTicketDocuments();
				return _serviceTicketDocuments;
			}
			set { _serviceTicketDocuments = value; }
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public VendorBill() {}

		public VendorBill(long id)
			: this(id, false){}

		public VendorBill(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public VendorBill(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

		public void LoadCollections()
		{
			LoadDetails();
			LoadServiceTicketDocuments();
		}

		private void LoadDetails()
		{
			_details = new List<VendorBillDetail>();
			const string query = @"SELECT * FROM VendorBillDetail WHERE VendorBillId = @VendorBillId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "VendorBillId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_details.Add(new VendorBillDetail(reader));
			Connection.Close();
		}

		private void LoadServiceTicketDocuments()
		{
			_serviceTicketDocuments = new List<ServiceTicketDocument>();
			if (IsNew) return;
			const string query = @"SELECT * FROM ServiceTicketDocument WHERE ServiceTicketId = @ServiceTicketId AND TenantId = @TenantId;";
			var parameters = new Dictionary<string, object> { { "ServiceTicketId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_serviceTicketDocuments.Add(new ServiceTicketDocument(reader));
			Connection.Close();
		}
	}
}
