﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("SalesRepresentative", ReadOnly = false, Source = EntitySource.TableView)]
	public class SalesRepresentative : Location
	{
        private List<SalesRepCommTier> _salesRepCommTiers;


		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("Name")]
		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

		[EnableSnapShot("Phone")]
		[Property("Phone", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Phone { get; set; }

		[EnableSnapShot("Mobile")]
		[Property("Mobile", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Mobile { get; set; }

		[EnableSnapShot("Fax")]
		[Property("Fax", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Fax { get; set; }

		[EnableSnapShot("Email")]
		[Property("Email", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Email { get; set; }

		[EnableSnapShot("SalesRepresentativeNumber", Description = "Sales Representative Number")]
		[Property("SalesRepresentativeNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string SalesRepresentativeNumber { get; set; }

		[EnableSnapShot("CompanyName", Description = "Sales Representative Company Name")]
		[Property("CompanyName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CompanyName { get; set; }

		[EnableSnapShot("DateCreated", Description = "Date Created")]
		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[EnableSnapShot("AdditionalEntityName", Description = "Additional Entity Name")]
		[Property("AdditionalEntityName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string AdditionalEntityName { get; set; }

		[EnableSnapShot("AdditionalEntityCommPercent", Description = "Additional Entity Commission Profit Percentage")]
		[Property("AdditionalEntityCommPercent", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal AdditionalEntityCommPercent { get; set; }


        public List<SalesRepCommTier> SalesRepCommTiers
        {
            get
            {
                if (_salesRepCommTiers == null) LoadSalesRepCommTiers();
                return _salesRepCommTiers;
            }
            set { _salesRepCommTiers = value; }
        }


		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public SalesRepresentative() { }

		public SalesRepresentative(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public SalesRepresentative(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long)) base.Delete();
		}


        public void LoadCollections()
        {
            LoadSalesRepCommTiers();
        }

        private void LoadSalesRepCommTiers()
        {
            _salesRepCommTiers = new List<SalesRepCommTier>();
            if (IsNew) return;
            const string query = @"SELECT * FROM SalesRepCommTier WHERE SalesRepresentativeId = @SalesRepresentativeId AND TenantId = @TenantId;";
            var parameters = new Dictionary<string, object> { { "SalesRepresentativeId", Id }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _salesRepCommTiers.Add(new SalesRepCommTier(reader));
            Connection.Close();
        }
	}
}
