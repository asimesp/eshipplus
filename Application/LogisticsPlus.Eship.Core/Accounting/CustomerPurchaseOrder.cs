﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("CustomerPurchaseOrder", ReadOnly = false, Source = EntitySource.TableView)]
	public class CustomerPurchaseOrder : TenantBase
	{
		private long _customerId;
	    private long _countryId;

	    private Country _country;
		private Customer _customer;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("PurchaseOrderNumber", Description = "Purchase Order Number")]
		[Property("PurchaseOrderNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key=false)]
		public string PurchaseOrderNumber { get; set; }

		[EnableSnapShot("ValidPostalCode", Description = "Purchase Order Postal Code")]
		[Property("ValidPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ValidPostalCode { get; set; }

		[EnableSnapShot("ApplyOnOrigin", Description = "Applies to Origin Postal Code")]
		[Property("ApplyOnOrigin", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ApplyOnOrigin { get; set; }

		[EnableSnapShot("ApplyOnDestination", Description = "Applies to Destination Postal Code")]
		[Property("ApplyOnDestination", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ApplyOnDestination { get; set; }

		[EnableSnapShot("MaximumUses", Description = "Purchase Order Maximum Uses")]
		[Property("MaximumUses", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int MaximumUses { get; set; }

		[EnableSnapShot("CurrentUses", Description = "Purchase Order Current Uses")]
		[Property("CurrentUses", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int CurrentUses { get; set; }

		[EnableSnapShot("ExpirationDate", Description = "Purchase Order Expiration Date")]
		[Property("ExpirationDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime ExpirationDate { get; set; }

		[EnableSnapShot("AdditionalPurchaseOrderNotes", Description = "Additional Purchase Order Notes")]
		[Property("AdditionalPurchaseOrderNotes", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string AdditionalPurchaseOrderNotes { get; set; }

        [EnableSnapShot("CountryId", Description = "Purchase Order country")]
        [Property("CountryId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long CountryId
        {
            get { return _countryId; }
            set
            {
                _countryId = value;
                if (_country != null && value != _country.Id) _country = null;
            }
        }

		[EnableSnapShot("CustomerId", Description = "Customer Reference")]
		[Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long CustomerId
		{
			get
			{
				if (_customer != null) _customerId = _customer.Id;
				return _customerId;
			}
			set
			{
				_customerId = value;
				if (_customer != null && value != _customer.Id) _customer = null;
			}
		}

		public Customer Customer
		{
			get { return _customer ?? (_customer = new Customer(_customerId)); }
			set
			{
				_customer = value;
				_customerId = value == null ? default(long) : value.Id;
			}
		}

        public Country Country
        {
            get { return _countryId == default(long) ? null : _country ?? (_country = new Country(_countryId)); }
            set
            {
                _country = value;
                _countryId = value == null ? default(long) : value.Id;
            }
        }

        public bool IsNew
		{
			get { return Id == default(long); }
		}

		public CustomerPurchaseOrder() { }
        
		public CustomerPurchaseOrder(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public CustomerPurchaseOrder(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			base.Delete();
		}
	}
}
