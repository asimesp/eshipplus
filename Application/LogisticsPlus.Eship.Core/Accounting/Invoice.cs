﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("Invoice", ReadOnly = false, Source = EntitySource.TableView)]
	public class Invoice : TenantBase
	{
		private long _prefixId;
		private long _customerLocationId;
		private long _userId;
		private long _originalInvoiceId;

		private CustomerLocation _customerLocation;
		private User _user;
		private Invoice _originalInvoice;
		private Prefix _prefix;

		private List<InvoiceDetail> _details;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("InvoiceNumber", Description = "Invoice Number")]
		[Property("InvoiceNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string InvoiceNumber { get; set; }

		[EnableSnapShot("PrefixId", Description = "Prefix Reference")]
		[Property("PrefixId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long PrefixId
		{
			get { return _prefixId; }
			set
			{
				_prefixId = value;
				if (_prefix != null && value != _prefix.Id) _prefix = null;
			}
		}

		[EnableSnapShot("HidePrefix", Description = "Hide Prefix")]
		[Property("HidePrefix", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool HidePrefix { get; set; }

		[EnableSnapShot("InvoiceDate", Description = "Invoice Date")]
		[Property("InvoiceDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime InvoiceDate { get; set; }

		[EnableSnapShot("DueDate", Description = "Due Date")]
		[Property("DueDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DueDate { get; set; }

		[EnableSnapShot("SpecialInstruction", Description = "Special Instructions")]
		[Property("SpecialInstruction", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string SpecialInstruction { get; set; }

		[EnableSnapShot("CustomerControlAccountNumber", Description = "Customer Control Account Number")]
		[Property("CustomerControlAccountNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomerControlAccountNumber { get; set; }

		[EnableSnapShot("InvoiceType", Description = "Invoice Type")]
		[Property("InvoiceType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public InvoiceType InvoiceType { get; set; }

		[EnableSnapShot("Posted")]
		[Property("Posted", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Posted { get; set; }

		[EnableSnapShot("Exported")]
		[Property("Exported", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Exported { get; set; }

		[EnableSnapShot("PostDate", Description = "Post Date")]
		[Property("PostDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime PostDate { get; set; }

		[EnableSnapShot("ExportDate", Description = "Export Date")]
		[Property("ExportDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime ExportDate { get; set; }

		[EnableSnapShot("PaidAmount", Description = "Amount Paid on Invoice")]
		[Property("PaidAmount", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal PaidAmount { get; set; }

		[EnableSnapShot("OriginalInvoiceId", Description = "Original Invoice Reference")]
		[Property("OriginalInvoiceId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long OriginalInvoiceId
		{
			get
			{
				if (_originalInvoice != null) _originalInvoiceId = _originalInvoice.Id;
				return _originalInvoiceId;
			}
			set
			{
				_originalInvoiceId = value;
				if (_originalInvoice != null && value != _originalInvoice.Id) _originalInvoice = null;
			}
		}

		[EnableSnapShot("FtpDelivered", Description = "Ftp Delivered")]
		[Property("FtpDelivered", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool FtpDelivered { get; set; }

		[EnableSnapShot("UserId", Description = "User Reference")]
		[Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long UserId
		{
			get { return _userId; }
			set
			{
				_userId = value;
				if (_user != null && value != _user.Id) _user = null;
			}
		}

		[EnableSnapShot("CustomerLocationId", Description = "Customer Location Reference")]
		[Property("CustomerLocationId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		private long CustomerLocationId
		{
			get { return _customerLocationId; }
			set
			{
				_customerLocationId = value;
				if (_customerLocation != null && value != _customerLocation.Id) _customerLocation = null;
			}
		}

		public Prefix Prefix
		{
			get { return _prefixId == default(long) ? null : _prefix ?? (_prefix = new Prefix(_prefixId, false)); }
			set
			{
				_prefix = value;
				_prefixId = value == null ? default(long) : value.Id;
			}
		}
		public CustomerLocation CustomerLocation
		{
			get
			{
				return CustomerLocationId == default(long) ? null : _customerLocation ?? (_customerLocation = new CustomerLocation(CustomerLocationId));
			}
			set
			{
				_customerLocation = value;
				CustomerLocationId = value == null ? default(long) : value.Id;
			}
		}
		public User User
		{
			get { return _userId == default(long) ? null : _user ?? (_user = new User(_userId, false)); }
			set
			{
				_user = value;
				_userId = value == null ? default(long) : value.Id;
			}
		}
		public Invoice OriginalInvoice
		{
			get
			{
				return _originalInvoiceId == default(long)
				       	? null
				       	: _originalInvoice ?? (_originalInvoice = new Invoice(_originalInvoiceId));
			}
			set
			{
				_originalInvoice = value;
				_originalInvoiceId = value == null ? default(long) : value.Id;
			}
		}

		public List<InvoiceDetail> Details
		{
			get
			{
				if (_details == null) LoadDetails();
				return _details;
			}
			set { _details = value; }
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public Invoice() {}

		public Invoice(long id)
			: this(id, false){}

		public Invoice(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public Invoice(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

		public void LoadCollections()
		{
			LoadDetails();
		}

		public List<ServiceTicket> RetrieveAssociatedServiceTickets()
		{
			var tickets = new List<ServiceTicket>();

			const string query =
				@"Select distinct
					s.*
				From
					Invoice as i
					inner join InvoiceDetail as d on d.InvoiceId = i.Id
					inner join ServiceTicket as s on d.ReferenceNumber = s.ServiceTicketNumber and d.ReferenceType = @RefType
				Where
					i.Id = @Id 
					and i.TenantId = @TenantId";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"Id", Id},
			                 		{"TenantId", TenantId},
			                 		{"RefType", DetailReferenceType.ServiceTicket}
			                 	};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					tickets.Add(new ServiceTicket(reader));
			Connection.Close();

			return tickets;
		}

		public List<Shipment> RetrieveAssociatedShipments()
		{
			var shipments = new List<Shipment>();

			const string query =
				@"Select distinct
					s.*
				From
					Invoice as i
					inner join InvoiceDetail as d on d.InvoiceId = i.Id
					inner join Shipment as s on d.ReferenceNumber = s.ShipmentNumber and d.ReferenceType = @RefType
				Where
					i.Id = @Id
					and i.TenantId = @TenantId";
			var parameters = new Dictionary<string, object>
			                 	{
			                 		{"Id", Id},
			                 		{"TenantId", TenantId},
			                 		{"RefType", DetailReferenceType.Shipment}
			                 	};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					shipments.Add(new Shipment(reader));
			Connection.Close();

			return shipments;
		}

		public List<InvoicePrintLog> RetrieveInvoicePrintLogs()
		{
			var logs = new List<InvoicePrintLog>();

			const string query =@"Select * From InvoicePrintLog Where InvoiceId = @Id and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> {{"Id", Id}, {"TenantId", TenantId}};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					logs.Add(new InvoicePrintLog(reader));
			Connection.Close();

			return logs;
		}

		private void LoadDetails()
		{
			_details = new List<InvoiceDetail>();
			const string query = @"SELECT * FROM InvoiceDetail WHERE InvoiceId = @InvoiceId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "InvoiceId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_details.Add(new InvoiceDetail(reader));
			Connection.Close();
		}
	}
}
