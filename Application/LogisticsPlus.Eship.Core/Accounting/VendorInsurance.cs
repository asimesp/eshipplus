﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("VendorInsurance", ReadOnly = false, Source = EntitySource.TableView)]
	public class VendorInsurance : TenantBase
	{
		private long _vendorId;
		private long _insuranceTypeId;

		private InsuranceType _insuranceType;
		private Vendor _vendor;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("CarrierName", Description = "Insurance Carrier Name")]
		[Property("CarrierName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CarrierName { get; set; }

		[EnableSnapShot("Required")]
		[Property("Required", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Required { get; set; }

		[EnableSnapShot("PolicyNumber", Description = "Policy Number")]
		[Property("PolicyNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string PolicyNumber { get; set; }

		[EnableSnapShot("EffectiveDate", Description = "Policy Effective Date")]
		[Property("EffectiveDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime EffectiveDate { get; set; }

		[EnableSnapShot("ExpirationDate", Description = "Policy Expiration Date")]
		[Property("ExpirationDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime ExpirationDate { get; set; }

		[EnableSnapShot("CertificateHolder", Description = "Certificate Holder")]
		[Property("CertificateHolder", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CertificateHolder { get; set; }

		[EnableSnapShot("CoverageAmount", Description = "Policy Coverage Amount")]
		[Property("CoverageAmount", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal CoverageAmount { get; set; }

		[EnableSnapShot("SpecialInstruction", Description = "Special Instructions")]
		[Property("SpecialInstruction", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string SpecialInstruction { get; set; }

		[EnableSnapShot("InsuranceTypeId", Description = "Equipment Type Reference")]
		[Property("InsuranceTypeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long InsuranceTypeId
		{
			get { return _insuranceTypeId; }
			set
			{
				_insuranceTypeId = value;
				if (_insuranceType != null && value != _insuranceType.Id) _insuranceType = null;
			}
		}

		[EnableSnapShot("VendorId", Description = "Vendor Reference")]
		[Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long VendorId
		{
			get
			{
				if (_vendor != null) _vendorId = _vendor.Id;
				return _vendorId;
			}
			set
			{
				_vendorId = value;
				if (_vendor != null && value != _vendor.Id) _vendor = null;
			}
		}

		public Vendor Vendor
		{
			get { return _vendor ?? (_vendor = new Vendor(_vendorId)); }
			set
			{
				_vendor = value;
				_vendorId = value == null ? default(long) : value.Id;
			}
		}
		public InsuranceType InsuranceType
		{
			get { return _insuranceTypeId == default(long) ? null : _insuranceType ?? (_insuranceType = new InsuranceType(_insuranceTypeId, false)); }
			set
			{
				_insuranceType = value;
				_insuranceTypeId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public VendorInsurance()
		{
			
		}

		public VendorInsurance(long id) : this(id, false)
		{
		}

		public VendorInsurance(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public VendorInsurance(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
