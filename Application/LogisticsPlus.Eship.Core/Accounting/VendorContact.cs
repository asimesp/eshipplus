﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("VendorContact", ReadOnly = false, Source = EntitySource.TableView)]
	public class VendorContact : Contact
	{
		private long _vendorLocationId;

		private VendorLocation _vendorLocation;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key =  true)]
		public long Id { get; private set; }

		[Property("VendorLocationId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long VendorLocationId
		{
			get
			{
				if (_vendorLocation != null) _vendorLocationId = _vendorLocation.Id;
				return _vendorLocationId;
			}
			set
			{
				_vendorLocationId = value;
				if (_vendorLocation != null && value != _vendorLocation.Id) _vendorLocation = null;
			}
		}

		public VendorLocation VendorLocation
		{
			get { return _vendorLocation ?? (_vendorLocation = new VendorLocation(_vendorLocationId)); }
			set
			{
				_vendorLocation = value;
				_vendorLocationId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew { get { return Id == default(long); } }

		public VendorContact()
		{
			
		}

		public VendorContact(long id) : this(id, false)
		{
		}

		public VendorContact(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public VendorContact(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
