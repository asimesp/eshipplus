﻿using System;
using System.Data;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("Charge", ReadOnly = false, Source = EntitySource.TableView)]
	public class Charge : TenantBase
	{
		private long _chargeCodeId;
		

		private ChargeCode _chargeCode;

		[EnableSnapShot("Quantity")]
		[Property("Quantity", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int Quantity { get; set; }

		[EnableSnapShot("UnitBuy", Description = "Buy Per Quantity Unit")]
		[Property("UnitBuy", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal UnitBuy { get; set; }

		[EnableSnapShot("UnitSell", Description = "Sell Per Quantity Unit")]
		[Property("UnitSell", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal UnitSell { get; set; }

		[EnableSnapShot("UnitDiscount", Description = "Discount Per Quantity Unit")]
		[Property("UnitDiscount", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal UnitDiscount { get; set; }

		[EnableSnapShot("Comment")]
		[Property("Comment", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Comment { get; set; }

		[EnableSnapShot("ChargeCodeId", Description = "Charge Code Reference")]
		[Property("ChargeCodeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ChargeCodeId
		{
			get { return _chargeCodeId; }
			set
			{
				_chargeCodeId = value;
				if (_chargeCode != null && value != _chargeCode.Id) _chargeCode = null;
			}
		}

		public decimal FinalBuy { get { return UnitBuy * (Quantity < 1 ? 1 : Quantity); } }

		public decimal FinalSell { get { return UnitSell * (Quantity < 1 ? 1 : Quantity); } }

		public decimal AmountDue { get { return (UnitSell - UnitDiscount) * (Quantity < 1 ? 1 : Quantity); } }

		public decimal Profit { get { return AmountDue - FinalBuy; } }

		public ChargeCode ChargeCode
		{
			get { return _chargeCodeId == default(long) ? null : _chargeCode ?? (_chargeCode = new ChargeCode(_chargeCodeId, false)); }
			set
			{
				_chargeCode = value;
				_chargeCodeId = value == null ? default(long) : value.Id;
			}
		}
	}
}
