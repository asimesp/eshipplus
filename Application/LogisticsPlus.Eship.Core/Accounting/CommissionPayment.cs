﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("CommissionPayment", ReadOnly = false, Source = EntitySource.TableView)]
	public class CommissionPayment : TenantBase
	{
		private SalesRepresentative _salesRepresentative;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("SalesRepresentativeId", Description = "Sales Representative Reference")]
		[Property("SalesRepresentativeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long SalesRepresentativeId { get; set; }
		
		[EnableSnapShot("ReferenceNumber", Description = "Reference Number")]
		[Property("ReferenceNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ReferenceNumber { get; set; }

		[EnableSnapShot("Type", Description = "Commission Payment Type")]
		[Property("Type", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public CommissionPaymentType Type { get; set; }

		[EnableSnapShot("DateCreated", Description = "Date Created")]
		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[EnableSnapShot("PaymentDate", Description = "Payment Date")]
		[Property("PaymentDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime PaymentDate { get; set; }

		[EnableSnapShot("AmountPaid", Description = "Amount Paid")]
		[Property("AmountPaid", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal AmountPaid { get; set; }

        [EnableSnapShot("AdditionalEntityCommission", Description = "Additional Entity Commission")]
        [Property("AdditionalEntityCommission", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool AdditionalEntityCommission { get; set; }

		public SalesRepresentative SalesRepresentative
		{
			get
			{
				return SalesRepresentativeId == default(long)
						? null
						: _salesRepresentative ?? (_salesRepresentative = new SalesRepresentative(SalesRepresentativeId, false));
			}
			set
			{
				_salesRepresentative = value;
				SalesRepresentativeId = value == null ? default(long) : value.Id;
			}
		}


		public bool IsNew
		{
			get { return Id == default(long); }
		}

	
		public CommissionPayment() { }

		public CommissionPayment(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public CommissionPayment(DbDataReader reader)
		{
			Load(reader);
		}

		
		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long)) base.Delete();
		}

		public new void TakeSnapShot()
		{
			GetSnapShot();
		}
	}
}
