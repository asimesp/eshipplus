﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("PendingVendor", ReadOnly = false, Source = EntitySource.TableView)]
	public class PendingVendor : TenantBase
	{
        private long _createdByUserId;

        private User _createdByUser;

		private List<PendingVendorInsurance> _insurances;
		private List<PendingVendorLocation> _locations;
		private List<PendingVendorService> _services;
		private List<PendingVendorEquipment> _equipments;
		private List<PendingVendorDocument> _documents;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("VendorNumber", Description = "Vendor Number")]
		[Property("VendorNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string VendorNumber { get; set; }

		[EnableSnapShot("Name")]
		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

		[EnableSnapShot("Notation", Description = "Special Notes")]
		[Property("Notation", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Notation { get; set; }

		[EnableSnapShot("Approved", Description = "Approval Status")]
		[Property("Approved", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Approved { get; set; }

		[EnableSnapShot("Rejected", Description = "Rejection Status")]
		[Property("Rejected", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Rejected { get; set; }

		[EnableSnapShot("DateCreated", Description = "Date Created")]
		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[EnableSnapShot("TSACertified", Description = "TSA Certified")]
		[Property("TSACertified", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool TSACertified { get; set; }

		[EnableSnapShot("CTPATCertified", Description = "CTPAT Certified")]
		[Property("CTPATCertified", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool CTPATCertified { get; set; }

		[EnableSnapShot("SmartWayCertified", Description = "Greenway Certified")]
		[Property("SmartWayCertified", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool SmartWayCertified { get; set; }

		[EnableSnapShot("PIPCertified", Description = "PIP Certified")]
		[Property("PIPCertified", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool PIPCertified { get; set; }

		[EnableSnapShot("CTPATNumber", Description = "CTPAT Number")]
		[Property("CTPATNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CTPATNumber { get; set; }

		[EnableSnapShot("PIPNumber", Description = "PIP Number")]
		[Property("PIPNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string PIPNumber { get; set; }

		[EnableSnapShot("Scac", Description = "SCAC")]
		[Property("Scac", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Scac { get; set; }

		[EnableSnapShot("FederalIDNumber", Description = "Federal ID Number")]
		[Property("FederalIDNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string FederalIDNumber { get; set; }

		[EnableSnapShot("BrokerReferenceNumber", Description = "Broker Reference Number")]
		[Property("BrokerReferenceNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string BrokerReferenceNumber { get; set; }

		[EnableSnapShot("BrokerReferenceNumberType", Description = "Broker Reference Number Type")]
		[Property("BrokerReferenceNumberType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public BrokerReferenceNumberType BrokerReferenceNumberType { get; set; }

		[EnableSnapShot("MC", Description = "MC Number")]
		[Property("MC", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string MC { get; set; }

		[EnableSnapShot("DOT", Description = "DOT Number")]
		[Property("DOT", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DOT { get; set; }

		[EnableSnapShot("TrackingUrl", Description = "Website Tracking Url")]
		[Property("TrackingUrl", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string TrackingUrl { get; set; }

		[EnableSnapShot("HandlesLessThanTruckload", Description = "Can Handle Less than Truckload")]
		[Property("HandlesLessThanTruckload", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool HandlesLessThanTruckload { get; set; }

		[EnableSnapShot("HandlesTruckload", Description = "Can Handle Truckload")]
		[Property("HandlesTruckload", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool HandlesTruckload { get; set; }

		[EnableSnapShot("HandlesAir", Description = "Can Handle Air")]
		[Property("HandlesAir", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool HandlesAir { get; set; }

		[EnableSnapShot("HandlesPartialTruckload", Description = "Can Handle Partial Truckload")]
		[Property("HandlesPartialTruckload", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool HandlesPartialTruckload { get; set; }

		[EnableSnapShot("HandlesRail", Description = "Can Handle Rail")]
		[Property("HandlesRail", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool HandlesRail { get; set; }

		[EnableSnapShot("HandlesSmallPack", Description = "Can Handle Small Package")]
		[Property("HandlesSmallPack", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool HandlesSmallPack { get; set; }

		[EnableSnapShot("IsCarrier", Description = "Is a Carrier")]
		[Property("IsCarrier", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool IsCarrier { get; set; }

		[EnableSnapShot("IsAgent", Description = "Is an Agent")]
		[Property("IsAgent", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool IsAgent { get; set; }

		[EnableSnapShot("IsBroker", Description = "Is a Broker")]
		[Property("IsBroker", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool IsBroker { get; set; }

		[EnableSnapShot("Notes", Description = "Quick Reference Notes")]
		[Property("Notes", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Notes { get; set; }

        [EnableSnapShot("CreatedByUserId", Description = "Created By User Reference")]
        [Property("CreatedByUserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long CreatedByUserId
        {
            get { return _createdByUserId; }
            set
            {
                _createdByUserId = value;
                if (_createdByUser != null && value != _createdByUser.Id) _createdByUser = null;
            }
        }


        public User CreatedByUser
        {
            get { return _createdByUserId == default(long) ? null : _createdByUser ?? (_createdByUser = new User(_createdByUserId, false)); }
            set
            {
                _createdByUser = value;
                _createdByUserId = value == null ? default(long) : value.Id;
            }
        }


		public List<PendingVendorLocation> Locations
		{
			get
			{
				if (_locations == null) LoadLocations();
				return _locations;
			}
			set { _locations = value; }
		}
		public List<PendingVendorService> Services
		{
			get
			{
				if (_services == null) LoadServices();
				return _services;
			}
			set { _services = value; }
		}
		public List<PendingVendorEquipment> Equipments
		{
			get
			{
				if (_equipments == null) LoadEquipments();
				return _equipments;
			}
			set { _equipments = value; }
		}
		public List<PendingVendorInsurance> Insurances
		{
			get
			{
				if (_insurances == null) LoadInsurance();
				return _insurances;
			}
			set { _insurances = value; }
		}
		public List<PendingVendorDocument> Documents
		{
			get
			{
				if (_documents == null) LoadDocuments();
				return _documents;
			}
			set { _documents = value; }
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public PendingVendor()
		{
			
		}

		public PendingVendor(long id) : this(id, false)
		{
		}

		public PendingVendor(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public PendingVendor(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}


		public void LoadCollections()
		{
			LoadInsurance();
			LoadLocations();
			LoadServices();
			LoadEquipments();
			LoadDocuments();
		}

		
		public List<EquipmentType> RetrieveEquipmentTypes()
		{
			var equipmentTypes = new List<EquipmentType>();
			const string query = @"select EquipmentType.* from EquipmentType, PendingVendorEquipment where 
				PendingVendorEquipment.EquipmentTypeId = EquipmentType.Id and PendingVendorEquipment.PendingVendorId = @PendingVendorId
				and PendingVendorEquipment.TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "TenantId", TenantId }, { "PendingVendorId", Id } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					equipmentTypes.Add(new EquipmentType(reader));
			Connection.Close();
			return equipmentTypes;
		}


		private void LoadInsurance()
		{
			_insurances = new List<PendingVendorInsurance>();
			const string query = "Select * from PendingVendorInsurance where PendingVendorId = @PendingVendorId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "PendingVendorId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_insurances.Add(new PendingVendorInsurance(reader));
			Connection.Close();
		}

		private void LoadLocations()
		{
			_locations = new List<PendingVendorLocation>();
			const string query = "Select * from PendingVendorLocation where PendingVendorId = @PendingVendorId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "PendingVendorId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_locations.Add(new PendingVendorLocation(reader));
			Connection.Close();
		}

		private void LoadServices()
		{
			_services = new List<PendingVendorService>();
			const string query = "Select * from PendingVendorService where PendingVendorId = @PendingVendorId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "PendingVendorId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_services.Add(new PendingVendorService(reader));
			Connection.Close();
		}

		private void LoadEquipments()
		{
			_equipments = new List<PendingVendorEquipment>();
			const string query = "Select * from PendingVendorEquipment where PendingVendorId = @PendingVendorId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "PendingVendorId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_equipments.Add(new PendingVendorEquipment(reader));
			Connection.Close();
		}

		private void LoadDocuments()
		{
			_documents = new List<PendingVendorDocument>();
			const string query = @"SELECT * FROM PendingVendorDocument WHERE PendingVendorId = @PendingVendorId AND TenantId = @TenantId;";
			var parameters = new Dictionary<string, object> { { "PendingVendorId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_documents.Add(new PendingVendorDocument(reader));
			Connection.Close();
		}
	}
}
