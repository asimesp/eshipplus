﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("PendingVendorDocument", ReadOnly = false, Source = EntitySource.TableView)]
	public class PendingVendorDocument : Document
	{
		private long _pendingVendorId;

		private PendingVendor _pendingVendor;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("IsInternal", Description = "Is Internal")]
		[Property("IsInternal", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool IsInternal { get; set; }

		[Property("PendingVendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long PendingVendorId
		{
			get
			{
				if (_pendingVendor != null) _pendingVendorId = _pendingVendor.Id;
				return _pendingVendorId;
			}
			set
			{
				_pendingVendorId = value;
				if (_pendingVendor != null && value != _pendingVendor.Id) _pendingVendor = null;
			}
		}

		public PendingVendor PendingVendor
		{
			get { return _pendingVendor ?? (_pendingVendor = new PendingVendor(_pendingVendorId)); }
			set
			{
				_pendingVendor = value;
				_pendingVendorId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public PendingVendorDocument() { }

		public PendingVendorDocument(long id) : this(id, false) { }

		public PendingVendorDocument(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public PendingVendorDocument(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
