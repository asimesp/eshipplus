﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
    [Serializable]
    [Entity("MiscReceiptApplication", ReadOnly = false, Source = EntitySource.TableView)]
    public class MiscReceiptApplication : TenantBase
    {
        private long _invoiceId;
        private long _miscReceiptId;

        private Invoice _invoice;
        private MiscReceipt _miscReceipt;

        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; private set; }

        [EnableSnapShot("MiscReceiptId", Description = "Misc Receipt Reference")]
        [Property("MiscReceiptId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long MiscReceiptId
        {
            get
            {
                if (_miscReceipt != null) _miscReceiptId = _miscReceipt.Id;
                return _miscReceiptId;
            }
            set
            {
                _miscReceiptId = value;
                if (_miscReceipt != null && value != _miscReceipt.Id) _miscReceipt = null;
            }
        }

        [EnableSnapShot("InvoiceId", Description = "Invoice Reference")]
        [Property("InvoiceId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long InvoiceId
        {
            get { return _invoiceId; }
            set
            {
                _invoiceId = value;
                if (_invoice != null && value != _invoice.Id) _invoice = null;
            }
        }

        [EnableSnapShot("ApplicationDate")]
        [Property("ApplicationDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime ApplicationDate { get; set; }

        [EnableSnapShot("Amount")]
        [Property("Amount", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal Amount { get; set; }


        public Invoice Invoice
        {
            get { return _invoiceId == default(long) ? null : _invoice ?? (_invoice = new Invoice(_invoiceId, false)); }
            set
            {
                _invoice = value;
                _invoiceId = value == null ? default(long) : value.Id;
            }
        }
        public MiscReceipt MiscReceipt
        {
            get { return _miscReceipt ?? (_miscReceipt = new MiscReceipt(_miscReceiptId)); }
            set
            {
                _miscReceipt = value;
                _miscReceiptId = value == null ? default(long) : value.Id;
            }
        }


        public bool IsNew
		{
			get { return Id == default(long); }
		}

        public MiscReceiptApplication()
		{
			
		}

		public MiscReceiptApplication(long id) : this(id, false)
		{
		}

		public MiscReceiptApplication(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

        public MiscReceiptApplication(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
    }
}
