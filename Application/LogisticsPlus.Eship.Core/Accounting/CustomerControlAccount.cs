﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("CustomerControlAccount", ReadOnly = false, Source = EntitySource.TableView)]
	public class CustomerControlAccount : Location
	{
		private long _customerId;

		private Customer _customer;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("AccountNumber", Description = "Customer Account Number")]
		[Property("AccountNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string AccountNumber { get; set; }

		[EnableSnapShot("GenericCategory", Description = "Generic Customer Category")]
		[Property("GenericCategory", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string GenericCategory { get; set; }

		[EnableSnapShot("Description")]
		[Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Description { get; set; }

		[EnableSnapShot("CustomerId", Description = "Customer Reference")]
		[Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long CustomerId
		{
			get
			{
				if (_customer != null) _customerId = _customer.Id;
				return _customerId;
			}
			set
			{
				_customerId = value;
				if (_customer != null && value != _customer.Id) _customer = null;
			}
		}

		public Customer Customer
		{
			get { return _customer ?? (_customer = new Customer(_customerId)); }
			set
			{
				_customer = value;
				_customerId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public CustomerControlAccount() { }

		public CustomerControlAccount(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public CustomerControlAccount(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			base.Delete();
		}
	}
}
