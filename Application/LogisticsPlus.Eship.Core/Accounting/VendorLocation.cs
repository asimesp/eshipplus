﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("VendorLocation", ReadOnly = false, Source = EntitySource.TableView)]
	public class VendorLocation : Location
	{
		private long _vendorId;

		private Vendor _vendor;

		private List<VendorContact> _contacts;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("LocationNumber", Description = "Location Reference Number")]
		[Property("LocationNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LocationNumber { get; set; }

		[EnableSnapShot("RemitToLocation", Description = "Is a Remit To Location")]
		[Property("RemitToLocation", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool RemitToLocation { get; set; }

        [EnableSnapShot("MainRemitToLocation", Description = "Is Main Remit To Location")]
        [Property("MainRemitToLocation", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool MainRemitToLocation { get; set; }

		[EnableSnapShot("Primary", Description = "Location Primary Designation")]
		[Property("Primary", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Primary { get; set; }

		[EnableSnapShot("DateCreated", Description = "Date Created")]
		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[EnableSnapShot("VendorId", Description = "Vendor Reference")]
		[Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long VendorId
		{
			get
			{
				if (_vendor != null) _vendorId = _vendor.Id;
				return _vendorId;
			}
			set
			{
				_vendorId = value;
				if (_vendor != null && value != _vendor.Id) _vendor = null;
			}
		}

		public Vendor Vendor
		{
			get { return _vendor ?? (_vendor = new Vendor(_vendorId)); }
			set
			{
				_vendor = value;
				_vendorId = value == null ? default(long) : value.Id;
			}
		}

		public List<VendorContact> Contacts
		{
			get
			{
				if (_contacts == null) LoadContacts();
				return _contacts;
			}
			set { _contacts = value; }
		}

		public bool IsNew { get { return Id == default(long); } }

		public VendorLocation()
		{
			
		}

		public VendorLocation(long id) : this(id, false)
		{
		}

		public VendorLocation(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public VendorLocation(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

		public void LoadCollections()
		{
			LoadContacts();
		}

		private void LoadContacts()
		{
			_contacts = new List<VendorContact>();
			const string query = "Select * from VendorContact where VendorLocationId = @VendorLocationId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "VendorLocationId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_contacts.Add(new VendorContact(reader));
			Connection.Close();
		}
	}
}
