﻿namespace LogisticsPlus.Eship.Core.Accounting
{
	public enum InvoiceType
	{
        // DO NOT RE-ORDER. SEE APPLICABLE JOINS ON DATABASE REFINVOICETYPE
		Invoice = 0,
		Supplemental,
		Credit
	}
}
