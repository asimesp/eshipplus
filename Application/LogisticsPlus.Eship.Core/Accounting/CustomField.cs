﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("CustomField", ReadOnly = false, Source = EntitySource.TableView)]
	public class CustomField : TenantBase
	{
		private long _customerId;

		private Customer _customer;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("Name")]
		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

		[EnableSnapShot("DisplayOnOrigin", Description = "Applies to Origin")]
		[Property("DisplayOnOrigin", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool DisplayOnOrigin { get; set; }

		[EnableSnapShot("DisplayOnDestination", Description = "Applies to Destination")]
		[Property("DisplayOnDestination", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool DisplayOnDestination { get; set; }

		[EnableSnapShot("ShowOnDocuments", Description = "Show On Applicable Documents")]
		[Property("ShowOnDocuments", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ShowOnDocuments { get; set; }

		[EnableSnapShot("Required")]
		[Property("Required", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Required { get; set; }

		[EnableSnapShot("CustomerId", Description = "Customer Reference")]
		[Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long CustomerId
		{
			get
			{
				if (_customer != null) _customerId = _customer.Id;
				return _customerId;
			}
			set
			{
				_customerId = value;
				if (_customer != null && value != _customer.Id) _customer = null;
			}
		}

		public Customer Customer
		{
			get { return _customer ?? (_customer = new Customer(_customerId)); }
			set
			{
				_customer = value;
				_customerId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public CustomField()
		{

		}

		public CustomField(long id) : this(id, false)
		{
		}

		public CustomField(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public CustomField(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

	}
}
