﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("VendorNoServiceDay", ReadOnly = false, Source = EntitySource.TableView)]
	public class VendorNoServiceDay : TenantBase
	{
		private long _vendorId;
		private long _noServiceDayId;

		private NoServiceDay _noServiceDay;
		private Vendor _vendor;

		[EnableSnapShot("NoServiceDayId", Description = "No Service Day Reference")]
		[Property("NoServiceDayId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long NoServiceDayId
		{
			get
			{
				if (_noServiceDay != null) _noServiceDayId = _noServiceDay.Id;
				return _noServiceDayId;
			}
			set
			{
				_noServiceDayId = value;
				if (_noServiceDay != null && value != _noServiceDay.Id) _noServiceDay = null;
			}
		}

		[EnableSnapShot("VendorId", Description = "Vendor Reference")]
		[Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long VendorId
		{
			get
			{
				if (_vendor != null) _vendorId = _vendor.Id;
				return _vendorId;
			}
			set
			{
				_vendorId = value;
				if (_vendor != null && value != _vendor.Id) _vendor = null;
			}
		}

		public Vendor Vendor
		{
			get { return _vendor ?? (_vendor = new Vendor(_vendorId)); }
			set
			{
				_vendor = value;
				_vendorId = value == null ? default(long) : value.Id;
			}
		}
		public NoServiceDay NoServiceDay
		{
			get { return _noServiceDay ?? (_noServiceDay = new NoServiceDay(_noServiceDayId)); }
			set
			{
				_noServiceDay = value;
				_noServiceDayId = value == null ? default(long) : value.Id;
			}
		}

		public VendorNoServiceDay()
		{
		}

		public VendorNoServiceDay(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			Insert();
		}

		public new void Delete()
		{
			base.Delete();
		}
	}
}
