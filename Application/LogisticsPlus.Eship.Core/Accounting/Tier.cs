﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("Tier", ReadOnly =  false, Source =  EntitySource.TableView)]
	public class Tier : TenantBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("Name")]
		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

		[EnableSnapShot("DateCreated", Description = "Date Created")]
		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[EnableSnapShot("LogoUrl", Description = "Logo Url")]
		[Property("LogoUrl", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LogoUrl { get; set; }

		[EnableSnapShot("TierNumber", Description = "Tier Number")]
		[Property("TierNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string TierNumber { get; set; }

		[EnableSnapShot("AdditionalBillOfLadingText", Description = "Additional Bill of Lading Text")]
		[Property("AdditionalBillOfLadingText", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string AdditionalBillOfLadingText { get; set; }

		[EnableSnapShot("LTLNotificationEmails", Description = "LTL Notification Emails")]
		[Property("LTLNotificationEmails", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LTLNotificationEmails { get; set; }

		[EnableSnapShot("FTLNotificationEmails", Description = "FTL Notification Emails")]
		[Property("FTLNotificationEmails", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string FTLNotificationEmails { get; set; }

		[EnableSnapShot("SPNotificationEmails", Description = "Small Pack Notification Emails")]
		[Property("SPNotificationEmails", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string SPNotificationEmails { get; set; }

		[EnableSnapShot("RailNotificationEmails", Description = "Rail Notification Emails")]
		[Property("RailNotificationEmails", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string RailNotificationEmails { get; set; }

		[EnableSnapShot("AirNotificationEmails", Description = "Air Notification Emails")]
		[Property("AirNotificationEmails", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string AirNotificationEmails { get; set; }
		
		[EnableSnapShot("GeneralNotificationEmails", Description = "General Notification Emails")]
		[Property("GeneralNotificationEmails", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string GeneralNotificationEmails { get; set; }

		[EnableSnapShot("PendingVendorNotificationEmails", Description = "Pending Vendor Notification Emails")]
		[Property("PendingVendorNotificationEmails", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string PendingVendorNotificationEmails { get; set; }

		[EnableSnapShot("Active", Description = "Active Status")]
		[Property("Active", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Active { get; set; }

		[EnableSnapShot("TollFreeContactNumber", Description = "Toll Free Contact Number")]
		[Property("TollFreeContactNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string TollFreeContactNumber { get; set; }

		[EnableSnapShot("TierSupportEmails", Description = "Tier Support Emails")]
		[Property("TierSupportEmails", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string TierSupportEmails { get; set; }

        [EnableSnapShot("SupportTollFree", Description = "Toll Free Support Number")]
        [Property("SupportTollFree", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string SupportTollFree { get; set; }


		[EnableSnapShot("AccountPayableDisputeNotificationEmails", Description = "Email Id's for sending dispute notificatio emails")]
		[Property("AccountPayableDisputeNotificationEmails", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string AccountPayableDisputeNotificationEmails { get; set; }

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public Tier()
		{
			
		}

		public Tier(long id) : this(id, false)
		{
		}

		public Tier(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if(takeSnapshot) TakeSnapShot();
		}

		public Tier(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

		public List<Customer> RetrieveTierCustomers()
		{
			var customers = new List<Customer>();
			const string query = "Select * from Customer where TenantId = @TenantId and TierId = @TierId order by [Name] ASC, CustomerNumber ASC";
			var parameters = new Dictionary<string, object> { { "TenantId", TenantId }, { "TierId", Id } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					customers.Add(new Customer(reader));
			Connection.Close();

			return customers;
		}
	}
}
