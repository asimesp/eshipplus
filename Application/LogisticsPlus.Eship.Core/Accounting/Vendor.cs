﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("Vendor", ReadOnly = false, Source = EntitySource.TableView)]
	public class Vendor : TenantBase
	{
		private long _vendorServiceRepUserId;
		private long _lastAuditedByUserId;

		private VendorCommunication _communication;
		private User _vendorServiceRep;
		private User _lastAuditedByUser;

		private List<VendorInsurance> _insurances;
		private List<VendorPackageCustomMapping> _vendorPackageCustomMappings;
		private List<VendorLocation> _locations;
		private List<VendorService> _services;
		private List<VendorEquipment> _equipments;
		private List<VendorRating> _ratings;
		private List<VendorNoServiceDay> _noServiceDays;
		private List<VendorDocument> _documents;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("VendorNumber", Description = "Vendor Number")]
		[Property("VendorNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string VendorNumber { get; set; }

		[EnableSnapShot("Name")]
		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

		[EnableSnapShot("Notation", Description = "Special Notes")]
		[Property("Notation", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Notation { get; set; }

		[EnableSnapShot("Active", Description = "Active Status")]
		[Property("Active", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Active { get; set; }

		[EnableSnapShot("DateCreated", Description = "Date Created")]
		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[EnableSnapShot("LogoUrl", Description = "Logo Url")]
		[Property("LogoUrl", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LogoUrl { get; set; }

		[EnableSnapShot("TSACertified", Description = "TSA Certified")]
		[Property("TSACertified", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool TSACertified { get; set; }

		[EnableSnapShot("CTPATCertified", Description = "CTPAT Certified")]
		[Property("CTPATCertified", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool CTPATCertified { get; set; }

		[EnableSnapShot("SmartWayCertified", Description = "Greenway Certified")]
		[Property("SmartWayCertified", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool SmartWayCertified { get; set; }

		[EnableSnapShot("PIPCertified", Description = "PIP Certified")]
		[Property("PIPCertified", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool PIPCertified { get; set; }

		[EnableSnapShot("CTPATNumber", Description = "CTPAT Number")]
		[Property("CTPATNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CTPATNumber { get; set; }

		[EnableSnapShot("PIPNumber", Description = "PIP Number")]
		[Property("PIPNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string PIPNumber { get; set; }

		[EnableSnapShot("Scac", Description = "SCAC")]
		[Property("Scac", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Scac { get; set; }

		[EnableSnapShot("FederalIDNumber", Description = "Federal ID Number")]
		[Property("FederalIDNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string FederalIDNumber { get; set; }

		[EnableSnapShot("BrokerReferenceNumber", Description = "Broker Reference Number")]
		[Property("BrokerReferenceNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string BrokerReferenceNumber { get; set; }

		[EnableSnapShot("BrokerReferenceNumberType", Description = "Broker Reference Number Type")]
		[Property("BrokerReferenceNumberType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public BrokerReferenceNumberType BrokerReferenceNumberType { get; set; }

		[EnableSnapShot("MC", Description = "MC Number")]
		[Property("MC", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string MC { get; set; }

		[EnableSnapShot("DOT", Description = "DOT Number")]
		[Property("DOT", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DOT { get; set; }

		[EnableSnapShot("TrackingUrl", Description = "Website Tracking Url")]
		[Property("TrackingUrl", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string TrackingUrl { get; set; }

		[EnableSnapShot("HandlesLessThanTruckload", Description = "Can Handle Less than Truckload")]
		[Property("HandlesLessThanTruckload", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool HandlesLessThanTruckload { get; set; }

		[EnableSnapShot("HandlesTruckload", Description = "Can Handle Truckload")]
		[Property("HandlesTruckload", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool HandlesTruckload { get; set; }

		[EnableSnapShot("HandlesAir", Description = "Can Handle Air")]
		[Property("HandlesAir", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool HandlesAir { get; set; }

		[EnableSnapShot("HandlesPartialTruckload", Description = "Can Handle Partial Truckload")]
		[Property("HandlesPartialTruckload", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool HandlesPartialTruckload { get; set; }

		[EnableSnapShot("HandlesRail", Description = "Can Handle Rail")]
		[Property("HandlesRail", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool HandlesRail { get; set; }

		[EnableSnapShot("HandlesSmallPack", Description = "Can Handle Small Package")]
		[Property("HandlesSmallPack", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool HandlesSmallPack { get; set; }

		[EnableSnapShot("IsCarrier", Description = "Is a Carrier")]
		[Property("IsCarrier", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool IsCarrier { get; set; }

		[EnableSnapShot("IsAgent", Description = "Is an Agent")]
		[Property("IsAgent", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool IsAgent { get; set; }

		[EnableSnapShot("IsBroker", Description = "Is a Broker")]
		[Property("IsBroker", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool IsBroker { get; set; }

		[EnableSnapShot("Notes", Description = "Quick Reference Notes")]
		[Property("Notes", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Notes { get; set; }

		[EnableSnapShot("VendorServiceRepId", Description = "Vendor Service Representative Reference")]
		[Property("VendorServiceRepId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false, Column = "VendorServiceRepUserId")]
		public long VendorServiceRepId
		{
			get { return _vendorServiceRepUserId; }
			set
			{
				_vendorServiceRepUserId = value;
				if (_vendorServiceRep != null && value != _vendorServiceRep.Id) _vendorServiceRep = null;
			}
		}

		[EnableSnapShot("LastAuditedByUserId", Description = "Vendor Last Audited By Reference")]
		[Property("LastAuditedByUserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long LastAuditedByUserId
		{
			get { return _lastAuditedByUserId; }
			set
			{
				_lastAuditedByUserId = value;
				if (_lastAuditedByUser != null && value != _lastAuditedByUser.Id) _lastAuditedByUser = null;
			}
		}

		[EnableSnapShot("LastAuditDate", Description = "Last Audit Date")]
		[Property("LastAuditDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime LastAuditDate { get; set; }

		[EnableSnapShot("CustomField1")]
		[Property("CustomField1", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomField1 { get; set; }

		[EnableSnapShot("CustomField2")]
		[Property("CustomField2", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomField2 { get; set; }

		[EnableSnapShot("CustomField3")]
		[Property("CustomField3", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomField3 { get; set; }

		[EnableSnapShot("CustomField4")]
		[Property("CustomField4", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomField4 { get; set; }

		[EnableSnapShot("ExcludeFromAvailableLoadsBlast", Description = "Exclude From Available Loads Blast")]
		[Property("ExcludeFromAvailableLoadsBlast", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ExcludeFromAvailableLoadsBlast { get; set; }

		[EnableSnapShot("CommunicationId", Description = "Communication Settings Reference")]
		[Property("CommunicationId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		private long CommunicationId { get; set; }


		public VendorCommunication Communication
		{
			get { return CommunicationId == default(long) ? null : _communication ?? (_communication = new VendorCommunication(CommunicationId)); }
			set
			{
				_communication = value;
				CommunicationId = value == null ? default(long) : value.Id;
			}
		}
		public User LastAuditedBy
		{
			get { return _lastAuditedByUserId == default(long) ? null : _lastAuditedByUser ?? (_lastAuditedByUser = new User(_lastAuditedByUserId, false)); }
			set
			{
				_lastAuditedByUser = value;
				_lastAuditedByUserId = value == null ? default(long) : value.Id;
			}
		}
		public User VendorServiceRep
		{
			get { return _vendorServiceRepUserId == default(long) ? null : _vendorServiceRep ?? (_vendorServiceRep = new User(_vendorServiceRepUserId, false)); }
			set
			{
				_vendorServiceRep = value;
				_vendorServiceRepUserId = value == null ? default(long) : value.Id;
			}
		}

		public List<VendorInsurance> Insurances
		{
			get
			{
				if (_insurances == null) LoadInsurance();
				return _insurances;
			}
			set { _insurances = value; }
		}
		public List<VendorLocation> Locations
		{
			get
			{
				if (_locations == null) LoadLocations();
				return _locations;
			}
			set { _locations = value; }
		}
		public List<VendorService> Services
		{
			get
			{
				if (_services == null) LoadServices();
				return _services;
			}
			set { _services = value; }
		}
		public List<VendorEquipment> Equipments
		{
			get
			{
				if (_equipments == null) LoadEquipments();
				return _equipments;
			}
			set { _equipments = value; }
		}
		public List<VendorPackageCustomMapping> VendorPackageCustomMappings
		{
			get
			{
				if (_vendorPackageCustomMappings == null) LoadVendorPackageCustomMapping();
				return _vendorPackageCustomMappings;
			}
			set { _vendorPackageCustomMappings = value; }
		}

		public List<VendorRating> Ratings
		{
			get
			{
				if (_ratings == null) LoadRatings();
				return _ratings;
			}
			set { _ratings = value; }
		}
		public List<VendorNoServiceDay> NoServiceDays
		{
			get
			{
				if (_noServiceDays == null) LoadNoServiceDays();
				return _noServiceDays;
			}
			set { _noServiceDays = value; }
		}
		public List<VendorDocument> Documents
		{
			get
			{
				if (_documents == null) LoadDocuments();
				return _documents;
			}
			set { _documents = value; }
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public Vendor()
		{
			
		}

		public Vendor(long id) : this(id, false)
		{
		}

		public Vendor(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public Vendor(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

		public void PurgeVendorTerminals()
		{
			const string query = @"delete from VendorTerminal where VendorId = @VendorId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "VendorId", Id }, { "TenantId", TenantId } };
			ExecuteNonQuery(query, parameters);
		}

		public void PurgeVendorPreferredLanes()
		{
			const string query = @"delete from VendorPreferredLane where VendorId = @VendorId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "VendorId", Id }, { "TenantId", TenantId } };
			ExecuteNonQuery(query, parameters);
		}
		

		public void LoadCollections()
		{
			LoadInsurance();
			LoadLocations();
			LoadServices();
			LoadEquipments();
			LoadRatings();
			LoadNoServiceDays();
			LoadDocuments();
			LoadVendorPackageCustomMapping();
		}

		
		public List<EquipmentType> RetrieveEquipmentTypes()
		{
			var equipmentTypes = new List<EquipmentType>();
			const string query = @"select EquipmentType.* from EquipmentType, VendorEquipment where 
				VendorEquipment.EquipmentTypeId = EquipmentType.Id and VendorEquipment.VendorId = @VendorId
				and VendorEquipment.TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "TenantId", TenantId }, { "VendorId", Id } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					equipmentTypes.Add(new EquipmentType(reader));
			Connection.Close();
			return equipmentTypes;
		}

		public List<NoServiceDay> RetrieveNoServiceDays()
		{
			var noServiceDays = new List<NoServiceDay>();
			const string query = @"select NoServiceDay.* from NoServiceDay, VendorNoServiceDay where 
				VendorNoServiceDay.NoServiceDayId = NoServiceDay.Id and VendorNoServiceDay.VendorId = @VendorId
				and VendorNoServiceDay.TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "TenantId", TenantId }, { "VendorId", Id } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					noServiceDays.Add(new NoServiceDay(reader));
			Connection.Close();
			return noServiceDays;
		}

		
		private void LoadInsurance()
		{
			_insurances = new List<VendorInsurance>();
			const string query = "Select * from VendorInsurance where VendorId = @VendorId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "VendorId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_insurances.Add(new VendorInsurance(reader));
			Connection.Close();
		}

		private void LoadLocations()
		{
			_locations = new List<VendorLocation>();
			const string query = "Select * from VendorLocation where VendorId = @VendorId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "VendorId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_locations.Add(new VendorLocation(reader));
			Connection.Close();
		}

		private void LoadServices()
		{
			_services = new List<VendorService>();
			const string query = "Select * from VendorService where VendorId = @VendorId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "VendorId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_services.Add(new VendorService(reader));
			Connection.Close();
		}

		private void LoadEquipments()
		{
			_equipments = new List<VendorEquipment>();
			const string query = "Select * from VendorEquipment where VendorId = @VendorId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "VendorId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_equipments.Add(new VendorEquipment(reader));
			Connection.Close();
		}

		private void LoadRatings()
		{
			_ratings = new List<VendorRating>();
			const string query = "Select * from VendorRating where VendorId = @VendorId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "VendorId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_ratings.Add(new VendorRating(reader));
			Connection.Close();
		}

		private void LoadNoServiceDays()
		{
			_noServiceDays = new List<VendorNoServiceDay>();
			const string query = "Select * from VendorNoServiceDay where VendorId = @VendorId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "VendorId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_noServiceDays.Add(new VendorNoServiceDay(reader));
			Connection.Close();
		}

		private void LoadDocuments()
		{
			_documents = new List<VendorDocument>();
			const string query = @"SELECT * FROM VendorDocument WHERE VendorId = @VendorId AND TenantId = @TenantId;";
			var parameters = new Dictionary<string, object> { { "VendorId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_documents.Add(new VendorDocument(reader));
			Connection.Close();
		}

		private void LoadVendorPackageCustomMapping()
		{
			_vendorPackageCustomMappings = new List<VendorPackageCustomMapping>();
			const string query = "Select * from VendorPackageCustomMapping where VendorId = @VendorId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "VendorId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_vendorPackageCustomMappings.Add(new VendorPackageCustomMapping(reader));
			Connection.Close();
		}
	}
}
