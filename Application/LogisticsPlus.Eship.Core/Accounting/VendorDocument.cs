﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Accounting
{
	[Serializable]
	[Entity("VendorDocument", ReadOnly = false, Source = EntitySource.TableView)]
	public class VendorDocument : Document
	{
		private long _vendorId;

		private Vendor _vendor;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("IsInternal", Description = "Is Internal")]
		[Property("IsInternal", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool IsInternal { get; set; }

		[Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long VendorId
		{
			get
			{
				if (_vendor != null) _vendorId = _vendor.Id;
				return _vendorId;
			}
			set
			{
				_vendorId = value;
				if (_vendor != null && value != _vendor.Id) _vendor = null;
			}
		}

		public Vendor Vendor
		{
			get { return _vendor ?? (_vendor = new Vendor(_vendorId)); }
			set
			{
				_vendor = value;
				_vendorId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public VendorDocument() { }

		public VendorDocument(long id) : this(id, false) { }

		public VendorDocument(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public VendorDocument(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
