﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core
{
	[Serializable]
	[Entity("AutoNumber", ReadOnly = false, Source = EntitySource.TableView)]
	public class AutoNumber : TenantBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[Property("Code", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public AutoNumberCode Code { get; set; }

		[EnableSnapShot("NextNumber", Description = "Next Number in Sequence")]
		[Property("NextNumber", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long NextNumber { get; set; }

		public bool IsNew { get { return Id == default(long); } }

		public AutoNumber()
		{
		}

		public AutoNumber(long id) : this(id, false)
		{
			
		}

		public AutoNumber(long id, bool takeSnapShot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if(takeSnapShot) TakeSnapShot();
		}

		public AutoNumber(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

	}
}
