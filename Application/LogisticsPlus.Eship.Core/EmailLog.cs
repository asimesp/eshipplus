﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core
{
    [Serializable]
    [Entity("EmailLog", ReadOnly = false, Source = EntitySource.TableView)]
    public class EmailLog : TenantBase
    {
        private List<EmailLogAttachment> _emailLogAttachments;


        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; private set; }

        [Property("From", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string From { get; set; }

        [Property("Tos", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Tos { get; set; }

        [Property("Ccs", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Ccs { get; set; }

        [Property("Bbcs", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Bbcs { get; set; }

        [Property("Body", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Body { get; set; }

        [Property("Subject", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Subject { get; set; }

        [Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime DateCreated { get; set; }

        [EnableSnapShot("Sent")]
        [Property("Sent", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool Sent { get; set; }


        public List<EmailLogAttachment> EmailLogAttachments
        {
            get
            {
                if (_emailLogAttachments == null) LoadEmailLogAttachments();
                return _emailLogAttachments;
            }
            set { _emailLogAttachments = value; }
        }


        public void Save()
		{
            if (Id == default(long)) Insert();
            else Update();
		}

        public EmailLog(long id)
        {
            Id = id;
            if (Id == default(long)) return;
            KeyLoaded = Load();
        }

		public EmailLog(DbDataReader reader)
		{
			Load(reader);
		}

        public EmailLog()
        {
            
        }

        private void LoadEmailLogAttachments()
        {
            _emailLogAttachments = new List<EmailLogAttachment>();
            const string query = @"SELECT * FROM EmailLogAttachment WHERE EmailLogId = @EmailLogId AND TenantId = @TenantId;";
            var parameters = new Dictionary<string, object> { { "EmailLogId", Id }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _emailLogAttachments.Add(new EmailLogAttachment(reader));
            Connection.Close();
        }
    }
}
