﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core
{
    [Serializable]
    [Entity("EmailLogAttachment", ReadOnly = false, Source = EntitySource.TableView)]
    public class EmailLogAttachment : TenantBase
    {
        private long _emailLogId;
        private EmailLog _emailLog;

        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; private set; }

        [Property("EmailLogId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        protected long EmailLogId
        {
            get
            {
                if (_emailLog != null) _emailLogId = _emailLog.Id;
                return _emailLogId;
            }
            set
            {
                _emailLogId = value;
                if (_emailLog != null && value != _emailLog.Id) _emailLog = null;
            }
        }
        
        [Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Name { get; set; }

        [Property("AttachmentBytes", AutoValueOnInsert = false, DataType = SqlDbType.VarBinary, Key = false)]
        public byte[] AttachmentBytes { get; set; }


        public EmailLog EmailLog
        {
            get { return _emailLog ?? (_emailLog = new EmailLog(_emailLogId)); }
            set
            {
                _emailLog = value;
                _emailLogId = value == null ? default(long) : value.Id;
            }
        }


        public bool IsNew
		{
			get { return Id == default(long); }
		}

		public EmailLogAttachment()
		{

		}

		public EmailLogAttachment(long id)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
		}

        public EmailLogAttachment(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
    }
}
