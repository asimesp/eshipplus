﻿namespace LogisticsPlus.Eship.Core
{
	public static class AuditLogConstants
	{
		public static string AddedNew = "Added New";
		public static string Delete = "Deleted";
		public static string Update = "Updated";
	}
}
