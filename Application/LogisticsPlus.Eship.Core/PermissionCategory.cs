﻿namespace LogisticsPlus.Eship.Core
{
	public enum PermissionCategory
	{
		/* DO NOT RE-ARRANGE */
		Operations = 0,
		Accounting,
		Registry,
		Rating,
		Utilities,
		BusinessIntelligence,
		Connect,
		Administration
	}
}
