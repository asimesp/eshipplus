﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.Core.Calculations
{
	public static class CubicFootCalculations
	{
		public static decimal CubicFoot { get { return 1728; } }

		public static decimal MinPcf { get { return 0.0000m; } }
		public static decimal MaxPcf { get { return 9999.9999m; } }


		public static decimal CubitFeet(this LoadOrderItem item)
		{
			return item.Length * item.Width * item.Height / CubicFoot;
		}

		public static decimal ActualCubitFeet(this ShipmentItem item)
		{
			return item.ActualLength*item.ActualWidth*item.ActualHeight/CubicFoot;
		}



		public static decimal PoundPerCubitFeet(this LoadOrderItem item)
		{
			var cf = (item.CubitFeet() * item.Quantity);
			return cf == 0 ? 0m : item.Weight / cf;
		}

		public static decimal ActualPoundPerCubitFeet(this ShipmentItem item)
		{
			var cf = (item.ActualCubitFeet() * item.Quantity);
			return cf == 0 ? 0m : item.ActualWeight / cf;
		}



		public static decimal TotalCubitFeet(this IEnumerable<LoadOrderItem> items)
		{
			return items.Sum(i => i.CubitFeet() * i.Quantity);
		}

		public static decimal TotalActualCubitFeet(this IEnumerable<ShipmentItem> items)
		{
			return items.Sum(i => i.ActualCubitFeet()*i.Quantity);
		}



		public static decimal TotalPoundPerCubitFeet(this IEnumerable<LoadOrderItem> items)
		{
			return items.Sum(i => i.PoundPerCubitFeet());
		}

		public static decimal TotalActualPoundPerCubitFeet(this IEnumerable<ShipmentItem> items)
		{
			return items.Sum(i => i.ActualPoundPerCubitFeet());
		}



		public static bool IsOutsideAllowablePcf(this LoadOrderItem item)
		{
			return item.PoundPerCubitFeet().IsOutsideAllowablePcf();
		}

		public static bool IsOutsideAllowablePcfActual(this ShipmentItem item)
		{
			return item.ActualPoundPerCubitFeet().IsOutsideAllowablePcf();
		}

		public static bool IsOutsideAllowablePcf(this decimal pcf)
		{
			return pcf < MinPcf || pcf > MaxPcf;
		}
	}
}
