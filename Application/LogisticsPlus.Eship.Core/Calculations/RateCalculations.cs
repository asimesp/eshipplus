﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Core.Calculations
{
	public static class RateCalculations
	{
		public static decimal CalculateRate(this RateType rateType, decimal seedValue, decimal mileage, decimal totalWeight, decimal totalLinehaul)
		{
			switch (rateType)
			{
				case RateType.RatePerMile:
					return seedValue*mileage;
				case RateType.Flat:
					return seedValue;
				case RateType.LineHaulPercentage:
					return seedValue*totalLinehaul/100;
				case RateType.PerHundredWeight:
					return seedValue*totalWeight/100;
			}
			return 0;
		}

		public static decimal CalculatePerHundredWeightRate(this decimal seedValue, decimal totalWeight)
		{
			return RateType.PerHundredWeight.CalculateRate(seedValue, 0m, totalWeight, 0m);
		}

		public static decimal CalculateRatePerMileRate(this decimal seedValue, decimal mileage)
		{
			return RateType.RatePerMile.CalculateRate(seedValue, mileage, 0m, 0m);
		}

		public static decimal CalculateLineHaulPercentageRate(this decimal seedValue, decimal totalLinehaul)
		{
			return RateType.LineHaulPercentage.CalculateRate(seedValue, 0m, 0m, totalLinehaul);
		}

		public static decimal CalculateFlatRate(this decimal seedValue)
		{
			return RateType.Flat.CalculateRate(seedValue, 0m, 0m, 0m);
		}



		public static decimal CalculateMarkedRate(this decimal value, decimal markupPercent, decimal markupValue, bool useLower)
		{
			var retValPercent = value*(1 + (markupPercent/100m));
			var retValValue = value + markupValue;


			return useLower
			       	? retValPercent < retValValue ? retValPercent : retValValue
			       	: retValPercent < retValValue ? retValValue : retValPercent;
		}



		public static decimal ApplyManipulations(this decimal value, decimal floor, decimal ceiling, decimal discountPercentage)
		{
			var retVal = value*(1 - (discountPercentage/100m));
			return retVal < floor ? floor : retVal > ceiling ? ceiling : retVal;
		}

		public static decimal ApplyManipulations(this decimal value, decimal floor, decimal ceiling)
		{
			return ApplyManipulations(value, floor, ceiling, 0m);
		}



		public static void ApplyApplicableProfitForNoProfitCharge(this Charge charge, CustomerRating rating)
		{
			// expected that profit is zero for incoming charge. DO NOT ERASE CALCULATED PROFITS!
			if (charge.Profit != 0) return;

			// since profit is not zero, then apply minimum profit first
			switch (charge.ChargeCode.Category)
			{
				case ChargeCodeCategory.Freight:
					charge.UnitSell = charge.UnitBuy
						.CalculateMarkedRate(rating.LineHaulProfitFloorPercentage,
											 rating.LineHaulProfitFloorValue,
											 rating.UseLineHaulMinimumFloor);
					break;
				case ChargeCodeCategory.Fuel:
					charge.UnitSell = charge.UnitBuy
						.CalculateMarkedRate(rating.FuelProfitFloorPercentage,
											 rating.FuelProfitFloorValue,
											 rating.UseFuelMinimumFloor);
					break;
				case ChargeCodeCategory.Accessorial:
					charge.UnitSell = charge.UnitBuy
						.CalculateMarkedRate(rating.AccessorialProfitFloorPercentage,
											 rating.AccessorialProfitFloorValue,
											 rating.UseAccessorialMinimumFloor);
					break;
				case ChargeCodeCategory.Service:
					charge.UnitSell = charge.UnitBuy
						.CalculateMarkedRate(rating.ServiceProfitFloorPercentage,
											 rating.ServiceProfitFloorValue,
											 rating.UseServiceMinimumFloor);
					break;
			}
			

			//	If profit still zero, apply maximum profit
			if (charge.Profit != 0) return;
			switch (charge.ChargeCode.Category)
			{
				case ChargeCodeCategory.Freight:
					charge.UnitSell = charge.UnitBuy
						.CalculateMarkedRate(rating.LineHaulProfitCeilingPercentage,
											 rating.LineHaulProfitCeilingValue,
											 rating.UseLineHaulMinimumCeiling);
					break;
				case ChargeCodeCategory.Fuel:
					charge.UnitSell = charge.UnitBuy
						.CalculateMarkedRate(rating.FuelProfitCeilingPercentage,
											 rating.FuelProfitCeilingValue,
											 rating.UseFuelMinimumCeiling);
					break;
				case ChargeCodeCategory.Accessorial:
					charge.UnitSell = charge.UnitBuy
						.CalculateMarkedRate(rating.AccessorialProfitCeilingPercentage,
											 rating.AccessorialProfitCeilingValue,
											 rating.UseAccessorialMinimumCeiling);
					break;
				case ChargeCodeCategory.Service:
					charge.UnitSell = charge.UnitBuy
						.CalculateMarkedRate(rating.ServiceProfitCeilingPercentage,
											 rating.ServiceProfitCeilingValue,
											 rating.UseServiceMinimumCeiling);
					break;
			}
		}

		public static decimal GetAdjustmentFator(this CustomerRating rating, ChargeCodeCategory category, IEnumerable<RateCharge> charges)
		{
			var cost = charges.Where(c => c.ChargeCode.Category == category).Sum(c => c.FinalBuy);
			var sell = charges.Where(c => c.ChargeCode.Category == category && c.ChargeCode.Project44Code != Project44ChargeCode.DSC).Sum(c => c.AmountDue);
			var profitValue = sell - cost;
			var profitPercent = cost == 0m ? 0m : profitValue / cost;

			var ceiling = rating.GetCeilingAdjustmentFator(category, profitPercent, profitValue);
			var floor = rating.GetFloorAdjustmentFator(category, profitPercent, profitValue);

			return ceiling != 1m ? ceiling : floor != 1m ? floor : 1m;
		}

		private static decimal GetCeilingAdjustmentFator(this CustomerRating rating, ChargeCodeCategory category, decimal profitPercent, decimal profitValue)
		{
			decimal factor;
			switch (category)
			{
				case ChargeCodeCategory.Fuel:
					factor = CeilingFactor(profitPercent, profitValue, rating.FuelProfitCeilingType,
					                       rating.FuelProfitCeilingPercentage/100m, rating.FuelProfitCeilingValue,
					                       rating.UseFuelMinimumCeiling);
					break;
				case ChargeCodeCategory.Accessorial:
					factor = CeilingFactor(profitPercent, profitValue, rating.AccessorialProfitCeilingType,
					                       rating.AccessorialProfitCeilingPercentage/100m, rating.AccessorialProfitCeilingValue,
					                       rating.UseAccessorialMinimumCeiling);
					break;
				case ChargeCodeCategory.Service:
					factor = CeilingFactor(profitPercent, profitValue, rating.ServiceProfitCeilingType,
					                       rating.ServiceProfitCeilingPercentage/100m, rating.ServiceProfitCeilingValue,
					                       rating.UseServiceMinimumCeiling);
					break;
				default:
					factor = CeilingFactor(profitPercent, profitValue, rating.LineHaulProfitCeilingType,
					                       rating.LineHaulProfitCeilingPercentage/100m, rating.LineHaulProfitCeilingValue,
					                       rating.UseLineHaulMinimumCeiling);
					break;
			}
			return factor;
		}

		private static decimal GetFloorAdjustmentFator(this CustomerRating rating, ChargeCodeCategory category, decimal profitPercent, decimal profitValue)
		{
			decimal factor;
			switch (category)
			{
				case ChargeCodeCategory.Fuel:
					factor = FloorFactor(profitPercent, profitValue, rating.FuelProfitFloorType,
					                     rating.FuelProfitFloorPercentage/100m, rating.FuelProfitFloorValue,
					                     rating.UseServiceMinimumFloor);
					break;
				case ChargeCodeCategory.Accessorial:
					factor = FloorFactor(profitPercent, profitValue, rating.AccessorialProfitFloorType,
					                     rating.AccessorialProfitFloorPercentage/100m, rating.AccessorialProfitFloorValue,
					                     rating.UseAccessorialMinimumFloor);
					break;
				case ChargeCodeCategory.Service:
					factor = FloorFactor(profitPercent, profitValue, rating.ServiceProfitFloorType,
					                     rating.ServiceProfitFloorPercentage/100m, rating.ServiceProfitFloorValue,
					                     rating.UseServiceMinimumFloor);
					break;
				default:
					factor = FloorFactor(profitPercent, profitValue, rating.LineHaulProfitFloorType,
					                     rating.LineHaulProfitFloorPercentage/100m, rating.LineHaulProfitFloorValue,
					                     rating.UseLineHaulMinimumFloor);
					break;
			}
			return factor;
		}

		private static decimal CeilingFactor(decimal actualPercent, decimal actualValue, ValuePercentageType type, decimal percentCap, decimal valueCap, bool useMinimumCeiling)
		{
			decimal factor;
			if (type == ValuePercentageType.Percentage)
			{
				factor = percentCap < actualPercent && actualPercent != 0m
				         	? 1m - (Math.Abs(percentCap - actualPercent)/actualPercent)
				         	: 1m;
			}
			else if (type == ValuePercentageType.Value)
			{
				factor = valueCap < actualValue && actualValue != 0m
				         	? 1m - (Math.Abs(valueCap - actualValue)/actualValue)
				         	: 1m;
			}
			else
			{
				var p = percentCap < actualPercent && actualPercent != 0m
				        	? 1m - (Math.Abs(percentCap - actualPercent)/actualPercent)
				        	: 1m;
				var v = valueCap < actualValue && actualValue != 0m
				        	? 1m - (Math.Abs(valueCap - actualValue)/actualValue)
				        	: 1m;
				factor = useMinimumCeiling
				         	? p < v ? p : v
				         	: p < v ? v : p;
			}
			return factor;
		}

		private static decimal FloorFactor(decimal actualPercent, decimal actualValue, ValuePercentageType type, decimal percentMin, decimal valueMin, bool useMinimumFloor)
		{
			decimal factor;
			if (type == ValuePercentageType.Percentage)
			{
				factor = percentMin > actualPercent && actualPercent != 0m
							? 1m + (Math.Abs(percentMin - actualPercent) / actualPercent)
							: 1m;
			}
			else if (type == ValuePercentageType.Value)
			{
				factor = valueMin > actualValue && actualValue != 0m
							? 1m + (Math.Abs(valueMin - actualValue) / actualValue)
							: 1m;
			}
			else
			{
				var p = percentMin > actualPercent && actualPercent != 0m
							? 1m + (Math.Abs(percentMin - actualPercent) / actualPercent)
							: 1m;
				var v = valueMin > actualValue && actualValue != 0m
							? 1m + (Math.Abs(valueMin - actualValue) / actualValue)
							: 1m;
				factor = useMinimumFloor
							? p < v ? p : v
							: p < v ? v : p;
			}
			return factor;
		}
	}
}
