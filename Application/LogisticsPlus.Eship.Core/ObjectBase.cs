﻿using System;
using System.Collections.Generic;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.Core
{
	[Serializable]
	public class ObjectBase : EntityBase
	{
		public bool KeyLoaded { get; protected set; }

		public void TakeSnapShot()
		{
			GetSnapShot();
		}

		public bool HasChanges()
		{
			return Diff().Count > 0;
		}

		public List<string> Changes()
		{
			return Diff();
		}

		public new void Copy<T>(T other) where T : ObjectBase
		{
			base.Copy(other);
		}
	}
}
