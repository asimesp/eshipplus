﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Administration;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core
{
	[Serializable]
	[Entity("Lock", ReadOnly = false, Source = EntitySource.TableView, SourceName = "EntityLock")]
	public class Lock : TenantBase
	{
		private User _user;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[Property("LockKey", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LockKey { get; set; }

		[Property("LockDateTime", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime LockDateTime { get; set; }

		[Property("EntityId", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string EntityId { get; set; }

		[Property("LockUserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		private long LockUserId { get; set; }

		public bool Locked { get { return Id != default(long); } }

		public User User
		{
			get { return LockUserId == default(long) ? null : _user ?? (_user = new User(LockUserId)); }
			set
			{
				_user = value;
				LockUserId = value == null ? default(long) : value.Id;
			}
		}

		public Lock()
		{
			
		}


		public Lock(long id)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
		}

		public Lock(DbDataReader reader)
		{
			Load(reader);
		}



		public void Secure()
		{
			if (Id != default(long))
				throw new InvalidOperationException("Lock is already secured.");
			Insert();
		}

		public void Refresh()
		{
			if(Id == default(long))
				throw new InvalidOperationException("Cannot refresh non existing lock.");
			LockDateTime = DateTime.Now;
			Update();
		}

		public new void Delete()
		{
			if (Id == default(long)) return;
			base.Delete();
		}
	}
}
