﻿using System;
using System.Data;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Sales
{
	[Serializable]
	[Entity("SalesLead", ReadOnly = false, Source = EntitySource.TableView)]
	public class SalesLead : TenantBase
	{
		private long _packageTypeId;

		private PackageType _packageType;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("DateCreated", Description = "Date Created")]
		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[EnableSnapShot("Archived", Description = "Archived")]
		[Property("Archived", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Archived { get; set; }

		[EnableSnapShot("Name")]
		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

		[EnableSnapShot("Phone")]
		[Property("Phone", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Phone { get; set; }

		[EnableSnapShot("RequestIPAddress", Description = "Request IP Address")]
		[Property("RequestIPAddress", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string RequestIPAddress { get; set; }

		[EnableSnapShot("ItemDescription", Description = "Item Description")]
		[Property("ItemDescription", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ItemDescription { get; set; }

		[EnableSnapShot("ItemFreightClass", Description = "Actual Freight Class")]
		[Property("ItemFreightClass", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
		public float ItemFreightClass { get; set; }

		[EnableSnapShot("ItemWeight", Description = "Item Weight")]
		[Property("ItemWeight", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal ItemWeight { get; set; }

		[EnableSnapShot("ItemLength", Description = "Item Length")]
		[Property("ItemLength", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal ItemLength { get; set; }

		[EnableSnapShot("ItemWidth", Description = "Item Width")]
		[Property("ItemWidth", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal ItemWidth { get; set; }

		[EnableSnapShot("ItemHeight", Description = "Item Height")]
		[Property("ItemHeight", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal ItemHeight { get; set; }

		[EnableSnapShot("OriginPostalCode", Description = "Origin Postal Code")]
		[Property("OriginPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginPostalCode { get; set; }

		[EnableSnapShot("DestinationPostalCode", Description = "Destination Postal Code")]
		[Property("DestinationPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationPostalCode { get; set; }

		[EnableSnapShot("PackageTypeId", Description = "Package Type Reference")]
		[Property("PackageTypeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long PackageTypeId
		{
			get { return _packageTypeId; }
			set
			{
				_packageTypeId = value;
				if (_packageType != null && value != _packageType.Id) _packageType = null;
			}
		}

		public PackageType PackageType
		{
			get { return _packageTypeId == default(long) ? null : _packageType ?? (_packageType = new PackageType(_packageTypeId)); }
			set
			{
				_packageType = value;
				_packageTypeId = value == null ? default(long) : value.Id;
			}
		}
	}
}
