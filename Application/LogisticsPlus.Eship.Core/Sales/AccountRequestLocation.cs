﻿using System;
using System.Data;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Sales
{
	[Serializable]
	[Entity("AccountRequestLocation", ReadOnly = true, Source = EntitySource.TableView)]
	public class AccountRequestLocation : Location
	{
		private long _accountRequestId;

		private AccountRequest _accountRequest;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("AccountRequestId", Description = "Account Request Reference")]
		[Property("AccountRequestId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		private long AccountRequestId
		{
			get
			{
				if (_accountRequest != null) _accountRequestId = _accountRequest.Id;
				return _accountRequestId;
			}
			set
			{
				_accountRequestId = value;
				if (_accountRequest != null && value != _accountRequest.Id) _accountRequest = null;
			}
		}

		public AccountRequest AccountRequest
		{
			get { return _accountRequest ?? (_accountRequest = new AccountRequest(_accountRequestId)); }
			set
			{
				_accountRequest = value;
				_accountRequestId = value == null ? default(long) : value.Id;
			}
		}

		public AccountRequestLocation()
		{
		}

		public AccountRequestLocation(long id)
		{
			Id = id;
			if (Id == default(long)) return;
			Load();
			TakeSnapShot();
		}
	}
}
