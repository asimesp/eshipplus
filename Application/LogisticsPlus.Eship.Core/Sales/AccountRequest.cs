﻿using System;
using System.Data;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Sales
{
	[Serializable]
	[Entity("AccountRequest", ReadOnly = false, Source = EntitySource.TableView)]
	public class AccountRequest : TenantBase
	{
		private AccountRequestLocation _billingLocation;
		private AccountRequestLocation _mailingLocation;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("Archived", Description = "Archived")]
		[Property("Archived", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Archived { get; set; }

		[EnableSnapShot("VerificationKey", Description = "Verification Key")]
		[Property("VerificationKey", AutoValueOnInsert = false, DataType = SqlDbType.UniqueIdentifier, Key = false)]
		public Guid VerificationKey { get; set; }

		[EnableSnapShot("ConfirmationNumber", Description = "Confirmation Number")]
		[Property("ConfirmationNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ConfirmationNumber { get; set; }

		[EnableSnapShot("DateCreated", Description = "Date Created")]
		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[EnableSnapShot("Name")]
		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

		[EnableSnapShot("DoingBusinessAs", Description = "Doing Business As")]
		[Property("DoingBusinessAs", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DoingBusinessAs { get; set; }

		[EnableSnapShot("TaxId", Description = "Tax Id")]
		[Property("TaxId", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string TaxId { get; set; }

		[EnableSnapShot("ReferredBy", Description = "Referred By")]
		[Property("ReferredBy", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ReferredBy { get; set; }

		[EnableSnapShot("Owner", Description = "Owner")]
		[Property("Owner", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Owner { get; set; }

		[EnableSnapShot("Website", Description = "Website")]
		[Property("Website", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Website { get; set; }

		[EnableSnapShot("InvoicingRequirements", Description = "Invoicing Requirements")]
		[Property("InvoicingRequirements", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string InvoicingRequirements { get; set; }

		[EnableSnapShot("BusinessContactName", Description = "Business Contact Name")]
		[Property("BusinessContactName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string BusinessContactName { get; set; }

		[EnableSnapShot("BusinessContactPhone", Description = "Business Contact Phone")]
		[Property("BusinessContactPhone", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string BusinessContactPhone { get; set; }

		[EnableSnapShot("BusinessContactFax", Description = "Business Contact Fax")]
		[Property("BusinessContactFax", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string BusinessContactFax { get; set; }

		[EnableSnapShot("BusinessContactEmail", Description = "Business Contact Email")]
		[Property("BusinessContactEmail", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string BusinessContactEmail { get; set; }
		
		[EnableSnapShot("AccountingContactName", Description = "Accounting Contact Name")]
		[Property("AccountingContactName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string AccountingContactName { get; set; }

		[EnableSnapShot("AccountingContactPhone", Description = "Accounting Contact Phone")]
		[Property("AccountingContactPhone", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string AccountingContactPhone { get; set; }

		[EnableSnapShot("AccountingContactFax", Description = "Accounting Contact Fax")]
		[Property("AccountingContactFax", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string AccountingContactFax { get; set; }

		[EnableSnapShot("AccountingContactEmail", Description = "Accounting Contact Email")]
		[Property("AccountingContactEmail", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string AccountingContactEmail { get; set; }
		
		[EnableSnapShot("TradeReference1Name", Description = "First Trade Reference Name")]
		[Property("TradeReference1Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string TradeReference1Name { get; set; }

		[EnableSnapShot("TradeReference1Phone", Description = "First Trade Reference Phone")]
		[Property("TradeReference1Phone", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string TradeReference1Phone { get; set; }

		[EnableSnapShot("TradeReference1Fax", Description = "First Trade Reference Fax")]
		[Property("TradeReference1Fax", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string TradeReference1Fax { get; set; }

		[EnableSnapShot("TradeReference1Email", Description = "First Trade Reference Email")]
		[Property("TradeReference1Email", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string TradeReference1Email { get; set; }
		
		[EnableSnapShot("TradeReference2Name", Description = "Second Trade Reference Name")]
		[Property("TradeReference2Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string TradeReference2Name { get; set; }

		[EnableSnapShot("TradeReference2Phone", Description = "Second Trade Reference Phone")]
		[Property("TradeReference2Phone", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string TradeReference2Phone { get; set; }

		[EnableSnapShot("TradeReference2Fax", Description = "Second Trade Reference Fax")]
		[Property("TradeReference2Fax", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string TradeReference2Fax { get; set; }

		[EnableSnapShot("TradeReference2Email", Description = "Second Trade Reference Email")]
		[Property("TradeReference2Email", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string TradeReference2Email { get; set; }
		
		[EnableSnapShot("TradeReference3Name", Description = "Third Trade Reference Name")]
		[Property("TradeReference3Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string TradeReference3Name { get; set; }

		[EnableSnapShot("TradeReference3Phone", Description = "Third Trade Reference Phone")]
		[Property("TradeReference3Phone", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string TradeReference3Phone { get; set; }

		[EnableSnapShot("TradeReference3Fax", Description = "Third Trade Reference Fax")]
		[Property("TradeReference3Fax", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string TradeReference3Fax { get; set; }

		[EnableSnapShot("TradeReference3Email", Description = "Third Trade Reference Email")]
		[Property("TradeReference3Email", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string TradeReference3Email { get; set; }
		
		[EnableSnapShot("BankReference1Name", Description = "First Bank Reference Name")]
		[Property("BankReference1Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string BankReference1Name { get; set; }

		[EnableSnapShot("BankReference1Phone", Description = "First Bank Reference Phone")]
		[Property("BankReference1Phone", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string BankReference1Phone { get; set; }

		[EnableSnapShot("BankReference1Fax", Description = "First Bank Reference Fax")]
		[Property("BankReference1Fax", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string BankReference1Fax { get; set; }

		[EnableSnapShot("BankReference1Email", Description = "First Bank Reference Email")]
		[Property("BankReference1Email", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string BankReference1Email { get; set; }
		
		[EnableSnapShot("BankReference2Name", Description = "Second Bank Reference Name")]
		[Property("BankReference2Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string BankReference2Name { get; set; }

		[EnableSnapShot("BankReference2Phone", Description = "Second Bank Reference Phone")]
		[Property("BankReference2Phone", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string BankReference2Phone { get; set; }

		[EnableSnapShot("BankReference2Fax", Description = "Second Bank Reference Fax")]
		[Property("BankReference2Fax", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string BankReference2Fax { get; set; }

		[EnableSnapShot("BankReference2Email", Description = "Second Bank Reference Email")]
		[Property("BankReference2Email", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string BankReference2Email { get; set; }

		[EnableSnapShot("BillingLocationId", Description = "Billing Location Reference")]
		[Property("BillingLocationId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		private long BillingLocationId { get; set; }

		[EnableSnapShot("MailingLocationId", Description = "Mailing Location Reference")]
		[Property("MailingLocationId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		private long MailingLocationId { get; set; }

		public AccountRequestLocation BillingLocation
		{
			get { return BillingLocationId == default(long) ? null : _billingLocation ?? (_billingLocation = new AccountRequestLocation(BillingLocationId)); }
			set
			{
				_billingLocation = value;
				BillingLocationId = value == null ? default(long) : value.Id;
			}
		}
		public AccountRequestLocation MailingLocation
		{
			get { return MailingLocationId == default(long) ? null : _mailingLocation ?? (_mailingLocation = new AccountRequestLocation(MailingLocationId)); }
			set
			{
				_mailingLocation = value;
				MailingLocationId = value == null ? default(long) : value.Id;
			}
		}

		public AccountRequest()
		{
		}

		public AccountRequest(long id)
		{
			Id = id;
			if (Id == default(long)) return;
			Load();
			TakeSnapShot();
		}
	}
}
