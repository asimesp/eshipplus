﻿using System;

namespace LogisticsPlus.Eship.Core
{
	[Serializable]
	public enum ProcQueObjType
	{
		//NOTE: DO NOT RE-ARRANGE
		ShipmentStatusQue = 0,
        ShipmentDocumentQue
	}
}
