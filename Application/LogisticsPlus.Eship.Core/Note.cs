﻿using System;
using System.Data;
using LogisticsPlus.Eship.Core.Administration;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core
{
	[Serializable]
	public class Note : TenantBase
	{
		[EnableSnapShot("Message")]
		[Property("Message", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Message { get; set; }

		[EnableSnapShot("Type", Description = "Note Type")]
		[Property("Type", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public NoteType Type { get; set; }

		[EnableSnapShot("Archived", Description = "Note Archived")]
		[Property("Archived", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Archived { get; set; }

		[EnableSnapShot("Classified", Description = "Note Classified")]
		[Property("Classified", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Classified { get; set; }

		[EnableSnapShot("UserId", Description = "User Reference")]
		[Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long UserId
		{
			get { return User == null ? default(long) : User.Id; }
			set
			{
				if (User != null && User.Id == value) return;
				User = value == default(long) ? null : new User(value);
			}
		}

		public User User { get; set; }
	}
}
