﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Administration;
using ObjToSql.Core;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core
{
	[Serializable]
    [Entity("AdminAuditLog", ReadOnly = false, Source = EntitySource.TableView)]
    public class AdminAuditLog : EntityBase
    {

        private long _adminUserId;

		private AdminUser _adminUser;

    	private readonly bool _keyLoaded;

	    [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[Property("EntityCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string EntityCode { get; set; }

		[Property("EntityId", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string EntityId { get; set; }

		[Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Description { get; set; }

		[Property("LogDateTime", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime LogDateTime { get; set; }

		[Property("AdminUserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long AdminUserId
		{
			get
			{
				if (_adminUser != null) _adminUserId = _adminUser.Id;
				return _adminUserId;
			}
			set
			{
				_adminUserId = value;
				if (_adminUser != null && value != _adminUser.Id) _adminUser = null;
			}
		}

		public AdminUser AdminUser
		{
			get { return _adminUser ?? (_adminUser = new AdminUser(_adminUserId)); }
			set
			{
				_adminUser = value;
				_adminUserId = value == null ? default(long) : value.Id;
			}
		}

		public void Log()
		{
			Insert();
		}

        public AdminAuditLog(long id)
        {
            Id = id;
            if (Id == default(long)) return;
            _keyLoaded = Load();
        }

		public AdminAuditLog(DbDataReader reader)
		{
			Load(reader);
		}
        
        public AdminAuditLog()
        {
            
        }

        public bool KeyLoaded
        {
            get { return _keyLoaded; }
        }
    }
}
