﻿namespace LogisticsPlus.Eship.Core
{
	public enum NoteType
	{
		Critical = 0,
		Normal,
		Finance,
		Operations,
	}
}
