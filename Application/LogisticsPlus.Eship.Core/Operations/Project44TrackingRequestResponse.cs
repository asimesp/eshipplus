﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("Project44TrackingUpdate", ReadOnly = false, Source = EntitySource.TableView)]
	public class Project44TrackingRequestResponse : ObjectBase
    {
        private long _shipmentId;
        private Shipment _shipment;

        [EnableSnapShot("Id", Description = "RequestData")]
        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; set; }


        [EnableSnapShot("ShipmentId", Description = "ShipmentId")]
        [Property("ShipmentId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long ShipmentId
        {
            get
            {
                if (_shipment != null) _shipmentId = _shipment.Id;
                return _shipmentId;
            }
            set
            {
                _shipmentId = value;
                if (_shipment != null && value != _shipment.Id) _shipment = null;
            }
        }

        [EnableSnapShot("RequestData", Description = "RequestData")]
        [Property("RequestData", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string RequestData { get; set; }

        [EnableSnapShot("ResponseData", Description = "ResponseData")]
        [Property("ResponseData", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ResponseData { get; set; }

        [EnableSnapShot("DateCreated", Description = "Date Created")]
        [Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime DateCreated { get; set; }

        [EnableSnapShot("Scac", Description = "Scac")]
        [Property("Scac", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Scac { get; set; }

        [EnableSnapShot("Project44Id", Description = "Project44Id")]
        [Property("Project44Id", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Project44Id { get; set; }

        public Shipment Shipment
        {
            get { return _shipmentId == default(long) ? null : _shipment ?? (_shipment = new Shipment(_shipmentId)); }
            set
            {
                _shipment = value;
                _shipmentId = value == null ? default(long) : value.Id;
            }
        }

        public bool IsNew
        {
            get { return Id == default(long); }
        }


        public Project44TrackingRequestResponse()
        {
        }


        public Project44TrackingRequestResponse(DbDataReader reader)
        {
            Load(reader);
        }


        public void Save()
        {
            if (Id == default(long)) Insert();
            else Update();
        }

        public new void Delete()
        {
            if (Id != default(long)) base.Delete();
        }
    }
}
