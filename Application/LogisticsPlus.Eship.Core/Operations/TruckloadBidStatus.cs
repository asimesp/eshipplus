﻿namespace LogisticsPlus.Eship.Core.Operations
{
	public enum TruckloadBidStatus
	{
		 // There are database queries that rely on this order there ADD TO, DO NOT RE-ORDER. 
		Processing = 0,
		Covered,
		Closed,
		NoMatch
	}
}
