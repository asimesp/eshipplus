﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("VendorPreferredLane", ReadOnly = false, Source = EntitySource.TableView)]
	public class VendorPreferredLane : TenantBase
	{
		private long _vendorId;
		private long _originCountryId;
		private long _destinationCountryId;

		private Country _destinationCountry;
		private Country _originCountry;
		private Vendor _vendor;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("OriginCity", Description = "Origin City")]
		[Property("OriginCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginCity { get; set; }

		[EnableSnapShot("OriginState", Description = "Origin State")]
		[Property("OriginState", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginState { get; set; }

		[EnableSnapShot("OriginCountryId", Description = "Origin Country Reference")]
		[Property("OriginCountryId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long OriginCountryId
		{
			get { return _originCountryId; }
			set
			{
				_originCountryId = value;
				if (_originCountry != null && value != _originCountry.Id) _originCountry = null;
			}
		}

		[EnableSnapShot("OriginPostalCode", Description = "Origin Postal Code")]
		[Property("OriginPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginPostalCode { get; set; }

		[EnableSnapShot("DestinationCity", Description = "Destination City")]
		[Property("DestinationCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationCity { get; set; }

		[EnableSnapShot("DestinationState", Description = "Destination State")]
		[Property("DestinationState", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationState { get; set; }

		[EnableSnapShot("DestinationCountryId", Description = "Destination Country Reference")]
		[Property("DestinationCountryId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long DestinationCountryId
		{
			get { return _destinationCountryId; }
			set
			{
				_destinationCountryId = value;
				if (_destinationCountry != null && value != _destinationCountry.Id) _destinationCountry = null;
			}
		}

		[EnableSnapShot("DestinationPostalCode", Description = "Destination Postal Code")]
		[Property("DestinationPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationPostalCode { get; set; }

		[EnableSnapShot("VendorId", Description = "Vendor Reference")]
		[Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long VendorId
		{
			get
			{
				if (_vendor != null) _vendorId = _vendor.Id;
				return _vendorId;
			}
			set
			{
				_vendorId = value;
				if (_vendor != null && value != _vendor.Id) _vendor = null;
			}
		}

		public Vendor Vendor
		{
			get{ return _vendorId == default(long) ? null : _vendor ?? (_vendor = new Vendor(_vendorId)); }
			set
			{
				_vendor = value;
				_vendorId = value == null ? default(long) : value.Id;
			}
		}
		public Country OriginCountry
		{
			get { return _originCountryId == default(long) ? null : _originCountry ?? (_originCountry = new Country(_originCountryId)); }
			set
			{
				_originCountry = value;
				_originCountryId = value == null ? default(long) : value.Id;
			}
		}
		public Country DestinationCountry
		{
			get { return _destinationCountryId == default(long) ? null : _destinationCountry ?? (_destinationCountry = new Country(_destinationCountryId)); }
			set
			{
				_destinationCountry = value;
				_destinationCountryId = value == null ? default(long) : value.Id;
			}
		}

		public string FullLane
		{
			get
			{
				return string.Format("{0} {1} {2} {3} - {4} {5} {6} {7}",
				                     OriginPostalCode, OriginCity, OriginState,
				                     OriginCountry == null ? string.Empty : OriginCountry.Name,
				                     DestinationPostalCode, DestinationCity, DestinationState,
				                     DestinationCountry == null ? string.Empty : DestinationCountry.Name
					);
			}
		}

		public bool IsNew { get { return Id == default(long); } }

		public VendorPreferredLane()
		{

		}

		public VendorPreferredLane(long id) : this(id, false)
		{
		}

		public VendorPreferredLane(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public VendorPreferredLane(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
