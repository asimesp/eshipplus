﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("ShipmentEquipment", ReadOnly = false, Source = EntitySource.TableView)]
	public class ShipmentEquipment : TenantBase
	{
		private long _shipmentId;
		private long _equipmentTypeId;

		private EquipmentType _equipmentType;
		private Shipment _shipment;

		[EnableSnapShot("EquipmentTypeId", Description = "Equipment Type Reference")]
		[Property("EquipmentTypeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long EquipmentTypeId
		{
			get
			{
				if (_equipmentType != null) _equipmentTypeId = _equipmentType.Id;
				return _equipmentTypeId;
			}
			set
			{
				_equipmentTypeId = value;
				if (_equipmentType != null && value != _equipmentType.Id) _equipmentType = null;
			}
		}

		[EnableSnapShot("ShipmentId", Description = "Shipment Reference")]
		[Property("ShipmentId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long ShipmentId
		{
			get
			{
				if (_shipment != null) _shipmentId = _shipment.Id;
				return _shipmentId;
			}
			set
			{
				_shipmentId = value;
				if (_shipment != null && value != _shipment.Id) _shipment = null;
			}
		}

		public Shipment Shipment
		{
			get { return _shipment ?? (_shipment = new Shipment(_shipmentId)); }
			set
			{
				_shipment = value;
				_shipmentId = value == null ? default(long) : value.Id;
			}
		}
		public EquipmentType EquipmentType
		{
			get { return _equipmentType ?? (_equipmentType = new EquipmentType(_equipmentTypeId)); }
			set
			{
				_equipmentType = value;
				_equipmentTypeId = value == null ? default(long) : value.Id;
			}
		}

		public ShipmentEquipment()
		{
		}

		public ShipmentEquipment(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			Insert();
		}

		public new void Delete()
		{
			base.Delete();
		}
	}
}
