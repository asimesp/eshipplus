﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Rating;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("ShipmentAutoRatingAccessorial", ReadOnly = false, Source = EntitySource.TableView)]
	public class ShipmentAutoRatingAccessorial : TenantBase
	{
		private long _shipmentId;

		private Shipment _shipment;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("ServiceDescription", Description = "Service Description")]
		[Property("ServiceDescription", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ServiceDescription { get; set; }

		[EnableSnapShot("RateType", Description = "Rate Type")]
		[Property("RateType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public RateType RateType { get; set; }

		[EnableSnapShot("Rate")]
		[Property("Rate", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal Rate { get; set; }

		[EnableSnapShot("BuyFloor", Description = "Buy Floor Value")]
		[Property("BuyFloor", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal BuyFloor { get; set; }

		[EnableSnapShot("BuyCeiling", Description = "Buy Ceiling Value")]
		[Property("BuyCeiling", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal BuyCeiling { get; set; }

		[EnableSnapShot("SellFloor", Description = "Sell Floor Value")]
		[Property("SellFloor", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal SellFloor { get; set; }

		[EnableSnapShot("SellCeiling", Description = "Sell Ceiling Value")]
		[Property("SellCeiling", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal SellCeiling { get; set; }

		[EnableSnapShot("MarkupValue", Description = "Markup Value")]
		[Property("MarkupValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal MarkupValue { get; set; }

		[EnableSnapShot("MarkupPercent", Description = "Markup Percent")]
		[Property("MarkupPercent", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal MarkupPercent { get; set; }

		[EnableSnapShot("UseLowerSell", Description = "Use Lower Sell")]
		[Property("UseLowerSell", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool UseLowerSell { get; set; }

		[EnableSnapShot("ShipmentId", Description = "Shipment Reference")]
		[Property("ShipmentId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long ShipmentId
		{
			get
			{
				if (_shipment != null) _shipmentId = _shipment.Id;
				return _shipmentId;
			}
			set
			{
				_shipmentId = value;
				if (_shipment != null && value != _shipment.Id) _shipment = null;
			}
		}

		public Shipment Shipment
		{
			get { return _shipment ?? (_shipment = new Shipment(_shipmentId)); }
			set
			{
				_shipment = value;
				_shipmentId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public ShipmentAutoRatingAccessorial() { }

		public ShipmentAutoRatingAccessorial(long id) : this(id, false) { }

		public ShipmentAutoRatingAccessorial(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public ShipmentAutoRatingAccessorial(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
