﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
    [Serializable]
    [Entity("VendorDispute", ReadOnly = false, Source = EntitySource.TableView)]
    public class VendorDispute : TenantBase
    {
        private long _vendorId;
        private long _createdByUserId;
        private long _resolvedByUserId;

        private Vendor _vendor;
        private User _createdByUser;
        private User _resolvedByUser;

        private List<VendorDisputeDetail> _vendorDisputeDetails;

        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; protected set; }

        [EnableSnapShot("DisputerReferenceNumber", Description = "Disputer Reference Number")]
        [Property("DisputerReferenceNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DisputerReferenceNumber { get; set; }

        [EnableSnapShot("VendorId", Description = "Vendor Id")]
        [Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long VendorId
        {
            get { return _vendorId; }
            set
            {
                _vendorId = value;
                if (_vendor != null && _vendor.Id != value) _vendor = null;
            }
        }

        [EnableSnapShot("CreatedByUserId", Description = "User Reference")]
        [Property("CreatedByUserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long CreatedByUserId
        {
            get { return _createdByUserId; }
            set
            {
                _createdByUserId = value;
                if (_createdByUser != null && value != _createdByUser.Id) _createdByUser = null;
            }
        }

        [EnableSnapShot("DateCreated", Description = "Date Created")]
        [Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime DateCreated { get; set; }

        [EnableSnapShot("IsResolved", Description = "Is Resolved")]
        [Property("IsResolved", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool IsResolved { get; set; }

        [EnableSnapShot("ResolutionDate", Description = "Resolution Date")]
        [Property("ResolutionDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime ResolutionDate { get; set; }

        [EnableSnapShot("ResolutionComments")]
        [Property("ResolutionComments", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ResolutionComments { get; set; }

		[EnableSnapShot("DisputeComments")]
		[Property("DisputeComments", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DisputeComments { get; set; }

        [EnableSnapShot("ResolvedByUserId", Description = "User Reference")]
        [Property("ResolvedByUserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long ResolvedByUserId
        {
            get { return _resolvedByUserId; }
            set
            {
                _resolvedByUserId = value;
                if (_resolvedByUser != null && value != _resolvedByUser.Id) _resolvedByUser = null;
            }
        }

        public List<VendorDisputeDetail> VendorDisputeDetails
        {
            get
            {
                if (_vendorDisputeDetails == null && Id != default(long)) LoadVendorDisputeDetails();
                return _vendorDisputeDetails;
            }
            set { _vendorDisputeDetails = value; }
        }

        public Vendor Vendor
        {
            get { return _vendor ?? (_vendor = new Vendor(_vendorId)); }
            set
            {
                _vendor = value;
                _vendorId = value == null ? default(long) : value.Id;
            }
        }

        public User CreatedByUser
        {
            get { return _createdByUserId == default(long) ? null : _createdByUser ?? (_createdByUser = new User(_createdByUserId, false)); }
            set
            {
                _createdByUser = value;
                _createdByUserId = value == null ? default(long) : value.Id;
            }
        }

        public User ResolvedByUser
        {
            get { return _resolvedByUserId == default(long) ? null : _resolvedByUser ?? (_resolvedByUser = new User(_resolvedByUserId, false)); }
            set
            {
                _resolvedByUser = value;
                _resolvedByUserId = value == null ? default(long) : value.Id;
            }
        }

        public bool IsNew
        {
            get { return Id == default(long); }
        }

        public VendorDispute() { }

        public VendorDispute(long id) : this(id, false) { }

        public VendorDispute(long id, bool takeSnapshot)
        {
            Id = id;
            if (Id == default(long)) return;
            KeyLoaded = Load();
            if (takeSnapshot) TakeSnapShot();
        }

        public VendorDispute(DbDataReader reader)
        {
            Load(reader);
        }

        public void Save()
        {
            if (Id == default(long)) Insert();
            else Update();
        }

        public new void Delete()
        {
            if (Id != default(long))
                base.Delete();
        }

		public void LoadCollection()
		{
			LoadVendorDisputeDetails();
		}

        private void LoadVendorDisputeDetails()
        {
            _vendorDisputeDetails = new List<VendorDisputeDetail>();
            const string query = @"SELECT * FROM VendorDisputeDetail WHERE DisputedId = @DisputedId AND TenantId = @TenantId;";
            var parameters = new Dictionary<string, object> { { "DisputedId", Id }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _vendorDisputeDetails.Add(new VendorDisputeDetail(reader));
            Connection.Close();
        }
    }
}