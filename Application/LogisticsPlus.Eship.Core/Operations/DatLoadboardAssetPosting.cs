﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Dat;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
    [Serializable]
    [Entity("DatLoadboardAssetPosting", ReadOnly = false, Source = EntitySource.TableView)]
    public class DatLoadboardAssetPosting : TenantBase
    {
        private long _userId;
        private User _user;

        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; protected set; }

        [EnableSnapShot("IdNumber", Description = "Id Number")]
        [Property("IdNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string IdNumber { get; set; }

        [EnableSnapShot("DateCreated", Description = "Date Created")]
        [Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime DateCreated { get; set; }

        [EnableSnapShot("ExpirationDate", Description = "Expiration Date")]
        [Property("ExpirationDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime ExpirationDate { get; set; }

        [EnableSnapShot("AssetId", Description = "Asset Id")]
        [Property("AssetId", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string AssetId { get; set; }

        [EnableSnapShot("HazardousMaterial", Description = "Is Hazardous Material")]
        [Property("HazardousMaterial", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool HazardousMaterial { get; set; }

        [EnableSnapShot("BaseRate", Description = "Base Rate")]
        [Property("BaseRate", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal BaseRate { get; set; }

        [EnableSnapShot("RateType", Description = "Rate Type")]
        [Property("RateType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public RateBasedOnType RateType { get; set; }

        [EnableSnapShot("Mileage", Description = "Mileage")]
        [Property("Mileage", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal Mileage { get; set; }

        [EnableSnapShot("UserId", Description = "User Reference")]
        [Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long UserId
        {
            get { return _userId; }
            set
            {
                _userId = value;
                if (_user != null && value != _user.Id) _user = null;
            }
        }

        [EnableSnapShot("SerializedComments", Description = "Serialized Comments")]
        [Property("SerializedComments", AutoValueOnInsert = false, DataType = SqlDbType.Text, Key = false)]
        public string SerializedComments { get; set; }

        public List<string> Comments
        {
            get { return SerializedComments.FromXml<List<string>>() ?? new List<string>(); }
        } 


        public User User
        {
            get { return _userId == default(long) ? null : _user ?? (_user = new User(_userId, false)); }
            set
            {
                _user = value;
                _userId = value == null ? default(long) : value.Id;
            }
        }

        public decimal Rate
        {
            get
            {
                return RateType == RateBasedOnType.Flat
                           ? BaseRate
                           : RateType == RateBasedOnType.PerMile
                                 ? BaseRate*Mileage
                                 : 0m;
            }
        }

		public bool IsNew
		{
			get { return Id == default(long); }
		}

        public DatLoadboardAssetPosting() { }

		public DatLoadboardAssetPosting(long id) : this(id, false) { }

		public DatLoadboardAssetPosting(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

        public DatLoadboardAssetPosting(DbDataReader reader)
		{
			Load(reader);
		}

        public DatLoadboardAssetPosting(string assetId, long tenantId)
		{
            const string query = @"Select * from DatLoadboardAssetPosting where AssetId = @AssetId And TenantId = @TenantId";
            var parameters = new Dictionary<string, object> { { "TenantId", tenantId }, { "AssetId", assetId } };
			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					Load(reader);
			Connection.Close();
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
    }
}
