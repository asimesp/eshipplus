﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("AddressBookService", ReadOnly = false, Source = EntitySource.TableView)]
	public class AddressBookService : TenantBase
	{
		private long _serviceId;
		private long _addressBookId;

		private AddressBook _addressBook;
		private Service _service;

		[EnableSnapShot("ServiceId", Description = "Service Reference")]
		[Property("ServiceId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ServiceId
		{
			get { return _serviceId; }
			set
			{
				_serviceId = value;
				if (_service != null && value != _service.Id) _service = null;
			}
		}

		[EnableSnapShot("AddressBookId", Description = "Address Book Reference")]
		[Property("AddressBookId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long AddressBookId
		{
			get
			{
				if (_addressBook != null) _addressBookId = _addressBook.Id;
				return _addressBookId;
			}
			set
			{
				_addressBookId = value;
				if (_addressBook != null && value != _addressBook.Id) _addressBook = null;
			}
		}

		public AddressBook AddressBook
		{
			get { return _addressBook ?? (_addressBook = new AddressBook(_addressBookId)); }
			set
			{
				_addressBook = value;
				_addressBookId = value == null ? default(long) : value.Id;
			}
		}
		public Service Service
		{
			get { return _serviceId == default(long) ? null : _service ?? (_service = new Service(_serviceId, false)); }
			set
			{
				_service = value;
				_serviceId = value == null ? default(long) : value.Id;
			}
		}

        public AddressBookService() { }

		public AddressBookService(DbDataReader reader)
		{
            Load(reader);
		}

        public void Save()
        {
            Insert();
        }

        public new void Delete()
        {
            base.Delete();
        }
	}
}
