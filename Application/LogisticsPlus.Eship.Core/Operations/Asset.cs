﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Cache;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("Asset", ReadOnly = false, Source = EntitySource.TableView)]
	public class Asset : TenantBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("AssetNumber", Description = "Customer Number")]
		[Property("AssetNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string AssetNumber { get; set; }

		[EnableSnapShot("DateCreated", Description = "Date Created")]
		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[EnableSnapShot("AssetType", Description = "Asset Type")]
		[Property("AssetType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public AssetType AssetType { get; set; }

		[EnableSnapShot("Active")]
		[Property("Active", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Active { get; set; }

		[EnableSnapShot("Description", Description = "Description")]
		[Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Description { get; set; }


		public bool IsNew
		{
			get { return Id == default(long); }
		}

		
		public Asset() { }

		public Asset(long id)
			: this(id, false)
		{
		}

		public Asset(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			if (CoreCache.Assets.ContainsKey(Id))
			{
				Copy(CoreCache.Assets[Id]);
				KeyLoaded = true;
			}
			else KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public Asset(DbDataReader reader)
		{
			Load(reader);
		}

		
		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long)) base.Delete();
		}

		public new void TakeSnapShot()
		{
			GetSnapShot();
		}
	}
}
