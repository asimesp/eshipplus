﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("LoadOrderContact", ReadOnly = false, Source = EntitySource.TableView)]
	public class LoadOrderContact : Contact
	{
		private long _loadOrderLocationId;

		private LoadOrderLocation _loadOrderLocation;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[Property("LoadOrderLocationId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long LoadOrderLocationId
		{
			get
			{
				if (_loadOrderLocation != null) _loadOrderLocationId = _loadOrderLocation.Id;
				return _loadOrderLocationId;
			}
			set
			{
				_loadOrderLocationId = value;
				if (_loadOrderLocation != null && value != _loadOrderLocation.Id) _loadOrderLocation = null;
			}
		}

		public LoadOrderLocation Location
		{
			get { return _loadOrderLocation ?? (_loadOrderLocation = new LoadOrderLocation(_loadOrderLocationId)); }
			set
			{
				_loadOrderLocation = value;
				_loadOrderLocationId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew { get { return Id == default(long); } }

		public LoadOrderContact()
		{
			
		}

		public LoadOrderContact(long id) : this(id, false)
		{
		}

		public LoadOrderContact(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public LoadOrderContact(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}