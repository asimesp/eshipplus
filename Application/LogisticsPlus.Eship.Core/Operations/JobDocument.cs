﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("JobDocument", ReadOnly = false, Source = EntitySource.TableView)]
	public class JobDocument : Document
	{
		private long _jobId;
		private Job _job;


		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[Property("JobId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long JobId
		{
			get
			{
				if (_job != null) _jobId = _job.Id;
				return _jobId;
			}
			set
			{
				_jobId = value;
				if (_job != null && value != _job.Id) _job = null;
			}
		}

		[EnableSnapShot("DateCreated", Description = "Date Created")]
		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }
		

		public Job Job
		{
			get { return _job ?? (_job = new Job(_jobId)); }
			set
			{
				_job = value;
				_jobId = value == null ? default(long) : value.Id;
			}
		}
		

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public JobDocument() { }

		public JobDocument(long id) : this(id, false) { }

		public JobDocument(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public JobDocument(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

	}
	

	
}
