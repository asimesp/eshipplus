﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("LibraryItem", ReadOnly = false, Source = EntitySource.TableView)]
	public class LibraryItem : TenantBase
	{
		private long _packageTypeId;

		private PackageType _packageType;
		private Customer _customer;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("Description")]
		[Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Description { get; set; }

		[EnableSnapShot("FreightClass", Description = "Freight Class")]
		[Property("FreightClass", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
		public double FreightClass { get; set; }

		[EnableSnapShot("Comment")]
		[Property("Comment", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Comment { get; set; }

		[EnableSnapShot("Weight")]
		[Property("Weight", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal Weight { get; set; }

		[EnableSnapShot("Length")]
		[Property("Length", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal Length { get; set; }

		[EnableSnapShot("Width")]
		[Property("Width", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal Width { get; set; }

		[EnableSnapShot("Height")]
		[Property("Height", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal Height { get; set; }

		[EnableSnapShot("IsStackable")]
		[Property("IsStackable", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool IsStackable { get; set; }

        [EnableSnapShot("HazardousMaterial", Description = "Is Hazardous Material")]
        [Property("HazardousMaterial", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool HazardousMaterial { get; set; }

		[EnableSnapShot("Quantity", Description = "Package Quantity")]
		[Property("Quantity", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int Quantity { get; set; }

		[EnableSnapShot("PieceCount", Description = "Piece Count")]
		[Property("PieceCount", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int PieceCount { get; set; }

		[EnableSnapShot("PackageTypeId", Description = "Package Type Reference")]
		[Property("PackageTypeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long PackageTypeId
		{
			get { return _packageTypeId; }
			set
			{
				_packageTypeId = value;
				if (_packageType != null && value != _packageType.Id) _packageType = null;
			}
		}

		[EnableSnapShot("NMFCCode", Description = "National Motor Freight Classification Code")]
		[Property("NMFCCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string NMFCCode { get; set; }

		[EnableSnapShot("HTSCode", Description = "Harmonized Tarriff Schedule Code")]
		[Property("HTSCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string HTSCode { get; set; }

		[EnableSnapShot("CustomerId", Description = "Customer Reference")]
		[Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		private long CustomerId { get; set; }

		public Customer Customer
		{
			get { return CustomerId == default(long) ? null : _customer ?? (_customer = new Customer(CustomerId)); }
			set
			{
				_customer = value;
				CustomerId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public PackageType PackageType
		{
			get { return _packageTypeId == default(long) ? null : _packageType ?? (_packageType = new PackageType(_packageTypeId)); }
			set
			{
				_packageType = value;
				_packageTypeId = value == null ? default(long) : value.Id;
			}
		}

		public LibraryItem(long id)
			: this(id, false)
		{
		}

		public LibraryItem() { }

		public LibraryItem(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public LibraryItem(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long)) base.Delete();
		}
	}
}
