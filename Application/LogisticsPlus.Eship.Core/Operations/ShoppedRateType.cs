﻿namespace LogisticsPlus.Eship.Core.Operations
{
    public enum ShoppedRateType
    {
        Quote = 0,
        Shipment,
        WSBooking,
        WSEstimate,
		Estimate,
    }
}
