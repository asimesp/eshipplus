﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("VendorRejectionLog", Source = EntitySource.TableView, ReadOnly = false)]
	public class VendorRejectionLog : TenantBase
	{
		private long _vendorId;
		private long _userId;
		private long _customerId;

		private Customer _customer;
		private User _user;
		private Vendor _vendor;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[Property("ShipmentNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false, Column = "LoadShipmentNumber")]
		public string ShipmentNumber { get; set; }

		[Property("ShipmentDateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false, Column = "LoadShipmentDateCreated")]
		public DateTime ShipmentDateCreated { get; set; }

		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[Property("OriginCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginCity { get; set; }

		[Property("OriginState", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginState { get; set; }

		[Property("OriginPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginPostalCode { get; set; }

		[Property("OriginCountryCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginCountryCode { get; set; }

		[Property("DestinationCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationCity { get; set; }

		[Property("DestinationState", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationState { get; set; }

		[Property("DestinationPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationPostalCode { get; set; }

		[Property("DestinationCountryCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationCountryCode { get; set; }

		[Property("ServiceMode", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ServiceMode ServiceMode { get; set; }

		[Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long VendorId
		{
			get
			{
				if (_vendor != null) _vendorId = _vendor.Id;
				return _vendorId;
			}
			set
			{
				_vendorId = value;
				if (_vendor != null && value != _vendor.Id) _vendor = null;
			}
		}

		[Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long UserId
		{
			get
			{
				if (_user != null) _userId = _user.Id;
				return _userId;
			}
			set
			{
				_userId = value;
				if (_user != null && value != _user.Id) _user = null;
			}
		}

		[Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long CustomerId
		{
			get
			{
				if (_customer != null) _customerId = _customer.Id;
				return _customerId;
			}
			set
			{
				_customerId = value;
				if (_customer != null && value != _customer.Id) _customer = null;
			}
		}

		public Vendor Vendor
		{
			get { return _vendorId == default(long) ? null : _vendor ?? (_vendor = new Vendor(_vendorId)); }
			set
			{
				_vendor = value;
				_vendorId = value == null ? default(long) : value.Id;
			}
		}
		public User User
		{
			get { return _userId == default(long) ? null : _user ?? (_user = new User(_userId)); }
			set
			{
				_user = value;
				_userId = value == null ? default(long) : value.Id;
			}
		}
		public Customer Customer
		{
			get { return _customerId == default(long) ? null : _customer ?? (_customer = new Customer(_customerId)); }
			set
			{
				_customer = value;
				_customerId = value == null ? default(long) : value.Id;
			}
		}


		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public VendorRejectionLog() { }

		public VendorRejectionLog(long id)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
		}

		public VendorRejectionLog(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			Insert();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
