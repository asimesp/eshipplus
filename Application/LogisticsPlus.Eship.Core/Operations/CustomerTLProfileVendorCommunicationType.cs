﻿namespace LogisticsPlus.Eship.Core.Operations
{
    public enum CustomerTLProfileVendorCommunicationType
    {
        // DO NOT RE-ORDER!
        Edi = 0,
        Email
    }
}
