﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Administration;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
    [Entity("CheckCall", ReadOnly = false, Source = EntitySource.TableView)]
    public class CheckCall : TenantBase
    {
		private long _shipmentId;
		private long _userId;

		private Shipment _shipment;
		private User _user;


        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; protected set; }

        [EnableSnapShot("ShipmentId", Description = "Shipment Reference")]
        [Property("ShipmentId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long ShipmentId
        {
            get
            {
                if (_shipment != null) _shipmentId = _shipment.Id;
                return _shipmentId;
            }
            set
            {
                _shipmentId = value;
                if (_shipment != null && value != _shipment.Id) _shipment = null;
            }
        }

        [EnableSnapShot("UserId", Description = "User Reference")]
        [Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        protected long UserId
        {
            get { return _userId; }
            set
            {
                _userId = value;
                if (_user != null && value != _user.Id) _user = null;
            }
        }

        [EnableSnapShot("CallDate", Description = "Call Date")]
        [Property("CallDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime CallDate { get; set; }

		[EnableSnapShot("EventDate", Description = "Call Date")]
		[Property("EventDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime EventDate { get; set; }

        [EnableSnapShot("CallNotes", Description = "Call Notes")]
        [Property("CallNotes", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string CallNotes { get; set; }

		[EnableSnapShot("EdiStatusCode", Description = "Edi Status Code")]
		[Property("EdiStatusCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string EdiStatusCode { get; set; }


        public Shipment Shipment
        {
            get { return _shipment ?? (_shipment = new Shipment(_shipmentId)); }
            set
            {
                _shipment = value;
                _shipmentId = value == null ? default(long) : value.Id;
            }
        }
        public User User
        {
            get { return _userId == default(long) ? null : _user ?? (_user = new User(_userId, false)); }
            set
            {
                _user = value;
                _userId = value == null ? default(long) : value.Id;
            }
        }

        public bool IsNew
        {
            get { return Id == default(long); }
        }

        public CheckCall() { }

        public CheckCall(long id) : this(id, false) { }

        public CheckCall(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

        public CheckCall(DbDataReader reader)
        {
            Load(reader);
        }

        public void Save()
        {
            if (Id == default(long)) Insert();
            else Update();
        }

        public new void Delete()
        {
            if (Id != default(long))
                base.Delete();
        }

    }
}
