﻿namespace LogisticsPlus.Eship.Core.Operations
{
	public enum ChargeStatus
	{
        // DO RE-ORDER. DATABASE NOT CONTAIN THAT VALUE
        Billed = 0,
        Open,
        Disputed
    }
}
