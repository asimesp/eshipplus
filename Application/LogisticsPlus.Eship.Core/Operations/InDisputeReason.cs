﻿namespace LogisticsPlus.Eship.Core.Operations
{
    public enum InDisputeReason
    {
        // DO NOT REORDER. SEE APPLICABLE JOINS ON DATABASE REFINDISPUTEREASON
        NotApplicable = 0,
        CustomerIssue,
        VendorIssue,
        WaitingOnOperator,
        Other
    }
}
