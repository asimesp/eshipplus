﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("ServiceTicketAsset", ReadOnly = false, Source = EntitySource.TableView)]
	public class ServiceTicketAsset : TenantBase
	{
		private long _serviceTicketId;
        private long _driverAssetId;
        private long _tractorAssetId;
        private long _trailerAssetId;

		private ServiceTicket _serviceTicket;
        private Asset _driverAsset;
        private Asset _tractorAsset;
        private Asset _trailerAsset;

        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; protected set; }

		[EnableSnapShot("ServiceTicketId", Description = "Service Ticket Reference")]
		[Property("ServiceTicketId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ServiceTicketId
		{
			get
			{
				if (_serviceTicket != null) _serviceTicketId = _serviceTicket.Id;
				return _serviceTicketId;
			}
			set
			{
				_serviceTicketId = value;
				if (_serviceTicket != null && value != _serviceTicket.Id) _serviceTicket = null;
			}
		}

        [EnableSnapShot("DriverAssetId", Description = "Driver Asset Reference")]
		[Property("DriverAssetId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long DriverAssetId
		{
			get { return _driverAssetId; }
			set
			{
				_driverAssetId = value;
				if (_driverAsset != null && _driverAsset.Id != value) _driverAsset = null;
			}
		}

		[EnableSnapShot("TractorAssetId", Description = "Tractor Asset Reference")]
		[Property("TractorAssetId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long TractorAssetId
		{
			get { return _tractorAssetId; }
			set
			{
				_tractorAssetId = value;
				if (_tractorAsset != null && _tractorAsset.Id != value) _tractorAsset = null;
			}
		}

		[EnableSnapShot("TrailerAssetId", Description = "Trailer Asset Reference")]
		[Property("TrailerAssetId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long TrailerAssetId
		{
			get { return _trailerAssetId; }
			set
			{
				_trailerAssetId = value;
				if (_trailerAsset != null && _trailerAsset.Id != value) _trailerAsset = null;
			}
		}

        [EnableSnapShot("MilesRun", Description = "Miles Run")]
        [Property("MilesRun", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal MilesRun { get; set; }

        [EnableSnapShot("Primary", Description = "Default Asset Designation")]
        [Property("Primary", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool Primary { get; set; }

        public Asset DriverAsset
        {
            get
            {
                return _driverAssetId == default(long) ? null : _driverAsset ?? (_driverAsset = new Asset(_driverAssetId, false));
            }
            set
            {
                _driverAsset = value;
                _driverAssetId = value == null ? default(long) : value.Id;
            }
        }
        public Asset TractorAsset
        {
            get
            {
                return _tractorAssetId == default(long) ? null : _tractorAsset ?? (_tractorAsset = new Asset(_tractorAssetId, false));
            }
            set
            {
                _tractorAsset = value;
                _tractorAssetId = value == null ? default(long) : value.Id;
            }
        }
        public Asset TrailerAsset
        {
            get
            {
                return _trailerAssetId == default(long) ? null : _trailerAsset ?? (_trailerAsset = new Asset(_trailerAssetId, false));
            }
            set
            {
                _trailerAsset = value;
                _trailerAssetId = value == null ? default(long) : value.Id;
            }
        }

		public ServiceTicket ServiceTicket
		{
			get { return _serviceTicket ?? (_serviceTicket = new ServiceTicket(_serviceTicketId)); }
			set
			{
				_serviceTicket = value;
				_serviceTicketId = value == null ? default(long) : value.Id;
			}
		}

        public bool IsNew
        {
            get { return Id == default(long); }
        }

		public ServiceTicketAsset(){ }

        public ServiceTicketAsset(long id) : this(id, false) { }

		public ServiceTicketAsset(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}


		public ServiceTicketAsset(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
            if (Id == default(long)) Insert();
            else Update();
		}

		public new void Delete()
		{
            if (Id != default(long))
                base.Delete();
		}
	}
}