﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("SMC3DispatchRequestResponse", ReadOnly = false, Source = EntitySource.TableView)]
    public class SMC3DispatchRequestResponse : SMC3RequestResponse
    {
        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

        public bool IsNew
		{
			get { return Id == default(long); }
		}


        public SMC3DispatchRequestResponse()
        {
        }


        public SMC3DispatchRequestResponse(DbDataReader reader)
		{
			Load(reader);
		}

		
		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long)) base.Delete();
		}
	}
}
