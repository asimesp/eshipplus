﻿namespace LogisticsPlus.Eship.Core.Operations
{
	public enum LoadOrderStatus
	{
        // There are database queries that rely on this order there ADD TO, DO NOT RE-ORDER. SEE APPLICABLE JOINS ON DATABASE REFLOADORDERSTATUS
		Offered = 0,
		Quoted,
		Cancelled,
		Covered,
		Lost,
		Accepted = 5,
		Shipment
	}
}
