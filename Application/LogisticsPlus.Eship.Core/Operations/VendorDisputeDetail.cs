﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;
using LogisticsPlus.Eship.Core.Registry;
using System.Collections.Generic;

namespace LogisticsPlus.Eship.Core.Operations
{
    [Serializable]
    [Entity("VendorDisputeDetail", ReadOnly = false, Source = EntitySource.TableView)]
    public class VendorDisputeDetail : TenantBase
    {
        private long _vendorDisputedId;

        private VendorDispute _vendorDisputed;
        private ChargeCode _chargeCode;
        private string _componentNumber;

        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; protected set; }

        [EnableSnapShot("DisputedId", Description = "Vendor Disputed Id")]
        [Property("DisputedId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long DisputedId
        {
            get { return _vendorDisputedId; }
            set
            {
                _vendorDisputedId = value;
                if (_vendorDisputed != null && _vendorDisputed.Id != value) _vendorDisputed = null;
            }
        }

        [EnableSnapShot("OriginalChargeAmount", Description = "Original Charge Amount")]
        [Property("OriginalChargeAmount", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal OriginalChargeAmount { get; set; }

        [EnableSnapShot("RevisedChargeAmount", Description = "Revised Charge Amount")]
        [Property("RevisedChargeAmount", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal RevisedChargeAmount { get; set; }


        [EnableSnapShot("ChargeLineIdType", Description = "Charge Line Type, It may shipment or service ticket")]
        [Property("ChargeLineIdType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public ChargeLineType ChargeLineIdType { get; set; }

        [EnableSnapShot("ChargeLineId", Description = "Charge Line Id. It may be shipment Id or serviceticket Id associated with charge.")]
        [Property("ChargeLineId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long ChargeLineId { get; set; }

        public VendorDispute VendorDispute
        {
            get { return _vendorDisputed ?? (_vendorDisputed = new VendorDispute(_vendorDisputedId)); }
            set
            {
                _vendorDisputed = value;
                _vendorDisputedId = value == null ? default(long) : value.Id;
            }
        }

        public ChargeCode ChargeCode
        {
            get { LoadChargeCode(); return _chargeCode; }
            private set
            {
                _chargeCode = value;
            }
        }

        public string ComponentNumber
        {
            get { LoadChargeComponent(); return _componentNumber; }
            private set
            {
                _componentNumber = value;
            }
        }


        public bool IsNew
        {
            get { return Id == default(long); }
        }

        public VendorDisputeDetail() { }

        public VendorDisputeDetail(long id) : this(id, false) { }

        public VendorDisputeDetail(long id, bool takeSnapshot)
        {
            Id = id;
            if (Id == default(long)) return;
            KeyLoaded = Load();
            if (takeSnapshot) TakeSnapShot();
        }

        public VendorDisputeDetail(DbDataReader reader)
        {
            Load(reader);
        }

        public void Save()
        {
            if (Id == default(long)) Insert();
            else Update();
        }

        public new void Delete()
        {
            if (Id != default(long))
                base.Delete();
        }

        private void LoadChargeCode()
        {
            string query =
                string.Format(@"SELECT * FROM {0} sc, ChargeCode cc WHERE sc.ChargeCodeId = cc.Id AND sc.Id = @Id AND cc.TenantId = @TenantId;",
                ChargeLineIdType == ChargeLineType.Shipment ? "ShipmentCharge" : "ServiceTicketCharge");
            var parameters = new Dictionary<string, object> { { "Id", ChargeLineId }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _chargeCode = new ChargeCode(reader);
            Connection.Close();
        }

        private void LoadChargeComponent()
        {
            Shipment shipment;
            ServiceTicket serviceTicket;

            string query =
                string.Format(@"SELECT * FROM {0} s, {1} sc WHERE sc.Id = @Id AND sc.{2} = s.Id AND s.TenantId = @TenantId;",
                ChargeLineIdType == ChargeLineType.Shipment ? "Shipment" : "ServiceTicket",
                ChargeLineIdType == ChargeLineType.Shipment ? "ShipmentCharge" : "ServiceTicketCharge",
                ChargeLineIdType == ChargeLineType.Shipment ? "ShipmentId" : "ServiceTicketId");
            var parameters = new Dictionary<string, object> { { "Id", ChargeLineId }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
            {
                while (reader.Read())
                {
                    if (ChargeLineIdType == ChargeLineType.Shipment)
                    {
                        shipment = new Shipment(reader);
                        ComponentNumber = shipment.ShipmentNumber;
                    }
                    else
                    {
                        serviceTicket = new ServiceTicket(reader);
                        ComponentNumber = serviceTicket.ServiceTicketNumber;
                    }
                }
            }
            Connection.Close();
        }
    }
}
