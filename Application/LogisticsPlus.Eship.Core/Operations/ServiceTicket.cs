﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("ServiceTicket", ReadOnly = false, Source = EntitySource.TableView)]
	public class ServiceTicket : TenantBase
	{
		private long _prefixId;
		private long _customerId;
		private long _userId;
		private long _accountBucketId;
		private long _salesRepresentativeId;
		private long _resellerAdditionId;
		private long _accountBucketUnitId;
		private long _overrideCustomerLocationId;
        private long _jobId;

		private CustomerLocation _overrideCustomerLocation;
		private Customer _customer;
		private ResellerAddition _resellerAddition;
		private AccountBucket _accountBucket;
		private Prefix _prefix;
		private User _user;
		private SalesRepresentative _salesRepresentative;
		private AccountBucketUnit _accountBucketUnit;
        private Job _job;

		private List<ServiceTicketVendor> _vendors;
		private List<ServiceTicketNote> _notes;
		private List<ServiceTicketItem> _items;
		private List<ServiceTicketCharge> _charges;
		private List<ServiceTicketDocument> _documents;
		private List<ServiceTicketEquipment> _equipments;
		private List<ServiceTicketService> _services;
		private List<ServiceTicketAsset> _assets;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("ServiceTicketNumber", Description = "Service Ticket Number")]
		[Property("ServiceTicketNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ServiceTicketNumber { get; set; }

		[EnableSnapShot("PrefixId", Description = "Prefix Reference")]
		[Property("PrefixId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long PrefixId
		{
			get { return _prefixId; }
			set
			{
				_prefixId = value;
				if (_prefix != null && value != _prefix.Id) _prefix = null;
			}
		}

		[EnableSnapShot("HidePrefix", Description = "Hide Prefix")]
		[Property("HidePrefix", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool HidePrefix { get; set; }

		[EnableSnapShot("AuditedForInvoicing", Description = "Audited For Invoicing")]
		[Property("AuditedForInvoicing", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool AuditedForInvoicing { get; set; }
		
		[EnableSnapShot("DateCreated", Description = "Date Created")]
		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[EnableSnapShot("TicketDate", Description = "Ticket Date")]
		[Property("TicketDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime TicketDate { get; set; }

		[EnableSnapShot("Status", Description = "Service Ticket Status")]
		[Property("Status", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ServiceTicketStatus Status { get; set; }

		[EnableSnapShot("AccountBucketId", Description = "Account Bucket Reference")]
		[Property("AccountBucketId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long AccountBucketId
		{
			get { return _accountBucketId; }
			set
			{
				_accountBucketId = value;
				if (_accountBucket != null && value != _accountBucket.Id) _accountBucket = null;
			}
		}

		[EnableSnapShot("UserId", Description = "User Reference")]
		[Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long UserId
		{
			get { return _userId; }
			set
			{
				_userId = value;
				if (_user != null && value != _user.Id) _user = null;
			}
		}

		[EnableSnapShot("CustomerId", Description = "Customer Reference")]
		[Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long CustomerId
		{
			get { return _customerId; }
			set
			{
				_customerId = value;
				if (_customer != null && value != _customer.Id) _customer = null;
			}
		}

		[EnableSnapShot("SalesRepresentativeId", Description = "Sales Representative Reference")]
		[Property("SalesRepresentativeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long SalesRepresentativeId
		{
			get { return _salesRepresentativeId; }
			set
			{
				_salesRepresentativeId = value;
				if (_salesRepresentative != null && _salesRepresentative.Id != value) _salesRepresentative = null;
			}
		}

		[EnableSnapShot("SalesRepresentativeCommissionPercent")]
		[Property("SalesRepresentativeCommissionPercent", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal SalesRepresentativeCommissionPercent { get; set; }

		[EnableSnapShot("SalesRepMinCommValue", Description = "Sales Representative Minimum Commission Value")]
		[Property("SalesRepMinCommValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal SalesRepMinCommValue { get; set; }

		[EnableSnapShot("SalesRepMaxCommValue", Description = "Sales Representative Maximum Commission Value")]
		[Property("SalesRepMaxCommValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal SalesRepMaxCommValue { get; set; }

		[EnableSnapShot("SalesRepAddlEntityName", Description = "Sales Representative Additional Entity Name")]
		[Property("SalesRepAddlEntityName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string SalesRepAddlEntityName { get; set; }

		[EnableSnapShot("SalesRepAddlEntityCommPercent", Description = "Sales Representative Additional Entity Commission Percent")]
		[Property("SalesRepAddlEntityCommPercent", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal SalesRepAddlEntityCommPercent { get; set; }

		[EnableSnapShot("ResellerAdditionId", Description = "Reseller Addittion Reference")]
		[Property("ResellerAdditionId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ResellerAdditionId
		{
			get { return _resellerAdditionId; }
			set
			{
				_resellerAdditionId = value;
				if (_resellerAddition != null && _resellerAddition.Id != value) _resellerAddition = null;
			}
		}

		[EnableSnapShot("BillReseller", Description = "Bill Reseller")]
		[Property("BillReseller", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool BillReseller { get; set; }

		[EnableSnapShot("ExternalReference1", Description = "External Reference 1")]
		[Property("ExternalReference1", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ExternalReference1 { get; set; }

		[EnableSnapShot("ExternalReference2", Description = "External Reference 2")]
		[Property("ExternalReference2", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ExternalReference2 { get; set; }

		[EnableSnapShot("AccountBucketUnitId", Description = "Account Bucket Unit Reference")]
		[Property("AccountBucketUnitId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long AccountBucketUnitId
		{
			get { return _accountBucketUnitId; }
			set
			{
				_accountBucketUnitId = value;
				if (_accountBucketUnit != null && _accountBucketUnit.Id != value) _accountBucketUnit = null;
			}
		}

		[EnableSnapShot("OverrideCustomerLocationId", Description = "Override Customer Location Reference")]
		[Property("OverrideCustomerLocationId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long OverrideCustomerLocationId
		{
			get { return _overrideCustomerLocationId; }
			set
			{
				_overrideCustomerLocationId = value;
				if (_overrideCustomerLocation != null && _overrideCustomerLocation.Id != value) _overrideCustomerLocation = null;
			}
		}

        [EnableSnapShot("JobId", Description = "Job Id")]
        [Property("JobId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long JobId
        {
            get
            {
	            return _jobId;
            }
            set
            {
                _jobId = value;
                if (_job != null && _job.Id != value) _job = null;
            }
        }

        [EnableSnapShot("JobStep", Description = "Job Step")]
        [Property("JobStep", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int JobStep { get; set; }

		public Customer Customer
		{
			get { return _customerId == default(long) ? null : _customer ?? (_customer = new Customer(_customerId, false)); }
			set
			{
				_customer = value;
				_customerId = value == null ? default(long) : value.Id;
			}
		}
		public CustomerLocation OverrideCustomerLocation
		{
			get
			{
				return _overrideCustomerLocationId == default(long) ? null : _overrideCustomerLocation ?? (_overrideCustomerLocation = new CustomerLocation(_overrideCustomerLocationId));
			}
			set
			{
				_overrideCustomerLocation = value;
				_overrideCustomerLocationId = value == null ? default(long) : value.Id;
			}
		}
		public User User
		{
			get { return _userId == default(long) ? null : _user ?? (_user = new User(_userId, false)); }
			set
			{
				_user = value;
				_userId = value == null ? default(long) : value.Id;
			}
		}
		public AccountBucket AccountBucket
		{
			get { return _accountBucketId == default(long) ? null : _accountBucket ?? (_accountBucket = new AccountBucket(_accountBucketId, false)); }
			set
			{
				_accountBucket = value;
				_accountBucketId = value == null ? default(long) : value.Id;
			}
		}
		public Prefix Prefix
		{
			get { return _prefixId == default(long) ? null : _prefix ?? (_prefix = new Prefix(_prefixId, false)); }
			set
			{
				_prefix = value;
				_prefixId = value == null ? default(long) : value.Id;
			}
		}
		public SalesRepresentative SalesRepresentative
		{
			get
			{
				return _salesRepresentativeId == default(long) ? null : _salesRepresentative ?? (_salesRepresentative = new SalesRepresentative(_salesRepresentativeId, false));
			}
			set
			{
				_salesRepresentative = value;
				_salesRepresentativeId = value == null ? default(long) : value.Id;
			}
		}
		public ResellerAddition ResellerAddition
		{
			get
			{
				return _resellerAdditionId == default(long) ? null : _resellerAddition ?? (_resellerAddition = new ResellerAddition(_resellerAdditionId, false));
			}
			set
			{
				_resellerAddition = value;
				_resellerAdditionId = value == null ? default(long) : value.Id;
			}
		}
		public AccountBucketUnit AccountBucketUnit
		{
			get
			{
				return _accountBucketUnitId == default(long) ? null : _accountBucketUnit ?? (_accountBucketUnit = new AccountBucketUnit(_accountBucketUnitId, false));
			}
			set
			{
				_accountBucketUnit = value;
				_accountBucketUnitId = value == null ? default(long) : value.Id;
			}
		}
        public Job Job
        {
            get
            {
                return _jobId == default(long) ? null : _job ?? (_job = new Job(_jobId, false));
            }
            set
            {
                _job = value;
                _jobId = value == null ? default(long) : value.Id;
            }
        }

		public List<ServiceTicketItem> Items
		{
			get
			{
				if (_items == null) LoadItems();
				return _items;
			}
			set { _items = value; }
		}
		public List<ServiceTicketCharge> Charges
		{
			get
			{
				if (_charges == null) LoadCharges();
				return _charges;
			}
			set { _charges = value; }
		}
		public List<ServiceTicketEquipment> Equipments
		{
			get
			{
				if (_equipments == null) LoadEquipments();
				return _equipments;
			}
			set { _equipments = value; }
		}
		public List<ServiceTicketService> Services
		{
			get
			{
				if (_services == null) LoadServices();
				return _services;
			}
			set { _services = value; }
		}
		public List<ServiceTicketDocument> Documents
		{
			get
			{
				if (_documents == null) LoadDocuments();
				return _documents;
			}
			set { _documents = value; }
		}
		public List<ServiceTicketAsset> Assets
		{
			get
			{
				if (_assets == null) LoadAssets();
				return _assets;
			}
			set { _assets = value; }
		}
		public List<ServiceTicketVendor> Vendors
		{
			get
			{
				if (_vendors == null) LoadVendors();
				return _vendors;
			}
			set { _vendors = value; }
		}
		public List<ServiceTicketNote> Notes
		{
			get
			{
				if (_notes == null) LoadNotes();
				return _notes;
			}
			set { _notes = value; }
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public ServiceTicket() { }

		public ServiceTicket(long id) : this(id, false) { }

		public ServiceTicket(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public ServiceTicket(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

		public void LoadCollections()
		{
			LoadNotes();
			LoadItems();
			LoadCharges();
			LoadVendors();
            LoadEquipments();
			LoadServices();
            LoadAssets();
			LoadDocuments();
		}

		private void LoadNotes()
		{
			_notes = new List<ServiceTicketNote>();
			const string query = @"SELECT * FROM ServiceTicketNote WHERE ServiceTicketId = @ServiceTicketId AND TenantId = @TenantId;";
			var parameters = new Dictionary<string, object> { { "ServiceTicketId", Id }, { "TenantId", TenantId } }; 
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_notes.Add(new ServiceTicketNote(reader));
			Connection.Close();
		}

		private void LoadItems()
		{
			_items = new List<ServiceTicketItem>();
			const string query = @"SELECT * FROM ServiceTicketItem WHERE ServiceTicketId = @ServiceTicketId AND TenantId = @TenantId;";
			var parameters = new Dictionary<string, object> { { "ServiceTicketId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_items.Add(new ServiceTicketItem(reader));
			Connection.Close();
		}

		private void LoadCharges()
		{
			_charges = new List<ServiceTicketCharge>();
			const string query = @"SELECT * FROM ServiceTicketCharge WHERE ServiceTicketId = @ServiceTicketId AND TenantId = @TenantId;";
			var parameters = new Dictionary<string, object> { { "ServiceTicketId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_charges.Add(new ServiceTicketCharge(reader));
			Connection.Close();
		}

		private void LoadVendors()
		{
			_vendors = new List<ServiceTicketVendor>();
			const string query = @"SELECT * FROM ServiceTicketVendor WHERE ServiceTicketId = @ServiceTicketId AND TenantId = @TenantId;";
			var parameters = new Dictionary<string, object> { { "ServiceTicketId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_vendors.Add(new ServiceTicketVendor(reader));
			Connection.Close();
		}

		private void LoadEquipments()
		{
			_equipments = new List<ServiceTicketEquipment>();
			const string query = @"SELECT * FROM ServiceTicketEquipment WHERE ServiceTicketId = @ServiceTicketId AND TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "ServiceTicketId", Id }, { "TenantId", TenantId } };
			
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_equipments.Add(new ServiceTicketEquipment(reader));
			Connection.Close();
		}
		
		private void LoadServices()
		{
			_services = new List<ServiceTicketService>();
			const string query = @"SELECT * FROM ServiceTicketService WHERE ServiceTicketId = @ServiceTicketId AND TenantId = @TenantId;";
			var parameters = new Dictionary<string, object> { { "ServiceTicketId", Id }, { "TenantId", TenantId } };
			
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_services.Add(new ServiceTicketService(reader));
			Connection.Close();
		}

		private void LoadAssets()
		{
			_assets = new List<ServiceTicketAsset>();
			const string query = @"SELECT * FROM ServiceTicketAsset WHERE ServiceTicketId = @ServiceTicketId AND TenantId = @TenantId;";
			var parameters = new Dictionary<string, object> { { "ServiceTicketId", Id }, { "TenantId", TenantId } };
			
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_assets.Add(new ServiceTicketAsset(reader));
			Connection.Close();
		}

		private void LoadDocuments()
		{
			_documents = new List<ServiceTicketDocument>();
			if (IsNew) return;
			const string query = @"SELECT * FROM ServiceTicketDocument WHERE ServiceTicketId = @ServiceTicketId AND TenantId = @TenantId;";
			var parameters = new Dictionary<string, object> { { "ServiceTicketId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_documents.Add(new ServiceTicketDocument(reader));
			Connection.Close();
		}
	}
}
