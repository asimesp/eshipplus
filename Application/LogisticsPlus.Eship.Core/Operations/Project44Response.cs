﻿using System;
using System.Data;
using LogisticsPlus.Eship.Core.Administration;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("Project44Response", ReadOnly = false, Source = EntitySource.TableView)]
	public class Project44Response : ObjectBase
    {
        private long _userId;
        private long _shipmentId;

        private User _user;
        private Shipment _shipment;


        [EnableSnapShot("ShipmentId", Description = "ShipmentId")]
        [Property("ShipmentId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long ShipmentId
        {
            get
            {
                if (_shipment != null) _shipmentId = _shipment.Id;
                return _shipmentId;
            }
            set
            {
                _shipmentId = value;
                if (_shipment != null && value != _shipment.Id) _shipment = null;
            }
        }

        [EnableSnapShot("ResponseData", Description = "ResponseData")]
        [Property("ResponseData", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ResponseData { get; set; }

        [EnableSnapShot("DateCreated", Description = "Date Created")]
        [Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime DateCreated { get; set; }

        [EnableSnapShot("Scac", Description = "Scac")]
        [Property("Scac", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Scac { get; set; }

        [EnableSnapShot("Project44Id", Description = "Project44Id")]
        [Property("Project44Id", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Project44Id { get; set; }

        [EnableSnapShot("UserId", Description = "User Id")]
        [Property("UserId", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long UserId
        {
            get
            {
                if (_user != null) _userId = _user.Id;
                return _userId;
            }
            set
            {
                _userId = value;
                if (_user != null && value != _user.Id) _user = null;
            }
        }

        public User User
        {
            get { return _userId == default(long) ? null : _user ?? (_user = new User(_userId)); }
            set
            {
                _user = value;
                _userId = value == null ? default(long) : value.Id;
            }
        }

        public Shipment Shipment
        {
            get { return _shipmentId == default(long) ? null : _shipment ?? (_shipment = new Shipment(_shipmentId)); }
            set
            {
                _shipment = value;
                _shipmentId = value == null ? default(long) : value.Id;
            }
        }
    }
}
