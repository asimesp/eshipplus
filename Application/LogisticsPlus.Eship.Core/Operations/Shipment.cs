﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
    [Entity("Shipment", ReadOnly = false, Source = EntitySource.TableView)]
    public class Shipment : TenantBase
    {
        private long _prefixId;
        private long _customerId;
        private long _userId;
        private long _carrierCoordinatorUserId;
		private long _shipmentCoordinatorUserId;
        private long _mileageSourceId;
        private long _salesRepresentativeId;
        private long _resellerAdditionId;
        private long _accountBucketUnitId;
		private long _shipmentPriorityId;
	    private long _jobId;
		
        private ShipmentPriority _shipmentPriority;
        private MileageSource _mileageSource;
        private ResellerAddition _resellerAddition;
        private SalesRepresentative _salesRepresentative;
        private Customer _customer;
        private Prefix _prefix;
        private User _user;
        private User _carrierCoordinator;
        private User _shipmentCoordinator;
        private AccountBucketUnit _accountBucketUnit;
	    private Job _job;

        private List<ShipmentVendor> _vendors;
        private List<ShipmentNote> _notes;
        private List<ShipmentItem> _items;
        private List<ShipmentCharge> _charges;
        private List<ShipmentLocation> _stops;
        private List<ShipmentEquipment> _equipments;
        private List<ShipmentReference> _customerReferences;
        private List<ShipmentService> _services;
        private List<ShipmentQuickPayOption> _quickPayOptions;
        private List<ShipmentAutoRatingAccessorial> _autoRatingAccessorials;
        private List<ShipmentDocument> _documents;
        private List<ShipmentAsset> _assets;
        private List<ShipmentAccountBucket> _accountBuckets;
        private List<CheckCall> _checkCalls;

        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; protected set; }

        [EnableSnapShot("ShipmentNumber", Description = "Shipment Number")]
        [Property("ShipmentNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ShipmentNumber { get; set; }

        [EnableSnapShot("PrefixId", Description = "Prefix Reference")]
        [Property("PrefixId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long PrefixId
        {
            get { return _prefixId; }
            set
            {
                _prefixId = value;
                if (_prefix != null && value != _prefix.Id) _prefix = null;
            }
        }

        [EnableSnapShot("HidePrefix", Description = "Hide Prefix")]
        [Property("HidePrefix", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool HidePrefix { get; set; }

        [EnableSnapShot("ShipperBol", Description = "Shipper Bill of Lading")]
        [Property("ShipperBol", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ShipperBol { get; set; }

        [EnableSnapShot("EnableQuickPay", Description = "QuickPay Option Enabled")]
        [Property("EnableQuickPay", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool EnableQuickPay { get; set; }

		[EnableSnapShot("CreatedInError", Description = "Created In Error")]
		[Property("CreatedInError", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool CreatedInError { get; set; }

        [EnableSnapShot("PurchaseOrderNumber", Description = "Purchase Order Number")]
        [Property("PurchaseOrderNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string PurchaseOrderNumber { get; set; }

        [EnableSnapShot("ShipperReference", Description = "Shipper Reference Number")]
        [Property("ShipperReference", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ShipperReference { get; set; }

        [EnableSnapShot("HazardousMaterial", Description = "Is Hazardous Material")]
        [Property("HazardousMaterial", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool HazardousMaterial { get; set; }

		[EnableSnapShot("AuditedForInvoicing", Description = "Audited For Invoicing")]
		[Property("AuditedForInvoicing", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool AuditedForInvoicing { get; set; }

        [EnableSnapShot("HazardousMaterialContactName", Description = "Hazardous Material Contact Name")]
        [Property("HazardousMaterialContactName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string HazardousMaterialContactName { get; set; }

        [EnableSnapShot("HazardousMaterialContactPhone", Description = "Hazardous Material Contact Phone")]
        [Property("HazardousMaterialContactPhone", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string HazardousMaterialContactPhone { get; set; }

        [EnableSnapShot("HazardousMaterialContactMobile", Description = "Hazardous Material Contact Mobile")]
        [Property("HazardousMaterialContactMobile", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string HazardousMaterialContactMobile { get; set; }

        [EnableSnapShot("HazardousMaterialContactEmail", Description = "Hazardous Material Contact Email")]
        [Property("HazardousMaterialContactEmail", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string HazardousMaterialContactEmail { get; set; }

        [EnableSnapShot("DesiredPickupDate", Description = "Desired Pickup Date")]
        [Property("DesiredPickupDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime DesiredPickupDate { get; set; }

        [EnableSnapShot("EarlyPickup", Description = "Early Pickup Time")]
        [Property("EarlyPickup", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string EarlyPickup { get; set; }

        [EnableSnapShot("LatePickup", Description = "Late Pickup Time")]
        [Property("LatePickup", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string LatePickup { get; set; }

        [EnableSnapShot("ActualPickupDate", Description = "Actual Pickup Date")]
        [Property("ActualPickupDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime ActualPickupDate { get; set; }

        [EnableSnapShot("EstimatedDeliveryDate", Description = "Estimated Delivery Date")]
        [Property("EstimatedDeliveryDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime EstimatedDeliveryDate { get; set; }

        [EnableSnapShot("EarlyDelivery", Description = "Early Delivery Time")]
        [Property("EarlyDelivery", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string EarlyDelivery { get; set; }

        [EnableSnapShot("LateDelivery", Description = "Late Delivery Time")]
        [Property("LateDelivery", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string LateDelivery { get; set; }

        [EnableSnapShot("ActualDeliveryDate", Description = "Actual Delivery Date")]
        [Property("ActualDeliveryDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime ActualDeliveryDate { get; set; }

        [EnableSnapShot("Status", Description = "Shipment Status")]
        [Property("Status", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public ShipmentStatus Status { get; set; }

        [EnableSnapShot("DateCreated", Description = "Date Created")]
        [Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime DateCreated { get; set; }

        [EnableSnapShot("ServiceMode", Description = "Service Mode")]
        [Property("ServiceMode", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public ServiceMode ServiceMode { get; set; }

        [EnableSnapShot("ShipmentAutorated", Description = "Is Autorated Shipment")]
        [Property("ShipmentAutorated", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool ShipmentAutorated { get; set; }

        [EnableSnapShot("OriginalRateValue", Description = "Origin Rate from Rating Engine")]
        [Property("OriginalRateValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal OriginalRateValue { get; set; }

        [EnableSnapShot("VendorDiscountPercentage", Description = "Vendor Discount Percentage")]
        [Property("VendorDiscountPercentage", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal VendorDiscountPercentage { get; set; }

        [EnableSnapShot("VendorFreightFloor", Description = "Vendor Freight Floor")]
        [Property("VendorFreightFloor", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal VendorFreightFloor { get; set; }

        [EnableSnapShot("VendorFreightCeiling", Description = "Vendor Freight Ceiling")]
        [Property("VendorFreightCeiling", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal VendorFreightCeiling { get; set; }

        [EnableSnapShot("VendorFuelPercent", Description = "Vendor Fuel Percent")]
        [Property("VendorFuelPercent", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal VendorFuelPercent { get; set; }

        [EnableSnapShot("CustomerFreightMarkupValue", Description = "Customer Freight Markup Value")]
        [Property("CustomerFreightMarkupValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal CustomerFreightMarkupValue { get; set; }

        [EnableSnapShot("CustomerFreightMarkupPercent", Description = "Customer Freight Markup Percent")]
        [Property("CustomerFreightMarkupPercent", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal CustomerFreightMarkupPercent { get; set; }

        [EnableSnapShot("UseLowerCustomerFreightMarkup", Description = "Use lower Customer Freight Markup")]
        [Property("UseLowerCustomerFreightMarkup", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool UseLowerCustomerFreightMarkup { get; set; }

        [EnableSnapShot("BilledWeight", Description = "Billed Weight")]
        [Property("BilledWeight", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal BilledWeight { get; set; }

		[EnableSnapShot("RatedWeight", Description = "Rated Weight")]
		[Property("RatedWeight", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal RatedWeight { get; set; }

		[EnableSnapShot("RatedCubicFeet", Description = "Rated Cubic Feet")]
		[Property("RatedCubicFeet", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal RatedCubicFeet { get; set; }

        [EnableSnapShot("RatedPcf", Description = "Rated Pounds per Cubic Foot")]
        [Property("RatedPcf", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal RatedPcf { get; set; }

        [EnableSnapShot("ApplyDiscount", Description = "Apply Discount")]
        [Property("ApplyDiscount", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool ApplyDiscount { get; set; }

		[EnableSnapShot("IsLTLPackageSpecificRate", Description = "Is LTL Package Specific Rate")]
		[Property("IsLTLPackageSpecificRate", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool IsLTLPackageSpecificRate { get; set; }

		[EnableSnapShot("IsPartialTruckload", Description = "Is Partial TruckLoad")]
		[Property("IsPartialTruckload", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool IsPartialTruckload { get; set; }

        [EnableSnapShot("LineHaulProfitAdjustmentRatio", Description = "Line Haul Profit Adjustment Ratio")]
        [Property("LineHaulProfitAdjustmentRatio", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal LineHaulProfitAdjustmentRatio { get; set; }

        [EnableSnapShot("FuelProfitAdjustmentRatio", Description = "Fuel Profit Adjustment Ratio")]
        [Property("FuelProfitAdjustmentRatio", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal FuelProfitAdjustmentRatio { get; set; }

        [EnableSnapShot("AccessorialProfitAdjustmentRatio", Description = "Accessorial Profit Adjustment Ratio")]
        [Property("AccessorialProfitAdjustmentRatio", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal AccessorialProfitAdjustmentRatio { get; set; }

        [EnableSnapShot("ServiceProfitAdjustmentRatio", Description = "Service Profit Adjustment Ratio")]
        [Property("ServiceProfitAdjustmentRatio", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal ServiceProfitAdjustmentRatio { get; set; }

        [EnableSnapShot("ResellerAdditionalFreightMarkup", Description = "Reseller Additional Freight Markup")]
        [Property("ResellerAdditionalFreightMarkup", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal ResellerAdditionalFreightMarkup { get; set; }

        [EnableSnapShot("ResellerAdditionalFreightMarkupIsPercent", Description = "Reseller Additional Freight Markup Is Percent")]
        [Property("ResellerAdditionalFreightMarkupIsPercent", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool ResellerAdditionalFreightMarkupIsPercent { get; set; }

        [EnableSnapShot("ResellerAdditionalFuelMarkup", Description = "Reseller Additional Fuel Markup")]
        [Property("ResellerAdditionalFuelMarkup", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal ResellerAdditionalFuelMarkup { get; set; }

        [EnableSnapShot("ResellerAdditionalFuelMarkupIsPercent", Description = "Reseller Additional Fuel Markup Is Percent")]
        [Property("ResellerAdditionalFuelMarkupIsPercent", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool ResellerAdditionalFuelMarkupIsPercent { get; set; }

        [EnableSnapShot("ResellerAdditionalAccessorialMarkup", Description = "Reseller Additional Accessorial Markup")]
        [Property("ResellerAdditionalAccessorialMarkup", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal ResellerAdditionalAccessorialMarkup { get; set; }

        [EnableSnapShot("ResellerAdditionalAccessorialMarkupIsPercent", Description = "Reseller Additional Accessorial Markup Is Percent")]
        [Property("ResellerAdditionalAccessorialMarkupIsPercent", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool ResellerAdditionalAccessorialMarkupIsPercent { get; set; }

        [EnableSnapShot("ResellerAdditionalServiceMarkup", Description = "Reseller Additional Service Markup")]
        [Property("ResellerAdditionalServiceMarkup", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal ResellerAdditionalServiceMarkup { get; set; }

        [EnableSnapShot("ResellerAdditionalServiceMarkupIsPercent", Description = "Reseller Additional Service Markup Is Percent")]
        [Property("ResellerAdditionalServiceMarkupIsPercent", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool ResellerAdditionalServiceMarkupIsPercent { get; set; }

        [EnableSnapShot("SmallPackageEngine", Description = "Small Package Engine")]
        [Property("SmallPackageEngine", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public SmallPackageEngine SmallPackageEngine { get; set; }

        [EnableSnapShot("SmallPackType", Description = "Small Pack Type")]
        [Property("SmallPackType", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string SmallPackType { get; set; }

        [EnableSnapShot("DirectPointRate", Description = "Rate Is Direct Point")]
        [Property("DirectPointRate", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool DirectPointRate { get; set; }

        [EnableSnapShot("DeclineInsurance", Description = "Decline Insurance")]
        [Property("DeclineInsurance", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool DeclineInsurance { get; set; }

        [EnableSnapShot("InDispute", Description = "Shipment In Dispute")]
        [Property("InDispute", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool InDispute { get; set; }

        [EnableSnapShot("InDisputeReason", Description = "Shipment In Dispute Reason")]
        [Property("InDisputeReason", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public InDisputeReason InDisputeReason { get; set; }

        [EnableSnapShot("VendorRatingOverrideAddress", Description = "Vendor Rating Profile Document Override Address")]
        [Property("VendorRatingOverrideAddress", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string VendorRatingOverrideAddress { get; set; }

		[EnableSnapShot("CareOfAddressFormatEnabled", Description = "Care Of Address Format Enabled")]
		[Property("CareOfAddressFormatEnabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool CareOfAddressFormatEnabled { get; set; }

        [EnableSnapShot("IsGuaranteedDeliveryService", Description = "Guaranteed Delivery Service is Enabled")]
        [Property("IsGuaranteedDeliveryService", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool IsGuaranteedDeliveryService { get; set; }

        [EnableSnapShot("GuaranteedDeliveryServiceTime", Description = "Guaranteed Delivery Service Time of Day")]
        [Property("GuaranteedDeliveryServiceTime", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string GuaranteedDeliveryServiceTime { get; set; }

        [EnableSnapShot("GuaranteedDeliveryServiceRateType", Description = "Guaranteed Delivery Service Rate Type")]
        [Property("GuaranteedDeliveryServiceRateType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public RateType GuaranteedDeliveryServiceRateType { get; set; }

        [EnableSnapShot("GuaranteedDeliveryServiceRate", Description = "Guaranteed Delivery Service Rate")]
        [Property("GuaranteedDeliveryServiceRate", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal GuaranteedDeliveryServiceRate { get; set; }

        [EnableSnapShot("IsExpeditedService", Description = "Is Expedited Service")]
        [Property("IsExpeditedService", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool IsExpeditedService { get; set; }

        [EnableSnapShot("ExpeditedServiceTime", Description = "Expedited Service Time")]
        [Property("ExpeditedServiceTime", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ExpeditedServiceTime { get; set; }

        [EnableSnapShot("ExpeditedServiceRate", Description = "Expedited Service Rate")]
        [Property("ExpeditedServiceRate", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal ExpeditedServiceRate { get; set; }

        [EnableSnapShot("LinearFootRuleBypassed", Description = "Linear Foot Rule Bypassed")]
		[Property("LinearFootRuleBypassed", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool LinearFootRuleBypassed { get; set; }

        [EnableSnapShot("GeneralBolComments", Description = "General Bill of Lading Comments")]
        [Property("GeneralBolComments", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string GeneralBolComments { get; set; }

        [EnableSnapShot("CriticalBolComments", Description = "Critical Bill of Lading Comments")]
        [Property("CriticalBolComments", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string CriticalBolComments { get; set; }

        [EnableSnapShot("MiscField1", Description = "Misc Fields")]
        [Property("MiscField1", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string MiscField1 { get; set; }

        [EnableSnapShot("MiscField2", Description = "Misc Fields")]
        [Property("MiscField2", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string MiscField2 { get; set; }

        [EnableSnapShot("EmptyMileage", Description = "Empty Mileage")]
        [Property("EmptyMileage", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal EmptyMileage { get; set; }

        [EnableSnapShot("Mileage")]
        [Property("Mileage", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal Mileage { get; set; }

        [EnableSnapShot("MileageSourceId", Description = "Mileage Source Reference")]
        [Property("MileageSourceId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long MileageSourceId
        {
            get { return _mileageSourceId; }
            set
            {
                _mileageSourceId = value;
                if (_mileageSource != null && _mileageSource.Id != value) _mileageSource = null;
            }
        }

        [EnableSnapShot("SalesRepresentativeId", Description = "Sales Representative Reference")]
        [Property("SalesRepresentativeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long SalesRepresentativeId
        {
            get { return _salesRepresentativeId; }
            set
            {
                _salesRepresentativeId = value;
                if (_salesRepresentative != null && _salesRepresentative.Id != value) _salesRepresentative = null;
            }
        }

		[EnableSnapShot("SalesRepresentativeCommissionPercent", Description = "Sales Representative Commission Percent")]
        [Property("SalesRepresentativeCommissionPercent", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal SalesRepresentativeCommissionPercent { get; set; }

		[EnableSnapShot("SalesRepMinCommValue", Description = "Sales Representative Minimum Commission Value")]
		[Property("SalesRepMinCommValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal SalesRepMinCommValue { get; set; }

		[EnableSnapShot("SalesRepMaxCommValue", Description = "Sales Representative Maximum Commission Value")]
		[Property("SalesRepMaxCommValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal SalesRepMaxCommValue { get; set; }

		[EnableSnapShot("SalesRepAddlEntityName", Description = "Sales Representative Additional Entity Name")]
		[Property("SalesRepAddlEntityName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string SalesRepAddlEntityName { get; set; }

		[EnableSnapShot("SalesRepAddlEntityCommPercent", Description = "Sales Representative Additional Entity Commission Percent")]
		[Property("SalesRepAddlEntityCommPercent", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal SalesRepAddlEntityCommPercent { get; set; }

        [EnableSnapShot("ResellerAdditionId", Description = "Reseller Addittion Reference")]
        [Property("ResellerAdditionId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long ResellerAdditionId
        {
            get { return _resellerAdditionId; }
            set
            {
                _resellerAdditionId = value;
                if (_resellerAddition != null && _resellerAddition.Id != value) _resellerAddition = null;
            }
        }

        [EnableSnapShot("AccountBucketUnitId", Description = "Account Bucket Unit Reference")]
        [Property("AccountBucketUnitId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long AccountBucketUnitId
        {
            get { return _accountBucketUnitId; }
            set
            {
                _accountBucketUnitId = value;
                if (_accountBucketUnit != null && _accountBucketUnit.Id != value) _accountBucketUnit = null;
            }
        }

        [EnableSnapShot("BillReseller", Description = "Bill Reseller")]
        [Property("BillReseller", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool BillReseller { get; set; }

		[EnableSnapShot("CarrierCoordinatorId", Description = "Carrier Coordinator Reference")]
		[Property("CarrierCoordinatorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false, Column = "CarrierCoordinatorUserId")]
		public long CarrierCoordinatorId
		{
			get { return _carrierCoordinatorUserId; }
			set
			{
				_carrierCoordinatorUserId = value;
				if (_carrierCoordinator != null && value != _carrierCoordinator.Id) _carrierCoordinator = null;
			}
		}

		[EnableSnapShot("ShipmentCoordinatorId", Description = "Shipment Coordinator Reference")]
		[Property("ShipmentCoordinatorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false, Column = "ShipmentCoordinatorUserId")]
		public long ShipmentCoordinatorId
		{
			get { return _shipmentCoordinatorUserId; }
			set
			{
				_shipmentCoordinatorUserId = value;
				if (_shipmentCoordinator != null && value != _shipmentCoordinator.Id) _shipmentCoordinator = null;
			}
		}

		[EnableSnapShot("BolFtpDelivered", Description = "Bill of Lading Ftp Delivered")]
		[Property("BolFtpDelivered", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool BolFtpDelivered { get; set; }

		[EnableSnapShot("StatementFtpDelivered", Description = "Shipment Statement Ftp Delivered")]
		[Property("StatementFtpDelivered", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool StatementFtpDelivered { get; set; }

        [EnableSnapShot("UserId", Description = "User Reference")]
        [Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        protected long UserId
        {
            get { return _userId; }
            set
            {
                _userId = value;
                if (_user != null && value != _user.Id) _user = null;
            }
        }

        [EnableSnapShot("CustomerId", Description = "Customer Reference")]
        [Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        protected long CustomerId
        {
            get { return _customerId; }
            set
            {
                _customerId = value;
                if (_customer != null && value != _customer.Id) _customer = null;
            }
        }

        [EnableSnapShot("OriginId", Description = "Origin Reference")]
        [Property("OriginId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        protected long OriginId
        {
            get { return Origin == null ? default(long) : Origin.Id; }
            set
            {
                if (Origin != null && Origin.Id == value) return;
                Origin = value == default(long) ? null : new ShipmentLocation(value, false);
            }
        }

        [EnableSnapShot("SendShipmentUpdateToOriginPrimaryContact", Description = "Send Shipment Update To Origin Primary Contact")]
        [Property("SendShipmentUpdateToOriginPrimaryContact", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool SendShipmentUpdateToOriginPrimaryContact { get; set; }

        [EnableSnapShot("DestinationId", Description = "Destination Reference")]
        [Property("DestinationId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        protected long DestinationId
        {
            get { return Destination == null ? default(long) : Destination.Id; }
            set
            {
                if (Destination != null && Destination.Id == value) return;
                Destination = value == default(long) ? null : new ShipmentLocation(value, false);
            }
        }

        [EnableSnapShot("SendShipmentUpdateToDestinationPrimaryContact", Description = "Send Shipment Update To Destination Primary Contact")]
        [Property("SendShipmentUpdateToDestinationPrimaryContact", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool SendShipmentUpdateToDestinationPrimaryContact { get; set; }

        [EnableSnapShot("ShipmentPriorityId", Description = "Shipment Priority Reference")]
		[Property("ShipmentPriorityId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ShipmentPriorityId
		{
			get { return _shipmentPriorityId; }
			set
			{
				_shipmentPriorityId = value;
				if (_shipmentPriority != null && value != _shipmentPriority.Id) _shipmentPriority = null;
			}
		}

		[EnableSnapShot("DriverName", Description = "Driver Name")]
		[Property("DriverName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DriverName { get; set; }

		[EnableSnapShot("DriverPhoneNumber", Description = "Driver Phone Number")]
		[Property("DriverPhoneNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DriverPhoneNumber { get; set; }

		[EnableSnapShot("DriverTrailerNumber", Description = "Driver Trailer Number")]
		[Property("DriverTrailerNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DriverTrailerNumber { get; set; }

		[EnableSnapShot("OriginTerminalInfoId", Description = "Origin LTL Terminal Info Reference")]
        [Property("OriginTerminalInfoId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        protected long OriginTerminalInfoId
        {
            get { return OriginTerminal == null ? default(long) : OriginTerminal.Id; }
            set
            {
                if (OriginTerminal != null && OriginTerminal.Id == value) return;
                OriginTerminal = value == default(long) ? null : new LTLTerminalInfo(value, false);
            }
        }

        [EnableSnapShot("DestinationTerminalInfoId", Description = "Destination LTL Terminal Info Reference")]
        [Property("DestinationTerminalInfoId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        protected long DestinationTerminalInfoId
        {
            get { return DestinationTerminal == null ? default(long) : DestinationTerminal.Id; }
            set
            {
                if (DestinationTerminal != null && DestinationTerminal.Id == value) return;
                DestinationTerminal = value == default(long) ? null : new LTLTerminalInfo(value, false);
            }
        }

        [EnableSnapShot("JobId", Description = "Job Id")]
        [Property("JobId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long JobId
        {
            get { return _jobId; }
            set
            {
                _jobId = value;
                if (_job != null && _job.Id != value) _job = null;
            }
        }

        [EnableSnapShot("JobStep", Description = "Job Step")]
        [Property("JobStep", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int JobStep { get; set; }

        [EnableSnapShot("Project44QuoteNumber", Description = "Project 44 Quote Number")]
        [Property("Project44QuoteNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Project44QuoteNumber { get; set; }

        public Prefix Prefix
        {
            get { return _prefixId == default(long) ? null : _prefix ?? (_prefix = new Prefix(_prefixId, false)); }
            set
            {
                _prefix = value;
                _prefixId = value == null ? default(long) : value.Id;
            }
        }
        public Customer Customer
        {
            get { return _customerId == default(long) ? null : _customer ?? (_customer = new Customer(_customerId, false)); }
            set
            {
                _customer = value;
                _customerId = value == null ? default(long) : value.Id;
            }
        }
        public User User
        {
            get { return _userId == default(long) ? null : _user ?? (_user = new User(_userId, false)); }
            set
            {
                _user = value;
                _userId = value == null ? default(long) : value.Id;
            }
        }
		public User CarrierCoordinator
		{
			get { return _carrierCoordinatorUserId == default(long) ? null : _carrierCoordinator ?? (_carrierCoordinator = new User(_carrierCoordinatorUserId, false)); }
			set
			{
				_carrierCoordinator = value;
				_carrierCoordinatorUserId = value == null ? default(long) : value.Id;
			}
		}
		public User ShipmentCoordinator
		{
			get { return _shipmentCoordinatorUserId == default(long) ? null : _shipmentCoordinator ?? (_shipmentCoordinator = new User(_shipmentCoordinatorUserId, false)); }
			set
			{
				_shipmentCoordinator = value;
				_shipmentCoordinatorUserId = value == null ? default(long) : value.Id;
			}
		}

        public ShipmentLocation Origin { get; set; }
        public ShipmentLocation Destination { get; set; }
        public LTLTerminalInfo OriginTerminal { get; set; }
        public LTLTerminalInfo DestinationTerminal { get; set; }

        
        public MileageSource MileageSource
        {
            get
            {
                return _mileageSourceId == default(long) ? null : _mileageSource ?? (_mileageSource = new MileageSource(_mileageSourceId, false));
            }
            set
            {
                _mileageSource = value;
                _mileageSourceId = value == null ? default(long) : value.Id;
            }
        }
        public SalesRepresentative SalesRepresentative
        {
            get
            {
                return _salesRepresentativeId == default(long) ? null : _salesRepresentative ?? (_salesRepresentative = new SalesRepresentative(_salesRepresentativeId, false));
            }
            set
            {
                _salesRepresentative = value;
                _salesRepresentativeId = value == null ? default(long) : value.Id;
            }
        }
        public ResellerAddition ResellerAddition
        {
            get
            {
                return _resellerAdditionId == default(long) ? null : _resellerAddition ?? (_resellerAddition = new ResellerAddition(_resellerAdditionId, false));
            }
            set
            {
                _resellerAddition = value;
                _resellerAdditionId = value == null ? default(long) : value.Id;
            }
        }
        public AccountBucketUnit AccountBucketUnit
        {
            get
            {
                return _accountBucketUnitId == default(long) ? null : _accountBucketUnit ?? (_accountBucketUnit = new AccountBucketUnit(_accountBucketUnitId, false));
            }
            set
            {
                _accountBucketUnit = value;
                _accountBucketUnitId = value == null ? default(long) : value.Id;
            }
        }
		public ShipmentPriority ShipmentPriority
		{
			get { return _shipmentPriorityId == default(long) ? null : _shipmentPriority ?? (_shipmentPriority = new ShipmentPriority(_shipmentPriorityId, false)); }
			set
			{
				_shipmentPriority = value;
				_shipmentPriorityId = value == null ? default(long) : value.Id;
			}
		}
        public Job Job
        {
            get
            {
                return _jobId == default(long) ? null : _job ?? (_job = new Job(_jobId, false));
            }
            set
            {
                _job = value;
                _jobId = value == null ? default(long) : value.Id;
            }
        }

        public List<ShipmentVendor> Vendors
        {
            get
            {
                if (_vendors == null) LoadVendors();
                return _vendors;
            }
            set { _vendors = value; }
        }
        public List<ShipmentNote> Notes
        {
            get
            {
                if (_notes == null) LoadNotes();
                return _notes;
            }
            set { _notes = value; }
        }
        public List<ShipmentItem> Items
        {
            get
            {
                if (_items == null) LoadItems();
                return _items;
            }
            set { _items = value; }
        }
        public List<ShipmentCharge> Charges
        {
            get
            {
                if (_charges == null) LoadCharges();
                return _charges;
            }
            set { _charges = value; }
        }
        public List<ShipmentLocation> Stops
        {
            get
            {
                if (_stops == null) LoadStops();
                return _stops;
            }
            set { _stops = value; }
        }
        public List<ShipmentEquipment> Equipments
        {
            get
            {
                if (_equipments == null) LoadEquipments();
                return _equipments;
            }
            set { _equipments = value; }
        }
        public List<ShipmentReference> CustomerReferences
        {
            get
            {
                if (_customerReferences == null) LoadCustomeReferences();
                return _customerReferences;
            }
            set { _customerReferences = value; }
        }
        public List<ShipmentService> Services
        {
            get
            {
                if (_services == null) LoadServices();
                return _services;
            }
            set { _services = value; }
        }
        public List<ShipmentQuickPayOption> QuickPayOptions
        {
            get
            {
                if (_quickPayOptions == null) LoadQuickPayOptions();
                return _quickPayOptions;
            }
            set { _quickPayOptions = value; }
        }
        public List<ShipmentAutoRatingAccessorial> AutoRatingAccessorials
        {
            get
            {
                if (_autoRatingAccessorials == null) LoadAutoRatingAccessorials();
                return _autoRatingAccessorials;
            }
            set { _autoRatingAccessorials = value; }
        }
        public List<ShipmentDocument> Documents
        {
            get
            {
                if (_documents == null) LoadDocuments();
                return _documents;
            }
            set { _documents = value; }
        }
        public List<ShipmentAsset> Assets
        {
            get
            {
                if (_assets == null) LoadAssets();
                return _assets;
            }
            set { _assets = value; }
        }
        public List<ShipmentAccountBucket> AccountBuckets
        {
            get
            {
                if (_accountBuckets == null) LoadAccountBuckets();
                return _accountBuckets;
            }
            set { _accountBuckets = value; }
        }
        public List<CheckCall> CheckCalls
        {
            get
            {
                if (_checkCalls == null) LoadCheckCalls();
                return _checkCalls;
            }
            set { _checkCalls = value; }
        }


        public bool IsNew
        {
            get { return Id == default(long); }
        }

        public Shipment() { }

        public Shipment(long id) : this(id, false) { }

        public Shipment(long id, bool takeSnapshot)
        {
            Id = id;
            if (Id == default(long)) return;
            KeyLoaded = Load();
            if (takeSnapshot) TakeSnapShot();
        }

        public Shipment(DbDataReader reader)
        {
            Load(reader);
        }

        public void Save()
        {
            if (Id == default(long)) Insert();
            else Update();
        }

        public new void Delete()
        {
            if (Id != default(long))
                base.Delete();
        }


        public ShipmentHistoricalData FetchHistoricalData()
        {
            if (Id == default(long)) return null;

            ShipmentHistoricalData data = null;

            const string query = @"SELECT * FROM ShipmentHistoricalData WHERE ShipmentId = @ShipmentId";
            var parameters = new Dictionary<string, object> { { "ShipmentId", Id } };

            using (var reader = GetReader(query, parameters))
                if (reader.Read())
                    data = new ShipmentHistoricalData(reader);
            Connection.Close();
            return data;
        }


        public void LoadCollections()
        {
            LoadNotes();
            LoadItems();
            LoadCharges();
            LoadVendors();
            LoadStops();
            LoadEquipments();
            LoadCustomeReferences();
            LoadServices();
            LoadQuickPayOptions();
            LoadAutoRatingAccessorials();
            LoadDocuments();
            LoadAssets();
            LoadAccountBuckets();
            LoadCheckCalls();
        }

        private void LoadNotes()
        {
            _notes = new List<ShipmentNote>();
			if (IsNew) return;
            const string query = @"SELECT * FROM ShipmentNote WHERE ShipmentId = @ShipmentId AND TenantId = @TenantId;";
            var parameters = new Dictionary<string, object> { { "ShipmentId", Id }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _notes.Add(new ShipmentNote(reader));
            Connection.Close();
        }

        private void LoadItems()
        {
            _items = new List<ShipmentItem>();
			if (IsNew) return;
            const string query = @"SELECT * FROM ShipmentItem WHERE ShipmentId = @ShipmentId AND TenantId = @TenantId;";
            var parameters = new Dictionary<string, object> { { "ShipmentId", Id }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _items.Add(new ShipmentItem(reader));
            Connection.Close();
        }

        private void LoadCharges()
        {
            _charges = new List<ShipmentCharge>();
			if (IsNew) return;
            const string query = @"SELECT * FROM ShipmentCharge WHERE ShipmentId = @ShipmentId AND TenantId = @TenantId;";
            var parameters = new Dictionary<string, object> { { "ShipmentId", Id }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _charges.Add(new ShipmentCharge(reader));
            Connection.Close();
        }

        private void LoadVendors()
        {
            _vendors = new List<ShipmentVendor>();
			if (IsNew) return;
            const string query = @"SELECT * FROM ShipmentVendor WHERE ShipmentId = @ShipmentId AND TenantId = @TenantId;";
            var parameters = new Dictionary<string, object> { { "ShipmentId", Id }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _vendors.Add(new ShipmentVendor(reader));
            Connection.Close();
        }

        private void LoadStops()
        {
            _stops = new List<ShipmentLocation>();
			if (IsNew) return;
            const string query = @"SELECT * FROM ShipmentLocation WHERE ShipmentId = @ShipmentId AND TenantId = @TenantId
								AND [Id] NOT IN (@OriginId, @DestinationId)";
            var parameters = new Dictionary<string, object>
			                 	{
			                 		{"ShipmentId", Id},
			                 		{"TenantId", TenantId},
			                 		{"OriginId", OriginId},
			                 		{"DestinationId", DestinationId}
			                 	};
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _stops.Add(new ShipmentLocation(reader));
            Connection.Close();
        }

        private void LoadEquipments()
        {
            _equipments = new List<ShipmentEquipment>();
			if (IsNew) return;
            const string query = @"SELECT * FROM ShipmentEquipment WHERE ShipmentId = @ShipmentId AND TenantId = @TenantId;";
            var parameters = new Dictionary<string, object> { { "ShipmentId", Id }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _equipments.Add(new ShipmentEquipment(reader));
            Connection.Close();
        }

        private void LoadCustomeReferences()
        {
            _customerReferences = new List<ShipmentReference>();
			if (IsNew) return;
            const string query = @"SELECT * FROM ShipmentReference WHERE ShipmentId = @ShipmentId AND TenantId = @TenantId;";
            var parameters = new Dictionary<string, object> { { "ShipmentId", Id }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _customerReferences.Add(new ShipmentReference(reader));
            Connection.Close();
        }

        private void LoadServices()
        {
            _services = new List<ShipmentService>();
			if (IsNew) return;
            const string query = @"SELECT * FROM ShipmentService WHERE ShipmentId = @ShipmentId AND TenantId = @TenantId;";
            var parameters = new Dictionary<string, object> { { "ShipmentId", Id }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _services.Add(new ShipmentService(reader));
            Connection.Close();
        }

        private void LoadQuickPayOptions()
        {
            _quickPayOptions = new List<ShipmentQuickPayOption>();
			if (IsNew) return;
            const string query = @"SELECT * FROM ShipmentQuickPayOption WHERE ShipmentId = @ShipmentId AND TenantId = @TenantId;";
            var parameters = new Dictionary<string, object> { { "ShipmentId", Id }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _quickPayOptions.Add(new ShipmentQuickPayOption(reader));
            Connection.Close();
        }

        private void LoadAutoRatingAccessorials()
        {
            _autoRatingAccessorials = new List<ShipmentAutoRatingAccessorial>();
			if (IsNew) return;
            const string query = @"SELECT * FROM ShipmentAutoRatingAccessorial WHERE ShipmentId = @ShipmentId AND TenantId = @TenantId;";
            var parameters = new Dictionary<string, object> { { "ShipmentId", Id }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _autoRatingAccessorials.Add(new ShipmentAutoRatingAccessorial(reader));
            Connection.Close();
        }

        private void LoadDocuments()
        {
            _documents = new List<ShipmentDocument>();
			if (IsNew) return;
            const string query = @"SELECT * FROM ShipmentDocument WHERE ShipmentId = @ShipmentId AND TenantId = @TenantId;";
            var parameters = new Dictionary<string, object> { { "ShipmentId", Id }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _documents.Add(new ShipmentDocument(reader));
            Connection.Close();
        }

        private void LoadAssets()
        {
            _assets = new List<ShipmentAsset>();
			if (IsNew) return;
            const string query = @"SELECT * FROM ShipmentAsset WHERE ShipmentId = @ShipmentId AND TenantId = @TenantId;";
            var parameters = new Dictionary<string, object> { { "ShipmentId", Id }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _assets.Add(new ShipmentAsset(reader));
            Connection.Close();
        }

        private void LoadAccountBuckets()
        {
            _accountBuckets = new List<ShipmentAccountBucket>();
			if (IsNew) return;
            const string query = @"SELECT * FROM ShipmentAccountBucket WHERE ShipmentId = @ShipmentId AND TenantId = @TenantId;";
            var parameters = new Dictionary<string, object> { { "ShipmentId", Id }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _accountBuckets.Add(new ShipmentAccountBucket(reader));
            Connection.Close();
        }

        private void LoadCheckCalls()
        {
            _checkCalls = new List<CheckCall>();
			if (IsNew) return;
            const string query = @"SELECT * FROM CheckCall WHERE ShipmentId = @ShipmentId AND TenantId = @TenantId";
            var parameters = new Dictionary<string, object> { { "ShipmentId", Id }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _checkCalls.Add(new CheckCall(reader));
            Connection.Close();
        }
    }
}
