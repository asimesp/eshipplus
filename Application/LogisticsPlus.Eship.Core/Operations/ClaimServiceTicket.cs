﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("ClaimServiceTicket", ReadOnly = false, Source = EntitySource.TableView)]
	public class ClaimServiceTicket : TenantBase
	{
        private long _serviceTicketId;
		private long _claimId;

	    private Claim _claim;
	    private ServiceTicket _serviceTicket;

	    [EnableSnapShot("ServiceTicketId", Description = "Service Ticket Reference")]
		[Property("ServiceTicketId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long ServiceTicketId
		{
			get
			{
                if (_serviceTicket != null) _serviceTicketId = _serviceTicket.Id;
                return _serviceTicketId;
			}
			set
			{
				_serviceTicketId = value;
				if (_serviceTicket != null && value != _serviceTicket.Id) _serviceTicket = null;
			}
		}

		[EnableSnapShot("ClaimId", Description = "Claim Reference")]
		[Property("ClaimId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
        public long ClaimId
        {
            get
            {
                if (_claim != null) _claimId = _claim.Id;
                return _claimId;
            }
            set
            {
                _claimId = value;
                if (_claim != null && value != _claim.Id) _claim = null;
            }
        }

        public Claim Claim
        {
            get { return _claim ?? (_claim = new Claim(_claimId)); }
            set
            {
                _claim = value;
                _claimId = value == null ? default(long) : value.Id;
            }
        }
		public ServiceTicket ServiceTicket
        {
            get { return _serviceTicket ?? (_serviceTicket = new ServiceTicket(_serviceTicketId)); }
            set
            {
                _serviceTicket = value;
                _serviceTicketId = value == null ? default(long) : value.Id;
            }
        }

		public ClaimServiceTicket()
		{
			
		}

		public ClaimServiceTicket(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			Insert();
		}

		public new void Delete()
		{
			base.Delete();
		}
	}
}
