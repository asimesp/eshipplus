﻿namespace LogisticsPlus.Eship.Core.Operations
{
	public enum ComponentType
	{
		LoadOrder = 0,
		Shipment,
		ServiceTicket
	}
}
