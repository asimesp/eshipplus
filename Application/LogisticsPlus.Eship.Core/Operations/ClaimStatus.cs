﻿namespace LogisticsPlus.Eship.Core.Operations
{
	public enum ClaimStatus
	{
        // DO NOT RE-ORDER. SEE APPLICABLE JOINS ON DATABASE REFCLAIMSTATUS
        Open = 0,
		ClosedResolved,
		ClosedUnresolved,
		Cancelled
	}
}
