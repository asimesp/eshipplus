﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
    [Serializable]
    [Entity("LoadOrderHistoricalData", ReadOnly = false, Source = EntitySource.TableView)]
    public class LoadOrderHistoricalData : ObjectBase
    {
        private long _loadOrderId;

        private LoadOrder _loadOrder;


        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; private set; }

        [EnableSnapShot("LoadOrderId", Description = "Load Order Reference")]
        [Property("LoadOrderId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long LoadOrderId
        {
            get
            {
	            if (_loadOrder != null) _loadOrderId = _loadOrder.Id;
	            return _loadOrderId;
            }
            set
            {
                _loadOrderId = value;
                if (_loadOrder != null && value != _loadOrder.Id) _loadOrder = null;
            }
        }

        [EnableSnapShot("WasOffered", Description = "Was Offered")]
        [Property("WasOffered", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool WasOffered { get; set; }

        [EnableSnapShot("WasQuoted", Description = "Was Quoted")]
        [Property("WasQuoted", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool WasQuoted { get; set; }

        [EnableSnapShot("WasCancelled", Description = "Was Cancelled")]
        [Property("WasCancelled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool WasCancelled { get; set; }

        [EnableSnapShot("WasCovered", Description = "Was Covered")]
        [Property("WasCovered", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool WasCovered { get; set; }

        [EnableSnapShot("WasLost", Description = "Was Lost")]
        [Property("WasLost", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool WasLost { get; set; }

        [EnableSnapShot("WasAccepted", Description = "Was Accepted")]
        [Property("WasAccepted", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool WasAccepted { get; set; }

        [EnableSnapShot("WasShipment", Description = "Was Shipment")]
        [Property("WasShipment", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool WasShipment { get; set; }

        public LoadOrder LoadOrder
        {
            get { return _loadOrderId == default(long) ? null : _loadOrder ?? (_loadOrder = new LoadOrder(_loadOrderId, false)); }
            set
            {
                _loadOrder = value;
                _loadOrderId = value == null ? default(long) : value.Id;
            }
        }


        public bool IsNew
		{
			get { return Id == default(long); }
		}

		public LoadOrderHistoricalData()
		{		
		}

		public LoadOrderHistoricalData(long id)
			: this(id, false)
		{

		}

        public LoadOrderHistoricalData(long id, bool takeSnapshot)
		{
            Id = id;
            if (Id == default(long)) return;
            KeyLoaded = Load();
            if (takeSnapshot) TakeSnapShot();
		}

        public LoadOrderHistoricalData(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
            if (Id == default(long)) Insert();
            else Update();
		}

		public new void Delete()
		{
            if (Id != default(long)) base.Delete();
		}
    }
}
