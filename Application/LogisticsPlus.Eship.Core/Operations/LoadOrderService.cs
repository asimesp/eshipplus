﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("LoadOrderService", ReadOnly = false, Source = EntitySource.TableView)]
	public class LoadOrderService : TenantBase
	{
		private long _serviceId;
		private long _loadOrderId;

		private LoadOrder _loadOrder;
		private Service _service;

		[EnableSnapShot("ServiceId", Description = "Service Reference")]
		[Property("ServiceId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long ServiceId
		{
			get
			{
				if (_service != null) _serviceId = _service.Id;
				return _serviceId;
			}
			set
			{
				_serviceId = value;
				if (_service != null && value != _service.Id) _service = null;
			}
		}

		[EnableSnapShot("LoadOrderId", Description = "Load Order Reference")]
		[Property("LoadOrderId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long LoadOrderId
		{
			get
			{
				if (_loadOrder != null) _loadOrderId = _loadOrder.Id;
				return _loadOrderId;
			}
			set
			{
				_loadOrderId = value;
				if (_loadOrder != null && value != _loadOrder.Id) _loadOrder = null;
			}
		}

		public LoadOrder LoadOrder
		{
			get { return _loadOrder ?? (_loadOrder = new LoadOrder(_loadOrderId)); }
			set
			{
				_loadOrder = value;
				_loadOrderId = value == null ? default(long) : value.Id;
			}
		}
		public Service Service
		{
			get { return _service ?? (_service = new Service(_serviceId)); }
			set
			{
				_service = value;
				_serviceId = value == null ? default(long) : value.Id;
			}
		}

		public LoadOrderService()
		{
		}

		public LoadOrderService(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			Insert();
		}

		public new void Delete()
		{
			base.Delete();
		}
	}
}
