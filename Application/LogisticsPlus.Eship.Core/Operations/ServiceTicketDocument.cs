﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("ServiceTicketDocument", ReadOnly = false, Source = EntitySource.TableView)]
	public class ServiceTicketDocument: Document
	{
		private long _ticketId;
		private long _vendorBillId;

		private ServiceTicket _ticket;
		private VendorBill _vendorBill;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("IsInternal", Description = "Is Internal")]
		[Property("IsInternal", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool IsInternal { get; set; }

		[EnableSnapShot("FtpDelivered", Description = "Ftp Delivered")]
		[Property("FtpDelivered", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool FtpDelivered { get; set; }

		[Property("ServiceTicketId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ServiceTicketId
		{
			get
			{
				if (_ticket != null) _ticketId = _ticket.Id;
				return _ticketId;
			}
			set
			{
				_ticketId = value;
				if (_ticket != null && value != _ticket.Id) _ticket = null;
			}
		}

		[EnableSnapShot("VendorBillId", Description = "Vendor Bill Id")]
		[Property("VendorBillId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long VendorBillId
		{
			get
			{
				if (_vendorBill != null) _vendorBillId = _vendorBill.Id;
				return _vendorBillId;
			}
			set
			{
				_vendorBillId = value;
				if (_vendorBill != null && value != _vendorBill.Id) _vendorBill = null;
			}
		}

		public ServiceTicket ServiceTicket
		{
			get { return _ticket ?? (_ticket = new ServiceTicket(_ticketId)); }
			set
			{
				_ticket = value;
				_ticketId = value == null ? default(long) : value.Id;
			}
		}

		public VendorBill VendorBill
		{
			get { return _vendorBill ?? (_vendorBill = new VendorBill(_vendorBillId)); }
			set
			{
				_vendorBill = value;
				_vendorBillId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public ServiceTicketDocument() { }

		public ServiceTicketDocument(long id) : this(id, false) { }

		public ServiceTicketDocument(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public ServiceTicketDocument(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
