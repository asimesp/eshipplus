﻿namespace LogisticsPlus.Eship.Core.Operations
{
	public enum MileageEngine
	{
        // DO NOT RE-ORDER. SEE APPLICABLE JOINS ON DATABASE REFMILEAGEENGINE
		LongitudeLatitude = 0,	// this will be the default mileage engine implementation!
		PcMiler25,
		Other
	}
}
