﻿namespace LogisticsPlus.Eship.Core.Operations
{
	public enum ClaimType
	{
        NotApplicable = 0,
        ConcealedDamage,
        ConcealedLoss,
		Damage,
        Delay,
        Loss,
        ShipmentLost,
        Shortage,
        Theft,
        VisualDamage,
        Wreck
	}
}
