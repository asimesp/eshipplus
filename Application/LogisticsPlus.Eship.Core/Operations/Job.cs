﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("Job", ReadOnly = false, Source = EntitySource.TableView)]
	public class Job : TenantBase
	{
		private long _customerId;
		private long _createdByUserId;

		private Customer _customer;
		private User _createdByUser;
        private List<JobDocument> _documents;


        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("JobNumber", Description = "Job Number")]
		[Property("JobNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string JobNumber { get; set; }

		[EnableSnapShot("DateCreated", Description = "Date Created")]
		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[EnableSnapShot("Status", Description = "Job Status")]
		[Property("Status", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public JobStatus Status { get; set; }

		[EnableSnapShot("ExternalReference1", Description = "External Reference 1")]
		[Property("ExternalReference1", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ExternalReference1 { get; set; }

		[EnableSnapShot("ExternalReference2", Description = "External Reference 2")]
		[Property("ExternalReference2", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ExternalReference2 { get; set; }

		[EnableSnapShot("CustomerId", Description = "Customer Reference")]
		[Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long CustomerId
		{
			get { return _customerId; }
			set
			{
				_customerId = value;
				if (_customer != null && value != _customer.Id) _customer = null;
			}
		}

		[EnableSnapShot("CreatedByUserId", Description = "User Reference")]
		[Property("CreatedByUserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long CreatedByUserId
		{
			get { return _createdByUserId; }
			set
			{
				_createdByUserId = value;
				if (_createdByUser != null && value != _createdByUser.Id) _createdByUser = null;
			}
		}

		

		public Customer Customer
		{
			get { return _customerId == default(long) ? null : _customer ?? (_customer = new Customer(_customerId, false)); }
			set
			{
				_customer = value;
				_customerId = value == null ? default(long) : value.Id;
			}
		}

		public User CreatedByUser
		{
			get { return _createdByUserId == default(long) ? null : _createdByUser ?? (_createdByUser = new User(_createdByUserId, false)); }
			set
			{
				_createdByUser = value;
				_createdByUserId = value == null ? default(long) : value.Id;
			}
		}

        public List<JobDocument> Documents
        {
            get
            {
                if (_documents == null) LoadDocuments();
                return _documents;
            }
            set { _documents = value; }
        }


        public bool IsNew
        {
            get { return Id <= default(long); }
        }

        public Job() { }

        public Job(long id) : this(id, false) { }

        public Job(long id, bool takeSnapshot)
        {
            Id = id;
            if (Id == default(long)) return;
            KeyLoaded = Load();
            if (takeSnapshot) TakeSnapShot();
        }

		public Job(DbDataReader reader)
        {
            Load(reader);
        }

        public void Save()
        {
            if (Id == default(long)) Insert();
            else Update();
        }

        public new void Delete()
        {
            if (Id != default(long))
                base.Delete();
        }

	    public void ResetId()
	    {
	        Id = default(long);
	    }

        private void LoadDocuments()
        {
            _documents = new List<JobDocument>();
            if (IsNew) return;
            const string query = @"SELECT * FROM JobDocument WHERE JobId = @JobId AND TenantId = @TenantId;";
            var parameters = new Dictionary<string, object> { { "JobId", Id }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _documents.Add(new JobDocument(reader));
            Connection.Close();
        }
    }
}
