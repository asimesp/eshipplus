﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
    [Serializable]
	[Entity("CustomerTLTenderingProfileVendor", ReadOnly = false, Source = EntitySource.TableView)]
    public class CustomerTLTenderingProfileVendor : TenantBase
    {
        private long _vendorId;
		private long _tlProfileId;

		private CustomerTLTenderingProfile _tlProfile;
		private Vendor _vendor;

        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; private set; }

		[EnableSnapShot("VendorId", Description = "Vendor Reference")]
		[Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long VendorId
		{
			get
			{ return _vendorId; }
			set
			{
				_vendorId = value;
				if (_vendor != null && value != _vendor.Id) _vendor = null;
			}
		}

        [EnableSnapShot("CommunicationType", Description = "Communication Type")]
        [Property("CommunicationType", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public CustomerTLProfileVendorCommunicationType CommunicationType { get; set; }
		
		[EnableSnapShot("TLTenderingProfileId", Description = "TLTenderingProfile Reference")]
        [Property("TLTenderingProfileId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long TLTenderingProfileId
		{
			get
			{
				if (_tlProfile != null) _tlProfileId = _tlProfile.Id;
				return _tlProfileId;
			}
			set
			{
				_tlProfileId = value;
				if (_tlProfile != null && value != _tlProfile.Id) _tlProfile = null;
			}
		}

		public CustomerTLTenderingProfile TLTenderingProfile
		{
			get { return _tlProfile ?? (_tlProfile = new CustomerTLTenderingProfile(_tlProfileId)); }
			set
			{
				_tlProfile = value;
				_tlProfileId = value == null ? default(long) : value.Id;
			}
		}
		public Vendor Vendor
		{
			get { return _vendor ?? (_vendor = new Vendor(_vendorId)); }
			set
			{
				_vendor = value;
				_vendorId = value == null ? default(long) : value.Id;
			}
		}

        public bool IsNew
        {
            get { return Id == default(long); }
        }

        public CustomerTLTenderingProfileVendor()
		{
			
		}

        public CustomerTLTenderingProfileVendor(long id) : this(id, false) { }

        public CustomerTLTenderingProfileVendor(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public CustomerTLTenderingProfileVendor(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
            if (Id == default(long)) Insert();
            else Update();
		}

		public new void Delete()
		{
			base.Delete();
		}
    }
}
