﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("ShipmentItem", ReadOnly = false, Source = EntitySource.TableView)]
	public class ShipmentItem : TenantBase
	{
		private long _packageTypeId;
		private long _shipmentId;

		private Shipment _shipment;
		private PackageType _packageType;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("Description")]
		[Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Description { get; set; }

		[EnableSnapShot("ActualFreightClass", Description = "Actual Freight Class")]
		[Property("ActualFreightClass", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
		public double ActualFreightClass { get; set; }

		[EnableSnapShot("RatedFreightClass", Description = "Rated Freight Class")]
		[Property("RatedFreightClass", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
		public double RatedFreightClass { get; set; }

		[EnableSnapShot("Comment")]
		[Property("Comment", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Comment { get; set; }

		[EnableSnapShot("ActualWeight", Description = "Actual Weight")]
		[Property("ActualWeight", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal ActualWeight { get; set; }

		[EnableSnapShot("ActualLength", Description = "Actual Length")]
		[Property("ActualLength", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal ActualLength { get; set; }

		[EnableSnapShot("ActualWidth", Description = "Actual Width")]
		[Property("ActualWidth", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal ActualWidth { get; set; }

		[EnableSnapShot("ActualHeight", Description = "Actual Height")]
		[Property("ActualHeight", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal ActualHeight { get; set; }

		[EnableSnapShot("Quantity", Description = "Package Quantity")]
		[Property("Quantity", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int Quantity { get; set; }

		[EnableSnapShot("PieceCount", Description = "Piece Count")]
		[Property("PieceCount", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int PieceCount { get; set; }

		[EnableSnapShot("IsStackable", Description = "Is Stackable")]
		[Property("IsStackable", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool IsStackable { get; set; }

		[EnableSnapShot("HazardousMaterial", Description = "Is Stackable")]
		[Property("HazardousMaterial", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool HazardousMaterial { get; set; }

		[EnableSnapShot("PackageTypeId", Description = "Package Type Reference")]
		[Property("PackageTypeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long PackageTypeId
		{
			get { return _packageTypeId; }
			set
			{
				_packageTypeId = value;
				if (_packageType != null && value != _packageType.Id) _packageType = null;
			}
		}

		[EnableSnapShot("Pickup", Description = "Pickup Location Stop Order Reference")]
		[Property("Pickup", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int Pickup { get; set; }

		[EnableSnapShot("Delivery", Description = "Delivery Location Stop Order Reference")]
		[Property("Delivery", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int Delivery { get; set; }

		[EnableSnapShot("NMFCCode", Description = "National Motor Freight Classification Code")]
		[Property("NMFCCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string NMFCCode { get; set; }

		[EnableSnapShot("HTSCode", Description = "Harmonized Tarriff Schedule Code")]
		[Property("HTSCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string HTSCode { get; set; }

		[EnableSnapShot("Value", Description = "Value")]
		[Property("Value", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal Value { get; set; }

		[EnableSnapShot("ShipmentId", Description = "Shipment Reference")]
		[Property("ShipmentId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long ShipmentId
		{
			get
			{
				if (_shipment != null) _shipmentId = _shipment.Id;
				return _shipmentId;
			}
			set
			{
				_shipmentId = value;
				if (_shipment != null && value != _shipment.Id) _shipment = null;
			}
		}

		public Shipment Shipment
		{
			get { return _shipment ?? (_shipment = new Shipment(_shipmentId)); }
			set
			{
				_shipment = value;
				_shipmentId = value == null ? default(long) : value.Id;
			}
		}
		public PackageType PackageType
		{
			get { return _packageTypeId == default(long) ? null : _packageType ?? (_packageType = new PackageType(_packageTypeId)); }
			set
			{
				_packageType = value;
				_packageTypeId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public ShipmentItem() { }

		public ShipmentItem(long id) : this(id, false) { }

		public ShipmentItem(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public ShipmentItem(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
