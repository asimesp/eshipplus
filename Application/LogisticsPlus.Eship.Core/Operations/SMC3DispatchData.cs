﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
    [Entity("SMC3DispatchData", ReadOnly = false, Source = EntitySource.TableView)]
	public class SMC3DispatchData : ObjectBase
    {
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

        [EnableSnapShot("TransactionId", Description = "Transaction Id")]
        [Property("TransactionId", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string TransactionId { get; set; }

        [EnableSnapShot("Scac", Description = "Scac")]
        [Property("Scac", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Scac { get; set; }

        [EnableSnapShot("PickupNumber", Description = "Pickup Number")]
        [Property("PickupNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string PickupNumber { get; set; }

        [EnableSnapShot("BarcodeNumber", Description = "Barcode Number")]
        [Property("BarcodeNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string BarcodeNumber { get; set; }

        [EnableSnapShot("DateAccepted", Description = "Date Accepted")]
        [Property("DateAccepted", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DateAccepted { get; set; }

        [EnableSnapShot("Identifiers", Description = "Identifiers")]
        [Property("Identifiers", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Identifiers { get; set; }

        [EnableSnapShot("ResponseStatus", Description = "Response Status")]
        [Property("ResponseStatus", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ResponseStatus { get; set; }

        [EnableSnapShot("ResponseCode", Description = "Response Code")]
        [Property("ResponseCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ResponseCode { get; set; }

        [EnableSnapShot("ResponseMessage", Description = "Response Message")]
        [Property("ResponseMessage", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ResponseMessage { get; set; }
        
        [EnableSnapShot("DateCreated", Description = "Date Created")]
        [Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime DateCreated { get; set; }
        
        public bool IsNew
		{
			get { return Id == default(long); }
		}

        public SMC3DispatchData()
        {
        }

        public SMC3DispatchData(DbDataReader reader)
		{
			Load(reader);
		}
        
		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long)) base.Delete();
		}

		public new void TakeSnapShot()
		{
			GetSnapShot();
		}
	}
}
