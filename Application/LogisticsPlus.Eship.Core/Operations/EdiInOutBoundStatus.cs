﻿namespace LogisticsPlus.Eship.Core.Operations
{
	public enum EdiInOutBoundStatus
	{
		// Do not re-order
		Accepted = 0,
		Received,
		Rejected,
		Sent,
		NotApplicable
	}
}
