﻿namespace LogisticsPlus.Eship.Core.Operations
{
	public enum AssetType
	{
		Driver = 0,
		IndependentTractor,
		Trailer,
		Other,
        OwnerOpTractor,
        CompanyTractor
	}
}
