﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("SMC3TrackingData", ReadOnly = false, Source = EntitySource.TableView)]
	public class SMC3TrackingData : ObjectBase
    {
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

        [EnableSnapShot("TransactionId", Description = "Transaction Id")]
        [Property("TransactionId", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string TransactionId { get; set; }

        [EnableSnapShot("AccountToken", Description = "Account Token")]
        [Property("AccountToken", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string AccountToken { get; set; }

        [EnableSnapShot("ReferenceNumber", Description = "Reference Number")]
        [Property("ReferenceNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ReferenceNumber { get; set; }

        [EnableSnapShot("ReferenceType", Description = "Reference Type")]
        [Property("ReferenceType", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ReferenceType { get; set; }

        [EnableSnapShot("SCAC", Description = "SCAC")]
        [Property("SCAC", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string SCAC { get; set; }

        [EnableSnapShot("Pro", Description = "Pro")]
        [Property("Pro", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Pro { get; set; }

        [EnableSnapShot("Bol", Description = "Bol")]
        [Property("Bol", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Bol { get; set; }

        [EnableSnapShot("PO", Description = "PO")]
        [Property("PO", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string PO { get; set; }

        [EnableSnapShot("StatusCode", Description = "Status Code")]
        [Property("StatusCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string StatusCode { get; set; }

        [EnableSnapShot("StatusEventType", Description = "Status Event Type")]
        [Property("StatusEventType", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string StatusEventType { get; set; }

        [EnableSnapShot("StatusDate", Description = "")]
        [Property("StatusDate", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string StatusDate { get; set; }

        [EnableSnapShot("StatusDescription", Description = "Status Description")]
        [Property("StatusDescription", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string StatusDescription { get; set; }

        [EnableSnapShot("StatusTime", Description = "Status Time")]
        [Property("StatusTime", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string StatusTime { get; set; }

        [EnableSnapShot("StatusCity", Description = "Status City")]
        [Property("StatusCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string StatusCity { get; set; }

        [EnableSnapShot("StatusCountry", Description = "Status Country")]
        [Property("StatusCountry", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string StatusCountry { get; set; }

        [EnableSnapShot("StatusStateProvince", Description = "Status State Province")]
        [Property("StatusStateProvince", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string StatusStateProvince { get; set; }

        [EnableSnapShot("StatusPostalCode", Description = "Status Postal Code")]
        [Property("StatusPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string StatusPostalCode { get; set; }

        [EnableSnapShot("AppointmentScheduled")]
        [Property("AppointmentScheduled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool AppointmentScheduled { get; set; }
        
        [EnableSnapShot("AppointmentDate", Description = "Appointment Date")]
        [Property("AppointmentDate", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string AppointmentDate { get; set; }

        [EnableSnapShot("AppointmentTime", Description = "Appointment Time")]
        [Property("AppointmentTime", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string AppointmentTime { get; set; }

        [EnableSnapShot("AppointmentType", Description = "Appointment Type")]
        [Property("AppointmentType", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string AppointmentType { get; set; }

        [EnableSnapShot("PickupDate", Description = "Pickup Date")]
        [Property("PickupDate", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string PickupDate { get; set; }

        [EnableSnapShot("PickupTime", Description = "Pickup Time")]
        [Property("PickupTime", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string PickupTime { get; set; }

        [EnableSnapShot("DeliveryDate", Description = "Delivery Date")]
        [Property("DeliveryDate", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DeliveryDate { get; set; }

        [EnableSnapShot("DeliveryTime", Description = "Delivery Time")]
        [Property("DeliveryTime", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DeliveryTime { get; set; }

        [EnableSnapShot("ETADestinationDate", Description = "ETA Destination Date")]
        [Property("ETADestinationDate", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ETADestinationDate { get; set; }

        [EnableSnapShot("ETADestinationTime", Description = "ETA Destination Time")]
        [Property("ETADestinationTime", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ETADestinationTime { get; set; }

        [EnableSnapShot("CCXLDestinationDate", Description = "CCXL Destination Date")]
        [Property("CCXLDestinationDate", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string CCXLDestinationDate { get; set; }

        [EnableSnapShot("CCXLDestinationTime", Description = "CCXL Destination Time")]
        [Property("CCXLDestinationTime", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string CCXLDestinationTime { get; set; }

        [EnableSnapShot("CCXLCalendarDays", Description = "CCXL Calendar Days")]
        [Property("CCXLCalendarDays", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string CCXLCalendarDays { get; set; }

        [EnableSnapShot("InterlineContactPhone", Description = "Interline Contac tPhone")]
        [Property("InterlineContactPhone", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string InterlineContactPhone { get; set; }

        [EnableSnapShot("InterlinePartnerName", Description = "Interline Partner Name")]
        [Property("InterlinePartnerName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string InterlinePartnerName { get; set; }

        [EnableSnapShot("InterlineNotes", Description = "Interline Notes")]
        [Property("InterlineNotes", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string InterlineNotes { get; set; }

        [EnableSnapShot("InterlinePro", Description = "Interline Pro")]
        [Property("InterlinePro", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string InterlinePro { get; set; }

        [EnableSnapShot("InterlineSCAC", Description = "Interline SCAC")]
        [Property("InterlineSCAC", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string InterlineSCAC { get; set; }

        [EnableSnapShot("OriginTerminalCode", Description = "Origin Terminal Code")]
        [Property("OriginTerminalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginTerminalCode { get; set; }

        [EnableSnapShot("OriginTerminalPhone", Description = "Origin Terminal Phone")]
        [Property("OriginTerminalPhone", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginTerminalPhone { get; set; }

        [EnableSnapShot("OriginTerminalFax", Description = "Origin Terminal Fax")]
        [Property("OriginTerminalFax", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginTerminalFax { get; set; }

        [EnableSnapShot("OriginTerminalTollFree", Description = "Origin Terminal Toll Free")]
        [Property("OriginTerminalTollFree", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginTerminalTollFree { get; set; }

        [EnableSnapShot("OriginTerminalContactName", Description = "Origin Terminal Contact Name")]
        [Property("OriginTerminalContactName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginTerminalContactName { get; set; }

        [EnableSnapShot("OriginTerminalEmail", Description = "Origin Terminal Email")]
        [Property("OriginTerminalEmail", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginTerminalEmail { get; set; }

        [EnableSnapShot("OriginTerminalCity", Description = "Origin Terminal City")]
        [Property("OriginTerminalCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginTerminalCity { get; set; }

        [EnableSnapShot("OriginTerminalCountry", Description = "Origin Terminal Country")]
        [Property("OriginTerminalCountry", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginTerminalCountry { get; set; }

        [EnableSnapShot("OriginTerminalStateProvince", Description = "Origin Terminal State Province")]
        [Property("OriginTerminalStateProvince", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginTerminalStateProvince { get; set; }

        [EnableSnapShot("OriginTerminalPostalCode", Description = "Origin Terminal Postal Code")]
        [Property("OriginTerminalPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginTerminalPostalCode { get; set; }

        [EnableSnapShot("DestinationTerminalCode", Description = "Destination Terminal Code")]
        [Property("DestinationTerminalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationTerminalCode { get; set; }

        [EnableSnapShot("DestinationTerminalPhone", Description = "Destination Terminal Phone")]
        [Property("DestinationTerminalPhone", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationTerminalPhone { get; set; }

        [EnableSnapShot("DestinationTerminalFax", Description = "Destination Terminal Fax")]
        [Property("DestinationTerminalFax", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationTerminalFax { get; set; }

        [EnableSnapShot("DestinationTerminalTollFree", Description = "Destination Terminal Toll Free")]
        [Property("DestinationTerminalTollFree", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationTerminalTollFree { get; set; }

        [EnableSnapShot("DestinationTerminalContactName", Description = "Destination Terminal Contact Name")]
        [Property("DestinationTerminalContactName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationTerminalContactName { get; set; }

        [EnableSnapShot("DestinationTerminalEmail", Description = "Destination Terminal Email")]
        [Property("DestinationTerminalEmail", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationTerminalEmail { get; set; }

        [EnableSnapShot("DestinationTerminalCity", Description = "Destination Terminal City")]
        [Property("DestinationTerminalCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationTerminalCity { get; set; }

        [EnableSnapShot("DestinationTerminalCountry", Description = "Destination Terminal Country")]
        [Property("DestinationTerminalCountry", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationTerminalCountry { get; set; }

        [EnableSnapShot("DestinationTerminalStateProvince", Description = "Destination Terminal State Province")]
        [Property("DestinationTerminalStateProvince", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationTerminalStateProvince { get; set; }

        [EnableSnapShot("DestinationTerminalPostalCode", Description = "Destination Terminal Postal Code")]
        [Property("DestinationTerminalPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationTerminalPostalCode { get; set; }

        [EnableSnapShot("EquipmentId", Description = "Equipment Id")]
        [Property("EquipmentId", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string EquipmentId { get; set; }

        [EnableSnapShot("EquipmentType", Description = "Equipment Type")]
        [Property("EquipmentType", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string EquipmentType { get; set; }
        
        [EnableSnapShot("Weight", Description = "Weight")]
        [Property("Weight", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal Weight { get; set; }

        [EnableSnapShot("WeightType", Description = "Weight Type")]
        [Property("WeightType", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string WeightType { get; set; }

        [EnableSnapShot("WeightUnit", Description = "Weight Unit")]
        [Property("WeightUnit", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string WeightUnit { get; set; }
        
        [EnableSnapShot("Pieces", Description = "Pieces")]
        [Property("Pieces", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int Pieces { get; set; }

        [EnableSnapShot("PackagingType", Description = "Packaging Type")]
        [Property("PackagingType", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string PackagingType { get; set; }
        
        [EnableSnapShot("HazardousMaterial")]
        [Property("HazardousMaterial", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool HazardousMaterial { get; set; }

        [EnableSnapShot("BillToAccount", Description = "Bill To Account")]
        [Property("BillToAccount", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string BillToAccount { get; set; }

        [EnableSnapShot("BillToAddress1", Description = "Bill To Address1")]
        [Property("BillToAddress1", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string BillToAddress1 { get; set; }

        [EnableSnapShot("BillToAddress2", Description = "Bill To Address2")]
        [Property("BillToAddress2", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string BillToAddress2 { get; set; }

        [EnableSnapShot("BillToCity", Description = "Bill To City")]
        [Property("BillToCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string BillToCity { get; set; }

        [EnableSnapShot("BillToCountry", Description = "Bill To Country")]
        [Property("BillToCountry", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string BillToCountry { get; set; }

        [EnableSnapShot("BillToName", Description = "Bill To Name")]
        [Property("BillToName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string BillToName { get; set; }

        [EnableSnapShot("BillToPhone", Description = "Bill To Phone")]
        [Property("BillToPhone", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string BillToPhone { get; set; }

        [EnableSnapShot("BillToStateProvince", Description = "Bill To State Province")]
        [Property("BillToStateProvince", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string BillToStateProvince { get; set; }

        [EnableSnapShot("BillToPostalCode", Description = "Bill To Postal Code")]
        [Property("BillToPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string BillToPostalCode { get; set; }

        [EnableSnapShot("PaymentTerms", Description = "Payment Terms")]
        [Property("PaymentTerms", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string PaymentTerms { get; set; }

        [EnableSnapShot("DestinationAccount", Description = "Destination Account")]
        [Property("DestinationAccount", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationAccount { get; set; }

        [EnableSnapShot("DestinationAddress1", Description = "Destination Address 1")]
        [Property("DestinationAddress1", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationAddress1 { get; set; }

        [EnableSnapShot("DestinationAddress2", Description = "Destination Address 2")]
        [Property("DestinationAddress2", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationAddress2 { get; set; }

        [EnableSnapShot("DestinationCity", Description = "Destination City")]
        [Property("DestinationCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationCity { get; set; }

        [EnableSnapShot("DestinationCountry", Description = "Destinatio nCountry")]
        [Property("DestinationCountry", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationCountry { get; set; }

        [EnableSnapShot("DestinationName", Description = "Destination Name")]
        [Property("DestinationName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationName { get; set; }

        [EnableSnapShot("DestinationPhone", Description = "Destination Phone")]
        [Property("DestinationPhone", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationPhone { get; set; }

        [EnableSnapShot("DestinationStateProvince", Description = "Destination State Province")]
        [Property("DestinationStateProvince", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationStateProvince { get; set; }

        [EnableSnapShot("DestinationPostalCode", Description = "Destination Postal Code")]
        [Property("DestinationPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationPostalCode { get; set; }

        [EnableSnapShot("OriginAccount", Description = "Origin Account")]
        [Property("OriginAccount", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginAccount { get; set; }

        [EnableSnapShot("OriginAddress1", Description = "Origin Address 1")]
        [Property("OriginAddress1", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginAddress1 { get; set; }

        [EnableSnapShot("OriginAddress2", Description = "Origin Address 2")]
        [Property("OriginAddress2", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginAddress2 { get; set; }

        [EnableSnapShot("OriginCity", Description = "Origin City")]
        [Property("OriginCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginCity { get; set; }

        [EnableSnapShot("OriginCountry", Description = "Origin Country")]
        [Property("OriginCountry", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginCountry { get; set; }

        [EnableSnapShot("OriginName", Description = "Origin Name")]
        [Property("OriginName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginName { get; set; }

        [EnableSnapShot("OriginPhone", Description = "Origin Phone")]
        [Property("OriginPhone", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginPhone { get; set; }

        [EnableSnapShot("OriginStateProvince", Description = "Origin State Province")]
        [Property("OriginStateProvince", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginStateProvince { get; set; }

        [EnableSnapShot("OriginPostalCode", Description = "Origin Postal Code")]
        [Property("OriginPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginPostalCode { get; set; }

        [EnableSnapShot("ResponseStatus", Description = "Response Status")]
        [Property("ResponseStatus", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ResponseStatus { get; set; }

        [EnableSnapShot("ResponseCode", Description = "Response Code")]
        [Property("ResponseCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ResponseCode { get; set; }

        [EnableSnapShot("ResponseMessage", Description = "Response Message")]
        [Property("ResponseMessage", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ResponseMessage { get; set; }
        
        [EnableSnapShot("DateCreated", Description = "Date Created")]
        [Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime DateCreated { get; set; }
        
        public bool IsNew
		{
			get { return Id == default(long); }
		}

        public SMC3TrackingData()
        {
        }

		public SMC3TrackingData(DbDataReader reader)
		{
			Load(reader);
		}
        
		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long)) base.Delete();
		}

		public new void TakeSnapShot()
		{
			GetSnapShot();
		}
	}
}
