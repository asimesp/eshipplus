﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
    [Serializable]
    [Entity("LoadOrderVendor", ReadOnly = false, Source = EntitySource.TableView)]
    public class LoadOrderVendor : TenantBase
    {
        private long _vendorId;
        private long _shipmentId;

        private LoadOrder _loadOrder;
        private Vendor _vendor;

        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; private set; }

        [EnableSnapShot("Primary")]
        [Property("Primary", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool Primary { get; set; }

        [EnableSnapShot("ProNumber", Description = "PRO Number")]
        [Property("ProNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ProNumber { get; set; }

        [EnableSnapShot("VendorId", Description = "Vendor Reference")]
        [Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long VendorId
        {
            get
            {
                if (_vendor != null) _vendorId = _vendor.Id;
                return _vendorId;
            }
            set
            {
                _vendorId = value;
                if (_vendor != null && value != _vendor.Id) _vendor = null;
            }
        }

        [EnableSnapShot("LoadOrderId", Description = "Load Order Reference")]
        [Property("LoadOrderId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        protected long LoadOrderId
        {
            get
            {
                if (_loadOrder != null) _shipmentId = _loadOrder.Id;
                return _shipmentId;
            }
            set
            {
                _shipmentId = value;
                if (_loadOrder != null && value != _loadOrder.Id) _loadOrder = null;
            }
        }

        public LoadOrder LoadOrder
        {
            get { return _loadOrder ?? (_loadOrder = new LoadOrder(_shipmentId)); }
            set
            {
                _loadOrder = value;
                _shipmentId = value == null ? default(long) : value.Id;
            }
        }
        public Vendor Vendor
        {
            get { return _vendor ?? (_vendor = new Vendor(_vendorId)); }
            set
            {
                _vendor = value;
                _vendorId = value == null ? default(long) : value.Id;
            }
        }

        public bool IsNew
        {
            get { return Id == default(long); }
        }

        public LoadOrderVendor() { }

        public LoadOrderVendor(long id) : this(id, false) { }

        public LoadOrderVendor(long id, bool takeSnapshot)
        {
            Id = id;
            if (Id == default(long)) return;
            KeyLoaded = Load();
            if (takeSnapshot) TakeSnapShot();
        }

        public LoadOrderVendor(DbDataReader reader)
        {
            Load(reader);
        }

        public void Save()
        {
            if (Id == default(long)) Insert();
            else Update();
        }

        public new void Delete()
        {
            if (Id != default(long))
                base.Delete();
        }
    }
}