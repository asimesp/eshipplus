﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("ServiceTicketItem", ReadOnly = false, Source = EntitySource.TableView)]
	public class ServiceTicketItem : TenantBase
	{
		private long _serviceTicketId;

		private ServiceTicket _serviceTicket;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("Description")]
		[Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Description { get; set; }

		[EnableSnapShot("Comment")]
		[Property("Comment", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Comment { get; set; }

		[EnableSnapShot("ServiceTicketId", Description = "Service Ticket Reference")]
		[Property("ServiceTicketId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long ServiceTicketId
		{
			get
			{
				if (_serviceTicket != null) _serviceTicketId = _serviceTicket.Id;
				return _serviceTicketId;
			}
			set
			{
				_serviceTicketId = value;
				if (_serviceTicket != null && value != _serviceTicket.Id) _serviceTicket = null;
			}
		}

		public ServiceTicket ServiceTicket
		{
			get { return _serviceTicket ?? (_serviceTicket = new ServiceTicket(_serviceTicketId)); }
			set
			{
				_serviceTicket = value;
				_serviceTicketId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public ServiceTicketItem() { }

		public ServiceTicketItem(long id) : this(id, false) { }

		public ServiceTicketItem(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public ServiceTicketItem(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
