﻿namespace LogisticsPlus.Eship.Core.Operations
{
    public enum RateSelectionFieldset
    {
        Ftl,
        LtlDensity,
        LtlFreightClass,
        NotApplicable,
        SmallPackage
    }
}
