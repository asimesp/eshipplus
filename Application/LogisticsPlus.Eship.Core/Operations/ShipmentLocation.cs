﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("ShipmentLocation", ReadOnly = false, Source = EntitySource.TableView)]
	public class ShipmentLocation : Location
	{
		private long _shipmentId;

		private Shipment _shipment;

		private List<ShipmentContact> _contacts;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("Description")]
		[Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Description { get; set; }

		[EnableSnapShot("SpecialInstructions", Description = "Special Instructions")]
		[Property("SpecialInstructions", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string SpecialInstructions { get; set; }

		[EnableSnapShot("GeneralInfo", Description = "General Info")]
		[Property("GeneralInfo", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string GeneralInfo { get; set; }

		[EnableSnapShot("Direction")]
		[Property("Direction", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Direction { get; set; }

		[EnableSnapShot("StopOrder", Description = "Location Stop Index")]
		[Property("StopOrder", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int StopOrder { get; set; }

		[EnableSnapShot("AppointmentDateTime", Description = "Appointment Date Time")]
		[Property("AppointmentDateTime", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime AppointmentDateTime { get; set; }

		[EnableSnapShot("MilesFromPreceedingStop", Description = "Miles From Preceeding Stop")]
		[Property("MilesFromPreceedingStop", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal MilesFromPreceedingStop { get; set; }

		[EnableSnapShot("ShipmentId", Description = "Shipment Reference")]
		[Property("ShipmentId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long ShipmentId
		{
			get
			{
				if (_shipment != null) _shipmentId = _shipment.Id;
				return _shipmentId;
			}
			set
			{
				_shipmentId = value;
				if (_shipment != null && value != _shipment.Id) _shipment = null;
			}
		}

		public Shipment Shipment
		{
			get { return _shipment ?? (_shipment = new Shipment(_shipmentId)); }
			set
			{
				_shipment = value;
				_shipmentId = value == null ? default(long) : value.Id;
			}
		}

		public List<ShipmentContact> Contacts
		{
			get
			{
				if (_contacts == null) LoadContacts();
				return _contacts;
			}
			set { _contacts = value; }
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public ShipmentLocation() { }

		public ShipmentLocation(long id) : this(id, false) { }

		public ShipmentLocation(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public ShipmentLocation(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

		public void LoadCollections()
		{
			LoadContacts();
		}

		private void LoadContacts()
		{
			_contacts = new List<ShipmentContact>();
			if (IsNew) return;
			const string query = @"SELECT * FROM ShipmentContact where ShipmentLocationId = @ShipmentLocationId and TenantId = @TenantId;";
			var parameters = new Dictionary<string, object> { { "ShipmentLocationId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_contacts.Add(new ShipmentContact(reader));
			Connection.Close();
		}
	}
}
