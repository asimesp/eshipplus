﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("AddressBookContact", ReadOnly = false, Source = EntitySource.TableView)]
	public class AddressBookContact : Contact
	{
		private long _addressBookId;

		private AddressBook _addressBook;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key =  true)]
		public long Id { get; private set; }

		[Property("AddressBookId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long AddressBookId
		{
			get
			{
				if (_addressBook != null) _addressBookId = _addressBook.Id;
				return _addressBookId;
			}
			set
			{
				_addressBookId = value;
				if (_addressBook != null && value != _addressBook.Id) _addressBook = null;
			}
		}

		public AddressBook AddressBook
		{
			get { return _addressBook ?? (_addressBook = new AddressBook(_addressBookId)); }
			set
			{
				_addressBook = value;
				_addressBookId = value == null ? default(long) : value.Id;
			}
		}

        public bool IsNew
        {
            get { return Id == default(long); }
        }

		public AddressBookContact(long id)
		{
			Id = id;
			if (Id == default(long)) return;
			Load();
		}

		public AddressBookContact(DbDataReader reader)
		{
		    Load(reader);
		}

        public AddressBookContact() { }

         public AddressBookContact(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

        public void Save()
        {
            if (Id == default(long)) Insert();
            else Update();
        }

        public new void Delete()
        {
            if (Id != default(long)) base.Delete();
        }
	}
}
