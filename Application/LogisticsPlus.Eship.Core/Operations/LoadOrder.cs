﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
    [Entity("LoadOrder", ReadOnly = false, Source = EntitySource.TableView)]
    public class LoadOrder : TenantBase
    {
        private long _prefixId;
        private long _customerId;
        private long _userId;
        private long _carrierCoordinatorUserId;
		private long _loadOrderCoordinatorUserId;
        private long _mileageSourceId;
        private long _salesRepresentativeId;
        private long _resellerAdditionId;
        private long _accountBucketUnitId;
		private long _shipmentPriorityId;
	    private long _jobId;

		private ShipmentPriority _shipmentPriority;
        private MileageSource _mileageSource;
        private ResellerAddition _resellerAddition;
        private SalesRepresentative _salesRepresentative;
        private Customer _customer;
        private Prefix _prefix;
        private User _user;
        private User _carrierCoordinator;
        private User _loadOrderCoordinator;
        private AccountBucketUnit _accountBucketUnit;
	    private Job _job;

		private List<LoadOrderVendor> _vendors;
		private List<LoadOrderNote> _notes;
		private List<LoadOrderItem> _items;
		private List<LoadOrderCharge> _charges;
		private List<LoadOrderLocation> _stops;
		private List<LoadOrderEquipment> _equipments;
		private List<LoadOrderReference> _customerReferences;
		private List<LoadOrderService> _services;
		private List<LoadOrderDocument> _documents;
		private List<LoadOrderAccountBucket> _accountBuckets;

        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; protected set; }

        [EnableSnapShot("LoadOrderNumber", Description = "Load Order Number")]
        [Property("LoadOrderNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string LoadOrderNumber { get; set; }

		[EnableSnapShot("Description", Description = "Dashboard Comments")]
		[Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Description { get; set; }

        [EnableSnapShot("PrefixId", Description = "Prefix Reference")]
        [Property("PrefixId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long PrefixId
        {
            get { return _prefixId; }
            set
            {
                _prefixId = value;
                if (_prefix != null && value != _prefix.Id) _prefix = null;
            }
        }

        [EnableSnapShot("HidePrefix", Description = "Hide Prefix")]
        [Property("HidePrefix", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool HidePrefix { get; set; }

        [EnableSnapShot("ShipperBol", Description = "Shipper Bill of Lading")]
        [Property("ShipperBol", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ShipperBol { get; set; }

        [EnableSnapShot("PurchaseOrderNumber", Description = "Purchase Order Number")]
        [Property("PurchaseOrderNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string PurchaseOrderNumber { get; set; }

        [EnableSnapShot("ShipperReference", Description = "Shipper Reference Number")]
        [Property("ShipperReference", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ShipperReference { get; set; }

        [EnableSnapShot("HazardousMaterial", Description = "Is Hazardous Material")]
        [Property("HazardousMaterial", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool HazardousMaterial { get; set; }

        [EnableSnapShot("HazardousMaterialContactName", Description = "Hazardous Material Contact Name")]
        [Property("HazardousMaterialContactName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string HazardousMaterialContactName { get; set; }

        [EnableSnapShot("HazardousMaterialContactPhone", Description = "Hazardous Material Contact Phone")]
        [Property("HazardousMaterialContactPhone", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string HazardousMaterialContactPhone { get; set; }

        [EnableSnapShot("HazardousMaterialContactMobile", Description = "Hazardous Material Contact Mobile")]
        [Property("HazardousMaterialContactMobile", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string HazardousMaterialContactMobile { get; set; }

        [EnableSnapShot("HazardousMaterialContactEmail", Description = "Hazardous Material Contact Email")]
        [Property("HazardousMaterialContactEmail", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string HazardousMaterialContactEmail { get; set; }

        [EnableSnapShot("DesiredPickupDate", Description = "Desired Pickup Date")]
        [Property("DesiredPickupDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime DesiredPickupDate { get; set; }

        [EnableSnapShot("EarlyPickup", Description = "Early Pickup Time")]
        [Property("EarlyPickup", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string EarlyPickup { get; set; }

        [EnableSnapShot("LatePickup", Description = "Late Pickup Time")]
        [Property("LatePickup", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string LatePickup { get; set; }

        [EnableSnapShot("EstimatedDeliveryDate", Description = "Estimated Delivery Date")]
        [Property("EstimatedDeliveryDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime EstimatedDeliveryDate { get; set; }

        [EnableSnapShot("EarlyDelivery", Description = "Early Delivery Time")]
        [Property("EarlyDelivery", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string EarlyDelivery { get; set; }

        [EnableSnapShot("LateDelivery", Description = "Late Delivery Time")]
        [Property("LateDelivery", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string LateDelivery { get; set; }

		[EnableSnapShot("Status", Description = "Load Order Status")]
		[Property("Status", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public LoadOrderStatus Status { get; set; }

		[EnableSnapShot("NotAvailableToLoadboards", Description = "Not Available To Loadboards")]
		[Property("NotAvailableToLoadboards", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool NotAvailableToLoadboards { get; set; }

        [EnableSnapShot("DateCreated", Description = "Date Created")]
        [Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime DateCreated { get; set; }

        [EnableSnapShot("ServiceMode", Description = "Service Mode")]
        [Property("ServiceMode", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public ServiceMode ServiceMode { get; set; }

		[EnableSnapShot("IsPartialTruckload", Description = "Is Partial TruckLoad")]
		[Property("IsPartialTruckload", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool IsPartialTruckload { get; set; }

        [EnableSnapShot("DeclineInsurance", Description = "Decline Insurance")]
        [Property("DeclineInsurance", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool DeclineInsurance { get; set; }

		[EnableSnapShot("LinearFootRuleBypassed", Description = "Linear Foot Rule Bypassed")]
		[Property("LinearFootRuleBypassed", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool LinearFootRuleBypassed { get; set; }

        [EnableSnapShot("GeneralBolComments", Description = "General Bill of Lading Comments")]
        [Property("GeneralBolComments", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string GeneralBolComments { get; set; }

        [EnableSnapShot("CriticalBolComments", Description = "Critical Bill of Lading Comments")]
        [Property("CriticalBolComments", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string CriticalBolComments { get; set; }

        [EnableSnapShot("MiscField1", Description = "Misc Fields")]
        [Property("MiscField1", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string MiscField1 { get; set; }

        [EnableSnapShot("MiscField2", Description = "Misc Fields")]
        [Property("MiscField2", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string MiscField2 { get; set; }

		[EnableSnapShot("DriverName", Description = "Driver Name")]
		[Property("DriverName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DriverName { get; set; }

		[EnableSnapShot("DriverPhoneNumber", Description = "Driver Phone Number")]
		[Property("DriverPhoneNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DriverPhoneNumber { get; set; }

		[EnableSnapShot("DriverTrailerNumber", Description = "Driver Trailer Number")]
		[Property("DriverTrailerNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DriverTrailerNumber { get; set; }

        [EnableSnapShot("EmptyMileage", Description = "Empty Mileage")]
        [Property("EmptyMileage", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal EmptyMileage { get; set; }

        [EnableSnapShot("Mileage")]
        [Property("Mileage", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal Mileage { get; set; }

        [EnableSnapShot("MileageSourceId", Description = "Mileage Source Reference")]
        [Property("MileageSourceId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long MileageSourceId
        {
            get { return _mileageSourceId; }
            set
            {
                _mileageSourceId = value;
                if (_mileageSource != null && _mileageSource.Id != value) _mileageSource = null;
            }
        }

        [EnableSnapShot("SalesRepresentativeId", Description = "Sales Representative Reference")]
        [Property("SalesRepresentativeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long SalesRepresentativeId
        {
            get { return _salesRepresentativeId; }
            set
            {
                _salesRepresentativeId = value;
                if (_salesRepresentative != null && _salesRepresentative.Id != value) _salesRepresentative = null;
            }
        }

		[EnableSnapShot("SalesRepresentativeCommissionPercent", Description = "Sales Representative Commission Percent")]
        [Property("SalesRepresentativeCommissionPercent", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal SalesRepresentativeCommissionPercent { get; set; }

		[EnableSnapShot("SalesRepMinCommValue", Description = "Sales Representative Minimum Commission Value")]
		[Property("SalesRepMinCommValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal SalesRepMinCommValue { get; set; }

		[EnableSnapShot("SalesRepMaxCommValue", Description = "Sales Representative Maximum Commission Value")]
		[Property("SalesRepMaxCommValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal SalesRepMaxCommValue { get; set; }

		[EnableSnapShot("SalesRepAddlEntityName", Description = "Sales Representative Additional Entity Name")]
		[Property("SalesRepAddlEntityName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string SalesRepAddlEntityName { get; set; }

		[EnableSnapShot("SalesRepAddlEntityCommPercent", Description = "Sales Representative Additional Entity Commission Percent")]
		[Property("SalesRepAddlEntityCommPercent", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal SalesRepAddlEntityCommPercent { get; set; }

        [EnableSnapShot("ResellerAdditionId", Description = "Reseller Addition Reference")]
        [Property("ResellerAdditionId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long ResellerAdditionId
        {
            get { return _resellerAdditionId; }
            set
            {
                _resellerAdditionId = value;
                if (_resellerAddition != null && _resellerAddition.Id != value) _resellerAddition = null;
            }
        }

        [EnableSnapShot("AccountBucketUnitId", Description = "Account Bucket Unit Reference")]
        [Property("AccountBucketUnitId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long AccountBucketUnitId
        {
            get { return _accountBucketUnitId; }
            set
            {
                _accountBucketUnitId = value;
                if (_accountBucketUnit != null && _accountBucketUnit.Id != value) _accountBucketUnit = null;
            }
        }

        [EnableSnapShot("BillReseller", Description = "Bill Reseller")]
        [Property("BillReseller", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool BillReseller { get; set; }

		[EnableSnapShot("CarrierCoordinatorId", Description = "Carrier Coordinator Reference")]
		[Property("CarrierCoordinatorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false, Column = "CarrierCoordinatorUserId")]
		public long CarrierCoordinatorId
		{
			get { return _carrierCoordinatorUserId; }
			set
			{
				_carrierCoordinatorUserId = value;
				if (_carrierCoordinator != null && value != _carrierCoordinator.Id) _carrierCoordinator = null;
			}
		}

		[EnableSnapShot("LoadOrderCoordinatorId", Description = "LoadOrder Coordinator Reference")]
		[Property("LoadOrderCoordinatorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false, Column = "LoadOrderCoordinatorUserId")]
		public long LoadOrderCoordinatorId
		{
			get { return _loadOrderCoordinatorUserId; }
			set
			{
				_loadOrderCoordinatorUserId = value;
				if (_loadOrderCoordinator != null && value != _loadOrderCoordinator.Id) _loadOrderCoordinator = null;
			}
		}

        [EnableSnapShot("UserId", Description = "User Reference")]
        [Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        protected long UserId
        {
            get { return _userId; }
            set
            {
                _userId = value;
                if (_user != null && value != _user.Id) _user = null;
            }
        }

        [EnableSnapShot("CustomerId", Description = "Customer Reference")]
        [Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        protected long CustomerId
        {
            get { return _customerId; }
            set
            {
                _customerId = value;
                if (_customer != null && value != _customer.Id) _customer = null;
            }
        }

		[EnableSnapShot("OriginId", Description = "Origin Reference")]
		[Property("OriginId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long OriginId
		{
			get { return Origin == null ? default(long) : Origin.Id; }
			set
			{
				if (Origin != null && Origin.Id == value) return;
				Origin = value == default(long) ? null : new LoadOrderLocation(value, false);
			}
		}

		[EnableSnapShot("DestinationId", Description = "Destination Reference")]
		[Property("DestinationId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long DestinationId
		{
			get { return Destination == null ? default(long) : Destination.Id; }
			set
			{
				if (Destination != null && Destination.Id == value) return;
				Destination = value == default(long) ? null : new LoadOrderLocation(value, false);
			}
		}

		[EnableSnapShot("ShipmentPriorityId", Description = "Shipment Priority Reference")]
		[Property("ShipmentPriorityId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ShipmentPriorityId
		{
			get { return _shipmentPriorityId; }
			set
			{
				_shipmentPriorityId = value;
				if (_shipmentPriority != null && value != _shipmentPriority.Id) _shipmentPriority = null;
			}
		}

        [EnableSnapShot("JobId", Description = "Job Id")]
        [Property("JobId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long JobId
        {
            get { return _jobId; }
            set
            {
                _jobId = value;
                if (_job != null && _job.Id != value) _job = null;
            }
        }

        [EnableSnapShot("JobStep", Description = "Job Step")]
        [Property("JobStep", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int JobStep { get; set; }

        public Prefix Prefix
        {
            get { return _prefixId == default(long) ? null : _prefix ?? (_prefix = new Prefix(_prefixId, false)); }
            set
            {
                _prefix = value;
                _prefixId = value == null ? default(long) : value.Id;
            }
        }
        public Customer Customer
        {
            get { return _customerId == default(long) ? null : _customer ?? (_customer = new Customer(_customerId, false)); }
            set
            {
                _customer = value;
                _customerId = value == null ? default(long) : value.Id;
            }
        }
        public User User
        {
            get { return _userId == default(long) ? null : _user ?? (_user = new User(_userId, false)); }
            set
            {
                _user = value;
                _userId = value == null ? default(long) : value.Id;
            }
        }
		public User CarrierCoordinator
		{
			get { return _carrierCoordinatorUserId == default(long) ? null : _carrierCoordinator ?? (_carrierCoordinator = new User(_carrierCoordinatorUserId, false)); }
			set
			{
				_carrierCoordinator = value;
				_carrierCoordinatorUserId = value == null ? default(long) : value.Id;
			}
		}
		public User LoadOrderCoordinator
		{
			get { return _loadOrderCoordinatorUserId == default(long) ? null : _loadOrderCoordinator ?? (_loadOrderCoordinator = new User(_loadOrderCoordinatorUserId, false)); }
			set
			{
				_loadOrderCoordinator = value;
				_loadOrderCoordinatorUserId = value == null ? default(long) : value.Id;
			}
		}


        public LoadOrderLocation Origin { get; set; }
        public LoadOrderLocation Destination { get; set; }

        public MileageSource MileageSource
        {
            get
            {
                return _mileageSourceId == default(long) ? null : _mileageSource ?? (_mileageSource = new MileageSource(_mileageSourceId, false));
            }
            set
            {
                _mileageSource = value;
                _mileageSourceId = value == null ? default(long) : value.Id;
            }
        }
        public SalesRepresentative SalesRepresentative
        {
            get
            {
                return _salesRepresentativeId == default(long) ? null : _salesRepresentative ?? (_salesRepresentative = new SalesRepresentative(_salesRepresentativeId, false));
            }
            set
            {
                _salesRepresentative = value;
                _salesRepresentativeId = value == null ? default(long) : value.Id;
            }
        }
        public ResellerAddition ResellerAddition
        {
            get
            {
                return _resellerAdditionId == default(long) ? null : _resellerAddition ?? (_resellerAddition = new ResellerAddition(_resellerAdditionId, false));
            }
            set
            {
                _resellerAddition = value;
                _resellerAdditionId = value == null ? default(long) : value.Id;
            }
        }
        public AccountBucketUnit AccountBucketUnit
        {
            get
            {
                return _accountBucketUnitId == default(long) ? null : _accountBucketUnit ?? (_accountBucketUnit = new AccountBucketUnit(_accountBucketUnitId, false));
            }
            set
            {
                _accountBucketUnit = value;
                _accountBucketUnitId = value == null ? default(long) : value.Id;
            }
        }

		public ShipmentPriority ShipmentPriority
		{
			get { return _shipmentPriorityId == default(long) ? null : _shipmentPriority ?? (_shipmentPriority = new ShipmentPriority(_shipmentPriorityId, false)); }
			set
			{
				_shipmentPriority = value;
				_shipmentPriorityId = value == null ? default(long) : value.Id;
			}
		}
        public Job Job
        {
            get
            {
                return _jobId == default(long) ? null : _job ?? (_job = new Job(_jobId, false));
            }
            set
            {
                _job = value;
                _jobId = value == null ? default(long) : value.Id;
            }
        }

		public List<LoadOrderVendor> Vendors
		{
		    get
		    {
		        if (_vendors == null) LoadVendors();
		        return _vendors;
		    }
		    set { _vendors = value; }
		}
		public List<LoadOrderNote> Notes
		{
			get
			{
				if (_notes == null) LoadNotes();
				return _notes;
			}
			set { _notes = value; }
		}
		public List<LoadOrderItem> Items
		{
			get
			{
				if (_items == null) LoadItems();
				return _items;
			}
			set { _items = value; }
		}
		public List<LoadOrderCharge> Charges
		{
			get
			{
				if (_charges == null) LoadCharges();
				return _charges;
			}
			set { _charges = value; }
		}
		public List<LoadOrderLocation> Stops
		{
		    get
		    {
		        if (_stops == null) LoadStops();
		        return _stops;
		    }
		    set { _stops = value; }
		}
		public List<LoadOrderEquipment> Equipments
		{
			get
			{
				if (_equipments == null) LoadEquipments();
				return _equipments;
			}
			set { _equipments = value; }
		}
		public List<LoadOrderReference> CustomerReferences
		{
			get
			{
				if (_customerReferences == null) LoadCustomeReferences();
				return _customerReferences;
			}
			set { _customerReferences = value; }
		}
		public List<LoadOrderService> Services
		{
			get
			{
				if (_services == null) LoadServices();
				return _services;
			}
			set { _services = value; }
		}
		public List<LoadOrderDocument> Documents
		{
			get
			{
				if (_documents == null) LoadDocuments();
				return _documents;
			}
			set { _documents = value; }
		}
		public List<LoadOrderAccountBucket> AccountBuckets
		{
			get
			{
				if (_accountBuckets == null) LoadAccountBuckets();
				return _accountBuckets;
			}
			set { _accountBuckets = value; }
		}


        public bool IsNew
        {
            get { return Id == default(long); }
        }

        public LoadOrder() { }

        public LoadOrder(long id) : this(id, false) { }

        public LoadOrder(long id, bool takeSnapshot)
        {
            Id = id;
            if (Id == default(long)) return;
            KeyLoaded = Load();
            if (takeSnapshot) TakeSnapShot();
        }

        public LoadOrder(DbDataReader reader)
        {
            Load(reader);
        }

        public void Save()
        {
            if (Id == default(long)) Insert();
            else Update();
        }

        public new void Delete()
        {
            if (Id != default(long))
                base.Delete();
        }

        public LoadOrderHistoricalData FetchHistoricalData()
        {
            if (Id == default(long)) return null;

            LoadOrderHistoricalData data = null;

            const string query = @"SELECT * FROM LoadOrderHistoricalData WHERE LoadOrderId = @LoadOrderId";
            var parameters = new Dictionary<string, object> { { "LoadOrderId", Id } };

            using (var reader = GetReader(query, parameters))
                if (reader.Read())
                    data = new LoadOrderHistoricalData(reader);
            Connection.Close();
            return data;
        }

        public void LoadCollections()
        {
            LoadNotes();
            LoadItems();
            LoadCharges();
            LoadVendors();
            LoadStops();
            LoadEquipments();
            LoadCustomeReferences();
            LoadServices();
            LoadDocuments();
            LoadAccountBuckets();
        }

        private void LoadNotes()
        {
            _notes = new List<LoadOrderNote>();
            if (IsNew) return;
            const string query = @"SELECT * FROM LoadOrderNote WHERE LoadOrderId = @LoadOrderId AND TenantId = @TenantId;";
            var parameters = new Dictionary<string, object> { { "LoadOrderId", Id }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _notes.Add(new LoadOrderNote(reader));
            Connection.Close();
        }

        private void LoadItems()
        {
            _items = new List<LoadOrderItem>();
            if (IsNew) return;
            const string query = @"SELECT * FROM LoadOrderItem WHERE LoadOrderId = @LoadOrderId AND TenantId = @TenantId;";
            var parameters = new Dictionary<string, object> { { "LoadOrderId", Id }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _items.Add(new LoadOrderItem(reader));
            Connection.Close();
        }

        private void LoadCharges()
        {
			_charges = new List<LoadOrderCharge>();
			if (IsNew) return;
			const string query = @"SELECT * FROM LoadOrderCharge WHERE LoadOrderId = @LoadOrderId AND TenantId = @TenantId;";
			var parameters = new Dictionary<string, object> { { "LoadOrderId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
			    while (reader.Read())
			        _charges.Add(new LoadOrderCharge(reader));
			Connection.Close();
        }

        private void LoadVendors()
        {
			_vendors = new List<LoadOrderVendor>();
			if (IsNew) return;
			const string query = @"SELECT * FROM LoadOrderVendor WHERE LoadOrderId = @LoadOrderId AND TenantId = @TenantId;";
			var parameters = new Dictionary<string, object> { { "LoadOrderId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
			    while (reader.Read())
			        _vendors.Add(new LoadOrderVendor(reader));
			Connection.Close();
        }

        private void LoadStops()
        {
            _stops = new List<LoadOrderLocation>();
            if (IsNew) return;
            const string query = @"SELECT * FROM LoadOrderLocation WHERE LoadOrderId = @LoadOrderId AND TenantId = @TenantId
								AND [Id] NOT IN (@OriginId, @DestinationId)";
            var parameters = new Dictionary<string, object>
                                {
                                    {"LoadOrderId", Id},
                                    {"TenantId", TenantId},
                                    {"OriginId", OriginId},
                                    {"DestinationId", DestinationId}
                                };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _stops.Add(new LoadOrderLocation(reader));
            Connection.Close();
        }

        private void LoadEquipments()
        {
            _equipments = new List<LoadOrderEquipment>();
            if (IsNew) return;
            const string query = @"SELECT * FROM LoadOrderEquipment WHERE LoadOrderId = @LoadOrderId AND TenantId = @TenantId;";
            var parameters = new Dictionary<string, object> { { "LoadOrderId", Id }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _equipments.Add(new LoadOrderEquipment(reader));
            Connection.Close();
        }

        private void LoadCustomeReferences()
        {
            _customerReferences = new List<LoadOrderReference>();
            if (IsNew) return;
            const string query = @"SELECT * FROM LoadOrderReference WHERE LoadOrderId = @LoadOrderId AND TenantId = @TenantId;";
            var parameters = new Dictionary<string, object> { { "LoadOrderId", Id }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _customerReferences.Add(new LoadOrderReference(reader));
            Connection.Close();
        }

        private void LoadServices()
        {
            _services = new List<LoadOrderService>();
            if (IsNew) return;
            const string query = @"SELECT * FROM LoadOrderService WHERE LoadOrderId = @LoadOrderId AND TenantId = @TenantId;";
            var parameters = new Dictionary<string, object> { { "LoadOrderId", Id }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _services.Add(new LoadOrderService(reader));
            Connection.Close();
        }

        private void LoadDocuments()
        {
            _documents = new List<LoadOrderDocument>();
            if (IsNew) return;
            const string query = @"SELECT * FROM LoadOrderDocument WHERE LoadOrderId = @LoadOrderId AND TenantId = @TenantId;";
            var parameters = new Dictionary<string, object> { { "LoadOrderId", Id }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _documents.Add(new LoadOrderDocument(reader));
            Connection.Close();
        }

        private void LoadAccountBuckets()
        {
            _accountBuckets = new List<LoadOrderAccountBucket>();
            if (IsNew) return;
            const string query = @"SELECT * FROM LoadOrderAccountBucket WHERE LoadOrderId = @LoadOrderId AND TenantId = @TenantId;";
            var parameters = new Dictionary<string, object> { { "LoadOrderId", Id }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _accountBuckets.Add(new LoadOrderAccountBucket(reader));
            Connection.Close();
        }
    }
}
