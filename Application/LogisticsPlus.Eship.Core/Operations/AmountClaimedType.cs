﻿namespace LogisticsPlus.Eship.Core.Operations
{
	public enum AmountClaimedType
	{
        NotApplicable = 0,
        FullValue,
        Repair,
        Allowance
	}
}
