﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("Project44TrackingData", ReadOnly = false, Source = EntitySource.TableView)]
	public class Project44TrackingData : ObjectBase
    {
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

        [EnableSnapShot("Project44Id", Description = "Project 44 Id")]
        [Property("Project44Id", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public string Project44Id { get; set; }

        [EnableSnapShot("ResponseData", Description = "Response Data")]
        [Property("ResponseData", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public string ResponseData { get; set; }

        [EnableSnapShot("DateCreated", Description = "Date Created")]
        [Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime DateCreated { get; set; }
        
        public bool IsNew
		{
			get { return Id == default(long); }
		}

        public Project44TrackingData()
        {
        }

		public Project44TrackingData(DbDataReader reader)
		{
			Load(reader);
		}
        
		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long)) base.Delete();
		}

		public new void TakeSnapShot()
		{
			GetSnapShot();
		}
	}
}
