﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("VendorTerminal", ReadOnly = false, Source = EntitySource.TableView)]
	public class VendorTerminal : Location
	{
		private long _vendorId;

		private Vendor _vendor;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("Name", Description = "Terminal Name")]
		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

		[EnableSnapShot("DateCreated", Description = "Date Created")]
		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[EnableSnapShot("ContactName", Description = "Contact Name")]
		[Property("ContactName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ContactName { get; set; }

		[EnableSnapShot("Comment", Description = "Comment")]
		[Property("Comment", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Comment { get; set; }

		[EnableSnapShot("Phone")]
		[Property("Phone", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Phone { get; set; }

		[EnableSnapShot("Mobile")]
		[Property("Mobile", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Mobile { get; set; }

		[EnableSnapShot("Fax")]
		[Property("Fax", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Fax { get; set; }

		[EnableSnapShot("Email")]
		[Property("Email", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Email { get; set; }

		[EnableSnapShot("VendorId", Description = "Vendor Reference")]
		[Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long VendorId
		{
			get
			{
				if (_vendor != null) _vendorId = _vendor.Id;
				return _vendorId;
			}
			set
			{
				_vendorId = value;
				if (_vendor != null && value != _vendor.Id) _vendor = null;
			}
		}

		public Vendor Vendor
		{
            get { return _vendorId == default(long) ? null : _vendor ?? (_vendor = new Vendor(_vendorId)); }
			set
			{
				_vendor = value;
				_vendorId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew { get { return Id == default(long); } }

		public VendorTerminal()
		{

		}

		public VendorTerminal(long id) : this(id, false)
		{
		}

		public VendorTerminal(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public VendorTerminal(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
