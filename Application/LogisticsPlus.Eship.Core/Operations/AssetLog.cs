﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
    [Entity("AssetLog", ReadOnly = false, Source = EntitySource.TableView)]
    public class AssetLog : Location
    {
        private long _assetId;
        private long _shipmentId;

        private Asset _asset;
        private Shipment _shipment;

        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; protected set; }

        [EnableSnapShot("DateCreated", Description = "Date Created")]
        [Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime DateCreated { get; set; }

        [EnableSnapShot("Comment", Description = "Comment")]
        [Property("Comment", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Comment { get; set; }

        [EnableSnapShot("MileageEngine", Description = "Mileage Engine Reference")]
        [Property("MileageEngine", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public MileageEngine MileageEngine { get; set; }

		[EnableSnapShot("MilesRun", Description = "Miles Run")]
		[Property("MilesRun", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal MilesRun { get; set; }

        [EnableSnapShot("LogDate", Description = "Log Date Reference")]
        [Property("LogDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime LogDate { get; set; }

        [EnableSnapShot("ShipmentId", Description = "Shipment Reference")]
        [Property("ShipmentId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long ShipmentId
        {
            get { return _shipmentId; }
            set
            {
                _shipmentId = value;
                if (_shipment != null && value != _shipment.Id) _shipment = null;
            }
        }

        [EnableSnapShot("AssetId", Description = "Asset Reference")]
        [Property("AssetId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long AssetId
        {
            get { return _assetId; }
            set
            {
                _assetId = value;
                if (_asset != null && value != _asset.Id) _asset = null;
            }
        }


        public Shipment Shipment
        {
            get { return _shipmentId == default(long) ? null : _shipment ?? (_shipment = new Shipment(_shipmentId, false)); }
            set
            {
                _shipment = value;
                _shipmentId = value == null ? default(long) : value.Id;
            }
        }

        public Asset Asset
        {
            get { return _assetId == default(long) ? null : _asset ?? (_asset = new Asset(_assetId, false)); }
            set
            {
                _asset = value;
                _assetId = value == null ? default(long) : value.Id;
            }
        }

        public bool IsNew
        {
            get { return Id == default(long); }
        }

        public AssetLog() { }

        public AssetLog(long id) : this(id, false) { }

        public AssetLog(long id, bool takeSnapshot)
        {
            Id = id;
            if (Id == default(long)) return;
            KeyLoaded = Load();
            if (takeSnapshot) TakeSnapShot();
        }

        public AssetLog(DbDataReader reader)
        {
            Load(reader);
        }


        public void Save()
        {
            if (Id == default(long)) Insert();
            else Update();
        }

        public new void Delete()
        {
            if (Id != default(long)) base.Delete();
        }
    }
}
