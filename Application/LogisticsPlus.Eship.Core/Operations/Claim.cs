﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("Claim", ReadOnly = false, Source = EntitySource.TableView)]
	public class Claim : TenantBase
	{
		private long _customerId;
		private long _userId;

		private User _user;
		private Customer _customer;

		private List<ClaimShipment> _shipments;
		private List<ClaimServiceTicket> _serviceTickets;
		private List<ClaimNote> _notes;
		private List<ClaimVendor> _vendors;
		private List<ClaimDocument> _documents;
			
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("ClaimNumber", Description = "Claim Number")]
		[Property("ClaimNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ClaimNumber { get; set; }

		[EnableSnapShot("DateCreated", Description = "Date Created")]
		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[EnableSnapShot("ClaimDate", Description = "Claim Date")]
		[Property("ClaimDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime ClaimDate { get; set; }

		[EnableSnapShot("Status", Description = "Claim Status")]
		[Property("Status", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ClaimStatus Status { get; set; }

		[EnableSnapShot("ClaimDetail", Description = "Claim Detail")]
		[Property("ClaimDetail", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ClaimDetail { get; set; }

		[EnableSnapShot("UserId", Description = "User Reference")]
		[Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long UserId
		{
			get { return _userId; }
			set
			{
				_userId = value;
				if (_user != null && value != _user.Id) _user = null;
			}
		}

		[EnableSnapShot("CustomerId", Description = "Customer Reference")]
		[Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long CustomerId
		{
			get { return _customerId; }
			set
			{
				_customerId = value;
				if (_customer != null && value != _customer.Id) _customer = null;
			}
		}

        [EnableSnapShot("ClaimType", Description = "Claim Type")]
        [Property("ClaimType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public ClaimType ClaimType { get; set; }

        [EnableSnapShot("IsRepairable", Description = "Is Repairable")]
        [Property("IsRepairable", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool IsRepairable { get; set; }

        [EnableSnapShot("EstimatedRepairCost", Description = "Estimated Repair Cost")]
        [Property("EstimatedRepairCost", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal EstimatedRepairCost { get; set; }

        [EnableSnapShot("AmountClaimed", Description = "Amount Claimed")]
        [Property("AmountClaimed", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal AmountClaimed { get; set; }

        [EnableSnapShot("AmountClaimedType", Description = "Amount Claimed Type")]
        [Property("AmountClaimedType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public AmountClaimedType AmountClaimedType { get; set; }

        [EnableSnapShot("ClaimantReferenceNumber", Description = "Claimant Reference Number")]
        [Property("ClaimantReferenceNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ClaimantReferenceNumber { get; set; }

        [EnableSnapShot("DateLpAcctToPayCarrier", Description = "Date Lp Account To Pay Carrier")]
        [Property("DateLpAcctToPayCarrier", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime DateLpAcctToPayCarrier { get; set; }

        [EnableSnapShot("Acknowledged", Description = "Acknowledged Date")]
        [Property("Acknowledged", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime Acknowledged { get; set; }

        [EnableSnapShot("CheckNumber", Description = "Check Number")]
        [Property("CheckNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string CheckNumber { get; set; }

        [EnableSnapShot("CheckAmount", Description = "Check Amount")]
        [Property("CheckAmount", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal CheckAmount { get; set; }

        public Customer Customer
		{
			get { return _customerId == default(long) ? null : _customer ?? (_customer = new Customer(_customerId, false)); }
			set
			{
				_customer = value;
				_customerId = value == null ? default(long) : value.Id;
			}
		}
		public User User
		{
			get { return _userId == default(long) ? null : _user ?? (_user = new User(_userId, false)); }
			set
			{
				_user = value;
				_userId = value == null ? default(long) : value.Id;
			}
		}

		public List<ClaimNote> Notes
		{
			get
			{
				if (_notes == null) LoadNotes();
				return _notes;
			}
			set { _notes = value; }
		}
		public List<ClaimShipment> Shipments
		{
			get
			{
				if (_shipments == null) LoadShipments();
				return _shipments;
			}
			set { _shipments = value; }
		}
		public List<ClaimServiceTicket> ServiceTickets
		{
			get
			{
				if (_serviceTickets == null) LoadServiceTickets();
				return _serviceTickets;
			}
			set { _serviceTickets = value; }
		}
		public List<ClaimVendor> Vendors
		{
			get
			{
				if (_vendors == null) LoadVendors();
				return _vendors;
			}
			set { _vendors = value; }
		}
		public List<ClaimDocument> Documents
		{
			get
			{
				if (_documents == null) LoadDocuments();
				return _documents;
			}
			set { _documents = value; }
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public Claim() { }

		public Claim(long id) : this(id, false) { }

		public Claim(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public Claim(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

		public void LoadCollections()
		{
			LoadNotes();
			LoadShipments();
			LoadServiceTickets();
			LoadVendors();
			LoadDocuments();
		}

		private void LoadNotes()
		{
			_notes = new List<ClaimNote>();
			const string query = @"SELECT * FROM ClaimNote WHERE ClaimId = @ClaimId AND TenantId = @TenantId;";
			var parameters = new Dictionary<string, object> { { "ClaimId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_notes.Add(new ClaimNote(reader));
			Connection.Close();
		}

		private void LoadShipments()
		{
			_shipments = new List<ClaimShipment>();
			const string query = @"SELECT * FROM ClaimShipment WHERE ClaimId = @ClaimId AND TenantId = @TenantId;";
			var parameters = new Dictionary<string, object> { { "ClaimId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_shipments.Add(new ClaimShipment(reader));
			Connection.Close();
		}

		private void LoadServiceTickets()
		{
			_serviceTickets = new List<ClaimServiceTicket>();
			const string query = @"SELECT * FROM ClaimServiceTicket WHERE ClaimId = @ClaimId AND TenantId = @TenantId;";
			var parameters = new Dictionary<string, object> { { "ClaimId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_serviceTickets.Add(new ClaimServiceTicket(reader));
			Connection.Close();
		}

		private void LoadVendors()
		{
			_vendors = new List<ClaimVendor>();
			const string query = @"SELECT * FROM ClaimVendor WHERE ClaimId = @ClaimId AND TenantId = @TenantId;";
			var parameters = new Dictionary<string, object> { { "ClaimId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_vendors.Add(new ClaimVendor(reader));
			Connection.Close();
		}

		private void LoadDocuments()
		{
			_documents = new List<ClaimDocument>();
			const string query = @"SELECT * FROM ClaimDocument WHERE ClaimId = @ClaimId AND TenantId = @TenantId;";
			var parameters = new Dictionary<string, object> { { "ClaimId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_documents.Add(new ClaimDocument(reader));
			Connection.Close();
		}
	}
}
