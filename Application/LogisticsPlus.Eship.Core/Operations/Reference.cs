﻿using System;
using System.Data;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	public class Reference : TenantBase
	{
		[EnableSnapShot("Name")]
		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

		[EnableSnapShot("Value")]
		[Property("Value", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Value { get; set; }

		[EnableSnapShot("DisplayOnOrigin", Description = "Applies to Origin")]
		[Property("DisplayOnOrigin", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool DisplayOnOrigin { get; set; }

		[EnableSnapShot("DisplayOnDestination", Description = "Applies to Destination")]
		[Property("DisplayOnDestination", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool DisplayOnDestination { get; set; }

		[EnableSnapShot("Required")]
		[Property("Required", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Required { get; set; }

	}
}
