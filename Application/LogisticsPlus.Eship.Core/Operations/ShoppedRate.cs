﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
    [Entity("ShoppedRate", ReadOnly = false, Source = EntitySource.TableView)]
    public class ShoppedRate : TenantBase
    {
        private long _loadOrderId;
        private long _shipmentId;
        private long _customerId;
        private long _userId;
        private long _originCountryId;
        private long _destinationCountryId;

       
        private LoadOrder _loadOrder;
        private Shipment _shipment;
        private Customer _customer;
        private User _user;
        private Country _originCountry;
        private Country _destinationCountry;

        private List<ShoppedRateService> _services;

        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; private set; }

		[EnableSnapShot("LoadOrderId", Description = "Load Order Reference")]
		[Property("LoadOrderId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long LoadOrderId
		{
			get
			{
				return _loadOrderId;
			}
			set
			{
				_loadOrderId = value;
				if (_loadOrder != null && value != _loadOrder.Id) _loadOrder = null;
			}
		}

        [EnableSnapShot("ShipmentId", Description = "Shipment Reference")]
        [Property("ShipmentId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long ShipmentId
        {
            get
            {
                return _shipmentId;
            }
            set
            {
                _shipmentId = value;
                if (_shipment != null && value != _shipment.Id) _shipment = null;
            }
        }

        [EnableSnapShot("ReferenceNumber", Description = "Reference Number")]
        [Property("ReferenceNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ReferenceNumber { get; set; }

        [EnableSnapShot("Type", Description = "Shopped Rate Type")]
        [Property("Type", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public ShoppedRateType Type { get; set; }

        [EnableSnapShot("NumberLineItems", Description = "Number of Line Items")]
        [Property("NumberLineItems", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int NumberLineItems { get; set; }

        [EnableSnapShot("Quantity", Description = "Quantity")]
        [Property("Quantity", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int Quantity { get; set; }

        [EnableSnapShot("Weight", Description = "Weight")]
        [Property("Weight", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal Weight { get; set; }

        [EnableSnapShot("HighestFreightClass", Description = "Highest Freight Class")]
        [Property("HighestFreightClass", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
        public double HighestFreightClass { get; set; }

        [EnableSnapShot("MostSignificantFreightClass", Description = "Most Significant Freight Class")]
        [Property("MostSignificantFreightClass", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
        public double MostSignificantFreightClass { get; set; }

        [EnableSnapShot("LowestTotalCharge", Description = "Lowest Total Charge")]
        [Property("LowestTotalCharge", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal LowestTotalCharge { get; set; }

        [EnableSnapShot("DateCreated", Description = "Date Created")]
		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

        [EnableSnapShot("UserId", Description = "User Reference")]
        [Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        protected long UserId
        {
            get { return _userId; }
            set
            {
                _userId = value;
                if (_user != null && value != _user.Id) _user = null;
            }
        }

        [EnableSnapShot("CustomerId", Description = "Customer Reference")]
        [Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        protected long CustomerId
        {
            get { return _customerId; }
            set
            {
                _customerId = value;
                if (_customer != null && value != _customer.Id) _customer = null;
            }
        }

        [EnableSnapShot("OriginCountryId", Description = "Country Reference")]
        [Property("OriginCountryId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long OriginCountryId
        {
            get { return _originCountryId; }
            set
            {
                _originCountryId = value;
                if (_originCountry != null && value != _originCountry.Id) _originCountry = null;
            }
        }

        [EnableSnapShot("DestinationCountryId", Description = "Country Reference")]
        [Property("DestinationCountryId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long DestinationCountryId
        {
            get { return _destinationCountryId; }
            set
            {
                _destinationCountryId = value;
                if (_destinationCountry != null && value != _destinationCountry.Id) _destinationCountry = null;
            }
        }

        [EnableSnapShot("OriginCity", Description = "Origin City")]
        [Property("OriginCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginCity { get; set; }

        [EnableSnapShot("OriginState", Description = "Origin State")]
        [Property("OriginState", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginState { get; set; }

        [EnableSnapShot("OriginPostalCode", Description = "Origin Postal Code")]
        [Property("OriginPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginPostalCode { get; set; }

        [EnableSnapShot("DestinationCity", Description = "Destination City")]
        [Property("DestinationCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationCity { get; set; }

        [EnableSnapShot("DestinationState", Description = "Destination State")]
        [Property("DestinationState", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationState { get; set; }

        [EnableSnapShot("DestinationPostalCode", Description = "Destination Postal Code")]
        [Property("DestinationPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationPostalCode { get; set; }

		[EnableSnapShot("VendorNumber", Description = "Vendor Number")]
		[Property("VendorNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string VendorNumber { get; set; }

		[EnableSnapShot("VendorName", Description = "Vendor Name")]
		[Property("VendorName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string VendorName { get; set; }

        public Country OriginCountry
        {
            get { return _originCountryId == default(long) ? null : _originCountry ?? (_originCountry = new Country(_originCountryId)); }
            set
            {
                _originCountry = value;
                _originCountryId = value == null ? default(long) : value.Id;
            }
        }
        public Country DestinationCountry
        {
            get { return _destinationCountryId == default(long) ? null : _destinationCountry ?? (_destinationCountry = new Country(_destinationCountryId)); }
            set
            {
                _destinationCountry = value;
                _destinationCountryId = value == null ? default(long) : value.Id;
            }
        }
        public Customer Customer
        {
            get { return _customerId == default(long) ? null : _customer ?? (_customer = new Customer(_customerId, false)); }
            set
            {
                _customer = value;
                _customerId = value == null ? default(long) : value.Id;
            }
        }
    	public Shipment Shipment
    	{
			get { return _shipmentId == default(long) ? null : _shipment ?? (_shipment = new Shipment(_shipmentId)); }
			set
			{
				_shipment = value;
				_shipmentId = value == null ? default(long) : value.Id;
			}
    	}
		public LoadOrder LoadOrder
		{
			get { return _loadOrderId == default(long) ? null : _loadOrder ?? (_loadOrder = new LoadOrder(_loadOrderId)); }
			set
			{
				_loadOrder = value;
				_loadOrderId = value == null ? default(long) : value.Id;
			}
		}
    	public User User
        {
            get { return _userId == default(long) ? null : _user ?? (_user = new User(_userId, false)); }
            set
            {
                _user = value;
                _userId = value == null ? default(long) : value.Id;
            }
        }

        public List<ShoppedRateService> Services
        {
            get
            {
                if (_services == null) LoadServices();
                return _services;
            }
            set { _services = value; }
        }

        public bool IsNew
        {
            get { return Id == default(long); }
        }

        public ShoppedRate() { }

        public ShoppedRate(long id) : this(id, false) { }

        public ShoppedRate(long id, bool takeSnapshot)
        {
            Id = id;
            if (Id == default(long)) return;
            KeyLoaded = Load();
            if (takeSnapshot) TakeSnapShot();
        }

        public ShoppedRate(DbDataReader reader)
        {
            Load(reader);
        }

        public void Save()
        {
            if (Id == default(long)) Insert();
            else Update();
        }

        public new void Delete()
        {
            if (Id != default(long))
                base.Delete();
        }

		public void LoadCollection()
		{
			LoadServices();
		}

        private void LoadServices()
        {
            _services = new List<ShoppedRateService>();
            const string query = @"SELECT * FROM ShoppedRateService WHERE ShoppedRateId = @ShoppedRateId AND TenantId = @TenantId";
            var parameters = new Dictionary<string, object> { { "ShoppedRateId", Id }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _services.Add(new ShoppedRateService(reader));
            Connection.Close();
        }
    }
}
