﻿namespace LogisticsPlus.Eship.Core.Operations
{
	public enum ShipmentStatus
	{
        // There are database queries that rely on this order there ADD TO, DO NOT RE-ORDER. SEE APPLICABLE JOINS ON DATABASE REFSHIPMENTSTATUS
		Scheduled = 0,
		InTransit,
		Delivered,
		Invoiced,
		Void
	}
}
