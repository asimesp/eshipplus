﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("LoadOrderLocation", ReadOnly = false, Source = EntitySource.TableView)]
	public class LoadOrderLocation : Location
	{
		private long _loadOrderId;

		private LoadOrder _loadOrder;

		private List<LoadOrderContact> _contacts;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; internal set; }

		[EnableSnapShot("Description")]
		[Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Description { get; set; }

		[EnableSnapShot("SpecialInstructions", Description = "Special Instructions")]
		[Property("SpecialInstructions", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string SpecialInstructions { get; set; }

		[EnableSnapShot("GeneralInfo", Description = "General Info")]
		[Property("GeneralInfo", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string GeneralInfo { get; set; }

		[EnableSnapShot("Direction")]
		[Property("Direction", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Direction { get; set; }

		[EnableSnapShot("StopOrder", Description = "Location Stop Index")]
		[Property("StopOrder", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int StopOrder { get; set; }

		[EnableSnapShot("AppointmentDateTime", Description = "Appointment Date Time")]
		[Property("AppointmentDateTime", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime AppointmentDateTime { get; set; }

		[EnableSnapShot("MilesFromPreceedingStop", Description = "Miles From Preceeding Stop")]
		[Property("MilesFromPreceedingStop", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal MilesFromPreceedingStop { get; set; }

		[EnableSnapShot("LoadOrderId", Description = "Load Order Reference")]
		[Property("LoadOrderId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long LoadOrderId
		{
			get
			{
				if (_loadOrder != null) _loadOrderId = _loadOrder.Id;
				return _loadOrderId;
			}
			set
			{
				_loadOrderId = value;
				if (_loadOrder != null && value != _loadOrder.Id) _loadOrder = null;
			}
		}

		public LoadOrder LoadOrder
		{
			get { return _loadOrder ?? (_loadOrder = new LoadOrder(_loadOrderId)); }
			set
			{
				_loadOrder = value;
				_loadOrderId = value == null ? default(long) : value.Id;
			}
		}

		public List<LoadOrderContact> Contacts
		{
			get
			{
				if (_contacts == null) LoadContacts();
				return _contacts;
			}
			set { _contacts = value; }
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public LoadOrderLocation() { }

		public LoadOrderLocation(long id) : this(id, false) { }

		public LoadOrderLocation(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public LoadOrderLocation(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

		public void LoadCollections()
		{
			LoadContacts();
		}

		private void LoadContacts()
		{
            _contacts = new List<LoadOrderContact>();
            if (IsNew) return;
            const string query = @"SELECT * FROM LoadOrderContact where LoadOrderLocationId = @LoadOrderLocationId and TenantId = @TenantId;";
            var parameters = new Dictionary<string, object> { { "LoadOrderLocationId", Id }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _contacts.Add(new LoadOrderContact(reader));
            Connection.Close();
		}
	}
}
