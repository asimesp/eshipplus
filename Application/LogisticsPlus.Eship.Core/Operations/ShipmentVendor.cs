﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;
using System.Collections.Generic;

namespace LogisticsPlus.Eship.Core.Operations
{
    [Serializable]
    [Entity("ShipmentVendor", ReadOnly = false, Source = EntitySource.TableView)]
    public class ShipmentVendor : TenantBase
    {
        private long _vendorId;
        private long _failureCodeId;
        private long _shipmentId;
        private bool _isRelationBetweenVendorAndVendorBill;

        private Shipment _shipment;
        private FailureCode _failureCode;
        private Vendor _vendor;

        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; private set; }

        [EnableSnapShot("Primary")]
        [Property("Primary", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool Primary { get; set; }

        [EnableSnapShot("FailureComments", Description = "Failure Comments")]
        [Property("FailureComments", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string FailureComments { get; set; }

        [EnableSnapShot("ProNumber", Description = "PRO Number")]
        [Property("ProNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ProNumber { get; set; }

        [EnableSnapShot("FailureCodeId", Description = "Failure Code Reference")]
        [Property("FailureCodeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long FailureCodeId
        {
            get
            {
                if (_failureCode != null) _failureCodeId = _failureCode.Id;
                return _failureCodeId;
            }
            set
            {
                _failureCodeId = value;
                if (_failureCode != null && value != _failureCode.Id) _failureCode = null;
            }
        }

        [EnableSnapShot("VendorId", Description = "Vendor Reference")]
        [Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long VendorId
        {
            get
            {
                if (_vendor != null) _vendorId = _vendor.Id;
                return _vendorId;
            }
            set
            {
                _vendorId = value;
                if (_vendor != null && value != _vendor.Id) _vendor = null;
            }
        }

        [EnableSnapShot("ShipmentId", Description = "Shipment Reference")]
        [Property("ShipmentId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        protected long ShipmentId
        {
            get
            {
                if (_shipment != null) _shipmentId = _shipment.Id;
                return _shipmentId;
            }
            set
            {
                _shipmentId = value;
                if (_shipment != null && value != _shipment.Id) _shipment = null;
            }
        }

        public Shipment Shipment
        {
            get { return _shipment ?? (_shipment = new Shipment(_shipmentId)); }
            set
            {
                _shipment = value;
                _shipmentId = value == null ? default(long) : value.Id;
            }
        }
        public Vendor Vendor
        {
            get { return _vendor ?? (_vendor = new Vendor(_vendorId)); }
            set
            {
                _vendor = value;
                _vendorId = value == null ? default(long) : value.Id;
            }
        }
        public FailureCode FailureCode
        {
            get { return _failureCodeId == default(long) ? null : _failureCode ?? (_failureCode = new FailureCode(_failureCodeId)); }
            set
            {
                _failureCode = value;
                _failureCodeId = value == null ? default(long) : value.Id;
            }
        }

        public bool IsRelationBetweenVendorAndVendorBill
        {
            get
            {
                if (Id != default(long)) CheckRelationBetweenVendorAndVendorBill();
                return _isRelationBetweenVendorAndVendorBill;
            }
        }

        public bool IsNew
        {
            get { return Id == default(long); }
        }

        public ShipmentVendor() { }

        public ShipmentVendor(long id) : this(id, false) { }

        public ShipmentVendor(long id, bool takeSnapshot)
        {
            Id = id;
            if (Id == default(long)) return;
            KeyLoaded = Load();
            if (takeSnapshot) TakeSnapShot();
        }

        public ShipmentVendor(DbDataReader reader)
        {
            Load(reader);
        }

        public void Save()
        {
            if (Id == default(long)) Insert();
            else Update();
        }

        public new void Delete()
        {
            if (Id != default(long))
                base.Delete();
        }

        public void CheckRelationBetweenVendorAndVendorBill()
        {
            _isRelationBetweenVendorAndVendorBill = false;

            const string query = @"SELECT sc.* from ShipmentCharge sc, ShipmentVendor sv WHERE sv.Id = @Id AND sc.ShipmentId = @ShipmentId AND sc.VendorId = sv.VendorId AND sc.TenantId = @TenantId";
            var parameters = new Dictionary<string, object> { { "Id", Id }, { "TenantId", TenantId }, { "ShipmentId", ShipmentId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _isRelationBetweenVendorAndVendorBill = (new ShipmentCharge(reader)).VendorBillId > 0;

            Connection.Close();
        }
    }
}