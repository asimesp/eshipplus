﻿using System;
using System.Data;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("SMC3RequestResponse", ReadOnly = false, Source = EntitySource.TableView)]
	public class SMC3RequestResponse : ObjectBase
    {
        private long _shipmentId;
        private Shipment _shipment;
        
        [EnableSnapShot("ShipmentId", Description = "ShipmentId")]
        [Property("ShipmentId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long ShipmentId
        {
            get
            {
                if (_shipment != null) _shipmentId = _shipment.Id;
                return _shipmentId;
            }
            set
            {
                _shipmentId = value;
                if (_shipment != null && value != _shipment.Id) _shipment = null;
            }
        }

        [EnableSnapShot("RequestData", Description = "RequestData")]
        [Property("RequestData", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string RequestData { get; set; }

        [EnableSnapShot("TransactionId", Description = "TransactionId")]
        [Property("TransactionId", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string TransactionId { get; set; }

        [EnableSnapShot("ResponseData", Description = "ResponseData")]
        [Property("ResponseData", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ResponseData { get; set; }

        [EnableSnapShot("DateCreated", Description = "Date Created")]
        [Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime DateCreated { get; set; }

        [EnableSnapShot("Scac", Description = "Scac")]
        [Property("Scac", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Scac { get; set; }

        public Shipment Shipment
        {
            get { return _shipmentId == default(long) ? null : _shipment ?? (_shipment = new Shipment(_shipmentId)); }
            set
            {
                _shipment = value;
                _shipmentId = value == null ? default(long) : value.Id;
            }
        }
	}
}
