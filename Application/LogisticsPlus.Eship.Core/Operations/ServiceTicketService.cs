﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("ServiceTicketService", ReadOnly = false, Source = EntitySource.TableView)]
	public class ServiceTicketService : TenantBase
	{
		private long _serviceTicketId;
		private long _serviceId;

		private Service _service;
		private ServiceTicket _serviceTicket;

		[EnableSnapShot("ServiceId", Description = "Service Reference")]
		[Property("ServiceId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ServiceId
		{
			get
			{
				if (_service != null) _serviceId = _service.Id;
				return _serviceId;
			}
			set
			{
				_serviceId = value;
				if (_service != null && value != _service.Id) _service = null;
			}
		}

		[EnableSnapShot("ServiceTicketId", Description = "Service Ticket Reference")]
		[Property("ServiceTicketId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ServiceTicketId
		{
			get
			{
				if (_serviceTicket != null) _serviceTicketId = _serviceTicket.Id;
				return _serviceTicketId;
			}
			set
			{
				_serviceTicketId = value;
				if (_serviceTicket != null && value != _serviceTicket.Id) _serviceTicket = null;
			}
		}

		public ServiceTicket ServiceTicket
		{
			get { return _serviceTicket ?? (_serviceTicket = new ServiceTicket(_serviceTicketId)); }
			set
			{
				_serviceTicket = value;
				_serviceTicketId = value == null ? default(long) : value.Id;
			}
		}
		public Service Service
		{
			get { return _service ?? (_service = new Service(_serviceId)); }
			set
			{
				_service = value;
				_serviceId = value == null ? default(long) : value.Id;
			}
		}

		public ServiceTicketService()
		{
		}

		public ServiceTicketService(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			Insert();
		}

		public new void Delete()
		{
			base.Delete();
		}
	}
}
