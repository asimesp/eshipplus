﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("Project44DispatchResponse", ReadOnly = false, Source = EntitySource.TableView)]
	public class Project44DispatchResponse : Project44Response
    {
        [EnableSnapShot("Id", Description = "Id")]
        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; set; }

        public bool IsNew
        {
            get { return Id == default(long); }
        }


        public Project44DispatchResponse()
        {
        }


        public Project44DispatchResponse(DbDataReader reader)
        {
            Load(reader);
        }


        public void Save()
        {
            if (Id == default(long)) Insert();
            else Update();
        }

        public new void Delete()
        {
            if (Id != default(long)) base.Delete();
        }
    }
}
