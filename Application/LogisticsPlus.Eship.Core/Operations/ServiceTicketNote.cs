﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("ServiceTicketNote", ReadOnly = false, Source = EntitySource.TableView)]
	public class ServiceTicketNote : Note
	{
		private long _serviceTicketId;

		private ServiceTicket _serviceTicket;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("ServiceTicketId", Description = "Service Ticket Reference")]
		[Property("ServiceTicketId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long ServiceTicketId
		{
			get
			{
				if (_serviceTicket != null) _serviceTicketId = _serviceTicket.Id;
				return _serviceTicketId;
			}
			set
			{
				_serviceTicketId = value;
				if (_serviceTicket != null && value != _serviceTicket.Id) _serviceTicket = null;
			}
		}

		public ServiceTicket ServiceTicket
		{
			get { return _serviceTicket ?? (_serviceTicket = new ServiceTicket(_serviceTicketId)); }
			set
			{
				_serviceTicket = value;
				_serviceTicketId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public ServiceTicketNote() { }

		public ServiceTicketNote(long id) : this(id, false) { }

		public ServiceTicketNote(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public ServiceTicketNote(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
