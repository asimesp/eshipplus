﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("ClaimDocument", ReadOnly = false, Source = EntitySource.TableView)]
	public class ClaimDocument : Document
	{
		private long _claimId;

		private Claim _claim;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[Property("ClaimId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long ClaimId
		{
			get
			{
				if (_claim != null) _claimId = _claim.Id;
				return _claimId;
			}
			set
			{
				_claimId = value;
				if (_claim != null && value != _claim.Id) _claim = null;
			}
		}

		public Claim Claim
		{
			get { return _claim ?? (_claim = new Claim(_claimId)); }
			set
			{
				_claim = value;
				_claimId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public ClaimDocument() { }

		public ClaimDocument(long id) : this(id, false) { }

		public ClaimDocument(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public ClaimDocument(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
