﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("LoadOrderEquipment", ReadOnly = false, Source = EntitySource.TableView)]
	public class LoadOrderEquipment : TenantBase
	{
		private long _loadOrderId;
		private long _equipmentTypeId;

		private EquipmentType _equipmentType;
		private LoadOrder _loadOrder;

		[EnableSnapShot("EquipmentTypeId", Description = "Equipment Type Reference")]
		[Property("EquipmentTypeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long EquipmentTypeId
		{
			get
			{
				if (_equipmentType != null) _equipmentTypeId = _equipmentType.Id;
				return _equipmentTypeId;
			}
			set
			{
				_equipmentTypeId = value;
				if (_equipmentType != null && value != _equipmentType.Id) _equipmentType = null;
			}
		}

		[EnableSnapShot("LoadOrderId", Description = "Load Order Reference")]
		[Property("LoadOrderId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long LoadOrderId
		{
			get
			{
				if (_loadOrder != null) _loadOrderId = _loadOrder.Id;
				return _loadOrderId;
			}
			set
			{
				_loadOrderId = value;
				if (_loadOrder != null && value != _loadOrder.Id) _loadOrder = null;
			}
		}

		public LoadOrder LoadOrder
		{
			get { return _loadOrder ?? (_loadOrder = new LoadOrder(_loadOrderId)); }
			set
			{
				_loadOrder = value;
				_loadOrderId = value == null ? default(long) : value.Id;
			}
		}
		public EquipmentType EquipmentType
		{
			get { return _equipmentType ?? (_equipmentType = new EquipmentType(_equipmentTypeId)); }
			set
			{
				_equipmentType = value;
				_equipmentTypeId = value == null ? default(long) : value.Id;
			}
		}

		public LoadOrderEquipment()
		{
		}

		public LoadOrderEquipment(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			Insert();
		}

		public new void Delete()
		{
			base.Delete();
		}
	}
}
