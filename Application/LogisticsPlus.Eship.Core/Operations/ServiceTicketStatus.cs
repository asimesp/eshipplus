﻿namespace LogisticsPlus.Eship.Core.Operations
{
	public enum ServiceTicketStatus
	{
        // DO NOT RE-ORDER. SEE APPLICABLE JOINS ON DATABASE REFSERVICETICKETSTATUS
        Open = 0,
		Invoiced,
		Closed
	}
}
