﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("ShipmentService", ReadOnly = false, Source = EntitySource.TableView)]
	public class ShipmentService : TenantBase
	{
		private long _serviceId;
		private long _shipmentId;

		private Shipment _shipment;
		private Service _service;

		[EnableSnapShot("ServiceId", Description = "Service Reference")]
		[Property("ServiceId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long ServiceId
		{
			get
			{
				if (_service != null) _serviceId = _service.Id;
				return _serviceId;
			}
			set
			{
				_serviceId = value;
				if (_service != null && value != _service.Id) _service = null;
			}
		}

		[EnableSnapShot("ShipmentId", Description = "Shipment Reference")]
		[Property("ShipmentId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long ShipmentId
		{
			get
			{
				if (_shipment != null) _shipmentId = _shipment.Id;
				return _shipmentId;
			}
			set
			{
				_shipmentId = value;
				if (_shipment != null && value != _shipment.Id) _shipment = null;
			}
		}

		public Shipment Shipment
		{
			get { return _shipment ?? (_shipment = new Shipment(_shipmentId)); }
			set
			{
				_shipment = value;
				_shipmentId = value == null ? default(long) : value.Id;
			}
		}
		public Service Service
		{
			get { return _service ?? (_service = new Service(_serviceId)); }
			set
			{
				_service = value;
				_serviceId = value == null ? default(long) : value.Id;
			}
		}

		public ShipmentService()
		{
		}

		public ShipmentService(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			Insert();
		}

		public new void Delete()
		{
			base.Delete();
		}
	}
}
