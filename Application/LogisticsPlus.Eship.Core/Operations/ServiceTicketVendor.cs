﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using ObjToSql.Core.Attributes;
using System.Collections.Generic;

namespace LogisticsPlus.Eship.Core.Operations
{
    [Serializable]
    [Entity("ServiceTicketVendor", ReadOnly = false, Source = EntitySource.TableView)]
    public class ServiceTicketVendor : TenantBase
    {
        private long _vendorId;
        private long _serviceTicketId;
        private bool _isRelationBetweenVendorAndVendorBill;

        private ServiceTicket _serviceTicket;
        private Vendor _vendor;

        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; private set; }

        [EnableSnapShot("Primary")]
        [Property("Primary", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool Primary { get; set; }

        [EnableSnapShot("VendorId", Description = "Vendor Reference")]
        [Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long VendorId
        {
            get
            {
                if (_vendor != null) _vendorId = _vendor.Id;
                return _vendorId;
            }
            set
            {
                _vendorId = value;
                if (_vendor != null && value != _vendor.Id) _vendor = null;
            }
        }

        [EnableSnapShot("ServiceTicketId", Description = "Service Ticket Reference")]
        [Property("ServiceTicketId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        protected long ServiceTicketId
        {
            get
            {
                if (_serviceTicket != null) _serviceTicketId = _serviceTicket.Id;
                return _serviceTicketId;
            }
            set
            {
                _serviceTicketId = value;
                if (_serviceTicket != null && value != _serviceTicket.Id) _serviceTicket = null;
            }
        }

        public ServiceTicket ServiceTicket
        {
            get { return _serviceTicket ?? (_serviceTicket = new ServiceTicket(_serviceTicketId)); }
            set
            {
                _serviceTicket = value;
                _serviceTicketId = value == null ? default(long) : value.Id;
            }
        }
        public Vendor Vendor
        {
            get { return _vendor ?? (_vendor = new Vendor(_vendorId)); }
            set
            {
                _vendor = value;
                _vendorId = value == null ? default(long) : value.Id;
            }
        }

        public bool IsRelationBetweenVendorAndVendorBill
        {
            get
            {
                if (Id != default(long)) CheckRelationBetweenVendorAndVendorBill();
                return _isRelationBetweenVendorAndVendorBill;
            }
        }

        public bool IsNew
        {
            get { return Id == default(long); }
        }

        public ServiceTicketVendor() { }

        public ServiceTicketVendor(long id) : this(id, false) { }

        public ServiceTicketVendor(long id, bool takeSnapshot)
        {
            Id = id;
            if (Id == default(long)) return;
            KeyLoaded = Load();
            if (takeSnapshot) TakeSnapShot();
        }

        public ServiceTicketVendor(DbDataReader reader)
        {
            Load(reader);
        }

        public void Save()
        {
            if (Id == default(long)) Insert();
            else Update();
        }

        public new void Delete()
        {
            if (Id != default(long))
                base.Delete();
        }

        public void CheckRelationBetweenVendorAndVendorBill()
        {
            _isRelationBetweenVendorAndVendorBill = false;

            const string query = @"SELECT sc.* from ServiceTicketCharge sc, ServiceTicketVendor sv WHERE sv.Id = @Id AND sc.ServiceTicketId = @ServiceTicketId AND sc.VendorId = sv.VendorId AND sc.TenantId = @TenantId";
            var parameters = new Dictionary<string, object> { { "Id", Id }, { "TenantId", TenantId }, { "ServiceTicketId", ServiceTicketId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _isRelationBetweenVendorAndVendorBill = (new ServiceTicketCharge(reader)).VendorBillId > 0;

            Connection.Close();
        }
    }
}