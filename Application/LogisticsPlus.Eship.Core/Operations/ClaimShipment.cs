﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("ClaimShipment", ReadOnly = false, Source = EntitySource.TableView)]
	public class ClaimShipment : TenantBase
	{
		private long _shipmentId;
		private long _claimId;

		private Claim _claim;
		private Shipment _shipment;

		[EnableSnapShot("ShipmentId", Description = "Shipment Reference")]
		[Property("ShipmentId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long ShipmentId
		{
			get
			{
				if (_shipment != null) _shipmentId = _shipment.Id;
				return _shipmentId;
			}
			set
			{
				_shipmentId = value;
				if (_shipment != null && value != _shipment.Id) _shipment = null;
			}
		}

		[EnableSnapShot("ClaimId", Description = "Claim Reference")]
		[Property("ClaimId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long ClaimId
		{
			get
			{
				if (_claim != null) _claimId = _claim.Id;
				return _claimId;
			}
			set
			{
				_claimId = value;
				if (_claim != null && value != _claim.Id) _claim = null;
			}
		}

		public Claim Claim
		{
			get { return _claim ?? (_claim = new Claim(_claimId)); }
			set
			{
				_claim = value;
				_claimId = value == null ? default(long) : value.Id;
			}
		}
		public Shipment Shipment
		{
			get { return _shipment ?? (_shipment = new Shipment(_shipmentId)); }
			set
			{
				_shipment = value;
				_shipmentId = value == null ? default(long) : value.Id;
			}
		}

		public ClaimShipment()
		{
			
		}

		public ClaimShipment(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			Insert();
		}

		public new void Delete()
		{
			base.Delete();
		}
	}
}
