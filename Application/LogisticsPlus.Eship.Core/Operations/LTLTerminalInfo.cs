﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("LTLTerminalInfo", ReadOnly = false, Source = EntitySource.TableView)]
	public class LTLTerminalInfo : Location
	{
		private const string BlankFaxNumber = "000-000-0000";

		private long _shipmentId;

		private Shipment _shipment;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("Code")]
		[Property("Code", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Code { get; set; }

		[EnableSnapShot("Name")]
		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

		[EnableSnapShot("Phone")]
		[Property("Phone", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Phone { get; set; }

		[EnableSnapShot("TollFree")]
		[Property("TollFree", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string TollFree { get; set; }

		[EnableSnapShot("Fax")]
		[Property("Fax", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Fax { get; set; }

		[EnableSnapShot("Email")]
		[Property("Email", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Email { get; set; }

		[EnableSnapShot("ContactName", Description = "Contact Name")]
		[Property("ContactName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ContactName { get; set; }

		[EnableSnapShot("ContactTitle", Description = "Contact Title")]
		[Property("ContactTitle", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ContactTitle { get; set; }

		[EnableSnapShot("ShipmentId", Description = "Shipment Reference")]
		[Property("ShipmentId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long ShipmentId
		{
			get
			{
				if (_shipment != null) _shipmentId = _shipment.Id;
				return _shipmentId;
			}
			set
			{
				_shipmentId = value;
				if (_shipment != null && value != _shipment.Id) _shipment = null;
			}
		}

		public Shipment Shipment
		{
			get { return _shipment ?? (_shipment = new Shipment(_shipmentId)); }
			set
			{
				_shipment = value;
				_shipmentId = value == null ? default(long) : value.Id;
			}
		}
		public bool ValidFax
		{
			get { return !string.IsNullOrEmpty(Fax) && Fax != BlankFaxNumber; }
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public LTLTerminalInfo() { }

		public LTLTerminalInfo(long id) : this(id, false) { }

		public LTLTerminalInfo(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public LTLTerminalInfo(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
