﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("ShipmentCharge", ReadOnly = false, Source = EntitySource.TableView)]
	public class ShipmentCharge : Charge
	{
		private long _shipmentId;
		private long _vendorId;
		private long _vendorBillId;

		private Shipment _shipment;
		private Vendor _vendor;
		private VendorBill _vendorBill;

        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("ShipmentId", Description = "Shipment Reference")]
		[Property("ShipmentId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long ShipmentId
		{
			get
			{
				if (_shipment != null) _shipmentId = _shipment.Id;
				return _shipmentId;
			}
			set
			{
				_shipmentId = value;
				if (_shipment != null && value != _shipment.Id) _shipment = null;
			}
		}
	
		[EnableSnapShot("VendorId", Description = "Vendor Id")]
		[Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long VendorId
		{
			get { return _vendorId; }
			set
			{
				_vendorId = value;
				if (_vendor != null && _vendor.Id != value) _vendor = null;
			}
		}

		[EnableSnapShot("VendorBillId", Description = "Vendor Bill Id")]
		[Property("VendorBillId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long VendorBillId
		{
			get { return _vendorBillId; }
			set
			{
				_vendorBillId = value;
				if (_vendorBill != null && _vendorBill.Id != value) _vendorBill = null;
			}
		}

		public Shipment Shipment
		{
			get { return _shipment ?? (_shipment = new Shipment(_shipmentId)); }
			set
			{
				_shipment = value;
				_shipmentId = value == null ? default(long) : value.Id;
			}
		}

		public Vendor Vendor
		{
			get { return _vendor ?? (_vendor = new Vendor(_vendorId)); }
			set
			{
				_vendor = value;
				_vendorId = value == null ? default(long) : value.Id;
			}
		}

		public VendorBill VendorBill
		{
			get { return _vendorBill ?? (_vendorBill = new VendorBill(_vendorBillId)); }
			set
			{
				_vendorBill = value;
				_vendorBillId = value == null ? default(long) : value.Id;
			}
		}

        public string VendorBillDocumentNumber
        {
            get { return VendorBill == null ? string.Empty : VendorBill.DocumentNumber; }
        }

        public bool IsNew
		{
			get { return Id == default(long); }
		}

		public ShipmentCharge() { }

		public ShipmentCharge(long id) : this(id, false) { }

		public ShipmentCharge(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public ShipmentCharge(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
