﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("LoadOrderAccountBucket", ReadOnly = false, Source = EntitySource.TableView)]
	public class LoadOrderAccountBucket : TenantBase
	{
		private long _accountBucketId;
		private long _loadOrderId;

		private LoadOrder _loadOrder;
		private AccountBucket _accountBucket;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; internal set; }

		[EnableSnapShot("Primary", Description = "Primary Designation")]
		[Property("Primary", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Primary { get; set; }

		[EnableSnapShot("LoadOrderId", Description = "Load Order Reference")]
		[Property("LoadOrderId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long LoadOrderId
		{
			get
			{
				if (_loadOrder != null) _loadOrderId = _loadOrder.Id;
				return _loadOrderId;
			}
			set
			{
				_loadOrderId = value;
				if (_loadOrder != null && value != _loadOrder.Id) _loadOrder = null;
			}
		}

		[EnableSnapShot("AccountBucketId", Description = "Account Bucket Reference")]
		[Property("AccountBucketId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long AccountBucketId
		{
			get
			{
				if (_accountBucket != null) _accountBucketId = _accountBucket.Id;
				return _accountBucketId;
			}
			set
			{
				_accountBucketId = value;
				if (_accountBucket != null && value != _accountBucket.Id) _accountBucket = null;
			}
		}

		public LoadOrder LoadOrder
		{
			get { return _loadOrder ?? (_loadOrder = new LoadOrder(_loadOrderId)); }
			set
			{
				_loadOrder = value;
				_loadOrderId = value == null ? default(long) : value.Id;
			}
		}
		public AccountBucket AccountBucket
		{
			get { return _accountBucketId == default(long) ? null : _accountBucket ?? (_accountBucket = new AccountBucket(_accountBucketId)); }
			set
			{
				_accountBucket = value;
				_accountBucketId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public LoadOrderAccountBucket() { }

		public LoadOrderAccountBucket(long id) : this(id, false) { }

		public LoadOrderAccountBucket(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public LoadOrderAccountBucket(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

	}
}
