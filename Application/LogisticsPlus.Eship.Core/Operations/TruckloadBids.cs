﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Rating;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("TruckloadTenderingBids", ReadOnly = false, Source = EntitySource.TableView)]
	public class TruckloadBids : TenantBase
	{
		private long _customerId;
		private long _loadOrderId;
		private long _firstPreferredVendorId;
		private long _secondPreferredVendorId;
		private long _thirdPreferredVendorId;
		private long _tlProfileId;
		private long _tlLaneId;
		private long _userId;
		

		private Customer _customer;
		private LoadOrder _loadOrder;
		private CustomerTLTenderingProfile _tlProfile;
		private CustomerTLTenderingProfileLane _tlLane;
		private Vendor _firstPrferredVendor;
		private Vendor _secondPreferredVendor;
		private Vendor _thirdPreferredVendor;
		private User _user;
		
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("CustomerId", Description = "Customer Reference")]
		[Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long CustomerId
		{
			get { return _customerId; }
			set
			{
				_customerId = value;
				if (_customer != null && value != _customer.Id) _customer = null;
			}
		}
		[EnableSnapShot("LoadOrderId", Description = "Customer Reference")]
		[Property("LoadOrderId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long LoadOrderId
		{
			get { return _loadOrderId; }
			set
			{
				_loadOrderId = value;
				if (_loadOrderId != null && value != _loadOrder.Id) _loadOrder = null;
			}
		}

		[EnableSnapShot("TLTenderingProfileId", Description = "TLTenderingProfile Reference")]
		[Property("TLTenderingProfileId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long TLTenderingProfileId
		{
			get
			{ return _tlProfileId; }
			set
			{
				_tlProfileId = value;
				if (_tlProfile != null && value != _tlProfile.Id) _tlProfile = null;
			}
		}
		[EnableSnapShot("TLTenderingProfileLaneId", Description = "TLTenderingProfileLane Reference")]
		[Property("TLTenderingProfileLaneId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long TLLaneId
		{
			get
			{ return _tlLaneId; }
			set
			{
				_tlLaneId = value;
				if (_tlLane != null && value != _tlLane.Id) _tlLane = null;
			}
		}

		[EnableSnapShot("FirstPreferredVendorId", Description = "First Preferred Vendor Reference")]
		[Property("FirstPreferredVendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long FirstPreferredVendorId
		{
			get
			{
				if (_firstPrferredVendor != null) _firstPreferredVendorId = _firstPrferredVendor.Id;
				return _firstPreferredVendorId;
			}
			set
			{
				_firstPreferredVendorId = value;
				if (_firstPrferredVendor != null && value != _firstPrferredVendor.Id) _firstPrferredVendor = null;
			}
		}

		[EnableSnapShot("SecondPreferredVendorId", Description = "First Preferred Vendor Reference")]
		[Property("SecondPreferredVendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long SecondPreferredVendorId
		{
			get
			{
				if (_secondPreferredVendor != null) _secondPreferredVendorId = _secondPreferredVendor.Id;
				return _secondPreferredVendorId;
			}
			set
			{
				_secondPreferredVendorId = value;
				if (_secondPreferredVendor != null && value != _secondPreferredVendor.Id) _secondPreferredVendor = null;
			}
		}

		[EnableSnapShot("ThirdPreferredVendorId", Description = "Third Preferred Vendor Reference")]
		[Property("ThirdPreferredVendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ThirdPreferredVendorId
		{
			get
			{
				if (_thirdPreferredVendor != null) _thirdPreferredVendorId = _thirdPreferredVendor.Id;
				return _thirdPreferredVendorId;
			}

			set
			{
				_thirdPreferredVendorId = value;
				if (_thirdPreferredVendor != null && value != _thirdPreferredVendor.Id) _thirdPreferredVendor = null;
			}
		}
		

		[EnableSnapShot("DateCreated", Description = "Date Created")]
		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime2, Key = false)]
		public DateTime DateCreated { get; set; }

		[EnableSnapShot("TimeLoadTendered1", Description = "Time Load Tendered to 1st Vendor")]
		[Property("TimeLoadTendered1", AutoValueOnInsert = false, DataType = SqlDbType.DateTime2, Key = false)]
		public DateTime TimeLoadTendered1 { get; set; }

		[EnableSnapShot("TimeLoadTendered2", Description = "Time Load Tendered to 2nd Vendor")]
		[Property("TimeLoadTendered2", AutoValueOnInsert = false, DataType = SqlDbType.DateTime2, Key = false)]
		public DateTime TimeLoadTendered2 { get; set; }

		[EnableSnapShot("TimeLoadTendered3", Description = "Time Load Tendered to 3rd Vendor")]
		[Property("TimeLoadTendered3", AutoValueOnInsert = false, DataType = SqlDbType.DateTime2, Key = false)]
		public DateTime TimeLoadTendered3 { get; set; }

		[EnableSnapShot("ExpirationTime1", Description = "Expiration time for 1st Vendor")]
		[Property("ExpirationTime1", AutoValueOnInsert = false, DataType = SqlDbType.DateTime2, Key = false)]
		public DateTime ExpirationTime1 { get; set; }

		[EnableSnapShot("ExpirationTime2", Description = "Expiration time for 2nd Vendor")]
		[Property("ExpirationTime2", AutoValueOnInsert = false, DataType = SqlDbType.DateTime2, Key = false)]
		public DateTime ExpirationTime2 { get; set; }

		[EnableSnapShot("ExpirationTime3", Description = "Expiration time for 3rd Vendor")]
		[Property("ExpirationTime3", AutoValueOnInsert = false, DataType = SqlDbType.DateTime2, Key = false)]
		public DateTime ExpirationTime3 { get; set; }


		[EnableSnapShot("UserId", Description = "User Reference")]
		[Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long UserId
		{
			get
			{
				if (_user != null) _userId = _user.Id;
				return _userId;
			}
			set
			{
				_userId = value;
				if (_user != null && value != _user.Id) _user = null;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public Customer Customer
		{
			get { return _customerId == default(long) ? null : _customer ?? (_customer = new Customer(_customerId, false)); }
			set
			{
				_customer = value;
				_customerId = value == null ? default(long) : value.Id;
			}
		}

		public User User
		{
			get { return _userId == default(long) ? null : _user ?? (_user = new User(_userId, false)); }
			set
			{
				_user = value;
				_userId = value == null ? default(long) : value.Id;
			}
		}
		


		public TruckloadBids()
		{
		}

		public TruckloadBids(long id)
			: this(id, false)
		{
		}

		public TruckloadBids(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public TruckloadBids(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
		

	}
}

