﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("ClaimNote", ReadOnly = false, Source = EntitySource.TableView)]
	public class ClaimNote : Note
	{
		private long _claimId;

		private Claim _claim;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("ClaimId", Description = "Claim Reference")]
		[Property("ClaimId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long ClaimId
		{
			get
			{
				if (_claim != null) _claimId = _claim.Id;
				return _claimId;
			}
			set
			{
				_claimId = value;
				if (_claim != null && value != _claim.Id) _claim = null;
			}
		}

        [EnableSnapShot("DateCreated", Description = "Date Created")]
        [Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime DateCreated { get; set; }

        [EnableSnapShot("SendReminder", Description = "Send Reminder")]
        [Property("SendReminder", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool SendReminder { get; set; }

        [EnableSnapShot("SendReminderOn", Description = "Send Reminder On Date")]
        [Property("SendReminderOn", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime SendReminderOn { get; set; }

        [EnableSnapShot("SendReminderEmails", Description = "Send Reminder Emails")]
        [Property("SendReminderEmails", AutoValueOnInsert = false, DataType = SqlDbType.Text, Key = false)]
        public string SendReminderEmails { get; set; }

		public Claim Claim
		{
			get { return _claim ?? (_claim = new Claim(_claimId)); }
			set
			{
				_claim = value;
				_claimId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public ClaimNote() { }

		public ClaimNote(long id) : this(id, false) { }

		public ClaimNote(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public ClaimNote(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
