﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("LoadOrderDocument", ReadOnly = false, Source = EntitySource.TableView)]
	public class LoadOrderDocument : Document
	{
		private long _loadOrderId;

		private LoadOrder _loadOrder;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("IsInternal", Description = "Is Internal")]
		[Property("IsInternal", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool IsInternal { get; set; }

		[Property("LoadOrderId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long LoadOrderId
		{
			get
			{
				if (_loadOrder != null) _loadOrderId = _loadOrder.Id;
				return _loadOrderId;
			}
			set
			{
				_loadOrderId = value;
				if (_loadOrder != null && value != _loadOrder.Id) _loadOrder = null;
			}
		}

		public LoadOrder LoadOrder
		{
			get { return _loadOrder ?? (_loadOrder = new LoadOrder(_loadOrderId)); }
			set
			{
				_loadOrder = value;
				_loadOrderId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public LoadOrderDocument() { }

		public LoadOrderDocument(long id) : this(id, false) { }

		public LoadOrderDocument(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public LoadOrderDocument(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
