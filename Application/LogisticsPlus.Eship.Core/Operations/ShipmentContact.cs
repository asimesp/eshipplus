﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("ShipmentContact", ReadOnly = false, Source = EntitySource.TableView)]
	public class ShipmentContact : Contact
	{
		private long _shipmentLocationId;

		private ShipmentLocation _shipmentLocation;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[Property("ShipmentLocationId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long ShipmentLocationId
		{
			get
			{
				if (_shipmentLocation != null) _shipmentLocationId = _shipmentLocation.Id;
				return _shipmentLocationId;
			}
			set
			{
				_shipmentLocationId = value;
				if (_shipmentLocation != null && value != _shipmentLocation.Id) _shipmentLocation = null;
			}
		}

		public ShipmentLocation Location
		{
			get { return _shipmentLocation ?? (_shipmentLocation = new ShipmentLocation(_shipmentLocationId)); }
			set
			{
				_shipmentLocation = value;
				_shipmentLocationId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew { get { return Id == default(long); } }

		public ShipmentContact()
		{
			
		}

		public ShipmentContact(long id) : this(id, false)
		{
		}

		public ShipmentContact(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public ShipmentContact(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}