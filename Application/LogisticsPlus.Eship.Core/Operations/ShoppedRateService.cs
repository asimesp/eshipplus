﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
    [Entity("ShoppedRateService", ReadOnly = false, Source = EntitySource.TableView)]
    public class ShoppedRateService : TenantBase
    {
        private long _serviceId;
        private long _shoppedRateId;

        private ShoppedRate _shoppedRate;
        private Service _service;

        [EnableSnapShot("ServiceId", Description = "Service Reference")]
        [Property("ServiceId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long ServiceId
        {
            get { return _serviceId; }
            set
            {
                _serviceId = value;
                if (_service != null && value != _service.Id) _service = null;
            }
        }

        [EnableSnapShot("ShoppedRateId", Description = "Address Book Reference")]
        [Property("ShoppedRateId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long ShoppedRateId
        {
            get
            {
                if (_shoppedRate != null) _shoppedRateId = _shoppedRate.Id;
                return _shoppedRateId;
            }
            set
            {
                _shoppedRateId = value;
                if (_shoppedRate != null && value != _shoppedRate.Id) _shoppedRate = null;
            }
        }

        public ShoppedRate ShoppedRate
        {
            get { return _shoppedRate ?? (_shoppedRate = new ShoppedRate(_shoppedRateId)); }
            set
            {
                _shoppedRate = value;
                _shoppedRateId = value == null ? default(long) : value.Id;
            }
        }
        public Service Service
        {
            get { return _serviceId == default(long) ? null : _service ?? (_service = new Service(_serviceId, false)); }
            set
            {
                _service = value;
                _serviceId = value == null ? default(long) : value.Id;
            }
        }

        public ShoppedRateService() { }

        public ShoppedRateService(DbDataReader reader)
        {
            Load(reader);
        }

        public void Save()
        {
            Insert();
        }

        public new void Delete()
        {
            base.Delete();
        }

    }
}
