﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("ShipmentDocument", ReadOnly = false, Source = EntitySource.TableView)]
	public class ShipmentDocument : Document
	{
		private long _shipmentId;
		private long _vendorBillId;

		private Shipment _shipment;
		private VendorBill _vendorBill;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("IsInternal", Description = "Is Internal")]
		[Property("IsInternal", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool IsInternal { get; set; }

		[EnableSnapShot("FtpDelivered", Description = "Ftp Delivered")]
		[Property("FtpDelivered", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool FtpDelivered { get; set; }

		[EnableSnapShot("VendorBillId", Description = "Vendor Bill Id")]
		[Property("VendorBillId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long VendorBillId 
			{
			get
			{
				if (_vendorBill != null) _vendorBillId = _vendorBill.Id;
				return _vendorBillId;
			}
			set
			{
				_vendorBillId = value;
				if (_vendorBill != null && value != _vendorBill.Id) _vendorBill = null;
			}
		}

		[Property("ShipmentId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ShipmentId
		{
			get
			{
				if (_shipment != null) _shipmentId = _shipment.Id;
				return _shipmentId;
			}
			set
			{
				_shipmentId = value;
				if (_shipment != null && value != _shipment.Id) _shipment = null;
			}
		}

		public Shipment Shipment
		{
			get { return _shipment ?? (_shipment = new Shipment(_shipmentId)); }
			set
			{
				_shipment = value;
				_shipmentId = value == null ? default(long) : value.Id;
			}
		}

		public VendorBill VendorBill
		{
			get { return _vendorBill ?? (_vendorBill = new VendorBill(_vendorBillId)); }
			set
			{
				_vendorBill = value;
				_vendorBillId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public ShipmentDocument() { }

		public ShipmentDocument(long id) : this(id, false) { }

		public ShipmentDocument(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public ShipmentDocument(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
