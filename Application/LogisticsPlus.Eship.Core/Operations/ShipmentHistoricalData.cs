﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
    [Serializable]
    [Entity("ShipmentHistoricalData", ReadOnly = false, Source = EntitySource.TableView)]
    public class ShipmentHistoricalData : ObjectBase
    {
        private long _shipmentId;

        private Shipment _shipment;


        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; private set; }

        [EnableSnapShot("ShipmentId", Description = "Shipment Reference")]
        [Property("ShipmentId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long ShipmentId
        {
            get
            {
	            if (_shipment != null) _shipmentId = _shipment.Id;
	            return _shipmentId;
            }
            set
            {
                _shipmentId = value;
                if (_shipment != null && value != _shipment.Id) _shipment = null;
            }
        }

        [EnableSnapShot("OriginalEstimatedDeliveryDate", Description = "Original Estimated Delivery Date")]
        [Property("OriginalEstimatedDeliveryDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime OriginalEstimatedDeliveryDate { get; set; }

        [EnableSnapShot("OriginalEstimatedPickupDate", Description = "Original Estimated Pickup Date")]
        [Property("OriginalEstimatedPickupDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime OriginalEstimatedPickupDate { get; set; }

        [EnableSnapShot("WasInDispute", Description = "Was In Dispute")]
        [Property("WasInDispute", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool WasInDispute { get; set; }

        [EnableSnapShot("WasInDisputeReason", Description = "Was In Dispute Reason")]
        [Property("WasInDisputeReason", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public InDisputeReason WasInDisputeReason { get; set; }


        public Shipment Shipment
        {
            get { return _shipmentId == default(long) ? null : _shipment ?? (_shipment = new Shipment(_shipmentId, false)); }
            set
            {
                _shipment = value;
                _shipmentId = value == null ? default(long) : value.Id;
            }
        }


        public bool IsNew
		{
			get { return Id == default(long); }
		}

		public ShipmentHistoricalData()
		{		
		}

		public ShipmentHistoricalData(long id)
			: this(id, false)
		{

		}

        public ShipmentHistoricalData(long id, bool takeSnapshot)
		{
            Id = id;
            if (Id == default(long)) return;
            KeyLoaded = Load();
            if (takeSnapshot) TakeSnapShot();
		}

        public ShipmentHistoricalData(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
            if (Id == default(long)) Insert();
            else Update();
		}

		public new void Delete()
		{
            if (Id != default(long)) base.Delete();
		}
    }
}
