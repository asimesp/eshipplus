﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("ShipmentAccountBucket", ReadOnly = false, Source = EntitySource.TableView)]
	public class ShipmentAccountBucket : TenantBase
	{
		private long _accountBucketId;
		private long _shipmentId;

		private Shipment _shipment;
		private AccountBucket _accountBucket;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; internal set; }

		[EnableSnapShot("Primary", Description = "Primary Designation")]
		[Property("Primary", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Primary { get; set; }

		[EnableSnapShot("ShipmentId", Description = "Shipment Reference")]
		[Property("ShipmentId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long ShipmentId
		{
			get
			{
				if (_shipment != null) _shipmentId = _shipment.Id;
				return _shipmentId;
			}
			set
			{
				_shipmentId = value;
				if (_shipment != null && value != _shipment.Id) _shipment = null;
			}
		}

		[EnableSnapShot("AccountBucketId", Description = "Account Bucket Reference")]
		[Property("AccountBucketId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long AccountBucketId
		{
			get
			{
				if (_accountBucket != null) _accountBucketId = _accountBucket.Id;
				return _accountBucketId;
			}
			set
			{
				_accountBucketId = value;
				if (_accountBucket != null && value != _accountBucket.Id) _accountBucket = null;
			}
		}

		public Shipment Shipment
		{
			get { return _shipment ?? (_shipment = new Shipment(_shipmentId)); }
			set
			{
				_shipment = value;
				_shipmentId = value == null ? default(long) : value.Id;
			}
		}
		public AccountBucket AccountBucket
		{
			get { return _accountBucketId == default(long) ? null : _accountBucket ?? (_accountBucket = new AccountBucket(_accountBucketId)); }
			set
			{
				_accountBucket = value;
				_accountBucketId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public ShipmentAccountBucket() { }

		public ShipmentAccountBucket(long id) : this(id, false) { }

		public ShipmentAccountBucket(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public ShipmentAccountBucket(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

	}
}
