﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("LoadOrderCharge", ReadOnly = false, Source = EntitySource.TableView)]
	public class LoadOrderCharge : Charge
	{
		private long _loadOrderId;
		private long _vendorId;

		private LoadOrder _loadOrder;
		private Vendor _vendor;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("LoadOrderId", Description = "Load Order Reference")]
		[Property("LoadOrderId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long LoadOrderId
		{
			get
			{
				if (_loadOrder != null) _loadOrderId = _loadOrder.Id;
				return _loadOrderId;
			}
			set
			{
				_loadOrderId = value;
				if (_loadOrder != null && value != _loadOrder.Id) _loadOrder = null;
			}
		}


		[EnableSnapShot("VendorId", Description = "Vendor Id")]
		[Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long VendorId
		{
			get { return _vendorId; }
			set
			{
				_vendorId = value;
				if (_vendor != null && _vendor.Id != value) _vendor = null;
			}
		}

		public LoadOrder LoadOrder
		{
			get { return _loadOrder ?? (_loadOrder = new LoadOrder(_loadOrderId)); }
			set
			{
				_loadOrder = value;
				_loadOrderId = value == null ? default(long) : value.Id;
			}
		}

		public Vendor Vendor
		{
			get { return _vendor ?? (_vendor = new Vendor(_vendorId)); }
			set
			{
				_vendor = value;
				_vendorId = value == null ? default(long) : value.Id;
			}
		}

        public bool IsNew
		{
			get { return Id == default(long); }
		}

		public LoadOrderCharge() { }

		public LoadOrderCharge(long id) : this(id, false) { }

		public LoadOrderCharge(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public LoadOrderCharge(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}