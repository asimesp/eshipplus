﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("AddressBook", ReadOnly = false, Source = EntitySource.TableView)]
	public class AddressBook : Location
	{
		private Customer _customer;

		private List<AddressBookService> _services;
		private List<AddressBookContact> _contacts;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("Description")]
		[Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Description { get; set; }

		[EnableSnapShot("OriginSpecialInstruction", Description = "Origin Special Instructions")]
		[Property("OriginSpecialInstruction", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginSpecialInstruction { get; set; }

		[EnableSnapShot("DestinationSpecialInstruction", Description = "Destination Special Instructions")]
		[Property("DestinationSpecialInstruction", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationSpecialInstruction { get; set; }

		[EnableSnapShot("GeneralInfo", Description = "General Info")]
		[Property("GeneralInfo", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string GeneralInfo { get; set; }

		[EnableSnapShot("Direction")]
		[Property("Direction", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Direction { get; set; }

		[EnableSnapShot("CustomerId", Description = "Customer Reference")]
		[Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		private long CustomerId { get; set; }

        public bool IsNew
        {
            get { return Id == default(long); }
        }

		public Customer Customer
		{
			get { return CustomerId == default(long) ? null : _customer ?? (_customer = new Customer(CustomerId)); }
			set
			{
				_customer = value;
				CustomerId = value == null ? default(long) : value.Id;
			}
		}

		public List<AddressBookService> Services
		{
			get
			{
				if (_services == null) LoadServices();
				return _services;
			}
			set { _services = value; }
		}

		public List<AddressBookContact> Contacts
		{
			get
			{
				if (_contacts == null) LoadContacts();
				return _contacts;
			}
			set { _contacts = value; }
		}

		public AddressBook() { }

		public AddressBook(long id) : this(id, false)
		{
		}

		public AddressBook(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

	    public AddressBook(DbDataReader reader)
		{
			Load(reader);
		}

	    public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

	    public new void Delete()
		{
			if (Id != default(long)) base.Delete();
		}

        public void LoadCollections()
        {
            LoadServices();
            LoadContacts();
        }

		private void LoadServices()
		{
			_services = new List<AddressBookService>();

			const string query =
				@"SELECT * FROM AddressBookService WHERE AddressBookId = @AddressBookId AND TenantId = @TenantId";

			var parameters = new Dictionary<string, object> {{"AddressBookId", Id}, {"TenantId", TenantId}};
			
            using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_services.Add(new AddressBookService(reader));
			Connection.Close();
		}
                
		private void LoadContacts()
		{
			_contacts = new List<AddressBookContact>();
			const string query =
				@"SELECT * FROM AddressBookContact WHERE AddressBookId = @AddressBookId AND TenantId = @TenantId";

			var parameters = new Dictionary<string, object> {{"AddressBookId", Id}, {"TenantId", TenantId}};

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_contacts.Add(new AddressBookContact(reader));
			Connection.Close();
		}
	}
}
