﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
	[Serializable]
	[Entity("ClaimVendor", ReadOnly = false, Source = EntitySource.TableView)]
	public class ClaimVendor : TenantBase
	{
		private long _vendorId;
		private long _claimId;

		private Claim _claim;
		private Vendor _vendor;

        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; private set; }

		[EnableSnapShot("VendorId", Description = "Vendor Reference")]
		[Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long VendorId
		{
			get
			{ return _vendorId; }
			set
			{
				_vendorId = value;
				if (_vendor != null && value != _vendor.Id) _vendor = null;
			}
		}

		[EnableSnapShot("ClaimId", Description = "Claim Reference")]
		[Property("ClaimId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ClaimId
		{
			get
			{
				if (_claim != null) _claimId = _claim.Id;
				return _claimId;
			}
			set
			{
				_claimId = value;
				if (_claim != null && value != _claim.Id) _claim = null;
			}
		}

        [EnableSnapShot("ProNumber", Description = "Pro Number")]
        [Property("ProNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ProNumber { get; set; }

        [EnableSnapShot("FreightBillNumber", Description = "Freight Bill Number")]
        [Property("FreightBillNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string FreightBillNumber { get; set; }

		public Claim Claim
		{
			get { return _claim ?? (_claim = new Claim(_claimId)); }
			set
			{
				_claim = value;
				_claimId = value == null ? default(long) : value.Id;
			}
		}
		public Vendor Vendor
		{
			get { return _vendor ?? (_vendor = new Vendor(_vendorId)); }
			set
			{
				_vendor = value;
				_vendorId = value == null ? default(long) : value.Id;
			}
		}

        public bool IsNew
        {
            get { return Id == default(long); }
        }

		public ClaimVendor()
		{
			
		}

        public ClaimVendor(long id) : this(id, false) { }

        public ClaimVendor(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public ClaimVendor(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
            if (Id == default(long)) Insert();
            else Update();
		}

		public new void Delete()
		{
			base.Delete();
		}
	}
}
