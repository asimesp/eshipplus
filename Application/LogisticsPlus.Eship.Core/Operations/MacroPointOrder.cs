﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Administration;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
    [Serializable]
    [Entity("MacroPointOrder", ReadOnly = false, Source = EntitySource.TableView)]
    public class MacroPointOrder : TenantBase
    {
        private long _userId;
        private User _user;

        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; protected set; }

        [EnableSnapShot("IdNumber", Description = "Id Number")]
        [Property("IdNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string IdNumber { get; set; }

        [EnableSnapShot("DateCreated", Description = "Date Created")]
        [Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime DateCreated { get; set; }

        [EnableSnapShot("DateStopRequested", Description = "Date Stop Requested")]
        [Property("DateStopRequested", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime DateStopRequested { get; set; }

        [EnableSnapShot("Number", Description = "Number")]
        [Property("Number", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Number { get; set; }

        [EnableSnapShot("NumberType", Description = "Number Type")]
        [Property("NumberType", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string NumberType { get; set; }

        [EnableSnapShot("OrderId", Description = "Order Id")]
        [Property("OrderId", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OrderId { get; set; }

        [EnableSnapShot("TrackCost", Description = "Track Cost")]
        [Property("TrackCost", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal TrackCost { get; set; }

        [EnableSnapShot("TrackDurationHours", Description = "Track Duration Hours")]
        [Property("TrackDurationHours", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int TrackDurationHours { get; set; }

        [EnableSnapShot("TrackIntervalMinutes", Description = "Track Interval Minutes")]
        [Property("TrackIntervalMinutes", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int TrackIntervalMinutes { get; set; }

        [EnableSnapShot("TrackingStatusCode", Description = "Tracking Status Code")]
        [Property("TrackingStatusCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string TrackingStatusCode { get; set; }

        [EnableSnapShot("TrackingStatusDescription", Description = "Tracking Status Description")]
        [Property("TrackingStatusDescription", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string TrackingStatusDescription { get; set; }

		[EnableSnapShot("EmailCopiesOfUpdatesTo", Description = "MacroPoint Email Copies Of Updates To")]
		[Property("EmailCopiesOfUpdatesTo", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string EmailCopiesOfUpdatesTo { get; set; }

		[EnableSnapShot("Notes", Description = "MacroPoint Notes")]
		[Property("Notes", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Notes { get; set; }

        [EnableSnapShot("StartTrackDateTime", Description = "Start Track Date Time")]
        [Property("StartTrackDateTime", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime StartTrackDateTime { get; set; }

        [EnableSnapShot("UserId", Description = "User Reference")]
        [Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        protected long UserId
        {
            get { return _userId; }
            set
            {
                _userId = value;
                if (_user != null && value != _user.Id) _user = null;
            }
        }


        public User User
        {
            get { return _userId == default(long) ? null : _user ?? (_user = new User(_userId, false)); }
            set
            {
                _user = value;
                _userId = value == null ? default(long) : value.Id;
            }
        }

		public bool IsNew
		{
			get { return Id == default(long); }
		}

        public MacroPointOrder() { }

		public MacroPointOrder(long id) : this(id, false) { }

		public MacroPointOrder(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

        public MacroPointOrder(DbDataReader reader)
		{
			Load(reader);
		}

		public MacroPointOrder(string orderId, long tenantId)
		{
			const string query = @"Select * from MacroPointOrder where OrderId = @OrderId And TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "TenantId", tenantId }, { "OrderId", orderId } };
			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					Load(reader);
			Connection.Close();
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

		public Shipment RetrieveCorrespondingShipment()
		{
			Shipment shipment = null;
			const string query =
				@"Select * from Shipment where ShipmentNumber = @ShipmentNumber And TenantId = @TenantId";
			var parameters = new Dictionary<string, object> {{"TenantId", TenantId}, {"ShipmentNumber", IdNumber}};
			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					shipment = new Shipment(reader);
			Connection.Close();
			return shipment;
		}
    }
}
