﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core
{
	[Serializable]
	[Entity("Tenant", ReadOnly =  false, Source = EntitySource.TableView)]
	public class Tenant : ObjectBase
	{
		private long _countryId;
		private long _hazardousMaterialServiceId;
		private long _borderCrossingServiceId;
		private long _guaranteedDeliveryServiceId;
		private long _defaultUnmappedChargeCodeId;
		private long _defaultSchedulingContactTypeId;
		private long _defaultPackagingTypeId;
		private long _availableLoadsContactTypeId;
		private long _defaultSystemUserId;
		private long _macroPointChargeCodeId;
		private long _podDocumentTagId;
		private long _bolDocumentTagId;
		private long _wniDocumentTagId;

		private DocumentTag _podDocumentTag;
		private DocumentTag _bolDocumentTag;
		private DocumentTag _wniDocumentTag;
		private ChargeCode _macroPointChargeCode;
		private ContactType _availableLoadsContactType;
		private ContactType _defaultSchedulingContactType;
		private PackageType _defaultPackagingType;
		private Service _borderCrossingService;
		private Service _hazardousMaterialService;
		private Service _guaranteedDeliveryService;
		private ChargeCode _defaultUnmappedChargeCode;
		private Country _country;
		private User _defaultSystemUser;

		private List<AutoNumber> _autoNumbers;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("Code")]
		[Property("Code", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Code { get; set; }

		[EnableSnapShot("Active")]
		[Property("Active", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Active { get; set; }

		[EnableSnapShot("Name")]
		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

		[EnableSnapShot("TenantScac", Description = "Tenant SCAC")]
		[Property("TenantScac", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string TenantScac { get; set; }

		[EnableSnapShot("LogoUrl", Description = "Logo Url")]
		[Property("LogoUrl", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LogoUrl { get; set; }

		[EnableSnapShot("DefaultCountryId", Description = "Default Country Reference")]
		[Property("DefaultCountryId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long DefaultCountryId
		{
			get { return _countryId; }
			set
			{
				_countryId = value;
				if (_country != null && value != _country.Id) _country = null;
			}
		}

		[EnableSnapShot("InsuranceCostPerDollar", Description = "Insurance Cost Per Dollar")]
		[Property("InsuranceCostPerDollar", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal InsuranceCostPerDollar { get; set; }

		[EnableSnapShot("InsuranceCostTotalPercentage", Description = "Insurance Cost Total Percentage")]
		[Property("InsuranceCostTotalPercentage", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal InsuranceCostTotalPercentage { get; set; }

		[EnableSnapShot("InsuranceCostPurchaseFloor", Description = "Insurance Cost Purchase Floor")]
		[Property("InsuranceCostPurchaseFloor", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal InsuranceCostPurchaseFloor { get; set; }

		[EnableSnapShot("InsuranceRevenueRatePerDollar", Description = "Insurance Revenue Rate Per Dollar")]
		[Property("InsuranceRevenueRatePerDollar", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal InsuranceRevenueRatePerDollar { get; set; }

		[EnableSnapShot("InsuranceRevenueTotalPercentage", Description = "Insurance Revenue Total Percentage")]
		[Property("InsuranceRevenueTotalPercentage", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal InsuranceRevenueTotalPercentage { get; set; }

		[EnableSnapShot("SMCRateWareParameterFile", Description = "SMC Rate Ware Parameter File")]
		[Property("SMCRateWareParameterFile", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string SMCRateWareParameterFile { get; set; }

		[EnableSnapShot("SMCRateWareEnabled", Description = "SMC Rate Ware Enabled")]
		[Property("SMCRateWareEnabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool SMCRateWareEnabled { get; set; }

		[EnableSnapShot("SMCCarrierConnectParameterFile", Description = "SMC Carrier Connect Parameter File")]
		[Property("SMCCarrierConnectParameterFile", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string SMCCarrierConnectParameterFile { get; set; }

		[EnableSnapShot("SMCCarrierConnectEnabled", Description = "SMC Carrier Connect Enabled")]
		[Property("SMCCarrierConnectEnabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool SMCCarrierConnectEnabled { get; set; }

		[EnableSnapShot("FedExSmallPackParameterFile", Description = "FedEx Small Pack Parameter File")]
		[Property("FedExSmallPackParameterFile", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string FedExSmallPackParameterFile { get; set; }

		[EnableSnapShot("FedExSmallPackEnabled", Description = "FedEx Small Pack Enabled")]
		[Property("FedExSmallPackEnabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool FedExSmallPackEnabled { get; set; }

		[EnableSnapShot("UpsSmallPackParameterFile", Description = "Ups Small Pack Parameter File")]
		[Property("UpsSmallPackParameterFile", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string UpsSmallPackParameterFile { get; set; }

		[EnableSnapShot("UpsSmallPackEnabled", Description = "Ups Small Pack Enabled")]
		[Property("UpsSmallPackEnabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool UpsSmallPackEnabled { get; set; }

		[EnableSnapShot("MacroPointParameterFile", Description = "Macro Point Parameter File")]
		[Property("MacroPointParameterFile", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string MacroPointParameterFile { get; set; }

		[EnableSnapShot("MacroPointEnabled", Description = "Macro Point Enabled")]
		[Property("MacroPointEnabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool MacroPointEnabled { get; set; }

		[EnableSnapShot("TermsAndConditionsFile", Description = "Terms and Conditions File")]
		[Property("TermsAndConditionsFile", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string TermsAndConditionsFile { get; set; }

		[EnableSnapShot("AutoRatingNoticeText", Description = "Auto Rating Notice Text")]
		[Property("AutoRatingNoticeText", AutoValueOnInsert = false, DataType = SqlDbType.Text, Key = false)]
		public string AutoRatingNoticeText { get; set; }

		[EnableSnapShot("BillingLocationId", Description = "Billing Location Reference")]
		[Property("BillingLocationId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long BillingLocationId
		{
			get { return BillingLocation == null ? default(long) : BillingLocation.Id; }
			set
			{
				if (BillingLocation != null && BillingLocation.Id == value) return;
				BillingLocation = value == default(long) ? null : new TenantLocation(value, false);
			}
		}

		[EnableSnapShot("MailingLocationId", Description = "Mailing Location Reference")]
		[Property("MailingLocationId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long MailingLocationId
		{
			get { return MailingLocation == null ? default(long) : MailingLocation.Id; }
			set
			{
				if (MailingLocation != null && MailingLocation.Id == value) return;
				MailingLocation = value == default(long) ? null : new TenantLocation(value, false);
			}
		}

		[EnableSnapShot("HazardousMaterialServiceId", Description = "Hazardous Material Service Reference")]
		[Property("HazardousMaterialServiceId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long HazardousMaterialServiceId
		{
			get
			{
				return _hazardousMaterialServiceId;
			}
			set
			{
				_hazardousMaterialServiceId = value;
				if (_hazardousMaterialService != null && value != _hazardousMaterialService.Id) _hazardousMaterialService = null;
			}
		}

		[EnableSnapShot("BorderCrossingServiceId", Description = "Border Crossing Service Reference")]
		[Property("BorderCrossingServiceId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long BorderCrossingServiceId
		{
			get
			{
				return _borderCrossingServiceId;
			}
			set
			{
				_borderCrossingServiceId = value;
				if (_borderCrossingService != null && value != _borderCrossingService.Id) _borderCrossingService = null;
			}
		}

		[EnableSnapShot("GuaranteedDeliveryServiceId", Description = "Guaranteed Delivery Service Reference")]
		[Property("GuaranteedDeliveryServiceId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long GuaranteedDeliveryServiceId
		{
			get
			{
				return _guaranteedDeliveryServiceId;
			}
			set
			{
				_guaranteedDeliveryServiceId = value;
				if (_guaranteedDeliveryService != null && value != _guaranteedDeliveryService.Id) _guaranteedDeliveryService = null;
			}
		}

	    [EnableSnapShot("DefaultUnmappedChargeCodeId", Description = "Default Unmapped Charge Code")]
		[Property("DefaultUnmappedChargeCodeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long DefaultUnmappedChargeCodeId
        {
			get
			{
				return _defaultUnmappedChargeCodeId;
			}
			set
			{
			    _defaultUnmappedChargeCodeId = value;
				if (_defaultUnmappedChargeCode != null && value != _defaultUnmappedChargeCode.Id) _defaultUnmappedChargeCode = null;
			}
		}

        [EnableSnapShot("DefaultSchedulingContactTypeId", Description = "Default Scheduling Contact Type Reference")]
		[Property("DefaultSchedulingContactTypeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long DefaultSchedulingContactTypeId
		{
			get
			{
				return _defaultSchedulingContactTypeId;
			}
			set
			{
				_defaultSchedulingContactTypeId = value;
				if (_defaultSchedulingContactType != null && value != _defaultSchedulingContactType.Id) _defaultSchedulingContactType = null;
			}
		}

		[EnableSnapShot("DefaultPackagingTypeId", Description = "Default Packaging Type Reference")]
		[Property("DefaultPackagingTypeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long DefaultPackagingTypeId
		{
			get
			{
				return _defaultPackagingTypeId;
			}
			set
			{
				_defaultPackagingTypeId = value;
				if (_defaultPackagingType != null && value != _defaultPackagingType.Id) _defaultPackagingType = null;
			}
		}

		[EnableSnapShot("AvailableLoadsContactTypeId", Description = "Available Loads Contact Type Reference")]
		[Property("AvailableLoadsContactTypeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long AvailableLoadsContactTypeId
		{
			get
			{
				return _availableLoadsContactTypeId;
			}
			set
			{
				_availableLoadsContactTypeId = value;
				if (_availableLoadsContactType != null && value != _availableLoadsContactType.Id) _availableLoadsContactType = null;
			}
		}

		[EnableSnapShot("DefaultSystemUserId", Description = "Default System User Reference")]
		[Property("DefaultSystemUserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long DefaultSystemUserId
		{
			get { return _defaultSystemUserId; }
			set
			{
				_defaultSystemUserId = value;
				if (_defaultSystemUser != null && value != _defaultSystemUser.Id) _defaultSystemUser = null;
			}
		}

		[EnableSnapShot("MacroPointChargeCodeId", Description = "Macro Point Charge Code Reference")]
		[Property("MacroPointChargeCodeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long MacroPointChargeCodeId
		{
			get { return _macroPointChargeCodeId; }
			set
			{
				_macroPointChargeCodeId = value;
				if (_macroPointChargeCode != null && value != _macroPointChargeCode.Id) _macroPointChargeCode = null;
			}
		}

		[EnableSnapShot("RatingNotice", Description = "Rating Notice Text")]
		[Property("RatingNotice", AutoValueOnInsert = false, DataType = SqlDbType.Text, Key = false)]
		public string RatingNotice { get; set; }

		[EnableSnapShot("BatchRatingAnalysisStartTime", Description = "Batch Rating Analysis Start Time")]
		[Property("BatchRatingAnalysisStartTime", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string BatchRatingAnalysisStartTime { get; set; }

		[EnableSnapShot("BatchRatingAnalysisEndTime", Description = "Batch Rating Analysis End Time")]
		[Property("BatchRatingAnalysisEndTime", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string BatchRatingAnalysisEndTime { get; set; }

		[EnableSnapShot("DefaultMileageEngine", Description = "Default System Mileage Engine Reference")]
		[Property("DefaultMileageEngine", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public MileageEngine DefaultMileageEngine { get; set; }

		[EnableSnapShot("TruckloadPickupTolerance", Description = "Truckload Pickup Tolerance Minutes")]
		[Property("TruckloadPickupTolerance", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int TruckloadPickupTolerance { get; set; }

		[EnableSnapShot("TruckloadDeliveryTolerance", Description = "Truckload Delivery Tolerance Minutes")]
		[Property("TruckloadDeliveryTolerance", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int TruckloadDeliveryTolerance { get; set; }

		[EnableSnapShot("PodDocumentTagId", Description = "POD Document Tag Reference")]
		[Property("PodDocumentTagId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long PodDocumentTagId
		{
			get { return _podDocumentTagId; }
			set
			{
				_podDocumentTagId = value;
				if (_podDocumentTag != null && value != _podDocumentTag.Id) _podDocumentTag = null;
			}
		}

		[EnableSnapShot("BolDocumentTagId", Description = "BOL Document Tag Reference")]
		[Property("BolDocumentTagId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long BolDocumentTagId
		{
			get { return _bolDocumentTagId; }
			set
			{
				_bolDocumentTagId = value;
				if (_bolDocumentTag != null && value != _bolDocumentTag.Id) _bolDocumentTag = null;
			}
		}

		[EnableSnapShot("WniDocumentTagId", Description = "WNI Document Tag Reference")]
		[Property("WniDocumentTagId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long WniDocumentTagId
		{
			get { return _wniDocumentTagId; }
			set
			{
				_wniDocumentTagId = value;
				if (_wniDocumentTag != null && value != _wniDocumentTag.Id) _wniDocumentTag = null;
			}
		}

		[EnableSnapShot("ShipmentDocImgRtrvAllowance", Description = "Shipment Document Image Retrieval Allowance Days")]
		[Property("ShipmentDocImgRtrvAllowance", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int ShipmentDocImgRtrvAllowance { get; set; }


        [EnableSnapShot("PaymentGatewayType", Description = "Payment Gateway Type")]
        [Property("PaymentGatewayType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public PaymentGatewayType PaymentGatewayType { get; set; }

        [EnableSnapShot("PaymentGatewayLoginId", Description = "Payment Gateway Login Id")]
        [Property("PaymentGatewayLoginId", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string PaymentGatewayLoginId { get; set; }

        [EnableSnapShot("PaymentGatewaySecret", Description = "Payment Gateway Secret")]
        [Property("PaymentGatewaySecret", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string PaymentGatewaySecret { get; set; }

        [EnableSnapShot("PaymentGatewayTransactionId", Description = "Payment Gateway Transaction Id")]
        [Property("PaymentGatewayTransactionId", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string PaymentGatewayTransactionId { get; set; }

		[EnableSnapShot("AutoNotificationSubjectPrefix", Description = "Auto Notification Subject Prefix")]
		[Property("AutoNotificationSubjectPrefix", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string AutoNotificationSubjectPrefix { get; set; }

        [EnableSnapShot("PaymentGatewayInTestMode", Description = "Payment Gateway Is In Test Mode")]
        [Property("PaymentGatewayInTestMode", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool PaymentGatewayInTestMode { get; set; }


	    public bool IsNew { get { return Id == default(long); } }

		public Country DefaultCountry
		{
			get { return _countryId == default(long) ? null : _country ?? (_country = new Country(_countryId)); }
			set
			{
				_country = value;
				_countryId = value == null ? default(long) : value.Id;
			}
		}
		public ContactType DefaultSchedulingContactType
		{
			get { return _defaultSchedulingContactTypeId == default(long) ? null : _defaultSchedulingContactType ?? (_defaultSchedulingContactType = new ContactType(_defaultSchedulingContactTypeId, false)); }
			set
			{
				_defaultSchedulingContactType = value;
				_defaultSchedulingContactTypeId = value == null ? default(long) : value.Id;
			}
		}
		public PackageType DefaultPackagingType
		{
			get{ return _defaultPackagingTypeId == default(long)? null : _defaultPackagingType ?? (_defaultPackagingType = new PackageType(_defaultPackagingTypeId, false)); }
			set
			{
				_defaultPackagingType = value;
				_defaultPackagingTypeId = value == null ? default(long) : value.Id;
			}
		}
		public ContactType AvailableLoadsContactType
		{
			get { return _availableLoadsContactTypeId == default(long) ? null : _availableLoadsContactType ?? (_availableLoadsContactType = new ContactType(_availableLoadsContactTypeId, false)); }
			set
			{
				_availableLoadsContactType = value;
				_availableLoadsContactTypeId = value == null ? default(long) : value.Id;
			}
		}
		public Service HazardousMaterialService
		{
			get { return _hazardousMaterialServiceId == default(long) ? null : _hazardousMaterialService ?? (_hazardousMaterialService = new Service(_hazardousMaterialServiceId, false)); }
			set
			{
				_hazardousMaterialService = value;
				_hazardousMaterialServiceId = value == null ? default(long) : value.Id;
			}
		}
		public Service BorderCrossingService
		{
			get { return _borderCrossingServiceId == default(long) ? null : _borderCrossingService ?? (_borderCrossingService = new Service(_borderCrossingServiceId, false)); }
			set
			{
				_borderCrossingService = value;
				_borderCrossingServiceId = value == null ? default(long) : value.Id;
			}
		}
		public User DefaultSystemUser
		{
			get
			{
				return _defaultSystemUserId == default(long)
				       	? null
				       	: _defaultSystemUser ?? (_defaultSystemUser = new User(_defaultSystemUserId, false));
			}
			set
			{
				_defaultSystemUser = value;
				_defaultSystemUserId = value == null ? default(long) : value.Id;
			}
		}
		public ChargeCode MacroPointChargeCode
		{
			get { return _macroPointChargeCodeId == default(long) ? null : _macroPointChargeCode ?? (_macroPointChargeCode = new ChargeCode(_macroPointChargeCodeId, false)); }
			set
			{
				_macroPointChargeCode = value;
				_macroPointChargeCodeId = value == null ? default(long) : value.Id;
			}
		}

		public DocumentTag PodDocumentTag
		{
			get { return _podDocumentTagId == default(long) ? null : _podDocumentTag ?? (_podDocumentTag = new DocumentTag(_podDocumentTagId, false)); }
			set
			{
				_podDocumentTag = value;
				_podDocumentTagId = value == null ? default(long) : value.Id;
			}
		}
		public DocumentTag BolDocumentTag
		{
			get { return _bolDocumentTagId == default(long) ? null : _bolDocumentTag ?? (_bolDocumentTag = new DocumentTag(_bolDocumentTagId, false)); }
			set
			{
				_bolDocumentTag = value;
				_bolDocumentTagId = value == null ? default(long) : value.Id;
			}
		}
		public DocumentTag WniDocumentTag
		{
			get { return _wniDocumentTagId == default(long) ? null : _wniDocumentTag ?? (_wniDocumentTag = new DocumentTag(_wniDocumentTagId, false)); }
			set
			{
				_wniDocumentTag = value;
				_wniDocumentTagId = value == null ? default(long) : value.Id;
			}
		}

		public List<AutoNumber> AutoNumbers
		{
			get
			{
				if (_autoNumbers == null) LoadAutoNumbers();
				return _autoNumbers;
			}
			set { _autoNumbers = value; }
		}

		public TenantLocation BillingLocation { get; set; }

		public TenantLocation MailingLocation { get; set; }

		public Tenant()
		{
			
		}

		public Tenant(long id) : this(id, false)
		{
			
		}

		public Tenant(long id, bool takeSnapShot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if(takeSnapShot)
			{
				TakeSnapShot();
				if (BillingLocation != null) BillingLocation.TakeSnapShot();
				if (MailingLocation != null) MailingLocation.TakeSnapShot();
			}
		}

		public Tenant(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}


		public void LoadCollections()
		{
			LoadAutoNumbers();
		}

		private void LoadAutoNumbers()
		{
			_autoNumbers = new List<AutoNumber>();
			const string query = "Select * from AutoNumber where TenantId = @TenantId";
			var parameters = new Dictionary<string, object> {{"TenantId", Id}};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_autoNumbers.Add(new AutoNumber(reader));
			Connection.Close();
		}
	}
}
