﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core
{
	[Serializable]
	[Entity("PermissionDetail", ReadOnly = false, Source = EntitySource.TableView)]
	public class PermissionDetail : TenantBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; set; }

		[EnableSnapShot("Code")]
		[Property("Code", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Code { get; set; }

		[EnableSnapShot("DisplayCode")]
		[Property("DisplayCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DisplayCode { get; set; }

		[EnableSnapShot("NavigationUrl")]
		[Property("NavigationUrl", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string NavigationUrl { get; set; }

		[EnableSnapShot("Category")]
		[Property("Category", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public PermissionCategory Category { get; set; }

        [EnableSnapShot("Type")]
		[Property("Type", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false, Column = "PermissionDetailType")]
		public PermissionType Type { get; set; }

		public bool IsNew { get { return Id == default(long); } }

		public PermissionDetail()
		{
			
		}

		public PermissionDetail(long id) : this(id, false)
		{
			
		}

		public PermissionDetail(long id, bool takeSnapShot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapShot) TakeSnapShot();
		}

		public PermissionDetail(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
