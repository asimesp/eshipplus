﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core
{
	[Serializable]
	[Entity("ProcQueObj", ReadOnly = false, Source = EntitySource.TableView)]
	public class ProcQueObj : ObjectBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[Property("ObjType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ProcQueObjType ObjType { get; set; }

		[Property("Xml", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Xml { get; set; }

		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public ProcQueObj()
		{		
		}

		public ProcQueObj(long id)
			: this(id, false)
		{

		}

		public ProcQueObj(long id, bool takeSnapShot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapShot) TakeSnapShot();
		}

		public ProcQueObj(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

	}
}
