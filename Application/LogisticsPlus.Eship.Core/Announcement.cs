﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core
{
	[Serializable]
	[Entity("Announcement", ReadOnly = false, Source = EntitySource.TableView)]
	public class Announcement : TenantBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("Enabled", Description = "Viewing Enabled")]
		[Property("Enabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Enabled { get; set; }

		[EnableSnapShot("Name")]
		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

		[EnableSnapShot("Type", Description = "Announcement Category")]
		[Property("Type", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public AnnouncementType Type { get; set; }

		[EnableSnapShot("Message")]
		[Property("Message", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Message { get; set; }

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public Announcement() { }

		public Announcement(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public Announcement(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			base.Delete();
		}

		public new void TakeSnapShot()
		{
			GetSnapShot();
		}
	}
}
