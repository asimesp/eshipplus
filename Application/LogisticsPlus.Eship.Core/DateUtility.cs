﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LogisticsPlus.Eship.Core
{
    public static class DateUtility
    {
    	private static readonly char[] Seperators = new[] {'+', '-'};
    	private const string Plus = "+";
    	private const string MonthAddition = "m";

    	public const string KeywordToday = "today";

        public static DateTime SystemEarliestDateTime
        {
            get { return new DateTime(1753, 1, 1, 0, 0, 0); }
        } 

        public static DateTime SystemLatestDateTime
        {
            get { return new DateTime(9999, 12, 31, 23, 59, 59); }
        }

        public static DateTime TimeToMinimum(this DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, 0, 0, 0);
        }

        public static DateTime TimeToMaximum(this DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, 23, 59, 59);
        }

        public static bool IsValidSystemDateTime(this DateTime value)
        {
            return SystemEarliestDateTime <= value && value <= SystemLatestDateTime;
        }

		public static bool IsValidSystemDateTime(this string value)
		{
			if (string.IsNullOrEmpty(value)) return false;

			if (value.ToArray().All(char.IsDigit))
				return SystemEarliestDateTime.ToOADate() <= value.ToDouble() &&
					   value.ToDouble() <= SystemLatestDateTime.ToOADate();

			DateTime x;
			return DateTime.TryParse(value, out x) && x.IsValidSystemDateTime();
		}
		
		public static bool IsKeywordDate(this string value)
		{
			return value.Trim().ToLower().StartsWith(KeywordToday);
		}

		public static bool IsSystemEarliestDate(this DateTime value)
		{
			return value == SystemEarliestDateTime;
		}

		public static bool IsSystemLatestDate(this DateTime value)
		{
			return value == SystemLatestDateTime;
		}

		public static string BuildKeywordDateString(this string dateKeyword, string timeString)
		{
			if (!dateKeyword.IsKeywordDate()) return string.Empty;
			if(!timeString.IsValidFreeFormTimeString()) return string.Empty;

			return string.Format("{0}+{1}", dateKeyword, timeString);
		}

		public static string GetKeywordDatePortion(this string value)
		{
			// assumption: format is always today+#+00:00 or today-#+00:00 or today+00:00 where # is an int, 00:00 is time string
			if(!value.IsKeywordDate()) return string.Empty;
			var split = value.Split(Seperators, StringSplitOptions.RemoveEmptyEntries);

			if (split.Length == 1)
				return value;

			if (split.Length == 2)
				return split[1].IsValidFreeFormTimeString() ? split[0] : value;

			if (split.Length == 3)
			{
				var idx = value.LastIndexOf(Plus, StringComparison.Ordinal);
				return idx >= 0 ? value.Substring(0, idx) : value;
			}

			return string.Empty;
		}

		public static string GetKeywordTimePortion(this string value)
		{
			if (!value.IsKeywordDate()) return string.Empty;
			var datePortion = value.GetKeywordDatePortion();
			var replace = string.IsNullOrEmpty(datePortion) ? string.Empty : value.Replace(datePortion, string.Empty);
			return string.IsNullOrEmpty(replace) ? TimeUtility.Default : replace.Replace(Plus, string.Empty);
		}

		public static DateTime ParseKeywordDate(this string keyword)
		{
			if (!keyword.IsKeywordDate()) return SystemEarliestDateTime;
			
			var datePortion = keyword.GetKeywordDatePortion();
			var timePorttion = keyword.GetKeywordTimePortion().Trim();

			var split = datePortion.ToLower().Split(Seperators, StringSplitOptions.RemoveEmptyEntries);
			var date = split.Length > 1 ? ParseDateAddition(split[1].Trim(), datePortion.Contains(Plus)) : DateTime.Today;

			return !string.IsNullOrEmpty(timePorttion) && timePorttion.IsValidTimeString() ? date.SetTime(timePorttion) : date;
		}

		private static DateTime ParseDateAddition(this string dateDelta, bool addDelta)
		{
			if(dateDelta.ToLower().Contains(MonthAddition))
			{
				dateDelta = dateDelta.Replace(MonthAddition, string.Empty).Replace(MonthAddition.ToUpper(), string.Empty);
				int month;
				return !int.TryParse(dateDelta, out month)
				       	? DateTime.Today
				       	: DateTime.Today.AddMonths(addDelta ? month : -1*month);
			}

			int days;
			return !int.TryParse(dateDelta, out days)
				? DateTime.Today
				: DateTime.Today.AddDays(addDelta ? days : -1 * days);
		}


		public static string FormattedShortDateSuppressEarliestDate(this object value)
		{
			var x = SystemEarliestDateTime;
			return (value == null ? x : DateTime.TryParse(value.ToString(), out x) ? x : SystemEarliestDateTime).FormattedShortDateSuppressEarliestDate();
		}

		public static string FormattedShortDateSuppressEarliestDate(this DateTime value)
		{
			return value == SystemEarliestDateTime ? string.Empty : value.FormattedShortDate();
		}

		public static string FormattedSuppressMidnightDate(this DateTime value)
		{
			return value.Hour == 0 && value.Minute == 0 && value.Second == 0 ? value.FormattedShortDate() : value.FormattedLongDate();
		}

    	public static string FormattedShortDate(this object value)
		{
			var x = SystemEarliestDateTime;
			return (value == null ? x : DateTime.TryParse(value.ToString(), out x) ? x : SystemEarliestDateTime).FormattedShortDate();
		}

		public static string FormattedShortDate(this DateTime date)
		{
			return string.Format("{0:MM/dd/yyyy}", date);
		}

		public static string FormattedShortDateAlt(this object value)
		{
			var x = SystemEarliestDateTime;
			return (value == null ? x : DateTime.TryParse(value.ToString(), out x) ? x : SystemEarliestDateTime).FormattedLongDateAlt();
		}

		public static string FormattedShortDateAlt(this DateTime date)
		{
			return string.Format("{0:yyyy-MM-dd}", date);
		}

		public static string FormattedLongDate(this object value)
		{
			var x = SystemEarliestDateTime;
			return (value == null ? x : DateTime.TryParse(value.ToString(), out x) ? x : SystemEarliestDateTime).FormattedLongDate();
		}

		public static string FormattedLongDate(this DateTime date)
		{
			return string.Format("{0:MM/dd/yyyy HH:mm:ss}", date);
		}

		public static string FormattedLongDateAlt(this object value)
		{
			var x = SystemEarliestDateTime;
			return (value == null ? x : DateTime.TryParse(value.ToString(), out x) ? x : SystemEarliestDateTime).FormattedLongDateAlt();
		}

		public static string FormattedLongDateAlt(this DateTime date)
		{
			return string.Format("{0:yyyy-MM-dd HH:mm:ss}", date);
		}

        public static string FormattedLongDateWithT(this DateTime dateTime)
        {
            return string.Format("{0:yyyy-MM-dd}T{0:HH:mm:ss}", dateTime);
        }

        public static string FormattedTimeOfDay(this DateTime date)
        {
            return string.Format("{0:HH:mm}", date);
        }

        public static string FormattedEasternTimeDate(this DateTime dateTime)
        {
            return string.Format("{0:yyyy-MM-dd}T{0:HH:mm}-05:00", dateTime);
        }

        

        public static DateTime FirstDateOfWeek(this DateTime date)
        {
            var returnDate = date.TimeToMinimum();
            while (returnDate.DayOfWeek != DayOfWeek.Sunday)
                returnDate = returnDate.AddDays(-1);
            return returnDate;
        }


        public static List<string> BuildMonthList()
        {
            var months = new List<int>();
            for (var i = 1; i < 13; i++) months.Add(i);
            return months.Select(m => m < 10 ? string.Format("0{0}", m) : m.ToString()).ToList();
        }

        public static List<string> BuildPresentAndFutureYearsList(int numberOfYearsIntoFuture)
        {
            var years = new List<int>();
            for (var i = DateTime.Now.Year; i < DateTime.Now.Year + numberOfYearsIntoFuture; i++) years.Add(i);
            return years.Select(y => y.ToString()).ToList();
        } 
    }
}
