﻿namespace LogisticsPlus.Eship.Core
{
	public enum AutoNumberCode
	{
		ShipmentNumber = 0,
		InvoiceNumber,
		CustomerNumber,
		CustomerLocationNumber,
		VendorNumber,
		VendorLocationNumber = 5,
		ClaimNumber,
		EdiTransactionSetControlNumber,
		ServiceTicketNumber,
		TierNumber,
		SalesRepresentativeNumber = 10,
		AssetNumber,
		JobNumber,
		VendorDisputeNumber,
	}
}
