﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Rating
{
	[Serializable]
	[Entity("LTLAdditionalCharge", ReadOnly = false, Source = EntitySource.TableView)]
	public class LTLAdditionalCharge : TenantBase
	{
		private long _chargeCodeId;
		private long _vendorRatingId;

		private VendorRating _vendorRating;
		private ChargeCode _chargeCode;

		private List<AdditionalChargeIndex> _additionalChargeIndices;	

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("Name")]
		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

		[EnableSnapShot("ChargeCodeId", Description = "Charge Code Reference")]
		[Property("ChargeCodeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ChargeCodeId
		{
			get { return _chargeCodeId; }
			set
			{
				_chargeCodeId = value;
				if (_chargeCode != null && value != _chargeCode.Id) _chargeCode = null;
			}
		}

		[EnableSnapShot("EffectiveDate", Description = "Effective Date")]
		[Property("EffectiveDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime EffectiveDate { get; set; }

		[EnableSnapShot("VendorRatingId", Description = "Vendor Rating Reference")]
		[Property("VendorRatingId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long VendorRatingId
		{
			get
			{
				if (_vendorRating != null) _vendorRatingId = _vendorRating.Id;
				return _vendorRatingId;
			}
			set
			{
				_vendorRatingId = value;
				if (_vendorRating != null && value != _vendorRating.Id) _vendorRating = null;
			}
		}

		public VendorRating VendorRating
		{
			get { return _vendorRating ?? (_vendorRating = new VendorRating(_vendorRatingId)); }
			set
			{
				_vendorRating = value;
				_vendorRatingId = value == null ? default(long) : value.Id;
			}
		}
		public ChargeCode ChargeCode
		{
			get { return _chargeCodeId == default(long) ? null : _chargeCode ?? (_chargeCode = new ChargeCode(_chargeCodeId, false)); }
			set
			{
				_chargeCode = value;
				_chargeCodeId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew { get { return Id == default(long); } }

		public List<AdditionalChargeIndex> AdditionalChargeIndices
		{
			get
			{
				if (_additionalChargeIndices == null) LoadIndices();
				return _additionalChargeIndices;
			}
			set { _additionalChargeIndices = value; }
		}

		public LTLAdditionalCharge()
		{
		}

		public LTLAdditionalCharge(long id) : this(id, false)
		{
			
		}

		public LTLAdditionalCharge(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public LTLAdditionalCharge(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

		public void LoadCollections()
		{
			LoadIndices();
		}

		private void LoadIndices()
		{
			_additionalChargeIndices = new List<AdditionalChargeIndex>();
			const string query =
				"Select * from AdditionalChargeIndex where LTLAdditionalChargeId = @LTLAdditionalChargeId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> {{"LTLAdditionalChargeId", Id}, {"TenantId", TenantId}};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_additionalChargeIndices.Add(new AdditionalChargeIndex(reader));
			Connection.Close();
		}
	}
}
