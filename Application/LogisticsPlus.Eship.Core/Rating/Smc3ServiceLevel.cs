﻿using System.Collections.Generic;
using System.Linq;

namespace LogisticsPlus.Eship.Core.Rating
{
	public static class Smc3ServiceLevel
	{
		//NOTE: do not re-arrange
		public static readonly List<string> ServiceLevels = new List<string>
			{
				"Standard-LTL",
				"GuaranteedAM",
				"GuaranteedPM",
				"Standard-TL",
				"Expedited",
				"Guaranteed",
				"Guaranteed5",
				"Accelerated",
				"Guar_12PM",
				"Guar_330PM",
				"Guar_9AM"

			};


		public static bool IsGuaranteedService(this string servicelevel)
		{
			return new List<string>
				{
					"GuaranteedAM",
					"GuaranteedPM",
					"Guaranteed",
					"Guaranteed5",
					"Guar_12PM",
					"Guar_330PM",
					"Guar_9AM",
				}.Any(i => i.ToLower() == servicelevel.ToLower());
		}
	}
}
