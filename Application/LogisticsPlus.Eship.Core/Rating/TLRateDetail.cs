﻿namespace LogisticsPlus.Eship.Core.Rating
{
    public class TLRateDetail
    {
        public string OriginFullAddress { get; set; }
        public string DestinationFullAddress { get; set; }
        public RateType RateType { get; set; }
        public decimal Rate { get; set; }
        public decimal Mileage { get; set; }
        public decimal TotalWeight { get; set; }
        public TLTariffType TariffType { get; set; }

        public TLRateDetail(TLRateDetail tlRateDetail)
        {
            OriginFullAddress = tlRateDetail.OriginFullAddress;
            DestinationFullAddress = tlRateDetail.DestinationFullAddress;
            RateType = tlRateDetail.RateType;
            Rate = tlRateDetail.Rate;
            Mileage = tlRateDetail.Mileage;
            TotalWeight = tlRateDetail.TotalWeight;
            TariffType = tlRateDetail.TariffType;
        }

        public TLRateDetail()
        {
        }
    }
}
