﻿namespace LogisticsPlus.Eship.Core.Rating
{
	public enum FuelIndexRegion
	{
		EastCoast = 0,
		NewEngland,
		CentralAtlantic,
		LowerAtlantic,
		Midwest,
		GulfCoast = 5,
		RockyMountain,
		WestCoast,
		California,
		National,
		WestCoastLessCalifornia = 10,
	}
}
