﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Rating
{
	[Serializable]
	[Entity("VendorRating", ReadOnly = false, Source = EntitySource.TableView)]
	public class VendorRating : TenantBase
	{
		private long _fuelChargeCodeId;
		private long _fuelTableId;
	    private long _ltlPackageSpecificRatePackageTypeId;
	    private long _ltlPackageSpecificFreightChargeCodeId;
		private long _vendorId;

		private Vendor _vendor;
		private FuelTable _fuelTable;
		private ChargeCode _fuelChargeCode;
	    private PackageType _ltlPackageSpecificRatePackageType;
	    private ChargeCode _ltlPackageSpecificFreightChargeCode;

		private List<DiscountTier> _discountTiers;
		private List<LTLAccessorial> _ltlAccessorials;
		private List<LTLAdditionalCharge> _ltlAdditionalCharges;
        private List<LTLCubicFootCapacityRule> _ltlCubicFootCapacityRules;
        private List<LTLGuaranteedCharge> _ltlGuaranteedCharges;
		private List<LTLOverLengthRule> _ltlOverLengthRules;
		private List<LTLPcfToFcConversion> _pcfToFcConvTab;
        private List<LTLPackageSpecificRate> _ltlPackageSpecificRates;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("Name")]
		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

		[EnableSnapShot("DisplayName", Description = "Display Name")]
		[Property("DisplayName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DisplayName { get; set; }

		[EnableSnapShot("Active", Description = "Active Status")]
		[Property("Active", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Active { get; set; }

	    [EnableSnapShot("Project44Profile", Description = "Project 44 Profile")]
		[Property("Project44Profile", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Project44Profile { get; set; }

	    [EnableSnapShot("Project44TradingPartnerCode", Description = "Project44 Trading Partner Code")]
	    [Property("Project44TradingPartnerCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
	    public string Project44TradingPartnerCode { get; set; }

        [EnableSnapShot("ExpirationDate", Description = "Expiration Date")]
		[Property("ExpirationDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime ExpirationDate { get; set; }

		[EnableSnapShot("FuelIndexRegion", Description = "Fuel Index Region")]
		[Property("FuelIndexRegion", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public FuelIndexRegion FuelIndexRegion { get; set; }

		[EnableSnapShot("CurrentLTLFuelMarkup", Description = "Current LTL Fuel Markup")]
		[Property("CurrentLTLFuelMarkup", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal CurrentLTLFuelMarkup { get; set; }

		[EnableSnapShot("FuelMarkupCeiling", Description = "Fuel Markup Ceiling")]
		[Property("FuelMarkupCeiling", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal FuelMarkupCeiling { get; set; }

		[EnableSnapShot("FuelMarkupFloor", Description = "Fuel Markup Floor")]
		[Property("FuelMarkupFloor", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal FuelMarkupFloor { get; set; }

		[EnableSnapShot("FuelUpdatesOn", Description = "Fuel Updates On")]
		[Property("FuelUpdatesOn", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public DayOfWeek FuelUpdatesOn { get; set; }

		[EnableSnapShot("FuelChargeCodeId", Description = "Fuel Charge Code Reference")]
		[Property("FuelChargeCodeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long FuelChargeCodeId
		{
			get { return _fuelChargeCodeId; }
			set
			{
				_fuelChargeCodeId = value;
				if (_fuelChargeCode != null && value != _fuelChargeCode.Id) _fuelChargeCode = null;
			}
		}

		[EnableSnapShot("HasAdditionalCharges", Description = "Has Additional Charges")]
		[Property("HasAdditionalCharges", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool HasAdditionalCharges { get; set; }

		[EnableSnapShot("LTLTariff", Description = "LTL Tariff")]
		[Property("LTLTariff", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LTLTariff { get; set; }

		[EnableSnapShot("Smc3ServiceLevel", Description = "Smc3 Service Level")]
		[Property("Smc3ServiceLevel", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Smc3ServiceLevel { get; set; }

		[EnableSnapShot("CubicCapacityPenaltyWidth", Description = "LTL Cubic Foot Capacity Penalty Width")]
		[Property("CubicCapacityPenaltyWidth", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal CubicCapacityPenaltyWidth { get; set; }

		[EnableSnapShot("CubicCapacityPenaltyHeight", Description = "LTL Cubic Foot Capacity Penalty Height")]
		[Property("CubicCapacityPenaltyHeight", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal CubicCapacityPenaltyHeight { get; set; }

		[EnableSnapShot("HasLTLCubicFootCapacityRules", Description = "Has LTL Cubic Foot Capacity Rules")]
		[Property("HasLTLCubicFootCapacityRules", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool HasLTLCubicFootCapacityRules { get; set; }

		[EnableSnapShot("LTLTruckLength", Description = "LTL Truck Maximum Length")]
		[Property("LTLTruckLength", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal LTLTruckLength { get; set; }

		[EnableSnapShot("LTLTruckWidth", Description = "LTL Truck Maximum Width")]
		[Property("LTLTruckWidth", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal LTLTruckWidth { get; set; }

		[EnableSnapShot("LTLTruckHeight", Description = "LTL Truck Maximum Height")]
		[Property("LTLTruckHeight", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal LTLTruckHeight { get; set; }

		[EnableSnapShot("LTLTruckWeight", Description = "LTL Truck Maximum Weight")]
		[Property("LTLTruckWeight", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal LTLTruckWeight { get; set; }

		[EnableSnapShot("LTLMinPickupWeight", Description = "LTL Minimum Pickup Weight")]
		[Property("LTLMinPickupWeight", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal LTLMinPickupWeight { get; set; }

		[EnableSnapShot("LTLTruckUnitWeight", Description = "LTL Truck Maximum Unit Weight")]
		[Property("LTLTruckUnitWeight", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal LTLTruckUnitWeight { get; set; }

		[EnableSnapShot("HasLTLOverLengthRules", Description = "Has Over Length Rules")]
		[Property("HasLTLOverLengthRules", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool HasLTLOverLengthRules { get; set; }

		[EnableSnapShot("OverrideAddress", Description = "Rating Profile Document Override Address")]
		[Property("OverrideAddress", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OverrideAddress { get; set; }

		[EnableSnapShot("FuelTableId", Description = "Fuel Table Reference")]
		[Property("FuelTableId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long FuelTableId
		{
			get { return _fuelTableId; }
			set
			{
				_fuelTableId = value;
				if (_fuelTable != null && _fuelTable.Id != value) _fuelTable = null;
			}
		}

		[EnableSnapShot("EnableLTLPcfToFcConversion", Description = "Enable LTL Pcf To Fc Conversion")]
		[Property("EnableLTLPcfToFcConversion", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool EnableLTLPcfToFcConversion { get; set; }

		[EnableSnapShot("LTLMaxCfForPcfConv", Description = "LTL Maximum Cubic Foot for Pcf Conversion")]
		[Property("LTLMaxCfForPcfConv", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal LTLMaxCfForPcfConv { get; set; }

        [EnableSnapShot("EnableLTLPackageSpecificRates", Description = "Enable Ltl Package Specific Rates")]
        [Property("EnableLTLPackageSpecificRates", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false, Column = "EnableLtlPackageSpecificRates")]
        public bool EnableLTLPackageSpecificRates { get; set; }

        [EnableSnapShot("LTLPackageSpecificRatePackageTypeId", Description = "Ltl Package Specific Rate Package Type Reference")]
        [Property("LTLPackageSpecificRatePackageTypeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false, Column = "LtlPackageSpecificRatePackageTypeId")]
        public long LTLPackageSpecificRatePackageTypeId
        {
            get { return _ltlPackageSpecificRatePackageTypeId; }
            set
            {
                _ltlPackageSpecificRatePackageTypeId = value;
                if (_ltlPackageSpecificRatePackageType != null && _ltlPackageSpecificRatePackageType.Id != value) _ltlPackageSpecificRatePackageType = null;
            }
        }

        [EnableSnapShot("LTLPackageSpecificFreightChargeCodeId", Description = "Package Specific Rate Freight Charge Code Reference")]
        [Property("LTLPackageSpecificFreightChargeCodeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long LTLPackageSpecificFreightChargeCodeId
        {
            get { return _ltlPackageSpecificFreightChargeCodeId; }
            set
            {
                _ltlPackageSpecificFreightChargeCodeId = value;
                if (_ltlPackageSpecificFreightChargeCode != null && value != _ltlPackageSpecificFreightChargeCode.Id) _ltlPackageSpecificFreightChargeCode = null;
            }
        }

		[EnableSnapShot("VendorId", Description = "Vendor Reference")]
		[Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long VendorId
		{
			get
			{
				if (_vendor != null) _vendorId = _vendor.Id;
				return _vendorId;
			}
			set
			{
				_vendorId = value;
				if (_vendor != null && value != _vendor.Id) _vendor = null;
			}
		}


        [EnableSnapShot("MaxPackageQuantityApplies", Description = "Max Package Quantity Applies")]
        [Property("MaxPackageQuantityApplies", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool MaxPackageQuantityApplies { get; set; }

        [EnableSnapShot("IndividualItemLimitsApply", Description = "Individual Item Limits Apply")]
        [Property("IndividualItemLimitsApply", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool IndividualItemLimitsApply { get; set; }

        [EnableSnapShot("MaxPackageQuantity", Description = "Max Package Quantity")]
        [Property("MaxPackageQuantity", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int MaxPackageQuantity { get; set; }

        [EnableSnapShot("MaxItemLength", Description = "Max Item Length")]
        [Property("MaxItemLength", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int MaxItemLength { get; set; }

        [EnableSnapShot("MaxItemWidth", Description = "Max Item Width")]
        [Property("MaxItemWidth", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int MaxItemWidth { get; set; }

        [EnableSnapShot("MaxItemHeight", Description = "Max Item Height")]
        [Property("MaxItemHeight", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int MaxItemHeight { get; set; }

        [EnableSnapShot("RatingDetailNotes", Description = "Rating Detail Notes")]
        [Property("RatingDetailNotes", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string RatingDetailNotes { get; set; }

		[EnableSnapShot("CriticalBOLNotes", Description = "Rating Critical Bill of Lading Notes")]
		[Property("CriticalBOLNotes", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CriticalBOLNotes { get; set; }


		public string RatingName { get { return string.IsNullOrEmpty(DisplayName) ? Name : DisplayName; } }
		
		public Vendor Vendor
		{
			get { return _vendor ?? (_vendor = new Vendor(_vendorId)); }
			set
			{
				_vendor = value;
				_vendorId = value == null ? default(long) : value.Id;
			}
		}
		public FuelTable FuelTable
		{
			get { return _fuelTableId == default(long) ? null : _fuelTable ?? (_fuelTable = new FuelTable(_fuelTableId)); }
			set
			{
				_fuelTable = value;
				_fuelTableId = value == null ? default(long) : value.Id;
			}
		}
		public ChargeCode FuelChargeCode
		{
			get { return _fuelChargeCodeId == default(long) ? null : _fuelChargeCode ?? (_fuelChargeCode = new ChargeCode(_fuelChargeCodeId, false)); }
			set
			{
				_fuelChargeCode = value;
				_fuelChargeCodeId = value == null ? default(long) : value.Id;
			}
		}
	    public PackageType LTLPackageSpecificRatePackageType
	    {
            get { return _ltlPackageSpecificRatePackageTypeId == default(long) ? null : _ltlPackageSpecificRatePackageType ?? (_ltlPackageSpecificRatePackageType = new PackageType(_ltlPackageSpecificRatePackageTypeId, false)); }
            set
            {
                _ltlPackageSpecificRatePackageType = value;
                _ltlPackageSpecificRatePackageTypeId = value == null ? default(long) : value.Id;
            }
	    }
        public ChargeCode LTLPackageSpecificFreightChargeCode
        {
            get { return _ltlPackageSpecificFreightChargeCodeId == default(long) ? null : _ltlPackageSpecificFreightChargeCode ?? (_ltlPackageSpecificFreightChargeCode = new ChargeCode(_ltlPackageSpecificFreightChargeCodeId, false)); }
            set
            {
                _ltlPackageSpecificFreightChargeCode = value;
                _ltlPackageSpecificFreightChargeCodeId = value == null ? default(long) : value.Id;
            }
        }

		public List<DiscountTier> LTLDiscountTiers
		{
			get
			{
				if (_discountTiers == null) LoadDiscountTiers();
				return _discountTiers;
			}
			set { _discountTiers = value; }
		}
		public List<LTLAccessorial> LTLAccessorials
		{
			get
			{
				if (_ltlAccessorials == null) LoadLTLAccessorials();
				return _ltlAccessorials;
			}
			set { _ltlAccessorials = value; }
		}
		public List<LTLAdditionalCharge> LTLAdditionalCharges
		{
			get
			{
				if (_ltlAdditionalCharges == null) LoadLTLAdditionalCharges();
				return _ltlAdditionalCharges;
			}
			set { _ltlAdditionalCharges = value; }
		}
		public List<LTLCubicFootCapacityRule> LTLCubicFootCapacityRules
		{
			get
			{
				if (_ltlCubicFootCapacityRules == null) LoadLTLCubicFootCapacityRules();
				return _ltlCubicFootCapacityRules;
			}
			set { _ltlCubicFootCapacityRules = value; }
		}
        public List<LTLGuaranteedCharge> LTLGuaranteedCharges
        {
            get
            {
                if (_ltlGuaranteedCharges == null) LoadLTLGuaranteedCharges();
                return _ltlGuaranteedCharges;
            }
            set { _ltlGuaranteedCharges = value; }
        }
		public List<LTLOverLengthRule> LTLOverLengthRules
		{
			get
			{
				if (_ltlOverLengthRules == null) LoadLTLOverLengthRules();
				return _ltlOverLengthRules;
			}
			set { _ltlOverLengthRules = value; }
		}
		public List<LTLPcfToFcConversion> LTLPcfToFcConvTab
		{
			get
			{
				if (_pcfToFcConvTab == null) LoadPcfToFcConvTab();
				return _pcfToFcConvTab;
			}
			set { _pcfToFcConvTab = value; }
		}
        public List<LTLPackageSpecificRate> LTLPackageSpecificRates
        {
            get
            {
                if (_ltlPackageSpecificRates == null) LoadLTLPackageSpecificRates();
                return _ltlPackageSpecificRates;
            }
            set { _ltlPackageSpecificRates = value; }
        }

		public bool IsNew { get { return Id == default(long); } }

		public VendorRating(){}

		public VendorRating(long id) : this(id, false)
		{
			
		}

		public VendorRating(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public VendorRating(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

		public void LoadCollections()
		{
			LoadPcfToFcConvTab();
			LoadDiscountTiers();
			LoadLTLAccessorials();
			LoadLTLAdditionalCharges();
			LoadLTLCubicFootCapacityRules();
            LoadLTLGuaranteedCharges();
			LoadLTLOverLengthRules();
            LoadLTLPackageSpecificRates();
		}

		private void LoadDiscountTiers()
		{
			_discountTiers = new List<DiscountTier>();
			const string query = "Select * from DiscountTier where VendorRatingId = @VendorRatingId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "VendorRatingId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_discountTiers.Add(new DiscountTier(reader));
			Connection.Close();
		}

		private void LoadLTLAccessorials()
		{
			_ltlAccessorials = new List<LTLAccessorial>();
			const string query = "Select * from LTLAccessorial where VendorRatingId = @VendorRatingId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "VendorRatingId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_ltlAccessorials.Add(new LTLAccessorial(reader));
			Connection.Close();
		}

		private void LoadLTLAdditionalCharges()
		{
			_ltlAdditionalCharges = new List<LTLAdditionalCharge>();
			const string query = "Select * from LTLAdditionalCharge where VendorRatingId = @VendorRatingId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "VendorRatingId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_ltlAdditionalCharges.Add(new LTLAdditionalCharge(reader));
			Connection.Close();
		}

		private void LoadLTLCubicFootCapacityRules()
		{
			_ltlCubicFootCapacityRules = new List<LTLCubicFootCapacityRule>();
			const string query = "Select * from LTLCubicFootCapacityRule where VendorRatingId = @VendorRatingId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "VendorRatingId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_ltlCubicFootCapacityRules.Add(new LTLCubicFootCapacityRule(reader));
			Connection.Close();
		}

        private void LoadLTLGuaranteedCharges()
        {
            _ltlGuaranteedCharges = new List<LTLGuaranteedCharge>();
            const string query = "Select * from LTLGuaranteedCharge where VendorRatingId = @VendorRatingId and TenantId = @TenantId";
            var parameters = new Dictionary<string, object> { { "VendorRatingId", Id }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _ltlGuaranteedCharges.Add(new LTLGuaranteedCharge(reader));
            Connection.Close();
        }

		private void LoadLTLOverLengthRules()
		{
			_ltlOverLengthRules = new List<LTLOverLengthRule>();
			const string query = "Select * from LTLOverLengthRule where VendorRatingId = @VendorRatingId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "VendorRatingId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_ltlOverLengthRules.Add(new LTLOverLengthRule(reader));
			Connection.Close();
		}

		private void LoadPcfToFcConvTab()
		{
			_pcfToFcConvTab = new List<LTLPcfToFcConversion>();
			const string query = "Select * from LTLPcfToFcConversion where VendorRatingId = @VendorRatingId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "VendorRatingId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_pcfToFcConvTab.Add(new LTLPcfToFcConversion(reader));
			Connection.Close();
		}

        private void LoadLTLPackageSpecificRates()
        {
            _ltlPackageSpecificRates = new List<LTLPackageSpecificRate>();
            const string query = "Select * from LTLPackageSpecificRate where VendorRatingId = @VendorRatingId and TenantId = @TenantId";
            var parameters = new Dictionary<string, object> { { "VendorRatingId", Id }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _ltlPackageSpecificRates.Add(new LTLPackageSpecificRate(reader));
            Connection.Close();
        }
	}
}
