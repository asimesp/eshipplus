﻿using System;
using System.Data;
using System.Data.Common;
using System.Linq;
using LogisticsPlus.Eship.Core.Calculations;
using LogisticsPlus.Eship.Core.Operations;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Rating
{
	[Serializable]
	[Entity("LTLPcfToFcConversion", ReadOnly = false, Source = EntitySource.TableView)]
	public class LTLPcfToFcConversion : TenantBase
	{
		public const int PcfLowerIndex = 0;
		public const int PcfUpperIndex = 1;
		public const int FcIndex = 2;

		public const int MaxRowLength = 3;

		private long _vendorRatingId;

		private VendorRating _vendorRating;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("EffectiveDate", Description = "Effective Date")]
		[Property("EffectiveDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime EffectiveDate { get; set; }

		[EnableSnapShot("VendorRatingId", Description = "Vendor Rating Reference")]
		[Property("VendorRatingId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long VendorRatingId
		{
			get
			{
				if (_vendorRating != null) _vendorRatingId = _vendorRating.Id;
				return _vendorRatingId;
			}
			set
			{
				_vendorRatingId = value;
				if (_vendorRating != null && value != _vendorRating.Id) _vendorRating = null;
			}
		}

		[EnableSnapShot("ConversionTableData", Description = "Conversion Table")]
		[Property("ConversionTableData", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false, Column = "ConversionTable")]
		public string ConversionTableData { get; set; }


		public VendorRating VendorRating
		{
			get { return _vendorRating ?? (_vendorRating = new VendorRating(_vendorRatingId)); }
			set
			{
				_vendorRating = value;
				_vendorRatingId = value == null ? default(long) : value.Id;
			}
		}

		public string[][] ConversionTable
		{
			get { return GetConversionTable(ConversionTableData); }
		}

		public bool IsNew { get { return Id == default(long); } }

		public LTLPcfToFcConversion()
		{
		}

		public LTLPcfToFcConversion(long id) : this(id, false)
		{
			
		}

		public LTLPcfToFcConversion(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public LTLPcfToFcConversion(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}


		public void SetItemActualFreightClass(ShipmentItem item)
		{
			var pcf = item.ActualPoundPerCubitFeet();
			var row = ConversionTable
				.Where(i => i.Length >= MaxRowLength)
				.FirstOrDefault(i => i[PcfLowerIndex].ToDecimal() <= pcf && pcf < i[PcfUpperIndex].ToDecimal());
			item.ActualFreightClass = row == null ? 0d : row[FcIndex].ToDouble();
		}


		public static string[][] GetConversionTable(string conversionTableData)
		{
			return string.IsNullOrEmpty(conversionTableData)
						? null
						: conversionTableData
							.Split(Environment.NewLine.ToArray(), StringSplitOptions.RemoveEmptyEntries)
							.Select(i => i.Split(CoreUtilities.DataSeperators()))
							.ToArray();
		}

		
	}
}
