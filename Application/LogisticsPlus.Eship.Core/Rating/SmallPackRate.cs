﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Rating
{
	[Serializable]
	[Entity("SmallPackRate", ReadOnly = false, Source = EntitySource.TableView)]
	public class SmallPackRate : TenantBase
	{
		private long _customerRatingId;
		private long _vendorId;
		private long _chargeCodeId;

		private ChargeCode _chargeCode;
		private Vendor _vendor;
		private CustomerRating _customerRating;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("SmallPackageEngine", Description = "Small Package Engine")]
		[Property("SmallPackageEngine", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public SmallPackageEngine SmallPackageEngine { get; set; }

		[EnableSnapShot("SmallPackType", Description = "Small Pack Type")]
		[Property("SmallPackType", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string SmallPackType { get; set; }

		[EnableSnapShot("MarkupPercent", Description = "Markup Percent")]
		[Property("MarkupPercent", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal MarkupPercent { get; set; }

		[EnableSnapShot("MarkupValue", Description = "Markup Value")]
		[Property("MarkupValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal MarkupValue { get; set; }

		[EnableSnapShot("UseMinimum", Description = "Use Minimum of Percent or Value")]
		[Property("UseMinimum", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool UseMinimum { get; set; }

		[EnableSnapShot("EffectiveDate", Description = "Effective Date")]
		[Property("EffectiveDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime EffectiveDate { get; set; }

		[EnableSnapShot("Active")]
		[Property("Active", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Active { get; set; }

		[EnableSnapShot("VendorId", Description = "Vendor Reference")]
		[Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long VendorId
		{
			get
			{
				if (_vendor != null) _vendorId = _vendor.Id;
				return _vendorId;
			}
			set
			{
				_vendorId = value;
				if (_vendor != null && value != _vendor.Id) _vendor = null;
			}
		}

		[EnableSnapShot("ChargeCodeId", Description = "Charge Code Reference")]
		[Property("ChargeCodeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ChargeCodeId
		{
			get { return _chargeCodeId; }
			set
			{
				_chargeCodeId = value;
				if (_chargeCode != null && value != _chargeCode.Id) _chargeCode = null;
			}
		}

		[EnableSnapShot("CustomerRatingId", Description = "Customer Rating Reference")]
		[Property("CustomerRatingId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long CustomerRatingId
		{
			get
			{
				if (_customerRating != null) _customerRatingId = _customerRating.Id;
				return _customerRatingId;
			}
			set
			{
				_customerRatingId = value;
				if (_customerRating != null && value != _customerRating.Id) _customerRating = null;
			}
		}

		public ChargeCode ChargeCode
		{
			get { return _chargeCodeId == default(long) ? null : _chargeCode ?? (_chargeCode = new ChargeCode(_chargeCodeId, false)); }
			set
			{
				_chargeCode = value;
				_chargeCodeId = value == null ? default(long) : value.Id;
			}
		}

		public CustomerRating CustomerRating
		{
			get { return _customerRating ?? (_customerRating = new CustomerRating(_customerRatingId)); }
			set
			{
				_customerRating = value;
				_customerRatingId = value == null ? default(long) : value.Id;
			}
		}
		public Vendor Vendor
		{
			get { return _vendor ?? (_vendor = new Vendor(_vendorId)); }
			set
			{
				_vendor = value;
				_vendorId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public SmallPackRate()
		{
			
		}

		public SmallPackRate(long id) : this(id, false)
		{
		}

		public SmallPackRate(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public SmallPackRate(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
