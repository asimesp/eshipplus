﻿using System;
using System.Data;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Rating
{
	[Serializable]
	public class RateMatrix : TenantBase
	{
		private long _chargeCodeId;
		private long _mileageSourceId;
		private long _originRegionId;
		private long _destinationRegionId;
		private long _originCountryId;
		private long _destinationCountryId;

		private Country _destinationCountry;
		private Country _originCountry;
		private ChargeCode _chargeCode;
		private Region _originRegion;
		private Region _destinationRegion;
		private MileageSource _mileageSource;

		[EnableSnapShot("OriginPostalCode", Description = "Origin Postal Code")]
		[Property("OriginPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginPostalCode { get; set; }

		[EnableSnapShot("OriginCountryId", Description = "Country Reference")]
		[Property("OriginCountryId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long OriginCountryId
		{
			get { return _originCountryId; }
			set
			{
				_originCountryId = value;
				if (_originCountry != null && value != _originCountry.Id) _originCountry = null;
			}
		}

		[EnableSnapShot("DestinationPostalCode", Description = "Destination Postal Code")]
		[Property("DestinationPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationPostalCode { get; set; }

		[EnableSnapShot("DestinationCountryId", Description = "Country Reference")]
		[Property("DestinationCountryId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long DestinationCountryId
		{
			get { return _destinationCountryId; }
			set
			{
				_destinationCountryId = value;
				if (_destinationCountry != null && value != _destinationCountry.Id) _destinationCountry = null;
			}
		}

		[EnableSnapShot("UsePostalCode", Description = "Use Postal Code")]
		[Property("UsePostalCode", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool UsePostalCode { get; set; }

		[EnableSnapShot("Active")]
		[Property("Active", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Active { get; set; }

		[EnableSnapShot("RateType", Description = "Rate Type")]
		[Property("RateType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public RateType RateType { get; set; }

		[EnableSnapShot("ChargeCodeId", Description = "Charge Code Reference")]
		[Property("ChargeCodeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ChargeCodeId
		{
			get { return _chargeCodeId; }
			set
			{
				_chargeCodeId = value;
				if (_chargeCode != null && value != _chargeCode.Id) _chargeCode = null;
			}
		}

		[EnableSnapShot("Mileage")]
		[Property("Mileage", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal Mileage { get; set; }

		[EnableSnapShot("MileageSourceId", Description = "Mileage Source Reference")]
		[Property("MileageSourceId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long MileageSourceId
		{
			get { return _mileageSourceId; }
			set
			{
				_mileageSourceId = value;
				if (_mileageSource != null && _mileageSource.Id != value) _mileageSource = null;
			}
		}

		[EnableSnapShot("OriginRegionId", Description = "Origin Region Reference")]
		[Property("OriginRegionId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long OriginRegionId
		{
			get { return _originRegionId; }
			set
			{
				_originRegionId = value;
				if (_originRegion != null && _originRegion.Id != value) _originRegion = null;
			}
		}

		[EnableSnapShot("DestinationRegionId", Description = "Destination Region Reference")]
		[Property("DestinationRegionId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long DestinationRegionId
		{
			get { return _destinationRegionId; }
			set
			{
				_destinationRegionId = value;
				if (_destinationRegion != null && _destinationRegion.Id != value) _destinationRegion = null;
			}
		}

		public Region OriginRegion
		{
			get { return _originRegionId == default(long) ? null : _originRegion ?? (_originRegion = new Region(_originRegionId, false)); }
			set
			{
				_originRegion = value;
				_originRegionId = value == null ? default(long) : value.Id;
			}
		}
		public Country OriginCountry
		{
			get { return _originCountryId == default(long) ? null : _originCountry ?? (_originCountry = new Country(_originCountryId)); }
			set
			{
				_originCountry = value;
				_originCountryId = value == null ? default(long) : value.Id;
			}
		}
		public Region DestinationRegion
		{
			get { return _destinationRegionId == default(long) ? null : _destinationRegion ?? (_destinationRegion = new Region(_destinationRegionId, false)); }
			set
			{
				_destinationRegion = value;
				_destinationRegionId = value == null ? default(long) : value.Id;
			}
		}
		public Country DestinationCountry
		{
			get { return _destinationCountryId == default(long) ? null : _destinationCountry ?? (_destinationCountry = new Country(_destinationCountryId)); }
			set
			{
				_destinationCountry = value;
				_destinationCountryId = value == null ? default(long) : value.Id;
			}
		}
		public ChargeCode ChargeCode
		{
			get { return _chargeCodeId == default(long) ? null : _chargeCode ?? (_chargeCode = new ChargeCode(_chargeCodeId, false)); }
			set
			{
				_chargeCode = value;
				_chargeCodeId = value == null ? default(long) : value.Id;
			}
		}
		public MileageSource MileageSource
		{
			get
			{
				return _mileageSourceId == default(long) ? null : _mileageSource ?? (_mileageSource = new MileageSource(_mileageSourceId, false));
			}
			set
			{
				_mileageSource = value;
				_mileageSourceId = value == null ? default(long) : value.Id;
			}
		}
	}
}
