﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Rating
{
	[Serializable]
	[Entity("LTLAccessorial", ReadOnly = false, Source = EntitySource.TableView)]
	public class LTLAccessorial : TenantBase
	{
		private long _serviceId;
		private long _vendorRatingId;

		private VendorRating _vendorRating;
		private Service _service;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("RateType", Description = "Rate Type")]
		[Property("RateType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public RateType RateType { get; set; }

		[EnableSnapShot("Rate")]
		[Property("Rate", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal Rate { get; set; }

		[EnableSnapShot("FloorValue", Description = "Floor Value")]
		[Property("FloorValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal FloorValue { get; set; }

		[EnableSnapShot("CeilingValue", Description = "Ceiling Value")]
		[Property("CeilingValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal CeilingValue { get; set; }

		[EnableSnapShot("EffectiveDate", Description = "Effective Date")]
		[Property("EffectiveDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime EffectiveDate { get; set; }

		[EnableSnapShot("ServiceId", Description = "Service Reference")]
		[Property("ServiceId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ServiceId
		{
			get { return _serviceId; }
			set
			{
				_serviceId = value;
				if (_service != null && value != _service.Id) _service = null;
			}
		}

		[EnableSnapShot("VendorRatingId", Description = "Vendor Rating Reference")]
		[Property("VendorRatingId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long VendorRatingId
		{
			get
			{
				if (_vendorRating != null) _vendorRatingId = _vendorRating.Id;
				return _vendorRatingId;
			}
			set
			{
				_vendorRatingId = value;
				if (_vendorRating != null && value != _vendorRating.Id) _vendorRating = null;
			}
		}

		public VendorRating VendorRating
		{
			get { return _vendorRating ?? (_vendorRating = new VendorRating(_vendorRatingId)); }
			set
			{
				_vendorRating = value;
				_vendorRatingId = value == null ? default(long) : value.Id;
			}
		}
		public Service Service
		{
			get { return _serviceId == default(long) ? null : _service ?? (_service = new Service(_serviceId, false)); }
			set
			{
				_service = value;
				_serviceId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew { get { return Id == default(long); } }

		public LTLAccessorial()
		{
		}

		public LTLAccessorial(long id) : this(id, false)
		{
			
		}

		public LTLAccessorial(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public LTLAccessorial(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
