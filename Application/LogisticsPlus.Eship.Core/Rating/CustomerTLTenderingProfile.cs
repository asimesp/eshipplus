﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Rating
{
	[Serializable]
	[Entity("CustomerTLTenderingProfile", ReadOnly = false, Source = EntitySource.TableView)]
	public class CustomerTLTenderingProfile : TenantBase
	{
		private long _customerId;

		private Customer _customer;

	    private List<CustomerTLTenderingProfileVendor> _vendors;
	    private List<CustomerTLTenderingProfileLane> _lanes;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("CustomerId", Description = "Customer Reference")]
		[Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long CustomerId
		{
			get { return _customerId; }
			set
			{
				_customerId = value;
				if (_customer != null && value != _customer.Id) _customer = null;
			}
		}

		[EnableSnapShot("MaxWaitTime", Description = "Max Wait Time")]
		[Property("MaxWaitTime", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int MaxWaitTime { get; set; }

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public Customer Customer
		{
			get { return _customerId == default(long) ? null : _customer ?? (_customer = new Customer(_customerId, false)); }
			set
			{
				_customer = value;
				_customerId = value == null ? default(long) : value.Id;
			}
		}

	    public List<CustomerTLTenderingProfileVendor> Vendors
	    {
	        get
	        {
	            if (_vendors == null) LoadVendors();
	            return _vendors;
	        }
	        set { _vendors = value; }
	    }

        public List<CustomerTLTenderingProfileLane> Lanes
        {
            get
            {
                if (_lanes == null) LoadLanes();
                return _lanes;
            }
            set { _lanes = value; }
        }

	

		public CustomerTLTenderingProfile()
		{
		}

		public CustomerTLTenderingProfile(long id) : this(id, false)
		{
		}

		public CustomerTLTenderingProfile(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public CustomerTLTenderingProfile(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

        public void LoadCollections()
        {
            LoadVendors();
            LoadLanes();
        }

        private void LoadVendors()
        {
            _vendors = new List<CustomerTLTenderingProfileVendor>();
            const string query = @"SELECT * FROM CustomerTLTenderingProfileVendor WHERE TLTenderingProfileId = @TLTenderingProfileId AND TenantId = @TenantId;";
            var parameters = new Dictionary<string, object> { { "TLTenderingProfileId", Id }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _vendors.Add(new CustomerTLTenderingProfileVendor(reader));
            Connection.Close();
        }

        private void LoadLanes()
        {
            _lanes = new List<CustomerTLTenderingProfileLane>();
            const string query = @"SELECT * FROM CustomerTLTenderingProfileLane WHERE TLTenderingProfileId = @TLTenderingProfileId AND TenantId = @TenantId;";
            var parameters = new Dictionary<string, object> { { "TLTenderingProfileId", Id }, { "TenantId", TenantId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                   _lanes.Add(new CustomerTLTenderingProfileLane(reader));
            Connection.Close();
        }


	}
}
	
