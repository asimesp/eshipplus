﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Rating
{
	[Serializable]
	[Entity("AdditionalChargeIndex", ReadOnly = false, Source = EntitySource.TableView)]
	public class AdditionalChargeIndex : TenantBase
	{
		private long _countryId;
		private long _ltlAdditionalChargeId;
		private long _regionId;

		private LTLAdditionalCharge _ltlAdditionalCharge;
		private Country _country;
		private Region _region;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("PostalCode", Description = "Postal Code")]
		[Property("PostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string PostalCode { get; set; }

		[EnableSnapShot("CountryId", Description = "Country Reference")]
		[Property("CountryId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long CountryId
		{
			get { return _countryId; }
			set
			{
				_countryId = value;
				if (_country != null && value != _country.Id) _country = null;
			}
		}

		[EnableSnapShot("UsePostalCode", Description = "Use Postal Code")]
		[Property("UsePostalCode", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool UsePostalCode { get; set; }

		[EnableSnapShot("ApplyOnOrigin", Description = "Applies to Origin Postal Code")]
		[Property("ApplyOnOrigin", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ApplyOnOrigin { get; set; }

		[EnableSnapShot("ApplyOnDestination", Description = "Applies to Destination Postal Code")]
		[Property("ApplyOnDestination", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ApplyOnDestination { get; set; }

		[EnableSnapShot("RateType", Description = "Rate Type")]
		[Property("RateType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public RateType RateType { get; set; }

		[EnableSnapShot("Rate")]
		[Property("Rate", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal Rate { get; set; }

		[EnableSnapShot("ChargeFloor", Description = "Floor Value")]
		[Property("ChargeFloor", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal ChargeFloor { get; set; }

		[EnableSnapShot("ChargeCeiling", Description = "Ceiling Value")]
		[Property("ChargeCeiling", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal ChargeCeiling { get; set; }

		[EnableSnapShot("LTLAdditionalChargeId", Description = "LTL Additional Charge Reference")]
		[Property("LTLAdditionalChargeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long LTLAdditionalChargeId
		{
			get
			{
				if (_ltlAdditionalCharge != null) _ltlAdditionalChargeId = _ltlAdditionalCharge.Id;
				return _ltlAdditionalChargeId;
			}
			set
			{
				_ltlAdditionalChargeId = value;
				if (_ltlAdditionalCharge != null && value != _ltlAdditionalCharge.Id) _ltlAdditionalCharge = null;
			}
		}

		[EnableSnapShot("RegionId", Description = "Region Reference")]
		[Property("RegionId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long RegionId
		{
			get { return _regionId; }
			set
			{
				_regionId = value;
				if (_region != null && _region.Id != value) _region = null;
			}
		}

		public Country Country
		{
			get { return _countryId == default(long) ? null : _country ?? (_country = new Country(_countryId)); }
			set
			{
				_country = value;
				_countryId = value == null ? default(long) : value.Id;
			}
		}
		public Region Region
		{
			get { return _regionId == default(long) ? null : _region ?? (_region = new Region(_regionId, false)); }
			set
			{
				_region = value;
				_regionId = value == null ? default(long) : value.Id;
			}
		}

		public LTLAdditionalCharge LTLAdditionalCharge
		{
			get { return _ltlAdditionalCharge ?? (_ltlAdditionalCharge = new LTLAdditionalCharge(_ltlAdditionalChargeId)); }
			set
			{
				_ltlAdditionalCharge = value;
				_ltlAdditionalChargeId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew { get { return Id == default(long); } }

		public AdditionalChargeIndex()
		{
		}

		public AdditionalChargeIndex(long id) : this(id, false)
		{
			
		}

		public AdditionalChargeIndex(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public AdditionalChargeIndex(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
