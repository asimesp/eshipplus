﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Cache;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Rating
{
	[Serializable]
	[Entity("CustomerRating", ReadOnly = false, Source = EntitySource.TableView)]
	public class CustomerRating : TenantBase
	{
		private long _insuranceChargeCodeId;
		private long _resellerAdditionId;
		private long _customerId;
		private long _tlFuelChargeCodeId;
		private long _tlFreightChargeCodeId;

		private Customer _customer;
		private ChargeCode _insuranceChargeCode;
		private ResellerAddition _resellerAddition;
		private ChargeCode _tlFuelChargeCode;
		private ChargeCode _tlFreightChargeCode;

		private List<LTLSellRate> _ltlSellRates;
		private List<TLSellRate> _tlSellRates;
		private List<SmallPackRate> _smallPackRates;
		private List<CustomerServiceMarkup> _customerServiceMarkups;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("InsuranceEnabled", Description = "Insurance Enabled")]
		[Property("InsuranceEnabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool InsuranceEnabled { get; set; }

		[EnableSnapShot("InsurancePurchaseFloor", Description = "Insurance Purchase Floor")]
		[Property("InsurancePurchaseFloor", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal InsurancePurchaseFloor { get; set; }

		[EnableSnapShot("InsuranceChargeCodeId", Description = "Insurance Charge Code Reference")]
		[Property("InsuranceChargeCodeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long InsuranceChargeCodeId
		{
			get { return _insuranceChargeCodeId; }
			set
			{
				_insuranceChargeCodeId = value;
				if (_insuranceChargeCode != null && value != _insuranceChargeCode.Id) _insuranceChargeCode = null;
			}
		}

		[EnableSnapShot("NoLineHaulProfit", Description = "Pass through Line Haul")]
		[Property("NoLineHaulProfit", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool NoLineHaulProfit { get; set; }

		[EnableSnapShot("LineHaulProfitCeilingType", Description = "Line Haul Profit Ceiling Type")]
		[Property("LineHaulProfitCeilingType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ValuePercentageType LineHaulProfitCeilingType { get; set; }

		[EnableSnapShot("LineHaulProfitCeilingValue", Description = "Line Haul Profit Ceiling Value")]
		[Property("LineHaulProfitCeilingValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal LineHaulProfitCeilingValue { get; set; }

		[EnableSnapShot("LineHaulProfitCeilingPercentage", Description = "Line Haul Profit Ceiling Percentage")]
		[Property("LineHaulProfitCeilingPercentage", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal LineHaulProfitCeilingPercentage { get; set; }

		[EnableSnapShot("UseLineHaulMinimumCeiling", Description = "Use Line Haul Minimum Ceiling")]
		[Property("UseLineHaulMinimumCeiling", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool UseLineHaulMinimumCeiling { get; set; }

		[EnableSnapShot("LineHaulProfitFloorType", Description = "Line Haul Profit Floor Type")]
		[Property("LineHaulProfitFloorType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ValuePercentageType LineHaulProfitFloorType { get; set; }

		[EnableSnapShot("LineHaulProfitFloorValue", Description = "Line Haul Profit Floor Value")]
		[Property("LineHaulProfitFloorValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal LineHaulProfitFloorValue { get; set; }

		[EnableSnapShot("LineHaulProfitFloorPercentage", Description = "Line Haul Profit Floor Percentage")]
		[Property("LineHaulProfitFloorPercentage", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal LineHaulProfitFloorPercentage { get; set; }

		[EnableSnapShot("UseLineHaulMinimumFloor", Description = "Use Line Haul Minimum Floor")]
		[Property("UseLineHaulMinimumFloor", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool UseLineHaulMinimumFloor { get; set; }

		[EnableSnapShot("NoFuelProfit", Description = "Pass through Fuel")]
		[Property("NoFuelProfit", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool NoFuelProfit { get; set; }

		[EnableSnapShot("FuelProfitCeilingType", Description = "Fuel Profit Ceiling Type")]
		[Property("FuelProfitCeilingType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ValuePercentageType FuelProfitCeilingType { get; set; }

		[EnableSnapShot("FuelProfitCeilingValue", Description = "Fuel Profit Ceiling Value")]
		[Property("FuelProfitCeilingValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal FuelProfitCeilingValue { get; set; }

		[EnableSnapShot("FuelProfitCeilingPercentage", Description = "Fuel Profit Ceiling Percentage")]
		[Property("FuelProfitCeilingPercentage", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal FuelProfitCeilingPercentage { get; set; }

		[EnableSnapShot("UseFuelMinimumCeiling", Description = "Use Fuel Minimum Ceiling")]
		[Property("UseFuelMinimumCeiling", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool UseFuelMinimumCeiling { get; set; }

		[EnableSnapShot("FuelProfitFloorType", Description = "Fuel Profit Floor Type")]
		[Property("FuelProfitFloorType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ValuePercentageType FuelProfitFloorType { get; set; }

		[EnableSnapShot("FuelProfitFloorValue", Description = "Fuel Profit Floor Value")]
		[Property("FuelProfitFloorValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal FuelProfitFloorValue { get; set; }

		[EnableSnapShot("FuelProfitFloorPercentage", Description = "Fuel Profit Floor Percentage")]
		[Property("FuelProfitFloorPercentage", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal FuelProfitFloorPercentage { get; set; }

		[EnableSnapShot("UseFuelMinimumFloor", Description = "Use Fuel Minimum Floor")]
		[Property("UseFuelMinimumFloor", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool UseFuelMinimumFloor { get; set; }
		
		[EnableSnapShot("NoAccessorialProfit", Description = "Pass through Accessorial")]
		[Property("NoAccessorialProfit", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool NoAccessorialProfit { get; set; }

		[EnableSnapShot("AccessorialProfitCeilingType", Description = "Accessorial Profit Ceiling Type")]
		[Property("AccessorialProfitCeilingType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ValuePercentageType AccessorialProfitCeilingType { get; set; }

		[EnableSnapShot("AccessorialProfitCeilingValue", Description = "Accessorial Profit Ceiling Value")]
		[Property("AccessorialProfitCeilingValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal AccessorialProfitCeilingValue { get; set; }

		[EnableSnapShot("AccessorialProfitCeilingPercentage", Description = "Accessorial Profit Ceiling Percentage")]
		[Property("AccessorialProfitCeilingPercentage", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal AccessorialProfitCeilingPercentage { get; set; }

		[EnableSnapShot("UseAccessorialMinimumCeiling", Description = "Use Accessorial Minimum Ceiling")]
		[Property("UseAccessorialMinimumCeiling", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool UseAccessorialMinimumCeiling { get; set; }

		[EnableSnapShot("AccessorialProfitFloorType", Description = "Accessorial Profit Floor Type")]
		[Property("AccessorialProfitFloorType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ValuePercentageType AccessorialProfitFloorType { get; set; }

		[EnableSnapShot("AccessorialProfitFloorValue", Description = "Accessorial Profit Floor Value")]
		[Property("AccessorialProfitFloorValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal AccessorialProfitFloorValue { get; set; }

		[EnableSnapShot("AccessorialProfitFloorPercentage", Description = "Accessorial Profit Floor Percentage")]
		[Property("AccessorialProfitFloorPercentage", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal AccessorialProfitFloorPercentage { get; set; }

		[EnableSnapShot("UseAccessorialMinimumFloor", Description = "Use Accessorial Minimum Floor")]
		[Property("UseAccessorialMinimumFloor", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool UseAccessorialMinimumFloor { get; set; }
		
		[EnableSnapShot("NoServiceProfit", Description = "Pass through Service")]
		[Property("NoServiceProfit", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool NoServiceProfit { get; set; }

		[EnableSnapShot("ServiceProfitCeilingType", Description = "Service Profit Ceiling Type")]
		[Property("ServiceProfitCeilingType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ValuePercentageType ServiceProfitCeilingType { get; set; }

		[EnableSnapShot("ServiceProfitCeilingValue", Description = "Service Profit Ceiling Value")]
		[Property("ServiceProfitCeilingValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal ServiceProfitCeilingValue { get; set; }

		[EnableSnapShot("ServiceProfitCeilingPercentage", Description = "Service Profit Ceiling Percentage")]
		[Property("ServiceProfitCeilingPercentage", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal ServiceProfitCeilingPercentage { get; set; }

		[EnableSnapShot("UseServiceMinimumCeiling", Description = "Use Service Minimum Ceiling")]
		[Property("UseServiceMinimumCeiling", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool UseServiceMinimumCeiling { get; set; }

		[EnableSnapShot("ServiceProfitFloorType", Description = "Service Profit Floor Type")]
		[Property("ServiceProfitFloorType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ValuePercentageType ServiceProfitFloorType { get; set; }

		[EnableSnapShot("ServiceProfitFloorValue", Description = "Service Profit Floor Value")]
		[Property("ServiceProfitFloorValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal ServiceProfitFloorValue { get; set; }

		[EnableSnapShot("ServiceProfitFloorPercentage", Description = "Service Profit Floor Percentage")]
		[Property("ServiceProfitFloorPercentage", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal ServiceProfitFloorPercentage { get; set; }

		[EnableSnapShot("UseServiceMinimumFloor", Description = "Use Service Minimum Floor")]
		[Property("UseServiceMinimumFloor", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool UseServiceMinimumFloor { get; set; }

		[EnableSnapShot("BillReseller", Description = "Reseller Receives Bill")]
		[Property("BillReseller", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool BillReseller { get; set; }

		[EnableSnapShot("ResellerAdditionId", Description = "Reseller Addition Reference")]
		[Property("ResellerAdditionId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ResellerAdditionId
		{
			get { return _resellerAdditionId; }
			set
			{
				_resellerAdditionId = value;
				if (_resellerAddition != null && value != _resellerAddition.Id) _resellerAddition = null;
			}
		}

		[EnableSnapShot("TLFuelRateType", Description = "TL Fuel Rate Type")]
		[Property("TLFuelRateType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public RateType TLFuelRateType { get; set; }

		[EnableSnapShot("TLFuelRate", Description = "TL Fuel Rate")]
		[Property("TLFuelRate", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal TLFuelRate { get; set; }

	    [EnableSnapShot("Project44AccountGroup", Description = "Project 44 Account Group")]
        [Property("Project44AccountGroup", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Project44AccountGroup { get; set; }

		[EnableSnapShot("TLFuelChargeCodeId", Description = "TL Fuel Charge Code Reference")]
		[Property("TLFuelChargeCodeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long TLFuelChargeCodeId
		{
			get { return _tlFuelChargeCodeId; }
			set
			{
				_tlFuelChargeCodeId = value;
				if (_tlFuelChargeCode != null && value != _tlFuelChargeCode.Id) _tlFuelChargeCode = null;
			}
		}

		[EnableSnapShot("TLFreightChargeCodeId", Description = "TL Freight Charge Code Reference")]
		[Property("TLFreightChargeCodeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long TLFreightChargeCodeId
		{
			get { return _tlFreightChargeCodeId; }
			set
			{
				_tlFreightChargeCodeId = value;
				if (_tlFreightChargeCode != null && value != _tlFreightChargeCode.Id) _tlFreightChargeCode = null;
			}
		}

		[EnableSnapShot("CustomerId", Description = "Customer Reference")]
		[Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long CustomerId
		{
			get
			{
				if (_customer != null) _customerId = _customer.Id;
				return _customerId;
			}
			set
			{
				_customerId = value;
				if (_customer != null && value != _customer.Id) _customer = null;
			}
		}

		public ResellerAddition ResellerAddition
		{
			get { return _resellerAdditionId == default(long) ? null : _resellerAddition ?? (_resellerAddition = new ResellerAddition(_resellerAdditionId, false)); }
			set
			{
				_resellerAddition = value;
				_resellerAdditionId = value == null ? default(long) : value.Id;
			}
		}
		public Customer Customer
		{
			get { return _customer ?? (_customer = new Customer(_customerId)); }
			set
			{
				_customer = value;
				_customerId = value == null ? default(long) : value.Id;
			}
		}
		public ChargeCode InsuranceChargeCode
		{
			get { return _insuranceChargeCodeId == default(long) ? null : _insuranceChargeCode ?? (_insuranceChargeCode = new ChargeCode(_insuranceChargeCodeId, false)); }
			set
			{
				_insuranceChargeCode = value;
				_insuranceChargeCodeId = value == null ? default(long) : value.Id;
			}
		}
		public ChargeCode TLFuelChargeCode
		{
			get { return _tlFuelChargeCodeId == default(long) ? null : _tlFuelChargeCode ?? (_tlFuelChargeCode = new ChargeCode(_tlFuelChargeCodeId, false)); }
			set
			{
				_tlFuelChargeCode = value;
				_tlFuelChargeCodeId = value == null ? default(long) : value.Id;
			}
		}
		public ChargeCode TLFreightChargeCode
		{
			get { return _tlFreightChargeCodeId == default(long) ? null : _tlFreightChargeCode ?? (_tlFreightChargeCode = new ChargeCode(_tlFreightChargeCodeId, false)); }
			set
			{
				_tlFreightChargeCode = value;
				_tlFreightChargeCodeId = value == null ? default(long) : value.Id;
			}
		}

		public List<LTLSellRate> LTLSellRates
		{
			get
			{
				if (_ltlSellRates == null) LoadLTLSellRates();
				return _ltlSellRates;
			}
			set { _ltlSellRates = value; }
		}
		public List<TLSellRate> TLSellRates
		{
			get
			{
				if (_tlSellRates == null) LoadTLSellRates();
				return _tlSellRates;
			}
			set { _tlSellRates = value; }
		}
		public List<SmallPackRate> SmallPackRates
		{
			get
			{
				if (_smallPackRates == null) LoadSmallPackRates();
				return _smallPackRates;
			}
			set { _smallPackRates = value; }
		}
		public List<CustomerServiceMarkup> CustomerServiceMarkups
		{
			get
			{
				if (_customerServiceMarkups == null) LoadCustomerServiceMarkups();
				return _customerServiceMarkups;
			}
			set { _customerServiceMarkups = value; }
		}

		public bool IsNew { get { return Id == default(long); } }

		public CustomerRating()
		{
			
		}

		public CustomerRating(long id) : this(id, false)
		{
		}

		public CustomerRating(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			if (CoreCache.CustomerRatings.ContainsKey(Id))
			{
				Copy(CoreCache.CustomerRatings[Id]);
				LTLSellRates = CoreCache.CustomerRatings[Id].LTLSellRates.Copy();
				SmallPackRates = CoreCache.CustomerRatings[Id].SmallPackRates.Copy();
				CustomerServiceMarkups = CoreCache.CustomerRatings[Id].CustomerServiceMarkups.Copy();
				KeyLoaded = true;
			}
			else
			{
				KeyLoaded = Load();
				if (KeyLoaded)
				{
					var r = new CustomerRating();
					r.Copy(this);				
					CoreCache.UpdateCache(r);
				}
			}
			if (takeSnapshot) TakeSnapShot();
		}

		public CustomerRating(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (IsNew) return;
			base.Delete();		
		}

		public void LoadCollections()
		{
			LoadLTLSellRates();
			LoadTLSellRates();
			LoadSmallPackRates();
			LoadCustomerServiceMarkups();
		}

		private void LoadLTLSellRates()
		{
			_ltlSellRates = new List<LTLSellRate>();
			const string query = "select * from LTLSellRate where CustomerRatingId = @CustomerRatingId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> {{"CustomerRatingId", Id}, {"TenantId", TenantId}};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_ltlSellRates.Add(new LTLSellRate(reader));
			Connection.Close();
		}

		private void LoadTLSellRates()
		{
			_tlSellRates = new List<TLSellRate>();
			const string query = "select * from TLSellRate where CustomerRatingId = @CustomerRatingId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "CustomerRatingId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_tlSellRates.Add(new TLSellRate(reader));
			Connection.Close();
		}

		private void LoadSmallPackRates()
		{
			_smallPackRates = new List<SmallPackRate>();
			const string query = "select * from SmallPackRate where CustomerRatingId = @CustomerRatingId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "CustomerRatingId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_smallPackRates.Add(new SmallPackRate(reader));
			Connection.Close();
		}

		private void LoadCustomerServiceMarkups()
		{
			_customerServiceMarkups = new List<CustomerServiceMarkup>();
			const string query = "select * from CustomerServiceMarkup where CustomerRatingId = @CustomerRatingId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "CustomerRatingId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_customerServiceMarkups.Add(new CustomerServiceMarkup(reader));
			Connection.Close();
		}
	}
}
