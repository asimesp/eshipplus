﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Core.Rating
{
	[Serializable]
    public class VendorRateAnalysisInput
    {
        public long TenantId { get; set; }

        public List<Vendor> Vendors { get; set; }

        public List<ServiceMode> ServiceModes { get; set; }

        public string Origin { get; set; }

        public string Destination { get; set; }

        public double ActualFreightClass { get; set; }

        public decimal Length { get; set; }
        public decimal Width { get; set; }
        public decimal Height { get; set; }
        public decimal Weight { get; set; }
        
        public string OriginPostalCode
        {
            get
            {
                var originData = Origin.Split(',');
                return originData[0];
            }
        }

        public string OriginCountryKey
        {
            get
            {
                var originData = Origin.Split(',');
                return originData[1];
            }
        }

        public string DestinationPostalCode
        {
            get
            {
                var destinationData = Destination.Split(',');
                return destinationData[0];
            }
        }

        public string DestinationCountryKey
        {
            get
            {
                var destinationData = Destination.Split(',');
                return destinationData[1];
            }
        }

       
    }
}
