﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Rating
{
	[Serializable]
    [Entity("BatchRateData", ReadOnly = false, Source = EntitySource.TableView)]
	public class BatchRateData : TenantBase
	{
		private long _submittedByUserId;
		private long _chargeCodeId;
		private long _vendorRatingId;
		private long _packageTypeId;

		private PackageType _packageType;
		private VendorRating _vendorRating;
		private ChargeCode _chargeCode;
		private User _submittedByUser;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

		[Property("GroupCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string GroupCode { get; set; }

		[Property("LaneData", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LaneData { get; set; }

		[Property("AnalysisEffectiveDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime AnalysisEffectiveDate { get; set; }

		[Property("ResultData", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ResultData { get; set; }

		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[Property("DateCompleted", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCompleted { get; set; }

		[Property("ServiceMode", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ServiceMode ServiceMode { get; set; }

		[Property("LTLIndirectPointEnabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool LTLIndirectPointEnabled { get; set; }

		[Property("SmallPackageEngine", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public SmallPackageEngine SmallPackageEngine { get; set; }

		[Property("SmallPackageEngineType", AutoValueOnInsert = false, DataType = SqlDbType.VarChar, Key = false)]
		public string SmallPackageEngineType { get; set; }

		[Property("ChargeCodeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ChargeCodeId
		{
			get { return _chargeCodeId; }
			set
			{
				_chargeCodeId = value;
				if (_chargeCode != null && value != _chargeCode.Id) _chargeCode = null;
			}
		}

		[Property("VendorRatingId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long VendorRatingId
		{
			get
			{
				if (_vendorRating != null) _vendorRatingId = _vendorRating.Id;
				return _vendorRatingId;
			}
			set
			{
				_vendorRatingId = value;
				if (_vendorRating != null && value != _vendorRating.Id) _vendorRating = null;
			}
		}

		[Property("SubmittedByUserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long SubmittedByUserId
		{
			get
			{
				if (_submittedByUser != null) _submittedByUserId = _submittedByUser.Id;
				return _submittedByUserId;
			}
			set
			{
				_submittedByUserId = value;
				if (_submittedByUser != null && value != _submittedByUser.Id) _submittedByUser = null;
			}
		}

		[EnableSnapShot("PackageTypeId", Description = "Package Type Reference")]
		[Property("PackageTypeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long PackageTypeId
		{
			get { return _packageTypeId; }
			set
			{
				_packageTypeId = value;
				if (_packageType != null && value != _packageType.Id) _packageType = null;
			}
		}

		public PackageType PackageType
		{
			get { return _packageTypeId == default(long) ? null : _packageType ?? (_packageType = new PackageType(_packageTypeId)); }
			set
			{
				_packageType = value;
				_packageTypeId = value == null ? default(long) : value.Id;
			}
		}
		public VendorRating VendorRating
		{
			get { return _vendorRatingId == default(long) ? null : _vendorRating ?? (_vendorRating = new VendorRating(_vendorRatingId, false)); }
			set
			{
				_vendorRating = value;
				_vendorRatingId = value == null ? default(long) : value.Id;
			}
		}
		public ChargeCode ChargeCode
		{
			get { return _chargeCodeId == default(long) ? null : _chargeCode ?? (_chargeCode = new ChargeCode(_chargeCodeId, false)); }
			set
			{
				_chargeCode = value;
				_chargeCodeId = value == null ? default(long) : value.Id;
			}
		}
		public User SubmittedByUser
		{
			get { return _submittedByUser ?? (_submittedByUser = new User(_submittedByUserId)); }
			set
			{
				_submittedByUser = value;
				_submittedByUserId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public BatchRateData()
		{

		}

		public BatchRateData(long id)
			: this(id, false)
		{
		}

		public BatchRateData(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public BatchRateData(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

	}
}
