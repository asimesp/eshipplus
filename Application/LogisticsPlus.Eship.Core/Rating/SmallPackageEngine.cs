﻿namespace LogisticsPlus.Eship.Core.Rating
{
	public enum SmallPackageEngine
	{
        // DO NOT RE-ORDER. SEE APPLICABLE JOINS ON DATABASE REFSMALLPACKAGEENGINE
        None = 0,
		FedEx,
		Ups
	}
}
