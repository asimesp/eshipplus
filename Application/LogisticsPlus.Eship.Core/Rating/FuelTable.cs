﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Rating
{
	[Serializable]
	[Entity("FuelTable", ReadOnly = false, Source = EntitySource.TableView)]
	public class FuelTable : TenantBase
	{
		private List<FuelIndex> _fuelIndices;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("Name")]
		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

		public List<FuelIndex> FuelIndices
		{
			get
			{
				if (_fuelIndices == null) LoadIndices();
				return _fuelIndices;
			}
			set { _fuelIndices = value; }
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public FuelTable()
		{
			
		}

		public FuelTable(long id) : this(id, false)
		{
		}

		public FuelTable(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public FuelTable(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

		public void LoadCollections()
		{
			LoadIndices();
		}

		private void LoadIndices()
		{
			_fuelIndices = new List<FuelIndex>();
			const string query = "select * from FuelIndex where FuelTableId = @FuelTableId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> {{"TenantId", TenantId}, {"FuelTableId", Id}};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_fuelIndices.Add(new FuelIndex(reader));
			Connection.Close();
		}
	}
}
