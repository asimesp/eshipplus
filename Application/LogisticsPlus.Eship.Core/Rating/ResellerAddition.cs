﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Rating
{
	[Serializable]
	[Entity("ResellerAddition", ReadOnly = false, Source = EntitySource.TableView)]
	public class ResellerAddition : TenantBase
	{
		private long _resellerCustomerAccountId;

		private Customer _resellerCustomerAccount;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("Name")]
		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

		[EnableSnapShot("LineHaulType", Description = "Line Haul Type")]
		[Property("LineHaulType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ValuePercentageType LineHaulType { get; set; }

		[EnableSnapShot("LineHaulValue", Description = "Line Haul Value")]
		[Property("LineHaulValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal LineHaulValue { get; set; }

		[EnableSnapShot("LineHaulPercentage", Description = "Line Haul Percentage")]
		[Property("LineHaulPercentage", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal LineHaulPercentage { get; set; }

		[EnableSnapShot("UseLineHaulMinimum", Description = "Use Line Haul Minimum ")]
		[Property("UseLineHaulMinimum", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool UseLineHaulMinimum { get; set; }

		[EnableSnapShot("FuelType", Description = "Fuel Type")]
		[Property("FuelType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ValuePercentageType FuelType { get; set; }

		[EnableSnapShot("FuelValue", Description = "Fuel Value")]
		[Property("FuelValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal FuelValue { get; set; }

		[EnableSnapShot("FuelPercentage", Description = "Fuel Percentage")]
		[Property("FuelPercentage", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal FuelPercentage { get; set; }

		[EnableSnapShot("UseFuelMinimum", Description = "Use Fuel Minimum ")]
		[Property("UseFuelMinimum", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool UseFuelMinimum { get; set; }

		[EnableSnapShot("AccessorialType", Description = "Accessorial Type")]
		[Property("AccessorialType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ValuePercentageType AccessorialType { get; set; }

		[EnableSnapShot("AccessorialValue", Description = "Accessorial Value")]
		[Property("AccessorialValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal AccessorialValue { get; set; }

		[EnableSnapShot("AccessorialPercentage", Description = "Accessorial Percentage")]
		[Property("AccessorialPercentage", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal AccessorialPercentage { get; set; }

		[EnableSnapShot("UseAccessorialMinimum", Description = "Use Accessorial Minimum ")]
		[Property("UseAccessorialMinimum", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool UseAccessorialMinimum { get; set; }

		[EnableSnapShot("ServiceType", Description = "Service Type")]
		[Property("ServiceType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ValuePercentageType ServiceType { get; set; }

		[EnableSnapShot("ServiceValue", Description = "Service Value")]
		[Property("ServiceValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal ServiceValue { get; set; }

		[EnableSnapShot("ServicePercentage", Description = "Service Percentage")]
		[Property("ServicePercentage", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal ServicePercentage { get; set; }

		[EnableSnapShot("UseServiceMinimum", Description = "Use Service Minimum ")]
		[Property("UseServiceMinimum", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool UseServiceMinimum { get; set; }

		[EnableSnapShot("ResellerCustomerAccountId", Description = "Reseller Customer Account Reference")]
		[Property("ResellerCustomerAccountId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ResellerCustomerAccountId
		{
			get
			{
				if (_resellerCustomerAccount != null) _resellerCustomerAccountId = _resellerCustomerAccount.Id;
				return _resellerCustomerAccountId;
			}
			set
			{
				_resellerCustomerAccountId = value;
				if (_resellerCustomerAccount != null && value != _resellerCustomerAccount.Id) _resellerCustomerAccount = null;
			}
		}

		public Customer ResellerCustomerAccount
		{
			get { return _resellerCustomerAccount ?? (_resellerCustomerAccount = new Customer(_resellerCustomerAccountId)); }
			set
			{
				_resellerCustomerAccount = value;
				_resellerCustomerAccountId = value == null ? default(long) : value.Id;
			}
		}


		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public ResellerAddition()
		{
		}

		public ResellerAddition(long id)
			: this(id, false)
		{
		}

		public ResellerAddition(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public ResellerAddition(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long)) base.Delete();
		}
	}
}
