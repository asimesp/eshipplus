﻿namespace LogisticsPlus.Eship.Core.Rating
{
	public enum WeightBreak
	{
        // DO NOT REORDER. 
		Ignore = 0,
		L5C,
		M5C,
		M1M,
		M2M,
		M5M = 5,
		M10M,
		M20M,
		M30M,
		M40M,
	}
}
