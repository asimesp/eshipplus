﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Rating
{
	[Serializable]
	[Entity("Area", ReadOnly = false, Source = EntitySource.TableView)]
	public class Area : TenantBase
	{
		private long _subRegionId;
		private long _countryId;
		private long _regionId;

		private Region _region;
		private Country _country;
		private Region _subRegion;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("PostalCode", Description = "Postal Code")]
		[Property("PostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string PostalCode { get; set; }

		[EnableSnapShot("CountryId", Description = "Country Reference")]
		[Property("CountryId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long CountryId
		{
			get { return _countryId; }
			set
			{
				_countryId = value;
				if (_country != null && value != _country.Id) _country = null;
			}
		}

		[EnableSnapShot("UseSubRegion", Description = "Area is Region")]
		[Property("UseSubRegion", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool UseSubRegion { get; set; }

		[EnableSnapShot("SubRegionId", Description = "Sub Region Reference")]
		[Property("SubRegionId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long SubRegionId
		{
			get { return _subRegionId; }
			set
			{
				_subRegionId = value;
				if (_subRegion != null && value != _subRegion.Id) _subRegion = null;
			}
		}

		[EnableSnapShot("RegionId", Description = "Region Reference")]
		[Property("RegionId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long RegionId
		{
			get
			{
				if (_region != null) _regionId = _region.Id;
				return _regionId;
			}
			set
			{
				_regionId = value;
				if (_region != null && value != _region.Id) _region = null;
			}
		}

		public Country Country
		{
			get { return _countryId == default(long) ? null : _country ?? (_country = new Country(_countryId)); }
			set
			{
				_country = value;
				_countryId = value == null ? default(long) : value.Id;
			}
		}
		public Region SubRegion
		{
			get { return _subRegionId == default(long) ? null : _subRegion ?? (_subRegion = new Region(_subRegionId, false)); }
			set
			{
				_subRegion = value;
				_subRegionId = value == null ? default(long) : value.Id;
			}
		}
		public Region Region
		{
			get { return _region ?? (_region = new Region(_regionId)); }
			set
			{
				_region = value;
				_regionId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public Area() { }

		public Area(long id) : this(id, false) { }

		public Area(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public Area(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
