﻿using System;
using LogisticsPlus.Eship.Core.Accounting;

namespace LogisticsPlus.Eship.Core.Rating
{
    [Serializable]
    public class RateCharge : Charge
    {
        private long _ltlGuaranteedChargeId;

        private LTLGuaranteedCharge _ltlGuaranteedCharge;

        public long LTLGuaranteedChargeId
        {
            get { return _ltlGuaranteedChargeId; }
            set
            {
                _ltlGuaranteedChargeId = value;
                if (_ltlGuaranteedCharge != null && value != _ltlGuaranteedCharge.Id) _ltlGuaranteedCharge = null;
            }
        }

        public LTLGuaranteedCharge LTLGuaranteedCharge
        {
            get { return _ltlGuaranteedChargeId == default(long) ? null : _ltlGuaranteedCharge ?? (_ltlGuaranteedCharge = new LTLGuaranteedCharge(_ltlGuaranteedChargeId, false)); }
            set
            {
                _ltlGuaranteedCharge = value;
                _ltlGuaranteedChargeId = value == null ? default(long) : value.Id;
            }
        }

        public bool IsProject44GuaranteedCharge { get; set; }

        public bool IsProject44ExpeditedServiceCharge { get; set; }

        public string Project44QuoteNumber { get; set; }

        public RateCharge()
        {
            
        }

        public RateCharge(Charge c)
        {
            Quantity = c.Quantity;
            UnitBuy = c.UnitBuy;
            UnitSell = c.UnitSell;
            UnitDiscount = c.UnitDiscount;
            Comment = c.Comment;
            ChargeCodeId = c.ChargeCodeId;
            TenantId = c.TenantId;
        }
    }
}
