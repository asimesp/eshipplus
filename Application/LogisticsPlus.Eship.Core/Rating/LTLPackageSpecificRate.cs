﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Rating
{
    [Serializable]
    [Entity("LTLPackageSpecificRate", ReadOnly = false, Source = EntitySource.TableView)]
    public class LTLPackageSpecificRate : TenantBase
    {
        private long _vendorRatingId;
        private long _originRegionId;
        private long _destinationRegionId;

        private VendorRating _vendorRating;
        private Region _originRegion;
        private Region _destinationRegion;


        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; protected set; }

        [EnableSnapShot("VendorRatingId", Description = "Vendor Rating Reference")]
        [Property("VendorRatingId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long VendorRatingId
        {
            get
            {
                if (_vendorRating != null) _vendorRatingId = _vendorRating.Id;
                return _vendorRatingId;
            }
            set
            {
                _vendorRatingId = value;
                if (_vendorRating != null && value != _vendorRating.Id) _vendorRating = null;
            }
        }

        [EnableSnapShot("EffectiveDate", Description = "Effective Date")]
        [Property("EffectiveDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime EffectiveDate { get; set; }

        [EnableSnapShot("OriginRegionId", Description = "Origin Region Reference")]
        [Property("OriginRegionId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long OriginRegionId
        {
            get { return _originRegionId; }
            set
            {
                _originRegionId = value;
                if (_originRegion != null && value != _originRegion.Id) _originRegion = null;
            }
        }

        [EnableSnapShot("DestinationRegionId", Description = "Destination Region Reference")]
        [Property("DestinationRegionId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long DestinationRegionId
        {
            get { return _destinationRegionId; }
            set
            {
                _destinationRegionId = value;
                if (_destinationRegion != null && value != _destinationRegion.Id) _destinationRegion = null;
            }
        }

        [EnableSnapShot("PackageQuantity", Description = "Package Quantity")]
        [Property("PackageQuantity", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public int PackageQuantity { get; set; }

        [EnableSnapShot("Rate", Description = "Rate")]
        [Property("Rate", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal Rate { get; set; }

		[EnableSnapShot("RatePriority", Description = "Rate Priority")]
		[Property("RatePriority", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int RatePriority { get; set; }


        public VendorRating VendorRating
        {
            get { return _vendorRating ?? (_vendorRating = new VendorRating(_vendorRatingId)); }
            set
            {
                _vendorRating = value;
                _vendorRatingId = value == null ? default(long) : value.Id;
            }
        }
        public Region OriginRegion
        {
            get { return _originRegionId == default(long) ? null : _originRegion ?? (_originRegion = new Region(_originRegionId, false)); }
            set
            {
                _originRegion = value;
                _originRegionId = value == null ? default(long) : value.Id;
            }
        }
        public Region DestinationRegion
        {
            get { return _destinationRegionId == default(long) ? null : _destinationRegion ?? (_destinationRegion = new Region(_destinationRegionId, false)); }
            set
            {
                _destinationRegion = value;
                _destinationRegionId = value == null ? default(long) : value.Id;
            }
        }


        public bool IsNew { get { return Id == default(long); } }

        public LTLPackageSpecificRate()
        {
        }

        public LTLPackageSpecificRate(long id)
            : this(id, false)
        {

        }

        public LTLPackageSpecificRate(long id, bool takeSnapshot)
        {
            Id = id;
            if (Id == default(long)) return;
            KeyLoaded = Load();
            if (takeSnapshot) TakeSnapShot();
        }

        public LTLPackageSpecificRate(DbDataReader reader)
        {
            Load(reader);
        }

        public void Save()
        {
            if (Id == default(long)) Insert();
            else Update();
        }

        public new void Delete()
        {
            if (Id != default(long))
                base.Delete();
        }

    }
}
