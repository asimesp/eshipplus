﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Cache;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Rating
{
	[Serializable]
    [Entity("SmallPackagingMap", ReadOnly = false, Source = EntitySource.TableView)]
    public class SmallPackagingMap : TenantBase
    {
        private long _packageTypeId;

        private PackageType _packageType;

        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; private set; }

        [EnableSnapShot("SmallPackageEngine", Description = "Small Package Engine")]
        [Property("SmallPackageEngine", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
        public SmallPackageEngine SmallPackageEngine { get; set; }

        [EnableSnapShot("PackageTypeId", Description = "Package Type Reference")]
        [Property("PackageTypeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long PackageTypeId
        {
            get { return _packageTypeId; }
            set
            {
                _packageTypeId = value;
                if (_packageType != null && value != _packageType.Id) _packageType = null;
            }
        }

        [EnableSnapShot("SmallPackEngineType", Description = "Small Pack Engine Package Type")]
        [Property("SmallPackEngineType", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string SmallPackEngineType { get; set; }

        [EnableSnapShot("RequiredLength", Description = "Required Length")]
        [Property("RequiredLength", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal RequiredLength { get; set; }

        [EnableSnapShot("RequiredWidth", Description = "Required Width")]
        [Property("RequiredWidth", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal RequiredWidth { get; set; }

        [EnableSnapShot("RequiredHeight", Description = "Required Height")]
        [Property("RequiredHeight", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal RequiredHeight { get; set; }

        public PackageType PackageType
        {
            get { return _packageTypeId == default(long) ? null : _packageType ?? (_packageType = new PackageType(_packageTypeId)); }
            set
            {
                _packageType = value;
                _packageTypeId = value == null ? default(long) : value.Id;
            }
        }

    	public SmallPackagingMap() { }

        public SmallPackagingMap(long id, bool takeSnapshot)
        {
            Id = id;
            if (Id == default(long)) return;
			if (CoreCache.SmallPackagingMaps.ContainsKey(Id))
			{
				Copy(CoreCache.SmallPackagingMaps[Id]);
				KeyLoaded = true;
			}
			else KeyLoaded = Load();
            if (takeSnapshot) TakeSnapShot();
        }

        public SmallPackagingMap(DbDataReader reader)
        {
            Load(reader);
        }

        public void Save()
        {
            if (Id == default(long)) Insert();
            else Update();
        }

        public new void Delete()
        {
            if (Id != default(long)) base.Delete();
        }

        public new void TakeSnapShot()
        {
            GetSnapShot();
        }

        public bool IsNew
        {
            get { return Id == default(long); }
        }

    }
}
