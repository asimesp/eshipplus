﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Cache;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Rating
{
	[Serializable]
	[Entity("SmallPackageServiceMap", ReadOnly = false, Source = EntitySource.TableView)]
	public class SmallPackageServiceMap : TenantBase
	{
		private long _serviceId;

		private Service _service;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("SmallPackageEngine", Description = "Small Package Engine")]
		[Property("SmallPackageEngine", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public SmallPackageEngine SmallPackageEngine { get; set; }

		[EnableSnapShot("ServiceId", Description = "Service Reference")]
		[Property("ServiceId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ServiceId
		{
			get { return _serviceId; }
			set
			{
				_serviceId = value;
				if (_service != null && value != _service.Id) _service = null;
			}
		}

		[EnableSnapShot("SmallPackEngineService", Description = "Small Pack Engine Service")]
		[Property("SmallPackEngineService", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string SmallPackEngineService { get; set; }

		public Service Service
		{
			get { return _serviceId == default(long) ? null : _service ?? (_service = new Service(_serviceId,false)); }
			set
			{
				_service = value;
				_serviceId = value == null ? default(long) : value.Id;
			}
		}

		public SmallPackageServiceMap() { }

        public SmallPackageServiceMap(long id, bool takeSnapshot)
        {
            Id = id;
            if (Id == default(long)) return;
			if (CoreCache.SmallPackageServiceMaps.ContainsKey(Id))
			{
				Copy(CoreCache.SmallPackageServiceMaps[Id]);
				KeyLoaded = true;
			}
			else KeyLoaded = Load();
            if (takeSnapshot) TakeSnapShot();
        }

        public SmallPackageServiceMap(DbDataReader reader)
        {
            Load(reader);
        }

        public void Save()
        {
            if (Id == default(long)) Insert();
            else Update();
        }

        public new void Delete()
        {
            if (Id != default(long)) base.Delete();
        }

        public new void TakeSnapShot()
        {
            GetSnapShot();
        }

        public bool IsNew
        {
            get { return Id == default(long); }
        }
	}
}
