﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.Core.Rating
{
	[Serializable]
	public class Rate
	{
		public const string ContractRate = "[Contract]";
		public const int DefaultTransitTime = -1;

        public bool IsProject44Rate { get; set; }
        public string Project44QuoteNumber { get; set; }
		public ServiceMode Mode { get; set; }
		public Vendor Vendor { get; set; }
		public LTLSellRate LTLSellRate { get; set; }
		public DiscountTier DiscountTier { get; set; }
		public LTLCubicFootCapacityRule CFCRule { get; set; }
		public string OriginTerminalCode { get; set; }
		public string DestinationTerminalCode { get; set; }
		public string SmallPackServiceType { get; set; }
		public SmallPackageEngine SmallPackEngine { get; set; }
		public List<RateCharge> Charges { get; set; }
		public decimal OriginalRateValue { get; set; }
		public bool DirectPointRate { get; set; }
		public bool ApplyDiscount { get; set; }
		public bool IsLTLPackageSpecificRate { get; set; }
		public int TransitDays { get; set; }
		public DateTime EstimatedDeliveryDate { get; set; }
		public decimal BilledWeight { get; set; }	
		public string ServiceLevel { get; set; }

        public decimal TotalActualShipmentWeight { get; set; }
        public decimal MarkupPercent { get; set; }
        public decimal MarkupValue { get; set; }

		public decimal RatedCubicFeet { get; set; }
		public decimal RatedWeight { get; set; }
		public decimal RatedPcf { get { return RatedCubicFeet == 0 ? 0 : RatedWeight/RatedCubicFeet; } }

		public decimal FuelMarkupPercent { get; set; }

		public decimal Mileage { get; set; }
		public long MileageSourceId { get; set; }

		public decimal FreightProfitAdjustment { get; set; }
		public decimal FuelProfitAdjustment { get; set; }
		public decimal AccessorialProfitAdjustment { get; set; }
		public decimal ServiceProfitAdjustment { get; set; }

		public bool NoFreightProfit { get; set; }
		public bool NoFuelProfit { get; set; }
		public bool NoAccessorialProfit { get; set; }
		public bool NoServiceProfit { get; set; }

		public decimal ResellerFreightMarkup { get; set; }
		public bool ResellerFreightMarkupIsPercent { get; set; }
		public decimal ResellerFuelMarkup { get; set; }
		public bool ResellerFuelMarkupIsPercent { get; set; }
		public decimal ResellerAccessorialMarkup { get; set; }
		public bool ResellerAccessorialMarkupIsPercent { get; set; }
		public decimal ResellerServiceMarkup { get; set; }
		public bool ResellerServiceMarkupIsPercent { get; set; }

		public long DiscountTierId { get { return DiscountTier == null ? default(long) : DiscountTier.Id; } }
		public long LTLSellRateId { get { return LTLSellRate == null ? default(long) : LTLSellRate.Id; } }
		public long CFCRuleId { get { return CFCRule == null ? default(long) : CFCRule.Id; } }

        public bool OverlengthRuleApplied { get; set; }

        public List<TLRateDetail> TLRateDetails { get; set; }

		public bool IsLTLDensityRate
		{
			get
			{
				return
					DiscountTier != null &&
					Mode == ServiceMode.LessThanTruckload &&
					(DiscountTier.RateBasis == DiscountTierRateBasis.Density ||
					 (DiscountTier.VendorRating != null && DiscountTier.VendorRating.EnableLTLPcfToFcConversion));
			}
		}

		public bool IsValid
		{
			get
			{
				return Mode == ServiceMode.LessThanTruckload || Mode == ServiceMode.SmallPackage
				       	? TransitDays != DefaultTransitTime && Charges.Count > 0
				       	: Charges.Count > 0;
			}
		}


		public bool IsGuaranteed
		{
			get
			{
			    return GuaranteedCharges.Any()
			           || (LTLSellRate != null && LTLSellRate.VendorRating != null &&
			               LTLSellRate.VendorRating.Smc3ServiceLevel.IsGuaranteedService() && !IsProject44Rate);
			}
		}

	    public bool IsExpedited
	    {
	        get { return ExpeditedCharges.Any(); }
	    }

        public List<RateCharge> BaseRateCharges
		{
			get
			{
				return Charges == null
					       ? new List<RateCharge>()
					       : Charges.Where(c => c.LTLGuaranteedChargeId == default(long) && !c.IsProject44GuaranteedCharge && !c.IsProject44ExpeditedServiceCharge).ToList();
			}
		}

		public List<RateCharge> GuaranteedCharges
		{
			get
			{
				return Charges == null
					       ? new List<RateCharge>()
					       : Charges.Where(c => c.LTLGuaranteedChargeId != default(long) || c.IsProject44GuaranteedCharge).ToList();
			}
		}

	    public List<RateCharge> ExpeditedCharges
	    {
	        get
	        {
	            return Charges == null
	                ? new List<RateCharge>()
	                : Charges.Where(c => c.IsProject44ExpeditedServiceCharge).ToList();
	        }
	    }

        public Rate()
		{
		    IsProject44Rate = false;
		    Project44QuoteNumber = string.Empty;
			Mode = ServiceMode.LessThanTruckload;
			Vendor = null;
			LTLSellRate = null;
			DiscountTier = null;
			Charges = new List<RateCharge>();
			OriginalRateValue = 0;
			DirectPointRate = true;
			ApplyDiscount = true;
			IsLTLPackageSpecificRate = false;
			TransitDays = DefaultTransitTime;
			EstimatedDeliveryDate = DateUtility.SystemEarliestDateTime;
			BilledWeight = 0;
			RatedWeight = 0;
			RatedCubicFeet = 0;

			FreightProfitAdjustment = 0;
			FuelProfitAdjustment = 0;
			AccessorialProfitAdjustment = 0;
			ServiceProfitAdjustment = 0;

			SmallPackServiceType = string.Empty;

            TLRateDetails = new List<TLRateDetail>();
		}
	}
}
