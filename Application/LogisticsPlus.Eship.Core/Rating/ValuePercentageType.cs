﻿namespace LogisticsPlus.Eship.Core.Rating
{
    // DO NOT RE-ORDER. SEE APPLICABLE JOINS ON DATABASE REFVALUEPERCENTAGETYPE
    public enum ValuePercentageType
	{
		Value = 0,
		Percentage,
		Both
	}
}
