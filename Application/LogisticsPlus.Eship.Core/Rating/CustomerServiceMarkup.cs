﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Rating
{
	[Serializable]
	[Entity("CustomerServiceMarkup", ReadOnly = false, Source = EntitySource.TableView)]
	public class CustomerServiceMarkup : TenantBase
	{
		private long _serviceId;
		private long _customerRatingId;

		private CustomerRating _customerRating;
		private Service _service;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("MarkupPercent", Description = "Markup Percent")]
		[Property("MarkupPercent", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal MarkupPercent { get; set; }

		[EnableSnapShot("MarkupValue", Description = "Markup Value")]
		[Property("MarkupValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal MarkupValue { get; set; }

		[EnableSnapShot("UseMinimum", Description = "Use Minimum of Percent or Value")]
		[Property("UseMinimum", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool UseMinimum { get; set; }

		[EnableSnapShot("EffectiveDate", Description = "Effective Date")]
		[Property("EffectiveDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime EffectiveDate { get; set; }

		[EnableSnapShot("Active")]
		[Property("Active", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Active { get; set; }

		[EnableSnapShot("ServiceChargeCeiling", Description = "Service Charge Ceiling")]
		[Property("ServiceChargeCeiling", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal ServiceChargeCeiling { get; set; }

		[EnableSnapShot("ServiceChargeFloor", Description = "Service Charge Floor")]
		[Property("ServiceChargeFloor", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal ServiceChargeFloor { get; set; }

		[EnableSnapShot("ServiceId", Description = "Service Reference")]
		[Property("ServiceId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ServiceId
		{
			get { return _serviceId; }
			set
			{
				_serviceId = value;
				if (_service != null && value != _service.Id) _service = null;
			}
		}

		[EnableSnapShot("CustomerRatingId", Description = "Customer Rating Reference")]
		[Property("CustomerRatingId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long CustomerRatingId
		{
			get
			{
				if (_customerRating != null) _customerRatingId = _customerRating.Id;
				return _customerRatingId;
			}
			set
			{
				_customerRatingId = value;
				if (_customerRating != null && value != _customerRating.Id) _customerRating = null;
			}
		}

		public CustomerRating CustomerRating
		{
			get { return _customerRating ?? (_customerRating = new CustomerRating(_customerRatingId)); }
			set
			{
				_customerRating = value;
				_customerRatingId = value == null ? default(long) : value.Id;
			}
		}
		public Service Service
		{
			get { return _serviceId == default(long) ? null : _service ?? (_service = new Service(_serviceId, false)); }
			set
			{
				_service = value;
				_serviceId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public CustomerServiceMarkup()
		{
			
		}

		public CustomerServiceMarkup(long id) : this(id, false)
		{
		}

		public CustomerServiceMarkup(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public CustomerServiceMarkup(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
