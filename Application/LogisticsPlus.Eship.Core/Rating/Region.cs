﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Rating
{
	[Serializable]
	[Entity("Region", ReadOnly = false, Source = EntitySource.TableView)]
	public class Region : TenantBase
	{
		private List<Area> _areas;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("Name")]
		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

		public List<Area> Areas
		{
			get
			{
				if (_areas == null) LoadAreas();
				return _areas;
			}
			set { _areas = value; }
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public Region() {}

		public Region(long id) : this(id, false)
		{
		}

		public Region(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public Region(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

		public void LoadCollections()
		{
			LoadAreas();
		}

		private void LoadAreas()
		{
			_areas = new List<Area>();
			const string query = "SELECT * FROM Area WHERE TenantId = @TenantId AND RegionId = @RegionId";
			var parameters = new Dictionary<string, object> { { "TenantId", TenantId }, { "RegionId", Id } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_areas.Add(new Area(reader));
			Connection.Close();
		}
	}
}
