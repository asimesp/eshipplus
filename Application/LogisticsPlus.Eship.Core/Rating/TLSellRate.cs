﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Rating
{
	[Serializable]
	[Entity("TLSellRate", ReadOnly = false, Source = EntitySource.TableView)]
	public class TLSellRate : TenantBase
	{
		private long _equipmentTypeId;
		private long _originCountryId;
		private long _destCountryId;
		private long _customerRatingId;
		private long _mileageSourceId;

		private MileageSource _mileageSource;
		private CustomerRating _customerRating;
		private Country _originCountry;
		private Country _destCountry;
		private EquipmentType _equipmentType;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("EquipmentTypeId", Description = "Equipment Type Reference")]
		[Property("EquipmentTypeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long EquipmentTypeId
		{
			get
			{
				if (_equipmentType != null) _equipmentTypeId = _equipmentType.Id;
				return _equipmentTypeId;
			}
			set
			{
				_equipmentTypeId = value;
				if (_equipmentType != null && value != _equipmentType.Id) _equipmentType = null;
			}
		}

		[EnableSnapShot("RateType", Description = "Rate Type")]
		[Property("RateType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public RateType RateType { get; set; }

		[EnableSnapShot("Rate")]
		[Property("Rate", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal Rate { get; set; }

		[EnableSnapShot("MinimumCharge", Description = "Minimum Charge")]
		[Property("MinimumCharge", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal MinimumCharge { get; set; }

		[EnableSnapShot("MinimumWeight", Description = "Minimum Weight")]
		[Property("MinimumWeight", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal MinimumWeight { get; set; }

		[EnableSnapShot("MaximumWeight", Description = "Maximum Weight")]
		[Property("MaximumWeight", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal MaximumWeight { get; set; }

		[EnableSnapShot("TariffType", Description = "Tariff Type")]
		[Property("TariffType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public TLTariffType TariffType { get; set; }

		[EnableSnapShot("EffectiveDate", Description = "Effective Date")]
		[Property("EffectiveDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime EffectiveDate { get; set; }

		[EnableSnapShot("OriginCity", Description = "Origin City")]
		[Property("OriginCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginCity { get; set; }

		[EnableSnapShot("OriginState", Description = "Origin State")]
		[Property("OriginState", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginState { get; set; }

		[EnableSnapShot("OriginCountryId", Description = "Origin Country Reference")]
		[Property("OriginCountryId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long OriginCountryId
		{
			get { return _originCountryId; }
			set
			{
				_originCountryId = value;
				if (_originCountry != null && value != _originCountry.Id) _originCountry = null;
			}
		}

		[EnableSnapShot("OriginPostalCode", Description = "Origin Postal Code")]
		[Property("OriginPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginPostalCode { get; set; }

		[EnableSnapShot("DestinationCity", Description = "Destination City")]
		[Property("DestinationCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationCity { get; set; }

		[EnableSnapShot("DestinationState", Description = "Destination State")]
		[Property("DestinationState", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationState { get; set; }

		[EnableSnapShot("DestinationCountryId", Description = "Destination Country Reference")]
		[Property("DestinationCountryId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long DestinationCountryId
		{
			get { return _destCountryId; }
			set
			{
				_destCountryId = value;
				if (_destCountry != null && value != _destCountry.Id) _destCountry = null;
			}
		}

		[EnableSnapShot("DestinationPostalCode", Description = "Destination Postal Code")]
		[Property("DestinationPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationPostalCode { get; set; }

		[EnableSnapShot("Mileage")]
		[Property("Mileage", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal Mileage { get; set; }

		[EnableSnapShot("MileageSourceId", Description = "Mileage Source Reference")]
		[Property("MileageSourceId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long MileageSourceId
		{
			get { return _mileageSourceId; }
			set
			{
				_mileageSourceId = value;
				if (_mileageSource != null && _mileageSource.Id != value) _mileageSource = null;
			}
		}

		[EnableSnapShot("CustomerRatingId", Description = "Customer Rating Reference")]
		[Property("CustomerRatingId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long CustomerRatingId
		{
			get
			{
				if (_customerRating != null) _customerRatingId = _customerRating.Id;
				return _customerRatingId;
			}
			set
			{
				_customerRatingId = value;
				if (_customerRating != null && value != _customerRating.Id) _customerRating = null;
			}
		}

		public CustomerRating CustomerRating
		{
			get { return _customerRating ?? (_customerRating = new CustomerRating(_customerRatingId)); }
			set
			{
				_customerRating = value;
				_customerRatingId = value == null ? default(long) : value.Id;
			}
		}

		public EquipmentType EquipmentType
		{
			get
			{
				return _equipmentTypeId == default(long) ? null : (_equipmentType ?? (_equipmentType = new EquipmentType(_equipmentTypeId)));
			}
			set
			{
				_equipmentType = value;
				_equipmentTypeId = value == null ? default(long) : value.Id;
			}
		}

		public Country OriginCountry
		{
			get { return _originCountryId == default(long) ? null : _originCountry ?? (_originCountry = new Country(_originCountryId)); }
			set
			{
				_originCountry = value;
				_originCountryId = value == null ? default(long) : value.Id;
			}
		}
		public Country DestinationCountry
		{
			get { return _destCountryId == default(long) ? null : _destCountry ?? (_destCountry = new Country(_destCountryId)); }
			set
			{
				_destCountry = value;
				_destCountryId = value == null ? default(long) : value.Id;
			}
		}
		public MileageSource MileageSource
		{
			get
			{
				return _mileageSourceId == default(long) ? null : _mileageSource ?? (_mileageSource = new MileageSource(_mileageSourceId, false));
			}
			set
			{
				_mileageSource = value;
				_mileageSourceId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public TLSellRate()
		{

		}

		public TLSellRate(long id)
			: this(id, false)
		{
		}

		public TLSellRate(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public TLSellRate(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
