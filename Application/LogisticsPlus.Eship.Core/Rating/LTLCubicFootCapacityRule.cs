﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Rating
{
	[Serializable]
	[Entity("LTLCubicFootCapacityRule", ReadOnly = false, Source = EntitySource.TableView)]
	public class LTLCubicFootCapacityRule : TenantBase
	{
		private long _vendorRatingId;

		private VendorRating _vendorRating;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("LowerBound", Description = "Lower Bound")]
		[Property("LowerBound", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal LowerBound { get; set; }

		[EnableSnapShot("UpperBound", Description = "Upper Bound")]
		[Property("UpperBound", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal UpperBound { get; set; }

		[EnableSnapShot("AppliedFreightClass", Description = "Applied Freight Class")]
		[Property("AppliedFreightClass", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
		public double AppliedFreightClass { get; set; }

		[EnableSnapShot("ApplyDiscount", Description = "Vendor Discount Applies")]
		[Property("ApplyDiscount", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ApplyDiscount { get; set; }

		[EnableSnapShot("ApplyFAK", Description = "FAK Applies")]
		[Property("ApplyFAK", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ApplyFAK { get; set; }

		[EnableSnapShot("AveragePoundPerCubicFootLimit", Description = "Average Pound Per Cubic Foot Limit")]
		[Property("AveragePoundPerCubicFootLimit", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal AveragePoundPerCubicFootLimit { get; set; }

		[EnableSnapShot("AveragePoundPerCubicFootApplies", Description = "Average Pound Per Cubic Foot Applies")]
		[Property("AveragePoundPerCubicFootApplies", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool AveragePoundPerCubicFootApplies { get; set; }

		[EnableSnapShot("PenaltyPoundPerCubicFoot", Description = "Penalty Pound Per Cubic Foot")]
		[Property("PenaltyPoundPerCubicFoot", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal PenaltyPoundPerCubicFoot	 { get; set; }

		[EnableSnapShot("EffectiveDate", Description = "Effective Date")]
		[Property("EffectiveDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime EffectiveDate { get; set; }

		[EnableSnapShot("VendorRatingId", Description = "Vendor Rating Reference")]
		[Property("VendorRatingId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long VendorRatingId
		{
			get
			{
				if (_vendorRating != null) _vendorRatingId = _vendorRating.Id;
				return _vendorRatingId;
			}
			set
			{
				_vendorRatingId = value;
				if (_vendorRating != null && value != _vendorRating.Id) _vendorRating = null;
			}
		}

		public VendorRating VendorRating
		{
			get { return _vendorRating ?? (_vendorRating = new VendorRating(_vendorRatingId)); }
			set
			{
				_vendorRating = value;
				_vendorRatingId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew { get { return Id == default(long); } }

		public LTLCubicFootCapacityRule()
		{
		}

		public LTLCubicFootCapacityRule(long id) : this(id, false)
		{
			
		}

		public LTLCubicFootCapacityRule(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public LTLCubicFootCapacityRule(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

		public static LTLCubicFootCapacityRule NewLTLCubicFootCapacityRule
		{
			get
			{
				return new LTLCubicFootCapacityRule
				       	{
				       		EffectiveDate = DateTime.Now,
							ApplyFAK = true,
							ApplyDiscount = true,
							AppliedFreightClass = 50d
				       	};
			}
		}
	}
}
