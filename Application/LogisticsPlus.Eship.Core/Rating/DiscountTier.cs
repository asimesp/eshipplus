﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Rating
{
	[Serializable]
	[Entity("DiscountTier", ReadOnly = false, Source = EntitySource.TableView)]
	public class DiscountTier : TenantBase
	{
		private long _freightChargeCodeId;
		private long _originRegionId;
		private long _destinationRegionId;
		private long _vendorRatingId;

		private VendorRating _vendorRating;
		private Region _originRegion;
		private Region _destinationRegion;
		private ChargeCode _freightChargeCode;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("DiscountPercent", Description = "Discount Percent")]
		[Property("DiscountPercent", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal DiscountPercent { get; set; }

		[EnableSnapShot("FloorValue", Description = "Floor Value")]
		[Property("FloorValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal FloorValue { get; set; }

		[EnableSnapShot("CeilingValue", Description = "Ceiling Value")]
		[Property("CeilingValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal CeilingValue { get; set; }

		[EnableSnapShot("FreightChargeCodeId", Description = "Freight Charge Code Reference")]
		[Property("FreightChargeCodeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long FreightChargeCodeId
		{
			get { return _freightChargeCodeId; }
			set
			{
				_freightChargeCodeId = value;
				if (_freightChargeCode != null && value != _freightChargeCode.Id) _freightChargeCode = null;
			}
		}

		[EnableSnapShot("TierPriority", Description = "Tier Priority")]
		[Property("TierPriority", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int TierPriority { get; set; }

		[EnableSnapShot("FAK50", Description = "FAK 50")]
		[Property("FAK50", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
		public double FAK50 { get; set; }

		[EnableSnapShot("FAK55", Description = "FAK 55")]
		[Property("FAK55", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
		public double FAK55 { get; set; }

		[EnableSnapShot("FAK60", Description = "FAK 60")]
		[Property("FAK60", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
		public double FAK60 { get; set; }

		[EnableSnapShot("FAK65", Description = "FAK 65")]
		[Property("FAK65", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
		public double FAK65 { get; set; }

		[EnableSnapShot("FAK70", Description = "FAK 70")]
		[Property("FAK70", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
		public double FAK70 { get; set; }

		[EnableSnapShot("FAK775", Description = "FAK 77.5")]
		[Property("FAK775", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
		public double FAK775 { get; set; }

		[EnableSnapShot("FAK85", Description = "FAK 85")]
		[Property("FAK85", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
		public double FAK85 { get; set; }

		[EnableSnapShot("FAK925", Description = "FAK 92.5")]
		[Property("FAK925", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
		public double FAK925 { get; set; }

		[EnableSnapShot("FAK100", Description = "FAK 100")]
		[Property("FAK100", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
		public double FAK100 { get; set; }

		[EnableSnapShot("FAK110", Description = "FAK 110")]
		[Property("FAK110", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
		public double FAK110 { get; set; }

		[EnableSnapShot("FAK125", Description = "FAK 125")]
		[Property("FAK125", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
		public double FAK125 { get; set; }

		[EnableSnapShot("FAK150", Description = "FAK 150")]
		[Property("FAK150", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
		public double FAK150 { get; set; }

		[EnableSnapShot("FAK175", Description = "FAK 175")]
		[Property("FAK175", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
		public double FAK175 { get; set; }

		[EnableSnapShot("FAK200", Description = "FAK 200")]
		[Property("FAK200", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
		public double FAK200 { get; set; }

		[EnableSnapShot("FAK250", Description = "FAK 250")]
		[Property("FAK250", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
		public double FAK250 { get; set; }

		[EnableSnapShot("FAK300", Description = "FAK 300")]
		[Property("FAK300", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
		public double FAK300 { get; set; }

		[EnableSnapShot("FAK400", Description = "FAK 400")]
		[Property("FAK400", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
		public double FAK400 { get; set; }

		[EnableSnapShot("FAK500", Description = "FAK 500")]
		[Property("FAK500", AutoValueOnInsert = false, DataType = SqlDbType.Float, Key = false)]
		public double FAK500 { get; set; }

		[EnableSnapShot("EffectiveDate", Description = "Effective Date")]
		[Property("EffectiveDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime EffectiveDate { get; set; }

		[EnableSnapShot("RateBasis", Description = "Rate Basis")]
		[Property("RateBasis", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public DiscountTierRateBasis RateBasis { get; set; }

		[EnableSnapShot("WeightBreak", Description = "Discount Weight Break")]
		[Property("WeightBreak", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public WeightBreak WeightBreak { get; set; }

		[EnableSnapShot("OriginRegionId", Description = "Origin Region Reference")]
		[Property("OriginRegionId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long OriginRegionId
		{
			get { return _originRegionId; }
			set
			{
				_originRegionId = value;
				if (_originRegion != null && value != _originRegion.Id) _originRegion = null;
			}
		}

		[EnableSnapShot("DestinationRegionId", Description = "Destination Region Reference")]
		[Property("DestinationRegionId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long DestinationRegionId
		{
			get { return _destinationRegionId; }
			set
			{
				_destinationRegionId = value;
				if (_destinationRegion != null && value != _destinationRegion.Id) _destinationRegion = null;
			}
		}

		[EnableSnapShot("VendorRatingId", Description = "Vendor Rating Reference")]
		[Property("VendorRatingId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long VendorRatingId
		{
			get
			{
				if (_vendorRating != null) _vendorRatingId = _vendorRating.Id;
				return _vendorRatingId;
			}
			set
			{
				_vendorRatingId = value;
				if (_vendorRating != null && value != _vendorRating.Id) _vendorRating = null;
			}
		}

		public VendorRating VendorRating
		{
			get { return _vendorRating ?? (_vendorRating = new VendorRating(_vendorRatingId)); }
			set
			{
				_vendorRating = value;
				_vendorRatingId = value == null ? default(long) : value.Id;
			}
		}
		public Region OriginRegion
		{
			get { return _originRegionId == default(long) ? null : _originRegion ?? (_originRegion = new Region(_originRegionId, false)); }
			set
			{
				_originRegion = value;
				_originRegionId = value == null ? default(long) : value.Id;
			}
		}
		public Region DestinationRegion
		{
			get { return _destinationRegionId == default(long) ? null : _destinationRegion ?? (_destinationRegion = new Region(_destinationRegionId, false)); }
			set
			{
				_destinationRegion = value;
				_destinationRegionId = value == null ? default(long) : value.Id;
			}
		}
		public ChargeCode FreightChargeCode
		{
			get { return _freightChargeCodeId == default(long) ? null : _freightChargeCode ?? (_freightChargeCode = new ChargeCode(_freightChargeCodeId, false)); }
			set
			{
				_freightChargeCode = value;
				_freightChargeCodeId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew { get { return Id == default(long); } }

		public DiscountTier()
		{
		}

		public DiscountTier(long id) : this(id, false)
		{

		}

		public DiscountTier(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public DiscountTier(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

		public static DiscountTier NewDiscountTier
		{
			get
			{
				return new DiscountTier
				       	{
				       		FAK100 = 100,
				       		FAK50 = 50,
				       		FAK110 = 110,
				       		FAK125 = 125,
				       		FAK150 = 150,
				       		FAK175 = 175,
				       		FAK200 = 200,
				       		FAK250 = 250,
				       		FAK300 = 300,
				       		FAK400 = 400,
				       		FAK500 = 500,
				       		FAK55 = 55,
				       		FAK60 = 60,
				       		FAK65 = 65,
				       		FAK70 = 70,
				       		FAK775 = 77.5,
				       		FAK85 = 85,
				       		FAK925 = 92.5, 
							EffectiveDate = DateTime.Now
				       	};
			}
		}
	}
}
