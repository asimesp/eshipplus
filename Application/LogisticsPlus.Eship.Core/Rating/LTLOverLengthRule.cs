﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Rating
{
	[Serializable]
	[Entity("LTLOverLengthRule", ReadOnly = false, Source = EntitySource.TableView)]
	public class LTLOverLengthRule : TenantBase
	{
		private long _chargeCodeId;
		private long _vendorRatingId;

		private VendorRating _vendorRating;
		private ChargeCode _chargeCode;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("LowerLengthBound", Description = "Lower Length Bound")]
		[Property("LowerLengthBound", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal LowerLengthBound { get; set; }

		[EnableSnapShot("UpperLengthBound", Description = "Upper Length Bound")]
		[Property("UpperLengthBound", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal UpperLengthBound { get; set; }

		[EnableSnapShot("Charge")]
		[Property("Charge", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal Charge { get; set; }

		[EnableSnapShot("ChargeCodeId", Description = "Charge Code Reference")]
		[Property("ChargeCodeId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ChargeCodeId
		{
			get { return _chargeCodeId; }
			set
			{
				_chargeCodeId = value;
				if (_chargeCode != null && value != _chargeCode.Id) _chargeCode = null;
			}
		}

		[EnableSnapShot("EffectiveDate", Description = "Effective Date")]
		[Property("EffectiveDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime EffectiveDate { get; set; }

		[EnableSnapShot("VendorRatingId", Description = "Vendor Rating Reference")]
		[Property("VendorRatingId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long VendorRatingId
		{
			get
			{
				if (_vendorRating != null) _vendorRatingId = _vendorRating.Id;
				return _vendorRatingId;
			}
			set
			{
				_vendorRatingId = value;
				if (_vendorRating != null && value != _vendorRating.Id) _vendorRating = null;
			}
		}

		public VendorRating VendorRating
		{
			get { return _vendorRating ?? (_vendorRating = new VendorRating(_vendorRatingId)); }
			set
			{
				_vendorRating = value;
				_vendorRatingId = value == null ? default(long) : value.Id;
			}
		}
		public ChargeCode ChargeCode
		{
			get { return _chargeCodeId == default(long) ? null : _chargeCode ?? (_chargeCode = new ChargeCode(_chargeCodeId, false)); }
			set
			{
				_chargeCode = value;
				_chargeCodeId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew { get { return Id == default(long); } }

		public LTLOverLengthRule()
		{
		}

		public LTLOverLengthRule(long id) : this(id, false)
		{
			
		}

		public LTLOverLengthRule(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public LTLOverLengthRule(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
