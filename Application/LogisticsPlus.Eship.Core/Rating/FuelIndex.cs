﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Rating
{
	[Serializable]
	[Entity("FuelIndex", ReadOnly = false, Source = EntitySource.TableView)]
	public class FuelIndex : TenantBase
	{
		private long _fuelTableId;

		private FuelTable _fuelTable;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("LowerBound", Description = "Lower Bound")]
		[Property("LowerBound", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal LowerBound { get; set; }

		[EnableSnapShot("UpperBound", Description = "Upper Bound")]
		[Property("UpperBound", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal UpperBound { get; set; }

		[EnableSnapShot("Surcharge", Description = "Surcharge")]
		[Property("Surcharge", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal Surcharge { get; set; }

		[EnableSnapShot("FuelTableId", Description = "Fuel Table Reference")]
		[Property("FuelTableId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long FuelTableId
		{
			get
			{
				if (_fuelTable != null) _fuelTableId = _fuelTable.Id;
				return _fuelTableId;
			}
			set
			{
				_fuelTableId = value;
				if (_fuelTable != null && value != _fuelTable.Id) _fuelTable = null;
			}
		}

		public FuelTable FuelTable
		{
			get { return _fuelTable ?? (_fuelTable = new FuelTable(_fuelTableId)); }
			set
			{
				_fuelTable = value;
				_fuelTableId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public FuelIndex()
		{
			
		}

		public FuelIndex(long id) : this(id, false)
		{
		}

		public FuelIndex(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public FuelIndex(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
