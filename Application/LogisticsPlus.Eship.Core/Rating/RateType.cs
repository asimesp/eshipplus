﻿namespace LogisticsPlus.Eship.Core.Rating
{
    // DO NOT RE-ORDER. SEE APPLICABLE JOINS ON DATABASE REFRATETYPE
    public enum RateType
	{
		Flat = 0,
		RatePerMile,
		PerHundredWeight,
		LineHaulPercentage
	}
}
