﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Rating
{
	[Serializable]
	[Entity("LTLSellRate", ReadOnly = false, Source = EntitySource.TableView)]
	public class LTLSellRate : TenantBase
	{
		private long _customerRatingId;
		private long _vendorRatingId;

		private VendorRating _vendorRating;
		private CustomerRating _customerRating;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("LTLIndirectPointEnabled", Description = "LTL Indirect Point Enabled")]
		[Property("LTLIndirectPointEnabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool LTLIndirectPointEnabled { get; set; }

		[EnableSnapShot("EffectiveDate", Description = "Effective Date")]
		[Property("EffectiveDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime EffectiveDate { get; set; }

		[EnableSnapShot("Active")]
		[Property("Active", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Active { get; set; }

		[EnableSnapShot("MarkupPercent", Description = "Markup Percent")]
		[Property("MarkupPercent", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal MarkupPercent { get; set; }

		[EnableSnapShot("MarkupValue", Description = "Markup Value")]
		[Property("MarkupValue", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal MarkupValue { get; set; }

		[EnableSnapShot("UseMinimum", Description = "Use Minimum of Percent or Value")]
		[Property("UseMinimum", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool UseMinimum { get; set; }


		[EnableSnapShot("StartOverrideWeightBreak", Description = "Maximum Weight Break for Markup Override")]
		[Property("StartOverrideWeightBreak", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public WeightBreak StartOverrideWeightBreak { get; set; }

		[EnableSnapShot("OverrideMarkupPercentL5C", Description = "Override Markup Percent L5C")]
		[Property("OverrideMarkupPercentL5C", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupPercentL5C { get; set; }

		[EnableSnapShot("OverrideMarkupValueL5C", Description = "Override Markup Value L5C")]
		[Property("OverrideMarkupValueL5C", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupValueL5C { get; set; }

		[EnableSnapShot("OverrideMarkupPercentM5C", Description = "Override Markup Percent M5C")]
		[Property("OverrideMarkupPercentM5C", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupPercentM5C { get; set; }

		[EnableSnapShot("OverrideMarkupValueM5C", Description = "Override Markup Value M5C")]
		[Property("OverrideMarkupValueM5C", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupValueM5C { get; set; }

		[EnableSnapShot("OverrideMarkupPercentM1M", Description = "Override Markup Percent M1M")]
		[Property("OverrideMarkupPercentM1M", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupPercentM1M { get; set; }

		[EnableSnapShot("OverrideMarkupValueM1M", Description = "Override Markup Value M1M")]
		[Property("OverrideMarkupValueM1M", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupValueM1M { get; set; }

		[EnableSnapShot("OverrideMarkupPercentM2M", Description = "Override Markup Percent M2M")]
		[Property("OverrideMarkupPercentM2M", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupPercentM2M { get; set; }

		[EnableSnapShot("OverrideMarkupValueM2M", Description = "Override Markup Value M2M")]
		[Property("OverrideMarkupValueM2M", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupValueM2M { get; set; }

		[EnableSnapShot("OverrideMarkupPercentM5M", Description = "Override Markup Percent M5M")]
		[Property("OverrideMarkupPercentM5M", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupPercentM5M { get; set; }

		[EnableSnapShot("OverrideMarkupValueM5M", Description = "Override Markup Value M5M")]
		[Property("OverrideMarkupValueM5M", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupValueM5M { get; set; }

		[EnableSnapShot("OverrideMarkupPercentM10M", Description = "Override Markup Percent M10M")]
		[Property("OverrideMarkupPercentM10M", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupPercentM10M { get; set; }

		[EnableSnapShot("OverrideMarkupValueM10M", Description = "Override Markup Value M10M")]
		[Property("OverrideMarkupValueM10M", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupValueM10M { get; set; }

		[EnableSnapShot("OverrideMarkupPercentM20M", Description = "Override Markup Percent M20M")]
		[Property("OverrideMarkupPercentM20M", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupPercentM20M { get; set; }

		[EnableSnapShot("OverrideMarkupValueM20M", Description = "Override Markup Value M20M")]
		[Property("OverrideMarkupValueM20M", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupValueM20M { get; set; }

		[EnableSnapShot("OverrideMarkupPercentM30M", Description = "Override Markup Percent M30M")]
		[Property("OverrideMarkupPercentM30M", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupPercentM30M { get; set; }

		[EnableSnapShot("OverrideMarkupValueM30M", Description = "Override Markup Value M30M")]
		[Property("OverrideMarkupValueM30M", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupValueM30M { get; set; }

		[EnableSnapShot("OverrideMarkupPercentM40M", Description = "Override Markup Percent M40M")]
		[Property("OverrideMarkupPercentM40M", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupPercentM40M { get; set; }

		[EnableSnapShot("OverrideMarkupValueM40M", Description = "Override Markup Value M40M")]
		[Property("OverrideMarkupValueM40M", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal OverrideMarkupValueM40M { get; set; }
        
        [EnableSnapShot("OverrideMarkupPercentVendorFloor", Description = "Override Markup Percent Vendor Floor")]
        [Property("OverrideMarkupPercentVendorFloor", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal	OverrideMarkupPercentVendorFloor { get; set; }

        [EnableSnapShot("OverrideMarkupValueVendorFloor", Description = "Override Markup Value Vendor Floor")]
        [Property("OverrideMarkupValueVendorFloor", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
        public decimal	OverrideMarkupValueVendorFloor { get; set; }

        [EnableSnapShot("OverrideMarkupVendorFloorEnabled", Description = "Override Markup Vendor Floor Enabled")]
        [Property("OverrideMarkupVendorFloorEnabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool OverrideMarkupVendorFloorEnabled { get; set; }


		[EnableSnapShot("VendorRatingId", Description = "Vendor Rating Reference")]
		[Property("VendorRatingId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long VendorRatingId
		{
			get
			{
				if (_vendorRating != null) _vendorRatingId = _vendorRating.Id;
				return _vendorRatingId;
			}
			set
			{
				_vendorRatingId = value;
				if (_vendorRating != null && value != _vendorRating.Id) _vendorRating = null;
			}
		}

		[EnableSnapShot("CustomerRatingId", Description = "Customer Rating Reference")]
		[Property("CustomerRatingId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long CustomerRatingId
		{
			get
			{
				if (_customerRating != null) _customerRatingId = _customerRating.Id;
				return _customerRatingId;
			}
			set
			{
				_customerRatingId = value;
				if (_customerRating != null && value != _customerRating.Id) _customerRating = null;
			}
		}

		public CustomerRating CustomerRating
		{
			get { return _customerRating ?? (_customerRating = new CustomerRating(_customerRatingId)); }
			set
			{
				_customerRating = value;
				_customerRatingId = value == null ? default(long) : value.Id;
			}
		}

		public VendorRating VendorRating
		{
			get { return _vendorRating ?? (_vendorRating = new VendorRating(_vendorRatingId)); }
			set
			{
				_vendorRating = value;
				_vendorRatingId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public LTLSellRate()
		{

		}

		public LTLSellRate(long id)
			: this(id, false)
		{
		}

		public LTLSellRate(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public LTLSellRate(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
