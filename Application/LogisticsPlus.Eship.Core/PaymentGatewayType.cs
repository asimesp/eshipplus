﻿namespace LogisticsPlus.Eship.Core
{
    public enum PaymentGatewayType
    {
        NotApplicable,
        AuthorizeNet,
        Check
    }
}
