﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core
{
	[Serializable]
	[Entity("TenantContact", ReadOnly = false, Source = EntitySource.TableView)]
	public class TenantContact : Contact
	{
		private long _tenantLocationId;

		private TenantLocation _tenantLocation;
		
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key =  true)]
		public long Id { get; private set; }

		[Property("TenantLocationId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long TenantLocationId
		{
			get
			{
				if (_tenantLocation != null) _tenantLocationId = _tenantLocation.Id;
				return _tenantLocationId;
			}
			set
			{
				_tenantLocationId = value;
				if (_tenantLocation != null && value != _tenantLocation.Id) _tenantLocation = null;
			}
		}

		public TenantLocation Location
		{
			get { return _tenantLocation ?? (_tenantLocation = new TenantLocation(_tenantLocationId)); }
			set
			{
				_tenantLocation = value;
				_tenantLocationId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public TenantContact()
		{
			
		}

		public TenantContact(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if(takeSnapshot) TakeSnapShot();
		}

		public TenantContact(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}