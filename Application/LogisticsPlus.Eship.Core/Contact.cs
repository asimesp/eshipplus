﻿using System;
using System.Data;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core
{
	[Serializable]
	public class Contact : TenantBase
	{
		private long _contactTypeId;

		private ContactType _contactType;

		[EnableSnapShot("Name")]
		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

		[EnableSnapShot("Phone")]
		[Property("Phone", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Phone { get; set; }

		[EnableSnapShot("Mobile")]
		[Property("Mobile", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Mobile { get; set; }

		[EnableSnapShot("Fax")]
		[Property("Fax", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Fax { get; set; }

		[EnableSnapShot("Email")]
		[Property("Email", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Email { get; set; }

		[EnableSnapShot("Comment")]
		[Property("Comment", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Comment { get; set; }

		[EnableSnapShot("Primary", Description = "Contact Primary Designation")]
		[Property("Primary", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Primary { get; set; }

		[EnableSnapShot("ContactTypeId", Description = "Contact Type Reference")]
		[Property("ContactTypeId", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public long ContactTypeId
		{
			get { return _contactTypeId; }
			set
			{
				_contactTypeId = value;
				if (_contactType != null && value != _contactType.Id) _contactType = null;
			}
		}

		public ContactType ContactType
		{
			get { return _contactTypeId == default(long) ? null : _contactType ?? (_contactType = new ContactType(_contactTypeId, false)); }
			set
			{
				_contactType = value;
				_contactTypeId = value == null ? default(long) : value.Id;
			}
		}
	}
}
