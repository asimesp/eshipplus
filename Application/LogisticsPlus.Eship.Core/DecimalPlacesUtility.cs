﻿using System.Collections.Generic;

namespace LogisticsPlus.Eship.Core
{
	public static class DecimalPlacesUtility
	{
		private static readonly List<string> Places = new List<string>();

		public static List<string> DecimalPlaces()
		{
			if (Places.Count > 0) return Places;
			for (var i = 0; i < 5; i++) Places.Add(i.ToString());
			return Places;
		}

		public static bool DecimalPlaceIsValid(this string place)
		{
			return DecimalPlaces().Contains(place);
		}
	}
}
