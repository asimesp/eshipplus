﻿using System;
using System.Data;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core
{
	[Serializable]
	public class TenantBase : ObjectBase
	{
		private long _tenantId = default(long);

		private Tenant _tenant;

		[Property("TenantId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long TenantId
		{
			get { return _tenantId; }
			set
			{
				_tenantId = value;
				if (_tenant != null && value != _tenant.Id) _tenant = null;
			}
		}
		
		public Tenant Tenant
		{
			get { return _tenantId == default(long) ? null : _tenant ?? (_tenant = new Tenant(_tenantId)); }
			set
			{
				_tenant = value;
				_tenantId = value == null ? default(long) : value.Id;
			}
		}
	}
}
