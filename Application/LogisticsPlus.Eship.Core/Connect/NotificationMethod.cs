﻿namespace LogisticsPlus.Eship.Core.Connect
{
    // DO NOT RE-ORDER. SEE APPLICABLE JOINS ON DATABASE REFNOTIFICATIONMETHOD
    public enum NotificationMethod
	{
		Ftp = 0,
		Edi,
		Email,
		Fax
	}
}
