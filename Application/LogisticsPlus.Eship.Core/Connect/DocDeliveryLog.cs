﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Connect
{
	[Serializable]
    [Entity("DocDeliveryLog", ReadOnly = false, Source = EntitySource.TableView)]
	public class DocDeliveryLog : TenantBase
    {
        private long _documentTagId;
		private long _userId;

		private User _user;
        private DocumentTag _documentTag;

        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; set; }
                
        [Property("EntityId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long EntityId { get; set; }

        [Property("EntityType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public DocDeliveryEntityType EntityType { get; set; }

        [Property("LocationPath", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LocationPath { get; set; }

        [Property("DocumentName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DocumentName { get; set; }

        [Property("LogDateTime", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime LogDateTime { get; set; }

        [Property("DeliveryWasSuccessful", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool DeliveryWasSuccessful { get; set; }

        [Property("FailedDeliveryMessage", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string FailedDeliveryMessage { get; set; }

        [Property("DocumentTagId", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public long DocumentTagId
        {
            get { return _documentTagId; }
            set
            {
                _documentTagId = value;
                if (_documentTag != null && value != _documentTag.Id) _documentTag = null;
            }
        }

		[Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long UserId
		{
			get
			{
				if (_user != null) _userId = _user.Id;
				return _userId;
			}
			set
			{
				_userId = value;
				if (_user != null && value != _user.Id) _user = null;
			}
		}

		public User User
		{
			get { return _userId == default(long) ? null : _user ?? (_user = new User(_userId)); }
			set
			{
				_user = value;
				_userId = value == null ? default(long) : value.Id;
			}
		}

        public DocumentTag DocumentTag
        {
            get { return _documentTagId == default(long) ? null : _documentTag ?? (_documentTag = new DocumentTag(_documentTagId, false)); }
            set
            {
                _documentTag = value;
                _documentTagId = value == null ? default(long) : value.Id;
            }
        }


        public bool IsNew
		{
			get { return Id == default(long); }
		}

		public DocDeliveryLog() { }

		public DocDeliveryLog(long id) : this(id, false) { }

		public DocDeliveryLog(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public DocDeliveryLog(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
