﻿namespace LogisticsPlus.Eship.Core.Connect
{
	public enum Milestone
	{
		// DO NOT CHANGE ORDER!!!
		NewLoadOrder = 0,
		LoadOrderVoided,
		NewShipmentRequest,
		ShipmentVoided,
		ShipmentInTransit,
		ShipmentDelivered = 5,
		InvoicePosted,
		ShipmentVoidReversed,
		ShipmentCheckCallUpdate,
		TruckloadBidNotification
	}
}
