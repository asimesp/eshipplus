﻿namespace LogisticsPlus.Eship.Core.Connect
{
    // DO NOT RE-ORDER. SEE APPLICABLE JOINS ON DATABASE REFFAXTRANSMISSIONSTATUS
    public enum FaxTransmissionStatus
	{
		Success = 0,
		Failed,
		Pending,
		NoFax,
        Queued
	}
}
