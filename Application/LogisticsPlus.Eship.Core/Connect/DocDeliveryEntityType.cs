﻿namespace LogisticsPlus.Eship.Core.Connect
{
	public enum DocDeliveryEntityType
	{
		// DO NOT RE-ARRANGE!  ADD TO LIST ONLY
		BillOfLading = 0,
		ShipmentDocument,
		ShipmentStatement,
		Invoice,
	}
}
