﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Connect
{
	[Serializable]
	[Entity("VendorCommunication", ReadOnly = false, Source = EntitySource.TableView)]
	public class VendorCommunication : Communication
	{
		private Vendor _vendor;

		private List<VendorNotification> _notifications;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("VendorId", Description = "Vendor Reference")]
		[Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		private long VendorId { get; set; }

		[EnableSnapShot("LoadTenderExpAllowance", Description = "Load Tender Expiration Allowance (mins)")]
		[Property("LoadTenderExpAllowance", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int LoadTenderExpAllowance { get; set; }

		[EnableSnapShot("Pickup990", Description = "Pickup 990 (Load Tender Response)")]
		[Property("Pickup990", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Pickup990 { get; set; }

		[EnableSnapShot("AcknowledgePickup990", Description = "Acknowledge Pickup 990 (Load Tender Response)")]
		[Property("AcknowledgePickup990", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool AcknowledgePickup990 { get; set; }

		[EnableSnapShot("DropOff204", Description = "Drop Off 204 (Load Tender)")]
		[Property("DropOff204", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool DropOff204 { get; set; }

		[EnableSnapShot("Pickup210", Description = "Pickup 210 (Freight Invoice)")]
		[Property("Pickup210", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Pickup210 { get; set; }

		[EnableSnapShot("AcknowledgePickup210", Description = "Acknowledge Pickup 210 (Freight Invoice)")]
		[Property("AcknowledgePickup210", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool AcknowledgePickup210 { get; set; }

		[EnableSnapShot("Pickup214", Description = "Pickup 214 (Shipment Status Update)")]
		[Property("Pickup214", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Pickup214 { get; set; }

		[EnableSnapShot("AcknowledgePickup214", Description = "Acknowledge Pickup 214 (Shipment Status Update)")]
		[Property("AcknowledgePickup214", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool AcknowledgePickup214 { get; set; }

		[EnableSnapShot("Pickup997", Description = "Pickup 997 (Acknowledgement)")]
		[Property("Pickup997", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Pickup997 { get; set; }

		[EnableSnapShot("ImgRtrvEnabled", Description = "Image Retrieval Enabled")]
		[Property("ImgRtrvEnabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ImgRtrvEnabled { get; set; }

		[EnableSnapShot("ShipmentDispatchEnabled", Description = "Shipment Dispatch Enabled")]
		[Property("ShipmentDispatchEnabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ShipmentDispatchEnabled { get; set; }

        [EnableSnapShot("DisableGuaranteedServiceDispatch", Description = "Disable Guaranteed Service Shipment Dispatch")]
        [Property("DisableGuaranteedServiceDispatch", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool DisableGuaranteedServiceDispatch { get; set; }

		[EnableSnapShot("CarrierIntegrationEngine", Description = "Carrier Integration Engine")]
		[Property("CarrierIntegrationEngine", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CarrierIntegrationEngine { get; set; }

		[EnableSnapShot("CarrierIntegrationUsername", Description = "Carrier Integration Username")]
		[Property("CarrierIntegrationUsername", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CarrierIntegrationUsername { get; set; }

		[EnableSnapShot("CarrierIntegrationPassword", Description = "Carrier Integration Password")]
		[Property("CarrierIntegrationPassword", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CarrierIntegrationPassword { get; set; }

        [EnableSnapShot("SMC3EvaEnabled", Description = "SMC3 Eva Enabled")]
        [Property("SMC3EvaEnabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool SMC3EvaEnabled { get; set; }

        [EnableSnapShot("IsInSMC3Testing", Description = "Is In SMC3 Testing")]
        [Property("IsInSMC3Testing", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool IsInSMC3Testing { get; set; }

        [EnableSnapShot("SMC3TestAccountToken", Description = "SMC3 Test Account Token")]
        [Property("SMC3TestAccountToken", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string SMC3TestAccountToken { get; set; }

        [EnableSnapShot("SMC3ProductionAccountToken", Description = "SMC3 Production Account Token")]
        [Property("SMC3ProductionAccountToken", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string SMC3ProductionAccountToken { get; set; }

        [EnableSnapShot("SMC3EvaSupportsDispatch", Description = "SMC3 Eva Supports Dispatch")]
        [Property("SMC3EvaSupportsDispatch", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool SMC3EvaSupportsDispatch { get; set; }

        [EnableSnapShot("SMC3EvaSupportsDocumentRetrieval", Description = "SMC3 Eva Supports Document Retrieval")]
        [Property("SMC3EvaSupportsDocumentRetrieval", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool SMC3EvaSupportsDocumentRetrieval { get; set; }

        [EnableSnapShot("SMC3EvaSupportsTracking", Description = "SMC3 Eva Supports Tracking")]
        [Property("SMC3EvaSupportsTracking", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool SMC3EvaSupportsTracking { get; set; }

	    [EnableSnapShot("Project44Enabled", Description = "Project 44 Enabled")]
	    [Property("Project44Enabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
	    public bool Project44Enabled { get; set; }

        [EnableSnapShot("Project44TrackingEnabled", Description = "Project 44 Tracking Enabled")]
	    [Property("Project44TrackingEnabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
	    public bool Project44TrackingEnabled { get; set; }

	    [EnableSnapShot("Project44DispatchEnabled", Description = "Project 44 Dispatch Enabled")]
	    [Property("Project44DispatchEnabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
	    public bool Project44DispatchEnabled { get; set; }

        public Vendor Vendor
		{
			get { return VendorId == default(long) ? null : _vendor ?? (_vendor = new Vendor(VendorId)); }
			set
			{
				_vendor = value;
				VendorId = value == null ? default(long) : value.Id;
			}
		}

		public List<VendorNotification> Notifications
		{
			get
			{
				if (_notifications == null) LoadNotifications();
				return _notifications;
			}
			set { _notifications = value; }
		}

		public bool IsNew { get { return Id == default(long); } }

		public VendorCommunication() {}

		public VendorCommunication(long id) : this(id, false) {}

		public VendorCommunication(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public VendorCommunication(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

		public void LoadCollections()
		{
			LoadNotifications();
		}

		private void LoadNotifications()
		{
			_notifications = new List<VendorNotification>();
			const string query = @"SELECT * FROM VendorNotification WHERE VendorCommunicationId = @VendorCommunicationId AND TenantId = @TenantId";
			IDictionary<string, object> parameters = new Dictionary<string, object> { { "VendorCommunicationId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_notifications.Add(new VendorNotification(reader));
			Connection.Close();
		}
	}
}