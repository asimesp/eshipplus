﻿namespace LogisticsPlus.Eship.Core.Connect
{
	public enum XmlConnectStatus
	{
        // DO NOT RE-ORDER. SEE APPLICABLE JOINS ON DATABASE REFXMLCONNECTSTATUS
        Accepted = 0,
		Rejected,
		Cancelled,
		Pending	// send but waiting acknowledgement!
	}
}
