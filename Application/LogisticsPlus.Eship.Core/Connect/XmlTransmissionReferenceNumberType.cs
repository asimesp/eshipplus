﻿namespace LogisticsPlus.Eship.Core.Connect
{
	public enum XmlTransmissionReferenceNumberType
	{
		Shipment = 0,
		Invoice,
		External
	}
}
