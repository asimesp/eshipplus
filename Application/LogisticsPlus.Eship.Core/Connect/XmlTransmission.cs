﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Administration;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Connect
{
	[Serializable]
	[Entity("XmlTransmission", ReadOnly = false, Source = EntitySource.TableView)]
	public class XmlTransmission : TenantBase
	{
		private long _userId;

		private User _user;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("TransmissionKey", Description = "Transmission Key")]
		[Property("TransmissionKey", AutoValueOnInsert = false, DataType = SqlDbType.UniqueIdentifier, Key = false)]
		public Guid TransmissionKey { get; set; }

		[EnableSnapShot("Xml", Description = "Xml Serialization")]
		[Property("Xml", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Xml { get; set; }

		[EnableSnapShot("DocumentType", Description = "Document Type")]
		[Property("DocumentType", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DocumentType { get; set; }

		[EnableSnapShot("CommunicationReferenceType", Description = "Communication Reference Type")]
		[Property("CommunicationReferenceType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public CommunicationReferenceType CommunicationReferenceType { get; set; }

		[EnableSnapShot("CommunicationReferenceId", Description = "Communication Reference Id")]
		[Property("CommunicationReferenceId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long CommunicationReferenceId { get; set; }

		[EnableSnapShot("TransmissionDateTime", Description = "Transmission Date Time")]
		[Property("TransmissionDateTime", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime TransmissionDateTime { get; set; }

		[EnableSnapShot("AcknowledgementDateTime", Description = "Acknowledgement Response Date Time")]
		[Property("AcknowledgementDateTime", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime AcknowledgementDateTime { get; set; }

		[EnableSnapShot("AcknowledgementMessage", Description = "Acknowledgement Response Message")]
		[Property("AcknowledgementMessage", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string AcknowledgementMessage { get; set; }

		[EnableSnapShot("ReferenceNumber", Description = "Reference Number")]
		[Property("ReferenceNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ReferenceNumber { get; set; }

		[EnableSnapShot("ReferenceNumberType", Description = "Xml Transmission Reference Number Type")]
		[Property("ReferenceNumberType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public XmlTransmissionReferenceNumberType ReferenceNumberType { get; set; }

		[EnableSnapShot("NotificationMethod", Description = "Notification Method")]
		[Property("NotificationMethod", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public NotificationMethod NotificationMethod { get; set; }

		[EnableSnapShot("SendOkay", Description = "Transmission Send Okay")]
		[Property("SendOkay", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool SendOkay { get; set; }

		[EnableSnapShot("Direction", Description = "Direction")]
		[Property("Direction", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public Direction Direction { get; set; }

		[EnableSnapShot("UserId", Description = "User Reference")]
		[Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long UserId
		{
			get { return _userId; }
			set
			{
				_userId = value;
				if (_user != null && value != _user.Id) _user = null;
			}
		}

		public User User
		{
			get { return _userId == default(long) ? null : _user ?? (_user = new User(_userId, false)); }
			set
			{
				_user = value;
				_userId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew { get { return Id == default(long); } }


		public XmlTransmission() {}

		public XmlTransmission(long id) : this(id, false) {}

		public XmlTransmission(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public XmlTransmission(DbDataReader reader)
		{
			Load(reader);
		}


		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}