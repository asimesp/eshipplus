﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Connect
{
	[Serializable]
	[Entity("VendorNotification", ReadOnly = false, Source = EntitySource.TableView)]
	public class VendorNotification : Notification
	{
		private long _vendorCommunicationId;

		private VendorCommunication _vendorCommunication;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("VendorCommunicationId", Description = "Vendor Communication Reference")]
		[Property("VendorCommunicationId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long VendorCommunicationId
		{
			get
			{
				if (_vendorCommunication != null) _vendorCommunicationId = _vendorCommunication.Id;
				return _vendorCommunicationId;
			}
			set
			{
				_vendorCommunicationId = value;
				if (_vendorCommunication != null && value != _vendorCommunication.Id) _vendorCommunication = null;
			}
		}

		public bool IsNew { get { return Id == default(long); } }

		public VendorCommunication VendorCommunication
		{
			get { return _vendorCommunication ?? (_vendorCommunication = new VendorCommunication(_vendorCommunicationId)); }
			set
			{
				_vendorCommunication = value;
				_vendorCommunicationId = value == null ? default(long) : value.Id;
			}
		}

		public VendorNotification() {}

		public VendorNotification(long id) : this(id, false) {}

		public VendorNotification(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public VendorNotification(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
