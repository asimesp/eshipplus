﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Administration;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Connect
{
	[Serializable]
	[Entity("RESTTransmissionLog", ReadOnly = false, Source = EntitySource.TableView)]
	public class RESTTransmissionLog : ObjectBase
	{
		private long _userId;

		private User _user;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[Property("Source", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public RESTTransmissionSource Source { get; set; }

		[Property("StreamData", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string StreamData { get; set; }

		[Property("AuthenticationData", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string AuthenticationData { get; set; }

		[Property("LogDateTime", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime LogDateTime { get; set; }

		[Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long UserId
		{
			get
			{
				if (_user != null) _userId = _user.Id;
				return _userId;
			}
			set
			{
				_userId = value;
				if (_user != null && value != _user.Id) _user = null;
			}
		}

		public User User
		{
			get { return _userId == default(long) ? null : _user ?? (_user = new User(_userId)); }
			set
			{
				_user = value;
				_userId = value == null ? default(long) : value.Id;
			}
		}

		public void Log()
		{
			Insert();
		}

		public RESTTransmissionLog(long id)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
		}

		public RESTTransmissionLog(DbDataReader reader)
		{
			Load(reader);
		}

		public RESTTransmissionLog()
		{

		}
	}
}
