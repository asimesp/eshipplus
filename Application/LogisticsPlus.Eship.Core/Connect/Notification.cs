﻿using System;
using System.Data;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Connect
{
	[Serializable]
	public class Notification : TenantBase
	{
		[EnableSnapShot("Milestone")]
		[Property("Milestone", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public Milestone Milestone { get; set; }

		[EnableSnapShot("NotificationMethod", Description = "Notification Method")]
		[Property("NotificationMethod", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public NotificationMethod NotificationMethod { get; set; }

		[EnableSnapShot("Fax")]
		[Property("Fax", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Fax { get; set; }

		[EnableSnapShot("Email")]
		[Property("Email", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Email { get; set; }

		[EnableSnapShot("Enabled", Description = "Notification Enabled")]
		[Property("Enabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Enabled { get; set; }
	}
}
