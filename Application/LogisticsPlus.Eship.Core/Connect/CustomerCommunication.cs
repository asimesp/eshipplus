﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Connect
{
	[Serializable]
	[Entity("CustomerCommunication", ReadOnly = false, Source = EntitySource.TableView)]
	public class CustomerCommunication : Communication
	{
		private Customer _customer;

		private List<DocDeliveryDocTag> _docDeliveryDocTags;
		private List<CustomerNotification> _notifications;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("CustomerId", Description = "Customer Reference")]
		[Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		private long CustomerId { get; set; }

		[EnableSnapShot("StopVendorNotifications", Description = "Stop Vendor Notifications")]
		[Property("StopVendorNotifications", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool StopVendorNotifications { get; set; }

		[EnableSnapShot("Pickup204", Description = "Pickup 204 (Load Tender)")]
		[Property("Pickup204", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Pickup204 { get; set; }

		[EnableSnapShot("AcknowledgePickup204", Description = "Acknowledge Pickup 204 (Load Tender)")]
		[Property("AcknowledgePickup204", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool AcknowledgePickup204 { get; set; }

		[EnableSnapShot("Pickup997", Description = "Pickup 997 (Acknowledgement)")]
		[Property("Pickup997", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Pickup997 { get; set; }

		[EnableSnapShot("EmailDocDeliveryEnabled", Description = "Email Docucment Delivery Enabled")]
		[Property("EmailDocDeliveryEnabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool EmailDocDeliveryEnabled { get; set; }

		[EnableSnapShot("FtpDocDeliveryEnabled", Description = "FTP Docucment Delivery Enabled")]
		[Property("FtpDocDeliveryEnabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool FtpDocDeliveryEnabled { get; set; }

		[EnableSnapShot("DocDeliveryFtpUrl", Description = "Document Delivery Ftp Url")]
		[Property("DocDeliveryFtpUrl", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DocDeliveryFtpUrl { get; set; }

		[EnableSnapShot("DocDeliverySecureFtp", Description = "Document Delivery Secure Ftp")]
		[Property("DocDeliverySecureFtp", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool DocDeliverySecureFtp { get; set; }

		[EnableSnapShot("DocDeliveryFtpUsername", Description = "Document Delivery Ftp Username")]
		[Property("DocDeliveryFtpUsername", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DocDeliveryFtpUsername { get; set; }

		[EnableSnapShot("DocDeliveryFtpPassword", Description = "Document Delivery Ftp Password")]
		[Property("DocDeliveryFtpPassword", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DocDeliveryFtpPassword { get; set; }

		[EnableSnapShot("DocDeliveryFtpDefaultFolder", Description = "Document Delivery Ftp Default Folder")]
		[Property("DocDeliveryFtpDefaultFolder", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DocDeliveryFtpDefaultFolder { get; set; }

		[EnableSnapShot("DeliverBolDoc", Description = "Delivery Shipment Bill of Lading")]
		[Property("DeliverBolDoc", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool DeliverBolDoc { get; set; }

		[EnableSnapShot("DeliverShipmentStatementDoc", Description = "Delivery Shipment Statement")]
		[Property("DeliverShipmentStatementDoc", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool DeliverShipmentStatementDoc { get; set; }

		[EnableSnapShot("DeliverInvoiceDoc", Description = "Deliver Invoice")]
		[Property("DeliverInvoiceDoc", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool DeliverInvoiceDoc { get; set; }

		[EnableSnapShot("DeliverInternalShipmentDocs", Description = "Deliver Internal Shipment Documents")]
		[Property("DeliverInternalShipmentDocs", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool DeliverInternalShipmentDocs { get; set; }

		[EnableSnapShot("HoldShipmentDocsTillInvoiced", Description = "Hold Shipment Document until Invoiced")]
		[Property("HoldShipmentDocsTillInvoiced", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool HoldShipmentDocsTillInvoiced { get; set; }

		[EnableSnapShot("StartDocDeliveriesFrom", Description = "Start Document Deliveries From")]
		[Property("StartDocDeliveriesFrom", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime StartDocDeliveriesFrom { get; set; }

		[EnableSnapShot("SendAveryLabel", Description = "Send AVery label via Email")]
		[Property("SendAveryLabel", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool SendAveryLabel { get; set; }

		[EnableSnapShot("SendStandardLabel", Description = "Send Standard Label via Email")]
		[Property("SendStandardLabel", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool SendStandardLabel { get; set; }

		[EnableSnapShot("DocumentLinkAccessKey", Description = "Document Link Access Key")]
		[Property("DocumentLinkAccessKey", AutoValueOnInsert = false, DataType = SqlDbType.UniqueIdentifier, Key = false)]
		public Guid DocumentLinkAccessKey { get; set; }

		public Customer Customer
		{
			get { return CustomerId == default(long) ? null : _customer ?? (_customer = new Customer(CustomerId)); }
			set
			{
				_customer = value;
				CustomerId = value == null ? default(long) : value.Id;
			}
		}

		public List<DocDeliveryDocTag> DocDeliveryDocTags
		{
			get
			{
				if (_docDeliveryDocTags == null) LoadDocDeliveryDocumentTags();
				return _docDeliveryDocTags;
			}
			set { _docDeliveryDocTags = value; }
		}
		public List<CustomerNotification> Notifications
		{
			get
			{
				if (_notifications == null) LoadNotifications();
				return _notifications;
			}
			set { _notifications = value; }
		}

		public bool IsNew { get { return Id == default(long); } }

		public CustomerCommunication() {}

		public CustomerCommunication(long id) : this(id, false) {}

		public CustomerCommunication(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public CustomerCommunication(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

		public void LoadCollections()
		{
			LoadDocDeliveryDocumentTags();
			LoadNotifications();
		}

		private void LoadDocDeliveryDocumentTags()
		{
			_docDeliveryDocTags = new List<DocDeliveryDocTag>();
			const string query = "Select * from DocDeliveryDocTag where CustomerCommunicationId = @CustomerCommunicationId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "CustomerCommunicationId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_docDeliveryDocTags.Add(new DocDeliveryDocTag(reader));
			Connection.Close();
		}

		private void LoadNotifications()
		{
			_notifications = new List<CustomerNotification>();
			const string query = @"SELECT * FROM CustomerNotification WHERE CustomerCommunicationId = @CustomerCommunicationId AND TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "CustomerCommunicationId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_notifications.Add(new CustomerNotification(reader));
			Connection.Close();
		}


	}
}