﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Connect
{
	[Serializable]
	[Entity("FaxTransmission", ReadOnly = false, Source = EntitySource.TableView)]
	public class FaxTransmission : TenantBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("ShipmentNumber", Description = "Shipment Number")]
		[Property("ShipmentNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ShipmentNumber { get; set; }

		[EnableSnapShot("SendDateTime", Description = "Send Date Time")]
		[Property("SendDateTime", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime SendDateTime { get; set; }

		[EnableSnapShot("ResponseDateTime", Description = "Response Date Time")]
		[Property("ResponseDateTime", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime ResponseDateTime { get; set; }

		[EnableSnapShot("LastStatusCheckDateTime", Description = "Last Status Check Date Time")]
		[Property("LastStatusCheckDateTime", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime LastStatusCheckDateTime { get; set; }

		[EnableSnapShot("Message", Description = "Message")]
		[Property("Message", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Message { get; set; }

		[EnableSnapShot("ResolutionComment", Description = "Resolution Comment")]
		[Property("ResolutionComment", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ResolutionComment { get; set; }

		[EnableSnapShot("Resolved", Description = "Resolution Comment")]
		[Property("Resolved", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Resolved { get; set; }

		[EnableSnapShot("Status", Description = "Fax Status")]
		[Property("Status", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public FaxTransmissionStatus Status { get; set; }

		[EnableSnapShot("TransmissionKey", Description = "Transmission Key")]
		[Property("TransmissionKey", AutoValueOnInsert = false, DataType = SqlDbType.UniqueIdentifier, Key = false)]
		public Guid TransmissionKey { get; set; }

		[EnableSnapShot("ExternalReference", Description = "External Reference")]
		[Property("ExternalReference", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ExternalReference { get; set; }

        [EnableSnapShot("FaxWrapper", Description = "Fax Wrapper")]
        [Property("FaxWrapper", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string FaxWrapper { get; set; }
		
		[EnableSnapShot("ServiceProvider", Description = "Service Provider")]
		[Property("ServiceProvider", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ServiceProvider ServiceProvider { get; set; }
		
		public bool IsNew { get { return Id == default(long); } }

		public FaxTransmission() {}

		public FaxTransmission(long id) : this(id, false) {}

		public FaxTransmission(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public FaxTransmission(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}