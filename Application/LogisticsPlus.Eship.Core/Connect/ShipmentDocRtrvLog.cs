﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Connect
{
	[Serializable]
	[Entity("ShipmentDocRtrvLog", ReadOnly = false, Source = EntitySource.TableView)]
	public class ShipmentDocRtrvLog : TenantBase
	{
		private long _communicationId;
		private long _userId;

		private User _user;
		private VendorCommunication _communication;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("ShipmentNumber", Description = "Shipment Number")]
		[Property("ShipmentNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ShipmentNumber { get; set; }

		[EnableSnapShot("DateCreated", Description = "Date Created")]
		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[EnableSnapShot("ProNumber", Description = "PRO Number")]
		[Property("ProNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ProNumber { get; set; }

		[EnableSnapShot("PodDate", Description = "POD Document Date")]
		[Property("PodDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime PodDate { get; set; }

		[EnableSnapShot("BolDate", Description = "BOL Document Date")]
		[Property("BolDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime BolDate { get; set; }

		[EnableSnapShot("WniDate", Description = "WNI Document Date")]
		[Property("WniDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime WniDate { get; set; }

		[EnableSnapShot("ExpirationDate", Description = "Expiration Date")]
		[Property("ExpirationDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime ExpirationDate { get; set; }

		[EnableSnapShot("CommunicationId", Description = "Communication Settings Reference")]
		[Property("CommunicationId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long CommunicationId
		{
			get { return _communicationId; }
			set
			{
				_communicationId = value;
				if (_communication != null && value != _communication.Id) _communication = null;
			}
		}

		[EnableSnapShot("UserId", Description = "User Reference")]
		[Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long UserId
		{
			get { return _userId; }
			set
			{
				_userId = value;
				if (_user != null && value != _user.Id) _user = null;
			}
		}

		public VendorCommunication Communication
		{
			get { return CommunicationId == default(long) ? null : _communication ?? (_communication = new VendorCommunication(CommunicationId)); }
			set
			{
				_communication = value;
				CommunicationId = value == null ? default(long) : value.Id;
			}
		}
		public User User
		{
			get { return _userId == default(long) ? null : _user ?? (_user = new User(_userId, false)); }
			set
			{
				_user = value;
				_userId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public ShipmentDocRtrvLog()
		{
			
		}

		public ShipmentDocRtrvLog(long id) : this(id, false)
		{
		}

		public ShipmentDocRtrvLog(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public ShipmentDocRtrvLog(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

		public Shipment RetrieveCorrespondingShipment()
		{
			Shipment shipment = null;
			const string query =
				@"Select * from Shipment where ShipmentNumber = @ShipmentNumber And TenantId = @TenantId";
			var parameters = new Dictionary<string, object> {{"TenantId", TenantId}, {"ShipmentNumber", ShipmentNumber}};
			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					shipment = new Shipment(reader);
			Connection.Close();
			return shipment;
		}
	}
}
