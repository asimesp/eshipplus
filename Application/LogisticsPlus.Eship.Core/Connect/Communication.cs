﻿using System;
using System.Data;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Connect
{
	[Serializable]
	public class Communication : TenantBase
	{
		[EnableSnapShot("ConnectGuid", Description = "Connect Unique Identifier")]
		[Property("ConnectGuid", AutoValueOnInsert = false, DataType = SqlDbType.UniqueIdentifier, Key = false)]
		public Guid ConnectGuid { get; set; }

		[EnableSnapShot("EnableWebServiceAPIAccess", Description = "Enable Web Service API Access")]
		[Property("EnableWebServiceAPIAccess", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool EnableWebServiceAPIAccess { get; set; }

		[EnableSnapShot("EdiEnabled", Description = "EDI Enabled")]
		[Property("EdiEnabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool EdiEnabled { get; set; }

		[EnableSnapShot("EdiCode", Description = "EDI Code")]
		[Property("EdiCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string EdiCode { get; set; }

		[EnableSnapShot("EdiVANUrl", Description = "Edi VAN Url")]
		[Property("EdiVANUrl", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string EdiVANUrl { get; set; }

		[EnableSnapShot("SecureEdiVAN", Description = "Secure Edi VAN (SSL)")]
		[Property("SecureEdiVAN", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool SecureEdiVAN { get; set; }

		[EnableSnapShot("EdiVANUsername", Description = "Edi VAN Username")]
		[Property("EdiVANUsername", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string EdiVANUsername { get; set; }

		[EnableSnapShot("EdiVANPassword", Description = "Edi VAN Password")]
		[Property("EdiVANPassword", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string EdiVANPassword { get; set; }

		[EnableSnapShot("EdiVANDefaultFolder", Description = "Edi VAN Default Folder")]
		[Property("EdiVANDefaultFolder", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string EdiVANDefaultFolder { get; set; }

		[EnableSnapShot("EdiVANEnvelopePath", Description = "Edi VAN Envelope Path")]
		[Property("EdiVANEnvelopePath", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string EdiVANEnvelopePath { get; set; }

		[EnableSnapShot("FtpEnabled", Description = "FTP Enabled")]
		[Property("FtpEnabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool FtpEnabled { get; set; }

		[EnableSnapShot("FtpUrl", Description = "Ftp Url")]
		[Property("FtpUrl", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string FtpUrl { get; set; }

		[EnableSnapShot("SecureFtp", Description = "Secure Ftp")]
		[Property("SecureFtp", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool SecureFtp { get; set; }

		[EnableSnapShot("SSHFtp", Description = "SSH Ftp")]
		[Property("SSHFtp", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool SSHFtp { get; set; }

		[EnableSnapShot("FtpUsername", Description = "Ftp Username")]
		[Property("FtpUsername", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string FtpUsername { get; set; }

		[EnableSnapShot("FtpPassword", Description = "Ftp Password")]
		[Property("FtpPassword", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string FtpPassword { get; set; }

		[EnableSnapShot("FtpDefaultFolder", Description = "Ftp Default Folder")]
		[Property("FtpDefaultFolder", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string FtpDefaultFolder { get; set; }

		[EnableSnapShot("UseSelectiveDropOff", Description = "Use Selective Drop Off")]
		[Property("UseSelectiveDropOff", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool UseSelectiveDropOff { get; set; }

		[EnableSnapShot("FtpDeleteFileAfterPickup", Description = "Delete File After FTP Pickup")]
		[Property("FtpDeleteFileAfterPickup", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool FtpDeleteFileAfterPickup { get; set; }

		[EnableSnapShot("EdiDeleteFileAfterPickup", Description = "Delete File After EDI Pickup")]
		[Property("EdiDeleteFileAfterPickup", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool EdiDeleteFileAfterPickup { get; set; }
	}
}
