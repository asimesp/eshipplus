﻿namespace LogisticsPlus.Eship.Core.Connect
{
	public enum ServiceProvider
	{
        // DO NOT RE-ORDER. SEE APPLICABLE JOINS ON DATABASE REFSERVICEPROVIDER
        None = 0,
		RingCentral,
		Interfax
	}
}
