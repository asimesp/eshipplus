﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Connect
{
	[Serializable]
	[Entity("DocDeliveryDocTag", ReadOnly = false, Source = EntitySource.TableView)]
	public class DocDeliveryDocTag : TenantBase
	{
		private long _documentTagId;
		private long _customerCommunicationId;

		private CustomerCommunication _customerCommunication;
		private DocumentTag _documentTag;

		[Property("CustomerCommunicationId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long CustomerCommunicationId
		{
			get
			{
				if (_customerCommunication != null) _customerCommunicationId = _customerCommunication.Id;
				return _customerCommunicationId;
			}
			set
			{
				_customerCommunicationId = value;
				if (_customerCommunication != null && value != _customerCommunication.Id) _customerCommunication = null;
			}
		}

		[Property("DocumentTagId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long DocumentTagId
		{
			get
			{
				if (_documentTag != null) _documentTagId = _documentTag.Id;
				return _documentTagId;
			}
			set
			{
				_documentTagId = value;
				if (_documentTag != null && value != _documentTag.Id) _documentTag = null;
			}
		}

		public CustomerCommunication CustomerCommunication
		{
			get { return _customerCommunication ?? (_customerCommunication = new CustomerCommunication(_customerCommunicationId)); }
			set
			{
				_customerCommunication = value;
				_customerCommunicationId = value == null ? default(long) : value.Id;
			}
		}
		public DocumentTag DocumentTag
		{
			get { return _documentTag ?? (_documentTag = new DocumentTag(_documentTagId)); }
			set
			{
				_documentTag = value;
				_documentTagId = value == null ? default(long) : value.Id;
			}
		}

		public DocDeliveryDocTag(){}

		public DocDeliveryDocTag(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			Insert();
		}

		public new void Delete()
		{
			base.Delete();
		}
	}
}
