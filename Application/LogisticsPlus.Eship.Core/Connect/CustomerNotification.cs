﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Connect
{
	[Serializable]
	[Entity("CustomerNotification", ReadOnly = false, Source = EntitySource.TableView)]
	public class CustomerNotification : Notification
	{
		private long _customerCommunicationId;

		private CustomerCommunication _customerCommunication;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("CustomerCommunicationId", Description = "Customer Communication Reference")]
		[Property("CustomerCommunicationId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long CustomerCommunicationId
		{
			get
			{
				if (_customerCommunication != null) _customerCommunicationId = _customerCommunication.Id;
				return _customerCommunicationId;
			}
			set
			{
				_customerCommunicationId = value;
				if (_customerCommunication != null && value != _customerCommunication.Id) _customerCommunication = null;
			}
		}

		public CustomerCommunication CustomerCommunication
		{
			get { return _customerCommunication ?? (_customerCommunication = new CustomerCommunication(_customerCommunicationId)); }
			set
			{
				_customerCommunication = value;
				_customerCommunicationId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew { get { return Id == default(long); } }

		public CustomerNotification() {}

		public CustomerNotification(long id) : this(id, false) {}

		public CustomerNotification(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public CustomerNotification(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
