﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Connect
{
	[Serializable]
	[Entity("XmlConnect", ReadOnly = false, Source = EntitySource.TableView)]
	public class XmlConnect : TenantBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("Xml", Description = "Xml Content")]
		[Property("Xml", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Xml { get; set; }

		[EnableSnapShot("ShipmentIdNumber", Description = "Shipment Id Number")]
		[Property("ShipmentIdNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ShipmentIdNumber { get; set; }

		[EnableSnapShot("ControlNumber", Description = "Transmission Control Number")]
		[Property("ControlNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ControlNumber { get; set; }

		[EnableSnapShot("OriginStreet1", Description = "Origin Street 1")]
		[Property("OriginStreet1", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginStreet1 { get; set; }

		[EnableSnapShot("OriginStreet2", Description = "Origin Street 2")]
		[Property("OriginStreet2", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginStreet2 { get; set; }

		[EnableSnapShot("OriginCity", Description = "Origin City")]
		[Property("OriginCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginCity { get; set; }

		[EnableSnapShot("OriginState", Description = "Origin State")]
		[Property("OriginState", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginState { get; set; }

		[EnableSnapShot("OriginCountryCode", Description = "Origin Country Code")]
		[Property("OriginCountryCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginCountryCode { get; set; }

		[EnableSnapShot("OriginCountryName", Description = "Origin Country Name")]
		[Property("OriginCountryName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginCountryName { get; set; }

		[EnableSnapShot("OriginPostalCode", Description = "Origin Postal Code")]
		[Property("OriginPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string OriginPostalCode { get; set; }

		[EnableSnapShot("DestinationStreet1", Description = "Destination Street 1")]
		[Property("DestinationStreet1", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationStreet1 { get; set; }

		[EnableSnapShot("DestinationStreet2", Description = "Destination Street 2")]
		[Property("DestinationStreet2", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationStreet2 { get; set; }

		[EnableSnapShot("DestinationCity", Description = "Destination City")]
		[Property("DestinationCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationCity { get; set; }

		[EnableSnapShot("DestinationState", Description = "Destination State")]
		[Property("DestinationState", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationState { get; set; }

		[EnableSnapShot("DestinationCountryCode", Description = "Destination Country Code")]
		[Property("DestinationCountryCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationCountryCode { get; set; }

		[EnableSnapShot("DestinationCountryName", Description = "Destination Country Name")]
		[Property("DestinationCountryName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationCountryName { get; set; }

		[EnableSnapShot("DestinationPostalCode", Description = "Destination Postal Code")]
		[Property("DestinationPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DestinationPostalCode { get; set; }

		[EnableSnapShot("ExpirationDate", Description = "Expiration Date")]
		[Property("ExpirationDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime ExpirationDate { get; set; }

		[EnableSnapShot("DateCreated", Description = "Date Created")]
		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[EnableSnapShot("ReceiptDate", Description = "Receipt Date")]
		[Property("ReceiptDate", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime ReceiptDate { get; set; }

		[EnableSnapShot("Direction")]
		[Property("Direction", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public Direction Direction { get; set; }

		[EnableSnapShot("DocumentType", Description = "EDI Document Type")]
		[Property("DocumentType", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public EdiDocumentType DocumentType { get; set; }

		[EnableSnapShot("CustomerNumber", Description = "Customer Number")]
		[Property("CustomerNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string CustomerNumber { get; set; }

		[EnableSnapShot("VendorNumber", Description = "Vendor Number")]
		[Property("VendorNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string VendorNumber { get; set; }

		[EnableSnapShot("VendorScac", Description = "Vendor Scac")]
		[Property("VendorScac", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string VendorScac { get; set; }

		[EnableSnapShot("PurchaseOrderNumber", Description = "Purchase Order Number")]
		[Property("PurchaseOrderNumber", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string PurchaseOrderNumber { get; set; }

		[EnableSnapShot("ShipperReference", Description = "Shipper Reference")]
		[Property("ShipperReference", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ShipperReference { get; set; }

		[EnableSnapShot("EquipmentDescriptionCode", Description = "Equipment Description Code")]
		[Property("EquipmentDescriptionCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string EquipmentDescriptionCode { get; set; }

		[EnableSnapShot("Status")]
		[Property("Status", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public XmlConnectStatus Status { get; set; }

		[EnableSnapShot("StatusMessage", Description = "Status Message")]
		[Property("StatusMessage", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string StatusMessage { get; set; }

		[EnableSnapShot("TransmissionOkay", Description = "Record Send/Acknowledgement Okay")]
		[Property("TransmissionOkay", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool TransmissionOkay { get; set; }

		[EnableSnapShot("FtpTransmission", Description = "Is Ftp Transmission")]
		[Property("FtpTransmission", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool FtpTransmission { get; set; }

		[EnableSnapShot("TotalStops", Description = "Total Stops")]
		[Property("TotalStops", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int TotalStops { get; set; }

		[EnableSnapShot("TotalWeight", Description = "Total Weight")]
		[Property("TotalWeight", AutoValueOnInsert = false, DataType = SqlDbType.Decimal, Key = false)]
		public decimal TotalWeight { get; set; }

		[EnableSnapShot("TotalPackages", Description = "Total Packages")]
		[Property("TotalPackages", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int TotalPackages { get; set; }

		[EnableSnapShot("TotalPieces", Description = "Total Pieces")]
		[Property("TotalPieces", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int TotalPieces { get; set; }

        [EnableSnapShot("VendorPro", Description = "Vendor Pro")]
        [Property("VendorPro", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string VendorPro { get; set; }


		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public XmlConnect()
		{		
		}

		public XmlConnect(long id)
			: this(id, false)
		{

		}

		public XmlConnect(long id, bool takeSnapShot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapShot) TakeSnapShot();
		}

		public XmlConnect(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

		public void SetEmpty()
		{
			ShipmentIdNumber = string.Empty;
			ControlNumber = string.Empty;
			OriginCity = string.Empty;
			OriginCountryCode = string.Empty;
			OriginCountryName = string.Empty;
			OriginPostalCode = string.Empty;
			OriginState = string.Empty;
			OriginStreet1 = string.Empty;
			OriginStreet2 = string.Empty;
			DestinationCity = string.Empty;
			DestinationCountryCode = string.Empty;
			DestinationCountryName = string.Empty;
			DestinationPostalCode = string.Empty;
			DestinationState = string.Empty;
			DestinationStreet1 = string.Empty;
			DestinationStreet2 = string.Empty;
			ExpirationDate = DateUtility.SystemEarliestDateTime;
			DateCreated = DateUtility.SystemEarliestDateTime;
			ReceiptDate = DateUtility.SystemEarliestDateTime;
			CustomerNumber = string.Empty;
			VendorNumber = string.Empty;
		    VendorPro = string.Empty;
			VendorScac = string.Empty;
			PurchaseOrderNumber = string.Empty;
			ShipperReference = string.Empty;
			EquipmentDescriptionCode = string.Empty;
			StatusMessage = string.Empty;
		}
	}
}
