﻿namespace LogisticsPlus.Eship.Core.Connect
{
    // DO NOT RE-ORDER. SEE APPLICABLE JOINS ON DATABASE REFEDIDOCUMENTTYPE
	public enum EdiDocumentType
	{
		EDI204 = 0,
		EDI210,
		EDI211,
		EDI214,
		EDI216,
		EDI990 = 5,
		EDI997
	}
}
