﻿using System;
using System.Data;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Administration
{
	[Serializable]
	public class Permission : TenantBase
	{
		[EnableSnapShot("Code")]
		[Property("Code", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Code { get; set; }

		[EnableSnapShot("Description")]
		[Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Description { get; set; }

		[EnableSnapShot("Grant")]
		[Property("Grant", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Grant { get; set; }

		[EnableSnapShot("Deny")]
		[Property("Deny", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Deny { get; set; }

		[EnableSnapShot("Modify")]
		[Property("Modify", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Modify { get; set; }

		[EnableSnapShot("Remove", Description = "Delete")]
		[Property("Remove", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false, Column = "Delete")]
		public bool Remove { get; set; }

		[EnableSnapShot("ModifyApplicable")]
		[Property("ModifyApplicable", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ModifyApplicable { get; set; }

		[EnableSnapShot("DeleteApplicable")]
		[Property("DeleteApplicable", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool DeleteApplicable { get; set; }
	}
}
