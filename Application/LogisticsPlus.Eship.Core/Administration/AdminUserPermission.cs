﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Administration
{
    [Serializable]
    [Entity("AdminUserPermission", ReadOnly = false, Source = EntitySource.TableView)]
    public class AdminUserPermission : Permission
    {
        private long _adminUserId;

		private AdminUser _adminUser;

    	public new long TenantId { get; private set; }

    	[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

    	[Property("AdminUserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long AdminUserId
		{
			get
			{
				if (_adminUser != null) _adminUserId = _adminUser.Id;
				return _adminUserId;
			}
			set
			{
				_adminUserId = value;
				if (_adminUser != null && value != _adminUser.Id) _adminUser = null;
			}
		}

		public AdminUser AdminUser
		{
			get { return _adminUser ?? (_adminUser = new AdminUser(_adminUserId)); }
			set
			{
				_adminUser = value;
				_adminUserId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public AdminUserPermission()
		{
			TenantId = default(long);
		}

		public AdminUserPermission(long id, bool takeSnapshot)
		{
			Id = id;
			TenantId = default(long);
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

        public AdminUserPermission(DbDataReader reader)
		{
			TenantId = default(long);
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
    }
}
