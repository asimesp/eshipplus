﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Administration
{
	[Serializable]
	[Entity("GroupPermission", ReadOnly = false, Source = EntitySource.TableView)]
	public class GroupPermission : Permission
	{
		private long _groupId;

		private Group _group;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[Property("GroupId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long GroupId
		{
			get
			{
				if (_group != null) _groupId = _group.Id;
				return _groupId;
			}
			set
			{
				_groupId = value;
				if (_group != null && value != _group.Id) _group = null;
			}
		}

		public Group Group
		{
			get { return _group ?? (_group = new Group(_groupId)); }
			set
			{
				_group = value;
				_groupId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public GroupPermission()
		{
		}

		public GroupPermission(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if(takeSnapshot) TakeSnapShot();
		}

		public GroupPermission(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

	}
}
