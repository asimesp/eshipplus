﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Administration
{
	[Serializable]
	[Entity("Group", ReadOnly = false, Source = EntitySource.TableView)]
	public class Group : TenantBase
	{
		private List<GroupPermission> _permissions;
		private List<GroupUser> _groupUsers;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("Name")]
		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

		[EnableSnapShot("Description")]
		[Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Description { get; set; }

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public List<GroupPermission> Permissions
		{
			get
			{
				if (_permissions == null) LoadPermissions();
				return _permissions;
			}
			set { _permissions = value; }
		}

		public List<GroupUser> GroupUsers
		{
			get
			{
				if (_groupUsers == null) LoadUsers();
				return _groupUsers;
			}
			set { _groupUsers = value; }
		}

		public Group()
		{
			
		}

		public Group(long id) : this(id, false)
		{
			
		}

		public Group(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if(takeSnapshot) TakeSnapShot();
		}

		public Group(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

		public void LoadCollections()
		{
			LoadPermissions();
			LoadUsers();
		}

		public List<User> RetrieveGroupUsers()
		{
			var users = new List<User>();
			const string query =
				@"Select [User].* from [User], GroupUser where GroupUser.[UserId] = [User].[Id] and GroupUser.TenantId = @TenantId 
				and GroupUser.GroupId = @GroupId order by [User].Username ASC, [User].FirstName ASC, [User].LastName ASC";
			var parameters = new Dictionary<string, object> { { "TenantId", TenantId }, { "GroupId", Id } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					users.Add(new User(reader));
			Connection.Close();
			return users;
		}

		private void LoadPermissions()
		{
			_permissions = new List<GroupPermission>();
			const string query = "Select * from GroupPermission where TenantId = @TenantId and GroupId = @GroupId order by Code ASC";
			var parameters = new Dictionary<string, object> {{"TenantId", TenantId}, {"GroupId", Id}};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_permissions.Add(new GroupPermission(reader));
			Connection.Close();
		}

		private void LoadUsers()
		{
			_groupUsers = new List<GroupUser>();
			const string query = "Select * from GroupUser where TenantId = @TenantId and GroupId = @GroupId";
			var parameters = new Dictionary<string, object> { { "TenantId", TenantId }, { "GroupId", Id } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_groupUsers.Add(new GroupUser(reader));
			Connection.Close();
		}
	}
}
