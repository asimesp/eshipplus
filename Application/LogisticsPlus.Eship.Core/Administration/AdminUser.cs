﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Administration
{
	[Serializable]
    [Entity("AdminUser", ReadOnly = false, Source = EntitySource.TableView)]
    public class AdminUser : ObjectBase
    {
        private static readonly TextEncryptor Encryptor = new TextEncryptor("Adu7UaLM3czua4SY3/b6uiGwjAzLsuctYq9GkBNgo1A=", "ig/EK2CKOfTml3qHvqcobQ==");

        private List<AdminUserPermission> _adminUserPermissions;


        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; private set; }

        [EnableSnapShot("Username")]
        [Property("Username", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Username { get; set; }

        [EnableSnapShot("FirstName", Description = "First Name")]
        [Property("FirstName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string FirstName { get; set; }

        [EnableSnapShot("LastName", Description = "Last Name")]
        [Property("LastName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string LastName { get; set; }

        [EnableSnapShot("Notes")]
        [Property("Notes", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Notes { get; set; }

        [EnableSnapShot("Email")]
        [Property("Email", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Email { get; set; }

        [EnableSnapShot("Enabled")]
        [Property("Enabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool Enabled { get; set; }

        [EnableSnapShot("EncryptedPassword")]
        [Property("EncryptedPassword", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false, Column = "Password")]
        private string EncryptedPassword { get; set; }

        public string Password
        {
            get { return Encryptor.Decrypt(EncryptedPassword); }
            set { EncryptedPassword = Encryptor.Encrypt(value); }
        }

        public bool IsNew
        {
            get { return Id == default(long); }
        }

        public List<AdminUserPermission> AdminUserPermissions
        {
            get
            {
                if (_adminUserPermissions == null) LoadUserPermissions();
                return _adminUserPermissions;
            }
            set { _adminUserPermissions = value; }
        }


        public AdminUser() { }

        public AdminUser(long id)
            : this(id, false) { }

        public AdminUser(long id, bool takeSnapShot)
        {
            Id = id;
            if (Id == default(long)) return;
            KeyLoaded = Load();
            if (takeSnapShot) TakeSnapShot();
        }

        public AdminUser(DbDataReader reader)
        {
            Load(reader);
        }

        public void Save()
        {
            if (Id == default(long)) Insert();
            else Update();
        }

        public new void Delete()
        {
            if (Id != default(long))
                base.Delete();
        }


		public void LoadCollections()
		{
			LoadUserPermissions();
		}

    	private void LoadUserPermissions()
        {
            _adminUserPermissions = new List<AdminUserPermission>();
            const string query = "SELECT * FROM AdminUserPermission WHERE AdminUserId = @UserId ORDER BY Code ASC";
            var parameters = new Dictionary<string, object> { { "UserId", Id } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _adminUserPermissions.Add(new AdminUserPermission(reader));
            Connection.Close();
        }
    }
}
