﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Administration
{
	[Serializable]
	[Entity("User", ReadOnly = false, Source = EntitySource.TableView)]
	public class User : Location
	{
		private static readonly TextEncryptor Encryptor = new TextEncryptor("Pda5Ciwyg8fxVOM51IDkINaCW29fCqLAy+NUKJt6wRw=", "druYXn8f3NYE0+Yrj7o7VQ==");

		private long _departmentId;
		private long _vendorPortalVendorId;

		private UserDepartment _department;
		private Vendor _vendorPortalVendor;

		private List<Permission> _condensedPermissions;
		private List<UserPermission> _userPermissions;
		private List<GroupPermission> _groupPermissions;
		private List<UserShipAs> _userShipAs;
		private List<GroupUser> _userGroups;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("Username")]
		[Property("Username", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Username { get; set; }

		[EnableSnapShot("FirstName", Description = "First Name")]
		[Property("FirstName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string FirstName { get; set; }

		[EnableSnapShot("LastName", Description = "Last Name")]
		[Property("LastName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LastName { get; set; }

        [EnableSnapShot("AdUserName", Description = "Active Directory Username")]
        [Property("AdUserName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string AdUserName { get; set; }

		[EnableSnapShot("TenantEmployee", Description = "Tenant Employee Designation")]
		[Property("TenantEmployee", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool TenantEmployee { get; set; }

		[EnableSnapShot("IsCarrierCoordinator", Description = "Is Carrier Coordinator Designation")]
		[Property("IsCarrierCoordinator", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool IsCarrierCoordinator { get; set; }

		[EnableSnapShot("IsShipmentCoordinator", Description = "Is Shipment Coordinator Designation")]
		[Property("IsShipmentCoordinator", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool IsShipmentCoordinator { get; set; }

		[EnableSnapShot("Phone")]
		[Property("Phone", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Phone { get; set; }

		[EnableSnapShot("Mobile")]
		[Property("Mobile", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Mobile { get; set; }

		[EnableSnapShot("Fax")]
		[Property("Fax", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Fax { get; set; }

		[EnableSnapShot("Email")]
		[Property("Email", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Email { get; set; }

		[EnableSnapShot("Enabled")]
		[Property("Enabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Enabled { get; set; }

		[EnableSnapShot("AlwaysShowRatingNotice", Description = "Always Show Rating Notice")]
		[Property("AlwaysShowRatingNotice", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool AlwaysShowRatingNotice { get; set; }

		[EnableSnapShot("ForcePasswordReset", Description = "User Must Reset Password on Next Login")]
		[Property("ForcePasswordReset", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ForcePasswordReset { get; set; }

		[EnableSnapShot("FailedLoginAttempts", Description = "User Failed Login Attempts")]
		[Property("FailedLoginAttempts", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int FailedLoginAttempts { get; set; }

		[EnableSnapShot("DepartmentId", Description = "User Department Reference")]
		[Property("DepartmentId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false, Column = "UserDepartmentId")]
		public long DepartmentId
		{
			get { return _departmentId; }
			set
			{
				_departmentId = value;
				if (_department != null && value != _department.Id) _department = null;
			}
		}

		[EnableSnapShot("CustomerId", Description = "Default Customer Reference")]
		[Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long CustomerId
		{
			get { return DefaultCustomer == null ? default(long) : DefaultCustomer.Id; }
			set
			{
				if (DefaultCustomer != null && DefaultCustomer.Id == value) return;
				DefaultCustomer = value == default(long) ? null : new Customer(value, false);
			}
		}

		[EnableSnapShot("EncryptedPassword")]
		[Property("EncryptedPassword", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false, Column = "Password")]
		private string EncryptedPassword { get; set; }

        [EnableSnapShot("AlwaysShowFinderParametersOnSearch", Description = "Always Show Finder Parameters On Search")]
        [Property("AlwaysShowFinderParametersOnSearch", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool AlwaysShowFinderParametersOnSearch { get; set; }

        [EnableSnapShot("QlikUserId", Description = "Qlik User Id")]
        [Property("QlikUserId", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string QlikUserId { get; set; }

        [EnableSnapShot("QlikUserDirectory", Description = "Qlik User Directory")]
        [Property("QlikUserDirectory", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string QlikUserDirectory { get; set; }

        [EnableSnapShot("DatLoadboardUsername", Description = "DAT Loadboard Username")]
        [Property("DatLoadboardUsername", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DatLoadboardUsername { get; set; }

        [EnableSnapShot("DatLoadboardPassword", Description = "DAT Loadboard Password")]
        [Property("DatLoadboardPassword", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DatLoadboardPassword { get; set; }


		[EnableSnapShot("VendorPortalVendorId", Description = "Vendor Portal VendorId")]
		[Property("VendorPortalVendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long VendorPortalVendorId
		{
			get { return _vendorPortalVendorId; }
			set
			{
				_vendorPortalVendorId = value;
				if (_vendorPortalVendor != null && _vendorPortalVendor.Id != value) _vendorPortalVendor = null;
			}
		}

		[EnableSnapShot("CanSeeAllVendorsInVendorPortal", Description = "User will see all vendor charges in Vendor Portal")]
		[Property("CanSeeAllVendorsInVendorPortal", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool CanSeeAllVendorsInVendorPortal { get; set; }



		public string Password
		{
			get { return Encryptor.Decrypt(EncryptedPassword); }
			set { EncryptedPassword = Encryptor.Encrypt(value); }
		}

		public Customer DefaultCustomer { get; set; }

		public UserDepartment Department
		{
			get { return _departmentId == default(long) ? null : _department ?? (_department = new UserDepartment(_departmentId, false)); }
			set
			{
				_department = value;
				_departmentId = value == null ? default(long) : value.Id;
			}
		}

		public Vendor VendorPortalVendor
		{
			get { return _vendorPortalVendor ?? (_vendorPortalVendor = new Vendor(_vendorPortalVendorId)); }
			set
			{
				_vendorPortalVendor = value;
				_vendorPortalVendorId = value == null ? default(long) : value.Id;
			}
		}

        public string FullName { get { return string.Format("{0} {1}", FirstName, LastName); } }

        public bool HasDatCredentials { get { return DatLoadboardUsername != string.Empty && DatLoadboardPassword != string.Empty; } }

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public List<UserPermission> UserPermissions
		{
			get
			{
				if (_userPermissions == null) LoadUserPermissions();
				return _userPermissions;
			}
			set { _userPermissions = value; }
		}

		public List<UserShipAs> UserShipAs
		{
			get
			{
				if (_userShipAs == null) LoadUserShipAs();
				return _userShipAs;
			}
			set { _userShipAs = value; }
		}

		public List<GroupPermission> GroupPermissions
		{
			get
			{
				if (_groupPermissions == null) LoadGroupPermissions();
				return _groupPermissions;
			}
			set { _groupPermissions = value; }
		}

		public List<Permission> AllPermissions
		{
			get { return CondensedPermissions(); }
		}

		public List<GroupUser> UserGroups
		{
			get
			{
				if (_userGroups == null) LoadUserGroups();
				return _userGroups;
			}
		}

		public User()
		{		
		}

		public User(long id)
			: this(id, false)
		{

		}

		public User(long id, bool takeSnapShot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapShot) TakeSnapShot();
		}

		public User(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

		public void PurgeUserSearchProfileDefault()
		{
			const string query = @"delete from SearchProfileDefault where UserId =  @UserId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> {{"UserId", Id}, {"TenantId", TenantId}};
			ExecuteNonQuery(query, parameters);
		}

		public void PurgeUserRecordLocks()
		{
			const string query = @"delete from EntityLock where LockUserId = @UserId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "UserId", Id }, { "TenantId", TenantId } };
			ExecuteNonQuery(query, parameters);
		}

		public void LoadCollections()
		{
			LoadUserPermissions();
			LoadGroupPermissions();
			LoadUserShipAs();
			LoadUserGroups();
		}

		private List<Permission> CondensedPermissions()
		{
			if (_condensedPermissions != null) return _condensedPermissions;

			// get condensed permission
			var permissions = new Dictionary<string, Permission>();
			ProcessPermissions(permissions, UserPermissions);
			ProcessPermissions(permissions, GroupPermissions);
			_condensedPermissions = permissions.Values.ToList();

			return _condensedPermissions;
		}

		public List<Group> RetrieveUsersGroups()
		{
			var groups = new List<Group>();
			const string query =
				@"Select [Group].* from [Group], GroupUser where GroupUser.[GroupId] = [Group].[Id] and GroupUser.TenantId = @TenantId 
				and GroupUser.UserId = @UserId order by [Group].[Name] ASC";
			var parameters = new Dictionary<string, object> { { "TenantId", TenantId }, { "UserId", Id } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					groups.Add(new Group(reader));
			Connection.Close();
			return groups;
		}

		public List<CustomerServiceRepresentative> RetrieveCustomersThatUserRepresents()
		{
			var customerReps = new List<CustomerServiceRepresentative>();
			const string query =
				@"Select CustomerServiceRepresentative.* from CustomerServiceRepresentative where CustomerServiceRepresentative.TenantId = @TenantId 
				and CustomerServiceRepresentative.UserId = @UserId";
			var parameters = new Dictionary<string, object> { { "TenantId", TenantId }, { "UserId", Id } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					customerReps.Add(new CustomerServiceRepresentative(reader));
			Connection.Close();
			return customerReps;
		}

		public List<Customer> RetrieveShipAsCustomers()
		{
			var customers = new List<Customer>();
			const string query =
				@"select Customer.* from Customer, UserShipAs where Customer.[Id] = UserShipAs.CustomerId 
				and UserShipAs.UserId = @UserId and UserShipAs.TenantId = @TenantId 
				order by Customer.[Name] ASC, Customer.CustomerNumber ASC";
			var parameters = new Dictionary<string, object> { { "TenantId", TenantId }, { "UserId", Id } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					customers.Add(new Customer(reader));
			Connection.Close();
			return customers;
		}

		private static void ProcessPermissions<T>(IDictionary<string, Permission> permissions, IEnumerable<T> processSet)
			where T : Permission
		{
			foreach (var permission in processSet)
			{
				if (!permissions.ContainsKey(permission.Code))
					permissions.Add(permission.Code, new Permission {Code = permission.Code, Description = permission.Description});

				permissions[permission.Code].Grant = permission.Grant ? permission.Grant : permissions[permission.Code].Grant;
				permissions[permission.Code].Modify = permission.Modify ? permission.Modify : permissions[permission.Code].Modify;
				permissions[permission.Code].Remove = permission.Remove ? permission.Remove : permissions[permission.Code].Remove;
				permissions[permission.Code].Deny = permission.Deny ? permission.Deny : permissions[permission.Code].Deny;
				permissions[permission.Code].ModifyApplicable = permission.ModifyApplicable
				                                                	? permission.ModifyApplicable
				                                                	: permissions[permission.Code].ModifyApplicable;
				permissions[permission.Code].DeleteApplicable = permission.DeleteApplicable
				                                                	? permission.DeleteApplicable
				                                                	: permissions[permission.Code].DeleteApplicable;
			}
		}

		private void LoadGroupPermissions()
		{
			_groupPermissions = new List<GroupPermission>();
			const string query =
				"Select * from UsersGroupPermissions where UserId = @UserId and TenantId = @TenantId Order By Code ASC";
			var parameters = new Dictionary<string, object> {{"UserId", Id}, {"TenantId", TenantId}};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_groupPermissions.Add(new GroupPermission(reader));
			Connection.Close();
		}

		private void LoadUserPermissions()
		{
			_userPermissions = new List<UserPermission>();
			const string query = "select * from UserPermission where UserId = @UserId and TenantId = @TenantId Order By Code ASC";
			var parameters = new Dictionary<string, object> {{"UserId", Id}, {"TenantId", TenantId}};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_userPermissions.Add(new UserPermission(reader));
			Connection.Close();
		}

		private void LoadUserShipAs()
		{
			_userShipAs = new List<UserShipAs>();
			const string query = "select * from UserShipAs where UserId = @UserId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> {{"UserId", Id}, {"TenantId", TenantId}};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_userShipAs.Add(new UserShipAs(reader));
			Connection.Close();
		}

		private void LoadUserGroups()
		{
			_userGroups = new List<GroupUser>();
			const string query = "select * from GroupUser where UserId = @UserId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> {{"UserId", Id}, {"TenantId", TenantId}};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_userGroups.Add(new GroupUser(reader));
			Connection.Close();
		}
	}
}
