﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Administration
{
	[Serializable]
	[Entity("GroupUser", ReadOnly = false, Source = EntitySource.TableView)]
	public class GroupUser : TenantBase
	{
		private long _userId;
		private long _groupId;

		private Group _group;
		private User _user;

		[Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long UserId
		{
			get
			{
				if (_user != null) _userId = _user.Id;
				return _userId;
			}
			set
			{
				_userId = value;
				if (_user != null && value != _user.Id) _user = null;
			}
		}

		[Property("GroupId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long GroupId
		{
			get
			{
				if (_group != null) _groupId = _group.Id;
				return _groupId;
			}
			set
			{
				_groupId = value;
				if (_group != null && value != _group.Id) _group = null;
			}
		}

		public Group Group
		{
			get { return _group ?? (_group = new Group(_groupId)); }
			set
			{
				_group = value;
				_groupId = value == null ? default(long) : value.Id;
			}
		}
		public User User
		{
			get { return _user ?? (_user = new User(_userId)); }
			set
			{
				_user = value;
				_userId = value == null ? default(long) : value.Id;
			}
		}

		public GroupUser()
		{
			
		}

		public GroupUser(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			Insert();
		}

		public new void Delete()
		{
			base.Delete();
		}
	}
}
