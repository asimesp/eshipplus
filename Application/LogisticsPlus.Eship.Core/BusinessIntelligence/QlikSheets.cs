﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
	[Serializable]
	[Entity("QlikSheet", ReadOnly = false, Source = EntitySource.TableView)]
	public class QlikSheet : TenantBase
	{
		private long _reportConfigurationId;

		private ReportConfiguration _reportConfiguration;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("Name")]
		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

		[EnableSnapShot("Link")]
		[Property("Link", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Link { get; set; }

		[EnableSnapShot("DisplayOrder", Description = "Display Order")]
		[Property("DisplayOrder", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public int DisplayOrder { get; set; }

		[Property("ReportConfigurationId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ReportConfigurationId
		{
			get
			{
				if (_reportConfiguration != null) _reportConfigurationId = _reportConfiguration.Id;
				return _reportConfigurationId;
			}
			set
			{
				_reportConfigurationId = value;
				if (_reportConfiguration != null && value != _reportConfiguration.Id) _reportConfiguration = null;
			}
		}

		public ReportConfiguration ReportConfiguration
		{
			get { return _reportConfiguration ?? (_reportConfiguration = new ReportConfiguration(_reportConfigurationId)); }
			set
			{
				_reportConfiguration = value;
				_reportConfigurationId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public QlikSheet() { }

		public QlikSheet(long id) : this(id, false) { }

		public QlikSheet(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public QlikSheet(DbDataReader reader)
		{
			Load(reader);
		}

        public void Save()
        {
            if (Id == default(long)) Insert();
            else Update();
        }

        public new void Delete()
        {
            if (Id != default(long))
                base.Delete();
        }
	}
}
