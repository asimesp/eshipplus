﻿using System;

namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
	[Serializable]
	public class SortColumn
	{
		public string ReportColumnName { get; set; }
		public int Priority { get; set; }
		public SortDirection Direction { get; set; }
	}
}
