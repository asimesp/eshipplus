﻿using System;
using System.Data;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
	[Serializable]
	public class RequiredValueParameter
	{
		public string Name { get; set; }
		public SqlDbType DataType { get; set; }
		public bool ReadOnly { get; set; }

		[XmlElement(IsNullable = true)]
		public string DefaultValue { get; set; }
	}
}
