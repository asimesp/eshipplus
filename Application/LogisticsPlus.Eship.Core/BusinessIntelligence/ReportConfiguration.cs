﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Administration;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
	[Serializable]
	[Entity("ReportConfiguration", ReadOnly = false, Source = EntitySource.TableView)]
	public class ReportConfiguration : TenantBase
	{
		private long _reportTemplateId;
		private long _customerGroupId;
		private long _vendorGroupId;
		private long _userId;

		private CustomerGroup _customerGroup;
		private VendorGroup _vendorGroup;
		private ReportTemplate _reportTemplate;
		private User _user;

		private List<QlikSheet> _qlikSheets;
		private List<ReportCustomizationUser> _selectUsers;
	    private List<ReportCustomizationGroup> _selectGroups;

        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("Name")]
		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

		[EnableSnapShot("Description")]
		[Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Description { get; set; }

		[EnableSnapShot("Visibility", Description = "Report Configuration Visibility")]
		[Property("Visibility", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ReportConfigurationVisibility Visibility { get; set; }

		[EnableSnapShot("SerializedCustomization", Description = "Serialized Customization")]
		[Property("SerializedCustomization", AutoValueOnInsert = false, DataType = SqlDbType.Text, Key = false)]
		public string SerializedCustomization { get; set; }

		[EnableSnapShot("CustomerGroupId", Description = "Customer Group Reference")]
		[Property("CustomerGroupId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long CustomerGroupId
		{
			get
			{
				if (_customerGroup != null) _customerGroupId = _customerGroup.Id;
				return _customerGroupId;
			}
			set
			{
				_customerGroupId = value;
				if (_customerGroup != null && value != _customerGroup.Id) _customerGroup = null;
			}
		}

		[EnableSnapShot("VendorGroupId", Description = "Vendor Group Reference")]
		[Property("VendorGroupId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long VendorGroupId
		{
			get
			{
				if (_vendorGroup != null) _vendorGroupId = _vendorGroup.Id;
				return _vendorGroupId;
			}
			set
			{
				_vendorGroupId = value;
				if (_vendorGroup != null && value != _vendorGroup.Id) _vendorGroup = null;
			}
		}

		[EnableSnapShot("ReadonlyCustomerGroupFilter", Description = "Customer Group Filter is Readonly")]
		[Property("ReadonlyCustomerGroupFilter", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ReadonlyCustomerGroupFilter { get; set; }

		[EnableSnapShot("ReadonlyVendorGroupFilter", Description = "Vendor Group Filter is Readonly")]
		[Property("ReadonlyVendorGroupFilter", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ReadonlyVendorGroupFilter { get; set; }

		[EnableSnapShot("QlikConfiguration", Description = "Qlik Configuration")]
		[Property("QlikConfiguration", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool QlikConfiguration { get; set; }

		[EnableSnapShot("UserId", Description = "User Reference")]
		[Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long UserId
		{
			get { return _userId; }
			set
			{
				_userId = value;
				if (_user != null && value != _user.Id) _user = null;
			}
		}

		[EnableSnapShot("ReportTemplateId", Description = "ReportTemplate Reference")]
		[Property("ReportTemplateId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ReportTemplateId
		{
			get { return _reportTemplateId; }
			set
			{
				_reportTemplateId = value;
				if (_reportTemplate != null && value != _reportTemplate.Id) _reportTemplate = null;
			}
		}

        [EnableSnapShot("LastRun", Description = "Last Run Date")]
        [Property("LastRun", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime LastRun { get; set; }

		public ReportTemplate ReportTemplate
		{
			get { return _reportTemplateId == default(long) ? null : _reportTemplate ?? (_reportTemplate = new ReportTemplate(_reportTemplateId)); }
			set
			{
				_reportTemplate = value;
				_reportTemplateId = value == null ? default(long) : value.Id;
			}
		}
		public CustomerGroup CustomerGroup
		{
			get { return _customerGroup ?? (_customerGroup = new CustomerGroup(_customerGroupId)); }
			set
			{
				_customerGroup = value;
				_customerGroupId = value == null ? default(long) : value.Id;
			}
		}
		public VendorGroup VendorGroup
		{
			get { return _vendorGroup ?? (_vendorGroup = new VendorGroup(_vendorGroupId)); }
			set
			{
				_vendorGroup = value;
				_vendorGroupId = value == null ? default(long) : value.Id;
			}
		}
		public User User
		{
			get { return _userId == default(long) ? null : _user ?? (_user = new User(_userId, false)); }
			set
			{
				_user = value;
				_userId = value == null ? default(long) : value.Id;
			}
		}

		public List<QlikSheet> QlikSheets
		{
			get
			{
				if (_qlikSheets == null) LoadQlikSheets();
				return _qlikSheets;
			}
			set { _qlikSheets = value; }
		}
		public List<ReportCustomizationUser> SelectUsers
		{
			get
			{
				if (_selectUsers == null) LoadSelectUsers();
				return _selectUsers;
			}
			set { _selectUsers = value; }
		}
	    public List<ReportCustomizationGroup> SelectGroups
	    {
	        get
	        {
	            if (_selectGroups == null) LoadSelectGroups();
	            return _selectGroups;
	        }
	        set { _selectGroups = value; }
	    }

        public bool IsNew
		{
			get { return Id == default(long); }
		}

		public ReportConfiguration()
		{
			
		}

		public ReportConfiguration(long id) : this(id, false)
		{
		}

		public ReportConfiguration(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public ReportConfiguration(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}

		public void LoadCollections()
		{
			LoadSelectUsers();
		    LoadSelectGroups();
            LoadQlikSheets();
		}

		private void LoadSelectUsers()
		{
			_selectUsers = new List<ReportCustomizationUser>();
			const string query =
				"Select * from ReportCustomizationUser where ReportConfigurationId = @ReportConfigurationId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "ReportConfigurationId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_selectUsers.Add(new ReportCustomizationUser(reader));
			Connection.Close();
		}

	    private void LoadSelectGroups()
	    {
	        _selectGroups = new List<ReportCustomizationGroup>();
	        const string query =
                "Select * from ReportCustomizationGroup where ReportConfigurationId = @ReportConfigurationId and TenantId = @TenantId";
	        var parameters = new Dictionary<string, object> { { "ReportConfigurationId", Id }, { "TenantId", TenantId } };
	        using (var reader = GetReader(query, parameters))
	            while (reader.Read())
	                _selectGroups.Add(new ReportCustomizationGroup(reader));
	        Connection.Close();
	    }

        private void LoadQlikSheets()
		{
			_qlikSheets = new List<QlikSheet>();
			const string query =
				"Select * from QlikSheet where ReportConfigurationId = @ReportConfigurationId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> { { "ReportConfigurationId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_qlikSheets.Add(new QlikSheet(reader));
			Connection.Close();
		}

		public List<User> RetrieveSelectUsers()
		{
			var users = new List<User>();
			const string query =
				@"SELECT [User].* FROM [User], ReportCustomizationUser 
					WHERE ReportCustomizationUser.[UserId] = [User].[Id] and ReportCustomizationUser.TenantId = @TenantId 
						AND ReportCustomizationUser.ReportConfigurationId = @ReportConfigurationId 
					ORDER BY [User].Username ASC, [User].FirstName ASC, [User].LastName ASC";
			var parameters = new Dictionary<string, object> { { "TenantId", TenantId }, { "ReportConfigurationId", Id } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					users.Add(new User(reader));
			Connection.Close();
			return users;
		}

	    public List<Group> RetrieveSelectGroups()
	    {
	        var groups = new List<Group>();
	        const string query =
                @"SELECT [Group].* FROM [Group], ReportCustomizationGroup
					WHERE ReportCustomizationGroup.[GroupId] = [Group].[Id] AND ReportCustomizationGroup.TenantId = @TenantId 
                        AND ReportCustomizationGroup.ReportConfigurationId = @ReportConfigurationId 
					ORDER BY [Group].Name ASC";
	        var parameters = new Dictionary<string, object> { { "TenantId", TenantId }, { "ReportConfigurationId", Id } };
	        using (var reader = GetReader(query, parameters))
	            while (reader.Read())
	                groups.Add(new Group(reader));
	        Connection.Close();
	        return groups;
	    }
    }
}
