﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Administration;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
    [Serializable]
    [Entity("BackgroundReportRunRecord", ReadOnly = false, Source = EntitySource.TableView)]
    public class BackgroundReportRunRecord : TenantBase
    {
        private long _userId;

        private User _user;

        [Property("Id",AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; protected set; }

        [EnableSnapShot("Query")]
        [Property("Query",AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Query { get; set; }

        [EnableSnapShot("Query")]
        [Property("Parameters",AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string Parameters { get; set; }

        [EnableSnapShot("DateCreated")]
        [Property("DateCreated",AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime DateCreated { get; set; }

        [EnableSnapShot("ExpirationDate")]
        [Property("ExpirationDate",AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime ExpirationDate { get; set; }

        [EnableSnapShot("ReportCustomization")]
        [Property("ReportCustomization",AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ReportCustomization { get; set; }

        [EnableSnapShot("CompletedReportFileName")]
        [Property("CompletedReportFileName",AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string CompletedReportFileName { get; set; }

        [EnableSnapShot("ReportName")]
        [Property("ReportName",AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string ReportName { get; set; }


        [EnableSnapShot("UserId")]
        [Property("UserId",AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long UserId
        {
            get
            {
                if (_user != null) _userId = _user.Id;
                return _userId;
            }
            set
            {
                _userId = value;
                if (_user != null) _user = null;
            }
        }

        public User User
        {
            get { return _user ?? (_user = new User(_userId)); }
            set
            {
                _user = value;
                _userId = value == null ? default(long) : value.Id;
            }
        }

        public bool IsNew
        {
            get { return Id == default(long); }
        }

        public BackgroundReportRunRecord()
        {

        }

        public BackgroundReportRunRecord(long id) : this(id, false)
        {
        }

        public BackgroundReportRunRecord(long id, bool takeSnapshot)
        {
            Id = id;
            if (Id == default(long)) return;
            KeyLoaded = Load();
            if (takeSnapshot) TakeSnapShot();
        }

        public BackgroundReportRunRecord(DbDataReader reader)
        {
            Load(reader);
        }

        public void Save()
        {
            if (Id == default(long)) Insert();
            else Update();
        }

        public new void Delete()
        {
            if (Id != default(long))
                base.Delete();
        }

        public void Reload(bool takeSnapshot = false)
        {
            if (Id == default(long))
            {
                return;
            }
            KeyLoaded = Load();
            if (takeSnapshot) TakeSnapShot();
        }
    }
}
