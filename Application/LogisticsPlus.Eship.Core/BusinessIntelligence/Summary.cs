﻿using System;

namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
	[Serializable]
	public class Summary
	{
		public string Name { get; set; }
		public string ReportColumnName { get; set; }
		public int SortOrder { get; set; }
	}
}
