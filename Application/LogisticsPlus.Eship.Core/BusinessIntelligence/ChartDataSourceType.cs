﻿namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
    public enum ChartDataSourceType
    {
        //Chart Data Source Types. DO NOT RE-ORDER
        Original = 0,
        Pivot
    }
}
