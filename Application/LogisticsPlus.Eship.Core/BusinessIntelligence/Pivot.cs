﻿using System;
using System.Collections.Generic;

namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
    [Serializable]
    public class Pivot
    {
        public string TableName { get; set; }
        public string GuidId { get; set; }
        public List<PivotTableColumn> PivotTableColumns;
    }
}
