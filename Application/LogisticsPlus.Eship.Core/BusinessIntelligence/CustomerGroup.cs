﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
	[Serializable]
	[Entity("CustomerGroup", ReadOnly = false, Source = EntitySource.TableView)]
	public class CustomerGroup : TenantBase
	{
		private List<CustomerGroupMap> _customerGroupMaps;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("Name")]
		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

		[EnableSnapShot("Description", Description = "Description")]
		[Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Description { get; set; }

	    public List<CustomerGroupMap> CustomerGroupMaps
		{
			get
			{
				if (_customerGroupMaps == null) LoadCustomerGroupMaps();
				return _customerGroupMaps;
			}
			set { _customerGroupMaps = value; }
		}

	    public bool IsNew
	    {
	        get { return Id == default(long); }
	    }

		public CustomerGroup() { }

		public CustomerGroup(long id) : this(id, false)
		{
		}

		public CustomerGroup(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

	    public CustomerGroup(DbDataReader reader)
		{
			Load(reader);
		}

	    public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

	    public new void Delete()
		{
			if (Id != default(long)) base.Delete();
		}

	    public new void TakeSnapShot()
		{
			GetSnapShot();
		}

        public void LoadCollections()
        {
            LoadCustomerGroupMaps();
        }

		public List<Customer> RetrieveGroupCustomers()
		{
			var customers = new List<Customer>();

			const string query =
				@"Select Customer.* from Customer, CustomerGroupMap where CustomerGroupMap.CustomerId = Customer.[Id] 
					and CustomerGroupMap.TenantId = @TenantId and CustomerGroupMap.CustomerGroupId = @CustomerGroupId";
			var parameters = new Dictionary<string, object> {{"CustomerGroupId", Id}, {"TenantId", TenantId}};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					customers.Add(new Customer(reader));
			Connection.Close();

			return customers;
		}

		private void LoadCustomerGroupMaps()
		{
			_customerGroupMaps = new List<CustomerGroupMap>();

			const string query =
				"SELECT * FROM CustomerGroupMap WHERE CustomerGroupId = @CustomerGroupId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> {{"CustomerGroupId", Id}, {"TenantId", TenantId}};
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_customerGroupMaps.Add(new CustomerGroupMap(reader));
			Connection.Close();
		}


	}
}
