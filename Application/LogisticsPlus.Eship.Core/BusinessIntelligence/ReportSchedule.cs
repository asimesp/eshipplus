﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Administration;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
	[Serializable]
	[Entity("ReportSchedule", ReadOnly = false, Source = EntitySource.TableView)]
	public class ReportSchedule : TenantBase
	{
		private long _reportConfigurationId;
		private long _userId;
		private long _customerGroupId;
		private long _vendorGroupId;

		private User _user;
		private CustomerGroup _customerGroup;
		private VendorGroup _vendorGroup;
		private ReportConfiguration _reportConfiguration;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[EnableSnapShot("ScheduleInterval", Description = "Report Schedule")]
		[Property("ScheduleInterval", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ScheduleInterval ScheduleInterval { get; set; }

		[EnableSnapShot("Start", Description = "Start Date")]
		[Property("Start", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime Start { get; set; }

		[EnableSnapShot("End", Description = "End Date")]
		[Property("End", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime End { get; set; }

		[EnableSnapShot("LastRun", Description = "Last Run Date")]
		[Property("LastRun", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime LastRun { get; set; }

		[EnableSnapShot("Enabled")]
		[Property("Enabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Enabled { get; set; }

		[EnableSnapShot("Time", Description = "Time of Day")]
		[Property("Time", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Time { get; set; }

		[EnableSnapShot("ExcludeSunday", Description = "Exclude Sunday")]
		[Property("ExcludeSunday", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ExcludeSunday { get; set; }

		[EnableSnapShot("ExcludeMonday", Description = "Exclude Monday")]
		[Property("ExcludeMonday", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ExcludeMonday { get; set; }

		[EnableSnapShot("ExcludeTuesday", Description = "Exclude Tuesday")]
		[Property("ExcludeTuesday", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ExcludeTuesday { get; set; }

		[EnableSnapShot("ExcludeWednesday", Description = "Exclude Wednesday")]
		[Property("ExcludeWednesday", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ExcludeWednesday { get; set; }

		[EnableSnapShot("ExcludeThursday", Description = "Exclude Thursday")]
		[Property("ExcludeThursday", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ExcludeThursday { get; set; }

		[EnableSnapShot("ExcludeFriday", Description = "Exclude Friday")]
		[Property("ExcludeFriday", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ExcludeFriday { get; set; }

		[EnableSnapShot("ExcludeSaturday", Description = "Exclude Saturday")]
		[Property("ExcludeSaturday", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ExcludeSaturday { get; set; }

		[EnableSnapShot("Notify", Description = "Notify on Report Schedule Completion")]
		[Property("Notify", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool Notify { get; set; }

		[EnableSnapShot("NotifyEmails", Description = "Notify Emails")]
		[Property("NotifyEmails", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string NotifyEmails { get; set; }

	    [EnableSnapShot("SuppressNotificationForEmptyReport", Description = "Suppress Notification For Empty Report")]
	    [Property("SuppressNotificationForEmptyReport", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool SuppressNotificationForEmptyReport { get; set; }


        [EnableSnapShot("FtpEnabled", Description = "FTP Enabled")]
		[Property("FtpEnabled", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool FtpEnabled { get; set; }

		[EnableSnapShot("FtpUrl", Description = "Ftp Url")]
		[Property("FtpUrl", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string FtpUrl { get; set; }

		[EnableSnapShot("SecureFtp", Description = "Secure Ftp")]
		[Property("SecureFtp", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool SecureFtp { get; set; }

		[EnableSnapShot("FtpUsername", Description = "Ftp Username")]
		[Property("FtpUsername", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string FtpUsername { get; set; }

		[EnableSnapShot("FtpPassword", Description = "Ftp Password")]
		[Property("FtpPassword", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string FtpPassword { get; set; }

		[EnableSnapShot("FtpDefaultFolder", Description = "Ftp Default Folder")]
		[Property("FtpDefaultFolder", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string FtpDefaultFolder { get; set; }

		[EnableSnapShot("DeliveryFilename", Description = "Delivery Filename")]
		[Property("DeliveryFilename", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string DeliveryFilename { get; set; }


		[EnableSnapShot("ReportConfigurationId", Description = "Report Configuration Reference")]
		[Property("ReportConfigurationId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long ReportConfigurationId
		{
			get
			{
				if (_reportConfiguration != null) _reportConfigurationId = _reportConfiguration.Id;
				return _reportConfigurationId;
			}
			set
			{
				_reportConfigurationId = value;
				if (_reportConfiguration != null && value != _reportConfiguration.Id) _reportConfiguration = null;
			}
		}

		[EnableSnapShot("Extension", Description = "Export File Extension")]
		[Property("Extension", AutoValueOnInsert = false, DataType = SqlDbType.Int, Key = false)]
		public ReportExportExtension Extension { get; set; }

		[EnableSnapShot("UserId", Description = "User Reference")]
		[Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long UserId
		{
			get
			{
				if (_user != null) _userId = _user.Id;
				return _userId;
			}
			set
			{
				_userId = value;
				if (_user != null && value != _reportConfiguration.Id) _user = null;
			}
		}

		[EnableSnapShot("CustomerGroupId", Description = "Customer Group Reference")]
		[Property("CustomerGroupId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long CustomerGroupId
		{
			get
			{
				if (_customerGroup != null) _customerGroupId = _customerGroup.Id;
				return _customerGroupId;
			}
			set
			{
				_customerGroupId = value;
				if (_customerGroup != null && value != _customerGroup.Id) _customerGroup = null;
			}
		}

		[EnableSnapShot("VendorGroupId", Description = "Vendor Group Reference")]
		[Property("VendorGroupId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long VendorGroupId
		{
			get
			{
				if (_vendorGroup != null) _vendorGroupId = _vendorGroup.Id;
				return _vendorGroupId;
			}
			set
			{
				_vendorGroupId = value;
				if (_vendorGroup != null && value != _vendorGroup.Id) _vendorGroup = null;
			}
		}

		public ReportConfiguration ReportConfiguration
		{
			get { return _reportConfiguration ?? (_reportConfiguration = new ReportConfiguration(_reportConfigurationId)); }
			set
			{
				_reportConfiguration = value;
				_reportConfigurationId = value == null ? default(long) : value.Id;
			}
		}
		public User User
		{
			get { return _user ?? (_user = new User(_userId)); }
			set
			{
				_user = value;
				_userId = value == null ? default(long) : value.Id;
			}
		}
		public CustomerGroup CustomerGroup
		{
			get { return _customerGroup ?? (_customerGroup = new CustomerGroup(_customerGroupId)); }
			set
			{
				_customerGroup = value;
				_customerGroupId = value == null ? default(long) : value.Id;
			}
		}
		public VendorGroup VendorGroup
		{
			get { return _vendorGroup ?? (_vendorGroup = new VendorGroup(_vendorGroupId)); }
			set
			{
				_vendorGroup = value;
				_vendorGroupId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public ReportSchedule()
		{
			
		}

		public ReportSchedule(long id) : this(id, false)
		{
		}

		public ReportSchedule(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public ReportSchedule(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
