﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
	[Serializable]
	public class ParameterColumn
	{
		public string ReportColumnName { get; set; }
		public Operator Operator { get; set; }
		public bool ReadOnly { get; set; }

		[XmlElement(IsNullable = true)]
		public string DefaultValue { get; set; }

		[XmlElement(IsNullable = true)]
		public string DefaultValue2 { get; set; }
	}
}
