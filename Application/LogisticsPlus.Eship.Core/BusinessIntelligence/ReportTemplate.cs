﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
	[Serializable]
	[Entity("ReportTemplate", ReadOnly = false, Source = EntitySource.TableView)]
	public class ReportTemplate : ObjectBase
	{
		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

        [EnableSnapShot("Name", Description = "Report Template Name")]
		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

        [EnableSnapShot("Description", Description = "Report Template Description")]
		[Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Description { get; set; }

        [EnableSnapShot("CanFilterByCustomerGroup", Description = "Report Template Can Filter By Customer Group")]
		[Property("CanFilterByCustomerGroup", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool CanFilterByCustomerGroup { get; set; }

        [EnableSnapShot("CanFilterByVendorGroup", Description = "Report Template Can Filter By Vendor Group")]
		[Property("CanFilterByVendorGroup", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool CanFilterByVendorGroup { get; set; }

        [EnableSnapShot("DefaultCustomization", Description = "Report Template Default Customization")]
        [Property("DefaultCustomization", AutoValueOnInsert = false, DataType = SqlDbType.Text, Key = false)]
        public string DefaultCustomization { get; set; }

        [EnableSnapShot("Query", Description = "Report Template Query")]
		[Property("Query", AutoValueOnInsert = false, DataType = SqlDbType.Text, Key = false)]
		public string Query { get; set; }

        public bool IsNew
		{
			get { return Id == default(long); }
		}
		
		public ReportTemplate()
		{
			
		}

		public ReportTemplate(long id)
            : this(id, false)
		{
		}

        public ReportTemplate(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public ReportTemplate(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
