﻿namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
	public enum ReportConfigurationVisibility
	{
        // DO NOT RE-ORDER. SEE APPLICABLE JOINS ON DATABASE REFREPORTCONFIGURATIONVISIBILITY
        AllUsers = 0,
		EmployeesOnly,
		SelectUsers
	}
}
