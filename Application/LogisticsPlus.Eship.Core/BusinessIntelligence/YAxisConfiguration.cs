﻿using System;
using System.Drawing;

namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
	[Serializable]
	public class YAxisConfiguration
	{
		public string ReportColumnName { get; set; }
		public ReportChartType Type { get; set; }
	}
}
