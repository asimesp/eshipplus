﻿namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
	public enum ReportExportExtension
	{
		/* Intensionally left lower case to start so ToString() will follow same */
		txt = 0,
		xlsx,
		xml,
        csv
	}
}
