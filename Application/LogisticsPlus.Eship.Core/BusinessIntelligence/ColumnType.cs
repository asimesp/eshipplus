﻿namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
	public enum ColumnType
	{
        NotApplicable = 0,
		Pivot,
        Aggregate
	}
}
