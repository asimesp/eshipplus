﻿using System;
using System.Collections.Generic;

namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
	[Serializable]
	public class ChartConfiguration
	{
		public string Title { get; set; }
		public string XAxisTitle { get; set; }
		public string YAxisTitle { get; set; }

		public string XAxisDataColumn { get; set; }

        public ChartDataSourceType DataSourceType { get; set; }
        public string PivotGuidId { get; set; }
        public string PivotTableName { get; set; }

		public List<YAxisConfiguration> YAxisDataConfigurations { get; set; }
	}
}
