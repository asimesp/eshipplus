﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Administration;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
	[Serializable]
	[Entity("SearchProfileDefault", ReadOnly = false, Source = EntitySource.TableView)]
	public class SearchProfileDefault : TenantBase
	{
		private long _userId;

		private User _user;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; protected set; }

		[Property("Profiles", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Profiles { get; set; }

        [Property("RateAndScheduleProfiles", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string RateAndScheduleProfiles { get; set; }

        [Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long UserId
		{
			get { return _userId; }
			set
			{
				_userId = value;
				if (_user != null && value != _user.Id) _user = null;
			}
		}

		public User User
		{
			get { return _userId == default(long) ? null : _user ?? (_user = new User(_userId, false)); }
			set
			{
				_user = value;
				_userId = value == null ? default(long) : value.Id;
			}
		}


		public bool IsNew
		{
			get { return Id == default(long); }
		}


		public SearchProfileDefault()
		{
			Profiles = string.Empty;
            RateAndScheduleProfiles = string.Empty;
        }

		public SearchProfileDefault(User user) : this() 
		{
			const string query = @"select * from SearchProfileDefault where UserId =  @UserId and TenantId = @TenantId";
			var parameters = new Dictionary<string, object> {{"UserId", user.Id}, {"TenantId", user.TenantId}};
			using (var reader = GetReader(query, parameters))
				if (reader.Read())
					Load(reader);
			Connection.Close();
		}

		public SearchProfileDefault(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
