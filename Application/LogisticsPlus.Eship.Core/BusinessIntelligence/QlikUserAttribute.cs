﻿using System;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
    [Serializable]
    [Entity("QlikUserAttribute", ReadOnly = false, Source = EntitySource.TableView, SourceName = "QlikUserAttributes")]
    public class QlikUserAttribute : ObjectBase
    {
        private string _qlikUserId;

        private QlikUser _qlikUser;


        [EnableSnapShot("UserId", Description = "User Id")]
        [Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = true, Column = "userid")]
        public string UserId
        {
            get
            {
                if (_qlikUser != null) _qlikUserId = _qlikUser.UserId;
                return _qlikUserId;
            }
            set
            {
                _qlikUserId = value;
                if (_qlikUser != null && value != _qlikUser.UserId) _qlikUser = null;
            }
        }

        [EnableSnapShot("Type")]
        [Property("Type", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = true, Column = "type")]
        public string Type { get; set; }

        [EnableSnapShot("Value")]
        [Property("Value", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = true, Column = "value")]
        public string Value { get; set; }

        public QlikUser QlikUser
        {
            get { return _qlikUser ?? (_qlikUser = new QlikUser(_qlikUserId)); }
            set
            {
                _qlikUser = value;
                _qlikUserId = value == null ? string.Empty : value.UserId;
            }
        }

        public bool IsNew
		{
			get { return KeyLoaded; }
		}
		
		public QlikUserAttribute()
		{
			
		}

        public QlikUserAttribute(string userId, string type, string value, bool takeSnapshot = false)
		{
            UserId = userId;
            Type = type;
            Value = value;
            if (string.IsNullOrEmpty(UserId) || string.IsNullOrEmpty(type) || string.IsNullOrEmpty(value)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

        public QlikUserAttribute(DbDataReader reader)
		{
			Load(reader);
            KeyLoaded = true;
		}

		public void Save()
		{
			if (!KeyLoaded)
			{
			    Insert();
			    KeyLoaded = true;
			}
			else
			{
			    Update();
			}
		}

		public new void Delete()
		{
			if (KeyLoaded) base.Delete();
		}
    }
}
