﻿using System;
using System.Data;

namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
	[Serializable]
	public class ReportColumn
	{
		public string Name { get; set; }
		public string Column { get; set; }
		public SqlDbType DataType { get; set; }
		public bool Custom { get; set; }
	}
}
