﻿namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
	public enum ReportChartType
	{
		//Chart Types
		Bar = 0,
		Column,
		Line,
		Pie,
		Point,
		StackedBar = 5,
		StackedColumn
	}
}
