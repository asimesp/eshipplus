﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
	[Serializable]
	public class ReportCustomization
	{
		public List<ReportColumn> DataColumns { get; set; }
		public List<Summary> Summaries { get; set; }
		public List<SortColumn> SortColumns { get; set; }
		public List<ParameterColumn> Parameters { get; set; }
		public List<RequiredValueParameter> RequiredValueParameters { get; set; }
        public List<Pivot> Pivots { get; set; }

	    public bool HideDataTable { get; set; }

        [XmlElement(IsNullable = true)]
        public List<ChartConfiguration> Charts { get; set; }
	}
}
