﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Administration;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
    [Serializable]
    [Entity("ReportCustomizationGroup", ReadOnly = false, Source = EntitySource.TableView)]
    public class ReportCustomizationGroup : TenantBase
    {
        private long _groupId;
        private long _reportConfigurationId;

        private ReportConfiguration _reportConfiguration;
        private Group _group;

        [Property("ReportConfigurationId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
        public long ReportConfigurationId
        {
            get
            {
                if (_reportConfiguration != null) _reportConfigurationId = _reportConfiguration.Id;
                return _reportConfigurationId;
            }
            set
            {
                _reportConfigurationId = value;
                if (_reportConfiguration != null && value != _reportConfiguration.Id) _reportConfiguration = null;
            }
        }

        [Property("GroupId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
        public long GroupId
        {
            get
            {
                if (_group != null) _groupId = _group.Id;
                return _groupId;
            }
            set
            {
                _groupId = value;
                if (_group != null && value != _group.Id) _group = null;
            }
        }

        public ReportConfiguration ReportConfiguration
        {
            get { return _reportConfiguration ?? (_reportConfiguration = new ReportConfiguration(_reportConfigurationId)); }
            set
            {
                _reportConfiguration = value;
                _reportConfigurationId = value == null ? default(long) : value.Id;
            }
        }
        public Group Group
        {
            get { return _group ?? (_group = new Group(_groupId)); }
            set
            {
                _group = value;
                _groupId = value == null ? default(long) : value.Id;
            }
        }

        public ReportCustomizationGroup()
        {

        }

        public ReportCustomizationGroup(DbDataReader reader)
        {
            Load(reader);
        }

        public void Save()
        {
            Insert();
        }

        public new void Delete()
        {
            base.Delete();
        }
    }
}
