﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
    [Serializable]
    [Entity("QlikUser", ReadOnly = false, Source = EntitySource.TableView, SourceName = "QlikUsers")]
    public class QlikUser : ObjectBase
    {
        private List<QlikUserAttribute> _qlikUserAttributes;


        [EnableSnapShot("UserId", Description = "User Id")]
        [Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = true, Column = "userid")]
        public string UserId { get; set; }

        [EnableSnapShot("Name")]
        [Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false, Column = "name")]
        public string Name { get; set; }


        public List<QlikUserAttribute> QlikUserAttributes
        {
            get
            {
                if (_qlikUserAttributes == null) LoadQlikUserAttributes();
                return _qlikUserAttributes;
            }
            set { _qlikUserAttributes = value; }
        }


        public bool IsNew
		{
			get { return !KeyLoaded; }
		}
		
		public QlikUser()
		{
			
		}

        public QlikUser(string userId, bool takeSnapshot = false)
		{
            UserId = userId;
			if (UserId == string.Empty) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

        public QlikUser(DbDataReader reader)
		{
            Load(reader);
            KeyLoaded = true;
		}

		public void Save()
		{
			if (!KeyLoaded)
			{
			    Insert();
			    KeyLoaded = true;
			}
			else
			{
			    Update();
			}
		}

		public new void Delete()
		{
			if (KeyLoaded) base.Delete();
		}


        public void LoadQlikUserAttributes()
        {
            _qlikUserAttributes = new List<QlikUserAttribute>();
            const string query = "SELECT * FROM QlikUserAttributes WHERE userid = @UserId";
            var parameters = new Dictionary<string, object> { { "UserId", UserId } };
            using (var reader = GetReader(query, parameters))
                while (reader.Read())
                    _qlikUserAttributes.Add(new QlikUserAttribute(reader));
            Connection.Close();
        }
    }
}
