﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Administration;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
	[Serializable]
	[Entity("ReportCustomizationUser", ReadOnly = false, Source = EntitySource.TableView)]
	public class ReportCustomizationUser : TenantBase
	{
		private long _userId;
		private long _reportConfigurationId;

		private ReportConfiguration _reportConfiguration;
		private User _user;

		[Property("ReportConfigurationId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long ReportConfigurationId
		{
			get
			{
				if (_reportConfiguration != null) _reportConfigurationId = _reportConfiguration.Id;
				return _reportConfigurationId;
			}
			set
			{
				_reportConfigurationId = value;
				if (_reportConfiguration != null && value != _reportConfiguration.Id) _reportConfiguration = null;
			}
		}

		[Property("UserId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long UserId
		{
			get
			{
				if (_user != null) _userId = _user.Id;
				return _userId;
			}
			set
			{
				_userId = value;
				if (_user != null && value != _user.Id) _user = null;
			}
		}

		public ReportConfiguration ReportConfiguration
		{
			get { return _reportConfiguration ?? (_reportConfiguration = new ReportConfiguration(_reportConfigurationId)); }
			set
			{
				_reportConfiguration = value;
				_reportConfigurationId = value == null ? default(long) : value.Id;
			}
		}
		public User User
		{
			get { return _user ?? (_user = new User(_userId)); }
			set
			{
				_user = value;
				_userId = value == null ? default(long) : value.Id;
			}
		}

		public ReportCustomizationUser()
		{
			
		}

		public ReportCustomizationUser(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			Insert();
		}

		public new void Delete()
		{
			base.Delete();
		}
	}
}
