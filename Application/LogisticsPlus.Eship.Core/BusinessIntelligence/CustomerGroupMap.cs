﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
	[Serializable]
	[Entity("CustomerGroupMap", ReadOnly = false, Source = EntitySource.TableView)]
	public class CustomerGroupMap : TenantBase
	{
		private long _customerId;
		private long _customerGroupId;

		private CustomerGroup _customerGroup;
		private Customer _customer;

		[Property("CustomerGroupId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long CustomerGroupId
		{
			get
			{
				if (_customerGroup != null) _customerGroupId = _customerGroup.Id;
				return _customerGroupId;
			}
			set
			{
				_customerGroupId = value;
				if (_customerGroup != null && value != _customerGroup.Id) _customerGroup = null;
			}
		}

	    [Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long CustomerId
		{
			get
			{
				if (_customer != null) _customerId = _customer.Id;
				return _customerId;
			}
			set
			{
				_customerId = value;
				if (_customer != null && value != _customer.Id) _customer = null;
			}
		}

		public Customer Customer
		{
			get { return _customer ?? (_customer = new Customer(_customerId)); }
			set
			{
				_customer = value;
				_customerId = value == null ? default(long) : value.Id;
			}
		}
		public CustomerGroup CustomerGroup
		{
			get { return _customerGroup ?? (_customerGroup = new CustomerGroup(_customerGroupId)); }
			set
			{
				_customerGroup = value;
				_customerGroupId = value == null ? default(long) : value.Id;
			}
		}

	    public CustomerGroupMap()
	    {
	    }

	    public CustomerGroupMap(DbDataReader reader)
		{
            Load(reader);
		}

		public void Save()
		{
			Insert();
		}

		public new void Delete()
		{
			base.Delete();
		}
	}
}
