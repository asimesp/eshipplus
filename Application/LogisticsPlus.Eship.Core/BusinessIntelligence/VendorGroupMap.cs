﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
	[Serializable]
	[Entity("VendorGroupMap", ReadOnly = false, Source = EntitySource.TableView)]
	public class VendorGroupMap : TenantBase
	{
		private long _vendorId;
		private long _vendorGroupId;

		private VendorGroup _vendorGroup;
		private Vendor _vendor;

	    [EnableSnapShot("VendorGroupId", Description = "Vendor Group Reference")]
	    [Property("VendorGroupId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long VendorGroupId
		{
			get
			{
				if (_vendorGroup != null) _vendorGroupId = _vendorGroup.Id;
				return _vendorGroupId;
			}
			set
			{
				_vendorGroupId = value;
				if (_vendorGroup != null && value != _vendorGroup.Id) _vendorGroup = null;
			}
		}

	    [EnableSnapShot("VendorId", Description = "Vendor Reference")]
	    [Property("VendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = true)]
		public long VendorId
		{
			get
			{
				if (_vendor != null) _vendorId = _vendor.Id;
				return _vendorId;
			}
			set
			{
				_vendorId = value;
				if (_vendor != null && value != _vendor.Id) _vendor = null;
			}
		}

		public Vendor Vendor
		{
			get { return _vendor ?? (_vendor = new Vendor(_vendorId)); }
			set
			{
				_vendor = value;
				_vendorId = value == null ? default(long) : value.Id;
			}
		}
		public VendorGroup VendorGroup
		{
			get { return _vendorGroup ?? (_vendorGroup = new VendorGroup(_vendorGroupId)); }
			set
			{
				_vendorGroup = value;
				_vendorGroupId = value == null ? default(long) : value.Id;
			}
		}

        public VendorGroupMap(DbDataReader reader)
        {
            Load(reader);
        }

        public VendorGroupMap()
        {

        }

        public void Save()
        {
            Insert();
        }

        public new void Delete()
        {
            base.Delete();
        }
	}
}
