﻿namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
	public enum Operator
	{
		/* DO NOT CHANGE ORDER!!! */
		Equal = 0,
		NotEqual,
		LessThan,
		GreaterThan,
		LessThanOrEqual,
		GreaterThanOrEqual = 5,
		Contains,
		NotContains,
		BeginsWith,
		NotBeginsWith,
		EndsWith = 10,
		NotEndsWith,
		In,
		NotIn,
		Between,			// NOTE: there is javascript tied to value 14 = Between and 15 = NotBetween. See ReportRunParameter.ascx
		NotBetween = 15,
	}
}
