﻿using System;

namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
	[Serializable]
	public enum SortDirection
	{
		Ascending = 0,
		Descending
	}
}
