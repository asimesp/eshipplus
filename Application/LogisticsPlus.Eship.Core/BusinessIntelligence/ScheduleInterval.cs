﻿using System;

namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
	[Serializable]
	public enum ScheduleInterval
	{
        // DO NOT RE-ORDER. SEE APPLICABLE JOINS ON DATABASE REFSCHEDULEINTERVAL
        Yearly = 0,
		Monthly,
		Weekly,
		Daily,
	}
}
