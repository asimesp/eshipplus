﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
	[Serializable]
	[Entity("VendorGroup", ReadOnly = false, Source = EntitySource.TableView)]
	public class VendorGroup : TenantBase
	{
		private List<VendorGroupMap> _vendorGroupMaps;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("Name")]
		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

		[EnableSnapShot("Description", Description = "Description")]
		[Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Description { get; set; }

		public List<VendorGroupMap> VendorGroupMaps
		{
			get
			{
				if (_vendorGroupMaps == null) LoadVendorGroupMaps();
				return _vendorGroupMaps;
			}
			set { _vendorGroupMaps = value; }
		}

        public bool IsNew
        {
            get { return Id == default(long); }
        }

		public VendorGroup(){}

		public VendorGroup(long id) : this(id, false)
		{
		}

		public VendorGroup(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

        public VendorGroup(DbDataReader reader)
		{
			Load(reader);
		}

        public void Save()
        {
            if (Id == default(long)) Insert();
            else Update();
        }

        public new void Delete()
        {
            if (Id != default(long)) base.Delete();
        }

        public new void TakeSnapShot()
        {
            GetSnapShot();
        }

        public void LoadCollections()
        {
            LoadVendorGroupMaps();
        }

		public List<Vendor> RetrieveGroupVendors()
		{
			var vendors = new List<Vendor>();

			const string query =
				@"Select Vendor.* from Vendor, VendorGroupMap where VendorGroupMap.VendorId = Vendor.[Id] 
					and VendorGroupMap.TenantId = @TenantId and VendorGroupMap.VendorGroupId = @VendorGroupId";
			var parameters = new Dictionary<string, object> { { "VendorGroupId", Id }, { "TenantId", TenantId } };
			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					vendors.Add(new Vendor(reader));
			Connection.Close();

			return vendors;
		}

		private void LoadVendorGroupMaps()
		{
		    _vendorGroupMaps = new List<VendorGroupMap>();

		    const string query = "SELECT * FROM VendorGroupMap WHERE VendorGroupId = @VendorGroupId";

            IDictionary<string, object> parameters = new Dictionary<string, object> { { "VendorGroupId", Id } };

			using (var reader = GetReader(query, parameters))
				while (reader.Read())
					_vendorGroupMaps.Add(new VendorGroupMap(reader));
			Connection.Close();
		}
	}
}
