﻿namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
	public enum AggregateType
	{
		//Aggregate Types. DO NOT RE-ORDER
		Min = 0,
        Max,
        Cnt,
        Sum,
        Avg
	}
}
