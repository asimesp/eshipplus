﻿using System;
using System.Collections.Generic;
using System.Data;

namespace LogisticsPlus.Eship.Core.BusinessIntelligence
{
    [Serializable]
    public class PivotTableColumn
    {
        public string ReportColumnName { get; set; }
        public SqlDbType DataType { get; set; }
        public ColumnType ColumnType { get; set; }

        public bool MinAggregate { get; set; }
        public bool MaxAggregate { get; set; }
        public bool AvgAggregate { get; set; }
        public bool CntAggregate { get; set; }
        public bool SumAggregate { get; set; }

        public List<AggregateType> AggregateTypes()
        {
            var aggTypes = new List<AggregateType>();

            if(MinAggregate) aggTypes.Add(AggregateType.Min);
            if(MaxAggregate) aggTypes.Add(AggregateType.Max);
            if(AvgAggregate) aggTypes.Add(AggregateType.Avg);
            if(CntAggregate) aggTypes.Add(AggregateType.Cnt);
            if(SumAggregate) aggTypes.Add(AggregateType.Sum);

            return aggTypes;
        }
    }
}
