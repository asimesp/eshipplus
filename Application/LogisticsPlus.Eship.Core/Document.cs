﻿using System;
using System.Data;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core
{
	[Serializable]
	public class Document : TenantBase
	{
		private long _documentTagId;

		private DocumentTag _documentTag;

		[EnableSnapShot("LocationPath", Description = "Document Reference Path")]
		[Property("LocationPath", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string LocationPath { get; set; }

		[EnableSnapShot("DocumentTagId", Description = "Document Tag Reference")]
		[Property("DocumentTagId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		public long DocumentTagId
		{
			get { return _documentTagId; }
			set
			{
				_documentTagId = value;
				if (_documentTag != null && value != _documentTag.Id) _documentTag = null;
			}
		}

		[EnableSnapShot("Name")]
		[Property("Name", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Name { get; set; }

		[EnableSnapShot("Description")]
		[Property("Description", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string Description { get; set; }

		public DocumentTag DocumentTag
		{
			get { return _documentTagId == default(long) ? null : _documentTag ?? (_documentTag = new DocumentTag(_documentTagId, false)); }
			set
			{
				_documentTag = value;
				_documentTagId = value == null ? default(long) : value.Id;
			}
		}
	}
}
