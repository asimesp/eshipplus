﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace LogisticsPlus.Eship.Core
{
	public class TextEncryptor
	{
		private readonly byte[] _key;
		private readonly byte[] _iv;

		/// <param name="key">An array of length 16, 24, or 32</param>
		/// <param name="iv">An array of length 16</param>
		public TextEncryptor(byte[] key, byte[] iv)
		{
			_key = key;
			_iv = iv;
		}

		public TextEncryptor(string key, string iv) : this(Convert.FromBase64String(key), Convert.FromBase64String(iv))
		{
		}

		public string Decrypt(string encryptedText)
		{
			if (string.IsNullOrEmpty(encryptedText))
				return string.Empty;

			//byte[] data;
			try
			{
				byte[] data = Convert.FromBase64String(encryptedText);

				using (var memory = new MemoryStream(data))
				using (var crypto = new RijndaelManaged())
				using (ICryptoTransform decryptor = crypto.CreateDecryptor(_key, _iv))
				using (var stream = new CryptoStream(memory, decryptor, CryptoStreamMode.Read))
				using (var reader = new StreamReader(stream))
					return reader.ReadToEnd();
			}
			catch
			{
				return string.Empty;
			}
		}

		public string Encrypt(string text)
		{
			if (string.IsNullOrEmpty(text))
				return string.Empty;

			using (var memory = new MemoryStream())
			{
				using (var crypto = new RijndaelManaged())
				using (ICryptoTransform encryptor = crypto.CreateEncryptor(_key, _iv))
				using (var stream = new CryptoStream(memory, encryptor, CryptoStreamMode.Write))
				using (var writer = new StreamWriter(stream))
					writer.Write(text);

				byte[] data = memory.ToArray();
				return Convert.ToBase64String(data);
			}
		}
	}
}
