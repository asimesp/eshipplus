﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core
{
	[Serializable]
	[Entity("DeveloperAccessRequest", ReadOnly = false, Source = EntitySource.TableView)]
	public class DeveloperAccessRequest : TenantBase
	{
		private long _customerId;

		private Customer _customer;

		[Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
		public long Id { get; private set; }

		[EnableSnapShot("ContactName", Description = "Contact Name")]
		[Property("ContactName", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ContactName { get; set; }

		[EnableSnapShot("ContactEmail", Description = "Contact Email")]
		[Property("ContactEmail", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string ContactEmail { get; set; }

		[EnableSnapShot("DateCreated", Description = "Date Record Created")]
		[Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
		public DateTime DateCreated { get; set; }

		[EnableSnapShot("TestInformation", Description = "Test Credentials/Information")]
		[Property("TestInformation", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
		public string TestInformation { get; set; }

		[EnableSnapShot("ProductionAccess", Description = "Production Authorization")]
		[Property("ProductionAccess", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
		public bool ProductionAccess { get; set; }

        [EnableSnapShot("AccessGranted", Description = "Development Authorization")]
        [Property("AccessGranted", AutoValueOnInsert = false, DataType = SqlDbType.Bit, Key = false)]
        public bool AccessGranted { get; set; }

		[EnableSnapShot("CustomerId", Description = "Customer Reference Key")]
		[Property("CustomerId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
		protected long CustomerId
		{
			get
			{
				if (_customer != null) _customerId = _customer.Id;
				return _customerId;
			}
			set
			{
				_customerId = value;
				if (_customer != null && value != _customer.Id) _customer = null;
			}
		}

		public Customer Customer
		{
			get { return _customer ?? (_customer = new Customer(_customerId)); }
			set
			{
				_customer = value;
				_customerId = value == null ? default(long) : value.Id;
			}
		}

		public bool IsNew
		{
			get { return Id == default(long); }
		}

		public DeveloperAccessRequest() { }

		public DeveloperAccessRequest(long id) : this(id, false) { }

		public DeveloperAccessRequest(long id, bool takeSnapshot)
		{
			Id = id;
			if (Id == default(long)) return;
			KeyLoaded = Load();
			if (takeSnapshot) TakeSnapShot();
		}

		public DeveloperAccessRequest(DbDataReader reader)
		{
			Load(reader);
		}

		public void Save()
		{
			if (Id == default(long)) Insert();
			else Update();
		}

		public new void Delete()
		{
			if (Id != default(long))
				base.Delete();
		}
	}
}
