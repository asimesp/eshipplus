﻿using System;
using System.Data;
using System.Data.Common;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Registry;
using ObjToSql.Core.Attributes;

namespace LogisticsPlus.Eship.Core.Operations
{
    [Serializable]
    [Entity("CustomerTLTenderingProfileLane", ReadOnly = false, Source = EntitySource.TableView)]
    public class CustomerTLTenderingProfileLane : TenantBase
    {
        private long _tlTenderProfileId;
        private long _originCountryId;
        private long _destinationCountryId;

		private long _firstPreferredVendorId;
		private long _secondPreferredVendorId;
		private long _thirdPreferredVendorId;

        private Country _destinationCountry;
        private Country _originCountry;
        private CustomerTLTenderingProfile _tlProfile;

	    private Vendor _firstPreferredVendor;
	    private Vendor _secondPreferredVendor;
	    private Vendor _thirdPreferredVendor;

        [Property("Id", AutoValueOnInsert = true, DataType = SqlDbType.BigInt, Key = true)]
        public long Id { get; protected set; }

        [EnableSnapShot("Index", Description = "Index")]
        [Property("Index", AutoValueOnInsert = true, DataType = SqlDbType.Int, Key = false)]
        public long Index { get; set; }

        [EnableSnapShot("DateCreated", Description = "Date Created")]
        [Property("DateCreated", AutoValueOnInsert = false, DataType = SqlDbType.DateTime, Key = false)]
        public DateTime DateCreated { get; set; }

        [EnableSnapShot("OriginCity", Description = "Origin City")]
        [Property("OriginCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginCity { get; set; }

        [EnableSnapShot("OriginState", Description = "Origin State")]
        [Property("OriginState", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginState { get; set; }

        [EnableSnapShot("OriginCountryId", Description = "Origin Country Reference")]
        [Property("OriginCountryId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long OriginCountryId
        {
            get { return _originCountryId; }
            set
            {
                _originCountryId = value;
                if (_originCountry != null && value != _originCountry.Id) _originCountry = null;
            }
        }

        [EnableSnapShot("OriginPostalCode", Description = "Origin Postal Code")]
        [Property("OriginPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string OriginPostalCode { get; set; }

        [EnableSnapShot("DestinationCity", Description = "Destination City")]
        [Property("DestinationCity", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationCity { get; set; }

        [EnableSnapShot("DestinationState", Description = "Destination State")]
        [Property("DestinationState", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationState { get; set; }

        [EnableSnapShot("DestinationCountryId", Description = "Destination Country Reference")]
        [Property("DestinationCountryId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long DestinationCountryId
        {
            get { return _destinationCountryId; }
            set
            {
                _destinationCountryId = value;
                if (_destinationCountry != null && value != _destinationCountry.Id) _destinationCountry = null;
            }
        }

        [EnableSnapShot("FirstPreferredVendorId", Description = "First Preferred Vendor Id")]
        [Property("FirstPreferredVendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long FirstPreferredVendorId
        {
            get
            {
                if (_firstPreferredVendor != null) _firstPreferredVendorId = _firstPreferredVendor.Id;
                return _firstPreferredVendorId;
            }
            set
            {
                _firstPreferredVendorId = value;
                if (_firstPreferredVendor != null && value != _firstPreferredVendor.Id) _firstPreferredVendor = null;
            }
        }

        [EnableSnapShot("SecondPreferredVendorId", Description = "Second Preferred Vendor Id")]
        [Property("SecondPreferredVendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long SecondPreferredVendorId
        {
         get
            {
                if (_secondPreferredVendor != null) _secondPreferredVendorId = _secondPreferredVendor.Id;
                return _secondPreferredVendorId;
            }
            set
            {
                _secondPreferredVendorId = value;
                if (_secondPreferredVendor != null && value != _secondPreferredVendor.Id) _secondPreferredVendor = null;
            }   
        }

        [EnableSnapShot("ThirdPreferredVendorId", Description = "Third Preferred Vendor Id")]
        [Property("ThirdPreferredVendorId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long ThirdPreferredVendorId
        {
            get
            {
                if (_thirdPreferredVendor != null) _thirdPreferredVendorId = _thirdPreferredVendor.Id;
                return _thirdPreferredVendorId;
            }
            set
            {
                _thirdPreferredVendorId = value;
                if (_thirdPreferredVendor != null && value != _thirdPreferredVendor.Id) _thirdPreferredVendor = null;
            }
        }

        [EnableSnapShot("DestinationPostalCode", Description = "Destination Postal Code")]
        [Property("DestinationPostalCode", AutoValueOnInsert = false, DataType = SqlDbType.NVarChar, Key = false)]
        public string DestinationPostalCode { get; set; }

        [EnableSnapShot("TLTenderingProfileId", Description = "Vendor Reference")]
        [Property("TLTenderingProfileId", AutoValueOnInsert = false, DataType = SqlDbType.BigInt, Key = false)]
        public long TLTenderingProfileId
        {
            get
            {
                if (_tlProfile != null) _tlTenderProfileId = _tlProfile.Id;
                return _tlTenderProfileId;
            }
            set
            {
                _tlTenderProfileId = value;
                if (_tlProfile != null && value != _tlProfile.Id) _tlProfile = null;
            }
        }

        public CustomerTLTenderingProfile TLTenderingProfile
        {
            get { return _tlProfile ?? (_tlProfile = new CustomerTLTenderingProfile(_tlTenderProfileId)); }
            set
            {
                _tlProfile = value;
                _tlTenderProfileId = value == null ? default(long) : value.Id;
            }
        }
        public Country OriginCountry
        {
            get { return _originCountryId == default(long) ? null : _originCountry ?? (_originCountry = new Country(_originCountryId)); }
            set
            {
                _originCountry = value;
                _originCountryId = value == null ? default(long) : value.Id;
            }
        }
        public Country DestinationCountry
        {
            get { return _destinationCountryId == default(long) ? null : _destinationCountry ?? (_destinationCountry = new Country(_destinationCountryId)); }
            set
            {
                _destinationCountry = value;
                _destinationCountryId = value == null ? default(long) : value.Id;
            }
        }

        public string FullLane
        {
            get
            {
                return string.Format("{0} {1} {2} {3} - {4} {5} {6} {7}",
                                     OriginPostalCode, OriginCity, OriginState,
                                     OriginCountry == null ? string.Empty : OriginCountry.Name,
                                     DestinationPostalCode, DestinationCity, DestinationState,
                                     DestinationCountry == null ? string.Empty : DestinationCountry.Name
                    );
            }
        }

		public Vendor FirstPreferredVendor
		{
            get { return _firstPreferredVendorId == default(long) ? null : _firstPreferredVendor ?? (_firstPreferredVendor = new Vendor(_firstPreferredVendorId)); }
            set
            {
                _firstPreferredVendor = value;
                _firstPreferredVendorId = value == null ? default(long) : value.Id;
            }
		}

        public Vendor SecondPreferredVendor
        {
            get { return _secondPreferredVendorId == default(long) ? null : _secondPreferredVendor ?? (_secondPreferredVendor = new Vendor(_secondPreferredVendorId)); }
            set
            {
                _secondPreferredVendor = value;
                _secondPreferredVendorId = value == null ? default(long) : value.Id;
            }
        }

        public Vendor ThirdPreferredVendor
        {
            get { return _thirdPreferredVendorId == default(long) ? null : _thirdPreferredVendor ?? (_thirdPreferredVendor = new Vendor(_thirdPreferredVendorId)); }
            set
            {
                _thirdPreferredVendor = value;
                _thirdPreferredVendorId = value == null ? default(long) : value.Id;
            }
        }
        public bool IsNew { get { return Id == default(long); } }

        public CustomerTLTenderingProfileLane()
        {

        }

        public CustomerTLTenderingProfileLane(long id)
            : this(id, false)
        {
        }

        public CustomerTLTenderingProfileLane(long id, bool takeSnapshot)
        {
            Id = id;
            if (Id == default(long)) return;
            KeyLoaded = Load();
            if (takeSnapshot) TakeSnapShot();
        }

        public CustomerTLTenderingProfileLane(DbDataReader reader)
        {
            Load(reader);
        }

        public void Save()
        {
            if (Id == default(long)) Insert();
            else Update();
        }

        public new void Delete()
        {
            if (Id != default(long))
                base.Delete();
        }
    }
}
