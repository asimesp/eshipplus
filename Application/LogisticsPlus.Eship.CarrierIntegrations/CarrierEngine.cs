﻿namespace LogisticsPlus.Eship.CarrierIntegrations
{
	public enum CarrierEngine
	{
		Estes,
        DaytonFreight,
        FedExFreightEconomy,
        FedExFreightPriority,
        Holland,
        PittOhio,
        SouthEasternFreight,
        SouthwesternMotorTransport,
        Ups,
		Ward,
        YrcStandard,
        YrcAccelerated,
        Project44
	}
}
