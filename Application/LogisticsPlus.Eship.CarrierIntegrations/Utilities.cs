﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticssPlus.FedExPlugin.PickupWebService;
using LogisticssPlus.FedExPlugin.Services;
using Contact = LogisticssPlus.FedExPlugin.PickupWebService.Contact;

namespace LogisticsPlus.Eship.CarrierIntegrations
{
    internal static class Utilities
    {
        internal const char PipeChar = '|';
        internal const string FunctionNotAvailableMsg = "Function not available. Please call carrier";

        private const string FormattedGuaranteedDelivery = "Guaranteed Delivery: {0} ";
        private const string FormattedSpecialServices = "Special Services: {0} ";
        internal const string FormattedInstructions = "Pickup Instructions: {0} ";

        private static readonly ServiceDispatchFlag[] FedExSupportedServices = new[]
            {
                ServiceDispatchFlag.LiftGate,
                ServiceDispatchFlag.BlindShipment,
                ServiceDispatchFlag.BrokerSelectOption,
                ServiceDispatchFlag.DangerousGoods,
                ServiceDispatchFlag.Detention,
                ServiceDispatchFlag.DryIce,
                ServiceDispatchFlag.Exhibition,
                ServiceDispatchFlag.ExtraLabor,
                ServiceDispatchFlag.ExtremeLength,
                ServiceDispatchFlag.HoldAtLocation,
                ServiceDispatchFlag.Inside,
                ServiceDispatchFlag.LimitedAccess,
                ServiceDispatchFlag.MarkingOrTagging,
                ServiceDispatchFlag.Port,
                ServiceDispatchFlag.PreDeliveryNotification,
                ServiceDispatchFlag.ProtectionFromFreezing,
                ServiceDispatchFlag.Saturday,
                ServiceDispatchFlag.SortAndSegregate,
                ServiceDispatchFlag.Storage
            };

        internal static int ToInt(this object value)
        {
            var x = default(int);
            return value == null
                       ? x
                       : value.GetType().IsEnum
                             ? (int)value
                             : Int32.TryParse(value.ToString(), out x) ? x : default(int);
        }

        internal static long ToLong(this object value)
        {
            var x = default(long);
            return value == null ? x : Int64.TryParse(value.ToString(), out x) ? x : default(long);
        }

        internal static string Truncate(this string value, int length)
        {
            return value.Length >= length ? value.Substring(0, length) : value;
        }

        internal static string GetPhoneNumber(this User user)
        {
            var raw = user.Phone;
            var number = raw.Where(char.IsDigit).Aggregate(string.Empty, (current, ch) => current + ch);
            if (number.Length > 10) number = number.Substring(1, 10);
            if (number.Length < 10)
            {
                number = user.DefaultCustomer.Tier.TollFreeContactNumber
                             .Where(char.IsDigit)
                             .Aggregate(string.Empty, (current, ch) => current + ch);
                if (number.Length > 10) number = number.Substring(1, 10);
            }
            if (number.Length < 10) number = "9999999999";
            return number;
        }

        internal static string GetFormattedTierPhoneNumber(this string phoneNumber)
        {
            var raw = phoneNumber;
            var number = raw.Where(char.IsDigit).Aggregate(string.Empty, (current, ch) => current + ch);
            if (number.Length > 10) number = number.Substring(1, 10);

            return number;
        }

        internal static string FormattedShortDateWithoutDelimiter(this DateTime date)
        {
            return string.Format("{0:yyyyMMdd}", date);
        }

        internal static string FormattedHollandTime(this DateTime date)
        {
            return string.Format("{0:HHmmss}", date);
        }

        internal static string FormattedTimeHoursMinutesNoColon(this DateTime date)
        {
            return string.Format("{0:HHmm}", date);
        }


        internal static string FormattedGuaranteedDeliveryNote(this Shipment shipment)
        {
            return shipment.IsGuaranteedDeliveryService
                       ? string.Format(FormattedGuaranteedDelivery, shipment.EstimatedDeliveryDate.SetTime(shipment.GuaranteedDeliveryServiceTime))
                       : string.Empty;
        }

        internal static string FormattedPickupInstructions(this string instructions)
        {
            return !string.IsNullOrEmpty(instructions)
                ? string.Format(FormattedInstructions, instructions)
                : string.Empty;
        }

        internal static string FormattedShipmentServices(this List<ShipmentService> services)
        {
            return services.Any(s => s.Service.ApplicableAtPickup && s.Service.Category == ServiceCategory.Accessorial)
                ? string.Format(FormattedSpecialServices, string.Join(",", services.Where(s => s.Service.ApplicableAtPickup && s.Service.Category == ServiceCategory.Accessorial).Select(s => s.Service.Description)))
                : string.Empty;
        }


        internal static CreatePickupRequest ToCreateFedExPickupRequest(this Shipment shipment, User activeUser, ServiceParams parameters, ServiceType serviceType)
        {
            var pickupNotes = shipment.Services.Any(s => s.Service.ApplicableAtPickup && s.Service.Category == ServiceCategory.Accessorial && !FedExSupportedServices.Contains(s.Service.DispatchFlag))
                ? string.Format(FormattedSpecialServices, string.Join(",", shipment.Services.Where(s => s.Service.ApplicableAtPickup && s.Service.Category == ServiceCategory.Accessorial && !FedExSupportedServices.Contains(s.Service.DispatchFlag)).Select(s => s.Service.Description)))
                : string.Empty;
            pickupNotes += shipment.Origin.SpecialInstructions.FormattedPickupInstructions();
            var tierPhoneNumber = activeUser.DefaultCustomer.Tier.TollFreeContactNumber.GetFormattedTierPhoneNumber();
            var contactPhone = string.IsNullOrEmpty(tierPhoneNumber) ? activeUser.GetPhoneNumber() : tierPhoneNumber;
            var pickupRequest = new CreatePickupRequest
                {
                    WebAuthenticationDetail = new WebAuthenticationDetail
                        {
                            UserCredential = new WebAuthenticationCredential
                                    {
                                        Key = parameters.Key,
                                        Password = parameters.Password
                                    }
                        },
                    ClientDetail = new ClientDetail
                        {
                            AccountNumber = parameters.AccountNumber,
                            MeterNumber = parameters.MeterNumber
                        },
                    TransactionDetail = new TransactionDetail
                        {
                            CustomerTransactionId = parameters.TransactionId
                        },
                    Version = new VersionId(),
                    OriginDetail = new PickupOriginDetail
                        {
                            PickupLocation = new ContactAndAddress
                                {
                                    Contact = new Contact
                                        {
                                            PersonName = activeUser.FullName,
                                            PhoneNumber = contactPhone,
                                            EMailAddress = activeUser.Email,
                                            FaxNumber = activeUser.Fax,
                                            CompanyName = shipment.Origin.Description
                                        },
                                    Address = new Address
                                        {
                                            StreetLines = new[] { shipment.Origin.Street1, shipment.Origin.Street2 },
                                            City = shipment.Origin.City,
                                            StateOrProvinceCode = shipment.Origin.State,
                                            PostalCode = shipment.Origin.PostalCode,
                                            CountryCode = shipment.Origin.Country.Code,
                                            CountryName = shipment.Origin.Country.Name,
                                        }
                                },
                            PackageLocation = PickupBuildingLocationType.NONE,
                            PackageLocationSpecified = false,
                            BuildingPartSpecified = false,
                            BuildingPartDescription = string.Empty,
                            BuildingPart = BuildingPartCode.BUILDING,
                            ReadyTimestamp = shipment.DesiredPickupDate.SetTime(shipment.EarlyPickup),
                            ReadyTimestampSpecified = true,
                            CompanyCloseTime = shipment.DesiredPickupDate.SetTime(shipment.LatePickup),
                            CompanyCloseTimeSpecified = true,
                        },
                    PackageCount = shipment.Items.Sum(i => i.Quantity).ToString(),
                    TotalWeight = new Weight
                        {
                            Value = shipment.Items.Sum(i => i.ActualWeight),
                            ValueSpecified = true,
                            Units = WeightUnits.LB,
                            UnitsSpecified = true
                        },
                    CarrierCode = CarrierCodeType.FXFR,
                    CarrierCodeSpecified = true,
                    OversizePackageCount = "0",
                    Remarks = pickupNotes,
                };

            var baseServices = shipment.Services.GetFedExServices();
            

            var requestItems = new List<FreightPickupLineItem>();
            foreach (var item in shipment.Items)
            {
                var stop = new ShipmentLocation();
                if (item.Delivery == shipment.Destination.StopOrder)
                    stop = shipment.Destination;
                else
                    stop = shipment.Stops.FirstOrDefault(s => s.StopOrder == item.Delivery) ?? new ShipmentLocation();

                var lineItem = new FreightPickupLineItem
                    {
                        Description = item.Description,
                        Destination = new Address
                            {
                                StreetLines = new[] { stop.Street1, stop.Street2 },
                                City = stop.City,
                                StateOrProvinceCode = stop.State,
                                PostalCode = stop.PostalCode,
                                CountryCode = (stop.Country ?? new Country()).Code,
                                CountryName = (stop.Country ?? new Country()).Name,
                            },
                        PackagingSpecified = false,
                        Pieces = item.PieceCount,
                        PiecesSpecified = true,
                        PurchaseOrderNumber = shipment.PurchaseOrderNumber,
                        TotalHandlingUnits = item.Quantity,
                        TotalHandlingUnitsSpecified = true,
                        Weight = new Weight
                            {
                                Units = WeightUnits.LB,
                                UnitsSpecified = true,
                                Value = item.ActualWeight,
                                ValueSpecified = true
                            },
                        Service = serviceType,
                        ServiceSpecified = true
                    };

                var services = new List<ShipmentSpecialServiceType>();

                services.AddRange(baseServices);

                if (item.HazardousMaterial)
                    services.Add(ShipmentSpecialServiceType.DANGEROUS_GOODS);

                if (!item.IsStackable)
                    services.Add(ShipmentSpecialServiceType.DO_NOT_STACK_PALLETS);

                lineItem.SpecialServicesRequested = new ShipmentSpecialServicesRequested { SpecialServiceTypes = services.ToArray() };
                lineItem.ServiceSpecified = services.Any();
                if (shipment.IsGuaranteedDeliveryService)
                {
                    lineItem.SpecialServicesRequested.CustomDeliveryWindowDetail = new CustomDeliveryWindowDetail
                        {
                            RequestDate = shipment.EstimatedDeliveryDate.SetTime(shipment.GuaranteedDeliveryServiceTime),
                            RequestDateSpecified = true,
                            RequestTime = shipment.EstimatedDeliveryDate.SetTime(shipment.GuaranteedDeliveryServiceTime),
                            RequestTimeSpecified = true,
                            Type = CustomDeliveryWindowType.BEFORE,
                            TypeSpecified = true,
                        };
                }
                requestItems.Add(lineItem);
            }

            pickupRequest.FreightPickupDetail = new FreightPickupDetail
                {
                    LineItems = requestItems.ToArray(),
                    Role = FreightShipmentRoleType.SHIPPER,
                    RoleSpecified = true,
                    Payment = PaymentType.THIRD_PARTY,
                    PaymentSpecified = true,
                };

            return pickupRequest;
        }

        internal static List<ShipmentSpecialServiceType> GetFedExServices(this List<ShipmentService> services)
        {
            var fedExServices = new List<ShipmentSpecialServiceType>();
            foreach (var shipmentService in services.Where(s => s.Service.ApplicableAtPickup && s.Service.Category == ServiceCategory.Accessorial))
            {
                switch (shipmentService.Service.DispatchFlag)
                {
                    case ServiceDispatchFlag.LiftGate:
                        fedExServices.Add(ShipmentSpecialServiceType.LIFTGATE_PICKUP);
                        break;
                    case ServiceDispatchFlag.BlindShipment:
                        fedExServices.Add(ShipmentSpecialServiceType.BLIND_SHIPMENT);
                        break;
                    case ServiceDispatchFlag.BrokerSelectOption:
                        fedExServices.Add(ShipmentSpecialServiceType.BROKER_SELECT_OPTION);
                        break;
                    case ServiceDispatchFlag.Detention:
                        fedExServices.Add(ShipmentSpecialServiceType.DETENTION);
                        break;
                    case ServiceDispatchFlag.DryIce:
                        fedExServices.Add(ShipmentSpecialServiceType.DRY_ICE);
                        break;
                    case ServiceDispatchFlag.Exhibition:
                        fedExServices.Add(ShipmentSpecialServiceType.EXHIBITION_PICKUP);
                        break;
                    case ServiceDispatchFlag.ExtraLabor:
                        fedExServices.Add(ShipmentSpecialServiceType.EXTRA_LABOR);
                        break;
                    case ServiceDispatchFlag.ExtremeLength:
                        fedExServices.Add(ShipmentSpecialServiceType.EXTREME_LENGTH);
                        break;
                    case ServiceDispatchFlag.HoldAtLocation:
                        fedExServices.Add(ShipmentSpecialServiceType.HOLD_AT_LOCATION);
                        break;
                    case ServiceDispatchFlag.Inside:
                        fedExServices.Add(ShipmentSpecialServiceType.INSIDE_PICKUP);
                        break;
                    case ServiceDispatchFlag.LimitedAccess:
                        fedExServices.Add(ShipmentSpecialServiceType.LIMITED_ACCESS_PICKUP);
                        break;
                    case ServiceDispatchFlag.MarkingOrTagging:
                        fedExServices.Add(ShipmentSpecialServiceType.MARKING_OR_TAGGING);
                        break;
                    case ServiceDispatchFlag.Port:
                        fedExServices.Add(ShipmentSpecialServiceType.PORT_PICKUP);
                        break;
                    case ServiceDispatchFlag.PreDeliveryNotification:
                        fedExServices.Add(ShipmentSpecialServiceType.PRE_DELIVERY_NOTIFICATION);
                        break;
                    case ServiceDispatchFlag.ProtectionFromFreezing:
                        fedExServices.Add(ShipmentSpecialServiceType.PROTECTION_FROM_FREEZING);
                        break;
                    case ServiceDispatchFlag.Saturday:
                        fedExServices.Add(ShipmentSpecialServiceType.SATURDAY_PICKUP);
                        break;
                    case ServiceDispatchFlag.SortAndSegregate:
                        fedExServices.Add(ShipmentSpecialServiceType.SORT_AND_SEGREGATE);
                        break;
                    case ServiceDispatchFlag.Storage:
                        fedExServices.Add(ShipmentSpecialServiceType.STORAGE);
                        break;
                }
            }
            return fedExServices;
        }
    }
}
