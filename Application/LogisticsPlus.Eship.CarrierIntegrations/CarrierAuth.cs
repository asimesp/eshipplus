﻿namespace LogisticsPlus.Eship.CarrierIntegrations
{
	public class CarrierAuth
	{
		public string Username { get; set; }
		public string Password { get; set; }
	}
}
