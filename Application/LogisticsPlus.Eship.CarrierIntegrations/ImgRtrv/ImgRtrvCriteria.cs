﻿using System.Collections.Generic;

namespace LogisticsPlus.Eship.CarrierIntegrations.ImgRtrv
{
	public class ImgRtrvCriteria
	{
		public string UniqueRequestKey { get; set; }
		public string ProNumber { get; set; }
		public string BolNumber { get; set; }

		public List<ImgRtrvDocType> ExcludeDocTypes { get; set; }

		public ImgRtrvCriteria()
		{
			ExcludeDocTypes = new List<ImgRtrvDocType>();
			UniqueRequestKey = string.Empty;
			ProNumber = string.Empty;
			BolNumber = string.Empty;
		}
	}
}
