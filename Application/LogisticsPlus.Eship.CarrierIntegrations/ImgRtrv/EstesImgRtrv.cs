﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.CarrierIntegrations.estesImgRetWs;

namespace LogisticsPlus.Eship.CarrierIntegrations.ImgRtrv
{
	public class EstesImgRtrv
	{
		// Doc types from EstesImgRtrv
		private const string Pod = "DR";
		private const string Bol = "BOL";

		private readonly ImageViewService _ews;

		public EstesImgRtrv(CarrierAuth auth)
		{
			_ews = new ImageViewService
				{
					auth = new AuthenticationType
						{
							user = auth.Username,
							password = auth.Password
						}
				};
		}

		public string Echo(ImgRtrvCriteria criteria)
		{
			return _ews.echo(criteria.UniqueRequestKey);
		}

		public List<ImgRtrvDoc> GetImages(List<ImgRtrvCriteria> requests)
		{
			var ret = new List<ImgRtrvDoc>();
			
			foreach (var request in requests)
			{
				try
				{
					var rq = new availRequest
						{
							requestID = request.UniqueRequestKey,
							Item = request.ProNumber
						};

					var rp = _ews.imageCheck(rq);

					var success = rp.Item as SuccessType;
					if (success == null) continue;

					foreach (var document in success.documents)
					{
						var imgRtrvDocType = GetDocType(document);

						if (request.ExcludeDocTypes.Contains(imgRtrvDocType)) continue;

						var imgr = new imgRequest
							{
								requestID = request.UniqueRequestKey,
								Item = success.search,
								document = document
							};

						var img = _ews.imageView(imgr);
						var doc = img.Item as ImagesType;
						if (doc == null) continue;
						foreach (var dimg in doc.image)
							ret.Add(new ImgRtrvDoc
								{
									UniqueRequestKey = request.UniqueRequestKey,
									Type = imgRtrvDocType,
									FileContent = Convert.FromBase64String(dimg.source),
									Filename = dimg.fileName
								});
					}
				}
				catch
				{
					// continue: When exception, estes doesn't have info we are looking for
					continue;
				}
			}

			return ret;
		}

		private static ImgRtrvDocType GetDocType(string document)
		{
			switch (document)
			{
				case Pod:
					return ImgRtrvDocType.POD;
				case Bol:
					return ImgRtrvDocType.BOL;
				default:
					return ImgRtrvDocType.NA;
			}
		}
	}
}
