﻿namespace LogisticsPlus.Eship.CarrierIntegrations.ImgRtrv
{
	public class ImgRtrvDoc
	{
		public string UniqueRequestKey { get; set; }
		public ImgRtrvDocType Type { get; set; }
		public string Filename { get; set; }
		public byte[] FileContent { get; set; }
	}
}
