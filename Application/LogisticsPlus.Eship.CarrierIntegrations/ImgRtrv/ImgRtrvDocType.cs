﻿namespace LogisticsPlus.Eship.CarrierIntegrations.ImgRtrv
{
	public enum ImgRtrvDocType
	{
		// Do not re-arrange
		POD,
		BOL,
		WNI,
		NA,
	}
}
