﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using LogisticsPlus.Eship.CarrierIntegrations.ImgRtrv.Dayton;
using LogisticsPlus.Eship.Core;

namespace LogisticsPlus.Eship.CarrierIntegrations.ImgRtrv
{
    public class DaytonFreightImgRtrv
    {
        //Doc Types from Dayton Freight
        private const string Pod = "DELIVERY RECEIPT";
        private const string Bol = "BILL OF LADING";

        private const string PngFormat = "png";

        private readonly CarrierAuth _credentials;

        public DaytonFreightImgRtrv(CarrierAuth auth)
        {
            _credentials = auth;
        }


        public List<ImgRtrvDoc> GetImages(List<ImgRtrvCriteria> requests)
        {
            var ret = new List<ImgRtrvDoc>();
            foreach (var request in requests)
            {
                try
                {
                    var imagesToGet = SendGetRequestToDayton<DaytonImageSearchResponse>(string.Format("https://api.daytonfreight.com/public/3/api/images/search?pro={0}", request.ProNumber));
                    
                    // Pause to prevent exceeding rate limit
                    Thread.Sleep(5000);

                    foreach (DaytonImageSearchResponseDocumentType document in imagesToGet.Documents.Types)
                    {
                        var type = GetDocType(document.Type);
                        if (request.ExcludeDocTypes.Contains(type) || type == ImgRtrvDocType.NA) continue;

                        var daytonImageResponse = SendGetRequestToDayton<DaytonImageResponse>(string.Format("https://api.daytonfreight.com/public/3/api/images?pro={0}&type={1}&hash={2}&format={3}", request.ProNumber, document.Type, document.Hash, PngFormat));
                        ret.AddRange(daytonImageResponse.ImageData.Images.Select((t, index) => new ImgRtrvDoc
                            {
                                FileContent = Convert.FromBase64String((string) t),
                                Filename = daytonImageResponse.ImageData.Images.Length > 1 
                                    ? string.Format("{0}_{1}.{2}", GetFilenameBasedOnDocType(type), index + 1, PngFormat) 
                                    : string.Format("{0}.{1}", GetFilenameBasedOnDocType(type), PngFormat), 
                                Type = type, 
                                UniqueRequestKey = request.UniqueRequestKey
                            }));

                        // Pause to prevent exceeding rate limit
                        Thread.Sleep(5000);
                    }
                }
                catch
                {
                    // continue: When exception, dayton freight doesn't have info we are looking for
                    continue;
                }
            }
            return ret;
        }


        private T SendGetRequestToDayton<T>(string url) where T : class
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "application/xml";
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:28.0) Gecko/20100101 Firefox/28.0";
            request.Headers[HttpRequestHeader.Authorization] = string.Format("{0} {1}", "Basic", Convert.ToBase64String(Encoding.ASCII.GetBytes(string.Format("{0}:{1}", _credentials.Username, _credentials.Password))));
            request.Host = "api.daytonfreight.com";

            string xml;
            var resopnse = (HttpWebResponse)request.GetResponse();
            using (var reader = new StreamReader(resopnse.GetResponseStream())) xml = reader.ReadToEnd();

            return xml.FromXml<T>();
        }


        private static ImgRtrvDocType GetDocType(string docType)
        {
            switch (docType)
            {
                case Bol:
                    return ImgRtrvDocType.BOL;
                case Pod:
                    return ImgRtrvDocType.POD;
                default:
                    return ImgRtrvDocType.NA;
            }
        }

        private static string GetFilenameBasedOnDocType(ImgRtrvDocType docType)
        {
            switch (docType)
            {
                case ImgRtrvDocType.BOL:
                    return "BillOfLading";
                case ImgRtrvDocType.POD:
                    return "ProofOfDelivery";
                case ImgRtrvDocType.WNI:
                    return "WeightAndNumbersInspection";
                default:
                    return string.Empty;
            }
        }
    }
}
