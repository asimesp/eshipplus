﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.CarrierIntegrations.ImgRtrv.Dayton
{
    [Serializable]
    [XmlRoot("imageDataResponse", Namespace = "http://api.daytonfreight.com")]
    public class DaytonImageResponse
    {
        [XmlElement("imageData")]
        public DaytonImageData ImageData { get; set; }
    }
}
