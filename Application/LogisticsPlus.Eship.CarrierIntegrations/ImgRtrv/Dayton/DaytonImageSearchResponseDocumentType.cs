﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.CarrierIntegrations.ImgRtrv.Dayton
{
    [Serializable]
    [XmlRoot("type")]
    public class DaytonImageSearchResponseDocumentType
    {
        [XmlElement("hash")]
        public string Hash { get; set; }

        [XmlElement("pages")]
        public int Pages { get; set; }

        [XmlElement("type")]
        public string Type { get; set; }
    }
}
