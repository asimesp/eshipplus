﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.CarrierIntegrations.ImgRtrv.Dayton
{
    [Serializable]
    [XmlRoot("document")]
    public class DaytonImageSearchResponseDocument
    {
        [XmlElement(Type = typeof(DaytonImageSearchResponseDocumentType), ElementName = "types")]
        public object[] Types { get; set; }
    }
}
