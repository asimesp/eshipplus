﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.CarrierIntegrations.ImgRtrv.Dayton
{
    [Serializable]
    [XmlRoot("imageSearchResponse", Namespace = "http://api.daytonfreight.com")]
    public class DaytonImageSearchResponse
    {
        [XmlElement("documents")]
        public DaytonImageSearchResponseDocument Documents { get; set; }
    }
}
