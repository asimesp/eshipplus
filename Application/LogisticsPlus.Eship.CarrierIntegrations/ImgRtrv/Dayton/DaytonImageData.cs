﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.CarrierIntegrations.ImgRtrv.Dayton
{
    [Serializable]
    [XmlRoot("imageData")]
    public class DaytonImageData
    {
        [XmlElement(Type = typeof(string), ElementName = "image")]
        public object[] Images { get; set; }
    }
}
