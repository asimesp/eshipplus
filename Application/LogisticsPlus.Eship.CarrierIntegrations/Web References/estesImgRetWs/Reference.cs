﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.18444.
// 
#pragma warning disable 1591

namespace LogisticsPlus.Eship.CarrierIntegrations.estesImgRetWs {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.18408")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="imageViewBinding", Namespace="http://ws.estesexpress.com/imageview")]
    public partial class ImageViewService : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback echoOperationCompleted;
        
        private AuthenticationType authField;
        
        private System.Threading.SendOrPostCallback imageCheckOperationCompleted;
        
        private System.Threading.SendOrPostCallback imageViewOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public ImageViewService() {
            this.Url = global::LogisticsPlus.Eship.CarrierIntegrations.Properties.Settings.Default.LogisticsPlus_Eship_ImageRetrieval_estesImgRetWs_ImageViewService;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public AuthenticationType auth {
            get {
                return this.authField;
            }
            set {
                this.authField = value;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event echoCompletedEventHandler echoCompleted;
        
        /// <remarks/>
        public event imageCheckCompletedEventHandler imageCheckCompleted;
        
        /// <remarks/>
        public event imageViewCompletedEventHandler imageViewCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://ws.estesexpress.com/echo", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("echoResponse", Namespace="http://ws.estesexpress.com/imageview")]
        public string echo([System.Xml.Serialization.XmlElementAttribute(Namespace="http://ws.estesexpress.com/imageview")] string echoRequest) {
            object[] results = this.Invoke("echo", new object[] {
                        echoRequest});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void echoAsync(string echoRequest) {
            this.echoAsync(echoRequest, null);
        }
        
        /// <remarks/>
        public void echoAsync(string echoRequest, object userState) {
            if ((this.echoOperationCompleted == null)) {
                this.echoOperationCompleted = new System.Threading.SendOrPostCallback(this.OnechoOperationCompleted);
            }
            this.InvokeAsync("echo", new object[] {
                        echoRequest}, this.echoOperationCompleted, userState);
        }
        
        private void OnechoOperationCompleted(object arg) {
            if ((this.echoCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.echoCompleted(this, new echoCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("auth")]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://ws.estesexpress.com/imagecheck", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("availResponse", Namespace="http://ws.estesexpress.com/imageview")]
        public availResponse imageCheck([System.Xml.Serialization.XmlElementAttribute(Namespace="http://ws.estesexpress.com/imageview")] availRequest availRequest) {
            object[] results = this.Invoke("imageCheck", new object[] {
                        availRequest});
            return ((availResponse)(results[0]));
        }
        
        /// <remarks/>
        public void imageCheckAsync(availRequest availRequest) {
            this.imageCheckAsync(availRequest, null);
        }
        
        /// <remarks/>
        public void imageCheckAsync(availRequest availRequest, object userState) {
            if ((this.imageCheckOperationCompleted == null)) {
                this.imageCheckOperationCompleted = new System.Threading.SendOrPostCallback(this.OnimageCheckOperationCompleted);
            }
            this.InvokeAsync("imageCheck", new object[] {
                        availRequest}, this.imageCheckOperationCompleted, userState);
        }
        
        private void OnimageCheckOperationCompleted(object arg) {
            if ((this.imageCheckCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.imageCheckCompleted(this, new imageCheckCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("auth")]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://ws.estesexpress.com/imageview", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("imgResponse", Namespace="http://ws.estesexpress.com/imageview")]
        public imgResponse imageView([System.Xml.Serialization.XmlElementAttribute(Namespace="http://ws.estesexpress.com/imageview")] imgRequest imgRequest) {
            object[] results = this.Invoke("imageView", new object[] {
                        imgRequest});
            return ((imgResponse)(results[0]));
        }
        
        /// <remarks/>
        public void imageViewAsync(imgRequest imgRequest) {
            this.imageViewAsync(imgRequest, null);
        }
        
        /// <remarks/>
        public void imageViewAsync(imgRequest imgRequest, object userState) {
            if ((this.imageViewOperationCompleted == null)) {
                this.imageViewOperationCompleted = new System.Threading.SendOrPostCallback(this.OnimageViewOperationCompleted);
            }
            this.InvokeAsync("imageView", new object[] {
                        imgRequest}, this.imageViewOperationCompleted, userState);
        }
        
        private void OnimageViewOperationCompleted(object arg) {
            if ((this.imageViewCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.imageViewCompleted(this, new imageViewCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://ws.estesexpress.com/imageview")]
    [System.Xml.Serialization.XmlRootAttribute("auth", Namespace="http://ws.estesexpress.com/imageview", IsNullable=false)]
    public partial class AuthenticationType : System.Web.Services.Protocols.SoapHeader {
        
        private string userField;
        
        private string passwordField;
        
        /// <remarks/>
        public string user {
            get {
                return this.userField;
            }
            set {
                this.userField = value;
            }
        }
        
        /// <remarks/>
        public string password {
            get {
                return this.passwordField;
            }
            set {
                this.passwordField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://ws.estesexpress.com/imageview")]
    public partial class ImagesType {
        
        private ImageType[] imageField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("image")]
        public ImageType[] image {
            get {
                return this.imageField;
            }
            set {
                this.imageField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://ws.estesexpress.com/imageview")]
    public partial class ImageType {
        
        private string fileNameField;
        
        private string sourceField;
        
        /// <remarks/>
        public string fileName {
            get {
                return this.fileNameField;
            }
            set {
                this.fileNameField = value;
            }
        }
        
        /// <remarks/>
        public string source {
            get {
                return this.sourceField;
            }
            set {
                this.sourceField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://ws.estesexpress.com/imageview")]
    public partial class SuccessType {
        
        private SearchCriteriaType searchField;
        
        private string[] documentsField;
        
        /// <remarks/>
        public SearchCriteriaType search {
            get {
                return this.searchField;
            }
            set {
                this.searchField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("document", IsNullable=false)]
        public string[] documents {
            get {
                return this.documentsField;
            }
            set {
                this.documentsField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://ws.estesexpress.com/imageview")]
    public partial class SearchCriteriaType {
        
        private string item1Field;
        
        private string item2Field;
        
        private string item3Field;
        
        private string item4Field;
        
        private string item5Field;
        
        /// <remarks/>
        public string item1 {
            get {
                return this.item1Field;
            }
            set {
                this.item1Field = value;
            }
        }
        
        /// <remarks/>
        public string item2 {
            get {
                return this.item2Field;
            }
            set {
                this.item2Field = value;
            }
        }
        
        /// <remarks/>
        public string item3 {
            get {
                return this.item3Field;
            }
            set {
                this.item3Field = value;
            }
        }
        
        /// <remarks/>
        public string item4 {
            get {
                return this.item4Field;
            }
            set {
                this.item4Field = value;
            }
        }
        
        /// <remarks/>
        public string item5 {
            get {
                return this.item5Field;
            }
            set {
                this.item5Field = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://ws.estesexpress.com/imageview")]
    public partial class availRequest {
        
        private string requestIDField;
        
        private object itemField;
        
        /// <remarks/>
        public string requestID {
            get {
                return this.requestIDField;
            }
            set {
                this.requestIDField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("pro", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("search", typeof(SearchCriteriaType))]
        public object Item {
            get {
                return this.itemField;
            }
            set {
                this.itemField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://ws.estesexpress.com/imageview")]
    public partial class availResponse {
        
        private string requestIDField;
        
        private object itemField;
        
        /// <remarks/>
        public string requestID {
            get {
                return this.requestIDField;
            }
            set {
                this.requestIDField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("errorMessage", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("success", typeof(SuccessType))]
        public object Item {
            get {
                return this.itemField;
            }
            set {
                this.itemField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://ws.estesexpress.com/imageview")]
    public partial class imgRequest {
        
        private string requestIDField;
        
        private object itemField;
        
        private string documentField;
        
        /// <remarks/>
        public string requestID {
            get {
                return this.requestIDField;
            }
            set {
                this.requestIDField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("pro", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("search", typeof(SearchCriteriaType))]
        public object Item {
            get {
                return this.itemField;
            }
            set {
                this.itemField = value;
            }
        }
        
        /// <remarks/>
        public string document {
            get {
                return this.documentField;
            }
            set {
                this.documentField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://ws.estesexpress.com/imageview")]
    public partial class imgResponse {
        
        private string requestIDField;
        
        private SearchCriteriaType searchField;
        
        private object itemField;
        
        /// <remarks/>
        public string requestID {
            get {
                return this.requestIDField;
            }
            set {
                this.requestIDField = value;
            }
        }
        
        /// <remarks/>
        public SearchCriteriaType search {
            get {
                return this.searchField;
            }
            set {
                this.searchField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("errorMessage", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("images", typeof(ImagesType))]
        public object Item {
            get {
                return this.itemField;
            }
            set {
                this.itemField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.18408")]
    public delegate void echoCompletedEventHandler(object sender, echoCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.18408")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class echoCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal echoCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.18408")]
    public delegate void imageCheckCompletedEventHandler(object sender, imageCheckCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.18408")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class imageCheckCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal imageCheckCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public availResponse Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((availResponse)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.18408")]
    public delegate void imageViewCompletedEventHandler(object sender, imageViewCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.18408")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class imageViewCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal imageViewCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public imgResponse Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((imgResponse)(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591