﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.18444.
// 
#pragma warning disable 1591

namespace LogisticsPlus.Eship.CarrierIntegrations.estesTrackingWs {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.18408")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="shipmentTrackingBinding", Namespace="http://ws.estesexpress.com/shipmenttracking")]
    public partial class ShipmentTrackingService : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback echoOperationCompleted;
        
        private AuthenticationType authField;
        
        private System.Threading.SendOrPostCallback trackShipmentsOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public ShipmentTrackingService() {
            this.Url = global::LogisticsPlus.Eship.CarrierIntegrations.Properties.Settings.Default.LogisticsPlus_Eship_CarrierIntegrations_estesTrackingWs_ShipmentTrackingService;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public AuthenticationType auth {
            get {
                return this.authField;
            }
            set {
                this.authField = value;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event echoCompletedEventHandler echoCompleted;
        
        /// <remarks/>
        public event trackShipmentsCompletedEventHandler trackShipmentsCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://ws.estesexpress.com/shipmenttracking/echo", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("echoResponse", Namespace="http://ws.estesexpress.com/shipmenttracking")]
        public string echo([System.Xml.Serialization.XmlElementAttribute(Namespace="http://ws.estesexpress.com/shipmenttracking")] EchoRequestType echoRequest) {
            object[] results = this.Invoke("echo", new object[] {
                        echoRequest});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void echoAsync(EchoRequestType echoRequest) {
            this.echoAsync(echoRequest, null);
        }
        
        /// <remarks/>
        public void echoAsync(EchoRequestType echoRequest, object userState) {
            if ((this.echoOperationCompleted == null)) {
                this.echoOperationCompleted = new System.Threading.SendOrPostCallback(this.OnechoOperationCompleted);
            }
            this.InvokeAsync("echo", new object[] {
                        echoRequest}, this.echoOperationCompleted, userState);
        }
        
        private void OnechoOperationCompleted(object arg) {
            if ((this.echoCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.echoCompleted(this, new echoCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapHeaderAttribute("auth")]
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://ws.estesexpress.com/shipmenttracking/trackShipments", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Bare)]
        [return: System.Xml.Serialization.XmlElementAttribute("trackingInfo", Namespace="http://ws.estesexpress.com/schema/2012/12/shipmenttracking")]
        public trackingInfo trackShipments([System.Xml.Serialization.XmlElementAttribute(Namespace="http://ws.estesexpress.com/schema/2012/12/shipmenttracking")] search search) {
            object[] results = this.Invoke("trackShipments", new object[] {
                        search});
            return ((trackingInfo)(results[0]));
        }
        
        /// <remarks/>
        public void trackShipmentsAsync(search search) {
            this.trackShipmentsAsync(search, null);
        }
        
        /// <remarks/>
        public void trackShipmentsAsync(search search, object userState) {
            if ((this.trackShipmentsOperationCompleted == null)) {
                this.trackShipmentsOperationCompleted = new System.Threading.SendOrPostCallback(this.OntrackShipmentsOperationCompleted);
            }
            this.InvokeAsync("trackShipments", new object[] {
                        search}, this.trackShipmentsOperationCompleted, userState);
        }
        
        private void OntrackShipmentsOperationCompleted(object arg) {
            if ((this.trackShipmentsCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.trackShipmentsCompleted(this, new trackShipmentsCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://ws.estesexpress.com/shipmenttracking")]
    [System.Xml.Serialization.XmlRootAttribute("auth", Namespace="http://ws.estesexpress.com/shipmenttracking", IsNullable=false)]
    public partial class AuthenticationType : System.Web.Services.Protocols.SoapHeader {
        
        private string userField;
        
        private string passwordField;
        
        /// <remarks/>
        public string user {
            get {
                return this.userField;
            }
            set {
                this.userField = value;
            }
        }
        
        /// <remarks/>
        public string password {
            get {
                return this.passwordField;
            }
            set {
                this.passwordField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://ws.estesexpress.com/schema/2012/12/shipmenttracking")]
    public partial class InterlineInfoType {
        
        private string freightBillField;
        
        private string scacField;
        
        private string nameField;
        
        private string typeField;
        
        /// <remarks/>
        public string freightBill {
            get {
                return this.freightBillField;
            }
            set {
                this.freightBillField = value;
            }
        }
        
        /// <remarks/>
        public string scac {
            get {
                return this.scacField;
            }
            set {
                this.scacField = value;
            }
        }
        
        /// <remarks/>
        public string name {
            get {
                return this.nameField;
            }
            set {
                this.nameField = value;
            }
        }
        
        /// <remarks/>
        public string type {
            get {
                return this.typeField;
            }
            set {
                this.typeField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://ws.estesexpress.com/schema/2012/12/shipmenttracking")]
    public partial class PhoneType {
        
        private ushort countryField;
        
        private bool countryFieldSpecified;
        
        private uint areaCodeField;
        
        private bool areaCodeFieldSpecified;
        
        private ulong subscriberField;
        
        private string extensionField;
        
        /// <remarks/>
        public ushort country {
            get {
                return this.countryField;
            }
            set {
                this.countryField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool countrySpecified {
            get {
                return this.countryFieldSpecified;
            }
            set {
                this.countryFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        public uint areaCode {
            get {
                return this.areaCodeField;
            }
            set {
                this.areaCodeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool areaCodeSpecified {
            get {
                return this.areaCodeFieldSpecified;
            }
            set {
                this.areaCodeFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        public ulong subscriber {
            get {
                return this.subscriberField;
            }
            set {
                this.subscriberField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="nonNegativeInteger")]
        public string extension {
            get {
                return this.extensionField;
            }
            set {
                this.extensionField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://ws.estesexpress.com/schema/2012/12/shipmenttracking")]
    public partial class TerminalType {
        
        private ushort numberField;
        
        private string nameField;
        
        private AddressType addressField;
        
        private PhoneType phoneField;
        
        private PhoneType faxField;
        
        /// <remarks/>
        public ushort number {
            get {
                return this.numberField;
            }
            set {
                this.numberField = value;
            }
        }
        
        /// <remarks/>
        public string name {
            get {
                return this.nameField;
            }
            set {
                this.nameField = value;
            }
        }
        
        /// <remarks/>
        public AddressType address {
            get {
                return this.addressField;
            }
            set {
                this.addressField = value;
            }
        }
        
        /// <remarks/>
        public PhoneType phone {
            get {
                return this.phoneField;
            }
            set {
                this.phoneField = value;
            }
        }
        
        /// <remarks/>
        public PhoneType fax {
            get {
                return this.faxField;
            }
            set {
                this.faxField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://ws.estesexpress.com/schema/2012/12/shipmenttracking")]
    public partial class AddressType {
        
        private string line1Field;
        
        private string line2Field;
        
        private string cityField;
        
        private string stateProvinceField;
        
        private string postalCodeField;
        
        private string countryCodeField;
        
        /// <remarks/>
        public string line1 {
            get {
                return this.line1Field;
            }
            set {
                this.line1Field = value;
            }
        }
        
        /// <remarks/>
        public string line2 {
            get {
                return this.line2Field;
            }
            set {
                this.line2Field = value;
            }
        }
        
        /// <remarks/>
        public string city {
            get {
                return this.cityField;
            }
            set {
                this.cityField = value;
            }
        }
        
        /// <remarks/>
        public string stateProvince {
            get {
                return this.stateProvinceField;
            }
            set {
                this.stateProvinceField = value;
            }
        }
        
        /// <remarks/>
        public string postalCode {
            get {
                return this.postalCodeField;
            }
            set {
                this.postalCodeField = value;
            }
        }
        
        /// <remarks/>
        public string countryCode {
            get {
                return this.countryCodeField;
            }
            set {
                this.countryCodeField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://ws.estesexpress.com/schema/2012/12/shipmenttracking")]
    public partial class CompanyType {
        
        private string referenceNumberField;
        
        private string nameField;
        
        private AddressType addressField;
        
        /// <remarks/>
        public string referenceNumber {
            get {
                return this.referenceNumberField;
            }
            set {
                this.referenceNumberField = value;
            }
        }
        
        /// <remarks/>
        public string name {
            get {
                return this.nameField;
            }
            set {
                this.nameField = value;
            }
        }
        
        /// <remarks/>
        public AddressType address {
            get {
                return this.addressField;
            }
            set {
                this.addressField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://ws.estesexpress.com/schema/2012/12/shipmenttracking")]
    public partial class AppointmentType {
        
        private System.DateTime apptDateField;
        
        private System.DateTime apptTimeField;
        
        private bool apptTimeFieldSpecified;
        
        private string statusField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime apptDate {
            get {
                return this.apptDateField;
            }
            set {
                this.apptDateField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="time")]
        public System.DateTime apptTime {
            get {
                return this.apptTimeField;
            }
            set {
                this.apptTimeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool apptTimeSpecified {
            get {
                return this.apptTimeFieldSpecified;
            }
            set {
                this.apptTimeFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        public string status {
            get {
                return this.statusField;
            }
            set {
                this.statusField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://ws.estesexpress.com/schema/2012/12/shipmenttracking")]
    public partial class MovementType {
        
        private System.DateTime movementDateField;
        
        private System.DateTime movementTimeField;
        
        private string messageField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime movementDate {
            get {
                return this.movementDateField;
            }
            set {
                this.movementDateField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="time")]
        public System.DateTime movementTime {
            get {
                return this.movementTimeField;
            }
            set {
                this.movementTimeField = value;
            }
        }
        
        /// <remarks/>
        public string message {
            get {
                return this.messageField;
            }
            set {
                this.messageField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://ws.estesexpress.com/schema/2012/12/shipmenttracking")]
    public partial class ShipmentType {
        
        private string proField;
        
        private string bolField;
        
        private string poField;
        
        private System.DateTime pickupDateField;
        
        private bool pickupDateFieldSpecified;
        
        private string serviceField;
        
        private string statusField;
        
        private MovementType[] movementHistoryField;
        
        private System.DateTime firstDeliveryDateField;
        
        private bool firstDeliveryDateFieldSpecified;
        
        private System.DateTime estimatedDeliveryDateField;
        
        private bool estimatedDeliveryDateFieldSpecified;
        
        private System.DateTime estimatedDeliveryTimeField;
        
        private bool estimatedDeliveryTimeFieldSpecified;
        
        private System.DateTime deliveryDateField;
        
        private bool deliveryDateFieldSpecified;
        
        private System.DateTime deliveryTimeField;
        
        private bool deliveryTimeFieldSpecified;
        
        private string receivedByField;
        
        private AppointmentType appointmentField;
        
        private uint piecesField;
        
        private bool piecesFieldSpecified;
        
        private uint dimensionalWeightField;
        
        private bool dimensionalWeightFieldSpecified;
        
        private uint weightField;
        
        private bool weightFieldSpecified;
        
        private CompanyType shipperField;
        
        private CompanyType consigneeField;
        
        private CompanyType thirdPartyField;
        
        private TerminalType destinationTerminalField;
        
        private InterlineInfoType interlineInfoField;
        
        private decimal freightChargesField;
        
        private bool freightChargesFieldSpecified;
        
        private string[] messagesField;
        
        /// <remarks/>
        public string pro {
            get {
                return this.proField;
            }
            set {
                this.proField = value;
            }
        }
        
        /// <remarks/>
        public string bol {
            get {
                return this.bolField;
            }
            set {
                this.bolField = value;
            }
        }
        
        /// <remarks/>
        public string po {
            get {
                return this.poField;
            }
            set {
                this.poField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime pickupDate {
            get {
                return this.pickupDateField;
            }
            set {
                this.pickupDateField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool pickupDateSpecified {
            get {
                return this.pickupDateFieldSpecified;
            }
            set {
                this.pickupDateFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        public string service {
            get {
                return this.serviceField;
            }
            set {
                this.serviceField = value;
            }
        }
        
        /// <remarks/>
        public string status {
            get {
                return this.statusField;
            }
            set {
                this.statusField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("message", IsNullable=false)]
        public MovementType[] movementHistory {
            get {
                return this.movementHistoryField;
            }
            set {
                this.movementHistoryField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime firstDeliveryDate {
            get {
                return this.firstDeliveryDateField;
            }
            set {
                this.firstDeliveryDateField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool firstDeliveryDateSpecified {
            get {
                return this.firstDeliveryDateFieldSpecified;
            }
            set {
                this.firstDeliveryDateFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime estimatedDeliveryDate {
            get {
                return this.estimatedDeliveryDateField;
            }
            set {
                this.estimatedDeliveryDateField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool estimatedDeliveryDateSpecified {
            get {
                return this.estimatedDeliveryDateFieldSpecified;
            }
            set {
                this.estimatedDeliveryDateFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="time")]
        public System.DateTime estimatedDeliveryTime {
            get {
                return this.estimatedDeliveryTimeField;
            }
            set {
                this.estimatedDeliveryTimeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool estimatedDeliveryTimeSpecified {
            get {
                return this.estimatedDeliveryTimeFieldSpecified;
            }
            set {
                this.estimatedDeliveryTimeFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="date")]
        public System.DateTime deliveryDate {
            get {
                return this.deliveryDateField;
            }
            set {
                this.deliveryDateField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool deliveryDateSpecified {
            get {
                return this.deliveryDateFieldSpecified;
            }
            set {
                this.deliveryDateFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType="time")]
        public System.DateTime deliveryTime {
            get {
                return this.deliveryTimeField;
            }
            set {
                this.deliveryTimeField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool deliveryTimeSpecified {
            get {
                return this.deliveryTimeFieldSpecified;
            }
            set {
                this.deliveryTimeFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        public string receivedBy {
            get {
                return this.receivedByField;
            }
            set {
                this.receivedByField = value;
            }
        }
        
        /// <remarks/>
        public AppointmentType appointment {
            get {
                return this.appointmentField;
            }
            set {
                this.appointmentField = value;
            }
        }
        
        /// <remarks/>
        public uint pieces {
            get {
                return this.piecesField;
            }
            set {
                this.piecesField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool piecesSpecified {
            get {
                return this.piecesFieldSpecified;
            }
            set {
                this.piecesFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        public uint dimensionalWeight {
            get {
                return this.dimensionalWeightField;
            }
            set {
                this.dimensionalWeightField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool dimensionalWeightSpecified {
            get {
                return this.dimensionalWeightFieldSpecified;
            }
            set {
                this.dimensionalWeightFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        public uint weight {
            get {
                return this.weightField;
            }
            set {
                this.weightField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool weightSpecified {
            get {
                return this.weightFieldSpecified;
            }
            set {
                this.weightFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        public CompanyType shipper {
            get {
                return this.shipperField;
            }
            set {
                this.shipperField = value;
            }
        }
        
        /// <remarks/>
        public CompanyType consignee {
            get {
                return this.consigneeField;
            }
            set {
                this.consigneeField = value;
            }
        }
        
        /// <remarks/>
        public CompanyType thirdParty {
            get {
                return this.thirdPartyField;
            }
            set {
                this.thirdPartyField = value;
            }
        }
        
        /// <remarks/>
        public TerminalType destinationTerminal {
            get {
                return this.destinationTerminalField;
            }
            set {
                this.destinationTerminalField = value;
            }
        }
        
        /// <remarks/>
        public InterlineInfoType interlineInfo {
            get {
                return this.interlineInfoField;
            }
            set {
                this.interlineInfoField = value;
            }
        }
        
        /// <remarks/>
        public decimal freightCharges {
            get {
                return this.freightChargesField;
            }
            set {
                this.freightChargesField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool freightChargesSpecified {
            get {
                return this.freightChargesFieldSpecified;
            }
            set {
                this.freightChargesFieldSpecified = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("message", IsNullable=false)]
        public string[] messages {
            get {
                return this.messagesField;
            }
            set {
                this.messagesField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://ws.estesexpress.com/shipmenttracking")]
    public partial class EchoRequestType {
        
        private string inputField;
        
        /// <remarks/>
        public string input {
            get {
                return this.inputField;
            }
            set {
                this.inputField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://ws.estesexpress.com/schema/2012/12/shipmenttracking")]
    public partial class search {
        
        private string requestIDField;
        
        private string itemField;
        
        private ItemChoiceType itemElementNameField;
        
        /// <remarks/>
        public string requestID {
            get {
                return this.requestIDField;
            }
            set {
                this.requestIDField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("bol", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("interlinePro", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("loadNumber", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("po", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("pro", typeof(string))]
        [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemElementName")]
        public string Item {
            get {
                return this.itemField;
            }
            set {
                this.itemField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public ItemChoiceType ItemElementName {
            get {
                return this.itemElementNameField;
            }
            set {
                this.itemElementNameField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://ws.estesexpress.com/schema/2012/12/shipmenttracking", IncludeInSchema=false)]
    public enum ItemChoiceType {
        
        /// <remarks/>
        bol,
        
        /// <remarks/>
        interlinePro,
        
        /// <remarks/>
        loadNumber,
        
        /// <remarks/>
        po,
        
        /// <remarks/>
        pro,
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.34234")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="http://ws.estesexpress.com/schema/2012/12/shipmenttracking")]
    public partial class trackingInfo {
        
        private string requestIDField;
        
        private ShipmentType[] shipmentsField;
        
        /// <remarks/>
        public string requestID {
            get {
                return this.requestIDField;
            }
            set {
                this.requestIDField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("shipment", IsNullable=false)]
        public ShipmentType[] shipments {
            get {
                return this.shipmentsField;
            }
            set {
                this.shipmentsField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.18408")]
    public delegate void echoCompletedEventHandler(object sender, echoCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.18408")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class echoCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal echoCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.18408")]
    public delegate void trackShipmentsCompletedEventHandler(object sender, trackShipmentsCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.18408")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class trackShipmentsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal trackShipmentsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public trackingInfo Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((trackingInfo)(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591