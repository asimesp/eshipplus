﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using LogisticsPlus.Eship.CarrierIntegrations.Dispatch.Ward;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch
{
    public class WardDispatch
    {
		private readonly CarrierAuth _credentials;

		public List<string> Messages { get; private set; }

		public WardDispatch(CarrierAuth auth)
		{
			_credentials = auth;
			Messages = new List<string>();
		}

        public string DispatchShipment(Shipment shipment, User activeUser)
        {
			Messages.Clear();

            var amount = shipment.Items.Sum(t => t.Value).ToInt();

	        var primaryVendor = shipment.Vendors.First(v => v.Primary).Vendor;
	        var shipmentPckgIds = shipment.Items.Select(i => i.PackageTypeId).Distinct().ToList();
	        var pckMap = primaryVendor.VendorPackageCustomMappings.FirstOrDefault(i => shipmentPckgIds.Contains(i.PackageTypeId));
	        var packageCode = pckMap != null ? pckMap.VendorCode : string.Empty;

            var tierPhoneNumber = activeUser.DefaultCustomer.Tier.TollFreeContactNumber.GetFormattedTierPhoneNumber();
            var contactPhone = string.IsNullOrEmpty(tierPhoneNumber) ? activeUser.GetPhoneNumber() : tierPhoneNumber;

            var pickup = new WardPickupReques
            {
                Body = new WardResponseBody
	                {
                    Request = new WardPickupRequestBody
	                    {
                        Shipment = new WardShipment
	                        {
                            RequestOrigin = string.Empty, 
                            ConsigneeAddress1 = shipment.Destination.Street1,
                            ConsigneeAddress2 = string.IsNullOrEmpty(shipment.Destination.Street2) ? string.Empty : shipment.Destination.Street2, 
                            ConsigneeName = shipment.Destination.Description,
                            ShipperRoutingSCAC = string.Empty,
                            NonStandardSizeDescription = string.Empty, 
                            WardAssuredTimeDefiniteEnd = string.Empty, 
                            WardAssuredTimeDefiniteStart = string.Empty, 
                            PickupShipmentInstruction1 = shipment.Origin.SpecialInstructions,
                            PickupShipmentInstruction2 = string.Empty,
                            PickupShipmentInstruction3 = string.Empty, 
                            PickupShipmentInstruction4 = string.Empty, 
                            FullValueInsuredAmount = amount.ToString(),
                            DeliveryAppntDate = shipment.ActualDeliveryDate.ToString("MMddyyyy"),
                            FullValue = amount == 0 ? "N" : "Y", 
                            WardAssured12PM = string.Empty, 
                            DeliveryAppntFlag = string.Empty,
                            ConsigneeCity = shipment.Destination.City, 
                            WardAssured03PM = string.Empty,
                            ConsigneeState = shipment.Destination.State, 
                            Freezable = "N", 
                            NonStandardSize = string.Empty, 
                            Hazardous = shipment.HazardousMaterial ? "Y" : "N", 
                            Weight = Math.Ceiling(shipment.Items.TotalActualWeight()).ToInt().ToString(),
                            PackageCode = packageCode,
                            ConsigneeZipcode = shipment.Destination.PostalCode, 
                            Pieces = shipment.Items.Sum(t=> t.Quantity).ToString(), 
                            WardAssuredTimeDefinite = string.Empty, 
                            RequestorReference = shipment.ShipmentNumber,
                            ConsigneeCode = string.Empty, 
                        },
                        ShipperInformation = new WardShipperInformation
	                        {
                            RequestOrigin = string.Empty, 
                            ThirdPartyContactName = string.Empty,
                            ShipperZipcode = shipment.Origin.PostalCode,
                            RequestorContactTelephone = contactPhone.StripNonDigits().Truncate(10), 
                            ThirdPartyContactEmail = string.Empty, 
                            WardAssuredContactEmail = string.Empty, 
                            ThirdPartyName = string.Empty, 
                            RequestorContactName = activeUser.FullName, 
                            WardAssuredContactTelephone = string.Empty, 
                            ShipperContactEmail = (shipment.Origin.Contacts.FirstOrDefault(t=> t.Primary) ?? new ShipmentContact()).Email ?? string.Empty, 
                            ShipperRestriction = string.Empty, 
                            RequestorRole = string.Empty,
                            ShipperAddress1 = shipment.Origin.Street1, 
                            ShipperAddress2 = shipment.Origin.Street2, 
                            PickupDate = shipment.DesiredPickupDate.ToString("MMddyyyy"), 
                            ShipperCity = shipment.Origin.City, 
                            ThirdParty = string.Empty, 
                            ShipperContactName = (shipment.Origin.Contacts.FirstOrDefault(t=> t.Primary) ?? new ShipmentContact()).Name ?? string.Empty, 
                            WardAssuredContactName = string.Empty, 
                            ShipperReadyTime = shipment.EarlyPickup.Replace(":", ""),
                            DriverNote1 = string.Empty, 
                            DriverNote2 = string.Empty, 
                            DriverNote3 = string.Empty, 
                            RequestorUser = _credentials.Username, 
                            ShipperContactTelephone = (shipment.Origin.Contacts.FirstOrDefault(t => t.Primary) ?? new ShipmentContact()).Phone.StripNonDigits().Truncate(10) ?? string.Empty,
                            ThirdPartyContactTelephone = string.Empty, 
                            RequestorContactEmail = activeUser.Email, 
                            ShipperName = shipment.Origin.Description, 
                            ShipperCode = string.Empty,
                            ShipperState = shipment.Origin.State, 
                            ShipperCloseTime = shipment.LatePickup.Replace(":", "")
                        }
                    }
                }
            };

			// send request
	        string content = pickup.ToXml();
	        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
	        var request = (HttpWebRequest)WebRequest.Create("http://208.51.75.23:6082/cgi-bin/map/PICKUP");
	        request.Method = "PUT";
	        request.ContentType = "application/xml";
	        request.KeepAlive = false;
	        request.Timeout = 60000;

	        using (var writer = new StreamWriter(request.GetRequestStream()))
		        writer.Write(content);

	        string xml;
	        using (var resopnse = (HttpWebResponse) request.GetResponse())
	        {
		        using (var reader = new StreamReader(resopnse.GetResponseStream())) xml = reader.ReadToEnd();
	        }
	        var response = xml.FromXml<WardPickupRequesResponse>();

	        var pickupConfirmation = response.Body.CreateResponse.CreateResult.PickupConfirmation;

	        if (!string.IsNullOrEmpty(response.Body.CreateResponse.CreateResult.Message))
		        Messages.Add(response.Body.CreateResponse.CreateResult.Message);

	        return pickupConfirmation.Trim() == "0" ? string.Empty : pickupConfirmation;
        }


		public bool CancelDispatchedShipment(string requestNumber)
		{
			throw new Exception(Utilities.FunctionNotAvailableMsg);
		}

		public string UpdateDispatchedShipment(Shipment shipment, User activeUser, string requestNumber)
		{
			throw new Exception(Utilities.FunctionNotAvailableMsg);
		}

		public object CheckDispatchedShimpent(Shipment shipment)
		{
			throw new Exception(Utilities.FunctionNotAvailableMsg);
		}
    }
}
