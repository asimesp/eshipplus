﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using LogisticsPlus.Eship.CarrierIntegrations.Dispatch.Dayton;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch
{
	public class DaytonDispatch
	{
		private readonly CarrierAuth _credentials;

		public DaytonDispatch(CarrierAuth auth)
		{
			_credentials = auth;
		}


		public string DispatchShipment(Shipment shipment, User activeUser)
        {
            var tierPhoneNumber = activeUser.DefaultCustomer.Tier.TollFreeContactNumber.GetFormattedTierPhoneNumber();
            var contactPhone = string.IsNullOrEmpty(tierPhoneNumber) ? activeUser.GetPhoneNumber(): tierPhoneNumber;
            var pickup = new DaytonPickupRequest
				{
					Contact = new DaytonPickupContact
						{
							Name = activeUser.FullName.Truncate(30),
							Phone = contactPhone
                    },
					CustomerReferenceNumber = shipment.ShipmentNumber,
					Details = shipment.Items.Select(i => new DaytonPickupItem
								{
									Comment = i.Description.Truncate(85),
									DestinationPostalCode = shipment.Destination.PostalCode,
									IsFood = false,
									IsFreezable = false,
									IsHazardous = i.HazardousMaterial,
									IsPoisonous = false,
									Quantity = i.Quantity == 0 ? 1 : i.Quantity,
									Weight = (int)i.ActualWeight
								}).ToArray(),
					EarlyPickupDateTime = shipment.DesiredPickupDate.SetTime(shipment.EarlyPickup).FormattedLongDateWithT(),
					LatePickupDateTime = shipment.DesiredPickupDate.SetTime(shipment.LatePickup).FormattedLongDateWithT(),
					PickupInstructions = shipment.Origin.SpecialInstructions.Truncate(85),
					Shipper = new DaytonPickupShipper
						{
							Address = new DaytonAddress
								{
									City = shipment.Origin.City,
									Name = shipment.Origin.Description.Truncate(30),
									PostalCode = shipment.Origin.PostalCode,
									State = shipment.Origin.State,
									Street1 = shipment.Origin.Street1.Truncate(30),
									Street2 = shipment.Origin.Street2.Truncate(30)
								},
							Name = shipment.Origin.Description.Truncate(30)
						},
					Requester = new DaytonPickupContact
						{
							Name = activeUser.FullName.Truncate(30),
							Phone = contactPhone,
						},
					EmailAddressesToSendConfirmation = new string[0],
					EmailAddressesToSendReceipt = new string[0]
				};

			if (shipment.IsGuaranteedDeliveryService)
				foreach (var item in pickup.Details)
					item.GuaranteedServiceOption = shipment.EstimatedDeliveryDate.SetTime(shipment.LateDelivery) <= shipment.EstimatedDeliveryDate.SetTime("12:00") ? "AM" : "PM";

			var reponse = SendDaytonWebRequest<DaytonPickupReponse>("https://api.daytonfreight.com/public/3/api/pickup", "PUT", pickup.ToXml(true));
			return reponse.PickupNumber;
		}

		public bool CancelDispatchedShipment(string requestNumber)
		{
			throw new Exception(Utilities.FunctionNotAvailableMsg);
		}

		public string UpdateDispatchedShipment(Shipment shipment, User activeUser, string requestNumber)
		{
			// Check if we will be able to cancel original request to recreate it
			if (!PickupIsEditable(requestNumber)) return string.Empty;

			// If this fails, then we know we cannot cancel original
			var newRequestNumber = DispatchShipment(shipment, activeUser);

			// Cancel original request
			CancelDispatchedShipment(requestNumber);

			return newRequestNumber;
		}

		public DaytonTrackingResult CheckDispatchedShimpent(Shipment shipment)
		{
			var number = string.Format("{0}{1}", shipment.Prefix == null || shipment.HidePrefix ? string.Empty : shipment.Prefix.Code, shipment.ShipmentNumber);
			var response = SendDaytonWebRequest<DaytonTrackingResponse>(string.Format("https://api.daytonfreight.com/public/3/api/tracking/bynumber?number={0}&type=2", number), "GET");
			return response.Results.FirstOrDefault() ?? new DaytonTrackingResult();
		}


		private bool PickupIsEditable(string requestNumber)
		{
			return SendDaytonWebRequest<DaytonPickupIsEditableRequest>(string.Format("https://api.daytonfreight.com/public/3/api/pickup?number={0}", requestNumber), "GET").IsEditable;
		}

		private T SendDaytonWebRequest<T>(string url, string httpMethod, string content = null) where T : class
		{
			var request = (HttpWebRequest)WebRequest.Create(url);
			request.Method = httpMethod;
			request.ContentType = "application/xml";
			request.Headers[HttpRequestHeader.Authorization] = string.Format("{0} {1}", "Basic", Convert.ToBase64String(Encoding.ASCII.GetBytes(string.Format("{0}:{1}", _credentials.Username, _credentials.Password))));
			request.Host = "api.daytonfreight.com";

			if (!string.IsNullOrEmpty(content))
				using (var writer = new StreamWriter(request.GetRequestStream()))
					writer.Write(content);

			string xml;
			var resopnse = (HttpWebResponse)request.GetResponse();
			using (var reader = new StreamReader(resopnse.GetResponseStream())) xml = reader.ReadToEnd();

			return xml.FromXml<T>();
		}
	}
}
