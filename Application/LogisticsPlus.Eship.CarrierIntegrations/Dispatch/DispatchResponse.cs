﻿using System.Collections.Generic;
using System.Linq;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch
{
	public class DispatchResponse
	{
		public string PickupNumber { get; set; }
		public string ProNumber { get; set; }
		public List<string> Errors { get; set; }
		public bool HasErrors { get { return Errors != null && Errors.Any(); } }
	}
}
