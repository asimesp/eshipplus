﻿using System;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticssPlus.FedExPlugin.PickupWebService;
using LogisticssPlus.FedExPlugin.Services;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch
{
    public class FedExEconomyDispatch
    {
        private const char PipeChar = '|';

        private readonly ServiceParams _serviceParams;


        public FedExEconomyDispatch(CarrierAuth auth)
        {
            var usernamePipeIndex = auth.Username.IndexOf(PipeChar);
            var passwordPipeIndex = auth.Password.IndexOf(PipeChar);

            _serviceParams = new ServiceParams
                {
                    TransactionId = "Logistics Plus Inc Service Production",
                    LocalCountry = "US",
                    Url = "https://gateway.fedex.com:443/web-services"
                };

            if (usernamePipeIndex > -1)
            {
                _serviceParams.AccountNumber = auth.Username.Substring(0, usernamePipeIndex);
                _serviceParams.MeterNumber = auth.Username.Substring(usernamePipeIndex + 1, auth.Username.Length - usernamePipeIndex - 1);
            }

            if (passwordPipeIndex > -1)
            {
                _serviceParams.Password = auth.Password.Substring(0, passwordPipeIndex);
                _serviceParams.Key = auth.Password.Substring(passwordPipeIndex + 1, auth.Password.Length - passwordPipeIndex - 1);
            }
        }


        public DispatchResponse DispatchShipment(Shipment shipment, User activeUser)
        {
            var service = new FedExPickupService(_serviceParams);
            var reply = service.CreateShipmentPickup(shipment.ToCreateFedExPickupRequest(activeUser, _serviceParams, ServiceType.FEDEX_FREIGHT_ECONOMY));

            return new DispatchResponse
            {
                Errors = service.Messages.HasErrors ? service.Messages.Errors : null,
                PickupNumber = reply != null ? reply.PickupConfirmationNumber : string.Empty,
                ProNumber = null
            };
        }

        public DispatchResponse UpdateDispatchedShipment(Shipment shipment, User activeUser, string requestNumber)
        {
            throw new Exception(Utilities.FunctionNotAvailableMsg);
        }

        public bool CancelDispatchedShipment(string requestNumber)
        {
            throw new Exception(Utilities.FunctionNotAvailableMsg);
        } 

        public string CheckDispatchedShipment(Shipment shipment)
        {
            throw new Exception(Utilities.FunctionNotAvailableMsg);
        }
    }
}
