﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using LogisticsPlus.Eship.CarrierIntegrations.Dispatch.Holland;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch
{
    public class HollandDispatch
    {
        private readonly CarrierAuth _credentials;

        public HollandDispatch(CarrierAuth auth)
        {
            _credentials = auth;
        }

        public DispatchResponse DispatchShipment(Shipment shipment, User activeUser)
        {
            var response = SendHollandWebRequest<HollandAccessKeyResponse>(string.Format("https://api.hollandregional.com/api/AccessKey/doAccessKey?username={0}&password={1}", _credentials.Username, _credentials.Password), "POST");

            var tierPhoneNumber = activeUser.DefaultCustomer.Tier.TollFreeContactNumber.GetFormattedTierPhoneNumber();
            var contactPhone = string.IsNullOrEmpty(tierPhoneNumber) ? activeUser.GetPhoneNumber() : tierPhoneNumber;

            var pickupUrl = string.Format("https://api.hollandregional.com/api/PickupRequest/SchedulePickup?accessKey={0}&accountId=399669&direction=3", response.AccessKeyUrlEncoded);
            pickupUrl += string.Format(@"&origCompany={0}&origAddr1={1}&origAddr2={2}&origCity={3}&origState={4}&origZip={5}&pkupContName={6}&pkupContPhone={7}&pkupContEmail={8}&thirdPtyContName={9}
                                        &thirdPtyContPhone={10}&thirdPtyContEmail={11}&pkupDate={12}&readyByTime={13}&dockClosingTime={14}&callBeforePickup=N",
                    shipment.Origin.Description, shipment.Origin.Street1, shipment.Origin.Street2, shipment.Origin.City,
                    shipment.Origin.State.Truncate(2), shipment.Origin.PostalCode.Truncate(5),
                    activeUser.FullName, contactPhone, activeUser.Email, activeUser.FullName,
                    contactPhone, activeUser.Email, shipment.DesiredPickupDate.FormattedShortDateWithoutDelimiter(),
                    shipment.DesiredPickupDate.SetTime(shipment.EarlyPickup).FormattedHollandTime(),
                    shipment.DesiredPickupDate.SetTime(shipment.LatePickup).FormattedHollandTime());

            var isLiftgate = shipment.Services.Any(s => s.Service.ApplicableAtPickup && s.Service.Category == ServiceCategory.Accessorial && s.Service.DispatchFlag == ServiceDispatchFlag.LiftGate);
            var isProtectFromFreezing = shipment.Services.Any(s => s.Service.ApplicableAtPickup && s.Service.Category == ServiceCategory.Accessorial && s.Service.DispatchFlag == ServiceDispatchFlag.ProtectionFromFreezing);

            var hollandService = "30";
            if (shipment.IsGuaranteedDeliveryService)
                switch (shipment.GuaranteedDeliveryServiceTime)
                {
                    case "17:00":
                        hollandService = "32";
                        break;
                    case "12:00":
                        hollandService = "33";
                        break;
                    case "03:30":
                        hollandService = "36";
                        break;
                    case "09:00":
                        hollandService = "37";
                        break;
                }

            for (var index = 0; index < shipment.Items.Count; index++)
            {
                var item = shipment.Items[index];
                var stop = new ShipmentLocation();
                if (item.Delivery == shipment.Destination.StopOrder)
                    stop = shipment.Destination;
                else
                    stop = shipment.Stops.FirstOrDefault(s => s.StopOrder == item.Delivery) ?? new ShipmentLocation();

                pickupUrl += string.Format("&destRef{0}={1}&destZip{0}={2}&destCity{0}={3}&destState{0}={4}&destQty{0}={5}&destPkg{0}=Pallets&destWt{0}={6}&destHazMat{0}={7}&destSvcType{0}={8}",
                        index + 1, stop.Description, stop.PostalCode.Truncate(5), stop.City, stop.State, item.Quantity, (int)Math.Ceiling(item.ActualWeight), item.HazardousMaterial ? "Y" : "N", hollandService);

                if (isLiftgate) pickupUrl += string.Format("&destLiftgate{0}=Y", index + 1);
                if (isProtectFromFreezing) pickupUrl += string.Format("&destFreezeProt{0}=Y", index + 1);
            }

            var pickupResponse = SendHollandWebRequest<HollandPickupRequestResponse>(pickupUrl, "POST");
            var dispatchResponse = new DispatchResponse{PickupNumber = pickupResponse.PickupRequestNumber};

            if(pickupResponse.Status.Code == 1)
                dispatchResponse.Errors = new List<string>{string.Format("{0} - {1}", pickupResponse.Status.ErrorType, pickupResponse.Status.Message)};

            return dispatchResponse;
        }

        public bool CancelDispatchedShipment(string requestNumber)
        {
            throw new Exception(Utilities.FunctionNotAvailableMsg);
        }

        public DispatchResponse UpdateDispatchedShipment(Shipment shipment, User activeUser, string requestNumber)
        {
            throw new Exception(Utilities.FunctionNotAvailableMsg);
        }

        public object CheckDispatchedShipment(Shipment shipment)
        {
            throw new Exception(Utilities.FunctionNotAvailableMsg);
        }


        private static T SendHollandWebRequest<T>(string url, string httpMethod, string content = null) where T : class
        {
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = httpMethod;
            request.ContentType = "application/xml";
            request.ContentLength = 0;

            if (!string.IsNullOrEmpty(content))
                using (var writer = new StreamWriter(request.GetRequestStream()))
                    writer.Write(content);

            string xml;
            var resopnse = (HttpWebResponse)request.GetResponse();
            using (var reader = new StreamReader(resopnse.GetResponseStream())) xml = reader.ReadToEnd();

            return xml.FromXml<T>();
        }
    }
}
