﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Smc;
using LogisticsPlus.Eship.Smc.Eva;
using Newtonsoft.Json;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch.EVA
{
    public class YrcDispatch
    {
        private readonly EvaSettings _settings;

        public YrcDispatch(EvaSettings settings)
        {
            _settings = settings;
        }

        public EvaSynchronousResponse DispatchShipment(Shipment shipment, VendorCommunication vcom)
        {
            var scac = vcom.CarrierIntegrationEngine == CarrierEngine.YrcStandard.FormattedString()
                           ? "RDWY"
                           : "YRCA";

            var primaryVendor = shipment.Vendors.First(v => v.Primary).Vendor;

            // TODO: Docs has a "Appointment required at pickup" service
            var dispatchRequest = SmcUtilities.GenerateGenericDispatchRequest(shipment, vcom);

            dispatchRequest.destinationCountry = GetYrcCountryCode(shipment.Destination.Country.Code);
            dispatchRequest.originCountry = GetYrcCountryCode(shipment.Origin.Country.Code);
            dispatchRequest.identifiers = new List<Identifier>
                                    {
                                        new Identifier {referenceNumber = shipment.ShipmentNumber, referenceType = "BL"}
                                        // TODO: Types supported (but not explicitly defined in docs)
                                        //         BL, CO, LO, PO, RN
                                    };
            dispatchRequest.paymentTerms = "THIRD_PARTY";
            dispatchRequest.scac = scac;
            dispatchRequest.serviceCode = "LTL"; // TODO: Refer to doc for other supported services that may apply here
            dispatchRequest.shipmentCommodity = shipment
                .Items
                .Select(i =>
                    {
                        var vendorPackageCustomMapping = primaryVendor.VendorPackageCustomMappings.FirstOrDefault(v => v.PackageTypeId == i.PackageTypeId);
                        return new ShipmentCommodity
                            {
                                classification = i.ActualFreightClass.ToString(),
                                density = null,
                                densityUnit = null,
                                description = i.Description.EmptyStringToNull(),
                                dimensionUnits = Units.Dimension.Feet,
                                height = i.ActualHeight.InchesToFeet().ToString("n4"),
                                itemNumber = null,
                                length = i.ActualLength.InchesToFeet().ToString("n4"), // TODO: Doc has N/A for this field
                                packagingType =
                                    vendorPackageCustomMapping != null
                                        ? vendorPackageCustomMapping.VendorCode
                                        : i.PackageType.TypeName, // TODO: Review this
                                pieces = i.Quantity.ToString(),
                                subNumber = null,
                                weight = Math.Ceiling(i.ActualWeight).ToInt().ToString(),
                                weightUnit = Units.Weight.Pounds,
                                width = i.ActualWidth.InchesToFeet().ToString("n4"),
                            };
                    })
                .ToList();
            dispatchRequest.shipmentCommodityFreightCodes = shipment.HazardousMaterial
                                                        ? new List<ShipmentCommodityFreightCode>
                                                            {
                                                                new ShipmentCommodityFreightCode
                                                                    {
                                                                        code = "HAZ",
                                                                        details = new List<ShipmentCommodityFreightCodeDetail>
                                                                                {
                                                                                    new ShipmentCommodityFreightCodeDetail
                                                                                        {
                                                                                            name = "Phone",
                                                                                            value = shipment.HazardousMaterialContactPhone
                                                                                        },
                                                                                    new ShipmentCommodityFreightCodeDetail
                                                                                        {
                                                                                            name = "Name",
                                                                                            value = shipment.HazardousMaterialContactName
                                                                                        },
                                                                                    new ShipmentCommodityFreightCodeDetail
                                                                                        {
                                                                                            name = "Mobile",
                                                                                            value = shipment.HazardousMaterialContactMobile
                                                                                        },
                                                                                    new ShipmentCommodityFreightCodeDetail
                                                                                        {
                                                                                            name = "Email",
                                                                                            value = shipment.HazardousMaterialContactEmail
                                                                                        }
                                                                                }
                                                                    }
                                                            }
                                                        : new List<ShipmentCommodityFreightCode>();

            var isLiftgate = shipment.Services.Any(s => s.Service.ApplicableAtPickup && 
                                                        s.Service.Category == ServiceCategory.Accessorial &&
                                                        s.Service.DispatchFlag == ServiceDispatchFlag.LiftGate);

            if (isLiftgate)
                dispatchRequest.shipmentCommodityFreightCodes.Add(new ShipmentCommodityFreightCode
                    {
                        code = "LFTP"
                    });

            var json = JsonConvert.SerializeObject(dispatchRequest);
            return InitiateRequest(new Uri(_settings.StatusUri), json);

		}

        private static EvaSynchronousResponse InitiateRequest(Uri uri, string json)
        {
            var request = WebRequest.Create(uri);
            request.Method = SmcConstants.PostMethod;
            request.ContentType = SmcConstants.JsonContentType;

            using (var writer = new StreamWriter(request.GetRequestStream()))
                writer.Write(json);

            var response = request.GetResponse();
            string responseData;
            var responseStream = response.GetResponseStream();
            if (responseStream == null)
            {
                return null;
            }
            using (var reader = new StreamReader(responseStream))
                responseData = reader.ReadToEnd();
            var result = JsonConvert.DeserializeObject<EvaSynchronousResponse>(responseData);
            return result;
        }

        private string GetYrcCountryCode(string code)
        {
            switch (code)
            {
                case "CA":
                    return "CAN";
                case "MX":
                    return "MEX";
                default:
                    return "USA";
            }
        }
    }
}
