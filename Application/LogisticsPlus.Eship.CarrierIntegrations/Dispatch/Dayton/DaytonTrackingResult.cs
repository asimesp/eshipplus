﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch.Dayton
{
    [Serializable]
    [XmlRoot("result")]
    public class DaytonTrackingResult
    {
        [XmlElement("appointmentFrom")]
        public object AppointmentFrom { get; set; }

        [XmlElement("appointmentTo")]
        public object AppointmentTo { get; set; }

        [XmlArray("billsOfLading"), XmlArrayItem("billOfLading")]
        public string[] BillsOfLading { get; set; }

        [XmlElement("charges")]
        public decimal Charges { get; set; }

        [XmlElement("consignee")]
        public DaytonAddress Consignee { get; set; }

        [XmlElement("deliveryDate")]
        public string DeliveryDate { get; set; }

        [XmlElement("destinationPartner")]
        public object DestinationPartner { get; set; }

        [XmlElement("destinationServiceCenter")]
        public DaytonServiceCenter DestinationServiceCenter { get; set; }

        [XmlArray("details"), XmlArrayItem("shipmentDetails")]
        public DaytonShipmentItem[] ShipmentItems { get; set; }

        [XmlElement("discount")]
        public decimal Discount { get; set; }

        [XmlElement("entryDate")]
        public string EntryDate { get; set; }

        [XmlElement("estimatedDeliveryDate")]
        public string EstimatedDeliveryDate { get; set; }

        [XmlElement("handlingUnits")]
        public decimal Quantity { get; set; }

        [XmlElement("isAppointment")]
        public bool IsAppointment { get; set; }

        [XmlElement("originPartner")]
        public object OriginPartner { get; set; }

        [XmlElement("originServiceCenter")]
        public DaytonServiceCenter OriginServiceCenter { get; set; }

        [XmlElement("pickupDate")]
        public string PickupDate { get; set; }

        [XmlElement("pro")]
        public string ProNumber { get; set; }

        [XmlArray("purchaseOrders"), XmlArrayItem("purchaseOrder")]
        public string[] PurchaseOrders { get; set; }

        [XmlElement("relatedShipments")]
        public object[] RelatedShipments { get; set; }

        [XmlElement("shipper")]
        public DaytonAddress Shipper { get; set; }

        [XmlArray("shipperNumbers"), XmlArrayItem("shipperNumber")]
        public string[] ShipperNumbers { get; set; }

        [XmlElement("status")]
        public DaytonTrackingStatus Status { get; set; }

        [XmlElement("terms")]
        public string Terms { get; set; }

        [XmlElement("weight")]
        public decimal Weight { get; set; }
    }
}
