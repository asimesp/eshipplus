﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch.Dayton
{
    [Serializable]
    [XmlRoot("serviceCenter")]
    public class DaytonServiceCenter
    {
        [XmlElement("address1")]
        public string Street1 { get; set; }

        [XmlElement("address2")]
        public string Street2 { get; set; }

        [XmlElement("city")]
        public string City { get; set; }

        [XmlElement("state")]
        public string State { get; set; }

        [XmlElement("zip")]
        public string PostalCode { get; set; }

        [XmlElement("name")]
        public string Name { get; set; }

        [XmlElement("fax")]
        public string Fax { get; set; }

        [XmlElement("id")]
        public string Id { get; set; }

        [XmlElement("phone")]
        public string Phone { get; set; }
    }
}
