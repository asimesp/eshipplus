﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch.Dayton
{
    [Serializable]
    [XmlRoot("trackingResponse", Namespace = "http://api.daytonfreight.com")]
    public class DaytonTrackingResponse
    {
        [XmlArray("results"), XmlArrayItem("result")]
        public DaytonTrackingResult[] Results { get; set; }
    }
}
