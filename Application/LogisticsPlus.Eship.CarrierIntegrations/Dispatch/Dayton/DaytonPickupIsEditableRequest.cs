﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch.Dayton
{
    [Serializable]
    [XmlRoot("getPickupResponse", Namespace = "http://api.daytonfreight.com")]
    public class DaytonPickupIsEditableRequest
    {
        [XmlElement("isEditable")]
        public bool IsEditable { get; set; }
    }
}
