﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch.Dayton
{
    [Serializable]
    [XmlRoot("shipmentDetails")]
    public class DaytonShipmentItem
    {
        [XmlElement("charges")]
        public decimal Charges { get; set; }

        [XmlElement("class")]
        public string FreightClass { get; set; }

        [XmlElement("description")]
        public string Description { get; set; }

        [XmlElement("handlingUnits")]
        public int Quantity { get; set; }

        [XmlElement("isHazardous")]
        public bool IsHazardous { get; set; }

        [XmlElement("packaging")]
        public string Packaging { get; set; }

        [XmlElement("rate")]
        public decimal Rate { get; set; }

        [XmlElement("weight")]
        public decimal Weight { get; set; }
    }
}
