﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch.Dayton
{
    [Serializable]
    [XmlRoot("shipper")]
    public class DaytonPickupShipper
    {
        [XmlElement("address")]
        public DaytonAddress Address { get; set; }

        [XmlElement("name")]
        public string Name { get; set; }
    }
}
