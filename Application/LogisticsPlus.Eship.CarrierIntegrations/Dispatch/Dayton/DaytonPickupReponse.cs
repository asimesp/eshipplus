﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch.Dayton
{
    [Serializable]
    [XmlRoot("createPickupResponse", Namespace = "http://api.daytonfreight.com")]
    public class DaytonPickupReponse
    {
        [XmlElement("pickupNumber")] 
        public string PickupNumber;
    }
}
