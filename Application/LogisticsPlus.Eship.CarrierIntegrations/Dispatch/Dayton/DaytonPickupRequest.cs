﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch.Dayton
{
    [Serializable]
    [XmlRoot("request", Namespace = "http://api.daytonfreight.com")]
    public class DaytonPickupRequest
    {
        [XmlElement("billingInstructions")]
        public string BillingInstructions { get; set; }

        [XmlElement("close")]
        public string LatePickupDateTime { get; set; }

        [XmlElement("contact")]
        public DaytonPickupContact Contact { get; set; }

        [XmlElement("customerReferenceNumber")]
        public string CustomerReferenceNumber { get; set; }

        [XmlArray("details"), XmlArrayItem("detail")]
        public DaytonPickupItem[] Details { get; set; }

        [XmlElement("pickupInstructions")]
        public string PickupInstructions { get; set; }

        [XmlElement("ready")]
        public string EarlyPickupDateTime { get; set; }

        [XmlElement("requester")]
        public DaytonPickupContact Requester { get; set; }

        [XmlArray("sendConfirmationTo"),XmlArrayItem("email") ]
        public string[] EmailAddressesToSendConfirmation { get; set; }

        [XmlArray("sendReceiptTo"), XmlArrayItem("email")]
        public string[] EmailAddressesToSendReceipt { get; set; }

        [XmlElement("shipper")]
        public DaytonPickupShipper Shipper { get; set; }
    }
}
