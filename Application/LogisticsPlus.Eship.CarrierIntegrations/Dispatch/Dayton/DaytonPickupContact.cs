﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch.Dayton
{
    [Serializable]
    [XmlRoot("contact")]
    public class DaytonPickupContact
    {
        [XmlElement("extension")]
        public string Extension { get; set; }

        [XmlElement("name")]
        public string Name { get; set; }

        [XmlElement("phone")]
        public string Phone { get; set; }
    }
}
