﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch.Dayton
{
    [Serializable]
    [XmlRoot("address")]
    public class DaytonAddress
    {
        [XmlElement("address1")]
        public string Street1 { get; set; }

        [XmlElement("address2")]
        public string Street2 { get; set; }

        [XmlElement("city")]
        public string City { get; set; }

        [XmlElement("state")]
        public string State { get; set; }

        [XmlElement("zip")]
        public string PostalCode { get; set; }

        [XmlElement("name")]
        public string Name { get; set; }
    }
}
