﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch.Dayton
{
    [Serializable]
    [XmlRoot("status")]
    public class DaytonTrackingStatus
    {
        [XmlElement("activity")]
        public string Activity { get; set; }

        [XmlElement("activityCode")]
        public string ActivityCode { get; set; }

        [XmlElement("serviceCenter")]
        public DaytonServiceCenter ServiceCenter { get; set; }

        [XmlElement("signedBy")]
        public string SignedBy { get; set; }

        [XmlElement("time")]
        public string StatusDate { get; set; }

        [XmlElement("trailer")]
        public string Trailer { get; set; }
    }
}
