﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch.Dayton
{
    [Serializable]
    [XmlRoot("detail")]
    public class DaytonPickupItem
    {
        [XmlElement("comment")]
        public string Comment { get; set; }

        [XmlElement("destinationZip")]
        public string DestinationPostalCode { get; set; }

        [XmlElement("handlingUnits")]
        public int Quantity { get; set; }

        [XmlElement("isFood")]
        public bool IsFood { get; set; }

        [XmlElement("isFreezable")]
        public bool IsFreezable { get; set; }

        [XmlElement("isHazardous")]
        public bool IsHazardous { get; set; }

        [XmlElement("isPoisonous")]
        public bool IsPoisonous { get; set; }

        [XmlElement("serviceOption")]
        public string GuaranteedServiceOption { get; set; }

        [XmlElement("weight")]
        public decimal Weight { get; set; }
    }
}
