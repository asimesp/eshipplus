﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using LogisticsPlus.Eship.CarrierIntegrations.Dispatch.SouthwesternMotorTransport;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch
{
    public class SouthwesternMotorTransportDispatch
    {
        private const string PickupCreatedSuccessfullyErrorCode = "SMT0000";

        private readonly CarrierAuth _credentials;

        public SouthwesternMotorTransportDispatch(CarrierAuth auth)
        {
            _credentials = auth;
        }

        public DispatchResponse DispatchShipment(Shipment shipment, User activeUser)
        {
            var sessionId = DateTime.Now.ToString("yyyyMMddhhmmffff");

            var pickupNotes = shipment.Services.FormattedShipmentServices();
            pickupNotes += shipment.FormattedGuaranteedDeliveryNote();

            var tierPhoneNumber = activeUser.DefaultCustomer.Tier.TollFreeContactNumber.GetFormattedTierPhoneNumber();
            var contactPhone = string.IsNullOrEmpty(tierPhoneNumber) ? activeUser.GetPhoneNumber() : tierPhoneNumber;

            var pickupRequest = new SmtlPickupRequest
                {
                    CloseTime = shipment.DesiredPickupDate.SetTime(shipment.LatePickup).FormattedTimeHoursMinutesNoColon(),
                    ReadyTime = shipment.DesiredPickupDate.SetTime(shipment.EarlyPickup).FormattedTimeHoursMinutesNoColon(),
                    Contact = activeUser.FullName,
                    DestinationCity = shipment.Destination.City,
                    DestinationState = shipment.Destination.State,
                    DestinationZip = shipment.Destination.PostalCode,
                    FreightIdNumber = shipment.ShipmentNumber,
                    FromCity = shipment.Origin.City,
                    FromState = shipment.Origin.State,
                    FromZip = shipment.Origin.PostalCode,
                    HazMat = shipment.HazardousMaterial ? "Y" : "N",
                    PackageCode = "PT", // Pallets
                    Phone = contactPhone,
                    PickupCommentOne = shipment.Origin.SpecialInstructions.Truncate(60),
                    PickupCommentTwo = pickupNotes.Truncate(60),
                    Pieces = shipment.Items.Sum(i => i.Quantity).ToString(),
                    SessionId = sessionId, 
                    ShipDate = shipment.DesiredPickupDate.FormattedShortDate(),
                    ShipperAddress = shipment.Origin.CombinedStreet,
                    ShipperName = shipment.Origin.Description,
                    SmtlIam = _credentials.Username,
                    Token = _credentials.Password,
                    Weight = ((int)Math.Ceiling(shipment.Items.Sum(i => i.ActualWeight))).ToString(),
                };

            var content = pickupRequest.ToXml(true, true);
            
            var request = (HttpWebRequest)WebRequest.Create(string.Format("http://www2.smtl.com/rpgsp/SMTLPCKUP.pgm?Request={0}&SessionId={1}", HttpUtility.UrlEncode(content), sessionId));
            request.Method = "GET";
            request.ContentType = "application/xml";

            string xml;
            var resopnse = (HttpWebResponse)request.GetResponse();
            using (var reader = new StreamReader(resopnse.GetResponseStream())) xml = reader.ReadToEnd();

            var response = xml.FromXml<SmtlPickUpResponse>();
            return new DispatchResponse
                {
                    Errors = response.ErrorCode == PickupCreatedSuccessfullyErrorCode ? new List<string>() : new List<string> { string.Format("{0}{1}", response.ErrorCode, response.ErrorMessage) },
                    PickupNumber = response.OnePickUp,
                    ProNumber = string.Empty
                };
        }

        public DispatchResponse UpdateDispatchedShipment(Shipment shipment, User activeUser, string requestNumber)
        {
            throw new Exception(Utilities.FunctionNotAvailableMsg);
        }

        public bool CancelDispatchedShipment(string requestNumber)
        {
            throw new Exception(Utilities.FunctionNotAvailableMsg);
        }

        public string CheckDispatchedShimpent(Shipment shipment)
        {
            throw new Exception(Utilities.FunctionNotAvailableMsg);
        }
    }
}
