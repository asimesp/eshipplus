﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using LogisticsPlus.Eship.CarrierIntegrations.upsDispatchWs;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch
{
    public class UpsDispatch
    {
        private const int UpsOverlengthInches = 180;

        private readonly CarrierAuth _credentials;

        public UpsDispatch(CarrierAuth auth)
        {
            _credentials = auth;
        }

        public DispatchResponse DispatchShipment(Shipment shipment, User activeUser)
        {
            var passwordPipeIndex = _credentials.Password.IndexOf(Utilities.PipeChar);

            var pickupNotes = shipment.Origin.SpecialInstructions;
            pickupNotes += shipment.Services.FormattedShipmentServices();
            pickupNotes += shipment.FormattedGuaranteedDeliveryNote();

            var earlyReadyTime = shipment.DesiredPickupDate.SetTime(shipment.EarlyPickup);
            if (earlyReadyTime < DateTime.Now && DateTime.Now.TimeToMinimum() == earlyReadyTime.TimeToMinimum()) earlyReadyTime = DateTime.Now.AddMinutes(3);

            var tierPhoneNumber = activeUser.DefaultCustomer.Tier.TollFreeContactNumber.GetFormattedTierPhoneNumber();
            var contactPhone = string.IsNullOrEmpty(tierPhoneNumber) ? activeUser.GetPhoneNumber() : tierPhoneNumber;

            var freightPickupRequest = new FreightPickupRequest
                {
                    Requester = new RequesterType
                        {
                            Name = activeUser.Tenant.Name.Truncate(35),
                            AttentionName = activeUser.FullName.Truncate(35),
                            Phone = new PhoneType {Number = contactPhone, Extension = string.Empty},
                            ThirdPartyIndicator = "1",
                            EMailAddress = activeUser.Email.Truncate(50),
                        },
                    ShipFrom = new ShipFromType
                        {
                            Address = new AddressType
                                {
                                    AddressLine = new[]{shipment.Origin.Street1.Truncate(35), shipment.Origin.Street2.Truncate(35)},
                                    City = shipment.Origin.City.Truncate(30),
                                    StateProvinceCode = shipment.Origin.State.Truncate(5),
                                    PostalCode = shipment.Origin.PostalCode.Truncate(10),
                                    CountryCode = shipment.Origin.Country.Code.Truncate(2),
                                },
                            AttentionName = activeUser.FullName.Truncate(25),
                            Name = shipment.Origin.Description.Truncate(35),
                            Phone = new PhoneType {Number = contactPhone, Extension = string.Empty},
                            EMailAddress = activeUser.Email.Truncate(50),
                        },
                    ShipTo = new ShipToType
                        {
                            Address = new AddressType
                                {
                                    AddressLine = new []{ shipment.Destination.Street1.Truncate(35), shipment.Destination.Street2.Truncate(35)},
                                    City = shipment.Destination.City.Truncate(30),
                                    CountryCode = shipment.Destination.Country.Code.Truncate(2),
                                    PostalCode = shipment.Destination.PostalCode.Truncate(10),
                                    StateProvinceCode = shipment.Destination.State.Truncate(2)
                                },
                            AttentionName = activeUser.FullName,
                            EMailAddress = activeUser.Email,
                            Phone = new PhoneType { Number = contactPhone, Extension = string.Empty},
                        },
                    PickupDate = shipment.DesiredPickupDate.FormattedShortDateWithoutDelimiter(),
                    EarliestTimeReady = earlyReadyTime.FormattedTimeHoursMinutesNoColon(),
                    LatestTimeReady = shipment.DesiredPickupDate.SetTime(shipment.LatePickup).FormattedTimeHoursMinutesNoColon(),
                    DeliveryInstructions = shipment.Destination.SpecialInstructions.Truncate(500),
                    PickupInstructions = pickupNotes.Truncate(500),
                    DestinationCountryCode = shipment.Destination.Country.Code.Truncate(2),
                    DestinationPostalCode = shipment.Destination.PostalCode.Truncate(10),
                    ShipmentDetail = shipment
                        .Items
                        .Select(i => new ShipmentDetailType
                            {
                                DescriptionOfCommodity = i.Description,
                                NumberOfPieces = i.Quantity.ToString(),
                                Weight = new WeightType
                                    {
                                        UnitOfMeasurement = new UnitOfMeasurementType {Code = "LBS"},
                                        Value = Math.Ceiling(i.ActualWeight).ToInt().ToString()
                                    },
                                HazmatIndicator = i.HazardousMaterial ? string.Empty : null,
                                PackagingType = new PickupCodeDescriptionType { Code = "PLT", Description = "PALLET" },
                            })
                        .ToArray(),
                    Request = new RequestType(),
                    
                };

            if (shipment.Items.Any(i => i.ActualLength > UpsOverlengthInches || i.ActualWidth > UpsOverlengthInches))
            {
                freightPickupRequest.ShipmentServiceOptions = new ShipmentServiceOptionsType
                    {
                        ExtremeLengthIndicator = "1"
                    };
            }

            var freightPickupService = new FreightPickupService
                {
                    UPSSecurityValue = new UPSSecurity
                        {
                            ServiceAccessToken = new UPSSecurityServiceAccessToken
                                {
                                    AccessLicenseNumber = _credentials.Password.Substring(passwordPipeIndex + 1, _credentials.Password.Length - passwordPipeIndex - 1)
                                },
                            UsernameToken = new UPSSecurityUsernameToken
                                {
                                    Username = _credentials.Username,
                                    Password = _credentials.Password.Substring(0, passwordPipeIndex)
                                }
                        }
                };
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            try
            {
                var freightPickupResponse = freightPickupService.ProcessFreightPickup(freightPickupRequest);

                return new DispatchResponse
                {
                    Errors = new List<string>(),
                    PickupNumber = freightPickupResponse.PickupRequestConfirmationNumber,
                    ProNumber = string.Empty
                };
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                return new DispatchResponse
                {
                    Errors = new[] { string.Format("Error: {0}{3}Inner Text: {1}{3}Xml Error: {2}", ex.Message, ex.Detail.LastChild.InnerText, ex.Detail.LastChild.OuterXml, Environment.NewLine) }.ToList(),
                    PickupNumber = string.Empty,
                    ProNumber = string.Empty
                };
            }
        }

        public DispatchResponse UpdateDispatchedShipment(Shipment shipment, User activeUser, string requestNumber)
        {
            throw new Exception(Utilities.FunctionNotAvailableMsg);
        }

        public bool CancelDispatchedShipment(string requestNumber)
        {
            throw new Exception(Utilities.FunctionNotAvailableMsg);
        }

        public string CheckDispatchedShimpent(Shipment shipment)
        {
            throw new Exception(Utilities.FunctionNotAvailableMsg);
        }
    }
}
