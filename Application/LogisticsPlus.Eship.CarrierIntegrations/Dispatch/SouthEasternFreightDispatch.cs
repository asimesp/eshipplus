﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.CarrierIntegrations.southEasternFreightDispatchWs;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core;
using Contact = LogisticsPlus.Eship.CarrierIntegrations.southEasternFreightDispatchWs.Contact;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch
{
    public class SouthEasternFreightDispatch
    {
        private readonly CarrierAuth _credentials;

        private readonly SeflPickupService _southEasternFreightWs;

        public SouthEasternFreightDispatch(CarrierAuth auth)
        {
            _credentials = auth;
            _southEasternFreightWs = new SeflPickupService();
        }


        public DispatchResponse DispatchShipment(Shipment shipment, User activeUser, bool isTest = false)
        {
            var passwordPipeIndex = _credentials.Password.IndexOf(Utilities.PipeChar);

            var pickupTime = shipment.DesiredPickupDate.SetTime(shipment.EarlyPickup).FormattedTimeHoursMinutesNoColon();
            var closeTime = shipment.DesiredPickupDate.SetTime(shipment.LatePickup).FormattedTimeHoursMinutesNoColon();

            var pickupNotes = shipment.Services.FormattedShipmentServices();
            pickupNotes += shipment.FormattedGuaranteedDeliveryNote();

            var tierPhoneNumber = activeUser.DefaultCustomer.Tier.TollFreeContactNumber.GetFormattedTierPhoneNumber();
            var contactPhone = string.IsNullOrEmpty(tierPhoneNumber) ? activeUser.GetPhoneNumber() : tierPhoneNumber;

            var pickup = new Pickup
                {
                    contact = new Contact
                        {
                            company = activeUser.Tenant.Name,
                            email = activeUser.Email,
                            phone = contactPhone,
                            name = activeUser.FullName,
                        },
                    shipper = new Shipper
                        {
                            username = _credentials.Username,
                            password = _credentials.Password.Substring(0, passwordPipeIndex),
                            acctNum = _credentials.Password.Substring(passwordPipeIndex + 1, _credentials.Password.Length - passwordPipeIndex - 1),
                            shipperName = shipment.Origin.Description,
                            address = shipment.Origin.CombinedStreet,
                            city = shipment.Origin.City,
                            state = shipment.Origin.State,
                            zip = shipment.Origin.PostalCode,
                            pickupContactName = activeUser.FullName,
                            phone = contactPhone,
                            email = activeUser.Email,
                            fax = !string.IsNullOrEmpty(activeUser.Fax) ? "0000000000" : activeUser.Fax,
                            requestPickupDate = shipment.DesiredPickupDate.FormattedShortDateWithoutDelimiter(),
                            requestPickupTime = pickupTime,
                            closeTime = closeTime,
                            developer = isTest.ToString(),
                            driverInstructions = shipment.Origin.SpecialInstructions,
                            pickupNotes = pickupNotes,
                            pickupTimeStart = pickupTime,
                            pickupTimeEnd = closeTime,
                            shipAs = "T", // Third-Party
                        },
                    consignees = shipment.Items.Select(item => new Consignee
                        {
                            destZip = shipment.Destination.PostalCode,
                            name = shipment.Destination.Description,
                            pieces = item.PieceCount < 1 ? 1.ToString() : item.PieceCount.ToString(),
                            pkgType = "PT", // Pallet
                            weight = ((int)Math.Ceiling(item.ActualWeight)).ToString(),
                        }).ToArray()
                };

            var result = _southEasternFreightWs.pickup(pickup);

            return new DispatchResponse
                {
                   Errors = result.errMessages != null ? result.errMessages.Select(e => e.ToString()).ToList() : new List<string>(),
                   PickupNumber = result.confirmation != null ? result.confirmation.confirmationNumber : string.Empty,
                   ProNumber = string.Empty,
                };
        }

        public bool CancelDispatchedShipment(string requestNumber)
        {
            throw new Exception(Utilities.FunctionNotAvailableMsg);
        }

        public DispatchResponse UpdateDispatchedShipment(Shipment shipment, User activeUser, string requestNumber)
        {
            throw new Exception(Utilities.FunctionNotAvailableMsg);
        }

        public string CheckDispatchedShimpent(Shipment shipment)
        {
            throw new Exception(Utilities.FunctionNotAvailableMsg);
            
        }
    }
}
