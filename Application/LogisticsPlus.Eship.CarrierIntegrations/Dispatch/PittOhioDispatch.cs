﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.CarrierIntegrations.pittOhioDispatchWs;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch
{
    public class PittOhioDispatch
    {
        private readonly CarrierAuth _credentials;
        private readonly B2BServices _pittOhioWs;

        private const int PittOhioOverLengthInches = 252;

        public PittOhioDispatch(CarrierAuth auth)
        {
            _credentials = auth;

            _pittOhioWs = new B2BServices();
        }


        public DispatchResponse DispatchShipment(Shipment shipment, User activeUser)
        {
            var tierPhoneNumber = activeUser.DefaultCustomer.Tier.TollFreeContactNumber.GetFormattedTierPhoneNumber();
            var contactPhone = string.IsNullOrEmpty(tierPhoneNumber) ? activeUser.GetPhoneNumber() : tierPhoneNumber;
           
            var getSessionResult = _pittOhioWs.GetSessionID(_credentials.Username, _credentials.Password);
            if (getSessionResult.Error)
            {
                return new DispatchResponse
                    {
                        Errors = new List<string>{getSessionResult.Result},
                        PickupNumber = string.Empty,
                        ProNumber = string.Empty
                    };
            }

            var sessionId = getSessionResult.Result;

            var pickupNotes = shipment.Services.FormattedShipmentServices();
            pickupNotes += shipment.Origin.SpecialInstructions.FormattedPickupInstructions();
            pickupNotes += shipment.FormattedGuaranteedDeliveryNote();

            var startPickupResult = _pittOhioWs
                .B2BStartThirdPartyPickupRequest(sessionId,
                                                 activeUser.Tenant.Name.Truncate(100),
                                                 activeUser.Street1.Truncate(100),
                                                 activeUser.Street2.Truncate(100),
                                                 activeUser.PostalCode.Truncate(10),
                                                 activeUser.FullName.Truncate(100),
                                                 contactPhone.Truncate(20),
                                                 activeUser.Email.Truncate(100),
                                                 shipment.Origin.Description.Truncate(100),
                                                 shipment.Origin.Street1.Truncate(100),
                                                 shipment.Origin.Street2.Truncate(100),
                                                 shipment.Origin.PostalCode.Truncate(10),
                                                 activeUser.FullName.Truncate(100),
                                                 contactPhone.Truncate(20),
                                                 activeUser.Email.Truncate(100),
                                                 shipment.DesiredPickupDate.SetTime(shipment.EarlyPickup),
                                                 shipment.DesiredPickupDate.SetTime(shipment.LatePickup), 
                                                 pickupNotes.Truncate(255));

            if (startPickupResult.Errors.Any())
            {
                return new DispatchResponse
                {
                    Errors = startPickupResult.Errors.ToList(),
                    PickupNumber = string.Empty,
                    ProNumber = string.Empty
                };
            }

            var addPickupShipmentResult = _pittOhioWs.B2BAddPickupRequestShipment(sessionId, startPickupResult.StagingID,
                                                                                  shipment.Destination.Description.Truncate(100),
                                                                                  shipment.Destination.CombinedStreet.Truncate(100),
                                                                                  shipment.Destination.PostalCode.Truncate(10),
                                                                                  activeUser.FullName.Truncate(100),
                                                                                  contactPhone.Truncate(20),
                                                                                  (int)Math.Ceiling(shipment.Items.Sum(i => i.ActualWeight)), 
                                                                                  shipment.Items.Sum(i => i.PieceCount),
                                                                                  shipment.Items.Sum(i => i.Quantity), 
                                                                                  shipment.Items.Any(i => i.ActualLength > PittOhioOverLengthInches || i.ActualWidth > PittOhioOverLengthInches),
                                                                                  shipment.HazardousMaterial,
                                                                                  shipment.Destination.SpecialInstructions);

            if (addPickupShipmentResult.Any())
            {
                return new DispatchResponse
                {
                    Errors = addPickupShipmentResult.ToList(),
                    PickupNumber = string.Empty,
                    ProNumber = string.Empty
                };
            }

            var submissionResult = _pittOhioWs.B2BSubmitPickupRequest(sessionId, startPickupResult.StagingID, true);

            return new DispatchResponse
                {
                    PickupNumber = submissionResult.PickupNumber.ToString(), 
                    Errors = submissionResult.Errors.ToList(),
                    ProNumber = string.Empty
                };
        }

        public bool CancelDispatchedShipment(string requestNumber)
        {
            throw new Exception(Utilities.FunctionNotAvailableMsg);
        }

        public DispatchResponse UpdateDispatchedShipment(Shipment shipment, User activeUser, string requestNumber)
        {
            throw new Exception(Utilities.FunctionNotAvailableMsg);
        }

        public string CheckDispatchedShimpent(Shipment shipment)
        {
            throw new Exception(Utilities.FunctionNotAvailableMsg);
        }
    }
}
