﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch.Ward
{
    [Serializable]
    [XmlRoot("request", Namespace = "")]
    public class WardPickupRequestBody
    {
        [XmlElement("ShipperInformation")]
        public WardShipperInformation ShipperInformation { get; set; }

        [XmlElement("Shipment")]
        public WardShipment Shipment { get; set; }
    }
}
