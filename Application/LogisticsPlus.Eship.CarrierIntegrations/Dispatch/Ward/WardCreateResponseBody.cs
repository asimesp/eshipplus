﻿using System.Xml.Serialization;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch.Ward
{
    [XmlRoot(ElementName = "Body", Namespace = "http://www.wardtrucking.com/WebService/pickup/")]
    public class WardCreateResponseBody
    {
        [XmlElement("CreateResponse")]
        public WardCreateResultResponse CreateResponse { get; set; }
    }
}