﻿using System;
using System.Web.Services.Description;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch.Ward
{
    [Serializable]
    [XmlRoot("Shipment")]
    public class WardShipment
    {
        [XmlElement("Pieces")]        
        public string Pieces { get; set; }

        [XmlElement("PackageCode")]
        public string PackageCode { get; set; }

        [XmlElement("Weight")]
        public string Weight { get; set; }

        [XmlElement("ConsigneeCode")]
        public string ConsigneeCode { get; set; }

        [XmlElement("ConsigneeName")]
        public string ConsigneeName { get; set; }

        [XmlElement("ConsigneeAddress1")]
        public string ConsigneeAddress1 { get; set; }

        [XmlElement("ConsigneeAddress2")]
        public string ConsigneeAddress2 { get; set; }

        [XmlElement("ConsigneeCity")]
        public string ConsigneeCity { get; set; }

        [XmlElement("ConsigneeState")]
        public string ConsigneeState { get; set; }

        [XmlElement("ConsigneeZipcode")]
        public string ConsigneeZipcode { get; set; }

        [XmlElement("ShipperRoutingSCAC")]
        public string ShipperRoutingSCAC { get; set; }

        [XmlElement("Hazardous")]
        public string Hazardous { get; set; }

        [XmlElement("Freezable")]
        public string Freezable { get; set; }

        [XmlElement("DeliveryAppntFlag")]
        public string DeliveryAppntFlag { get; set; }

        [XmlElement("DeliveryAppntDate")]
        public string DeliveryAppntDate { get; set; }

        [XmlElement("WardAssured12PM")]
        public string WardAssured12PM { get; set; }

        [XmlElement("WardAssured03PM")]
        public string WardAssured03PM { get; set; }

        [XmlElement("WardAssuredTimeDefinite")]
        public string WardAssuredTimeDefinite { get; set; }

        [XmlElement("WardAssuredTimeDefiniteStart")]
        public string WardAssuredTimeDefiniteStart { get; set; }

        [XmlElement("WardAssuredTimeDefiniteEnd")]
        public string WardAssuredTimeDefiniteEnd { get; set; }

        [XmlElement("FullValue")]
        public string FullValue { get; set; }

        [XmlElement("FullValueInsuredAmount")]
        public string FullValueInsuredAmount { get; set; }

        [XmlElement("NonStandardSize")]
        public string NonStandardSize { get; set; }

        [XmlElement("NonStandardSizeDescription")]
        public string NonStandardSizeDescription { get; set; }

        [XmlElement("RequestorReference")]
        public string RequestorReference { get; set; }

        [XmlElement("PickupShipmentInstruction1")]
        public string PickupShipmentInstruction1 { get; set; }

        [XmlElement("PickupShipmentInstruction2")]
        public string PickupShipmentInstruction2 { get; set; }

        [XmlElement("PickupShipmentInstruction3")]
        public string PickupShipmentInstruction3 { get; set; }

        [XmlElement("PickupShipmentInstruction4")]
        public string PickupShipmentInstruction4 { get; set; }

        [XmlElement("RequestOrigin")]
        public string RequestOrigin { get; set; }
    }
}