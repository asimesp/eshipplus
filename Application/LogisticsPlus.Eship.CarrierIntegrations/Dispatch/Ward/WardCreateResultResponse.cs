﻿using System.Xml.Serialization;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch.Ward
{
    [XmlRoot(ElementName = "CreateResponse")]
    public class WardCreateResultResponse
    {
        [XmlElement("CreateResult")]
        public WardPickupRequestResponseBody CreateResult { get; set; }
    }
}