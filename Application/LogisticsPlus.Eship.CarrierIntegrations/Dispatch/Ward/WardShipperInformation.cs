﻿using System;
using System.Text.RegularExpressions;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch.Ward
{
    [Serializable]
    [XmlRoot("ShipperInformation")]
    public class WardShipperInformation
    {
        private string _shipperContactTelephone;
        private string _requestorContactTelephone;
        private string _wardAssuredContactTelephone;

        [XmlElement("ShipperCode")]
        public string ShipperCode { get; set; }

        [XmlElement("ShipperName")]
        public string ShipperName { get; set; }

        [XmlElement("ShipperAddress1")]
        public string ShipperAddress1 { get; set; }

        [XmlElement("ShipperAddress2")]
        public string ShipperAddress2 { get; set; }

        [XmlElement("ShipperCity")]
        public string ShipperCity { get; set; }

        [XmlElement("ShipperState")]
        public string ShipperState { get; set; }

        [XmlElement("ShipperZipcode")]
        public string ShipperZipcode { get; set; }

        [XmlElement("ShipperContactName")]
        public string ShipperContactName { get; set; }

        [XmlElement("ShipperContactTelephone")]
        public string ShipperContactTelephone
        {
            get { return _shipperContactTelephone; }
            set { _shipperContactTelephone = EncodePhoneNumber(value); }
        }

        [XmlElement("ShipperContactEmail")]
        public string ShipperContactEmail { get; set; }

        [XmlElement("ShipperReadyTime")]
        public string ShipperReadyTime { get; set; }

        [XmlElement("ShipperCloseTime")]
        public string ShipperCloseTime { get; set; }

        [XmlElement("PickupDate")]
        public string PickupDate { get; set; }

        [XmlElement("ThirdParty")]
        public string ThirdParty { get; set; }

        [XmlElement("ThirdPartyName")]
        public string ThirdPartyName { get; set; }

        [XmlElement("ThirdPartyContactName")]
        public string ThirdPartyContactName { get; set; }

        [XmlElement("ThirdPartyContactTelephone")]
        public string ThirdPartyContactTelephone { get; set; }

        [XmlElement("ThirdPartyContactEmail")]
        public string ThirdPartyContactEmail { get; set; }

        [XmlElement("WardAssuredContactName")]
        public string WardAssuredContactName { get; set; }

        [XmlElement("WardAssuredContactTelephone")]
        public string WardAssuredContactTelephone
        {
            get { return _wardAssuredContactTelephone; }
            set { _wardAssuredContactTelephone = EncodePhoneNumber(value);  }
        }

        [XmlElement("WardAssuredContactEmail")]
        public string WardAssuredContactEmail { get; set; }

        [XmlElement("ShipperRestriction")]
        public string ShipperRestriction { get; set; }

        [XmlElement("DriverNote1")]
        public string DriverNote1 { get; set; }

        [XmlElement("DriverNote2")]
        public string DriverNote2 { get; set; }

        [XmlElement("DriverNote3")]
        public string DriverNote3 { get; set; }

        [XmlElement("RequestOrigin")]
        public string RequestOrigin { get; set; }

        [XmlElement("RequestorUser")]
        public string RequestorUser { get; set; }

        [XmlElement("RequestorRole")]
        public string RequestorRole { get; set; }

        [XmlElement("RequestorContactName")]
        public string RequestorContactName { get; set; }

        [XmlElement("RequestorContactTelephone")]
        public string RequestorContactTelephone
        {
            get { return _requestorContactTelephone; }
            set { _requestorContactTelephone = EncodePhoneNumber(value); }
        }

        [XmlElement("RequestorContactEmail")]
        public string RequestorContactEmail { get; set; }

        private string EncodePhoneNumber(string phoneNumber)
        {
            string resultString = String.Empty;
            if (string.IsNullOrEmpty(phoneNumber)) return resultString;
            try
            {
                Regex regexObj = new Regex(@"[^\d]");
                resultString = regexObj.Replace(phoneNumber, "");
            }
            catch (ArgumentException ex)
            {
                // Syntax error in the regular expression
            }
            return resultString;
        }

    }
}