﻿using System.Xml.Serialization;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch.Ward
{
    [XmlRoot(ElementName = "Body", Namespace = "")]
    public class WardResponseBody
    {
        //[XmlNamespaceDeclarations]
        //public XmlSerializerNamespaces Xmlns = new XmlSerializerNamespaces();

        [XmlElement("request")]
        public WardPickupRequestBody Request { get; set; }
    }
}
