using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch.Ward
{
    [Serializable]
    [XmlRoot("CreateResult")]
    public class WardPickupRequestResponseBody
    {
        [XmlElement("PickupConfirmation")]
        public string PickupConfirmation { get; set; }

        [XmlElement("Message")]
        public string Message { get; set; }

        [XmlElement("PickupTerminal")]
        public string PickupTerminal { get; set; }

        [XmlElement("WardTelephone")]
        public string WardTelephone { get; set; }

        [XmlElement("WardEmail")]
        public string WardEmail { get; set; }
    }
}