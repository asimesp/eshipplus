﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch.SouthwesternMotorTransport
{
    [Serializable]
    [XmlRoot("SMTLPickUpResponse")]
    public class SmtlPickUpResponse
    {
        [XmlElement("ErrorCode")]
        public string ErrorCode { get; set; }

        [XmlElement("ErrorMessage")]
        public string ErrorMessage { get; set; }

        [XmlElement("OnePickUp")]
        public string OnePickUp { get; set; }

        [XmlElement("TwoPickUp")]
        public string TwoPickUp { get; set; }

        [XmlElement("ThrPickUp")]
        public string ThrPickUp { get; set; }
    }
}
