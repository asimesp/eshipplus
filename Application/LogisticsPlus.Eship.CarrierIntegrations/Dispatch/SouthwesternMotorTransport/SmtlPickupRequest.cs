﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch.SouthwesternMotorTransport
{
    [Serializable]
    [XmlRoot("SMTLPickUpRequest")]
    public class SmtlPickupRequest
    {
        [XmlElement("SMTLIam")]
        public string SmtlIam { get; set; }

        public string Token { get; set; }

        public string SessionId { get; set; }

        public string ShipperName { get; set; }

        public string ShipperAddress { get; set; }

        public string FromCity { get; set; }

        public string FromState { get; set; }

        public string FromZip { get; set; }

        public string ShipDate { get; set; }

        public string ReadyTime { get; set; }

        public string CloseTime { get; set; }

        public string Phone { get; set; }

        public string Contact { get; set; }

        [XmlElement("OneCity")]
        public string DestinationCity { get; set; }

        [XmlElement("OneState")]
        public string DestinationState { get; set; }

        [XmlElement("OneZip")]
        public string DestinationZip { get; set; }

        [XmlElement("OnePUComment1")]
        public string PickupCommentOne { get; set; }

        [XmlElement("OnePUComment2")]
        public string PickupCommentTwo { get; set; }

        [XmlElement("OnePieces")]
        public string Pieces { get; set; }

        [XmlElement("OneWeight")]
        public string Weight { get; set; }

        [XmlElement("OnePckgCode")]
        public string PackageCode { get; set; }

        [XmlElement("OneHazMat")]
        public string HazMat { get; set; }

        [XmlElement("OneFrtIdNbr")]
        public string FreightIdNumber { get; set; }
    }
}
