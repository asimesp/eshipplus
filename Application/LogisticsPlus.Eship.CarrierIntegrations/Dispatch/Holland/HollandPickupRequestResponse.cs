﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch.Holland
{
    [Serializable]
    [XmlRoot("PICKUPREQUESTRESPONSE")]
    public class HollandPickupRequestResponse
    {
        [XmlElement("STATUS")]
        public HollandStatus Status { get; set; }

        [XmlElement("PICKUPREQUESTCONFIRMATIONNUMBER")]
        public string PickupRequestNumber { get; set; }
    }
}
