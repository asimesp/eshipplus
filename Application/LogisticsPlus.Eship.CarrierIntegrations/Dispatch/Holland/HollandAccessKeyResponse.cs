﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch.Holland
{
    [Serializable]
    [XmlRoot("AccessKeyResponse", Namespace = "")]
    public class HollandAccessKeyResponse
    {
        [XmlElement("STATUS")]
        public HollandStatus Status { get; set; }

        [XmlElement("ACCESSKEY")]
        public string AccessKey { get; set; }

        [XmlElement("ACCESSKEY_ENCODED")]
        public string AccessKeyUrlEncoded { get; set; }
    }
}
