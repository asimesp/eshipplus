﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch.Holland
{
    [Serializable]
    [XmlRoot("STATUS")]
    public class HollandStatus
    {
        [XmlElement("CODE")]
        public int Code { get; set; }

        [XmlElement("VIEW")]
        public string View { get; set; }

        [XmlElement("VERSION")]
        public HollandVersion Version { get; set; }

        [XmlElement("ERRORTYPE", IsNullable = true)]
        public string ErrorType { get; set; }

        [XmlElement("MESSAGE", IsNullable = true)]
        public string Message { get; set; }
    }
}
