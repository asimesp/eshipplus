﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch.Holland
{
    [Serializable]
    [XmlRoot("VERSION")]
    public class HollandVersion
    {
        [XmlElement("CURRENT")]
        public string CurrentVersion { get; set; }

        [XmlElement("CURRENT_RELEASE_DATE")]
        public string CurrentVersionReleaseDate { get; set; }

        [XmlElement("LATEST")]
        public string LatestVersion { get; set; }

        [XmlElement("LATEST_RELEASE_DATE")]
        public string LatestVersionReleaseDate { get; set; }
    }
}
