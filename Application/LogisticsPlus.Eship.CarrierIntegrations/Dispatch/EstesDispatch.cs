﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using LogisticsPlus.Eship.CarrierIntegrations.estesDispatchWs;
using LogisticsPlus.Eship.CarrierIntegrations.estesTrackingWs;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;

namespace LogisticsPlus.Eship.CarrierIntegrations.Dispatch
{
	public class EstesDispatch
	{
		private readonly estespickupbasewsprovidersoapwspickupRequestSSL _ews;
		private readonly ShipmentTrackingService _estws;

		public EstesDispatch(CarrierAuth auth)
		{
			_ews = new estespickupbasewsprovidersoapwspickupRequestSSL
				{
					Credentials = new NetworkCredential(auth.Username, auth.Password)
				};

			_estws = new ShipmentTrackingService
				{
					auth = new AuthenticationType
						{
							user = auth.Username,
							password = auth.Password
						}
				};
		}

		public string Echo()
		{		
			return _estws.echo(new EchoRequestType {input = "Echo Test"});
		}

		public DispatchResponse DispatchShipment(Shipment shipment, User activeUser)
		{
			SoapError[] msg;

            var tierPhoneNumber = activeUser.DefaultCustomer.Tier.TollFreeContactNumber.GetFormattedTierPhoneNumber();
            var phone = string.IsNullOrEmpty(tierPhoneNumber) ? activeUser.GetPhoneNumber() : tierPhoneNumber;
            var request = new PickupWebServiceInput
				{
					shipper = new PickupWebServiceShipper
						{
							shipperAddress = new shipperAddress
								{
									addressInfo = new addressInfo
										{
											addressLine1 = shipment.Origin.Street1.Truncate(30),
											addressLine2 = string.IsNullOrEmpty(shipment.Origin.Street2) ? null : shipment.Origin.Street2.Truncate(30),
											city = shipment.Origin.City.Truncate(20),
											postalCode = shipment.Origin.PostalCode.Truncate(6),
											countryAbbrev = (shipment.Origin.Country ?? new Country()).Code.Truncate(2),
											stateProvince = shipment.Origin.State.Truncate(2),
											postalCode4 = null,
										},
								},
							shipperContacts = new[]
								{
									new shipperContact
										{
											contactInfo = new contactInfo
												{
													name = new name
														{
															firstName = activeUser.FirstName.Truncate(30),
															lastName = activeUser.LastName.Truncate(30)
														},
													email = string.IsNullOrEmpty(activeUser.Email) ? null : activeUser.Email,
													phone = new phone
														{
															areaCode = (uint) phone.Substring(0, 3).ToInt(),
															number = (uint) phone.Substring(3).ToInt()
														},
													fax = null,
													notificationMethod = null,
													receiveNotifications = receiveNotifications.N,
												},
											id = 0,
											operation = null,
											idSpecified = false,
										}
								},
							shipperName = shipment.Origin.Description.Truncate(30),
							accountCode = null,
						},
					addresses = new[]
						{
							new PickupWebServiceAddress
								{
									addressInfo = new addressInfo2
										{
											addressType = "C", // consignee
											addressLine1 = shipment.Destination.Street1.Truncate(30),
											addressLine2 =
												string.IsNullOrEmpty(shipment.Destination.Street2) ? null : shipment.Destination.Street2.Truncate(30),
											countryAbbrev = (shipment.Destination.Country ?? new Country()).Code.Truncate(2),
											city = shipment.Destination.City.Truncate(30),
											postalCode = shipment.Destination.PostalCode.Truncate(6),
											stateProvince = shipment.Destination.State.Truncate(2),
											postalCode4 = null,
										},
									id = 0,
									operation = null,
									idSpecified = false,
								}
						},
                    trailer = null,
					requestNumber = null,
					requestAction = null,
					paymentTerms = null,
					pickupDate = shipment.DesiredPickupDate.Date,
					pickupEndTime = (uint) (shipment.LatePickup.Replace(":", string.Empty)).ToInt(),
					pickupEndTimeSpecified = true,
					pickupStartTime = (uint)(shipment.EarlyPickup.Replace(":", string.Empty)).ToInt(),
					pickupStartTimeSpecified = true,
					totalPieces = (uint)shipment.Items.Sum(i => i.Quantity),
					totalPiecesSpecified = true,
					totalWeight = (uint) Math.Ceiling(shipment.Items.Sum(i => i.ActualWeight)),
					totalWeightSpecified = true,
					totalHandlingUnits = (uint) shipment.Items.Sum(i => i.PieceCount),
					totalHandlingUnitsSpecified = shipment.Items.Sum(i=>i.PieceCount) > 0,
					referenceNumbers = null,
					commodities = null,
					comments = null,
					contacts = null,
					notifications = null,
					whoRequested = null,
					expeditedCode = null,
					hazmatFlag = null, // TODO: HAZMAT UPDATE - Review possible implementation of hazmat update. Docs say in addition to setting this you must provide additional details
					                  // TODO:                 and identify the hazmat items in the commodity collection. Refer to Estes doc for specifics.
				};

            var isLiftgate = shipment.Services.Any(s => s.Service.ApplicableAtPickup && s.Service.Category == ServiceCategory.Accessorial && s.Service.DispatchFlag == ServiceDispatchFlag.LiftGate);
		    if (isLiftgate)
		        request.trailer = new PickupWebServiceTrailer
		            {
		                trailerInfo = new trailerInfo
		                    {
		                        type = "LG",
                                length = string.Empty
		                    }
		            };


			var comments = new List<PickupWebServiceComment>();
			if (!string.IsNullOrEmpty(shipment.Origin.SpecialInstructions))
				comments.Add(new PickupWebServiceComment
					{
						commentInfo = new commentInfo {type = type3.SSI, commentText = shipment.Origin.SpecialInstructions.Truncate(250)}
					});

            var services = string.Join(",", shipment.Services.Where(s => s.Service.ApplicableAtPickup && s.Service.Category == ServiceCategory.Accessorial && s.Service.DispatchFlag != ServiceDispatchFlag.LiftGate).Select(s => s.Service.Description).ToArray()).Truncate(250);
			if (!string.IsNullOrEmpty(services))
				comments.Add(new PickupWebServiceComment {commentInfo = new commentInfo {type = type3.SCM, commentText = services}});

            if(shipment.IsGuaranteedDeliveryService)
                comments.Add(new PickupWebServiceComment { commentInfo = new commentInfo { type = type3.SSI, commentText = shipment.FormattedGuaranteedDeliveryNote().Truncate(250) } });

			request.comments = comments.Any() ? comments.ToArray() : null;

			var response = new DispatchResponse
				{
					PickupNumber = _ews.createPickupRequestWS(request, out msg)
				};
			if (msg != null && msg.Any(m=>!string.IsNullOrEmpty(m.code) || !string.IsNullOrEmpty(m.description) || !string.IsNullOrEmpty(m.elementName)))
				response.Errors = msg
					.Where(m => !string.IsNullOrEmpty(m.code) || !string.IsNullOrEmpty(m.description) || !string.IsNullOrEmpty(m.elementName))
					.Select(m => string.Format("{0}: {1} [{2}]", m.code, m.description, m.elementName)).ToList();

			return response;
		}

		public DispatchResponse UpdateDispatchedShipment(Shipment shipment, User activeUser, string requestNumber)
		{
			SoapError[] msg;
			var request = new PickupWebServiceInput
			{
				requestNumber = requestNumber,
				shipper = new PickupWebServiceShipper
				{
					shipperAddress = new shipperAddress
					{
						addressInfo = new addressInfo
						{
							addressLine1 = shipment.Origin.Street1,
							addressLine2 = shipment.Origin.Street2,
							city = shipment.Origin.City,
							postalCode = shipment.Origin.PostalCode,
							countryAbbrev = (shipment.Origin.Country ?? new Country()).Code,
							postalCode4 = string.Empty,
							stateProvince = shipment.Origin.State
						}
					},
					shipperContacts = new[]
								{
									new shipperContact
										{
											contactInfo = new contactInfo
												{
													email = activeUser.Email,
												},
										}
								},
					shipperName = shipment.Origin.Description,
				},
				pickupDate = shipment.DesiredPickupDate,
				pickupStartTime = (uint)(shipment.EarlyPickup.Replace(":", string.Empty)).ToInt(),
				pickupEndTime = (uint)(shipment.LatePickup.Replace(":", string.Empty)).ToInt(),
				totalHandlingUnits = (uint)shipment.Items.Sum(i => i.Quantity),
				totalWeight = (uint)shipment.Items.Sum(i => i.ActualWeight),
				comments = new[]
						{
							new PickupWebServiceComment
								{
									commentInfo = new commentInfo
										{
											type = type3.SSI,
											commentText = shipment.Origin.SpecialInstructions
										}
								},
							new PickupWebServiceComment
								{
									commentInfo = new commentInfo
										{
											type = type3.SCM,
											commentText = string.Join(",", shipment.Services.Select(s => s.Service.Description).ToArray()).Truncate(250)
										}
								}
						}
			};

			var response = new DispatchResponse
			{
				PickupNumber = _ews.updatePickupRequestWS(request, out msg)
			};
			if (msg != null && msg.Any())
				response.Errors = msg.Select(m => string.Format("{0}: {1} [{2}]", m.code, m.description, m.elementName)).ToList();

			return response;
		}

		public string CancelDispatchedShipment(string requestNumber)
		{
			return _ews.cancelPickupRequestWS(requestNumber);
		}

		public string CheckDispatchedShimpent(Shipment shipment)
		{
			var number = string.Format("{0}{1}", shipment.Prefix == null || shipment.HidePrefix ? string.Empty : shipment.Prefix.Code, shipment.ShipmentNumber);
			var search = new search {requestID = number, ItemElementName = ItemChoiceType.bol, Item = number};
			trackingInfo output;

			try
			{
				output = _estws.trackShipments(search); // exception returned in case where no results found!
			}
			catch
			{
				output = null;
			}

			if (output == null || output.shipments.All(s => s.bol != number))
			{
				search = new search { requestID = shipment.ShipmentNumber, ItemElementName = ItemChoiceType.bol, Item = shipment.ShipmentNumber };
				try
				{
					output = _estws.trackShipments(search); // exception returned in case where no results found!
				}
				catch
				{
					output = null;
				}
			}

			return output == null || output.shipments.All(s => s.bol != number)
				       ? string.Empty
				       : output.shipments.First(s => s.bol == number).pro;
		}
	}
}
