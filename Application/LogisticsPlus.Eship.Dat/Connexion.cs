﻿using System;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel;
using LogisticsPlus.Eship.Dat.Responses;

namespace LogisticsPlus.Eship.Dat
{
    public class Connexion
    {
        private static bool _isInitialized;

        private static readonly object LockObj = new object();

        private static Dictionary<string, AuthenticationInformation> _loginTokens;


        private static ApplicationHeader _applicationHeader;
        private static CorrelationHeader _correlationHeader;
        private static TfmiFreightMatchingPortTypeClient _client;
        private static TfmiFreightMatchingPortTypeClient _secureClient;


        private string Username { get; set; }

        private string Password { get; set; }


        public static void Initialize(string url, string secureUrl)
        {
            if (_isInitialized) return;

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            _loginTokens = new Dictionary<string, AuthenticationInformation>();
            _applicationHeader = new ApplicationHeader { application = "eShipPlus Connexion Integration", applicationVersion = "2.0" };
            _correlationHeader = new CorrelationHeader();
            _client = new TfmiFreightMatchingPortTypeClient(new BasicHttpBinding(BasicHttpSecurityMode.None) { MaxReceivedMessageSize = 2 << 20 }, new EndpointAddress(url));
            _secureClient = new TfmiFreightMatchingPortTypeClient(new BasicHttpsBinding { MaxReceivedMessageSize = 2 << 20 }, new EndpointAddress(secureUrl));
            _isInitialized = true;
        }


        public bool AccountHasAccessTo(CapabilityType type)
        {
            GetOrRefreshSessionToken(Username, Password);

            var key = string.Format("{0}{1}", Username, Password);
            if (_loginTokens.ContainsKey(key))
                switch (type)
                {
                    case CapabilityType.ManagePostings:
                        return _loginTokens[key].CanManagePostings;
                    case CapabilityType.Search:
                        return _loginTokens[key].CanSearch;
                    case CapabilityType.AlarmMatch:
                        return _loginTokens[key].CanAlarmMatch;
                    case CapabilityType.LookupSpotRates:
                        return _loginTokens[key].CanLookupSpotRates;
                    case CapabilityType.LookupContractRates:
                        return _loginTokens[key].CanLookupContractRates;
                    case CapabilityType.LookupBasicCarrierInfo:
                        return _loginTokens[key].CanLookupBasicCarrierInfo;
                    case CapabilityType.LookupPremiumCarrierInfo:
                        return _loginTokens[key].CanLookupPremiumCarrierInfo;
                }

            return false;
        }

        public DatLookupCapabilitiesResponse LookupCapabilities(LookupCapabilitiesRequest lookupCapabilitiesRequest)
        {
            var loginResult = GetOrRefreshSessionToken(Username, Password);
            if (loginResult.HasError) return new DatLookupCapabilitiesResponse { LookupCapabilitiesSuccessData = null, Error = loginResult.Error };
            var sessionHeader = loginResult.SessionHeader;

            WarningHeader warningHeader;
            LookupCapabilitiesResponse lookupCapabilitiesResponse;
            _client.LookupCapabilities(_applicationHeader, ref _correlationHeader, ref sessionHeader, lookupCapabilitiesRequest, out warningHeader, out lookupCapabilitiesResponse);

            return new DatLookupCapabilitiesResponse
            {
                LookupCapabilitiesSuccessData = lookupCapabilitiesResponse.lookupCapabilitiesResult.Item as LookupCapabilitiesSuccessData,
                Error = lookupCapabilitiesResponse.lookupCapabilitiesResult.Item as ServiceError
            };
        }
        
        public DatLookupAssetResponse LookupAsset(LookupAssetRequest lookupAssetRequest)
        {
            var loginResult = GetOrRefreshSessionToken(Username, Password);
            if (loginResult.HasError) return new DatLookupAssetResponse { LookupAssetSuccessData = null, Error = loginResult.Error };
            var sessionHeader = loginResult.SessionHeader;
            WarningHeader warningHeader;
            LookupAssetResponse lookupAssetResponse;

            _client.LookupAsset(_applicationHeader, ref _correlationHeader, ref sessionHeader, lookupAssetRequest, out warningHeader, out lookupAssetResponse);
            return new DatLookupAssetResponse
                {
                    LookupAssetSuccessData = lookupAssetResponse.lookupAssetResult.Item as LookupAssetSuccessData,
                    Error = lookupAssetResponse.lookupAssetResult.Item as ServiceError
                };
        }

        public DatPostAssetResponse PostAsset(PostAssetRequest postAssetRequest)
        {
            var loginResult = GetOrRefreshSessionToken(Username, Password);
            if (loginResult.HasError) return new DatPostAssetResponse { PostAssetSuccessData = null, Errors = new List<ServiceError> { loginResult.Error } };
            var sessionHeader = loginResult.SessionHeader;
            WarningHeader warningHeader;
            PostAssetResponse postAssetResponse;
           
            _client.PostAsset(_applicationHeader, ref _correlationHeader, ref sessionHeader, postAssetRequest, out warningHeader, out postAssetResponse);

            var successes = new List<PostAssetSuccessData>();
            var errors = new List<ServiceError>();

            foreach (var postAssetResult in postAssetResponse.postAssetResults)
            {
                var successResult = postAssetResult.Item as PostAssetSuccessData;
                var errorResult = postAssetResult.Item as ServiceError;

                if (successResult != null) successes.Add(successResult);
                else if (errorResult != null) errors.Add(errorResult);
            }

            return new DatPostAssetResponse
                {
                    PostAssetSuccessData = successes,
                    Errors = errors
                };
        }

        public DatUpdateAssetResponse UpdateAsset(UpdateAssetRequest updateAssetRequest)
        {
            var loginResult = GetOrRefreshSessionToken(Username, Password);
            if (loginResult.HasError) return new DatUpdateAssetResponse {UpdateAssetSuccessData = null, Error = loginResult.Error};
            var sessionHeader = loginResult.SessionHeader;

            WarningHeader warningHeader;
            UpdateAssetResponse updateAssetResponse;
            _client.UpdateAsset(_applicationHeader, ref _correlationHeader, ref sessionHeader, updateAssetRequest,
                                out warningHeader, out updateAssetResponse);
            return new DatUpdateAssetResponse
                {
                    UpdateAssetSuccessData = updateAssetResponse.updateAssetResult.Item as UpdateAssetSuccessData,
                    Error = updateAssetResponse.updateAssetResult.Item as ServiceError
                };
        }

        public DatDeleteAssetResponse DeleteAsset(DeleteAssetRequest deleteAssetRequest)
        {
            var loginResult = GetOrRefreshSessionToken(Username, Password);
            if (loginResult.HasError) return new DatDeleteAssetResponse { DeleteAssetSuccessData = null, Error = loginResult.Error };
            var sessionHeader = loginResult.SessionHeader;

            WarningHeader warningHeader;
            DeleteAssetResponse deleteAssetResponse;
            _client.DeleteAsset(_applicationHeader, ref _correlationHeader, ref sessionHeader, deleteAssetRequest, out warningHeader, out deleteAssetResponse);
            return new DatDeleteAssetResponse
                {
                    DeleteAssetSuccessData = deleteAssetResponse.deleteAssetResult.Item as DeleteAssetSuccessData,
                    Error = deleteAssetResponse.deleteAssetResult.Item as ServiceError
                };
        }

        public DatCountAssetsResponse CountAssets(CountAssetsRequest countAssetRequest)
        {
            var loginResult = GetOrRefreshSessionToken(Username, Password);
            if (loginResult.HasError) return new DatCountAssetsResponse { CountAssetsSuccessData = null, Error = loginResult.Error };
            var sessionHeader = loginResult.SessionHeader;

            WarningHeader warningHeader;
            CountAssetsResponse countAssetResponse;
            _client.CountAssets(_applicationHeader, ref _correlationHeader, ref sessionHeader, countAssetRequest, out warningHeader, out countAssetResponse);
            return new DatCountAssetsResponse
                {
                    CountAssetsSuccessData = countAssetResponse.countAssetsResult.Item as CountAssetsSuccessData,
                    Error = countAssetResponse.countAssetsResult.Item as ServiceError
                };
        }

        public void DeleteAllAssets()
        {
            var deleteAssetRequest = new DeleteAssetRequest { deleteAssetOperation = new DeleteAssetOperation { Item = new DeleteAllMyAssets() } };
            var sessionHeader = GetOrRefreshSessionToken(Username, Password).SessionHeader;

            WarningHeader warningHeader;
            DeleteAssetResponse deleteAssetResponse;
            _client.DeleteAsset(_applicationHeader, ref _correlationHeader, ref sessionHeader, deleteAssetRequest, out warningHeader, out deleteAssetResponse);
        }


        public DatLookupAlarmResponse LookupAlarm(LookupAlarmRequest lookupAlarmRequest)
        {
            var loginResult = GetOrRefreshSessionToken(Username, Password);
            if (loginResult.HasError) return new DatLookupAlarmResponse { LookupAlarmSuccessData = null, Error = loginResult.Error };
            var sessionHeader = loginResult.SessionHeader;

            WarningHeader warningHeader;
            LookupAlarmResponse lookupAlarmResponse;
            _client.LookupAlarm(_applicationHeader, ref _correlationHeader, ref sessionHeader, lookupAlarmRequest, out warningHeader, out lookupAlarmResponse);
            return new DatLookupAlarmResponse
                {
                    LookupAlarmSuccessData = lookupAlarmResponse.lookupAlarmResult.Item as LookupAlarmSuccessData,
                    Error = lookupAlarmResponse.lookupAlarmResult.Item as ServiceError
                };
        }

        public DatCreateAlarmResponse CreateAlarm(CreateAlarmRequest createAlarmRequest)
        {
            var loginResult = GetOrRefreshSessionToken(Username, Password);
            if (loginResult.HasError) return new DatCreateAlarmResponse { CreateAlarmSuccessData = null, Error = loginResult.Error };
            var sessionHeader = loginResult.SessionHeader;

            WarningHeader warningHeader;
            CreateAlarmResponse createAlarmResponse;
            _client.CreateAlarm(_applicationHeader, ref _correlationHeader, ref sessionHeader, createAlarmRequest, out warningHeader, out createAlarmResponse);
            return new DatCreateAlarmResponse
                {
                    CreateAlarmSuccessData = createAlarmResponse.createAlarmResult.Item as CreateAlarmSuccessData,
                    Error = createAlarmResponse.createAlarmResult.Item as ServiceError
                };
        }

        public DatUpdateAlarmResponse UpdateAlarm(UpdateAlarmRequest updateAlarmRequest)
        {
            var loginResult = GetOrRefreshSessionToken(Username, Password);
            if (loginResult.HasError) return new DatUpdateAlarmResponse { UpdateAlarmSuccessData = null, Error = loginResult.Error };
            var sessionHeader = loginResult.SessionHeader;

            WarningHeader warningHeader;
            UpdateAlarmResponse updateAlarmResponse;
            _client.UpdateAlarm(_applicationHeader, ref _correlationHeader, ref sessionHeader, updateAlarmRequest, out warningHeader, out updateAlarmResponse);
            return new DatUpdateAlarmResponse
                {
                    UpdateAlarmSuccessData = updateAlarmResponse.updateAlarmResult.Item as UpdateAlarmSuccessData,
                    Error = updateAlarmResponse.updateAlarmResult.Item as ServiceError
                };
        }

        public DatDeleteAlarmResponse DeleteAlarm(DeleteAlarmRequest deleteAlarmRequest)
        {
            var loginResult = GetOrRefreshSessionToken(Username, Password);
            if (loginResult.HasError) return new DatDeleteAlarmResponse { DeleteAlarmSuccessData = null, Error = loginResult.Error };
            var sessionHeader = loginResult.SessionHeader;

            WarningHeader warningHeader;
            DeleteAlarmResponse deleteAlarmResponse;
            _client.DeleteAlarm(_applicationHeader, ref _correlationHeader, ref sessionHeader, deleteAlarmRequest, out warningHeader, out deleteAlarmResponse);
            return new DatDeleteAlarmResponse
                {
                    DeleteAlarmSuccessData = deleteAlarmResponse.deleteAlarmResult.Item as DeleteAlarmSuccessData,
                    Error = deleteAlarmResponse.deleteAlarmResult.Item as ServiceError
                };
        }


        public DatCreateSearchResponse Search(CreateSearchRequest searchRequest)
        {
            var loginResult = GetOrRefreshSessionToken(Username, Password);
            if (loginResult.HasError) return new DatCreateSearchResponse { CreateSearchSuccessData = null, Error = loginResult.Error };
            var sessionHeader = loginResult.SessionHeader;

            WarningHeader warningHeader;
            CreateSearchResponse createSearchResponse;
            _client.CreateSearch(_applicationHeader, ref _correlationHeader, ref sessionHeader, searchRequest, out warningHeader, out createSearchResponse);
            return new DatCreateSearchResponse
                {
                    CreateSearchSuccessData = createSearchResponse.createSearchResult.Item as CreateSearchSuccessData,
                    Error = createSearchResponse.createSearchResult.Item as ServiceError
                };
        }


        public DatLookupDoeFuelPricesResponse LookupDoeFuelPrices(LookupDoeFuelPricesRequest lookupDoeFuelPricesRequest)
        {
            var loginResult = GetOrRefreshSessionToken(Username, Password);
            if (loginResult.HasError) return new DatLookupDoeFuelPricesResponse { LookupDoeFuelPricesSuccessData = null, Error = loginResult.Error };
            var sessionHeader = loginResult.SessionHeader;

            WarningHeader warningHeader;
            LookupDoeFuelPricesResponse lookupDoeFuelPricesResponse;
            _client.LookupDoeFuelPrices(_applicationHeader, ref _correlationHeader, ref sessionHeader, lookupDoeFuelPricesRequest, out warningHeader, out lookupDoeFuelPricesResponse);
            return new DatLookupDoeFuelPricesResponse
                {
                    LookupDoeFuelPricesSuccessData = lookupDoeFuelPricesResponse.lookupDoeFuelPricesResult.Item as LookupDoeFuelPricesSuccessData,
                    Error = lookupDoeFuelPricesResponse.lookupDoeFuelPricesResult.Item as ServiceError
                };
        }


        public DatLookupRateResponse LookupCurrentRate(LookupRateRequest lookupRateRequest)
        {
            var loginResult = GetOrRefreshSessionToken(Username, Password);
            if (loginResult.HasError) return new DatLookupRateResponse { LookupRateSuccessData = null, Errors = new List<ServiceError> { loginResult.Error } };
            var sessionHeader = loginResult.SessionHeader;

            WarningHeader warningHeader;
            LookupRateResponse lookupRateResponse;
            _client.LookupRate(_applicationHeader, ref _correlationHeader, ref sessionHeader, lookupRateRequest, out warningHeader, out lookupRateResponse);

            var successes = new List<LookupRateSuccessData>();
            var errors = new List<ServiceError>();

            foreach (var lookupRateResult in lookupRateResponse.lookupRateResults)
            {
                var successResult = lookupRateResult.Item as LookupRateSuccessData;
                var errorResult = lookupRateResult.Item as ServiceError;

                if (successResult != null) successes.Add(successResult);
                else if (errorResult != null) errors.Add(errorResult);
            }

            return new DatLookupRateResponse
                {
                    LookupRateSuccessData = successes,
                    Errors = errors
                };
        }

        public DatLookupHistoricContractRatesResponse LookupHistoricContract(LookupHistoricContractRatesRequest lookupHistoricContractRatesOperation)
        {
            var loginResult = GetOrRefreshSessionToken(Username, Password);
            if (loginResult.HasError) return new DatLookupHistoricContractRatesResponse { LookupHistoricContractRatesSuccessData = null, Error = loginResult.Error };
            var sessionHeader = loginResult.SessionHeader;

            WarningHeader warningHeader;
            LookupHistoricContractRatesResponse lookupHistoricContractRatesResponse;
            _client.LookupHistoricContractRates(_applicationHeader, ref _correlationHeader, ref sessionHeader, lookupHistoricContractRatesOperation, out warningHeader, out lookupHistoricContractRatesResponse);
            return new DatLookupHistoricContractRatesResponse
                {
                    LookupHistoricContractRatesSuccessData = lookupHistoricContractRatesResponse.lookupHistoricContractRatesResult.Item as LookupHistoricContractRatesSuccessData,
                    Error = lookupHistoricContractRatesResponse.lookupHistoricContractRatesResult.Item as ServiceError
                };
        }

        public DatLookupHistoricSpotRatesResponse LookupHistoricSpot(LookupHistoricSpotRatesRequest lookupHistoricSpotRatesRequest)
        {
            var loginResult = GetOrRefreshSessionToken(Username, Password);
            if (loginResult.HasError) return new DatLookupHistoricSpotRatesResponse { LookupHistoricSpotRatesSuccessData = null, Error = loginResult.Error };
            var sessionHeader = loginResult.SessionHeader;

            WarningHeader warningHeader;
            LookupHistoricSpotRatesResponse lookupHistoricSpotRatesResponse;
            _client.LookupHistoricSpotRates(_applicationHeader, ref _correlationHeader, ref sessionHeader, lookupHistoricSpotRatesRequest, out warningHeader, out lookupHistoricSpotRatesResponse);

            return new DatLookupHistoricSpotRatesResponse
                {
                    LookupHistoricSpotRatesSuccessData = lookupHistoricSpotRatesResponse.lookupHistoricSpotRatesResult.Item as LookupHistoricSpotRatesSuccessData,
                    Error = lookupHistoricSpotRatesResponse.lookupHistoricSpotRatesResult.Item as ServiceError
                };
        }


        public DatLookupDobCarriersResponse LookupDobCarriersByCarrierId(string carrierId)
        {
            return LookupDobCarriers(new LookupDobCarriersRequest { lookupDobCarriersOperations = new LookupDobCarriersOperation { carrierId = carrierId } });
        }

        public DatLookupDobEventsResponse LookupDobEvents(DateTime since)
        {
            var operation = new LookupDobEventsOperation { sinceDate = since };
            var request = new LookupDobEventsRequest { lookupDobEventsOperations = operation };

            return LookupDobEvents(request);
        }

        public DatLookupDobSignedCarriersResponse LookupSignedCarriers()
        {
            var operation = new LookupDobSignedCarriersOperation();
            var request = new LookupDobSignedCarriersRequest { lookupDobSignedCarriersOperations = operation };

            return LookupSignedCarriers(request);
        }

        public DatLookupCarrierResponse LookupCarrierByDotNumber(int dotNumber)
        {
            var lookupCarrierRequest = BuildLookupCarrierRequest(dotNumber, ItemChoiceType2.dotNumber);
            return LookupCarrier(lookupCarrierRequest);
        }

        public DatLookupCarrierResponse LookupCarrierByMcNumber(int mcNumber)
        {
            var lookupCarrierRequest = BuildLookupCarrierRequest(mcNumber, ItemChoiceType2.mcNumber);
            return LookupCarrier(lookupCarrierRequest);
        }

        public DatLookupCarrierResponse LookupCarrierByUserId(int userId)
        {
            var lookupCarrierRequest = BuildLookupCarrierRequest(userId, ItemChoiceType2.userId);
            return LookupCarrier(lookupCarrierRequest);
        }

        public DatLookupCarrierResponse LookupCarrier(LookupCarrierRequest lookupCarrierRequest)
        {
            var loginResult = GetOrRefreshSessionToken(Username, Password);
            if (loginResult.HasError) return new DatLookupCarrierResponse { LookupCarrierSuccessData = null, Error = loginResult.Error };
            var sessionHeader = loginResult.SessionHeader;

            WarningHeader warningHeader;
            LookupCarrierResponse lookupCarrierResponse;
            _client.LookupCarrier(_applicationHeader, ref _correlationHeader, ref sessionHeader, lookupCarrierRequest, out warningHeader, out lookupCarrierResponse);
            var lookupCarrierResult = lookupCarrierResponse.lookupCarrierResult[0];
            return new DatLookupCarrierResponse
                {
                    LookupCarrierSuccessData = lookupCarrierResult.Item as LookupCarrierSuccessData,
                    Error = lookupCarrierResult.Item as ServiceError
                };
        }


        private DatLookupDobCarriersResponse LookupDobCarriers(LookupDobCarriersRequest lookupDobCarriersRequest)
        {
            var loginResult = GetOrRefreshSessionToken(Username, Password);
            if (loginResult.HasError) return new DatLookupDobCarriersResponse { LookupDobCarriersSuccessData = null, Error = loginResult.Error };
            var sessionHeader = loginResult.SessionHeader;

            WarningHeader warningHeader;
            LookupDobCarriersResponse lookupDobCarriersResponse;
            _secureClient.LookupDobCarriers(_applicationHeader, ref _correlationHeader, ref sessionHeader, lookupDobCarriersRequest, out warningHeader, out lookupDobCarriersResponse);
            return new DatLookupDobCarriersResponse
                {
                    LookupDobCarriersSuccessData = lookupDobCarriersResponse.lookupDobCarriersResults.Item as LookupDobCarriersSuccessData,
                    Error = lookupDobCarriersResponse.lookupDobCarriersResults.Item as ServiceError
                };
        }

        private DatLookupDobEventsResponse LookupDobEvents(LookupDobEventsRequest request)
        {
            var loginResult = GetOrRefreshSessionToken(Username, Password);
            if (loginResult.HasError) return new DatLookupDobEventsResponse { LookupDobEventsSuccessData = null, Error = loginResult.Error };
            var sessionHeader = loginResult.SessionHeader;

            WarningHeader warningHeader;
            LookupDobEventsResponse lookupDobEventsResponse;
            _secureClient.lookupDobEvents(_applicationHeader, ref _correlationHeader, ref sessionHeader, request, out warningHeader, out lookupDobEventsResponse);
            return new DatLookupDobEventsResponse
                {
                    LookupDobEventsSuccessData = lookupDobEventsResponse.lookupDobEventsResults.Item as LookupDobEventsSuccessData,
                    Error = lookupDobEventsResponse.lookupDobEventsResults.Item as ServiceError
                };
        }

        private DatLookupDobSignedCarriersResponse LookupSignedCarriers(LookupDobSignedCarriersRequest request)
        {
            var loginResult = GetOrRefreshSessionToken(Username, Password);
            if (loginResult.HasError) return new DatLookupDobSignedCarriersResponse { LookupDobSignedCarriersSuccessData = null, Error = loginResult.Error };
            var sessionHeader = loginResult.SessionHeader;

            WarningHeader warningHeader;
            LookupDobSignedCarriersResponse signedCarriersResponse;
            _secureClient.LookupDobSignedCarriers(_applicationHeader, ref _correlationHeader, ref sessionHeader, request, out warningHeader, out signedCarriersResponse);
            return new DatLookupDobSignedCarriersResponse
                {
                    LookupDobSignedCarriersSuccessData = signedCarriersResponse.lookupDobSignedCarriersResults.Item as LookupDobSignedCarriersSuccessData,
                    Error = signedCarriersResponse.lookupDobSignedCarriersResults.Item as ServiceError
                };
        }


        private static LookupCarrierRequest BuildLookupCarrierRequest(object item, ItemChoiceType2 itemElementName)
        {
            var lookupCarrierOperation = new LookupCarrierOperation
            {
                Item = item,
                ItemElementName = itemElementName,
                includeDotAuthority = true,
                includeDotAuthoritySpecified = true,
                includeDotInsurance = true,
                includeDotInsuranceSpecified = true,
                includeDotProfile = true,
                includeDotProfileSpecified = true,
                includeFmcsaCrashes = true,
                includeFmcsaCrashesSpecified = true,
                includeFmcsaInspections = true,
                includeFmcsaInspectionsSpecified = true,
                includeFmcsaSafeStat = true,
                includeFmcsaSafeStatSpecified = true,
                includeFmcsaSafetyRating = true,
                includeFmcsaSafetyRatingSpecified = true,
                includeCsa2010Basic = true,
                includeCsa2010BasicSpecified = true,
                includeCsa2010SafetyFitness = true,
                includeCsa2010SafetyFitnessSpecified = true,
                includeExtendedProfile = true,
                includeExtendedProfileSpecified = true
            };

            return new LookupCarrierRequest { lookupCarrierOperation = new[] { lookupCarrierOperation } };
        }


        private static DatLoginResponse GetOrRefreshSessionToken(string username, string password)
        {
            if (!_isInitialized) throw new Exception("Connexion static method Initialize must be called before making requests.");
            var key = string.Format("{0}{1}", username, password);

            // return existing session token if unexpired
            if (_loginTokens.ContainsKey(key) && _loginTokens[key].SessionToken.expiration > DateTime.Now.AddMinutes(1))
                return new DatLoginResponse
                    {
                        SessionHeader = new SessionHeader
                            {
                                sessionToken = new SessionToken
                                    {
                                        expiration = _loginTokens[key].SessionToken.expiration,
                                        primary = _loginTokens[key].SessionToken.primary,
                                        secondary = _loginTokens[key].SessionToken.secondary,
                                        expirationSpecified = true
                                    }
                            },
                        Error = null
                    };

            var loginRequest = new LoginRequest
            {
                loginOperation = new LoginOperation
                {
                    loginId = username,
                    password = password,
                    thirdPartyId = "eShipPlus"
                }
            };

            LoginResponse loginResponse;
            WarningHeader warningHeader;
            var refSessionHeader = new SessionHeader { sessionToken = new SessionToken { primary = new byte[] { }, secondary = new byte[] { } } };
            var correlationHeader = _correlationHeader;
            _client.Login(_applicationHeader, ref correlationHeader, ref refSessionHeader, loginRequest, out warningHeader, out loginResponse);


            var loginSuccessData = loginResponse.loginResult.Item as LoginSuccessData;
            var error = loginResponse.loginResult.Item as ServiceError;
            var sessionHeader = loginSuccessData == null
                                    ? null
                                    : new SessionHeader
                                    {
                                        sessionToken = new SessionToken
                                        {
                                            expiration = loginSuccessData.expiration,
                                            primary = loginSuccessData.token.primary,
                                            secondary = loginSuccessData.token.secondary,
                                            expirationSpecified = true
                                        }
                                    };

            if (sessionHeader == null) return new DatLoginResponse { SessionHeader = null, Error = error };
            var lookupCapabilitiesRequest = new LookupCapabilitiesRequest
                {
                    lookupCapabilitiesOperation =
                        new LookupCapabilitiesOperation
                            {
                                capability =
                                    new[]
                                        {
                                            CapabilityType.AlarmMatch, CapabilityType.LookupBasicCarrierInfo,
                                            CapabilityType.LookupContractRates, CapabilityType.LookupPremiumCarrierInfo,
                                            CapabilityType.LookupSpotRates, CapabilityType.ManagePostings,
                                            CapabilityType.Search
                                        }
                            }
                };

            LookupCapabilitiesResponse lookupCapabilitiesResponse;

            var refSessHeader = sessionHeader;
            _client.LookupCapabilities(_applicationHeader, ref _correlationHeader, ref refSessHeader, lookupCapabilitiesRequest, out warningHeader, out lookupCapabilitiesResponse);
            var lookupResponse = new DatLookupCapabilitiesResponse
                {
                    LookupCapabilitiesSuccessData = lookupCapabilitiesResponse.lookupCapabilitiesResult.Item as LookupCapabilitiesSuccessData,
                    Error = lookupCapabilitiesResponse.lookupCapabilitiesResult.Item as ServiceError
                };
            if (lookupResponse.HasError) return new DatLoginResponse { SessionHeader = null, Error = lookupResponse .Error};


            lock (LockObj)
            {
                var authInfo = new AuthenticationInformation
                    {
                        SessionToken = loginSuccessData.token,
                        CanAlarmMatch = lookupResponse.LookupCapabilitiesSuccessData.capabilityPresent[0],
                        CanLookupBasicCarrierInfo = lookupResponse.LookupCapabilitiesSuccessData.capabilityPresent[1],
                        CanLookupContractRates = lookupResponse.LookupCapabilitiesSuccessData.capabilityPresent[2],
                        CanLookupPremiumCarrierInfo = lookupResponse.LookupCapabilitiesSuccessData.capabilityPresent[3],
                        CanLookupSpotRates = lookupResponse.LookupCapabilitiesSuccessData.capabilityPresent[4],
                        CanManagePostings = lookupResponse.LookupCapabilitiesSuccessData.capabilityPresent[5],
                        CanSearch = lookupResponse.LookupCapabilitiesSuccessData.capabilityPresent[6],
                    };
                
                // loginSuccessData.token.expiration does not have the expiration date but sessionHeader.sessionToken.expiration does
                authInfo.SessionToken.expiration = sessionHeader.sessionToken.expiration;

                if (!_loginTokens.ContainsKey(key)) _loginTokens.Add(key, authInfo);
                else _loginTokens[key] = authInfo;
            }

            

            return new DatLoginResponse {SessionHeader = sessionHeader, Error = null};
        }


        public Connexion(string username, string password)
        {
            Username = username;
            Password = password;
        }
    }
}
