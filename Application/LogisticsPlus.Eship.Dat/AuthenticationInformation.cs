﻿namespace LogisticsPlus.Eship.Dat
{
    public class AuthenticationInformation
    {
        public SessionToken SessionToken { get; set; }

        public bool CanAlarmMatch { get; set; }
        public bool CanLookupBasicCarrierInfo { get; set; }
        public bool CanLookupContractRates { get; set; }
        public bool CanLookupPremiumCarrierInfo { get; set; }
        public bool CanLookupSpotRates { get; set; }
        public bool CanManagePostings { get; set; }
        public bool CanSearch { get; set; }
    }
}
