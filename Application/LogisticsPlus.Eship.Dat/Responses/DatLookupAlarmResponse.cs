﻿namespace LogisticsPlus.Eship.Dat.Responses
{
    public class DatLookupAlarmResponse : DatResponseBase
    {
        public LookupAlarmSuccessData LookupAlarmSuccessData { get; set; }
    }
}
