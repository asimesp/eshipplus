﻿namespace LogisticsPlus.Eship.Dat.Responses
{
    public class DatLookupAssetResponse : DatResponseBase
    {
        public LookupAssetSuccessData LookupAssetSuccessData { get; set; }
    }
}
