﻿using System.Collections.Generic;

namespace LogisticsPlus.Eship.Dat.Responses
{
    public class DatLookupRateResponse
    {
        public List<LookupRateSuccessData> LookupRateSuccessData { get; set; }

        public List<ServiceError> Errors { get; set; }
    }
}
