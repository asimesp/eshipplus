﻿namespace LogisticsPlus.Eship.Dat.Responses
{
    public class DatDeleteAlarmResponse : DatResponseBase
    {
        public DeleteAlarmSuccessData DeleteAlarmSuccessData { get; set; }
    }
}
