﻿namespace LogisticsPlus.Eship.Dat.Responses
{
    public class DatLookupCarrierResponse : DatResponseBase
    {
        public LookupCarrierSuccessData LookupCarrierSuccessData { get; set; }
    }
}
