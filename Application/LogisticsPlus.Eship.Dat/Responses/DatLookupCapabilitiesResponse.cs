﻿namespace LogisticsPlus.Eship.Dat.Responses
{
    public class DatLookupCapabilitiesResponse : DatResponseBase
    {
        public LookupCapabilitiesSuccessData LookupCapabilitiesSuccessData { get; set; }
    }
}
