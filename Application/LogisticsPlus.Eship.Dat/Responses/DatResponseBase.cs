﻿namespace LogisticsPlus.Eship.Dat.Responses
{
    public class DatResponseBase
    {
        public bool HasError { get { return Error != null; } }

        public ServiceError Error { get; set; }
    }
}
