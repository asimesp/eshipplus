﻿namespace LogisticsPlus.Eship.Dat.Responses
{
    public class DatLookupHistoricSpotRatesResponse : DatResponseBase
    {
        public LookupHistoricSpotRatesSuccessData LookupHistoricSpotRatesSuccessData { get; set; }
    }
}
