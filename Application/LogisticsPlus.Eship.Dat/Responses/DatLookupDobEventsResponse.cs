﻿namespace LogisticsPlus.Eship.Dat.Responses
{
    public class DatLookupDobEventsResponse : DatResponseBase
    {
        public LookupDobEventsSuccessData LookupDobEventsSuccessData { get; set; }
    }
}
