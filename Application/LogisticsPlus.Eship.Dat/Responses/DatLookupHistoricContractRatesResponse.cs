﻿namespace LogisticsPlus.Eship.Dat.Responses
{
    public class DatLookupHistoricContractRatesResponse : DatResponseBase
    {
        public LookupHistoricContractRatesSuccessData LookupHistoricContractRatesSuccessData { get; set; }
    }
}
