﻿namespace LogisticsPlus.Eship.Dat.Responses
{
    public class DatLoginResponse : DatResponseBase
    {
        public SessionHeader SessionHeader { get; set; }
    }
}
