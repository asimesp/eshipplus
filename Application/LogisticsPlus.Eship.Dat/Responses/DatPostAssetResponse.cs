﻿using System.Collections.Generic;

namespace LogisticsPlus.Eship.Dat.Responses
{
    public class DatPostAssetResponse : DatMultipleErrorResponseBase
    {
        public List<PostAssetSuccessData> PostAssetSuccessData { get; set; }
    }
}
