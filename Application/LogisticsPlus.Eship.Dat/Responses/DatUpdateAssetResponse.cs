﻿namespace LogisticsPlus.Eship.Dat.Responses
{
    public class DatUpdateAssetResponse : DatResponseBase
    {
        public UpdateAssetSuccessData UpdateAssetSuccessData { get; set; }
    }
}