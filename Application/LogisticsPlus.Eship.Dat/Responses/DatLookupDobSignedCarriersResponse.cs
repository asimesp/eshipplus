﻿namespace LogisticsPlus.Eship.Dat.Responses
{
    public class DatLookupDobSignedCarriersResponse : DatResponseBase
    {
        public LookupDobSignedCarriersSuccessData LookupDobSignedCarriersSuccessData { get; set; }
    }
}
