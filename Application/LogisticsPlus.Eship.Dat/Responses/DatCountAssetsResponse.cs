﻿namespace LogisticsPlus.Eship.Dat.Responses
{
    public class DatCountAssetsResponse : DatResponseBase
    {
        public CountAssetsSuccessData CountAssetsSuccessData { get; set; }
    }
}
