﻿namespace LogisticsPlus.Eship.Dat.Responses
{
    public class DatLookupDobCarriersResponse : DatResponseBase
    {
        public LookupDobCarriersSuccessData LookupDobCarriersSuccessData { get; set; }
    }
}
