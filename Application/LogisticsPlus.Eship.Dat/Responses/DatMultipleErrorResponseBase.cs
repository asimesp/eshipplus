﻿using System.Collections.Generic;
using System.Linq;

namespace LogisticsPlus.Eship.Dat.Responses
{
    public class DatMultipleErrorResponseBase
    {
        public bool HasError { get { return Errors != null && Errors.Any(); } }

        public List<ServiceError> Errors { get; set; }
    }
}
