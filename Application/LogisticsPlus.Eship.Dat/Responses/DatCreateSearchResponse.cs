﻿namespace LogisticsPlus.Eship.Dat.Responses
{
    public class DatCreateSearchResponse : DatResponseBase
    {
        public CreateSearchSuccessData CreateSearchSuccessData { get; set; }
    }
}
