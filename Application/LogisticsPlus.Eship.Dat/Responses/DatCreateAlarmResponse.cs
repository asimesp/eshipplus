﻿namespace LogisticsPlus.Eship.Dat.Responses
{
    public class DatCreateAlarmResponse : DatResponseBase
    {
        public CreateAlarmSuccessData CreateAlarmSuccessData { get; set; }
    }
}
