﻿namespace LogisticsPlus.Eship.Dat.Responses
{
    public class DatDeleteAssetResponse : DatResponseBase
    {
        public DeleteAssetSuccessData DeleteAssetSuccessData { get; set; }
    }
}
