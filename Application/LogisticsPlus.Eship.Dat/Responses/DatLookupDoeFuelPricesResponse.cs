﻿namespace LogisticsPlus.Eship.Dat.Responses
{
    public class DatLookupDoeFuelPricesResponse : DatResponseBase
    {
        public LookupDoeFuelPricesSuccessData LookupDoeFuelPricesSuccessData { get; set; }
    }
}
