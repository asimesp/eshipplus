﻿namespace LogisticsPlus.Eship.Dat.Responses
{
    public class DatUpdateAlarmResponse : DatResponseBase
    {
        public UpdateAlarmSuccessData UpdateAlarmSuccessData { get; set; }
    }
}
