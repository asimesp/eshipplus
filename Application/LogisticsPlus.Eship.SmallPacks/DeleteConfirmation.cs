﻿using System.Collections.Generic;

namespace LogisticsPlus.Eship.SmallPacks
{
	public class DeleteConfirmation
	{
		public bool Success { get; set; }

		public List<string> Errors { get; set; }
		public List<string> Warnings { get; set; }

		public DeleteConfirmation()
		{
			Success = false;
			Errors = new List<string>();
			Warnings = new List<string>();
		}
	}
}
