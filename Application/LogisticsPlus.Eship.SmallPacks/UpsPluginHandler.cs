﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.UpsPlugin;
using LogisticssPlus.FedExPlugin.Services;

namespace LogisticsPlus.Eship.SmallPacks
{
	//UPS handler functions still integrated but will not generate rates - intentional set this way Jan 30 2015
	public class UpsPluginHandler
	{
		private readonly UpsServiceParams _parameters;
		private readonly List<SmallPackagingMap> _packagingMaps;
		private readonly List<SmallPackageServiceMap> _serviceMaps;

		public UpsPluginHandler(UpsServiceParams parameters, List<SmallPackagingMap> packagingMaps, List<SmallPackageServiceMap> serviceMaps)
		{
			_parameters = parameters;
			_packagingMaps = packagingMaps;
			_serviceMaps = serviceMaps;
		}

		public DeleteConfirmation DeleteShipment(Shipment shipment)
		{
			var confirmation = new DeleteConfirmation();
			try
			{
				var service = new UpsService(_parameters);
				service.VoidShipment(shipment.Vendors.First(v => v.Primary).ProNumber);
				confirmation.Success = true;
			}
			catch (UpsException upsE)
			{
				confirmation.Errors.Add(upsE.ToString());
				confirmation.Success = false;
			}
			return confirmation;
		}

		public BookingConfirmation BookShipment(Shipment shipment)
		{
			var confirmation = new BookingConfirmation();
			try
			{
				var service = new UpsService(_parameters);
				var upsShipment = GetShipment(shipment, false, shipment.SmallPackType);
				var response = service.BookShipment(upsShipment);

				if (response.IsSuccessful)
				{
					// set tracking number
					var vendor = shipment.Vendors.First(v => v.Primary);
					vendor.ProNumber = response.ShippingNumber;

					// save the high-value report if any
					if (response.HighValueReportData != null)
					{
						confirmation.Documents.Add(
							new BookingDocument
								{
									Name = string.Format("High_Value_Report.{0}", response.HighValueReportFormat),
									Content = response.HighValueReportData
								});
					}

					// save the package's labels
					foreach (var item in response.Items)
						if (item.LabelData != null)
						{
							confirmation.Documents
								.Add(new BookingDocument
										{
											Content = item.LabelData,
											Name = string.Format("{0}.{1}", item.PackageTrackingNumber, item.LabelFormat)
										});
						}
				}
			}
			catch (UpsException upsE)
			{
				confirmation.Errors.Add(upsE.ToString());
			}
			return confirmation;
		}

		public List<Rate> GetRates(Shipment shipment)
		{
			return new List<Rate>();
			// disabled engine Jan 30 2015

			try
			{
				var service = new UpsService(_parameters);
				var upsShipment = GetShipment(shipment, true, string.Empty);
				var upsResponse = service.GetRates(upsShipment);
				return ProcessRateResponses(upsResponse.Items);
			}
			catch
			{
				return new List<Rate>();
			}
		}

		private UpsShipment GetShipment(Shipment shipment, bool ratingOnly, string selectedServiceType)
		{
			var upsShipment = new UpsShipment { ShipmentDate = shipment.DesiredPickupDate };
			var contact = shipment.Destination.Contacts.FirstOrDefault(c => c.Primary) ?? shipment.Destination.Contacts.FirstOrDefault();
			upsShipment.Destination = GetUpsLocation(shipment.Destination, contact ?? new Contact());
			contact = shipment.Origin.Contacts.FirstOrDefault(c => c.Primary) ?? shipment.Origin.Contacts.FirstOrDefault();
			upsShipment.Origin = GetUpsLocation(shipment.Origin, contact ?? new Contact());

			upsShipment.Packages.AddRange(GetUpsItems(shipment.Items));

			var description = string.Empty;
			upsShipment.Description = shipment.Items.Aggregate(description, (i, desc) => string.Format("{0}\n\r{1}", desc, i));

			SetServices(upsShipment, shipment.Services.Select(qs => qs.ServiceId));

			if (!ratingOnly)
			{
				// for booking purposes
				upsShipment.ServiceType = selectedServiceType.ToEnum<UpsServiceType>();
				upsShipment.CustomsPaymentType = CustomsPaymentType.None;

				if (shipment.IsInternational())
				{
					// payor and value
					upsShipment.CustomsPaymentType = CustomsPaymentType.Shipper;
					upsShipment.ConsigneeAccountNumber = string.Empty;
				}
			}

			return upsShipment;
		}

		private void SetServices(UpsShipment upsShipment, IEnumerable<long> serviceIds)
		{
			upsShipment.DeliveryConfirmation = UpsDeliveryConfirmationType.NoConformationRequired;

			foreach (var id in serviceIds)
			{
				if (_serviceMaps.Any(m => m.ServiceId == id && m.SmallPackEngineService == SmallPackConstants.ResidenceService.ResidenceDelivery))
				{
					upsShipment.DestinationIsResidentialAddress = true;
				}
				if (_serviceMaps.Any(m => m.ServiceId == id &&
										  (m.SmallPackEngineService == SmallPackConstants.Signatures.Adult ||
										   m.SmallPackEngineService == SmallPackConstants.Signatures.ServiceDefault)))
				{
					switch (_serviceMaps.First(m => m.ServiceId == id).SmallPackEngineService)
					{
						case SmallPackConstants.Signatures.Adult:
							upsShipment.DeliveryConfirmation = UpsDeliveryConfirmationType.AdultSignatureRequired;
							break;
						default:
							upsShipment.DeliveryConfirmation = UpsDeliveryConfirmationType.SignatureRequired;
							break;
					}
				}
			}
		}

		private UpsShippingParty GetUpsLocation(Location location, Contact contact)
		{
			return new UpsShippingParty
					{
						AddressLine1 = location.Street1,
						AddressLine2 = location.Street2,
						AttentionName = contact.Name,
						CountryCode = location.Country.Code,
						City = location.City,
						EmailAddress = contact.Email,
						Fax = contact.Fax,
						Name = contact.Name,
						Phone = contact.Phone,
						PostalCode = location.PostalCode,
						StateProvinceCode = location.State
					};
		}

		private IEnumerable<UpsPackage> GetUpsItems(IEnumerable<ShipmentItem> items)
		{
			var packages = new List<UpsPackage>();
			foreach (var item in items)
			{
				var map = _packagingMaps.GetPackageType(item.PackageTypeId, SmallPackageEngine.Ups);
				for (var i = 0; i < item.Quantity; i++)
				{
					// do not use dimensions if the packaging is 'Envelope'
					float width = 0, height = 0, length = 0;
					var smallPackEngineType = map != null ? map.SmallPackEngineType : RateServiceUtilities.PackageTypes.YourPackaging;
					if (smallPackEngineType.ToLower() == SmallPackConstants.UpsPackageType.YourPackaging.ToString().ToLower())
					{
						width = (float)item.ActualWidth;
						height = (float)item.ActualHeight;
						length = (float)item.ActualLength;
					}

					var upsPackage = new UpsPackage
					{
						DeclaredValue = (float)Math.Ceiling(item.Value / item.Quantity),
						Weight = (float)(item.ActualWeight / item.Quantity),
						PackagingType = smallPackEngineType,
						Width = width,
						Height = height,
						Length = length
					};
					packages.Add(upsPackage);
				}
			}
			return packages;
		}

		private List<Rate> ProcessRateResponses(IEnumerable<UpsResponseItem> upsResponseItems)
		{
			var rates = new Dictionary<UpsServiceType, Rate>();
			foreach (var rateResponse in upsResponseItems)
			{
				if (!rates.ContainsKey(rateResponse.ServiceType)) rates.Add(rateResponse.ServiceType, new Rate());

				var rate = new Rate
							{
								BilledWeight = (decimal)rateResponse.BillingWeight,
								TransitDays = rateResponse.BusinessDaysInTransit,
								Mode = ServiceMode.SmallPackage,
								OriginalRateValue = (decimal)rateResponse.TotalCharges.Value,
								SmallPackServiceType = rateResponse.ServiceType.ToString(),
								SmallPackEngine = SmallPackageEngine.FedEx,
								Charges = new List<RateCharge> { new RateCharge { UnitBuy = (decimal)rateResponse.TotalCharges.Value, Quantity = 1 } }
							};
				if (rate.Charges.Sum(c => c.FinalBuy) > rates[rateResponse.ServiceType].Charges.Sum(c => c.FinalBuy))
					rates[rateResponse.ServiceType] = rate;
			}
			return rates.Values.ToList();
		}
	}
}
