﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;

namespace LogisticsPlus.Eship.SmallPacks
{
	internal static class SmallPacksUtilities
	{
		internal static Dictionary<int, string> GetAll<TEnum>() where TEnum : struct
		{
			return GetAll<TEnum>(true);
		}

		internal static Dictionary<int, string> GetAll<TEnum>(bool formatName) where TEnum : struct
		{
			var enumerationType = typeof(TEnum);

			if (!enumerationType.IsEnum)
				throw new ArgumentException("Enumeration type is expected.");

			var dictionary = new Dictionary<int, string>();

			foreach (int value in Enum.GetValues(enumerationType))
			{
				var name = formatName
							? Enum.GetName(enumerationType, value).FormattedString()
							: Enum.GetName(enumerationType, value);
				if (!dictionary.ContainsKey(value)) dictionary.Add(value, name);
			}

			return dictionary;
		}

		internal static T ToEnum<T>(this string value)
		{
			return (T)Enum.Parse(typeof(T), value, true);
		}
	}
}
