﻿namespace LogisticsPlus.Eship.SmallPacks
{
	public static class SmallPackConstants
	{
		public static class Signatures
		{
			public const string Adult = "ADULT";
			public const string ServiceDefault = "SERVICE_DEFAULT";
		}

		public class ResidenceService
		{
			public const string ResidenceDelivery = "SHIP_TO_RESIDENCE";
			public const string PremiumResidenceDelivery = "PREMIUM_SHIP_TO_RESIDENCE";
		}

		public enum UpsPackageType
		{
			Envelope,
			YourPackaging,
			Box10Kg,
			Box25Kg,
			Pak,
			Tube
		}
	}
}
