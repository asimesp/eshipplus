﻿using System;
using System.Linq;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticssPlus.FedExPlugin.Core;
using LogisticssPlus.FedExPlugin.Core.Tracking;
using LogisticssPlus.FedExPlugin.Services;
using LogisticssPlus.FedExPlugin.RateWebService;
using FedExAddress = LogisticssPlus.FedExPlugin.Core.FedExAddress;
using FedExCommodity = LogisticssPlus.FedExPlugin.Core.FedExCommodity;
using FedExContact = LogisticssPlus.FedExPlugin.Core.FedExContact;

namespace LogisticsPlus.Eship.SmallPacks
{
	public  class FedExPluginHandler
	{
		private readonly ServiceParams _parameters;
		private readonly List<SmallPackagingMap> _packagingMaps;
		private readonly List<SmallPackageServiceMap> _serviceMaps;

		public FedExPluginHandler(ServiceParams parameters, List<SmallPackagingMap> packagingMaps, List<SmallPackageServiceMap> serviceMaps)
		{
			_parameters = parameters;
			_packagingMaps = packagingMaps;
			_serviceMaps = serviceMaps;
		}

		public DeleteConfirmation DeleteShipment(Shipment shipment)
		{
			var service = new FedExShipService(_parameters);
			var deleteRequest = new FedExDeleteRequestDetail
			                    	{
			                    		ServiceType = shipment.SmallPackType,
			                    		ShipmentDate = shipment.DesiredPickupDate,
			                    		ShipmentNumber = shipment.ShipmentNumber,
			                    		TrackingNumber = shipment.Vendors.First(v => v.Primary).ProNumber
			                    	};
			var confirmation = new DeleteConfirmation {Success = service.DeleteShipment(deleteRequest)};
			if(service.Messages.HasErrors) confirmation.Errors = service.Messages.Errors;
			if(service.Messages.HasWarnings) confirmation.Warnings = service.Messages.Warnings;
			return confirmation;
		}

		public BookingConfirmation BookShipment(Shipment shipment)
		{
			var service = new FedExShipService(_parameters);
			var fedExShipment = GetFedExShipment(shipment, false, shipment.SmallPackType);
			var fedExConfirmations = service.GetShipmentConfirmations(fedExShipment);
			var confirmation = new BookingConfirmation();

			if (!service.Messages.HasErrors)
			{
				// set tracking number
				foreach (var fedExConfirmation in fedExConfirmations.Where(c => c.IsMaster))
				{
					var vendor = shipment.Vendors.First(v => v.Primary);
					vendor.ProNumber = fedExConfirmation.TrackingNumber;
					break;
				}

				foreach (var fedExConfirmation in fedExConfirmations)
				{
					if (fedExConfirmation.Label != null)
					{
						confirmation.Documents.Add(
							new BookingDocument
								{
									Name = string.Format("{0}.{1}", fedExConfirmation.Label.Name, fedExConfirmation.Label.Format),
									Content = fedExConfirmation.Label.Content
								});
					}

					if (fedExConfirmation.AdditionalDocuments != null)
					{
						foreach (var document in fedExConfirmation.AdditionalDocuments)
							confirmation.Documents.Add(new BookingDocument
							                           	{
							                           		Content = document.Content,
							                           		Name = string.Format("{0}.{1}", document.Name, document.Format)
							                           	});
					}
				}
			}
			else
			{
				confirmation.Errors = service.Messages.Errors;
			}
			return confirmation;
		}

		public List<Rate> GetRates(Shipment shipment)
		{
			var service = new FedExRateService(_parameters);
			var fedExShipment = GetFedExShipment(shipment, true, string.Empty);
			var rateReplies = IsValid(fedExShipment) ? service.GetRates(fedExShipment) : new List<FedExRateReply>();
			return ProcessRateReplies(rateReplies);
		}

        public List<FedExTrackDetail> GetTrackingInformation(string trackingNumber, DateTime pickupDate, int dateRange)
        {
            var service = new FedExTrackService(_parameters);
            return service.GetTrackingDetails(trackingNumber, pickupDate, dateRange);
        }

		private FedExShipment GetFedExShipment(Shipment shipment, bool ratingOnly, string selectedServiceType)
		{
			var fedExShipment = new FedExShipment
			{
				DropOffType = DropoffType.BUSINESS_SERVICE_CENTER.ToString(),
				Items = GetFedExItems(shipment.Items),
				ShipmentDate = shipment.DesiredPickupDate
			};
			fedExShipment.PackagingType = fedExShipment.Items.Any() ? fedExShipment.Items[0].PackageType : string.Empty;
			var contact = shipment.Destination.Contacts.FirstOrDefault(c => c.Primary) ?? shipment.Destination.Contacts.FirstOrDefault();
			fedExShipment.Destination = GetFedExLocation(shipment.Destination, contact ?? new Core.Contact());
			contact = shipment.Origin.Contacts.FirstOrDefault(c => c.Primary) ?? shipment.Origin.Contacts.FirstOrDefault();
			fedExShipment.Origin = GetFedExLocation(shipment.Origin, contact ?? new Core.Contact());

			SetPackageOptions(fedExShipment, shipment.Services.Select(ss => ss.ServiceId));

			// haz mat
			if (shipment.HazardousMaterial)
				fedExShipment.DangerousGoodsDetail = new FedExDangerousGoods
				                                     	{
				                                     		Accessibility = DangerousGoodsAccessibilityType.ACCESSIBLE.ToString(),
				                                     		CargoAircraftOnly = false,
				                                     		IsHazMat = true,
				                                     		HazMatDetails = new FedExHazMat
				                                     		                	{
				                                     		                		ContactPerson = shipment.HazardousMaterialContactName,
				                                     		                		ContactPhone = shipment.HazardousMaterialContactPhone,
				                                     		                		Quantity = shipment.Items.Sum(i => i.Quantity),
				                                     		                		Units = shipment.Items.Sum(i => i.PieceCount).ToString()
				                                     		                	}
				                                     	};
			

			if (!ratingOnly)
			{
				// for booking purposes
				fedExShipment.ServiceType = selectedServiceType;
				fedExShipment.References.CustomerReference = shipment.ShipperReference;
				fedExShipment.References.PoNumber = shipment.PurchaseOrderNumber;

				if (shipment.IsInternational())
				{
					// commodities
					fedExShipment.Commodities.AddRange(
						shipment.Items.Select(item =>
										   new FedExCommodity
										   {
											   Description = item.Description,
											   CountryOfManufacture = string.Empty,
											   Pieces = item.PieceCount,
											   Weight = (float)item.ActualWeight,
											   Quantity = item.Quantity,
											   UnitPrice = (float)item.Value,
											   CustomsValue = (float)item.Value,
											   HarmonizedTariffSchedule = item.HTSCode
										   }
							));

					// default export details
					fedExShipment.SetExportDetails();

					// payor and value
					fedExShipment.CustomsPaymentType = "SENDER";
					fedExShipment.CustomsShipmentValue = shipment.Items.Sum(i => i.Value);
					fedExShipment.CustomsPayorAccount = _parameters.AccountNumber;
				}
			}

			return fedExShipment;
		}

		private void SetPackageOptions(FedExShipment fedExShipment, IEnumerable<long> serviceIds)
		{
			fedExShipment.SignatureType = SignatureOptionType.NO_SIGNATURE_REQUIRED.ToString();

			foreach (var id in serviceIds)
			{
				if (
					_serviceMaps.Any(
						m => m.ServiceId == id && m.SmallPackEngineService == SmallPackConstants.ResidenceService.ResidenceDelivery))
				{
					fedExShipment.HomeDelivery = true;
					fedExShipment.HomeDeliveryPremium = string.Empty;
				}
				if (
					_serviceMaps.Any(
						m => m.ServiceId == id && m.SmallPackEngineService == SmallPackConstants.ResidenceService.PremiumResidenceDelivery))
				{
					fedExShipment.HomeDelivery = true;
					fedExShipment.HomeDeliveryPremium = SmallPackConstants.ResidenceService.PremiumResidenceDelivery;
				}
				if (_serviceMaps.Any(m => m.ServiceId == id &&
				                          (m.SmallPackEngineService == SmallPackConstants.Signatures.Adult ||
				                           m.SmallPackEngineService == SmallPackConstants.Signatures.ServiceDefault)))
				{
					fedExShipment.SignatureType = _serviceMaps.First(m => m.ServiceId == id).SmallPackEngineService;
				}
			}
		}

		private static FedExLocation GetFedExLocation(Location location, Core.Contact contact)
		{
			return new FedExLocation
			       	{
			       		Address = new FedExAddress
			       		          	{
			       		          		Country = location.Country == null ? string.Empty : location.Country.Code,
			       		          		PostalCode = location.PostalCode,
			       		          		City = location.City,
			       		          		State = location.State,
			       		          		Street = location.CombinedStreet
			       		          	},
			       		Contact = new FedExContact
			       		          	{
			       		          		Company = contact.Name,
			       		          		Person = contact.Name,
			       		          		Phone = contact.Phone
			       		          	}
			       	};
		}

		private List<FedExItem> GetFedExItems(IEnumerable<ShipmentItem> items)
		{
			var fedExItems = new List<FedExItem>();
			foreach (var item in items)
			{
				var map = _packagingMaps.GetPackageType(item.PackageTypeId, SmallPackageEngine.FedEx);
				for (var i = 0; i < item.Quantity; i++)
				{
					var fi = new FedExItem
					         	{
					         		Description = item.Description,
					         		Height = (float) (map != null && map.RequiredHeight != 0 ? map.RequiredHeight : item.ActualHeight),
					         		InsuredValue = (float) Math.Ceiling(item.Value/item.Quantity),
					         		Length = (float) (map != null && map.RequiredLength != 0 ? map.RequiredLength : item.ActualLength),
					         		PackageType = map != null ? map.SmallPackEngineType : RateServiceUtilities.PackageTypes.YourPackaging,
					         		Weight = (float) (item.ActualWeight/item.Quantity),
					         		Width = (float) (map != null && map.RequiredWidth != 0 ? map.RequiredWidth : item.ActualWidth)
					         	};
					fedExItems.Add(fi);
				}
			}
			return fedExItems;
		}

		private static List<Rate> ProcessRateReplies(IEnumerable<FedExRateReply> fedExRateReplies)
		{
			var rates = new Dictionary<string, Rate>();
			foreach (var rateReply in fedExRateReplies)
			{
				if (!rates.ContainsKey(rateReply.ServiceType)) rates.Add(rateReply.ServiceType, new Rate());

				foreach (var rateReplyRate in rateReply.Rates)
				{
					var rate = new Rate
					           	{
					           		BilledWeight = rateReplyRate.BillingWeight,
					           		TransitDays = rateReply.TransitTime,
									EstimatedDeliveryDate = rateReply.EstimatedDeliveryDate,
					           		Mode = ServiceMode.SmallPackage,
					           		OriginalRateValue = rateReplyRate.NetCharge,
					           		SmallPackServiceType = rateReply.ServiceType,
					           		SmallPackEngine = SmallPackageEngine.FedEx,
					           		Charges = new List<RateCharge> {new RateCharge {UnitBuy = rateReplyRate.NetCharge, Quantity = 1}}
					           	};
					if (rate.Charges.Sum(c => c.FinalBuy) > rates[rateReply.ServiceType].Charges.Sum(c => c.FinalBuy))
						rates[rateReply.ServiceType] = rate;
				}
			}
			return rates.Values.ToList();
		}

		private static bool IsValid(FedExShipment shipment)
		{
			return !string.IsNullOrEmpty(shipment.PackagingType) && shipment.Items.All(item => !string.IsNullOrEmpty(item.PackageType));
		}
	}
}