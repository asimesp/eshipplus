﻿using System;

namespace LogisticsPlus.Eship.SmallPacks
{
	public class BookingDocument
	{
		public Byte[] Content { get; set; }
		public string Name { get; set; }
	}
}
