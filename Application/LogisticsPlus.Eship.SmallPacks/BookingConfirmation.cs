﻿using System.Collections.Generic;

namespace LogisticsPlus.Eship.SmallPacks
{
	public class BookingConfirmation
	{
		public List<string> Errors { get; set; }
		public List<BookingDocument> Documents { get; set; }

		public BookingConfirmation()
		{
			Errors = new List<string>();
			Documents = new List<BookingDocument>();
		}
	}
}
