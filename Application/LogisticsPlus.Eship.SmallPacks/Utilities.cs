﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticssPlus.FedExPlugin.Core;
using LogisticssPlus.FedExPlugin.RateWebService;

namespace LogisticsPlus.Eship.SmallPacks
{
	internal static class Utilities
	{
		internal static SmallPackagingMap GetPackageType(this IEnumerable<SmallPackagingMap> maps, long packageTypeId, SmallPackageEngine engine)
		{
			var map = maps.FirstOrDefault(m => m.PackageTypeId == packageTypeId && m.SmallPackageEngine == engine);
			return map;
		}


		internal static bool IsInternational(this Shipment shipment)
		{
			return shipment.Origin.CountryId != shipment.Destination.CountryId;
		}


		internal static void SetExportDetails(this FedExShipment fedExShipment)
		{
			fedExShipment.ExportDetailsRequested = false;
			fedExShipment.ExportDetails.B13AFillingOption = B13AFilingOptionType.NOT_REQUIRED.ToString();
			fedExShipment.ExportDetails.ExportComplianceStatement = string.Empty;
			fedExShipment.ExportDetails.PermitNumber = string.Empty;

			fedExShipment.ExportDetails.DestinationControlDetailSpecified = false;
			fedExShipment.ExportDetails.DestinationControlCountries = string.Empty;
			fedExShipment.ExportDetails.DestinationDetailsEndUser = string.Empty;
			fedExShipment.ExportDetails.DestinationControlStatementTypes =
				new List<string>
					{
						DestinationControlStatementType.DEPARTMENT_OF_STATE.ToString(),
						DestinationControlStatementType.DEPARTMENT_OF_COMMERCE.ToString()
					};
		}

		internal static void SetCustomsValueAndPayor(FedExShipment fedExShipment, decimal totalItemsValue, string accountNumber)
		{
			fedExShipment.CustomsPaymentType = "SENDER";
			fedExShipment.CustomsShipmentValue = totalItemsValue;
			fedExShipment.CustomsPayorAccount = accountNumber;
		}
	}
}
