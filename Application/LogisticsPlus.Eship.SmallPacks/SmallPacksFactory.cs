﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Rating;
using LogisticssPlus.FedExPlugin.Services;
using LogisticssPlus.FedExPlugin.ShipWebService;

namespace LogisticsPlus.Eship.SmallPacks
{
	public static class SmallPacksFactory
	{
		public static Dictionary<SmallPackageEngine, Dictionary<string, string>> RetrievePackageTypes()
		{
			var d = new Dictionary<SmallPackageEngine, Dictionary<string, string>>();

			// FedEx package type
			d.Add(SmallPackageEngine.FedEx, RateServiceUtilities.PackagingTypes.ToDictionary(i => i.ValueMember, i => i.DisplayMember));
			
			// Ups package type
			var ups = SmallPacksUtilities.GetAll<SmallPackConstants.UpsPackageType>(false);
			d.Add(SmallPackageEngine.Ups, ups.Keys.Select(k => ups[k]).ToDictionary(i => i, i => i.FormattedString()));

			return d;
		}

		public static Dictionary<string, string>  RetrieveServiceOptions()
		{
			return new Dictionary<string, string>
			       	{
			       		{SmallPackConstants.ResidenceService.ResidenceDelivery, "Residence Delivery"},
			       		{SmallPackConstants.ResidenceService.PremiumResidenceDelivery, "Residence Delivery Premium"},
			       		{SmallPackConstants.Signatures.Adult, "Adult Signature"},
			       		{SmallPackConstants.Signatures.ServiceDefault, "Service Signature Default"},
			       	};	
		}
		
		public static Dictionary<SmallPackageEngine, List<string>> RetrieveServiceTypes()
		{
			var d = new Dictionary<SmallPackageEngine, List<string>>();
			
			// FedEx Setup
			var fedEx = SmallPacksUtilities.GetAll<ServiceType>();
			d.Add(SmallPackageEngine.FedEx, fedEx.Values.OrderBy(t => t).ToList());

			// ups setup
			var ups = SmallPacksUtilities.GetAll<UpsPlugin.UpsServiceType>();
			d.Add(SmallPackageEngine.Ups, ups.Values.OrderBy(t => t).ToList());

			return d;
		}
	}
}
