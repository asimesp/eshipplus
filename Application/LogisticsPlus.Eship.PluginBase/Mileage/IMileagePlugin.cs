﻿using System;

namespace LogisticsPlus.Eship.PluginBase.Mileage
{
	public interface IMileagePlugin : IBase, IDisposable
	{
		/// <summary>
		///		Indicates whether or not plugin is initialized.
		/// </summary>
		bool IsInitialized { get; }

		/// <summary>
		///		Default initializer
		/// </summary>
		/// <returns>True if successful else false </returns>
		bool Initialize();

		/// <summary>
		///		Parameterized initializer.
		/// </summary>
		/// <param name="parameter">Parameter with which to initialize</param>
		/// <returns>True if successful else false</returns>
		bool Initialize<T>(T parameter);

		/// <summary>
		///		Calculate distance from start to end in miles
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <returns>Distance if successful else -1</returns>
		double GetMiles(Point start, Point end);

		/// <summary>
		///		Calculate distance from start to end in miles
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		///<param name="setup">Method internal setup parameter</param>
		///<returns>Distance if successful else -1</returns>
		double GetMiles<T>(Point start, Point end, T setup);

		/// <summary>
		///		Calculate distance from start to end in miles
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <returns>Distance if successful else -1</returns>
		double GetKilometers(Point start, Point end);

		/// <summary>
		///		Calculate distance from start to end in kilometers
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		///<param name="setup">Method internal setup parameter</param>
		///<returns>Distance if successful else -1</returns>
		double GetKilometers<T>(Point start, Point end, T setup);
		
	}
}
