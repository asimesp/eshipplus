﻿using System;

namespace LogisticsPlus.Eship.PluginBase.Mileage
{
    [Serializable]
	public class Point
	{
		public PointType Type { get; set; }

		public double Longitude { get; set; }
		public double Latitude { get; set; }

		public string PostalCode { get; set; }
		public object Country { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string Street { get; set; }
	}
}
