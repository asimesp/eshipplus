﻿namespace LogisticsPlus.Eship.PluginBase
{
	public interface IBase
	{
		/// <summary>
		///		Last Error message from processing if applicable.
		/// </summary>
		string LastErrorMessage { get; set; }
	}
}
