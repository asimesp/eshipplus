﻿namespace LogisticsPlus.Eship.PluginBase
{
	public class PluginBaseConstants
	{
		public const double ErrorMilesValue = -1;

		// 1 mile = 1.60934 kilometers
		public const double RatioMilesToKilometers = 1.60934;


	    public const string MapQuestMilesUnit = "m";
	}
}
