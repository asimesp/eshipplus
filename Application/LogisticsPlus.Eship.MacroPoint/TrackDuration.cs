﻿namespace LogisticsPlus.Eship.MacroPoint
{
	public enum TrackDuration
	{
		// All values are expressed in hours!
		OneHour = 1,
		TwoHours = 2,
		FourHours = 4,
		EightHours = 8,
		TwelveHours = 12,
		EighteenHours = 18,
		OneDay = 24,
		TwoDays = 48,
		ThreeDays = 72,
		FourDays = 96,
		FiveDays = 120,
		SixDays = 144,
		OneWeek = 168
	}
}
