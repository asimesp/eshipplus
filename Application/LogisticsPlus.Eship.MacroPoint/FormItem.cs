﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.MacroPoint
{
    [Serializable]
    [XmlRoot("FormItem")]
    public class FormItem
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public string ItemId { get; set; }
        public string Value { get; set; }

        public FormItem()
        {
            Type = null;
            Name = null;
            ItemId = null;
            Value = null;
        }
    }
}
