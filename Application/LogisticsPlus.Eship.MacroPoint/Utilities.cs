﻿using System;
using System.Linq;
using System.Net;
using System.Web;
using LogisticsPlus.Eship.Core;

namespace LogisticsPlus.Eship.MacroPoint
{
	internal static class Utilities
	{
		internal static HttpWebResponse ToHttpWebResponse(this WebResponse response)
		{
			return (HttpWebResponse) response;
		}

		internal static T ToEnum<T>(this string value)
		{
			return (T)Enum.Parse(typeof(T), value, true);
		}

		internal static double ToDouble(this object value)
		{
			var x = default(double);
			return value == null ? x : Double.TryParse(value.ToString(), out x) ? x : default(double);
		}

		internal static DateTime ToDateTime(this object value)
		{
			var x = DateUtility.SystemEarliestDateTime;

			return value == null
					? x
					: value.ToString().ToArray().All(char.IsDigit)
					   ? value.ToString().IsValidSystemDateTime() ? DateTime.FromOADate(value.ToDouble()) : x
					   : DateTime.TryParse(value.ToString(), out x) ? x : DateUtility.SystemEarliestDateTime;
		}

		internal static decimal ToDecimal(this object value)
		{
			var x = default(decimal);
			return value == null ? x : Decimal.TryParse(value.ToString(), out x) ? x : default(decimal);
		}

		internal static int ToInt(this object value)
		{
			var x = default(int);
			return value == null
					? x
					: value.GetType().IsEnum
						? (int)value
						: Int32.TryParse(value.ToString(), out x) ? x : default(int);
		}

		internal static string GetString(this object value)
		{
			return value == null ? string.Empty : value.ToString();
		}


		internal static void SendMacroPointReponse(this HttpResponse response, string data)
		{
			response.Clear();
			response.ContentType = MacroPointConstants.PlainContentType	;
			response.AddHeader("Content-Type", MacroPointConstants.PlainContentType);
			response.AddHeader("Content-Length", data.Length.ToString());
			response.Write(data);
			HttpContext.Current.ApplicationInstance.CompleteRequest();
		}

		internal static string SpaceJoin(this string[] values)
		{
			return string.Join(" ", values.Where(v => !string.IsNullOrEmpty(v)).ToArray());
		}


        internal static string ParseIdNumber(this string value)
        {
            if (value == null) return string.Empty;
            var indexOfDelimiter = value.IndexOf('[');
            return indexOfDelimiter > 0 ? value.Substring(0, indexOfDelimiter - 1) : value;
        }
	}
}
