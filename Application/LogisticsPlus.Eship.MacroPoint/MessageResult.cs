﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.MacroPoint
{
	[Serializable]
	[XmlRoot("MessageResult", Namespace = "http://macropoint-lite.com/xml/1.0")]
	public class MessageResult
	{
		public List<Error> Errors { get; set; }

		public MessageResult()
		{
			Errors = null;
		}
	}
}
