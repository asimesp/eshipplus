﻿using System;

namespace LogisticsPlus.Eship.MacroPoint
{
	[Serializable]
	public class Notification
	{
		public string PartnerMPID { get; set; }
		public string IDNumber { get; set; }
		public string TrackDurationInHours { get; set; }
		public string TrackIntervalInMinutes { get; set; }
	}
}
