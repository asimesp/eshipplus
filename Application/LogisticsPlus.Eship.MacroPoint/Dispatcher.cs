﻿using System;

namespace LogisticsPlus.Eship.MacroPoint
{
	[Serializable]
	public class Dispatcher
	{
		public string Name { get; set; }
		public string Phone { get; set; }
		public string Email { get; set; }
	}
}
