﻿namespace LogisticsPlus.Eship.MacroPoint
{
	public enum OrderResultStatus
	{
		//DO NOT RE-ORDER
		Success = 0,
		PartialSuccess,
		Failure,
	}
}
