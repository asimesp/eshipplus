﻿namespace LogisticsPlus.Eship.MacroPoint
{
    public enum ErrorCodes
    {

        AllArgumentDomainRuleValidationExceptions = 1000,
        InsufficientFunds = 1001,
        PossibleLandLineOrIncompatibleCellPhone = 1002,
        PhoneOnDoNotContactList = 1003,
        CustomTextIndicatingThatValidationUrlWasUnavailable = 1009,
        OrderIdNotValid = 2000,
        OrderIsCompletedOrExpired = 2001,
        OrderDurationCannotBeLessThanCurrentDuration = 2002,
        OrderMessageFeatureNotProvisioned = 2005,
        OrderMessageUrlEmpty = 2006,
        FeatureNotProvisioned = 3000,
        OrderNotEligibleForLocationUpdateNow = 5000,
        UnexpectedErrorOccurredAdministratorNotified = 9999,
    };
}
