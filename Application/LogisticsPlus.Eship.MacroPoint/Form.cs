﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.MacroPoint
{
    [Serializable]
    [XmlRoot("FormItems")]
    public class Form
    {
        [XmlAttribute("loadId")]
        public string LoadId { get; set; }

        [XmlAttribute("stopId")]
        public string StopId { get; set; }

        [XmlAttribute("stopName")]
        public string StopName { get; set; }

        [XmlAttribute("eventName")]
        public string EventName { get; set; }

        [XmlAttribute("formName")]
        public string FormName { get; set; }

        [XmlAttribute("dateInUtc")]
        public string DateInUtc { get; set; }

        [XmlAttribute("mpOrderId")]
        public string MpOrderId { get; set; }

        [XmlAttribute("mpTrackingRequestId")]
        public string MpTrackingRequestId { get; set; }

        [XmlElement(Type = typeof(FormItem), ElementName = "FormItem")]
        public object[] FormItems { get; set; } 


        public Form()
        {
            LoadId = string.Empty;
            StopId = string.Empty;
            StopName = string.Empty;
            EventName = string.Empty;
            FormName = string.Empty;
            DateInUtc = string.Empty;
            FormItems = null;
        }
    }
}
