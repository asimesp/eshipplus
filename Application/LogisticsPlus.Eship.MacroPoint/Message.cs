﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.MacroPoint
{
	[Serializable]
	[XmlRoot("Message", Namespace = "http://macropoint-lite.com/xml/1.0")]
	public class Message
	{
		public string OrderID { get; set; }
		public string Text { get; set; }

		public Message()
		{
			OrderID = null;
			Text = null;
		}
	}
}
