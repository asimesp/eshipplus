﻿using System;

namespace LogisticsPlus.Eship.MacroPoint
{
	[Serializable]
	public class Carrier
	{
		public string Name { get; set; }
		public Dispatcher Dispatcher { get; set; }
	}
}
