﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.MacroPoint
{
	[Serializable]
	[XmlRoot("OrderResult")]
	public class OrderResult
	{
		/// <summary>
		/// order id 32 digit alphanumeric code
		/// </summary>
		public string OrderID { get; set; }

		/// <summary>
		/// Will be one of OrderResultStatus.[Success | PartialSuccess | Failure]
		/// </summary>
		public string Status { get; set; }

		/// <summary>
		/// If Status is PartialSuccess | Failure, Error contained here
		/// </summary>
		public List<Error> Errors { get; set; }

		public OrderResult()
		{
			OrderID = null;
			Status = null;
			Errors = null;
		}
	}
}
