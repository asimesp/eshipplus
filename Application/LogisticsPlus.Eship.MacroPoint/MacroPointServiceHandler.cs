﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.MacroPoint
{
	public class MacroPointServiceHandler : EntityBase
	{
		public struct Keys
		{
			public const string Code = "Code";
			public const string Event = "Event";
			public const string EventDateTime = "EventDateTime";
			public const string Id = "ID";
			public const string Latitude = "Latitude";
			public const string Longitude = "Longitude";
			public const string Uncertainty = "Uncertainty";
			public const string Street1 = "Street1";
			public const string Street2 = "Street2";
			public const string Neighborhood = "Neighborhood";
			public const string City = "City";
			public const string StateOrProvince = "State";
			public const string PostalCode = "Postal";
			public const string Country = "Country";
			public const string LocationDateTimeUtc = "LocationDateTimeUTC";
			public const string Message = "Message";
			public const string Stop = "Stop";
			public const string MpOrderId = "MPOrderID"; 
		}

	    private const string TimeZone = "ET";

		private const string OrderRecordNotFound = "Order record not found!";
		private const string ProcessingErr = "Processing Error: ";
		private const string CorrespondingShipmentRecordNotFound = "Corresponding shipment record not found!";

	    private const string Ellipses = "...";

	    private const int CallNotesLengthLimit = 490;

		private readonly MacroPointSettings _settings;
		private readonly User _user;

		public List<string> Errors { get; private set; }

		public MacroPointServiceHandler(MacroPointSettings settings, User user)
		{
			if (settings == null)
				throw new ArgumentNullException("settings");
			if (user == null)
				throw new ArgumentNullException("user");

			_settings = settings;
			_user = user;
			Errors = new List<string>();
		}



		private void ProcessWebException(WebException ex)
		{
			var errorResponse = ex.Response.ToHttpWebResponse();
			var responseStream = errorResponse.GetResponseStream();
			if (responseStream != null)
			{
				string xml;
				using (var reader = new StreamReader(responseStream))
					xml = reader.ReadToEnd();
				var result = xml.FromXml<OrderResult>();
				Errors.AddRange(result.Errors.Select(e => string.Format("{0}: {1}", e.Code, e.Message)));
			}
		}

		public MacroPointOrder SubmitOrder(Order order, TrackingPriceBreak priceBreak)
		{
			Errors.Clear();
            var uri = new Uri("https://macropoint-lite.com/api/1.0/orders/createorder");

			// intentionally limiting to one and only one shipment per order
			if (order.Notifications != null && order.Notifications.Count != 1)
			{
				Errors.Add("Order should have one and only one notification");
				return null;
			}

			try
			{
			    var request = WebRequest.Create(uri);
			    request.Method = MacroPointConstants.PostMethod;
			    request.ContentType = MacroPointConstants.XmlContentType;
			    request.Credentials = new NetworkCredential(_settings.UserId, _settings.Password);

			    using (var writer = new StreamWriter(request.GetRequestStream()))
			        writer.Write(order.ToXml(true, true));

			    var response = request.GetResponse();
			    string xml;
			    var responseStream = response.GetResponseStream();
			    if (responseStream == null)
			    {
			        Errors.Add("Error reading response stream!");
			        return null;
			    }
			    using (var reader = new StreamReader(responseStream))
			        xml = reader.ReadToEnd();
			    var result = xml.FromXml<OrderResult>();
			    switch (response.ToHttpWebResponse().StatusCode)
			    {
			        case HttpStatusCode.Created:
			            if (result.Errors.Any()) Errors.AddRange(result.Errors.Select(e => string.Format("{0}: {1}", e.Code, e.Message)));
			            return new MacroPointOrder
			                {
			                    DateCreated = DateTime.Now,
			                    DateStopRequested = DateUtility.SystemEarliestDateTime,
			                    IdNumber = order.Notifications[0].IDNumber.ParseIdNumber(),
			                    Number = order.Number.Value,
			                    NumberType = order.Number.Type,
			                    OrderId = result.OrderID,
			                    StartTrackDateTime = order.TrackStartDateTime.Replace(TimeZone, string.Empty).ToDateTime(),
			                    TenantId = _user.TenantId,
			                    TrackCost = priceBreak.Price.ToDecimal(),
			                    TrackDurationHours = priceBreak.TrackDurationHours.ToInt(),
			                    TrackIntervalMinutes = priceBreak.TrackInternvalMinutes.ToInt(),
			                    TrackingStatusCode = string.Empty,
			                    TrackingStatusDescription = string.Empty,
								EmailCopiesOfUpdatesTo = order.EmailCopiesOfUpdatesTo.GetString(),
								Notes = order.Notes.GetString(),
			                    User = _user
			                };
			        default:
			            Errors.AddRange(result.Errors.Select(e => string.Format("{0}: {1}", e.Code, e.Message)));
			            return null;
			    }

			}
			catch (WebException ex)
			{
                ProcessWebException(ex);
				return null;
			}
			catch (Exception ex)
			{
                Errors.Add(ex.ToString());
				return null;
			}
		}

		public bool StopOrder(MacroPointOrder mpo)
		{
			Errors.Clear();
			var uri = new Uri(string.Format("https://macropoint-lite.com/api/1.0/orders/stoporder/{0}", mpo.OrderId));

			try
			{
				var request = WebRequest.Create(uri);
				request.Method = MacroPointConstants.PostMethod;
				request.ContentType = MacroPointConstants.XmlContentType;
			    request.ContentLength = default(int);
				request.Credentials = new NetworkCredential(_settings.UserId, _settings.Password);
				var response = request.GetResponse();
				var responseStream = response.GetResponseStream();
				if (responseStream == null)
				{
					Errors.Add("Error reading response stream!");
					return false;
				}

				var httpWebResponse = response.ToHttpWebResponse();
				switch (httpWebResponse.StatusCode)
				{
					case HttpStatusCode.OK:
						mpo.DateStopRequested = DateTime.Now;
						return true;
					default:
						Errors.Add(string.Format("Unhandled return status code. [Code: {0}, Desc: {1}]", httpWebResponse.StatusCode,
						                         httpWebResponse.StatusDescription));
						return false;
				}
			}
			catch (WebException ex)
			{
				ProcessWebException(ex);
				return false;
			}
			catch (Exception ex)
			{
				Errors.Add(ex.ToString());
				return false;
			}
		}

        public bool RequestLocationUpdate(MacroPointOrder mpo)
        {
            Errors.Clear();
            var uri = new Uri(string.Format("https://macropoint-lite.com/api/1.0/orders/updatelocation/{0}", mpo.OrderId));

            try
            {
                var request = WebRequest.Create(uri);
                request.Method = MacroPointConstants.PostMethod;
                request.ContentType = MacroPointConstants.XmlContentType;
                request.ContentLength = default(int);
                request.Credentials = new NetworkCredential(_settings.UserId, _settings.Password);
                var response = request.GetResponse();
                var responseStream = response.GetResponseStream();
                if (responseStream == null)
                {
                    Errors.Add("Error reading response stream!");
                    return false;
                }
                string xml;
                using (var reader = new StreamReader(responseStream))
                    xml = reader.ReadToEnd();
                var result = xml.FromXml<UpdateLocationResult>();
                var httpWebResponse = response.ToHttpWebResponse();
                switch (httpWebResponse.StatusCode)
                {
                    case HttpStatusCode.OK:
                        return true;
                    default:
                        Errors.AddRange(result.Errors.Select(e => string.Format("{0}: {1}", e.Code, e.Message)));
                        return false;
                }
            }
            catch (WebException ex)
            {
                var errorResponse = ex.Response.ToHttpWebResponse();
                var responseStream = errorResponse.GetResponseStream();
                if (responseStream != null)
                {
                    string xml;
                    using (var reader = new StreamReader(responseStream))
                        xml = reader.ReadToEnd();
                    var result = xml.FromXml<UpdateLocationResult>();
                    Errors.AddRange(result.Errors.Select(e => string.Format("{0}: {1}", e.Code, e.Message)));
                }
                return false;
            }
            catch (Exception ex)
            {
                Errors.Add(ex.ToString());
                return false;
            }
        }


		public MacroPointOrder UpdateOrderStatus(HttpRequest request, HttpResponse response)
		{
			try
			{
				var order = new MacroPointOrder(request.QueryString[Keys.MpOrderId], _user.TenantId);
				if (order.IsNew)
				{
					response.StatusCode = HttpStatusCode.BadRequest.ToInt();
					response.SendMacroPointReponse(OrderRecordNotFound);
					return null;
				}

                order.TakeSnapShot();
				order.TrackingStatusCode = request.QueryString[Keys.Code];
				order.TrackingStatusDescription = request.QueryString[Keys.Message];

                response.StatusCode = HttpStatusCode.OK.ToInt();
                response.SendMacroPointReponse(OrderResultStatus.Success.ToString());

				return order;
			}
			catch (Exception e)
			{
				response.StatusCode = HttpStatusCode.BadRequest.ToInt();
				response.SendMacroPointReponse(ProcessingErr + e);
				return null;
			}
		}

		public CheckCall ProcessEventUpdate(HttpRequest request, HttpResponse response)
		{
			try
			{
				var order = new MacroPointOrder(request.QueryString[Keys.MpOrderId], _user.TenantId);
				if (order.IsNew)
				{
					response.StatusCode = HttpStatusCode.BadRequest.ToInt();
					response.SendMacroPointReponse(OrderRecordNotFound);
					return null;
				}
				var shipment = order.RetrieveCorrespondingShipment();
				if (shipment == null)
				{
					response.StatusCode = HttpStatusCode.BadRequest.ToInt();
					response.SendMacroPointReponse(CorrespondingShipmentRecordNotFound);
					return null;
				}


				var checkCall = new CheckCall
					{
						CallDate = DateTime.Now,
						CallNotes = request.QueryString[Keys.Stop],
						EdiStatusCode = request.QueryString[Keys.Event],
						EventDate = request.QueryString[Keys.EventDateTime].ToDateTime(),
						Shipment = shipment,
						TenantId = _user.TenantId,
						User = _user,
					};

				response.StatusCode = HttpStatusCode.OK.ToInt();
				response.SendMacroPointReponse(OrderResultStatus.Success.ToString());

				return checkCall;
			}
			catch (Exception e)
			{
				response.StatusCode = HttpStatusCode.BadRequest.ToInt();
				response.SendMacroPointReponse(ProcessingErr + e);
				return null;
			}
		}

		public List<CheckCall> ProcessLocationNotification(HttpRequest request, HttpResponse response)
		{
            try
            {
                var order = new MacroPointOrder(request.QueryString[Keys.MpOrderId], _user.TenantId);
                if (order.IsNew)
                {
                    response.StatusCode = HttpStatusCode.BadRequest.ToInt();
					response.SendMacroPointReponse(OrderRecordNotFound);
                    return null;
                }
                var shipment = order.RetrieveCorrespondingShipment();
                if (shipment == null)
                {
                    response.StatusCode = HttpStatusCode.BadRequest.ToInt();
					response.SendMacroPointReponse(CorrespondingShipmentRecordNotFound);
                    return null;
                }

                var callNotes = "Location Notification -";

				if (!string.IsNullOrEmpty(request.QueryString[Keys.Latitude]) || !string.IsNullOrEmpty(request.QueryString[Keys.Longitude]))
					callNotes += string.Format(" Point: {0}, {1};", request.QueryString[Keys.Latitude], request.QueryString[Keys.Longitude]);

				if (!string.IsNullOrEmpty(request.QueryString[Keys.Uncertainty]))
					callNotes += string.Format(" Uncertainty: {0};", request.QueryString[Keys.Uncertainty]);

	            var address = new[]
		            {
			            request.QueryString[Keys.Street1],
			            request.QueryString[Keys.Street2],
			            request.QueryString[Keys.Neighborhood],
			            request.QueryString[Keys.City],
			            request.QueryString[Keys.StateOrProvince],
			            request.QueryString[Keys.PostalCode],
			            request.QueryString[Keys.Country]
		            }.SpaceJoin();

                callNotes += string.Format(" Address: {0};", address);

                var callNotesSpillover = string.Empty;

                if (callNotes.Length >= CallNotesLengthLimit)
                {
                    var lastSpaceIndex = callNotes.Substring(0, CallNotesLengthLimit).LastIndexOf(" ", StringComparison.Ordinal);
                    callNotesSpillover = Ellipses + callNotes.Substring(lastSpaceIndex, callNotes.Length - lastSpaceIndex);
                    callNotes = callNotes.Substring(0, lastSpaceIndex) + Ellipses;
                }

	            var checkCall1 = new CheckCall
		            {
			            CallDate = DateTime.Now,
			            CallNotes = callNotes,
			            EdiStatusCode = string.Empty,
						EventDate = request.QueryString[Keys.LocationDateTimeUtc].ToDateTime(),
			            Shipment = shipment,
			            TenantId = _user.TenantId,
			            User = _user,
		            };

                CheckCall checkCall2 = null;
                if (!string.IsNullOrEmpty(callNotesSpillover))
                {
                    checkCall2 = new CheckCall
                    {
                        CallDate = DateTime.Now,
                        CallNotes = callNotesSpillover,
						EventDate = request.QueryString[Keys.LocationDateTimeUtc].ToDateTime(),
                        EdiStatusCode = string.Empty,
                        Shipment = shipment,
                        TenantId = _user.TenantId,
                        User = _user
                    };
                }

                response.StatusCode = HttpStatusCode.OK.ToInt();
				response.SendMacroPointReponse(OrderResultStatus.Success.ToString());

                var checkCalls = new List<CheckCall> {checkCall1};
	            if(checkCall2 != null) checkCalls.Add(checkCall2);
                return checkCalls;
            }
            catch (Exception e)
            {
                response.StatusCode = HttpStatusCode.BadRequest.ToInt();
				response.SendMacroPointReponse(ProcessingErr + e);
                return null;
            }
		}

	    public Form ProcessForm(HttpRequest request, HttpResponse response)
	    {
            try
            {
                string xml;
			    using (var reader = new StreamReader(request.InputStream))
			        xml = reader.ReadToEnd();
			    var form = xml.FromXml<Form>();

                if (form == null)
                {
                    response.StatusCode = HttpStatusCode.BadRequest.ToInt();
                    response.SendMacroPointReponse(ProcessingErr);
                    return null;
                }

                response.StatusCode = HttpStatusCode.OK.ToInt();
                response.SendMacroPointReponse(OrderResultStatus.Success.ToString());

                return form;
            }
            catch (Exception e)
            {
                response.StatusCode = HttpStatusCode.BadRequest.ToInt();
                response.SendMacroPointReponse(ProcessingErr + e);
                return null;
            }
	    }
	}
}