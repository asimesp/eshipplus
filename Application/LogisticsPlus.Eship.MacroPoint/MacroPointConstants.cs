﻿namespace LogisticsPlus.Eship.MacroPoint
{
	internal class MacroPointConstants
	{
		public const string PostMethod = "POST";
		public const string XmlContentType = "text/xml";
		public const string PlainContentType = "text/plain";
	}
}
