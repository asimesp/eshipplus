﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.UI;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.MacroPoint
{
	public abstract class MacroPointServiceProxyPageBase : Page
	{
		private const string LocStatus = "LC";
		private const string StatusChange = "SC";
		private const string EventUpdate = "EU";

		public abstract MacroPointSettings GetSettings(Tenant tenant);
		public abstract Tenant GetTenant(string idNumber, string orderId);
		public abstract void SaveMacroPointOrder(MacroPointOrder order);
		public abstract void SaveShipmentCheckCalls(List<CheckCall> checkCalls);
	    public abstract void ProcessMacroPointForm(Form form);

		protected override void OnLoad(EventArgs e)
		{
            // if no query string, assume form
            if (Request.QueryString.Count == 0)
            {
                var form = new MacroPointServiceHandler(new MacroPointSettings(), new User()).ProcessForm(Request, Response);
                if (form == null) return;
                form.LoadId = form.LoadId.ParseIdNumber();
                ProcessMacroPointForm(form);
                return;
            }

			var idNumber = Request.QueryString[MacroPointServiceHandler.Keys.Id].GetString().ParseIdNumber();
			var orderId = Request.QueryString[MacroPointServiceHandler.Keys.MpOrderId].GetString();

		    var tenant = GetTenant(idNumber, orderId);
			if (tenant == null)
			{
				Response.StatusCode = HttpStatusCode.BadRequest.ToInt();
				Response.SendMacroPointReponse("Internal error. Tenant missing!");
				return;
			}

			var settings = GetSettings(tenant);
			if (settings == null)
			{
				Response.StatusCode = HttpStatusCode.BadRequest.ToInt();
				Response.SendMacroPointReponse("Unable to find integration variables");
				return;
			}

			var service = new MacroPointServiceHandler(settings, tenant.DefaultSystemUser);

			switch (GetFunctionCode())
			{
				case LocStatus:
					ProcessNotifyOnLocation(service);
					break;
				case StatusChange:
					ProcessStatusChange(service);
					break;
				case EventUpdate:
					ProcessEventUpdate(service);
					break;
				default:
					Response.StatusCode = HttpStatusCode.BadRequest.ToInt();
					Response.SendMacroPointReponse("Unknown operations code");
					break;
			}

			base.OnLoad(e);
		}

	    private string GetFunctionCode()
		{
			if (Request.QueryString.AllKeys.Contains(MacroPointServiceHandler.Keys.Event))
				return EventUpdate;
			if (Request.QueryString.AllKeys.Contains(MacroPointServiceHandler.Keys.Code))
				return StatusChange;
			if (Request.QueryString.AllKeys.Contains(MacroPointServiceHandler.Keys.LocationDateTimeUtc))
				return LocStatus;
			return string.Empty;
		}

		private void ProcessEventUpdate(MacroPointServiceHandler service)
		{
			var cc = service.ProcessEventUpdate(Request, Response);
			if (cc == null) return;
			SaveShipmentCheckCalls(new List<CheckCall> {cc});
		}

		private void ProcessStatusChange(MacroPointServiceHandler service)
		{
			var order = service.UpdateOrderStatus(Request, Response);
			if (order == null) return;
			SaveMacroPointOrder(order);
		}

		private void ProcessNotifyOnLocation(MacroPointServiceHandler service)
		{
			var ccs = service.ProcessLocationNotification(Request, Response);
			if (ccs == null) return;
			SaveShipmentCheckCalls(ccs);
		}
	}
}
