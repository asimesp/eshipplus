﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.MacroPoint
{
    [Serializable]
    [XmlRoot("UpdateLocationResult")]
    public class UpdateLocationResult
    {
        /// <summary>
        /// If the UpdateLocation failed for any reason, Error contained here
        /// </summary>
        public List<Error> Errors { get; set; }

        
        public UpdateLocationResult()
        {
            Errors = null;
        }
    }
}
