﻿using System;

namespace LogisticsPlus.Eship.MacroPoint
{
	[Serializable]
	public class Stop
	{
		public string StopID { get; set; }
		public string Name { get; set; }
		public string StopType { get; set; }
		public Address Address { get; set; }
		public string Latitude { get; set; }
		public string Longitude { get; set; }
		public string StartDateTime { get; set; }
		public string EndDateTime { get; set; }
	}
}
