﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.MacroPoint
{
	[Serializable]
	[XmlRoot("Order", Namespace = "http://macropoint-lite.com/xml/1.0")]
	public class Order
	{
		public Number Number { get; set; }
		public string TrackStartDateTime { get; set; }
		public List<Notification> Notifications { get; set; }
		public Carrier Carrier { get; set; }
		public TripSheet TripSheet { get; set; }
		public string EmailCopiesOfUpdatesTo { get; set; }
		public string Notes { get; set; }

		public Order()
		{
			Number = null;
			TrackStartDateTime = null;
			Notifications = null;
			Carrier = null;
			TripSheet = null;
			EmailCopiesOfUpdatesTo = null;
			Notes = null;
		}
	}
}