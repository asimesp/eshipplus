﻿namespace LogisticsPlus.Eship.MacroPoint
{
    public enum OrderTrackingStatusCodes
    {

        RequestingInstallation = 1001,
        ReadyToTrack = 1020,
        TrackingNow = 1021,
        TrackingCompletedSuccessfully = 1022,
        StoppedByCreator = 1050,
        StoppedEarlyByDriver = 1051,
        DeniedByDriver = 1052,
        LocationHiddenByDriver = 1053,
        TrackingWaitingForUpdate = 1060,
        IncompatiblePhone = 1061,
        InvalidNumberNotTrackable = 1062,
        LandlineNotTrackable = 1063,
        DriverRefusedInstallation = 1064,
        ExpiredWithoutInstallation = 1070,
        ExpiredWithoutLocation = 1071,
        TripSheetDeployed = 1110,
        TripSheetInProgress = 1111,
        TripSheetCompleted = 1112,
        TripSheetExpired = 1113,
    }
}
