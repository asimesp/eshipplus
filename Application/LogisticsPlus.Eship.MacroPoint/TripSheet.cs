﻿using System;
using System.Collections.Generic;

namespace LogisticsPlus.Eship.MacroPoint
{
	[Serializable]
	public class TripSheet
	{
		public List<Stop> Stops { get; set; }
	}
}
