﻿using System;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.MacroPoint
{
	[Serializable]
	public class Number
	{
		[XmlAttribute("Type")]
		public string Type { get; set; }

		[XmlText]
		public string Value { get; set; }
	}
}