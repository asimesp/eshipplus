﻿using System;

namespace LogisticsPlus.Eship.MacroPoint
{
	[Serializable]
	public class Error
	{
		public string Code { get; set; }
		public string Message { get; set; }
	}
}
