﻿namespace LogisticsPlus.Eship.MacroPoint
{
	public enum TrackInterval
	{
		// All Values are in minutes
		FifteenMinutes = 15,
		ThirtyMinutes = 30,
		OneHour = 60,
		TwoHours = 120,
		FourHours = 240,
		EightHours = 480,
		TwelveHours = 720,
	}
}
