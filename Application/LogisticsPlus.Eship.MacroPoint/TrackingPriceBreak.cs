﻿using System;
using LogisticsPlus.Eship.Core;

namespace LogisticsPlus.Eship.MacroPoint
{
	[Serializable]
	public class TrackingPriceBreak
	{
		public string TrackInternvalMinutes { get; set; }
		public string TrackDurationHours { get; set; }
		public string Price { get; set; }
		public string Category { get; set; }

		public string ListKey
		{
			get
			{
				return string.Format("{0}: every {1} for {2} [{3:c2}]",
				                     Category, TrackInternvalMinutes.ToEnum<TrackInterval>().FormattedString().ToLower(),
				                     TrackDurationHours.ToEnum<TrackDuration>().FormattedString().ToLower(), Price.ToDecimal())
									 .Replace("one", "1")
									 .Replace("two", "2")
									 .Replace("three", "3")
									 .Replace("four", "4")
									 .Replace("five", "5")
									 .Replace("six", "6")
									 .Replace("seven", "7");
			}
		}
	}
}
