﻿using System;
using System.Collections.Generic;

namespace LogisticsPlus.Eship.MacroPoint
{
	[Serializable]
	public class MacroPointSettings
	{
		public string Key { get; set; }
		public string UserId { get; set; }
		public string Password { get; set; }
		public string PingCost { get; set; }
		public List<TrackingPriceBreak> PriceBreaks { get; set; }

		public MacroPointSettings()
		{
			Key = null;
			UserId = null;
			Password = null;
			PriceBreaks = null;
		}
	}
}
