﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.MapQuestMilesPlugin
{
	internal static class Utilities
	{
        /// <summary>
        /// Serializes object to xml string.
        /// </summary>
        /// <typeparam name="T">T must be a class</typeparam>
        /// <param name="value">object to serialize</param>
        /// <param name="removeDefaultNamespaces">remove default namespaces from xml document generation</param>
        /// <param name="removeEncoding">remove version and encoding xml line</param>
        /// <returns>xml stream of object</returns>
        public static string ToXml<T>(this T value, bool removeDefaultNamespaces = false, bool removeEncoding = false) where T : class
        {
            string xml;

            using (var stream = new MemoryStream())
            using (var writer = new StreamWriter(stream, Encoding.UTF8))
            {
                var serializer = new XmlSerializer(typeof(T));
                serializer.Serialize(writer, value);
                xml = new UTF8Encoding().GetString(stream.ToArray());
            }

            if (removeDefaultNamespaces)
                xml = xml.RemoveDefaultNamespace();
            if (removeEncoding)
                xml = xml.RemoveEncoding();

            return xml;
        }

		/// <summary>
		/// Deserializes a string file to an object
		/// </summary>
		/// <typeparam name="T">T must be a class</typeparam>
		/// <param name="xml">XML to deserialize</param>
		/// <returns>object T instantiated if successful else null</returns>
		public static T FromXml<T>(this string xml) where T : class
		{
			T item;
			try
			{
				using (var stream = new MemoryStream(new UTF8Encoding().GetBytes(xml)))
				{
					var serializer = new XmlSerializer(typeof(T));
					item = serializer.Deserialize(stream) as T;
				}
			}
			catch
			{
				item = null;
			}
			return item;
		}


		public static double GetMiles(this MapQuestMiles miles, string start, string end)
		{
		    var routeRequest = new Route
		        {
		            AllToAll = false,
		            Locations = new List<string> {start, end},
                    Unit = PluginBase.PluginBaseConstants.MapQuestMilesUnit
		        };

            var uri = new Uri(string.Format("http://open.mapquestapi.com/directions/v2/routematrix?key={0}&outFormat=xml&inFormat=xml", "Fmjtd%7Cluu82q0rn9%2C2x%3Do5-94yxhw"));

            try
			{
				var request = WebRequest.Create(uri);
				request.Method = "POST";
			    request.ContentType = "text/xml";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                    writer.Write(routeRequest.ToXml(true, true));

				var response = request.GetResponse();
				string xml;
				var responseStream = response.GetResponseStream();
				if (responseStream == null)
				{
					miles.LastErrorMessage = "Error reading response stream!";
					return PluginBase.PluginBaseConstants.ErrorMilesValue;
				}
				using (var reader = new StreamReader(responseStream))
					xml = reader.ReadToEnd();
				var result = xml.FromXml<Response>();
				if (result == null) miles.LastErrorMessage = "Error deserializing returned data!";

			    return result != null && !string.IsNullOrEmpty(result.Distance) 
                    ? Convert.ToDouble(result.Distance.Split(new[] { ',' })[1]) 
                    : PluginBase.PluginBaseConstants.ErrorMilesValue;
			}
			catch (Exception ex)
			{
				miles.LastErrorMessage = ex.Message;
				return PluginBase.PluginBaseConstants.ErrorMilesValue;
			}
		}

        public static string StripXmlNamespacesAndEncoding(this string xml)
		{
			if (string.IsNullOrEmpty(xml)) return xml;

			var start = xml.IndexOf("<?xml", StringComparison.Ordinal);
			var end = xml.IndexOf(">", StringComparison.Ordinal);
			if (start == -1 || end == -1) return xml;

			xml = end + 1 >= xml.Length ? string.Empty : xml.Substring(end + 1);

			var ai = xml.IndexOf(">", StringComparison.Ordinal);
			if (ai == -1) return xml;
			var si = xml.IndexOf(" ", StringComparison.Ordinal);
			if (si == -1) return xml;

			xml = xml.Replace(xml.Substring(si, ai - si), string.Empty);

			return xml;
		}

		private static string RemoveEncoding(this string xml)
		{
			if (string.IsNullOrEmpty(xml)) return xml;

			var start = xml.IndexOf("<?xml", StringComparison.Ordinal);
			var end = xml.IndexOf(">", StringComparison.Ordinal);
			if (start == -1 || end == -1) return xml;

			xml = end + 1 >= xml.Length ? string.Empty : xml.Substring(end + 1);

			return xml;
		}

		private static string RemoveDefaultNamespace(this string xml)
		{
			if (string.IsNullOrEmpty(xml)) return xml;

            // xsi
            var start = xml.IndexOf("xmlns:xsi", StringComparison.Ordinal);
            if (start == -1) return xml;
            int end;
            for (end = start; end < xml.Length; end++)
                if (xml[end] == ' ' || xml[end] == '>') break;
            xml = xml.Replace(xml.Substring(start - 1, end - start + 1), string.Empty);

            // xsd
            start = xml.IndexOf("xmlns:xsd", StringComparison.Ordinal);
            if (start == -1) return xml;
            for (end = start; end < xml.Length; end++)
                if (xml[end] == ' ' || xml[end] == '>') break;
            xml = xml.Replace(xml.Substring(start - 1, end - start + 1), string.Empty);

			return xml;
		}
	}


}
