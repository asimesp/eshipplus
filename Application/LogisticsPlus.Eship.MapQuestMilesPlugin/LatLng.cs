﻿using System.Xml.Serialization;

namespace LogisticsPlus.Eship.MapQuestMilesPlugin
{
    public class LatLng
    {
        [XmlElement("lat")]
        public string Latitude { get; set; }

        [XmlElement("lng")]
        public string Longitude { get; set; }
    }
}
