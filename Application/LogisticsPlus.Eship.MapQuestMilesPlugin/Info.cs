﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.MapQuestMilesPlugin
{
    public class Info
    {
        [XmlElement("statusCode")]
        public string StatusCode { get; set; }

        [XmlArray("messages")]
        [XmlArrayItem("message")]
        public List<string> Messages { get; set; }

        [XmlElement("copyright")]
        public CopyrightInfo Copyright { get; set; }
    }
}
