﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.MapQuestMilesPlugin
{
    [XmlRoot("response")]
    public class Response
    {
        [XmlElement("info")]
        public Info Info { get; set; }

        [XmlElement("allToAll")]
        public bool AllToAll { get; set; }

        [XmlElement("manyToOne")]
        public bool ManyToOne { get; set; }

        [XmlArray("locations")]
        [XmlArrayItem("location")]
        public List<Location> Locations { get; set; }

        [XmlElement("distance")]
        public string Distance { get; set; }

        [XmlElement("time")]
        public string Time { get; set; }
    }
}
