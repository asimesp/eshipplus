﻿using System.Xml.Serialization;

namespace LogisticsPlus.Eship.MapQuestMilesPlugin
{
    public class Location
    {
        [XmlElement("street")]
        public string Street { get; set; }

        [XmlElement("adminArea5")]
        public string City { get; set; }

        [XmlElement("adminArea3")]
        public string State { get; set; }

        [XmlElement("adminArea4")]
        public string County { get; set; }

        [XmlElement("postalCode")]
        public string PostalCode { get; set; }

        [XmlElement("adminArea1")]
        public string Country { get; set; }

        [XmlElement("geocodeQuality")]
        public string GeoCodeQuality { get; set; }

        [XmlElement("geocodeQualityCode")]
        public string GeoCodeQualityCode { get; set; }

        [XmlElement("dragPoint")]
        public string DragPoint { get; set; }

        [XmlElement("sideOfStreet")]
        public string SideOfStreet { get; set; }

        [XmlElement("displayLatLng")]
        public DisplayLatLng DisplayLatitudeLongitude { get; set; }

        [XmlElement("linkId")]
        public string LinkId { get; set; }

        [XmlElement("type")]
        public string Type { get; set; }

        [XmlElement("latLng")]
        public LatLng Coordinates { get; set; }
    }
}
