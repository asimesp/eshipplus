﻿using System;
using LogisticsPlus.Eship.PluginBase.Mileage;

namespace LogisticsPlus.Eship.MapQuestMilesPlugin
{
	public class MapQuestMiles : IMileagePlugin
    {
		public string LastErrorMessage { get; set; }

		public bool IsInitialized { get; private set; }



		public bool Initialize()
		{
			IsInitialized = true;
			return IsInitialized;
		}

		public bool Initialize<T>(T parameter)
		{
			throw new NotSupportedException("Parameterized initialization is not supported by this implementation.");
		}


        public double GetKilometers(Point start, Point end)
		{
			// 1 mile = 1.60934 kilometers
            var miles = GetMiles(start, end);
            return (int) miles == (int) PluginBase.PluginBaseConstants.ErrorMilesValue
                       ? PluginBase.PluginBaseConstants.ErrorMilesValue
                       : miles*PluginBase.PluginBaseConstants.RatioMilesToKilometers;
		}

		public double GetMiles(Point start, Point end)
		{
			try
			{
				if (!IsInitialized)
				{
					LastErrorMessage = "Engine is not initialized!";
					return PluginBase.PluginBaseConstants.ErrorMilesValue;
				}

				var p1 = string.Format("{0} {1} {2} {3}", start.City, start.State, start.PostalCode, start.Country);
				var p2 = string.Format("{0} {1} {2} {3}", end.City, end.State, end.PostalCode, end.Country);
				var ret = this.GetMiles(p1, p2);
				if ((int)ret == (int)PluginBase.PluginBaseConstants.ErrorMilesValue)
				{
					LastErrorMessage = "Invalid mileage value returned. The mileage engine may be unavailable";
					return ret;
				}
				return ret;
			}
			catch (Exception e)
			{
				LastErrorMessage = e.Message;
				return PluginBase.PluginBaseConstants.ErrorMilesValue;
			}
		}


		public double GetMiles<T>(Point start, Point end, T setup)
		{
			throw new NotSupportedException("Parameterized mileage method is not supported by this implementation.");
		}

		public double GetKilometers<T>(Point start, Point end, T setup)
		{
			throw new NotSupportedException("Parameterized kilometer method is not supported by this implementation.");
		}



		public void Dispose()
		{
		}
    }
}
