﻿using System.Xml.Serialization;

namespace LogisticsPlus.Eship.MapQuestMilesPlugin
{
    public class CopyrightInfo
    {
        [XmlElement("imageUrl")]
        public string ImageUrl { get; set; }

        [XmlElement("imageAltText")]
        public string ImageAltText { get; set; }

        [XmlElement("text")]
        public string Text { get; set; }
    }
}
