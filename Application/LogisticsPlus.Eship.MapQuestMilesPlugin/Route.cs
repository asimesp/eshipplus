﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace LogisticsPlus.Eship.MapQuestMilesPlugin
{
    [XmlRoot("route")]
    public class Route
    {
        [XmlArray("locations")]
        [XmlArrayItem("location")]
        public List<string> Locations { get; set; }

        [XmlElement("unit")]
        public string Unit { get; set; }

        [XmlElement("allToAll")]
        public bool AllToAll { get; set; }
    }
}
