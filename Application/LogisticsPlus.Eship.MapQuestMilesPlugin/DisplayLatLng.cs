﻿using System.Xml.Serialization;

namespace LogisticsPlus.Eship.MapQuestMilesPlugin
{
    public class DisplayLatLng
    {
        [XmlElement("latLng")]
        public LatLng LatitudeLongitudeCoordinates { get; set; }
    }
}
