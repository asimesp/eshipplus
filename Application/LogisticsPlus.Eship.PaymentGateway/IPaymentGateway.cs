﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Accounting;

namespace LogisticsPlus.Eship.PaymentGateway
{
    public interface IPaymentGateway
    {
        string AddCustomerInformation(Customer customer);
        bool DeleteCustomerInformation(Customer customer);

        string AddPaymentProfile(PaymentInformation paymentInfo);
        bool RemovePaymentProfile(string cimId, string paymentProfileId);
        List<PaymentGatewayPaymentProfile> GetPaymentProfiles(string cimId);
        PaymentGatewayPaymentProfile GetExistingPaymentProfile(string cimId, string profileId);
        bool UpdatePaymentProfile(string cimId, PaymentGatewayPaymentProfile profile);

        PaymentGatewayResponse GetTransactionStatus(string transactionId);

        PaymentGatewayResponse AuthorizePayment(string profileId, string paymentProfileId, decimal amount, string description);
        PaymentGatewayResponse AuthorizePayment(PaymentInformation paymentInfo);

        PaymentGatewayResponse CapturePreauthorizedPayment(string profileId, string paymentProfileId, string transactionId, decimal amount);
        PaymentGatewayResponse CapturePreauthorizedPayment(string transactionId, decimal amount);

        PaymentGatewayResponse AuthorizeAndCapture(PaymentInformation paymentInfo);
        PaymentGatewayResponse AuthorizeAndCapture(string profileId, string paymentProfileId, decimal amount, string description);
        PaymentGatewayResponse Refund(string transactionId, decimal amount);
        PaymentGatewayResponse Void(string transactionId);
    }
}
