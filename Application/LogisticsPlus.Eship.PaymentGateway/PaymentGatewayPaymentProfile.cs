﻿namespace LogisticsPlus.Eship.PaymentGateway
{
    public class PaymentGatewayPaymentProfile
    {
        public string PaymentProfileId { get; set; }
        public string CreditCardNumber { get; set; }
        public string ExpirationDateYear { get; set; }
        public string ExpirationDateMonth { get; set; }
        public string SecurityCode { get; set; }
        public string BillingFirstName { get; set; }
        public string BillingLastName { get; set; }
        public string BillingStreet { get; set; }
        public string BillingZip { get; set; }
        public string BillingCity { get; set; }
        public string BillingState { get; set; }
        public string BillingCountry { get; set; }
    }
}
