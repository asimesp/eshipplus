﻿using AuthorizeNet;

namespace LogisticsPlus.Eship.PaymentGateway
{
    internal static class Utilities
    {
        internal static TransactionStatus GetTransactionStatusFromAuthorizeNetTransactionStatus(this string value)
        {
            switch (value)
            {
                case "authorizedPendingCapture":
                    return TransactionStatus.AuthorizedPendingCapture;
                case "capturedPendingSettlement":
                    return TransactionStatus.CapturedPendingSettlement;
                case "communicationError":
                    return TransactionStatus.CommunicationError;
                case "refundSettledSuccessfully":
                    return TransactionStatus.RefundSettledSuccessfully;
                case "refundPendingSettlement":
                    return TransactionStatus.RefundPendingSettlement;
                case "approvedReview":
                    return TransactionStatus.ApprovedReview;
                case "declined":
                    return TransactionStatus.Declined;
                case "couldNotVoid":
                    return TransactionStatus.CouldNotVoid;
                case "expired":
                    return TransactionStatus.Expired;
                case "generalError":
                    return TransactionStatus.GeneralError;
                case "failedReview":
                    return TransactionStatus.FailedReview;
                case "settledSuccessfully":
                    return TransactionStatus.SettledSuccessfully;
                case "settlementError":
                    return TransactionStatus.SettlementError;
                case "underReview":
                    return TransactionStatus.UnderReview;
                case "voided":
                    return TransactionStatus.Voided;
                case "FDSPendingReview":
                    return TransactionStatus.FdsPendingReview;
                case "FDSAuthorizedPendingReview":
                    return TransactionStatus.FdsAuthorizedPendingReview;
                case "returnedItem":
                    return TransactionStatus.ReturnedItem;
                default:
                    return TransactionStatus.NotApplicable;
            }
        }

        internal static PaymentGatewayPaymentProfile GetPaymentGatewayPaymentProfileFromAuthorizeNetPaymentProfile(this PaymentProfile value)
        {
            return new PaymentGatewayPaymentProfile
                {
                    PaymentProfileId = value.ProfileID,
                    BillingStreet = value.BillingAddress.Street,
                    BillingCity = value.BillingAddress.City,
                    BillingFirstName = value.BillingAddress.First,
                    BillingLastName = value.BillingAddress.Last,
                    BillingCountry = value.BillingAddress.Country,
                    BillingState = value.BillingAddress.State,
                    BillingZip = value.BillingAddress.Zip,
                    ExpirationDateYear = !string.IsNullOrEmpty(value.CardExpiration) && value.CardExpiration.Length == 7
                            ? value.CardExpiration.Substring(0, 4)
                            : "XX",
                    ExpirationDateMonth = !string.IsNullOrEmpty(value.CardExpiration) && value.CardExpiration.Length == 7
                            ? value.CardExpiration.Substring(5, 2)
                            : "XX",
                    CreditCardNumber = value.CardNumber,
                    SecurityCode = string.Empty
                };
        }
    }
}
