﻿using System.Collections.Generic;
using System.Linq;
using AuthorizeNet;
using AuthorizeNet.APICore;
using AuthorizeNet.Api.Controllers;
using AuthorizeNet.Api.Controllers.Bases;
using LogisticsPlus.Eship.Core.Accounting;
using ANetApiRequest = AuthorizeNet.Api.Contracts.V1.ANetApiRequest;
using ANetApiResponse = AuthorizeNet.Api.Contracts.V1.ANetApiResponse;
using Customer = AuthorizeNet.Customer;
using Environment = AuthorizeNet.Environment;
using ItemChoiceType = AuthorizeNet.Api.Contracts.V1.ItemChoiceType;
using createTransactionRequest = AuthorizeNet.Api.Contracts.V1.createTransactionRequest;
using creditCardMaskedType = AuthorizeNet.Api.Contracts.V1.creditCardMaskedType;
using creditCardType = AuthorizeNet.Api.Contracts.V1.creditCardType;
using getTransactionDetailsRequest = AuthorizeNet.Api.Contracts.V1.getTransactionDetailsRequest;
using getTransactionDetailsResponse = AuthorizeNet.Api.Contracts.V1.getTransactionDetailsResponse;
using merchantAuthenticationType = AuthorizeNet.Api.Contracts.V1.merchantAuthenticationType;
using messageTypeEnum = AuthorizeNet.Api.Contracts.V1.messageTypeEnum;
using paymentType = AuthorizeNet.Api.Contracts.V1.paymentType;
using transactionRequestType = AuthorizeNet.Api.Contracts.V1.transactionRequestType;
using transactionTypeEnum = AuthorizeNet.Api.Contracts.V1.transactionTypeEnum;

namespace LogisticsPlus.Eship.PaymentGateway.Gateways
{
    public class AuthorizeNetGateway : IPaymentGateway
    {
        private const string BlockedOutExpirationDate = "XXXX";

        private readonly CustomerGateway _customerGateway;
        private readonly Gateway _gateway;

        public AuthorizeNetGateway(PaymentGatewayCredentials credentials, bool inTestMode)
        {
            if (credentials == null) return;

            _customerGateway = new CustomerGateway(credentials.LoginId, credentials.TransactionId, inTestMode ? ServiceMode.Test : ServiceMode.Live);
            _gateway = new Gateway(credentials.LoginId, credentials.TransactionId, inTestMode);

            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = inTestMode ? Environment.SANDBOX : Environment.PRODUCTION;
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType
            {
                name = credentials.LoginId,
                ItemElementName = ItemChoiceType.transactionKey,
                Item = credentials.TransactionId,
            };
        }


        private static getTransactionDetailsResponse GetTransactionDetails(string transactionId)
        {
            var request = new getTransactionDetailsRequest { transId = transactionId };

            var controller = new getTransactionDetailsController(request);
            controller.Execute();

            return controller.GetApiResponse();
        }

        private static creditCardType GetCreditCardFromSuccessfulTransactionDetailResponse(getTransactionDetailsResponse transactionDetailsResponse)
        {
            var cardNumberLastFourDigits = ((creditCardMaskedType)transactionDetailsResponse.transaction.payment.Item).cardNumber;

            if (cardNumberLastFourDigits.Length >= 4)
                cardNumberLastFourDigits = cardNumberLastFourDigits.Substring(cardNumberLastFourDigits.Length - 4, 4);
            return new creditCardType { cardNumber = cardNumberLastFourDigits, expirationDate = BlockedOutExpirationDate };
        }


        public string AddCustomerInformation(Core.Accounting.Customer customer)
        {
            var customerPrimaryContactEmail = ((customer.Locations.FirstOrDefault(l => l.MainBillToLocation) ?? new CustomerLocation()).Contacts.FirstOrDefault(c => c.Primary) ?? new CustomerContact()).Email ?? string.Empty;
            return _customerGateway.CreateCustomer(customerPrimaryContactEmail, customer.Name, customer.CustomerNumber).ProfileID;
        }

        public bool DeleteCustomerInformation(Core.Accounting.Customer customer)
        {
            var authorizeNetCustomer = _customerGateway.GetCustomer(customer.PaymentGatewayKey);

            foreach (var profile in authorizeNetCustomer.PaymentProfiles)
                _customerGateway.DeletePaymentProfile(customer.PaymentGatewayKey, profile.ProfileID);

            return _customerGateway.DeleteCustomer(customer.PaymentGatewayKey);
        }

        public Customer GetCustomer(string profileId)
        {
            return _customerGateway.GetCustomer(profileId);
        }


        public string AddPaymentProfile(PaymentInformation paymentInfo)
        {
            var cleanCardNumber = paymentInfo.CardNumber.Replace(" ", string.Empty);

            var paymentProfile = GetExistingPaymentProfile(paymentInfo.CustomerPaymentGatewayKey, cleanCardNumber);

            if (!string.IsNullOrEmpty(paymentProfile.PaymentProfileId))
                return paymentProfile.PaymentProfileId;

            var address = new Address
            {
                City = paymentInfo.BillingCity,
                Street = paymentInfo.BillingAddress,
                State = paymentInfo.BillingState,
                Zip = paymentInfo.BillingZip,
                First = paymentInfo.BillingFirstName,
                Last = paymentInfo.BillingLastName,
                Country = paymentInfo.BillingCountry,
            };

            return _customerGateway.AddCreditCard(paymentInfo.CustomerPaymentGatewayKey, cleanCardNumber,
                                                  paymentInfo.ExpMonth, paymentInfo.ExpYear,
                                                  paymentInfo.SecurityCode, address);
        }

        public List<PaymentGatewayPaymentProfile> GetPaymentProfiles(string cimId)
        {
            var paymentProfiles = new List<PaymentProfile>();
            var customer = GetCustomer(cimId);

            if (customer != null) paymentProfiles = customer.PaymentProfiles.ToList();

            return paymentProfiles.Select(p => p.GetPaymentGatewayPaymentProfileFromAuthorizeNetPaymentProfile()).ToList();
        }

        public PaymentGatewayPaymentProfile GetExistingPaymentProfile(string cimId, string profileId)
        {
            var customer = GetCustomer(cimId);
            PaymentProfile profile = null;
            if (customer != null)
                profile = customer.PaymentProfiles.FirstOrDefault(p => p.ProfileID == profileId);

            return profile != null ? profile.GetPaymentGatewayPaymentProfileFromAuthorizeNetPaymentProfile() : new PaymentGatewayPaymentProfile();
        }

        public bool RemovePaymentProfile(string cimId, string paymentProfileId)
        {
            return _customerGateway.DeletePaymentProfile(cimId, paymentProfileId);
        }

        public bool UpdatePaymentProfile(string cimId, PaymentGatewayPaymentProfile profile)
        {
            var customer = GetCustomer(cimId);
            var existingProfile = customer.PaymentProfiles.FirstOrDefault(p => p.ProfileID == profile.PaymentProfileId) ?? new PaymentProfile(new customerPaymentProfileMaskedType());
            existingProfile.BillingAddress = new Address
            {
                City = profile.BillingCity,
                Country = profile.BillingCountry,
                First = profile.BillingFirstName,
                Last = profile.BillingLastName,
                State = profile.BillingState,
                Street = profile.BillingStreet,
                Zip = profile.BillingZip
            };

            return _customerGateway.UpdatePaymentProfile(cimId, existingProfile);
        }


        public PaymentGatewayResponse GetTransactionStatus(string transactionId)
        {
            var response = GetTransactionDetails(transactionId);
            if (response != null && response.messages.resultCode == messageTypeEnum.Ok && response.transaction != null)
            {
                return new PaymentGatewayResponse
                {
                    Approved = true,
                    ErrorMessage = string.Empty,
                    TransactionId = transactionId,
                    TransactionStatus = response.transaction.transactionStatus.GetTransactionStatusFromAuthorizeNetTransactionStatus()
                };
            }

            var errorMsgExists = response != null && response.messages.message.Length > 0;
            return new PaymentGatewayResponse
                {
                    Approved = false,
                    ErrorMessage = string.Format("{0} {1}",
                        errorMsgExists ? response.messages.message[0].code : "Transaction response was not returned from payment gateway",
                        errorMsgExists ? response.messages.message[0].text : string.Empty),
                    TransactionId = transactionId,
                    TransactionStatus = TransactionStatus.NotApplicable
                };
        }


        public PaymentGatewayResponse AuthorizePayment(string profileId, string paymentProfileId, decimal amount, string description)
        {
            var response = _customerGateway.Authorize(new Order(profileId, paymentProfileId, string.Empty)
                {
                    Amount = amount,
                    Description = description
                });

            return new PaymentGatewayResponse
            {
                Approved = response.Approved,
                ErrorMessage = response.ResponseCode,
                TransactionId = response.TransactionID,
                TransactionStatus = TransactionStatus.NotApplicable
            };
        }
        
        public PaymentGatewayResponse AuthorizePayment(PaymentInformation paymentInfo)
        {
            var request = new AuthorizationRequest(paymentInfo.CardNumber, string.Format("{0}{1}", paymentInfo.ExpMonth, paymentInfo.ExpYear), paymentInfo.ChargeAmount, paymentInfo.Description, false);
            request.AddCardCode(paymentInfo.SecurityCode);
            request.City = paymentInfo.BillingCity;
            request.Address = paymentInfo.BillingAddress;
            request.State = paymentInfo.BillingState;
            request.Zip = paymentInfo.BillingZip;
            request.Country = paymentInfo.BillingCountry;
            request.CustId = paymentInfo.CustomerNumber;
            request.DuplicateWindow = "10";

            var response = _gateway.Send(request);

            return new PaymentGatewayResponse
            {
                Approved = response.Approved,
                ErrorMessage = response.Message,
                TransactionId = response.TransactionID,
                TransactionStatus = TransactionStatus.NotApplicable
            };
        }


        public PaymentGatewayResponse CapturePreauthorizedPayment(string profileId, string paymentProfileId, string transactionId, decimal amount)
        {
            var response = _customerGateway.PriorAuthCapture(profileId, paymentProfileId, transactionId, amount);

            return new PaymentGatewayResponse
            {
                Approved = response.Approved,
                ErrorMessage = response.ResponseCode,
                TransactionId = response.TransactionID,
                TransactionStatus = TransactionStatus.NotApplicable
            };
        }

        public PaymentGatewayResponse CapturePreauthorizedPayment(string transactionId, decimal amount)
        {
            var request = new PriorAuthCaptureRequest(amount, transactionId) {DuplicateWindow = "10"};
            var response = _gateway.Send(request);

            return new PaymentGatewayResponse
            {
                Approved = response.Approved,
                ErrorMessage = response.Message,
                TransactionId = response.TransactionID,
                TransactionStatus = TransactionStatus.NotApplicable
            };
        }


        public PaymentGatewayResponse AuthorizeAndCapture(PaymentInformation paymentInfo)
        {
            var request = new AuthorizationRequest(paymentInfo.CardNumber, string.Format("{0}{1}", paymentInfo.ExpMonth, paymentInfo.ExpYear), paymentInfo.ChargeAmount, paymentInfo.Description, true);
            request.AddCardCode(paymentInfo.SecurityCode);
            request.City = paymentInfo.BillingCity;
            request.Address = paymentInfo.BillingAddress;
            request.State = paymentInfo.BillingState;
            request.Zip = paymentInfo.BillingZip;
            request.Country = paymentInfo.BillingCountry;
            request.CustId = paymentInfo.CustomerNumber;
            request.DuplicateWindow = "10";

            var response = _gateway.Send(request);

            return new PaymentGatewayResponse
                {
                    Approved = response.Approved,
                    ErrorMessage = response.Message,
                    TransactionId = response.TransactionID,
                    TransactionStatus = TransactionStatus.NotApplicable
                };
        }

        public PaymentGatewayResponse AuthorizeAndCapture(string profileId, string paymentProfileId, decimal amount, string description)
        {
            var response = _customerGateway.AuthorizeAndCapture(new Order(profileId, paymentProfileId, string.Empty)
                {
                    Amount = amount,
                    Description = description,
                });

            return new PaymentGatewayResponse
            {
                Approved = response.Approved,
                ErrorMessage = response.ResponseCode,
                TransactionId = response.TransactionID,
                TransactionStatus = TransactionStatus.NotApplicable
            };
        }


        public PaymentGatewayResponse Refund(string transactionId, decimal amount)
        {
            var transDetailResponse = GetTransactionDetails(transactionId);
            if (transDetailResponse == null || transDetailResponse.messages.resultCode != messageTypeEnum.Ok || transDetailResponse.transaction == null)
            {
                var transDetailErrorMsgExists = transDetailResponse != null && transDetailResponse.messages.message.Length > 0;
                return new PaymentGatewayResponse
                {
                    Approved = false,
                    ErrorMessage = string.Format("{0} {1}",
                        transDetailErrorMsgExists ? transDetailResponse.messages.message[0].code : "Transaction response was not returned from payment gateway",
                        transDetailErrorMsgExists ? transDetailResponse.messages.message[0].text : string.Empty),
                    TransactionId = transactionId,
                    TransactionStatus = TransactionStatus.NotApplicable
                };
            }

            var transactionRequest = new transactionRequestType
            {
                transactionType = transactionTypeEnum.refundTransaction.ToString(),    // refund type
                payment = new paymentType { Item = GetCreditCardFromSuccessfulTransactionDetailResponse(transDetailResponse) },
                amount = amount,
                refTransId = transactionId
            };

            var controller = new createTransactionController(new createTransactionRequest { transactionRequest = transactionRequest });
            controller.Execute();
            var response = controller.GetApiResponse();

            if (response != null && response.messages.resultCode == messageTypeEnum.Ok && response.transactionResponse != null)
            {
                return new PaymentGatewayResponse
                {
                    Approved = true,
                    ErrorMessage = string.Empty,
                    TransactionId = response.transactionResponse.transId,
                    TransactionStatus = TransactionStatus.NotApplicable
                };
            }

            var errorMsgExists = response != null && response.transactionResponse != null && response.transactionResponse.errors.Length > 0;
            return new PaymentGatewayResponse
            {
                Approved = false,
                ErrorMessage = string.Format("{0} {1}",
                    errorMsgExists ? response.transactionResponse.errors[0].errorCode : "Refund response was not returned from payment gateway",
                    errorMsgExists ? response.transactionResponse.errors[0].errorText : string.Empty),
                TransactionId = string.Empty,
                TransactionStatus = TransactionStatus.NotApplicable
            };
        }

        public PaymentGatewayResponse Void(string transactionId)
        {
            var transDetailResponse = GetTransactionDetails(transactionId);
            if (transDetailResponse == null || transDetailResponse.messages.resultCode != messageTypeEnum.Ok || transDetailResponse.transaction == null)
            {
                var transDetailErrorMsgExists = transDetailResponse != null && transDetailResponse.messages.message.Length > 0;
                return new PaymentGatewayResponse
                {
                    Approved = false,
                    ErrorMessage = string.Format("{0} {1}",
                        transDetailErrorMsgExists ? transDetailResponse.messages.message[0].code : "Transaction response was not returned from payment gateway",
                        transDetailErrorMsgExists ? transDetailResponse.messages.message[0].text : string.Empty),
                    TransactionId = transactionId,
                    TransactionStatus = TransactionStatus.NotApplicable
                };
            }

            var transactionRequest = new transactionRequestType
                {
                    transactionType = transactionTypeEnum.voidTransaction.ToString(),    // refund type
                    payment = new paymentType { Item = GetCreditCardFromSuccessfulTransactionDetailResponse(transDetailResponse) },
                    refTransId = transactionId
                };

            var controller = new createTransactionController(new createTransactionRequest { transactionRequest = transactionRequest });
            controller.Execute();
            var response = controller.GetApiResponse();

            if (response != null && response.messages.resultCode == messageTypeEnum.Ok && response.transactionResponse != null)
            {
                return new PaymentGatewayResponse
                {
                    Approved = true,
                    ErrorMessage = string.Empty,
                    TransactionId = response.transactionResponse.transId,
                    TransactionStatus = TransactionStatus.NotApplicable
                };
            }

            var errorMsgExists = response != null && response.transactionResponse != null && response.transactionResponse.errors.Length > 0;
            return new PaymentGatewayResponse
            {
                Approved = false,
                ErrorMessage = string.Format("{0} {1}",
                    errorMsgExists ? response.transactionResponse.errors[0].errorCode : "Void response was not returned from payment gateway",
                    errorMsgExists ? response.transactionResponse.errors[0].errorText : string.Empty),
                TransactionId = string.Empty,
                TransactionStatus = TransactionStatus.NotApplicable
            };
        }
    }
}
