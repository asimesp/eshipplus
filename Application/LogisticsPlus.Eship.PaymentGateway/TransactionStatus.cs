﻿namespace LogisticsPlus.Eship.PaymentGateway
{
    public enum TransactionStatus
    {
        NotApplicable,
        AuthorizedPendingCapture,
        CapturedPendingSettlement,
        CommunicationError,
        RefundSettledSuccessfully,
        RefundPendingSettlement,
        ApprovedReview,
        Declined,
        CouldNotVoid,
        Expired,
        GeneralError,
        FailedReview,
        SettledSuccessfully,
        SettlementError,
        UnderReview,
        Voided,
        FdsPendingReview,
        FdsAuthorizedPendingReview,
        ReturnedItem,
    }
}
