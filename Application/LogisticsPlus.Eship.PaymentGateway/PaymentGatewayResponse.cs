﻿namespace LogisticsPlus.Eship.PaymentGateway
{
    public class PaymentGatewayResponse
    {
        public bool Approved { get; set; }
        public string ErrorMessage { get; set; }
        public string TransactionId { get; set; }
        public TransactionStatus TransactionStatus { get; set; }
    }
}
