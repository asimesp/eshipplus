﻿namespace LogisticsPlus.Eship.PaymentGateway
{
    public class PaymentGatewayCredentials
    {
        public string LoginId { get; set; }
        public string TransactionId { get; set; }
        public string Secret { get; set; }
    }
}
