﻿namespace LogisticsPlus.Eship.PaymentGateway
{
    public class PaymentInformation
    {
        public string NameOnCard { get; set; }
        public string Description { get; set; }
        public string CardNumber { get; set; }
        public int ExpMonth { get; set; }
        public int ExpYear { get; set; }
        public string SecurityCode { get; set; }
        public decimal ChargeAmount { get; set; }
        public string BillingFirstName { get; set; }
        public string BillingLastName { get; set; }
        public string BillingAddress { get; set; }
        public string BillingZip { get; set; }
        public string BillingCity { get; set; }
        public string BillingState { get; set; }
        public string BillingCountry { get; set; }
        public string CustomerPaymentGatewayKey { get; set; }
        public string CustomerPaymentProfileId { get; set; }
        public string CustomerNumber { get; set; }
        public string CheckNumber { get; set; }
    }
}
