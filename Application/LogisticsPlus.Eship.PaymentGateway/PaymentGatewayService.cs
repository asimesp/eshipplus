﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.PaymentGateway.Gateways;

namespace LogisticsPlus.Eship.PaymentGateway
{
    public class PaymentGatewayService
    {
        private const string PaymentGatewayNotInitializedError = "Payment Gateway has not been initialized.";

        private readonly IPaymentGateway _gateway;

        public PaymentGatewayService(PaymentGatewayType type, PaymentGatewayCredentials credentials, bool inTestMode)
        {
            switch (type)
            {
                case PaymentGatewayType.AuthorizeNet:
                    _gateway = new AuthorizeNetGateway(credentials, inTestMode);
                    break;
            }
        }


        public string AddCustomerInformation(Customer customer)
        {
            return _gateway != null ? _gateway.AddCustomerInformation(customer) : string.Empty;
        }

        public bool DeleteCustomerInformation(Customer customer)
        {
            return _gateway != null && _gateway.DeleteCustomerInformation(customer);
        }


        public string AddPaymentProfile(PaymentInformation paymentInfo)
        {
            return _gateway != null ? _gateway.AddPaymentProfile(paymentInfo) : string.Empty;
        }

        public bool RemovePaymentProfile(string cimId, string paymentProfileId)
        {
            return _gateway != null && _gateway.RemovePaymentProfile(cimId, paymentProfileId);
        }

        public PaymentGatewayPaymentProfile GetExistingPaymentProfile(string cimId, string profileId)
        {
            return _gateway != null ? _gateway.GetExistingPaymentProfile(cimId, profileId) : new PaymentGatewayPaymentProfile();
        }

        public List<PaymentGatewayPaymentProfile> GetPaymentProfiles(string cimId)
        {
            return _gateway != null ? _gateway.GetPaymentProfiles(cimId) : new List<PaymentGatewayPaymentProfile>();
        }

        public bool UpdatePaymentProfile(string cimId, PaymentGatewayPaymentProfile profile)
        {
            return _gateway != null && _gateway.UpdatePaymentProfile(cimId, profile);
        }


        public PaymentGatewayResponse GetTransactionStatus(string transactionId)
        {
            return _gateway != null ? _gateway.GetTransactionStatus(transactionId) : new PaymentGatewayResponse { Approved = false, ErrorMessage = PaymentGatewayNotInitializedError };
        }


        public PaymentGatewayResponse AuthorizePayment(string profileId, string paymentProfileId, decimal amount, string description)
        {
            return _gateway != null ? _gateway.AuthorizePayment(profileId, paymentProfileId, amount, description) : new PaymentGatewayResponse { Approved = false, ErrorMessage = PaymentGatewayNotInitializedError };
        }

        public PaymentGatewayResponse AuthorizePayment(PaymentInformation paymentInfo)
        {
            return _gateway != null ? _gateway.AuthorizePayment(paymentInfo) : new PaymentGatewayResponse { Approved = false, ErrorMessage = PaymentGatewayNotInitializedError };
        }


        public PaymentGatewayResponse CapturePreauthorizedPayment(string profileId, string paymentProfileId, string transactionId, decimal amount)
        {
            return _gateway != null ? _gateway.CapturePreauthorizedPayment(profileId, paymentProfileId,transactionId ,amount) : new PaymentGatewayResponse { Approved = false, ErrorMessage = PaymentGatewayNotInitializedError };
        }

        public PaymentGatewayResponse CapturePreauthorizedPayment(string transactionId, decimal amount)
        {
            return _gateway != null ? _gateway.CapturePreauthorizedPayment(transactionId, amount) : new PaymentGatewayResponse { Approved = false, ErrorMessage = PaymentGatewayNotInitializedError };
        }


        public PaymentGatewayResponse AuthorizeAndCapture(PaymentInformation paymentInfo)
        {
            return _gateway != null ? _gateway.AuthorizeAndCapture(paymentInfo) : new PaymentGatewayResponse { Approved = false, ErrorMessage = PaymentGatewayNotInitializedError };
        }

        public PaymentGatewayResponse AuthorizeAndCapture(string profileId, string paymentProfileId, decimal amount, string description)
        {
            return _gateway != null ? _gateway.AuthorizeAndCapture(profileId, paymentProfileId, amount, description) : new PaymentGatewayResponse { Approved = false, ErrorMessage = PaymentGatewayNotInitializedError };
        }

        public PaymentGatewayResponse Refund(string transactionId, decimal amount)
        {
            return _gateway != null ? _gateway.Refund(transactionId, amount) : new PaymentGatewayResponse { Approved = false, ErrorMessage = PaymentGatewayNotInitializedError };
        }

        public PaymentGatewayResponse Void(string transactionId)
        {
            return _gateway != null ? _gateway.Void(transactionId) : new PaymentGatewayResponse { Approved = false, ErrorMessage = PaymentGatewayNotInitializedError };
        }
    }
}
