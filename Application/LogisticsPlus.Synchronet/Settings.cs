﻿using System.Configuration;
using LogisticsPlus.Eship.Processor;
using ObjToSql.Core;

namespace LogisticsPlus.Synchronet
{
	internal class Settings
    {

        public static string DefaultConnectionString
        {
            get { return GetConnectionString("DefaultConnectionString"); }
        }

		public static string DefaultCustomerNumber
		{
			get { return GetSetting("DefaultCustomerNumber"); }
		}

        public static string DefaultTierNumber
        {
            get { return GetSetting("DefaultTierNumber"); }
        }

		public static string DefaultAccountBucketCode
		{
			get { return GetSetting("DefaultAccountBucketCode"); }
		}

		public static string DefaultPrefixCode
		{
			get { return GetSetting("DefaultPrefixCode"); }
		}
		
        public static string DefaultTenantCode
        {
            get { return GetSetting("DefaultTenantCode"); }
        }
		
        public static string DefaultDirectoryForFiles
        {
            get { return GetSetting("DefaultDirectoryForFiles"); }
        }

		public static string DefaultVendorNumber
		{
			get { return GetSetting("DefaultVendorNumber"); }
		}

		public static string EShipSynchronetChargeCodeMapFile
		{
			get { return GetSetting("eShipSynchronetChargeCodeMapFile"); }
		}

        public static string FileSignatureForCustomer
        {
            get { return GetSetting("FileSignatureForCustomer"); }
        }

        public static string FileSignatureForVendor
        {
            get { return GetSetting("FileSignatureForVendor"); }
        }

		public static string FileSignatureForTransactionHeader
		{
			get { return GetSetting("FileSignatureForTransactionHeader"); }
		}

		public static string FileSignatureForTransactionDetail
		{
			get { return GetSetting("FileSignatureForTransactionDetail"); }
		}

        public static DatabaseType DefaultDatabaseType
        {
            get { return DatabaseType.MsSql; }
        }

        public static bool SubstituteParameters
        {
            get { return GetSetting("SubstituteParameters").ToBoolean(); }
        }

        public static int DefaultQueryTimeout
        {
            get { return GetSetting("DefaultQueryTimeout").ToInt(); }
        }

		public static string RecordsToReProcessFile
		{
			get { return GetSetting("RecordsToReProcessFile"); }
		}



		public static string ServiceTicketDocumentPhysicalPath(long tenantId, long ticketId)
		{

			return string.Format("{0}{1}/{2}{3}/", GetSetting("BaseFilePathForDocuments"), tenantId, GetSetting("ServiceTicketFolder"), ticketId);
		}

		public static string ServiceTicketDocumentVirtualPath(long tenantid, long ticketId)
		{
			return string.Format("{0}{1}/{2}{3}/", GetSetting("VirtualPath"), tenantid, GetSetting("ServiceTicketFolder"), ticketId);
		}



        private static string GetConnectionString(string name)
        {
            return ConfigurationManager.ConnectionStrings[name].ConnectionString;
        }

        public static string GetSetting(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

		public static string MailFrom
		{
			get { return GetSetting("MailFrom"); }
		}

		public static string MailTo
		{
			get { return GetSetting("MailTo"); }
		}
    }
}
