﻿using System;
using System.IO;

namespace LogisticsPlus.Synchronet
{
    public class LogWriterAdapater : IDisposable
    {
        private readonly StreamWriter _writer;

        public LogWriterAdapater(string file)
        {
            _writer = new StreamWriter(new FileStream(file, FileMode.Create));
        }


        public void Dispose()
        {
            _writer.Flush();
            _writer.Dispose();
        }

		public void Write(string line, params object[] args)
        {
			_writer.WriteLine(line, args);
        }

		public void WriteTabbedLine(string line, params object[] args)
        {
			_writer.WriteLine("\t{0}", string.Format(line, args));
        }

        public void WriteError(string line, params object[] args)
        {
	        _writer.WriteLine("[ERROR]: {0}", string.Format(line, args));
        }

		public void WriteTabbedError(string line, params object[] args)
		{
			_writer.WriteLine("\t[ERROR]: {0}", string.Format(line, args));
		}
    }
}
