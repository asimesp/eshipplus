﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Cache;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Synchronet.Helpers;
using ObjToSql.Core;

namespace LogisticsPlus.Synchronet
{	
	
    public class Program
    {
        private const int CustomerFile = 0;
        private const int VendorFile = 1;
        private const int HeaderFile = 2;
        private const int DetailFile = 3;

        private const string ExceptionMessage = "Exception: {0}";
		private const string EmailSubjectAndBody = "Synchronet Importer Log";

	    private const string DefaultDocumentTag = "MISC";

        private static void Main()
        {
            // database path and type setup
            DatabaseConnection.DatabaseType = Settings.DefaultDatabaseType;
            DatabaseConnection.DefaultConnectionString = Settings.DefaultConnectionString;
            DatabaseConnection.SubstituteParameters = Settings.SubstituteParameters;
			DatabaseConnection.DefaultCommandTimeout = Settings.DefaultQueryTimeout;

            var startupPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var failedFilesFolder = startupPath + "\\FailedFiles\\";
            var processedFilesFolder = startupPath + "\\ProcessedFiles\\";
            var logFilesFolder = startupPath + "\\Logs\\";

	        var reprocessRecordsFile = startupPath + "\\" + Settings.RecordsToReProcessFile;
			var eShipSynchronetChargeCodeMapFile = startupPath + "\\" + Settings.EShipSynchronetChargeCodeMapFile;

			CoreCache.Initialize();


            if (!Directory.Exists(logFilesFolder)) Directory.CreateDirectory(logFilesFolder);
            if (!Directory.Exists(processedFilesFolder)) Directory.CreateDirectory(processedFilesFolder);
            if (!Directory.Exists(failedFilesFolder)) Directory.CreateDirectory(failedFilesFolder);

			var fileAttachments = new List<Attachment>();  // for email attachments

            var logFilePath = string.Format("{0}{1:yyyyMMdd_hhmmss}_ImportLog.txt", logFilesFolder, DateTime.Now);
            using (var logWriter = new LogWriterAdapater(logFilePath))
            {
                try
                {
                    var files = Directory
                            .GetFiles(Settings.DefaultDirectoryForFiles)
                            .Where(f => f.ToLower().EndsWith(FileExtensions.xlsx.GetString()) | f.ToLower().EndsWith(FileExtensions.txt.GetString()))
                            .ToList();

                    if (!files.Any())
                    {
                        logWriter.Write(string.Empty);
                        logWriter.WriteTabbedLine("No files found for import.");
                    }

					var headers = new List<TransactionHeader>();
					var details = new List<TransactionDetail>();

					if (File.Exists(reprocessRecordsFile))
					{
						var data = reprocessRecordsFile
							.ReadFromFile()
							.FromUtf8Bytes()
							.FromXml<List<TransactionHeader>>();
						logWriter.Write("{0} record(s) to reprocess read!", data.Count);
						headers.AddRange(data);
						MoveFileToFolder(new FileInfo(reprocessRecordsFile), processedFilesFolder); // move file to save for history
					}

                    foreach (var fileInfo in files.Select(cfn => new FileInfo(cfn)))
                    {
                        logWriter.Write(string.Empty);
                        logWriter.WriteTabbedLine(string.Format("Processing File: {0}", fileInfo.Name));

                        var hasFileSigForCustomer = fileInfo.Name.ContainsFileSignature(Settings.FileSignatureForCustomer);
                        var hasFileSigForVendor = fileInfo.Name.ContainsFileSignature(Settings.FileSignatureForVendor);
                        var hasFileSigForTransactionHeader = fileInfo.Name.ContainsFileSignature(Settings.FileSignatureForTransactionHeader);
                        var hasFileSigForTransactionDetail = fileInfo.Name.ContainsFileSignature(Settings.FileSignatureForTransactionDetail);

						fileAttachments.Add(new Attachment(new MemoryStream(fileInfo.FullName.ReadFromFile()), fileInfo.Name)); // add to email attachment

                        if (!(hasFileSigForVendor ^ hasFileSigForCustomer ^ hasFileSigForTransactionHeader ^ hasFileSigForTransactionDetail))
                        {
                            logWriter.WriteTabbedError("Due to ambiguity of file import type, skipping file");
                            MoveFileToFolder(fileInfo, failedFilesFolder);
                            continue;
                        }

                        var type = default(int);

                        if (hasFileSigForCustomer) type = CustomerFile;
                        if (hasFileSigForVendor) type = VendorFile;
                        if (hasFileSigForTransactionHeader) type = HeaderFile;
                        if (hasFileSigForTransactionDetail) type = DetailFile;

                        var lines = fileInfo.GetImportData(true);

                        switch (type)
                        {
                            case CustomerFile:
                                lines.ProcessAndImportCustomerFile(logWriter);
                                break;
                            case VendorFile:
								lines.ProcessAndImportVendorFile(logWriter);
                                break;
							case HeaderFile:
		                        headers.AddRange(lines.Select(l => new TransactionHeader(l)).ToList());
		                        break;
							case DetailFile:
		                        details.AddRange(lines.Select(d => new TransactionDetail(d)).ToList());
		                        break;
                        }

						MoveFileToFolder(fileInfo, processedFilesFolder);
						logWriter.WriteTabbedLine("File Processed Successfully");
                    }

					// process transaction files
	                var tenant = new TenantSearch().FetchTenants(new List<ParameterColumn>()).First(t => t.Code == Settings.DefaultTenantCode);
	                var defaultCustomer = new CustomerSearch().FetchCustomerByNumber(Settings.DefaultCustomerNumber, tenant.Id);
	                var defaultVendor = new VendorSearch().FetchVendorByNumber(Settings.DefaultVendorNumber, tenant.Id);
	                var glCodeChargeCodeMap = new FileInfo(eShipSynchronetChargeCodeMapFile)
		                .GetImportData(true)
		                .Select(l => new
			                {
				                GlCode = l[2] + l[0] + l[1],
				                EShipCode = l[3]
			                })
		                .ToDictionary(i => i.GlCode, i => i.EShipCode);
	                var mgr = new EshipProcessesManager(tenant, glCodeChargeCodeMap, defaultCustomer, defaultVendor);

					// account for orphaned details saved and reloaded (see first option below for saving records for later).
	                var orphanedHeaders = headers.Where(h => h.IsOrphanedHeader).ToList();
	                headers = headers.Where(h => !h.IsOrphanedHeader).ToList();
	                details.AddRange(orphanedHeaders.Where(h => h.Details != null && h.Details.Any()).SelectMany(h => h.Details).ToList());

					
	                headers.GroupHeadersAndDetails(ref details);
	                headers = headers.ProcessForApHeadersWithMultipleInvoiceRefNo();

	                var recordsForLater = new List<TransactionHeader>();
	                
	                if (details.Any())
	                {
		                recordsForLater.Add(new TransactionHeader {Details = details});
	                }
	                if (headers.Any(h => !h.Details.Any()))
	                {
		                recordsForLater.AddRange(headers.Where(h => !h.Details.Any()));
	                }

					if (headers.Any(h => h.Details.Any()))
					{
						foreach (var header in headers.Where(h => h.Details.Any()).ToList())
						{
							var msgs = mgr.ProcessTransactionHeader(header);
							if (msgs.All(m => m.Type != ValidateMessageType.Error)) continue;
							logWriter.WriteError("Error processing transaction ID: [{0}]", header.TransactionId);
							foreach (var msg in msgs) logWriter.WriteTabbedLine(msg.Message);
							recordsForLater.Add(header);
						}
					}

					// serialize bad records
					if (recordsForLater.Any())
					{
						recordsForLater.ToXml().ToUtf8Bytes().WriteToFile(startupPath, Settings.RecordsToReProcessFile, string.Empty);
					}


					// documents
					logWriter.Write("Checking for documents import");
	                var docPath = Path.Combine(Settings.DefaultDirectoryForFiles, "Documents");
	                var docs = Directory.GetFiles(docPath).ToList();
	                logWriter.Write("Found {0} document(s) for import", docs.Count);
					var tags = new DocumentTagSearch().FetchDocumentTags(new List<ParameterColumn>(), tenant.Id);
					foreach (var doc in docs)
					{
						logWriter.WriteTabbedLine("Importing {0} ...", doc);
						var fi = new FileInfo(doc);
						var parts = fi.Name.Split(new[] { '_' });
						var tranId = parts.Length > 0 ? parts[0] : string.Empty;
						var containerNum = parts.Length > 1 ? parts[1] : string.Empty;
						var tag = parts.Length > 2 ? parts[2] : string.Empty;
						var ticket = mgr.FetchServiceTicketByTransactionIdAndContainerNumber(tranId, containerNum);

						if (ticket == null)
						{
							logWriter.WriteTabbedError("Unable to find matching service ticket for transaction id: [{0}] and container number: [{1}]", tranId, containerNum);
							continue;
						}

						var docTag = tags.FirstOrDefault(t => t.Code.ToLower() == tag.ToLower()) ??
									 tags.FirstOrDefault(t => t.Code.ToLower() == DefaultDocumentTag.ToLower());
						if (docTag == null)
						{
							logWriter.WriteTabbedError("Unable to match document tag [{0}] for document.  Default document tag [{1}] also unmatched!", tag, DefaultDocumentTag);
							continue;
						}

						var serviceTicketDocumentVirtualPath = Settings.ServiceTicketDocumentVirtualPath(ticket.TenantId, ticket.Id);
						var newFileName = string.Format("{0}{1}", Guid.NewGuid(), fi.Extension);
						var msgs = mgr.AddDocumentToServiceTicket(ticket, fi, docTag, newFileName,
						                                          string.Format("{0}{1}", serviceTicketDocumentVirtualPath, newFileName),
						                                          fi.Name);
						if (msgs.All(m => m.Type != ValidateMessageType.Error))
						{
							var serviceTicketDocumentPhysicalPath = Settings.ServiceTicketDocumentPhysicalPath(ticket.TenantId, ticket.Id);
							var copyToPath = string.Format("{0}{1}", serviceTicketDocumentPhysicalPath, newFileName);
							try
							{
								if (!Directory.Exists(serviceTicketDocumentPhysicalPath)) Directory.CreateDirectory(serviceTicketDocumentPhysicalPath);
								File.Copy(doc, copyToPath);
								logWriter.WriteTabbedLine("Imported successful: {0}", doc);
								File.Delete(doc);
							}
							catch (Exception ex)
							{
								logWriter.WriteTabbedError("Error copying file [{0}]. Error: {1}", doc, ex.Message);
							}
							
						}
						else
						{
							foreach (var msg in msgs)
								logWriter.WriteTabbedError(msg.Message);
						}
					}
                }
                catch (Exception ex)
                {
                    logWriter.WriteError(string.Format(ExceptionMessage, ex));
                }
            }

			// send files
	        fileAttachments.Add(new Attachment(logFilePath));
			SendLogs(fileAttachments);

        }

        public static void MoveFileToFolder(FileInfo fileToMove, string folderToMoveTo)
        {
            // ensure unique filename when moving file to folder
            var newPath = string.Format("{0}{1}", folderToMoveTo, fileToMove.Name);
            var cnt = 1;
            while (File.Exists(newPath))
                newPath = string.Format("{0}{1}({2}){3}", folderToMoveTo, fileToMove.Name.Replace(fileToMove.Extension, string.Empty), cnt++, fileToMove.Extension);
            File.Move(fileToMove.FullName, newPath);
        }

	    public static void SendLogs(List<Attachment> fileAttachments)
	    {
		    try
		    {
			    var msg = new MailMessage
				    {
					    From = new MailAddress(Settings.MailFrom),
					    Subject = EmailSubjectAndBody,
					    Body = EmailSubjectAndBody
				    };
			    foreach (var attachment in fileAttachments)
				    msg.Attachments.Add(attachment);
				
			    string[] tos = Settings.MailTo.Split(new[] {';'}, StringSplitOptions.RemoveEmptyEntries);
			    foreach (var to in tos) msg.To.Add(new MailAddress(to));
			    new SmtpClient().Send(msg);
		    }
		    catch (Exception)
		    {
		    }
	    }
        
    }
}
