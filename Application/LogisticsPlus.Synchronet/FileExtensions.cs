﻿namespace LogisticsPlus.Synchronet
{
    public enum FileExtensions
    {
        /* Intensionally left lower case to start so ToString() will follow same */
        xlsx = 0,
		csv,
		txt
    }
}
