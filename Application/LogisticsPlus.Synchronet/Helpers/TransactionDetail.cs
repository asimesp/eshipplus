﻿using System;
using LogisticsPlus.Eship.Processor;

namespace LogisticsPlus.Synchronet.Helpers
{
	[Serializable]
	public class TransactionDetail
	{
		private string[] _data;

		public string[] Data { get { return _data; } set { _data = value; } }

		public string TransactionId
		{
			get { return _data[TransDetailConsts.TransactionId]; }
			set { _data[TransDetailConsts.TransactionId] = value; }
		}

		public string ContainerNumber
		{
			get { return _data[TransDetailConsts.ContainerNumber]; }
			set { _data[TransDetailConsts.ContainerNumber] = value; }
		}

		public string SizeType
		{
			get { return _data[TransDetailConsts.SizeType]; }
			set { _data[TransDetailConsts.SizeType] = value; }
		}

		public string GlCode
		{
			get { return _data[TransDetailConsts.GlCode] + _data[TransDetailConsts.Type]  + _data[TransDetailConsts.Description]; }
		}

		public decimal InvoiceAmount
		{
			get { return _data[TransDetailConsts.InvoiceAmount].ToDecimal(); }
			set { _data[TransDetailConsts.InvoiceAmount] = value.GetString(); }
		}

		public string InvoiceRefNo
		{
			get { return _data[TransDetailConsts.InvoiceRefNo]; }
		}

		public string BookingNumber
		{
			get { return _data[TransDetailConsts.BookingNumber]; }
			set { _data[TransDetailConsts.BookingNumber] = value; }
		}


		public TransactionDetail() : this(new string[TransDetailConsts.ColumnCount])
		{
		}

		public TransactionDetail(string[] data)
		{
			_data = data;
		}
	}
}
