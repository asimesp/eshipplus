﻿
namespace LogisticsPlus.Synchronet.Helpers
{
	public class TransDetailConsts
	{
		public const int TransactionId = 0;
		public const int ContainerNumber = 1;
		public const int SizeType = 2;
		public const int Type = 3;
		public const int Description = 4;
		public const int GlCode = 5;
		public const int InvoiceAmount = 6;
		public const int BookingNumber = 7;
		public const int EmptyPickup = 8;
		public const int EmptyReDelivery = 9;
		public const int InvoiceRefNo = 10;
		public const int ReleaseNo = 11;
		public const int OriginCode = 12;
		public const int Origin = 13;
		public const int DestinationCode = 14;
		public const int Destination = 15;
		public const int Commodity = 16;
		public const int Weight = 17;
		public const int Count = 18;
		public const int Shipper = 19;
		public const int NotifyParty = 20;
		public const int UConsignee = 21;
		public const int BeOwner = 22;

		public const int ColumnCount = 22;
	}
}
