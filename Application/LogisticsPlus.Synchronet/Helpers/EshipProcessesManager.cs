﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.Accounting;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.Processor.Views.Operations;
using ObjToSql.Core;
using LogisticsPlus.Eship.Core.Administration;



namespace LogisticsPlus.Synchronet.Helpers
{
	public class EshipProcessesManager : EntityBase
	{
		private const string ExceptionMessage = "Exception: {0}";

		private readonly Tenant _tenant;
		private readonly Customer _defaultCustomer;
		private readonly Vendor _defaultVendor;
		private readonly AccountBucket _accountBucket;
		private readonly User _user;

		private readonly List<ChargeCode> _chargeCodes;
		private readonly Dictionary<string, string> _glCodeChargeCodeMap;
		private readonly AutoNumberProcessor _autoNumberProcessor = new AutoNumberProcessor();

		private readonly CustomerSearch _customerSearch = new CustomerSearch();
		private readonly VendorSearch _vendorSearch = new VendorSearch();
		private readonly JobSearch _jobSearch = new JobSearch();
		private readonly AutoNumberProcessor _autoNumber = new AutoNumberProcessor();

		private readonly ServiceTicketValidator _ticketValidator = new ServiceTicketValidator();
		private readonly InvoiceValidator _invoiceValidator = new InvoiceValidator();
		private readonly VendorBillValidator _billValidator = new VendorBillValidator();
		private readonly VendorValidator _vendorValidator = new VendorValidator();
		private readonly CustomerValidator _customerValidator = new CustomerValidator();
		private readonly JobValidator _jobValidator = new JobValidator();

		readonly Dictionary<string, Customer> _customers = new Dictionary<string, Customer>();
		readonly Dictionary<string, Vendor> _vendors = new Dictionary<string, Vendor>();

		public EshipProcessesManager(Tenant tenant)
		{
			_tenant = tenant;
			_user = tenant.DefaultSystemUser;
		}

		public EshipProcessesManager(Tenant tenant, Dictionary<string, string> glCodeChargeCodeMap, Customer defaultCustomer, Vendor defaultVendor)
		{
			_tenant = tenant;
			_glCodeChargeCodeMap = glCodeChargeCodeMap;
			_defaultCustomer = defaultCustomer;
			_defaultVendor = defaultVendor;
			_accountBucket = new AccountBucketSearch().FetchAccountBucketByCode(Settings.DefaultAccountBucketCode, tenant.Id);
			_chargeCodes = new ChargeCodeSearch().FetchChargeCodes(tenant.Id);
			_user = tenant.DefaultSystemUser;

			AccountingSearchFields.Initialize();
			OperationsSearchFields.Initialize();
		}
		

		public List<ValidationMessage> ProcessTransactionHeader(TransactionHeader header)
		{
			// Validation
			var msgs = new List<ValidationMessage>();

			var billNumbers = header.IsAPTransaction ? header.UniqueInvoiceRefNos() : new List<string>();
			if(billNumbers.Count > 1)
				msgs.Add(ValidationMessage.Error("More than one distinct AP Invoice/Credit number. [{0}]", billNumbers.ToArray().CommaJoin()));

			var unMatchGlCodes = header
				.Details
				.Where(d => !_glCodeChargeCodeMap.ContainsKey(d.GlCode))
				.Select(d => d.GlCode)
				.ToArray();
			if (unMatchGlCodes.Any())
				msgs.Add(ValidationMessage.Error("Unmatched GL Codes: [{0}]", unMatchGlCodes.CommaJoin()));

			var parameters = new List<ParameterColumn> { AccountingSearchFields.CustomField2.ToParameterColumn() };
			parameters[0].Operator = Operator.Contains;
			parameters[0].DefaultValue = header.AccountLabel;
			if (string.IsNullOrEmpty(parameters[0].DefaultValue)) parameters[0].DefaultValue = DateTime.Now.FormattedLongDateAlt(); // hack to ensure we don't pull items with no value in custom field 2
			var custCriteria = new CustomerViewSearchCriteria { Parameters = parameters };

			Customer customer;
			if (header.IsARTransaction)
			{
				customer = _customers.ContainsKey(header.AccountLabel) ? _customers[header.AccountLabel] : null;
				if (customer == null)
				{
					customer = _customerSearch.FetchCustomers(custCriteria, _tenant.Id).FirstOrDefault(c => c.CustomField2.ToLower().Contains(header.AccountLabel.ToLower()));
					if (customer == null)
						msgs.Add(ValidationMessage.Error("Unmatch customer: [Label: [{0}], Name: [{1}]", header.AccountLabel,
														 header.AccountName));
				} else if (!_customers.ContainsKey(customer.CustomField2)) _customers.Add(customer.CustomField2, customer);
			}
			else
				customer = _defaultCustomer;

			CustomerLocation customerLocation = null;
			if (customer != null)
			{
				customerLocation = (customer.Locations.FirstOrDefault(l => l.LocationNumber == header.AddressId)
									?? customer
										   .Locations
										   .FirstOrDefault(l =>
											   {
												   var hash = (l.Street1 + l.Street2 + l.City + l.State + l.PostalCode).ToAlphaNumericOnly();
												   return hash == header.AddressHash;
											   }))
								   ?? customer.Locations.FirstOrDefault(l => l.BillToLocation && l.MainBillToLocation)
								   ?? customer.Locations.FirstOrDefault(l => l.BillToLocation);
			}
			if (header.IsARTransaction && customerLocation == null)
				msgs.Add(ValidationMessage.Error("Unmatch customer location: [Label: [{0}], Name: [{1}]", header.AccountLabel,
														 header.AccountName));


			Vendor vendor;
			if (header.IsAPTransaction)
			{
				vendor = _vendors.ContainsKey(header.AccountLabel) ? _vendors[header.AccountLabel] : null;
				if (vendor == null)
				{
					vendor = _vendorSearch.FetchVendors(parameters, _tenant.Id).FirstOrDefault(v => v.CustomField2.ToLower().Contains(header.AccountLabel.ToLower()));
					if (vendor == null)
						msgs.Add(ValidationMessage.Error("Unmatched vendor: [Label: [{0}], Name: [{1}]", header.AccountLabel,
						                                 header.AccountName));
				} else if (!_vendors.ContainsKey(vendor.CustomField2)) _vendors.Add(vendor.CustomField2, vendor);
			}
			else
				vendor = _defaultVendor;

			VendorLocation vendorLocation = null;
			if (vendor != null)
			{
				vendorLocation = (vendor.Locations.FirstOrDefault(l => l.LocationNumber == header.AddressId)
								  ?? vendor.Locations
										   .FirstOrDefault(l =>
											   {
												   var hash =
													   (l.Street1 + l.Street2 + l.City + l.State + l.PostalCode).ToAlphaNumericOnly();
												   return hash == header.AddressHash;
											   }))
								 ?? vendor.Locations.FirstOrDefault(l => l.RemitToLocation && l.MainRemitToLocation);
			}
			if (header.IsAPTransaction && vendorLocation == null)
				msgs.Add(ValidationMessage.Error("Unmatched vendor location [Label: [{0}], Name: [{1}]", header.AccountLabel,
												 header.AccountName));


			if (msgs.Any(m => m.Type == ValidateMessageType.Error)) return msgs; // break if errors


			// processing
			var tickets = GenerateServiceTickets(header, customer, customerLocation, vendor);

			// check for reseller after generating tickets!!! want tickets to have original customer but invoices will have reseller customer if applicable
			if (customer != null && customer.RatingId != default(long) && customer.Rating.BillReseller)
			{

				var rcl = customer.Rating.ResellerAddition.ResellerCustomerAccount.Locations.FirstOrDefault(l => l.BillToLocation && l.MainBillToLocation) ??
						  customer.Rating.ResellerAddition.ResellerCustomerAccount.Locations.FirstOrDefault(l => l.BillToLocation);
				if (rcl != null)
				{
					customerLocation = rcl;
					customer = rcl.Customer;
				}
			}
			
			// generate invoices
			Invoice arInv = null;
			Invoice arCrd = null;
			VendorBill apInv = null;
			VendorBill apCrd = null;
			
			GenerateInvoiceRecords(header, customerLocation, customer, tickets, vendorLocation, ref arInv, ref arCrd, ref apInv, ref apCrd);

			// validate and save

            // Generate jobs first

			List<Job> jobs;
		    try
		    {
			    var validationMsgs = SetExistingJobsAndCreateNewJobsWhereApplicable(ref tickets, out jobs);		    
                if (validationMsgs.Any())
                {
                    msgs.AddRange(validationMsgs);
                    return msgs;
                }
		    }
		    catch (Exception ex)
		    {
                msgs.Add(ValidationMessage.Error(ex.ToString()));
		        return msgs;
		    }

			Connection = DatabaseConnection.DefaultConnection;
			BeginTransaction();
			try
			{
				_billValidator.Connection = Connection;
				_billValidator.Transaction = Transaction;

				_invoiceValidator.Connection = Connection;
				_invoiceValidator.Transaction = Transaction;

				_ticketValidator.Connection = Connection;
				_ticketValidator.Transaction = Transaction;

				_jobValidator.Connection = Connection;
				_jobValidator.Transaction = Transaction;

				// save jobs first
				foreach (var job in jobs)
				{
					if (!_jobValidator.IsValid(job))
					{
						RollBackTransaction();
						msgs.AddRange(_jobValidator.Messages);
						return msgs;
					}

					job.Save();
					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} Job: {1}", AuditLogConstants.AddedNew, job.JobNumber),
						TenantId = _tenant.Id,
						User = _tenant.DefaultSystemUser,
						EntityCode = job.EntityName(),
						EntityId = job.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

					if (tickets.ContainsKey(job.ExternalReference1))
						tickets[job.ExternalReference1].Job = job;
				}

				// service tickets
				foreach (var ticket in tickets.Values)
				{
					if (!_ticketValidator.IsValid(ticket))
					{
						RollBackTransaction();
						msgs.AddRange(_ticketValidator.Messages);
						return msgs;
					}
					if (ticket.Job == null)
					{
						RollBackTransaction();
						msgs.Add(new ValidationMessage
							{
								Message = "Service Ticket cannot be saved without an associated Job",
								Type = ValidateMessageType.Error
							});
						return msgs;
					}
					
					// save
					ticket.Connection = Connection;
					ticket.Transaction = Transaction;
					ticket.Save();
					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} Service Ticket: {1}", AuditLogConstants.AddedNew, ticket.ServiceTicketNumber),
						TenantId = _tenant.Id,
						User = _tenant.DefaultSystemUser,
						EntityCode = ticket.EntityName(),
						EntityId = ticket.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
					ProcessServiceTicketCharges(ticket);
					ProcessServiceTicketItems(ticket);
					ProcessServiceTicketVendors(ticket);
				}


				// vendor bills
				if (apInv != null)
				{
					// validate invoice
					_billValidator.Messages.Clear();
					if (!_billValidator.IsValid(apInv))
					{
						RollBackTransaction();
						msgs.AddRange(_billValidator.Messages);
						return msgs;
					}

					// save
					apInv.Connection = Connection;
					apInv.Transaction = Transaction;
					apInv.Save();
					new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} Vendor Bill: {1}", AuditLogConstants.AddedNew, apInv.DocumentNumber),
							TenantId = _tenant.Id,
							User = _tenant.DefaultSystemUser,
							EntityCode = apInv.EntityName(),
							EntityId = apInv.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();
					ProcessVendorBillDetails(apInv);
				}

				// ap credit
				if (apCrd != null)
				{
					// validate invoice
					_billValidator.Messages.Clear();
					if (!_billValidator.IsValid(apCrd))
					{
						RollBackTransaction();
						msgs.AddRange(_billValidator.Messages);
						return msgs;
					}

					// save
					apCrd.Connection = Connection;
					apCrd.Transaction = Transaction;
					apCrd.Save();
					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} Vendor Bill: {1}", AuditLogConstants.AddedNew, apCrd.DocumentNumber),
						TenantId = _tenant.Id,
						User = _tenant.DefaultSystemUser,
						EntityCode = apCrd.EntityName(),
						EntityId = apCrd.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
					ProcessVendorBillDetails(apCrd);
				}


				// ar invoice
				if (arInv != null)
				{
					// validate invoice
					_invoiceValidator.Messages.Clear();
					if (!_invoiceValidator.IsValid(arInv))
					{
						RollBackTransaction();
						msgs.AddRange(_invoiceValidator.Messages);
						return msgs;
					}

					// save
					arInv.Connection = Connection;
					arInv.Transaction = Transaction;
					arInv.Save();
					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} Invoice: {1}", AuditLogConstants.AddedNew, arInv.InvoiceNumber),
						TenantId = _tenant.Id,
						User = _tenant.DefaultSystemUser,
						EntityCode = arInv.EntityName(),
						EntityId = arInv.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
					ProcessInvoiceDetails(arInv);
				}

				// ar credit
				if (arCrd != null)
				{
					// validate invoice
					_invoiceValidator.Messages.Clear();
					if (!_invoiceValidator.IsValid(arCrd))
					{
						RollBackTransaction();
						msgs.AddRange(_invoiceValidator.Messages);
						return msgs;
					}

					// save
					arCrd.Connection = Connection;
					arCrd.Transaction = Transaction;
					arCrd.Save();
					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} Invoice: {1}", AuditLogConstants.AddedNew, arCrd.InvoiceNumber),
						TenantId = _tenant.Id,
						User = _tenant.DefaultSystemUser,
						EntityCode = arCrd.EntityName(),
						EntityId = arCrd.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
					ProcessInvoiceDetails(arCrd);
				}


				CommitTransaction();
			}
			catch (Exception ex)
			{
				RollBackTransaction();
				msgs.Add(ValidationMessage.Error(ex.ToString()));
			}


			return msgs;
		}



		private void GenerateInvoiceRecords(TransactionHeader header, CustomerLocation customerLocation, Customer customer,
											Dictionary<string, ServiceTicket> tickets, VendorLocation vendorLocation,
											ref Invoice arInv, ref Invoice arCrd, ref VendorBill apInv, ref VendorBill apCrd)
		{
			const string casSuffix = "-CAS";
			const string inv = "-Inv";
			switch (header.Type)
			{
				case TransHeaderConsts.TypeARCredit:
					arInv = new Invoice
						{
							CustomerControlAccountNumber = string.Empty,
							CustomerLocation = customerLocation,
							Details = tickets
								.Values
								.SelectMany(t => t.Charges
												  .Select(c => new InvoiceDetail
													  {
														  AccountBucket = t.AccountBucket,
														  ChargeCode = c.ChargeCode,
														  Comment = c.Comment,
														  Invoice = null,
														  Quantity = c.Quantity,
														  ReferenceNumber = t.ServiceTicketNumber,
														  ReferenceType = DetailReferenceType.ServiceTicket,
														  Tenant = _tenant,
														  UnitDiscount = 0m,
														  UnitSell = 0m,
													  }))
								.ToList(),
							DueDate = header.DueDate,
							ExportDate = DateUtility.SystemEarliestDateTime,
							Exported = false,
							FtpDelivered = false,
							HidePrefix = customer != null && customer.HidePrefix,
							InvoiceDate = header.TransactionDate,
							InvoiceNumber = header.TransactionId + inv + casSuffix,
							InvoiceType = InvoiceType.Invoice,
							OriginalInvoice = null,
							PaidAmount = 0m,
							PostDate = DateTime.Now, // auto posting
							Posted = true,
							PrefixId = customer == null ? default(long) : customer.PrefixId,
							SpecialInstruction = string.Empty,
							Tenant = _tenant,
							User = _tenant.DefaultSystemUser,
						};
					arInv.AppendNumberSuffix();

					arCrd = new Invoice
						{
							CustomerControlAccountNumber = string.Empty,
							CustomerLocation = customerLocation,
							Details = tickets
								.Values
								.SelectMany(t => t.Charges
												  .Select(c => new InvoiceDetail
													  {
														  AccountBucket = t.AccountBucket,
														  ChargeCode = c.ChargeCode,
														  Comment = c.Comment,
														  Invoice = null,
														  Quantity = c.Quantity,
														  ReferenceNumber = t.ServiceTicketNumber,
														  ReferenceType = DetailReferenceType.ServiceTicket,
														  Tenant = _tenant,
														  UnitDiscount = c.UnitDiscount,
														  UnitSell = c.UnitSell,
													  }))
								.ToList(),
							DueDate = header.DueDate,
							ExportDate = DateUtility.SystemEarliestDateTime,
							Exported = false,
							FtpDelivered = false,
							HidePrefix = customer != null && customer.HidePrefix,
							InvoiceDate = header.TransactionDate,
							InvoiceNumber = header.TransactionId + casSuffix,
							InvoiceType = InvoiceType.Credit,
							OriginalInvoice = arInv,
							PaidAmount = 0m,
							PostDate = DateTime.Now, // auto posting
							Posted = true,
							PrefixId = customer == null ? default(long) : customer.PrefixId,
							SpecialInstruction = string.Empty,
							Tenant = _tenant,
							User = _tenant.DefaultSystemUser,
						};
					arCrd.AppendNumberSuffix();

					break;
				case TransHeaderConsts.TypeARInvoice:
					arInv = new Invoice
						{
							CustomerControlAccountNumber = string.Empty,
							CustomerLocation = customerLocation,
							Details = tickets
								.Values
								.SelectMany(t => t.Charges
												  .Select(c => new InvoiceDetail
													  {
														  AccountBucket = t.AccountBucket,
														  ChargeCode = c.ChargeCode,
														  Comment = c.Comment,
														  Invoice = null,
														  Quantity = c.Quantity,
														  ReferenceNumber = t.ServiceTicketNumber,
														  ReferenceType = DetailReferenceType.ServiceTicket,
														  Tenant = _tenant,
														  UnitDiscount = c.UnitDiscount,
														  UnitSell = c.UnitSell,
													  }))
								.ToList(),
							DueDate = header.DueDate,
							ExportDate = DateUtility.SystemEarliestDateTime,
							Exported = false,
							FtpDelivered = false,
							HidePrefix = customer != null && customer.HidePrefix,
							InvoiceDate = header.TransactionDate,
							InvoiceNumber = header.TransactionId + casSuffix,
							InvoiceType = InvoiceType.Invoice,
							OriginalInvoice = null,
							PaidAmount = 0m,
							PostDate = DateTime.Now, // auto posting
							Posted = true,
							PrefixId = customer == null ? default(long) : customer.PrefixId,
							SpecialInstruction = string.Empty,
							Tenant = _tenant,
							User = _tenant.DefaultSystemUser,
						};
					arInv.AppendNumberSuffix();

					break;
				case TransHeaderConsts.TypeAPInvoice:	// synchronet considers AP Invoices are return to the vendors hence AP and Invoice
					apInv = new VendorBill
						{
							ApplyToDocument = null,
							BillType = InvoiceType.Invoice,
							DateCreated = DateTime.Now,
							Details = tickets
								.Values
								.SelectMany(t => t.Charges
												  .Select(c => new VendorBillDetail
													  {
														  AccountBucket = t.AccountBucket,
														  ChargeCode = c.ChargeCode,
														  Note = c.Comment,
														  Quantity = c.Quantity,
														  ReferenceNumber = t.ServiceTicketNumber,
														  ReferenceType = DetailReferenceType.ServiceTicket,
														  Tenant = _tenant,
														  UnitBuy = c.UnitBuy,
														  VendorBill = null,
													  }))
								.ToList(),
							DocumentDate = header.TransactionDate,
							DocumentNumber = header.UniqueInvoiceRefNos().Any() ? header.UniqueInvoiceRefNos().First() : header.TransactionId,
							ExportDate = DateUtility.SystemEarliestDateTime,
							Exported = false,
							PostDate = DateTime.Now, // auto posting
							Posted = true,
							Tenant = _tenant,
							User = _tenant.DefaultSystemUser,
							VendorLocation = vendorLocation,
						};
					

					break;
				case TransHeaderConsts.TypeAPCredit: // synchronet considers AP Credit as what they pay their vendors

					apInv = new VendorBill
					{
						ApplyToDocument = null,
						BillType = InvoiceType.Invoice,
						DateCreated = DateTime.Now,
						Details = tickets
							.Values
							.SelectMany(t => t.Charges
											  .Select(c => new VendorBillDetail
											  {
												  AccountBucket = t.AccountBucket,
												  ChargeCode = c.ChargeCode,
												  Note = c.Comment,
												  Quantity = c.Quantity,
												  ReferenceNumber = t.ServiceTicketNumber,
												  ReferenceType = DetailReferenceType.ServiceTicket,
												  Tenant = _tenant,
												  UnitBuy = 0,
												  VendorBill = null,
											  }))
							.ToList(),
						DocumentDate = header.TransactionDate,
						DocumentNumber = (header.UniqueInvoiceRefNos().Any() ? header.UniqueInvoiceRefNos().First() : header.TransactionId) + inv,
						ExportDate = DateUtility.SystemEarliestDateTime,
						Exported = false,
						PostDate = DateTime.Now, // auto posting
						Posted = true,
						Tenant = _tenant,
						User = _tenant.DefaultSystemUser,
						VendorLocation = vendorLocation,
					};

					apCrd = new VendorBill
					{
						ApplyToDocument = apInv,
						BillType = InvoiceType.Credit,
						DateCreated = DateTime.Now,
						Details = tickets
							.Values
							.SelectMany(t => t.Charges
											  .Select(c => new VendorBillDetail
											  {
												  AccountBucket = t.AccountBucket,
												  ChargeCode = c.ChargeCode,
												  Note = c.Comment,
												  Quantity = c.Quantity,
												  ReferenceNumber = t.ServiceTicketNumber,
												  ReferenceType = DetailReferenceType.ServiceTicket,
												  Tenant = _tenant,
												  UnitBuy = c.UnitBuy,
												  VendorBill = null,
											  }))
							.ToList(),
						DocumentDate = header.TransactionDate,
						DocumentNumber = header.UniqueInvoiceRefNos().Any() ? header.UniqueInvoiceRefNos().First() : header.TransactionId,
						ExportDate = DateUtility.SystemEarliestDateTime,
						Exported = false,
						PostDate = DateTime.Now, // auto posting
						Posted = true,
						Tenant = _tenant,
						User = _tenant.DefaultSystemUser,
						VendorLocation = vendorLocation,
					};

					break;
			}

			if (arInv != null)
				foreach (var detail in arInv.Details) detail.Invoice = arInv;
			if (arCrd != null)
				foreach (var detail in arCrd.Details) detail.Invoice = arCrd;
			if (apInv != null)
				foreach (var detail in apInv.Details) detail.VendorBill = apInv;
			if (apCrd != null)
				foreach (var detail in apCrd.Details) detail.VendorBill = apCrd;

			// auto flag as exported if all details items on invoices have amountdue = zero
			if (arInv != null && arInv.Details.All(d => d.AmountDue == 0))
			{
				arInv.ExportDate = DateTime.Now;
				arInv.Exported = true;
			}
			if (apInv != null && apInv.Details.All(d => d.TotalAmount == 0))
			{
				apInv.ExportDate = DateTime.Now;
				apInv.Exported = true;
			}
		}

		private Dictionary<string, ServiceTicket> GenerateServiceTickets(TransactionHeader header, Customer customer, CustomerLocation customerLocation,
												  Vendor vendor)
		{
			var tickets = new Dictionary<string, ServiceTicket>();
			foreach (var detail in header.Details)
			{
				ServiceTicket ticket;

				if (!tickets.ContainsKey(detail.BookingNumber))
				{
					var rep = (customer == null ? null : customer.SalesRepresentative);
					var tier = rep == null ? null : rep.ApplicableTier(DateTime.Now, ServiceMode.NotApplicable, customer);
					var rating = customer != null && customer.RatingId != default(long) ? customer.Rating : null;
					var resellerAddition = rating != null && rating.ResellerAdditionId != default(long)
						                       ? rating.ResellerAddition
						                       : null;


					var ticketNumber = _autoNumber.NextNumber(AutoNumberCode.ServiceTicketNumber, _tenant.DefaultSystemUser);
					ticket = new ServiceTicket
						{
							Customer = customer,
							Vendors = new List<ServiceTicketVendor>(),
							DateCreated = DateTime.Now,
							AccountBucket = _accountBucket,
							Assets = new List<ServiceTicketAsset>(),
							AuditedForInvoicing = true,
							AccountBucketUnit = null,
							BillReseller = rating != null && rating.BillReseller,
							Charges = new List<ServiceTicketCharge>(),
							Documents = new List<ServiceTicketDocument>(),
							Equipments = new List<ServiceTicketEquipment>(),
							HidePrefix = false,
							Items = new List<ServiceTicketItem>(),
							Notes = new List<ServiceTicketNote>(),
							ExternalReference1 = detail.BookingNumber,
							ExternalReference2 = string.Empty,
							OverrideCustomerLocation = customerLocation,
							PrefixId = customer == null ? default(long) : customer.PrefixId,
							ResellerAddition = resellerAddition,
							SalesRepAddlEntityCommPercent = rep == null ? 0m : rep.AdditionalEntityCommPercent,
							SalesRepAddlEntityName = rep == null ? string.Empty : rep.AdditionalEntityName,
							SalesRepMaxCommValue = tier == null ? 0m : tier.CeilingValue,
							SalesRepMinCommValue = tier == null ? 0m : tier.FloorValue,
							SalesRepresentative = rep,
							SalesRepresentativeCommissionPercent = tier == null ? 0m : tier.CommissionPercent,
							ServiceTicketNumber = ticketNumber == default(long) ? string.Empty : ticketNumber.GetString(),
							Services = new List<ServiceTicketService>(),
							Status = header.IsARTransaction ? ServiceTicketStatus.Invoiced : ServiceTicketStatus.Open,
							TicketDate = header.TransactionDate,
							User = _tenant.DefaultSystemUser,
							Tenant = _tenant,
						};


					if (vendor != null) ticket.Vendors.Add(new ServiceTicketVendor { ServiceTicket = ticket, Vendor = vendor, Tenant = vendor.Tenant, Primary = true});

					// to collection
					tickets.Add(detail.BookingNumber, ticket);


				}
				else ticket = tickets[detail.BookingNumber];

				
				// add charges
				ticket.Charges.Add(new ServiceTicketCharge
					{
						ServiceTicket = ticket,
						Tenant = _tenant,
						ChargeCode = _chargeCodes.FirstOrDefault(c => c.Code == _glCodeChargeCodeMap[detail.GlCode]),
						Comment = string.Format("{0}_{1}",detail.TransactionId,  detail.ContainerNumber),
						Quantity = 1,
						UnitBuy = header.IsARTransaction ? 0m : Math.Abs(detail.InvoiceAmount),
						UnitDiscount = 0m,
						UnitSell = header.IsARTransaction ? Math.Abs(detail.InvoiceAmount) : 0m,
					});
				// add items
				ticket.Items.Add(new ServiceTicketItem
					{
						ServiceTicket = ticket,
						Tenant = _tenant,
						Description = string.Format("Container #: {0}", detail.ContainerNumber),
						Comment = string.Format("Transaction Id: {0}", detail.TransactionId),
					});
				
			}
			return tickets;
		}


		private IEnumerable<ValidationMessage> SetExistingJobsAndCreateNewJobsWhereApplicable(ref Dictionary<string, ServiceTicket> tickets, out List<Job> jobs)
		{
			jobs = new List<Job>();
			var msgs = new List<ValidationMessage>();

			var parameter = new ParameterColumn
				{
					ReportColumnName = OperationsSearchFields.ExternalReference1.Name,
					Operator = Operator.Equal,
				};
			var criteria = new JobViewSearchCriteria();
			criteria.Parameters.Add(parameter);
			criteria.ActiveUserId = _tenant.DefaultSystemUserId;

			foreach (var serviceTicket in tickets.Values)
			{
				try
				{
					criteria.Parameters[0].DefaultValue = serviceTicket.ExternalReference1;
					var jobDashboardDto = _jobSearch.FetchJobDashboardDtos(criteria, serviceTicket.TenantId).FirstOrDefault();
					if (jobDashboardDto != null)
					{
						serviceTicket.JobId = jobDashboardDto.Id;
						continue;
					}

					var job = new Job
						{
							DateCreated = DateTime.Now,
							CreatedByUser = _tenant.DefaultSystemUser,
							Customer = serviceTicket.Customer,
							Documents = new List<JobDocument>(),
							ExternalReference1 = serviceTicket.ExternalReference1,
							Status = JobStatus.InProgress,
							ExternalReference2 = string.Empty,
							Tenant = _tenant,
							JobNumber = _autoNumber.NextNumber(AutoNumberCode.JobNumber, _tenant.DefaultSystemUser).ToString()
						};
					jobs.Add(job);
				}
				catch (Exception ex)
				{
					msgs.Add(ValidationMessage.Error(ex.ToString()));
				    return msgs;
				}
			}

			return msgs;

		}


		private void ProcessServiceTicketCharges(ServiceTicket serviceTicket)
		{

			foreach (var charge in serviceTicket.Charges)
			{
				charge.Connection = Connection;
				charge.Transaction = Transaction;

				charge.Save();
				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Service Ticket Number:{2} Charge Ref#:{3}",
												AuditLogConstants.AddedNew, charge.EntityName(), serviceTicket.ServiceTicketNumber, charge.Id),
					TenantId = _tenant.Id,
					User = _tenant.DefaultSystemUser,
					EntityCode = serviceTicket.EntityName(),
					EntityId = serviceTicket.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();
			}
		}

		private void ProcessServiceTicketItems(ServiceTicket serviceTicket)
		{
			foreach (var i in serviceTicket.Items)
			{
				i.Connection = Connection;
				i.Transaction = Transaction;

				i.Save();
				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Service Ticket Number:{2} Item Ref#:{3}",
												AuditLogConstants.AddedNew, i.EntityName(), serviceTicket.ServiceTicketNumber, i.Id),
					TenantId = _tenant.Id,
					User = _tenant.DefaultSystemUser,
					EntityCode = serviceTicket.EntityName(),
					EntityId = serviceTicket.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

			}
		}

		private void ProcessServiceTicketVendors(ServiceTicket serviceTicket)
		{
			foreach (var qv in serviceTicket.Vendors)
			{
				qv.Connection = Connection;
				qv.Transaction = Transaction;

				qv.Save();
				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Service Ticket Number:{2} Vendor Number:{3}",
						                            AuditLogConstants.AddedNew, qv.EntityName(), serviceTicket.ServiceTicketNumber,
						                            qv.Vendor.VendorNumber),
						TenantId = _tenant.Id,
						User = _tenant.DefaultSystemUser,
						EntityCode = serviceTicket.EntityName(),
						EntityId = serviceTicket.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
			}
		}


		private void ProcessVendorBillDetails(VendorBill bill)
		{
			foreach (var i in bill.Details)
			{
				i.Connection = Connection;
				i.Transaction = Transaction;

				i.Save();
				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Vendor Bill Number:{2} Detail Ref#:{3}",
												AuditLogConstants.AddedNew, i.EntityName(), bill.DocumentNumber, i.Id),
					TenantId = _tenant.Id,
					User = _tenant.DefaultSystemUser,
					EntityCode = bill.EntityName(),
					EntityId = bill.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();
			}
		}


		private void ProcessInvoiceDetails(Invoice invoice)
		{
			foreach (var i in invoice.Details)
			{
				i.Connection = Connection;
				i.Transaction = Transaction;

					i.Save();
					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Invoice Number:{2} Detail Ref#:{3}",
													AuditLogConstants.AddedNew, i.EntityName(), invoice.InvoiceNumber, i.Id),
						TenantId = _tenant.Id,
						User = _tenant.DefaultSystemUser,
						EntityCode = invoice.EntityName(),
						EntityId = invoice.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
			}
		}





		public void SaveCustomers(List<Customer> customers, LogWriterAdapater logWriter, Tenant tenant)
		{

			foreach (var customer in customers)
			{
				var nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.CustomerNumber, _user);
				customer.CustomerNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();
				var customerLocations = customer.Locations;
				customer.LoadCollections();
				customer.Locations = customerLocations;

				foreach (var location in customer.Locations.Where(location => String.IsNullOrEmpty(location.LocationNumber)))
				{
					nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.CustomerLocationNumber, _user);
					location.LocationNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();
				}


				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();

				try
				{
					// link up
					_customerValidator.Connection = Connection;
					_customerValidator.Transaction = Transaction;

					customer.Connection = Connection;
					customer.Transaction = Transaction;

					// validate customer
					_customerValidator.Messages.Clear();
					if (!_customerValidator.IsValid(customer))
					{
						RollBackTransaction();
						logWriter.Write(string.Empty);
						logWriter.WriteError(string.Format("There was an error saving \"{0}\":", customer.Name));

						foreach (var message in _customerValidator.Messages)
						{
							logWriter.WriteTabbedLine(message.Message);
						}
						return;
					}

					customer.Save();

					new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1}", AuditLogConstants.AddedNew, customer.Name),
						TenantId = _user.TenantId,
						User = _user,
						EntityCode = customer.EntityName(),
						EntityId = customer.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();

					ProcessCustomerLocations(customer);

					CommitTransaction();

					logWriter.Write(string.Empty);
					logWriter.WriteTabbedLine(string.Format("Saved Customer: {0}", customer.Name));

				}
				catch (Exception ex)
				{
					RollBackTransaction();
					logWriter.Write(string.Empty);
					logWriter.WriteError(string.Format(ExceptionMessage, ex.Message));
				}
			}

		}

		private void ProcessCustomerLocations(Customer customer)
		{
			var locations = customer.Locations;
			foreach (var location in locations)
			{
				location.Connection = Connection;
				location.Transaction = Transaction;

				location.Save();

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Customer:{2} Location #:{3}",
												AuditLogConstants.AddedNew, location.EntityName(), customer.Name, location.LocationNumber),
					TenantId = _user.TenantId,
					User = _user,
					EntityCode = customer.EntityName(),
					EntityId = customer.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				ProcessCustomerLocationContact(customer, location);
			}
		}

		private void ProcessCustomerLocationContact(Customer customer, CustomerLocation location)
		{
			foreach (var c in location.Contacts)
			{
				c.Connection = Connection;
				c.Transaction = Transaction;
				c.Save();

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Customer:{2} Location #:{3} Contact Ref: {4}",
												AuditLogConstants.AddedNew, c.EntityName(), customer.Name,
												location.LocationNumber,
												c.Id),
					TenantId = _user.TenantId,
					User = _user,
					EntityCode = customer.EntityName(),
					EntityId = customer.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

			}
		}





		public void SaveVendors(List<Vendor> vendors, LogWriterAdapater logWriter)
		{

			foreach (var vendor in vendors)
			{
				var nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.VendorNumber, _user);
				vendor.VendorNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();
				var vendorLocations = vendor.Locations;
				vendor.LoadCollections();
				vendor.Locations = vendorLocations;

				foreach (var location in vendor.Locations.Where(location => String.IsNullOrEmpty(location.LocationNumber)))
				{
					nextNumber = _autoNumberProcessor.NextNumber(AutoNumberCode.CustomerLocationNumber, _user);
					location.LocationNumber = nextNumber == default(long) ? string.Empty : nextNumber.GetString();
				}

				Connection = DatabaseConnection.DefaultConnection;
				BeginTransaction();

				try
				{
					// link up
					_vendorValidator.Connection = Connection;
					_vendorValidator.Transaction = Transaction;

					vendor.Connection = Connection;
					vendor.Transaction = Transaction;

					// validate vendor
					_vendorValidator.Messages.Clear();
					if (!_vendorValidator.IsValid(vendor))
					{
						RollBackTransaction();
						logWriter.Write(string.Empty);
						logWriter.WriteError(string.Format("There was an error saving \"{0}\":", vendor.Name));

						foreach (var message in _vendorValidator.Messages)
						{
							logWriter.WriteTabbedLine(message.Message);
						}

					}
					else
					{
						vendor.Save();

						new AuditLog
						{
							Connection = Connection,
							Transaction = Transaction,
							Description = string.Format("{0} {1}", AuditLogConstants.AddedNew, vendor.Name),
							TenantId = _user.TenantId,
							User = _user,
							EntityCode = vendor.EntityName(),
							EntityId = vendor.Id.ToString(),
							LogDateTime = DateTime.Now
						}.Log();

						ProcessVendorLocations(vendor);

						CommitTransaction();

						logWriter.Write(string.Empty);
						logWriter.WriteTabbedLine(string.Format("Saved Vendor: {0}", vendor.Name));
					}



				}
				catch (Exception ex)
				{
					RollBackTransaction();
					logWriter.Write(string.Empty);
					logWriter.WriteError(string.Format(ExceptionMessage, ex.Message));
				}
			}

		}

		private void ProcessVendorLocations(Vendor vendor)
		{
			var locations = vendor.Locations;
			foreach (var location in locations)
			{
				location.Connection = Connection;
				location.Transaction = Transaction;

				location.Save();

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Vendor:{2} Location #:{3}",
												AuditLogConstants.AddedNew, location.EntityName(), vendor.Name, location.LocationNumber),
					TenantId = _user.TenantId,
					User = _user,
					EntityCode = vendor.EntityName(),
					EntityId = vendor.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

				ProcessVendorLocationContact(vendor, location);
			}
		}

		private void ProcessVendorLocationContact(Vendor vendor, VendorLocation location)
		{
			foreach (var c in location.Contacts)
			{
				c.Connection = Connection;
				c.Transaction = Transaction;
				c.Save();

				new AuditLog
				{
					Connection = Connection,
					Transaction = Transaction,
					Description = string.Format("{0} {1} Vendor:{2} Location #:{3} Contact Ref: {4}",
												AuditLogConstants.AddedNew, c.EntityName(), vendor.Name,
												location.LocationNumber,
												c.Id),
					TenantId = _user.TenantId,
					User = _user,
					EntityCode = vendor.EntityName(),
					EntityId = vendor.Id.ToString(),
					LogDateTime = DateTime.Now
				}.Log();

			}
		}


		public ServiceTicket FetchServiceTicketByTransactionIdAndContainerNumber(string transactionId, string containerNumber)
		{
			const string query = "select t.* from ServiceTicket t, ServiceTicketCharge c where t.Id = c.ServiceTicketId and c.Comment = @Criteria";
			var parameters = new Dictionary<string, object>
				{
					{"Criteria", string.Format("{0}_{1}", transactionId, containerNumber)}
				};
			ServiceTicket ticket = null;
			using(var reader = GetReader(query, parameters))
				if (reader.Read())
					ticket = new ServiceTicket(reader);
			Connection.Close();
			return ticket;
		}

		public List<ValidationMessage> AddDocumentToServiceTicket(ServiceTicket ticket, FileInfo doc, DocumentTag docTag, string docName, string docPath, string docDescription)
		{
			try
			{
				var msgs = new List<ValidationMessage>();

				var document = new ServiceTicketDocument
					{

						Name = docName,
						Description = docDescription,
						DocumentTag = docTag,
						LocationPath = docPath,
						IsInternal = true,
						ServiceTicketId = ticket.Id,
						TenantId = _tenant.Id
					};
				msgs.AddRange(SaveServiceTicketDocument(document));
				return msgs;
			}
			catch (Exception ex)
			{
				return new List<ValidationMessage> {ValidationMessage.Error(ex.Message)};
			}
			
		}

		private IEnumerable<ValidationMessage> SaveServiceTicketDocument(ServiceTicketDocument document)
		{
			var validationMessages = new List<ValidationMessage>();
			
			Connection = DatabaseConnection.DefaultConnection;
			try
			{
				document.Connection = Connection;
				document.Transaction = Transaction;

				document.Save();
					
				new AuditLog
					{
						Connection = Connection,
						Transaction = Transaction,
						Description = string.Format("{0} {1} Service Ticket Document:{2} Detail Ref#:{3}",
						                            AuditLogConstants.AddedNew, document.EntityName(), document.LocationPath, document.Id),
						TenantId = _tenant.Id,
						User = _tenant.DefaultSystemUser,
						EntityCode = document.EntityName(),
						EntityId = document.Id.ToString(),
						LogDateTime = DateTime.Now
					}.Log();
			}
			catch (Exception ex)
			{
				validationMessages.Add(ValidationMessage.Error(ex.Message)); 
			}
			return validationMessages;
		}
	}
}
