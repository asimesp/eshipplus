﻿namespace LogisticsPlus.Synchronet.Helpers
{
    public class CustomerConsts
    {
        // NOTE: Fields intentionally skip some numbers as not all data in the files we receive can be mapped.
        public const int Name = 0;
        public const int Code = 3;
        public const int ReceivableTerms = 5;
        public const int AddressId = 6;
        public const int Address1 = 8;
        public const int Address2 = 9;

		public const int Address3 = 10;
		public const int Address4 = 11;

		public const int City = 12;
		public const int State = 13;
		public const int PostalCode = 14;
		public const int Country = 15;
        public const int Attention = 16;
    }
}
