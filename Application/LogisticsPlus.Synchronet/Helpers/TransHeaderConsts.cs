﻿namespace LogisticsPlus.Synchronet.Helpers
{
	public class TransHeaderConsts
	{
		public const int TransactionId = 0;
		public const int Type = 1;
		public const int TransactionDate = 2;
		public const int GlDate = 3;
		public const int DueDate = 4;
		public const int Amount = 5;
		public const int AccountLabel = 6;
		public const int AccountName = 7;
		public const int AddressId = 8;
		public const int AddressLabel = 9;
		public const int InvoiceAddress1 = 10;
		public const int InvoiceAddress2 = 11;
		public const int InvoiceAddress3 = 12;
		public const int InvoiceAddress4 = 13;
		public const int InvoiceAddressCity = 14;
		public const int InvoiceAddressState = 15;
		public const int InvoiceAddressZip = 16;
		public const int AddressCountry = 17;
		public const int Attention = 18;
		public const int EmailTo = 19;
		public const int EmailCc = 20;


		public const int ColumnCount = 20;

		public const string TypeAPInvoice = "Credit Note-AP";
		public const string TypeARCredit = "Credit Note-AR";
		public const string TypeAPCredit = "Invoice-AP";
		public const string TypeARInvoice = "Invoice-AR";
	}
}
