﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor;

namespace LogisticsPlus.Synchronet.Helpers
{
	[Serializable]
	public class TransactionHeader
	{
		private string[] _data;

		public string[] Data { get { return _data; } set { _data = value; } }

		public string TransactionId
		{
			get { return _data[TransHeaderConsts.TransactionId]; }
			set { _data[TransHeaderConsts.TransactionId] = value; }
		}

		public string Type
		{
			get { return _data[TransHeaderConsts.Type]; }
			set { _data[TransHeaderConsts.Type] = value; }
		}

		public DateTime TransactionDate
		{
			get { return _data[TransHeaderConsts.TransactionDate].ToDateTime(); }
			set { _data[TransHeaderConsts.TransactionDate] = value.FormattedShortDateAlt(); }
		}

		public DateTime DueDate
		{
			get { return _data[TransHeaderConsts.DueDate].ToDateTime(); }
			set { _data[TransHeaderConsts.DueDate] = value.FormattedShortDateAlt(); }
		}

		public decimal Amount
		{
			get { return Math.Abs(_data[TransHeaderConsts.Amount].ToDecimal()); }
			set { _data[TransHeaderConsts.Amount] = ((Type == TransHeaderConsts.TypeAPInvoice || Type == TransHeaderConsts.TypeARCredit ? -1 : 1) * value).GetString(); }
		}

		public string AccountLabel
		{
			get { return _data[TransHeaderConsts.AccountLabel]; }
			set { _data[TransHeaderConsts.AccountLabel] = value; }
		}

		public string AccountName
		{
			get { return _data[TransHeaderConsts.AccountName]; }
			set { _data[TransHeaderConsts.AccountName] = value; }
		}

		public string AddressId
		{
			get { return _data[TransHeaderConsts.AddressId]; }
			set { _data[TransHeaderConsts.AddressId] = value; }
		}

		public string AddressHash
		{
			get
			{
				var value = _data[TransHeaderConsts.InvoiceAddress1] + _data[TransHeaderConsts.InvoiceAddress2] +
				            _data[TransHeaderConsts.InvoiceAddressCity] + _data[TransHeaderConsts.InvoiceAddressState] +
				            _data[TransHeaderConsts.InvoiceAddressZip];
				return value.ToAlphaNumericOnly();
			}
		}

		public decimal DetailSummedAmount
		{
			get { return Math.Abs(Details.Sum(d => d.InvoiceAmount)); }
		}

		public bool IsARTransaction
		{
			get { return Type == TransHeaderConsts.TypeARInvoice || Type == TransHeaderConsts.TypeARCredit; }
		}

		public bool IsAPTransaction
		{
			get { return !IsARTransaction; }
		}

		public bool IsCredit
		{
			get { return Type == TransHeaderConsts.TypeAPCredit || Type == TransHeaderConsts.TypeARCredit; }
		}

		public List<TransactionDetail> Details { get; set; }


		public bool IsOrphanedHeader
		{
			get { return _data == null || (TransactionId == null && Type == null && Amount == 0 && AccountLabel == null); }
		}


		public TransactionHeader() : this(new string[TransHeaderConsts.ColumnCount])
		{
		}

		public TransactionHeader(string[] data)
		{
			_data = data;
			Details = new List<TransactionDetail>();
		}
	}
}
