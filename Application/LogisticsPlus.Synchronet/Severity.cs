﻿namespace LogisticsPlus.Synchronet
{
    /// <summary>
    /// Log entry severity enum
    /// </summary>
    public enum Severity
    {
        Error,
        Info,
        Debug
    }
}
