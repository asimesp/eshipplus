﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Synchronet.Helpers;
using OfficeOpenXml;

namespace LogisticsPlus.Synchronet
{
    public static class Utilities
    {

        public static string Truncate(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }



		private static char[] ImportSeperators()
		{
			return new[] { '\t', '|' };
		}

		internal static char[] Seperators()
		{
			var chars = ImportSeperators().ToList();
			chars.AddRange(Environment.NewLine.ToArray());
			return chars.ToArray();
		}

        internal static List<string[]> GetImportData(this FileInfo arg, bool removeEmptyRow, bool removeHeader = true)
        {
			List<string[]> lines;

			if (arg.Name.EndsWith(FileExtensions.xlsx.GetString()))
			{
				// reading from xlsx file
				using (var fileStream = arg.Open(FileMode.Open))
				{
					var pck = new ExcelPackage(fileStream);
					lines = GetLinesFromPackage(removeEmptyRow, removeHeader, pck);
				}
			}
			else if (arg.Name.EndsWith(FileExtensions.csv.GetString()))
			{
				// reading from csv file
				using (var streamReader = arg.OpenText())
					lines = GetLinesFromReaderCsv(streamReader, removeEmptyRow, removeHeader);
			}
			else
			{
				// reading from txt file
				using (var fileStream = arg.OpenRead())
				using (var streamReader = new StreamReader(fileStream))
					lines = GetLinesFromReader(removeEmptyRow, removeHeader, streamReader);

			}

			return lines;
        }

		private static List<string[]> GetLinesFromReaderCsv(this TextReader reader, bool removeEmptyRow = true, bool removeHeader = true)
		{
			string contents;
			using (reader) contents = reader.ReadToEnd();
			var allLines = contents.Split(Environment.NewLine.ToArray(), StringSplitOptions.RemoveEmptyEntries).ToList();
			if (removeHeader && allLines.Count > 0) allLines.RemoveAt(0); // remove header
			var lines = removeEmptyRow
							? allLines.Select(l => l.Split(',')).Where(l => !string.IsNullOrEmpty(l.SpaceJoin().Trim())).ToList()
							: allLines.Select(l => l.Split(',')).ToList();
			return lines;
		}

		private static List<string[]> GetLinesFromReader(bool removeEmptyRow, bool removeHeader, StreamReader reader)
		{
			string contents;
			using (reader) contents = reader.ReadToEnd();
			var allLines = contents.Split(Environment.NewLine.ToArray(), StringSplitOptions.RemoveEmptyEntries).ToList();
			if (removeHeader && allLines.Count > 0) allLines.RemoveAt(0); // remove header
			List<string[]> lines = removeEmptyRow
							 ? allLines
								   .Select(l => l.Split(ImportSeperators()))
								   .Where(l => !string.IsNullOrEmpty(l.SpaceJoin().Trim()))
								   .ToList()
							 : allLines
								   .Select(l => l.Split(ImportSeperators()))
								   .ToList();
			return lines;
		}

        private static List<string[]> GetLinesFromPackage(bool removeEmptyRow, bool removeHeader, ExcelPackage pck)
        {
            var ws = pck.Workbook.Worksheets.FirstOrDefault();
            if (ws == null)
                throw new NullReferenceException("No data sheet in excel file!");

            var lines = new List<string[]>();
            var startIndex = removeHeader ? 2 : 1;
            for (var r = startIndex; r <= ws.Dimension.End.Row; r++)
            {
                var row = new string[ws.Dimension.End.Column];
                for (var c = 1; c <= ws.Dimension.End.Column; c++)
                    row[c - 1] = ws.Cells[r, c].Value.GetString();
                if (removeEmptyRow && !string.IsNullOrEmpty(row.SpaceJoin().Trim())) lines.Add(row);
            }
            return lines;
        }


		public static byte[] ToUtf8Bytes(this string value)
		{
			return string.IsNullOrEmpty(value) ? null : new UTF8Encoding().GetBytes(value);
		}

		public static string FromUtf8Bytes(this byte[] value)
		{
			return value == null ? string.Empty : new UTF8Encoding().GetString(value);
		}


		public static byte[] ReadFromFile(this string fullFilePath)
		{
			if (!File.Exists(fullFilePath)) return new byte[0];
			try
			{
				byte[] contents;

				using (var stream = new FileStream(fullFilePath, FileMode.Open, FileAccess.Read))
				{
					contents = new byte[stream.Length];
					for (var i = 0; i < stream.Length; i++)
						stream.Read(contents, i, 1);
				}

				return contents;
			}
			catch (Exception)
			{
				return new byte[0];
			}
		}

		public static void WriteToFile(this byte[] contents, string physicalPath, string filename, string oldFile)
		{
			if (!string.IsNullOrEmpty(oldFile) && File.Exists(oldFile)) File.Delete(oldFile);

			if (!Directory.Exists(physicalPath)) Directory.CreateDirectory(physicalPath);
			using (var stream = new FileStream(Path.Combine(physicalPath, filename), FileMode.Create))
			using (var writer = new BinaryWriter(stream))
				writer.Write(contents);
		}


		public static string ToAlphaNumericOnly(this string value)
		{
			if (string.IsNullOrEmpty(value)) return string.Empty;

			return value.Where(char.IsLetterOrDigit).Aggregate(string.Empty, (current, ch) => current + ch);
		}

        internal static bool ContainsFileSignature(this string value, string fileSig)
        {
            return !string.IsNullOrEmpty(fileSig) && value.ToLower().Contains(fileSig.ToLower());
        }
		


		internal static void GroupHeadersAndDetails(this List<TransactionHeader> headers, ref List<TransactionDetail> details)
		{
			var dd = details
				.GroupBy(d => d.TransactionId)
				.ToDictionary(g => g.Key, g => g.ToList());

			foreach (var header in headers)
			{
				if(dd.ContainsKey(header.TransactionId)) header.Details.AddRange(dd[header.TransactionId]);
				dd.Remove(header.TransactionId); // remove already process details.  Any details left after processing are orphaned records!
			}

			details = dd.Values.SelectMany(d => d).ToList(); // copy out orphaned records
		}

		internal static List<string> UniqueInvoiceRefNos(this TransactionHeader header)
		{
			return header.Details.Select(d => d.InvoiceRefNo).Where(n => !string.IsNullOrEmpty(n)).Distinct().ToList();
		}

      
        internal static void ProcessAndImportCustomerFile(this List<string[]> lines, LogWriterAdapater logWriter)
        {
            var universalCustomers = new Dictionary<string, Customer>();
            var individualCustomers = new List<Customer>();
	        var tenant = new TenantSearch().FetchTenants(new List<ParameterColumn>()).FirstOrDefault(line => line.Code == Settings.DefaultTenantCode);
	        var tier = new TierSearch().FetchTierByTierNumber(Settings.DefaultTierNumber, tenant.Id);
	        var accountBucket = new AccountBucketSearch().FetchAccountBucketByCode(Settings.DefaultAccountBucketCode, tenant.Id);
	       
            foreach (var line in lines)
            {
				string postalCode;
				PostalCodeViewSearchDto postalCodeDetails;
	            var postalCodeSearch = new PostalCodeSearch();
	            var prefeix = new PrefixSearch().FetchPrefixByCode(Settings.DefaultPrefixCode, tenant.Id);
				var groupKey = line[CustomerConsts.Code];
	            if (!universalCustomers.ContainsKey(groupKey) || string.IsNullOrEmpty(groupKey))
	            {
	              
		            var customer = new Customer
			            {
				            Name = line[CustomerConsts.Name],
							InvoiceTerms = line[CustomerConsts.ReceivableTerms].ToInt(),
				            DefaultAccountBucket = accountBucket,
				            DateCreated = DateTime.Now,
				            Documents = new List<CustomerDocument>(),
				            Tier = tier,
				            TenantId = tenant.Id,

				            AdditionalBillOfLadingText = string.Empty,
				            InvalidPurchaseOrderNumberMessage = string.Empty,
				            CustomVendorSelectionMessage = string.Empty,
				            CustomField1 = string.Empty,
							CustomField2 = line[CustomerConsts.Code],
				            CustomField3 = string.Empty,
				            CustomField4 = string.Empty,
				            ShipperBillOfLadingPrefix = string.Empty,
				            ShipperBillOfLadingSuffix = string.Empty,
				            LogoUrl = string.Empty,
				            Notes = string.Empty,
				            AuditInstructions = string.Empty,
				            PaymentGatewayKey = string.Empty,		
							Prefix = prefeix,
							Active = true
			            };

					if (!string.IsNullOrEmpty(line[CustomerConsts.AddressId]) && !string.IsNullOrEmpty(line[VendorConsts.PostalCode]))
		            {
						 postalCode = line[CustomerConsts.PostalCode].Replace('-', ' ');
						 postalCodeDetails = postalCodeSearch.FetchPostalCodesByCodeAndState(postalCode, line[CustomerConsts.State]).FirstOrDefault();
			           
			            customer.Locations = new List<CustomerLocation>
				            {
					            new CustomerLocation
						            {
							            TenantId = tenant.Id,
							            LocationNumber = line[CustomerConsts.AddressId],
							            Street1 = line[CustomerConsts.Address1],
							            Street2 = line[CustomerConsts.Address2],
							            City = line[CustomerConsts.City],
							            State = line[CustomerConsts.State],
							            PostalCode = line[CustomerConsts.PostalCode],
							            CountryId = postalCodeDetails != null ? postalCodeDetails.CountryId : tenant.DefaultCountryId,
							            DateCreated = DateTime.Now,
							            Primary = true,
										BillToLocation = true,
										MainBillToLocation = true,
							            Customer = customer
						            }
				            };

						if (!string.IsNullOrEmpty(line[CustomerConsts.Attention]))
			            {
				            customer.Locations.First().Contacts = new List<CustomerContact>
					            {
						            new CustomerContact
							            {
								            Name = line[CustomerConsts.Attention],
								            TenantId = tenant.Id,
								            Primary = true,
								            ContactTypeId = tenant.AvailableLoadsContactTypeId,
								            CustomerLocation = customer.Locations.First(),
											Comment = String.Empty,
											Email = string.Empty,
											Fax = string.Empty,
											Mobile = string.Empty,
											Phone = string.Empty,
							            }
					            };
			            }

			            if (string.IsNullOrEmpty(groupKey))
				            individualCustomers.Add(customer);
			            else
				            universalCustomers.Add(groupKey, customer);
		            }
	            }
	            else
	            {
					if (!string.IsNullOrEmpty(line[CustomerConsts.AddressId]) && !string.IsNullOrEmpty(line[VendorConsts.PostalCode]))
		            {
			            if (universalCustomers[groupKey].Locations == null) universalCustomers[groupKey].Locations = new List<CustomerLocation>();

						 postalCode = line[CustomerConsts.PostalCode].Replace('-', ' ');
						 postalCodeDetails = postalCodeSearch.FetchPostalCodesByCodeAndState(postalCode, line[CustomerConsts.State]).FirstOrDefault();

			            universalCustomers[groupKey].Locations.Add(new CustomerLocation
				            {
					            TenantId = tenant.Id,
					            LocationNumber = line[CustomerConsts.AddressId],
					            Street1 = line[CustomerConsts.Address1],
					            Street2 = line[CustomerConsts.Address2],
					            City = line[CustomerConsts.City],
					            State = line[CustomerConsts.State],
					            PostalCode = line[CustomerConsts.PostalCode],
					            CountryId = postalCodeDetails != null ? postalCodeDetails.CountryId : tenant.DefaultCountryId,
					            DateCreated = DateTime.Now,
					            Customer = universalCustomers[groupKey],
								Primary = false,
								BillToLocation = true,
								MainBillToLocation = false,

				            });

						if (!string.IsNullOrEmpty(line[CustomerConsts.Attention]))
			            {
				            universalCustomers[groupKey].Locations.First().Contacts = new List<CustomerContact>
					            {
						            new CustomerContact
							            {
								            Name = line[CustomerConsts.Attention],
								            TenantId = tenant.Id,
								            Primary = true,
								            ContactTypeId = tenant.AvailableLoadsContactTypeId,
								            CustomerLocation = universalCustomers[groupKey].Locations.First(),
											Comment = String.Empty,
											Email = string.Empty,
											Fax = string.Empty,
											Mobile = string.Empty,
											Phone = string.Empty,
							            }
					            };
			            }
		            }
	            }
            }

            var customersToImport = universalCustomers.Values.ToList();
            customersToImport.AddRange(individualCustomers);

            var customerHandler = new EshipProcessesManager(tenant);
            customerHandler.SaveCustomers(customersToImport, logWriter, tenant);
        }

		internal static void ProcessAndImportVendorFile(this List<string[]> lines, LogWriterAdapater logWriter)
		{
			var universalVendors = new Dictionary<string, Vendor>();
			var individualVendors = new List<Vendor>();
			var tenant = new TenantSearch().FetchTenants(new List<ParameterColumn>()).FirstOrDefault(line => line.Code == Settings.DefaultTenantCode);
		
			foreach (var line in lines)
			{
				string postalCode;
				PostalCodeViewSearchDto postalCodeDetails;
				var postalCodeSearch = new PostalCodeSearch();
				
				var groupKey = line[VendorConsts.Code];
				if (!universalVendors.ContainsKey(groupKey) || string.IsNullOrEmpty(groupKey))
				{
					var vendor = new Vendor
						{
							Name = line[VendorConsts.Name],
							TenantId = tenant.Id,
							Scac = line[VendorConsts.ScacCode],
							Notation = string.Empty,
							Notes = string.Empty,
							CustomField1 = string.Empty,
							CustomField2 = line[VendorConsts.Code],
							CustomField3 = string.Empty,
							CustomField4 = string.Empty,
							CTPATNumber = string.Empty,
							PIPNumber = string.Empty,
							FederalIDNumber = string.Empty,
							BrokerReferenceNumber = string.Empty,
							LogoUrl = string.Empty,
							MC = line[VendorConsts.MCNumber],
							DOT = string.Empty,
							TrackingUrl = string.Empty,
							LastAuditDate = DateUtility.SystemEarliestDateTime,
							DateCreated = DateTime.Now,
							Active = true,
							Documents = new List<VendorDocument>(),
						};
					


					if (!string.IsNullOrEmpty(line[VendorConsts.AddressId]) && !string.IsNullOrEmpty(line[VendorConsts.PostalCode]))
					{
						postalCode = line[CustomerConsts.PostalCode].Replace('-', ' ');
						postalCodeDetails = postalCodeSearch.FetchPostalCodesByCodeAndState(postalCode, line[CustomerConsts.State]).FirstOrDefault();
				
						vendor.Locations = new List<VendorLocation>
							{
								new VendorLocation
									{
										TenantId = tenant.Id,
										LocationNumber = line[VendorConsts.AddressId],
										Street1 = line[VendorConsts.Address1],
										Street2 = line[VendorConsts.Address2],
										City = line[VendorConsts.City],
										State = line[VendorConsts.State],
										PostalCode = line[VendorConsts.PostalCode],
										CountryId = postalCodeDetails != null ? postalCodeDetails.CountryId : tenant.DefaultCountryId,
										Primary = true,
										RemitToLocation = true,
										MainRemitToLocation = true,
										DateCreated = DateTime.Now,
										Vendor = vendor,
									}
							};

						if (!string.IsNullOrEmpty(line[VendorConsts.Attention]))
						{
							vendor.Locations.First().Contacts = new List<VendorContact>
								{
									new VendorContact
										{
											Name = line[VendorConsts.Attention],
											TenantId = tenant.Id,
											Primary = true,
											ContactTypeId = tenant.AvailableLoadsContactTypeId,
											VendorLocation = vendor.Locations.First(),
											Comment = string.Empty,
											Email = string.Empty,
											Fax = string.Empty,
											Mobile = string.Empty,
											Phone = string.Empty,
										}
								};
						}

						if (string.IsNullOrEmpty(groupKey))
							individualVendors.Add(vendor);
						else
							universalVendors.Add(groupKey, vendor);
					}
				}
				else
				{
					if (!string.IsNullOrEmpty(line[VendorConsts.AddressId]) && !string.IsNullOrEmpty(line[VendorConsts.PostalCode]))
					{
						if (universalVendors[groupKey].Locations == null) universalVendors[groupKey].Locations = new List<VendorLocation>();
						postalCode = line[CustomerConsts.PostalCode].Replace('-', ' ');
						postalCodeDetails = postalCodeSearch.FetchPostalCodesByCodeAndState(postalCode, line[CustomerConsts.State]).FirstOrDefault();

						universalVendors[groupKey].Locations.Add(new VendorLocation
							{
								TenantId = tenant.Id,
								LocationNumber = line[VendorConsts.AddressId],
								Street1 = line[VendorConsts.Address1],
								Street2 = line[VendorConsts.Address2],
								City = line[VendorConsts.City],
								State = line[VendorConsts.State],
								PostalCode = line[VendorConsts.PostalCode],
								CountryId = postalCodeDetails != null ? postalCodeDetails.CountryId : tenant.DefaultCountryId,
								DateCreated = DateTime.Now,
								Primary = false,
								RemitToLocation = true,
								MainRemitToLocation = false,
								Vendor = universalVendors[groupKey],
							});

						if (!string.IsNullOrEmpty(line[VendorConsts.Attention]))
						{
							universalVendors[groupKey].Locations.First().Contacts = new List<VendorContact>
								{
									new VendorContact
										{
											Name = line[VendorConsts.Attention],
											TenantId = tenant.Id,
											Primary = true,
											ContactTypeId = tenant.AvailableLoadsContactTypeId,
											VendorLocation = universalVendors[groupKey].Locations.First(),
											Comment = string.Empty,
											Email = string.Empty,
											Fax = string.Empty,
											Mobile = string.Empty,
											Phone = string.Empty,
										}
								};
						}
					}
				}
			}

			var vendorsToImport = universalVendors.Values.ToList();
			vendorsToImport.AddRange(individualVendors);

			var vendorHandler = new EshipProcessesManager(tenant);
			vendorHandler.SaveVendors(vendorsToImport, logWriter);
		}

		internal static void ProcessTransactions(this List<string[]> headers, List<string[]> lines,
		                                         LogWriterAdapater logWriter)
		{
			
		}



		internal static List<TransactionHeader> ProcessForApHeadersWithMultipleInvoiceRefNo(this List<TransactionHeader> headers)
		{
			var hdrs = new List<TransactionHeader>();

			foreach (var header in headers)
			{
				if (header.IsAPTransaction && header.UniqueInvoiceRefNos().Count > 1)
				{
					foreach (var invRef in header.UniqueInvoiceRefNos())
					{
						var hdr = new TransactionHeader(header.Data)
							{
								Details = header.Details.Where(d => d.InvoiceRefNo == invRef).ToList()
							};
						hdrs.Add(hdr);
					}
				}
				else hdrs.Add(header);
			}

			return hdrs;
		}

    }
}
