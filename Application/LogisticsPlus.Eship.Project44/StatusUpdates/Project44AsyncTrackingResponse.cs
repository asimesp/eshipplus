﻿using System;

namespace LogisticsPlus.Eship.Project44.StatusUpdates
{
    [Serializable]
    public class Project44AsyncTrackingResponse
    {
        public Project44AsyncShipmentStatus previousShipmentStatus { get; set; }

        public Project44AsyncShipmentStatus latestShipmentStatus { get; set; }

        public Project44AsyncTrackingResponse()
        {
            previousShipmentStatus = new Project44AsyncShipmentStatus();
            latestShipmentStatus = new Project44AsyncShipmentStatus();
        }
    }
}