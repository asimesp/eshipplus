﻿using System;
using System.Collections.Generic;

namespace LogisticsPlus.Eship.Project44.StatusUpdates
{
    [Serializable]
    public class Project44AsyncShipmentCapacityProviderAccountGroup
    {
        public string code { get; set; }

        public List<Project44AsyncShipmentCapacityProviderAccountGroup> accounts { get; set; }

        public Project44AsyncShipmentCapacityProviderAccountGroup()
        {
            code = string.Empty;
            accounts = new List<Project44AsyncShipmentCapacityProviderAccountGroup>();
        }
    }
}
