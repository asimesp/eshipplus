﻿using System;
using LogisticsPlus.Eship.Core;

namespace LogisticsPlus.Eship.Project44.StatusUpdates
{
    [Serializable]
    public class Project44AsyncTrackingTimeStamp
    {
        public DateTime date { get; set; }

        public string time { get; set; }

        public string timeZoneOffset { get; set; }

        public Project44AsyncTrackingTimeStamp()
        {
            date = DateUtility.SystemEarliestDateTime;
            time = string.Empty;
            timeZoneOffset = string.Empty;
        }
    }
}