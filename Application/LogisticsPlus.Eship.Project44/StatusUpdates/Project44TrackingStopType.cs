﻿namespace LogisticsPlus.Eship.Project44.StatusUpdates
{
    public static class Project44TrackingStopType
    {
        public static string Destination = "DESTINATION";
        public static string Origin = "ORIGIN";
        public static string Terminal = "TERMINAL";
    }
}
