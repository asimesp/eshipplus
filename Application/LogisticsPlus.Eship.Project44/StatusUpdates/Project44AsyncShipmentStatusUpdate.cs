﻿using System;
using LogisticsPlus.Eship.Core;

namespace LogisticsPlus.Eship.Project44.StatusUpdates
{
    [Serializable]
    public class Project44AsyncShipmentStatusUpdate
    {
        public Project44AsyncTrackingTimeStamp timestamp { get; set; }

        public DateTime retrievalDateTime{ get; set; }

        public string statusCode { get; set; }

        public Project44AsyncShipmentStatusUpdateReason statusReason { get; set; }

        public Project44AsyncShipmentAddress address { get; set; }

        public string stopId { get; set; }

        public string stopType { get; set; }

        public string stopNumber { get; set; }

        public Project44AsyncShipmentStatusUpdate()
        {
            timestamp = new Project44AsyncTrackingTimeStamp();
            retrievalDateTime = DateUtility.SystemEarliestDateTime;
            statusCode = string.Empty;
            statusReason = new Project44AsyncShipmentStatusUpdateReason();
            address = new Project44AsyncShipmentAddress();
            stopId = string.Empty;
            stopType = string.Empty;
            stopNumber = string.Empty;
        }

    }
}
