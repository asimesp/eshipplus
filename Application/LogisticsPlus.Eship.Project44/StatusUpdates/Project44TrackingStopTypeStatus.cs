﻿namespace LogisticsPlus.Eship.Project44.StatusUpdates
{
    public static class Project44TrackingStopTypeStatus
    {
        public static string Arrived = "ARRIVED";
        public static string Departed = "DEPARTED";
        public static string EnRoute = "EN_ROUTE";
        public static string Unknown = "UNKNOWN";
    }
}
