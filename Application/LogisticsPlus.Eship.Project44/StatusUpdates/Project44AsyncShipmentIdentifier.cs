﻿using System;

namespace LogisticsPlus.Eship.Project44.StatusUpdates
{
    [Serializable]
    public class Project44AsyncShipmentIdentifier
    {
        public string type { get; set; }
        public string value { get; set; }
        public bool primaryForType { get; set; }
        public string source{ get; set; }

        public Project44AsyncShipmentIdentifier()
        {
            type = string.Empty;
            value = string.Empty;
            primaryForType = false;
            source = string.Empty;;
        }
    }
}
