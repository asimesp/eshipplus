﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace LogisticsPlus.Eship.Project44.StatusUpdates
{
    [Serializable]
    public class Project44AsyncShipmentStatus
    {
        public Project44AsyncShipment shipment { get; set; }

        public Project44AsyncShipmentStatusUpdate latestStatusUpdate { get; set; }

        public List<Project44AsyncShipmentStatusUpdate> statusUpdates { get; set; }

        public List<Project44AsyncShipmentStatusImageReference> imageReferences { get; set; }

        public List<Project44AsyncShipmentStatusInfoMessage> infoMessages { get; set; }

        public Project44AsyncShipmentStatus()
        {
            shipment = new Project44AsyncShipment();
            latestStatusUpdate = new Project44AsyncShipmentStatusUpdate();
            statusUpdates = new List<Project44AsyncShipmentStatusUpdate>();
            imageReferences = new List<Project44AsyncShipmentStatusImageReference>();
            infoMessages = new List<Project44AsyncShipmentStatusInfoMessage>();
        }
    }
}
