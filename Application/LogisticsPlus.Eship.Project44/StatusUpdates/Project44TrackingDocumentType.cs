﻿namespace LogisticsPlus.Eship.Project44.StatusUpdates
{
    public static class Project44TrackingDocumentType
    {
        public static string BillOfLading = "BILL_OF_LADING";
        public static string DeliveryReceipt = "DELIVERY_RECEIPT";
        public static string WeightCertificate = "WEIGHT_CERTIFICATE";
        public static string Invoice = "INVOICE";
    }
}
