﻿using System;

namespace LogisticsPlus.Eship.Project44.StatusUpdates
{
    [Serializable]
    public class Project44AsyncShipmentStopLocation
    {
        public Project44AsyncShipmentAddress address { get; set; }

        public Project44AsyncShipmentStopContact contact { get; set; }

        public Project44AsyncShipmentStopLocation()
        {
            address = new Project44AsyncShipmentAddress();
            contact = new Project44AsyncShipmentStopContact();
        }
    }
}
