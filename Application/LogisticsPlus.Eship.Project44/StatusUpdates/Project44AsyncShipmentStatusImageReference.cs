﻿using System;

namespace LogisticsPlus.Eship.Project44.StatusUpdates
{
    [Serializable]
    public class Project44AsyncShipmentStatusImageReference
    {
        public string imageUrl { get; set; }

        public string documentType { get; set; }

        public string imageFormat { get; set; }

        public Project44AsyncShipmentStatusImageReference()
        {
            imageUrl = string.Empty;
            documentType = string.Empty;
            imageFormat = string.Empty;
        }
    }
}
