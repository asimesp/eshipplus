﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using Newtonsoft.Json;
using HttpResponse = System.Web.HttpResponse;

namespace LogisticsPlus.Eship.Project44.StatusUpdates
{
	public class TrackingServiceHandler
    {
		private readonly Project44ServiceSettings _settings;

		public List<string> Errors { get; private set; }

        public string ResponseBody { get; private set; }
        public Project44AsyncTrackingResponse ProcessTrackingUpdate(HttpRequest request, HttpResponse response)
        {
            var responseBody = string.Empty;
            try
            {
                string streamData;
                using (var reader = new StreamReader(request.InputStream))
                    streamData = reader.ReadToEnd();

                responseBody = streamData;
                var trackingData = JsonConvert.DeserializeObject<Project44AsyncTrackingResponse>(streamData,
                    new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore
                    });

                ResponseBody = responseBody;
                return trackingData;
            }
            catch (Exception e)
            {
                ResponseBody = responseBody;
                Errors.Add(e.ToString());
                return null;
            }
        }

        public TrackingServiceHandler()
        {
            Errors = new List<string>();
        }
    }
}
