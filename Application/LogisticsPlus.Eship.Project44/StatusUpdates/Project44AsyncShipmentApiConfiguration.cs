﻿using System;

namespace LogisticsPlus.Eship.Project44.StatusUpdates
{
    [Serializable]
    public class Project44AsyncShipmentApiConfiguration
    {
        public bool fallBackToDefaultAccountGroup { get; set; }

        public Project44AsyncShipmentApiConfiguration()
        {
            fallBackToDefaultAccountGroup = false;
        }
    }
}
