﻿using System;
using System.Collections.Generic;

namespace LogisticsPlus.Eship.Project44.StatusUpdates
{
    [Serializable]
    public class Project44AsyncShipment
    {
        public Project44AsyncShipmentCapacityProviderAccountGroup capacityProviderAccountGroup { get; set; }

        public List<Project44AsyncShipmentIdentifier> shipmentIdentifiers { get; set; }

        public List<Project44AsyncShipmentStop> shipmentStops { get; set; }

        public Project44AsyncShipmentApiConfiguration apiConfiguration { get; set; }

        public string id { get; set; }

        public Project44AsyncShipment()
        {
            id = string.Empty;
            capacityProviderAccountGroup = new Project44AsyncShipmentCapacityProviderAccountGroup();
            shipmentIdentifiers = new List<Project44AsyncShipmentIdentifier>();
            shipmentStops = new List<Project44AsyncShipmentStop>();
            apiConfiguration = new Project44AsyncShipmentApiConfiguration();
        }
    }
}
