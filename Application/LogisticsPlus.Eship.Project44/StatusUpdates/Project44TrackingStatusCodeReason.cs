﻿namespace LogisticsPlus.Eship.Project44.StatusUpdates
{
    public static class Project44TrackingStatusCodeReason
    {
        public static string Canceled = "CANCELED";
        public static string Delayed = "DELAYED";
        public static string Delivered = "DELIVERED";
        public static string DeliveryAppointment = "DELIVERY_APPOINTMENT";
        public static string DeliveryInfo = "DELIVERY_INFO";
        public static string DeliveryMissed = "DELIVERY_MISSED";
        public static string DepartureInfo = "DEPARTURE_INFO";
        public static string DepatureMissed = "DEPARTURE_MISSED";
        public static string Exception = "EXCEPTION";
        public static string Held = "HELD";
        public static string InterlineInfo = "INTERLINE_INFO";
        public static string InterlineMissed = "INTERLINE_MISSED";
        public static string PickupInfo = "PICKUP_INFO";
        public static string PickupMissed = "PICKUP_MISSED";
        public static string TimedOut = "TIMED_OUT";
    }
}
