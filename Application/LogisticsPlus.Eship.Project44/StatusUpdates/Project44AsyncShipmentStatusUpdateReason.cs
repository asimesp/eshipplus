﻿using System;

namespace LogisticsPlus.Eship.Project44.StatusUpdates
{
    [Serializable]
    public class Project44AsyncShipmentStatusUpdateReason
    {
        public string code { get; set; }

        public string description { get; set; }

        public Project44AsyncShipmentStatusUpdateReason()
        {
            code = string.Empty;
            description = string.Empty;
        }
    }
}
