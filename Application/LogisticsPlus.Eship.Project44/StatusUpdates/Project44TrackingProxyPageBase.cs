﻿using System.Collections.Generic;
using System.Linq;
using System.Web.UI;

namespace LogisticsPlus.Eship.Project44.StatusUpdates
{
    public abstract class Project44TrackingProxyPageBase : Page
    {
        public abstract void QueTrackingDataAsCheckCalls(Project44AsyncTrackingResponse data);
        public abstract void SaveP44TrackingInformation(Project44AsyncTrackingResponse data);

        public abstract void LogErrors(List<string> errors);

        public void ProcessTrackingResponse()
        {
            var handler = new TrackingServiceHandler();
            var data = handler.ProcessTrackingUpdate(Request, Response);

            if (handler.Errors.Any())
            {
                var errorMessages = handler.Errors.Select(m => string.Format("{0} \n\n\n Response Body: \n {1}", m, handler.ResponseBody)).ToList();
                LogErrors(errorMessages);
                return;
            }

            var responseSuccessful = data != null;



            // If response code is 200-7 or higher, there is insufficient information to validate the BOL (Appendix D of main EVA doc)
            if (responseSuccessful)
                QueTrackingDataAsCheckCalls(data);


            SaveP44TrackingInformation(data);

        }


    }
}
