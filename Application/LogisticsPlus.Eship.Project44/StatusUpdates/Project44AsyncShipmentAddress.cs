﻿using System;
using System.Collections.Generic;

namespace LogisticsPlus.Eship.Project44.StatusUpdates
{
    [Serializable]
    public class Project44AsyncShipmentAddress
    {
        public string postalCode { get; set; }

        public List<string> addressLines { get; set; }

        public string city { get; set; }

        public string state { get; set; }

        public string country { get; set; }

        public Project44AsyncShipmentAddress()
        {
            postalCode = string.Empty;
            addressLines = new List<string>();
            city = string.Empty;
            state = string.Empty;
            country = string.Empty;
        }
    }
}
