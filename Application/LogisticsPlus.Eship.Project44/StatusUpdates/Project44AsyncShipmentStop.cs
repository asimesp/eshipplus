﻿using System;

namespace LogisticsPlus.Eship.Project44.StatusUpdates
{
    [Serializable]
    public class Project44AsyncShipmentStop
    {
        public long id { get; set; }

        public string stopType { get; set; }

        public Project44AsyncShipmentStopLocation location { get; set; }

        public Project44AsyncShipmentStopAppointment appointmentWindow { get; set; }

        public string stopName { get; set; }

        public int stopNumber { get; set; }

        public Project44AsyncShipmentStop()
        {
            id = default(long);
            stopType = string.Empty;
            location = new Project44AsyncShipmentStopLocation();
            appointmentWindow = new Project44AsyncShipmentStopAppointment();
            stopName = string.Empty;
            stopNumber = default(int);
        }
    }
}
