﻿namespace LogisticsPlus.Eship.Project44.StatusUpdates
{
    public static class Project44TrackingStatusCode
    {
        public static string AtStop = "AT_STOP";
        public static string Completed = "COMPLETED";
        public static string InTransit = "IN_TRANSIT";
        public static string Info = "INFO";
        public static string OutToStop = "OUT_TO_STOP";
    }
}
