﻿using System;

namespace LogisticsPlus.Eship.Project44.StatusUpdates
{
    [Serializable]
    public class Project44AsyncShipmentStopContact
    {
        public string companyName { get; set; }
        public string contactName { get; set; }
        public string phoneNumber { get; set; }
        public string phoneNumber2 { get; set; }
        public string email { get; set; }
        public string faxNumber { get; set; }

        public Project44AsyncShipmentStopContact()
        {
            companyName = string.Empty;
            contactName = string.Empty;
            phoneNumber = string.Empty;
            phoneNumber2 = string.Empty;
            email = string.Empty;
            faxNumber = string.Empty;
        }
    }
}
