﻿using System;

namespace LogisticsPlus.Eship.Project44.StatusUpdates
{
    [Serializable]
    public class Project44AsyncShipmentStopAppointment
    {
        public long id { get; set; }
        public string stopType { get; set; }

        public Project44AsyncShipmentStopAppointment()
        {
            id = default(long);
            stopType = string.Empty;
        }
    }
}
