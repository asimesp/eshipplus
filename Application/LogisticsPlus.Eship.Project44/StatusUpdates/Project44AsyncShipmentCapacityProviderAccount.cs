﻿using System;

namespace LogisticsPlus.Eship.Project44.StatusUpdates
{
    [Serializable]
    public class Project44AsyncShipmentCapacityProviderAccount
    {
        public string code { get; set; }
    }
}
