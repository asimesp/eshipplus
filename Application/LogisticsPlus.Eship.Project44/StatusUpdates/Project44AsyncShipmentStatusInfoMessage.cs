﻿using System;

namespace LogisticsPlus.Eship.Project44.StatusUpdates
{
    [Serializable]
    public class Project44AsyncShipmentStatusInfoMessage
    {
        public string severity { get; set; }
        public string message { get; set; }
        public string diagnostic { get; set; }
        public string source { get; set; }

        public Project44AsyncShipmentStatusInfoMessage()
        {
            severity = string.Empty;
            message = string.Empty;
            diagnostic = string.Empty;
            source = string.Empty;
        }

    }
}
