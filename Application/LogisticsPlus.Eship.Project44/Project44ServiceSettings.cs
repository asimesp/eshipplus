﻿using System;

namespace LogisticsPlus.Eship.Project44
{
	[Serializable]
	public class Project44ServiceSettings
    {
		public string Username { get; set; }
		public string Password { get; set; }
		public string Hostname { get; set; }
		public string PrimaryLocationId { get; set; }


		public void Clear()
		{
		    Username = string.Empty;
			Password = string.Empty;
		    Hostname = string.Empty;
		    PrimaryLocationId = string.Empty;
		}
	}
}
