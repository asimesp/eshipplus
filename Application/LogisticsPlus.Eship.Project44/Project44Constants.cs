﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;
using P44SDK.V4.Model;

namespace LogisticsPlus.Eship.Project44
{
    public static class Project44Constants
    {
        public static List<Project44ChargeCode> FuelChargeCodes
        {
            get
            {
                return new List<Project44ChargeCode>
                {
                    Project44ChargeCode.AFSC,
                    Project44ChargeCode.FSC,
                    Project44ChargeCode.OFSC
                };
            }
        }

        public static List<ServiceLevel.CodeEnum?> GuaranteedServiceLevels
        {
            get
            {
                return new List<ServiceLevel.CodeEnum?>
                {
                    ServiceLevel.CodeEnum.GS10,
                    ServiceLevel.CodeEnum.GS11,
                    ServiceLevel.CodeEnum.GS14,
                    ServiceLevel.CodeEnum.GS15,
                    ServiceLevel.CodeEnum.GS1530,
                    ServiceLevel.CodeEnum.GS8,
                    ServiceLevel.CodeEnum.GS9,
                    ServiceLevel.CodeEnum.GSAM,
                    ServiceLevel.CodeEnum.GSFM,
                    ServiceLevel.CodeEnum.GSMUL,
                    ServiceLevel.CodeEnum.GSSING,
                    ServiceLevel.CodeEnum.GUR,
                    ServiceLevel.CodeEnum.GURWIN
                };
            }
        }

        public static List<ServiceLevel.CodeEnum?> ExpeditedServiceLevels
        {
            get
            {
                return new List<ServiceLevel.CodeEnum?>
                {
                    ServiceLevel.CodeEnum.EXPEDITE,
                    ServiceLevel.CodeEnum.EXPEDITE17,
                    ServiceLevel.CodeEnum.EXPEDITEAM
                };
            }
        }

        public static string GetGuaranteedServiceTime(this Project44ChargeCode code)
        {
            switch (code)
            {
                default:
                    return string.Empty;
                case Project44ChargeCode.GS10:
                    return "10:00 AM";
                case Project44ChargeCode.GS11:
                    return "11:00 AM";
                case Project44ChargeCode.GS14:
                    return "2:00 PM";
                case Project44ChargeCode.GS15:
                    return "3:00 PM";
                case Project44ChargeCode.GS1530:
                    return "3:30 PM";
                case Project44ChargeCode.GS8:
                    return "8:00 AM";
                case Project44ChargeCode.GS9:
                    return "9:00 AM";
                case Project44ChargeCode.GSAM:
                    return "AM (Noon)";
                case Project44ChargeCode.GSFM:
                    return "Friday to Monday";
                case Project44ChargeCode.GSMUL:
                    return "Multi-Hour Window";
                case Project44ChargeCode.GSSING:
                    return "Single-Hour Window";
                case Project44ChargeCode.GUR:
                    return "5:00 PM";
                case Project44ChargeCode.GURWIN:
                    return "Within Window";
            }
        }

        public static string GetExpeditedServiceTime(this Project44ChargeCode code)
        {
            switch (code)
            {
                default:
                    return string.Empty;
                case Project44ChargeCode.EXPEDITE:
                    return "Expedited";
                case Project44ChargeCode.EXPEDITEAM:
                    return "12:00 PM";
                case Project44ChargeCode.EXPEDITE17:
                    return "5:00 PM";
            }
        }


        public static LineItem.FreightClassEnum? GetFreightClass(this double freightClass)
        {
            if (freightClass <= 50)
                return LineItem.FreightClassEnum._50;
            if (freightClass == 55)
                return LineItem.FreightClassEnum._55;
            if (freightClass == 60)
                return LineItem.FreightClassEnum._60;
            if (freightClass == 65)
                return LineItem.FreightClassEnum._65;
            if (freightClass == 70)
                return LineItem.FreightClassEnum._70;
            if (freightClass == 77.5)
                return LineItem.FreightClassEnum._775;
            if (freightClass == 85)
                return LineItem.FreightClassEnum._85;
            if (freightClass == 92.5)
                return LineItem.FreightClassEnum._925;
            if (freightClass == 100)
                return LineItem.FreightClassEnum._100;
            if (freightClass == 110)
                return LineItem.FreightClassEnum._110;
            if (freightClass == 125)
                return LineItem.FreightClassEnum._125;
            if (freightClass == 150)
                return LineItem.FreightClassEnum._150;
            if (freightClass == 175)
                return LineItem.FreightClassEnum._175;
            if (freightClass == 200)
                return LineItem.FreightClassEnum._200;
            if (freightClass == 250)
                return LineItem.FreightClassEnum._250;
            if (freightClass == 300)
                return LineItem.FreightClassEnum._300;
            if (freightClass == 400)
                return LineItem.FreightClassEnum._400;
            if (freightClass == 500)
                return LineItem.FreightClassEnum._500;

            return LineItem.FreightClassEnum._50;
        }


        public static string GetServiceCodeDescription(this Project44ServiceCode code)
        {
            switch (code)
            {
                default:
                    return "N/A";
                case Project44ServiceCode.AIRPU:
                    return "Airport Pickup";
                case Project44ServiceCode.APPTPU:
                    return "Pickup Appointment";
                case Project44ServiceCode.CAMPPU:
                    return "Camp Pickup";
                case Project44ServiceCode.CHRCPU:
                    return "Church Pickup";
                case Project44ServiceCode.CLUBPU:
                    return "Country Club Pickup";
                case Project44ServiceCode.CNVPU:
                    return "Convention/Tradeshow Pickup";
                case Project44ServiceCode.CONPU:
                    return "Construction Site Pickup";
                case Project44ServiceCode.DOCKPU:
                    return "Dock Pickup";
                case Project44ServiceCode.EDUPU:
                    return "School Pickup";
                case Project44ServiceCode.FARMPU:
                    return "Farm Pickup";
                case Project44ServiceCode.GROPU:
                    return "Grocery Warehouse Pickup";
                case Project44ServiceCode.HOSPU:
                    return "Hospital Pickup";
                case Project44ServiceCode.HOTLPU:
                    return "Hotel Pickup";
                case Project44ServiceCode.INPU:
                    return "Inside Pickup";
                case Project44ServiceCode.LGPU:
                    return "Liftgate Pickup";
                case Project44ServiceCode.LTDPU:
                    return "Limited Access Pickup";
                case Project44ServiceCode.MILPU:
                    return "Military Installation Pickup";
                case Project44ServiceCode.MINEPU:
                    return "Mine Site Pickup";
                case Project44ServiceCode.NARPU:
                    return "Native American Reservation Pickup";
                case Project44ServiceCode.PARKPU:
                    return "Fair/Amusement/Park Pickup";
                case Project44ServiceCode.PIERPU:
                    return "Pier Pickup";
                case Project44ServiceCode.PRISPU:
                    return "Prison Pickup";
                case Project44ServiceCode.RESPU:
                    return "Residential Pickup";
                case Project44ServiceCode.SATPU:
                    return "Saturday Pickup";
                case Project44ServiceCode.SORTPU:
                    return "Sort/Segregate Pickup";
                case Project44ServiceCode.SSTORPU:
                    return "Self-Storage Pickup";
                case Project44ServiceCode.AIRDEL:
                    return "Airport Delivery";
                case Project44ServiceCode.APPT:
                    return "Delivery Appointment";
                case Project44ServiceCode.APPTDEL:
                    return "Delivery Appointment";
                case Project44ServiceCode.CAMPDEL:
                    return "Camp Delivery";
                case Project44ServiceCode.CHRCDEL:
                    return "Church Delivery";
                case Project44ServiceCode.CLUBDEL:
                    return "Country Club Delivery";
                case Project44ServiceCode.CNVDEL:
                    return "Convention/Tradeshow Delivery";
                case Project44ServiceCode.CONDEL:
                    return "Construction Site Delivery";
                case Project44ServiceCode.DCDEL:
                    return "Distribution Center Delivery";
                case Project44ServiceCode.EDUDEL:
                    return "School Delivery";
                case Project44ServiceCode.FARMDEL:
                    return "Farm Delivery";
                case Project44ServiceCode.GOVDEL:
                    return "Government Site Delivery";
                case Project44ServiceCode.GRODEL:
                    return "Grocery Warehouse Delivery";
                case Project44ServiceCode.HOSDEL:
                    return "Hospital Delivery";
                case Project44ServiceCode.HOTLDEL:
                    return "Hotel Delivery";
                case Project44ServiceCode.INDEL:
                    return "Inside Delivery";
                case Project44ServiceCode.INEDEL:
                    return "Inside Delivery With Elevator";
                case Project44ServiceCode.INGDEL:
                    return "Inside Delivery Ground Floor";
                case Project44ServiceCode.INNEDEL:
                    return "Inside Delivery No Elevator";
                case Project44ServiceCode.LGDEL:
                    return "Liftgate Delivery";
                case Project44ServiceCode.LTDDEL:
                    return "Limited Access Delivery";
                case Project44ServiceCode.MALLDEL:
                    return "Mall Delivery";
                case Project44ServiceCode.MILDEL:
                    return "Military Installation Delivery";
                case Project44ServiceCode.MINEDEL:
                    return "Mine Site Delivery";
                case Project44ServiceCode.NBDEL:
                    return "Non-Business Hours Delivery";
                case Project44ServiceCode.NCDEL:
                    return "Non-Commercial Delivery";
                case Project44ServiceCode.NOTIFY:
                    return "Delivery Notification";
                case Project44ServiceCode.PARKDEL:
                    return "Fair/Amusement/Park Delivery";
                case Project44ServiceCode.PIERDEL:
                    return "Pier Delivery";
                case Project44ServiceCode.PRISDEL:
                    return "Prison Delivery";
                case Project44ServiceCode.RESDEL:
                    return "Residential Delivery";
                case Project44ServiceCode.RSRTDEL:
                    return "Resort Delivery";
                case Project44ServiceCode.SATDEL:
                    return "Saturday Delivery";
                case Project44ServiceCode.SORTDEL:
                    return "Sort/Segregate Delivery";
                case Project44ServiceCode.SSTORDEL:
                    return "Self-Storage Delivery";
                case Project44ServiceCode.UNLOADDEL:
                    return "Unload at Destination";
                case Project44ServiceCode.WEDEL:
                    return "Weekend Delivery";
                case Project44ServiceCode.ACCELERATED:
                    return "Expedite Shipment Non-Guaranteed";
                case Project44ServiceCode.ARCHR:
                    return "Arbitrary Charge";
                case Project44ServiceCode.BLIND:
                    return "Blind Shipment";
                case Project44ServiceCode.CANFEE:
                    return "Northbound Canadian Border Crossing Document Handling Fee";
                case Project44ServiceCode.CAPLOAD:
                    return "Capacity Load";
                case Project44ServiceCode.COD:
                    return "COD";
                case Project44ServiceCode.CREDEL:
                    return "Redeliver to Consignee";
                case Project44ServiceCode.DRAY:
                    return "Drayage";
                case Project44ServiceCode.ELS:
                    return "Excessive Length Shipment";
                case Project44ServiceCode.ELS12:
                    return "Excessive Length, Over 12ft";
                case Project44ServiceCode.ELS12_20:
                    return "Excessive Length, 12 to 20ft";
                case Project44ServiceCode.ELS12_24:
                    return "Excessive Length, 12 to 24ft";
                case Project44ServiceCode.ELS13_27:
                    return "Excessive Length, 13 to 27ft";
                case Project44ServiceCode.ELS14:
                    return "Excessive Length, Over 14ft";
                case Project44ServiceCode.ELS14_24:
                    return "Excessive Length, 14 to 24ft";
                case Project44ServiceCode.ELS14_26:
                    return "Excessive Length, 14 to 26ft";
                case Project44ServiceCode.ELS15:
                    return "Excessive Length, Over 15ft";
                case Project44ServiceCode.ELS15_27:
                    return "Excessive Length, 15 to 27ft";
                case Project44ServiceCode.ELS20:
                    return "Excessive Length, Over 20ft";
                case Project44ServiceCode.ELS20_28:
                    return "Excessive Length, 20 to 28ft";
                case Project44ServiceCode.ELS21:
                    return "Excessive Length, Over 21ft";
                case Project44ServiceCode.ELS24:
                    return "Excessive Length, Over 24ft";
                case Project44ServiceCode.ELS27:
                    return "Excessive Length, Over 27ft";
                case Project44ServiceCode.ELS28:
                    return "Excessive Length, Over 28ft";
                case Project44ServiceCode.ELS30:
                    return "Excessive Length, Over 30ft";
                case Project44ServiceCode.ELS8_12:
                    return "Excessive Length, 8 to 12ft";
                case Project44ServiceCode.EV:
                    return "Excessive value";
                case Project44ServiceCode.EXPEDITE:
                    return "Expedite Shipment";
                case Project44ServiceCode.EXPEDITE17:
                    return "Expedite before 5pm";
                case Project44ServiceCode.EXPEDITEAM:
                    return "Expedite before 12pm";
                case Project44ServiceCode.EXPORT:
                    return "Export Shipment";
                case Project44ServiceCode.FRZABLE:
                    return "Freezable";
                case Project44ServiceCode.GS10:
                    return "Guaranteed by 10AM";
                case Project44ServiceCode.GS11:
                    return "Guaranteed by 11AM";
                case Project44ServiceCode.GS14:
                    return "Guaranteed by 2PM";
                case Project44ServiceCode.GS15:
                    return "Guaranteed by 3PM";
                case Project44ServiceCode.GS1530:
                    return "Guaranteed by 3:30PM";
                case Project44ServiceCode.GS8:
                    return "Guaranteed by 8AM";
                case Project44ServiceCode.GS9:
                    return "Guaranteed by 9AM";
                case Project44ServiceCode.GSAM:
                    return "Guaranteed by AM (Noon)";
                case Project44ServiceCode.GSMUL:
                    return "Guaranteed Multi-Hour Window";
                case Project44ServiceCode.GUR:
                    return "Guaranteed";
                case Project44ServiceCode.GURWIN:
                    return "Guaranteed Within Window";
                case Project44ServiceCode.HAWAII:
                    return "Hawaiian Will Call";
                case Project44ServiceCode.HAZM:
                    return "Hazmat";
                case Project44ServiceCode.IMPORT:
                    return "Import Shipment";
                case Project44ServiceCode.LUMPER:
                    return "Lumper Service";
                case Project44ServiceCode.MAINTEMP:
                    return "Maintain Temperature";
                case Project44ServiceCode.MARKING:
                    return "Marking or Tagging";
                case Project44ServiceCode.O750U6:
                    return "Over 750, Under 6";
                case Project44ServiceCode.OVDIM:
                    return "Over Dimension Freight";
                case Project44ServiceCode.PFH:
                    return "Protect From Heat";
                case Project44ServiceCode.PFZ:
                    return "Protect From Freezing";
                case Project44ServiceCode.POISON:
                    return "Poison (Hazmat B)";
                case Project44ServiceCode.RECON:
                    return "Reconsignment Fee";
                case Project44ServiceCode.SINGSHIP:
                    return "Single Shipment";
                case Project44ServiceCode.WINSPC:
                    return "Weight Inspection";
            }
        }


        public static Address.CountryEnum? ConvertCoreProject44CountryCodeToEnum(this Project44CountryCode code)
        {
            switch (code)
            {
                default:
                    return Address.CountryEnum.US;
                case Project44CountryCode.CA:
                    return Address.CountryEnum.CA;
                case Project44CountryCode.MX:
                    return Address.CountryEnum.MX;
            }
        }


        public static string GetCountryCodeDescription(this Project44CountryCode code)
        {
            switch (code)
            {
                default:
                    return "N/A";
                case Project44CountryCode.US:
                    return "United States";
                case Project44CountryCode.CA:
                    return "Canada";
                case Project44CountryCode.MX:
                    return "Mexico";
            }
        }

        public static string GetChargeCodeDescription(this Project44ChargeCode code)
        {
            switch (code)
            {
                case Project44ChargeCode.ACC:
                    return "Accessorial Charge";
                case Project44ChargeCode.ACCELERATED:
                    return "Accelerated";
                case Project44ChargeCode.ADD_CHRG:
                    return "Additional Charge";
                case Project44ChargeCode.ADVCOL:
                    return "Advanced Collection";
                case Project44ChargeCode.AFSC:
                    return "Air Fuel Surcharge";
                case Project44ChargeCode.AFTRHR:
                    return "After Hours Charge";
                case Project44ChargeCode.AFTRHRDEL:
                    return "After Hours Delivery";
                case Project44ChargeCode.AIR:
                    return "Air Freight";
                case Project44ChargeCode.AIRBAG:
                    return "Air Bag";
                case Project44ChargeCode.AIRDEL:
                    return "Airport Delivery";
                case Project44ChargeCode.AIRPU:
                    return "Airport Pickup";
                case Project44ChargeCode.APPT:
                    return "Delivery Appointment";
                case Project44ChargeCode.APPTDEL:
                    return "Delivery Appointment";
                case Project44ChargeCode.APPTPU:
                    return "Pickup Appointment";
                case Project44ChargeCode.ARCHR:
                    return "Arbitrary Charge";
                case Project44ChargeCode.ASWGHT:
                    return "Charged as Weight";
                case Project44ChargeCode.BAGLINER:
                    return "Bag Liner";
                case Project44ChargeCode.BLIND:
                    return "Blind Shipment";
                case Project44ChargeCode.BOBTAIL:
                    return "Bobtail";
                case Project44ChargeCode.BOND:
                    return "In Bond Shipment";
                case Project44ChargeCode.BORDCROSS:
                    return "Border Crossing Fee";
                case Project44ChargeCode.BULKHEAD:
                    return "Bulkhead";
                case Project44ChargeCode.BYDIND:
                    return "Beyond-Indirect Service";
                case Project44ChargeCode.CAMPDEL:
                    return "Camp Delivery";
                case Project44ChargeCode.CAMPPU:
                    return "Camp Pickup";
                case Project44ChargeCode.CANFEE:
                    return "Northbound Canadian Border Crossing Document Handling Fee";
                case Project44ChargeCode.CAPLOAD:
                    return "Capacity Load";
                case Project44ChargeCode.CAPLOADPUP:
                    return "Capacity Load Pup";
                case Project44ChargeCode.CAPLOADTRA:
                    return "Capacity Load Trailer";
                case Project44ChargeCode.CARBFEE:
                    return "CARB Compliance Fee";
                case Project44ChargeCode.CERT:
                    return "Certified Pickup fee";
                case Project44ChargeCode.CERTPU:
                    return "Certified Pickup";
                case Project44ChargeCode.CHASSIS:
                    return "Chassis Truck";
                case Project44ChargeCode.CHNGBOL:
                    return "Change of BOL";
                case Project44ChargeCode.CHRCDEL:
                    return "Church Delivery";
                case Project44ChargeCode.CHRCPU:
                    return "Church Pickup";
                case Project44ChargeCode.CLUBDEL:
                    return "Country Club Delivery";
                case Project44ChargeCode.CLUBPU:
                    return "Country Club Pickup";
                case Project44ChargeCode.CNTER20:
                    return "Container 20 Feet Long";
                case Project44ChargeCode.CNTER40:
                    return "Container 40 Feet Long";
                case Project44ChargeCode.CNTER40H:
                    return "Container 40H Feet Long";
                case Project44ChargeCode.CNTER45:
                    return "Container 45 Feet Long";
                case Project44ChargeCode.CNTER53:
                    return "Container 53 Feet Long";
                case Project44ChargeCode.CNV:
                    return "Convention/Tradeshow Pickup or Delivery";
                case Project44ChargeCode.CNVDEL:
                    return "Convention/Tradeshow Delivery";
                case Project44ChargeCode.CNVPU:
                    return "Convention/Tradeshow Pickup";
                case Project44ChargeCode.COD:
                    return "COD";
                case Project44ChargeCode.COMDEL:
                    return "Commercial Delivery";
                case Project44ChargeCode.COMPU:
                    return "Commercial Pickup";
                case Project44ChargeCode.CONDEL:
                    return "Construction Site Delivery";
                case Project44ChargeCode.CONPU:
                    return "Construction Site Pickup";
                case Project44ChargeCode.CONS:
                    return "Construction Site Pickup or Delivery";
                case Project44ChargeCode.CONSOL:
                    return "Consolidation";
                case Project44ChargeCode.CORBOL:
                    return "Corrected BOL";
                case Project44ChargeCode.CORFEE:
                    return "Correction/Verification Fee";
                case Project44ChargeCode.CRANE:
                    return "Crane Permits";
                case Project44ChargeCode.CREDEL:
                    return "Redeliver to Consignee";
                case Project44ChargeCode.CROSS:
                    return "Cross Dock";
                case Project44ChargeCode.CUSINBOND:
                    return "Customs or In-Bond Freight charge";
                case Project44ChargeCode.DCDEL:
                    return "Distribution Center Delivery";
                case Project44ChargeCode.DECVAL:
                    return "Declared Value Pickup or Delivery";
                case Project44ChargeCode.DEF:
                    return "Deficit Charge";
                case Project44ChargeCode.DELBY:
                    return "Delivery By Charge";
                case Project44ChargeCode.DELC:
                    return "Delivery Charge";
                case Project44ChargeCode.DELON:
                    return "Delivery On Charge";
                case Project44ChargeCode.DESCINS:
                    return "Description Inspection";
                case Project44ChargeCode.DET:
                    return "Detention";
                case Project44ChargeCode.DIMWGHT:
                    return "Dimensional Weight";
                case Project44ChargeCode.DLYCHRG:
                    return "Delay Charge";
                case Project44ChargeCode.DOCFEE:
                    return "Document Fee";
                case Project44ChargeCode.DOCKDEL:
                    return "Dock Delivery";
                case Project44ChargeCode.DOCKPU:
                    return "Dock Pickup";
                case Project44ChargeCode.DRASST:
                    return "Driver Assistance";
                case Project44ChargeCode.DRAY:
                    return "Drayage";
                case Project44ChargeCode.DROPPULL:
                    return "Drop Pull";
                case Project44ChargeCode.DRYRU:
                    return "Dry Run";
                case Project44ChargeCode.DSC:
                    return "Discount";
                case Project44ChargeCode.DVALDEL:
                    return "Declared Value Delivery";
                case Project44ChargeCode.DVALPU:
                    return "Declared Value Pickup";
                case Project44ChargeCode.EDU:
                    return "School Pickup or Delviery";
                case Project44ChargeCode.EDUDEL:
                    return "School Delivery";
                case Project44ChargeCode.EDUPU:
                    return "School Pickup";
                case Project44ChargeCode.ELS:
                    return "Excessive Length Shipment";
                case Project44ChargeCode.ELS12:
                    return "Excessive Length, Over 12ft";
                case Project44ChargeCode.ELS12_20:
                    return "Excessive Length, 12 to 20ft";
                case Project44ChargeCode.ELS12_24:
                    return "Excessive Length, 12 to 24ft";
                case Project44ChargeCode.ELS13_27:
                    return "Excessive Length, 13 to 27ft";
                case Project44ChargeCode.ELS14:
                    return "Excessive Length, Over 14ft";
                case Project44ChargeCode.ELS14_24:
                    return "Excessive Length, 14 to 24ft";
                case Project44ChargeCode.ELS14_26:
                    return "Excessive Length, 14 to 26ft";
                case Project44ChargeCode.ELS15:
                    return "Excessive Length, Over 15ft";
                case Project44ChargeCode.ELS15_27:
                    return "Excessive Length, 15 to 27ft";
                case Project44ChargeCode.ELS20:
                    return "Excessive Length, Over 20ft";
                case Project44ChargeCode.ELS20_28:
                    return "Excessive Length, 20 to 28ft";
                case Project44ChargeCode.ELS21:
                    return "Excessive Length, Over 21ft";
                case Project44ChargeCode.ELS24:
                    return "Excessive Length, Over 24ft";
                case Project44ChargeCode.ELS27:
                    return "Excessive Length, Over 27ft";
                case Project44ChargeCode.ELS28:
                    return "Excessive Length, Over 28ft";
                case Project44ChargeCode.ELS30:
                    return "Excessive Length, Over 30ft";
                case Project44ChargeCode.ELS8_12:
                    return "Excessive Length, 8 to 12ft";
                case Project44ChargeCode.EMERGENCYDEL:
                    return "Emergency Delivery";
                case Project44ChargeCode.EV:
                    return "Excessive value";
                case Project44ChargeCode.EXCHG:
                    return "Pallet Exchange Pickup or Delivery";
                case Project44ChargeCode.EXLIAB:
                    return "Excess Liability Coverage Charge";
                case Project44ChargeCode.EXPEDITE:
                    return "Expedite Shipment";
                case Project44ChargeCode.EXPEDITE17:
                    return "Expedite before 5pm";
                case Project44ChargeCode.EXPEDITEAM:
                    return "Expedite before 12pm";
                case Project44ChargeCode.EXPORT:
                    return "Export Shipment";
                case Project44ChargeCode.FARM:
                    return "Farm Pickup or Delivery";
                case Project44ChargeCode.FARMDEL:
                    return "Farm Delivery";
                case Project44ChargeCode.FARMPU:
                    return "Farm Pickup";
                case Project44ChargeCode.FBEDDEL:
                    return "Flatbed Delivery";
                case Project44ChargeCode.FBEDPU:
                    return "Flatbed Pickup";
                case Project44ChargeCode.FERRY:
                    return "Ferry Charge";
                case Project44ChargeCode.FINSUR:
                    return "Full Value Insurance Charge";
                case Project44ChargeCode.FLTTRCK:
                    return "Flat Track";
                case Project44ChargeCode.FOOD:
                    return "Shipment Contains Food";
                case Project44ChargeCode.FORK:
                    return "Forklift";
                case Project44ChargeCode.FRIDGE:
                    return "Refrigerate";
                case Project44ChargeCode.FRZABLE:
                    return "Freezable";
                case Project44ChargeCode.FSC:
                    return "Fuel Surcharge";
                case Project44ChargeCode.GFC:
                    return "Gross Freight Charge";
                case Project44ChargeCode.GOV:
                    return "Government Site Pickup or Delivery";
                case Project44ChargeCode.GOVDEL:
                    return "Government Site Delivery";
                case Project44ChargeCode.GOVPU:
                    return "Government Site Pickup";
                case Project44ChargeCode.GRO:
                    return "Grocery Warehouse Pickup or Delivery";
                case Project44ChargeCode.GRODEL:
                    return "Grocery Warehouse Delivery";
                case Project44ChargeCode.GROPU:
                    return "Grocery Warehouse Pickup";
                case Project44ChargeCode.GS10:
                    return "Guaranteed by 10AM";
                case Project44ChargeCode.GS11:
                    return "Guaranteed by 11AM";
                case Project44ChargeCode.GS14:
                    return "Guaranteed by 2PM";
                case Project44ChargeCode.GS15:
                    return "Guaranteed by 3PM";
                case Project44ChargeCode.GS1530:
                    return "Guaranteed by 3:30PM";
                case Project44ChargeCode.GS8:
                    return "Guaranteed by 8AM";
                case Project44ChargeCode.GS9:
                    return "Guaranteed by 9AM";
                case Project44ChargeCode.GSAM:
                    return "Guaranteed by AM (Noon)";
                case Project44ChargeCode.GSFM:
                    return "Guaranteed Friday to Monday";
                case Project44ChargeCode.GSMUL:
                    return "Guaranteed Multi-Hour Window";
                case Project44ChargeCode.GSSING:
                    return "Guaranteed Single-Hour Window";
                case Project44ChargeCode.GST:
                    return "Goods and Services Tax";
                case Project44ChargeCode.GUR:
                    return "Guaranteed";
                case Project44ChargeCode.GURWIN:
                    return "Guaranteed Within Window";
                case Project44ChargeCode.HAWAII:
                    return "Hawaiian Will Call";
                case Project44ChargeCode.HAZM:
                    return "Hazmat";
                case Project44ChargeCode.HCD:
                    return "High Cost Delivery";
                case Project44ChargeCode.HCP:
                    return "High Cost Pickup";
                case Project44ChargeCode.HCSER:
                    return "High Cost Service Area";
                case Project44ChargeCode.HDAYDEL:
                    return "Holiday Delivery";
                case Project44ChargeCode.HDAYPU:
                    return "Holiday Pickup";
                case Project44ChargeCode.HEAT:
                    return "Heat";
                case Project44ChargeCode.HIGHDOCK:
                    return "High Dock Equipment Needed";
                case Project44ChargeCode.HMLAND:
                    return "HomeLand Security";
                case Project44ChargeCode.HOLIWEEK:
                    return "Holiday/Weekend Service";
                case Project44ChargeCode.HOSDEL:
                    return "Hospital Delivery";
                case Project44ChargeCode.HOSPU:
                    return "Hospital Pickup";
                case Project44ChargeCode.HOTLDEL:
                    return "Hotel Delivery";
                case Project44ChargeCode.HOTLPU:
                    return "Hotel Pickup";
                case Project44ChargeCode.HST:
                    return "Harmonized Sales Tax";
                case Project44ChargeCode.IMPORT:
                    return "Import Shipment";
                case Project44ChargeCode.INBND:
                    return "Inbound Freight";
                case Project44ChargeCode.INDEL:
                    return "Inside Delivery";
                case Project44ChargeCode.INEDEL:
                    return "Inside Delivery With Elevator";
                case Project44ChargeCode.INGDEL:
                    return "Inside Delivery Ground Floor";
                case Project44ChargeCode.INNEDEL:
                    return "Inside Delivery No Elevator";
                case Project44ChargeCode.INPU:
                    return "Inside Pickup";
                case Project44ChargeCode.INS:
                    return "Inside Pickup or Delivery";
                case Project44ChargeCode.INT:
                    return "Interline Shipment";
                case Project44ChargeCode.IREG:
                    return "Inter-Regional Shipment";
                case Project44ChargeCode.ITEM:
                    return "Item Charge";
                case Project44ChargeCode.LABEL:
                    return "Handling and Labeling";
                case Project44ChargeCode.LGDEL:
                    return "Liftgate Delivery";
                case Project44ChargeCode.LGPU:
                    return "Liftgate Pickup";
                case Project44ChargeCode.LGSER:
                    return "Liftgate Pickup or Delivery";
                case Project44ChargeCode.LH:
                    return "Linehaul";
                case Project44ChargeCode.LIQUOR:
                    return "Liquor Permit";
                case Project44ChargeCode.LOAD:
                    return "Driver Load";
                case Project44ChargeCode.LOADCNT:
                    return "Driver Load and Count";
                case Project44ChargeCode.LOADUNLOAD:
                    return "Driver Load/Unload";
                case Project44ChargeCode.LTDACC:
                    return "Limited Access Pickup or Delivery";
                case Project44ChargeCode.LTDDEL:
                    return "Limited Access Delivery";
                case Project44ChargeCode.LTDPU:
                    return "Limited Access Pickup";
                case Project44ChargeCode.LUMPER:
                    return "Lumper Service";
                case Project44ChargeCode.LYOVR:
                    return "Layover";
                case Project44ChargeCode.MAINTEMP:
                    return "Maintain Temperature";
                case Project44ChargeCode.MALL:
                    return "Mall Pickup or Delivery";
                case Project44ChargeCode.MALLDEL:
                    return "Mall Delivery";
                case Project44ChargeCode.MALLPU:
                    return "Mall Pickup";
                case Project44ChargeCode.MARKING:
                    return "Marking or Tagging";
                case Project44ChargeCode.MILDEL:
                    return "Military Installation Delivery";
                case Project44ChargeCode.MILPU:
                    return "Military Installation Pickup";
                case Project44ChargeCode.MIN:
                    return "Minimum Charge";
                case Project44ChargeCode.MINEDEL:
                    return "Mine Site Delivery";
                case Project44ChargeCode.MINEPU:
                    return "Mine Site Pickup";
                case Project44ChargeCode.NARDEL:
                    return "Native American Reservation Delivery";
                case Project44ChargeCode.NARPU:
                    return "Native American Reservation Pickup";
                case Project44ChargeCode.NBDEL:
                    return "Non-Business Hours Delivery";
                case Project44ChargeCode.NBPU:
                    return "Non-Business Hours Pickup";
                case Project44ChargeCode.NCDEL:
                    return "Non-Commercial Delivery";
                case Project44ChargeCode.NCPU:
                    return "Non-Commercial Pickup";
                case Project44ChargeCode.NCSER:
                    return "Non-Commercial Pickup or Delivery";
                case Project44ChargeCode.NFC:
                    return "Net Freight Charge";
                case Project44ChargeCode.NOTIFY:
                    return "Notify Before Delivery";
                case Project44ChargeCode.NURSDEL:
                    return "Nursing Home Delivery";
                case Project44ChargeCode.NURSPU:
                    return "Nursing Home Pickup";
                case Project44ChargeCode.O750U6:
                    return "Over 750, Under 6";
                case Project44ChargeCode.OCEAN:
                    return "Ocean Charge";
                case Project44ChargeCode.OFSC:
                    return "Ocean Fuel Surcharge";
                case Project44ChargeCode.OILSANDS:
                    return "Oil Sands";
                case Project44ChargeCode.OPENTOP:
                    return "Open Top Truck";
                case Project44ChargeCode.OVDIM:
                    return "Over Dimension Freight";
                case Project44ChargeCode.OVERWGHT:
                    return "Overweight";
                case Project44ChargeCode.OVR_UNDR:
                    return "Over Weight Under Feet";
                case Project44ChargeCode.OVSZEDEL:
                    return "Oversized Delivery";
                case Project44ChargeCode.OVSZEPU:
                    return "Oversized Pickup";
                case Project44ChargeCode.PALLET:
                    return "Pallet";
                case Project44ChargeCode.PAPER:
                    return "Paper Invoice Fee";
                case Project44ChargeCode.PARKDEL:
                    return "Fair/Amusement/Park Delivery";
                case Project44ChargeCode.PARKPU:
                    return "Fair/Amusement/Park Pickup";
                case Project44ChargeCode.PFH:
                    return "Protect From Heat";
                case Project44ChargeCode.PFZ:
                    return "Protect From Freezing";
                case Project44ChargeCode.PFZC:
                    return "Protect From Freezing Canada";
                case Project44ChargeCode.PIERDEL:
                    return "Pier Delivery";
                case Project44ChargeCode.PIERPU:
                    return "Pier Pickup";
                case Project44ChargeCode.PJACK:
                    return "Pallet Jack Pickup or Delivery";
                case Project44ChargeCode.PJACKDEL:
                    return "Pallet Jack Delivery";
                case Project44ChargeCode.PJACKPU:
                    return "Pallet Jack Pickup";
                case Project44ChargeCode.PKGFEE:
                    return "Packaging Fee";
                case Project44ChargeCode.POISON:
                    return "Poison (Hazmat B)";
                case Project44ChargeCode.PORTCON:
                    return "Port Congestion Charge";
                case Project44ChargeCode.PRISDEL:
                    return "Prison Delivery";
                case Project44ChargeCode.PRISON:
                    return "Prison Pickup or Delivery";
                case Project44ChargeCode.PRISPU:
                    return "Prison Pickup";
                case Project44ChargeCode.PRMIT:
                    return "Permit";
                case Project44ChargeCode.PROTECT:
                    return "Protective Service";
                case Project44ChargeCode.PROTECTDRM:
                    return "Protective Drum Cover";
                case Project44ChargeCode.PROTECTPAL:
                    return "Protective Pallet Cover";
                case Project44ChargeCode.PST:
                    return "Provencial Sales Tax";
                case Project44ChargeCode.PUC:
                    return "Pickup Charge";
                case Project44ChargeCode.QST:
                    return "Quebec Sales Tax";
                case Project44ChargeCode.RECON:
                    return "Reconsignment Fee";
                case Project44ChargeCode.REDEL:
                    return "Redelivery Fee";
                case Project44ChargeCode.REEFER:
                    return "Refrigerated Van";
                case Project44ChargeCode.REG:
                    return "Regional Shipment";
                case Project44ChargeCode.RES:
                    return "Residential Pickup or Delivery";
                case Project44ChargeCode.RESDEL:
                    return "Residential Delivery";
                case Project44ChargeCode.RESPU:
                    return "Residential Pickup";
                case Project44ChargeCode.RMVDEB:
                    return "Remove Debris";
                case Project44ChargeCode.RSRTDEL:
                    return "Resort Delivery";
                case Project44ChargeCode.RSRTPU:
                    return "Resort Pickup";
                case Project44ChargeCode.SATDEL:
                    return "Saturday Delivery";
                case Project44ChargeCode.SATPU:
                    return "Saturday Pickup";
                case Project44ChargeCode.SECINS:
                    return "Security Inspection";
                case Project44ChargeCode.SHIPDIV:
                    return "Secure Shipment Divider (Bulkhead) Charge";
                case Project44ChargeCode.SIGN:
                    return "Requires Signature";
                case Project44ChargeCode.SINGSHIP:
                    return "Single Shipment";
                case Project44ChargeCode.SORT:
                    return "Sort and/or Segregate Pickup or Delivery";
                case Project44ChargeCode.SORTDEL:
                    return "Sort/Segregate Delivery";
                case Project44ChargeCode.SORTPU:
                    return "Sort/Segregate Pickup";
                case Project44ChargeCode.SPECIALEQUIP:
                    return "Special Equipment Needed";
                case Project44ChargeCode.SSTORDEL:
                    return "Self-Storage Delivery";
                case Project44ChargeCode.SSTORPU:
                    return "Self-Storage Pickup";
                case Project44ChargeCode.STD:
                    return "Standard Rate";
                case Project44ChargeCode.STORAGE:
                    return "Storage";
                case Project44ChargeCode.STPOFF:
                    return "Stop Off";
                case Project44ChargeCode.STRAIGHT:
                    return "Straight Truck";
                case Project44ChargeCode.SUNDEL:
                    return "Sunday Delivery";
                case Project44ChargeCode.SUNPU:
                    return "Sunday Pickup";
                case Project44ChargeCode.TANK:
                    return "Tanker Truck";
                case Project44ChargeCode.TARP:
                    return "Tarp Charge";
                case Project44ChargeCode.TAX:
                    return "Tax";
                case Project44ChargeCode.TCKNU:
                    return "Truck Not Used";
                case Project44ChargeCode.TMCHG:
                    return "Team Charge";
                case Project44ChargeCode.TOLL:
                    return "Toll Charge";
                case Project44ChargeCode.TPBOXCHARGE:
                    return "TruckPack Charge per Box";
                case Project44ChargeCode.TPDELCHARGE:
                    return "TruckPack Delivery Charge";
                case Project44ChargeCode.TRIAXLE:
                    return "Triaxle Trailer Charge";
                case Project44ChargeCode.TSAC:
                    return "TSA Certification";
                case Project44ChargeCode.TWOMAN:
                    return "Two Man Delivery Required";
                default:
                case Project44ChargeCode.UNKCHRG:
                    return "Unknown Charge";
                case Project44ChargeCode.UNLOAD:
                    return "Driver Unload";
                case Project44ChargeCode.UNLOADCNT:
                    return "Driver Unload and Count";
                case Project44ChargeCode.UNLOADDEL:
                    return "Driver Unload at Destination";
                case Project44ChargeCode.UNPCK:
                    return "Unpacking";
                case Project44ChargeCode.UTL:
                    return "Utility Site Pickup or Delivery";
                case Project44ChargeCode.UTLDEL:
                    return "Utility Site Delivery";
                case Project44ChargeCode.UTLPU:
                    return "Utility Site Pickup";
                case Project44ChargeCode.WEDEL:
                    return "Weekend Delivery";
                case Project44ChargeCode.WEPU:
                    return "Weekend Pickup";
                case Project44ChargeCode.WESER:
                    return "Weekend Service";
                case Project44ChargeCode.WGHT:
                    return "Weight Fee";
                case Project44ChargeCode.WGTV:
                    return "Weight Verification Fee";
                case Project44ChargeCode.WHITEGLOVE:
                    return "White Glove Service";
                case Project44ChargeCode.WINSPC:
                    return "Weight Inspection";
                case Project44ChargeCode.WIRE:
                    return "Wire Transfer Fee";
                case Project44ChargeCode.WVERI:
                    return "Weight Verification";
                case Project44ChargeCode.XCHGDEL:
                    return "Pallet Exchange Delivery";
                case Project44ChargeCode.XCHGPU:
                    return "Pallet Exchange Pickup";
                case Project44ChargeCode.XFERDEL:
                    return "Dock Transfer Delivery";
                case Project44ChargeCode.XFERPU:
                    return "Dock Transfer Pickup";
                case Project44ChargeCode.XTRLBR:
                    return "Extra Labor";
            }
        }

        public static string GetPackageTypeDescription(this LineItem.PackageTypeEnum packageType)
        {
            switch (packageType)
            {
                case LineItem.PackageTypeEnum.PLT:
                    return "Pallet";
                case LineItem.PackageTypeEnum.BAG:
                    return "Bag";
                case LineItem.PackageTypeEnum.BALE:
                    return "Bale";
                case LineItem.PackageTypeEnum.BOX:
                    return "Box";
                case LineItem.PackageTypeEnum.BUCKET:
                    return "Bucket";
                case LineItem.PackageTypeEnum.PAIL:
                    return "Pail";
                case LineItem.PackageTypeEnum.BUNDLE:
                    return "Bundle";
                case LineItem.PackageTypeEnum.CAN:
                    return "Can";
                case LineItem.PackageTypeEnum.CARTON:
                    return "Carton";
                case LineItem.PackageTypeEnum.CASE:
                    return "Case";
                case LineItem.PackageTypeEnum.COIL:
                    return "Coil";
                case LineItem.PackageTypeEnum.CRATE:
                    return "Crate";
                case LineItem.PackageTypeEnum.CYLINDER:
                    return "Cylinder";
                case LineItem.PackageTypeEnum.DRUM:
                    return "Drum";
                case LineItem.PackageTypeEnum.PIECES:
                    return "Pieces";
                case LineItem.PackageTypeEnum.REEL:
                    return "Reel";
                case LineItem.PackageTypeEnum.ROLL:
                    return "Roll";
                case LineItem.PackageTypeEnum.SKID:
                    return "Skid";
                case LineItem.PackageTypeEnum.TUBE:
                    return "Tube";
                default:
                    return string.Empty;
            }
        }
    }
}
