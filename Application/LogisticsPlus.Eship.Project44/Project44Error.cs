﻿namespace LogisticsPlus.Eship.Project44
{
    public class Project44Error
    {
        public string severity { get; set; }
        public string message { get; set; }
        public string diagnostic { get; set; }
        public string source { get; set; }
    }
}
