﻿using System.Collections.Generic;

namespace LogisticsPlus.Eship.Project44
{
    public class Project44ErrorResponse
    {
        public string httpStatusCode { get; set; }

        public string httpMessage { get; set; }

        public List<Project44Error> errors { get; set; }

        public string supportReferenceId { get; set; }
    }
}
