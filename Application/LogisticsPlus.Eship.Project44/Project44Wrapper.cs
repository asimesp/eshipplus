﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using Newtonsoft.Json;
using P44SDK.V4.Api;
using P44SDK.V4.Client;
using P44SDK.V4.Model;
using Contact = P44SDK.V4.Model.Contact;
using Location = P44SDK.V4.Model.Location;
using Shipment = LogisticsPlus.Eship.Core.Operations.Shipment;

namespace LogisticsPlus.Eship.Project44
{
    public class Project44Wrapper
    {
        public List<string> ErrorMessages { get; set; }

        private readonly string _hostName;
        private readonly string _userName;
        private readonly string _password;
        private readonly string _primaryLocationId;

        public Project44Wrapper(Project44ServiceSettings settings)
        {
            _hostName = settings.Hostname;
            _userName = settings.Username;
            _password = settings.Password;
            _primaryLocationId = settings.PrimaryLocationId;
            ErrorMessages = new List<string>();
        }

        public LtlDispatchedShipmentConfirmation DispatchShipment(Shipment shipment, string vendorScac)
        {
            var dispatchApi = new LTLDispatchApi(_hostName)
            {
                Configuration =
                {
                    Username = _userName,
                    Password = _password
                }
            };

            var p44Shipment = GenerateProject44ShipmentForDispatch(shipment, vendorScac);

            try
            {
                var dispatchResponse = dispatchApi.CreateShipment(p44Shipment);
                return dispatchResponse;
            }
            catch (ApiException ex)
            {
                var body = (string) ex.ErrorContent;

                var error = JsonConvert.DeserializeObject<Project44ErrorResponse>(body);
                ErrorMessages.AddRange(error.errors.Select(e => string.Format("{0} - {1}", e.message, e.diagnostic)));
                return null;
            }
            
        }

        public LtlShipmentCancellationConfirmation CancelDispatchedShipment(Shipment shipment, string pickupNumber)
        {
            var dispatchApi = new LTLDispatchApi(_hostName)
            {
                Configuration =
                {
                    Username = _userName,
                    Password = _password
                }
            };

            var capacityGroup = new CapacityProviderAccountGroup(string.Empty, new List<CapacityProviderAccount>
            {
                new CapacityProviderAccount(shipment.Vendors.First(v => v.Primary).Vendor.Scac)
            });

            var originAddress = new Address(
                shipment.Origin.PostalCode, new List<string>
                {
                    shipment.Origin.Street1,
                    shipment.Origin.Street2
                }, shipment.Origin.City, shipment.Origin.State, shipment.Origin.Country.Project44CountryCode.ConvertCoreProject44CountryCodeToEnum());

            var cancellationRequest = new LtlShipmentCancellation(capacityGroup, originAddress, new List<ShipmentIdentifier>
            {
                new ShipmentIdentifier(ShipmentIdentifier.TypeEnum.PRO, shipment.Vendors.First(v => v.Primary).ProNumber)
            });

            try
            {
                var cancelResponse = dispatchApi.CancelShipment(cancellationRequest);
                return cancelResponse;
            }
            catch (ApiException ex)
            {
                var body = (string)ex.ErrorContent;

                var error = JsonConvert.DeserializeObject<Project44ErrorResponse>(body);
                ErrorMessages.AddRange(error.errors.Select(e => string.Format("{0} - {1}", e.message, e.diagnostic)));
                return null;
            }

        }

        public List<RateQuote> GetRates(Shipment shipment, List<string> vendorScacs, string accountGroup)
        {
            var quotesApi = new LTLQuotesApi(_hostName)
            {
                Configuration =
                {
                    Username = _userName,
                    Password = _password
                }
            };


            var origin = new Address(shipment.Origin.PostalCode, new List<string>(), shipment.Origin.City, shipment.Origin.State, shipment.Origin.Country.Project44CountryCode.ConvertCoreProject44CountryCodeToEnum());

            var destination = new Address(shipment.Destination.PostalCode, new List<string>(), shipment.Destination.City, shipment.Destination.State, shipment.Destination.Country.Project44CountryCode.ConvertCoreProject44CountryCodeToEnum());

            var lineItems = shipment.Items.Select(i => new LineItem
                 (i.ActualWeight,
                new CubicDimension(i.ActualLength, i.ActualWidth, i.ActualHeight),
                i.ActualFreightClass.GetFreightClass(),
                (LineItem.PackageTypeEnum) Enum.Parse(typeof(LineItem.PackageTypeEnum),
                    i.PackageType != null?i.PackageType.Project44Code.ToString(): default(int).ToString() ),
                i.Quantity, i.PieceCount < 1 ? 1 : i.PieceCount, i.Description, i.IsStackable, i.NMFCCode,
                string.Empty)).ToList();

            var capacityGroup = new CapacityProviderAccountGroup(accountGroup, vendorScacs.Select(s =>  new CapacityProviderAccount(s)).ToList());


			//TODO: use p44service mapping to get correct codes
            var accessorials = shipment.Services.Select(s => new AccessorialService(s.Service.Project44Code.ToString())).ToList();

            var rateRequest = new RateQuoteQuery(origin, destination, lineItems, capacityGroup,
                PickupWindow: new LocalDateTimeWindow(shipment.DesiredPickupDate.ToString("yyyy-MM-dd"),
                    shipment.EarlyPickup, shipment.LatePickup),
                AccessorialServices: accessorials,
                ApiConfiguration: new RateQuoteApiConfiguration
                {
                    AccessorialServiceConfiguration = new AccessorialServiceConfiguration
                    {
                        FetchAllServiceLevels = true
                    }
                });

            try
            {
                var rateResults = quotesApi.QueryRateQuotes(rateRequest);
                return rateResults.RateQuotes;
            }
            catch (ApiException ex)
            {
                var body = (string)ex.ErrorContent;

                var error = JsonConvert.DeserializeObject<Project44ErrorResponse>(body);
                ErrorMessages.AddRange(error.errors.Select(e => $"{e.message} - {e.diagnostic}"));
                return null;
            }
        }

        public List<RateQuote> GetRates(LaneRateRequest request)
        {
            var quotesApi = new LTLQuotesApi(_hostName)
            {
                Configuration =
                {
                    Username = _userName,
                    Password = _password
                }
            };


            var origin = new Address(request.OriginPostalCode, Country:  request.OriginCountry);
            var destination = new Address(request.DestinationPostalCode, Country:  request.DestinationCountry);

            var lineItems = new List<LineItem>
            {
                new LineItem(request.Weight,
                    new CubicDimension(request.Length, request.Width, request.Height),
                    request.FreightClass.GetFreightClass(),
                    LineItem.PackageTypeEnum.PLT, 1, 1, string.Empty, false, string.Empty,
                    string.Empty)
            };

            var rateRequest = new RateQuoteQuery(origin, destination, lineItems);

            try
            {
                var rateResults = quotesApi.QueryRateQuotes(rateRequest);
                return rateResults.RateQuotes;
            }
            catch (ApiException ex)
            {
                var body = (string)ex.ErrorContent;

                var error = JsonConvert.DeserializeObject<Project44ErrorResponse>(body);
                ErrorMessages.AddRange(error.errors.Select(e => $"{e.message} - {e.diagnostic}"));
                return null;
            }
        }

        public LtlTrackedShipmentConfirmation InitiateTracking(Shipment shipment, string vendorScac)
        {
            var ltlTrackingApi = new LTLTrackingApi(_hostName)
            {
                Configuration =
                {
                    Username = _userName,
                    Password = _password
                }
            };

            var p44Shipment = GenerateProject44ShipmentForTracking(shipment, vendorScac);

            try
            {
                var trackingResponse = ltlTrackingApi.CreateShipment(p44Shipment);
                return trackingResponse;
            }
            catch (ApiException ex)
            {
                var body = (string)ex.ErrorContent;

                var error = JsonConvert.DeserializeObject<Project44ErrorResponse>(body);
                ErrorMessages.AddRange(error.errors.Select(e => $"{e.message} - {e.diagnostic}"));
                return null;
            }
        }

        public CapacityProviderAccountGroupInfoCollection GetCapacityProviderGroups()
        {
            var groupsApi = new CapacityProviderAccountGroupManagementApi(_hostName)
            {
                Configuration =
                {
                    Username = _userName,
                    Password = _password
                }
            };

            try
            {
                var groups = groupsApi.QueryCapacityProviderAccountGroup();
                return groups;
            }
            catch (ApiException ex)
            {
                var body = (string)ex.ErrorContent;

                var error = JsonConvert.DeserializeObject<Project44ErrorResponse>(body);
                ErrorMessages.AddRange(error.errors.Select(e => $"{e.message} - {e.diagnostic}"));
                return null;
            }
        }

        private LtlTrackedShipment GenerateProject44ShipmentForTracking(Shipment shipment, string vendorScac)
        {
            // Capacity Group
            var capacityGroup = new CapacityProviderAccountGroup(string.Empty, new List<CapacityProviderAccount>
            {
                new CapacityProviderAccount(vendorScac)
            });

            //Origin
            var originContact = shipment.Origin.Contacts.First(c => c.Primary);
            var stops = new List<LtlTrackedShipmentStop>
            {
                new LtlTrackedShipmentStop(null, LtlTrackedShipmentStop.StopTypeEnum.ORIGIN, new Location
                (new Address(shipment.Origin.PostalCode, new List<string>
                {
                    shipment.Origin.Street1,
                    shipment.Origin.Street2
                }, shipment.Origin.City, shipment.Origin.State, shipment.Origin.Country.Project44CountryCode.ConvertCoreProject44CountryCodeToEnum()), new Contact(
                    ContactName: originContact.Name, PhoneNumber: originContact.Phone,
                    Email: originContact.Email, FaxNumber: originContact.Fax,
                        CompanyName: shipment.Origin.Description)),
                    AppointmentWindow: new ZonedDateTimeWindow(shipment.DesiredPickupDate, shipment.DesiredPickupDate.SetToEndOfDay()))
            };

            // Destination
            var destinationContact = shipment.Destination.Contacts.First(c => c.Primary);
            stops.Add(new LtlTrackedShipmentStop(null, LtlTrackedShipmentStop.StopTypeEnum.DESTINATION, new Location
            (new Address(shipment.Destination.PostalCode, new List<string>
            {
                shipment.Destination.Street1,
                shipment.Destination.Street2
            }, shipment.Destination.City, shipment.Destination.State, shipment.Destination.Country.Project44CountryCode.ConvertCoreProject44CountryCodeToEnum()), new Contact(
                ContactName: destinationContact.Name, PhoneNumber: destinationContact.Phone,
                Email: destinationContact.Email, FaxNumber: destinationContact.Fax,
                    CompanyName: shipment.Destination.Description)),
                AppointmentWindow: new ZonedDateTimeWindow(shipment.EstimatedDeliveryDate, shipment.EstimatedDeliveryDate.SetToEndOfDay())));

            var trackingRequest = new LtlTrackedShipment(capacityGroup, new List<LtlShipmentIdentifier>
                                                {
                                                    new LtlShipmentIdentifier(LtlShipmentIdentifier.TypeEnum.BILLOFLADING, shipment.ShipmentNumber)
                                                }, stops);

            if (!string.IsNullOrEmpty(shipment.PurchaseOrderNumber))
                trackingRequest.ShipmentIdentifiers.Add(new LtlShipmentIdentifier(LtlShipmentIdentifier.TypeEnum.PURCHASEORDER, shipment.PurchaseOrderNumber));

            if (!string.IsNullOrEmpty(shipment.ShipperReference))
                trackingRequest.ShipmentIdentifiers.Add(new LtlShipmentIdentifier(LtlShipmentIdentifier.TypeEnum.EXTERNAL, shipment.ShipperReference));

            return trackingRequest;
        }

        private LtlDispatchedShipment GenerateProject44ShipmentForDispatch(Shipment shipment, string vendorScac)
        {
            // Origin
            var originContact = shipment.Origin.Contacts.First(c => c.Primary);
            var origin = new Location
            (new Address(shipment.Origin.PostalCode, new List<string>
                {
                    shipment.Origin.Street1,
                    shipment.Origin.Street2
                }, shipment.Origin.City, shipment.Origin.State, shipment.Origin.Country.Project44CountryCode.ConvertCoreProject44CountryCodeToEnum()),
            new Contact(ContactName: originContact.Name, PhoneNumber: originContact.Phone,
                    Email: originContact.Email, FaxNumber: originContact.Fax, CompanyName: shipment.Origin.Description));

            // Destination
            var destinationContact = shipment.Origin.Contacts.First(c => c.Primary);
            var destination = new Location
            (new Address(shipment.Destination.PostalCode, new List<string>
                {
                    shipment.Destination.Street1,
                    shipment.Destination.Street2
                }, shipment.Destination.City, shipment.Destination.State, shipment.Destination.Country.Project44CountryCode.ConvertCoreProject44CountryCodeToEnum()),
                new Contact(ContactName: destinationContact.Name, PhoneNumber: destinationContact.Phone,
                    Email: destinationContact.Email, FaxNumber: destinationContact.Fax, CompanyName: shipment.Destination.Description));

            // Line Items
            var lineItems = shipment.Items.Select(i => new LineItem(i.ActualWeight,
                new CubicDimension(i.ActualLength, i.ActualWidth, i.ActualHeight),
                i.ActualFreightClass.GetFreightClass(),
                (LineItem.PackageTypeEnum)Enum.Parse(typeof(LineItem.PackageTypeEnum), i.PackageType.Project44Code.ToString()), i.Quantity,
                i.PieceCount < 1 ? 1 : i.PieceCount, i.Description, i.IsStackable, i.NMFCCode, string.Empty)).ToList();

            // Capacity Group
            var capacityGroup = new CapacityProviderAccountGroup(string.Empty, new List<CapacityProviderAccount>
            {
                new CapacityProviderAccount(vendorScac)
            });

            // Requestor Location
            var locationApi = new LocationManagementApi(_hostName)
            {
                Configuration =
                {
                    Username = _userName,
                    Password = _password
                }
            };
            locationApi.Configuration.AddDefaultHeader("Content-Type", "application/json");

            var requestorLocation = locationApi.GetLocation(_primaryLocationId);
            requestorLocation.Id = null; // Id must be null in all calls not for location updating

            // Accesorials
            var accessorials = shipment.Services.Select(s => new AccessorialService(s.Service.Project44Code.ToString())).ToList();

            var dispatchRequest = new LtlDispatchedShipment(capacityGroup, origin, destination, CarrierCode: vendorScac,
                                                DeliveryNote: shipment.Destination.SpecialInstructions, LineItems: lineItems,
                                                PickupNote: shipment.Origin.SpecialInstructions,
                                                PickupWindow: new LocalDateTimeWindow(shipment.DesiredPickupDate.ToString("yyyy-MM-dd"), shipment.EarlyPickup, shipment.LatePickup),
                                                ShipmentIdentifiers: new List<LtlShipmentIdentifier>
                                                {
                                                    new LtlShipmentIdentifier(LtlShipmentIdentifier.TypeEnum.BILLOFLADING, shipment.ShipmentNumber),
                                                },
                                                AccessorialServices: accessorials,
                                                RequesterLocation: requestorLocation,
                                                CapacityProviderQuoteNumber: shipment.Project44QuoteNumber,
                                                ApiConfiguration: new ShipmentApiConfiguration
                                                {
                                                    NoteConfiguration = new ShipmentNoteConfiguration(true, new List<ShipmentNoteSection>
                                                    {
                                                        new ShipmentNoteSection(ShipmentNoteSection.NameEnum.PICKUPNOTE),
                                                        new ShipmentNoteSection(ShipmentNoteSection.NameEnum.DELIVERYNOTE),
                                                        new ShipmentNoteSection(ShipmentNoteSection.NameEnum.PRIORITYACCESSORIALS),
                                                        new ShipmentNoteSection(ShipmentNoteSection.NameEnum.PICKUPACCESSORIALS),
                                                        new ShipmentNoteSection(ShipmentNoteSection.NameEnum.DELIVERYACCESSORIALS),
                                                        new ShipmentNoteSection(ShipmentNoteSection.NameEnum.OTHERACCESSORIALS),
                                                        new ShipmentNoteSection(ShipmentNoteSection.NameEnum.DIMENSIONS)
                                                    })
                                                });

            if (!string.IsNullOrEmpty(shipment.PurchaseOrderNumber))
                dispatchRequest.ShipmentIdentifiers.Add(new LtlShipmentIdentifier(LtlShipmentIdentifier.TypeEnum.PURCHASEORDER, shipment.PurchaseOrderNumber));

            if (!string.IsNullOrEmpty(shipment.ShipperReference))
                dispatchRequest.ShipmentIdentifiers.Add(new LtlShipmentIdentifier(LtlShipmentIdentifier.TypeEnum.EXTERNAL, shipment.ShipperReference));

            return dispatchRequest;
        }
    }
}
