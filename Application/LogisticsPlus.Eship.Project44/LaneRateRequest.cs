﻿using P44SDK.V4.Model;

namespace LogisticsPlus.Eship.Project44
{
    public class LaneRateRequest
    {
        public string OriginPostalCode { get; set; }
        public Address.CountryEnum OriginCountry { get; set; }
        public string DestinationPostalCode { get; set; }
        public Address.CountryEnum DestinationCountry { get; set; }

        public double FreightClass { get; set; }

        public decimal? Weight { get; set; }
        public decimal? Length { get; set; }
        public decimal? Width { get; set; }
        public decimal? Height { get; set; }

        public string Scac { get; set; }
    }
}
