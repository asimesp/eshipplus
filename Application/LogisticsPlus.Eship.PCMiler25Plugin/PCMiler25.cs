﻿using System;
using LogisticsPlus.Eship.PluginBase.Mileage;

namespace LogisticsPlus.Eship.PCMiler25Plugin
{
	public class PcMiler25 : IMileagePlugin
	{
		private const int ValidServerInitializationValue = 10000;

		private short _server;

		public string LastErrorMessage { get; set; }

		public bool IsInitialized { get; private set; }

		

		public bool Initialize()
		{
			try
			{
				_server = PCMDLLINT.PCM.PCMSOpenServer(0, 0);

				IsInitialized = _server == ValidServerInitializationValue;

				if (!IsInitialized)
					LastErrorMessage = string.Format("Failed PC Miler 25 server initialization. Value: {0}", _server);

				return IsInitialized;
			}
			catch (Exception e)
			{
				LastErrorMessage = e.Message;
				IsInitialized = false;
				return false;
			}
		}

		public bool Initialize<T>(T parameter)
		{
			throw new NotSupportedException("Parameterized initialization is not supported by this implementation.");
		}


		public double GetMiles(Point start, Point end)
		{
			try
			{
				if(!IsInitialized)
				{
					LastErrorMessage = "Engine is not initialized!";
					return PluginBase.PluginBaseConstants.ErrorMilesValue;
				}

				var p1 = string.Format("{0} {1}", start.PostalCode, start.Country);
				var p2 = string.Format("{0} {1}", end.PostalCode, end.Country);
				var ret = PCMDLLINT.PCM.PCMSCalcDistance(_server, p1, p2);
				if(ret == (int)PluginBase.PluginBaseConstants.ErrorMilesValue)
				{
					LastErrorMessage = "Invalid mileage value returned. The mileage engine may be unavailable";
					return ret;
				}
				return ret / 10d;
			}
			catch (Exception e)
			{
				LastErrorMessage = e.Message;
				return PluginBase.PluginBaseConstants.ErrorMilesValue;
			}
		}

		public double GetKilometers(Point start, Point end)
		{
			return GetMiles(start, end) * PluginBase.PluginBaseConstants.RatioMilesToKilometers;
		}


		public double GetMiles<T>(Point start, Point end, T setup)
		{
			throw new NotSupportedException("Parameterized mileage method is not supported by this implementation.");
		}

		public double GetKilometers<T>(Point start, Point end, T setup)
		{
			throw new NotSupportedException("Parameterized kilometer method is not supported by this implementation.");
		}



		public void Dispose()
		{
			PCMDLLINT.PCM.PCMSCloseServer(_server);
		}
	}
}
