﻿namespace LogisticsPlus.Eship.WebApplication.Documents
{
	public class ShipmentFields : BaseFields
	{
		// Shipment dtails
		public const string UploadedImages = "[#UploadedImages#]";

		public const string ShipmentNumber = "[#ShipmentNumber#]";
        public const string Prefix = "[#Prefix#]";
        public const string ServiceMode = "[#ServiceMode#]";
		public const string ShipmentDate = "[#ShipmentDate#]";
		public const string EarliestPickup = "[#EarliestPickup#]";
		public const string LastestPickup = "[#LastestPickup#]";
	    public const string EstimatedDeliveryDate = "[#EstimatedDeliveryDate#]";
		public const string EarliestDelivery = "[#EarliestDelivery#]";
		public const string LastestDelivery = "[#LastestDelivery#]";
		public const string GeneralBOLComments = "[#GeneralBOLComments#]";
		public const string CriticalBOLComments = "[#CriticalBOLComments#]";
		public const string PurchaseOrder = "[#PurchaseOrder#]";
		public const string ShipperReference = "[#ShipperReference#]";
		public const string LogoUrl = "[#LogoUrl#]";
		public const string CustomerBOLText = "[#CustomerBOLText#]";
		public const string TierBOLText = "[#TierBOLText#]";
		public const string ShipperBillOfLadingNumber = "[#ShipperBillOfLadingNumber#]";
        public const string TenantAutoRatingNotice = "[#TenantAutoRatingNotice#]";
        public const string VendorRatingProfileAddressWrapperStart = "[#VendorRatingProfileAddressWrapperStart#]";
		public const string VendorRatingProfileAddressWrapperEnd = "[#VendorRatingProfileAddressWrapperEnd#]";


		//Customer
		public const string CustomerNumber = "[#CustomerNumber#]";
		public const string CustomerName = "[#CustomerName#]";

		public const string CareOfWrapperStart = "[#CareOfWrapperStart#]";
		public const string CareOfWrapperEnd = "[#CareOfWrapperEnd#]";
		
		//Tier
		public const string TierNumber = "[#TierNumber#]";
		public const string TierName = "[#TierName#]";

		// vendor
		public const string PrimaryVendorName = "[#PrimaryVendorName#]";
		public const string PrimaryVendorPro = "[#PrimaryVendorPro#]";
        public const string PrimaryVendorScac = "[#PrimaryVendorScac#]";

        public const string SecondaryVendors = "[#SecondaryVendors#]";
        public const string VendorsWrapperStart = "[#VendorsWrapperStart#]";
		public const string VendorsWrapperEnd = "[#VendorsWrapperEnd#]";
		public const string VendorName = "[#VendorName#]";
		public const string VendorPro = "[#VendorPro#]";

		// user to contact
		public const string ContactPhone = "[#ContactPhone#]";
		public const string ContactName = "[#ContactName#]";
		public const string ContactTollFree = "[#ContactTollFree#]";

		public const string CreatedByContactPhone = "[#CreatedByContactPhone#]";
		public const string CreatedByContactName = "[#CreatedByContactName#]";

        public const string CreatedByUserDepartmentPhone = "[#CreatedByUserDepartmentPhone#]";

        //department
		public const string DepartmentPhone = "[#DepartmentPhone#]";
        public const string DepartmentFax = "[#DepartmentFax#]";
        public const string DepartmentEmail = "[#DepartmentEmail#]";

		// origin
		public const string OriginDescription = "[#OriginDescription#]";
		public const string OriginStreet1 = "[#OriginStreet1#]";
		public const string OriginStreet2 = "[#OriginStreet2#]";
		public const string OriginCity = "[#OriginCity#]";
		public const string OriginState = "[#OriginState#]";
		public const string OriginPostalCode = "[#OriginPostalCode#]";
		public const string OriginCountry = "[#OriginCountry#]";
		public const string OriginGeneralInfo= "[#OriginGenInfo#]";
		public const string OriginDirections= "[#OriginDirections#]";

		public const string OriginPrimaryContactEnabled = "[#OriginPrimaryContactEnabled#]";
		public const string OriginPrimaryContact = "[#OriginPrimaryContact#]";
		public const string OriginPrimaryPhone = "[#OriginPrimaryPhone#]";
		public const string OriginPrimaryFax = "[#OriginPrimaryFax#]";
		public const string OriginPrimaryMobile = "[#OriginPrimaryMobile#]";
		public const string OriginPrimaryEmail = "[#OriginPrimaryEmail#]";

		public const string OriginContactsWrapperStart = "[#OriginContactsWrapperStart#]";
		public const string OriginContactsWrapperEnd = "[#OriginContactsWrapperEnd#]";
		public const string OriginContact = "[#OriginContact#]";
		public const string OriginPhone = "[#OriginPhone#]";
		public const string OriginFax = "[#OriginFax#]";
		public const string OriginMobile = "[#OriginMobile#]";
		public const string OriginEmail = "[#OriginEmail#]";

		public const string OriginInstructions = "[#OriginInstructions#]";

		public const string OriginReferencesWrapperStart = "[#OriginReferencesWrapperStart#]";
		public const string OriginReferencesWrapperEnd = "[#OriginReferencesWrapperEnd#]";
		public const string OriginReferenceName = "[#OriginReferenceName#]";
		public const string OriginReferenceValue = "[#OriginReferenceValue#]";

		public const string OriginTerminalCode = "[#OriginTerminalCode#]";
		public const string OriginTerminalName = "[#OriginTerminalName#]";
		public const string OriginTerminalPhone = "[#OriginTerminalPhone#]";
		public const string OriginTerminalFax = "[#OriginTerminalFax#]";
		public const string OriginTerminalTollFree = "[#OriginTerminalTollFree#]";
		public const string OriginTerminalEmail = "[#OriginTerminalEmail#]";
		public const string OriginTerminalContactName = "[#OriginTerminalContactName#]";
		public const string OriginTerminalContactTitle = "[#OriginTerminalContactTitle#]";

		// destination
		public const string DestinationDescription = "[#DestinationDescription#]";
		public const string DestinationStreet1 = "[#DestinationStreet1#]";
		public const string DestinationStreet2 = "[#DestinationStreet2#]";
		public const string DestinationCity = "[#DestinationCity#]";
		public const string DestinationState = "[#DestinationState#]";
		public const string DestinationPostalCode = "[#DestinationPostalCode#]";
		public const string DestinationCountry = "[#DestinationCountry#]";
        public const string DestinationGeneralInfo= "[#DestinationGenInfo#]";
	    public const string DestinationDirections = "[#DestinationDirections#]";

		public const string DestinationPrimaryContactEnabled = "[#DestinationPrimaryContactEnabled#]";
		public const string DestinationPrimaryContact = "[#DestinationPrimaryContact#]";
		public const string DestinationPrimaryPhone = "[#DestinationPrimaryPhone#]";
		public const string DestinationPrimaryFax = "[#DestinationPrimaryFax#]";
		public const string DestinationPrimaryMobile = "[#DestinationPrimaryMobile#]";
		public const string DestinationPrimaryEmail = "[#DestinationPrimaryEmail#]";

		public const string DestinationContactsWrapperStart = "[#DestinationContactsWrapperStart#]";
		public const string DestinationContactsWrapperEnd = "[#DestinationContactsWrapperEnd#]";
		public const string DestinationContact = "[#DestinationContact#]";
		public const string DestinationPhone = "[#DestinationPhone#]";
		public const string DestinationFax = "[#DestinationFax#]";
		public const string DestinationMobile = "[#DestinationMobile#]";
		public const string DestinationEmail = "[#DestinationEmail#]";

		public const string DestinationInstructions = "[#DestinationInstructions#]";

		public const string DestinationReferencesWrapperStart = "[#DestinationReferencesWrapperStart#]";
		public const string DestinationReferencesWrapperEnd = "[#DestinationReferencesWrapperEnd#]";
		public const string DestinationReferenceName = "[#DestinationReferenceName#]";
		public const string DestinationReferenceValue = "[#DestinationReferenceValue#]";

		public const string DestinationTerminalCode = "[#DestinationTerminalCode#]";
		public const string DestinationTerminalName = "[#DestinationTerminalName#]";
		public const string DestinationTerminalPhone = "[#DestinationTerminalPhone#]";
		public const string DestinationTerminalFax = "[#DestinationTerminalFax#]";
		public const string DestinationTerminalTollFree = "[#DestinationTerminalTollFree#]";
		public const string DestinationTerminalEmail = "[#DestinationTerminalEmail#]";
		public const string DestinationTerminalContactName = "[#DestinationTerminalContactName#]";
		public const string DestinationTerminalContactTitle = "[#DestinationTerminalContactTitle#]";

		// items
		public const string ItemsTotalWeight = "[#ItemsTotalWeight#]";
		public const string ItemsTotalValue = "[#ItemsTotalValue#]";
		public const string ItemsWrapperStart = "[#ItemsWrapperStart#]";
		public const string ItemsWrapperEnd = "[#ItemsWrapperEnd#]";
		public const string ItemQuantity = "[#ItemQuantity#]";
		public const string ItemPieceCount = "[#ItemPieceCount#]";
		public const string ItemPackageType = "[#ItemPackageType#]";
		public const string ItemDescription = "[#ItemDescription#]";
		public const string ItemTotalWeight = "[#ItemTotalWeight#]";
		public const string ItemLength = "[#ItemLength#]";
		public const string ItemWidth = "[#ItemWidth#]";
		public const string ItemHeight = "[#ItemHeight#]";
		public const string ItemFreightClass = "[#ItemFreightClass#]";
		public const string ItemStackable = "[#ItemStackable#]";
		public const string ItemValue = "[#ItemValue#]";
		public const string ItemNMFC = "[#ItemNMFC#]";
		public const string ItemHTS = "[#ItemHTS#]";
		public const string ItemPickup = "[#ItemPickup#]";
		public const string ItemDelivery = "[#ItemDelivery#]";
		public const string ItemComments = "[#ItemComments#]";
		public const string ItemHazardous = "[#ItemHazardous#]";
	    public const string OriginItemPickup = "[#OriginItemPickup#]";
	    public const string DestinationItemDelivery = "[#DestinationItemDelivery#]";
        public const string ItemsTotalQuantity = "[#ItemsTotalQuantity#]";
        public const string ItemsTotalPieceCount = "[#ItemsTotalPieceCount#]";
        public const string PiecesAbbreviation = "[#Pcs#]";
        public const string ItemHazmat = "[#ItemHazmat#]";

		// stops
        public const string StopsOuterWrapperStart = "[#StopsOuterWrapperStart#]";
        public const string StopsOuterWrapperEnd = "[#StopsOuterWrapperEnd#]";
		public const string StopsWrapperStart = "[#StopsWrapperStart#]";
		public const string StopsWrapperEnd = "[#StopsWrapperEnd#]";
		public const string StopDescription = "[#StopDescription#]";
		public const string StopStreet1 = "[#StopStreet1#]";
		public const string StopStreet2 = "[#StopStreet2#]";
		public const string StopCity = "[#StopCity#]";
		public const string StopState = "[#StopState#]";
		public const string StopPostalCode = "[#StopPostalCode#]";
		public const string StopCountry = "[#StopCountry#]";
        public const string StopNumber = "[#StopNumber#]";
        public const string StopFullAddress = "[#StopFullAddress#]";
        public const string StopGeneralInfo= "[#StopGenInfo#]";
	    public const string StopDirections = "[#StopDirections#]";

		public const string StopPrimaryContactEnabled = "[#StopPrimaryContactEnabled#]";
		public const string StopPrimaryContact = "[#StopPrimaryContact#]";
		public const string StopPrimaryPhone = "[#StopPrimaryPhone#]";
		public const string StopPrimaryFax = "[#StopPrimaryFax#]";
		public const string StopPrimaryMobile = "[#StopPrimaryMobile#]";
		public const string StopPrimaryEmail = "[#StopPrimaryEmail#]";

		public const string StopContactsWrapperStart = "[#StopContactsWrapperStart#]";
		public const string StopContactsWrapperEnd = "[#StopContactsWrapperEnd#]";
		public const string StopContact = "[#StopContact#]";
		public const string StopPhone = "[#StopPhone#]";
		public const string StopFax = "[#StopFax#]";
		public const string StopMobile = "[#StopMobile#]";
		public const string StopEmail = "[#StopEmail#]";

		public const string StopInstructions = "[#StopInstructions#]";
		public const string StopAppointmentDateTime = "[#StopAppointmentDateTime#]";

		//Critical Notes
		public const string CriticalNotesWrapperStart = "[#CriticalNotesWrapperStart#]";
		public const string CriticalNotesWrapperEnd = "[#CriticalNotesWrapperEnd#]";
		public const string CriticalNotesMessage = "[#CriticalNotesMessage#]";

		//General Notes
		public const string GeneralNotesWrapperStart = "[#GeneralNotesWrapperStart#]";
		public const string GeneralNotesWrapperEnd = "[#GeneralNotesWrapperEnd#]";
		public const string GeneralNotesMessage = "[#GeneralNotesMessage#]";

        //Misc Notes
        public const string MiscellaneousField1 = "[#MiscellaneousField1#]";
        public const string MiscellaneousField2 = "[#MiscellaneousField2#]";


		// accessorials
		public const string AccessorialsWrapperStart = "[#AccessorialsWrapperStart#]";
		public const string AccessorialsWrapperEnd = "[#AccessorialsWrapperEnd#]";
		public const string AccessorialsDescription = "[#AccessorialsDescription#]";
		public const string AccessorialsCode = "[#AccessorialsCode#]";

		// hazardous materials
		public const string HazardousMaterialWrapperStart = "[#HazardousMaterialWrapperStart#]";
		public const string HazardousMaterialWrapperEnd = "[#HazardousMaterialWrapperEnd#]";
		public const string HazardousMaterialContact = "[#HazardousMaterialContact#]";
		public const string HazardousMaterialPhone = "[#HazardousMaterialPhone#]";
		public const string HazardousMaterialMobile = "[#HazardousMaterialMobile#]";
		public const string HazardousMaterialEmail = "[#HazardousMaterialEmail#]";

        //fields for route summary where we need to show items for orgin, each stop, and destination
        public const string OriginItemsWrapperStart = "[#OriginItemsWrapperStart#]";
        public const string OriginItemQuantity = "[#OriginItemQuantity#]";
	    public const string OriginItemPackageType = "[#OriginItemPackageType#]";
	    public const string OriginItemLength = "[#OriginItemLength#]";
        public const string OriginItemWidth = "[#OriginItemWidth#]";
        public const string OriginItemHeight = "[#OriginItemHeight#]";
        public const string OriginItemTotalWeight = "[#OriginItemTotalWeight#]";
        public const string OriginItemFreightClass = "[#OriginItemFreightClass#]";
        public const string OriginItemsWrapperEnd = "[#OriginItemsWrapperEnd#]";
        public const string OriginItemsTotalWeight = "[#OriginItemsTotalWeight#]";
        public const string OriginItemDescription = "[#OriginItemDescription#]";

        public const string DestinationItemsWrapperStart = "[#DestinationItemsWrapperStart#]";
        public const string DestinationItemQuantity = "[#DestinationItemQuantity#]";
        public const string DestinationItemPackageType = "[#DestinationItemPackageType#]";
        public const string DestinationItemLength = "[#DestinationItemLength#]";
        public const string DestinationItemWidth = "[#DestinationItemWidth#]";
        public const string DestinationItemHeight = "[#DestinationItemHeight#]";
        public const string DestinationItemTotalWeight = "[#DestinationItemTotalWeight#]";
        public const string DestinationItemFreightClass = "[#DestinationItemFreightClass#]";
        public const string DestinationItemsWrapperEnd = "[#DestinationItemsWrapperEnd#]";
        public const string DestinationItemsTotalWeight = "[#DestinationItemsTotalWeight#]";
        public const string DestinationItemDescription = "[#DestinationItemDescription#]";
        public const string DestinationMilesFromPreceedingStop = "[#DestinationMilesFromPreceedingStop#]";

	    public const string MilesFromPreceedingStop = "[#MilesFromPreceedingStop#]";
	    public const string TotalMiles = "[#TotalMiles#]";

        // charges
        public const string TotalFreightCharges = "[#TotalFreightCharge#]";
        public const string TotalFuelCharges = "[#TotalFuelCharge#]";
        public const string TotalAccessorialCharges = "[#TotalAccessorialCharge#]";
        public const string TotalOtherCharges = "[#TotalOtherCharge#]";

        public const string PageNumber = "[#PageNumber#]";
        public const string TotalPages = "[#TotalPage#]";
    }
}
