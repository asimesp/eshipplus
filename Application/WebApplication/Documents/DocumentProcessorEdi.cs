﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Extern;
using LogisticsPlus.Eship.Extern.Edi;
using LogisticsPlus.Eship.Extern.Edi.Segments;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.WebApplication.Members;
using LogisticsPlus.Eship.WebApplication.Members.Connect;
using LogisticsPlus.Eship.WebApplication.Members.Operations;
using LogisticsPlus.Eship.WebApplication.Utilities;
using Contact = LogisticsPlus.Eship.Extern.Edi.Segments.Contact;

namespace LogisticsPlus.Eship.WebApplication.Documents
{
    public partial class DocumentProcessor
    {
		public static string GenerateEdiMessage(this MemberPageBase page, Communication communication, XmlTransmission transmission)
		{
			return page.Server.GenerateEdiMessage(communication, transmission.Xml, transmission.DocumentType, transmission.Id.ToString());
		}

		public static string GenerateEdiMessage(this HttpServerUtility server, Communication communication, string xmlBody, string documentType, string uniqueKey)
		{
			var template = !string.IsNullOrEmpty(communication.EdiVANEnvelopePath)
							? server.ReadFromFile(communication.EdiVANEnvelopePath).FromUtf8Bytes()
							: string.Empty;

			// if no template, return body of message!
			if (string.IsNullOrEmpty(template)) return xmlBody;

			// details
			template = template.Replace(EdiFields.ReceiverId, communication.EdiCode);
			template = template.Replace(EdiFields.DocumentType, documentType);
			template = template.Replace(EdiFields.UniqueKey, uniqueKey);
			template = template.Replace(EdiFields.DateCreated, DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss"));
			template = template.Replace(EdiFields.Body, xmlBody.StripXmlNamespacesAndEncoding());

			return template;
		}


        public static string GenerateLoadTender(this HttpServerUtility server, Load loadTender, Customer customer, Direction direction, bool removeBodyWrapper = false, List<EdiDiff> differences = null, bool emailResponse = false, string vendorId = "")
        {
			// logo url
			var logoUrl = customer == null ? string.Empty : customer.RetrievePageLogoUrl();
			logoUrl = logoUrl.Replace("~", WebApplicationSettings.SiteRoot);

	        return server.GenerateLoadTender(loadTender, customer == null ? string.Empty : customer.Name, direction, removeBodyWrapper, logoUrl, differences, emailResponse, vendorId);
        }

		public static string GenerateLoadTender(this HttpServerUtility server, Load loadTender, string accountName, Direction direction, bool removeBodyWrapper = false, string logoUrl = WebApplicationConstants.AppDefaultLogoUrl, List<EdiDiff> differences = null, bool emailResponse = false, string vendorId = "")
        {
			// clean up logo URL
	        if (logoUrl.StartsWith("~"))
		        logoUrl = logoUrl.Replace("~", WebApplicationSettings.SiteRoot);

			// get template
			var template = server.ReadFromFile(emailResponse? SystemTemplates.Edi204EmailTemplate: SystemTemplates.Edi204DetailTemplate).FromUtf8Bytes();

            // body wrapper
            if (removeBodyWrapper)
            {
                template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
                template = template.Replace(BaseFields.BodyWrapperEnd, WebApplicationSettings.Brand);
            }
            
            //Beginning Segment Shipment Information
            var beginningSegment = loadTender.BeginningSegment ?? new BeginningSegmentShipmentInfo();
            template = template.Replace(LoadTenderFields.ShipmentInformationDirection, direction.GetString());
            template = template.Replace(LoadTenderFields.ShipmentInformationAccountName, accountName);
            template = template.Replace(LoadTenderFields.ShipmentStandardPointLocationCode, beginningSegment.StandardPointLocationCode);
            template = template.Replace(LoadTenderFields.ShipmentStandardPointLocationCode, beginningSegment.StandardPointLocationCode);
            template = template.Replace(LoadTenderFields.ShipmentWeightUnitQualifier, beginningSegment.WeightUnitQualifier.GetDescription());
            template = template.Replace(LoadTenderFields.ShipmentTotalEquipment, beginningSegment.TotalEquipment.GetString());
            template = template.Replace(LoadTenderFields.ShipmentWeightCode, beginningSegment.ShipmentWeightCode);
            template = template.Replace(LoadTenderFields.ShipmentCustomsDocumentHandlingCode, beginningSegment.CustomsDocumentHandlingCode);
            template = template.Replace(LoadTenderFields.ShipmentPaymentMethodCode, beginningSegment.PaymentMethodCode.GetDescription());
            template = template.Replace(LoadTenderFields.ShipmentTariffServiceCode, beginningSegment.TariffServiceCode);
            template = template.Replace(LoadTenderFields.ShipmentStandardCarrierAlphaCode, beginningSegment.StandardCarrierAlphaCode);
            template = template.Replace(LoadTenderFields.ShipmentMethodOfPayment, beginningSegment.ShipmentMethodOfPayment);
            template = template.Replace(LoadTenderFields.ShipmentQualifier, beginningSegment.ShipmentQualifier.GetString());
            template = template.Replace(LoadTenderFields.ShipmentTransportationTermsCode, beginningSegment.TransportationTermsCode);
			template = template.Replace(LoadTenderFields.LogoUrl, logoUrl);

            //Link to the XmlConnectRecords page where the shipment Id is displayed

			var pageAddress = emailResponse ? LoadTenderResponseView.PageAddress : XmlConnectRecordView.PageAddress;
            pageAddress = WebApplicationSettings.SiteRoot.EndsWith("/")
                    ? WebApplicationSettings.SiteRoot + pageAddress.Replace("~/", string.Empty)
                    : WebApplicationSettings.SiteRoot + pageAddress.Replace("~", string.Empty);

            template = template.Replace(LoadTenderFields.PageAddress, pageAddress);

			//for email notofication pass in vendor id instead of doc type.
			template = emailResponse ? template.Replace(LoadTenderFields.VendorIdKey, WebApplicationConstants.LoadTenderEmailResponseVendorIdKey) : template.Replace(LoadTenderFields.DocumentTypeKey, WebApplicationConstants.XmlConnectRecordsDocTypeKey);
			template = emailResponse ? template.Replace(LoadTenderFields.VendorId, vendorId) : template.Replace(LoadTenderFields.DocumentType, EdiDocumentType.EDI204.ToInt().ToString());
            
            template = template.Replace(LoadTenderFields.ShipmentIdNumberKey, WebApplicationConstants.XmlConnectRecordsShipmentIdKey);
            template = template.Replace(LoadTenderFields.ShipmentIdNumber, beginningSegment.ShipmentIdNumber);

            //Set Purpose
            var purpose = loadTender.SetPurpose ?? new SetPurpose();
            template = template.Replace(LoadTenderFields.SetPurposeTransactionSetPurpose, purpose.TransactionSetPurpose.GetDescription());
            template = template.Replace(LoadTenderFields.SetPurposeApplicationType, purpose.ApplicationType.GetDescription());

            //Expiration Date
            template = template.Replace(LoadTenderFields.ExpirationDate, loadTender.ExpirationDate.FormattedLongDateAlt());
            
            //List of Load Reference Numbers
            if (template.Contains(LoadTenderFields.LoadReferenceWrapperStart) && loadTender.LoadReferenceNumbers != null && loadTender.LoadReferenceNumbers.Any())
            {
                var content = template.RetrieveWrapperSection(LoadTenderFields.LoadReferenceWrapperStart, LoadTenderFields.LoadReferenceWrapperEnd);
                var lines = loadTender.LoadReferenceNumbers.Any()
                                ? loadTender.LoadReferenceNumbers
                                    .Select(r => content
                                                    .Replace(LoadTenderFields.LoadReferenceIdentificationQualifier, r.ReferenceIdentificationQualifier.GetDescription())
                                                    .Replace(LoadTenderFields.LoadReferenceIdentification, r.ReferenceIdentification)
                                                    .Replace(LoadTenderFields.LoadReferenceDescription, r.Description))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(LoadTenderFields.LoadReferenceOuterWrapperStart, string.Empty);
                template = template.Replace(LoadTenderFields.LoadReferenceWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(LoadTenderFields.LoadReferenceWrapperEnd, string.Empty);
                template = template.Replace(LoadTenderFields.LoadReferenceOuterWrapperEnd, string.Empty);
            }
            else
            {
                var content = template.RetrieveWrapperSection(LoadTenderFields.LoadReferenceOuterWrapperStart, LoadTenderFields.LoadReferenceOuterWrapperEnd);
                template = template.Replace(LoadTenderFields.LoadReferenceOuterWrapperStart, string.Empty);
                template = template.Replace(content, string.Empty);
                template = template.Replace(LoadTenderFields.LoadReferenceOuterWrapperEnd, string.Empty);
            }

            //Handling Requirements
            if (template.Contains(LoadTenderFields.HandlingRequirementWrapperStart) && loadTender.HandlingRequirements != null && loadTender.HandlingRequirements.Any())
            {
                var content = template.RetrieveWrapperSection(LoadTenderFields.HandlingRequirementWrapperStart, LoadTenderFields.HandlingRequirementWrapperEnd);
                var lines = loadTender.HandlingRequirements.Any()
                                ? loadTender.HandlingRequirements
                                    .Select(r => content
                                                    .Replace(LoadTenderFields.HandlingRequirementSpecialHandlingCode, r.SpecialHandlingCode)
                                                    .Replace(LoadTenderFields.HandlingRequirementSpecialServicesCode, r.SpecialServicesCode)
                                                    .Replace(LoadTenderFields.HandlingRequirementSpecialHandlingDescription, r.SpecialHandlingDescription))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(LoadTenderFields.HandlingRequirementOuterWrapperStart, string.Empty);
                template = template.Replace(LoadTenderFields.HandlingRequirementWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(LoadTenderFields.HandlingRequirementWrapperEnd, string.Empty);
                template = template.Replace(LoadTenderFields.HandlingRequirementOuterWrapperEnd, string.Empty);
            } else {
                var content = template.RetrieveWrapperSection(LoadTenderFields.HandlingRequirementOuterWrapperStart, LoadTenderFields.HandlingRequirementOuterWrapperEnd);
                template = template.Replace(LoadTenderFields.HandlingRequirementOuterWrapperStart, string.Empty);
                template = template.Replace(content, string.Empty);
                template = template.Replace(LoadTenderFields.HandlingRequirementOuterWrapperEnd, string.Empty);
            }

            //Remarks
            var remarks = loadTender.Remarks ?? new Remarks();
            template = template.Replace(LoadTenderFields.FreeFormMessage, remarks.FreeFormMessage);
            template = template.Replace(LoadTenderFields.FreeFormMessage2, remarks.FreeFormMessage2);

            //Equipment Detail
            var equipmentDetail = loadTender.EquipmentDetail ?? new EquipmentDetail();
            template = template.Replace(LoadTenderFields.EquipmentDetailEquipmentNumber, equipmentDetail.EquipmentNumber);
            template = template.Replace(LoadTenderFields.EquipmentDetailEquipmentDescriptionCode, equipmentDetail.EquipmentDescriptionCode);
            
            //Stops List
            if (template.Contains(LoadTenderFields.StopWrapperStart) && loadTender.Stops.Any())
            {
                var content = template.RetrieveWrapperSection(LoadTenderFields.StopWrapperStart, LoadTenderFields.StopWrapperEnd);
                var lines = new List<string>();

                foreach (var stop in loadTender.Stops)
                {
                    var sLine = content;
                    //Replace all fields before the list of OrderIdDetails
                    sLine = sLine.Replace(LoadTenderFields.StopDateTime, stop.DateTime.FormattedLongDateAlt());

                    var locationName = stop.LocationName ?? new Name();
                    sLine = sLine.Replace(LoadTenderFields.StopLocationEntityCodeIdCode, locationName.EntityIdentifierCode.GetDescription());
                    sLine = sLine.Replace(LoadTenderFields.StopLocationEntityName, locationName.EntityName.GetString());
                    sLine = sLine.Replace(LoadTenderFields.StopLocationIdCodeQualifier, locationName.IdentificationCodeQualifier.GetString());
                    sLine = sLine.Replace(LoadTenderFields.StopLocationIdCode, locationName.IdentificationCode.GetString());

                    //Location Street Address
                    var addressInfo = stop.LocationStreetInformation ?? new AddressInformation();
                    sLine = sLine.Replace(LoadTenderFields.StopLocationAddressInfo1, addressInfo.AddressInfo1.GetString());
                    sLine = sLine.Replace(LoadTenderFields.StopLocationAddressInfo2, addressInfo.AddressInfo2.GetString());

                    //Geographic information
                    var geoInfo = stop.GeographicLocation ?? new GeographicLocation();
                    sLine = sLine.Replace(LoadTenderFields.StopCityName, geoInfo.CityName.GetString());
                    sLine = sLine.Replace(LoadTenderFields.StopStateOrProvinceCode, geoInfo.StateOrProvinceCode.GetString());
                    sLine = sLine.Replace(LoadTenderFields.StopPostalCode, geoInfo.PostalCode.GetString());
                    sLine = sLine.Replace(LoadTenderFields.StopCountryCode, geoInfo.CountryCode.GetString());
                    sLine = sLine.Replace(LoadTenderFields.StopLocationQualifier, geoInfo.LocationQualifier.GetString());
                    sLine = sLine.Replace(LoadTenderFields.StopLocationIdentifier, geoInfo.LocationIdentifier.GetString());

                    //Location Contact
                    var contact = stop.LocationContact ?? new Contact();
                    sLine = sLine.Replace(LoadTenderFields.StopLocationContactFunctionCode, contact.ContactFunctionCode.GetDescription());
                    sLine = sLine.Replace(LoadTenderFields.StopLocationContactName, contact.Name.GetString());
                    sLine = sLine.Replace(LoadTenderFields.StopLocationContactCommNumberQualifier, contact.CommunicationNumberQualifier.GetDescription());
                    sLine = sLine.Replace(LoadTenderFields.StopLocationContactCommNumber, contact.CommunicationNumber.GetString());
                    sLine = sLine.Replace(LoadTenderFields.StopLocationContactInquiryReference, contact.ContactInquiryReference.GetString());

                    //Stop Hazardous Material Contact
                    sLine = sLine.Replace(LoadTenderFields.IsHazMat, stop.IsHazardousMaterial.GetString());

                    var hazMatContact = stop.LocationContact ?? new Contact();
                    sLine = sLine.Replace(LoadTenderFields.StopHazMatContactFunctionCode, hazMatContact.ContactFunctionCode.GetDescription());
                    sLine = sLine.Replace(LoadTenderFields.StopHazMatContactName, hazMatContact.Name.GetString());
                    sLine = sLine.Replace(LoadTenderFields.StopHazMatContactCommNumberQualifier, hazMatContact.CommunicationNumberQualifier.GetDescription());
                    sLine = sLine.Replace(LoadTenderFields.StopHazMatContactCommNumber, hazMatContact.CommunicationNumber.GetString());
                    sLine = sLine.Replace(LoadTenderFields.StopHazMatContactInquiryRef, hazMatContact.ContactInquiryReference.GetString());

                    
                    //Stop Details
                    var stopDetails = stop.StopOffDetails ?? new StopOffDetails();
                    sLine = sLine.Replace(LoadTenderFields.StopSequenceNumber, stopDetails.StopSequenceNumber.GetString());
                    sLine = sLine.Replace(LoadTenderFields.StopReasonCode, stopDetails.StopReasonCode.GetDescription());
                    sLine = sLine.Replace(LoadTenderFields.StopSpecialInstructions, stopDetails.SpecialInstructions.GetString());

                    // Stop List of Order Identification Detail
                    if (sLine.Contains(LoadTenderFields.StopOrderIdDetailsWrapperStart) && stop.OrderIdentificationDetails.Any())
                    {
                        var sContent = sLine.RetrieveWrapperSection(LoadTenderFields.StopOrderIdDetailsWrapperStart, LoadTenderFields.StopOrderIdDetailsWrapperEnd);
                        var sLines = stop.OrderIdentificationDetails != null
                                        ? stop.OrderIdentificationDetails
                                            .Select(d => sContent
                                                            .Replace(LoadTenderFields.StopOrderIdDetailsRefId, d.ReferenceIdentification.GetString())
                                                            .Replace(LoadTenderFields.StopOrderIdDetailsQty, d.Quantity == default(int) ? string.Empty : d.Quantity.GetString())
                                                            .Replace(LoadTenderFields.StopOrderIdDetailsWeight, d.Weight == default(decimal) ? string.Empty : Math.Round(d.Weight, 0, MidpointRounding.AwayFromZero).GetString())
                                                            .Replace(LoadTenderFields.StopOrderIdDetailsLength, d.Length == default(decimal) ? string.Empty : Math.Round(d.Length, 2, MidpointRounding.AwayFromZero).GetString())
                                                            .Replace(LoadTenderFields.StopOrderIdDetailsWidth, d.Width == default(decimal) ? string.Empty : Math.Round(d.Width, 2, MidpointRounding.AwayFromZero).GetString())
                                                            .Replace(LoadTenderFields.StopOrderIdDetailsHeight, d.Height == default(decimal) ? string.Empty : Math.Round(d.Height, 2, MidpointRounding.AwayFromZero).GetString())
                                                            .Replace(LoadTenderFields.StopOrderIdDetailsNMFCCode, d.NMFCCode.GetString())
                                                            .Replace(LoadTenderFields.StopOrderIdDetailsItemDesc, d.ItemDescription.GetString())
                                                            .Replace(LoadTenderFields.StopOrderIdDetailsCommodityCode, d.CommodityCode.GetString())
                                                            .Replace(LoadTenderFields.StopOrderIdDetailsPackageType, d.PackageTypeCode.GetString())
                                                            .Replace(LoadTenderFields.StopOrderIdDetailsNumOfUnitsShipped, d.NumberOfIndividualUnitsShipped == default(int) ? string.Empty : d.NumberOfIndividualUnitsShipped.GetString())
                                                            .Replace(LoadTenderFields.StopOrderIdDetailsFreightClass, d.FreightClass.GetString())
                                                            )
                                            .ToArray()
                                        : new string[0];
                        sLine = sLine.Replace(LoadTenderFields.StopOrderIdDetailsOuterWrapperStart, string.Empty);
                        sLine = sLine.Replace(LoadTenderFields.StopOrderIdDetailsWrapperStart, string.Empty);
                        sLine = sLine.Replace(sContent, string.Join(string.Empty, sLines));
                        sLine = sLine.Replace(LoadTenderFields.StopOrderIdDetailsWrapperEnd, string.Empty);
                        sLine = sLine.Replace(LoadTenderFields.StopOrderIdDetailsOuterWrapperEnd, string.Empty);
                    } else{
                        var sContent = sLine.RetrieveWrapperSection(LoadTenderFields.StopOrderIdDetailsOuterWrapperStart, LoadTenderFields.StopOrderIdDetailsOuterWrapperEnd);
                        sLine = sLine.Replace(LoadTenderFields.StopOrderIdDetailsOuterWrapperStart, string.Empty);
                        sLine = sLine.Replace(sContent, string.Empty);
                        sLine = sLine.Replace(LoadTenderFields.StopOrderIdDetailsOuterWrapperEnd, string.Empty);
                    }
                    lines.Add(sLine);
                }
                template = template.Replace(content, string.Join(string.Empty, lines.ToArray()));
                template = template.Replace(LoadTenderFields.StopWrapperStart, string.Empty);
                template = template.Replace(LoadTenderFields.StopOrderIdDetailsWrapperStart, string.Empty);
                template = template.Replace(LoadTenderFields.StopOrderIdDetailsWrapperEnd, string.Empty);
                template = template.Replace(LoadTenderFields.StopWrapperEnd, string.Empty);
            }
            else
            {
                if (template.Contains(LoadTenderFields.StopWrapperStart))
                {
                    var content = template.RetrieveWrapperSection(LoadTenderFields.StopWrapperStart, LoadTenderFields.StopWrapperEnd);
                    template = template.Replace(LoadTenderFields.StopWrapperStart, string.Empty);
                    template = template.Replace(content, string.Empty);
                    template = template.Replace(LoadTenderFields.StopWrapperEnd, string.Empty);
                }

                if (template.Contains(LoadTenderFields.StopOrderIdDetailsWrapperEnd))
                {
                    var content = template.RetrieveWrapperSection(LoadTenderFields.StopOrderIdDetailsWrapperStart, LoadTenderFields.StopOrderIdDetailsWrapperEnd);
                    template = template.Replace(LoadTenderFields.StopOrderIdDetailsWrapperStart, string.Empty);
                    template = template.Replace(content, string.Empty);
                    template = template.Replace(LoadTenderFields.StopOrderIdDetailsWrapperEnd, string.Empty);
                }
            }

            //Shipment Weight Package and Quantity Data
            var shipmentData = loadTender.ShipmentWeightAndTrackingData ?? new ShipmentWghtPkgAndQtyData();
            template = template.Replace(LoadTenderFields.ShipmentWghtPkgAndQtyDataWeightUnitCode, shipmentData.WeightUnitCode.GetDescription());
            template = template.Replace(LoadTenderFields.ShipmentWghtPkgAndQtyDataWeight, shipmentData.Weight.GetString());
            template = template.Replace(LoadTenderFields.ShipmentWghtPkgAndQtyDataLadingPieces, shipmentData.LadingPieces.GetString());
            template = template.Replace(LoadTenderFields.ShipmentWghtPkgAndQtyDataLadingPackages, shipmentData.LadingPackages.GetString());

            //List of Changes
            if (template.Contains(LoadTenderFields.ChangeListWrapperStart) && differences != null && differences.Any())
            {
                var content = template.RetrieveWrapperSection(LoadTenderFields.ChangeListWrapperStart, LoadTenderFields.ChangeListWrapperEnd);
                var lines = differences
                    .Select(d => content
                        .Replace(LoadTenderFields.ChangeListField, d.Description)
                        .Replace(LoadTenderFields.ChangeListOldValue, d.OldValue)
                        .Replace(LoadTenderFields.ChangeListNewValue, d.NewValue))
                    .ToArray();
                               
                template = template.Replace(LoadTenderFields.ChangeListOuterWrapperStart, string.Empty);
                template = template.Replace(LoadTenderFields.ChangeListWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(LoadTenderFields.ChangeListWrapperEnd, string.Empty);
                template = template.Replace(LoadTenderFields.ChangeListOuterWrapperEnd, string.Empty);
            }
            else
            {
                var content = template.RetrieveWrapperSection(LoadTenderFields.ChangeListOuterWrapperStart, LoadTenderFields.ChangeListOuterWrapperEnd);
                template = template.Replace(LoadTenderFields.ChangeListOuterWrapperStart, string.Empty);
                template = template.Replace(content, string.Empty);
                template = template.Replace(LoadTenderFields.ChangeListOuterWrapperEnd, string.Empty);
            }

            return template;
        }


		public static string GenerateLoadTenderResponseNotification(this HttpServerUtility server, LoadResponse loadTenderResponse, Vendor vendor, bool removeBodyWrapper = false)
		{
			// clean up logo URL
			var logoUrl = WebApplicationConstants.AppDefaultLogoUrl;
			if (logoUrl.StartsWith("~"))
				logoUrl = logoUrl.Replace("~", WebApplicationSettings.SiteRoot);

			// get template
			var template = server.ReadFromFile(SystemTemplates.Edi990DetailTemplate).FromUtf8Bytes();

			var pageAddress = XmlConnectRecordView.PageAddress;
			pageAddress = WebApplicationSettings.SiteRoot.EndsWith("/")
					? WebApplicationSettings.SiteRoot + pageAddress.Replace("~/", string.Empty)
					: WebApplicationSettings.SiteRoot + pageAddress.Replace("~", string.Empty);

			// body wrapper
			if (removeBodyWrapper)
			{
				template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
				template = template.Replace(BaseFields.BodyWrapperEnd, WebApplicationSettings.Brand);
			}

			//Beginning Segment Shipment Information
			var beginningSegment = loadTenderResponse.BeginningSegment ?? new BeginningSegmentBooking();
			template = template.Replace(LoadTenderFields.ShipmentIdNumber, beginningSegment.ShipmentIdentificationNumber);
			template = template.Replace(LoadTenderFields.ShipmentInformationAccountName, vendor == null ? string.Empty : vendor.Name);
			template = template.Replace(LoadTenderFields.BeginningSegmentDate, beginningSegment.Date);
			template = template.Replace(LoadTenderFields.Status, beginningSegment.ReservationActionCode.GetDescription());
			template = template.Replace(LoadTenderFields.StatusMessage, beginningSegment.DeclineReason.GetString());
			template = template.Replace(LoadTenderFields.LogoUrl, logoUrl);
			template = template.Replace(LoadTenderFields.PageAddress, pageAddress);
			template = template.Replace(LoadTenderFields.DocumentTypeKey, WebApplicationConstants.XmlConnectRecordsDocTypeKey);
			template = template.Replace(LoadTenderFields.DocumentType, EdiDocumentType.EDI204.ToInt().ToString());
			template = template.Replace(LoadTenderFields.ShipmentIdNumberKey, WebApplicationConstants.XmlConnectRecordsShipmentIdKey);


			return template;
		}

		public static string GenerateLoadTenderResponseEmail(this HttpServerUtility server, LoadOrder load, Vendor vendor, bool removeBodyWrapper = false)
		{
			// clean up logo URL
			var logoUrl = WebApplicationConstants.AppDefaultLogoUrl;
			if (logoUrl.StartsWith("~"))
				logoUrl = logoUrl.Replace("~", WebApplicationSettings.SiteRoot);

			// get template
			var template = server.ReadFromFile(SystemTemplates.EmailTenderResponseTemplate).FromUtf8Bytes();

			var pageAddress = LoadOrderView.PageAddress;
			pageAddress = WebApplicationSettings.SiteRoot.EndsWith("/")
					? WebApplicationSettings.SiteRoot + pageAddress.Replace("~/", string.Empty)
					: WebApplicationSettings.SiteRoot + pageAddress.Replace("~", string.Empty);

			// body wrapper
			if (removeBodyWrapper)
			{
				template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
				template = template.Replace(BaseFields.BodyWrapperEnd, WebApplicationSettings.Brand);
			}
			
			template = template.Replace(LoadTenderFields.ShipmentIdNumber, load.LoadOrderNumber);
			template = template.Replace(LoadTenderFields.ShipmentInformationAccountName, vendor == null ? string.Empty : vendor.Name);
			template = template.Replace(LoadTenderFields.BeginningSegmentDate, DateTime.Now.ToString() );
			template = template.Replace(LoadTenderFields.Status, XmlConnectStatus.Accepted.ToString());
			template = template.Replace(LoadTenderFields.StatusMessage, string.Empty);
			template = template.Replace(LoadTenderFields.LogoUrl, logoUrl);
			template = template.Replace(LoadTenderFields.PageAddress, pageAddress);
			template = template.Replace(LoadTenderFields.ShipmentIdNumberKey, WebApplicationConstants.TransferNumber);

			return template;
		}
    }
}