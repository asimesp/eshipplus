﻿namespace LogisticsPlus.Eship.WebApplication.Documents
{
    public class BookingRequestFields
    {
        //BookingRequest Details
        public const string BookingRequestNumber = "[#BookingRequestNumber#]";
        public const string BookingRequestDate = "[#BookingRequestDate#]";
        public const string DesiredPickupDate = "[#DesiredPickupDate#]";
        public const string EarliestPickup = "[#EarliestPickup#]";
        public const string LatestPickup = "[#LatestPickup#]";
        public const string DesiredDeliveryDate = "[#DesiredDeliveryDate#]";
        public const string EarliestDelivery = "[#EarliestDelivery#]";
        public const string LatestDelivery = "[#LatestDelivery#]";
        public const string LogoUrl = "[#LogoUrl#]";
        public const string UserName = "[#UserName#]";
        public const string UserPhone = "[#UserPhone#]";
        public const string UserEmail = "[#UserEmail#]";
        public const string PurchaseOrderNumber = "[#PurchaseOrderNumber#]";

        //Bill to
        public const string BillToDescription = "[#BillToDescription#]";
        public const string BillToStreet1 = "[#BillToStreet1#]";
        public const string BillToStreet2 = "[#BillToStreet2#]";
        public const string BillToCity = "[#BillToCity#]";
        public const string BillToState = "[#BillToState#]";
        public const string BillToPostalCode = "[#BillToPostalCode#]";
        public const string BillToCountry = "[#BillToCountry#]";

        //Origin
        public const string OriginDescription = "[#OriginDescription#]";
        public const string OriginStreet1 = "[#OriginStreet1#]";
        public const string OriginStreet2 = "[#OriginStreet2#]";
        public const string OriginCity = "[#OriginCity#]";
        public const string OriginState = "[#OriginState#]";
        public const string OriginPostalCode = "[#OriginPostalCode#]";
        public const string OriginCountry = "[#OriginCountry#]";
        
        public const string OriginPrimaryContactEnabled = "[#OriginPrimaryContactEnabled#]";
        public const string OriginPrimaryContact = "[#OriginPrimaryContact#]";
        public const string OriginPrimaryPhone = "[#OriginPrimaryPhone#]";
        public const string OriginPrimaryFax = "[#OriginPrimaryFax#]";
        public const string OriginPrimaryMobile = "[#OriginPrimaryMobile#]";
        public const string OriginPrimaryEmail = "[#OriginPrimaryEmail#]";

        public const string OriginReferencesWrapperStart = "[#OriginReferencesWrapperStart#]";
        public const string OriginReferencesWrapperEnd = "[#OriginReferencesWrapperEnd#]";
        public const string OriginReferenceName = "[#OriginReferenceName#]";
        public const string OriginReferenceValue = "[#OriginReferenceValue#]";

		public const string OriginInstructions = "[#OriginInstructions#]";

        // destination
        public const string DestinationDescription = "[#DestinationDescription#]";
        public const string DestinationStreet1 = "[#DestinationStreet1#]";
        public const string DestinationStreet2 = "[#DestinationStreet2#]";
        public const string DestinationCity = "[#DestinationCity#]";
        public const string DestinationState = "[#DestinationState#]";
        public const string DestinationPostalCode = "[#DestinationPostalCode#]";
        public const string DestinationCountry = "[#DestinationCountry#]";

        public const string DestinationPrimaryContactEnabled = "[#DestinationPrimaryContactEnabled#]";
        public const string DestinationPrimaryContact = "[#DestinationPrimaryContact#]";
        public const string DestinationPrimaryPhone = "[#DestinationPrimaryPhone#]";
        public const string DestinationPrimaryFax = "[#DestinationPrimaryFax#]";
        public const string DestinationPrimaryMobile = "[#DestinationPrimaryMobile#]";
        public const string DestinationPrimaryEmail = "[#DestinationPrimaryEmail#]";

        public const string DestinationReferencesWrapperStart = "[#DestinationReferencesWrapperStart#]";
        public const string DestinationReferencesWrapperEnd = "[#DestinationReferencesWrapperEnd#]";
        public const string DestinationReferenceName = "[#DestinationReferenceName#]";
        public const string DestinationReferenceValue = "[#DestinationReferenceValue#]";

		public const string DestinationInstructions = "[#DestinationInstructions#]";

        //Equipment
        public const string EquipmentWrapperStart = "[#EquipmentWrapperStart#]";
		public const string EquipmentWrapperEnd = "[#EquipmentWrapperEnd#]";
		public const string Equipment = "[#Equipment#]";

        // Items
        public const string ItemsTotalWeight = "[#ItemsTotalWeight#]";
        public const string ItemsTotalValue = "[#ItemsTotalValue#]";
        public const string ItemsWrapperStart = "[#ItemsWrapperStart#]";
        public const string ItemsWrapperEnd = "[#ItemsWrapperEnd#]";
        public const string ItemQuantity = "[#ItemQuantity#]";
        public const string ItemPieceCount = "[#ItemPieceCount#]";
        public const string ItemPackageType = "[#ItemPackageType#]";
        public const string ItemDescription = "[#ItemDescription#]";
        public const string ItemTotalWeight = "[#ItemTotalWeight#]";
        public const string ItemLength = "[#ItemLength#]";
        public const string ItemWidth = "[#ItemWidth#]";
        public const string ItemHeight = "[#ItemHeight#]";
        public const string ItemFreightClass = "[#ItemFreightClass#]";
        public const string ItemStackable = "[#ItemStackable#]";
        public const string ItemValue = "[#ItemValue#]";
        public const string ItemNMFC = "[#ItemNMFC#]";
        public const string ItemHTS = "[#ItemHTS#]";
        public const string ItemPickup = "[#ItemPickup#]";
        public const string ItemDelivery = "[#ItemDelivery#]";
        public const string ItemComments = "[#ItemComments#]";
		public const string ItemHazardous = "[#ItemHazardous#]";

        // accessorials
        public const string AccessorialsWrapperStart = "[#AccessorialsWrapperStart#]";
        public const string AccessorialsWrapperEnd = "[#AccessorialsWrapperEnd#]";
        public const string AccessorialsDescription = "[#AccessorialsDescription#]";
        public const string AccessorialsCode = "[#AccessorialsCode#]";
    }
}
