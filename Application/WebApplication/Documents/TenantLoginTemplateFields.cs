﻿namespace LogisticsPlus.Eship.WebApplication.Documents
{
	public class TenantLoginTemplateFields
	{
		public const string LogoUrl = "[#LogoUrl#]";
		public const string SiteRoot = "[#SiteRoot#]";
		public const string AccessCode = "[#AccessCode#]";
		public const string Alerts = "[#Alerts#]";
	}
}
