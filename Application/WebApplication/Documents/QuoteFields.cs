﻿namespace LogisticsPlus.Eship.WebApplication.Documents
{
    public class QuoteFields : BaseFields
    {
        //Quote Details
        public const string QuoteNumber = "[#QuoteNumber#]";
        public const string QuoteDate = "[#QuoteDate#]";
    	public const string QuotePrefix = "[#QuotePrefix#]";
        public const string ServiceMode = "[#ServiceMode#]";
        public const string DesiredShipmentDate = "[#DesiredShipmentDate#]";
        public const string LogoUrl = "[#LogoUrl#]";

		public const string QuoteEquipmentWrapperStart = "[#QuoteEquipmentWrapperStart#]";
		public const string QuoteEquipmentWrapperEnd = "[#QuoteEquipmentWrapperEnd#]";
		public const string QuoteEquipment = "[#QuoteEquipment#]";

        // contact
        public const string ContactPhone = "[#ContactPhone#]";
        public const string ContactName = "[#ContactName#]";
        public const string ContactTollFree = "[#ContactTollFree#]";

        //department
    	public const string DepartmentPhone = "[#DepartmentPhone#]";
        public const string DepartmentFax = "[#DepartmentFax#]";
        public const string DepartmentEmail = "[#DepartmentEmail#]";

        //Bill to
        public const string BillToDescription = "[#BillToDescription#]";
        public const string BillToStreet1 = "[#BillToStreet1#]";
        public const string BillToStreet2 = "[#BillToStreet2#]";
        public const string BillToCity = "[#BillToCity#]";
        public const string BillToState = "[#BillToState#]";
        public const string BillToPostalCode = "[#BillToPostalCode#]";
        public const string BillToCountry = "[#BillToCountry#]";

        //Origin
		public const string OriginDescription = "[#OriginDescription#]";
		public const string OriginStreet1 = "[#OriginStreet1#]";
		public const string OriginStreet2 = "[#OriginStreet2#]";
		public const string OriginCity = "[#OriginCity#]";
		public const string OriginState = "[#OriginState#]";
		public const string OriginPostalCode = "[#OriginPostalCode#]";
		public const string OriginCountry = "[#OriginCountry#]";

		public const string OriginPrimaryContactEnabled = "[#OriginPrimaryContactEnabled#]";
		public const string OriginPrimaryContact = "[#OriginPrimaryContact#]";
		public const string OriginPrimaryPhone = "[#OriginPrimaryPhone#]";
		public const string OriginPrimaryFax = "[#OriginPrimaryFax#]";
		public const string OriginPrimaryMobile = "[#OriginPrimaryMobile#]";
		public const string OriginPrimaryEmail = "[#OriginPrimaryEmail#]";

		public const string OriginContactsWrapperStart = "[#OriginContactsWrapperStart#]";
		public const string OriginContactsWrapperEnd = "[#OriginContactsWrapperEnd#]";
		public const string OriginContact = "[#OriginContact#]";
		public const string OriginPhone = "[#OriginPhone#]";
		public const string OriginFax = "[#OriginFax#]";
		public const string OriginMobile = "[#OriginMobile#]";
		public const string OriginEmail = "[#OriginEmail#]";

		public const string OriginInstructions = "[#OriginInstructions#]";

		public const string OriginReferencesWrapperStart = "[#OriginReferencesWrapperStart#]";
		public const string OriginReferencesWrapperEnd = "[#OriginReferencesWrapperEnd#]";
		public const string OriginReferenceName = "[#OriginReferenceName#]";
		public const string OriginReferenceValue = "[#OriginReferenceValue#]";

        // destination
		public const string DestinationDescription = "[#DestinationDescription#]";
		public const string DestinationStreet1 = "[#DestinationStreet1#]";
		public const string DestinationStreet2 = "[#DestinationStreet2#]";
		public const string DestinationCity = "[#DestinationCity#]";
		public const string DestinationState = "[#DestinationState#]";
		public const string DestinationPostalCode = "[#DestinationPostalCode#]";
		public const string DestinationCountry = "[#DestinationCountry#]";

		public const string DestinationPrimaryContactEnabled = "[#DestinationPrimaryContactEnabled#]";
		public const string DestinationPrimaryContact = "[#DestinationPrimaryContact#]";
		public const string DestinationPrimaryPhone = "[#DestinationPrimaryPhone#]";
		public const string DestinationPrimaryFax = "[#DestinationPrimaryFax#]";
		public const string DestinationPrimaryMobile = "[#DestinationPrimaryMobile#]";
		public const string DestinationPrimaryEmail = "[#DestinationPrimaryEmail#]";

		public const string DestinationContactsWrapperStart = "[#DestinationContactsWrapperStart#]";
		public const string DestinationContactsWrapperEnd = "[#DestinationContactsWrapperEnd#]";
		public const string DestinationContact = "[#DestinationContact#]";
		public const string DestinationPhone = "[#DestinationPhone#]";
		public const string DestinationFax = "[#DestinationFax#]";
		public const string DestinationMobile = "[#DestinationMobile#]";
		public const string DestinationEmail = "[#DestinationEmail#]";

		public const string DestinationInstructions = "[#DestinationInstructions#]";

		public const string DestinationReferencesWrapperStart = "[#DestinationReferencesWrapperStart#]";
		public const string DestinationReferencesWrapperEnd = "[#DestinationReferencesWrapperEnd#]";
		public const string DestinationReferenceName = "[#DestinationReferenceName#]";
		public const string DestinationReferenceValue = "[#DestinationReferenceValue#]";

        // Items
        public const string ItemsTotalWeight = "[#ItemsTotalWeight#]";
		public const string ItemsTotalValue = "[#ItemsTotalValue#]";
		public const string ItemsWrapperStart = "[#ItemsWrapperStart#]";
		public const string ItemsWrapperEnd = "[#ItemsWrapperEnd#]";
		public const string ItemQuantity = "[#ItemQuantity#]";
		public const string ItemPieceCount = "[#ItemPieceCount#]";
		public const string ItemPackageType = "[#ItemPackageType#]";
		public const string ItemDescription = "[#ItemDescription#]";
		public const string ItemTotalWeight = "[#ItemTotalWeight#]";
		public const string ItemLength = "[#ItemLength#]";
		public const string ItemWidth = "[#ItemWidth#]";
		public const string ItemHeight = "[#ItemHeight#]";
		public const string ItemFreightClass = "[#ItemFreightClass#]";
		public const string ItemStackable = "[#ItemStackable#]";
		public const string ItemValue = "[#ItemValue#]";
		public const string ItemNMFC = "[#ItemNMFC#]";
		public const string ItemHTS = "[#ItemHTS#]";
		public const string ItemPickup = "[#ItemPickup#]";
		public const string ItemDelivery = "[#ItemDelivery#]";
		public const string ItemComments = "[#ItemComments#]";
		public const string ItemHazardous = "[#ItemHazardous#]";
		
		// stops
		public const string StopsWrapperStart = "[#StopsWrapperStart#]";
		public const string StopsWrapperEnd = "[#StopsWrapperEnd#]";
		public const string StopDescription = "[#StopDescription#]";
		public const string StopStreet1 = "[#StopStreet1#]";
		public const string StopStreet2 = "[#StopStreet2#]";
		public const string StopCity = "[#StopCity#]";
		public const string StopState = "[#StopState#]";
		public const string StopPostalCode = "[#StopPostalCode#]";
		public const string StopCountry = "[#StopCountry#]";

		public const string StopPrimaryContactEnabled = "[#StopPrimaryContactEnabled#]";
		public const string StopPrimaryContact = "[#StopPrimaryContact#]";
		public const string StopPrimaryPhone = "[#StopPrimaryPhone#]";
		public const string StopPrimaryFax = "[#StopPrimaryFax#]";
		public const string StopPrimaryMobile = "[#StopPrimaryMobile#]";
		public const string StopPrimaryEmail = "[#StopPrimaryEmail#]";

		public const string StopContactsWrapperStart = "[#StopContactsWrapperStart#]";
		public const string StopContactsWrapperEnd = "[#StopContactsWrapperEnd#]";
		public const string StopContact = "[#StopContact#]";
		public const string StopPhone = "[#StopPhone#]";
		public const string StopFax = "[#StopFax#]";
		public const string StopMobile = "[#StopMobile#]";
		public const string StopEmail = "[#StopEmail#]";

		public const string StopInstructions = "[#StopInstructions#]";

        //Charges
        public const string ChargesWrapperStart = "[#ChargesWrapperStart#]";
        public const string ChargesWrapperEnd = "[#ChargesWrapperEnd#]";
        public const string ChargesDescription = "[#ChargesDescription#]";
        public const string ChargesComment = "[#ChargesComment#]";
        public const string ChargesAmountDue = "[#ChargesAmountDue#]";
        public const string ChargesTotalAmount = "[#ChargesTotalAmount#]";


        //Notes
        public const string NotesWrapperStart = "[#NotesWrapperStart#]";
        public const string NotesWrapperEnd = "[#NotesWrapperEnd#]";
        public const string NotesMessage = "[#NotesMessage#]";

		// hazardous materials
		public const string HazardousMaterialWrapperStart = "[#HazardousMaterialWrapperStart#]";
		public const string HazardousMaterialWrapperEnd = "[#HazardousMaterialWrapperEnd#]";
		public const string HazardousMaterialContact = "[#HazardousMaterialContact#]";
		public const string HazardousMaterialPhone = "[#HazardousMaterialPhone#]";
		public const string HazardousMaterialMobile = "[#HazardousMaterialMobile#]";
		public const string HazardousMaterialEmail = "[#HazardousMaterialEmail#]";
    }
}
