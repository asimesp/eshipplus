﻿namespace LogisticsPlus.Eship.WebApplication.Documents
{
	public class BaseFields
	{
		// body wrapper
		public const string BodyWrapperStart = "[#BodyWrapperStart#]";
		public const string BodyWrapperEnd = "[#BodyWrapperEnd#]";
	}
}
