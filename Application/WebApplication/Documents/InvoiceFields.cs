﻿namespace LogisticsPlus.Eship.WebApplication.Documents
{
    public class InvoiceFields
    {
        //Details
    	public const string UploadedImages = "[#UploadedImages#]";
        public const string InvoiceNumber = "[#InvoiceNumber#]";
		public const string TotalLineLabel = "[#TotalLineLabel#]";
        
        public const string InvoiceDate = "[#InvoiceDate#]";
        public const string DueDate = "[#DueDate#]";
        public const string SpecialInstructions = "[#SpecialInstructions#]";
        public const string InvoiceType = "[#InvoiceType#]";
        public const string PostDate = "[#PostDate#]";
        public const string PaidAmount = "[#PaidAmount#]";
        public const string LogoUrl = "[#LogoUrl#]";
        public const string ShipmentDate = "[#ShipmentDate#]";
        public const string ActualDeliveryDate = "[#ActualDeliveryDate#]";
        public const string ServiceMode = "[#ServiceMode#]";
		public const string CustomerControlAccountNumber  = "[#CustomerControlAccountNumber#]";
        public const string DetailDateHeader = "[#DetailDateTypeHeader#]";
		public const string InvoiceDetailsHeader = "[#InvoiceDetailsHeader#]";
        public const string LatePaymentMessage = "[#LatePaymentMessage#]";
        public const string PaymentMessageWrapperStart = "[#PaymentMessageWrapperStart#]";
        public const string PaymentMessageWrapperEnd = "[#PaymentMessageWrapperEnd#]";

		//Original Invoice
		public const string OriginalInvoiceWrapperStart = "[#OriginalInvoiceWrapperStart#]";
		public const string OriginalInvoiceWrapperEnd = "[#OriginalInvoiceWrapperEnd#]";
		public const string OriginalInvoiceNumber = "[#OriginalInvoiceNumber#]";
		public const string OriginalInvoicePrefix = "[#OriginalInvoicePrefix#]";

        //Customer
        public const string CustomerTerms = "[#CustomerTerms#]";
        public const string CustomerNumber = "[#CustomerNumber#]";
    	public const string CustomerName = "[#CustomerName#]";

        public const string Prefix = "[#Prefix#]";

        // vendor
        public const string PrimaryVendorName = "[#PrimaryVendorName#]";
        public const string PrimaryVendorPro = "[#PrimaryVendorPro#]";
        public const string VendorsWrapperStart = "[#VendorsWrapperStart#]";
        public const string VendorsWrapperEnd = "[#VendorsWrapperEnd#]";
        public const string VendorName = "[#VendorName#]";
        public const string VendorPro = "[#VendorPro#]";

        //Bill to
        public const string BillToDescription = "[#BillToDescription#]";
        public const string BillToStreet1 = "[#BillToStreet1#]";
        public const string BillToStreet2 = "[#BillToStreet2#]";
        public const string BillToCity = "[#BillToCity#]";
        public const string BillToState = "[#BillToState#]";
        public const string BillToPostalCode = "[#BillToPostalCode#]";
        public const string BillToCountry = "[#BillToCountry#]";

        // origin
        public const string OriginDescription = "[#OriginDescription#]";
        public const string OriginStreet1 = "[#OriginStreet1#]";
        public const string OriginStreet2 = "[#OriginStreet2#]";
        public const string OriginCity = "[#OriginCity#]";
        public const string OriginState = "[#OriginState#]";
        public const string OriginPostalCode = "[#OriginPostalCode#]";
        public const string OriginCountry = "[#OriginCountry#]";

		public const string OriginPrimaryContactEnabled = "[#OriginPrimaryContactEnabled#]";
		public const string OriginPrimaryContact = "[#OriginPrimaryContact#]";
		public const string OriginPrimaryPhone = "[#OriginPrimaryPhone#]";
		public const string OriginPrimaryFax = "[#OriginPrimaryFax#]";
		public const string OriginPrimaryMobile = "[#OriginPrimaryMobile#]";
		public const string OriginPrimaryEmail = "[#OriginPrimaryEmail#]";

		public const string OriginContactsWrapperStart = "[#OriginContactsWrapperStart#]";
		public const string OriginContactsWrapperEnd = "[#OriginContactsWrapperEnd#]";
		public const string OriginContact = "[#OriginContact#]";
		public const string OriginPhone = "[#OriginPhone#]";
		public const string OriginFax = "[#OriginFax#]";
		public const string OriginMobile = "[#OriginMobile#]";
		public const string OriginEmail = "[#OriginEmail#]";

		public const string OriginInstructions = "[#OriginInstructions#]";

		public const string OriginReferencesWrapperStart = "[#OriginReferencesWrapperStart#]";
		public const string OriginReferencesWrapperEnd = "[#OriginReferencesWrapperEnd#]";
		public const string OriginReferenceName = "[#OriginReferenceName#]";
		public const string OriginReferenceValue = "[#OriginReferenceValue#]";

        // destination
        public const string DestinationDescription = "[#DestinationDescription#]";
        public const string DestinationStreet1 = "[#DestinationStreet1#]";
        public const string DestinationStreet2 = "[#DestinationStreet2#]";
        public const string DestinationCity = "[#DestinationCity#]";
        public const string DestinationState = "[#DestinationState#]";
        public const string DestinationPostalCode = "[#DestinationPostalCode#]";
        public const string DestinationCountry = "[#DestinationCountry#]";

		public const string DestinationPrimaryContactEnabled = "[#DestinationPrimaryContactEnabled#]";
		public const string DestinationPrimaryContact = "[#DestinationPrimaryContact#]";
		public const string DestinationPrimaryPhone = "[#DestinationPrimaryPhone#]";
		public const string DestinationPrimaryFax = "[#DestinationPrimaryFax#]";
		public const string DestinationPrimaryMobile = "[#DestinationPrimaryMobile#]";
		public const string DestinationPrimaryEmail = "[#DestinationPrimaryEmail#]";

		public const string DestinationContactsWrapperStart = "[#DestinationContactsWrapperStart#]";
		public const string DestinationContactsWrapperEnd = "[#DestinationContactsWrapperEnd#]";
		public const string DestinationContact = "[#DestinationContact#]";
		public const string DestinationPhone = "[#DestinationPhone#]";
		public const string DestinationFax = "[#DestinationFax#]";
		public const string DestinationMobile = "[#DestinationMobile#]";
		public const string DestinationEmail = "[#DestinationEmail#]";

		public const string DestinationInstructions = "[#DestinationInstructions#]";

		public const string DestinationReferencesWrapperStart = "[#DestinationReferencesWrapperStart#]";
		public const string DestinationReferencesWrapperEnd = "[#DestinationReferencesWrapperEnd#]";
		public const string DestinationReferenceName = "[#DestinationReferenceName#]";
		public const string DestinationReferenceValue = "[#DestinationReferenceValue#]";

        //PrimaryContact
        public const string PrimaryContactName = "[#Name#]";
        public const string PrimaryContactPhone = "[#Phone#]";
        public const string PrimaryContactMobile = "[#Mobile#]";
        public const string PrimaryContactFax = "[#Fax#]";
        public const string PrimaryContactEmail = "[#Email#]"; 

        //Additional Contacts
        public const string ContactsWrapperStart = "[#ContactsWrapperStart#]";
        public const string ContactName = "[#ContactName#]";
        public const string ContactPhone = "[#ContactPhone#]";
        public const string ContactMobile = "[#ContactMobile#]";
        public const string ContactFax = "[#ContactFax#]";
        public const string ContactEmail = "[#ContactEmail#]";
        public const string ContactsWrapperEnd = "[#ContactWrapperEnd#]";

        //InvoiceDetail
        public const string InvoiceDetailWrapperStart = "[#InvoiceDetailWrapperStart#]";
        public const string Quantity = "[#Quantity#]";
        public const string UnitSell = "[#UnitSell#]";
        public const string UnitDiscount = "[#UnitDiscount#]";
        public const string ReferenceNumber = "[#ReferenceNumber#]";
        public const string ReferenceType = "[#ReferenceType#]";
        public const string ShipmentPrefix = "[#ShipmentPrefix#]";
        public const string Comment = "[#Comment#]";
        public const string Description = "[#Description#]";
        public const string ChargeCode = "[#ChargeCode#]";
        public const string ChargeCodeDescription = "[#ChargeCodeDescription#]";
		public const string AccountBucket = "[#AccountBucket#]";
		public const string AccountBucketDescription = "[#AccountBucketDescription#]";
		public const string PrefixRef = "[#PrefixRef#]";
		public const string PrefixRefDescription = "[#PrefixRefDescription#]";

        public const string AmountDue = "[#AmountDue#]";
        public const string ShipmentNumber = "[#ShipmentNumber#]";
		public const string ShipperBOL = "[#ShipperBOL#]";

		public const string FreightCharges = "[#FreightCharges#]";
		public const string FuelCharges = "[#FuelCharges#]";
		public const string AccessorialCharges = "[#AccessorialCharges#]";
		public const string MiscCharges = "[#MiscCharges#]";
		public const string TotalCharges = "[#TotalCharges#]";

        public const string InvoiceDetailWrapperEnd = "[#InvoiceDetailWrapperEnd#]";

		//Invoice Detail Info
		public const string PurchaseOrderNumber = "[#PurchaseOrderNumber#]";
		public const string ShipperReferenceNumber = "[#ShipperReferenceNumber#]";
		public const string ShipmentReferenceName = "[#ShipmentReferenceName#]";
		public const string ShipmentReferenceValue = "[#ShipmentReferenceValue#]";
		public const string InvoiceDetailShipmentReferencesWrapperStart = "[#InvoiceDetailShipmentReferencesWrapperStart#]";
		public const string InvoiceDetailShipmentReferencesWrapperEnd = "[#InvoiceDetailShipmentReferencesWrapperEnd#]";
        
        public const string TotalAmountDue = "[#TotalAmountDue#]";

        //Notes
        public const string NotesWrapperStart = "[#NotesWrapperStart#]";
        public const string Note = "[#Note#]";
		public const string NotesWrapperEnd = "[#NotesWrapperEnd#]";

		//service Ticket
    	public const string ServiceTicketNumber = "[#ServiceTicketNumber#]";
		public const string ServiceTicketDate = "[#ServiceTicketDate#]";

        //Service Ticket Items
        public const string ServiceTicketItemsWrapperStart = "[#ServiceTicketItemsWrapperStart#]";
        public const string ServiceTicketComment = "[#ServiceTicketComment#]";
		public const string ServiceTicketDescription = "[#ServiceTicketDescription#]";
        public const string ServiceTicketItemsWrapperEnd = "[#ServiceTicketItemsWrapperEnd#]";

        // Shipment Items
        public const string ItemsTotalWeight = "[#ItemsTotalWeight#]";
        public const string ItemsTotalValue = "[#ItemsTotalValue#]";
        public const string ItemsWrapperStart = "[#ItemsWrapperStart#]";
        public const string ItemsWrapperEnd = "[#ItemsWrapperEnd#]";
        public const string ItemQuantity = "[#ItemQuantity#]";
        public const string ItemPieceCount = "[#ItemPieceCount#]";
        public const string ItemPackageType = "[#ItemPackageType#]";
        public const string ItemDescription = "[#ItemDescription#]";
        public const string ItemTotalWeight = "[#ItemTotalWeight#]";
        public const string ItemLength = "[#ItemLength#]";
        public const string ItemWidth = "[#ItemWidth#]";
        public const string ItemHeight = "[#ItemHeight#]";
        public const string ItemFreightClass = "[#ItemFreightClass#]";
        public const string ItemStackable = "[#ItemStackable#]";
        public const string ItemValue = "[#ItemValue#]";
        public const string ItemNMFC = "[#ItemNMFC#]";
        public const string ItemHTS = "[#ItemHTS#]";
        public const string ItemComments = "[#ItemComments#]";

		//shipment Misc Notes
		public const string MiscellaneousField1 = "[#MiscellaneousField1#]";
		public const string MiscellaneousField2 = "[#MiscellaneousField2#]";

        //Original Invoice Summary
        public const string OriginalInvoiceSummaryWrapperStart = "[#OriginalInvoiceSummaryWrapperStart#]";
        public const string OriginalInvoiceSummaryWrapperEnd = "[#OriginalInvoiceSummaryWrapperEnd#]";
        public const string OriginalInvoiceSummaryReferenceType = "[#OriginalInvoiceSummaryReferenceType#]";
        public const string OriginalInvoiceSummaryReferenceNumber = "[#OriginalInvoiceSummaryReferenceNumber#]";
        public const string OriginalInvoiceSummaryTotal = "[#OriginalInvoiceSummaryTotal#]";
        public const string OriginalInvoiceReferencePrefix = "[#OriginalInvoiceReferencePrefix#]";


    }
}
