﻿namespace LogisticsPlus.Eship.WebApplication.Documents
{
	public class EdiFields
	{
		public const string ReceiverId = "[#ReceiverId#]";
		public const string DocumentType = "[#DocumentType#]";
		public const string UniqueKey = "[#UniqueKey#]";
		public const string DateCreated = "[#DateCreated#]";
		public const string Body = "[#Body#]";
	}
}
