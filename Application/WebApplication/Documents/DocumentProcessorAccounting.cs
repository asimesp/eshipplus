﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.WebApplication.Members;
using LogisticsPlus.Eship.WebApplication.Members.Accounting;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Documents
{
    public static partial class DocumentProcessor
    {
        public static string GenerateInvoice(this MemberPageBase page, Invoice invoice)
        {
            return page.GenerateInvoice(invoice, false);
        }

        public static string GenerateInvoice(this MemberPageBase page, Invoice invoice, bool removeBodyWrapper)
        {
            var content = string.Empty;

            if (invoice.Details.GroupBy(d => d.ReferenceNumber, d => d).Select(g => g.Key).Count() > 1) // multiple
            {
                content = page.GenerateStatementInvoice(invoice, removeBodyWrapper);
                page.LogInvoicePrint(invoice);
                return content;
            }

            Invoice originalInvoice = null;
            DetailReferenceType detailReferenceType;
            switch (invoice.InvoiceType)
            {
                case InvoiceType.Supplemental:
                case InvoiceType.Credit:

                    originalInvoice = invoice.OriginalInvoice;
                    if (invoice.Details.All(d => string.IsNullOrEmpty(d.ReferenceNumber)))
                    {
                        var refTypeCount = originalInvoice.Details.GroupBy(d => d.ReferenceNumber, d => d).Select(g => g.Key).Count();
                        detailReferenceType = refTypeCount == 1
                                                  ? originalInvoice.Details.First().ReferenceType
                                                  : DetailReferenceType.Miscellaneous;
                    }
                    else
                    {
                        var refTypeCount = invoice.Details.GroupBy(d => d.ReferenceNumber, d => d).Select(g => g.Key).Count();
                        detailReferenceType = refTypeCount == 1
                                                  ? invoice.Details.First().ReferenceType
                                                  : DetailReferenceType.Miscellaneous;
                    }

                    break;
                default:
                    detailReferenceType = invoice.Details.First().ReferenceType;
                    break;
            }

            switch (detailReferenceType)
            {
                case DetailReferenceType.ServiceTicket:
                    content = page.GenerateServiceTicketInvoice(invoice, originalInvoice, removeBodyWrapper);
                    page.LogInvoicePrint(invoice);
                    break;
                case DetailReferenceType.Miscellaneous:
                    content = page.GenerateMiscellaneousInvoice(invoice, removeBodyWrapper);
                    page.LogInvoicePrint(invoice);
                    break;
                case DetailReferenceType.Shipment:
                    content = page.GenerateShipmentInvoice(invoice, originalInvoice, removeBodyWrapper);
                    page.LogInvoicePrint(invoice);
                    break;
            }
            return content;
        }


        private static string GenerateStatementInvoice(this MemberPageBase page, Invoice invoice, bool removeBodyWrapper)
        {
            // get template
            Decimals dp;
            var template = page.Server
                .FetchTemplate(invoice.TenantId, invoice.PrefixId, ServiceMode.NotApplicable,
                               DocumentTemplateCategory.StatementInvoice, out dp);

            // logo url
            var logoUrl = invoice.CustomerLocation.Customer.RetrievePageLogoUrl();
            var siteRoot = page.Request.ResolveSiteRootWithHttp();
            logoUrl = logoUrl.Replace("~", siteRoot);

            // body wrapper
            if (removeBodyWrapper)
            {
                template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
                template = template.Replace(BaseFields.BodyWrapperEnd, WebApplicationSettings.Brand);
            }

            // general details
            var combine = siteRoot + WebApplicationSettings.UploadedImagesFolder(invoice.TenantId).Replace("~", string.Empty);
            template = template.Replace(InvoiceFields.UploadedImages, combine);
            template = template.Replace(InvoiceFields.InvoiceNumber, invoice.InvoiceNumber);
            var prefix = invoice.Prefix == null || invoice.HidePrefix ? string.Empty : invoice.Prefix.Code;
            template = template.Replace(InvoiceFields.Prefix, prefix);
            template = template.Replace(InvoiceFields.InvoiceDate, invoice.InvoiceDate == DateUtility.SystemEarliestDateTime
                ? string.Empty
                : invoice.InvoiceDate.FormattedShortDate());
            template = template.Replace(InvoiceFields.PostDate, invoice.PostDate == DateUtility.SystemEarliestDateTime
                ? string.Empty
                : invoice.PostDate.FormattedShortDate());
            template = template.Replace(InvoiceFields.DueDate, invoice.DueDate == DateUtility.SystemEarliestDateTime
                ? string.Empty
                : invoice.DueDate.FormattedShortDate());
            template = template.Replace(InvoiceFields.SpecialInstructions, invoice.SpecialInstruction);
            template = template.Replace(InvoiceFields.InvoiceType, invoice.InvoiceType.ToString());
            template = template.Replace(InvoiceFields.PaidAmount, invoice.PaidAmount.ToString());
            template = template.Replace(InvoiceFields.LogoUrl, logoUrl);
            template = template.Replace(InvoiceFields.CustomerControlAccountNumber, invoice.CustomerControlAccountNumber);
            template = template.Replace(InvoiceFields.TotalLineLabel, invoice.InvoiceType == InvoiceType.Credit ? "Credit Due:" : "Amount Due:");


            // bill to
            var billTo = invoice.CustomerLocation;
            template = template.Replace(InvoiceFields.BillToDescription, billTo.Customer.Name);
            template = template.Replace(InvoiceFields.BillToStreet1, billTo.Street1);
            template = template.Replace(InvoiceFields.BillToStreet2, billTo.Street2);
            template = template.Replace(InvoiceFields.BillToCity, billTo.City);
            template = template.Replace(InvoiceFields.BillToState, billTo.State);
            template = template.Replace(InvoiceFields.BillToPostalCode, billTo.PostalCode);
            template = template.Replace(InvoiceFields.BillToCountry, billTo.Country.Name);

            //shipment details
            if (template.Contains(InvoiceFields.InvoiceDetailShipmentReferencesWrapperStart))
            {
                var content = template.RetrieveWrapperSection(InvoiceFields.InvoiceDetailShipmentReferencesWrapperStart,
                    InvoiceFields.InvoiceDetailShipmentReferencesWrapperEnd);

                var lines = new List<string>();
                var details = invoice.Details
                    .GroupBy(d => string.Format("{0}{1}", d.ReferenceType, d.ReferenceNumber))
                    .Select(g =>
                    {
                        var detail = g.First();
                        return new
                        {
                            detail.ReferenceNumber,
                            detail.ReferenceType,
                            detail.TenantId,
                        };
                    }).Where(g => g.ReferenceType == DetailReferenceType.Shipment)
                    .ToList();
                foreach (var d in details)
                {
                    var sLine = content;

                    sLine = sLine.Replace(InvoiceFields.Prefix, d.ReferenceNumber);
                    sLine = sLine.Replace(InvoiceFields.ReferenceType, d.ReferenceType.FormattedString());

                    if (d.ReferenceType != DetailReferenceType.Shipment) continue;
                    var shipment = new ShipmentSearch().FetchShipmentByShipmentNumber(d.ReferenceNumber, d.TenantId);
                    if (shipment == null) continue;

                    sLine = sLine.Replace(InvoiceFields.CustomerTerms, invoice.InvoiceType == InvoiceType.Credit
                        ? string.Empty
                        : string.Format("Net {0}", shipment.Customer.InvoiceTerms.ToString()));
                    sLine = sLine.Replace(InvoiceFields.ShipmentPrefix, shipment.HidePrefix || shipment.Prefix == null
                        ? string.Empty
                        : shipment.Prefix.Code);
                    sLine = sLine.Replace(InvoiceFields.ShipmentNumber, shipment.ShipmentNumber);
                    sLine = sLine.Replace(InvoiceFields.ShipperBOL, shipment.ShipperBol);
                    sLine = sLine.Replace(InvoiceFields.ShipmentDate, shipment.ActualPickupDate == DateUtility.SystemEarliestDateTime
                        ? string.Empty
                        : shipment.ActualPickupDate.FormattedShortDate());
                    sLine = sLine.Replace(InvoiceFields.ActualDeliveryDate, shipment.ActualDeliveryDate == DateUtility.SystemEarliestDateTime
                        ? string.Empty
                        : shipment.ActualDeliveryDate.FormattedShortDate());

                    sLine = sLine.Replace(InvoiceFields.ServiceMode, shipment.ServiceMode.FormattedString());
                    sLine = sLine.Replace(InvoiceFields.PurchaseOrderNumber, shipment.PurchaseOrderNumber);
                    sLine = sLine.Replace(InvoiceFields.ShipperReferenceNumber, shipment.ShipperReference);
                    sLine = sLine.Replace(InvoiceFields.ItemsTotalWeight, shipment.Items.Sum(item => item.ActualWeight).ToString("f2"));
                    sLine = sLine.Replace(InvoiceFields.MiscellaneousField1, shipment.MiscField1);
                    sLine = sLine.Replace(InvoiceFields.DetailDateHeader, invoice.InvoiceType == InvoiceType.Credit ? "Invoice" : "Due");

                    // vendors
                    if (template.Contains(InvoiceFields.PrimaryVendorName) || template.Contains(InvoiceFields.PrimaryVendorPro))
                    {
                        var sv = shipment.Vendors.FirstOrDefault(v => v.Primary);
                        sLine = sLine.Replace(InvoiceFields.PrimaryVendorName, sv == null ? string.Empty : sv.Vendor.Name);
                        sLine = sLine.Replace(InvoiceFields.PrimaryVendorPro, sv == null ? string.Empty : sv.ProNumber);
                    }


                    if (sLine.Contains(InvoiceFields.ItemsWrapperStart))
                    {
                        var innerContent = sLine.RetrieveWrapperSection(InvoiceFields.ItemsWrapperStart,
                            InvoiceFields.ItemsWrapperEnd);
                        var innerLines = new List<string>();

                        var subDetails = invoice.Details
                            .Where(i => d.ReferenceNumber == i.ReferenceNumber)
                            .ToList();

                        sLine = sLine.Replace(InvoiceFields.TotalCharges, subDetails.Sum(item => item.AmountDue).ToString("f2"));
                        foreach (var charge in subDetails)
                        {
                            var sline2 = innerContent;
                            sline2 = sline2.Replace(InvoiceFields.ChargeCodeDescription, charge.ChargeCode.Description);
                            sline2 = sline2.Replace(InvoiceFields.Comment, charge.Comment);

                            sline2 = sline2.Replace(InvoiceFields.AmountDue, charge.AmountDue.ToString("f2"));
                            innerLines.Add(sline2);
                        }
                        sLine = sLine.Replace(InvoiceFields.ItemsWrapperStart, string.Empty);
                        sLine = sLine.Replace(innerContent, string.Join(string.Empty, innerLines.ToArray()));
                        sLine = sLine.Replace(InvoiceFields.ItemsWrapperEnd, string.Empty);
                    }

                    if (sLine.Contains(InvoiceFields.InvoiceDetailWrapperStart))
                    {
                        var innerContent = sLine.RetrieveWrapperSection(InvoiceFields.InvoiceDetailWrapperStart,
                            InvoiceFields.InvoiceDetailWrapperEnd);
                        var innerLines = new List<string>();
                        foreach (var item in shipment.Items)
                        {
                            var sline2 = innerContent;
                            sline2 = sline2.Replace(InvoiceFields.ItemQuantity, item.Quantity.ToString("D"));
                            sline2 = sline2.Replace(InvoiceFields.ItemDescription, item.Description);
                            sline2 = sline2.Replace(InvoiceFields.ItemPackageType, item.PackageType.TypeName);
                            sline2 = sline2.Replace(InvoiceFields.ItemLength, item.ActualLength.ToString("f2"));
                            sline2 = sline2.Replace(InvoiceFields.ItemHeight, item.ActualHeight.ToString("f2"));
                            sline2 = sline2.Replace(InvoiceFields.ItemWidth, item.ActualWidth.ToString("f2"));
                            sline2 = sline2.Replace(InvoiceFields.ItemTotalWeight, item.ActualWeight.ToString("f2"));
                            sline2 = sline2.Replace(InvoiceFields.ItemFreightClass, item.ActualFreightClass.ToString("f2"));
                            innerLines.Add(sline2);
                        }
                        sLine = sLine.Replace(InvoiceFields.InvoiceDetailWrapperStart, string.Empty);
                        sLine = sLine.Replace(innerContent, string.Join(string.Empty, innerLines.ToArray()));
                        sLine = sLine.Replace(InvoiceFields.InvoiceDetailWrapperEnd, string.Empty);
                    }

                    //origin
                    var sOrigin = shipment.Origin;
                    sLine = sLine.Replace(InvoiceFields.OriginDescription, sOrigin == null ? string.Empty : sOrigin.Description);
                    sLine = sLine.Replace(InvoiceFields.OriginStreet1, sOrigin == null ? string.Empty : sOrigin.Street1);
                    sLine = sLine.Replace(InvoiceFields.OriginStreet2, sOrigin == null ? string.Empty : sOrigin.Street2);
                    sLine = sLine.Replace(InvoiceFields.OriginCity, sOrigin == null ? string.Empty : sOrigin.City);
                    sLine = sLine.Replace(InvoiceFields.OriginState, sOrigin == null ? string.Empty : sOrigin.State);
                    sLine = sLine.Replace(InvoiceFields.OriginPostalCode, sOrigin == null ? string.Empty : sOrigin.PostalCode);
                    sLine = sLine.Replace(InvoiceFields.OriginCountry, sOrigin == null ? string.Empty : sOrigin.Country.Name);

                    var sDestination = shipment.Destination;
                    // destination
                    sLine = sLine.Replace(InvoiceFields.DestinationDescription, sDestination == null ? string.Empty : sDestination.Description);
                    sLine = sLine.Replace(InvoiceFields.DestinationStreet1, sDestination == null ? string.Empty : sDestination.Street1);
                    sLine = sLine.Replace(InvoiceFields.DestinationStreet2, sDestination == null ? string.Empty : sDestination.Street2);
                    sLine = sLine.Replace(InvoiceFields.DestinationCity, sDestination == null ? string.Empty : sDestination.City);
                    sLine = sLine.Replace(InvoiceFields.DestinationState, sDestination == null ? string.Empty : sDestination.State);
                    sLine = sLine.Replace(InvoiceFields.DestinationPostalCode, sDestination == null ? string.Empty : sDestination.PostalCode);
                    sLine = sLine.Replace(InvoiceFields.DestinationCountry, sDestination == null ? string.Empty : sDestination.Country.Name);
                    lines.Add(sLine);
                }

                template = template.Replace(InvoiceFields.InvoiceDetailShipmentReferencesWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines.ToArray()));
                template = template.Replace(InvoiceFields.InvoiceDetailShipmentReferencesWrapperEnd, string.Empty);
            }


            if (template.Contains(InvoiceFields.ServiceTicketItemsWrapperStart))
            {
                var content = template.RetrieveWrapperSection(InvoiceFields.ServiceTicketItemsWrapperStart,
                    InvoiceFields.ServiceTicketItemsWrapperEnd);

                var lines = new List<string>();
                var details = invoice.Details
                    .GroupBy(d => string.Format("{0}{1}", d.ReferenceType, d.ReferenceNumber))
                    .Select(g =>
                    {
                        var detail = g.First();
                        return new
                        {
                            detail.ReferenceNumber,
                            detail.ReferenceType,
                            detail.TenantId,
                        };
                    })
                    .Where(g => g.ReferenceType == DetailReferenceType.ServiceTicket)
                    .ToList();
                foreach (var d in details)
                {
                    var sLine = content;

                    sLine = sLine.Replace(InvoiceFields.Prefix, d.ReferenceNumber);
                    sLine = sLine.Replace(InvoiceFields.ReferenceType, d.ReferenceType.FormattedString());

                    if (d.ReferenceType != DetailReferenceType.ServiceTicket) continue;
                    var serviceTicket = d.ReferenceType == DetailReferenceType.ServiceTicket
                        ? new ServiceTicketSearch().FetchServiceTicketByServiceTicketNumber(d.ReferenceNumber, d.TenantId)
                        : new ServiceTicket();
                    if (serviceTicket == null) continue;

                    sLine = sLine.Replace(InvoiceFields.ServiceTicketNumber, serviceTicket.ServiceTicketNumber);
                    sLine = sLine.Replace(InvoiceFields.ServiceTicketDate, serviceTicket.TicketDate == DateUtility.SystemEarliestDateTime
                        ? string.Empty
                        : serviceTicket.TicketDate.FormattedShortDate());
                    sLine = sLine.Replace(InvoiceFields.DetailDateHeader, invoice.InvoiceType == InvoiceType.Credit ? "Invoice" : "Due");

                    if (sLine.Contains(InvoiceFields.ItemsWrapperStart))
                    {
                        var innerContent = sLine.RetrieveWrapperSection(InvoiceFields.ItemsWrapperStart,
                            InvoiceFields.ItemsWrapperEnd);
                        var innerLines = new List<string>();

                        foreach (var item in serviceTicket.Items)
                        {
                            var sline2 = innerContent;
                            sline2 = sline2.Replace(InvoiceFields.ServiceTicketDescription, item.Description);
                            sline2 = sline2.Replace(InvoiceFields.ServiceTicketComment, item.Comment);
                            innerLines.Add(sline2);
                        }
                        sLine = sLine.Replace(InvoiceFields.ItemsWrapperStart, string.Empty);
                        sLine = sLine.Replace(innerContent, string.Join(string.Empty, innerLines.ToArray()));
                        sLine = sLine.Replace(InvoiceFields.ItemsWrapperEnd, string.Empty);
                    }

                    if (sLine.Contains(InvoiceFields.InvoiceDetailWrapperStart))
                    {
                        var innerContent = sLine.RetrieveWrapperSection(InvoiceFields.InvoiceDetailWrapperStart,
                            InvoiceFields.InvoiceDetailWrapperEnd);
                        var innerLines = new List<string>();

                        var subDetails = invoice.Details
                            .Where(i => d.ReferenceNumber == i.ReferenceNumber)
                            .ToList();

                        sLine = sLine.Replace(InvoiceFields.TotalCharges, subDetails.Sum(item => item.AmountDue).ToString("f2"));
                        foreach (var charge in subDetails)
                        {
                            var sline2 = innerContent;
                            sline2 = sline2.Replace(InvoiceFields.ChargeCodeDescription, charge.ChargeCode.Description);
                            sline2 = sline2.Replace(InvoiceFields.Comment, charge.Comment);
                            sline2 = sline2.Replace(InvoiceFields.AmountDue, charge.AmountDue.ToString("f2"));
                            innerLines.Add(sline2);
                        }
                        sLine = sLine.Replace(InvoiceFields.InvoiceDetailWrapperStart, string.Empty);
                        sLine = sLine.Replace(innerContent, string.Join(string.Empty, innerLines.ToArray()));
                        sLine = sLine.Replace(InvoiceFields.InvoiceDetailWrapperEnd, string.Empty);
                    }
                    lines.Add(sLine);
                }

                template = template.Replace(InvoiceFields.ServiceTicketItemsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines.ToArray()));
                template = template.Replace(InvoiceFields.ServiceTicketItemsWrapperEnd, string.Empty);
            }

            //customer
            template = template.Replace(InvoiceFields.CustomerTerms, invoice.InvoiceType == InvoiceType.Credit ? string.Empty : string.Format("Net {0}", invoice.CustomerLocation.Customer.InvoiceTerms.ToString()));
            template = template.Replace(InvoiceFields.CustomerNumber, invoice.CustomerLocation.Customer.CustomerNumber);
            template = template.Replace(InvoiceFields.CustomerName, invoice.CustomerLocation.Customer.Name);

            // primary contact
            var contact = invoice.CustomerLocation.Contacts.FirstOrDefault(c => c.Primary);
            if (contact != null) contact = invoice.CustomerLocation.Contacts.FirstOrDefault(c => c.Primary);
            template = template.Replace(InvoiceFields.PrimaryContactName, contact == null ? string.Empty : contact.Name);
            template = template.Replace(InvoiceFields.PrimaryContactPhone, contact == null ? string.Empty : contact.Phone);
            template = template.Replace(InvoiceFields.PrimaryContactMobile, contact == null ? string.Empty : contact.Mobile);
            template = template.Replace(InvoiceFields.PrimaryContactFax, contact == null ? string.Empty : contact.Fax);
            template = template.Replace(InvoiceFields.PrimaryContactEmail, contact == null ? string.Empty : contact.Email);

            if (template.Contains(InvoiceFields.ContactsWrapperStart))
            {
                var content = template.RetrieveWrapperSection(InvoiceFields.ContactsWrapperStart, InvoiceFields.ContactsWrapperEnd);
                var lines = invoice.CustomerLocation.Contacts.Any()
                                ? invoice.CustomerLocation.Contacts
                                    .Select(c => content
                                                    .Replace(InvoiceFields.ContactName, c.Name)
                                                    .Replace(InvoiceFields.ContactPhone, c.Phone)
                                                    .Replace(InvoiceFields.ContactMobile, c.Mobile)
                                                    .Replace(InvoiceFields.ContactFax, c.Fax)
                                                    .Replace(InvoiceFields.ContactEmail, c.Email))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(InvoiceFields.ContactsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(InvoiceFields.ContactsWrapperEnd, string.Empty);
            }


            // invoice Details
            var hasDetails = invoice.Details.Any();
            template = template.Replace(InvoiceFields.TotalAmountDue,
                                        hasDetails ? invoice.Details.Sum(i => Math.Round(i.AmountDue, 2)).ToString(dp.CurrencyNumber) : string.Empty);
            if (template.Contains(InvoiceFields.InvoiceDetailWrapperStart))
            {
                var content = template.RetrieveWrapperSection(InvoiceFields.InvoiceDetailWrapperStart, InvoiceFields.InvoiceDetailWrapperEnd);
                var lines = new List<string>();
                var details = invoice.Details
                    .GroupBy(d => string.Format("{0}{1}", d.ReferenceType, d.ReferenceNumber))
                    .Select(g =>
                                {
                                    var detail = g.First();
                                    return new
                                    {
                                        detail.ReferenceNumber,
                                        detail.ReferenceType,
                                        detail.TenantId,
                                        FreightCharges = g.Where(d => d.ChargeCode.Category == ChargeCodeCategory.Freight)
                                                    .Sum(c => c.AmountDue),
                                        FuelCharges = g.Where(d => d.ChargeCode.Category == ChargeCodeCategory.Fuel)
                                                    .Sum(c => c.AmountDue),
                                        AccessorialCharges = g.Where(d => d.ChargeCode.Category == ChargeCodeCategory.Accessorial)
                                                    .Sum(c => c.AmountDue),
                                        ServiceCharges = g.Where(d => d.ChargeCode.Category == ChargeCodeCategory.Service)
                                                    .Sum(c => c.AmountDue)
                                    };
                                })
                    .ToList();
                foreach (var d in details)
                {
                    var sLine = content;
                    sLine = sLine.Replace(InvoiceFields.ReferenceNumber, d.ReferenceNumber);
                    sLine = sLine.Replace(InvoiceFields.ReferenceType, d.ReferenceType.FormattedString());

                    var shipment = d.ReferenceType == DetailReferenceType.Shipment
                                    ? new ShipmentSearch().FetchShipmentByShipmentNumber(d.ReferenceNumber, d.TenantId)
                                    : new Shipment();

                    var serviceTicket = d.ReferenceType == DetailReferenceType.ServiceTicket
                                    ? new ServiceTicketSearch().FetchServiceTicketByServiceTicketNumber(d.ReferenceNumber, d.TenantId)
                                    : new ServiceTicket();

                    sLine = sLine.Replace(InvoiceFields.PurchaseOrderNumber, shipment.PurchaseOrderNumber);
                    sLine = sLine.Replace(InvoiceFields.ShipperReferenceNumber, shipment.ShipperReference);

                    sLine = sLine.Replace(InvoiceFields.PrefixRef, d.ReferenceType == DetailReferenceType.Shipment
                                                                    ? shipment.Prefix == null ? string.Empty : shipment.Prefix.Code
                                                                    : serviceTicket.Prefix == null ? string.Empty : serviceTicket.Prefix.Code);

                    var totalCharges = d.FreightCharges + d.FuelCharges + d.AccessorialCharges + d.ServiceCharges;

                    sLine = sLine.Replace(InvoiceFields.FreightCharges, d.FreightCharges.ToString("f2"));
                    sLine = sLine.Replace(InvoiceFields.FuelCharges, d.FuelCharges.ToString("f2"));
                    sLine = sLine.Replace(InvoiceFields.AccessorialCharges, d.AccessorialCharges.ToString("f2"));
                    sLine = sLine.Replace(InvoiceFields.MiscCharges, d.ServiceCharges.ToString("f2"));
                    sLine = sLine.Replace(InvoiceFields.TotalCharges, totalCharges.ToString("f2"));

                    if (sLine.Contains(InvoiceFields.InvoiceDetailShipmentReferencesWrapperStart))
                    {

                        var rContent = sLine.RetrieveWrapperSection(InvoiceFields.InvoiceDetailShipmentReferencesWrapperStart,
                                                                    InvoiceFields.InvoiceDetailShipmentReferencesWrapperEnd);
                        var rLines = shipment.CustomerReferences.Any()
                                        ? shipment.CustomerReferences
                                            .Select(r => rContent
                                                            .Replace(InvoiceFields.ShipmentReferenceName, r.Name)
                                                            .Replace(InvoiceFields.ShipmentReferenceValue, r.Value))
                                            .ToArray()
                                        : new string[0];

                        sLine = sLine.Replace(InvoiceFields.InvoiceDetailShipmentReferencesWrapperStart, string.Empty);
                        sLine = sLine.Replace(rContent, string.Join(string.Empty, rLines));
                        sLine = sLine.Replace(InvoiceFields.InvoiceDetailShipmentReferencesWrapperEnd, string.Empty);
                    }
                    lines.Add(sLine);
                }

                template = template.Replace(InvoiceFields.InvoiceDetailWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines.ToArray()));
                template = template.Replace(InvoiceFields.InvoiceDetailWrapperEnd, string.Empty);

            }


            return template;
        }

        private static string GenerateServiceTicketInvoice(this MemberPageBase page, Invoice invoice, Invoice originalInvoice, bool removeBodyWrapper)
        {
            // service ticket
            var details = originalInvoice == null ? invoice.Details : originalInvoice.Details;
            var ticket = new ServiceTicketSearch()
                .FetchServiceTicketByServiceTicketNumber(details.First().ReferenceNumber, invoice.TenantId);

            // get template
            Decimals dp;
            var template = page.Server
                .FetchTemplate(invoice.TenantId, invoice.PrefixId, ServiceMode.NotApplicable,
                               DocumentTemplateCategory.ServiceTicketInvoice, out dp);

            // logo url
            var logoUrl = invoice.CustomerLocation.Customer.RetrievePageLogoUrl();
            var siteRoot = page.Request.ResolveSiteRootWithHttp();
            logoUrl = logoUrl.Replace("~", siteRoot);

            // body wrapper
            if (removeBodyWrapper)
            {
                template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
                template = template.Replace(BaseFields.BodyWrapperEnd, WebApplicationSettings.Brand);
            }

            // general details
            var combine = siteRoot + WebApplicationSettings.UploadedImagesFolder(invoice.TenantId).Replace("~", string.Empty);
            template = template.Replace(InvoiceFields.UploadedImages, combine);
            template = template.Replace(InvoiceFields.InvoiceNumber, invoice.InvoiceNumber);
            var prefix = invoice.Prefix == null || invoice.HidePrefix ? string.Empty : invoice.Prefix.Code;
            template = template.Replace(InvoiceFields.Prefix, prefix);
            template = template.Replace(InvoiceFields.InvoiceDate, invoice.InvoiceDate.FormattedShortDate());
            template = template.Replace(InvoiceFields.PostDate, invoice.PostDate.FormattedShortDate());
            template = template.Replace(InvoiceFields.DueDate, invoice.DueDate.FormattedShortDate());
            template = template.Replace(InvoiceFields.SpecialInstructions, invoice.SpecialInstruction);
            template = template.Replace(InvoiceFields.InvoiceType, invoice.InvoiceType.ToString());
            template = template.Replace(InvoiceFields.PaidAmount, invoice.PaidAmount.ToString());
            template = template.Replace(InvoiceFields.LogoUrl, logoUrl);
            template = template.Replace(InvoiceFields.CustomerControlAccountNumber, invoice.CustomerControlAccountNumber);
            template = template.Replace(InvoiceFields.TotalLineLabel, invoice.InvoiceType == InvoiceType.Credit ? "Credit Due:" : "Amount Due:");
            template = template.Replace(InvoiceFields.DetailDateHeader, invoice.InvoiceType == InvoiceType.Credit ? "Invoice" : "Due");
            template = template.Replace(InvoiceFields.InvoiceDetailsHeader, invoice.InvoiceType == InvoiceType.Credit ? "Credit(s)" : "Charge(s)");
            template = template.Replace(InvoiceFields.LatePaymentMessage, invoice.InvoiceType == InvoiceType.Credit
                                                                              ? string.Empty
                                                                              : "Payment must be received by due date or a 1.5% per month penalty will apply with a minimum of US $20");

            if (template.Contains(InvoiceFields.PaymentMessageWrapperStart))
            {
                if (invoice.InvoiceType == InvoiceType.Credit)
                {
                    var content = template.RetrieveWrapperSection(InvoiceFields.PaymentMessageWrapperStart,
                                                              InvoiceFields.PaymentMessageWrapperEnd);
                    template = template.Replace(content, string.Empty);
                }
                template = template.Replace(InvoiceFields.PaymentMessageWrapperStart, string.Empty);
                template = template.Replace(InvoiceFields.PaymentMessageWrapperEnd, string.Empty);
            }

            if (template.Contains(InvoiceFields.OriginalInvoiceWrapperStart))
            {
                if (originalInvoice != null)
                {
                    var originalPrefixCode = originalInvoice.Prefix == null ? string.Empty : originalInvoice.Prefix.Code;
                    var originalPrefix = originalInvoice.HidePrefix ? string.Empty : originalPrefixCode;
                    template = template.Replace(InvoiceFields.OriginalInvoicePrefix, originalPrefix);
                    template = template.Replace(InvoiceFields.OriginalInvoiceNumber, originalInvoice.InvoiceNumber);

                }
                else
                {
                    var content = template.RetrieveWrapperSection(InvoiceFields.OriginalInvoiceWrapperStart, InvoiceFields.OriginalInvoiceWrapperEnd);
                    template = template.Replace(content, string.Empty);
                }

                template = template.Replace(InvoiceFields.OriginalInvoiceWrapperStart, string.Empty);
                template = template.Replace(InvoiceFields.OriginalInvoiceWrapperEnd, string.Empty);
            }

            // service ticket details
            template = template.Replace(InvoiceFields.ServiceTicketNumber, ticket.ServiceTicketNumber);
            template = template.Replace(InvoiceFields.ServiceTicketDate, ticket.TicketDate.FormattedShortDate());

            // vendors
            var pv = ticket.Vendors.FirstOrDefault(v => v.Primary);
            template = template.Replace(InvoiceFields.PrimaryVendorName, pv == null ? string.Empty : pv.Vendor.Name);
            if (template.Contains(InvoiceFields.VendorsWrapperStart))
            {
                var content = template.RetrieveWrapperSection(InvoiceFields.VendorsWrapperStart, InvoiceFields.VendorsWrapperEnd);
                var lines = ticket.Vendors.Any()
                                ? ticket.Vendors
                                    .Select(v => content.Replace(InvoiceFields.VendorName, v.Vendor.Name))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(InvoiceFields.VendorsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(InvoiceFields.VendorsWrapperEnd, string.Empty);
            }

            // bill to
            var billTo = invoice.CustomerLocation;
            template = template.Replace(InvoiceFields.BillToDescription, billTo.Customer.Name);
            template = template.Replace(InvoiceFields.BillToStreet1, billTo.Street1);
            template = template.Replace(InvoiceFields.BillToStreet2, billTo.Street2);
            template = template.Replace(InvoiceFields.BillToCity, billTo.City);
            template = template.Replace(InvoiceFields.BillToState, billTo.State);
            template = template.Replace(InvoiceFields.BillToPostalCode, billTo.PostalCode);
            template = template.Replace(InvoiceFields.BillToCountry, billTo.Country.Name);

            //customer
            template = template.Replace(InvoiceFields.CustomerTerms, invoice.InvoiceType == InvoiceType.Credit ? string.Empty : string.Format("Net {0}", invoice.CustomerLocation.Customer.InvoiceTerms.ToString()));
            template = template.Replace(InvoiceFields.CustomerNumber, invoice.CustomerLocation.Customer.CustomerNumber);
            template = template.Replace(InvoiceFields.CustomerName, invoice.CustomerLocation.Customer.Name);

            // primary contact
            var contact = invoice.CustomerLocation.Contacts.FirstOrDefault(c => c.Primary);
            if (contact != null) contact = invoice.CustomerLocation.Contacts.FirstOrDefault(c => c.Primary);
            template = template.Replace(InvoiceFields.PrimaryContactName, contact == null ? string.Empty : contact.Name);
            template = template.Replace(InvoiceFields.PrimaryContactPhone, contact == null ? string.Empty : contact.Phone);
            template = template.Replace(InvoiceFields.PrimaryContactMobile, contact == null ? string.Empty : contact.Mobile);
            template = template.Replace(InvoiceFields.PrimaryContactFax, contact == null ? string.Empty : contact.Fax);
            template = template.Replace(InvoiceFields.PrimaryContactEmail, contact == null ? string.Empty : contact.Email);

            if (template.Contains(InvoiceFields.ContactsWrapperStart))
            {
                var content = template.RetrieveWrapperSection(InvoiceFields.ContactsWrapperStart, InvoiceFields.ContactsWrapperEnd);
                var lines = invoice.CustomerLocation.Contacts.Any()
                                ? invoice.CustomerLocation.Contacts
                                    .Select(c => content
                                                    .Replace(InvoiceFields.ContactName, c.Name)
                                                    .Replace(InvoiceFields.ContactPhone, c.Phone)
                                                    .Replace(InvoiceFields.ContactMobile, c.Mobile)
                                                    .Replace(InvoiceFields.ContactFax, c.Fax)
                                                    .Replace(InvoiceFields.ContactEmail, c.Email))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(InvoiceFields.ContactsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(InvoiceFields.ContactsWrapperEnd, string.Empty);
            }

            // invoice Details
            var hasDetails = invoice.Details.Any();
            template = template.Replace(InvoiceFields.TotalAmountDue,
                                        hasDetails ? invoice.Details.Sum(i => Math.Round(i.AmountDue, 2)).ToString(dp.CurrencyNumber) : string.Empty);
            if (template.Contains(InvoiceFields.InvoiceDetailWrapperStart))
            {

                var content = template.RetrieveWrapperSection(InvoiceFields.InvoiceDetailWrapperStart,
                                                              InvoiceFields.InvoiceDetailWrapperEnd);
                var lines = new List<string>();
                foreach (var d in invoice.Details)
                {
                    var sLine = content;

                    sLine = sLine.Replace(InvoiceFields.Quantity, d.Quantity.ToString());
                    sLine = sLine.Replace(InvoiceFields.UnitSell, d.UnitSell.ToString());
                    sLine = sLine.Replace(InvoiceFields.UnitDiscount, d.UnitDiscount.ToString());
                    sLine = sLine.Replace(InvoiceFields.AmountDue, d.AmountDue.ToString(dp.CurrencyNumber));
                    sLine = sLine.Replace(InvoiceFields.ReferenceNumber, d.ReferenceNumber);
                    sLine = sLine.Replace(InvoiceFields.ReferenceType, d.ReferenceType.FormattedString());
                    sLine = sLine.Replace(InvoiceFields.Comment, d.Comment);
                    sLine = sLine.Replace(InvoiceFields.ChargeCode, d.ChargeCode.Code);
                    sLine = sLine.Replace(InvoiceFields.ChargeCodeDescription, d.ChargeCode.Description);
                    sLine = sLine.Replace(InvoiceFields.AccountBucket, d.AccountBucket.Code);
                    sLine = sLine.Replace(InvoiceFields.AccountBucketDescription, d.AccountBucket.Description);

                    lines.Add(sLine);
                }

                template = template.Replace(InvoiceFields.InvoiceDetailWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines.ToArray()));
                template = template.Replace(InvoiceFields.InvoiceDetailWrapperEnd, string.Empty);
            }

            // service ticket items
            if (template.Contains(InvoiceFields.ServiceTicketItemsWrapperStart))
            {
                var content = template.RetrieveWrapperSection(InvoiceFields.ServiceTicketItemsWrapperStart,
                                                              InvoiceFields.ServiceTicketItemsWrapperEnd);
                var lines = ticket.Items.Any()
                                ? ticket.Items
                                    .Select(i => content
                                                    .Replace(InvoiceFields.ServiceTicketDescription, i.Description)
                                                    .Replace(InvoiceFields.ServiceTicketComment, i.Comment))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(InvoiceFields.ServiceTicketItemsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(InvoiceFields.ServiceTicketItemsWrapperEnd, string.Empty);
            }

            // service ticket notes
            if (template.Contains(InvoiceFields.NotesWrapperStart))
            {
                var content = template.RetrieveWrapperSection(InvoiceFields.NotesWrapperStart, InvoiceFields.NotesWrapperEnd);
                var lines = ticket.Notes.Any()
                                ? ticket.Notes
                                    .Where(n => !n.Classified)
                                    .Select(note => content.Replace(InvoiceFields.Note, note.Message))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(InvoiceFields.NotesWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(InvoiceFields.NotesWrapperEnd, string.Empty);
            }

            return template;
        }

        private static string GenerateMiscellaneousInvoice(this MemberPageBase page, Invoice invoice, bool removeBodyWrapper)
        {
            // get template
            Decimals dp;
            var template = page.Server
                .FetchTemplate(invoice.TenantId, invoice.PrefixId, ServiceMode.NotApplicable,
                               DocumentTemplateCategory.MiscellaneousInvoice, out dp);

            // logo url
            var logoUrl = invoice.CustomerLocation.Customer.RetrievePageLogoUrl();
            var siteRoot = page.Request.ResolveSiteRootWithHttp();
            logoUrl = logoUrl.Replace("~", siteRoot);

            // body wrapper
            if (removeBodyWrapper)
            {
                template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
                template = template.Replace(BaseFields.BodyWrapperEnd, WebApplicationSettings.Brand);
            }

            // general details
            var combine = siteRoot + WebApplicationSettings.UploadedImagesFolder(invoice.TenantId).Replace("~", string.Empty);
            template = template.Replace(InvoiceFields.UploadedImages, combine);

            template = template.Replace(InvoiceFields.InvoiceNumber, invoice.InvoiceNumber);
            var prefix = invoice.Prefix == null || invoice.HidePrefix ? string.Empty : invoice.Prefix.Code;
            template = template.Replace(InvoiceFields.Prefix, prefix);
            template = template.Replace(InvoiceFields.InvoiceDate, invoice.InvoiceDate.FormattedShortDate());
            template = template.Replace(InvoiceFields.PostDate, invoice.PostDate.FormattedShortDate());
            template = template.Replace(InvoiceFields.DueDate, invoice.DueDate.FormattedShortDate());
            template = template.Replace(InvoiceFields.SpecialInstructions, invoice.SpecialInstruction);
            template = template.Replace(InvoiceFields.InvoiceType, invoice.InvoiceType.ToString());
            template = template.Replace(InvoiceFields.PaidAmount, invoice.PaidAmount.ToString());
            template = template.Replace(InvoiceFields.LogoUrl, logoUrl);
            template = template.Replace(InvoiceFields.CustomerControlAccountNumber, invoice.CustomerControlAccountNumber);
            template = template.Replace(InvoiceFields.TotalLineLabel, invoice.InvoiceType == InvoiceType.Credit ? "Credit Due:" : "Amount Due:");

            //original Invoice
            if (template.Contains(InvoiceFields.OriginalInvoiceWrapperStart))
            {
                var oi = invoice.OriginalInvoiceId == default(long) ? null : invoice.OriginalInvoice;

                var originalInvoiceNumber = oi == null ? string.Empty : oi.InvoiceNumber;
                var originalPrefix = oi == null ? string.Empty : oi.Prefix == null || oi.HidePrefix ? string.Empty : oi.Prefix.Code;

                var content = template.RetrieveWrapperSection(InvoiceFields.OriginalInvoiceWrapperStart, InvoiceFields.OriginalInvoiceWrapperEnd);
                var rcontent = content.Replace(InvoiceFields.OriginalInvoiceNumber, originalInvoiceNumber);
                rcontent = rcontent.Replace(InvoiceFields.OriginalInvoicePrefix, originalPrefix);

                template = template.Replace(InvoiceFields.OriginalInvoiceWrapperStart, string.Empty);
                template = template.Replace(content, oi == null ? string.Empty : rcontent);
                template = template.Replace(InvoiceFields.OriginalInvoiceWrapperEnd, string.Empty);
            }

            // bill to
            var billTo = invoice.CustomerLocation;
            template = template.Replace(InvoiceFields.BillToDescription, billTo.Customer.Name);
            template = template.Replace(InvoiceFields.BillToStreet1, billTo.Street1);
            template = template.Replace(InvoiceFields.BillToStreet2, billTo.Street2);
            template = template.Replace(InvoiceFields.BillToCity, billTo.City);
            template = template.Replace(InvoiceFields.BillToState, billTo.State);
            template = template.Replace(InvoiceFields.BillToPostalCode, billTo.PostalCode);
            template = template.Replace(InvoiceFields.BillToCountry, billTo.Country.Name);

            //customer
            template = template.Replace(InvoiceFields.CustomerTerms, string.Format("Net {0}", invoice.CustomerLocation.Customer.InvoiceTerms));
            template = template.Replace(InvoiceFields.CustomerNumber, invoice.CustomerLocation.Customer.CustomerNumber);
            template = template.Replace(InvoiceFields.CustomerName, invoice.CustomerLocation.Customer.Name);

            // primary contact
            var contact = invoice.CustomerLocation.Contacts.FirstOrDefault(c => c.Primary);
            if (contact != null) contact = invoice.CustomerLocation.Contacts.FirstOrDefault(c => c.Primary);
            template = template.Replace(InvoiceFields.PrimaryContactName, contact == null ? string.Empty : contact.Name);
            template = template.Replace(InvoiceFields.PrimaryContactPhone, contact == null ? string.Empty : contact.Phone);
            template = template.Replace(InvoiceFields.PrimaryContactMobile, contact == null ? string.Empty : contact.Mobile);
            template = template.Replace(InvoiceFields.PrimaryContactFax, contact == null ? string.Empty : contact.Fax);
            template = template.Replace(InvoiceFields.PrimaryContactEmail, contact == null ? string.Empty : contact.Email);

            if (template.Contains(InvoiceFields.ContactsWrapperStart))
            {
                var content = template.RetrieveWrapperSection(InvoiceFields.ContactsWrapperStart, InvoiceFields.ContactsWrapperEnd);
                var lines = invoice.CustomerLocation.Contacts.Any()
                                ? invoice.CustomerLocation.Contacts
                                    .Select(c => content
                                                    .Replace(InvoiceFields.ContactName, c.Name)
                                                    .Replace(InvoiceFields.ContactPhone, c.Phone)
                                                    .Replace(InvoiceFields.ContactMobile, c.Mobile)
                                                    .Replace(InvoiceFields.ContactFax, c.Fax)
                                                    .Replace(InvoiceFields.ContactEmail, c.Email))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(InvoiceFields.ContactsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(InvoiceFields.ContactsWrapperEnd, string.Empty);
            }

            // invoice Details
            var hasDetails = invoice.Details.Any();
            template = template.Replace(InvoiceFields.TotalAmountDue,
                                        hasDetails ? invoice.Details.Sum(i => Math.Round(i.AmountDue, 2)).ToString(dp.CurrencyNumber) : string.Empty);
            if (template.Contains(InvoiceFields.InvoiceDetailWrapperStart))
            {

                var content = template.RetrieveWrapperSection(InvoiceFields.InvoiceDetailWrapperStart,
                                                              InvoiceFields.InvoiceDetailWrapperEnd);
                var lines = new List<string>();
                foreach (var d in invoice.Details)
                {
                    var sLine = content;

                    sLine = sLine.Replace(InvoiceFields.Quantity, d.Quantity.ToString());
                    sLine = sLine.Replace(InvoiceFields.UnitSell, d.UnitSell.ToString());
                    sLine = sLine.Replace(InvoiceFields.UnitDiscount, d.UnitDiscount.ToString());
                    sLine = sLine.Replace(InvoiceFields.AmountDue, d.AmountDue.ToString(dp.CurrencyNumber));
                    sLine = sLine.Replace(InvoiceFields.ReferenceNumber, d.ReferenceNumber);
                    sLine = sLine.Replace(InvoiceFields.ReferenceType, d.ReferenceType.FormattedString());
                    sLine = sLine.Replace(InvoiceFields.Comment, d.Comment);
                    sLine = sLine.Replace(InvoiceFields.ChargeCode, d.ChargeCode.Code);
                    sLine = sLine.Replace(InvoiceFields.ChargeCodeDescription, d.ChargeCode.Description);
                    sLine = sLine.Replace(InvoiceFields.AccountBucket, d.AccountBucket.Code);
                    sLine = sLine.Replace(InvoiceFields.AccountBucketDescription, d.AccountBucket.Description);
                    sLine = sLine.Replace(InvoiceFields.AmountDue, d.AmountDue.ToString(dp.CurrencyNumber));

                    lines.Add(sLine);
                }

                template = template.Replace(InvoiceFields.InvoiceDetailWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines.ToArray()));
                template = template.Replace(InvoiceFields.InvoiceDetailWrapperEnd, string.Empty);
            }

            //Original Invoice Summary
            if (template.Contains(InvoiceFields.OriginalInvoiceSummaryWrapperStart))
            {

                var originalInvoice = invoice.OriginalInvoice;
                var content = template.RetrieveWrapperSection(InvoiceFields.OriginalInvoiceSummaryWrapperStart,
                                                              InvoiceFields.OriginalInvoiceSummaryWrapperEnd);
                var lines = new List<string>();
                var shSearch = new ShipmentSearch();
                var stSearch = new ServiceTicketSearch();
                if (originalInvoice != null)
                {
                    var details = originalInvoice.Details
                        .GroupBy(d => string.Format("{0}-{1}", d.ReferenceType, d.ReferenceNumber))
                        .Select(i =>
                                    {
                                        var d = i.First();
                                        return new
                                        {
                                            d.ReferenceNumber,
                                            d.ReferenceType,
                                            d.TenantId,
                                            AmountDue = i.Select(x => x.AmountDue).Sum()
                                        };
                                    });
                    foreach (var d in details)
                    {
                        var sLine = content;

                        var ip = originalInvoice.Prefix;
                        sLine = sLine.Replace(InvoiceFields.OriginalInvoicePrefix, ip == null ? string.Empty : ip.Code);
                        sLine = sLine.Replace(InvoiceFields.OriginalInvoiceNumber, originalInvoice.InvoiceNumber);
                        sLine = sLine.Replace(InvoiceFields.OriginalInvoiceSummaryReferenceNumber, d.ReferenceNumber);
                        sLine = sLine.Replace(InvoiceFields.OriginalInvoiceSummaryReferenceType, d.ReferenceType.ToString());
                        sLine = sLine.Replace(InvoiceFields.OriginalInvoiceSummaryTotal, d.AmountDue.ToString(dp.CurrencyNumber));

                        var sp = string.Empty;
                        if (sLine.Contains(InvoiceFields.OriginalInvoiceReferencePrefix))
                            switch (d.ReferenceType)
                            {
                                case DetailReferenceType.Shipment:
                                    var sh = shSearch.FetchShipmentByShipmentNumber(d.ReferenceNumber, d.TenantId);
                                    sp = sh == null ? string.Empty : sh.Prefix == null ? string.Empty : sh.Prefix.Code;
                                    break;
                                case DetailReferenceType.ServiceTicket:
                                    var st = stSearch.FetchServiceTicketByServiceTicketNumber(d.ReferenceNumber, d.TenantId);
                                    sp = st == null ? string.Empty : st.Prefix == null ? string.Empty : st.Prefix.Code;
                                    break;
                                default:
                                    sp = string.Empty;
                                    break;
                            }

                        sLine = sLine.Replace(InvoiceFields.OriginalInvoiceReferencePrefix, sp);

                        lines.Add(sLine);
                    }
                }

                template = template.Replace(InvoiceFields.OriginalInvoiceSummaryWrapperStart, string.Empty);
                template = template.Replace(content, originalInvoice == null
                                                        ? string.Empty
                                                        : string.Join(string.Empty, lines.ToArray()));
                template = template.Replace(InvoiceFields.OriginalInvoiceSummaryWrapperEnd, string.Empty);
            }

            return template;
        }

        private static string GenerateShipmentInvoice(this MemberPageBase page, Invoice invoice, Invoice originalInvoice, bool removeBodyWrapper)
        {
            // shipmentDetailDateHeader
            //if the original invoice is null, use current invoice to get shipment number
            var details = originalInvoice == null ? invoice.Details : originalInvoice.Details;
            var shipment = new ShipmentSearch().FetchShipmentByShipmentNumber(details.First().ReferenceNumber, invoice.TenantId);

            // get template
            Decimals dp;
            var template = page.Server
                    .FetchTemplate(invoice.TenantId, invoice.PrefixId, shipment.ServiceMode,
                                   DocumentTemplateCategory.ShipmentInvoice, out dp);
            if (string.IsNullOrEmpty(template)) // if not specific service mode applicable templates, check general
                template = page.Server
                .FetchTemplate(invoice.TenantId, invoice.PrefixId, ServiceMode.NotApplicable,
                               DocumentTemplateCategory.ShipmentInvoice, out dp);


            // logo url
            var logoUrl = invoice.CustomerLocation.Customer.RetrievePageLogoUrl();
            var siteRoot = page.Request.ResolveSiteRootWithHttp();
            logoUrl = logoUrl.Replace("~", siteRoot);

            // body wrapper
            if (removeBodyWrapper)
            {
                template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
                template = template.Replace(BaseFields.BodyWrapperEnd, WebApplicationSettings.Brand);
            }

            // general details
            var combine = siteRoot + WebApplicationSettings.UploadedImagesFolder(invoice.TenantId).Replace("~", string.Empty);
            template = template.Replace(InvoiceFields.UploadedImages, combine);
            template = template.Replace(InvoiceFields.InvoiceNumber, invoice.InvoiceNumber);
            var prefix = invoice.Prefix == null || invoice.HidePrefix ? string.Empty : invoice.Prefix.Code;
            template = template.Replace(InvoiceFields.Prefix, prefix);
            template = template.Replace(InvoiceFields.InvoiceDate, invoice.InvoiceDate.FormattedShortDate());
            template = template.Replace(InvoiceFields.PostDate, invoice.PostDate.FormattedShortDate());
            template = template.Replace(InvoiceFields.DueDate, (invoice.InvoiceType == InvoiceType.Credit && originalInvoice != null) ? originalInvoice.InvoiceDate.FormattedShortDate() : invoice.DueDate.FormattedShortDate());
            template = template.Replace(InvoiceFields.SpecialInstructions, invoice.SpecialInstruction);
            template = template.Replace(InvoiceFields.InvoiceType, invoice.InvoiceType.ToString());
            template = template.Replace(InvoiceFields.PaidAmount, invoice.PaidAmount.ToString());
            template = template.Replace(InvoiceFields.LogoUrl, logoUrl);
            template = template.Replace(InvoiceFields.CustomerControlAccountNumber, invoice.CustomerControlAccountNumber);
            template = template.Replace(InvoiceFields.TotalLineLabel, invoice.InvoiceType == InvoiceType.Credit ? "Credit Due:" : "Amount Due:");
            template = template.Replace(InvoiceFields.DetailDateHeader, invoice.InvoiceType == InvoiceType.Credit ? "Invoice" : "Due");
            template = template.Replace(InvoiceFields.InvoiceDetailsHeader, invoice.InvoiceType == InvoiceType.Credit ? "Credit(s)" : "Charge(s)");
            template = template.Replace(InvoiceFields.LatePaymentMessage, invoice.InvoiceType == InvoiceType.Credit
                                                                              ? string.Empty
                                                                              : "Payment must be received by due date or a 1.5% per month penalty will apply with <br> a minimum of US $20");

            if (template.Contains(InvoiceFields.PaymentMessageWrapperStart))
            {
                if (invoice.InvoiceType == InvoiceType.Credit)
                {
                    var content = template.RetrieveWrapperSection(InvoiceFields.PaymentMessageWrapperStart,
                                                              InvoiceFields.PaymentMessageWrapperEnd);
                    template = template.Replace(content, string.Empty);
                }
                template = template.Replace(InvoiceFields.PaymentMessageWrapperStart, string.Empty);
                template = template.Replace(InvoiceFields.PaymentMessageWrapperEnd, string.Empty);
            }

            if (template.Contains(InvoiceFields.OriginalInvoiceWrapperStart))
            {
                if (originalInvoice != null)
                {
                    var originalPrefixCode = originalInvoice.Prefix == null ? string.Empty : originalInvoice.Prefix.Code;
                    var originalPrefix = originalInvoice.HidePrefix ? string.Empty : originalPrefixCode;
                    template = template.Replace(InvoiceFields.OriginalInvoicePrefix, originalPrefix);
                    template = template.Replace(InvoiceFields.OriginalInvoiceNumber, originalInvoice.InvoiceNumber);

                }
                else
                {
                    var content = template.RetrieveWrapperSection(InvoiceFields.OriginalInvoiceWrapperStart, InvoiceFields.OriginalInvoiceWrapperEnd);
                    template = template.Replace(content, string.Empty);
                }

                template = template.Replace(InvoiceFields.OriginalInvoiceWrapperStart, string.Empty);
                template = template.Replace(InvoiceFields.OriginalInvoiceWrapperEnd, string.Empty);
            }

            // bill to
            var billTo = invoice.CustomerLocation;
            template = template.Replace(InvoiceFields.BillToDescription, billTo.Customer.Name);
            template = template.Replace(InvoiceFields.BillToStreet1, billTo.Street1);
            template = template.Replace(InvoiceFields.BillToStreet2, billTo.Street2);
            template = template.Replace(InvoiceFields.BillToCity, billTo.City);
            template = template.Replace(InvoiceFields.BillToState, billTo.State);
            template = template.Replace(InvoiceFields.BillToPostalCode, billTo.PostalCode);
            template = template.Replace(InvoiceFields.BillToCountry, billTo.Country.Name);

            //customer
            template = template.Replace(InvoiceFields.CustomerTerms, invoice.InvoiceType == InvoiceType.Credit ? string.Empty : string.Format("Net {0}", invoice.CustomerLocation.Customer.InvoiceTerms.ToString()));
            template = template.Replace(InvoiceFields.CustomerNumber, invoice.CustomerLocation.Customer.CustomerNumber);
            template = template.Replace(InvoiceFields.CustomerName, invoice.CustomerLocation.Customer.Name);

            // primary contact
            var contact = invoice.CustomerLocation.Contacts.FirstOrDefault(c => c.Primary);
            if (contact != null) contact = invoice.CustomerLocation.Contacts.FirstOrDefault(c => c.Primary);
            template = template.Replace(InvoiceFields.PrimaryContactName, contact == null ? string.Empty : contact.Name);
            template = template.Replace(InvoiceFields.PrimaryContactPhone, contact == null ? string.Empty : contact.Phone);
            template = template.Replace(InvoiceFields.PrimaryContactMobile, contact == null ? string.Empty : contact.Mobile);
            template = template.Replace(InvoiceFields.PrimaryContactFax, contact == null ? string.Empty : contact.Fax);
            template = template.Replace(InvoiceFields.PrimaryContactEmail, contact == null ? string.Empty : contact.Email);

            if (template.Contains(InvoiceFields.ContactsWrapperStart))
            {
                var content = template.RetrieveWrapperSection(InvoiceFields.ContactsWrapperStart, InvoiceFields.ContactsWrapperEnd);
                var lines = invoice.CustomerLocation.Contacts.Any()
                                ? invoice.CustomerLocation.Contacts
                                    .Select(c => content
                                                    .Replace(InvoiceFields.ContactName, c.Name)
                                                    .Replace(InvoiceFields.ContactPhone, c.Phone)
                                                    .Replace(InvoiceFields.ContactMobile, c.Mobile)
                                                    .Replace(InvoiceFields.ContactFax, c.Fax)
                                                    .Replace(InvoiceFields.ContactEmail, c.Email))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(InvoiceFields.ContactsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(InvoiceFields.ContactsWrapperEnd, string.Empty);
            }

            // invoice Details
            var hasDetails = invoice.Details.Any();
            template = template.Replace(InvoiceFields.TotalAmountDue,
                                        hasDetails ? invoice.Details.Sum(i => Math.Round(i.AmountDue, 2)).ToString(dp.CurrencyNumber) : string.Empty);
            if (template.Contains(InvoiceFields.InvoiceDetailWrapperStart))
            {

                var content = template.RetrieveWrapperSection(InvoiceFields.InvoiceDetailWrapperStart,
                                                              InvoiceFields.InvoiceDetailWrapperEnd);
                var lines = new List<string>();
                foreach (var d in invoice.Details)
                {
                    var sLine = content;

                    sLine = sLine.Replace(InvoiceFields.Quantity, d.Quantity.ToString());
                    sLine = sLine.Replace(InvoiceFields.UnitSell, d.UnitSell.ToString(dp.CurrencyNumber));
                    sLine = sLine.Replace(InvoiceFields.UnitDiscount, d.UnitDiscount.ToString(dp.CurrencyNumber));
                    sLine = sLine.Replace(InvoiceFields.AmountDue, d.AmountDue.ToString(dp.CurrencyNumber));
                    sLine = sLine.Replace(InvoiceFields.ReferenceNumber, d.ReferenceNumber);
                    sLine = sLine.Replace(InvoiceFields.ReferenceType, d.ReferenceType.FormattedString());
                    sLine = sLine.Replace(InvoiceFields.Comment, d.Comment);
                    sLine = sLine.Replace(InvoiceFields.ChargeCode, d.ChargeCode.Code);
                    sLine = sLine.Replace(InvoiceFields.ChargeCodeDescription, d.ChargeCode.Description);
                    sLine = sLine.Replace(InvoiceFields.AccountBucket, d.AccountBucket.Code);
                    sLine = sLine.Replace(InvoiceFields.AccountBucketDescription, d.AccountBucket.Description);

                    lines.Add(sLine);
                }

                template = template.Replace(InvoiceFields.InvoiceDetailWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines.ToArray()));
                template = template.Replace(InvoiceFields.InvoiceDetailWrapperEnd, string.Empty);
            }


            // shipment details
            template = template.Replace(InvoiceFields.ShipmentPrefix, shipment.HidePrefix || shipment.Prefix == null ? string.Empty : shipment.Prefix.Code);
            template = template.Replace(InvoiceFields.ShipmentNumber, shipment.ShipmentNumber);
            template = template.Replace(InvoiceFields.ShipperBOL, shipment.ShipperBol);
            template = template.Replace(InvoiceFields.ShipmentDate, shipment.ActualPickupDate == DateUtility.SystemEarliestDateTime
                                                                     ? string.Empty
                                                                     : shipment.ActualPickupDate.FormattedShortDate());
            template = template.Replace(InvoiceFields.ActualDeliveryDate, shipment.ActualDeliveryDate == DateUtility.SystemEarliestDateTime
                                                                     ? string.Empty
                                                                     : shipment.ActualDeliveryDate.FormattedShortDate());

            template = template.Replace(InvoiceFields.ServiceMode, shipment.ServiceMode.FormattedString());
            template = template.Replace(InvoiceFields.PurchaseOrderNumber, shipment.PurchaseOrderNumber);
            template = template.Replace(InvoiceFields.ShipperReferenceNumber, shipment.ShipperReference);
            template = template.Replace(InvoiceFields.MiscellaneousField1, shipment.MiscField1);
            template = template.Replace(InvoiceFields.MiscellaneousField2, shipment.MiscField2);

            // vendors
            if (template.Contains(InvoiceFields.PrimaryVendorName) || template.Contains(InvoiceFields.PrimaryVendorPro))
            {
                var sv = shipment.Vendors.FirstOrDefault(v => v.Primary);
                template = template.Replace(InvoiceFields.PrimaryVendorName, sv == null ? string.Empty : sv.Vendor.Name);
                template = template.Replace(InvoiceFields.PrimaryVendorPro, sv == null ? string.Empty : sv.ProNumber);
            }
            if (template.Contains(InvoiceFields.VendorsWrapperStart))
            {
                var content = template.RetrieveWrapperSection(InvoiceFields.VendorsWrapperStart, InvoiceFields.VendorsWrapperEnd);
                var lines = shipment.Vendors.Any()
                                ? shipment.Vendors
                                    .Select(v => content
                                                    .Replace(InvoiceFields.VendorName, v.Vendor.Name)
                                                    .Replace(InvoiceFields.VendorPro, v.ProNumber))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(InvoiceFields.VendorsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(InvoiceFields.VendorsWrapperEnd, string.Empty);
            }

            //origin
            template = template.Replace(InvoiceFields.OriginDescription, shipment.Origin.Description);
            template = template.Replace(InvoiceFields.OriginStreet1, shipment.Origin.Street1);
            template = template.Replace(InvoiceFields.OriginStreet2, shipment.Origin.Street2);
            template = template.Replace(InvoiceFields.OriginCity, shipment.Origin.City);
            template = template.Replace(InvoiceFields.OriginState, shipment.Origin.State);
            template = template.Replace(InvoiceFields.OriginPostalCode, shipment.Origin.PostalCode);
            template = template.Replace(InvoiceFields.OriginCountry, shipment.Origin.Country.Name);
            template = template.Replace(InvoiceFields.OriginInstructions, shipment.Origin.SpecialInstructions);

            if (template.Contains(InvoiceFields.OriginPrimaryContactEnabled))
            {
                var sc = shipment.Origin.Contacts.FirstOrDefault(c => c.Primary) ?? new ShipmentContact();
                template = template.Replace(InvoiceFields.OriginPrimaryContactEnabled, string.Empty);
                template = template.Replace(InvoiceFields.OriginPrimaryContact, sc.Name);
                template = template.Replace(InvoiceFields.OriginPrimaryPhone, sc.Phone);
                template = template.Replace(InvoiceFields.OriginPrimaryFax, sc.Fax);
                template = template.Replace(InvoiceFields.OriginPrimaryMobile, sc.Mobile);
                template = template.Replace(InvoiceFields.OriginPrimaryEmail, sc.Email);
            }
            if (template.Contains(InvoiceFields.OriginContactsWrapperStart))
            {
                var content = template.RetrieveWrapperSection(InvoiceFields.OriginContactsWrapperStart, InvoiceFields.OriginContactsWrapperEnd);
                var lines = shipment.Origin.Contacts.Any()
                                ? shipment.Origin.Contacts
                                    .Select(c => content
                                                    .Replace(InvoiceFields.OriginContact, c.Name)
                                                    .Replace(InvoiceFields.OriginPhone, c.Phone)
                                                    .Replace(InvoiceFields.OriginFax, c.Fax)
                                                    .Replace(InvoiceFields.OriginMobile, c.Mobile)
                                                    .Replace(InvoiceFields.OriginEmail, c.Email))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(InvoiceFields.OriginContactsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(InvoiceFields.OriginContactsWrapperEnd, string.Empty);
            }
            if (template.Contains(InvoiceFields.OriginReferencesWrapperStart))
            {
                var content = template.RetrieveWrapperSection(InvoiceFields.OriginReferencesWrapperStart, InvoiceFields.OriginReferencesWrapperEnd);
                var lines = shipment.CustomerReferences.Any(r => r.DisplayOnOrigin)
                                ? shipment.CustomerReferences
                                    .Where(r => r.DisplayOnOrigin)
                                    .Select(r => content
                                                    .Replace(InvoiceFields.OriginReferenceName, r.Name)
                                                    .Replace(InvoiceFields.OriginReferenceValue, r.Value))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(InvoiceFields.OriginReferencesWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(InvoiceFields.OriginReferencesWrapperEnd, string.Empty);
            }

            // destination
            template = template.Replace(InvoiceFields.DestinationDescription, shipment.Destination.Description);
            template = template.Replace(InvoiceFields.DestinationStreet1, shipment.Destination.Street1);
            template = template.Replace(InvoiceFields.DestinationStreet2, shipment.Destination.Street2);
            template = template.Replace(InvoiceFields.DestinationCity, shipment.Destination.City);
            template = template.Replace(InvoiceFields.DestinationState, shipment.Destination.State);
            template = template.Replace(InvoiceFields.DestinationPostalCode, shipment.Destination.PostalCode);
            template = template.Replace(InvoiceFields.DestinationCountry, shipment.Destination.Country.Name);
            template = template.Replace(InvoiceFields.DestinationInstructions, shipment.Destination.SpecialInstructions);

            if (template.Contains(InvoiceFields.DestinationPrimaryContactEnabled))
            {
                var sc = shipment.Destination.Contacts.FirstOrDefault(c => c.Primary) ?? new ShipmentContact();
                template = template.Replace(InvoiceFields.DestinationPrimaryContactEnabled, string.Empty);
                template = template.Replace(InvoiceFields.DestinationPrimaryContact, sc.Name);
                template = template.Replace(InvoiceFields.DestinationPrimaryPhone, sc.Phone);
                template = template.Replace(InvoiceFields.DestinationPrimaryFax, sc.Fax);
                template = template.Replace(InvoiceFields.DestinationPrimaryMobile, sc.Mobile);
                template = template.Replace(InvoiceFields.DestinationPrimaryEmail, sc.Email);
            }
            if (template.Contains(InvoiceFields.DestinationContactsWrapperStart))
            {
                var content = template.RetrieveWrapperSection(InvoiceFields.DestinationContactsWrapperStart, InvoiceFields.DestinationContactsWrapperEnd);
                var lines = shipment.Destination.Contacts.Any()
                                ? shipment.Destination.Contacts
                                    .Select(c => content
                                                    .Replace(InvoiceFields.DestinationContact, c.Name)
                                                    .Replace(InvoiceFields.DestinationPhone, c.Phone)
                                                    .Replace(InvoiceFields.DestinationFax, c.Fax)
                                                    .Replace(InvoiceFields.DestinationMobile, c.Mobile)
                                                    .Replace(InvoiceFields.DestinationEmail, c.Email))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(InvoiceFields.DestinationContactsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(InvoiceFields.DestinationContactsWrapperEnd, string.Empty);
            }
            if (template.Contains(InvoiceFields.DestinationReferencesWrapperStart))
            {
                var content = template.RetrieveWrapperSection(InvoiceFields.DestinationReferencesWrapperStart, InvoiceFields.DestinationReferencesWrapperEnd);
                var lines = shipment.CustomerReferences.Any(r => r.DisplayOnDestination)
                                ? shipment.CustomerReferences
                                    .Where(r => r.DisplayOnDestination)
                                    .Select(r => content
                                                    .Replace(InvoiceFields.DestinationReferenceName, r.Name)
                                                    .Replace(InvoiceFields.DestinationReferenceValue, r.Value))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(InvoiceFields.DestinationReferencesWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(InvoiceFields.DestinationReferencesWrapperEnd, string.Empty);
            }

            // items
            var hasItems = shipment.Items.Any();
            var itemsTotalValue = shipment.Items.Sum(i => i.Value);
            template = template.Replace(InvoiceFields.ItemsTotalValue, hasItems ? itemsTotalValue == 0 ? "$" : itemsTotalValue.ToString(dp.Currency) : string.Empty);
            template = template.Replace(InvoiceFields.ItemsTotalWeight, hasItems ? shipment.Items.Sum(i => i.ActualWeight).ToString(dp.Weight) : string.Empty);
            if (template.Contains(InvoiceFields.ItemsWrapperStart))
            {
                var content = template.RetrieveWrapperSection(InvoiceFields.ItemsWrapperStart, InvoiceFields.ItemsWrapperEnd);
                var lines = hasItems
                                ? shipment.Items
                                    .Select(i => content
                                                    .Replace(InvoiceFields.ItemQuantity, i.Quantity.ToString())
                                                    .Replace(InvoiceFields.ItemPieceCount, i.PieceCount == 0 ? string.Empty : i.PieceCount.ToString())
                                                    .Replace(InvoiceFields.ItemPackageType, i.PackageType.TypeName)
                                                    .Replace(InvoiceFields.ItemDescription, i.Description)
                                                    .Replace(InvoiceFields.ItemTotalWeight, i.ActualWeight.ToString(dp.Weight))
                                                    .Replace(InvoiceFields.ItemLength, i.ActualLength.ToString(dp.Dimension))
                                                    .Replace(InvoiceFields.ItemWidth, i.ActualWidth.ToString(dp.Dimension))
                                                    .Replace(InvoiceFields.ItemHeight, i.ActualHeight.ToString(dp.Dimension))
                                                    .Replace(InvoiceFields.ItemFreightClass, i.ActualFreightClass.ToString())
                                                    .Replace(InvoiceFields.ItemStackable, i.IsStackable ? "[STACKABLE]" : string.Empty)
                                                    .Replace(InvoiceFields.ItemValue, i.Value == 0 ? "$" : i.Value.ToString(dp.Currency))
                                                    .Replace(InvoiceFields.ItemNMFC, i.NMFCCode)
                                                    .Replace(InvoiceFields.ItemHTS, i.HTSCode)
                                                    .Replace(InvoiceFields.ItemComments, i.Comment))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(InvoiceFields.ItemsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(InvoiceFields.ItemsWrapperEnd, string.Empty);
            }

            //General notes
            if (template.Contains(InvoiceFields.NotesWrapperStart))
            {
                var content = template.RetrieveWrapperSection(InvoiceFields.NotesWrapperStart, InvoiceFields.NotesWrapperEnd);
                var lines = shipment.Notes.Any()
                                ? shipment.Notes.Where(n => n.Type == NoteType.Normal && !n.Classified)
                                    .Select(note => content.Replace(InvoiceFields.Note, note.Message))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(InvoiceFields.NotesWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(InvoiceFields.NotesWrapperEnd, string.Empty);
            }

            return template;
        }


        public static string GeneratePostedInvoiceNotification(this MemberPageBase page, Invoice invoice)
        {
            //get template
            var template = page.Server.ReadFromFile(SystemTemplates.GenerateInvoicePostedTemplate).FromUtf8Bytes();

            // logo url
            var customer = invoice.CustomerLocation.Customer ?? page.ActiveUser.DefaultCustomer;
            var logoUrl = customer.RetrievePageLogoUrl();
            var siteRoot = page.Request.ResolveSiteRootWithHttp();
            logoUrl = logoUrl.Replace("~", siteRoot);

            // body wrapper
            template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
            template = template.Replace(BaseFields.BodyWrapperEnd, string.Empty);

            // details
            template = template.Replace(EmailTemplateFields.SiteRoot, siteRoot);
            template = template.Replace(EmailTemplateFields.LogoUrl, logoUrl);
            template = template.Replace(EmailTemplateFields.AccountName, customer.Name);
            template = template.Replace(EmailTemplateFields.Brand, WebApplicationSettings.Brand);
            template = template.Replace(EmailTemplateFields.InvoiceNumber, invoice.InvoiceNumber);
            template = template.Replace(EmailTemplateFields.InvoiceDocumentDownloadLink,
                                        string.Format("<a href='{0}{1}?{2}={3}'>Link</a>", WebApplicationSettings.SiteRoot,
                                                      CustomerInvoiceDocumentView.PageAddress.Replace("~", string.Empty),
                                                      WebApplicationConstants.TransferNumber,
                                                      invoice.InvoiceNumber.UrlTextEncrypt()));
            template = template.Replace(EmailTemplateFields.PostDate, invoice.PostDate.FormattedShortDate());
            template = template.Replace(EmailTemplateFields.DueDate, invoice.DueDate.FormattedShortDate());

            var permission = page.ActiveUser.RetrievePermission(ViewCode.InvoiceDashboard);
            var content = template.RetrieveWrapperSection(EmailTemplateFields.ViewInvoiceDashboardWrapperStart,
                                                          EmailTemplateFields.ViewInvoiceDashboardWrapperEnd);
            template = template.Replace(EmailTemplateFields.ViewInvoiceDashboardWrapperStart, string.Empty);
            if (!permission.Grant || permission.Deny) template = template.Replace(content, string.Empty);
            template = template.Replace(EmailTemplateFields.ViewInvoiceDashboardWrapperEnd, string.Empty);

            return template;
        }


        public static string GenerateVendorChargeConfirmNotification(this MemberPageBase page, List<ChargeViewSearchDto> charges)
        {
            if (!charges.Any()) return null;

            // get template
            var template = page.Server.ReadFromFile(SystemTemplates.VendorChargeConfirmationTemplate).FromUtf8Bytes();

            // logo url
            var customer = page.ActiveUser.DefaultCustomer;
            var logoUrl = customer == null ? string.Empty : customer.RetrievePageLogoUrl();
            var siteRoot = page.Request.ResolveSiteRootWithHttp();
            logoUrl = logoUrl.Replace("~", siteRoot);

            // body wrapper
            template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
            template = template.Replace(BaseFields.BodyWrapperEnd, string.Empty);
            template = template.Replace(EmailTemplateFields.AccountName, page.ActiveUser.FullName);

            // details
            template = template.Replace(EmailTemplateFields.SiteRoot, siteRoot);
            template = template.Replace(EmailTemplateFields.LogoUrl, logoUrl);

            //vendor
            template = template.Replace(ChargeFields.Vendor, page.ActiveUser.VendorPortalVendor.Name);



            // items
            if (template.Contains(ChargeFields.GroupChargeWrapperStart))
            {
                var contentGroup = template.RetrieveWrapperSection(ChargeFields.GroupChargeWrapperStart, ChargeFields.GroupChargeWrapperEnd);

                var groups = charges
                    .GroupBy(i => new { i.Number, i.ChargeType })
                    .Select(x =>
                    new ChargeViewSearchDto
                    {
                        Number = x.First().Number,
                        ChargeType = x.First().ChargeType,
                        UnitBuy = (x.Sum(s => s.Quantity * s.UnitBuy))
                    }).ToList();

                var linesGroups = groups.Select(groupe =>
                {
                    var repitingBlock = contentGroup
                         .Replace(ChargeFields.DocumentNumber, groupe.Number)
                         .Replace(ChargeFields.DocumentType, groupe.ChargeType.ToString())
                         .Replace(ChargeFields.UnitBuyTotal, groupe.UnitBuy.ToString());

                    var repeatingChargeLine = repitingBlock.RetrieveWrapperSection(ChargeFields.ViewChargeWrapperStart, ChargeFields.ViewChargeWrapperEnd); // get block for Charges replacing
                    var groupCharges = charges.Where(c => (c.Number == groupe.Number) && (c.ChargeType == groupe.ChargeType)).ToList(); // get charges for current group

                    var repeatingChargeLines = groupCharges.Any()
                    ? groupCharges.Select(charge => repeatingChargeLine
                        .Replace(ChargeFields.DocumentNumber, charge.Number)
                        .Replace(ChargeFields.Comment, charge.Comment)
                        .Replace(ChargeFields.UnitBuyTotal, charge.FinalBuy.ToString())
                        .Replace(ChargeFields.ChargeCode, charge.ChargeCode.Code)
                        .Replace(ChargeFields.Qty, charge.Quantity.ToString())
                        .Replace(ChargeFields.UnitBuy, charge.UnitBuy.ToString()))
                    : new string[0];

                    repitingBlock = repitingBlock.Replace(ChargeFields.ViewChargeWrapperStart, string.Empty);
                    repitingBlock = repitingBlock.Replace(repeatingChargeLine, string.Join(string.Empty, repeatingChargeLines));
                    repitingBlock = repitingBlock.Replace(ChargeFields.ViewChargeWrapperEnd, string.Empty);

                    return repitingBlock;
                }).ToList();

                template = template.Replace(ChargeFields.GroupChargeWrapperStart, string.Empty);
                template = template.Replace(contentGroup, string.Join(string.Empty, linesGroups));
                template = template.Replace(ChargeFields.GroupChargeWrapperEnd, string.Empty);
            }

            return template;
        }

        public static string GenerateVendorDisputeResolvedNotification(this MemberPageBase page, List<ChargeViewSearchDto> charges, VendorDispute dispute)
        {
            if (!charges.Any()) return null;

            // get template
            var template = page.Server.ReadFromFile(SystemTemplates.VendorDisputeResolvedTemplate).FromUtf8Bytes();

            // logo url
            var customer = dispute.CreatedByUser.DefaultCustomer;
            var logoUrl = customer == null ? string.Empty : customer.RetrievePageLogoUrl();
            var siteRoot = page.Request.ResolveSiteRootWithHttp();
            logoUrl = logoUrl.Replace("~", siteRoot);

            // body wrapper
            template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
            template = template.Replace(BaseFields.BodyWrapperEnd, string.Empty);
            template = template.Replace(EmailTemplateFields.AccountName, page.ActiveUser.FullName);

            // details
            template = template.Replace(EmailTemplateFields.SiteRoot, siteRoot);
            template = template.Replace(EmailTemplateFields.LogoUrl, logoUrl);

            //vendor
            template = template.Replace(ChargeFields.Vendor, dispute.CreatedByUser.VendorPortalVendor.Name);
            template = template.Replace(ChargeFields.VendorDisputeReference, dispute.DisputerReferenceNumber);



            // items
            if (template.Contains(ChargeFields.GroupChargeWrapperStart))
            {
                var contentGroup = template.RetrieveWrapperSection(ChargeFields.GroupChargeWrapperStart, ChargeFields.GroupChargeWrapperEnd);

                var groups = charges
                    .GroupBy(i => new { i.Number, i.ChargeType })
                    .Select(x =>
                    new ChargeViewSearchDto
                    {
                        Number = x.First().Number,
                        ChargeType = x.First().ChargeType,
                        UnitBuy = (x.Sum(s => s.Quantity * s.UnitBuy))
                    }).ToList();

                var linesGroups = groups.Select(groupe =>
                {
                    var repitingBlock = contentGroup
                         .Replace(ChargeFields.DocumentNumber, groupe.Number)
                         .Replace(ChargeFields.DocumentType, groupe.ChargeType.ToString())
                         .Replace(ChargeFields.UnitBuyTotal, groupe.UnitBuy.ToString());

                    var repeatingChargeLine = repitingBlock.RetrieveWrapperSection(ChargeFields.ViewChargeWrapperStart, ChargeFields.ViewChargeWrapperEnd); // get block for Charges replacing
                    var groupCharges = charges.Where(c => (c.Number == groupe.Number) && (c.ChargeType == groupe.ChargeType)).ToList(); // get charges for current group

                    var repeatingChargeLines = groupCharges.Any()
                    ? groupCharges.Select(charge => repeatingChargeLine
                        .Replace(ChargeFields.DocumentNumber, charge.Number)
                        .Replace(ChargeFields.Comment, charge.Comment)
                        .Replace(ChargeFields.UnitBuyTotal, charge.FinalBuy.ToString())
                        .Replace(ChargeFields.ChargeCode, charge.ChargeCode.Code)
                        .Replace(ChargeFields.Qty, charge.Quantity.ToString())
                        .Replace(ChargeFields.UnitBuy, charge.UnitBuy.ToString()))
                    : new string[0];

                    repitingBlock = repitingBlock.Replace(ChargeFields.ViewChargeWrapperStart, string.Empty);
                    repitingBlock = repitingBlock.Replace(repeatingChargeLine, string.Join(string.Empty, repeatingChargeLines));
                    repitingBlock = repitingBlock.Replace(ChargeFields.ViewChargeWrapperEnd, string.Empty);

                    return repitingBlock;
                }).ToList();

                template = template.Replace(ChargeFields.GroupChargeWrapperStart, string.Empty);
                template = template.Replace(contentGroup, string.Join(string.Empty, linesGroups));
                template = template.Replace(ChargeFields.GroupChargeWrapperEnd, string.Empty);

                template = template.Replace(ChargeFields.ResolutionComment, dispute.ResolutionComments);
            }

            return template;
        }

        public static string GenerateVendorChargeDisputeNotification(this MemberPageBase page, List<ChargeViewSearchDto> charges)
        {
            if (!charges.Any()) return null;

            // get template
            var template = page.Server.ReadFromFile(SystemTemplates.VendorChargeDisputeTemplate).FromUtf8Bytes();

            // logo url
            var customer = page.ActiveUser.DefaultCustomer;
            var logoUrl = customer == null ? string.Empty : customer.RetrievePageLogoUrl();
            var siteRoot = page.Request.ResolveSiteRootWithHttp();
            logoUrl = logoUrl.Replace("~", siteRoot);

            // body wrapper
            template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
            template = template.Replace(BaseFields.BodyWrapperEnd, string.Empty);
            template = template.Replace(EmailTemplateFields.AccountName, page.ActiveUser.FullName);

            // details
            template = template.Replace(EmailTemplateFields.SiteRoot, siteRoot);
            template = template.Replace(EmailTemplateFields.LogoUrl, logoUrl);

            //vendor
            template = template.Replace(ChargeFields.Vendor, page.ActiveUser.VendorPortalVendor.Name);


            // items
            if (template.Contains(ChargeFields.GroupChargeWrapperStart))
            {
                var contentGroup = template.RetrieveWrapperSection(ChargeFields.GroupChargeWrapperStart, ChargeFields.GroupChargeWrapperEnd);

                var groups = charges
                    .GroupBy(i => new { i.Number, i.ChargeType })
                    .Select(x =>
                    new ChargeViewSearchDto
                    {
                        Number = x.First().Number,
                        ChargeType = x.First().ChargeType,
                        UnitBuy = (x.Sum(s => s.Quantity * s.UnitBuy))
                    }).ToList();

                var linesGroups = groups.Select(groupe =>
                {
                    var repitingBlock = contentGroup
                         .Replace(ChargeFields.DocumentNumber, groupe.Number)
                         .Replace(ChargeFields.DocumentType, groupe.ChargeType.ToString())
                         .Replace(ChargeFields.UnitBuyTotal, groupe.UnitBuy.ToString());

                    var repeatingChargeLine = repitingBlock.RetrieveWrapperSection(ChargeFields.ViewChargeWrapperStart, ChargeFields.ViewChargeWrapperEnd); // get block for Charges replacing
                    var groupCharges = charges.Where(c => (c.Number == groupe.Number) && (c.ChargeType == groupe.ChargeType)).ToList(); // get charges for current group

                    var repeatingChargeLines = groupCharges.Any()
                    ? groupCharges.Select(charge => repeatingChargeLine
                        .Replace(ChargeFields.DocumentNumber, charge.Number)
                        .Replace(ChargeFields.Comment, charge.Comment)
                        .Replace(ChargeFields.UnitBuyTotal, charge.FinalBuy.ToString())
                        .Replace(ChargeFields.ChargeCode, charge.ChargeCode.Code)
                        .Replace(ChargeFields.Qty, charge.Quantity.ToString())
                        .Replace(ChargeFields.UnitBuy, charge.UnitBuy.ToString()))
                    : new string[0];

                    repitingBlock = repitingBlock.Replace(ChargeFields.ViewChargeWrapperStart, string.Empty);
                    repitingBlock = repitingBlock.Replace(repeatingChargeLine, string.Join(string.Empty, repeatingChargeLines));
                    repitingBlock = repitingBlock.Replace(ChargeFields.ViewChargeWrapperEnd, string.Empty);

                    return repitingBlock;
                }).ToList();

                template = template.Replace(ChargeFields.GroupChargeWrapperStart, string.Empty);
                template = template.Replace(contentGroup, string.Join(string.Empty, linesGroups));
                template = template.Replace(ChargeFields.GroupChargeWrapperEnd, string.Empty);
            }

            return template;
        }


        public static string GenerateShipmentStatement(this MemberPageBase page, Shipment shipment, List<AuditInvoiceDto> invoices)
        {
            return page.GenerateShipmentStatement(shipment, invoices, false);
        }

        public static string GenerateShipmentStatement(this MemberPageBase page, Shipment shipment, List<AuditInvoiceDto> invoices, bool removeBodyWrapper)
        {

            // get template
            Decimals dp;
            var template = page.Server
                .FetchTemplate(shipment.TenantId, shipment.PrefixId, ServiceMode.NotApplicable,
                               DocumentTemplateCategory.ShipmentStatement, out dp);

            // logo url
            var logoUrl = shipment.Customer.RetrievePageLogoUrl();
            var siteRoot = page.Request.ResolveSiteRootWithHttp();
            logoUrl = logoUrl.Replace("~", siteRoot);

            // body wrapper
            if (removeBodyWrapper)
            {
                template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
                template = template.Replace(BaseFields.BodyWrapperEnd, WebApplicationSettings.Brand);
            }

            //general details
            var combine = siteRoot + WebApplicationSettings.UploadedImagesFolder(shipment.TenantId).Replace("~", string.Empty);
            template = template.Replace(InvoiceFields.UploadedImages, combine);
            template = template.Replace(InvoiceFields.LogoUrl, logoUrl);
            template = template.Replace(InvoiceFields.CustomerNumber, shipment.Customer.CustomerNumber);
            template = template.Replace(InvoiceFields.ShipmentDate, shipment.ActualPickupDate == DateUtility.SystemEarliestDateTime
                                                                     ? string.Empty
                                                                     : shipment.ActualPickupDate.FormattedShortDate());
            var prefix = shipment.Prefix == null || shipment.HidePrefix ? string.Empty : shipment.Prefix.Code;
            template = template.Replace(InvoiceFields.Prefix, prefix);
            template = template.Replace(InvoiceFields.ShipmentNumber, shipment.ShipmentNumber);
            template = template.Replace(InvoiceFields.ServiceMode, shipment.ServiceMode.ToString());
            template = template.Replace(InvoiceFields.PurchaseOrderNumber, shipment.PurchaseOrderNumber);
            template = template.Replace(InvoiceFields.ShipperReferenceNumber, shipment.ShipperReference);

            template = template.Replace(InvoiceFields.LatePaymentMessage, "Payment must be received by due date or a 1.5% per month penalty will apply with a minimum of US $20");
            if (template.Contains(InvoiceFields.PaymentMessageWrapperStart))
            {
                template = template.Replace(InvoiceFields.PaymentMessageWrapperStart, string.Empty);
                template = template.Replace(InvoiceFields.PaymentMessageWrapperEnd, string.Empty);
            }


            // vendors
            if (template.Contains(InvoiceFields.PrimaryVendorName) || template.Contains(InvoiceFields.PrimaryVendorPro))
            {
                var sv = shipment.Vendors.FirstOrDefault(v => v.Primary);
                template = template.Replace(InvoiceFields.PrimaryVendorName, sv == null ? string.Empty : sv.Vendor.Name);
                template = template.Replace(InvoiceFields.PrimaryVendorPro, sv == null ? string.Empty : sv.ProNumber);
            }

            // bill to
            var billTo = shipment.Customer.Locations.FirstOrDefault(l => l.BillToLocation && l.MainBillToLocation);
            if (billTo != null)
            {
                template = template.Replace(InvoiceFields.BillToDescription, billTo.Customer.Name);
                template = template.Replace(InvoiceFields.BillToStreet1, billTo.Street1);
                template = template.Replace(InvoiceFields.BillToStreet2, billTo.Street2);
                template = template.Replace(InvoiceFields.BillToCity, billTo.City);
                template = template.Replace(InvoiceFields.BillToState, billTo.State);
                template = template.Replace(InvoiceFields.BillToPostalCode, billTo.PostalCode);
                template = template.Replace(InvoiceFields.BillToCountry, billTo.Country.Name);
            }
            else
            {
                template = template.Replace(InvoiceFields.BillToDescription, WebApplicationConstants.NoBillToAddressOnFile);
                template = template.Replace(InvoiceFields.BillToStreet1, string.Empty);
                template = template.Replace(InvoiceFields.BillToStreet2, string.Empty);
                template = template.Replace(InvoiceFields.BillToCity, string.Empty);
                template = template.Replace(InvoiceFields.BillToState, string.Empty);
                template = template.Replace(InvoiceFields.BillToPostalCode, string.Empty);
                template = template.Replace(InvoiceFields.BillToCountry, string.Empty);
            }

            //origin
            template = template.Replace(InvoiceFields.OriginDescription, shipment.Origin.Description);
            template = template.Replace(InvoiceFields.OriginStreet1, shipment.Origin.Street1);
            template = template.Replace(InvoiceFields.OriginStreet2, shipment.Origin.Street2);
            template = template.Replace(InvoiceFields.OriginCity, shipment.Origin.City);
            template = template.Replace(InvoiceFields.OriginState, shipment.Origin.State);
            template = template.Replace(InvoiceFields.OriginPostalCode, shipment.Origin.PostalCode);
            template = template.Replace(InvoiceFields.OriginCountry, shipment.Origin.Country.Name);
            template = template.Replace(InvoiceFields.OriginInstructions, shipment.Origin.SpecialInstructions);


            // destination
            template = template.Replace(InvoiceFields.DestinationDescription, shipment.Destination.Description);
            template = template.Replace(InvoiceFields.DestinationStreet1, shipment.Destination.Street1);
            template = template.Replace(InvoiceFields.DestinationStreet2, shipment.Destination.Street2);
            template = template.Replace(InvoiceFields.DestinationCity, shipment.Destination.City);
            template = template.Replace(InvoiceFields.DestinationState, shipment.Destination.State);
            template = template.Replace(InvoiceFields.DestinationPostalCode, shipment.Destination.PostalCode);
            template = template.Replace(InvoiceFields.DestinationCountry, shipment.Destination.Country.Name);
            template = template.Replace(InvoiceFields.DestinationInstructions, shipment.Destination.SpecialInstructions);


            // items
            var hasItems = shipment.Items.Any();
            var itemsTotalValue = shipment.Items.Sum(i => i.Value);
            template = template.Replace(InvoiceFields.ItemsTotalValue, hasItems ? itemsTotalValue == 0 ? "$" : itemsTotalValue.ToString(dp.Currency) : string.Empty);
            template = template.Replace(InvoiceFields.ItemsTotalWeight, hasItems ? shipment.Items.Sum(i => i.ActualWeight).ToString(dp.Weight) : string.Empty);
            if (template.Contains(InvoiceFields.ItemsWrapperStart))
            {
                var content = template.RetrieveWrapperSection(InvoiceFields.ItemsWrapperStart, InvoiceFields.ItemsWrapperEnd);
                var lines = hasItems
                                ? shipment.Items
                                    .Select(i => content
                                                    .Replace(InvoiceFields.ItemQuantity, i.Quantity.ToString())
                                                    .Replace(InvoiceFields.ItemPieceCount, i.PieceCount == 0 ? string.Empty : i.PieceCount.ToString())
                                                    .Replace(InvoiceFields.ItemPackageType, i.PackageType.TypeName)
                                                    .Replace(InvoiceFields.ItemDescription, i.Description)
                                                    .Replace(InvoiceFields.ItemTotalWeight, i.ActualWeight.ToString(dp.Weight))
                                                    .Replace(InvoiceFields.ItemLength, i.ActualLength.ToString(dp.Dimension))
                                                    .Replace(InvoiceFields.ItemWidth, i.ActualWidth.ToString(dp.Dimension))
                                                    .Replace(InvoiceFields.ItemHeight, i.ActualHeight.ToString(dp.Dimension))
                                                    .Replace(InvoiceFields.ItemFreightClass, i.ActualFreightClass.ToString())
                                                    .Replace(InvoiceFields.ItemStackable, i.IsStackable ? "[STACKABLE]" : string.Empty)
                                                    .Replace(InvoiceFields.ItemValue, i.Value == 0 ? "$" : i.Value.ToString(dp.Currency))
                                                    .Replace(InvoiceFields.ItemNMFC, i.NMFCCode)
                                                    .Replace(InvoiceFields.ItemHTS, i.HTSCode)
                                                    .Replace(InvoiceFields.ItemComments, i.Comment))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(InvoiceFields.ItemsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(InvoiceFields.ItemsWrapperEnd, string.Empty);
            }

            // invoice Details
            var invoiceAmountsDue = invoices.Where(i => i.InvoiceType == InvoiceType.Invoice).Sum(k => Math.Round(k.AmountDue, 2));
            var supplementalAmountsDue = invoices.Where(i => i.InvoiceType == InvoiceType.Supplemental).Sum(k => k.AmountDue);
            var creditAmountsDue = invoices.Where(i => i.InvoiceType == InvoiceType.Credit).Sum(k => k.AmountDue);
            var netAmountDue = (invoiceAmountsDue + supplementalAmountsDue - creditAmountsDue);

            template = template.Replace(InvoiceFields.TotalAmountDue, Math.Abs(netAmountDue).ToString(dp.CurrencyNumber));

            template = template.Replace(InvoiceFields.TotalLineLabel, netAmountDue > default(decimal) ? "Amount Due:" : "Credit Due:");

            if (template.Contains(InvoiceFields.InvoiceDetailWrapperStart))
            {

                var content = template.RetrieveWrapperSection(InvoiceFields.InvoiceDetailWrapperStart,
                                                              InvoiceFields.InvoiceDetailWrapperEnd);

                var lines = new List<string>();
                foreach (var i in invoices.OrderByDescending(i => i.InvoiceNumber))
                {
                    var sLine = content;

                    sLine = sLine.Replace(InvoiceFields.InvoiceNumber, i.InvoiceNumber);
                    sLine = sLine.Replace(InvoiceFields.InvoiceType, i.InvoiceType.ToString());
                    sLine = sLine.Replace(InvoiceFields.InvoiceDate, i.InvoiceDate.FormattedShortDate());
                    sLine = sLine.Replace(InvoiceFields.DueDate, i.InvoiceType == InvoiceType.Credit ? string.Empty : i.DueDate.FormattedShortDate());
                    sLine = sLine.Replace(InvoiceFields.ChargeCodeDescription, i.ChargeCodeDescription);
                    sLine = sLine.Replace(InvoiceFields.AmountDue, i.InvoiceType == InvoiceType.Credit ? string.Format("({0})", i.AmountDue.ToString(dp.CurrencyNumber)) : i.AmountDue.ToString(dp.CurrencyNumber));

                    lines.Add(sLine);
                }

                template = template.Replace(InvoiceFields.InvoiceDetailWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines.ToArray()));
                template = template.Replace(InvoiceFields.InvoiceDetailWrapperEnd, string.Empty);
            }

            return template;
        }


        public static string GenerateNewPendingVendorNotification(this MemberPageBase page, PendingVendor pendingVendor)
        {
            // get template
            var template = page.Server.ReadFromFile(SystemTemplates.NewPendingVendorNotificationTemplate).FromUtf8Bytes();

            // logo url
            var logoUrl = string.IsNullOrEmpty(page.ActiveUser.DefaultCustomer.Tier.LogoUrl) ? page.ActiveUser.Tenant.LogoUrl : page.ActiveUser.DefaultCustomer.Tier.LogoUrl;
            var siteRoot = page.Request.ResolveSiteRootWithHttp();
            logoUrl = logoUrl.Replace("~", siteRoot);

            // body wrapper
            template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
            template = template.Replace(BaseFields.BodyWrapperEnd, string.Empty);

            // details
            template = template.Replace(EmailTemplateFields.SiteRoot, siteRoot);
            template = template.Replace(EmailTemplateFields.LogoUrl, logoUrl);
            template = template.Replace(EmailTemplateFields.AccountName, page.ActiveUser.DefaultCustomer.Name);
            template = template.Replace(EmailTemplateFields.Brand, WebApplicationSettings.Brand);
            template = template.Replace(EmailTemplateFields.User,
                                        string.Format("{0} {1}", page.ActiveUser.FirstName, page.ActiveUser.LastName));
            template = template.Replace(EmailTemplateFields.VendorName, pendingVendor.Name);
            template = template.Replace(EmailTemplateFields.VendorNumber, pendingVendor.VendorNumber);
            template = template.Replace(EmailTemplateFields.RequestDate, pendingVendor.DateCreated.GetString());

            return template;
        }
    }
}
