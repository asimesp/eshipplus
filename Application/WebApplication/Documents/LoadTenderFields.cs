﻿namespace LogisticsPlus.Eship.WebApplication.Documents
{
    public class LoadTenderFields
    {
		public const string LogoUrl = "[#LogoUrl#]";

        
        public const string PageAddress = "[#PageAddress#]";
        public const string DocumentType = "[#DocumentType#]";
        public const string DocumentTypeKey = "[#DocumentTypeKey#]";
        public const string ShipmentIdNumber = "[#ShipmentIdNumber#]";
        public const string ShipmentIdNumberKey = "[#ShipmentIdNumberKey#]";
		public const string VendorId = "[#VendorId#]";
		public const string VendorIdKey = "[#VendorIdKey#]";

		public const string Status = "[#Status#]";
		public const string StatusMessage = "[#StatusMessage#]";

        //Beginning Segment Shipment Information
        public const string ShipmentInformationAccountName = "[#ShipmentInformationAccountName#]";
        public const string ShipmentInformationDirection = "[#ShipmentInformationDirection#]";
        public const string ShipmentStandardPointLocationCode = "[#ShipmentStandardPointLocationCode#]";
        public const string ShipmentWeightUnitQualifier = "[#ShipmentWeightUnitQualifier#]";
        public const string ShipmentTotalEquipment = "[#ShipmentTotalEquipment#]";
        public const string ShipmentWeightCode = "[#ShipmentWeightCode#]";
        public const string ShipmentCustomsDocumentHandlingCode = "[#ShipmentCustomsDocumentHandlingCode#]";
        public const string ShipmentPaymentMethodCode = "[#ShipmentPaymentMethodCode#]";
        public const string ShipmentTariffServiceCode = "[#ShipmentTariffServiceCode#]";
        public const string ShipmentStandardCarrierAlphaCode = "[#ShipmentStandardCarrierAlphaCode#]";
        public const string ShipmentMethodOfPayment = "[#ShipmentMethodOfPayment#]";
        public const string ShipmentQualifier = "[#ShipmentQualifier#]";
        public const string ShipmentTransportationTermsCode = "[#ShipmentTransportationTermsCode#]";
        
        //Set Purpose
        public const string SetPurposeTransactionSetPurpose = "[#SetPurposeTransactionSetPurpose#]";
        public const string SetPurposeApplicationType = "[#SetPurposeApplicationType#]";

        //Expiration Date
        public const string ExpirationDate = "[#ExpirationDate#]";
		public const string BeginningSegmentDate = "[#BeginningSegmentDate#]";

        //List of Load Reference Numbers
        public const string LoadReferenceOuterWrapperStart = "[#LoadReferenceOuterWrapperStart#]";
        public const string LoadReferenceOuterWrapperEnd = "[#LoadReferenceOuterWrapperEnd#]";
        public const string LoadReferenceWrapperStart = "[#LoadReferenceWrapperStart#]";
        public const string LoadReferenceWrapperEnd = "[#LoadReferenceWrapperEnd#]";
        public const string LoadReferenceIdentificationQualifier = "[#LoadReferenceIdentificationQualifier#]";
        public const string LoadReferenceIdentification = "[#LoadReferenceIdentification#]";
        public const string LoadReferenceDescription = "[#LoadReferenceDescription#]";

        public const string HandlingRequirementOuterWrapperStart = "[#HandlingRequirementOuterWrapperStart#]";
        public const string HandlingRequirementOuterWrapperEnd = "[#HandlingRequirementOuterWrapperEnd#]";
        public const string HandlingRequirementWrapperStart = "[#HandlingRequirementWrapperStart#]";
        public const string HandlingRequirementWrapperEnd = "[#HandlingRequirementWrapperEnd#]";
        public const string HandlingRequirementSpecialHandlingCode = "[#HandlingRequirementSpecialHandlingCode#]";
        public const string HandlingRequirementSpecialServicesCode = "[#HandlingRequirementSpecialServicesCode#]";
        public const string HandlingRequirementSpecialHandlingDescription = "[#HandlingRequirementSpecialHandlingDescription#]";

        //Remarks
        public const string FreeFormMessage = "[#FreeFormMessage#]";
        public const string FreeFormMessage2 = "[#FreeFormMessage2#]";

        //Equipment Detail
        public const string EquipmentDetailEquipmentNumber = "[#EquipmentDetailEquipmentNumber#]";
        public const string EquipmentDetailEquipmentDescriptionCode = "[#EquipmentDetailEquipmentDescriptionCode#]";

        //Stops
        public const string StopWrapperStart = "[#StopWrapperStart#]";
        public const string StopWrapperEnd = "[#StopWrapperEnd#]";

        //Stop Details
        public const string StopSequenceNumber = "[#StopSequenceNumber#]";
        public const string StopReasonCode = "[#StopReasonCode#]";
        public const string StopSpecialInstructions = "[#StopSpecialInstructions#]";
        public const string StopDateTime = "[#StopDateTime#]";
        public const string StopLocationEntityCodeIdCode = "[#StopLocationEntityCodeIdCode#]";
        public const string StopLocationEntityName = "[#StopLocationEntityName#]";
        public const string StopLocationIdCodeQualifier = "[#StopLocationIdCodeQualifier#]";
        public const string StopLocationIdCode = "[#StopLocationIdCode#]";

        public const string StopLocationAddressInfo1 = "[#StopLocationAddressInfo1#]";
        public const string StopLocationAddressInfo2 = "[#StopLocationAddressInfo2#]";
        public const string StopCityName = "[#StopCityName#]";
        public const string StopStateOrProvinceCode = "[#StopStateOrProvinceCode#]";
        public const string StopPostalCode = "[#StopPostalCode#]";
        public const string StopCountryCode = "[#StopCountryCode#]";
        public const string StopLocationQualifier = "[#StopLocationQualifier#]";
        public const string StopLocationIdentifier = "[#StopLocationIdentifier#]";

        //Stop Location Contact
        public const string StopLocationContactFunctionCode = "[#StopLocationContactFunctionCode#]";
        public const string StopLocationContactName = "[#StopLocationContactName#]";
        public const string StopLocationContactCommNumberQualifier = "[#StopLocationContactCommNumberQualifier#]";
        public const string StopLocationContactCommNumber = "[#StopLocationContactCommNumber#]";
        public const string StopLocationContactInquiryReference = "[#StopLocationContactInquiryReference#]";

        // Stop List of Order Identification Detail
        public const string StopOrderIdDetailsOuterWrapperStart = "[#StopOrderIdDetailsOuterWrapperStart#]";
        public const string StopOrderIdDetailsOuterWrapperEnd = "[#StopOrderIdDetailsOuterWrapperEnd#]";
        public const string StopOrderIdDetailsWrapperStart = "[#StopOrderIdDetailsWrapperStart#]";
        public const string StopOrderIdDetailsWrapperEnd = "[#StopOrderIdDetailsWrapperEnd#]";

        public const string StopOrderIdDetailsRefId = "[#StopOrderIdDetailsRefId#]";
        public const string StopOrderIdDetailsQty = "[#StopOrderIdDetailsQty#]";
        public const string StopOrderIdDetailsWeight = "[#StopOrderIdDetailsWeight#]";
        public const string StopOrderIdDetailsLength = "[#StopOrderIdDetailsLength#]";
        public const string StopOrderIdDetailsWidth = "[#StopOrderIdDetailsWidth#]";
        public const string StopOrderIdDetailsHeight = "[#StopOrderIdDetailsHeight#]";
        public const string StopOrderIdDetailsNMFCCode = "[#StopOrderIdDetailsNMFCCode#]";
        public const string StopOrderIdDetailsItemDesc = "[#StopOrderIdDetailsItemDesc#]";
        public const string StopOrderIdDetailsCommodityCode = "[#StopOrderIdDetailsCommodityCode#]";
        public const string StopOrderIdDetailsPackageType = "[#StopOrderIdDetailsPackageType#]";
        public const string StopOrderIdDetailsNumOfUnitsShipped = "[#StopOrderIdDetailsNumOfUnitsShipped#]";
        public const string StopOrderIdDetailsFreightClass = "[#StopOrderIdDetailsFreightClass#]";


        public const string IsHazMat = "[#IsHazMat#]";

        //Stop Hazardous Material Contact
        public const string StopHazMatContactFunctionCode = "[#StopHazMatContactFunctionCode#]";
        public const string StopHazMatContactName = "[#StopHazMatContactName#]";
        public const string StopHazMatContactCommNumberQualifier = "[#StopHazMatContactCommNumberQualifier#]";
        public const string StopHazMatContactCommNumber = "[#StopHazMatContactCommNumber#]";
        public const string StopHazMatContactInquiryRef = "[#StopHazMatContactInquiryRef#]";


        //Shipment Weight Package and Quantity Data
        public const string ShipmentWghtPkgAndQtyDataWeightUnitCode = "[#ShipmentWghtPkgAndQtyDataWeightUnitCode#]";
        public const string ShipmentWghtPkgAndQtyDataWeight = "[#ShipmentWghtPkgAndQtyDataWeight#]";
        public const string ShipmentWghtPkgAndQtyDataLadingPieces = "[#ShipmentWghtPkgAndQtyDataLadingPieces#]";
        public const string ShipmentWghtPkgAndQtyDataLadingPackages = "[#ShipmentWghtPkgAndQtyDataLadingPackages#]";

        //Change List
        public const string ChangeListOuterWrapperStart = "[#ChangeListOuterWrapperStart#]";
        public const string ChangeListOuterWrapperEnd = "[#ChangeListOuterWrapperEnd#]";
        public const string ChangeListWrapperStart = "[#ChangeListWrapperStart#]";
        public const string ChangeListWrapperEnd = "[#ChangeListWrapperEnd#]";
        public const string ChangeListField = "[#ChangeListField#]";
        public const string ChangeListOldValue = "[#ChangeListOldValue#]";
        public const string ChangeListNewValue = "[#ChangeListNewValue#]";
        
    }
}