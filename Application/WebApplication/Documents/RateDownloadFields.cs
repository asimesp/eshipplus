﻿namespace LogisticsPlus.Eship.WebApplication.Documents
{
	public class RateDownloadFields : BaseFields
	{
		public const string LogoUrl = "[#LogoUrl#]";
		public const string AccountName = "[#AccountName#]";
        public const string AccountNumber = "[#AccountNumber#]";
		public const string CustomVendorSelectionMessage = "[#CustomVendorSelectionMessage#]";


		public const string EstimatedDeliveryDate = "[#EstimatedDeliveryDate#]";

		// warnings
		public const string WarningsWrapperStart = "[#WarningsWrapperStart#]";
		public const string WarningsWrapperEnd = "[#WarningsWrapperEnd#]";
		public const string WarningPackage = "[#WarningPackage#]";
		public const string WarningLTLFreightClass = "[#WarningLTLFreightClass#]";
		public const string WarningLTLDensity = "[#WarningLTLDensity#]";
		public const string WarningFTL = "[#WarningFTL#]";

		// origin
		public const string OriginPostalCode = "[#OriginPostalCode#]";
		public const string OriginCountry = "[#OriginCountry#]";

		// destination
		public const string DestinationPostalCode = "[#DestinationPostalCode#]";
		public const string DestinationCountry = "[#DestinationCountry#]";

		// items
		public const string ItemsTotalWeight = "[#ItemsTotalWeight#]";
		public const string ItemsTotalValue = "[#ItemsTotalValue#]";
		public const string ItemsWrapperStart = "[#ItemsWrapperStart#]";
		public const string ItemsWrapperEnd = "[#ItemsWrapperEnd#]";
		public const string ItemQuantity = "[#ItemQuantity#]";
		public const string ItemPieceCount = "[#ItemPieceCount#]";
		public const string ItemPackageType = "[#ItemPackageType#]";
		public const string ItemDescription = "[#ItemDescription#]";
		public const string ItemTotalWeight = "[#ItemTotalWeight#]";
		public const string ItemLength = "[#ItemLength#]";
		public const string ItemWidth = "[#ItemWidth#]";
		public const string ItemHeight = "[#ItemHeight#]";
		public const string ItemFreightClass = "[#ItemFreightClass#]";
		public const string ItemStackable = "[#ItemStackable#]";
		public const string ItemValue = "[#ItemValue#]";
		public const string ItemNMFC = "[#ItemNMFC#]";
		public const string ItemHTS = "[#ItemHTS#]";
		public const string ItemPickup = "[#ItemPickup#]";
		public const string ItemDelivery = "[#ItemDelivery#]";
		public const string ItemComments = "[#ItemComments#]";
        public const string ItemHazardous = "[#ItemHazardous#]";

		// accessorials
		public const string AccessorialsWrapperStart = "[#AccessorialsWrapperStart#]";
		public const string AccessorialsWrapperEnd = "[#AccessorialsWrapperEnd#]";
		public const string AccessorialsDescription = "[#AccessorialsDescription#]";
		public const string AccessorialsCode = "[#AccessorialsCode#]";

		// charge summary
		public const string ChargesWrapperStart = "[#ChargesWrapperStart#]";
		public const string ChargesWrapperEnd = "[#ChargesWrapperEnd#]";
		public const string ChargeCarrier = "[#ChargeCarrier#]";
		public const string ChargeMode = "[#ChargeMode#]";
		public const string ChargeTotalFreight = "[#ChargeTotalFreight#]";
		public const string ChargeTotalFuel = "[#ChargeTotalFuel#]";
		public const string ChargeTotalAccessorial = "[#ChargeTotalAccessorial#]";
		public const string ChargeTotalService = "[#ChargeTotalService#]";
		public const string ChargeTotalShipment = "[#ChargeTotalShipment#]";
        public const string ChargeTransit = "[#ChargeTransit#]";

        //logos
        public const string SmartwayLogo = "[#SmartwayLogo#]";
        public const string TiaMemberLogo = "[#TiaMemberLogo#]";
	}
}
