﻿namespace LogisticsPlus.Eship.WebApplication.Documents
{
    public class AvailableLoadsFields
    {
		public const string LoadNumber = "[#LoadNumber#]";
        public const string DesiredShipmentDate = "[#DesiredShipmentDate#]";
        public const string OriginCityState = "[#OriginCityState#]";
        public const string DestinationCityState = "[#DestinationCityState#]";
        public const string Dimensions = "[#Dimensions#]";
        public const string Weight = "[#Weight#]";
        public const string Type = "[#Type#]";
        public const string ContactPhone = "[#ContactPhone#]";
        public const string Comments = "[#Comments#]";
        public const string AvailableLoadsWrapperStart = "[#AvailableLoadsWrapperStart#]";
        public const string AvailableLoadsWrapperEnd = "[#AvailableLoadsWrapperEnd#]";
    }
}
