﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.WebApplication.Members;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;
using Newtonsoft.Json;

namespace LogisticsPlus.Eship.WebApplication.Documents
{
    public static partial class DocumentProcessor
    {
        public static string GenerateLoadOrderNotification(this MemberPageBase page, LoadOrder loadOrder)
        {
            // get template
            var template = page.Server.ReadFromFile(SystemTemplates.LoadOrderEmailTemplate).FromUtf8Bytes();

            // logo url
            var customer = loadOrder.Customer;
            var logoUrl = customer == null ? string.Empty : customer.RetrievePageLogoUrl();
            var siteRoot = page.Request.ResolveSiteRootWithHttp();
            logoUrl = logoUrl.Replace("~", siteRoot);

            // body wrapper
            template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
            template = template.Replace(BaseFields.BodyWrapperEnd, string.Empty);

            // details
            template = template.Replace(EmailTemplateFields.SiteRoot, siteRoot);
            template = template.Replace(EmailTemplateFields.LogoUrl, logoUrl);
            template = template.Replace(EmailTemplateFields.AccountName, loadOrder.Customer.Name);
            template = template.Replace(EmailTemplateFields.Brand, WebApplicationSettings.Brand);
            template = template.Replace(EmailTemplateFields.LoadNumber, loadOrder.LoadOrderNumber);
            template = template.Replace(EmailTemplateFields.LoadStatus, loadOrder.Status.GetString().FormattedString());
            template = template.Replace(ShipmentFields.OriginCity, loadOrder.Origin.City);
            template = template.Replace(ShipmentFields.OriginState, loadOrder.Origin.State);
            template = template.Replace(ShipmentFields.OriginPostalCode, loadOrder.Origin.PostalCode);
            template = template.Replace(ShipmentFields.DestinationCity, loadOrder.Destination.City);
            template = template.Replace(ShipmentFields.DestinationState, loadOrder.Destination.State);
            template = template.Replace(ShipmentFields.DestinationPostalCode, loadOrder.Destination.PostalCode);

            var permission = page.ActiveUser.RetrievePermission(ViewCode.CustomerLoadDashboard);
            var content = template.RetrieveWrapperSection(EmailTemplateFields.ViewLoadsDashboardWrapperStart,
                                                          EmailTemplateFields.ViewLoadsDashboardWrapperEnd);
            template = template.Replace(EmailTemplateFields.ViewLoadsDashboardWrapperStart, string.Empty);
            if (!permission.Grant || permission.Deny) template = template.Replace(content, string.Empty);
            template = template.Replace(EmailTemplateFields.ViewLoadsDashboardWrapperEnd, string.Empty);

            return template;
        }



        public static string GenerateCancelledShipmentReversedNotification(this MemberPageBase page, Shipment shipment)
        {
            // get template
            var template = page.Server.ReadFromFile(SystemTemplates.CancelledShipmentReversedEmailTemplate).FromUtf8Bytes();

            // logo url
            var customer = shipment.Customer;
            var logoUrl = customer == null ? string.Empty : customer.RetrievePageLogoUrl();
            var siteRoot = page.Request.ResolveSiteRootWithHttp();
            logoUrl = logoUrl.Replace("~", siteRoot);

            // body wrapper
            template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
            template = template.Replace(BaseFields.BodyWrapperEnd, string.Empty);

            // details
            template = template.Replace(EmailTemplateFields.SiteRoot, siteRoot);
            template = template.Replace(EmailTemplateFields.LogoUrl, logoUrl);
            template = template.Replace(EmailTemplateFields.AccountName, shipment.Customer.Name);
            template = template.Replace(EmailTemplateFields.Brand, WebApplicationSettings.Brand);
            template = template.Replace(EmailTemplateFields.ShipmentNumber, shipment.ShipmentNumber);
            template = template.Replace(ShipmentFields.OriginCity, shipment.Origin.City);
            template = template.Replace(ShipmentFields.OriginState, shipment.Origin.State);
            template = template.Replace(ShipmentFields.OriginPostalCode, shipment.Origin.PostalCode);
            template = template.Replace(ShipmentFields.DestinationCity, shipment.Destination.City);
            template = template.Replace(ShipmentFields.DestinationState, shipment.Destination.State);
            template = template.Replace(ShipmentFields.DestinationPostalCode, shipment.Destination.PostalCode);
            template = template.Replace(ShipmentFields.ShipperReference, shipment.ShipperReference);
            template = template.Replace(ShipmentFields.PurchaseOrder, shipment.PurchaseOrderNumber);

            var permission = page.ActiveUser.RetrievePermission(ViewCode.CustomerLoadDashboard);
            var content = template.RetrieveWrapperSection(EmailTemplateFields.ViewShipmentDashboardWrapperStart,
                                                          EmailTemplateFields.ViewShipmentDashboardWrapperEnd);
            template = template.Replace(EmailTemplateFields.ViewShipmentDashboardWrapperStart, string.Empty);
            if (!permission.Grant || permission.Deny) template = template.Replace(content, string.Empty);
            template = template.Replace(EmailTemplateFields.ViewShipmentDashboardWrapperEnd, string.Empty);

            content = template.RetrieveWrapperSection(EmailTemplateFields.AdditionalShipmentInsuranceWrapperStart,
                                                      EmailTemplateFields.AdditionalShipmentInsuranceWrapperEnd);
            template = template.Replace(EmailTemplateFields.AdditionalShipmentInsuranceWrapperStart, string.Empty);
            if (customer == null || customer.Rating == null || !customer.Rating.InsuranceEnabled ||
                (customer.Rating.InsuranceEnabled && shipment.DeclineInsurance))
                template = template.Replace(content, string.Empty);
            template = template.Replace(EmailTemplateFields.AdditionalShipmentInsuranceWrapperEnd, string.Empty);

            return template;
        }

        public static string GenerateCancelledShipmentNotification(this MemberPageBase page, Shipment shipment)
        {
            // get template
            var template = page.Server.ReadFromFile(SystemTemplates.CancelledShipmentEmailTemplate).FromUtf8Bytes();

            // logo url
            var customer = shipment.Customer;
            var logoUrl = customer == null ? string.Empty : customer.RetrievePageLogoUrl();
            var siteRoot = page.Request.ResolveSiteRootWithHttp();
            logoUrl = logoUrl.Replace("~", siteRoot);

            // body wrapper
            template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
            template = template.Replace(BaseFields.BodyWrapperEnd, string.Empty);

            // details
            template = template.Replace(EmailTemplateFields.SiteRoot, siteRoot);
            template = template.Replace(EmailTemplateFields.LogoUrl, logoUrl);
            template = template.Replace(EmailTemplateFields.AccountName, shipment.Customer.Name);
            template = template.Replace(EmailTemplateFields.Brand, WebApplicationSettings.Brand);
            template = template.Replace(EmailTemplateFields.ShipmentNumber, shipment.ShipmentNumber);
            template = template.Replace(ShipmentFields.OriginCity, shipment.Origin.City);
            template = template.Replace(ShipmentFields.OriginState, shipment.Origin.State);
            template = template.Replace(ShipmentFields.OriginPostalCode, shipment.Origin.PostalCode);
            template = template.Replace(ShipmentFields.DestinationCity, shipment.Destination.City);
            template = template.Replace(ShipmentFields.DestinationState, shipment.Destination.State);
            template = template.Replace(ShipmentFields.DestinationPostalCode, shipment.Destination.PostalCode);
            template = template.Replace(ShipmentFields.ShipperReference, shipment.ShipperReference);
            template = template.Replace(ShipmentFields.PurchaseOrder, shipment.PurchaseOrderNumber);

            var permission = page.ActiveUser.RetrievePermission(ViewCode.CustomerLoadDashboard);
            var content = template.RetrieveWrapperSection(EmailTemplateFields.ViewShipmentDashboardWrapperStart,
                                                          EmailTemplateFields.ViewShipmentDashboardWrapperEnd);
            template = template.Replace(EmailTemplateFields.ViewShipmentDashboardWrapperStart, string.Empty);
            if (!permission.Grant || permission.Deny) template = template.Replace(content, string.Empty);
            template = template.Replace(EmailTemplateFields.ViewShipmentDashboardWrapperEnd, string.Empty);

            content = template.RetrieveWrapperSection(EmailTemplateFields.AdditionalShipmentInsuranceWrapperStart,
                                                      EmailTemplateFields.AdditionalShipmentInsuranceWrapperEnd);
            template = template.Replace(EmailTemplateFields.AdditionalShipmentInsuranceWrapperStart, string.Empty);
            if (customer == null || customer.Rating == null || !customer.Rating.InsuranceEnabled ||
                (customer.Rating.InsuranceEnabled && shipment.DeclineInsurance))
                template = template.Replace(content, string.Empty);
            template = template.Replace(EmailTemplateFields.AdditionalShipmentInsuranceWrapperEnd, string.Empty);

            return template;
        }

        public static string GenerateNewShipmentNotification(this MemberPageBase page, Shipment shipment)
        {
            // get template
            var template = page.Server.ReadFromFile(SystemTemplates.NewShipmentEmailTemplate).FromUtf8Bytes();

            // logo url
            var customer = shipment.Customer;
            var logoUrl = customer == null ? string.Empty : customer.RetrievePageLogoUrl();
            var siteRoot = page.Request.ResolveSiteRootWithHttp();
            logoUrl = logoUrl.Replace("~", siteRoot);

            // body wrapper
            template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
            template = template.Replace(BaseFields.BodyWrapperEnd, string.Empty);

            // details
            template = template.Replace(EmailTemplateFields.SiteRoot, siteRoot);
            template = template.Replace(EmailTemplateFields.LogoUrl, logoUrl);
            template = template.Replace(EmailTemplateFields.AccountName, shipment.Customer.Name);
            template = template.Replace(EmailTemplateFields.Brand, WebApplicationSettings.Brand);
            template = template.Replace(EmailTemplateFields.ShipmentNumber, shipment.ShipmentNumber);
            template = template.Replace(ShipmentFields.OriginCity, shipment.Origin.City);
            template = template.Replace(ShipmentFields.OriginState, shipment.Origin.State);
            template = template.Replace(ShipmentFields.OriginPostalCode, shipment.Origin.PostalCode);
            template = template.Replace(ShipmentFields.DestinationCity, shipment.Destination.City);
            template = template.Replace(ShipmentFields.DestinationState, shipment.Destination.State);
            template = template.Replace(ShipmentFields.DestinationPostalCode, shipment.Destination.PostalCode);
            template = template.Replace(ShipmentFields.ShipperReference, shipment.ShipperReference);
            template = template.Replace(ShipmentFields.PurchaseOrder, shipment.PurchaseOrderNumber);

            var permission = page.ActiveUser.RetrievePermission(ViewCode.CustomerLoadDashboard);
            var content = template.RetrieveWrapperSection(EmailTemplateFields.ViewShipmentDashboardWrapperStart,
                                                          EmailTemplateFields.ViewShipmentDashboardWrapperEnd);
            template = template.Replace(EmailTemplateFields.ViewShipmentDashboardWrapperStart, string.Empty);
            if (!permission.Grant || permission.Deny) template = template.Replace(content, string.Empty);
            template = template.Replace(EmailTemplateFields.ViewShipmentDashboardWrapperEnd, string.Empty);

            content = template.RetrieveWrapperSection(EmailTemplateFields.AdditionalShipmentInsuranceWrapperStart,
                                                      EmailTemplateFields.AdditionalShipmentInsuranceWrapperEnd);
            template = template.Replace(EmailTemplateFields.AdditionalShipmentInsuranceWrapperStart, string.Empty);
            if (customer == null || customer.Rating == null || !customer.Rating.InsuranceEnabled ||
                (customer.Rating.InsuranceEnabled && shipment.DeclineInsurance))
                template = template.Replace(content, string.Empty);
            template = template.Replace(EmailTemplateFields.AdditionalShipmentInsuranceWrapperEnd, string.Empty);

            return template;
        }

        public static string GenerateShipmentDeliveredNotification(this MemberPageBase page, Shipment shipment)
        {
            // get template
            var template = page.Server.ReadFromFile(SystemTemplates.ShipmentDeliveredEmailTemplate).FromUtf8Bytes();

            // logo url
            var customer = shipment.Customer;
            var logoUrl = customer == null ? string.Empty : customer.RetrievePageLogoUrl();
            var siteRoot = page.Request.ResolveSiteRootWithHttp();
            logoUrl = logoUrl.Replace("~", siteRoot);

            // body wrapper
            template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
            template = template.Replace(BaseFields.BodyWrapperEnd, string.Empty);

            // details
            template = template.Replace(EmailTemplateFields.SiteRoot, siteRoot);
            template = template.Replace(EmailTemplateFields.LogoUrl, logoUrl);
            template = template.Replace(EmailTemplateFields.AccountName, shipment.Customer.Name);
            template = template.Replace(EmailTemplateFields.Brand, WebApplicationSettings.Brand);
            template = template.Replace(EmailTemplateFields.ShipmentNumber, shipment.ShipmentNumber);
            template = template.Replace(EmailTemplateFields.ShipmentDeliveryDate, shipment.ActualDeliveryDate.FormattedShortDate());
            template = template.Replace(ShipmentFields.OriginCity, shipment.Origin.City);
            template = template.Replace(ShipmentFields.OriginState, shipment.Origin.State);
            template = template.Replace(ShipmentFields.OriginPostalCode, shipment.Origin.PostalCode);
            template = template.Replace(ShipmentFields.DestinationCity, shipment.Destination.City);
            template = template.Replace(ShipmentFields.DestinationState, shipment.Destination.State);
            template = template.Replace(ShipmentFields.DestinationPostalCode, shipment.Destination.PostalCode);
            template = template.Replace(ShipmentFields.ShipperReference, shipment.ShipperReference);
            template = template.Replace(ShipmentFields.PurchaseOrder, shipment.PurchaseOrderNumber);

            var permission = page.ActiveUser.RetrievePermission(ViewCode.CustomerLoadDashboard);
            var content = template.RetrieveWrapperSection(EmailTemplateFields.ViewShipmentDashboardWrapperStart,
                                                          EmailTemplateFields.ViewShipmentDashboardWrapperEnd);
            template = template.Replace(EmailTemplateFields.ViewShipmentDashboardWrapperStart, string.Empty);
            if (!permission.Grant || permission.Deny) template = template.Replace(content, string.Empty);
            template = template.Replace(EmailTemplateFields.ViewShipmentDashboardWrapperEnd, string.Empty);

            content = template.RetrieveWrapperSection(EmailTemplateFields.AdditionalShipmentInsuranceWrapperStart,
                                                      EmailTemplateFields.AdditionalShipmentInsuranceWrapperEnd);
            template = template.Replace(EmailTemplateFields.AdditionalShipmentInsuranceWrapperStart, string.Empty);
            if (customer == null || customer.Rating == null || !customer.Rating.InsuranceEnabled ||
                (customer.Rating.InsuranceEnabled && shipment.DeclineInsurance))
                template = template.Replace(content, string.Empty);
            template = template.Replace(EmailTemplateFields.AdditionalShipmentInsuranceWrapperEnd, string.Empty);

            return template;
        }

        public static string GenerateShipmentInTransitNotification(this MemberPageBase page, Shipment shipment)
        {
            // get template
            var template = page.Server.ReadFromFile(SystemTemplates.ShipmentInTransitEmailTemplate).FromUtf8Bytes();

            // logo url
            var customer = shipment.Customer;
            var logoUrl = customer == null ? string.Empty : customer.RetrievePageLogoUrl();
            var siteRoot = page.Request.ResolveSiteRootWithHttp();
            logoUrl = logoUrl.Replace("~", siteRoot);

            // body wrapper
            template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
            template = template.Replace(BaseFields.BodyWrapperEnd, string.Empty);

            // details
            template = template.Replace(EmailTemplateFields.SiteRoot, siteRoot);
            template = template.Replace(EmailTemplateFields.LogoUrl, logoUrl);
            template = template.Replace(EmailTemplateFields.AccountName, shipment.Customer.Name);
            template = template.Replace(EmailTemplateFields.Brand, WebApplicationSettings.Brand);
            template = template.Replace(EmailTemplateFields.ShipmentNumber, shipment.ShipmentNumber);
            template = template.Replace(EmailTemplateFields.ShipmentPickupDate, shipment.ActualPickupDate.FormattedShortDate());
            template = template.Replace(EmailTemplateFields.ShipmentEstimatedDeliveryDate, shipment.EstimatedDeliveryDate.FormattedShortDate());
            template = template.Replace(ShipmentFields.OriginCity, shipment.Origin.City);
            template = template.Replace(ShipmentFields.OriginState, shipment.Origin.State);
            template = template.Replace(ShipmentFields.OriginPostalCode, shipment.Origin.PostalCode);
            template = template.Replace(ShipmentFields.DestinationCity, shipment.Destination.City);
            template = template.Replace(ShipmentFields.DestinationState, shipment.Destination.State);
            template = template.Replace(ShipmentFields.DestinationPostalCode, shipment.Destination.PostalCode);
            template = template.Replace(ShipmentFields.ShipperReference, shipment.ShipperReference);
            template = template.Replace(ShipmentFields.PurchaseOrder, shipment.PurchaseOrderNumber);

            var permission = page.ActiveUser.RetrievePermission(ViewCode.CustomerLoadDashboard);
            var content = template.RetrieveWrapperSection(EmailTemplateFields.ViewShipmentDashboardWrapperStart,
                                                          EmailTemplateFields.ViewShipmentDashboardWrapperEnd);
            template = template.Replace(EmailTemplateFields.ViewShipmentDashboardWrapperStart, string.Empty);
            if (!permission.Grant || permission.Deny) template = template.Replace(content, string.Empty);
            template = template.Replace(EmailTemplateFields.ViewShipmentDashboardWrapperEnd, string.Empty);

            content = template.RetrieveWrapperSection(EmailTemplateFields.AdditionalShipmentInsuranceWrapperStart,
                                                      EmailTemplateFields.AdditionalShipmentInsuranceWrapperEnd);
            template = template.Replace(EmailTemplateFields.AdditionalShipmentInsuranceWrapperStart, string.Empty);
            if (customer == null || customer.Rating == null || !customer.Rating.InsuranceEnabled ||
                (customer.Rating.InsuranceEnabled && shipment.DeclineInsurance))
                template = template.Replace(content, string.Empty);
            template = template.Replace(EmailTemplateFields.AdditionalShipmentInsuranceWrapperEnd, string.Empty);

            return template;
        }

        public static string GenerateCheckCallNotification(this MemberPageBase page, Shipment shipment)
        {
            //get template
            var template = page.Server.ReadFromFile(SystemTemplates.CheckCallNotificationTemplate).FromUtf8Bytes();

            // logo url
            var customer = shipment.Customer;
            var logoUrl = customer == null ? string.Empty : customer.RetrievePageLogoUrl();
            var siteRoot = page.Request.ResolveSiteRootWithHttp();
            logoUrl = logoUrl.Replace("~", siteRoot);

            var content = template.RetrieveWrapperSection(EmailTemplateFields.CheckCallWrapperStart, EmailTemplateFields.CheckCallWrapperEnd);
            var lines = new List<string>();

            //details
            template = template.Replace(EmailTemplateFields.SiteRoot, siteRoot);
            template = template.Replace(EmailTemplateFields.LogoUrl, logoUrl);
            template = template.Replace(EmailTemplateFields.Brand, WebApplicationSettings.Brand);
            template = template.Replace(EmailTemplateFields.ShipmentNumber, shipment.ShipmentNumber);
            template = template.Replace(EmailTemplateFields.AccountName, shipment.Customer.Name);
            template = template.Replace(EmailTemplateFields.Prefix, shipment.Prefix == null ? string.Empty : shipment.Prefix.Code);
            template = template.Replace(ShipmentFields.OriginCity, shipment.Origin.City);
            template = template.Replace(ShipmentFields.OriginState, shipment.Origin.State);
            template = template.Replace(ShipmentFields.OriginPostalCode, shipment.Origin.PostalCode);
            template = template.Replace(ShipmentFields.DestinationCity, shipment.Destination.City);
            template = template.Replace(ShipmentFields.DestinationState, shipment.Destination.State);
            template = template.Replace(ShipmentFields.DestinationPostalCode, shipment.Destination.PostalCode);
            template = template.Replace(ShipmentFields.ShipperReference, shipment.ShipperReference);
            template = template.Replace(ShipmentFields.PurchaseOrder, shipment.PurchaseOrderNumber);

            foreach (var checkCall in shipment.CheckCalls)
            {
                var sLine = content;

                sLine = sLine.Replace(EmailTemplateFields.CheckCallDate, checkCall.CallDate.FormattedSuppressMidnightDate());
                sLine = sLine.Replace(EmailTemplateFields.CheckCallEventDate,
                                      checkCall.EventDate == DateUtility.SystemEarliestDateTime
                                          ? string.Empty
                                          : checkCall.EventDate.FormattedSuppressMidnightDate());
                sLine = sLine.Replace(EmailTemplateFields.CheckCallNote, checkCall.CallNotes);

                lines.Add(sLine);
            }

            template = template.Replace(EmailTemplateFields.CheckCallWrapperStart, string.Empty);
            template = template.Replace(content, string.Join(string.Empty, lines.ToArray()));
            template = template.Replace(EmailTemplateFields.CheckCallWrapperEnd, string.Empty);

            // body wrapper
            template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
            template = template.Replace(BaseFields.BodyWrapperEnd, string.Empty);

            return template;
        }



        private static string GetApplicableVendorCerts(Vendor vendor)
        {
            var certs = string.Empty;

            if (vendor != null)
            {
                if (vendor.TSACertified) certs += " <span style='vertical-align: super; font-size: .8em;'>(1)</span>";
                if (vendor.SmartWayCertified) certs += " <span style='vertical-align: super; font-size: .8em;'>(2)</span>";
                if (vendor.CTPATCertified) certs += " <span style='vertical-align: super; font-size: .8em;'>(3)</span>";
                if (vendor.PIPCertified) certs += " <span style='vertical-align: super; font-size: .8em;'>(4)</span>";
            }

            return certs;
        }

        public static string GenerateRateDownload(this MemberPageBase page, Shipment shipment, List<Rate> rates, bool useVendorRatingActualName)
        {
            // formats
            var d = new Decimals(null);

            // get template
            var template = page.Server.ReadFromFile(SystemTemplates.RateDownloadTemplate).FromUtf8Bytes();

            // logo url
            var customer = shipment.Customer;
            var logoUrl = customer == null ? string.Empty : customer.RetrievePageLogoUrl();
            var siteRoot = page.Request.ResolveSiteRootWithHttp();
            logoUrl = logoUrl.Replace("~", siteRoot);

            // body wrapper
            template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
            template = template.Replace(BaseFields.BodyWrapperEnd, WebApplicationSettings.Brand);

            // general details
            template = template.Replace(RateDownloadFields.AccountName, customer == null ? string.Empty : customer.Name);
            template = template.Replace(RateDownloadFields.AccountNumber, customer == null ? string.Empty : customer.CustomerNumber);
            template = template.Replace(EmailTemplateFields.SiteRoot, siteRoot);
            template = template.Replace(EmailTemplateFields.LogoUrl, logoUrl);

            // warnings
            var itemsPackageInValid = shipment.ItemsInvalidForPackageRating();
            var itemsLTLFreightClassInValid = shipment.ItemsInvalidForLTLFreightClassRating();
            var itemsLTLDensityInValid = shipment.ItemsInvalidForLTLDensityRating();
            var itemsFTLInValid = shipment.ItemsInvalidForFTLRating();
            var removeWarnings = string.IsNullOrEmpty(itemsLTLFreightClassInValid) &&
                                 string.IsNullOrEmpty(itemsLTLDensityInValid)
                                 && string.IsNullOrEmpty(itemsFTLInValid) && string.IsNullOrEmpty(itemsPackageInValid);

            var content = template.RetrieveWrapperSection(RateDownloadFields.WarningsWrapperStart,
                                                          RateDownloadFields.WarningsWrapperEnd);
            var replacement = content
                .Replace(RateDownloadFields.WarningFTL, itemsFTLInValid)
                .Replace(RateDownloadFields.WarningLTLFreightClass, itemsLTLFreightClassInValid)
                .Replace(RateDownloadFields.WarningLTLDensity, itemsLTLDensityInValid)
                .Replace(RateDownloadFields.WarningPackage, itemsPackageInValid);

            template = template.Replace(RateDownloadFields.WarningsWrapperStart, string.Empty);
            template = template.Replace(content, removeWarnings ? string.Empty : replacement);
            template = template.Replace(RateDownloadFields.WarningsWrapperEnd, string.Empty);

            //origin
            template = template.Replace(RateDownloadFields.OriginPostalCode, shipment.Origin.PostalCode);
            template = template.Replace(RateDownloadFields.OriginCountry, shipment.Origin.Country.Name);

            //destination
            template = template.Replace(RateDownloadFields.DestinationPostalCode, shipment.Destination.PostalCode);
            template = template.Replace(RateDownloadFields.DestinationCountry, shipment.Destination.Country.Name);

            // accessorials
            content = template.RetrieveWrapperSection(RateDownloadFields.AccessorialsWrapperStart,
                                                      RateDownloadFields.AccessorialsWrapperEnd);
            var lines = shipment.Services.Any()
                            ? shipment.Services
                                .Select(s => content
                                                .Replace(RateDownloadFields.AccessorialsCode, s.Service.Code)
                                                .Replace(RateDownloadFields.AccessorialsDescription, s.Service.Description))
                                .ToArray()
                            : new string[0];

            template = template.Replace(RateDownloadFields.AccessorialsWrapperStart, string.Empty);
            template = template.Replace(content, string.Join(string.Empty, lines));
            template = template.Replace(RateDownloadFields.AccessorialsWrapperEnd, string.Empty);

            // items
            var hasItems = shipment.Items.Any();
            var itemsTotalValue = shipment.Items.Sum(i => i.Value);

            template = template.Replace(RateDownloadFields.ItemsTotalValue,
                                        hasItems ? itemsTotalValue == 0 ? "$" : itemsTotalValue.ToString(d.Currency) : string.Empty);

            template = template.Replace(RateDownloadFields.ItemsTotalWeight,
                                        hasItems ? shipment.Items.Sum(i => i.ActualWeight).ToString(d.Weight) : string.Empty);
            var lastStop = shipment.Destination.StopOrder;
            content = template.RetrieveWrapperSection(RateDownloadFields.ItemsWrapperStart, RateDownloadFields.ItemsWrapperEnd);
            lines = hasItems
                        ? shipment.Items
                            .Select(i => content
                                            .Replace(RateDownloadFields.ItemQuantity, i.Quantity.ToString())
                                            .Replace(RateDownloadFields.ItemPieceCount, i.PieceCount == 0 ? string.Empty : i.PieceCount.ToString())
                                            .Replace(RateDownloadFields.ItemPackageType, i.PackageType == null ? string.Empty : i.PackageType.TypeName)
                                            .Replace(RateDownloadFields.ItemDescription, i.Description)
                                            .Replace(RateDownloadFields.ItemTotalWeight, i.ActualWeight.ToString(d.Weight))
                                            .Replace(RateDownloadFields.ItemLength, i.ActualLength.ToString(d.Dimension))
                                            .Replace(RateDownloadFields.ItemWidth, i.ActualWidth.ToString(d.Dimension))
                                            .Replace(RateDownloadFields.ItemHeight, i.ActualHeight.ToString(d.Dimension))
                                            .Replace(RateDownloadFields.ItemFreightClass, i.ActualFreightClass.ToString())
                                            .Replace(RateDownloadFields.ItemStackable, i.IsStackable ? "[STACKABLE]" : string.Empty)
                                            .Replace(RateDownloadFields.ItemValue, i.Value == 0 ? "$" : i.Value.ToString(d.Currency))
                                            .Replace(RateDownloadFields.ItemNMFC, i.NMFCCode)
                                            .Replace(RateDownloadFields.ItemHTS, i.HTSCode)
                                            .Replace(RateDownloadFields.ItemPickup, i.Pickup.GetStopDescription(lastStop))
                                            .Replace(RateDownloadFields.ItemDelivery, i.Delivery.GetStopDescription(lastStop))
                                            .Replace(RateDownloadFields.ItemHazardous, i.HazardousMaterial ? WebApplicationConstants.HtmlCheck : string.Empty)
                                            .Replace(RateDownloadFields.ItemComments, i.Comment))
                            .ToArray()
                        : new string[0];

            template = template.Replace(RateDownloadFields.ItemsWrapperStart, string.Empty);
            template = template.Replace(content, string.Join(string.Empty, lines));
            template = template.Replace(RateDownloadFields.ItemsWrapperEnd, string.Empty);

            // rate charges
            content = template.RetrieveWrapperSection(RateDownloadFields.ChargesWrapperStart, RateDownloadFields.ChargesWrapperEnd);
            lines = rates
                .Select(r =>
                            {
                                var chargeCarrier = r.Mode == ServiceMode.LessThanTruckload
                                                        ? useVendorRatingActualName
                                                            ? r.LTLSellRate.VendorRating.Name
                                                            : r.LTLSellRate.VendorRating.DisplayName
                                                        : r.Mode == ServiceMode.SmallPackage
                                                            ? r.Vendor.Name
                                                            : "Contract Rate";
                                var vendor = r.Mode == ServiceMode.LessThanTruckload
                                                ? r.LTLSellRate.VendorRating.Vendor
                                                : r.Mode == ServiceMode.SmallPackage
                                                    ? r.Vendor
                                                    : null;
                                chargeCarrier += GetApplicableVendorCerts(vendor);

                                if (r.CFCRuleId != default(long))
                                    chargeCarrier += "<div style='padding-top:3px; font-size:.7em;'>* Cubic foot adjustments</div>";
                                if (r.OverlengthRuleApplied)
                                    chargeCarrier += "<div style='padding-top:3px; font-size:.7em;'>* Overlength piece</div>";

                                return content
                                    .Replace(RateDownloadFields.ChargeCarrier, chargeCarrier)
                                    .Replace(RateDownloadFields.ChargeMode, r.Mode.FormattedString())
                                    .Replace(RateDownloadFields.ChargeTotalFreight,
                                             r.Charges.Where(c => c.ChargeCode.Category == ChargeCodeCategory.Freight)
                                              .Sum(c => c.AmountDue)
                                              .ToString(d.Currency))
                                    .Replace(RateDownloadFields.ChargeTotalFuel,
                                             r.Charges.Where(c => c.ChargeCode.Category == ChargeCodeCategory.Fuel)
                                              .Sum(c => c.AmountDue)
                                              .ToString(d.Currency))
                                    .Replace(RateDownloadFields.ChargeTotalAccessorial,
                                             r.Charges.Where(c => c.ChargeCode.Category == ChargeCodeCategory.Accessorial)
                                              .Sum(c => c.AmountDue)
                                              .ToString(d.Currency))
                                    .Replace(RateDownloadFields.ChargeTotalService,
                                             r.Charges.Where(c => c.ChargeCode.Category == ChargeCodeCategory.Service)
                                              .Sum(c => c.AmountDue)
                                              .ToString(d.Currency))
                                    .Replace(RateDownloadFields.ChargeTotalShipment, r.Charges.Sum(c => c.AmountDue).ToString(d.Currency))
                                    .Replace(RateDownloadFields.EstimatedDeliveryDate, r.EstimatedDeliveryDate.FormattedShortDateAlt())
                                    .Replace(RateDownloadFields.ChargeTransit, r.TransitDays.ToString());
                            })
                .ToArray();

            template = template.Replace(RateDownloadFields.ChargesWrapperStart, string.Empty);
            template = template.Replace(content, string.Join(string.Empty, lines));
            template = template.Replace(RateDownloadFields.ChargesWrapperEnd, string.Empty);

            var smartwayLogoUrl = "~/Images/Certifications/SmartWay.jpg".Replace("~", page.Request.ResolveSiteRootWithHttp());
            template = template.Replace(RateDownloadFields.SmartwayLogo, smartwayLogoUrl);
            var tiaLogoUrl = "~/Images/Certifications/tiaMember.jpg".Replace("~", page.Request.ResolveSiteRootWithHttp());
            template = template.Replace(RateDownloadFields.TiaMemberLogo, tiaLogoUrl);

            return template;
        }



        public static string GenerateBOL(this MemberPageBase page, Shipment shipment, bool removeBodyWrapper = false)
        {
            // get template
            Decimals d;
            var template = page.Server
                .FetchTemplate(shipment.TenantId, shipment.PrefixId, shipment.ServiceMode, DocumentTemplateCategory.BillOfLading, out d);

            // logo url
            var siteRoot = page.Request.ResolveSiteRootWithHttp();
            var logoUrl = shipment.Customer.RetrievePageLogoUrl();
            logoUrl = logoUrl.Replace("~", siteRoot);

            // check void
            if (shipment.Status == ShipmentStatus.Void)
            {
                var voidAdded = string.Format("{0}{1}", BaseFields.BodyWrapperStart, string.Format(VoidWaterMark, siteRoot));
                template = template.Replace(BaseFields.BodyWrapperStart, voidAdded);
            }

            // body wrapper
            if (removeBodyWrapper)
            {
                template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
                template = template.Replace(BaseFields.BodyWrapperEnd, WebApplicationSettings.Brand);
            }

            // general details
            var combine = siteRoot + WebApplicationSettings.UploadedImagesFolder(shipment.TenantId).Replace("~", string.Empty);
            template = template.Replace(ShipmentFields.UploadedImages, combine);

            template = template.Replace(ShipmentFields.ShipmentNumber, shipment.ShipmentNumber);
            template = template.Replace(ShipmentFields.Prefix, shipment.HidePrefix || shipment.Prefix == null ? string.Empty : shipment.Prefix.Code);
            template = template.Replace(ShipmentFields.ShipmentDate, shipment.DesiredPickupDate.FormattedShortDate());
            template = template.Replace(ShipmentFields.EarliestPickup, shipment.EarlyPickup);
            template = template.Replace(ShipmentFields.LastestPickup, shipment.LatePickup);
            template = template.Replace(ShipmentFields.EarliestDelivery, shipment.EarlyDelivery);
            template = template.Replace(ShipmentFields.LastestDelivery, shipment.LateDelivery);
            template = template.Replace(ShipmentFields.GeneralBOLComments, shipment.GeneralBolComments);
            template = template.Replace(ShipmentFields.CriticalBOLComments, shipment.CriticalBolComments);
            template = template.Replace(ShipmentFields.PurchaseOrder, shipment.PurchaseOrderNumber);
            template = template.Replace(ShipmentFields.ShipperReference, shipment.ShipperReference);
            template = template.Replace(ShipmentFields.LogoUrl, logoUrl);
            template = template.Replace(ShipmentFields.CustomerBOLText, shipment.Customer.AdditionalBillOfLadingText);
            template = template.Replace(ShipmentFields.TierBOLText, shipment.Customer.Tier.AdditionalBillOfLadingText);
            template = template.Replace(ShipmentFields.ShipperBillOfLadingNumber, shipment.ShipperBol);
            template = template.Replace(ShipmentFields.MiscellaneousField1, shipment.MiscField1);
            template = template.Replace(ShipmentFields.MiscellaneousField2, shipment.MiscField2);


            //Tier
            template = template.Replace(ShipmentFields.TierNumber, shipment.Customer.Tier.TierNumber);
            template = template.Replace(ShipmentFields.TierName, shipment.Customer.Tier.Name);

            //customer
            template = template.Replace(ShipmentFields.CustomerNumber, shipment.Customer.CustomerNumber);
            template = template.Replace(ShipmentFields.CustomerName, shipment.Customer.Name);

            // vendors
            if (template.Contains(ShipmentFields.PrimaryVendorName) || template.Contains(ShipmentFields.PrimaryVendorPro))
            {
                var sv = shipment.Vendors.FirstOrDefault(v => v.Primary);
                template = template.Replace(ShipmentFields.PrimaryVendorName, sv == null ? string.Empty : sv.Vendor.Name);
                template = template.Replace(ShipmentFields.PrimaryVendorPro, sv == null ? string.Empty : sv.ProNumber);
            }
            if (template.Contains(ShipmentFields.VendorsWrapperStart))
            {
                var content = template.RetrieveWrapperSection(ShipmentFields.VendorsWrapperStart, ShipmentFields.VendorsWrapperEnd);
                var lines = shipment.Vendors.Any()
                                ? shipment.Vendors
                                    .Select(v => content
                                                    .Replace(ShipmentFields.VendorName, v.Vendor.Name)
                                                    .Replace(ShipmentFields.VendorPro, v.ProNumber))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(ShipmentFields.VendorsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(ShipmentFields.VendorsWrapperEnd, string.Empty);
            }

            var primaryVendor = shipment.Vendors.First(v => v.Primary);
            template = template.Replace(ShipmentFields.PrimaryVendorPro, primaryVendor.ProNumber);
            template = template.Replace(ShipmentFields.PrimaryVendorScac, primaryVendor.Vendor.Scac);

            // user to contact 
            var contactUser = shipment.CarrierCoordinator ?? shipment.User;
            template = template.Replace(ShipmentFields.ContactName, string.Format("{0} {1}", contactUser.FirstName, contactUser.LastName));
            template = template.Replace(ShipmentFields.ContactPhone, contactUser.Phone);
            template = template.Replace(ShipmentFields.ContactTollFree, shipment.Customer.Tier.TollFreeContactNumber);

            // creatd by contact info specific
            template = template.Replace(ShipmentFields.CreatedByContactName, string.Format("{0} {1}", shipment.User.FirstName, shipment.User.LastName));
            template = template.Replace(ShipmentFields.CreatedByContactPhone, shipment.User.Phone);

            //department info
            var dept = contactUser.Department;
            template = template.Replace(ShipmentFields.DepartmentPhone, dept == null ? string.Empty : dept.Phone);
            template = template.Replace(ShipmentFields.DepartmentFax, dept == null ? string.Empty : dept.Fax);
            template = template.Replace(ShipmentFields.DepartmentEmail, dept == null ? string.Empty : dept.Email);

            //created by user department
            var userDepartment = shipment.User.Department;
            template = template.Replace(ShipmentFields.CreatedByUserDepartmentPhone, userDepartment == null ? string.Empty : userDepartment.Phone);

            //origin
            template = template.Replace(ShipmentFields.OriginDescription, shipment.Origin.Description);
            template = template.Replace(ShipmentFields.OriginStreet1, shipment.Origin.Street1);
            template = template.Replace(ShipmentFields.OriginStreet2, shipment.Origin.Street2);
            template = template.Replace(ShipmentFields.OriginCity, shipment.Origin.City);
            template = template.Replace(ShipmentFields.OriginState, shipment.Origin.State);
            template = template.Replace(ShipmentFields.OriginPostalCode, shipment.Origin.PostalCode);
            template = template.Replace(ShipmentFields.OriginCountry, shipment.Origin.Country.Name);
            template = template.Replace(ShipmentFields.OriginInstructions, shipment.Origin.SpecialInstructions);
            template = template.Replace(ShipmentFields.OriginGeneralInfo, shipment.Origin.GeneralInfo);
            template = template.Replace(ShipmentFields.OriginDirections, shipment.Origin.Direction);

            if (template.Contains(ShipmentFields.OriginPrimaryContactEnabled))
            {
                var sc = shipment.Origin.Contacts.FirstOrDefault(c => c.Primary) ?? new ShipmentContact();
                template = template.Replace(ShipmentFields.OriginPrimaryContactEnabled, string.Empty);
                template = template.Replace(ShipmentFields.OriginPrimaryContact, sc.Name);
                template = template.Replace(ShipmentFields.OriginPrimaryPhone, sc.Phone);
                template = template.Replace(ShipmentFields.OriginPrimaryFax, sc.Fax);
                template = template.Replace(ShipmentFields.OriginPrimaryMobile, sc.Mobile);
                template = template.Replace(ShipmentFields.OriginPrimaryEmail, sc.Email);
            }
            if (template.Contains(ShipmentFields.OriginContactsWrapperStart))
            {
                var content = template.RetrieveWrapperSection(ShipmentFields.OriginContactsWrapperStart, ShipmentFields.OriginContactsWrapperEnd);
                var lines = shipment.Origin.Contacts.Any()
                                ? shipment.Origin.Contacts
                                    .Select(c => content
                                                    .Replace(ShipmentFields.OriginContact, c.Name)
                                                    .Replace(ShipmentFields.OriginPhone, c.Phone)
                                                    .Replace(ShipmentFields.OriginFax, c.Fax)
                                                    .Replace(ShipmentFields.OriginMobile, c.Mobile)
                                                    .Replace(ShipmentFields.OriginEmail, c.Email))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(ShipmentFields.OriginContactsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(ShipmentFields.OriginContactsWrapperEnd, string.Empty);
            }
            if (template.Contains(ShipmentFields.OriginReferencesWrapperStart))
            {
                var content = template.RetrieveWrapperSection(ShipmentFields.OriginReferencesWrapperStart, ShipmentFields.OriginReferencesWrapperEnd);
                var lines = shipment.CustomerReferences.Any(r => r.DisplayOnOrigin)
                                ? shipment.CustomerReferences
                                    .Where(r => r.DisplayOnOrigin)
                                    .Select(r => content
                                                    .Replace(ShipmentFields.OriginReferenceName, r.Name)
                                                    .Replace(ShipmentFields.OriginReferenceValue, r.Value))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(ShipmentFields.OriginReferencesWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(ShipmentFields.OriginReferencesWrapperEnd, string.Empty);
            }

            var terminal = shipment.OriginTerminal ?? new LTLTerminalInfo();
            template = template.Replace(ShipmentFields.OriginTerminalCode, terminal.Code);
            template = template.Replace(ShipmentFields.OriginTerminalName, terminal.Name);
            template = template.Replace(ShipmentFields.OriginTerminalPhone, terminal.Phone);
            template = template.Replace(ShipmentFields.OriginTerminalFax, terminal.Fax);
            template = template.Replace(ShipmentFields.OriginTerminalTollFree, terminal.TollFree);
            template = template.Replace(ShipmentFields.OriginTerminalEmail, terminal.Email);
            template = template.Replace(ShipmentFields.OriginTerminalContactName, terminal.ContactName);
            template = template.Replace(ShipmentFields.OriginTerminalContactTitle, terminal.ContactTitle);


            // destination
            template = template.Replace(ShipmentFields.DestinationDescription, shipment.Destination.Description);
            template = template.Replace(ShipmentFields.DestinationStreet1, shipment.Destination.Street1);
            template = template.Replace(ShipmentFields.DestinationStreet2, shipment.Destination.Street2);
            template = template.Replace(ShipmentFields.DestinationCity, shipment.Destination.City);
            template = template.Replace(ShipmentFields.DestinationState, shipment.Destination.State);
            template = template.Replace(ShipmentFields.DestinationPostalCode, shipment.Destination.PostalCode);
            template = template.Replace(ShipmentFields.DestinationCountry, shipment.Destination.Country.Name);
            template = template.Replace(ShipmentFields.DestinationInstructions, shipment.Destination.SpecialInstructions);
            template = template.Replace(ShipmentFields.DestinationGeneralInfo, shipment.Destination.GeneralInfo);

            if (template.Contains(ShipmentFields.DestinationPrimaryContactEnabled))
            {
                var sc = shipment.Destination.Contacts.FirstOrDefault(c => c.Primary) ?? new ShipmentContact();
                template = template.Replace(ShipmentFields.DestinationPrimaryContactEnabled, string.Empty);
                template = template.Replace(ShipmentFields.DestinationPrimaryContact, sc.Name);
                template = template.Replace(ShipmentFields.DestinationPrimaryPhone, sc.Phone);
                template = template.Replace(ShipmentFields.DestinationPrimaryFax, sc.Fax);
                template = template.Replace(ShipmentFields.DestinationPrimaryMobile, sc.Mobile);
                template = template.Replace(ShipmentFields.DestinationPrimaryEmail, sc.Email);
            }
            if (template.Contains(ShipmentFields.DestinationContactsWrapperStart))
            {
                var content = template.RetrieveWrapperSection(ShipmentFields.DestinationContactsWrapperStart, ShipmentFields.DestinationContactsWrapperEnd);
                var lines = shipment.Destination.Contacts.Any()
                                ? shipment.Destination.Contacts
                                    .Select(c => content
                                                    .Replace(ShipmentFields.DestinationContact, c.Name)
                                                    .Replace(ShipmentFields.DestinationPhone, c.Phone)
                                                    .Replace(ShipmentFields.DestinationFax, c.Fax)
                                                    .Replace(ShipmentFields.DestinationMobile, c.Mobile)
                                                    .Replace(ShipmentFields.DestinationEmail, c.Email))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(ShipmentFields.DestinationContactsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(ShipmentFields.DestinationContactsWrapperEnd, string.Empty);
            }
            if (template.Contains(ShipmentFields.DestinationReferencesWrapperStart))
            {
                var content = template.RetrieveWrapperSection(ShipmentFields.DestinationReferencesWrapperStart, ShipmentFields.DestinationReferencesWrapperEnd);
                var lines = shipment.CustomerReferences.Any(r => r.DisplayOnDestination)
                                ? shipment.CustomerReferences
                                    .Where(r => r.DisplayOnDestination)
                                    .Select(r => content
                                                    .Replace(ShipmentFields.DestinationReferenceName, r.Name)
                                                    .Replace(ShipmentFields.DestinationReferenceValue, r.Value))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(ShipmentFields.DestinationReferencesWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(ShipmentFields.DestinationReferencesWrapperEnd, string.Empty);
            }

            terminal = shipment.DestinationTerminal ?? new LTLTerminalInfo();
            template = template.Replace(ShipmentFields.DestinationTerminalCode, terminal.Code);
            template = template.Replace(ShipmentFields.DestinationTerminalName, terminal.Name);
            template = template.Replace(ShipmentFields.DestinationTerminalPhone, terminal.Phone);
            template = template.Replace(ShipmentFields.DestinationTerminalFax, terminal.Fax);
            template = template.Replace(ShipmentFields.DestinationTerminalTollFree, terminal.TollFree);
            template = template.Replace(ShipmentFields.DestinationTerminalEmail, terminal.Email);
            template = template.Replace(ShipmentFields.DestinationTerminalContactName, terminal.ContactName);
            template = template.Replace(ShipmentFields.DestinationTerminalContactTitle, terminal.ContactTitle);

            // items
            var hasItems = shipment.Items.Any();
            var itemsTotalValue = shipment.Items.Sum(i => i.Value);
            template = template.Replace(ShipmentFields.ItemsTotalValue, hasItems ? itemsTotalValue == 0 ? "$" : itemsTotalValue.ToString(d.Currency) : string.Empty);
            template = template.Replace(ShipmentFields.ItemsTotalWeight, hasItems ? shipment.Items.Sum(i => i.ActualWeight).ToString(d.Weight) : string.Empty);
            template = template.Replace(ShipmentFields.ItemsTotalQuantity, hasItems ? shipment.Items.Sum(i => i.Quantity).ToString() : string.Empty);
            template = template.Replace(ShipmentFields.ItemsTotalPieceCount, hasItems ? shipment.Items.Sum(i => i.PieceCount).ToString() : string.Empty);

            if (template.Contains(ShipmentFields.ItemsWrapperStart))
            {
                var lastStop = shipment.Destination.StopOrder;
                var content = template.RetrieveWrapperSection(ShipmentFields.ItemsWrapperStart, ShipmentFields.ItemsWrapperEnd);
                var lines = hasItems
                                ? shipment.Items.OrderBy(i => i.Pickup).ThenBy(itt => itt.Delivery)
                                    .Select(i => content
                                                    .Replace(ShipmentFields.ItemQuantity, i.Quantity.ToString())
                                                    .Replace(ShipmentFields.ItemPieceCount, i.PieceCount == 0 ? string.Empty : i.PieceCount.ToString())
                                                    .Replace(ShipmentFields.PiecesAbbreviation, i.PieceCount == 0 ? string.Empty : "Pcs")
                                                    .Replace(ShipmentFields.ItemPackageType, i.PackageType.TypeName)
                                                    .Replace(ShipmentFields.ItemDescription, i.Description)
                                                    .Replace(ShipmentFields.ItemTotalWeight, i.ActualWeight.ToString(d.Weight))
                                                    .Replace(ShipmentFields.ItemLength, i.ActualLength.ToString(d.Dimension))
                                                    .Replace(ShipmentFields.ItemWidth, i.ActualWidth.ToString(d.Dimension))
                                                    .Replace(ShipmentFields.ItemHeight, i.ActualHeight.ToString(d.Dimension))
                                                    .Replace(ShipmentFields.ItemFreightClass, i.ActualFreightClass.ToString())
                                                    .Replace(ShipmentFields.ItemStackable, i.IsStackable ? "[STACKABLE]" : string.Empty)
                                                    .Replace(ShipmentFields.ItemValue, i.Value == 0 ? "$" : i.Value.ToString(d.Currency))
                                                    .Replace(ShipmentFields.ItemNMFC, i.NMFCCode)
                                                    .Replace(ShipmentFields.ItemHTS, i.HTSCode)
                                                    .Replace(ShipmentFields.ItemPickup, i.Pickup.GetStopDescription(lastStop))
                                                    .Replace(ShipmentFields.ItemDelivery, i.Delivery.GetStopDescription(lastStop))
                                                    .Replace(ShipmentFields.ItemHazardous, i.HazardousMaterial ? WebApplicationConstants.HtmlCheck : string.Empty)
                                                    .Replace(ShipmentFields.ItemComments, i.Comment))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(ShipmentFields.ItemsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(ShipmentFields.ItemsWrapperEnd, string.Empty);
            }

            // stops
            if (template.Contains(ShipmentFields.StopsWrapperStart))
            {
                var content = template.RetrieveWrapperSection(ShipmentFields.StopsWrapperStart, ShipmentFields.StopsWrapperEnd);
                var lines = new List<string>();
                foreach (var stop in shipment.Stops)
                {
                    var sLine = content;
                    sLine = sLine.Replace(ShipmentFields.StopDescription, stop.Description);
                    sLine = sLine.Replace(ShipmentFields.StopStreet1, stop.Street1);
                    sLine = sLine.Replace(ShipmentFields.StopStreet2, stop.Street2);
                    sLine = sLine.Replace(ShipmentFields.StopCity, stop.City);
                    sLine = sLine.Replace(ShipmentFields.StopState, stop.State);
                    sLine = sLine.Replace(ShipmentFields.StopPostalCode, stop.PostalCode);
                    sLine = sLine.Replace(ShipmentFields.StopCountry, stop.Country.Name);
                    sLine = sLine.Replace(ShipmentFields.StopInstructions, stop.SpecialInstructions);
                    sLine = sLine.Replace(ShipmentFields.StopGeneralInfo, stop.GeneralInfo);
                    sLine = sLine.Replace(ShipmentFields.StopDirections, stop.Direction);

                    if (sLine.Contains(ShipmentFields.StopPrimaryContactEnabled))
                    {
                        var sc = stop.Contacts.FirstOrDefault(c => c.Primary) ?? new ShipmentContact();
                        sLine = sLine.Replace(ShipmentFields.StopPrimaryContactEnabled, string.Empty);
                        sLine = sLine.Replace(ShipmentFields.StopPrimaryContact, sc.Name);
                        sLine = sLine.Replace(ShipmentFields.StopPrimaryPhone, sc.Phone);
                        sLine = sLine.Replace(ShipmentFields.StopPrimaryFax, sc.Fax);
                        sLine = sLine.Replace(ShipmentFields.StopPrimaryMobile, sc.Mobile);
                        sLine = sLine.Replace(ShipmentFields.StopPrimaryEmail, sc.Email);
                    }
                    if (sLine.Contains(ShipmentFields.StopContactsWrapperStart))
                    {
                        var sContent = sLine.RetrieveWrapperSection(ShipmentFields.StopContactsWrapperStart, ShipmentFields.StopContactsWrapperEnd);
                        var sLines = stop.Contacts.Any()
                                        ? stop.Contacts
                                            .Select(c => sLine
                                                            .Replace(ShipmentFields.StopContact, c.Name)
                                                            .Replace(ShipmentFields.StopPhone, c.Phone)
                                                            .Replace(ShipmentFields.StopFax, c.Fax)
                                                            .Replace(ShipmentFields.StopMobile, c.Mobile)
                                                            .Replace(ShipmentFields.StopEmail, c.Email))
                                            .ToArray()
                                        : new string[0];

                        sLine = sLine.Replace(ShipmentFields.StopContactsWrapperStart, string.Empty);
                        sLine = sLine.Replace(sContent, string.Join(string.Empty, sLines));
                        sLine = sLine.Replace(ShipmentFields.StopContactsWrapperEnd, string.Empty);
                    }
                    lines.Add(sLine);
                }

                template = template.Replace(ShipmentFields.StopsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines.ToArray()));
                template = template.Replace(ShipmentFields.StopsWrapperEnd, string.Empty);
            }

            // accessorials
            if (template.Contains(ShipmentFields.AccessorialsWrapperStart))
            {
                var content = template.RetrieveWrapperSection(ShipmentFields.AccessorialsWrapperStart, ShipmentFields.AccessorialsWrapperEnd);
                var lines = shipment.Services.Any()
                                ? shipment.Services
                                    .Select(s => content
                                                    .Replace(ShipmentFields.AccessorialsCode, s.Service.Code)
                                                    .Replace(ShipmentFields.AccessorialsDescription, s.Service.Description))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(ShipmentFields.AccessorialsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(ShipmentFields.AccessorialsWrapperEnd, string.Empty);
            }

            //Critical notes
            if (template.Contains(ShipmentFields.CriticalNotesWrapperStart))
            {
                var content = template.RetrieveWrapperSection(ShipmentFields.CriticalNotesWrapperStart, ShipmentFields.CriticalNotesWrapperEnd);
                var lines = shipment.Notes.Any()
                                ? shipment.Notes
                                    .Where(n => n.Type == NoteType.Critical && !n.Classified)
                                    .Select(note => content.Replace(ShipmentFields.CriticalNotesMessage, note.Message))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(ShipmentFields.CriticalNotesWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(ShipmentFields.CriticalNotesWrapperEnd, string.Empty);
            }

            //General notes
            if (template.Contains(ShipmentFields.GeneralNotesWrapperStart))
            {
                var content = template.RetrieveWrapperSection(ShipmentFields.GeneralNotesWrapperStart, ShipmentFields.GeneralNotesWrapperEnd);
                var lines = shipment.Notes.Any()
                                ? shipment.Notes
                                    .Where(n => n.Type == NoteType.Normal && !n.Classified)
                                    .Select(note => content.Replace(ShipmentFields.GeneralNotesMessage, note.Message))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(ShipmentFields.GeneralNotesWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(ShipmentFields.GeneralNotesWrapperEnd, string.Empty);
            }

            // harzardous materials
            if (template.Contains(ShipmentFields.HazardousMaterialWrapperStart))
            {
                var content = template.RetrieveWrapperSection(ShipmentFields.HazardousMaterialWrapperStart, ShipmentFields.HazardousMaterialWrapperEnd);
                template = template.Replace(ShipmentFields.HazardousMaterialWrapperStart, string.Empty);
                if (shipment.HazardousMaterial)
                {
                    template = template.Replace(ShipmentFields.HazardousMaterialContact, shipment.HazardousMaterialContactName);
                    template = template.Replace(ShipmentFields.HazardousMaterialPhone, shipment.HazardousMaterialContactPhone);
                    template = template.Replace(ShipmentFields.HazardousMaterialMobile, shipment.HazardousMaterialContactMobile);
                    template = template.Replace(ShipmentFields.HazardousMaterialEmail, shipment.HazardousMaterialContactEmail);
                }
                else
                    template = template.Replace(content, string.Empty);
                template = template.Replace(ShipmentFields.HazardousMaterialWrapperEnd, string.Empty);
            }

            template = template.Replace(ShipmentFields.ItemHazmat, shipment.HazardousMaterial ? WebApplicationConstants.X : string.Empty);

            // rating override bill to address
            if (!string.IsNullOrEmpty(shipment.VendorRatingOverrideAddress) && template.Contains(ShipmentFields.VendorRatingProfileAddressWrapperStart))
            {
                var content = template.RetrieveWrapperSection(ShipmentFields.VendorRatingProfileAddressWrapperStart, ShipmentFields.VendorRatingProfileAddressWrapperEnd);
                template = template.Replace(ShipmentFields.VendorRatingProfileAddressWrapperStart, string.Empty);
                template = template.Replace(content, shipment.VendorRatingOverrideAddress.ReplaceNewLineWithHtmlBreak());
                template = template.Replace(ShipmentFields.VendorRatingProfileAddressWrapperEnd, string.Empty);
            }
            else
            {
                template = template.Replace(ShipmentFields.VendorRatingProfileAddressWrapperStart, string.Empty);
                template = template.Replace(ShipmentFields.VendorRatingProfileAddressWrapperEnd, string.Empty);
            }


            // Care of address
            if (shipment.CareOfAddressFormatEnabled && template.Contains(ShipmentFields.CareOfWrapperStart))
            {
                template = template.Replace(ShipmentFields.CareOfWrapperStart, string.Empty);
                template = template.Replace(ShipmentFields.CareOfWrapperEnd, string.Empty);
            }
            else if (!shipment.CareOfAddressFormatEnabled && template.Contains(ShipmentFields.CareOfWrapperStart))
            {
                var content = template.RetrieveWrapperSection(ShipmentFields.CareOfWrapperStart, ShipmentFields.CareOfWrapperEnd);
                template = template.Replace(ShipmentFields.CareOfWrapperStart, string.Empty);
                template = template.Replace(content, string.Empty);
                template = template.Replace(ShipmentFields.CareOfWrapperEnd, string.Empty);
            }

            return template;
        }
        public static string GenerateShipmentLabels(this MemberPageBase page, Shipment shipment, bool removeBodyWrapper = false, DocumentTemplateCategory templateCategory = DocumentTemplateCategory.ProLabel4x6inch)
        {
            // get template
            Decimals d;
            string template = "";
            if (templateCategory == DocumentTemplateCategory.ProLabel4x6inch)
                template = page.Server.ReadFromFile(SystemTemplates.ProLabel4x6Template).FromUtf8Bytes();
            else if (templateCategory == DocumentTemplateCategory.ProLabelAvery3x4inch)
                template = page.Server.ReadFromFile(SystemTemplates.ProLabelAvery3x4Template).FromUtf8Bytes();

            // body wrapper
            if (removeBodyWrapper)
            {
                template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
                template = template.Replace(BaseFields.BodyWrapperEnd, "");
            }

            template = template.Replace(ShipmentFields.ShipmentNumber, shipment.ShipmentNumber);

            //To
            template = template.Replace(ShipmentFields.OriginDescription, shipment.Origin.Description);
            template = template.Replace(ShipmentFields.OriginStreet1, shipment.Origin.Street1);
            template = template.Replace(ShipmentFields.OriginStreet2, shipment.Origin.Street2);
            template = template.Replace(ShipmentFields.OriginCity, shipment.Origin.City);
            template = template.Replace(ShipmentFields.OriginState, shipment.Origin.State);
            template = template.Replace(ShipmentFields.OriginPostalCode, shipment.Origin.PostalCode);
            template = template.Replace(ShipmentFields.OriginCountry, shipment.Origin.Country.Name);
            template = template.Replace(ShipmentFields.OriginInstructions, shipment.Origin.SpecialInstructions);
            template = template.Replace(ShipmentFields.OriginGeneralInfo, shipment.Origin.GeneralInfo);
            template = template.Replace(ShipmentFields.OriginDirections, shipment.Origin.Direction);
            var sc = shipment.Origin.Contacts.FirstOrDefault(c => c.Primary) ?? new ShipmentContact();
            template = template.Replace(ShipmentFields.OriginPrimaryContactEnabled, string.Empty);
            template = template.Replace(ShipmentFields.OriginPrimaryContact, sc.Name);
            template = template.Replace(ShipmentFields.OriginPrimaryPhone, sc.Phone);
            template = template.Replace(ShipmentFields.OriginPrimaryFax, sc.Fax);
            template = template.Replace(ShipmentFields.OriginPrimaryMobile, sc.Mobile);
            template = template.Replace(ShipmentFields.OriginPrimaryEmail, sc.Email);

            // From
            template = template.Replace(ShipmentFields.DestinationDescription, shipment.Destination.Description);
            template = template.Replace(ShipmentFields.DestinationStreet1, shipment.Destination.Street1);
            template = template.Replace(ShipmentFields.DestinationStreet2, shipment.Destination.Street2);
            template = template.Replace(ShipmentFields.DestinationCity, shipment.Destination.City);
            template = template.Replace(ShipmentFields.DestinationState, shipment.Destination.State);
            template = template.Replace(ShipmentFields.DestinationPostalCode, shipment.Destination.PostalCode);
            template = template.Replace(ShipmentFields.DestinationCountry, shipment.Destination.Country.Name);
            template = template.Replace(ShipmentFields.DestinationInstructions, shipment.Destination.SpecialInstructions);
            template = template.Replace(ShipmentFields.DestinationGeneralInfo, shipment.Destination.GeneralInfo);
            var dc = shipment.Destination.Contacts.FirstOrDefault(c => c.Primary) ?? new ShipmentContact();
            template = template.Replace(ShipmentFields.DestinationPrimaryContactEnabled, string.Empty);
            template = template.Replace(ShipmentFields.DestinationPrimaryContact, dc.Name);
            template = template.Replace(ShipmentFields.DestinationPrimaryPhone, dc.Phone);
            template = template.Replace(ShipmentFields.DestinationPrimaryFax, dc.Fax);
            template = template.Replace(ShipmentFields.DestinationPrimaryMobile, dc.Mobile);
            template = template.Replace(ShipmentFields.DestinationPrimaryEmail, dc.Email);


            // items
            var hasItems = shipment.Items.Any();
            var itemsTotalValue = shipment.Items.Sum(i => i.Value);
            template = template.Replace(ShipmentFields.ItemsTotalQuantity, hasItems ? shipment.Items.Sum(i => i.Quantity).ToString() : string.Empty);
            template = template.Replace(ShipmentFields.ItemsTotalPieceCount, hasItems ? shipment.Items.Sum(i => i.PieceCount).ToString() : string.Empty);

            if (template.Contains(ShipmentFields.ItemsWrapperStart))
            {
                var lastStop = shipment.Destination.StopOrder;
                var content = template.RetrieveWrapperSection(ShipmentFields.ItemsWrapperStart, ShipmentFields.ItemsWrapperEnd);

                List<string> itemsArray = new List<string>();
                foreach (var shipmentItem in shipment.Items)
                {
                    var itemQuantity = shipmentItem.Quantity;
                    string section = "";
                    for (int i = 1; i <= itemQuantity; i++)
                    {
                        section = content.Replace(ShipmentFields.PageNumber, (i).ToString());
                        section = section.Replace(ShipmentFields.TotalPages, itemQuantity.ToString());
                        section = section.Replace(ShipmentFields.ItemPackageType, shipmentItem.PackageType.TypeName.ToString());
                        itemsArray.Add(section);
                    }
                }

                template = template.Replace(ShipmentFields.ItemsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, itemsArray));
                template = template.Replace(ShipmentFields.ItemsWrapperEnd, string.Empty);
            }

            return template;
        }
        public static string GenerateQuote(this MemberPageBase page, LoadOrder quotedLoad, bool removeBodyWrapper = false)
        {
            // get template
            Decimals d;
            var template = page.Server
                .FetchTemplate(quotedLoad.TenantId, quotedLoad.PrefixId, quotedLoad.ServiceMode, DocumentTemplateCategory.Quote, out d);

            // logo url
            var logoUrl = quotedLoad.Customer.RetrievePageLogoUrl();
            var siteRoot = page.Request.ResolveSiteRootWithHttp();
            logoUrl = logoUrl.Replace("~", siteRoot);

            // check void
            if (new[] { LoadOrderStatus.Lost, LoadOrderStatus.Cancelled }.Contains(quotedLoad.Status))
            {
                var voidAdded = string.Format("{0}{1}", BaseFields.BodyWrapperStart, string.Format(VoidWaterMark, siteRoot));
                template = template.Replace(BaseFields.BodyWrapperStart, voidAdded);
            }

            // general details
            template = template.Replace(QuoteFields.QuoteNumber, quotedLoad.LoadOrderNumber);
            template = template.Replace(QuoteFields.QuoteDate, quotedLoad.DateCreated.FormattedShortDate());
            template = template.Replace(QuoteFields.QuotePrefix, quotedLoad.HidePrefix || quotedLoad.Prefix == null ? string.Empty : quotedLoad.Prefix.Code);
            template = template.Replace(QuoteFields.ServiceMode, quotedLoad.ServiceMode.FormattedString());
            template = template.Replace(QuoteFields.DesiredShipmentDate, quotedLoad.DesiredPickupDate.FormattedShortDate());
            template = template.Replace(QuoteFields.LogoUrl, logoUrl);

            // bill to
            var billTo = quotedLoad.Customer.Locations.FirstOrDefault(v => v.BillToLocation && v.MainBillToLocation);
            if (billTo != null)
            {
                template = template.Replace(QuoteFields.BillToDescription, billTo.Customer.Name);
                template = template.Replace(QuoteFields.BillToStreet1, billTo.Street1);
                template = template.Replace(QuoteFields.BillToStreet2, billTo.Street2);
                template = template.Replace(QuoteFields.BillToCity, billTo.City);
                template = template.Replace(QuoteFields.BillToState, billTo.State);
                template = template.Replace(QuoteFields.BillToPostalCode, billTo.PostalCode);
                template = template.Replace(QuoteFields.BillToCountry, billTo.Country.Name);
            }
            else
            {
                template = template.Replace(QuoteFields.BillToDescription, WebApplicationConstants.NoBillToAddressOnFile);
                template = template.Replace(QuoteFields.BillToStreet1, string.Empty);
                template = template.Replace(QuoteFields.BillToStreet2, string.Empty);
                template = template.Replace(QuoteFields.BillToCity, string.Empty);
                template = template.Replace(QuoteFields.BillToState, string.Empty);
                template = template.Replace(QuoteFields.BillToPostalCode, string.Empty);
                template = template.Replace(QuoteFields.BillToCountry, string.Empty);
            }


            // user to contact
            template = template.Replace(QuoteFields.ContactName, string.Format("{0} {1}", quotedLoad.User.FirstName, quotedLoad.User.LastName));
            template = template.Replace(QuoteFields.ContactPhone, quotedLoad.User.Phone);
            template = template.Replace(QuoteFields.ContactTollFree, quotedLoad.Customer.Tier.TollFreeContactNumber);

            //department info
            var dept = quotedLoad.User.Department;
            template = template.Replace(QuoteFields.DepartmentPhone, dept == null ? string.Empty : dept.Phone);
            template = template.Replace(QuoteFields.DepartmentFax, dept == null ? string.Empty : dept.Fax);
            template = template.Replace(QuoteFields.DepartmentEmail, dept == null ? string.Empty : dept.Email);

            //origin
            template = template.Replace(QuoteFields.OriginDescription, quotedLoad.Origin.Description);
            template = template.Replace(QuoteFields.OriginStreet1, quotedLoad.Origin.Street1);
            template = template.Replace(QuoteFields.OriginStreet2, quotedLoad.Origin.Street2);
            template = template.Replace(QuoteFields.OriginCity, quotedLoad.Origin.City);
            template = template.Replace(QuoteFields.OriginState, quotedLoad.Origin.State);
            template = template.Replace(QuoteFields.OriginPostalCode, quotedLoad.Origin.PostalCode);
            template = template.Replace(QuoteFields.OriginCountry, quotedLoad.Origin.Country.Name);
            template = template.Replace(QuoteFields.OriginInstructions, quotedLoad.Origin.SpecialInstructions);

            if (template.Contains(QuoteFields.OriginPrimaryContactEnabled))
            {
                var sc = quotedLoad.Origin.Contacts.FirstOrDefault(c => c.Primary) ?? new LoadOrderContact();
                template = template.Replace(QuoteFields.OriginPrimaryContactEnabled, string.Empty);
                template = template.Replace(QuoteFields.OriginPrimaryContact, sc.Name);
                template = template.Replace(QuoteFields.OriginPrimaryPhone, sc.Phone);
                template = template.Replace(QuoteFields.OriginPrimaryFax, sc.Fax);
                template = template.Replace(QuoteFields.OriginPrimaryMobile, sc.Mobile);
                template = template.Replace(QuoteFields.OriginPrimaryEmail, sc.Email);
            }
            if (template.Contains(QuoteFields.OriginContactsWrapperStart))
            {
                var content = template.RetrieveWrapperSection(QuoteFields.OriginContactsWrapperStart, QuoteFields.OriginContactsWrapperEnd);
                var lines = quotedLoad.Origin.Contacts.Any()
                                ? quotedLoad.Origin.Contacts
                                    .Select(c => content
                                                    .Replace(QuoteFields.OriginContact, c.Name)
                                                    .Replace(QuoteFields.OriginPhone, c.Phone)
                                                    .Replace(QuoteFields.OriginFax, c.Fax)
                                                    .Replace(QuoteFields.OriginMobile, c.Mobile)
                                                    .Replace(QuoteFields.OriginEmail, c.Email))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(QuoteFields.OriginContactsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(QuoteFields.OriginContactsWrapperEnd, string.Empty);
            }
            if (template.Contains(QuoteFields.OriginReferencesWrapperStart))
            {
                var content = template.RetrieveWrapperSection(QuoteFields.OriginReferencesWrapperStart, QuoteFields.OriginReferencesWrapperEnd);
                var lines = quotedLoad.CustomerReferences.Any(r => r.DisplayOnOrigin)
                                ? quotedLoad.CustomerReferences
                                    .Where(r => r.DisplayOnOrigin)
                                    .Select(r => content
                                                    .Replace(QuoteFields.OriginReferenceName, r.Name)
                                                    .Replace(QuoteFields.OriginReferenceValue, r.Value))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(QuoteFields.OriginReferencesWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(QuoteFields.OriginReferencesWrapperEnd, string.Empty);
            }

            // destination
            template = template.Replace(QuoteFields.DestinationDescription, quotedLoad.Destination.Description);
            template = template.Replace(QuoteFields.DestinationStreet1, quotedLoad.Destination.Street1);
            template = template.Replace(QuoteFields.DestinationStreet2, quotedLoad.Destination.Street2);
            template = template.Replace(QuoteFields.DestinationCity, quotedLoad.Destination.City);
            template = template.Replace(QuoteFields.DestinationState, quotedLoad.Destination.State);
            template = template.Replace(QuoteFields.DestinationPostalCode, quotedLoad.Destination.PostalCode);
            template = template.Replace(QuoteFields.DestinationCountry, quotedLoad.Destination.Country.Name);
            template = template.Replace(QuoteFields.DestinationInstructions, quotedLoad.Destination.SpecialInstructions);

            if (template.Contains(QuoteFields.DestinationPrimaryContactEnabled))
            {
                var sc = quotedLoad.Destination.Contacts.FirstOrDefault(c => c.Primary) ?? new LoadOrderContact();
                template = template.Replace(QuoteFields.DestinationPrimaryContactEnabled, string.Empty);
                template = template.Replace(QuoteFields.DestinationPrimaryContact, sc.Name);
                template = template.Replace(QuoteFields.DestinationPrimaryPhone, sc.Phone);
                template = template.Replace(QuoteFields.DestinationPrimaryFax, sc.Fax);
                template = template.Replace(QuoteFields.DestinationPrimaryMobile, sc.Mobile);
                template = template.Replace(QuoteFields.DestinationPrimaryEmail, sc.Email);
            }
            if (template.Contains(QuoteFields.DestinationContactsWrapperStart))
            {
                var content = template.RetrieveWrapperSection(QuoteFields.DestinationContactsWrapperStart, QuoteFields.DestinationContactsWrapperEnd);
                var lines = quotedLoad.Destination.Contacts.Any()
                                ? quotedLoad.Destination.Contacts
                                    .Select(c => content
                                            .Replace(QuoteFields.DestinationContact, c.Name)
                                            .Replace(QuoteFields.DestinationPhone, c.Phone)
                                            .Replace(QuoteFields.DestinationFax, c.Fax)
                                            .Replace(QuoteFields.DestinationMobile, c.Mobile)
                                            .Replace(QuoteFields.DestinationEmail, c.Email))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(QuoteFields.DestinationContactsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(QuoteFields.DestinationContactsWrapperEnd, string.Empty);
            }
            if (template.Contains(QuoteFields.DestinationReferencesWrapperStart))
            {
                var content = template.RetrieveWrapperSection(QuoteFields.DestinationReferencesWrapperStart, QuoteFields.DestinationReferencesWrapperEnd);
                var lines = quotedLoad.CustomerReferences.Any(r => r.DisplayOnDestination)
                                ? quotedLoad.CustomerReferences
                                    .Where(r => r.DisplayOnDestination)
                                    .Select(r => content
                                            .Replace(QuoteFields.DestinationReferenceName, r.Name)
                                            .Replace(QuoteFields.DestinationReferenceValue, r.Value))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(QuoteFields.DestinationReferencesWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(QuoteFields.DestinationReferencesWrapperEnd, string.Empty);
            }

            // items
            var hasItems = quotedLoad.Items.Any();
            var itemsTotalValue = quotedLoad.Items.Sum(i => i.Value);
            template = template.Replace(QuoteFields.ItemsTotalValue, hasItems ? itemsTotalValue == 0 ? "$" : itemsTotalValue.ToString(d.Currency) : string.Empty);
            template = template.Replace(QuoteFields.ItemsTotalWeight, hasItems ? quotedLoad.Items.Sum(i => i.Weight).ToString(d.Weight) : string.Empty);
            if (template.Contains(QuoteFields.ItemsWrapperStart))
            {
                var lastStop = quotedLoad.Destination.StopOrder;
                var content = template.RetrieveWrapperSection(QuoteFields.ItemsWrapperStart, QuoteFields.ItemsWrapperEnd);
                var lines = hasItems
                                ? quotedLoad.Items
                                    .Select(i => content
                                                    .Replace(QuoteFields.ItemQuantity, i.Quantity.ToString())
                                                    .Replace(QuoteFields.ItemPieceCount, i.PieceCount == 0 ? string.Empty : i.PieceCount.ToString())
                                                    .Replace(QuoteFields.ItemPackageType, i.PackageType.TypeName)
                                                    .Replace(QuoteFields.ItemDescription, i.Description)
                                                    .Replace(QuoteFields.ItemTotalWeight, i.Weight.ToString(d.Weight))
                                                    .Replace(QuoteFields.ItemLength, i.Length.ToString(d.Dimension))
                                                    .Replace(QuoteFields.ItemWidth, i.Width.ToString(d.Dimension))
                                                    .Replace(QuoteFields.ItemHeight, i.Height.ToString(d.Dimension))
                                                    .Replace(QuoteFields.ItemFreightClass, i.FreightClass.ToString())
                                                    .Replace(QuoteFields.ItemStackable, i.IsStackable ? "[STACKABLE]" : string.Empty)
                                                    .Replace(QuoteFields.ItemValue, i.Value == 0 ? "$" : i.Value.ToString(d.Currency))
                                                    .Replace(QuoteFields.ItemNMFC, i.NMFCCode)
                                                    .Replace(QuoteFields.ItemHTS, i.HTSCode)
                                                    .Replace(QuoteFields.ItemPickup, i.Pickup.GetStopDescription(lastStop))
                                                    .Replace(QuoteFields.ItemDelivery, i.Delivery.GetStopDescription(lastStop))
                                                    .Replace(QuoteFields.ItemComments, i.Comment)
                                                    .Replace(QuoteFields.ItemHazardous, i.HazardousMaterial ? WebApplicationConstants.HtmlCheck : string.Empty))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(QuoteFields.ItemsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(QuoteFields.ItemsWrapperEnd, string.Empty);
            }

            // stops
            if (template.Contains(QuoteFields.StopsWrapperStart))
            {
                var content = template.RetrieveWrapperSection(QuoteFields.StopsWrapperStart, QuoteFields.StopsWrapperEnd);
                var lines = new List<string>();
                foreach (var stop in quotedLoad.Stops)
                {
                    var sLine = content;
                    sLine = sLine.Replace(QuoteFields.StopDescription, stop.Description);
                    sLine = sLine.Replace(QuoteFields.StopStreet1, stop.Street1);
                    sLine = sLine.Replace(QuoteFields.StopStreet2, stop.Street2);
                    sLine = sLine.Replace(QuoteFields.StopCity, stop.City);
                    sLine = sLine.Replace(QuoteFields.StopState, stop.State);
                    sLine = sLine.Replace(QuoteFields.StopPostalCode, stop.PostalCode);
                    sLine = sLine.Replace(QuoteFields.StopCountry, stop.Country.Name);
                    sLine = sLine.Replace(QuoteFields.StopInstructions, stop.SpecialInstructions);

                    if (sLine.Contains(QuoteFields.StopPrimaryContactEnabled))
                    {
                        var sc = stop.Contacts.FirstOrDefault(c => c.Primary) ?? new LoadOrderContact();
                        sLine = sLine.Replace(QuoteFields.StopPrimaryContactEnabled, string.Empty);
                        sLine = sLine.Replace(QuoteFields.StopPrimaryContact, sc.Name);
                        sLine = sLine.Replace(QuoteFields.StopPrimaryPhone, sc.Phone);
                        sLine = sLine.Replace(QuoteFields.StopPrimaryFax, sc.Fax);
                        sLine = sLine.Replace(QuoteFields.StopPrimaryMobile, sc.Mobile);
                        sLine = sLine.Replace(QuoteFields.StopPrimaryEmail, sc.Email);
                    }
                    if (sLine.Contains(QuoteFields.StopContactsWrapperStart))
                    {
                        var sContent = sLine.RetrieveWrapperSection(QuoteFields.StopContactsWrapperStart, QuoteFields.StopContactsWrapperEnd);
                        var sLines = stop.Contacts.Any()
                                        ? stop.Contacts
                                            .Select(c => sLine
                                                            .Replace(QuoteFields.StopContact, c.Name)
                                                            .Replace(QuoteFields.StopPhone, c.Phone)
                                                            .Replace(QuoteFields.StopFax, c.Fax)
                                                            .Replace(QuoteFields.StopMobile, c.Mobile)
                                                            .Replace(QuoteFields.StopEmail, c.Email))
                                            .ToArray()
                                        : new string[0];

                        sLine = sLine.Replace(QuoteFields.StopContactsWrapperStart, string.Empty);
                        sLine = sLine.Replace(sContent, string.Join(string.Empty, sLines));
                        sLine = sLine.Replace(QuoteFields.StopContactsWrapperEnd, string.Empty);
                    }
                    lines.Add(sLine);
                }

                template = template.Replace(QuoteFields.StopsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines.ToArray()));
                template = template.Replace(QuoteFields.StopsWrapperEnd, string.Empty);
            }

            // charges
            var hasCharges = quotedLoad.Charges.Any();
            template = template.Replace(QuoteFields.ChargesTotalAmount, hasCharges ? quotedLoad.Charges.Sum(i => i.AmountDue).ToString(d.CurrencyNumber) : string.Empty);
            if (template.Contains(QuoteFields.ChargesWrapperStart))
            {
                var content = template.RetrieveWrapperSection(QuoteFields.ChargesWrapperStart, QuoteFields.ChargesWrapperEnd);
                var lines = quotedLoad.Charges.Any()
                                ? quotedLoad.Charges
                                    .Select(r => content
                                                    .Replace(QuoteFields.ChargesAmountDue, r.AmountDue.ToString(d.Currency))
                                                    .Replace(QuoteFields.ChargesDescription, r.ChargeCode.Description)
                                                    .Replace(QuoteFields.ChargesDescription, r.Comment))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(QuoteFields.ChargesWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(QuoteFields.ChargesWrapperEnd, string.Empty);
            }

            //notes
            if (template.Contains(QuoteFields.NotesWrapperStart))
            {
                var content = template.RetrieveWrapperSection(QuoteFields.NotesWrapperStart, QuoteFields.NotesWrapperEnd);
                var lines = quotedLoad.Notes.Any()
                                ? quotedLoad.Notes
                                    .Where(n => !n.Classified)
                                    .Select(note => content.Replace(QuoteFields.NotesMessage, note.Message))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(QuoteFields.NotesWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(QuoteFields.NotesWrapperEnd, string.Empty);
            }

            //equipment
            if (template.Contains(QuoteFields.QuoteEquipmentWrapperStart))
            {
                var content = template.RetrieveWrapperSection(QuoteFields.QuoteEquipmentWrapperStart, QuoteFields.QuoteEquipmentWrapperEnd);
                var lines = quotedLoad.Equipments.Any()
                                ? quotedLoad.Equipments
                                    .Select(note => content.Replace(QuoteFields.QuoteEquipment, note.EquipmentType.FormattedString()))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(QuoteFields.QuoteEquipmentWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(QuoteFields.QuoteEquipmentWrapperEnd, string.Empty);
            }

            // harzardous materials
            if (template.Contains(QuoteFields.HazardousMaterialWrapperStart))
            {
                var content = template.RetrieveWrapperSection(QuoteFields.HazardousMaterialWrapperStart, QuoteFields.HazardousMaterialWrapperEnd);
                template = template.Replace(QuoteFields.HazardousMaterialWrapperStart, string.Empty);
                if (quotedLoad.HazardousMaterial)
                {
                    template = template.Replace(QuoteFields.HazardousMaterialContact, quotedLoad.HazardousMaterialContactName);
                    template = template.Replace(QuoteFields.HazardousMaterialPhone, quotedLoad.HazardousMaterialContactPhone);
                    template = template.Replace(QuoteFields.HazardousMaterialMobile, quotedLoad.HazardousMaterialContactMobile);
                    template = template.Replace(QuoteFields.HazardousMaterialEmail, quotedLoad.HazardousMaterialContactEmail);
                }
                else
                    template = template.Replace(content, string.Empty);

                template = template.Replace(ShipmentFields.HazardousMaterialWrapperEnd, string.Empty);
            }

            // body wrapper
            if (removeBodyWrapper)
            {
                template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
                template = template.Replace(BaseFields.BodyWrapperEnd, WebApplicationSettings.Brand);
            }

            return template;
        }

        public static string GenerateCarrierAgreement(this MemberPageBase page, Shipment shipment, bool removeBodyWrapper = false)
        {
            // get template
            Decimals d;
            var template = page.Server
                .FetchTemplate(shipment.TenantId, shipment.PrefixId, shipment.ServiceMode, DocumentTemplateCategory.RateConfirmation, out d);

            // logo url
            var logoUrl = shipment.Customer.RetrievePageLogoUrl();
            var siteRoot = page.Request.ResolveSiteRootWithHttp();
            logoUrl = logoUrl.Replace("~", siteRoot);

            // check void
            if (shipment.Status == ShipmentStatus.Void)
            {
                var voidAdded = string.Format("{0}{1}", BaseFields.BodyWrapperStart, string.Format(VoidWaterMark, siteRoot));
                template = template.Replace(BaseFields.BodyWrapperStart, voidAdded);
            }

            // body wrapper
            if (removeBodyWrapper)
            {
                template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
                template = template.Replace(BaseFields.BodyWrapperEnd, WebApplicationSettings.Brand);
            }

            // general details
            var combine = siteRoot + WebApplicationSettings.UploadedImagesFolder(shipment.TenantId).Replace("~", string.Empty);
            template = template.Replace(ShipmentFields.UploadedImages, combine);
            template = template.Replace(VendorRateConfirmationFields.CustomerName, shipment.Customer.Name);
            template = template.Replace(VendorRateConfirmationFields.EarliestPickup, shipment.EarlyPickup);
            template = template.Replace(VendorRateConfirmationFields.LastestPickup, shipment.LatePickup);
            template = template.Replace(VendorRateConfirmationFields.EarliestDelivery, shipment.EarlyDelivery);
            template = template.Replace(VendorRateConfirmationFields.LastestDelivery, shipment.LateDelivery);
            template = template.Replace(VendorRateConfirmationFields.ShipmentNumber, shipment.ShipmentNumber);
            template = template.Replace(VendorRateConfirmationFields.ShipperReference, shipment.ShipperReference);
            template = template.Replace(VendorRateConfirmationFields.Prefix, shipment.Prefix == null ? string.Empty : shipment.Prefix.Code);
            template = template.Replace(VendorRateConfirmationFields.ShipmentDate, shipment.DesiredPickupDate.FormattedShortDate());
            template = template.Replace(VendorRateConfirmationFields.DeliveryDate, shipment.EstimatedDeliveryDate.FormattedShortDate());
            template = template.Replace(VendorRateConfirmationFields.PurchaseOrderNumber, shipment.PurchaseOrderNumber);
            template = template.Replace(VendorRateConfirmationFields.ShipperReference, shipment.ShipperReference);
            template = template.Replace(VendorRateConfirmationFields.LogoUrl, logoUrl);
            template = template.Replace(ShipmentFields.GeneralBOLComments, shipment.GeneralBolComments);
            template = template.Replace(ShipmentFields.CriticalBOLComments, shipment.CriticalBolComments);

            //Is Partial Truckload?
            if (!shipment.IsPartialTruckload && template.Contains(VendorRateConfirmationFields.IsPartialTruckLoadWrapperStart))
            {
                var content = template.RetrieveWrapperSection(VendorRateConfirmationFields.IsPartialTruckLoadWrapperStart, VendorRateConfirmationFields.IsPartialTruckLoadWrapperEnd);
                template = template.Replace(content, string.Empty);
            }
            template = template.Replace(VendorRateConfirmationFields.IsPartialTruckLoadWrapperStart, string.Empty);
            template = template.Replace(VendorRateConfirmationFields.IsPartialTruckLoadWrapperEnd, string.Empty);

            // vendors
            if (template.Contains(VendorRateConfirmationFields.PrimaryVendorName) || template.Contains(ShipmentFields.PrimaryVendorPro))
            {
                var sv = shipment.Vendors.FirstOrDefault(v => v.Primary);
                template = template.Replace(VendorRateConfirmationFields.PrimaryVendorName, sv == null ? string.Empty : sv.Vendor.Name);
                template = template.Replace(VendorRateConfirmationFields.PrimaryVendorPro, sv == null ? string.Empty : sv.ProNumber);
            }

            // user to contact 
            var contactUser = shipment.CarrierCoordinator ?? shipment.User;
            template = template.Replace(VendorRateConfirmationFields.ContactName, string.Format("{0} {1}", contactUser.FirstName, contactUser.LastName));
            template = template.Replace(VendorRateConfirmationFields.ContactPhone, contactUser.Phone);
            template = template.Replace(VendorRateConfirmationFields.ContactTollFree, shipment.Customer.Tier.TollFreeContactNumber);

            // creatd by contact info specific
            template = template.Replace(VendorRateConfirmationFields.CreatedByContactName, string.Format("{0} {1}", shipment.User.FirstName, shipment.User.LastName));
            template = template.Replace(VendorRateConfirmationFields.CreatedByContactPhone, shipment.User.Phone);

            //department info
            var dept = contactUser.Department;
            template = template.Replace(VendorRateConfirmationFields.DepartmentPhone, dept == null ? string.Empty : dept.Phone);
            template = template.Replace(VendorRateConfirmationFields.DepartmentFax, dept == null ? string.Empty : dept.Fax);
            template = template.Replace(VendorRateConfirmationFields.DepartmentEmail, dept == null ? string.Empty : dept.Email);

            //created by user department
            var userDepartment = shipment.User.Department;
            template = template.Replace(ShipmentFields.CreatedByUserDepartmentPhone, userDepartment == null ? string.Empty : userDepartment.Phone);

            //origin
            template = template.Replace(VendorRateConfirmationFields.OriginDescription, shipment.Origin.Description);
            template = template.Replace(VendorRateConfirmationFields.OriginStreet1, shipment.Origin.Street1);
            template = template.Replace(VendorRateConfirmationFields.OriginStreet2, shipment.Origin.Street2);
            template = template.Replace(VendorRateConfirmationFields.OriginCity, shipment.Origin.City);
            template = template.Replace(VendorRateConfirmationFields.OriginState, shipment.Origin.State);
            template = template.Replace(VendorRateConfirmationFields.OriginPostalCode, shipment.Origin.PostalCode);
            template = template.Replace(VendorRateConfirmationFields.OriginCountry, shipment.Origin.Country.Name);
            template = template.Replace(VendorRateConfirmationFields.OriginInstructions, shipment.Origin.SpecialInstructions);
            template = template.Replace(VendorRateConfirmationFields.OriginGeneralInfo, shipment.Origin.GeneralInfo);
            template = template.Replace(VendorRateConfirmationFields.OriginDirections, shipment.Origin.Direction);

            if (template.Contains(VendorRateConfirmationFields.OriginPrimaryContactEnabled))
            {
                var sc = shipment.Origin.Contacts.FirstOrDefault(c => c.Primary) ?? new ShipmentContact();
                template = template.Replace(VendorRateConfirmationFields.OriginPrimaryContactEnabled, string.Empty);
                template = template.Replace(VendorRateConfirmationFields.OriginPrimaryContact, sc.Name);
                template = template.Replace(VendorRateConfirmationFields.OriginPrimaryPhone, sc.Phone);
                template = template.Replace(VendorRateConfirmationFields.OriginPrimaryFax, sc.Fax);
                template = template.Replace(VendorRateConfirmationFields.OriginPrimaryMobile, sc.Mobile);
                template = template.Replace(VendorRateConfirmationFields.OriginPrimaryEmail, sc.Email);
            }
            if (template.Contains(VendorRateConfirmationFields.OriginContactsWrapperStart))
            {
                var content = template.RetrieveWrapperSection(VendorRateConfirmationFields.OriginContactsWrapperStart, VendorRateConfirmationFields.OriginContactsWrapperEnd);
                var lines = shipment.Origin.Contacts.Any()
                                ? shipment.Origin.Contacts
                                    .Select(c => content
                                                    .Replace(VendorRateConfirmationFields.OriginContact, c.Name)
                                                    .Replace(VendorRateConfirmationFields.OriginPhone, c.Phone)
                                                    .Replace(VendorRateConfirmationFields.OriginFax, c.Fax)
                                                    .Replace(VendorRateConfirmationFields.OriginMobile, c.Mobile)
                                                    .Replace(VendorRateConfirmationFields.OriginEmail, c.Email))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(VendorRateConfirmationFields.OriginContactsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(VendorRateConfirmationFields.OriginContactsWrapperEnd, string.Empty);
            }
            if (template.Contains(VendorRateConfirmationFields.OriginReferencesWrapperStart))
            {
                var content = template.RetrieveWrapperSection(VendorRateConfirmationFields.OriginReferencesWrapperStart, VendorRateConfirmationFields.OriginReferencesWrapperEnd);
                var lines = shipment.CustomerReferences.Any(r => r.DisplayOnOrigin)
                                ? shipment.CustomerReferences
                                    .Where(r => r.DisplayOnOrigin)
                                    .Select(r => content
                                                    .Replace(VendorRateConfirmationFields.OriginReferenceName, r.Name)
                                                    .Replace(VendorRateConfirmationFields.OriginReferenceValue, r.Value))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(VendorRateConfirmationFields.OriginReferencesWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(VendorRateConfirmationFields.OriginReferencesWrapperEnd, string.Empty);
            }

            // destination
            template = template.Replace(VendorRateConfirmationFields.DestinationDescription, shipment.Destination.Description);
            template = template.Replace(VendorRateConfirmationFields.DestinationStreet1, shipment.Destination.Street1);
            template = template.Replace(VendorRateConfirmationFields.DestinationStreet2, shipment.Destination.Street2);
            template = template.Replace(VendorRateConfirmationFields.DestinationCity, shipment.Destination.City);
            template = template.Replace(VendorRateConfirmationFields.DestinationState, shipment.Destination.State);
            template = template.Replace(VendorRateConfirmationFields.DestinationPostalCode, shipment.Destination.PostalCode);
            template = template.Replace(VendorRateConfirmationFields.DestinationCountry, shipment.Destination.Country.Name);
            template = template.Replace(VendorRateConfirmationFields.DestinationInstructions, shipment.Destination.SpecialInstructions);
            template = template.Replace(VendorRateConfirmationFields.DestinationGeneralInfo, shipment.Destination.GeneralInfo);
            template = template.Replace(VendorRateConfirmationFields.DestinationDirections, shipment.Destination.Direction);

            if (template.Contains(VendorRateConfirmationFields.DestinationPrimaryContactEnabled))
            {
                var sc = shipment.Destination.Contacts.FirstOrDefault(c => c.Primary) ?? new ShipmentContact();
                template = template.Replace(VendorRateConfirmationFields.DestinationPrimaryContactEnabled, string.Empty);
                template = template.Replace(VendorRateConfirmationFields.DestinationPrimaryContact, sc.Name);
                template = template.Replace(VendorRateConfirmationFields.DestinationPrimaryPhone, sc.Phone);
                template = template.Replace(VendorRateConfirmationFields.DestinationPrimaryFax, sc.Fax);
                template = template.Replace(VendorRateConfirmationFields.DestinationPrimaryMobile, sc.Mobile);
                template = template.Replace(VendorRateConfirmationFields.DestinationPrimaryEmail, sc.Email);
            }
            if (template.Contains(VendorRateConfirmationFields.DestinationContactsWrapperStart))
            {
                var content = template.RetrieveWrapperSection(VendorRateConfirmationFields.DestinationContactsWrapperStart, VendorRateConfirmationFields.DestinationContactsWrapperEnd);
                var lines = shipment.Destination.Contacts.Any()
                                ? shipment.Destination.Contacts
                                    .Select(c => content
                                            .Replace(VendorRateConfirmationFields.DestinationContact, c.Name)
                                            .Replace(VendorRateConfirmationFields.DestinationPhone, c.Phone)
                                            .Replace(VendorRateConfirmationFields.DestinationFax, c.Fax)
                                            .Replace(VendorRateConfirmationFields.DestinationMobile, c.Mobile)
                                            .Replace(VendorRateConfirmationFields.DestinationEmail, c.Email))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(VendorRateConfirmationFields.DestinationContactsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(VendorRateConfirmationFields.DestinationContactsWrapperEnd, string.Empty);
            }
            if (template.Contains(VendorRateConfirmationFields.DestinationReferencesWrapperStart))
            {
                var content = template.RetrieveWrapperSection(VendorRateConfirmationFields.DestinationReferencesWrapperStart, VendorRateConfirmationFields.DestinationReferencesWrapperEnd);
                var lines = shipment.CustomerReferences.Any(r => r.DisplayOnDestination)
                                ? shipment.CustomerReferences
                                    .Where(r => r.DisplayOnDestination)
                                    .Select(r => content
                                            .Replace(VendorRateConfirmationFields.DestinationReferenceName, r.Name)
                                            .Replace(VendorRateConfirmationFields.DestinationReferenceValue, r.Value))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(VendorRateConfirmationFields.DestinationReferencesWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(VendorRateConfirmationFields.DestinationReferencesWrapperEnd, string.Empty);
            }

            // items
            var hasItems = shipment.Items.Any();
            var itemsTotalValue = shipment.Items.Sum(i => i.Value);
            template = template.Replace(VendorRateConfirmationFields.ItemsTotalValue, hasItems ? itemsTotalValue == 0 ? "$" : itemsTotalValue.ToString(d.Currency) : string.Empty);
            template = template.Replace(VendorRateConfirmationFields.ItemsTotalWeight, hasItems ? shipment.Items.Sum(i => i.ActualWeight).ToString(d.Weight) : string.Empty);
            if (template.Contains(VendorRateConfirmationFields.ItemsWrapperStart))
            {
                var lastStop = shipment.Destination.StopOrder;
                var content = template.RetrieveWrapperSection(VendorRateConfirmationFields.ItemsWrapperStart, VendorRateConfirmationFields.ItemsWrapperEnd);
                var lines = hasItems
                                ? shipment.Items.OrderBy(it => it.Pickup).ThenBy(itt => itt.Delivery)
                                    .Select(i => content
                                                    .Replace(VendorRateConfirmationFields.ItemQuantity, i.Quantity.ToString())
                                                    .Replace(VendorRateConfirmationFields.ItemPieceCount, i.PieceCount == 0 ? string.Empty : i.PieceCount.ToString())
                                                    .Replace(VendorRateConfirmationFields.ItemPackageType, i.PackageType.TypeName)
                                                    .Replace(VendorRateConfirmationFields.ItemDescription, i.Description)
                                                    .Replace(VendorRateConfirmationFields.ItemTotalWeight, i.ActualWeight.ToString(d.Weight))
                                                    .Replace(VendorRateConfirmationFields.ItemLength, i.ActualLength.ToString(d.Dimension))
                                                    .Replace(VendorRateConfirmationFields.ItemWidth, i.ActualWidth.ToString(d.Dimension))
                                                    .Replace(VendorRateConfirmationFields.ItemHeight, i.ActualHeight.ToString(d.Dimension))
                                                    .Replace(VendorRateConfirmationFields.ItemFreightClass, i.ActualFreightClass.ToString())
                                                    .Replace(VendorRateConfirmationFields.ItemStackable, i.IsStackable ? "[STACKABLE]" : string.Empty)
                                                    .Replace(VendorRateConfirmationFields.ItemValue, i.Value == 0 ? "$" : i.Value.ToString(d.Currency))
                                                    .Replace(VendorRateConfirmationFields.ItemNMFC, i.NMFCCode)
                                                    .Replace(VendorRateConfirmationFields.ItemHTS, i.HTSCode)
                                                    .Replace(VendorRateConfirmationFields.ItemPickup, i.Pickup.GetStopDescription(lastStop))
                                                    .Replace(VendorRateConfirmationFields.ItemDelivery, i.Delivery.GetStopDescription(lastStop))
                                                    .Replace(VendorRateConfirmationFields.ItemComments, i.Comment)
                                                    .Replace(VendorRateConfirmationFields.ItemHazardous, i.HazardousMaterial ? WebApplicationConstants.HtmlCheck : string.Empty))


                                    .ToArray()
                                : new string[0];

                template = template.Replace(VendorRateConfirmationFields.ItemsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(VendorRateConfirmationFields.ItemsWrapperEnd, string.Empty);
            }

            // charges
            var hasCharges = shipment.Charges.Any();
            template = template.Replace(VendorRateConfirmationFields.ChargesTotalAmount, hasCharges ? shipment.Charges.Where(c => !c.ChargeCode.SurpressOnCarrierRateAgreement).Sum(i => i.FinalBuy).ToString(d.CurrencyNumber) : string.Empty);
            if (template.Contains(VendorRateConfirmationFields.ChargesWrapperStart))
            {
                var content = template.RetrieveWrapperSection(VendorRateConfirmationFields.ChargesWrapperStart, VendorRateConfirmationFields.ChargesWrapperEnd);
                var lines = shipment.Charges.Any()
                                ? shipment.Charges
                                    .Where(c => c.FinalBuy > 0 && !c.ChargeCode.SurpressOnCarrierRateAgreement)
                                    .Select(r => content
                                                    .Replace(VendorRateConfirmationFields.ChargesAmountDue, r.FinalBuy.ToString(d.Currency))
                                                    .Replace(VendorRateConfirmationFields.ChargesDescription, r.ChargeCode.Description)
                                                    .Replace(VendorRateConfirmationFields.ChargesComment, r.Comment))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(VendorRateConfirmationFields.ChargesWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(VendorRateConfirmationFields.ChargesWrapperEnd, string.Empty);
            }

            //Critical notes
            if (template.Contains(VendorRateConfirmationFields.CriticalNotesWrapperStart))
            {
                var content = template.RetrieveWrapperSection(VendorRateConfirmationFields.CriticalNotesWrapperStart, VendorRateConfirmationFields.CriticalNotesWrapperEnd);
                var lines = shipment.Notes.Any()
                                ? shipment.Notes.Where(n => n.Type == NoteType.Critical && !n.Classified)
                                    .Select(note => content.Replace(VendorRateConfirmationFields.CriticalNotesMessage, note.Message))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(VendorRateConfirmationFields.CriticalNotesWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(VendorRateConfirmationFields.CriticalNotesWrapperEnd, string.Empty);
            }

            //General notes
            if (template.Contains(VendorRateConfirmationFields.GeneralNotesWrapperStart))
            {
                var content = template.RetrieveWrapperSection(VendorRateConfirmationFields.GeneralNotesWrapperStart, VendorRateConfirmationFields.GeneralNotesWrapperEnd);
                var lines = shipment.Notes.Any()
                                ? shipment.Notes.Where(n => n.Type == NoteType.Normal && !n.Classified)
                                    .Select(note => content.Replace(VendorRateConfirmationFields.GeneralNotesMessage, note.Message))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(VendorRateConfirmationFields.GeneralNotesWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(VendorRateConfirmationFields.GeneralNotesWrapperEnd, string.Empty);
            }

            //equipment
            if (template.Contains(VendorRateConfirmationFields.EquipmentWrapperStart))
            {
                var content = template.RetrieveWrapperSection(VendorRateConfirmationFields.EquipmentWrapperStart, VendorRateConfirmationFields.EquipmentWrapperEnd);
                var lines = shipment.Equipments.Any()
                                ? shipment.Equipments
                                    .Select(note => content.Replace(VendorRateConfirmationFields.EquipmentType, note.EquipmentType.FormattedString()))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(VendorRateConfirmationFields.EquipmentWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(VendorRateConfirmationFields.EquipmentWrapperEnd, string.Empty);
            }

            // rating override bill to address
            if (!string.IsNullOrEmpty(shipment.VendorRatingOverrideAddress) && template.Contains(VendorRateConfirmationFields.VendorRatingProfileAddressWrapperStart))
            {
                var content = template.RetrieveWrapperSection(VendorRateConfirmationFields.VendorRatingProfileAddressWrapperStart,
                                                              VendorRateConfirmationFields.VendorRatingProfileAddressWrapperEnd);
                template = template.Replace(VendorRateConfirmationFields.VendorRatingProfileAddressWrapperStart, string.Empty);
                template = template.Replace(content, shipment.VendorRatingOverrideAddress.ReplaceNewLineWithHtmlBreak());
                template = template.Replace(VendorRateConfirmationFields.VendorRatingProfileAddressWrapperEnd, string.Empty);
            }
            else
            {
                template = template.Replace(VendorRateConfirmationFields.VendorRatingProfileAddressWrapperStart, string.Empty);
                template = template.Replace(VendorRateConfirmationFields.VendorRatingProfileAddressWrapperEnd, string.Empty);
            }

            // stops
            if (template.Contains(ShipmentFields.StopsWrapperStart) && shipment.Stops.Any())
            {
                var content = template.RetrieveWrapperSection(ShipmentFields.StopsWrapperStart, ShipmentFields.StopsWrapperEnd);
                var lines = new List<string>();
                foreach (var stop in shipment.Stops)
                {
                    var sLine = content;
                    sLine = sLine.Replace(ShipmentFields.StopDescription, stop.Description);
                    sLine = sLine.Replace(ShipmentFields.StopNumber, stop.StopOrder.ToString());
                    sLine = sLine.Replace(ShipmentFields.StopInstructions, stop.SpecialInstructions);
                    sLine = sLine.Replace(ShipmentFields.StopFullAddress, stop.FullAddress);
                    sLine = sLine.Replace(ShipmentFields.StopGeneralInfo, stop.GeneralInfo);
                    sLine = sLine.Replace(ShipmentFields.StopDirections, stop.Direction);
                    sLine = sLine.Replace(ShipmentFields.StopAppointmentDateTime, stop.AppointmentDateTime.FormattedSuppressMidnightDate());

                    if (sLine.Contains(ShipmentFields.StopPrimaryContactEnabled))
                    {
                        var sc = stop.Contacts.FirstOrDefault(c => c.Primary) ?? new ShipmentContact();
                        sLine = sLine.Replace(ShipmentFields.StopPrimaryContactEnabled, string.Empty);
                        sLine = sLine.Replace(ShipmentFields.StopPrimaryContact, sc.Name);
                        sLine = sLine.Replace(ShipmentFields.StopPrimaryPhone, sc.Phone);
                        sLine = sLine.Replace(ShipmentFields.StopPrimaryFax, sc.Fax);
                        sLine = sLine.Replace(ShipmentFields.StopPrimaryMobile, sc.Mobile);
                        sLine = sLine.Replace(ShipmentFields.StopPrimaryEmail, sc.Email);
                    }
                    if (sLine.Contains(ShipmentFields.StopContactsWrapperStart))
                    {
                        var sContent = sLine.RetrieveWrapperSection(ShipmentFields.StopContactsWrapperStart, ShipmentFields.StopContactsWrapperEnd);
                        var sLines = stop.Contacts.Any()
                                        ? stop.Contacts
                                            .Select(c => sLine
                                                            .Replace(ShipmentFields.StopContact, c.Name)
                                                            .Replace(ShipmentFields.StopPhone, c.Phone)
                                                            .Replace(ShipmentFields.StopFax, c.Fax)
                                                            .Replace(ShipmentFields.StopMobile, c.Mobile)
                                                            .Replace(ShipmentFields.StopEmail, c.Email))
                                            .ToArray()
                                        : new string[0];

                        sLine = sLine.Replace(ShipmentFields.StopContactsWrapperStart, string.Empty);
                        sLine = sLine.Replace(sContent, string.Join(string.Empty, sLines));
                        sLine = sLine.Replace(ShipmentFields.StopContactsWrapperEnd, string.Empty);
                    }
                    lines.Add(sLine);
                }
                template = template.Replace(ShipmentFields.StopsOuterWrapperStart, string.Empty);
                template = template.Replace(ShipmentFields.StopsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines.ToArray()));
                template = template.Replace(ShipmentFields.StopsWrapperEnd, string.Empty);
                template = template.Replace(ShipmentFields.StopsOuterWrapperEnd, string.Empty);
            }
            else
            {
                if (template.Contains(ShipmentFields.StopsOuterWrapperStart))
                {
                    var content = template.RetrieveWrapperSection(ShipmentFields.StopsOuterWrapperStart, ShipmentFields.StopsOuterWrapperEnd);
                    template = template.Replace(ShipmentFields.StopsOuterWrapperStart, string.Empty);
                    template = template.Replace(content, string.Empty);
                    template = template.Replace(ShipmentFields.StopsOuterWrapperEnd, string.Empty);
                }

                if (template.Contains(ShipmentFields.StopsWrapperStart))
                {
                    var content = template.RetrieveWrapperSection(ShipmentFields.StopsWrapperStart, ShipmentFields.StopsWrapperEnd);
                    template = template.Replace(ShipmentFields.StopsWrapperStart, string.Empty);
                    template = template.Replace(content, string.Empty);
                    template = template.Replace(ShipmentFields.StopsWrapperEnd, string.Empty);
                }
            }
            return template;
        }

        public static string GenerateAvailableLoadsEmail(this MemberControlBase control, List<LoadOrder> loads)
        {
            //get template
            var template = control.Server.ReadFromFile(SystemTemplates.AvailableLoadsTemplate).FromUtf8Bytes();

            // logo url
            var logoUrl = control.ActiveUser.Tenant.LogoUrl;
            var siteRoot = control.Request.ResolveSiteRootWithHttp();
            logoUrl = logoUrl.Replace("~", siteRoot);

            // body wrapper
            template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
            template = template.Replace(BaseFields.BodyWrapperEnd, string.Empty);

            // details
            template = template.Replace(EmailTemplateFields.SiteRoot, siteRoot);
            template = template.Replace(EmailTemplateFields.LogoUrl, logoUrl);
            template = template.Replace(EmailTemplateFields.Brand, WebApplicationSettings.Brand);

            if (template.Contains(AvailableLoadsFields.AvailableLoadsWrapperStart))
            {
                var content = template.RetrieveWrapperSection(AvailableLoadsFields.AvailableLoadsWrapperStart, AvailableLoadsFields.AvailableLoadsWrapperEnd);
                //test for number of booking requetss here
                var lines = loads.Any()
                                ? loads.Select
                                    (l =>
                                        {
                                            var items = string.Join("<br />",
                                                                    l.Items
                                                                    .Select(i => string.Format("{0}x{1}x{2} {3}", i.Length, i.Width, i.Height, i.Description))
                                                                    .ToArray());
                                            var contact = l.User.TenantEmployee
                                                            ? string.Format("{0}", l.User.Phone)
                                                            : string.Empty;
                                            var origin = l.Origin ?? new LoadOrderLocation();
                                            var destination = l.Destination ?? new LoadOrderLocation();
                                            return content
                                                .Replace(AvailableLoadsFields.LoadNumber, l.LoadOrderNumber)
                                                .Replace(AvailableLoadsFields.DesiredShipmentDate, l.DesiredPickupDate.ToShortDateString())
                                                .Replace(AvailableLoadsFields.OriginCityState, string.Format("{0} {1}", origin.City, origin.State))
                                                .Replace(AvailableLoadsFields.DestinationCityState, string.Format("{0} {1}", destination.City, destination.State))
                                                .Replace(AvailableLoadsFields.DesiredShipmentDate, string.Format("{0} {1}", destination.City, destination.State))
                                                .Replace(AvailableLoadsFields.Dimensions, items)
                                                .Replace(AvailableLoadsFields.Weight, l.Items.Sum(item => item.Weight).ToString())
                                                .Replace(AvailableLoadsFields.Type, l.Equipments.Select(e => e.EquipmentType.FormattedString()).ToArray().CommaJoin())
                                                .Replace(AvailableLoadsFields.Comments, l.Description)
                                                .Replace(AvailableLoadsFields.ContactPhone, contact);
                                        }).ToArray()
                                : new string[0];
                template = template.Replace(AvailableLoadsFields.AvailableLoadsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(AvailableLoadsFields.AvailableLoadsWrapperEnd, string.Empty);
            }
            return template;
        }

        public static string GenerateShipmentRouteSummary(this MemberPageBase page, Shipment shipment, bool removeBodyWrapper = false)
        {
            //get template
            var template = page.Server.ReadFromFile(SystemTemplates.ShipmentRouteSummaryTemplate).FromUtf8Bytes();

            // logo url
            var customer = shipment.Customer;
            var logoUrl = customer == null ? string.Empty : customer.RetrievePageLogoUrl();
            var siteRoot = page.Request.ResolveSiteRootWithHttp();
            logoUrl = logoUrl.Replace("~", siteRoot);

            // body wrapper
            if (removeBodyWrapper)
            {
                template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
                template = template.Replace(BaseFields.BodyWrapperEnd, WebApplicationSettings.Brand);
            }

            //origin details
            var originContact = shipment.Origin.Contacts.FirstOrDefault(c => c.Primary);
            template = template.Replace(ShipmentFields.Prefix, shipment.Prefix == null ? string.Empty : shipment.Prefix.Code);
            template = template.Replace(ShipmentFields.ShipmentNumber, shipment.ShipmentNumber);
            template = template.Replace(ShipmentFields.OriginPrimaryContact, originContact == null ? string.Empty : originContact.Name);
            template = template.Replace(ShipmentFields.OriginPrimaryPhone, originContact == null ? string.Empty : originContact.Phone);
            template = template.Replace(ShipmentFields.OriginDescription, shipment.Origin.Description);
            template = template.Replace(ShipmentFields.OriginStreet1, shipment.Origin.Street1);
            template = template.Replace(ShipmentFields.OriginStreet2, shipment.Origin.Street2);
            template = template.Replace(ShipmentFields.OriginCity, shipment.Origin.City);
            template = template.Replace(ShipmentFields.OriginState, shipment.Origin.State);
            template = template.Replace(ShipmentFields.OriginPostalCode, shipment.Origin.PostalCode);
            template = template.Replace(ShipmentFields.OriginCountry, shipment.Origin.Country.Name);
            template = template.Replace(EmailTemplateFields.SiteRoot, siteRoot);
            template = template.Replace(EmailTemplateFields.LogoUrl, logoUrl);

            template = template.Replace(ShipmentFields.TotalMiles, shipment.Mileage > 0 ? shipment.Mileage.ToString("n2") : string.Empty);

            var primaryVendor = shipment.Vendors.Any(v => v.Primary)
                                    ? shipment.Vendors.First(v => v.Primary).Vendor
                                    : new Vendor();
            template = template.Replace(ShipmentFields.PrimaryVendorName, string.Format("{0} - {1}", primaryVendor.Scac, primaryVendor.Name));
            template = template.Replace(ShipmentFields.SecondaryVendors, shipment.Vendors.Where(v => !v.Primary).Select(v => string.Format("{0} - {1}", v.Vendor.Scac, v.Vendor.Name)).ToArray().CommaJoin());

            //origin items
            var originItems = shipment.Items.Where(item => item.Pickup == default(int)).ToList();
            var hasItems = originItems.Any();
            var originItemsTotalWeight = originItems.Sum(i => i.ActualWeight);
            template = template.Replace(ShipmentFields.OriginItemsTotalWeight, hasItems ? originItemsTotalWeight == 0 ? string.Empty : originItemsTotalWeight.ToString("n0") : string.Empty);

            if (template.Contains(ShipmentFields.OriginItemsWrapperStart))
            {
                var content = template.RetrieveWrapperSection(ShipmentFields.OriginItemsWrapperStart, ShipmentFields.OriginItemsWrapperEnd);
                var lines = hasItems
                                ? originItems
                                    .Select(i => content
                                                    .Replace(ShipmentFields.OriginItemQuantity, i.Quantity.ToString())
                                                    .Replace(ShipmentFields.OriginItemPackageType, i.PackageType.TypeName)
                                                    .Replace(ShipmentFields.OriginItemDescription, i.Description)
                                                    .Replace(ShipmentFields.OriginItemTotalWeight, i.ActualWeight.ToString("n0"))
                                                    .Replace(ShipmentFields.OriginItemLength, i.ActualLength == default(decimal) ? string.Empty : i.ActualLength.ToString("n0"))
                                                    .Replace(ShipmentFields.OriginItemWidth, i.ActualWidth == default(decimal) ? string.Empty : i.ActualWidth.ToString("n0"))
                                                    .Replace(ShipmentFields.OriginItemHeight, i.ActualHeight == default(decimal) ? string.Empty : i.ActualHeight.ToString("n0"))
                                                    .Replace(ShipmentFields.OriginItemPickup, WebApplicationConstants.X))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(ShipmentFields.OriginItemsWrapperStart, string.Empty);

                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(ShipmentFields.OriginItemsWrapperEnd, string.Empty);
            }

            //destination details
            var destinationContact = shipment.Destination.Contacts.FirstOrDefault(c => c.Primary);
            template = template.Replace(ShipmentFields.DestinationPrimaryContact, destinationContact == null ? string.Empty : destinationContact.Name);
            template = template.Replace(ShipmentFields.DestinationPrimaryPhone, destinationContact == null ? string.Empty : destinationContact.Phone);
            template = template.Replace(ShipmentFields.DestinationDescription, shipment.Destination.Description);
            template = template.Replace(ShipmentFields.DestinationStreet1, shipment.Destination.Street1);
            template = template.Replace(ShipmentFields.DestinationStreet2, shipment.Destination.Street2);
            template = template.Replace(ShipmentFields.DestinationCity, shipment.Destination.City);
            template = template.Replace(ShipmentFields.DestinationState, shipment.Destination.State);
            template = template.Replace(ShipmentFields.DestinationPostalCode, shipment.Destination.PostalCode);
            template = template.Replace(ShipmentFields.DestinationCountry, shipment.Destination.Country.Name);
            template = template.Replace(ShipmentFields.DestinationMilesFromPreceedingStop, shipment.Destination.MilesFromPreceedingStop > 0 ? shipment.Destination.MilesFromPreceedingStop.ToString("n2") : string.Empty);

            //destination items
            var destinationItems = shipment.Items.Where(item => item.Delivery == shipment.Stops.Count + 1);
            var destinationhasItems = destinationItems.Any();
            var destinationItemsTotalWeight = destinationItems.Sum(i => i.ActualWeight);
            template = template.Replace(ShipmentFields.DestinationItemsTotalWeight, destinationhasItems ? destinationItemsTotalWeight == 0 ? string.Empty : destinationItemsTotalWeight.ToString("n0") : string.Empty);
            if (template.Contains(ShipmentFields.DestinationItemsWrapperStart))
            {
                var content = template.RetrieveWrapperSection(ShipmentFields.DestinationItemsWrapperStart, ShipmentFields.DestinationItemsWrapperEnd);
                var lines = hasItems
                                ? destinationItems
                                    .Select(i => content
                                                    .Replace(ShipmentFields.DestinationItemQuantity, i.Quantity.ToString())
                                                    .Replace(ShipmentFields.DestinationItemPackageType, i.PackageType.TypeName)
                                                    .Replace(ShipmentFields.DestinationItemDescription, i.Description)
                                                    .Replace(ShipmentFields.DestinationItemTotalWeight, i.ActualWeight.ToString("n0"))
                                                    .Replace(ShipmentFields.DestinationItemLength, i.ActualLength == default(decimal) ? string.Empty : i.ActualLength.ToString("n0"))
                                                    .Replace(ShipmentFields.DestinationItemWidth, i.ActualWidth == default(decimal) ? string.Empty : i.ActualWidth.ToString("n0"))
                                                    .Replace(ShipmentFields.DestinationItemHeight, i.ActualHeight == default(decimal) ? string.Empty : i.ActualHeight.ToString("n0"))
                                                    .Replace(ShipmentFields.DestinationItemDelivery, WebApplicationConstants.X))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(ShipmentFields.DestinationItemsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(ShipmentFields.DestinationItemsWrapperEnd, string.Empty);
            }

            var primeStopsContent = template.RetrieveWrapperSection(ShipmentFields.StopsWrapperStart, ShipmentFields.StopsWrapperEnd);
            if (shipment.Stops.Any())
            {
                var stops = shipment.Stops.OrderBy(s => s.StopOrder);
                var finishedStopsContent = new List<string>();

                foreach (var stop in stops)
                {
                    var stopContent = primeStopsContent;
                    var stopConact = stop.Contacts.FirstOrDefault(c => c.Primary);
                    stopContent = stopContent.Replace(ShipmentFields.StopNumber, stop.StopOrder.ToString());
                    stopContent = stopContent.Replace(ShipmentFields.StopPrimaryContact, stopConact == null ? string.Empty : stopConact.Name);
                    stopContent = stopContent.Replace(ShipmentFields.StopPrimaryPhone, stopConact == null ? string.Empty : stopConact.Phone);
                    stopContent = stopContent.Replace(ShipmentFields.StopDescription, stop.Description);
                    stopContent = stopContent.Replace(ShipmentFields.StopStreet1, stop.Street1);
                    stopContent = stopContent.Replace(ShipmentFields.StopStreet2, stop.Street2);
                    stopContent = stopContent.Replace(ShipmentFields.StopCity, stop.City);
                    stopContent = stopContent.Replace(ShipmentFields.StopState, stop.State);
                    stopContent = stopContent.Replace(ShipmentFields.StopPostalCode, stop.PostalCode);
                    stopContent = stopContent.Replace(ShipmentFields.StopCountry, stop.Country.Name);
                    stopContent = stopContent.Replace(ShipmentFields.MilesFromPreceedingStop, stop.MilesFromPreceedingStop > 0 ? stop.MilesFromPreceedingStop.ToString("n2") : string.Empty);

                    var stopsItemsContent = stopContent.RetrieveWrapperSection(ShipmentFields.ItemsWrapperStart, ShipmentFields.ItemsWrapperEnd);
                    var items = shipment.Items.Where(i => i.Pickup == stop.StopOrder || i.Delivery == stop.StopOrder);
                    var stopHasItems = items.Any();
                    var itemsTotalWeight = items.Sum(i => i.ActualWeight);
                    stopContent = stopContent.Replace(ShipmentFields.ItemsTotalWeight, stopHasItems ? itemsTotalWeight.ToString("n2") : string.Empty);
                    var lines = items.Any()
                        ?
                            items.Select(i => stopsItemsContent
                                      .Replace(ShipmentFields.ItemQuantity, i.Quantity.ToString())
                                      .Replace(ShipmentFields.ItemPackageType, i.PackageType.TypeName)
                                      .Replace(ShipmentFields.ItemDescription, i.Description)
                                      .Replace(ShipmentFields.ItemTotalWeight, i.ActualWeight.ToString("n0"))
                                      .Replace(ShipmentFields.ItemLength, i.ActualLength == default(decimal) ? string.Empty : i.ActualLength.ToString("n0"))
                                      .Replace(ShipmentFields.ItemWidth, i.ActualWidth == default(decimal) ? string.Empty : i.ActualWidth.ToString("n0"))
                                      .Replace(ShipmentFields.ItemHeight, i.ActualHeight == default(decimal) ? string.Empty : i.ActualHeight.ToString("n0"))
                                      .Replace(ShipmentFields.ItemPickup, i.Pickup == stop.StopOrder ? WebApplicationConstants.X : string.Empty)
                                      .Replace(ShipmentFields.ItemDelivery, i.Delivery == stop.StopOrder ? WebApplicationConstants.X : string.Empty))
                    .ToArray()
                    : new string[0];

                    stopContent = stopContent.Replace(stopsItemsContent, string.Join(string.Empty, lines));

                    stopContent = stopContent.Replace(ShipmentFields.ItemsWrapperStart, string.Empty);
                    stopContent = stopContent.Replace(ShipmentFields.ItemsWrapperEnd, string.Empty);
                    finishedStopsContent.Add(stopContent);
                }
                template = template.Replace(ShipmentFields.StopsWrapperStart, string.Empty);
                template = template.Replace(primeStopsContent, string.Join(string.Empty, finishedStopsContent.ToArray()));
                template = template.Replace(ShipmentFields.StopsWrapperEnd, string.Empty);
            }
            if (!shipment.Stops.Any())
            {
                template = template.Replace(ShipmentFields.StopsWrapperStart, string.Empty);
                template = template.Replace(primeStopsContent, string.Empty);
                template = template.Replace(ShipmentFields.StopsWrapperEnd, string.Empty);
            }


            return template;
        }

        public static string GenerateLoadOrderDetails(this MemberPageBase page, LoadOrder load, bool removeBodyWrapper = false)
        {
            //get template
            Decimals d;
            var template = page.Server
                .FetchTemplate(load.TenantId, default(long), ServiceMode.NotApplicable, DocumentTemplateCategory.LoadOrder, out d);

            // body wrapper
            if (removeBodyWrapper)
            {
                template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
                template = template.Replace(BaseFields.BodyWrapperEnd, WebApplicationSettings.Brand);
            }

            // logo url
            var logoUrl = load.Customer.RetrievePageLogoUrl();
            var siteRoot = page.Request.ResolveSiteRootWithHttp();
            logoUrl = logoUrl.Replace("~", siteRoot);
            template = template.Replace(BookingRequestFields.LogoUrl, logoUrl);


            // bill to
            var billTo = load.Customer.Locations.FirstOrDefault(v => v.BillToLocation && v.MainBillToLocation);
            if (billTo != null)
            {
                template = template.Replace(BookingRequestFields.BillToDescription, billTo.Customer.Name);
                template = template.Replace(BookingRequestFields.BillToStreet1, billTo.Street1);
                template = template.Replace(BookingRequestFields.BillToStreet2, billTo.Street2);
                template = template.Replace(BookingRequestFields.BillToCity, billTo.City);
                template = template.Replace(BookingRequestFields.BillToState, billTo.State);
                template = template.Replace(BookingRequestFields.BillToPostalCode, billTo.PostalCode);
                template = template.Replace(BookingRequestFields.BillToCountry, billTo.Country.Name);
            }
            else
            {
                template = template.Replace(BookingRequestFields.BillToDescription, WebApplicationConstants.NoBillToAddressOnFile);
                template = template.Replace(BookingRequestFields.BillToStreet1, string.Empty);
                template = template.Replace(BookingRequestFields.BillToStreet2, string.Empty);
                template = template.Replace(BookingRequestFields.BillToCity, string.Empty);
                template = template.Replace(BookingRequestFields.BillToState, string.Empty);
                template = template.Replace(BookingRequestFields.BillToPostalCode, string.Empty);
                template = template.Replace(BookingRequestFields.BillToCountry, string.Empty);
            }

            //equipment
            if (template.Contains(BookingRequestFields.EquipmentWrapperStart))
            {
                var content = template.RetrieveWrapperSection(BookingRequestFields.EquipmentWrapperStart, BookingRequestFields.EquipmentWrapperEnd);
                var lines = load.Equipments.Any()
                                ? load.Equipments
                                    .Select(note => content.Replace(BookingRequestFields.Equipment, note.EquipmentType.FormattedString()))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(BookingRequestFields.EquipmentWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(BookingRequestFields.EquipmentWrapperEnd, string.Empty);
            }

            // user to contact 
            template = template.Replace(ShipmentFields.ContactName, string.Format("{0} {1}", load.User.FirstName, load.User.LastName));
            template = template.Replace(ShipmentFields.ContactPhone, load.User.Phone);
            template = template.Replace(ShipmentFields.ContactTollFree, load.Customer.Tier.TollFreeContactNumber);

            //department info
            var dept = load.User.Department;
            template = template.Replace(ShipmentFields.DepartmentPhone, dept == null ? string.Empty : dept.Phone);
            template = template.Replace(ShipmentFields.DepartmentFax, dept == null ? string.Empty : dept.Fax);
            template = template.Replace(ShipmentFields.DepartmentEmail, dept == null ? string.Empty : dept.Email);

            // general details
            template = template.Replace(BookingRequestFields.BookingRequestNumber, load.LoadOrderNumber);
            template = template.Replace(BookingRequestFields.BookingRequestDate, load.DateCreated.FormattedShortDate());
            template = template.Replace(BookingRequestFields.DesiredPickupDate, load.DesiredPickupDate.FormattedShortDate());
            template = template.Replace(BookingRequestFields.EarliestPickup, load.EarlyPickup);
            template = template.Replace(BookingRequestFields.LatestPickup, load.LatePickup);
            template = template.Replace(BookingRequestFields.DesiredDeliveryDate, load.EstimatedDeliveryDate.FormattedShortDate());
            template = template.Replace(BookingRequestFields.EarliestDelivery, load.EarlyDelivery);
            template = template.Replace(BookingRequestFields.LatestDelivery, load.LateDelivery);
            template = template.Replace(BookingRequestFields.UserName, load.User.Username);
            template = template.Replace(BookingRequestFields.UserPhone, load.User.Phone);
            template = template.Replace(BookingRequestFields.UserEmail, load.User.Email);
            template = template.Replace(BookingRequestFields.PurchaseOrderNumber, load.PurchaseOrderNumber);


            //origin
            var origin = load.Origin ?? new LoadOrderLocation();
            template = template.Replace(BookingRequestFields.OriginDescription, origin.Description);
            template = template.Replace(BookingRequestFields.OriginStreet1, origin.Street1);
            template = template.Replace(BookingRequestFields.OriginStreet2, origin.Street2);
            template = template.Replace(BookingRequestFields.OriginCity, origin.City);
            template = template.Replace(BookingRequestFields.OriginState, origin.State);
            template = template.Replace(BookingRequestFields.OriginPostalCode, origin.PostalCode);
            template = template.Replace(BookingRequestFields.OriginCountry, origin.Country.Name);
            template = template.Replace(BookingRequestFields.OriginInstructions, origin.SpecialInstructions);

            if (template.Contains(BookingRequestFields.OriginPrimaryContactEnabled))
            {
                var sc = origin.Contacts.FirstOrDefault(c => c.Primary) ?? new LoadOrderContact();
                template = template.Replace(BookingRequestFields.OriginPrimaryContactEnabled, string.Empty);
                template = template.Replace(BookingRequestFields.OriginPrimaryContact, sc.Name);
                template = template.Replace(BookingRequestFields.OriginPrimaryPhone, sc.Phone);
                template = template.Replace(BookingRequestFields.OriginPrimaryFax, sc.Fax);
                template = template.Replace(BookingRequestFields.OriginPrimaryMobile, sc.Mobile);
                template = template.Replace(BookingRequestFields.OriginPrimaryEmail, sc.Email);
            }
            if (template.Contains(BookingRequestFields.OriginReferencesWrapperStart))
            {
                var content = template.RetrieveWrapperSection(BookingRequestFields.OriginReferencesWrapperStart, BookingRequestFields.OriginReferencesWrapperEnd);
                var lines = load.CustomerReferences.Any(r => r.DisplayOnOrigin)
                                ? load.CustomerReferences
                                    .Where(r => r.DisplayOnOrigin)
                                    .Select(r => content
                                                    .Replace(BookingRequestFields.OriginReferenceName, r.Name)
                                                    .Replace(BookingRequestFields.OriginReferenceValue, r.Value))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(BookingRequestFields.OriginReferencesWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(BookingRequestFields.OriginReferencesWrapperEnd, string.Empty);
            }

            // destination
            var destination = load.Destination ?? new LoadOrderLocation();
            template = template.Replace(BookingRequestFields.DestinationDescription, destination.Description);
            template = template.Replace(BookingRequestFields.DestinationStreet1, destination.Street1);
            template = template.Replace(BookingRequestFields.DestinationStreet2, destination.Street2);
            template = template.Replace(BookingRequestFields.DestinationCity, destination.City);
            template = template.Replace(BookingRequestFields.DestinationState, destination.State);
            template = template.Replace(BookingRequestFields.DestinationPostalCode, destination.PostalCode);
            template = template.Replace(BookingRequestFields.DestinationCountry, destination.Country.Name);
            template = template.Replace(BookingRequestFields.DestinationInstructions, destination.SpecialInstructions);

            if (template.Contains(BookingRequestFields.DestinationPrimaryContactEnabled))
            {
                var sc = destination.Contacts.FirstOrDefault(c => c.Primary) ?? new LoadOrderContact();
                template = template.Replace(BookingRequestFields.DestinationPrimaryContactEnabled, string.Empty);
                template = template.Replace(BookingRequestFields.DestinationPrimaryContact, sc.Name);
                template = template.Replace(BookingRequestFields.DestinationPrimaryPhone, sc.Phone);
                template = template.Replace(BookingRequestFields.DestinationPrimaryFax, sc.Fax);
                template = template.Replace(BookingRequestFields.DestinationPrimaryMobile, sc.Mobile);
                template = template.Replace(BookingRequestFields.DestinationPrimaryEmail, sc.Email);
            }
            if (template.Contains(BookingRequestFields.DestinationReferencesWrapperStart))
            {
                var content = template.RetrieveWrapperSection(BookingRequestFields.DestinationReferencesWrapperStart, BookingRequestFields.DestinationReferencesWrapperEnd);
                var lines = load.CustomerReferences.Any(r => r.DisplayOnDestination)
                                ? load.CustomerReferences
                                    .Where(r => r.DisplayOnDestination)
                                    .Select(r => content
                                            .Replace(BookingRequestFields.DestinationReferenceName, r.Name)
                                            .Replace(BookingRequestFields.DestinationReferenceValue, r.Value))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(BookingRequestFields.DestinationReferencesWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(BookingRequestFields.DestinationReferencesWrapperEnd, string.Empty);
            }

            // items
            var hasItems = load.Items.Any();
            var itemsTotalValue = load.Items.Sum(i => i.Value);
            template = template.Replace(BookingRequestFields.ItemsTotalValue, hasItems ? itemsTotalValue == 0 ? "$" : itemsTotalValue.ToString("n2") : string.Empty);
            template = template.Replace(BookingRequestFields.ItemsTotalWeight, hasItems ? load.Items.Sum(i => i.Weight).ToString("n0") : string.Empty);
            if (template.Contains(BookingRequestFields.ItemsWrapperStart))
            {
                var lastStop = destination.StopOrder;
                var content = template.RetrieveWrapperSection(BookingRequestFields.ItemsWrapperStart, BookingRequestFields.ItemsWrapperEnd);
                var lines = hasItems
                                ? load.Items
                                    .Select(i => content
                                                    .Replace(BookingRequestFields.ItemQuantity, i.Quantity.ToString())
                                                    .Replace(BookingRequestFields.ItemPieceCount, i.PieceCount == 0 ? string.Empty : i.PieceCount.ToString())
                                                    .Replace(BookingRequestFields.ItemPackageType, i.PackageType.TypeName)
                                                    .Replace(BookingRequestFields.ItemDescription, i.Description)
                                                    .Replace(BookingRequestFields.ItemTotalWeight, i.Weight.ToString("n0"))
                                                    .Replace(BookingRequestFields.ItemLength, i.Length.ToString("n0"))
                                                    .Replace(BookingRequestFields.ItemWidth, i.Width.ToString("n0"))
                                                    .Replace(BookingRequestFields.ItemHeight, i.Height.ToString("n0"))
                                                    .Replace(BookingRequestFields.ItemFreightClass, i.FreightClass.ToString())
                                                    .Replace(BookingRequestFields.ItemStackable, i.IsStackable ? "[STACKABLE]" : string.Empty)
                                                    .Replace(BookingRequestFields.ItemValue, i.Value == 0 ? "$" : i.Value.ToString("n2"))
                                                    .Replace(BookingRequestFields.ItemNMFC, i.NMFCCode)
                                                    .Replace(BookingRequestFields.ItemHTS, i.HTSCode)
                                                    .Replace(BookingRequestFields.ItemPickup, i.Pickup.GetStopDescription(lastStop))
                                                    .Replace(BookingRequestFields.ItemDelivery, i.Delivery.GetStopDescription(lastStop))
                                                    .Replace(BookingRequestFields.ItemComments, i.Comment)
                                                    .Replace(BookingRequestFields.ItemHazardous, i.HazardousMaterial ? WebApplicationConstants.HtmlCheck : string.Empty))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(BookingRequestFields.ItemsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(BookingRequestFields.ItemsWrapperEnd, string.Empty);
            }

            // accessorials
            if (template.Contains(BookingRequestFields.AccessorialsWrapperStart))
            {
                var content = template.RetrieveWrapperSection(BookingRequestFields.AccessorialsWrapperStart, BookingRequestFields.AccessorialsWrapperEnd);
                var lines = load.Services.Any()
                                ? load.Services
                                    .Select(s => content
                                                    .Replace(BookingRequestFields.AccessorialsCode, s.Service.Code)
                                                    .Replace(BookingRequestFields.AccessorialsDescription, s.Service.Description))
                                    .ToArray()
                                : new string[0];

                template = template.Replace(BookingRequestFields.AccessorialsWrapperStart, string.Empty);
                template = template.Replace(content, string.Join(string.Empty, lines));
                template = template.Replace(BookingRequestFields.AccessorialsWrapperEnd, string.Empty);
            }

            return template;
        }


        public static string GenerateShipmentDetails(this MemberPageBase page, Shipment shipment, bool removeBodyWrapper = false)
        {
            // get template
            var d = new Decimals(null);
            var template = page.Server.ReadFromFile(SystemTemplates.ShipmentDetailsTemplate).FromUtf8Bytes();

            // logo url
            var siteRoot = page.Request.ResolveSiteRootWithHttp();
            var logoUrl = shipment.Customer.RetrievePageLogoUrl();
            logoUrl = logoUrl.Replace("~", siteRoot);

            // body wrapper
            if (removeBodyWrapper)
            {
                template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
                template = template.Replace(BaseFields.BodyWrapperEnd, WebApplicationSettings.Brand);
            }

            // general details
            template = template.Replace(ShipmentFields.ShipmentNumber, shipment.ShipmentNumber);
            template = template.Replace(ShipmentFields.Prefix, shipment.HidePrefix || shipment.Prefix == null ? string.Empty : shipment.Prefix.Code);
            template = template.Replace(ShipmentFields.ShipmentDate, shipment.DesiredPickupDate.FormattedShortDate());
            template = template.Replace(ShipmentFields.EarliestPickup, shipment.EarlyPickup);
            template = template.Replace(ShipmentFields.LastestPickup, shipment.LatePickup);
            template = template.Replace(ShipmentFields.PurchaseOrder, shipment.PurchaseOrderNumber);
            template = template.Replace(ShipmentFields.ShipperReference, shipment.ShipperReference);
            template = template.Replace(ShipmentFields.LogoUrl, logoUrl);
            template = template.Replace(ShipmentFields.TenantAutoRatingNotice, shipment.Tenant.AutoRatingNoticeText.GetString());

            // vendors
            var sv = shipment.Vendors.FirstOrDefault(v => v.Primary);
            template = template.Replace(ShipmentFields.PrimaryVendorName, sv == null ? string.Empty : sv.Vendor.Name);
            template = template.Replace(ShipmentFields.PrimaryVendorPro, sv == null ? string.Empty : sv.ProNumber);

            //origin
            template = template.Replace(ShipmentFields.OriginDescription, shipment.Origin.Description);
            template = template.Replace(ShipmentFields.OriginStreet1, shipment.Origin.Street1);
            template = template.Replace(ShipmentFields.OriginStreet2, shipment.Origin.Street2);
            template = template.Replace(ShipmentFields.OriginCity, shipment.Origin.City);
            template = template.Replace(ShipmentFields.OriginState, shipment.Origin.State);
            template = template.Replace(ShipmentFields.OriginPostalCode, shipment.Origin.PostalCode);
            template = template.Replace(ShipmentFields.OriginCountry, shipment.Origin.Country.Name);
            template = template.Replace(ShipmentFields.OriginInstructions, shipment.Origin.SpecialInstructions);

            var primaryOriginContact = shipment.Origin.Contacts.FirstOrDefault(c => c.Primary) ?? new ShipmentContact();
            template = template.Replace(ShipmentFields.OriginPrimaryContact, primaryOriginContact.Name);
            template = template.Replace(ShipmentFields.OriginPrimaryPhone, primaryOriginContact.Phone);
            template = template.Replace(ShipmentFields.OriginPrimaryFax, primaryOriginContact.Fax);
            template = template.Replace(ShipmentFields.OriginPrimaryMobile, primaryOriginContact.Mobile);
            template = template.Replace(ShipmentFields.OriginPrimaryEmail, primaryOriginContact.Email);

            // destination
            template = template.Replace(ShipmentFields.DestinationDescription, shipment.Destination.Description);
            template = template.Replace(ShipmentFields.DestinationStreet1, shipment.Destination.Street1);
            template = template.Replace(ShipmentFields.DestinationStreet2, shipment.Destination.Street2);
            template = template.Replace(ShipmentFields.DestinationCity, shipment.Destination.City);
            template = template.Replace(ShipmentFields.DestinationState, shipment.Destination.State);
            template = template.Replace(ShipmentFields.DestinationPostalCode, shipment.Destination.PostalCode);
            template = template.Replace(ShipmentFields.DestinationCountry, shipment.Destination.Country.Name);
            template = template.Replace(ShipmentFields.DestinationInstructions, shipment.Destination.SpecialInstructions);

            var destinationPrimaryContact = shipment.Destination.Contacts.FirstOrDefault(c => c.Primary) ?? new ShipmentContact();
            template = template.Replace(ShipmentFields.DestinationPrimaryContact, destinationPrimaryContact.Name);
            template = template.Replace(ShipmentFields.DestinationPrimaryPhone, destinationPrimaryContact.Phone);
            template = template.Replace(ShipmentFields.DestinationPrimaryFax, destinationPrimaryContact.Fax);
            template = template.Replace(ShipmentFields.DestinationPrimaryMobile, destinationPrimaryContact.Mobile);
            template = template.Replace(ShipmentFields.DestinationPrimaryEmail, destinationPrimaryContact.Email);

            // items
            var hasItems = shipment.Items.Any();
            template = template.Replace(ShipmentFields.ItemsTotalWeight, hasItems ? shipment.Items.Sum(i => i.ActualWeight).ToString(d.Weight) : string.Empty);

            var itemWrapperSection = template.RetrieveWrapperSection(ShipmentFields.ItemsWrapperStart, ShipmentFields.ItemsWrapperEnd);
            var itemLines = hasItems
                            ? shipment.Items.OrderBy(i => i.Pickup).ThenBy(itt => itt.Delivery)
                                .Select(i => itemWrapperSection
                                                .Replace(ShipmentFields.ItemQuantity, i.Quantity.ToString())
                                                .Replace(ShipmentFields.ItemPieceCount, i.PieceCount == 0 ? string.Empty : i.PieceCount.ToString())
                                                .Replace(ShipmentFields.PiecesAbbreviation, i.PieceCount == 0 ? string.Empty : "Pcs")
                                                .Replace(ShipmentFields.ItemPackageType, i.PackageType.TypeName)
                                                .Replace(ShipmentFields.ItemDescription, i.Description)
                                                .Replace(ShipmentFields.ItemTotalWeight, i.ActualWeight.ToString(d.Weight))
                                                .Replace(ShipmentFields.ItemLength, i.ActualLength.ToString(d.Dimension))
                                                .Replace(ShipmentFields.ItemWidth, i.ActualWidth.ToString(d.Dimension))
                                                .Replace(ShipmentFields.ItemHeight, i.ActualHeight.ToString(d.Dimension))
                                                .Replace(ShipmentFields.ItemFreightClass, i.ActualFreightClass.ToString())
                                                .Replace(ShipmentFields.ItemStackable, i.IsStackable ? "[STACKABLE]" : string.Empty)
                                                .Replace(ShipmentFields.ItemValue, i.Value == 0 ? "$" : i.Value.ToString(d.Currency))
                                                .Replace(ShipmentFields.ItemNMFC, i.NMFCCode)
                                                .Replace(ShipmentFields.ItemHTS, i.HTSCode)
                                                .Replace(ShipmentFields.ItemHazardous, i.HazardousMaterial ? WebApplicationConstants.HtmlCheck : string.Empty)
                                                .Replace(ShipmentFields.ItemComments, i.Comment))
                                .ToArray()
                            : new string[0];

            template = template.Replace(ShipmentFields.ItemsWrapperStart, string.Empty);
            template = template.Replace(itemWrapperSection, string.Join(string.Empty, itemLines));
            template = template.Replace(ShipmentFields.ItemsWrapperEnd, string.Empty);

            // accessorials
            var accessorialsWrapperSection = template.RetrieveWrapperSection(ShipmentFields.AccessorialsWrapperStart, ShipmentFields.AccessorialsWrapperEnd);
            var accessorialLines = shipment.Services.Any()
                            ? shipment.Services
                                .Select(s => accessorialsWrapperSection.Replace(ShipmentFields.AccessorialsDescription, s.Service.Description))
                                .ToArray()
                            : new string[0];

            template = template.Replace(ShipmentFields.AccessorialsWrapperStart, string.Empty);
            template = template.Replace(accessorialsWrapperSection, string.Join(", ", accessorialLines));
            template = template.Replace(ShipmentFields.AccessorialsWrapperEnd, string.Empty);


            // charges
            var hasCharges = shipment.Charges.Any();
            template = template.Replace(VendorRateConfirmationFields.ChargesTotalAmount, hasCharges ? shipment.Charges.Sum(i => i.AmountDue).ToString(d.CurrencyNumber) : string.Empty);
            var chargesWrapperSection = template.RetrieveWrapperSection(VendorRateConfirmationFields.ChargesWrapperStart, VendorRateConfirmationFields.ChargesWrapperEnd);
            var chargesLines = shipment.Charges.Any()
                            ? shipment.Charges
                                .Select(r => chargesWrapperSection
                                                .Replace(VendorRateConfirmationFields.ChargesAmountDue, r.AmountDue.ToString(d.Currency))
                                                .Replace(VendorRateConfirmationFields.ChargesDescription, r.ChargeCode.Description)
                                                .Replace(VendorRateConfirmationFields.ChargesQuantity, r.Quantity.ToString()))
                                .ToArray()
                            : new string[0];

            template = template.Replace(VendorRateConfirmationFields.ChargesWrapperStart, string.Empty);
            template = template.Replace(chargesWrapperSection, string.Join(string.Empty, chargesLines));
            template = template.Replace(VendorRateConfirmationFields.ChargesWrapperEnd, string.Empty);

            return template;
        }

        public static string GenerateRateAndSchedulePreAuthorization(this MemberPageBase page, Shipment shipment, bool removeBodyWrapper = false)
        {
            // get template
            var d = new Decimals(null);
            var template = page.Server.ReadFromFile(SystemTemplates.RateAndSchedulePreauthorization).FromUtf8Bytes();

            // logo url
            var customer = shipment.Customer;
            var logoUrl = customer == null ? string.Empty : customer.RetrievePageLogoUrl();
            var siteRoot = page.Request.ResolveSiteRootWithHttp();
            logoUrl = logoUrl.Replace("~", siteRoot);

            // body wrapper
            if (removeBodyWrapper)
            {
                template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
                template = template.Replace(BaseFields.BodyWrapperEnd, WebApplicationSettings.Brand);
            }

            // general details
            template = template.Replace(ShipmentFields.CustomerNumber, shipment.Customer.CustomerNumber);
            template = template.Replace(ShipmentFields.CustomerName, shipment.Customer.Name);
            template = template.Replace(ShipmentFields.ShipmentDate, shipment.DesiredPickupDate.FormattedShortDate());
            template = template.Replace(ShipmentFields.EstimatedDeliveryDate, shipment.EstimatedDeliveryDate.FormattedShortDate());
            template = template.Replace(ShipmentFields.ServiceMode, shipment.ServiceMode.FormattedString());
            template = template.Replace(ShipmentFields.EarliestPickup, shipment.EarlyPickup);
            template = template.Replace(ShipmentFields.LastestPickup, shipment.LatePickup);
            template = template.Replace(ShipmentFields.PurchaseOrder, shipment.PurchaseOrderNumber);
            template = template.Replace(ShipmentFields.ShipperReference, shipment.ShipperReference);
            template = template.Replace(EmailTemplateFields.SiteRoot, siteRoot);
            template = template.Replace(EmailTemplateFields.LogoUrl, logoUrl);
            template = template.Replace(ShipmentFields.TenantAutoRatingNotice, shipment.Tenant.AutoRatingNoticeText.GetString());

            // vendors
            var sv = shipment.Vendors.FirstOrDefault(v => v.Primary);
            template = template.Replace(ShipmentFields.PrimaryVendorName, sv == null ? string.Empty : sv.Vendor.Name);

            //origin
            template = template.Replace(ShipmentFields.OriginDescription, shipment.Origin.Description);
            template = template.Replace(ShipmentFields.OriginStreet1, shipment.Origin.Street1);
            template = template.Replace(ShipmentFields.OriginStreet2, shipment.Origin.Street2);
            template = template.Replace(ShipmentFields.OriginCity, shipment.Origin.City);
            template = template.Replace(ShipmentFields.OriginState, shipment.Origin.State);
            template = template.Replace(ShipmentFields.OriginPostalCode, shipment.Origin.PostalCode);
            template = template.Replace(ShipmentFields.OriginCountry, shipment.Origin.Country.Name);
            template = template.Replace(ShipmentFields.OriginInstructions, shipment.Origin.SpecialInstructions);

            var primaryOriginContact = shipment.Origin.Contacts.FirstOrDefault(c => c.Primary) ?? new ShipmentContact();
            template = template.Replace(ShipmentFields.OriginPrimaryContact, primaryOriginContact.Name);
            template = template.Replace(ShipmentFields.OriginPrimaryPhone, primaryOriginContact.Phone);
            template = template.Replace(ShipmentFields.OriginPrimaryFax, primaryOriginContact.Fax);
            template = template.Replace(ShipmentFields.OriginPrimaryMobile, primaryOriginContact.Mobile);
            template = template.Replace(ShipmentFields.OriginPrimaryEmail, primaryOriginContact.Email);

            // destination
            template = template.Replace(ShipmentFields.DestinationDescription, shipment.Destination.Description);
            template = template.Replace(ShipmentFields.DestinationStreet1, shipment.Destination.Street1);
            template = template.Replace(ShipmentFields.DestinationStreet2, shipment.Destination.Street2);
            template = template.Replace(ShipmentFields.DestinationCity, shipment.Destination.City);
            template = template.Replace(ShipmentFields.DestinationState, shipment.Destination.State);
            template = template.Replace(ShipmentFields.DestinationPostalCode, shipment.Destination.PostalCode);
            template = template.Replace(ShipmentFields.DestinationCountry, shipment.Destination.Country.Name);
            template = template.Replace(ShipmentFields.DestinationInstructions, shipment.Destination.SpecialInstructions);

            var destinationPrimaryContact = shipment.Destination.Contacts.FirstOrDefault(c => c.Primary) ?? new ShipmentContact();
            template = template.Replace(ShipmentFields.DestinationPrimaryContact, destinationPrimaryContact.Name);
            template = template.Replace(ShipmentFields.DestinationPrimaryPhone, destinationPrimaryContact.Phone);
            template = template.Replace(ShipmentFields.DestinationPrimaryFax, destinationPrimaryContact.Fax);
            template = template.Replace(ShipmentFields.DestinationPrimaryMobile, destinationPrimaryContact.Mobile);
            template = template.Replace(ShipmentFields.DestinationPrimaryEmail, destinationPrimaryContact.Email);

            // items
            var hasItems = shipment.Items.Any();
            template = template.Replace(ShipmentFields.ItemsTotalWeight, hasItems ? shipment.Items.Sum(i => i.ActualWeight).ToString(d.Weight) : string.Empty);

            var itemWrapperSection = template.RetrieveWrapperSection(ShipmentFields.ItemsWrapperStart, ShipmentFields.ItemsWrapperEnd);
            var itemLines = hasItems
                            ? shipment.Items.OrderBy(i => i.Pickup).ThenBy(itt => itt.Delivery)
                                .Select(i => itemWrapperSection
                                                .Replace(ShipmentFields.ItemQuantity, i.Quantity.ToString())
                                                .Replace(ShipmentFields.ItemPieceCount, i.PieceCount == 0 ? string.Empty : i.PieceCount.ToString())
                                                .Replace(ShipmentFields.ItemPackageType, i.PackageType.TypeName)
                                                .Replace(ShipmentFields.ItemDescription, i.Description)
                                                .Replace(ShipmentFields.ItemTotalWeight, i.ActualWeight.ToString(d.Weight))
                                                .Replace(ShipmentFields.ItemLength, i.ActualLength.ToString(d.Dimension))
                                                .Replace(ShipmentFields.ItemWidth, i.ActualWidth.ToString(d.Dimension))
                                                .Replace(ShipmentFields.ItemHeight, i.ActualHeight.ToString(d.Dimension))
                                                .Replace(ShipmentFields.ItemFreightClass, i.ActualFreightClass.ToString())
                                                .Replace(ShipmentFields.ItemStackable, i.IsStackable ? "[STACKABLE]" : string.Empty)
                                                .Replace(ShipmentFields.ItemValue, i.Value == 0 ? "$" : i.Value.ToString(d.Currency))
                                                .Replace(ShipmentFields.ItemNMFC, i.NMFCCode)
                                                .Replace(ShipmentFields.ItemHTS, i.HTSCode)
                                                .Replace(ShipmentFields.ItemHazardous, i.HazardousMaterial ? WebApplicationConstants.HtmlCheck : string.Empty)
                                                .Replace(ShipmentFields.ItemComments, i.Comment))
                                .ToArray()
                            : new string[0];

            template = template.Replace(ShipmentFields.ItemsWrapperStart, string.Empty);
            template = template.Replace(itemWrapperSection, string.Join(string.Empty, itemLines));
            template = template.Replace(ShipmentFields.ItemsWrapperEnd, string.Empty);

            // accessorials
            var accessorialsWrapperSection = template.RetrieveWrapperSection(ShipmentFields.AccessorialsWrapperStart, ShipmentFields.AccessorialsWrapperEnd);
            var accessorialLines = shipment.Services.Any()
                            ? shipment.Services
                                .Select(s => accessorialsWrapperSection.Replace(ShipmentFields.AccessorialsDescription, s.Service.Description))
                                .ToArray()
                            : new string[0];

            template = template.Replace(ShipmentFields.AccessorialsWrapperStart, string.Empty);
            template = template.Replace(accessorialsWrapperSection, string.Join(", ", accessorialLines));
            template = template.Replace(ShipmentFields.AccessorialsWrapperEnd, string.Empty);

            // references
            var hasReferences = shipment.CustomerReferences.Any();
            var referenceWrapperSection = template.RetrieveWrapperSection(ShipmentFields.OriginReferencesWrapperStart, ShipmentFields.OriginReferencesWrapperEnd);
            var referenceLines = hasReferences
                            ? shipment.CustomerReferences
                                .Select(i => referenceWrapperSection
                                                .Replace(ShipmentFields.OriginReferenceName, i.Name)
                                                .Replace(ShipmentFields.OriginReferenceValue, i.Value))
                                .ToArray()
                            : new string[0];

            template = template.Replace(ShipmentFields.OriginReferencesWrapperStart, string.Empty);
            template = template.Replace(referenceWrapperSection, string.Join(string.Empty, referenceLines));
            template = template.Replace(ShipmentFields.OriginReferencesWrapperEnd, string.Empty);

            // rate details
            var hasCharges = shipment.Charges.Any();

            template = template.Replace(ShipmentFields.TotalAccessorialCharges, hasCharges ? shipment.Charges.Where(c => c.ChargeCode.Category == ChargeCodeCategory.Accessorial).Sum(i => i.AmountDue).ToString(d.CurrencyNumber) : string.Empty);
            template = template.Replace(ShipmentFields.TotalFreightCharges, hasCharges ? shipment.Charges.Where(c => c.ChargeCode.Category == ChargeCodeCategory.Freight).Sum(i => i.AmountDue).ToString(d.CurrencyNumber) : string.Empty);
            template = template.Replace(ShipmentFields.TotalFuelCharges, hasCharges ? shipment.Charges.Where(c => c.ChargeCode.Category == ChargeCodeCategory.Fuel).Sum(i => i.AmountDue).ToString(d.CurrencyNumber) : string.Empty);
            template = template.Replace(ShipmentFields.TotalOtherCharges, hasCharges ? shipment.Charges.Where(c => c.ChargeCode.Category == ChargeCodeCategory.Service).Sum(i => i.AmountDue).ToString(d.CurrencyNumber) : string.Empty);

            // harzardous materials
            var content = template.RetrieveWrapperSection(ShipmentFields.HazardousMaterialWrapperStart, ShipmentFields.HazardousMaterialWrapperEnd);
            template = template.Replace(ShipmentFields.HazardousMaterialWrapperStart, string.Empty);
            if (shipment.HazardousMaterial)
            {
                template = template.Replace(ShipmentFields.HazardousMaterialContact, shipment.HazardousMaterialContactName);
                template = template.Replace(ShipmentFields.HazardousMaterialPhone, shipment.HazardousMaterialContactPhone);
                template = template.Replace(ShipmentFields.HazardousMaterialMobile, shipment.HazardousMaterialContactMobile);
                template = template.Replace(ShipmentFields.HazardousMaterialEmail, shipment.HazardousMaterialContactEmail);
            }
            else
                template = template.Replace(content, string.Empty);
            template = template.Replace(ShipmentFields.HazardousMaterialWrapperEnd, string.Empty);


            // charges
            template = template.Replace(VendorRateConfirmationFields.ChargesTotalAmount, hasCharges ? shipment.Charges.Sum(i => i.AmountDue).ToString(d.CurrencyNumber) : string.Empty);
            var chargesWrapperSection = template.RetrieveWrapperSection(VendorRateConfirmationFields.ChargesWrapperStart, VendorRateConfirmationFields.ChargesWrapperEnd);
            var chargesLines = shipment.Charges.Any()
                            ? shipment.Charges
                                .Select(r => chargesWrapperSection
                                                .Replace(VendorRateConfirmationFields.ChargesCode, r.ChargeCode.Code.GetString())
                                                .Replace(VendorRateConfirmationFields.ChargesCategory, r.ChargeCode.Category.GetString())
                                                .Replace(VendorRateConfirmationFields.ChargesAmountDue, r.AmountDue.ToString(d.Currency))
                                                .Replace(VendorRateConfirmationFields.ChargesDescription, r.ChargeCode.Description)
                                                .Replace(VendorRateConfirmationFields.ChargesQuantity, r.Quantity.ToString()))
                                .ToArray()
                            : new string[0];

            template = template.Replace(VendorRateConfirmationFields.ChargesWrapperStart, string.Empty);
            template = template.Replace(chargesWrapperSection, string.Join(string.Empty, chargesLines));
            template = template.Replace(VendorRateConfirmationFields.ChargesWrapperEnd, string.Empty);

            return template;
        }
    }
}
