﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.WebApplication.Members;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Documents
{
	public static partial class DocumentProcessor
	{
		private const string VoidWaterMark =
			"<div style=\"position: absolute; z-index: 100\"><img src=\"{0}/images/voidWatermark.png\" alt=\"Void Watermark\"></div>";


		private static readonly object DocumentProcessorCacheLockObj = new object();
		private static readonly Dictionary<string, DocumentTemplateCache> DocTempateCache = new Dictionary<string,DocumentTemplateCache>();


		public static void ClearCache()
		{
			lock(DocumentProcessorCacheLockObj)
			{
				DocTempateCache.Clear();
			}
		}

		public static void Drop(DocumentTemplate template)
		{
			var key = DocTempateCache.Keys.FirstOrDefault(k => DocTempateCache[k].Template.Id == template.Id);
			if (!string.IsNullOrEmpty(key))
				lock (DocumentProcessorCacheLockObj)
				{
					DocTempateCache.Remove(key);
				}
		}


		public static string GenerateReportReadyNotification(this HttpServerUtility server, string reportName, bool hasAttachment, bool isForBackgroundReport = false)
		{
			// get template
			var template = server.ReadFromFile(SystemTemplates.ReportReadyNotificationTemplate).FromUtf8Bytes();

			template = template.Replace(EmailTemplateFields.SiteRoot, WebApplicationSettings.SiteRoot);

			// body wrapper
			template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
			template = template.Replace(BaseFields.BodyWrapperEnd, string.Empty);

			// details
			template = template.Replace(EmailTemplateFields.ReportName, reportName);
			template = template.Replace(EmailTemplateFields.Brand, WebApplicationSettings.Brand);

		    if (isForBackgroundReport)
		    {
		        template = template.Replace(EmailTemplateFields.BackgroundReportWrapperStart, string.Empty);
		        template = template.Replace(EmailTemplateFields.BackgroundReportWrapperEnd, string.Empty);
		        template = template.Replace(EmailTemplateFields.SiteRoot, WebApplicationSettings.SiteRoot);

		        var content = template.RetrieveWrapperSection(EmailTemplateFields.NoAttachmentWrapperStart,
		            EmailTemplateFields.NoAttachmentWrapperEnd);
		        template = template.Replace(EmailTemplateFields.NoAttachmentWrapperStart, string.Empty);
		        template = template.Replace(content, string.Empty);
		        template = template.Replace(EmailTemplateFields.NoAttachmentWrapperEnd, string.Empty);

		        content = template.RetrieveWrapperSection(EmailTemplateFields.AttachmentWrapperStart,
		            EmailTemplateFields.AttachmentWrapperEnd);
		        template = template.Replace(EmailTemplateFields.AttachmentWrapperStart, string.Empty);
		        template = template.Replace(content, string.Empty);
		        template = template.Replace(EmailTemplateFields.AttachmentWrapperEnd, string.Empty);
		    }
            else if (hasAttachment)
			{
			    var content = template.RetrieveWrapperSection(EmailTemplateFields.NoAttachmentWrapperStart,
			        EmailTemplateFields.NoAttachmentWrapperEnd);
			    template = template.Replace(EmailTemplateFields.NoAttachmentWrapperStart, string.Empty);
			    template = template.Replace(content, string.Empty);
			    template = template.Replace(EmailTemplateFields.NoAttachmentWrapperEnd, string.Empty);
			    template = template.Replace(EmailTemplateFields.AttachmentWrapperStart, string.Empty);
			    template = template.Replace(EmailTemplateFields.AttachmentWrapperEnd, string.Empty);

			    // clear out information for background report ready reports
			    var backgroundReportContent = template.RetrieveWrapperSection(EmailTemplateFields.BackgroundReportWrapperStart,
			        EmailTemplateFields.BackgroundReportWrapperEnd);
			    template = template.Replace(EmailTemplateFields.BackgroundReportWrapperStart, string.Empty);
			    template = template.Replace(backgroundReportContent, string.Empty);
			    template = template.Replace(EmailTemplateFields.BackgroundReportWrapperEnd, string.Empty);
            }
			else
			{
			    var content = template.RetrieveWrapperSection(EmailTemplateFields.AttachmentWrapperStart,
			        EmailTemplateFields.AttachmentWrapperEnd);
			    template = template.Replace(EmailTemplateFields.NoAttachmentWrapperStart, string.Empty);
			    template = template.Replace(EmailTemplateFields.NoAttachmentWrapperEnd, string.Empty);
			    template = template.Replace(EmailTemplateFields.AttachmentWrapperStart, string.Empty);
			    template = template.Replace(content, string.Empty);
			    template = template.Replace(EmailTemplateFields.AttachmentWrapperEnd, string.Empty);

			    // clear out information for background report ready reports
			    var backgroundReportContent = template.RetrieveWrapperSection(EmailTemplateFields.BackgroundReportWrapperStart,
			        EmailTemplateFields.BackgroundReportWrapperEnd);
			    template = template.Replace(EmailTemplateFields.BackgroundReportWrapperStart, string.Empty);
			    template = template.Replace(backgroundReportContent, string.Empty);
			    template = template.Replace(EmailTemplateFields.BackgroundReportWrapperEnd, string.Empty);
            }

			return template;
		}

        public static string GenerateClaimNoteReminderNotification(this HttpServerUtility server, ClaimNote note)
        {
            // get template
            var template = server.ReadFromFile(SystemTemplates.ClaimNoteReminderNotification).FromUtf8Bytes();

            template = template.Replace(EmailTemplateFields.SiteRoot, WebApplicationSettings.SiteRoot);
            template = template.Replace(EmailTemplateFields.Brand, WebApplicationSettings.Brand);

            // body wrapper
            template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
            template = template.Replace(BaseFields.BodyWrapperEnd, string.Empty);

            // details
            template = template.Replace(EmailTemplateFields.ClaimNumber, note.Claim.ClaimNumber);
            template = template.Replace(EmailTemplateFields.ClaimNoteMessage, note.Message);
            return template;
        }

		public static string GenerateXmlTransmissionErrorNotification(this HttpServerUtility server, string transmissionType, string xmlTransmissionErrorMessage)
		{
			// get template
			var template = server.ReadFromFile(SystemTemplates.XmlTransmissionErrorTemplate).FromUtf8Bytes();

			// body wrapper
			template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
			template = template.Replace(BaseFields.BodyWrapperEnd, string.Empty);

			// details
			template = template.Replace(EmailTemplateFields.TranmissionType, transmissionType);
			template = template.Replace(EmailTemplateFields.XmlTransmissionErrorMessage, xmlTransmissionErrorMessage);
			template = template.Replace(EmailTemplateFields.Brand, WebApplicationSettings.Brand);

			return template;
		}



		public static string GenerateDeveloperAccessRequestNotification(this MemberPageBase page, DeveloperAccessRequest request, bool production)
		{
			//get template
			var template = production
							   ? page.Server.ReadFromFile(SystemTemplates.DeveloperAccessRequestProductionTemplate).FromUtf8Bytes()
							   : page.Server.ReadFromFile(SystemTemplates.DeveloperAccessRequestTemplate).FromUtf8Bytes();

			// logo url
			var customer = request.Customer;
			var logoUrl = customer == null ? string.Empty : customer.RetrievePageLogoUrl();
			var siteRoot = page.Request.ResolveSiteRootWithHttp();
			logoUrl = logoUrl.Replace("~", siteRoot);

			// body wrapper
			template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
			template = template.Replace(BaseFields.BodyWrapperEnd, string.Empty);

			// details
			template = template.Replace(EmailTemplateFields.SiteRoot, siteRoot);
			template = template.Replace(EmailTemplateFields.LogoUrl, logoUrl);
			template = template.Replace(EmailTemplateFields.AccountName, request.Customer.Name);
			template = template.Replace(EmailTemplateFields.Brand, WebApplicationSettings.Brand);
			template = template.Replace(EmailTemplateFields.User,
										string.Format("{0} {1}", page.ActiveUser.FirstName, page.ActiveUser.LastName));
			template = template.Replace(EmailTemplateFields.CustomerNumber, request.Customer.CustomerNumber);
			template = template.Replace(EmailTemplateFields.CustomerName, request.Customer.Name);
			template = template.Replace(EmailTemplateFields.ContactName, request.ContactName);
			template = template.Replace(EmailTemplateFields.ContactEmail, request.ContactEmail);

			return template;
		}

		public static string GenerateDeveloperAccessRequestUpdateNotification(this MemberPageBase page, DeveloperAccessRequest request, string message)
		{
			//get template
			var template = page.Server.ReadFromFile(SystemTemplates.DeveloperAccessRequestUpdateTemplate).FromUtf8Bytes();

			// logo url
			var customer = request.Customer;
			var logoUrl = customer == null ? string.Empty : customer.RetrievePageLogoUrl();
			var siteRoot = page.Request.ResolveSiteRootWithHttp();
			logoUrl = logoUrl.Replace("~", siteRoot);

			// body wrapper
			template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
			template = template.Replace(BaseFields.BodyWrapperEnd, string.Empty);

			// details
			template = template.Replace(EmailTemplateFields.SiteRoot, siteRoot);
			template = template.Replace(EmailTemplateFields.LogoUrl, logoUrl);
			template = template.Replace(EmailTemplateFields.AccountName, request.Customer.Name);
			template = template.Replace(EmailTemplateFields.Brand, WebApplicationSettings.Brand);
			template = template.Replace(EmailTemplateFields.User,
										string.Format("{0} {1}", page.ActiveUser.FirstName, page.ActiveUser.LastName));
			template = template.Replace(EmailTemplateFields.CustomerNumber, request.Customer.CustomerNumber);
			template = template.Replace(EmailTemplateFields.CustomerName, request.Customer.Name);
			template = template.Replace(EmailTemplateFields.ContactName, request.ContactName);
			template = template.Replace(EmailTemplateFields.ContactEmail, request.ContactEmail);
			template = template.Replace(EmailTemplateFields.SupportMessage, message);

			return template;
		}

		public static string GenerateDeveloperAccessSupportRequestNotification(this MemberPageBase page, DeveloperAccessRequest request, string message, string category)
		{
			//get template
			var template = page.Server.ReadFromFile(SystemTemplates.DeveloperAccessRequestSupportTemplate).FromUtf8Bytes();

			// logo url
			var customer = request.Customer;
			var logoUrl = customer == null ? string.Empty : customer.RetrievePageLogoUrl();
			var siteRoot = page.Request.ResolveSiteRootWithHttp();
			logoUrl = logoUrl.Replace("~", siteRoot);

			// body wrapper
			template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
			template = template.Replace(BaseFields.BodyWrapperEnd, string.Empty);

			// details
			template = template.Replace(EmailTemplateFields.SiteRoot, siteRoot);
			template = template.Replace(EmailTemplateFields.LogoUrl, logoUrl);
			template = template.Replace(EmailTemplateFields.AccountName, request.Customer.Name);
			template = template.Replace(EmailTemplateFields.Brand, WebApplicationSettings.Brand);
			template = template.Replace(EmailTemplateFields.User,
										string.Format("{0} {1}", page.ActiveUser.FirstName, page.ActiveUser.LastName));
			template = template.Replace(EmailTemplateFields.CustomerNumber, request.Customer.CustomerNumber);
			template = template.Replace(EmailTemplateFields.CustomerName, request.Customer.Name);
			template = template.Replace(EmailTemplateFields.ContactName, request.ContactName);
			template = template.Replace(EmailTemplateFields.ContactEmail, request.ContactEmail);
			template = template.Replace(EmailTemplateFields.SupportCategory, category);
			template = template.Replace(EmailTemplateFields.SupportMessage, message);

			return template;
		}


        public static string GenerateDevelopmentAccessRequestApprovalNotification(this MemberPageBase page, DeveloperAccessRequest request, string name)
        {
            //get template
            var template =  page.Server.ReadFromFile(SystemTemplates.DeveloperAccessRequestDevelopmentApprovalTemplate).FromUtf8Bytes();

            // logo url
            var customer = request.Customer;
            var logoUrl = customer == null ? string.Empty : customer.RetrievePageLogoUrl();
            var siteRoot = page.Request.ResolveSiteRootWithHttp();
            logoUrl = logoUrl.Replace("~", siteRoot);

            // body wrapper
            template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
            template = template.Replace(BaseFields.BodyWrapperEnd, string.Empty);

            // details
            template = template.Replace(EmailTemplateFields.SiteRoot, siteRoot);
            template = template.Replace(EmailTemplateFields.LogoUrl, logoUrl);
            template = template.Replace(EmailTemplateFields.AccountName, request.Customer.Name);
            template = template.Replace(EmailTemplateFields.Brand, WebApplicationSettings.Brand);
            template = template.Replace(EmailTemplateFields.ContactName, request.ContactName);
            template = template.Replace(EmailTemplateFields.CustomerName, request.Customer.Name);
            template = template.Replace(EmailTemplateFields.RequestDate, request.DateCreated.FormattedShortDate());

           template = string.IsNullOrEmpty(name) 
               ? template.Replace(EmailTemplateFields.User, string.Empty)
               : template.Replace(EmailTemplateFields.User, string.Format("It was requested by {0}.", name));

            return template;
        }

        public static string GenerateProductionAccessRequestApprovalNotification(this MemberPageBase page, DeveloperAccessRequest request, string name)
        {
            //get template
            var template =  page.Server.ReadFromFile(SystemTemplates.DeveloperAccessRequestProductionApprovalTemplate).FromUtf8Bytes();

            // logo url
            var customer = request.Customer;
            var logoUrl = customer == null ? string.Empty : customer.RetrievePageLogoUrl();
            var siteRoot = page.Request.ResolveSiteRootWithHttp();
            logoUrl = logoUrl.Replace("~", siteRoot);

            // body wrapper
            template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
            template = template.Replace(BaseFields.BodyWrapperEnd, string.Empty);

            // details
            template = template.Replace(EmailTemplateFields.SiteRoot, siteRoot);
            template = template.Replace(EmailTemplateFields.LogoUrl, logoUrl);
            template = template.Replace(EmailTemplateFields.AccountName, request.Customer.Name);
            template = template.Replace(EmailTemplateFields.Brand, WebApplicationSettings.Brand);
            template = template.Replace(EmailTemplateFields.ContactName, request.ContactName);
            template = template.Replace(EmailTemplateFields.CustomerName, request.Customer.Name);
            template = template.Replace(EmailTemplateFields.RequestDate, request.DateCreated.FormattedShortDate());

            template = string.IsNullOrEmpty(name) 
                ? template.Replace(EmailTemplateFields.User, string.Empty) 
                : template.Replace(EmailTemplateFields.User, string.Format("It was requested by {0}.", name));

            return template;
        }


		public static string GenerateSupportEmail(this MemberControlBase control, EmailMessage message)
		{
			//get template
			var template = control.Server.ReadFromFile(SystemTemplates.CustomerSupportEmailTemplate).FromUtf8Bytes();

			// logo url
            var logoUrl = control.ActiveUser.Tenant.LogoUrl;
            var siteRoot = control.Request.ResolveSiteRootWithHttp();
            logoUrl = logoUrl.Replace("~", siteRoot);

			// body wrapper
			template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
			template = template.Replace(BaseFields.BodyWrapperEnd, string.Empty);

			// details
            template = template.Replace(EmailTemplateFields.LogoUrl, logoUrl);
			template = template.Replace(EmailTemplateFields.Brand, WebApplicationSettings.Brand);
			template = template.Replace(EmailTemplateFields.User, string.Format("{0} {1}", control.ActiveUser.FirstName, control.ActiveUser.LastName));
			template = template.Replace(EmailTemplateFields.CustomerName, control.ActiveUser.DefaultCustomer.Name);
			template = template.Replace(EmailTemplateFields.CustomerNumber, control.ActiveUser.DefaultCustomer.CustomerNumber);
			template = template.Replace(EmailTemplateFields.SupportRequestBody, message.Body);
			template = template.Replace(EmailTemplateFields.ReplyToEmail, message.ReplyEmail);

			return template;
		}

		public static string GenerateSupportEmail(this MemberPageBase page, EmailMessage message)
		{
			//get template
			var template = page.Server.ReadFromFile(SystemTemplates.CustomerSupportEmailTemplate).FromUtf8Bytes();

			// logo url
            var logoUrl = page.ActiveUser.Tenant.LogoUrl;
            var siteRoot = page.Request.ResolveSiteRootWithHttp();
            logoUrl = logoUrl.Replace("~", siteRoot);

			// body wrapper
			template = template.Replace(BaseFields.BodyWrapperStart, string.Empty);
			template = template.Replace(BaseFields.BodyWrapperEnd, string.Empty);

			// details
            template = template.Replace(EmailTemplateFields.LogoUrl, logoUrl);
			template = template.Replace(EmailTemplateFields.Brand, WebApplicationSettings.Brand);
			template = template.Replace(EmailTemplateFields.User, string.Format("{0} {1}", page.ActiveUser.FirstName, page.ActiveUser.LastName));
			template = template.Replace(EmailTemplateFields.CustomerName, page.ActiveUser.DefaultCustomer.Name);
			template = template.Replace(EmailTemplateFields.CustomerNumber, page.ActiveUser.DefaultCustomer.CustomerNumber);
			template = template.Replace(EmailTemplateFields.SupportRequestBody, message.Body);
			template = template.Replace(EmailTemplateFields.ReplyToEmail, message.ReplyEmail);

			return template;
		}



		public static string GenerateTenantLoginPage(this Page page, Tenant tenant, string customTemplate, string alerts)
		{
			//get template
			var template = string.IsNullOrEmpty(customTemplate)
				? page.Server.ReadFromFile(SystemTemplates.TenantLoginTemplate).FromUtf8Bytes()
				: customTemplate;

			// logo url
			var logoUrl = tenant.LogoUrl;
			var siteRoot = page.Request.ResolveSiteRootWithHttp();
			logoUrl = logoUrl.Replace("~", siteRoot);

			// details
			template = template.Replace(TenantLoginTemplateFields.AccessCode, tenant.Code);
			template = template.Replace(TenantLoginTemplateFields.LogoUrl, logoUrl);
			template = template.Replace(TenantLoginTemplateFields.SiteRoot, siteRoot);
			template = template.Replace(TenantLoginTemplateFields.Alerts, alerts);

			return template;
		}




		public static string RetrieveWrapperSection(this string template, string wrapperStart, string wrapperEnd)
		{
			var startIndex = template.IndexOf(wrapperStart, StringComparison.Ordinal);
			var endIndex = template.IndexOf(wrapperEnd, StringComparison.Ordinal);
			var subStringStart = startIndex + wrapperStart.Length;
			return startIndex >= 0 && endIndex >= 0 && subStringStart <= template.Length
					? template.Substring(subStringStart, endIndex - subStringStart)
					: string.Empty;
		}

		private static string GetStopDescription(this int itemOrder, int lastStopOrder)
		{
			return itemOrder == 0 ? "Origin" : itemOrder == lastStopOrder ? "Destination" : string.Format("Stop {0}", itemOrder);
		}

		private static string FetchTemplate(this HttpServerUtility server, long tenantId, long prefixId, ServiceMode mode, DocumentTemplateCategory category,
			out Decimals decimals)
		{
			var key = string.Format("{0}_{1}_{2}_{3}", tenantId, prefixId, mode.ToInt(), category.ToInt());

			if(DocTempateCache.ContainsKey(key))
			{
				decimals = new Decimals(DocTempateCache[key].Template);
				return DocTempateCache[key].TemplateContent;
			}

			var template = new DocumentTemplateSearch().FetchApplicableDocumentTemplate(tenantId, prefixId, mode, category);
			decimals = new Decimals(template);
			var content = template == null ? string.Empty : server.ReadFromFile(template.TemplatePath).FromUtf8Bytes();

			if (template != null) // cache only if template is not null!
			{
				lock (DocumentProcessorCacheLockObj)
				{
					if (!DocTempateCache.ContainsKey(key))
						DocTempateCache.Add(key, new DocumentTemplateCache(key, content, template));
				}
			}

			return content;
		}

		internal class Decimals
		{
			private string WeightPlaces { get; set; }
			private string CurrentPlaces { get; set; }
			private string DimensionPlaces { get; set; }

			public Decimals(DocumentTemplate template)
			{
				WeightPlaces = template == null ? "2" : template.WeightDecimals;
				CurrentPlaces = template == null ? "2" : template.CurrencyDecimals;
				DimensionPlaces = template == null ? "2" : template.DimensionDecimals;
			}

			public string Currency
			{
				get { return string.Format("c{0}", CurrentPlaces); }
			}

			public string CurrencyNumber
			{
				get { return string.Format("n{0}", CurrentPlaces); }
			}

			public string Dimension
			{
				get { return string.Format("n{0}", DimensionPlaces); }
			}

			public string Weight
			{
				get { return string.Format("n{0}", WeightPlaces); }
			}
		}

		internal class DocumentTemplateCache
		{
			public string Key { get; private set; }
			public DocumentTemplate Template { get; private set; }
			public string TemplateContent { get; private set; }

			public DocumentTemplateCache(string key, string templateContent, DocumentTemplate template)
			{
				Key = key;
				TemplateContent = templateContent;
				Template = template;
			}
		}
	}
}
