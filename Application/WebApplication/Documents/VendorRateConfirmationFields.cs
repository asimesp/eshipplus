﻿namespace LogisticsPlus.Eship.WebApplication.Documents
{
    public class VendorRateConfirmationFields
    {
        // Body
        public const string BodyWrapperStart = "[#BodyWrapperStart#]";
        public const string BodyWrapperEnd = "[#BodyWrapperEnd#]";

        //Details
		public const string Prefix = "[#Prefix#]";
		public const string EarliestPickup = "[#EarliestPickup#]";
		public const string LastestPickup = "[#LastestPickup#]";
		public const string EarliestDelivery = "[#EarliestDelivery#]";
		public const string LastestDelivery = "[#LastestDelivery#]";
        public const string CustomerName = "[#CustomerName#]";
        public const string ShipmentNumber = "[#ShipmentNumber#]";
        public const string ShipmentDate = "[#ShipmentDate#]";
		public const string PurchaseOrderNumber = "[#PurchaseOrderNumber#]";
        public const string Carrier = "[#Carrier#]";
		public const string ShipperReference = "[#ShipperReference#]";
        public const string DeliveryDate = "[#DeliveryDate#]";
		public const string LogoUrl = "[#LogoUrl#]";
		public const string VendorRatingProfileAddressWrapperStart = "[#VendorRatingProfileAddressWrapperStart#]";
		public const string VendorRatingProfileAddressWrapperEnd = "[#VendorRatingProfileAddressWrapperEnd#]";
        public const string IsPartialTruckLoadWrapperStart = "[#IsPartialTruckLoadWrapperStart#]";
        public const string IsPartialTruckLoadWrapperEnd = "[#IsPartialTruckLoadWrapperEnd#]";


		// vendor
		public const string PrimaryVendorName = "[#PrimaryVendorName#]";
		public const string PrimaryVendorPro = "[#PrimaryVendorPro#]";

		public const string VendorsWrapperStart = "[#VendorsWrapperStart#]";
		public const string VendorsWrapperEnd = "[#VendorsWrapperEnd#]";
		public const string VendorName = "[#VendorName#]";
		public const string VendorPro = "[#VendorPro#]";

		// user to contact
		public const string ContactPhone = "[#ContactPhone#]";
		public const string ContactName = "[#ContactName#]";
		public const string ContactTollFree = "[#ContactTollFree#]";

        public const string CreatedByContactPhone = "[#CreatedByContactPhone#]";
        public const string CreatedByContactName = "[#CreatedByContactName#]";

        public const string CreatedByUserDepartmentPhone = "[#CreatedByUserDepartmentPhone#]";
        
        //department
    	public const string DepartmentPhone = "[#DepartmentPhone#]";
        public const string DepartmentFax = "[#DepartmentFax#]";
        public const string DepartmentEmail = "[#DepartmentEmail#]";

		// origin
		public const string OriginDescription = "[#OriginDescription#]";
		public const string OriginStreet1 = "[#OriginStreet1#]";
		public const string OriginStreet2 = "[#OriginStreet2#]";
		public const string OriginCity = "[#OriginCity#]";
		public const string OriginState = "[#OriginState#]";
		public const string OriginPostalCode = "[#OriginPostalCode#]";
		public const string OriginCountry = "[#OriginCountry#]";
		public const string OriginGeneralInfo = "[#OriginGeneralInfo#]";
		public const string OriginDirections= "[#OriginDirections#]";

		public const string OriginPrimaryContactEnabled = "[#OriginPrimaryContactEnabled#]";
		public const string OriginPrimaryContact = "[#OriginPrimaryContact#]";
		public const string OriginPrimaryPhone = "[#OriginPrimaryPhone#]";
		public const string OriginPrimaryFax = "[#OriginPrimaryFax#]";
		public const string OriginPrimaryMobile = "[#OriginPrimaryMobile#]";
		public const string OriginPrimaryEmail = "[#OriginPrimaryEmail#]";

		public const string OriginContactsWrapperStart = "[#OriginContactsWrapperStart#]";
		public const string OriginContactsWrapperEnd = "[#OriginContactsWrapperEnd#]";
		public const string OriginContact = "[#OriginContact#]";
		public const string OriginPhone = "[#OriginPhone#]";
		public const string OriginFax = "[#OriginFax#]";
		public const string OriginMobile = "[#OriginMobile#]";
		public const string OriginEmail = "[#OriginEmail#]";

		public const string OriginInstructions = "[#OriginInstructions#]";

		public const string OriginReferencesWrapperStart = "[#OriginReferencesWrapperStart#]";
		public const string OriginReferencesWrapperEnd = "[#OriginReferencesWrapperEnd#]";
		public const string OriginReferenceName = "[#OriginReferenceName#]";
		public const string OriginReferenceValue = "[#OriginReferenceValue#]";

		public const string OriginTerminalCode = "[#OriginTerminalCode#]";
		public const string OriginTerminalName = "[#OriginTerminalName#]";
		public const string OriginTerminalPhone = "[#OriginTerminalPhone#]";
		public const string OriginTerminalFax = "[#OriginTerminalFax#]";
		public const string OriginTerminalTollFree = "[#OriginTerminalTollFree#]";
		public const string OriginTerminalEmail = "[#OriginTerminalEmail#]";
		public const string OriginTerminalContactName = "[#OriginTerminalContactName#]";
		public const string OriginTerminalContactTitle = "[#OriginTerminalContactTitle#]";

		// destination
		public const string DestinationDescription = "[#DestinationDescription#]";
		public const string DestinationStreet1 = "[#DestinationStreet1#]";
		public const string DestinationStreet2 = "[#DestinationStreet2#]";
		public const string DestinationCity = "[#DestinationCity#]";
		public const string DestinationState = "[#DestinationState#]";
		public const string DestinationPostalCode = "[#DestinationPostalCode#]";
		public const string DestinationCountry = "[#DestinationCountry#]";
        public const string DestinationGeneralInfo = "[#DestinationGeneralInfo#]";
        public const string DestinationDirections = "[#DestinationDirections#]";

		public const string DestinationPrimaryContactEnabled = "[#DestinationPrimaryContactEnabled#]";
		public const string DestinationPrimaryContact = "[#DestinationPrimaryContact#]";
		public const string DestinationPrimaryPhone = "[#DestinationPrimaryPhone#]";
		public const string DestinationPrimaryFax = "[#DestinationPrimaryFax#]";
		public const string DestinationPrimaryMobile = "[#DestinationPrimaryMobile#]";
		public const string DestinationPrimaryEmail = "[#DestinationPrimaryEmail#]";

		public const string DestinationContactsWrapperStart = "[#DestinationContactsWrapperStart#]";
		public const string DestinationContactsWrapperEnd = "[#DestinationContactsWrapperEnd#]";
		public const string DestinationContact = "[#DestinationContact#]";
		public const string DestinationPhone = "[#DestinationPhone#]";
		public const string DestinationFax = "[#DestinationFax#]";
		public const string DestinationMobile = "[#DestinationMobile#]";
		public const string DestinationEmail = "[#DestinationEmail#]";

		public const string DestinationInstructions = "[#DestinationInstructions#]";

		public const string DestinationReferencesWrapperStart = "[#DestinationReferencesWrapperStart#]";
		public const string DestinationReferencesWrapperEnd = "[#DestinationReferencesWrapperEnd#]";
		public const string DestinationReferenceName = "[#DestinationReferenceName#]";
		public const string DestinationReferenceValue = "[#DestinationReferenceValue#]";

		public const string DestinationTerminalCode = "[#DestinationTerminalCode#]";
		public const string DestinationTerminalName = "[#DestinationTerminalName#]";
		public const string DestinationTerminalPhone = "[#DestinationTerminalPhone#]";
		public const string DestinationTerminalFax = "[#DestinationTerminalFax#]";
		public const string DestinationTerminalTollFree = "[#DestinationTerminalTollFree#]";
		public const string DestinationTerminalEmail = "[#DestinationTerminalEmail#]";
		public const string DestinationTerminalContactName = "[#DestinationTerminalContactName#]";
		public const string DestinationTerminalContactTitle = "[#DestinationTerminalContactTitle#]";

		//Charges
		public const string ChargesWrapperStart = "[#ChargesWrapperStart#]";
		public const string ChargesWrapperEnd = "[#ChargesWrapperEnd#]";
		public const string ChargesDescription = "[#ChargesDescription#]";
		public const string ChargesComment = "[#ChargesComment#]";
		public const string ChargesAmountDue = "[#ChargesAmountDue#]";
        public const string ChargesTotalAmount = "[#ChargesTotalAmount#]";
        public const string ChargesQuantity = "[#ChargesQuantity#]";
        public const string ChargesCode = "[#ChargesCode#]";
        public const string ChargesCategory = "[#ChargesCategory#]";

        //Critical Notes
        public const string CriticalNotesWrapperStart = "[#CriticalNotesWrapperStart#]";
        public const string CriticalNotesWrapperEnd = "[#CriticalNotesWrapperEnd#]";
        public const string CriticalNotesMessage = "[#CriticalNotesMessage#]";

        //General Notes
        public const string GeneralNotesWrapperStart = "[#GeneralNotesWrapperStart#]";
        public const string GeneralNotesWrapperEnd = "[#GeneralNotesWrapperEnd#]";
        public const string GeneralNotesMessage = "[#GeneralNotesMessage#]";

        //Equipment
        public const string EquipmentWrapperStart = "[#EquipmentWrapperStart#]";
        public const string EquipmentWrapperEnd = "[#EquipmentWrapperEnd#]";
        public const string EquipmentType = "[#EquipmentType#]";

		// items
		public const string ItemsTotalWeight = "[#ItemsTotalWeight#]";
		public const string ItemsTotalValue = "[#ItemsTotalValue#]";
		public const string ItemsWrapperStart = "[#ItemsWrapperStart#]";
		public const string ItemsWrapperEnd = "[#ItemsWrapperEnd#]";
		public const string ItemQuantity = "[#ItemQuantity#]";
		public const string ItemPieceCount = "[#ItemPieceCount#]";
		public const string ItemPackageType = "[#ItemPackageType#]";
		public const string ItemDescription = "[#ItemDescription#]";
		public const string ItemTotalWeight = "[#ItemTotalWeight#]";
		public const string ItemLength = "[#ItemLength#]";
		public const string ItemWidth = "[#ItemWidth#]";
		public const string ItemHeight = "[#ItemHeight#]";
		public const string ItemFreightClass = "[#ItemFreightClass#]";
		public const string ItemStackable = "[#ItemStackable#]";
		public const string ItemValue = "[#ItemValue#]";
		public const string ItemNMFC = "[#ItemNMFC#]";
		public const string ItemHTS = "[#ItemHTS#]";
		public const string ItemPickup = "[#ItemPickup#]";
		public const string ItemDelivery = "[#ItemDelivery#]";
		public const string ItemComments = "[#ItemComments#]";
		public const string ItemHazardous = "[#ItemHazardous#]";
    }
}
