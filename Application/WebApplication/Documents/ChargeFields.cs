﻿namespace LogisticsPlus.Eship.WebApplication.Documents
{
	public class ChargeFields : BaseFields
	{
        // groups
        public const string GroupChargeWrapperStart = "[#GroupChargeWrapperStart#]";
        public const string GroupChargeWrapperEnd = "[#GroupChargeWrapperEnd#]";

        // charges
        public const string ViewChargeWrapperStart = "[#ViewChargeWrapperStart#]";
        public const string ViewChargeWrapperEnd = "[#ViewChargeWrapperEnd#]";

        // Charge details
        public const string Vendor = "[#Vendor#]";

		public const string DocumentNumber = "[#DocumentNumber#]";
		public const string VendorDisputeReference = "[#DisputeRefernceNumber#]";
        public const string AccountName = "[#AccountName#]";
        public const string DocumentType = "[#DocumentType#]";
        public const string UnitBuyTotal = "[#UnitBuyTotal#]";
        public const string UnitSellTotal = "[#UnitSellTotal#]";
        public const string UnitDiscountTotal = "[#UnitDiscountTotal#]";
        public const string ChargeCode = "[#ChargeCode#]";
        public const string Comment = "[#Comment#]";
        public const string Qty = "[#Qty#]";
        public const string UnitBuy = "[#UnitBuy#]";

		//Vendor Charge Dispute
		public const string ResolutionComment = "[#ResolutionComment#]";
    }
}
