﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Extern.Ws;
using LogisticsPlus.Eship.WebApplication.Utilities;
using Newtonsoft.Json;

namespace LogisticsPlus.Eship.WebApplication.Services.Rest
{
	public partial class GetAccessorialServices : RESTApiPageBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
                var authData = GetAuthentication();

				string streamData;
				using (var reader = new StreamReader(Request.InputStream))
					streamData = reader.ReadToEnd();
				_logManager.Log(streamData, RESTTransmissionSource.GetAccessorialServicesRequest,
				                authenticationData: WSUtilitiesV4.ObscureRESTPassword(authData));

				var messages = new List<WSMessage>();
				ActiveUser = JsonConvert.DeserializeObject<WSAuthentication>(authData).IsValidUser(messages);
				if (messages.ContainsErrors())
				{
					JSON = JsonConvert.SerializeObject(messages.ToMessageReturnCollection<WSAccessorialService>());
					Response.SendErrorResponse(JSON);
					_logManager.Log(JSON, RESTTransmissionSource.GetAccessorialServicesResponse, ActiveUser);
				    return;
				}
					
				EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tMethod:{4}\t IP:{5}",
														   ActiveUser.Tenant.Name, ActiveUser.FirstName, ActiveUser.LastName, ActiveUser.Username,
														   "REST API: Get Accessorial Services", HttpContext.Current.Request.UserHostAddress);

				JSON = JsonConvert.SerializeObject(eShipPlusServiceBase.FetchAccessorialServices(ActiveUser.TenantId));
				_logManager.Log(JSON, RESTTransmissionSource.GetAccessorialServicesResponse, ActiveUser);
				Response.SendSuccessfulyResponse(JSON);
			}
			catch (Exception ex)
			{
				ErrorLogger.LogError(ex, Context);
				JSON = JsonConvert.SerializeObject(
					new[] {new WSMessage {Type = WSEnums.MessageType.Error, Value = ex.Message}}
						.ToMessageReturnCollection<WSAccessorialService>());
				Response.SendErrorResponse(JSON);
				_logManager.Log(JSON, RESTTransmissionSource.GetAccessorialServicesResponse, ActiveUser);
			}
		}
	}
}