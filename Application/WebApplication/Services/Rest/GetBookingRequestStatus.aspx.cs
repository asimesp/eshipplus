﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Extern.Ws;
using LogisticsPlus.Eship.WebApplication.Utilities;
using Newtonsoft.Json;

namespace LogisticsPlus.Eship.WebApplication.Services.Rest
{
	public partial class GetBookingRequestStatus : RESTApiPageBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
                var authData = GetAuthentication();

				string streamData;
				using (var reader = new StreamReader(Request.InputStream))
					streamData = reader.ReadToEnd();
                _logManager.Log(streamData, RESTTransmissionSource.GetBookingRequestStatusRequest, authenticationData: WSUtilitiesV4.ObscureRESTPassword(authData));

                var bookingNumber = JsonConvert.DeserializeObject<string>(streamData);

				var messages = new List<WSMessage>();

				Customer customer;
                ActiveUser = JsonConvert.DeserializeObject<WSAuthentication>(authData).IsValidUser(messages, out customer);
				if (messages.ContainsErrors())
				{
					JSON = JsonConvert.SerializeObject(messages.ToMessageReturn<WSBookingRequestStatus>());
					Response.SendErrorResponse(JSON);
					_logManager.Log(JSON, RESTTransmissionSource.GetBookingRequestStatusResponse, ActiveUser);
				    return;
				}

				EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tMethod:{4}\t IP:{5}", ActiveUser.Tenant.Name, ActiveUser.FirstName, ActiveUser.LastName, ActiveUser.Username, "REST API: Get Booking Request Status", HttpContext.Current.Request.UserHostAddress);

                JSON = JsonConvert.SerializeObject(eShipPlusServiceBase.FetchBookingRequestStatus(ActiveUser, bookingNumber, customer));
				_logManager.Log(JSON, RESTTransmissionSource.GetBookingRequestStatusResponse, ActiveUser);
				Response.SendSuccessfulyResponse(JSON);
			}
			catch (Exception ex)
			{
				ErrorLogger.LogError(ex, Context);
				JSON = JsonConvert.SerializeObject(
					new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = ex.Message } }
						.ToMessageReturn<WSBookingRequestStatus>());
				Response.SendErrorResponse(JSON);
				_logManager.Log(JSON, RESTTransmissionSource.GetBookingRequestStatusResponse, ActiveUser);
			}
		}
	}
}