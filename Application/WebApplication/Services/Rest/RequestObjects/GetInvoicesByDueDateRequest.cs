﻿using System;

namespace LogisticsPlus.Eship.WebApplication.Services.Rest.RequestObjects
{
	public class GetInvoicesByDueDateRequest
	{
		public DateTime Startdate { get; set; }
		public DateTime EndDate { get; set; }
	}
}