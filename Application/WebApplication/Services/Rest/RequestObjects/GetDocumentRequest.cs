﻿using System;

namespace LogisticsPlus.Eship.WebApplication.Services.Rest.RequestObjects
{
	public class GetDocumentRequest
    {
		public string ReferenceNumber { get; set; }
		public long DocumentId { get; set; }
	}
}