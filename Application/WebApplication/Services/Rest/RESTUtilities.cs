﻿using System.Net;
using System.Web;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Services.Rest
{
	public static class RESTUtilities
	{
		private const string JsonContentType = "application/json";

		public static void SendResponse(this HttpResponse response, HttpStatusCode statusCode, string text = null)
		{
			var status = statusCode.ToString();
			text = text ?? status;
			response.Clear();
			response.ContentType = JsonContentType;
			response.StatusCode = statusCode.ToInt();
			response.BufferOutput = true;
			response.AddHeader("Content-Type", JsonContentType);

			//response.AddHeader("Content-Length", text.Length.ToString());
			//response.Write(text.GetString());


			var data = text.ToUtf8Bytes();
			response.AddHeader("Content-Length", data.Length.ToString());
			response.BinaryWrite(data);

			response.Flush();
			//response.End();
			//HttpContext.Current.ApplicationInstance.CompleteRequest();
		}


		public static void SendSuccessfulyResponse(this HttpResponse response, string message)
		{
			SendResponse(response, HttpStatusCode.OK, message);
		}

		public static void SendErrorResponse(this HttpResponse response, string message)
		{
			SendResponse(response, HttpStatusCode.BadRequest, message);
		}
	}
}