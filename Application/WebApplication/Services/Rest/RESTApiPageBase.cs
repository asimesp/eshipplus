﻿using System;
using System.Web.UI;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Extern.Ws;
using LogisticsPlus.Eship.Processor.Handlers.Connect;
using LogisticsPlus.Eship.WebApplication.Utilities;
using Newtonsoft.Json;

namespace LogisticsPlus.Eship.WebApplication.Services.Rest
{
	public class RESTApiPageBase : Page
	{
		public User ActiveUser { get; set; }

		protected readonly RESTTransmissionLogManager _logManager = new RESTTransmissionLogManager();

		protected string JSON = string.Empty;


		protected string GetAuthentication()
		{
			try
			{
				// Expected: Authentication JSON as base64string from UTF8 byte array
				var authentication = Request.Headers["eShipPlusAuth"];
				if (string.IsNullOrWhiteSpace(authentication)) return JsonConvert.SerializeObject(new WSAuthentication());
				return Convert.FromBase64String(authentication).FromUtf8Bytes();
			}
			catch (Exception ex)
			{
				ErrorLogger.LogError(ex, Context);
				return JsonConvert.SerializeObject(new WSAuthentication());
			}
		}
	}
}