﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Extern.Ws;
using LogisticsPlus.Eship.WebApplication.Utilities;
using Newtonsoft.Json;

namespace LogisticsPlus.Eship.WebApplication.Services.Rest
{
	public partial class GetTimes : RESTApiPageBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
                var authData = GetAuthentication();

				string streamData;
				using (var reader = new StreamReader(Request.InputStream))
					streamData = reader.ReadToEnd();
			    _logManager.Log(streamData, RESTTransmissionSource.GetTimesRequest,
			                    authenticationData: WSUtilitiesV4.ObscureRESTPassword(authData));

				var messages = new List<WSMessage>();
				ActiveUser = JsonConvert.DeserializeObject<WSAuthentication>(authData).IsValidUser(messages);
				if (messages.ContainsErrors())
				{
					JSON = JsonConvert.SerializeObject(messages.ToMessageReturnCollection<WSTime2>());
					Response.SendErrorResponse(JSON);
					_logManager.Log(JSON, RESTTransmissionSource.GetTimesResponse, ActiveUser);
				    return;
				}

				EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tMethod:{4}\t IP:{5}",
															ActiveUser.Tenant.Name, ActiveUser.FirstName, ActiveUser.LastName, ActiveUser.Username, 
															"REST API: Get Times", HttpContext.Current.Request.UserHostAddress);

				JSON = JsonConvert.SerializeObject(eShipPlusServiceBase.FetchTimes());
				_logManager.Log(JSON, RESTTransmissionSource.GetTimesResponse, ActiveUser);
				Response.SendSuccessfulyResponse(JSON);
			}
			catch (Exception ex)
			{
				ErrorLogger.LogError(ex, Context);
				JSON = JsonConvert.SerializeObject(
					new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = ex.Message } }
						.ToMessageReturnCollection<WSTime2>());
				Response.SendErrorResponse(JSON);
				_logManager.Log(JSON, RESTTransmissionSource.GetTimesResponse, ActiveUser);
			}
		}
	}
}