﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Extern.Ws;
using LogisticsPlus.Eship.WebApplication.Utilities;
using Newtonsoft.Json;

namespace LogisticsPlus.Eship.WebApplication.Services.Rest
{
	public partial class RetrievePONumber : RESTApiPageBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
                var authData = GetAuthentication();

				string streamData;
				using (var reader = new StreamReader(Request.InputStream))
					streamData = reader.ReadToEnd();
                _logManager.Log(streamData, RESTTransmissionSource.RetrievePONumberRequest, authenticationData: WSUtilitiesV4.ObscureRESTPassword(authData));

				var poNumber = JsonConvert.DeserializeObject<string>(streamData);

				var messages = new List<WSMessage>();

                ActiveUser = JsonConvert.DeserializeObject<WSAuthentication>(authData).IsValidUser(messages);
				if (messages.ContainsErrors())
				{
					JSON = JsonConvert.SerializeObject(messages.ToMessageReturn<WSCustomerPONumber>());
					Response.SendErrorResponse(JSON);
					_logManager.Log(JSON, RESTTransmissionSource.RetrievePONumberResponse, ActiveUser);
				    return;
				}

				EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tMethod:{4}\t IP:{5}", ActiveUser.Tenant.Name, ActiveUser.FirstName, ActiveUser.LastName, ActiveUser.Username, "REST API: Retrieve PO Number", HttpContext.Current.Request.UserHostAddress);

                JSON = JsonConvert.SerializeObject(eShipPlusServiceBase.FetchPONumber(ActiveUser, poNumber));
				_logManager.Log(JSON, RESTTransmissionSource.RetrievePONumberResponse, ActiveUser);
				Response.SendSuccessfulyResponse(JSON);
			}
			catch (Exception ex)
			{
				ErrorLogger.LogError(ex, Context);
				JSON = JsonConvert.SerializeObject(
					new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = ex.Message } }
						.ToMessageReturn<WSCustomerPONumber>());
				Response.SendErrorResponse(JSON);
				_logManager.Log(JSON, RESTTransmissionSource.RetrievePONumberResponse, ActiveUser);
			}
		}
	}
}