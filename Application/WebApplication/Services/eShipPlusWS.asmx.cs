﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Extern.Ws;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Services
{
	/// <summary>
	/// Summary description for eShipPlusWS
	/// </summary>
	[WebService(Namespace = "http://www.eshipplus.com/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
	[System.Web.Script.Services.ScriptService]
	public class eShipPlusWS : WebService
	{
		#region Field options i.e. Packaging, Services, Freight Classes, Commodity Types, cities, countries

		[WebMethod(Description = "Returns List of Shipping Services available per mode(s) selected.")]
		public WSOptionsReturn GetAvailableServices(WSCredentials credentials, WSEnums.Mode[] modes)
		{
			var opReturn = new WSOptionsReturn();
			try
			{
				var messages = new List<WSMessage>();

				var user = credentials.CheckExistingValidUser(messages);
				opReturn.Messages = messages.ToArray();
				if (opReturn.ContainsErrorMessage) return opReturn;

				var services = new ServiceSearch().FetchServicesByCategory(ServiceCategory.Accessorial, user.TenantId);
				var list = services.Select(service => new WSOption { Key = service.Code, Value = service.Description }).ToList();
				if (list.Count == 0)
					messages.Add(WSMessage.InformationMessage("No services available for selected mode(s)."));

				opReturn.Messages = messages.ToArray();
				opReturn.Options = list.ToArray();
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				opReturn.Messages = new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } };
			}
			return opReturn;
		}

		[WebMethod(Description = "Returns list of item commodify types available.")]
		public WSOptionsReturn GetCommodityTypes(WSCredentials credentials)
		{
			var opReturn = new WSOptionsReturn();
			try
			{
				var messages = new List<WSMessage>();

				credentials.CheckExistingValidUser(messages);
				opReturn.Messages = messages.ToArray();
				if (opReturn.ContainsErrorMessage) return opReturn;

				return new WSOptionsReturn { Options = new WSOption[0] };
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				opReturn.Messages = new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } };
			}
			return opReturn;
		}

		[WebMethod(Description = "Returns list of LTL freight classes supported by service.")]
		public WSOptionsReturn GetLTLFreightClasses(WSCredentials credentials)
		{
			var opReturn = new WSOptionsReturn();
			try
			{
				var messages = new List<WSMessage>();

				credentials.CheckExistingValidUser(messages);
				opReturn.Messages = messages.ToArray();
				if (opReturn.ContainsErrorMessage) return opReturn;

				var freightClasses = WebApplicationSettings.FreightClasses;
				opReturn.Options = new WSOption[freightClasses.Count];
				for (int i = 0; i < opReturn.Options.Length; i++)
					opReturn.Options[i] = new WSOption { Key = freightClasses[i].ToString(), Value = freightClasses[i].ToString() };
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				opReturn.Messages = new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } };
			}
			return opReturn;
		}

		[WebMethod(Description = "Returns list of item package types available for selected mode(s).")]
		public WSOptionsReturn GetItemPackaging(WSCredentials credentials, WSEnums.Mode[] modes)
		{
			var opReturn = new WSOptionsReturn();
			try
			{
				var messages = new List<WSMessage>();

				var user = credentials.CheckExistingValidUser(messages);
				opReturn.Messages = messages.ToArray();
				if (opReturn.ContainsErrorMessage) return opReturn;

                var packageTypes = ProcessorVars.RegistryCache[user.TenantId].PackageTypes;
				var list = packageTypes.Select(packageType => new WSOption { Key = packageType.Id.ToString(), Value = packageType.TypeName }).ToList();
				if (list.Count == 0)
					messages.Add(WSMessage.InformationMessage("No package types available for selected mode(s)."));

				opReturn.Messages = messages.ToArray();
				opReturn.Options = list.ToArray();
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				opReturn.Messages = new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } };
			}
			return opReturn;
		}

		[WebMethod(Description = "Returns list of serviced cities per country requested")]
		public WSOptionsReturn GetCities(WSCredentials credentials, string countryCode)
		{
			var opReturn = new WSOptionsReturn();
			try
			{
				var messages = new List<WSMessage>();

				credentials.CheckExistingValidUser(messages);
				opReturn.Messages = messages.ToArray();
				if (opReturn.ContainsErrorMessage) return opReturn;

				var list = new List<WSOption>();

				if (list.Count == 0)
					messages.Add(WSMessage.InformationMessage("No cities found for country selected!"));

				opReturn.Messages = messages.ToArray();
				opReturn.Options = list.ToArray();
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				opReturn.Messages = new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } };
			}
			return opReturn;
		}

		[WebMethod(Description = "Returns list of serviced countries")]
		public WSOptionsReturn GetCountries(WSCredentials credentials)
		{
			var opReturn = new WSOptionsReturn();
			try
			{
				var messages = new List<WSMessage>();

				credentials.CheckExistingValidUser(messages);
				opReturn.Messages = messages.ToArray();
				if (opReturn.ContainsErrorMessage) return opReturn;

                var countries = ProcessorVars.RegistryCache.Countries.Values
                    .OrderByDescending(t => t.SortWeight)
                    .ThenBy(t => t.Name)
                    .ToList();
				opReturn.Options = new WSOption[countries.Count];
				for (int i = 0; i < opReturn.Options.Length; i++)
					opReturn.Options[i] = new WSOption { Key = countries[i].Code, Value = countries[i].Name };
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				opReturn.Messages = new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } };
			}
			return opReturn;
		}

		[WebMethod(Description = "Returns list of serviced countries requiring postal codes")]
		public WSOptionsReturn GetCountriesRequiringPostalCodes(WSCredentials credentials)
		{
			var opReturn = new WSOptionsReturn();
			try
			{
				var messages = new List<WSMessage>();

				credentials.CheckExistingValidUser(messages);
				opReturn.Messages = messages.ToArray();
				if (opReturn.ContainsErrorMessage) return opReturn;

                var countries = ProcessorVars.RegistryCache.Countries.Values
                    .OrderByDescending(t => t.SortWeight)
                    .ThenBy(t => t.Name);
				opReturn.Options = countries
					.Where(c => c.EmploysPostalCodes)
					.Select(country => new WSOption { Key = country.Code, Value = country.Name })
					.ToArray();
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				opReturn.Messages = new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } };
			}
			return opReturn;
		}

		#endregion

		#region Shipment Rating, Booking

		[WebMethod(Description = "Returns shipment rates available for each specified shipment in the array.")]
		public WSShipmentReturn[] GetMultipleShippingRates(WSShipment[] shipments)
		{
			return new[]
			       	{
			       		new WSShipmentReturn
			       			{
			       				Messages = new[] {WSMessage.ErrorMessage("Temporarily Disabled Feature!")}
			       			}
			       	};
		}

		[WebMethod(Description = "Returns shipment rates available for specified shipment.")]
		public WSShipmentReturn GetShippingRates(WSShipment shipment)
		{
			var shipReturn = new WSShipmentReturn();
			if (shipment == null)
			{
				shipReturn.Messages = new[] { WSMessage.ErrorMessage("Missing shipment object.") };
				return shipReturn;
			}

			try
			{
				var messages = new List<WSMessage>();

				shipment.Credentials.CheckExistingValidUser(messages);
				shipReturn.Messages = messages.ToArray();
				if (shipReturn.ContainsErrorMessage) return shipReturn;


				var shipToRate = shipment.GetShipmentToRate(messages);
				shipReturn.Messages = messages.ToArray();
				if (shipReturn.ContainsErrorMessage) return shipReturn;

				var rates = shipToRate.GetShippingRates(shipment.RequestedModes, Context);
				shipment.AvailableRates = shipToRate.GetWsRates(rates, Context).ToArray();
				shipToRate.CreateAndShoppedRate(shipment.AvailableRates.ToList(), ShoppedRateType.WSEstimate, Context);

				shipReturn.Shipment = shipment;
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				shipReturn.Messages = new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } };
			}
			return shipReturn;
		}

		[WebMethod(Description = "Books each shipment returning confirmations on booking processes.")]
		public WSShipmentReturn[] BookMultipleShipments(WSShipment[] shipments)
		{
			return new[]
			       	{
			       		new WSShipmentReturn
			       			{
			       				Messages = new[] {WSMessage.ErrorMessage("Temporarily Disabled Feature!")}
			       			}
			       	};
		}

		[WebMethod(Description = "Books shipment returning confirmations on booking processes.")]
		public WSShipmentReturn BookShipment(WSShipment shipment)
		{
			var shipReturn = new WSShipmentReturn();
			if (shipment == null)
			{
				shipReturn.Messages = new[] { WSMessage.ErrorMessage("Missing shipment object.") };
				return shipReturn;
			}

			//shipReturn.Messages = new[] {WSMessage.ErrorMessage("Booking has been disabled! Please see System Administrator")};
			//return shipReturn;

			try
			{
				var messages = new List<WSMessage>();

				shipment.Credentials.CheckExistingValidUser(messages);
				shipReturn.Messages = messages.ToArray();
				if (shipReturn.ContainsErrorMessage) return shipReturn;

				//re-do re-rate process
				var s = shipment.GetShipmentToRate(messages);
				shipReturn.Messages = messages.ToArray();
				if (shipReturn.ContainsErrorMessage) return shipReturn;

				var rates = s.GetShippingRates(shipment.RequestedModes, Context);
				shipment.AvailableRates = s.GetWsRates(rates, Context).ToArray();

				// create and save quote
				var shoppedRate = s.CreateAndShoppedRate(shipment.AvailableRates.ToList(), ShoppedRateType.WSBooking, Context);

				// update shipment for booking process!
				s.UpdateShipmentToBook(shipment, rates, messages, Context);
				shipReturn.Messages = messages.ToArray();
				if (shipReturn.ContainsErrorMessage) return shipReturn;

				// finalize shipment booking
				s.BookShipment(shoppedRate, messages, Context);
				shipReturn.Messages = messages.ToArray();
				if (shipReturn.ContainsErrorMessage) return shipReturn;

				shipment.ReturnConfirmations = s.Documents
					.Select(d => new WSDocument
					             	{
					             		DocType = Context.Server.GetFileExtension(d.LocationPath).GetDocumentType(),
					             		DocImage = Context.Server.ReadFromFile(d.LocationPath),
					             		Name = d.Name
					             	})
					.ToArray();

				shipment.BookedShipmentNumber = s.ShipmentNumber;

				shipReturn.Messages = messages.ToArray();
				shipReturn.Shipment = shipment;
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				shipReturn.Messages = new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } };
			}
			return shipReturn;
		}

		#endregion	

		#region Shipment Status & Pro Number Retrieval

		[WebMethod(Description = "Retrieves shipment status and pro number (i.e. tracking number) for list of shipments specified")]
		public WSStatusReturn GetShipmentStatus(WSCredentials credentials, string[] shipments)
		{
			var statusReturn = new WSStatusReturn();
			try
			{
				var messages = new List<WSMessage>();

				var user = credentials.CheckExistingValidUser(messages);
				statusReturn.Messages = messages.ToArray();
				if (statusReturn.ContainsErrorMessage) return statusReturn;

				if (shipments == null || shipments.Length == 0)
				{
					statusReturn.Messages = new[] { WSMessage.ErrorMessage("No shipments to check.") };
					return statusReturn;
				}

				var search = new ShipmentSearch();
				var statuses = new List<WSStatus>();
				foreach (var number in shipments)
					if (!string.IsNullOrEmpty(number))
					{
						var shipment = search.FetchShipmentByShipmentNumber(number, user.TenantId);
						if (shipment == null)
							messages.Add(WSMessage.ErrorMessage(string.Format("Shipment [{0}] not found.", number)));
						else
						{
							var vendor = shipment.Vendors.FirstOrDefault(v => v.Primary);
							statuses.Add(new WSStatus
							{
								BillOfLading = shipment.ShipmentNumber,
								ProNumber = vendor == null ? string.Empty : vendor.ProNumber,
								Status = shipment.Status.FormattedString(),
								DeliveryDate = shipment.ActualDeliveryDate != DateUtility.SystemEarliestDateTime
												? shipment.ActualDeliveryDate.FormattedLongDateAlt()
												: string.Empty
							});
						}
					}
					else
						messages.Add(WSMessage.ErrorMessage(string.Format("Invalid shipment [{0}].", number)));

				statusReturn.Messages = messages.ToArray();
				statusReturn.Statuses = statuses.ToArray();
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				statusReturn.Messages = new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } };
			}
			return statusReturn;
		}

		#endregion
	}
}
