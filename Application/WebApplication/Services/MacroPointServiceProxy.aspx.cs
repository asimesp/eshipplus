﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.MacroPoint;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.Utilities.Que;
using LogisticsPlus.Eship.WebApplication.Notifications;

namespace LogisticsPlus.Eship.WebApplication.Services
{
    public partial class MacroPointServiceProxy : MacroPointServiceProxyPageBase
    {
        private const string Ellipses = "...";

        private const string ImageDataUri = "data:image/jpeg;base64,";

        private const int CallNotesLengthLimit = 490;


        public override MacroPointSettings GetSettings(Tenant tenant)
        {
            return ProcessorVars.IntegrationVariables[tenant.Id].MacroPointParams;
        }

        public override Tenant GetTenant(string idNumber, string orderId)
        {
            var ids = new MacroPointOrderSearch().FetchTenantIdsByIdNumAndOrderId(orderId, idNumber);

            // finding more than one records proves that Macro Point Order Id is not unique across their system. It is expected to be unique but no documentation confirms this!
            return !ids.Any() || ids.Count > 1 ? null : new Tenant(ids[0]);
        }

        public override void SaveMacroPointOrder(MacroPointOrder order)
        {
            Exception ex;
            new MacroPointOrderUpdateHandler().SaveMacroPointOrder(order, out ex);
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public override void SaveShipmentCheckCalls(List<CheckCall> checkCalls)
        {
            var shipment = checkCalls[0].Shipment;
            shipment.LoadCollections();
            shipment.CheckCalls.AddRange(checkCalls);
            Exception ex;

            var errs = new ShipmentUpdateHandler().UpdateShipmentTrackingInfo(shipment, out ex);
            if (ex != null) ErrorLogger.LogError(ex, Context);
            if (ex == null && errs.Any())
            {
                var handler = new ProcQueObjProcessHandler();
                foreach (var cc in checkCalls.Where(c => c.IsNew))
                {
                    var obj = new ProcQueObj
                        {
                            ObjType = ProcQueObjType.ShipmentStatusQue,
                            Xml = new ShipmentStatusUpdateQue(cc).ToXml(),
                            DateCreated = DateTime.Now
                        };
                    handler.Save(obj, out ex);
                    if (ex != null) ErrorLogger.LogError(ex, Context);
                }
            }
            if (!errs.Any()) ProcessNotificationsForCheckCalls(checkCalls);
        }

        public override void ProcessMacroPointForm(Form form)
        {
            var tenant = GetTenant(form.LoadId, form.MpOrderId);
            if (tenant == null) return;

            var orderDto = new MacroPointOrderSearch().FetchMacroPointOrderByIdNumber(form.LoadId, tenant.Id);
            var order = new MacroPointOrder(orderDto != null ? orderDto.Id : default(long));

            if (!order.KeyLoaded) return;

            var shipment = order.RetrieveCorrespondingShipment();

            if (shipment.IsNew) return;

            shipment.LoadCollections();

            var formItems = form.FormItems.Select(fi => fi as FormItem);

            // create check calls from text fields in form
            var checkCalls = new List<CheckCall>();
            var textFormItems = formItems.Where(fi => fi.Type == FormItemType.Text.ToString());

            if (textFormItems.Any())
            {
                var callNotes = string.Format("Form: {0} - ", form.FormName);

                foreach (var formItem in textFormItems)
                    callNotes += string.Format("{0}: {1}; ", formItem.Name, formItem.Value);

                var callNotesList = new List<string>();

                // split up call notes into size that fits into CallNotes field
                if (callNotes.Length > CallNotesLengthLimit)
                {
                    while (callNotes.Length > CallNotesLengthLimit)
                    {
                        var lastSpaceIndex = callNotes.Substring(0, CallNotesLengthLimit).LastIndexOf(" ", StringComparison.Ordinal);
                        callNotesList.Add(callNotes.Substring(0, lastSpaceIndex) + Ellipses);
                        callNotes = Ellipses + callNotes.Substring(lastSpaceIndex);
                    }
                    
                }
                callNotesList.Add(callNotes);

                checkCalls = callNotesList.Select(callNote => new CheckCall
                    {
                        CallDate = DateTime.Now,
                        CallNotes = callNote,
                        EdiStatusCode = string.Empty,
                        EventDate = form.DateInUtc.ToDateTime(),
                        Shipment = shipment,
                        TenantId = shipment.TenantId,
                        User = shipment.Tenant.DefaultSystemUser,
                    }).ToList();
            }


            shipment.CheckCalls.AddRange(checkCalls);

            // create shipment document for each image field in form
            var imageFormItems = formItems.Where(i => i.Type == FormItemType.Image.ToString()).ToArray();

            var documents = new List<ShipmentDocument>();

            foreach (var formItem in imageFormItems)
            {
                var bytes = Convert.FromBase64String(formItem.Value.Replace(ImageDataUri, string.Empty));
                if (bytes.Length == 0) continue;

                var shipmentFolder = WebApplicationSettings.ShipmentFolder(shipment.TenantId, shipment.Id);
                var filePath = Path.Combine(shipmentFolder, string.Format("{0}.jpg", formItem.Name.FormattedString()));
                var fi = new FileInfo(Server.MakeUniqueFilename(filePath)); // ensure unique filename

                var documentTag = shipment.Tenant.PodDocumentTag;
                if (documentTag == null) continue;

                var document = new ShipmentDocument
                {
                    TenantId = shipment.TenantId,
                    Shipment = shipment,
                    Description = string.Format("{0} {1}", shipment.ShipmentNumber, documentTag.Code),
                    DocumentTag = documentTag,
                    IsInternal = false,
                    LocationPath = Path.Combine(shipmentFolder, fi.Name),
                    Name = fi.Name,
                };
                
                // write to server and add to list for save
                bytes.WriteToFile(Server.MapPath(shipmentFolder), fi.Name, string.Empty);

                documents.Add(document);
            }

            shipment.Documents.AddRange(documents);

            // save the shipment
            Exception ex;
            var errs = new ShipmentUpdateHandler().UpdateShipmentTrackingInfo(shipment, out ex);
            if (ex != null)
            {
                ErrorLogger.LogError(ex, Context);
                foreach (var document in documents)
                    Server.RemoveFiles(new[] { document.LocationPath });
            }

            if (ex == null && errs.Any())
            {
                var handler = new ProcQueObjProcessHandler();
                foreach (var cc in checkCalls.Where(c => c.IsNew))
                {
                    var obj = new ProcQueObj
                    {
                        ObjType = ProcQueObjType.ShipmentStatusQue,
                        Xml = new ShipmentStatusUpdateQue(cc).ToXml(),
                        DateCreated = DateTime.Now
                    };
                    handler.Save(obj, out ex);
                    if (ex != null) ErrorLogger.LogError(ex, Context);
                }
                foreach (var document in documents.Where(d => d.IsNew))
                {
                    var obj = new ProcQueObj
                    {
                        ObjType = ProcQueObjType.ShipmentDocumentQue,
                        Xml = new ShipmentDocumentQue(document).ToXml(),
                        DateCreated = DateTime.Now
                    };
                    handler.Save(obj, out ex);
                    if (ex != null) ErrorLogger.LogError(ex, Context);
                }
            }

            if (!errs.Any()) ProcessNotificationsForCheckCalls(checkCalls);
        }


        private void ProcessNotificationsForCheckCalls(IEnumerable<CheckCall> checkCalls)
        {
            var checkCallsToUpdate = checkCalls
                .Where(c => c.Shipment.Customer.Communication != null && (c.Shipment.Customer.Communication.EdiEnabled || c.Shipment.Customer.Communication.FtpEnabled))
                .Where(c => c.Shipment.Customer.Communication.Notifications.Any(n => Milestone.ShipmentCheckCallUpdate == n.Milestone && n.Enabled))
                .Where(c => !string.IsNullOrEmpty(c.EdiStatusCode))
                .ToList();
            if (!checkCallsToUpdate.Any()) return;

            var shipment = checkCallsToUpdate.First().Shipment;

            Server.NotifyCheckCallUpdateEdiFtpOnly(shipment, checkCallsToUpdate, shipment.Tenant.DefaultSystemUser);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            // all processing handled in ProxyPageBase
        }
    }
}