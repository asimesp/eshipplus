﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Dat;
using LogisticsPlus.Eship.Dat.Responses;
using LogisticsPlus.Eship.Extern.Ws;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Handlers.Cms;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Connect;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;
using Newtonsoft.Json;
using Shipment = LogisticsPlus.Eship.Core.Operations.Shipment;

namespace LogisticsPlus.Eship.WebApplication.Services
{
	/// <summary>
	/// Summary description for eShipPlusWSv4P
	/// </summary>
	[WebService(Namespace = "http://www.eshipplus.com")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
	[ScriptService]
	public class eShipPlusWSv4P : WebService
	{
		//NOTE: KEEP METHODS A-Z GROUPED THEN A-Z WITHIN GROUP

		[WebMethod(Description = "Returns List of Available Booking Requests.")]
		public WSAvailableLoadsReturn GetAvailableLoads(WSCredentials credentials)
		{
			var alReturn = new WSAvailableLoadsReturn();
			try
			{
				var messages = new List<WSMessage>();

				var user = credentials.CheckExistingValidUser(messages);
				alReturn.Messages = messages.ToArray();
				if (alReturn.ContainsErrorMessage) return alReturn;

				if (!user.HasAccessTo(ViewCode.AvailableLoads))
				{
					alReturn.Messages = new[] { WSMessage.ErrorMessage("User account is not authorized for this function.") };
					return alReturn;
				}

				var loads = new LoadOrderSearch().FetchAvailableLoads(user.TenantId);
				alReturn.AvailableLoads = new WSAvailableLoad[loads.Count];

				alReturn.AvailableLoads = loads
					.Select(r => new WSAvailableLoad
					{
						LoadNumber = r.LoadOrderNumber,
						PickUpDate = r.DesiredPickupDate,
						DeliveryDate = r.EstimatedDeliveryDate,
						OriginCity = r.Origin.City,
						OriginState = r.Origin.State,
						DestinationCity = r.Destination.City,
						DestinationState = r.Destination.State,
						EquipmentTypes = r.Equipments.Aggregate(string.Empty, (a, id) => a + string.Format("{0}, ", id.EquipmentType.FormattedString())),
						ContactPhone = r.User.Phone,
						Comments = r.Description,
						Items = r.Items.Select(bi => new WSItem
						{
							Weight = bi.Weight.ToDouble(),
							WeightUnit = WSEnums.WeightUnit.Lb,
							Quantity = bi.Quantity,
							Height = Math.Ceiling(bi.Height).ToInt(),
							Width = Math.Ceiling(bi.Width).ToInt(),
							Length = Math.Ceiling(bi.Length).ToInt(),
							DimsUnit = WSEnums.DimensionUnit.In,
							IsStackable = bi.IsStackable,
							DeclaredValue = bi.Value,
							Description = bi.Description,
							NationalMotorFreightClassification = bi.NMFCCode,
							HarmonizedTariffSchedule = bi.HTSCode,
							SaidToContain = bi.PieceCount,
							Packaging = new WSOption { Key = bi.PackageTypeId.ToString(), Value = bi.PackageType.TypeName },
							FreightClass = new WSOption { Key = bi.FreightClass.ToString(), Value = bi.FreightClass.ToString() },
							CommodityType = null
						}).ToArray()
					})
					.ToArray();
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				alReturn.Messages = new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } };
			}
			return alReturn;
		}


		
		[WebMethod(Description = "Update single fax transmission record")]
		public WSReturn UpdateFaxTransmission(string key, string message, FaxTransmissionStatus status)
		{
			try
			{
				var t = new FaxTransmissionSearch().FetchFaxTransmission(new Guid(key));

				if (t == null)
					return new WSReturn { Messages = new[] { WSMessage.ErrorMessage("No fax transmission record for key {0} found!", key) } };

				// update
				t.Message = string.Format("{0} {1}", t.Message, message);
				if (t.Message.Length > 200) t.Message = t.Message.Substring(0, 200);
				t.Status = status;
				if(t.ResponseDateTime == DateUtility.SystemEarliestDateTime) t.ResponseDateTime = DateTime.Now;
				t.LastStatusCheckDateTime = DateTime.Now;

				var msgs = new ShipmentFaxTransmissionHandler().SaveFaxTransmission(t);
				return new WSReturn { Messages = msgs.Select(WSMessage.Message).ToArray() };
			}
			catch (Exception e)
			{
				return new WSReturn { Messages = new[] { WSMessage.ErrorMessage(e.Message) } };
			}
		}

		[WebMethod(Description = "Update multiple fax transmission record")]
		public WSFaxTransmissionUpdateReturn[] UpdateFaxTransmissions(WSFaxTransmissionUpdate[] updates)
		{
			try
			{
				var search = new FaxTransmissionSearch();
				var rets = new List<WSFaxTransmissionUpdateReturn>();
				var handler = new ShipmentFaxTransmissionHandler();

				foreach (var update in updates)
				{
					var t = search.FetchFaxTransmission(new Guid(update.Key));

					if (t == null)
					{
						rets.Add(new WSFaxTransmissionUpdateReturn { Messages = new[] { WSMessage.ErrorMessage("No fax transmission record for key {0} found!", update.Key) } });
						continue;
					}

					// update
					t.Message = string.Format("{0} {1}", t.Message, update.Message);
					if (t.Message.Length > 200) t.Message = t.Message.Substring(0, 200);
					t.Status = update.Status;
					if (t.ResponseDateTime == DateUtility.SystemEarliestDateTime) t.ResponseDateTime = DateTime.Now;
					t.LastStatusCheckDateTime = DateTime.Now;


					var msgs = handler.SaveFaxTransmission(t);
					rets.Add(new WSFaxTransmissionUpdateReturn { Messages = msgs.Select(WSMessage.Message).ToArray(), TransmissionUpdate = update });
				}

				return rets.ToArray();
			}
			catch (Exception e)
			{
				return new[] { new WSFaxTransmissionUpdateReturn { Messages = new[] { WSMessage.ErrorMessage(e.Message) } } };
			}
		}

		

		[WebMethod(Description = "Returns List of Tracking Info Requests for tracking.")]
		public WSTrackingInfoRequestReturn GetTrackingInfoRequests(WSCredentials credentials, string scac, DateTime cutoffDate)
		{
			var trackingInfoRequestReturn = new WSTrackingInfoRequestReturn();
			try
			{
				var messages = new List<WSMessage>();

				var user = credentials.CheckExistingValidUser(messages);
				trackingInfoRequestReturn.Messages = messages.ToArray();
				if (trackingInfoRequestReturn.ContainsErrorMessage) return trackingInfoRequestReturn;

				if (!user.HasAccessTo(ViewCode.Shipment))
				{
					trackingInfoRequestReturn.Messages = new[] { WSMessage.ErrorMessage("User account is not authorized for this function.") };
					return trackingInfoRequestReturn;
				}

				trackingInfoRequestReturn.TrackingInfoRequests = new TrackingInfoRequestDto()
					.FetchTrackingInfoRequests(scac, cutoffDate.TimeToMinimum(), user.TenantId).Select(t => t.ToWSTrackingInfoRequest())
					.ToArray();
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				trackingInfoRequestReturn.Messages = new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } };
			}
			return trackingInfoRequestReturn;
		}

		[WebMethod(Description = "Receives Shipment Tracking Data for Updating.")]
		public WSReturn UpdateTrackingInfoRequests(WSCredentials credentials, WSShipmentTrackingInfo shipmentTrackingInfo)
		{
			var ret = new WSReturn();
			try
			{
				var messages = new List<WSMessage>();

				var user = credentials.CheckExistingValidUser(messages);
				ret.Messages = messages.ToArray();
				if (ret.ContainsErrorMessage) return ret;

				if (!user.HasAccessTo(ViewCode.Shipment) || !user.RetrievePermission(ViewCode.Shipment).Modify)
				{
					ret.Messages = new[] { WSMessage.ErrorMessage("User account is not authorized for this function.") };
					return ret;
				}

                // find shipment
			    var shipment = new ShipmentSearch()
			        .FetchShipmentByShipmentNumber(shipmentTrackingInfo.ShipmentNumber, user.TenantId);

                if(shipment == null)
                {
                    ret.Messages = new[] {WSMessage.ErrorMessage("Shipment record not found.")};
                    return ret;
                }

                // update shipment record
                shipment.TakeSnapShot();
				var shipmentDocRtrvLogRequiredIfApplicable = false;
			    var vendor = shipment.Vendors.FirstOrDefault(sv => sv.Vendor.Scac == shipmentTrackingInfo.Scac);
                if (vendor != null && vendor.Primary)
                {
                    if (shipmentTrackingInfo.PickupDatePresent)
                        shipment.ActualPickupDate = shipmentTrackingInfo.PickUpDate;
                    if (shipmentTrackingInfo.DeliveryDatePresent)
                    {
	                    if (shipment.ActualDeliveryDate != shipmentTrackingInfo.DeliveryDate) shipmentDocRtrvLogRequiredIfApplicable = true;
	                    shipment.ActualDeliveryDate = shipmentTrackingInfo.DeliveryDate;
                    }
                    shipment.Status = shipment.InferShipmentStatus(false);
                }
                if (vendor != null)
                {
                    vendor.TakeSnapShot();
                    if (shipmentTrackingInfo.ProNumberIsPresent)
                    {
						if (vendor.ProNumber != shipmentTrackingInfo.ProNumber) shipmentDocRtrvLogRequiredIfApplicable = true;
	                    vendor.ProNumber = shipmentTrackingInfo.ProNumber;
                    }
                }

				// Doc Rtrv Log.
				if (shipmentDocRtrvLogRequiredIfApplicable) shipment.InsertShipmentDocRtrvLogFor(user);

                // update shipment
				ret.Messages = new ShipmentUpdateHandler()
			        .UpdateShipmentTrackingInfo(shipment, user)
			        .Select(WSMessage.Message)
			        .ToArray();

			    return ret;
			}
			catch (Exception e)
			{
				return new WSReturn { Messages = new[] { WSMessage.ErrorMessage(e.Message) } };
			}
		}



		[WebMethod(Description = "Find active account bucket")]
		[ScriptMethod]
		public object GetActiveAccountBucket(long tenantId, string acctCode, object callReturnArgs)
		{
			var search = new AccountBucketSearch();
			var acctBucket = search.FetchAccountBucketByCode(acctCode.RetrieveSearchCodeFromAutoCompleteString(), tenantId);
			var msg = acctBucket == null ? "Account bucket not found!" : !acctBucket.Active ? "Account bucket is inactive!" : "";
			if (acctBucket != null && !acctBucket.Active) acctBucket = null;

            var units = acctBucket == null ? null : ProcessorVars.RegistryCache[acctBucket.TenantId].AccountBucketUnits
                .Where(a => a.AccountBucketId == acctBucket.Id && a.Active)
                .Select(u => new { u.Id, u.Name })
                .ToList();
			var instructionUnit = new {Id = default(long), Name = WebApplicationConstants.ChooseOne};
			if (units != null) units.Insert(0, instructionUnit);
			return new
			       	{
			       		Args = callReturnArgs,
			       		Code = acctBucket == null ? string.Empty : acctBucket.Code,
			       		Description = acctBucket == null ? string.Empty : acctBucket.Description,
			       		Id = acctBucket == null ? default(long) : acctBucket.Id,
						Units = units == null ? new[] { instructionUnit } : units.ToArray(),
			       		ErrorMessage = acctBucket == null ? msg : string.Empty
			       	};
		}

		[WebMethod(Description = "Find active account bucket unit")]
		[ScriptMethod] 
		public object GetAccountBucketUnits(long acctBucketId, object callReturnArgs)
		{
			var acctBucket = new AccountBucket(acctBucketId);
			var msg = !acctBucket.KeyLoaded ? "Account bucket not found!" : String.Empty;
			if (!acctBucket.KeyLoaded) acctBucket = null;

			var units = acctBucket == null ? null : ProcessorVars.RegistryCache[acctBucket.TenantId].AccountBucketUnits
                .Where(a => a.AccountBucketId == acctBucket.Id && a.Active)
                .Select(u => new { u.Id, u.Name })
                .ToList();
			var instructionUnit = new { Id = default(long), Name = WebApplicationConstants.ChooseOne };
			if (units != null) units.Insert(0, instructionUnit);
			return new
			{
				Args = callReturnArgs,
				Units = units == null ? new[] { instructionUnit } : units.ToArray(),
				ErrorMessage = acctBucket == null ? msg : string.Empty
			};
		}

		[WebMethod(Description = "Find active user designated as carrier coordinator")]
		[ScriptMethod]
		public object GetActiveCarrierCoordinatorUser(string tenantCode, string activeUsername, string searchUsername, object callReturnArgs)
		{
			var search = new UserSearch();
			var activeUser = search.FetchUserByUsernameAndAccessCode(activeUsername, tenantCode);
            var user = search.FetchUserByUsernameAndAccessCode(searchUsername.RetrieveSearchCodeFromAutoCompleteString(), tenantCode);

			var msg = string.Empty;
			if (user == null)
			{
				msg = "User not found!";
			}
			else if (!user.Enabled)
			{
				msg = "User account is disabled!";
				user = null;
			}
			else if (!user.IsCarrierCoordinator)
			{
				msg = "User is not a carrier coordinator!";
				user = null;
			}
			else if (activeUser == null)
			{
				msg = "Unable to verify active user!";
				user = null;
			}
			else if (!activeUser.TenantEmployee && !user.TenantEmployee)
			{
				if (activeUser.DepartmentId == default(long))
				{
					msg = "Active user department is invalid.  Cannot load coordinator as a result!";
					user = null;
				}
				else if(activeUser.DepartmentId != user.DepartmentId)
				{
					msg = "Active user and selected user do not belong to same department.  Cannot load coordinator as a result!";
					user = null;
				}
			}

			return new
			{
				Args = callReturnArgs,
				Username = user == null ? string.Empty : user.Username,
				FullName = user == null ? string.Empty : string.Format("{0} {1}", user.FirstName, user.LastName),
				Id = user == null ? default(long) : user.Id,
				ErrorMessage = user == null ? msg : string.Empty
			};
		}

		[WebMethod(Description = "Find active user designated as shipment coordinator")]
		[ScriptMethod]
		public object GetActiveShipmentCoordinatorUser(string tenantCode, string activeUsername, string searchUsername, object callReturnArgs)
		{
			var search = new UserSearch();
			var activeUser = search.FetchUserByUsernameAndAccessCode(activeUsername, tenantCode);
            var user = search.FetchUserByUsernameAndAccessCode(searchUsername.RetrieveSearchCodeFromAutoCompleteString(), tenantCode);

			var msg = string.Empty;
			if (user == null)
			{
				msg = "User not found!";
			}
			else if (!user.Enabled)
			{
				msg = "User account is disabled!";
				user = null;
			}
			else if (!user.IsShipmentCoordinator)
			{
				msg = "User is not a shipment coordinator!";
				user = null;
			}
			else if (activeUser == null)
			{
				msg = "Unable to verify active user!";
				user = null;
			}
			else if (!activeUser.TenantEmployee && !user.TenantEmployee)
			{
				if (activeUser.DepartmentId == default(long))
				{
					msg = "Active user department is invalid.  Cannot load coordinator as a result!";
					user = null;
				}
				else if (activeUser.DepartmentId != user.DepartmentId)
				{
					msg = "Active user and selected user do not belong to same department.  Cannot load coordinator as a result!";
					user = null;
				}
			}

			return new
			{
				Args = callReturnArgs,
				Username = user == null ? string.Empty : user.Username,
				FullName = user == null ? string.Empty : string.Format("{0} {1}", user.FirstName, user.LastName),
				Id = user == null ? default(long) : user.Id,
				ErrorMessage = user == null ? msg : string.Empty
			};
		}


		[WebMethod(Description = "Find active prefix")]
		[ScriptMethod]
		public object GetActivePrefix(long tenantId, string prefixCode, object callReturnArgs)
		{
			var search = new PrefixSearch();
			var prefix = search.FetchPrefixByCode(prefixCode.RetrieveSearchCodeFromAutoCompleteString(), tenantId);
			var msg = prefix == null ? "Prefix not found!" : !prefix.Active ? "Prefix is inactive!" : "";
			if (prefix != null && !prefix.Active) prefix = null;
			return new
			       	{
			       		Args = callReturnArgs,
			       		Code = prefix == null ? string.Empty : prefix.Code,
			       		Description = prefix == null ? string.Empty : prefix.Description,
			       		Id = prefix == null ? default(long) : prefix.Id,
			       		ErrorMessage = prefix == null ? msg : string.Empty
			       	};
		}

		[WebMethod(Description = "Find city and state related to postal code")]
		[ScriptMethod]
		public object GetCityAndState(string postalCode, long countryId, object callReturnArgs)
		{
			var p = new PostalCodeSearch().FetchPostalCodeByCode(postalCode, countryId);
			return new
			       	{
			       		Args = callReturnArgs,
			       		City = p == null ? string.Empty : p.City,
			       		State = p == null ? string.Empty : p.State,
			       		ErrorMessage = p == null ? "City and state not found for postal code." : string.Empty
			       	};
		}



		[WebMethod(Description = "Set the content's current status")]
		[ScriptMethod]
		public string SetContentCurrentStatus(long adminUserId, long contentId, bool status)
		{
			var user = new AdminUser(adminUserId);
			if (!user.KeyLoaded) return "Unable to process last action. Invalid user!";

			var handler = new CurrentContentUpdateHandler(user);
			Exception ex;
			var msg = handler.SetandSaveContentCurrentStatus(contentId, status, out ex);
			if (ex != null) ErrorLogger.LogError(ex, Context);

			return msg.Type == ValidateMessageType.Information ? string.Empty : msg.Message;
		}
		
		[WebMethod(Description = "Set the news's archive status")]
		[ScriptMethod]
		public string SetNewsHiddenStatus(long adminUserId, long newId, bool status)
		{
			var user = new AdminUser(adminUserId);
			if (!user.KeyLoaded) return "Unable to process last action. Invalid user!";

			var handler = new NewsHiddenStatusUpdateHandler(user);
			Exception ex;
			var msg = handler.SetAndSaveNewsArchivedStatus(newId, status, out ex);
			if (ex != null) ErrorLogger.LogError(ex, Context);

			return msg.Type == ValidateMessageType.Information ? string.Empty : msg.Message;
		}




		[WebMethod(Description = "Validate time string")]
		[ScriptMethod]
		public object DateStringIsValid(string dateString, object callReturnArgs)
		{
			var valid = dateString.IsKeywordDate()
				? dateString.ParseKeywordDate().IsValidSystemDateTime()
				: dateString.IsValidSystemDateTime();
			return new
				{
					Args = callReturnArgs,
					Valid = valid,
					ErrorMessage =
						valid
							? string.Empty
							: string.Format("Date is invalid. Value must be between {0} and {1}. [Value = {2}]",
							                DateUtility.SystemEarliestDateTime, DateUtility.SystemLatestDateTime, dateString)
				};
		}

		[WebMethod(Description = "Validate time string")]
		[ScriptMethod]
		public object TimeStringIsValid(string timeString, object callReturnArgs)
		{
			var valid = timeString.IsValidFreeFormTimeString();
			return new
			{
				Args = callReturnArgs,
				Valid = valid,
				ErrorMessage = valid ? string.Empty : string.Format("Time is invalid. Value must be between 00:00 and 23:59. [Value = {0}]", timeString)
			};
		}



		[WebMethod(Description = "Return list of account buckets for tenant (required Id in context key)")]
		[ScriptMethod]
		public string[] GetAccountBucketList(string prefixText, int count, string contextKey)
		{
			var search = new AccountBucketSearch();
			var values = contextKey.RetrieveContextKeyValues();
			var tenantId = values.ContainsKey(AutoCompleteUtility.Keys.Tenant) ? values[AutoCompleteUtility.Keys.Tenant].ToLong() : default(long);

			var criteria = RegistrySearchFields.Description.ToParameterColumn();
			criteria.Operator = prefixText.GetOperator();
			criteria.DefaultValue = prefixText.CleanPrefixText();

			var criteriaList = new List<ParameterColumn> { criteria };
			var vendors = search.FetchAccountBuckets(criteriaList, tenantId)
								  .OrderBy(c => c.Code)
								  .Select(c => c.FormatForAutoComplete())
								  .Take(count)
								  .ToArray();
			return vendors;
		}

		[WebMethod(Description = "Return list of customer visible address for user (required Id in context key)")]
		[ScriptMethod]
		public string[] GetAddressList(string prefixText, int count, string contextKey)
		{
			var dto = new AddressBookViewSearchDto();
			var values = contextKey.RetrieveContextKeyValues();
			var userId = values.ContainsKey(AutoCompleteUtility.Keys.ActiveUser) ? values[AutoCompleteUtility.Keys.ActiveUser].ToLong() : default(long);
			var customerId = values.ContainsKey(AutoCompleteUtility.Keys.Customer) ? values[AutoCompleteUtility.Keys.Customer].ToLong() : default(long);
			var tenantId = values.ContainsKey(AutoCompleteUtility.Keys.Tenant) ? values[AutoCompleteUtility.Keys.Tenant].ToLong() : default(long);

			var address = dto
				.FetchAddressBookDtoSingleCriteriaAndDefaultsSearch(tenantId, userId, customerId, prefixText.CleanPrefixText(), prefixText.GetOperator());

			var addressList = address
				.OrderBy(a => a.Description)
				.Select(a => string.Format("{0}{1}", AutoCompleteUtility.Equal, a.FormatForAutoComplete()))
				.Take(count)
				.ToArray();
			return addressList;
		}

		[WebMethod(Description = "Return list of customers visible to Active User (required Id in context key)")]
		[ScriptMethod]
		public string[] GetCustomerList(string prefixText, int count, string contextKey)
		{
			var search = new CustomerSearch();
			var values = contextKey.RetrieveContextKeyValues();
			var user = new User(values.ContainsKey(AutoCompleteUtility.Keys.ActiveUser) ? values[AutoCompleteUtility.Keys.ActiveUser].ToLong() : default(long));

			if (!user.KeyLoaded) return new string[0];

			var criteria = new CustomerViewSearchCriteria { OnlyActiveCustomers = true, };
			criteria.Parameters.Add(AccountingSearchFields.CustomerName.ToParameterColumn());
			criteria.Parameters[0].Operator = prefixText.GetOperator();
			criteria.Parameters[0].DefaultValue = prefixText.CleanPrefixText();
			var customers = user.FilterCustomersForUser(search.FetchCustomers(criteria, user.TenantId))
								  .OrderBy(c => c.Name)
								  .Select(c => c.FormatForAutoComplete())
								  .Take(count)
								  .ToArray();
			return customers;
		}

		[WebMethod(Description = "Return list of prefixes for tenant (required Id in context key)")]
		[ScriptMethod]
		public string[] GetPrefixList(string prefixText, int count, string contextKey)
		{
			var search = new PrefixSearch();
			var values = contextKey.RetrieveContextKeyValues();
			var tenantId = values.ContainsKey(AutoCompleteUtility.Keys.Tenant) ? values[AutoCompleteUtility.Keys.Tenant].ToLong() : default(long);

			var criteria = RegistrySearchFields.Description.ToParameterColumn();
			criteria.Operator = prefixText.GetOperator();
			criteria.DefaultValue = prefixText.CleanPrefixText();

			var criteriaList = new List<ParameterColumn> { criteria };
			var vendors = search.FetchPrefixes(criteriaList, tenantId)
								  .OrderBy(c => c.Code)
								  .Select(c => c.FormatForAutoComplete())
								  .Take(count)
								  .ToArray();
			return vendors;
		}

        [WebMethod(Description = "Return list of qlik user attributes (required admin user Id in context key)")]
        [ScriptMethod]
        public string[] GetQlikUserAttributeTypeList(string prefixText, int count, string contextKey)
        {
            var values = contextKey.RetrieveContextKeyValues();
            var superUserId = values.ContainsKey(AutoCompleteUtility.Keys.ActiveSuperUser)
                             ? values[AutoCompleteUtility.Keys.ActiveSuperUser].ToLong()
                             : default(long);

            var activeUser = new AdminUser(superUserId);
            if (!activeUser.KeyLoaded) return new string[0];

            var qlikUserAttributeTypes = new QlikUserSearch()
                .FetchAllDistinctQlikUserAttributeTypes(prefixText.WrapWithSqlWildCard())
                .OrderBy(s => s)
                .Take(count)
                .ToArray();
            return qlikUserAttributeTypes;
        }

        [WebMethod(Description = "Return list of qlik user attributes (required admin user Id in context key)")]
        [ScriptMethod]
        public string[] GetQlikUserAttributeValueListForType(string prefixText, int count, string contextKey)
        {
            var values = contextKey.RetrieveContextKeyValues();
            var superUserId = values.ContainsKey(AutoCompleteUtility.Keys.ActiveSuperUser)
                             ? values[AutoCompleteUtility.Keys.ActiveSuperUser].ToLong()
                             : default(long);

            var activeUser = new AdminUser(superUserId);
            if (!activeUser.KeyLoaded) return new string[0];

            var typeToFilterBy = values.ContainsKey(AutoCompleteUtility.Keys.QlikUserAttributeType)
                            ? values[AutoCompleteUtility.Keys.QlikUserAttributeType]
                            : SearchUtilities.WildCard;

            var qlikUserAttributeValues = new QlikUserSearch()
                .FetchAllDistinctQlikUserAttributeValues(prefixText.WrapWithSqlWildCard(), typeToFilterBy)
                .OrderBy(v => v)
                .Take(count)
                .ToArray();
            return qlikUserAttributeValues;
        }

		[WebMethod(Description = "Return list of sales representatives for tenant (required Id in context key)")]
		[ScriptMethod]
		public string[] GetSalesRepresentativesList(string prefixText, int count, string contextKey)
		{
			var search = new SalesRepresentativeSearch();
			var values = contextKey.RetrieveContextKeyValues();
			var tenantId = values.ContainsKey(AutoCompleteUtility.Keys.Tenant) ? values[AutoCompleteUtility.Keys.Tenant].ToLong() : default(long);

			var tiers = search
				.FetchSaleRepresentativeByCriteriaForNameOrCompanyName(tenantId, prefixText.CleanPrefixText(), prefixText.GetOperator())
								  .OrderBy(c => c.Name)
								  .Select(c => c.FormatForAutoComplete())
								  .Take(count)
								  .ToArray();
			return tiers;
		}

		[WebMethod(Description = "Return list of tiers for tenant (required Id in context key)")]
		[ScriptMethod]
		public string[] GetTierList(string prefixText, int count, string contextKey)
		{
			var search = new TierSearch();
			var values = contextKey.RetrieveContextKeyValues();
			var tenantId = values.ContainsKey(AutoCompleteUtility.Keys.Tenant) ? values[AutoCompleteUtility.Keys.Tenant].ToLong() : default(long);

			var criteria = AccountingSearchFields.TierName.ToParameterColumn();
			criteria.Operator = prefixText.GetOperator();
			criteria.DefaultValue = prefixText.CleanPrefixText();

			var criteriaList = new List<ParameterColumn> { criteria };
			var tiers = search.FetchTiers(criteriaList, tenantId)
								  .OrderBy(c => c.Name)
								  .Select(c => c.FormatForAutoComplete())
								  .Take(count)
								  .ToArray();
			return tiers;
		}

		[WebMethod(Description = "Return list of users visible to active user (required Id in context key)")]
		[ScriptMethod]
		public string[] GetUserList(string prefixText, int count, string contextKey)
		{
			var search = new UserSearch();
			var values = contextKey.RetrieveContextKeyValues();
			var userId = values.ContainsKey(AutoCompleteUtility.Keys.ActiveUser)
				             ? values[AutoCompleteUtility.Keys.ActiveUser].ToLong()
				             : default(long);

            var isShipmentCoordinatorFilter = values.ContainsKey(AutoCompleteUtility.Keys.IsShipmentCoordinator);
            var isCarrierCoorinatorFilter = values.ContainsKey(AutoCompleteUtility.Keys.IsCarrierCoordinator);

			var activeUser = new User(userId);

			if (!activeUser.KeyLoaded) return new string[0];

			var deptCode = activeUser.Department == null ? string.Empty : activeUser.Department.Code;

			var users = search
				.FetchUsersByCriteriaWithSearchDefaults(activeUser.TenantId, prefixText.CleanPrefixText(), prefixText.GetOperator())
                .Where(u => (u.TenantEmployee || u.DepartmentCode == deptCode) 
                    && (!isCarrierCoorinatorFilter || values[AutoCompleteUtility.Keys.IsCarrierCoordinator].ToBoolean() == u.IsCarrierCoordinator) 
                    && (!isShipmentCoordinatorFilter || values[AutoCompleteUtility.Keys.IsShipmentCoordinator].ToBoolean() == u.IsShipmentCoordinator))
				.OrderBy(u => u.FirstName)
				.ThenBy(u => u.LastName)
				.Select(c => c.FormatForAutoComplete())
				.Take(count)
				.ToArray();
			return users;
		}

		[WebMethod(Description = "Return list of vendors for tenant (required Id in context key)")]
        [ScriptMethod]
        public string[] GetVendorList(string prefixText, int count, string contextKey)
        {
            var search = new VendorSearch();
            var values = contextKey.RetrieveContextKeyValues();
            var tenantId = values.ContainsKey(AutoCompleteUtility.Keys.Tenant) ? values[AutoCompleteUtility.Keys.Tenant].ToLong() : default(long);

            var criteria = AccountingSearchFields.VendorName.ToParameterColumn();
			criteria.Operator = prefixText.GetOperator();
			criteria.DefaultValue = prefixText.CleanPrefixText();

            var criteriaList = new List<ParameterColumn> {criteria};
            var vendors = search.FetchVendors(criteriaList, tenantId)
                                  .OrderBy(c => c.Name)
                                  .Select(c => c.FormatForAutoComplete())
								  .Take(count)
                                  .ToArray();
            return vendors;
        }


        [WebMethod(Description = "Posts a shipment/load order as an asset on DAT Loadboard")]
        [ScriptMethod]
        public object PostAssetToDat(string userId, string loadOrderId, float baseRate, string rateType, string[] comments, object callReturnArgs)
        {
            try
            {
                var user = new User(userId.UrlTextDecrypt().ToLong());

                var service = new Connexion(user.DatLoadboardUsername, user.DatLoadboardPassword);
                var rateBasedOn = rateType.ToEnum<RateBasedOnType>();

                var loadOrder = new LoadOrder(loadOrderId.ToLong());

                // validate incoming data
                var errors = new List<string>();
                if (user.IsNew) errors.Add("User must be a valid eShipPlus user");
                if (!user.HasDatCredentials) errors.Add("User must have DAT credentials");
                if (loadOrder.IsNew) errors.Add("Load Order must be a valid eShipPlus Load Order");
                if (!loadOrder.Equipments.Any()) errors.Add("Load Order must have an equipment selected");
                if (comments.Length > 2) errors.Add("DAT postings can have a maximum of two comments");
                if (loadOrder.Origin.City == string.Empty || loadOrder.Origin.State == string.Empty || loadOrder.Origin.PostalCode == string.Empty) 
                    errors.Add("Load Order requires a valid Origin");
                if (loadOrder.Destination.City == string.Empty || loadOrder.Destination.State == string.Empty || loadOrder.Destination.PostalCode == string.Empty)
                    errors.Add("Load Order requires a valid Destination");

                if(errors.Any())
                    return new
                    {
                        ErrorMessage = errors.ToArray().CommaJoin(),
                        Args = callReturnArgs,
                        AssetId = string.Empty,
                        ExpirationDate = string.Empty,
                        DateCreated = string.Empty,
                        BaseRate = 0m,
                        RateType = string.Empty,
                        Mileage = 0,
                        WasSentAsHazMat = false,
                        PostedBy = string.Empty,
                    };

                var equipmentType = loadOrder.Equipments.First().EquipmentType.DatEquipmentType;
                if (equipmentType == DatEquipmentType.NotApplicable)
                {
                    errors.Add("Load Order does not have a valid DAT equipment type.");

                    return new
                        {
                            ErrorMessage = errors.ToArray().CommaJoin(),
                            Args = callReturnArgs,
                            AssetId = string.Empty,
                            ExpirationDate = string.Empty,
                            DateCreated = string.Empty,
                            BaseRate = 0m,
                            RateType = string.Empty,
                            Mileage = 0,
                            WasSentAsHazMat = false,
                            PostedBy = string.Empty,
                        };
                }

                var requestRate = new ShipmentRate
                    {
                        baseRateDollars = baseRate,
                        rateBasedOn = rateBasedOn,
                        rateMiles = (int) loadOrder.Mileage,
                        rateMilesSpecified = rateBasedOn == RateBasedOnType.PerMile
                    };

                var request = loadOrder.ToPostAssetRequest(requestRate, equipmentType.ToString().ToEnum<Dat.EquipmentType>(), comments.ToList());

                var response = service.PostAsset(request);

                var asset = response.PostAssetSuccessData != null && response.PostAssetSuccessData.Any() && !response.HasError
                                      ? response.PostAssetSuccessData.First().asset
                                      : null;

                DatLoadboardAssetPosting posting = null;

                if (!response.HasError && asset != null)
                {
                    Exception ex = null;
                    posting = new DatLoadboardAssetPosting
                        {
                            AssetId = asset.assetId,
                            DateCreated = asset.status.startDate,
                            ExpirationDate = asset.status.endDate,
                            HazardousMaterial = loadOrder.HazardousMaterial,
                            IdNumber = loadOrder.LoadOrderNumber,
                            BaseRate = requestRate.baseRateDollars.ToDecimal(),
                            Mileage = requestRate.rateMiles,
                            RateType = requestRate.rateBasedOn,
                            TenantId = loadOrder.TenantId,
                            UserId = user.Id,
                            SerializedComments = comments.ToList().ToXml()
                        };
                    var errMsgs = new DatLoadboardAssetPostingUpdateHandler().SaveDatLoadboardAssetPosting(posting, loadOrder, out ex, user);

                    if (ex != null || errMsgs.Any())
                    {
                        // If we fail to save the posting locally, remove it from DAT
                        service.DeleteAsset(new DeleteAssetRequest
                        {
                            deleteAssetOperation = new DeleteAssetOperation { Item = new DeleteAssetsByAssetIds { assetIds = new[] { posting.AssetId } } }
                        });

                        return new
                            {
                                ErrorMessage = ex != null
                                                   ? ex.Message
                                                   : errMsgs.Where(e => e.Type == ValidateMessageType.Error)
                                                            .Select(e => e.Message)
                                                            .ToArray()
                                                            .CommaJoin(),
                                Args = callReturnArgs,
                                AssetId = string.Empty,
                                ExpirationDate = string.Empty,
                                DateCreated = string.Empty,
                                BaseRate = 0m,
                                RateType = string.Empty,
                                Mileage = 0,
                                WasSentAsHazMat = false,
                                PostedBy = string.Empty,
                            };
                    }
                }

                return new
                {
                    ErrorMessage = response.HasError ? response.Errors.Select(e => string.Format("{0}: {1}", e.message, e.detailedMessage)).ToArray().CommaJoin() : string.Empty,
                    Args = callReturnArgs,
                    AssetId = posting == null ? string.Empty : posting.AssetId,
                    ExpirationDate = posting == null ? string.Empty : posting.ExpirationDate.FormattedLongDate(),
                    DateCreated = posting == null ? string.Empty : posting.DateCreated.FormattedLongDate(),
                    BaseRate = posting != null ? posting.BaseRate : 0m,
                    RateType = posting != null ? posting.RateType.FormattedString() : string.Empty,
                    Mileage = posting != null ? posting.Mileage : 0m,
                    WasSentAsHazMat = posting != null && posting.HazardousMaterial,
                    PostedBy = posting != null ? posting.User.FullName : string.Empty,
                };
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex, Context);
                return new
                {
                    Args = callReturnArgs,
                    ErrorMessage = ex.Message,
                    AssetId = string.Empty,
                    ExpirationDate = string.Empty,
                    DateCreated = string.Empty,
                    BaseRate = 0m,
                    RateType = string.Empty,
                    Mileage = 0,
                    WasSentAsHazMat = false,
                    PostedBy = string.Empty,
                };
            }
        }

	    [WebMethod(Description = "Updates an asset on DAT Loadboard")]
	    [ScriptMethod]
	    public object UpdateAssetOnDat(string userId, string loadOrderId, string tenantId,
                                       float rate, string rateType, string assetId, string[] comments, object callReturnArgs)
	    {
	        try
	        {
	            var user = new User(userId.UrlTextDecrypt().ToLong());

	            var service = new Connexion(user.DatLoadboardUsername, user.DatLoadboardPassword);
	            var rateBasedOn = rateType.ToEnum<RateBasedOnType>();

	            var loadOrder = new LoadOrder(loadOrderId.ToLong());

	            // validate incoming data
	            var errors = new List<string>();

	            if (user.IsNew) errors.Add("User must be a valid eShipPlus user");
                if (!user.HasDatCredentials) errors.Add("User must have DAT credentials");
	            if (loadOrder.IsNew) errors.Add("Load Order must be a valid eShipPlus Load Order");
                if (comments.Length > 2) errors.Add("Assets can have a maximum of two comments");
	            
                if (errors.Any())
	                return new
	                    {
                            Args = callReturnArgs,
	                        ErrorMessage = errors.ToArray().CommaJoin(),
                            Success = false,
                            BaseRate = default(decimal),
                            RateType = string.Empty
	                    };

	            var requestRate = new ShipmentRate
	                {
	                    baseRateDollars = rate,
	                    rateBasedOn = rateBasedOn,
	                    rateMiles = (int) loadOrder.Mileage,
	                    rateMilesSpecified = rateBasedOn == RateBasedOnType.PerMile
	                };

	            var request = loadOrder.ToUpdateAssetRequest(requestRate, comments.ToList(), assetId);


                Exception ex = null;

                // grab the original DatLoadboardAssetPosting in the event that an error occurs while sending the update request to DAT
                var originalPosting = new DatLoadboardAssetPosting(assetId, tenantId.ToLong());

                // fetch the original DatLoadboardAssetPosting and update it
	            var posting = new DatLoadboardAssetPosting(assetId, tenantId.ToLong());
                posting.TakeSnapShot();

	            posting.BaseRate = requestRate.baseRateDollars.ToDecimal();
	            posting.Mileage = requestRate.rateMiles;
	            posting.RateType = requestRate.rateBasedOn;
	            posting.SerializedComments = comments.ToList().ToXml();

                var errMsgs =
                    new DatLoadboardAssetPostingUpdateHandler().SaveDatLoadboardAssetPosting(
                        posting, loadOrder, out ex, user);

                if (ex != null || errMsgs.Any())
                {
                    return new
                        {
                            Args = callReturnArgs,
                            ErrorMessage = ex != null
                                               ? ex.Message
                                               : errMsgs.Where(e => e.Type == ValidateMessageType.Error)
                                                        .Select(e => e.Message)
                                                        .ToArray()
                                                        .CommaJoin(),
                            Success = false,
                            BaseRate = default(decimal),
                            RateType = string.Empty
                        };
                }

                var response = service.UpdateAsset(request);

                // there was an error updating the asset , so revert the changes on the local db
                if (response.HasError)
                {
                    originalPosting.Save();
                }

                return new
	                {
                        Args = callReturnArgs,
	                    ErrorMessage =
	                        response.HasError
                                ? string.Format("{0}: {1}", response.Error.message, response.Error.detailedMessage)
	                            : string.Empty,
	                    Success = response.UpdateAssetSuccessData != null,
                        posting.BaseRate,
                        RateType = posting.RateType.ToString()
	                };
	        }
	        catch (Exception ex)
	        {
	            ErrorLogger.LogError(ex, Context);
	            return new
	                {
                        Args = callReturnArgs,
	                    ErrorMessage = ex.Message,
	                    Success = false,
                        BaseRate = default(decimal),
                        RateType = string.Empty
	                };
	        }
	    }

	    [WebMethod(Description = "Deletes an asset from DAT Loadboard")]
        [ScriptMethod]
        public object DeleteAssetFromDat(string userId, string loadOrderId, object callReturnArgs)
        {
            try
            {
                var user = new User(userId.UrlTextDecrypt().ToLong());
                var service = new Connexion(user.DatLoadboardUsername, user.DatLoadboardPassword);

                var loadOrder = new LoadOrder(loadOrderId.ToLong());

                var posting = new DatLoadboardAssetPostingSearch().FetchDatLoadboardAssetPostingByIdNumber(loadOrder.LoadOrderNumber, loadOrder.TenantId);

                // validate incoming data
                var errors = new List<string>();
                if (user.IsNew) errors.Add("User must be a valid eShipPlus user");
                if (!user.HasDatCredentials) errors.Add("User must have DAT credentials");
                if (loadOrder.IsNew) errors.Add("Load Order must be a valid eShipPlus Load Order");
                if (posting == null || posting.IsNew) errors.Add(string.Format("Could not find DAT Loadboard asset posting record related to Load Order # {0}", loadOrder.LoadOrderNumber));
                if (errors.Any())
                    return new
                    {
                        Args = callReturnArgs,
                        ErrorMessage = errors.ToArray().CommaJoin(),
                        Success = false
                    };

                var response = service.DeleteAsset(new DeleteAssetRequest
                    {
                        deleteAssetOperation = new DeleteAssetOperation {Item = new DeleteAssetsByAssetIds {assetIds = new[] {posting.AssetId}}}
                    });

                if (response.HasError)
                {
                    return new
                    {
                        Args = callReturnArgs,
                        ErrorMessage = string.Format("{0}: {1}. {2}", response.Error.message, response.Error.detailedMessage,
                                                "Go to the Dat Loadboard page if you wish to force the removal of this posting."),
                        Success = false
                    };
                }

                Exception ex;
                var errMsgs = new DatLoadboardAssetPostingUpdateHandler().DeleteDatLoadboardAssetPosting(posting, loadOrder, out ex, user);

                if (ex != null || errMsgs.Any())
                {
                    return new
                    {
                        Args = callReturnArgs,
                        ErrorMessage = ex != null ? ex.Message : errMsgs.Where(e => e.Type == ValidateMessageType.Error).Select(e => e.Message).ToArray().CommaJoin(),
                        Success = response.DeleteAssetSuccessData != null
                    };
                }

                return new
                    {
                        Args = callReturnArgs,
                        ErrorMessage = string.Empty,
                        Success = response.DeleteAssetSuccessData != null
                    };
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex, Context);
                return new
                {
                    Args = callReturnArgs,
                    ErrorMessage = ex.Message,
                    Success = false
                };
            }
        }

	    [WebMethod(Description = "Looks up a rate for a DAT Asset")]
	    [ScriptMethod]
        public object LookupCurrentDatRate(string userId, string loadOrderId, string assetId, object callReturnArgs)
	    {
	        try
	        {
	            var user = new User(userId.UrlTextDecrypt().ToLong());
	            var service = new Connexion(user.DatLoadboardUsername, user.DatLoadboardPassword);


	            var loadOrder = new LoadOrder(loadOrderId.ToLong());

	            // validate incoming data
	            var errors = new List<string>();
	            if (user.IsNew) errors.Add("User must be a valid eShipPlus user");
                if (!user.HasDatCredentials) errors.Add("User must have DAT credentials");
                if (!loadOrder.Equipments.Any()) errors.Add("Load Order must have an equipment selected");
	            if (loadOrder.IsNew) errors.Add("Load Order must be a valid eShipPlus Load Order");
	            if (errors.Any())
	                return new
	                    {
                            Args = callReturnArgs,
	                        ErrorMessage = errors.ToArray().CommaJoin(),
	                        Success = false
	                    };

	            var request = loadOrder.ToLookupRateRequest();

	            var response = service.LookupCurrentRate(request);

	            var errorList = response.Errors.Aggregate(string.Empty,
	                                                      (current, error) =>
	                                                      current +
                                                          (string.Format("{0}: {1}", error.message, error.detailedMessage) + Environment.NewLine));

                if (errorList != string.Empty)
	            {
                    return new
                    {
                        Args = callReturnArgs,
                        ErrorMessage = errorList,
                        Success = false
                    };
	            }

                var rates = response.LookupRateSuccessData.Select(rate => new List<string> { rate.spotRate.averageFuelSurchargeRate.ToString(), 
                                                                                             rate.spotRate.estimatedLinehaulRate.ToString(),
                                                                                             rate.spotRate.estimatedLinehaulTotal.ToString(),
                                                                                             rate.spotRate.highLinehaulRate.ToString(),
                                                                                             rate.spotRate.highLinehaulTotal.ToString(),
                                                                                             rate.spotRate.lowLinehaulRate.ToString(),
                                                                                             rate.spotRate.lowLinehaulTotal.ToString(),
                                                                                             }).ToList();

	            var jsonRates = JsonConvert.SerializeObject(rates);
                return new
	                {
                        Args = callReturnArgs,
	                    Success = true,
                        ErrorMessage = string.Empty,
                        Rates = jsonRates
	                };
	        }

	        catch (Exception ex)
	        {
	            ErrorLogger.LogError(ex, Context);
	            return new
	                {
                        Args = callReturnArgs,
	                    ErrorMessage = ex.Message,
	                    Success = false
	                };
	        }
	    }
	}
}
