﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MacroPointServiceTestNotificationSender.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Services.MacroPointServiceTestNotificationSender" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table>
                <tr>
                    <td style="text-align: right;">OrderID:
                    </td>
                    <td>
                        <eShip:CustomTextBox runat="server" ID="txtOrderId" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right;">Id Number:
                    </td>
                    <td>
                        <eShip:CustomTextBox runat="server" ID="txtIdNumber" />
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td>
                        For Location Notifications
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right;">Latitude:
                    </td>
                    <td>
                        <eShip:CustomTextBox runat="server" ID="txtLatitude" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right;">Longitude:
                    </td>
                    <td>
                        <eShip:CustomTextBox runat="server" ID="txtLongitude" />
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td>&nbsp;
                    </td>
                    <td>
                        <asp:Button runat="server" Text="Send Order Status Update" ID="btnSendOrderStatusUpdate" OnClick="OnSendOrderStatusUpdateClicked" />
                        <br />
                        <asp:Button runat="server" Text="Send Event Update" ID="btnSendEventUpdate" OnClick="OnSendEventUpdateClicked" />
                        <br />
                        <asp:Button runat="server" Text="Send Location Notification" ID="btnSendLocationNotification" OnClick="OnSendLocationNotificationClicked" />
                        <br/>
                        <asp:Button runat="server" Text ="Send Form" ID="btnSendForm" OnClick="OnSendFormClicked"/>
                    </td>

                </tr>
                <tr>
                    <td>&nbsp;       
                    </td>
                </tr>

                <tr>
                    <td>Result of request:
                    </td>
                    <td>
                        <asp:Literal runat="server" ID="litResult" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
