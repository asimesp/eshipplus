﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Extern.Ws;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.Operations;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.Utilities.ImmitationViews;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Services
{
    public static class eShipPlusServiceBase
    {
        public static WSAccessorialService[] FetchAccessorialServices(long tenantId)
        {
            var list = new ServiceSearch()
                .FetchServicesByCategory(ServiceCategory.Accessorial, tenantId)
                .Select(service => new WSAccessorialService
                {
                    ServiceCode = service.Code,
                    ServiceDescription = service.Description,
                    BillingCode = service.ChargeCode
                })
                .ToArray();

            return !list.Any()
                    ? new[] { WSMessage.InformationMessage("No services available.") }.ToMessageReturnCollection<WSAccessorialService>()
                    : list;
        }

        public static WSFreightClass[] FetchLTLFreightClasses()
        {
            return WebApplicationSettings.FreightClasses
                    .Select(fc => new WSFreightClass { FreightClass = fc })
                    .ToArray();
        }

        public static WSItemPackage[] FetchItemPackaging(long tenantId)
        {
            var list = ProcessorVars.RegistryCache[tenantId].PackageTypes
                    .Where(p => p.Active)
                    .Select(p => new WSItemPackage
                    {
                        Key = p.Id,
                        PackageName = p.TypeName,
                        DefaultHeight = p.DefaultHeight,
                        DefaultLength = p.DefaultLength,
                        DefaultWidth = p.DefaultWidth
                    })
                    .ToArray();

            return !list.Any()
                    ? new[] { WSMessage.InformationMessage("No package types available.") }.ToMessageReturnCollection<WSItemPackage>()
                    : list;
        }

        public static WSCountry[] FetchCountries()
        {
            var list = ProcessorVars.RegistryCache.Countries.Values
                    .OrderByDescending(t => t.SortWeight)
                    .ThenBy(t => t.Name)
                    .Select(c => new WSCountry { Name = c.Name, Code = c.Code, UsesPostalCode = c.EmploysPostalCodes })
                    .ToArray();

            return !list.Any()
                    ? new[] { WSMessage.InformationMessage("No countries available.") }.ToMessageReturnCollection<WSCountry>()
                    : list;
        }

        public static WSTime2[] FetchTimes()
        {
            return TimeUtility.BuildTimeList()
                    .Select(t => new WSTime2 { Time = t })
                    .ToArray();
        }

        public static WSOptions FetchInputOptions(long tenantId)
        {
            // accesorial services
            var services = new ServiceSearch().FetchServicesByCategory(ServiceCategory.Accessorial, tenantId)
                .Select(service => new WSAccessorialService
                {
                    ServiceCode = service.Code,
                    ServiceDescription = service.Description,
                    BillingCode = service.ChargeCode
                })
                .ToArray();

            // item packaging
            var packaging = ProcessorVars.RegistryCache[tenantId].PackageTypes
                .Where(p => p.Active)
                .Select(p => new WSItemPackage
                {
                    Key = p.Id,
                    PackageName = p.TypeName,
                    DefaultHeight = p.DefaultHeight,
                    DefaultLength = p.DefaultLength,
                    DefaultWidth = p.DefaultWidth
                })
                .ToArray();

            // countries
            var countries = ProcessorVars.RegistryCache.Countries.Values
                .OrderByDescending(t => t.SortWeight)
                .ThenBy(t => t.Name)
                .Select(c => new WSCountry { Name = c.Name, Code = c.Code, UsesPostalCode = c.EmploysPostalCodes })
                .ToArray();

            return new WSOptions
            {
                AccessorialServices = !services.Any()
                                        ? new[] { WSMessage.InformationMessage("No services available.") }.
                                            ToMessageReturnCollection<WSAccessorialService>()
                                        : services,
                LTLFreightClasses = WebApplicationSettings.FreightClasses
                    .Select(fc => new WSFreightClass { FreightClass = fc })
                    .ToArray(),
                ItemPackaging = !packaging.Any()
                                    ? new[] { WSMessage.InformationMessage("No package types available.") }.
                                        ToMessageReturnCollection<WSItemPackage>()
                                    : packaging,
                Countries = !countries.Any()
                                ? new[] { WSMessage.InformationMessage("No countries available.") }.ToMessageReturnCollection
                                    <WSCountry>()
                                : countries,
                Times = TimeUtility.BuildTimeList()
                    .Select(t => new WSTime2 { Time = t })
                    .ToArray()
            };
        }

        public static WSReference[] FetchCustomerCustomReferences(Customer customer)
        {
            var list = customer.CustomFields
                    .Select(c => new WSReference { Name = c.Name, Value = string.Empty, Required = c.Required })
                    .ToArray();

            return list;
        }

        public static WSShipmentDocument FetchDocument(User user, string referenceNumber, long documentId, Customer customer)
        {
            var shipment = new ShipmentSearch().FetchShipmentByShipmentNumber(referenceNumber, user.TenantId);
            var loadOrder = new LoadOrderSearch().FetchLoadOrderByLoadNumber(referenceNumber, user.TenantId);
            WSShipmentDocument document = null;

            if (shipment != null)
            {
                if (!user.HasAccessTo(ViewCode.Shipment))
                {
                    var msg = WSMessage.ErrorMessage(ProcessorVars.WSNoPermission);
                    EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                    return new[] { msg }.ToMessageReturn<WSShipmentDocument>();

                }

                var number = customer == null ? string.Empty : customer.CustomerNumber;
                if (shipment.Customer.CustomerNumber != number)
                {
                    var msg = WSMessage.ErrorMessage("Shipment entry [Number: {0}] not available to this account!", shipment.ShipmentNumber);
                    EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                    return new[] { msg }.ToMessageReturn<WSShipmentDocument>();
                }


                var matchingDocument = shipment.Documents.FirstOrDefault(d => d.Id == documentId);
                if (matchingDocument != null)
                {
                    var bytes = HttpContext.Current.Server.ReadFromFile(matchingDocument.LocationPath);

                    document = new WSShipmentDocument
                    {
                        DocumentFileName = HttpContext.Current.Server.GetFileName(matchingDocument.LocationPath),
                        DocumentKey = matchingDocument.Id,
                        EncodedDocumentBytes = Convert.ToBase64String(bytes)
                    };
                }
            }
            else if (loadOrder != null)
            {
                if (!user.HasAccessTo(ViewCode.LoadOrder))
                {
                    var msg = WSMessage.ErrorMessage(ProcessorVars.WSNoPermission);
                    EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                    return new[] { msg }.ToMessageReturn<WSShipmentDocument>();

                }

                var number = customer == null ? string.Empty : customer.CustomerNumber;
                if (loadOrder.Customer.CustomerNumber != number)
                {
                    var msg = WSMessage.ErrorMessage("Load Order entry [Number: {0}] not available to this account!", loadOrder.LoadOrderNumber);
                    EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                    return new[] { msg }.ToMessageReturn<WSShipmentDocument>();
                }

                var matchingDocument = loadOrder.Documents.FirstOrDefault(d => d.Id == documentId);
                if (matchingDocument != null)
                {
                    var bytes = HttpContext.Current.Server.ReadFromFile(matchingDocument.LocationPath);

                    document = new WSShipmentDocument
                    {
                        DocumentFileName = HttpContext.Current.Server.GetFileName(matchingDocument.LocationPath),
                        DocumentKey = matchingDocument.Id,
                        EncodedDocumentBytes = Convert.ToBase64String(bytes)
                    };
                }
            }

            return document ?? new[]
                           {new WSMessage {Type = WSEnums.MessageType.Error, Value = WebApplicationConstants.NoResults}}
                       .ToMessageReturn<WSShipmentDocument>();

        }


        public static WSCustomerPONumber FetchPONumber(User user, string poNumber)
        {
            if (!user.HasAccessTo(ViewCode.CustomerPurchaseOrder))
            {
                var msg = WSMessage.ErrorMessage(ProcessorVars.WSNoPermission);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSCustomerPONumber>();
            }

            if (poNumber == null) poNumber = string.Empty;
            var po = new CustomerPurchaseOrderSearch()
                .FetchCustomerPurchaseOrder(user.TenantId, user.DefaultCustomer.Id, poNumber);

            if (po == null)
            {
                var msg = WSMessage.ErrorMessage("Purchase order entry [{0}] not found!", poNumber);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSCustomerPONumber>();
            }

            return new WSCustomerPONumber
            {
                AdditionalPurchaseOrderNotes = po.AdditionalPurchaseOrderNotes,
                Country = new WSCountry
                {
                    Code = po.Country.Code,
                    Name = po.Country.Name,
                    UsesPostalCode = po.Country.EmploysPostalCodes
                },
                CurrentUses = po.CurrentUses,
                MaximumUses = po.MaximumUses,
                ExpirationDate = po.ExpirationDate,
                PONumber = po.PurchaseOrderNumber,
                ValidPostalCode = po.ValidPostalCode,
                Validity = po.ApplyOnDestination
                            ? WSEnums.PONumberValidity.Destination
                            : WSEnums.PONumberValidity.Origin

            };
        }

        public static WSReturn DeletePONumber(User user, string poNumber)
        {
            if (!user.HasAccessToModify(ViewCode.CustomerPurchaseOrder))
            {
                var msg = WSMessage.ErrorMessage(ProcessorVars.WSNoPermission);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSCustomerPONumber>();
            }

            if (poNumber == null) poNumber = string.Empty;
            var po = new CustomerPurchaseOrderSearch()
                .FetchCustomerPurchaseOrder(user.TenantId, user.DefaultCustomer.Id, poNumber);

            if (po == null)
            {
                var msg = WSMessage.ErrorMessage("Purchase order entry [{0}] not found!", poNumber);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSReturn>();
            }

            var view = new CustomerPurchaseOrderImmitationView(user);
            view.DeletePurchaseOrder(po);
            return new WSReturn { Messages = view.Messages.ToArray() };
        }

        public static WSReturn UpdatePONumber(User user, WSCustomerPONumber customerPONumber)
        {
            if (!user.HasAccessToModify(ViewCode.CustomerPurchaseOrder))
            {
                var msg = WSMessage.ErrorMessage(ProcessorVars.WSNoPermission);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSCustomerPONumber>();
            }

            if (customerPONumber.PONumber == null) customerPONumber.PONumber = string.Empty;
            var po = new CustomerPurchaseOrderSearch()
                .FetchCustomerPurchaseOrder(user.TenantId, user.DefaultCustomer.Id, customerPONumber.PONumber);

            if (po == null)
            {
                var msg = WSMessage.ErrorMessage("Purchase order entry [{0}] not found!", customerPONumber.PONumber);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSReturn>();
            }

            var view = new CustomerPurchaseOrderImmitationView(user);
            view.LockRecord(po);
            if (view.Messages.ContainsErrors()) return view.Messages.ToMessageReturn<WSReturn>();

            po.TakeSnapShot();
            po.AdditionalPurchaseOrderNotes = customerPONumber.AdditionalPurchaseOrderNotes;
            po.ApplyOnOrigin = customerPONumber.Validity == WSEnums.PONumberValidity.Origin;
            po.ApplyOnDestination = customerPONumber.Validity == WSEnums.PONumberValidity.Destination;
            po.CountryId = customerPONumber.Country == null
                            ? default(long)
                            : new CountrySearch().FetchCountryIdByCode(customerPONumber.Country.Code.GetString());
            po.CurrentUses = customerPONumber.CurrentUses;
            po.MaximumUses = customerPONumber.MaximumUses;
            po.ExpirationDate = customerPONumber.ExpirationDate;
            po.PurchaseOrderNumber = customerPONumber.PONumber;
            po.ValidPostalCode = customerPONumber.ValidPostalCode;

            view.SavePurchaseOrder(po);
            view.UnLockRecord(po);
            return new WSReturn { Messages = view.Messages.ToArray() };
        }

        public static WSReturn AddPONumber(User user, WSCustomerPONumber customerPONumber)
        {
            if (!user.HasAccessToModify(ViewCode.CustomerPurchaseOrder))
            {
                var msg = WSMessage.ErrorMessage(ProcessorVars.WSNoPermission);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSCustomerPONumber>();
            }


            if (customerPONumber.PONumber == null) customerPONumber.PONumber = string.Empty;
            var po = new CustomerPurchaseOrderSearch()
                .FetchCustomerPurchaseOrder(user.TenantId, user.DefaultCustomer.Id, customerPONumber.PONumber);

            if (po != null)
            {
                var msg = WSMessage.ErrorMessage("Purchase order entry [{0}] already exist!", customerPONumber.PONumber);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSReturn>();
            }

            po = new CustomerPurchaseOrder
            {
                AdditionalPurchaseOrderNotes = customerPONumber.AdditionalPurchaseOrderNotes,
                ApplyOnOrigin = customerPONumber.Validity == WSEnums.PONumberValidity.Origin,
                ApplyOnDestination = customerPONumber.Validity == WSEnums.PONumberValidity.Destination,
                CountryId = customerPONumber.Country == null
                                ? default(long)
                                : new CountrySearch().FetchCountryIdByCode(customerPONumber.Country.Code.GetString()),
                CurrentUses = customerPONumber.CurrentUses,
                MaximumUses = customerPONumber.MaximumUses,
                ExpirationDate = customerPONumber.ExpirationDate,
                PurchaseOrderNumber = customerPONumber.PONumber,
                ValidPostalCode = customerPONumber.ValidPostalCode,
                TenantId = user.TenantId,
                Customer = user.DefaultCustomer
            };

            var view = new CustomerPurchaseOrderImmitationView(user);
            view.SavePurchaseOrder(po);
            return new WSReturn { Messages = view.Messages.ToArray() };
        }

        public static WSReturn AddPONumbers(User user, WSCustomerPONumber[] customerPONumbers)
        {
            if (!user.HasAccessToModify(ViewCode.CustomerPurchaseOrder))
            {
                var msg = WSMessage.ErrorMessage(ProcessorVars.WSNoPermission);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSCustomerPONumber>();
            }


            if (customerPONumbers.Count() > 1000)
            {
                var msg = WSMessage.ErrorMessage("Record count limit of 1000 exceeded.  Please resubmit with less than 1000 PO Numbers in batch.");
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSReturn>();
            }

            var pos = customerPONumbers
                .Select(po => new CustomerPurchaseOrder
                {
                    AdditionalPurchaseOrderNotes = po.AdditionalPurchaseOrderNotes,
                    ApplyOnOrigin = po.Validity == WSEnums.PONumberValidity.Origin,
                    ApplyOnDestination =
                        po.Validity == WSEnums.PONumberValidity.Destination,
                    CountryId = po.Country == null
                                    ? default(long)
                                    : new CountrySearch().FetchCountryIdByCode(
                                        po.Country.Code.GetString()),
                    CurrentUses = po.CurrentUses,
                    MaximumUses = po.MaximumUses,
                    ExpirationDate = po.ExpirationDate,
                    PurchaseOrderNumber = po.PONumber,
                    ValidPostalCode = po.ValidPostalCode,
                    TenantId = user.TenantId,
                    Customer = user.DefaultCustomer
                })
                .ToList();

            var view = new CustomerPurchaseOrderImmitationView(user);
            view.BatchImportPurchaseOrders(pos);
            return new WSReturn { Messages = view.Messages.ToArray() };
        }



        public static WSBookingRequestStatus FetchBookingRequestStatus(User user, string bookingNumber, Customer customer)
        {
            if (!user.HasAccessTo(ViewCode.LoadOrder, ViewCode.CustomerLoadDashboard))
            {
                var msg = WSMessage.ErrorMessage(ProcessorVars.WSNoPermission);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSBookingRequestStatus>();
            }

            if (bookingNumber == null) bookingNumber = string.Empty;
            var load = new LoadOrderSearch().FetchLoadOrderByLoadNumber(bookingNumber, user.TenantId);

            if (load == null)
            {
                var msg = WSMessage.ErrorMessage("Load record [Number: {0}] not found!", bookingNumber);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSBookingRequestStatus>();
            }


            var number = customer == null ? string.Empty : customer.CustomerNumber;

            if (load.Customer.CustomerNumber != number)
            {
                var msg = WSMessage.ErrorMessage("Load record [Number: {0}] not available to this account!", bookingNumber);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSBookingRequestStatus>();
            }

            var shipment = new ShipmentSearch().FetchShipmentByShipmentNumber(load.LoadOrderNumber, user.TenantId);
            var sv = shipment == null ? null : shipment.Vendors.FirstOrDefault(v => v.Primary);
            var shipmentStatus = shipment == null
                                     ? null
                                     : new WSShipmentStatus
                                     {
                                         ShipmentNumber = shipment.ShipmentNumber,
                                         Status = shipment.Status,
                                         Mode = shipment.ServiceMode,
                                         ActualDeliveryDate = shipment.ActualDeliveryDate,
                                         ActualPickupDate = shipment.ActualPickupDate,
                                         DateCreated = shipment.DateCreated,
                                         EstimateDeliveryDate = shipment.EstimatedDeliveryDate,
                                         EstimatePickupDate = shipment.DesiredPickupDate,
                                         IsDelivered = shipment.ActualDeliveryDate != DateUtility.SystemEarliestDateTime,
                                         IsPickedUp = shipment.ActualPickupDate != DateUtility.SystemEarliestDateTime,
                                         Pro = sv == null ? string.Empty : sv.ProNumber,
                                         VendorKey = sv == null ? string.Empty : sv.Vendor.VendorNumber,
                                         VendorName = sv == null ? string.Empty : sv.Vendor.Name,
                                         VendorScac = sv == null ? string.Empty : sv.Vendor.Scac,
                                         CheckCalls = shipment.CheckCalls
                                                              .Select(cc => new WSCheckCall
                                                              {
                                                                  CallNotes = cc.CallNotes,
                                                                  CallDate = cc.CallDate.FormattedSuppressMidnightDate(),
                                                                  EventDate = cc.EventDate == DateUtility.SystemEarliestDateTime
                                                                                  ? string.Empty
                                                                                  : cc.EventDate.FormattedSuppressMidnightDate(),
                                                                  StatusCode = cc.EdiStatusCode
                                                              })
                                                              .ToArray()
                                     };

            return new WSBookingRequestStatus
            {
                BookingNumber = load.LoadOrderNumber,
                Status = load.Status == LoadOrderStatus.Shipment
                            ? WSEnums.BookingRequestStatus.Covered
                            : load.Status == LoadOrderStatus.Cancelled
                                ? WSEnums.BookingRequestStatus.Cancelled
                                : WSEnums.BookingRequestStatus.PendingCover,
                Shipments = shipmentStatus == null ? new WSShipmentStatus[0] : new[] { shipmentStatus }
            };
        }

        public static WSShipmentStatus FetchShipmentStatus(User user, string shipmentNumber, Customer customer, bool condenseResponse = false)
        {
            if (!user.HasAccessTo(ViewCode.Shipment, ViewCode.ShipmentDashboard, ViewCode.CustomerLoadDashboard))
            {
                var msg = WSMessage.ErrorMessage(ProcessorVars.WSNoPermission);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSShipmentStatus>();
            }
            if (shipmentNumber == null) shipmentNumber = string.Empty;
            var shipment = new ShipmentSearch().FetchShipmentByShipmentNumber(shipmentNumber, user.TenantId);

            if (shipment == null)
            {
                var msg = WSMessage.ErrorMessage("Shipment entry [Number: {0}] not found!", shipmentNumber);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSShipmentStatus>();
            }

            var number = customer == null ? string.Empty : customer.CustomerNumber;
            if (shipment.Customer.CustomerNumber != number)
            {
                var msg = WSMessage.ErrorMessage("Shipment entry [Number: {0}] not available to this account!", shipmentNumber);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSShipmentStatus>();
            }

            var sv = shipment.Vendors.FirstOrDefault(v => v.Primary);
            var status = new WSShipmentStatus
            {
                ShipmentNumber = shipment.ShipmentNumber,
                Status = shipment.Status,
                Mode = shipment.ServiceMode,
                ActualDeliveryDate = shipment.ActualDeliveryDate,
                ActualPickupDate = shipment.ActualPickupDate,
                DateCreated = shipment.DateCreated,
                EstimateDeliveryDate = shipment.EstimatedDeliveryDate,
                EstimatePickupDate = shipment.DesiredPickupDate,
                IsDelivered = shipment.ActualDeliveryDate != DateUtility.SystemEarliestDateTime,
                IsPickedUp = shipment.ActualPickupDate != DateUtility.SystemEarliestDateTime,
                Pro = sv == null ? string.Empty : sv.ProNumber,
                VendorKey = sv == null ? string.Empty : sv.Vendor.VendorNumber,
                VendorName = sv == null ? string.Empty : sv.Vendor.Name,
                VendorScac = sv == null ? string.Empty : sv.Vendor.Scac,
                CheckCalls = null,
                BillingDetails = null,
                Documents = null
            };

            if (!condenseResponse)
            {
                status.CheckCalls = shipment.CheckCalls
                                            .Select(cc => new WSCheckCall
                                            {
                                                CallNotes = cc.CallNotes,
                                                CallDate = cc.CallDate.FormattedSuppressMidnightDate(),
                                                EventDate = cc.EventDate == DateUtility.SystemEarliestDateTime
                                                                    ? string.Empty
                                                                    : cc.EventDate.FormattedSuppressMidnightDate(),
                                                StatusCode = cc.EdiStatusCode
                                            })
                                            .ToArray();

                status.BillingDetails = shipment.Charges.Select(bd => new WSBillingDetail
                {
                    AmountDue = bd.AmountDue,
                    BillingCode = bd.ChargeCode.Code,
                    Category = bd.ChargeCode.Category,
                    Description = bd.ChargeCode.Description,
                    ReferenceNumber = shipmentNumber,
                    ReferenceType = DetailReferenceType.Shipment
                }).ToArray();

                status.Documents = shipment.Documents.Where(d => !d.IsInternal).Select(d => new WSShipmentDocumentInfo
	                {
                    DocumentName = d.Name,
                    DocumentKey = d.Id
                }).ToArray();
            }

            return status;
        }



        public static WSReturn CancelShipment(User user, string shipmentNumber, Customer customer)
        {
            if (!user.HasAccessTo(ViewCode.CustomerLoadDashboard) && !user.HasAccessToModify(ViewCode.Shipment))
            {
                var msg = WSMessage.ErrorMessage(ProcessorVars.WSNoPermission);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSReturn>();
            }

            if (shipmentNumber == null) shipmentNumber = string.Empty;
            var shipment = new ShipmentSearch().FetchShipmentByShipmentNumber(shipmentNumber, user.TenantId);

            if (shipment == null)
            {
                var msg = WSMessage.ErrorMessage("Shipment entry [Number: {0}] not found!", shipmentNumber);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSReturn>();
            }

            var number = customer == null ? string.Empty : customer.CustomerNumber;
            if (shipment.Customer.CustomerNumber != number)
            {
                var msg = WSMessage.ErrorMessage("Shipment entry [Number: {0}] not available to this account!", shipmentNumber);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSShipmentStatus>();
            }

            var view = new CustomerLoadDashboardImmitationView(user);
            view.CancelShipment(shipment);
            return new WSReturn { Messages = view.Messages.ToArray() };
        }

        public static WSReturn CancelBookingRequest(User user, string bookingNumber, Customer customer)
        {
            if (!user.HasAccessToModify(ViewCode.LoadOrder))
            {
                var msg = WSMessage.ErrorMessage(ProcessorVars.WSNoPermission);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSReturn>();
            }

            if (bookingNumber == null) bookingNumber = string.Empty;
            var load = new LoadOrderSearch().FetchLoadOrderByLoadNumber(bookingNumber, user.TenantId);

            if (load == null)
            {
                var msg = WSMessage.ErrorMessage("Load record [Number: {0}] not found!", bookingNumber);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSBookingRequestStatus>();
            }

            var number = customer == null ? string.Empty : customer.CustomerNumber;
            if (load.Customer.CustomerNumber != number)
            {
                var msg = WSMessage.ErrorMessage("Load record [Number: {0}] not available to this account!", bookingNumber);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSShipmentStatus>();
            }

            var view = new LoadOrderImmitationView(user);

            view.LockRecord(load);
            if (view.Messages.ContainsErrors()) return view.Messages.ToMessageReturn<WSReturn>();

            view.CancelLoadOrder(load);
            return new WSReturn { Messages = view.Messages.ToArray() };
        }



        public static WSInvoice FetchInvoice(User user, string invoiceNumber, Customer customer)
        {
            if (!user.HasAccessTo(ViewCode.CustomerInvoiceDashboard, ViewCode.InvoiceDashboard, ViewCode.Invoice))
            {
                var msg = WSMessage.ErrorMessage(ProcessorVars.WSNoPermission);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSInvoice>();
            }

            if (invoiceNumber == null) invoiceNumber = string.Empty;
            var invoice = new InvoiceSearch().FetchInvoiceByInvoiceNumber(invoiceNumber, user.TenantId);

            if (invoice == null || !invoice.Posted)
            {
                var msg = WSMessage.ErrorMessage("Invoice entry [Number: {0}] not found!", invoiceNumber);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSInvoice>();
            }

            var number = customer == null ? string.Empty : customer.CustomerNumber;
            var location = invoice.CustomerLocation;
            if (location.Customer.CustomerNumber != number)
            {
                var msg = WSMessage.ErrorMessage("Invoice entry [Number: {0}] not available to this account!", invoiceNumber);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSInvoice>();
            }

            var contact = (location.Contacts.FirstOrDefault(c => c.Primary) ?? location.Contacts.FirstOrDefault()) ?? new CustomerContact();
            var wsInvoice = new WSInvoice
            {
                Billing = new WSLocation2
                {
                    City = location.City,
                    State = location.State,
                    Country = new WSCountry
                    {
                        Code = location.Country.Code,
                        Name = location.Country.Name,
                        UsesPostalCode = location.Country.EmploysPostalCodes
                    },
                    Contact = contact.Name,
                    Email = contact.Email,
                    Fax = contact.Fax,
                    Description = location.Customer.Name,
                    Phone = contact.Phone,
                    PostalCode = location.PostalCode,
                    SpecialInstructions = invoice.SpecialInstruction,
                    GeneralInfo = string.Empty,
                    Direction = string.Empty,
                    Street = location.Street1,
                    StreetExtra = location.Street2,
                    ContactComment = contact.Comment,
                    Mobile = contact.Mobile
                },
                BillingDetails = invoice.Details
                    .Select(d => new WSBillingDetail
                    {
                        AmountDue = d.AmountDue,
                        BillingCode = d.ChargeCode.Code,
                        Category = d.ChargeCode.Category,
                        Description = d.ChargeCode.Description,
                        ReferenceNumber = d.ReferenceNumber,
                        ReferenceType = d.ReferenceType
                    })
                    .ToArray(),
                CustomerControlAccount = invoice.CustomerControlAccountNumber,
                Date = invoice.PostDate,
                DueDate = invoice.DueDate,
                InvoiceNumber = invoice.InvoiceNumber,
                Terms = location.Customer.InvoiceTerms,
                Type = invoice.InvoiceType
            };

            foreach (var detail in wsInvoice.BillingDetails)
            {
                wsInvoice.TotalAmountDue += detail.AmountDue;
                switch (detail.Category)
                {
                    case ChargeCodeCategory.Fuel:
                        wsInvoice.TotalFuelCharges += detail.AmountDue;
                        break;
                    case ChargeCodeCategory.Freight:
                        wsInvoice.TotalFreightCharges += detail.AmountDue;
                        break;
                    case ChargeCodeCategory.Accessorial:
                        wsInvoice.TotalAccessorialCharges += detail.AmountDue;
                        break;
                    case ChargeCodeCategory.Service:
                        wsInvoice.TotalServiceCharges += detail.AmountDue;
                        break;
                }
            }

            return wsInvoice;
        }

        public static WSInvoiceSummary[] FetchInvoicesByDueDate(User user, DateTime startDate, DateTime endDate, Customer customer)
        {
            if (!user.HasAccessTo(ViewCode.CustomerInvoiceDashboard, ViewCode.InvoiceDashboard, ViewCode.Invoice))
            {
                var msg = WSMessage.ErrorMessage(ProcessorVars.WSNoPermission);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturnCollection<WSInvoiceSummary>();
            }

            var criteria = new InvoiceViewSearchCriteria { ActiveUserId = user.Id, };
            var p = AccountingSearchFields.DueDate.ToParameterColumn();
            p.Operator = Operator.Between;
            p.DefaultValue = startDate.TimeToMinimum().ToString();
            p.DefaultValue2 = endDate.TimeToMaximum().ToString();
            criteria.Parameters.Add(p);
            var invoices = new InvoiceSearch().FetchInvoiceDashboardDtos(criteria, user.TenantId);

            var number = customer == null ? string.Empty : customer.CustomerNumber;
            var wsInvoices = invoices
                .Where(i => i.Posted && i.CustomerNumber == number)
                .Select(i => new WSInvoiceSummary
                {
                    Date = i.PostDate,
                    DueDate = i.DueDate,
                    InvoiceNumber = i.InvoiceNumber,
                    InvoiceType = i.InvoiceType,
                    LocationCity = i.LocationCity,
                    LocationCountryName = i.LocationCountryName,
                    LocationPostalCode = i.LocationPostalCode,
                    LocationState = i.LocationState,
                    LocationStreet = i.LocationStreet1,
                    LocationStreetExtra = i.LocationStreet2,
                    OriginalInvoiceNumber = i.OriginalInvoiceNumber,
                    SpecialInstruction = i.SpecialInstruction,
                    TotalAmount = i.AmountDue
                })
                .ToArray();

            return wsInvoices.Any()
                    ? wsInvoices
                    : new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = WebApplicationConstants.NoResults } }.ToMessageReturnCollection
                        <WSInvoiceSummary>();
        }

        public static WSInvoiceSummary[] FetchInvoicesByDate(User user, DateTime startDate, DateTime endDate, Customer customer)
        {
            if (!user.HasAccessTo(ViewCode.CustomerInvoiceDashboard, ViewCode.InvoiceDashboard, ViewCode.Invoice))
            {
                var msg = WSMessage.ErrorMessage(ProcessorVars.WSNoPermission);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturnCollection<WSInvoiceSummary>();
            }

            var criteria = new InvoiceViewSearchCriteria { ActiveUserId = user.Id, };
            var p = AccountingSearchFields.PostDate.ToParameterColumn();
            p.Operator = Operator.Between;
            p.DefaultValue = startDate.TimeToMinimum().ToString();
            p.DefaultValue2 = endDate.TimeToMaximum().ToString();
            criteria.Parameters.Add(p);
            var invoices = new InvoiceSearch().FetchInvoiceDashboardDtos(criteria, user.TenantId);

            var number = customer == null ? string.Empty : customer.CustomerNumber;
            var wsInvoices = invoices
                .Where(i => i.Posted && i.CustomerNumber == number)
                .Select(i => new WSInvoiceSummary
                {
                    Date = i.PostDate,
                    DueDate = i.DueDate,
                    InvoiceNumber = i.InvoiceNumber,
                    InvoiceType = i.InvoiceType,
                    LocationCity = i.LocationCity,
                    LocationCountryName = i.LocationCountryName,
                    LocationPostalCode = i.LocationPostalCode,
                    LocationState = i.LocationState,
                    LocationStreet = i.LocationStreet1,
                    LocationStreetExtra = i.LocationStreet2,
                    OriginalInvoiceNumber = i.OriginalInvoiceNumber,
                    SpecialInstruction = i.SpecialInstruction,
                    TotalAmount = i.AmountDue
                })
                .ToArray();

            return wsInvoices.Any()
                    ? wsInvoices
                    : new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = WebApplicationConstants.NoResults } }.ToMessageReturnCollection
                        <WSInvoiceSummary>();
        }

        public static WSInvoiceSummary[] FetchInvoicesForShipment(User user, string shipmentNumber, Customer customer)
        {
            if (!user.HasAccessTo(ViewCode.CustomerInvoiceDashboard, ViewCode.InvoiceDashboard, ViewCode.Invoice))
            {
                var msg = WSMessage.ErrorMessage(ProcessorVars.WSNoPermission);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturnCollection<WSInvoiceSummary>();
            }

            var dto = new InvoiceSummaryDto();
            if (shipmentNumber == null) shipmentNumber = string.Empty;
            var invoices = dto.FetchInvoices(shipmentNumber, DetailReferenceType.Shipment, user.TenantId);
            var adjustments = new List<InvoiceSummaryDto>();
            foreach (var invoice in invoices)
                adjustments.AddRange(dto.FetchInvoiceSupplementalsAndCredits(invoice.Id, user.TenantId));
            invoices.AddRange(adjustments);

            var number = customer == null ? string.Empty : customer.CustomerNumber;
            var wsInvoices = invoices
                .Where(i => i.Posted && i.CustomerNumber == number)
                .Select(i => new WSInvoiceSummary
                {
                    Date = i.PostDate,
                    DueDate = i.DueDate,
                    InvoiceNumber = i.InvoiceNumber,
                    InvoiceType = i.InvoiceType,
                    LocationCity = i.City,
                    LocationCountryName = i.CountryName,
                    LocationPostalCode = i.PostalCode,
                    LocationState = i.State,
                    LocationStreet = i.Street1,
                    LocationStreetExtra = i.Street2,
                    OriginalInvoiceNumber = i.OriginalInvoiceNumber,
                    SpecialInstruction = i.SpecialInstruction,
                    TotalAmount = i.TotalAmount
                })
                .ToArray();

            return wsInvoices.Any()
                    ? wsInvoices
                    : new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = WebApplicationConstants.NoResults } }.ToMessageReturnCollection
                        <WSInvoiceSummary>();
        }



        public static WSBookingRequest SaveBookingRequest(User user, WSBookingRequest request, Customer customer)
        {
            if (!user.HasAccessTo(ViewCode.RateAndSchedule) && !user.HasAccessToModify(ViewCode.LoadOrder))
            {
                var msg = WSMessage.ErrorMessage(ProcessorVars.WSNoPermission);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSBookingRequest>();
            }

            if (request.DisableApiBookingNotifications && !user.HasAccessTo(ViewCode.CanDisableApiBookingNotifications))
            {
                var msg = WSMessage.ErrorMessage("You do not have permission to disable booking notifications");
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSBookingRequest>();
            }

            if (request.OverrideApiRatingDates)
            {
                // if applicable, ensure user has permission to rate a historical date
                if (request.DesiredShipmentDate.TimeToMinimum() < DateTime.Now.TimeToMinimum() && !user.HasAccessTo(ViewCode.CanOverrideApiRatingDates))
                {
                    var msg = WSMessage.ErrorMessage("You have selected a date prior to today but do not have access to perform this function!");
                    EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                    return new[] { msg }.ToMessageReturn<WSBookingRequest>();
                }
            }
            else
            {
                // check shipment date
                if (request.DesiredShipmentDate.TimeToMinimum() < DateTime.Now.TimeToMinimum())
                {
                    var msg = WSMessage.ErrorMessage("Desired shipment date must greater than or equal to today.");
                    EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                    return new[] { msg }.ToMessageReturn<WSBookingRequest>();
                }
            }

            var load = request.ToOfferedLoadOrder(user, customer);

            var view = new RateAndScheduleImmitationView(user);
            view.Save(load);
            if (view.Messages.ContainsErrors()) return view.Messages.ToMessageReturn<WSBookingRequest>();

            if (!request.DisableApiBookingNotifications)
            {
                // send email notifications
                view.ProcessesNotification(load, HttpContext.Current);
            }
           
            request.Messages = view.Messages.ToArray();
            request.SubmittedRequestNumber = !view.Messages.ContainsErrors() ? view.LoadOrder.LoadOrderNumber : string.Empty;

            return request;
        }

        public static WSShipment2 GetRates(User user, WSShipment2 shipment, Customer customer)
        {
            var messages = new List<WSMessage>();

            if (!user.HasAccessTo(ViewCode.RateAndSchedule, ViewCode.EstimateRate, ViewCode.Shipment, ViewCode.LoadOrder))
            {
                var msg = WSMessage.ErrorMessage(ProcessorVars.WSNoPermission);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSShipment2>();
            }

            if (shipment.OverrideApiRatingDates)
            {
                // if applicable, ensure user has permission to rate a historical date
                if (shipment.ShipmentDate.TimeToMinimum() < DateTime.Now.TimeToMinimum() && !user.HasAccessTo(ViewCode.CanOverrideApiRatingDates))
                {
                    var msg = WSMessage.ErrorMessage("You have selected a date prior to today but do not have access to perform this function!");
                    EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                    return new[] { msg }.ToMessageReturn<WSShipment2>();
                }

                // ensure date does not exceed the 7 day limit
                if (shipment.ShipmentDate.TimeToMinimum() > DateTime.Now.AddDays(7).TimeToMinimum())
                {
                    var msg = WSMessage.ErrorMessage("Shipment date cannot exceed today+7 days.");
                    EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}",
                        user.Tenant.Name, user.FirstName, user.LastName,
                        user.Username, msg.Value,
                        HttpContext.Current.Request.UserHostAddress);
                    return new[] { msg }.ToMessageReturn<WSShipment2>();
                }
            }
            else
            {
                // check shipment date
                if (shipment.ShipmentDate.TimeToMinimum() < DateTime.Now.TimeToMinimum() ||
                    shipment.ShipmentDate.TimeToMinimum() > DateTime.Now.AddDays(7).TimeToMinimum())
                {
                    var msg = WSMessage.ErrorMessage("Shipment date must be between today and today+7 days.");
                    EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                    return new[] { msg }.ToMessageReturn<WSShipment2>();
                }
            }


            // validate minimum required rating fields
            messages.AddRange(shipment.ValidateMinimumForRating());
            if (messages.ContainsErrors())
            {
                foreach (var msg in messages.Where(m => m.Type == WSEnums.MessageType.Error))
                    EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return messages.ToMessageReturn<WSShipment2>();
            }

            // rate shipment
            var shipmentForRating = shipment.ToShipmentForRating(user, customer);
            var rates = shipmentForRating.RateShipment(HttpContext.Current);
            shipment.AvailableRates = rates.ToWSRates(HttpContext.Current);

            // save shopped rate
            var shoppedRate = shipmentForRating.GetShoppedRate(shipment.AvailableRates, ShoppedRateType.WSEstimate);
            var view = new RateAndScheduleImmitationView(user);
            view.Save(shoppedRate);
            if (view.Messages.ContainsErrors())
            {
                var msg = string.Join(Environment.NewLine,
                                      view.Messages.Where(m => m.Type == WSEnums.MessageType.Error).Select(m => m.Value).ToArray());
                ErrorLogger.LogError(new Exception(msg), HttpContext.Current);
            }

            // set customer custom references
            if (customer.CustomFields.Any())
            {
                var customReferences = customer.CustomFields
                    .Select(c => new WSReference { Name = c.Name, Value = string.Empty, Required = c.Required })
                    .ToArray();
                var existingRefNames = shipment.CustomReferences == null
                                        ? new List<string>()
                                        : shipment.CustomReferences.Select(r => r.Name).ToList();
                var refsToAdd = customReferences.Where(r => !existingRefNames.Contains(r.Name)).ToList();
                if (refsToAdd.Any())
                {
                    if (shipment.CustomReferences != null) refsToAdd.AddRange(shipment.CustomReferences);
                    shipment.CustomReferences = refsToAdd.ToArray();
                }
            }

            return shipment;
        }

        public static WSShipment2 BookShipment(User user, WSShipment2 shipment, Customer customer)
        {
            var messages = new List<WSMessage>();

            if (shipment.SelectedRate == null)
            {
                var msg = WSMessage.ErrorMessage("Selected rate is missing");
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSShipment2>();
            }

            if (shipment.DisableApiBookingNotifications && !user.HasAccessTo(ViewCode.CanDisableApiBookingNotifications))
            {
                var msg = WSMessage.ErrorMessage("You do not have permission to disable booking notifications");
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSShipment2>();
            }

            var noRateAndScheduleAccess = !user.HasAccessTo(ViewCode.RateAndSchedule);
            if (shipment.SelectedRate.ServiceMode == ServiceMode.Truckload &&
                noRateAndScheduleAccess && !user.HasAccessToModify(ViewCode.LoadOrder))
            {
                var msg = WSMessage.ErrorMessage(ProcessorVars.WSNoPermission);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSShipment2>();
            }

            if (noRateAndScheduleAccess && !user.HasAccessToModify(ViewCode.Shipment))
            {
                var msg = WSMessage.ErrorMessage(ProcessorVars.WSNoPermission);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSShipment2>();
            }

            if (shipment.OverrideApiRatingDates)
            {
                // if applicable, ensure user has permission to rate a historical date
                if (shipment.ShipmentDate.TimeToMinimum() < DateTime.Now.TimeToMinimum() && !user.HasAccessTo(ViewCode.CanOverrideApiRatingDates))
                {
                    var msg = WSMessage.ErrorMessage("You have selected a date prior to today but do not have access to perform this function!");
                    EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                    return new[] { msg }.ToMessageReturn<WSShipment2>();
                }

                // ensure date does not exceed the 7 day limit
                if (shipment.ShipmentDate.TimeToMinimum() > DateTime.Now.AddDays(7).TimeToMinimum())
                {
                    var msg = WSMessage.ErrorMessage("Shipment date cannot exceed today+7 days.");
                    EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}",
                        user.Tenant.Name, user.FirstName, user.LastName,
                        user.Username, msg.Value,
                        HttpContext.Current.Request.UserHostAddress);
                    return new[] { msg }.ToMessageReturn<WSShipment2>();
                }
            }
            else
            {
                // check shipment date
                if (shipment.ShipmentDate.TimeToMinimum() < DateTime.Now.TimeToMinimum() ||
                    shipment.ShipmentDate.TimeToMinimum() > DateTime.Now.AddDays(7).TimeToMinimum())
                {
                    var msg = WSMessage.ErrorMessage("Shipment date must be between today and today+7 days.");
                    EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                    return new[] { msg }.ToMessageReturn<WSShipment2>();
                }
            }

            // validate minimum required rating fields
            messages.AddRange(shipment.ValidateMinimumForRating());
            if (messages.ContainsErrors())
            {
                foreach (var msg in messages.Where(m => m.Type == WSEnums.MessageType.Error))
                    EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return messages.ToMessageReturn<WSShipment2>();
            }

            // re-rate shipment (rate checking)
            var sh = shipment.ToShipmentForRating(user, customer);
            var rates = sh.RateShipment(HttpContext.Current);
            shipment.AvailableRates = rates.ToWSRates(HttpContext.Current);

            // book shipment
            var matchingRate = shipment.SelectedRate.FindMatchingRate(rates);
            if (matchingRate == null)
            {
                var msg = WSMessage.ErrorMessage("Unable to match selected rate.");
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSShipment2>();
            }

            // validate shipment item dimensions when LTL rate selected
            if (matchingRate.Mode == ServiceMode.LessThanTruckload)
            {
                var msgs = new List<WSMessage>();
                foreach (var item in shipment.Items)
                {
                    if (item.Length == 0 || item.Height == 0 || item.Width == 0)
                    {
                        var msg = WSMessage.ErrorMessage("Less Than Truckload shipment item must have non-zero dimensions. [L:{0}, W:{1}, H:{2}]", item.Length, item.Width, item.Height);
                        EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                        msgs.Add(msg);
                    }
                }

                if (msgs.Any()) return msgs.ToMessageReturn<WSShipment2>();
            }

            // account for guaranteed service
            var gsChg = matchingRate.GetGuarRateCharge(shipment.SelectedRate.SelectedGuarOption);
            shipment.BookedRate = matchingRate.ToWSRate(gsChg); // must have prior to update for booking
            messages.AddRange(shipment.CheckVarianceInSelectedAndBookedRates()); // warn about changes to rate
            Exception carrierConnectException;
            sh.UpdateShipmentForBooking(shipment, matchingRate, gsChg, out carrierConnectException);
            if (carrierConnectException != null) ErrorLogger.LogError(carrierConnectException, HttpContext.Current);


            // view for saving
            var view = new RateAndScheduleImmitationView(user);


            // book shipment/record quote
            ShoppedRate shoppedRate;



            switch (matchingRate.Mode)
            {
                case ServiceMode.Truckload:
                    var load = shipment.ToQuotedLoadOrder(user, customer, matchingRate);
                    view.Save(load);
                    if (view.Messages.ContainsErrors()) return view.Messages.ToMessageReturn<WSShipment2>();

                    if (!shipment.DisableApiBookingNotifications)
                    {
                        // send email notifications
                        view.ProcessesNotification(load, HttpContext.Current);
                    }

                    shoppedRate = sh.GetShoppedRate(shipment.AvailableRates, ShoppedRateType.Quote);
                    shoppedRate.LoadOrder = view.LoadOrder;
                    shoppedRate.ReferenceNumber = view.LoadOrder != null ? view.LoadOrder.LoadOrderNumber : string.Empty;

                    shipment.BookingReferenceNumber = load.LoadOrderNumber;
                    shipment.BookingReferenceNumberType = WSEnums.BookingReferenceNumberType.Quote;
                    shipment.ShipperBOL = string.Empty;

                    shipment.ReturnConfirmations = new[]
                                                        {
                                                            new WSDocument
                                                                {
                                                                    Name = string.Format("{0}_Quote", load.LoadOrderNumber),
                                                                    DocType = WSEnums.DocumentType.Pdf,
                                                                    DocImage = view.GenerateQuoteDocument(load, HttpContext.Current)
                                                                }
                                                        };
                    break;
                default:

                    view.Save(sh);
                    if (view.Messages.ContainsErrors()) return view.Messages.ToMessageReturn<WSShipment2>();
                    if (matchingRate.Mode == ServiceMode.SmallPackage) view.BookPackageShipment(sh, HttpContext.Current);
                    if (matchingRate.Mode == ServiceMode.LessThanTruckload)
                    {
                        view.InitiateLTLShipmentDispatch(sh);
                        view.InitiateLTLShipmentTracking(sh);
                    }
                    if (view.Messages.Any()) messages.AddRange(view.Messages);


                    if (!shipment.DisableApiBookingNotifications)
                    {
                        // send email notifications
                        view.ProcessesNotification(sh, HttpContext.Current);
                    }

                    shoppedRate = sh.GetShoppedRate(shipment.AvailableRates, ShoppedRateType.WSBooking);
                    shoppedRate.Shipment = view.Shipment;
                    shoppedRate.ReferenceNumber = view.Shipment != null ? view.Shipment.ShipmentNumber : string.Empty;

                    shipment.BookingReferenceNumber = sh.ShipmentNumber;
                    shipment.BookingReferenceNumberType = WSEnums.BookingReferenceNumberType.Shipment;
                    shipment.ShipperBOL = sh.ShipperBol;

                    // documents
                    var wsDocuments = sh.Documents
                        .Select(d => new WSDocument
                        {
                            DocType = HttpContext.Current.Server.GetFileExtension(d.LocationPath).GetDocumentType(),
                            DocImage = HttpContext.Current.Server.ReadFromFile(d.LocationPath),
                            Name = d.Name
                        })
                        .ToList();
                    wsDocuments.Add(new WSDocument
                    {
                        Name = string.Format("{0}_BillOfLading", sh.ShipmentNumber),
                        DocType = WSEnums.DocumentType.Pdf,
                        DocImage = view.GenerateBOLDocument(sh, HttpContext.Current)
                    });

                    wsDocuments.Add(new WSDocument
                    {
                        Name = string.Format("ProLabel4x6inch_{0}", sh.ShipmentNumber),
                        DocType = WSEnums.DocumentType.Pdf,
                        DocImage = view.GenerateProLabels4x6Document(sh, HttpContext.Current, DocumentTemplateCategory.ProLabel4x6inch.ToString())
                    });

                    wsDocuments.Add(new WSDocument
                    {
                        Name = string.Format("ProLabelAvery3x4inch_{0}", sh.ShipmentNumber),
                        DocType = WSEnums.DocumentType.Pdf,
                        DocImage = view.GenerateProLabelAvery3x4Document(sh, HttpContext.Current, DocumentTemplateCategory.ProLabelAvery3x4inch.ToString())
                    });
                    shipment.ReturnConfirmations = wsDocuments.ToArray();
                    break;
            }

            // save shopped rate
            view.Save(shoppedRate);
            if (view.Messages.ContainsErrors())
            {
                var msg = string.Join(Environment.NewLine,
                                      view.Messages.Where(m => m.Type == WSEnums.MessageType.Error).Select(m => m.Value).ToArray());
                ErrorLogger.LogError(new Exception(msg), HttpContext.Current);
            }

            shipment.Messages = messages.ToArray();
            return shipment;
        }

        public static WSShipment2 SaveQuote(User user, WSShipment2 quote, Customer customer)
        {
            var messages = new List<WSMessage>();

            if (quote.SelectedRate == null)
            {
                var msg = WSMessage.ErrorMessage("Selected rate is missing");
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}",
                                                            user.Tenant.Name, user.FirstName, user.LastName,
                                                            user.Username, msg.Value,
                                                            HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSShipment2>();
            }

            if (quote.DisableApiBookingNotifications && !user.HasAccessTo(ViewCode.CanDisableApiBookingNotifications))
            {
                var msg = WSMessage.ErrorMessage("You do not have permission to disable booking notifications");
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSShipment2>();
            }

            var noRateAndScheduleAccess = !user.HasAccessTo(ViewCode.RateAndSchedule);
            if (noRateAndScheduleAccess && !user.HasAccessTo(ViewCode.CanSubmitRatedQuote))
            {
                var msg = WSMessage.ErrorMessage(ProcessorVars.WSNoPermission);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}",
                                                            user.Tenant.Name, user.FirstName, user.LastName,
                                                            user.Username, msg.Value,
                                                            HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSShipment2>();
            }

            if (quote.OverrideApiRatingDates)
            {
                // if applicable, ensure user has permission to rate a historical date
                if (quote.ShipmentDate.TimeToMinimum() < DateTime.Now.TimeToMinimum() && !user.HasAccessTo(ViewCode.CanOverrideApiRatingDates))
                {
                    var msg = WSMessage.ErrorMessage("You have selected a date prior to today but do not have access to perform this function!");
                    EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                    return new[] { msg }.ToMessageReturn<WSShipment2>();
                }

                // ensure date does not exceed the 7 day limit
                if (quote.ShipmentDate.TimeToMinimum() > DateTime.Now.AddDays(7).TimeToMinimum())
                {
                    var msg = WSMessage.ErrorMessage("Quote date cannot exceed today+7 days.");
                    EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}",
                        user.Tenant.Name, user.FirstName, user.LastName,
                        user.Username, msg.Value,
                        HttpContext.Current.Request.UserHostAddress);
                    return new[] { msg }.ToMessageReturn<WSShipment2>();
                }
            }
            else
            {
                // check quoted load date
                if (quote.ShipmentDate.TimeToMinimum() < DateTime.Now.TimeToMinimum() ||
                    quote.ShipmentDate.TimeToMinimum() > DateTime.Now.AddDays(7).TimeToMinimum())
                {
                    var msg = WSMessage.ErrorMessage("Quote date must be between today and today+7 days.");
                    EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}",
                        user.Tenant.Name, user.FirstName, user.LastName,
                        user.Username, msg.Value,
                        HttpContext.Current.Request.UserHostAddress);
                    return new[] { msg }.ToMessageReturn<WSShipment2>();
                }
            }



            // validate minimum required rating fields
            messages.AddRange(quote.ValidateMinimumForRating());
            if (messages.ContainsErrors())
            {
                foreach (var msg in messages.Where(m => m.Type == WSEnums.MessageType.Error))
                    EnvironmentUtilities.PageAccessLogger.Error(
                        "Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName,
                        user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return messages.ToMessageReturn<WSShipment2>();
            }

            // re-rate quoted load (rate checking)
            var sh = quote.ToShipmentForRating(user, customer);
            var rates = sh.RateShipment(HttpContext.Current);
            quote.AvailableRates = rates.ToWSRates(HttpContext.Current);

            // prepare to save quote
            var matchingRate = quote.SelectedRate.FindMatchingRate(rates);
            if (matchingRate == null)
            {
                var msg = WSMessage.ErrorMessage("Unable to match selected rate.");
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}",
                                                            user.Tenant.Name, user.FirstName, user.LastName,
                                                            user.Username, msg.Value,
                                                            HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSShipment2>();
            }

            // account for guaranteed service
            var gsChg = matchingRate.GetGuarRateCharge(quote.SelectedRate.SelectedGuarOption);
            quote.BookedRate = matchingRate.ToWSRate(gsChg); // must have prior to update for saving quote
            messages.AddRange(quote.CheckVarianceInSelectedAndBookedRates()); // warn about changes to rate

            // view for saving
            var view = new RateAndScheduleImmitationView(user);

            // generate LoadOrder object
            var load = quote.ToQuotedLoadOrder(user, customer, matchingRate, gsChg);

            load.EstimatedDeliveryDate = quote.BookedRate.EstimatedDeliveryDate;
            load.DesiredPickupDate = sh.DesiredPickupDate;

            // add primary vendor
            var vendor = new VendorSearch().FetchVendorByNumber(quote.BookedRate.CarrierKey, sh.User.TenantId);
            load.Vendors.Add(new LoadOrderVendor
            {
                LoadOrder = load,
                TenantId = load.TenantId,
                Vendor = vendor,
                Primary = true,
                ProNumber = string.Empty,
            });

            view.Save(load);
            if (view.Messages.ContainsErrors()) return view.Messages.ToMessageReturn<WSShipment2>();


            if (!quote.DisableApiBookingNotifications)
            {
                // send email notifications
                view.ProcessesNotification(load, HttpContext.Current);
            }


            // save shopped rate
            var shoppedRate = sh.GetShoppedRate(quote.AvailableRates, ShoppedRateType.Quote);
            shoppedRate.LoadOrder = view.LoadOrder;
            shoppedRate.ReferenceNumber = view.LoadOrder != null ? view.LoadOrder.LoadOrderNumber : string.Empty;

            view.Save(shoppedRate);

            if (view.Messages.ContainsErrors())
            {
                var msg = string.Join(Environment.NewLine,
                                      view.Messages.Where(m => m.Type == WSEnums.MessageType.Error)
                                          .Select(m => m.Value)
                                          .ToArray());
                ErrorLogger.LogError(new Exception(msg), HttpContext.Current);
            }

            // documents
            var wsDocuments = sh.Documents
                .Select(d => new WSDocument
                {
                    DocType = HttpContext.Current.Server.GetFileExtension(d.LocationPath).GetDocumentType(),
                    DocImage = HttpContext.Current.Server.ReadFromFile(d.LocationPath),
                    Name = d.Name
                })
                .ToList();
            wsDocuments.Add(new WSDocument
            {
                Name = string.Format("{0}_Quote", load.LoadOrderNumber),
                DocType = WSEnums.DocumentType.Pdf,
                DocImage = view.GenerateQuoteDocument(load, HttpContext.Current)
            });


            quote.ReturnConfirmations = wsDocuments.ToArray();
            quote.BookingReferenceNumber = load.LoadOrderNumber;
            quote.Messages = messages.ToArray();

            return quote;
        }

        public static WSShipment2 ConvertQuoteToShipment(User user, string quoteNumber)
        {
            var messages = new List<WSMessage>();

            var loadOrder = new LoadOrderSearch().FetchLoadOrderByLoadNumber(quoteNumber, user.TenantId);
            if (loadOrder == null)
            {
                var msg = WSMessage.ErrorMessage("Quote entry [Number: {0}] not found!", quoteNumber);
                EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                return new[] { msg }.ToMessageReturn<WSShipment2>();
            }

            // views for saving and document generation
            var view = new LoadOrderImmitationView(user);
            var rsView = new RateAndScheduleImmitationView(user);

            loadOrder.LoadCollections();
            loadOrder.TakeSnapShot();

            view.LockRecord(loadOrder);
            if (view.Messages.ContainsErrors()) return view.Messages.ToMessageReturn<WSShipment2>();

            view.ConvertLoadOrderToShipment(loadOrder);
            if (view.Messages.ContainsErrors())
            {
                view.UnLockRecord(loadOrder);
                return view.Messages.ToMessageReturn<WSShipment2>();
            }

            view.UnLockRecord(loadOrder);

            var shipment = new ShipmentSearch().FetchShipmentByShipmentNumber(loadOrder.LoadOrderNumber,
                                                                              loadOrder.TenantId);

            // documents
            var wsDocuments = new List<WSDocument>
                    {
                        new WSDocument
                            {
                                Name = string.Format("{0}_BillOfLading", loadOrder.LoadOrderNumber),
                                DocType = WSEnums.DocumentType.Pdf,
                                DocImage = rsView.GenerateBOLDocument(shipment, HttpContext.Current)
                            }
                    };

            var wsShipment = new WSShipment2
            {
                ReferenceNumber = shipment.ShipmentNumber,
                ReturnConfirmations = wsDocuments.ToArray(),
                Messages = messages.ToArray()
            };

            return wsShipment;
        }



        public static WSVendorProNumber UpdateVendorProNumber(WSVendorProNumber proNumber, User user)
        {
            var loadOrder = new LoadOrderSearch().FetchLoadOrderByLoadNumber(proNumber.ReferenceNumber, user.TenantId);
            var shipment = new ShipmentSearch().FetchShipmentByShipmentNumber(proNumber.ReferenceNumber, user.TenantId);
            var saveAttempted = false;
            var messages = new List<WSMessage>();

            if (loadOrder != null)
            {
                saveAttempted = true;

                if (!user.HasAccessTo(ViewCode.LoadOrder))
                {
                    messages.Add(WSMessage.ErrorMessage("You do not have permission to modify Quotes"));
                }
                else
                {
                    loadOrder.LoadCollections();

                    if (!loadOrder.Vendors.Any() || loadOrder.Vendors.FirstOrDefault(v => v.Vendor.Scac == proNumber.VendorScac) == null)
                    {
                        var msg = WSMessage.ErrorMessage("Could not find vendor with SCAC '{0}' for Quote # {1}!",
                                                         proNumber.VendorScac, proNumber.ReferenceNumber);
                        messages.Add(msg);
                        EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                    }
                    else
                    {
                        loadOrder.Vendors.First(v => v.Vendor.Scac == proNumber.VendorScac).TakeSnapShot();
                        loadOrder.Vendors.First(v => v.Vendor.Scac == proNumber.VendorScac).ProNumber = proNumber.VendorProNumber;

                        var view = new LoadOrderImmitationView(user);
                        view.LockRecord(loadOrder);
                        view.SaveLoadOrder(loadOrder);
                        view.UnLockRecord(loadOrder);

                        if (view.Messages.ContainsErrors())
                        {
                            messages.AddRange(view.Messages);
                        }
                        else
                        {
                            messages.Add(
                                WSMessage.InformationMessage(
                                    "Vendor Pro Number '{0}' applied to Quote # {1} for Vendor SCAC '{2}'",
                                    proNumber.VendorProNumber, proNumber.ReferenceNumber, proNumber.VendorScac));
                        }
                    }
                }
            }
            if (shipment != null)
            {
                saveAttempted = true;

                if (!user.HasAccessTo(ViewCode.Shipment))
                {
                    messages.Add(WSMessage.ErrorMessage("You do not have permission to modify Shipments"));
                }
                else
                {
                    shipment.LoadCollections();

                    if (!shipment.Vendors.Any() || shipment.Vendors.FirstOrDefault(v => v.Vendor.Scac == proNumber.VendorScac) == null)
                    {
                        var msg = WSMessage.ErrorMessage("Could not find vendor with SCAC '{0}' for Shipment # {1}!",
                                                         proNumber.VendorScac, proNumber.ReferenceNumber);
                        messages.Add(msg);
                        EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, msg.Value, HttpContext.Current.Request.UserHostAddress);
                    }
                    else
                    {
                        shipment.Vendors.First(v => v.Vendor.Scac == proNumber.VendorScac).TakeSnapShot();
                        shipment.Vendors.First(v => v.Vendor.Scac == proNumber.VendorScac).ProNumber = proNumber.VendorProNumber;

                        var view = new ShipmentImmitationView(user);
                        view.LockRecord(shipment);
                        view.SaveShipment(shipment);
                        view.UnLockRecord(shipment);


                        if (view.Messages.ContainsErrors())
                        {
                            messages.AddRange(view.Messages);
                        }
                        else
                        {
                            messages.Add(
                               WSMessage.InformationMessage(
                                   "Vendor Pro Number '{0}' applied to Shipment # {1} for Vendor SCAC '{2}'",
                                   proNumber.VendorProNumber, proNumber.ReferenceNumber, proNumber.VendorScac));
                        }

                    }
                }
            }

            if (saveAttempted)
            {
                proNumber.Messages = messages.ToArray();
                return proNumber;
            }


            var message = WSMessage.ErrorMessage("Reference Number '{0}' not found!", proNumber.ReferenceNumber);
            EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1} {2} ({3})\tMessage:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, message.Value, HttpContext.Current.Request.UserHostAddress);
            return new[] { message }.ToMessageReturn<WSVendorProNumber>();
        }
    }
}