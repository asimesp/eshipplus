﻿using System;
using System.Web.Services.Protocols;

namespace LogisticsPlus.Eship.WebApplication.Services
{
	public class AuthenticationToken : SoapHeader
	{
		public string AccessCode { get; set; }
		public string Username { get; set; }
		public string Password { get; set; }
		public Guid AccessKey { get; set; }

		public AuthenticationToken()
		{
			Username = string.Empty;
			Password = string.Empty;
			AccessKey = Guid.Empty;
		}

		public void SetKey(string accesskey)
		{
			try
			{
				AccessKey = new Guid(accesskey);
			}
			catch
			{
				AccessKey = Guid.Empty;
			}
		}
	}
}
