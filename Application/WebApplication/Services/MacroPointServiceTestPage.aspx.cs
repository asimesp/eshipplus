﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web.UI;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.MacroPoint;
using LogisticsPlus.Eship.Processor;

namespace LogisticsPlus.Eship.WebApplication.Services
{
    public partial class MacroPointServiceTestPage : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var page = (Page)sender;
            var stream = new StreamReader(page.Request.InputStream);
            var xml = stream.ReadToEnd();

            OrderResult orderResult;

            var order = xml.FromXml<Order>();

            //Test if we can create an order from the xml given, if not, return bad request 
            if (order == null)
            {
                page.Response.StatusCode = HttpStatusCode.BadRequest.ToInt();

                orderResult = new OrderResult
                {
                    OrderID = null,
                    Status = OrderResultStatus.Failure.GetString(),
                    Errors = new List<Error>
                            {
                                (new Error
                                    {
                                        Code = ErrorCodes.AllArgumentDomainRuleValidationExceptions.ToInt().GetString(),
                                        Message = ErrorCodes.AllArgumentDomainRuleValidationExceptions.GetString(),
                                    })
                            }
                };

                page.Response.Write(orderResult.ToXml(true, true));
                return;
            }

            orderResult = new OrderResult
            {
                Status = OrderResultStatus.Success.GetString(),
                OrderID = string.Format("{0}{1}{2}", DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second),
                Errors = new List<Error>()
            };

            page.Response.StatusCode = HttpStatusCode.Created.ToInt();
            page.Response.Write(orderResult.ToXml(true, true));
        }
    }
}