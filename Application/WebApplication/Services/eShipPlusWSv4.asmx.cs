﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Extern.Ws;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Services
{
	/// <summary>
	/// Summary description for eShipPlusWSv4
	/// </summary>
	[WebService(Namespace = "http://www.eshipplus.com/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
	[System.Web.Script.Services.ScriptService]
	public class eShipPlusWSv4 : WebService
	{
		private const string AuthenticationMemberName = "AuthenticationToken";

		public AuthenticationToken AuthenticationToken;

		[SoapHeader(AuthenticationMemberName)]
		[WebMethod(Description = "Returns List of Accessorial Services available.")]
		public WSAccessorialService[] GetAccessorialServices()
		{
			try
			{
				var messages = new List<WSMessage>();

				var user = AuthenticationToken.IsValidUser(messages);
				if (messages.ContainsErrors()) return messages.ToMessageReturnCollection<WSAccessorialService>();

			    EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tMethod:{4}\t IP:{5}",
			                                               user.Tenant.Name, user.FirstName, user.LastName, user.Username,
                                                           "Get Accessorial Services", HttpContext.Current.Request.UserHostAddress);

			    return eShipPlusServiceBase.FetchAccessorialServices(user.TenantId);
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				return new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } }.ToMessageReturnCollection<WSAccessorialService>();
			}
		}

		[SoapHeader(AuthenticationMemberName)]
		[WebMethod(Description = "Returns list of LTL freight classes supported by service.")]
		public WSFreightClass[] GetLTLFreightClasses()
		{
			try
			{
				var messages = new List<WSMessage>();

				var user = AuthenticationToken.IsValidUser(messages);
				if (messages.ContainsErrors()) return messages.ToMessageReturnCollection<WSFreightClass>();

			    EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tMethod:{4}\t IP:{5}",
			                                               user.Tenant.Name, user.FirstName, user.LastName, user.Username,
			                                               "Get LTL Freight Classes", HttpContext.Current.Request.UserHostAddress);

			    return eShipPlusServiceBase.FetchLTLFreightClasses();
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				return new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } }.ToMessageReturnCollection<WSFreightClass>();
			}
		}

		[SoapHeader(AuthenticationMemberName)]
		[WebMethod(Description = "Returns list of item package types available.")]
		public WSItemPackage[] GetItemPackaging()
		{
			try
			{
				var messages = new List<WSMessage>();

				var user = AuthenticationToken.IsValidUser(messages);
				if (messages.ContainsErrors()) return messages.ToMessageReturnCollection<WSItemPackage>();

                EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tMethod:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, "Get Item Packaging", HttpContext.Current.Request.UserHostAddress);

			    return eShipPlusServiceBase.FetchItemPackaging(user.TenantId);
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				return new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } }.ToMessageReturnCollection<WSItemPackage>();
			}
		}

		[SoapHeader(AuthenticationMemberName)]
		[WebMethod(Description = "Returns list of serviced countries.")]
		public WSCountry[] GetCountries()
		{
			try
			{
				var messages = new List<WSMessage>();

				var user = AuthenticationToken.IsValidUser(messages);
				if (messages.ContainsErrors()) return messages.ToMessageReturnCollection<WSCountry>();

                EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tMethod:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, "Get Countries", HttpContext.Current.Request.UserHostAddress);

			    return eShipPlusServiceBase.FetchCountries();
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				return new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } }.ToMessageReturnCollection<WSCountry>();
			}
		}

		[SoapHeader(AuthenticationMemberName)]
		[WebMethod(Description = "Returns list of time string objects acceptable by service.")]
		public WSTime2[] GetTimes()
		{
			try
			{
				var messages = new List<WSMessage>();

				var user  = AuthenticationToken.IsValidUser(messages);
				if (messages.ContainsErrors()) return messages.ToMessageReturnCollection<WSTime2>();

                EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tMethod:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, "Get Times", HttpContext.Current.Request.UserHostAddress);

			    return eShipPlusServiceBase.FetchTimes();
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				return new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } }.ToMessageReturnCollection<WSTime2>();
			}
		}

		[SoapHeader(AuthenticationMemberName)]
		[WebMethod(Description = "Returns single object containing Accessorial Services, LTL Freight Classes, item package types, countries and times.")]
		public WSOptions GetInputOptions()
		{
			try
			{
				var messages = new List<WSMessage>();

				var user = AuthenticationToken.IsValidUser(messages);
				if (messages.ContainsErrors()) return messages.ToMessageReturn<WSOptions>();

                EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tMethod:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, "Get Input Options", HttpContext.Current.Request.UserHostAddress);

			    return eShipPlusServiceBase.FetchInputOptions(user.TenantId);
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				return new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } }.ToMessageReturn<WSOptions>();
			}
		}

		[SoapHeader(AuthenticationMemberName)]
		[WebMethod(Description = "Returns list of custom reference fields specific to account.")]
		public WSReference[] GetCustomerCustomReferences()
		{
			try
			{
				var messages = new List<WSMessage>();

				Customer customer;
				var user = AuthenticationToken.IsValidUser(messages, out customer);
				if (messages.ContainsErrors()) return messages.ToMessageReturnCollection<WSReference>();

                EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tMethod:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, "Get Customer Custom References", HttpContext.Current.Request.UserHostAddress);

			    return eShipPlusServiceBase.FetchCustomerCustomReferences(customer);
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				return new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } }.ToMessageReturnCollection<WSReference>();
			}
		}

	    [SoapHeader(AuthenticationMemberName)]
	    [WebMethod(Description = "Returns specified external shipment/quote document.")]
	    public WSShipmentDocument GetShipmentDocument(string referenceNumber, long documentId)
	    {
	        try
	        {
	            var messages = new List<WSMessage>();

	            Customer customer;
	            var user = AuthenticationToken.IsValidUser(messages, out customer);
	            if (messages.ContainsErrors()) return messages.ToMessageReturn<WSShipmentDocument>();

	            EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tMethod:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, "Get Shipment Document", HttpContext.Current.Request.UserHostAddress);

	            return eShipPlusServiceBase.FetchDocument(user, referenceNumber, documentId, customer);
	        }
	        catch (Exception e)
	        {
	            ErrorLogger.LogError(e, Context);
	            return new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } }.ToMessageReturn<WSShipmentDocument>();
            }
	    }




        [SoapHeader(AuthenticationMemberName)]
		[WebMethod(Description = "Returns customer purchase order.")]
		public WSCustomerPONumber RetrievePONumber(string poNumber)
		{
			try
			{
				var messages = new List<WSMessage>();

				var user = AuthenticationToken.IsValidUser(messages);
				if (messages.ContainsErrors()) return messages.ToMessageReturn<WSCustomerPONumber>();

                EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tMethod:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, "Retrieve PO Number", HttpContext.Current.Request.UserHostAddress);

			    return eShipPlusServiceBase.FetchPONumber(user, poNumber);
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				return new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } }.ToMessageReturn<WSCustomerPONumber>();
			}
		}

		[SoapHeader(AuthenticationMemberName)]
		[WebMethod(Description = "Deletes a PO Number entry.")]
		public WSReturn DeletePONumber(string poNumber)
		{
			try
			{
				var messages = new List<WSMessage>();

				var user = AuthenticationToken.IsValidUser(messages);
				if (messages.ContainsErrors()) return messages.ToMessageReturn<WSReturn>();

                EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tMethod:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, "Delete PO Number", HttpContext.Current.Request.UserHostAddress);

			    return eShipPlusServiceBase.DeletePONumber(user, poNumber);
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				return new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } }.ToMessageReturn<WSReturn>();
			}
		}

		[SoapHeader(AuthenticationMemberName)]
		[WebMethod(Description = "Updates a PO Number entry.")]
		public WSReturn UpdatePONumber(WSCustomerPONumber customerPONumber)
		{
			try
			{
				var messages = new List<WSMessage>();

				var user = AuthenticationToken.IsValidUser(messages);
				if (messages.ContainsErrors()) return messages.ToMessageReturn<WSReturn>();

                EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tMethod:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, "Update PO Number", HttpContext.Current.Request.UserHostAddress);

			    return eShipPlusServiceBase.UpdatePONumber(user, customerPONumber);
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				return new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } }.ToMessageReturn<WSReturn>();
			}
		}

		[SoapHeader(AuthenticationMemberName)]
		[WebMethod(Description = "Adds a PO Number entry.")]
		public WSReturn AddPONumber(WSCustomerPONumber customerPONumber)
		{
			try
			{
				var messages = new List<WSMessage>();

				var user = AuthenticationToken.IsValidUser(messages);
				if (messages.ContainsErrors()) return messages.ToMessageReturn<WSReturn>();

                EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tMethod:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, "Add PO Number", HttpContext.Current.Request.UserHostAddress);

			    return eShipPlusServiceBase.AddPONumber(user, customerPONumber);
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				return new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } }.ToMessageReturn<WSReturn>();
			}
		}

		[SoapHeader(AuthenticationMemberName)]
		[WebMethod(Description = "Adds a PO Number entry.")]
		public WSReturn AddPONumbers(WSCustomerPONumber[] customerPONumbers)
		{
			try
			{
				var messages = new List<WSMessage>();

				var user = AuthenticationToken.IsValidUser(messages);
				if (messages.ContainsErrors()) return messages.ToMessageReturn<WSReturn>();

                EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tMethod:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, "Add PO Numbers", HttpContext.Current.Request.UserHostAddress);

			    return eShipPlusServiceBase.AddPONumbers(user, customerPONumbers);
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				return new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } }.ToMessageReturn<WSReturn>();
			}
		}



		[SoapHeader(AuthenticationMemberName)]
		[WebMethod(Description = "Returns status of a booking request.")]
		public WSBookingRequestStatus GetBookingRequestStatus(string bookingNumber)
		{
			try
			{
				var messages = new List<WSMessage>();

				Customer customer;
				var user = AuthenticationToken.IsValidUser(messages, out customer);
				if (messages.ContainsErrors()) return messages.ToMessageReturn<WSBookingRequestStatus>();

                EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tMethod:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, "Get Booking Request Status", HttpContext.Current.Request.UserHostAddress);

			    return eShipPlusServiceBase.FetchBookingRequestStatus(user, bookingNumber, customer);
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				return new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } }.ToMessageReturn<WSBookingRequestStatus>();
			}
		}

		[SoapHeader(AuthenticationMemberName)]
		[WebMethod(Description = "Returns status of a shipment.")]
		public WSShipmentStatus GetShipmentStatus(string shipmentNumber)
		{
			try
			{
				var messages = new List<WSMessage>();

				Customer customer;
				var user = AuthenticationToken.IsValidUser(messages, out customer);
				if (messages.ContainsErrors()) return messages.ToMessageReturn<WSShipmentStatus>();

                EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tMethod:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, "Get Shipment Status", HttpContext.Current.Request.UserHostAddress);

			    return eShipPlusServiceBase.FetchShipmentStatus(user, shipmentNumber, customer);
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				return new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } }.ToMessageReturn<WSShipmentStatus>();
			}
		}




		[SoapHeader(AuthenticationMemberName)]
		[WebMethod(Description = "Processes a shipment cancellation.")]
		public WSReturn RequestShipmentCancellation(string shipmentNumber)
		{
			try
			{
				var messages = new List<WSMessage>();

				Customer customer;
				var user = AuthenticationToken.IsValidUser(messages, out customer);
				if (messages.ContainsErrors()) return messages.ToMessageReturn<WSReturn>();

                EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tMethod:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, "Request Shipment Cancellation", HttpContext.Current.Request.UserHostAddress);

			    return eShipPlusServiceBase.CancelShipment(user, shipmentNumber, customer);
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				return new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } }.ToMessageReturn<WSReturn>();
			}
		}


		[SoapHeader(AuthenticationMemberName)]
		[WebMethod(Description = "Processes a booking reqeust cancellation.")]
		public WSReturn RequestBookingRequestCancellation(string bookingNumber)
		{
			try
			{
				var messages = new List<WSMessage>();

				Customer customer;
				var user = AuthenticationToken.IsValidUser(messages, out customer);
				if (messages.ContainsErrors()) return messages.ToMessageReturn<WSBookingRequestStatus>();

                EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tMethod:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, "Request Booking Request Cancellation", HttpContext.Current.Request.UserHostAddress);

			    return eShipPlusServiceBase.CancelBookingRequest(user, bookingNumber, customer);
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				return new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } }.ToMessageReturn<WSReturn>();
			}
		}




		[SoapHeader(AuthenticationMemberName)]
		[WebMethod(Description = "Returns invoice record detail.")]
		public WSInvoice GetInvoice(string invoiceNumber)
		{
			try
			{
				var messages = new List<WSMessage>();

				Customer customer;
				var user = AuthenticationToken.IsValidUser(messages, out customer);
				if (messages.ContainsErrors()) return messages.ToMessageReturn<WSInvoice>();

                EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tMethod:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, "Get Invoice", HttpContext.Current.Request.UserHostAddress);

			    return eShipPlusServiceBase.FetchInvoice(user, invoiceNumber, customer);
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				return new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } }.ToMessageReturn<WSInvoice>();
			}
		}

		[SoapHeader(AuthenticationMemberName)]
		[WebMethod(Description = "Returns invoice record details.")]
		public WSInvoiceSummary[] GetInvoicesByDueDate(DateTime startdate, DateTime endDate)
		{
			try
			{
				var messages = new List<WSMessage>();

				Customer customer;
				var user = AuthenticationToken.IsValidUser(messages, out customer);
				if (messages.ContainsErrors()) return messages.ToMessageReturnCollection<WSInvoiceSummary>();

                EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tMethod:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, "Get Invoice By Due Date", HttpContext.Current.Request.UserHostAddress);

			    return eShipPlusServiceBase.FetchInvoicesByDueDate(user, startdate, endDate, customer);
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				return new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } }.ToMessageReturnCollection<WSInvoiceSummary>();
			}
		}

		[SoapHeader(AuthenticationMemberName)]
		[WebMethod(Description = "Returns invoice record details.")]
		public WSInvoiceSummary[] GetInvoicesByDate(DateTime startdate, DateTime endDate)
		{
			try
			{
				var messages = new List<WSMessage>();

				Customer customer;
				var user = AuthenticationToken.IsValidUser(messages, out customer);
				if (messages.ContainsErrors()) return messages.ToMessageReturnCollection<WSInvoiceSummary>();

                EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tMethod:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, "Get Invoice By Date", HttpContext.Current.Request.UserHostAddress);

			    return eShipPlusServiceBase.FetchInvoicesByDate(user, startdate, endDate, customer);
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				return
					new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } }.ToMessageReturnCollection
						<WSInvoiceSummary>();
			}
		}

		[SoapHeader(AuthenticationMemberName)]
		[WebMethod(Description = "Returns invoice record details for a particular shipment.")]
		public WSInvoiceSummary[] GetInvoicesForShipment(string shipmentNumber)
		{
			try
			{
				var messages = new List<WSMessage>();

				Customer customer;
				var user = AuthenticationToken.IsValidUser(messages, out customer);
				if (messages.ContainsErrors()) return messages.ToMessageReturnCollection<WSInvoiceSummary>();

                EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tMethod:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, "Get Invoices For Shipment", HttpContext.Current.Request.UserHostAddress);

			    return eShipPlusServiceBase.FetchInvoicesForShipment(user, shipmentNumber, customer);
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				return new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } }.ToMessageReturnCollection<WSInvoiceSummary>();
			}
		}




		[SoapHeader(AuthenticationMemberName)]
		[WebMethod(Description = "Submits a loading booking request to operations department be covered.")]
		public WSBookingRequest SubmitBookingRequest(WSBookingRequest request)
		{
			try
			{
				var messages = new List<WSMessage>();

				Customer customer;
				var user = AuthenticationToken.IsValidUser(messages, out customer);
				if (messages.ContainsErrors()) return messages.ToMessageReturn<WSBookingRequest>();

                EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tMethod:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, "Submit Booking Request", HttpContext.Current.Request.UserHostAddress);

			    return eShipPlusServiceBase.SaveBookingRequest(user, request, customer);
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				return new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } }.ToMessageReturn<WSBookingRequest>();
			}
		}

		[SoapHeader(AuthenticationMemberName)]
		[WebMethod(Description = "Get shipping rates for submitted shipment.")]
		public WSShipment2 Rate(WSShipment2 shipment)
		{
			try
			{
				var messages = new List<WSMessage>();

				Customer customer;
				var user = AuthenticationToken.IsValidUser(messages, out customer);
				if (messages.ContainsErrors()) return messages.ToMessageReturn<WSShipment2>();

                EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tMethod:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, "Rate", HttpContext.Current.Request.UserHostAddress);

			    return eShipPlusServiceBase.GetRates(user, shipment, customer);
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				return new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } }.ToMessageReturn<WSShipment2>();
			}
		}

		[SoapHeader(AuthenticationMemberName)]
		[WebMethod(Description = "Book submitted shipment.")]
		public WSShipment2 Book(WSShipment2 shipment)
		{
			try
			{
				var messages = new List<WSMessage>();

				Customer customer;
				var user = AuthenticationToken.IsValidUser(messages, out customer);
				if (messages.ContainsErrors()) return messages.ToMessageReturn<WSShipment2>();

                EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tMethod:{4}\t IP:{5}", user.Tenant.Name, user.FirstName, user.LastName, user.Username, "Book", HttpContext.Current.Request.UserHostAddress);

			    return eShipPlusServiceBase.BookShipment(user, shipment, customer);
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
				return new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } }.ToMessageReturn<WSShipment2>();
			}
		}

        [SoapHeader(AuthenticationMemberName)]
        [WebMethod(Description = "Saves a quote with a selected rate.")]
        public WSShipment2 SaveQuote(WSShipment2 quote)
        {
            try
            {
                var messages = new List<WSMessage>();

                Customer customer;
                var user = AuthenticationToken.IsValidUser(messages, out customer);
                if (messages.ContainsErrors()) return messages.ToMessageReturn<WSShipment2>();

                EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tMethod:{4}\t IP:{5}",
                                                           user.Tenant.Name, user.FirstName, user.LastName, user.Username,
                                                           "Save Quote", HttpContext.Current.Request.UserHostAddress);

                return eShipPlusServiceBase.SaveQuote(user, quote, customer);
            }
            catch (Exception e)
            {
                ErrorLogger.LogError(e, Context);
                return new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } }.ToMessageReturn<WSShipment2>();
            }
        }

        [SoapHeader(AuthenticationMemberName)]
        [WebMethod(Description = "Converts a quote to a shipment.")]
        public WSShipment2 ConvertQuoteToShipment(string quoteNumber)
        {
            try
            {
                var messages = new List<WSMessage>();

                Customer customer;
                var user = AuthenticationToken.IsValidUser(messages, out customer);
                if (messages.ContainsErrors()) return messages.ToMessageReturn<WSShipment2>();

                EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tMethod:{4}\t IP:{5}",
                                                           user.Tenant.Name, user.FirstName, user.LastName,
                                                           user.Username, "Convert Quote to Shipment",
                                                           HttpContext.Current.Request.UserHostAddress);

                return eShipPlusServiceBase.ConvertQuoteToShipment(user, quoteNumber);
            }
            catch (Exception e)
            {
                ErrorLogger.LogError(e, Context);
                return new[] { new WSMessage { Type = WSEnums.MessageType.Error, Value = e.Message } }.ToMessageReturn<WSShipment2>();
            }
        }
	}
}
