﻿using System;
using System.IO;
using System.Net;
using System.Web.UI;

namespace LogisticsPlus.Eship.WebApplication.Services
{
    public partial class MacroPointServiceTestNotificationSender : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void OnSendOrderStatusUpdateClicked(object sender, EventArgs e)
        {
            var request = WebRequest.Create(string.Format("http://localhost:53038/Services/MacroPointServiceProxy.aspx?MPOrderID={0}&ID={1}&Code={2}&Message={3}",
                txtOrderId.Text,    
                txtIdNumber.Text,
                    "1021",
                    "Tracking Now"));
            SendRequestAndDisplayResult(request);
        }

        protected void OnSendEventUpdateClicked(object sender, EventArgs e)
        {
            var request = WebRequest.Create(string.Format("http://localhost:53038/Services/MacroPointServiceProxy.aspx?MPOrderID={0}&ID={1}&Stop={2}&Event={3}&EventDateTime={4}",
                txtOrderId.Text,        
                txtIdNumber.Text,
                        "Name of a Stop",
                        "X3",
                        "2014-9-17 19:35Z"));
            SendRequestAndDisplayResult(request);
        }

        protected void OnSendLocationNotificationClicked(object sender, EventArgs e)
        {
            var request = WebRequest.Create(string.Format("http://localhost:53038/Services/MacroPointServiceProxy.aspx?MPOrderID={0}&ID={1}&Latitude={2}&Longitude={3}&Uncertainty={4}&Street1={5}&Street2={6}&Neighborhood={7}&City={8}&State={9}&Postal={10}&Country={11}&LocationDateTimeUTC={12}",
                txtOrderId.Text,        
                txtIdNumber.Text,
                        txtLatitude.Text,
                        txtLongitude.Text,
                        "10",
                        "Test St.",
                        "Apt 3",
                        "West Lake",
                        "CityVille",
                        "CA",
                        "91351",
                        "USA",
                        "2014-9-20 09:32Z"));
            SendRequestAndDisplayResult(request);
        }

        protected void OnSendFormClicked(object sender, EventArgs e)
        {
            var request = WebRequest.Create(string.Format("{0}/Services/MacroPointServiceProxy.aspx", WebApplicationSettings.SiteRoot));
            request.Method = "POST";
            request.ContentType = "text/xml";

            // using an actual form created and sent from Macropoint to eShipPlus
            const string formText = @"<FormItems xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" loadId=""56756 [Reno NV 89511 to Minneapolis MN 55419]"" mpOrderId=""1C4C5DE5F9F44E0EA2F64EF089B73224"" mpTrackingRequestId="""" stopName=""LAKESHORE PADDLEBOARD COMPANY"" eventName=""Departed"" formName=""Departed Pickup"" dateInUtc=""2015-04-23T13:23:17.9491036Z"" stopId=""0"">
                      <FormItem>
                        <Type>Text</Type>
                        <Name>Pieces</Name>
                        <ItemId>pieces</ItemId>
                        <Value>1</Value>
                      </FormItem>
                      <FormItem>
                        <Type>Text</Type>
                        <Name>Weight</Name>
                        <ItemId>weight</ItemId>
                        <Value>200</Value>
                      </FormItem>
                      <FormItem>
                        <Type>Text</Type>
                        <Name>BOL #</Name>
                        <ItemId>bol</ItemId>
                        <Value>1234567890</Value>
                      </FormItem>
                      <FormItem>
                        <Type>Text</Type>
                        <Name>Notes</Name>
                        <ItemId>notes</ItemId>
                        <Value>Test</Value>
                      </FormItem>
                    </FormItems>";
            
            using (var writer = new StreamWriter(request.GetRequestStream()))
                writer.Write(formText);

            SendRequestAndDisplayResult(request);
        }


        private void SendRequestAndDisplayResult(WebRequest request)
        {
            try
            {
                var response = request.GetResponse();
                litResult.Text = ((HttpWebResponse) response).StatusCode == HttpStatusCode.OK
                                     ? "Request suceeded"
                                     : "Request Failed";
            }
            catch (WebException ex)
            {
                var errorResponse = (HttpWebResponse)ex.Response;
                var responseStream = errorResponse.GetResponseStream();
                if (responseStream != null)
                {
                    string data;
                    using (var reader = new StreamReader(responseStream))
                        data = reader.ReadToEnd();
                    litResult.Text = data;
                }
            }
            catch (Exception ex)
            {
                litResult.Text = ex.Message;
            }
        }
    }
}