Vendor Number	Document Number	Invoice Type	Original Invoice Number	Document Date	Reference Number	Reference Type	Quantity	Unit Buy	Account Bucket	Charge Code
See Notes #7	document number	See Notes #8	See Notes #9 	See Notes #6	See Notes #10	See Notes #11	Integer Value	Decimal Value	See Notes #12	See Notes #13

Notes:
1. Input file must have a header row.
2. Input file can be Excel (extension xlsx) or delimited file (extension txt)
3. Acceptable file delimiters are tab, comma, semicolon and colon
4. If using excel file, input data must be on a worksheet called "data"
5. File upload is limited to 1,000 records										
6. Dates must be in YYYY-MM-DD format ex. 2012-05-23										
7. Values for the Vendor Number field must exist and be valid Vendor Numbers
8. Values for the Invoice Type are as follows:
	0 for Invoice
	2 for Credit
9. Original Invoice Number is left blank if the Invoice Type field has '0' as its value. Otherwise, if Invoice Type field is '2' then this field must be the original Invoice Number
10. Values for the Reference Number field are either the Shipment Number or Service Ticket Number based on reference type and must exist	
11. Values for the Reference Type are as follows:
	0 for Shipment
	1 for Service Ticket

12. Values for the Account Bucket Code field must exist and be valid Account Bucket Codes
13. Values for the Charge Code field must exist and be valid Charge Codes	

Tip: Excel formatting can be sometimes problematic. If using excel and having trouble upload files, consider formatting all your cells as text