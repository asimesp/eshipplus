Code	Description	Category	Exclude On Score Card	Active
code	description	See Notes #5	'True' or 'False'	'True' or 'False'

Notes:
1. Input file must have a header row.
2. Input file can be Excel (extension xlsx) or delimited file (extension txt)
3. Acceptable file delimiters are tab, comma, semicolon and colon
4. If using excel file, input data must be on a worksheet called "data"	
5. Values for the Category field are as follows:
	0 for Freight
	1 for Service		
	2 for Sales Representative		
	3 for User

Tip: Excel formatting can be sometimes problematic. If using excel and having trouble upload files, consider formatting all your cells as text