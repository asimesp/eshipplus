Customer Number	Effective Date	Origin City	Origin State	Origin Country	Origin Postal Code	Destination City	Destination State	Destination Country	Destination Postal Code
See Notes #5	See Notes #6	origin city	origin state	origin country	origin postal code	destination city	destination state	destination country	destination postal code

Notes:
1. Input file must have a header row.
2. Input file can be Excel (extension xlsx) or delimited file (extension txt)
3. Acceptable file delimiters are tab, comma, semicolon and colon
4. If using excel file, input data must be on a worksheet called "data"	
5. Values for the Customer Number field must exist and be valid Customer Numbers
6. Values for the Effective Date field must in the the following Date format: YYYY-MM-DD															

Tip: Excel formatting can be sometimes problematic. If using excel and having trouble upload files, consider formatting all your cells as text