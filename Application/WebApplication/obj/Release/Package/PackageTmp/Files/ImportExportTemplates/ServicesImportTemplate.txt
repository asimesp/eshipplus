Code	Category	Description	Dispatch Flag	Applicable at Pickup	Applicable at Delivery	Charge Code
Service Code	See Notes #5	Description	See Notes #6	'True' or 'False'	'True' or 'False'	See Notes #7 
			
Notes:
1. Input file must have a header row.
2. Input file can be Excel (extension xlsx) or delimited file (extension txt)
3. Acceptable file delimiters are tab, comma, semicolon and colon
4. If using excel file, input data must be on a worksheet called "data"	
5. Valid values for the Category field are as follows:
	0 for Freight
	1 for Service		
	2 for Accessorial

6. Valid values for the Dispatch Flag field are as follows:
	0 for None
    1 for LiftGate
    2 for BlindShipment
    3 for BrokerSelectOption
    4 for CashOnDelivery
    5 for DangerousGoods
    6 for Detention
    7 for DryIce
    8 for Exhibition
    9 for ExtraLabor
    10 for ExtremeLength
    11 for HoldAtLocation
    12 for Holiday
    13 for Inside
    14 for LimitedAccess
    15 for MarkingOrTagging
    16 for Port
    17 for PreDeliveryNotification
    18 for ProtectionFromFreezing
    19 for Saturday
    20 for SortAndSegregate
    21 for Storage
    22 for Sunday

7. Charge Code field has to have an existing Charge Code filled in
	Examples of this are '20DRAY' for 20ft DRAYAGE or 'ADDI' for Additional Stop
	These codes can be found on the Charge Codes page in the Registry section
	
Tip: Excel formatting can be sometimes problematic. If using excel and having trouble upload files, consider formatting all your cells as text