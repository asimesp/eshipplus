Customer Number	Effective Date	Small Package Engine	Small Package Type
See Notes #5	See Notes #6	See Notes #7	See Notes #8

Notes:
1. Input file must have a header row.
2. Input file can be Excel (extension xlsx) or delimited file (extension txt)
3. Acceptable file delimiters are tab, comma, semicolon and colon
4. If using excel file, input data must be on a worksheet called "data"	
5. Values for the Customer Number field must exist and be valid Customer Numbers
6. Values for the Effective Date field must in the the following Date format: YYYY-MM-DD															
7. Values for the Small Package Engine field are as follows:
	1 for FedEx					
	2 for Ups
8. Values for the Small Package Type must exist and be valid Small Package Types of the Small Package Engine being used (Ex: FEDEX_GROUND, INTERNATIONAL_ECONOMY for FedEx...)

Tip: Excel formatting can be sometimes problematic. If using excel and having trouble upload files, consider formatting all your cells as text