Vendor Number	Origin City	Origin State	Origin Postal Code	Origin Country	Destination City	Destination State	Destination Postal Code	Destination Country
See Notes #5	Origin city	Origin state	See Notes #6	See Notes #7	Destination city	Destination state	See Notes #6	See Notes #7

Notes:												
1. Input file must have a header row.												
2. Input file can be Excel (extension xlsx) or delimited file (extension txt)												
3. Acceptable file delimiters are tab, comma, semicolon and colon												
4. If using excel file, input data must be on a worksheet called "data"	
5. Values for the Vendor Number must be existing Vendor Numbers from Vendors											
6. Values for the Origin Postal Code and Destination Postal Code must exist and be valid Postal Codes												
7. Values for the Origin Country Code and Destination Country Code must exist and be valid Country Codes				

Tip: Excel formatting can be sometimes problematic. If using excel and having trouble upload files, consider formatting all your cells as text