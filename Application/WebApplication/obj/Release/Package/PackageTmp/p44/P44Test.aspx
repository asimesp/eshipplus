﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="P44Test.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.p44.P44Test" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            BOL:
            <asp:TextBox runat="server" ID="txtBol"></asp:TextBox><br />
            Shipment Prefix:
            <asp:TextBox runat="server" ID="txtPrefix"></asp:TextBox><br />
            SCAC:
            <asp:TextBox runat="server" ID="txtScac"></asp:TextBox><br /><br />
            Shipment Id:
            <asp:TextBox runat="server" ID="txtShipmentId"></asp:TextBox><br /><br /><br />
            
            Origin Company Name:
            <asp:TextBox runat="server" ID="txtOriginName"></asp:TextBox><br />
            Origin Street 1:
            <asp:TextBox runat="server" ID="txtOriginStreet1"></asp:TextBox><br />
            Origin Street 2:
            <asp:TextBox runat="server" ID="txtOriginStreet2"></asp:TextBox><br />
            Origin City:
            <asp:TextBox runat="server" ID="txtOriginCity"></asp:TextBox><br />
            Origin State:
            <asp:TextBox runat="server" ID="txtOriginState"></asp:TextBox><br />
            Origin Postal Code
            <asp:TextBox runat="server" ID="txtOriginPostalCode"></asp:TextBox><br />
            Origin Contact Name:
            <asp:TextBox runat="server" ID="txtOriginContactName"></asp:TextBox><br />
            Origin Contact Phone:
            <asp:TextBox runat="server" ID="txtOriginContactPhone"></asp:TextBox><br />
            Origin Contact Email:
            <asp:TextBox runat="server" ID="txtOriginContactEmail"></asp:TextBox><br />
            Origin Contact Fax:
            <asp:TextBox runat="server" ID="txtOriginContactFax"></asp:TextBox><br /><br />
            Desired Pickup Date:
            <asp:TextBox runat="server" ID="txtDesiredPickupDate"></asp:TextBox><br /><br />
            
            Destination Company Name:
            <asp:TextBox runat="server" ID="txtDestinationName"></asp:TextBox><br />
            Destination Street 1:
            <asp:TextBox runat="server" ID="txtDestinationStreet1"></asp:TextBox><br />
            Destination Street 2:
            <asp:TextBox runat="server" ID="txtDestinationStreet2"></asp:TextBox><br />
            Destination City:
            <asp:TextBox runat="server" ID="txtDestinationCity"></asp:TextBox><br />
            Destination State:
            <asp:TextBox runat="server" ID="txtDestinationState"></asp:TextBox><br />
            Destination Postal Code
            <asp:TextBox runat="server" ID="txtDestinationPostalCode"></asp:TextBox><br />
            Destination Contact Name:
            <asp:TextBox runat="server" ID="txtDestinationContactName"></asp:TextBox><br />
            Destination Contact Phone:
            <asp:TextBox runat="server" ID="txtDestinationContactPhone"></asp:TextBox><br />
            Destination Contact Email:
            <asp:TextBox runat="server" ID="txtDestinationContactEmail"></asp:TextBox><br />
            Destination Contact Fax:
            <asp:TextBox runat="server" ID="txtDestinationContactFax"></asp:TextBox><br /><br /><br />
            Desired Delivery Date:
            <asp:TextBox runat="server" ID="txtDesiredDeliveryDate"></asp:TextBox><br /><br />
            

            <asp:Button runat="server" ID="btnSubmit" OnClick="OnSubmitTrackingClicked" Text="Track"/>
            <br/><br/>
            <asp:Literal runat="server" ID="litMsg"></asp:Literal>
        </div>
    </form>
</body>
</html>
