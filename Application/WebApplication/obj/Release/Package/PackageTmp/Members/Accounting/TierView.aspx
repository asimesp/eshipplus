﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="TierView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.TierView" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        ShowEdit="true" ShowDelete="true" ShowSave="true" ShowFind="true" OnFind="OnFindClicked" OnUnlock="OnUnlockClicked"
        OnSave="OnSaveClicked" OnDelete="OnDeleteClicked" OnNew="OnNewClicked" OnEdit="OnEditClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Tiers
               <eShip:RecordIdentityDisplayControl runat="server" ID="ridcRecordIdentity" TargetControlId="txtName" />
            </h3>

            <div class="lockMessage">
                <asp:Literal ID="litMessage" runat="server" />
            </div>
        </div>

        <hr class="dark mb5" />

        <div class="errorMsgLit">
            <asp:Literal runat="server" ID="litErrorMessages" />
        </div>

        <eShip:TierFinderControl ID="tierFinder" runat="server" Visible="false" OpenForEditEnabled="true"
            OnItemSelected="OnTierFinderItemSelected" OnSelectionCancel="OnTierFinderSelectionCancelled" />
        <eShip:CustomHiddenField runat="server" ID="hidTierId" />
        <ajax:TabContainer ID="tabTiers" runat="server" CssClass="ajaxCustom">
            <ajax:TabPanel runat="server" ID="tabDetails" HeaderText="Details">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlDetails">
                        <div class="rowgroup mb20">
                            <div class="fieldgroup mr10">
                                <label class="wlabel blue">Tier Number</label>
                                <eShip:CustomTextBox ID="txtTierNumber" runat="server" CssClass="w100 disabled" MaxLength="50" ReadOnly="True" />
                            </div>
                            <div class="fieldgroup mr10">
                                <label class="wlabel blue">Date Created</label>
                                <eShip:CustomTextBox ID="txtDateCreated" runat="server" CssClass="w200 disabled" ReadOnly="True" />
                            </div>
                            <div class="fieldgroup_s mr10 vlinedarkright">
                                <asp:CheckBox ID="chkActive" runat="server" CssClass="jQueryUniform" />
                                <label class="blue">Active</label>
                            </div>
                            <div class="fieldgroup mr10">
                                <label class="wlabel blue">Logo Image <span class="fs85em">(Max: 75px by 400px)</span></label>
                                <asp:FileUpload ID="fupLogoUrl" runat="server" CssClass="jQueryUniform" />
                                <asp:Button ID="btnClearLogoUrl" runat="server" Text="CLEAR LOGO" OnClick="OnClearLogoUrlClicked" CausesValidation="False" />

                            </div>
                            <div class="fieldgroup">
                                <asp:Image runat="server" ID="imgLogo" CssClass="Img60MaxHeight" Visible="False" />
                                <eShip:CustomHiddenField runat="server" ID="hidLogoUrl" />
                            </div>
                        </div>
                        <hr class="dark" />
                        <div class="rowgroup">
                            <div class="col_1_2 bbox vlinedarkright">
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Name</label>
                                        <eShip:CustomTextBox ID="txtName" runat="server" CssClass="w200" MaxLength="50" />
                                    </div>
                                    <div class="fieldgroup_s">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Additional BOL Text
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleAdditionalBolText" TargetControlId="txtAdditionalBolText" MaxLength="500" />
                                        </label>
                                        <eShip:CustomTextBox ID="txtAdditionalBolText" runat="server" CssClass="w420 h150" TextMode="MultiLine" />
                                    </div>
                                </div>
                            </div>
                            <div class="col_1_2 pl46 bbox">

                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Toll-Free Contact Number</label>
                                        <eShip:CustomTextBox ID="txtTollFreeContactNumber" runat="server" CssClass="w200" MaxLength="25" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Toll-Free Support Number</label>
                                        <eShip:CustomTextBox ID="txtSupportTollFree" runat="server" CssClass="w200" MaxLength="25" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Support Emails
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleSupportEmails" TargetControlId="txtSupportEmails" MaxLength="500" />
                                        </label>
                                        <eShip:CustomTextBox ID="txtSupportEmails" runat="server" CssClass="w420 h150" TextMode="MultiLine" />
                                    </div>
                                </div>
                            </div>
                        </div>

                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabNotificationEmails" HeaderText="Notification Emails">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlNotificationEmails">
                        <div class="rowgroup">
                            <div class="row bottom_shadow">
                                <ul class="display-inline note mt5 pt5">
                                    <li>Emails are delimited by a new line, comma, semi-colon, colon, tab, or vertical bar</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col_1_2 bbox vlinedarkright">
                            <div class="rowgroup">
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            General Notification Emails
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleGeneralNotificationEmails" TargetControlId="txtGeneralNotificationEmails" MaxLength="500" />
                                        </label>
                                        <eShip:CustomTextBox ID="txtGeneralNotificationEmails" runat="server" CssClass="w420 h150" TextMode="MultiLine" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            LTL Notification Emails
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleLTLNotificationEmails" TargetControlId="txtLTLNotificationEmails" MaxLength="500" />
                                        </label>
                                        <eShip:CustomTextBox ID="txtLTLNotificationEmails" runat="server" CssClass="w420 h150" TextMode="MultiLine" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            SP Notification Emails
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleSPNotificationEmails" TargetControlId="txtSPNotificationEmails" MaxLength="500" />
                                        </label>
                                        <eShip:CustomTextBox ID="txtSPNotificationEmails" runat="server" CssClass="w420 h150" TextMode="MultiLine" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Air Notification Emails
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleAirNotificationEmails" TargetControlId="txtAirNotificationEmails" MaxLength="500" />
                                        </label>
                                        <eShip:CustomTextBox ID="txtAirNotificationEmails" runat="server" CssClass="w420 h150" TextMode="MultiLine" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col_1_2 bbox pl46">
                            <div class="rowgroup">
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Pending Vendor Notification Emails
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamlePendingVendorNotificationEmails" TargetControlId="txtPendingVendorNotificationEmails" MaxLength="500" />
                                        </label>
                                        <eShip:CustomTextBox ID="txtPendingVendorNotificationEmails" runat="server" CssClass="w420 h150" TextMode="MultiLine" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            FTL Notification Emails
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleFTLNotificationEmails" TargetControlId="txtFTLNotificationEmails" MaxLength="500" />
                                        </label>
                                        <eShip:CustomTextBox ID="txtFTLNotificationEmails" runat="server" CssClass="w420 h150" TextMode="MultiLine" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Rail Notification Emails
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleRailNotificationEmails" TargetControlId="txtRailNotificationEmails" MaxLength="500" />
                                        </label>
                                        <eShip:CustomTextBox ID="txtRailNotificationEmails" runat="server" CssClass="w420 h150" TextMode="MultiLine" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Account Payable Dispute Notification Emails
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleAccountPayableDisputeNotificationEmails" TargetControlId="txtAccountPayableDisputeNotificationEmails" MaxLength="500" />
                                        </label>
                                        <eShip:CustomTextBox runat="server" ID="txtAccountPayableDisputeNotificationEmails" TextMode="MultiLine" CssClass="w420 h150" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabCustomers" HeaderText="Customers">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlCustomers">
                        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheTierCustomersTable" TableId="tierCustomersTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                        <table class="stripe" id="tierCustomersTable">
                            <tr>
                                <th style="width: 60%;">Name
                                </th>
                                <th style="width: 40%;">Customer Number
                                </th>
                            </tr>
                            <asp:Literal runat="server" ID="litCustomers" />
                        </table>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabAuditLog" HeaderText="Audit Log">
                <ContentTemplate>
                    <eShip:AuditLogControl ID="auditLogs" runat="server" />
                </ContentTemplate>
            </ajax:TabPanel>
        </ajax:TabContainer>
    </div>

    <script type="text/javascript">
        function ClearHidFlag() {
            jsHelper.SetEmpty('<%= hidFlag.ClientID %>');
        }
    </script>

    <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
        Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" />
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
    <eShip:CustomHiddenField runat="server" ID="hidFilesToDelete" />
</asp:Content>
