﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShipmentDocumentGeneratorControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls.ShipmentDocumentGeneratorControl" %>

<asp:Panel runat="server" ID="pnlShipmentDocumentGenerator" CssClass="popupControl popupControlOverW500">
    <div class="popheader">
        <h4>
            <asp:Label runat="server" ID="lblInvoiceNumber" />
            Shipment Documents<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" Text="" /></small>
            <asp:ImageButton ImageUrl="~/images/icons2/close.png" CssClass="close" CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
        </h4>
    </div>
    <div class="rowgroup pl20">
        <asp:Repeater runat="server" ID="rptDocuments">
            <ItemTemplate>
                <div class="row">
                    <div class="fieldgroup">
                        <label>
                        <asp:HyperLink NavigateUrl='<%# Eval("Url") %>' class="link_nounderline blue" Target="_blank"  runat="server">
                             <%# Eval("Text") %> 
                        </asp:HyperLink></label>    
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:Panel>
<asp:Panel ID="pnlShipmentDocumentGeneratorDimScreen" CssClass="dimBackgroundControl" runat="server"
    Visible="false" />
<eShip:CustomHiddenField ID="hidShipmentId" runat="server" />
