﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VendorBillFinderControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls.VendorBillFinderControl" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<asp:Panel runat="server" ID="pnlVendorBillFinderContent" CssClass="popupControl" DefaultButton="btnSearch">
    <div class="popheader">
        <h4>Vendor Bill Finder<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" Text="" /></small>
            <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/icons2/close.png" CssClass="close"
                CausesValidation="false" OnClick="OnCancelClicked" runat="server" />
        </h4>
    </div>
    <script type="text/javascript">
        $(function () {
            if (jsHelper.IsTrue($('#<%= hidAreFiltersShowing.ClientID%>').val())) {
                $('#<%= pnlSearch.ClientID%>').show();
                $('#<%= pnlSearchShowFilters.ClientID%>').hide();
            } else {
                $('#<%= pnlSearch.ClientID%>').hide();
                $('#<%= pnlSearchShowFilters.ClientID%>').show();
            }
        });

        function ToggleFilters() {
            $('#<%= hidAreFiltersShowing.ClientID%>').val(jsHelper.IsTrue($('#<%= hidAreFiltersShowing.ClientID%>').val()) ? false : true);
            $('#<%= pnlSearch.ClientID%>').toggle();
            $('#<%= pnlSearchShowFilters.ClientID%>').toggle();
            var top = $('#<%= pnlVendorBillFinderContent.ClientID%> div[class="finderScroll"]').offset().top - $('#<%= pnlVendorBillFinderContent.ClientID%>').offset().top - 2;
            $('#<%= pnlVendorBillFinderContent.ClientID%> div[class="finderScroll"] > div[id^="hd"]').css('top', top + 'px');
        }
    </script>
    <table id="finderTable" class="poptable">
        <tr>
            <td>
                <asp:Panel runat="server" ID="pnlSearchShowFilters" class="rowgroup mb0" style="display:none;">
                    <div class="row">
                        <div class="fieldgroup">
                            <asp:Button ID="btnProcessSelected2" runat="server" Text="ADD SELECTED" OnClick="OnProcessSelectedClicked" CausesValidation="false" />
                        </div>
                        <div class="fieldgroup right">
                            <button onclick="ToggleFilters(); return false;">Show Filters</button>
                        </div>
                        <div class="fieldgroup right mb5 mt5 mr10">
                            <span class="fs75em  ">*** If you would like to show parameters when searching change the 'Always Show Finder Parameters On Search' option on your <asp:HyperLink ID="hypGoToUserProfile" Target="_blank" CssClass="blue" runat="server" Text="User Profile"/></span>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
                    <div class="mt5">
                        <div class="rowgroup mb0">
                            <div class="row">
                                <div class="fieldgroup">
                                    <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                                        CausesValidation="False" CssClass="add" />
                                </div>
                                <div class="fieldgroup right">
                                    <button onclick="ToggleFilters(); return false;">Hide Filters</button>
                                </div>
                                <div class="right">
                                    <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="VendorBills" ShowAutoRefresh="False"
                                        OnProcessingError="OnSearchProfilesProcessingError" OnSearchProfileSelected="OnSearchProfilesProfileSelected" />
                                </div>
                            </div>
                        </div>
                        <hr class="mb5" />
                        <script type="text/javascript">$(window).load(function () {SetControlFocus();});</script><table class="mb5" id="parametersTable">
                            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                                OnItemDataBound="OnParameterItemDataBound">
                                <LayoutTemplate>
                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                                        OnRemoveParameter="OnParameterRemove" />
                                </ItemTemplate>
                            </asp:ListView>
                        </table>
                        <div class="pl10 row mb5">
                            <asp:Button ID="btnSearch" runat="server" Text="SEARCH" OnClick="OnSearchClicked" CausesValidation="False" CssClass="mr10" />
                            <asp:Button ID="btnCancel" OnClick="OnCancelClicked" runat="server" Text="CANCEL" CssClass="mr10" CausesValidation="false" />
                            <asp:Button ID="btnProcessSelected" runat="server" Text="ADD SELECTED" OnClick="OnProcessSelectedClicked" CausesValidation="False" />
                            <label class="right pr10">Sort: <asp:Literal runat="server" ID="litSortOrder" Text='<%# WebApplicationConstants.Default %>' /></label>
                        </div>
                    </div>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel runat="server" ID="upDataUpdate">
        <ContentTemplate>
            <eShip:ContinuousScrollExtender ID="upcseDataUpdate" runat="server" BindableControlId="lstSearchResults" UpdatePanelToExtendId="upDataUpdate" ScrollableDivId="vendorBillFinderScrollSection"
                SelectionCheckBoxControlId="chkSelected" SelectionUniqueRefHiddenFieldId="hidVendorBillId" />
            <eShip:TableFreezeHeaderExtender runat="server" ID="tfheUsersFinderTable" TableId="vendorBillFinderTable" ScrollerContainerId="vendorBillFinderScrollSection" ScrollOffsetControlId="pnlVendorBillFinderContent"
                IgnoreHeaderBackgroundFill="True" HeaderZIndex="2050" />
            <div class="finderScroll" id="vendorBillFinderScrollSection">
                <table id="vendorBillFinderTable" class="stripe">
                    <tr>
                        <th style="width: 15%;">
                            <eShip:AltUniformCheckBox ID="chkSelectAllRecords" runat="server" OnClientSideClicked="javascript:SetSelectedCheckBoxesStatus(this, 'vendorBillFinderTable');" />
                        </th>
                        <th style="width: 5%;">
                            <asp:LinkButton runat="server" ID="lbtnSortDocumentNumber" CssClass="link_nounderline white" CausesValidation="False" OnCommand="OnSortData">
                                Document
                            </asp:LinkButton>
                        </th>
                        <th style="width: 5%;">
                            <asp:LinkButton runat="server" ID="lbtnSortDateCreated" CssClass="link_nounderline white" CausesValidation="False" OnCommand="OnSortData">
                                Date Created
                            </asp:LinkButton>
                        </th>
                        <th style="width: 5%;">
                            <asp:LinkButton runat="server" ID="lbtnSortDocumentDate" CssClass="link_nounderline white" CausesValidation="False" OnCommand="OnSortData">
                                Document Date
                            </asp:LinkButton>
                        </th>
                        <th style="width: 5%;">
                            <asp:LinkButton runat="server" ID="lbtnSortPostDate" CssClass="link_nounderline white" CausesValidation="False" OnCommand="OnSortData">
                                Posted Date
                            </asp:LinkButton>
                        </th>
                        <th style="width: 10%;">
                            <asp:LinkButton runat="server" ID="lbtnSortTypeText" CssClass="link_nounderline white" CausesValidation="False" OnCommand="OnSortData">
                                Type
                            </asp:LinkButton>
                        </th>
                        <th style="width: 35%;">Vendor&nbsp;
                                <asp:LinkButton runat="server" ID="lbtnSortVendorNumber" CssClass="link_nounderline white" CausesValidation="False" OnCommand="OnSortData">
                                Number
                                </asp:LinkButton>/
                                <asp:LinkButton runat="server" ID="lbtnSortVendorName" CssClass="link_nounderline white" CausesValidation="False" OnCommand="OnSortData">
                                Name
                                </asp:LinkButton>
                        </th>
                        <th style="width: 15%;">
                            <asp:LinkButton runat="server" ID="lbtnSortOriginalInvoice" CssClass="link_nounderline white" CausesValidation="False" OnCommand="OnSortData">
                                   Original <abbr title="Invoice">Inv.</abbr> #
                            </asp:LinkButton>
                        </th>
                    </tr>
                    <asp:ListView ID="lstSearchResults" runat="server" ItemPlaceholderID="itemPlaceHolder">
                        <LayoutTemplate>
                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Button ID="btnSelect" runat="server" Text="Select" OnClick="OnSelectClicked" Visible='<%# !EnableMultiSelection %>' CausesValidation="false" />
                                    <asp:Button ID="btnEditSelect" runat="server" Text="Edit" OnClick="OnEditSelectClicked" Visible='<%# !EnableMultiSelection && OpenForEditEnabled %>' CausesValidation="false" />
                                    <eShip:AltUniformCheckBox runat="server" ID="chkSelected" Visible='<%# EnableMultiSelection %>' OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'vendorBillFinderTable', 'chkSelectAllRecords');" />
                                    <eShip:CustomHiddenField ID="hidVendorBillId" Value='<%# Eval("Id") %>' runat="server" />
                                </td>
                                <td>
                                    <%# Eval("DocumentNumber")%>
                                </td>
                                <td>
                                    <%# Eval("DateCreated").FormattedShortDate()%>
                                </td>
                                <td>
                                    <%# !Eval("DocumentDate").ToDateTime().Equals(DateUtility.SystemEarliestDateTime) ? Eval("DocumentDate").FormattedShortDate() : string.Empty %>
                                </td>
                                <td>
                                    <%# !Eval("PostDate").ToDateTime().Equals(DateUtility.SystemEarliestDateTime) ? Eval("PostDate").FormattedShortDate() : string.Empty %>
                                </td>
                                <td>
                                    <%# Eval("BillType").FormattedString() %>
                                </td>
                                <td>
                                    <%# Eval("VendorNumber") %> - <%# Eval("VendorName") %>
                                </td>
                                <td>
                                    <%# Eval("OriginalInvoiceNumber")%>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnSortDocumentNumber" />
            <asp:PostBackTrigger ControlID="lbtnSortDateCreated" />
            <asp:PostBackTrigger ControlID="lbtnSortDocumentDate" />
            <asp:PostBackTrigger ControlID="lbtnSortPostDate" />
            <asp:PostBackTrigger ControlID="lbtnSortTypeText" />
            <asp:PostBackTrigger ControlID="lbtnSortVendorNumber" />
            <asp:PostBackTrigger ControlID="lbtnSortVendorName" />
            <asp:PostBackTrigger ControlID="lbtnSortOriginalInvoice" />
            <asp:PostBackTrigger ControlID="lstSearchResults" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Panel>
<eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
<asp:Panel ID="pnlInvoiceFinderDimScreen" CssClass="dimBackground" runat="server"
    Visible="false" />
<eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information"/>
<eShip:CustomHiddenField runat="server" ID="hidEditSelected" />
<eShip:CustomHiddenField runat="server" ID="hidOpenForEditEnabled" />
<eShip:CustomHiddenField runat="server" ID="hidVendorBillFinderEnableMultiSelection" />
<eShip:CustomHiddenField runat="server" ID="hidFilterForInvoiceType" />
<eShip:CustomHiddenField runat="server" ID="hidInvoiceToExclude" />
<eShip:CustomHiddenField runat="server" ID="hidSortAscending" />
<eShip:CustomHiddenField runat="server" ID="hidSortField" />
<eShip:CustomHiddenField runat="server" ID="hidAreFiltersShowing" Value="true" />