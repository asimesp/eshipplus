﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentEntryControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls.PaymentEntryControl" %>
<%@ Register TagPrefix="eShip" TagName="PaymentProfileManagerControl" Src="~/Members/Accounting/Controls/PaymentProfileManagerControl.ascx" %>
<asp:Panel runat="server" ID="pnlPaymentEntry" Visible="false" CssClass="popup">
    <div class="popheader">
        <h4>Payment Entry <asp:Literal runat="server" ID="litTitle" /></h4>
        <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close" CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
    </div>
    <div class="row">
        <asp:Panel runat="server" ID="pnlDemoAccount" CssClass="fieldgroup pl10 pt10" Visible="False">
            <span class="red" >
                This customer account is in demo mode and cannot process payments
            </span>
        </asp:Panel>
    </div>
    <asp:Panel runat="server" ID="pnlNonCreditCardPayment">
        <div class="row pt10 pl10">
            <asp:Panel runat="server" ID="pnlCheckInformation">
                <table class="poptable">
                    <tr>
                        <td class="text-right pt10" style="width: 40%">
                            <label class="upper">Check Number:</label>
                        </td>
                        <td class="pt10">
                            <eShip:CustomTextBox runat="server" ID="txtCheckNumber" CssClass="w200" MaxLength="100" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <table class="poptable">
                <tr>
                    <td class="text-right pt10" style="width: 40%">
                        <label class="upper">Amount:</label>
                    </td>
                    <td class="pt10">
                        <eShip:CustomTextBox runat="server" ID="txtNonCreditCardAmountPaid" CssClass="w100" Type="FloatingPointNumbers" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button runat="server" ID="btnNonCreditCardPay" OnClick="OnNonCreditCardPayClicked" Text="Pay" CausesValidation="False" />
                        <asp:Button runat="server" ID="btnCloseNonCreditCardMiscReceiptPaymentEntry" OnClick="OnCloseClicked" Text="Cancel" CausesValidation="False" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlCreditCardPayment" CssClass="row pt10 pl10">
        <div class="col_1_2 bbox vlinedarkright pl10">
            <div class="row">
                <asp:Panel runat="server" ID="pnlPaymentProfiles">
                    <h5 class="pl20 mb0 pb10">Saved Credit Cards</h5>
                    <table class="poptable">
                        <tr>
                            <td class="pl20" style="width:65%">
                                <asp:DropDownList runat="server" ID="ddlSavedCreditCards" DataTextField="Text" DataValueField="Value" CssClass="w420" AutoPostBack="True" OnSelectedIndexChanged="OnSavedCreditCardsSelectedIndexChanged" />
                            </td>
                            <td>
                                <asp:Button runat="server" ID="btnManagePaymentProfiles" OnClick="OnManagePaymentProfilesClicked" Text="Manage" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <h5 class="pl20">Card Information</h5>
                <table class="poptable">
                    <tr>
                        <td class="text-right" style="width: 40%">
                            <label class="upper">Name As It Appears On Card:</label>
                        </td>
                        <td>
                            <eShip:CustomTextBox runat="server" ID="txtNameOnCard" CssClass="w200" MaxLength="200" />
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">
                            <label class="upper">Card Number:</label>
                        </td>
                        <td>
                            <eShip:CustomTextBox runat="server" ID="txtCardNumber" CssClass="w200" Type="NumbersOnly" AutoCompleteType="Disabled" />
                        </td>
                    </tr>
                    <tr runat="server" id="trCardExpirationDate">
                        <td class="text-right">
                            <label class="upper">Expiration Date:</label>
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlCardExpirationMonth" DataTextField="Text" DataValueField="Value" CssClass="w60" />/
                                <asp:DropDownList runat="server" ID="ddlCardExpirationYear" DataTextField="Text" DataValueField="Value" CssClass="w80" />
                        </td>
                    </tr>
                    <tr runat="server" id="trCardSecurityCode">
                        <td class="text-right">
                            <label class="upper">
                                <abbr title='Visa®, Mastercard®, and Discover® cardholders:
    Turn your card over and look at the signature box. You should see
    either the entire 16-digit credit card number or just the last 
    four digits followed by a special 3-digit code. This 3-digit code
    is your CVV number / Card Security Code.
                                    
American Express® cardholders:
    Look for the 4-digit code printed on the front of your card just
    above and to the right of your main credit card number. This 
    4-digit code is your Card Identification Number (CID). The CID 
    is the four-digit code printed just above the Account Number. '>
                                    Security Code:</abbr></label>
                        </td>
                        <td>
                            <eShip:CustomTextBox runat="server" ID="txtCardSecurityCode" CssClass="w80" Type="NumbersOnly" MaxLength="5" AutoCompleteType="Disabled" />
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right pt10">
                            <label class="upper">Amount ($):</label>
                        </td>
                        <td class="pt10">
                            <eShip:CustomTextBox runat="server" ID="txtAmountPaid" CssClass="w100" Type="FloatingPointNumbers" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col_1_2 bbox pl20">
            <div class="row">
                <script type="text/javascript">
                    $(document).ready(function () {
                        $('#<%= txtBillingPostalCode.ClientID %>').change(function () {
                            getCityAndState();
                        });

                        $(jsHelper.AddHashTag('<%= ddlBillingCountry.ClientID %>')).change(function () {
                            getCityAndState();
                        });

                        function getCityAndState() {
                            var ids = new ControlIds();
                            ids.City = $('#<%= txtBillingCity.ClientID %>').attr('id');
                            ids.State = $('#<%= txtBillingState.ClientID %>').attr('id');
                            FindCityAndState(
                                $('#<%= txtBillingPostalCode.ClientID %>').val(),
                            $('#<%= ddlBillingCountry.ClientID %>').val(),
                            ids);
                        }
                    });
                </script>
                <h5 class="pl40 mb0 pb10">Billing Information</h5>
                <table class="poptable">
                    <tr>
                        <td class="text-right" style="width: 30%">
                            <label class="upper">First Name</label>
                        </td>
                        <td>
                            <eShip:CustomTextBox runat="server" ID="txtFirstName" CssClass="w200" />
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">
                            <label class="upper">Last Name</label>
                        </td>
                        <td>
                            <eShip:CustomTextBox runat="server" ID="txtLastName" CssClass="w200" />
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">
                            <label class="upper">Street:</label>
                        </td>
                        <td>
                            <eShip:CustomTextBox runat="server" ID="txtBillingStreet" CssClass="w200" />
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">
                            <label class="upper">City:</label>
                        </td>
                        <td>
                            <eShip:CustomTextBox runat="server" ID="txtBillingCity" CssClass="w200" />
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">
                            <label class="upper">State:</label>
                        </td>
                        <td>
                            <eShip:CustomTextBox runat="server" ID="txtBillingState" CssClass="w200" />
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">
                            <label class="upper">Postal Code:</label>
                        </td>
                        <td>
                            <eShip:CustomTextBox runat="server" ID="txtBillingPostalCode" CssClass="w200" />
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">
                            <label class="upper">Country:</label>
                        </td>
                        <td>
                            <eShip:CachedObjectDropDownList Type="Countries" ID="ddlBillingCountry" runat="server" CssClass="w200"  DefaultValue="0"
                                EnableChooseOne="True"  />
                        </td>
                    </tr>
                    <tr>
                        <td class="pt20 pb10"></td>
                        <td class="pt20 pb10">
                            <asp:Button runat="server" ID="btnPay" OnClick="OnPayClicked" Text="Pay" CausesValidation="False" OnClientClick="javascript:ShowProcessingDivBlock();"/>
                            <asp:Button runat="server" ID="btnCloseMiscReceiptEntry" OnClick="OnCloseClicked" Text="Cancel" CausesValidation="False" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
</asp:Panel>

<asp:Panel runat="server" ID="pnlManagePaymentProfiles" CssClass="popupControl" Visible="False">
    <div class="popheader">
        <h4>Payment Profiles For <asp:Literal runat="server" ID="litPaymentProfileCustomerTitle"/></h4>
        <asp:ImageButton ID="ibtnCloseManagePaymentProfiles" ImageUrl="~/images/icons2/close.png" CssClass="close" CausesValidation="false" OnClick="OnCloseManagePaymentProfilesClicked" runat="server" />
    </div>
    <div class="pt10"></div>
    <eShip:PaymentProfileManagerControl runat="server" ID="ppmPaymentProfileManager" OnProcessingMessages="OnPaymentProfileManagerProcessingMessages"/>
</asp:Panel>

<asp:Panel ID="pnlManagePaymentProfilesDimScreen" CssClass="dimBackgroundControl" runat="server" Visible="false" />
<asp:Panel ID="pnlPaymentEntryDimScreen" CssClass="dimBackground" runat="server" Visible="false" />

<eShip:CustomHiddenField runat="server" ID="hidPaymentGatewayType" />
<eShip:CustomHiddenField runat="server" ID="hidCustomer" />
<eShip:CustomHiddenField runat="server" ID="hidFlag" />
