﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="InvoiceControlAccountUpdateView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.InvoiceControlAccountUpdateView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Administration" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Operations" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>
<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="false"
        ShowEdit="false" ShowDelete="false" ShowSave="True" ShowFind="false" ShowImport="False"
        ShowExport="false" OnSave="OnSaveInvoices" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
                Invoice Customer Control Account Updates
            </h3>
        </div>
        <hr class="dark mb10" />
        <div>
            <ul>
                <li class="note">Only invoices, not posted and missing control accounts for customer requiring control
				account will be displayed. </li>
                <li class="note">If no control account is selected for invoice record, it will not be updated.</li>
            </ul>
        </div>
        <hr class="fat" />
        <eShip:CustomerControlAccountFinderControl ID="customerControlAccountFinder" runat="server"
            Visible="False" OnItemSelected="OnCustomerControlAccountFinderItemSelected" OnSelectionCancel="OnCustomerControlAccountFinderSelectionCancel" />
        <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
        <div class="rowgroup">
            <eShip:TableFreezeHeaderExtender runat="server" ID="tfheInvoicesTable" TableId="invoicesTable" HeaderZIndex="2" />
            <asp:ListView ID="lstInvoices" runat="server" ItemPlaceholderID="itemPlaceHolder">
                <LayoutTemplate>
                    <h5>Invoices to Update</h5>
                    <table class="line2 pl2" id="invoicesTable">
                        <tr>
                            <th style="width: 8%;">Invoice Number
                            </th>
                            <th style="width: 8%;">Invoice Date
                            </th>
                            <th style="width: 15%;">Invoice Customer
                            </th>
                            <th style="width: 8%;">Shipment Number
                            </th>
                            <th style="width: 18%;">Shipment Origin
                            </th>
                            <th style="width: 18%;">Shipment Destination
                            </th>
                            <th style="width: 9%;">Invoice Created By
                            </th>
                            <th style="width: 16%;">Control Account
                            </th>

                        </tr>
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <eShip:CustomHiddenField runat="server" ID="hidInvoiceId" Value='<%# Eval("InvoiceId") %>' />
                            <eShip:CustomHiddenField runat="server" ID="hidItemIndex" Value='<%# Container.DataItemIndex %>' />
                            <%# ActiveUser.HasAccessTo(ViewCode.Invoice) 
                                    ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Invoice Record'>{2}</a>", 
                                        ResolveUrl(InvoiceView.PageAddress),
                                        WebApplicationConstants.TransferNumber, 
                                        Eval("InvoiceNumber"))  
                                    : Eval("InvoiceNumber") %>
                        </td>
                        <td>
                            <%# Eval("InvoiceDate").FormattedShortDate() %>
                        </td>
                        <td>
                            <eShip:CustomHiddenField runat="server" ID="hidCustomerId" Value='<%# Eval("CustomerId") %>' />
                            <%# ActiveUser.HasAccessTo(ViewCode.Customer) 
                                    ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Customer Record'>{3}</a>", 
                                        ResolveUrl(CustomerView.PageAddress),
                                        WebApplicationConstants.TransferNumber, 
                                        Eval("CustomerId").GetString().UrlTextEncrypt(),  
                                        string.Format("{0} - {1}", Eval("InvoiceCustomerNumber"), Eval("InvoiceCustomerName")))  
                                    : string.Format("{0} - {1}", Eval("InvoiceCustomerNumber"), Eval("InvoiceCustomerName")) %>

                        </td>
                        <td>
                            <%# ActiveUser.HasAccessTo(ViewCode.Shipment) 
                                    ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Shipment Record'>{2}</a>", 
                                        ResolveUrl(ShipmentView.PageAddress),
                                        WebApplicationConstants.TransferNumber, 
                                        Eval("ShipmentNumber"))  
                                    : Eval("ShipmentNumber") %>
                        </td>
                        <td>
                            <%# Eval("ShipmentOrigin") %>
                        </td>
                        <td>
                            <%# Eval("ShipmentDestination") %>
                        </td>
                        <td>
                            <%# ActiveUser.HasAccessTo(ViewCode.User) 
                                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To User Record'>{3}</a>", 
                                                    ResolveUrl(UserView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    Eval("UserId").GetString().UrlTextEncrypt(),
                                                    string.Format("{0} {1}", Eval("UserFirstName"), Eval("UserLastName")))  
                                                : string.Format("{0} {1}", Eval("UserFirstName"), Eval("UserLastName")) %>
                        </td>
                        <td class="text-right">
                            <div class="fieldgroup text-left">
                                <eShip:CustomTextBox runat="server" ID="txtControlAccount" CssClass="w100" />
                            </div>
                            <div class="fieldgroup">
                                <asp:ImageButton runat="server" ID="ibtnControlAccountSearch" CausesValidation="False"
                                    ImageUrl="~/images/icons2/search_dark.png" OnClick="OnCustomerControlAccountSearchClicked" />
                            </div>

                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </div>

        <div class="rowgroup">
            <eShip:TableFreezeHeaderExtender runat="server" ID="tfheInvoicesCompleteTable" TableId="completeInvoiceTable" HeaderZIndex="2" />
            <asp:ListView ID="lstCompleteInvoices" runat="server" ItemPlaceholderID="itemPlaceHolder">
                <LayoutTemplate>
                    <h5>Invoices Update Result</h5>
                    <table class="line2 pl2" id="completeInvoiceTable">
                        <tr>
                            <th style="width: 20%;">Message Type
                            </th>
                            <th style="width: 80%;">Update Message
                            </th>
                        </tr>
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr>
                        <td class="top">
                            <%# Eval("Type")%>
                        </td>
                        <td class="top">
                            <%# Eval("Message")%>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </div>
        <eShip:CustomHiddenField runat="server" ID="hidEditingIndex" />
    </div>
</asp:Content>
