﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="AverageWeeklyFuelView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.AverageWeeklyFuelView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        OnNew="OnNewClick" OnImport="OnImportClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
                Average Weekly Fuels
                <small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>
        <span class="clearfix"></span>
        <hr class="dark mb10" />
        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
            <table>
                <tr>
                    <td class="text-right">
                        <label class="upper blue">START EFFECTIVE DATE:</label>
                    </td>
                    <td>
                        <eShip:CustomTextBox runat="server" ID="txtStartDate" CssClass="w100" Type="Date" placeholder="99/99/9999"/>
                    </td>
                    <td class="text-right">
                        <label class="upper blue">END EFFECTIVE DATE:</label>
                    </td>
                    <td>
                        <eShip:CustomTextBox runat="server" ID="txtEndDate"  CssClass="w100" Type="Date" placeholder="99/99/9999"/>
                    </td>
                </tr>
            </table>
            <hr class="fat" />
            <div class="row mb20">
                <div class="row">
                    <div class="right">
                        <asp:Button ID="btnSearch" runat="server" Text="SEARCH" OnClick="OnSearchClicked" CausesValidation="true" CssClass="mr10" />
                        <asp:Button ID="btnExport" runat="server" Text="EXPORT" OnClick="OnExportClicked" ValidationGroup="SearchGroup" CausesValidation="true" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheAccountBucketTable" TableId="averageWeeklyFuelTable" HeaderZIndex="2" />
        <asp:ListView ID="lvwRegistry" runat="server" ItemPlaceholderID="itemPlaceHolder">
            <LayoutTemplate>
                <div class="rowgroup">
                    <table class="line2 pl2" id="averageWeeklyFuelTable">
                        <tr>
                            <th style="width: 5%">Index</th>
                            <th style="width: 25%">Effective</th>
                            <th style="width: 25%">Created</th>
                            <th style="width: 37%">Code</th>
                            <th style="width: 8%" class="text-center">Action</th>
                        </tr>
                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                    </table>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td rowspan="2" class="top text-center">
                        <%# string.Format("{0}. ", Container.DataItemIndex+1) %>
                    </td>
                    <td>
                        <%# Eval("EffectiveDate").FormattedShortDate()%>
                    </td>
                    <td>
                        <%# Eval("DateCreated").FormattedShortDate() %>
                    </td>
                    <td>
                        <%# Eval("ChargeCodeDescription") + " (" + Eval("ChargeCode") + ")" %>
                    </td>
                    <td rowspan="2" class="top text-center">
                        <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png"
                            ToolTip="Edit" CausesValidation="false" OnClick="OnEditClicked" Enabled='<%# Access.Modify %>' />
                        <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                            ToolTip="Delete" CausesValidation="false" OnClick="OnDeleteClicked" Enabled='<%# Access.Remove %>' />
                        <ajax:ConfirmButtonExtender runat="server" ID="cbeDelete" TargetControlID="ibtnDelete" ConfirmText="Are you sure you want to delete record?" />
                    </td>
                </tr>
                <tr class="f9">
                    <td colspan="3" class="forceLeftBorder">
                        <eShip:CustomHiddenField ID="averageWeeklyFuelId" runat="server" Value='<%# Eval("Id") %>' />
                        <div class="rowgroup">
                            <div class="row">
                                <div class="fieldgroup mr20">
                                    <label class="text-right blue upper w200">East Coast ($):</label>
                                    <label class="pl5 w90"><%# Eval("EastCoastCost") %></label>
                                </div>
                                <div class="fieldgroup mr20">
                                    <label class="text-right blue upper w200">New England ($):</label>
                                    <label class="pl5 w90"><%# Eval("NewEnglandCost")%></label>
                                </div>
                                <div class="fieldgroup">
                                    <label class="text-right blue upper w200">Central Atlantic ($):</label>
                                    <label class="pl5 w90"><%# Eval("CentralAtlanticCost")%></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="fieldgroup mr20">
                                    <label class="text-right blue upper w200">Lower Atlantic ($):</label>
                                    <label class="pl5 w90"><%# Eval("LowerAtlanticCost") %></label>
                                </div>

                                <div class="fieldgroup mr20">
                                    <label class="text-right blue upper w200">Midwest ($):</label>
                                    <label class="pl5 w90"><%# Eval("MidwestCost")%></label>
                                </div>
                                <div class="fieldgroup">
                                    <label class="text-right blue upper w200">Gulf Coast ($):</label>
                                    <label class="pl5 w90"><%# Eval("GulfCoastCost")%></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="fieldgroup mr20">
                                    <label class="text-right blue upper w200">Rocky Mountain ($):</label>
                                    <label class="pl5 w90"><%# Eval("RockyMountainCost")%></label>
                                </div>
                                <div class="fieldgroup mr20">
                                    <label class="text-right blue upper w200">West Coast ($):</label>
                                    <label class="pl5 w90"><%# Eval("WestCoastCost")%></label>
                                </div>
                                <div class="fieldgroup mr20">
                                    <label class="text-right blue upper w200">National ($):</label>
                                    <label class="pl5 w90"><%# Eval("NationalCost")%></label>
                                </div>
                                <div class="fieldgroup mr20">
                                    <label class="text-right blue upper w200">California ($):</label>
                                    <label class="pl5 w90"><%# Eval("CaliforniaCost")%></label>
                                </div>
                                <div class="fieldgroup">
                                    <label class="text-right blue upper w200">West Coast w/o California ($):</label>
                                    <label class="pl5 w90">
                                        <%# Eval("WestCoastLessCaliforniaCost")%></label>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
        <asp:Panel ID="pnlEdit" runat="server" Visible="false" DefaultButton="btnSave">
            <div class="popupControl popupControlOverW500">
                <div class="popheader">
                    <h4>Add/Modify Average Weekly Fuel
                    </h4>
                    <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                        CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
                </div>
                <div class="row">
                    <table class="poptable">
                        <tr>
                            <td colspan="2" class="red">
                                <asp:Literal ID="litMessage" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right" style="width: 50%">
                                <label class="upper">Effective Date: </label>
                            </td>
                            <td>
                                <eShip:CustomHiddenField ID="hidAverageWeeklyFuelId" runat="server" />
                                <eShip:CustomTextBox runat="server" ID="txtEffectiveDate"  CssClass="w100" Type="Date" placeholder="99/99/9999" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Charge Code: </label>
                            </td>
                            <td>
                                <eShip:CachedObjectDropDownList runat="server" ID ="ddlChargeCodes" CssClass="w150" Type="ChargeCodes" DefaultValue="0" EnableChooseOne="True"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">East Coast Cost ($): </label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtEastCoastCost" runat="server" MaxLength="8" CssClass="w150" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvEastCoastCost" ControlToValidate="txtEastCoastCost"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtEastCoastCost"
                                    FilterType="Custom, Numbers" ValidChars="." />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">New England Cost ($): </label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtNewEnglandCost" runat="server" MaxLength="8" CssClass="w150" />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtNewEnglandCost"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtNewEnglandCost"
                                    FilterType="Custom, Numbers" ValidChars="." />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Central Atlantic Cost ($): </label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtCentralAtlanticCost" runat="server" MaxLength="8" CssClass="w150" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvCentralAtlanticCost" ControlToValidate="txtCentralAtlanticCost"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtCentralAtlanticCost"
                                    FilterType="Custom, Numbers" ValidChars="." />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Lower Atlantic Cost ($): </label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtLowerAtlanticCost" runat="server" MaxLength="8" CssClass="w150" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvLowerAtlanticCost" ControlToValidate="txtLowerAtlanticCost"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtLowerAtlanticCost"
                                    FilterType="Custom, Numbers" ValidChars="." />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Midwest Cost ($): </label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtMidwestCost" runat="server" MaxLength="8" CssClass="w150" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvMidwestCost" ControlToValidate="txtMidwestCost"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtMidwestCost"
                                    FilterType="Custom, Numbers" ValidChars="." />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Gulf Coast Cost ($): </label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtGulfCoastCost" runat="server" MaxLength="8" CssClass="w150" />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator6" ControlToValidate="txtGulfCoastCost"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtGulfCoastCost"
                                    FilterType="Custom, Numbers" ValidChars="." />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Rocky Mountain Cost ($): </label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtRockyMountainCost" runat="server" MaxLength="8" CssClass="w150" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvRockyMountainCost" ControlToValidate="txtRockyMountainCost"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="txtRockyMountainCost"
                                    FilterType="Custom, Numbers" ValidChars="." />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">West Coast Cost ($): </label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtWestCoastCost" runat="server" MaxLength="8" CssClass="w150" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvWestCoastCost" ControlToValidate="txtWestCoastCost"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="txtWestCoastCost"
                                    FilterType="Custom, Numbers" ValidChars="." />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">West Coast Less California Cost ($): </label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtWestCoastLessCaliforniaCost" runat="server" MaxLength="8" CssClass="w150" />
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtWestCoastLessCaliforniaCost"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" TargetControlID="txtWestCoastLessCaliforniaCost"
                                    FilterType="Custom, Numbers" ValidChars="." />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">California Cost ($): </label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtCaliforniaCost" runat="server" MaxLength="8" CssClass="w150" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvCaliforniaCost" ControlToValidate="txtCaliforniaCost"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" TargetControlID="txtCaliforniaCost"
                                    FilterType="Custom, Numbers" ValidChars="." />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">National Cost ($): </label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtNationalCost" runat="server" MaxLength="8" CssClass="w150" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvNationalCost" ControlToValidate="txtNationalCost"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" TargetControlID="txtNationalCost"
                                    FilterType="Custom, Numbers" ValidChars="." />
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td class="pt10">
                                <asp:Button ID="btnSave" Text="Save" OnClick="OnSaveClick" runat="server" />
                                <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnCloseClicked" runat="server" CausesValidation="false" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
        <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information" OnOkay="OnOkayProcess" />
        <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
            OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
        <asp:Panel ID="pnlDimScreen" CssClass="dimBackgroundControl" runat="server" Visible="false" />
        <eShip:CustomHiddenField runat="server" ID="hidFlag" />
    </div>
</asp:Content>
