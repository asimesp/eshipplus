﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="ShipmentAuditView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.ShipmentAuditView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowMore="True" OnCommand="OnToolbarCommand" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                CssClass="pageHeaderIcon" />
            <h3>Shipment Audit
                <div class="pageHeaderRecDesc">[<asp:Literal runat="server" ID="litShipmentHeader" />]</div>
            </h3>
        </div>

        <hr class="dark" />

        <eShip:DocumentDisplayControl runat="server" ID="documentDisplay" Visible="false" OnClose="OnDocumentDisplayClosed" />
        <ajax:TabContainer ID="tabShipment" runat="server" CssClass="ajaxCustom">
            <ajax:TabPanel ID="tabDetails" runat="server" HeaderText="Details">
                <ContentTemplate>
                    <div class="rowgroup">
                        <div class="col_1_2 bbox pr30">
                            <h5>General Information</h5>
                            <div class="row">
                                <div class="fieldgroup">
                                    <label class="wlabel">Shipment Number</label>
                                    <eShip:CustomTextBox runat="server" CssClass="w200 disabled" ReadOnly="True" ID="txtShipmentNumber" />
                                </div>
                                <div class="fieldgroup_s mr13">
                                    <asp:HyperLink runat="server" ID="hypShipmentView" Target="_blank">
                                        <asp:Image runat="server" ID="imgGoToShipmentView" ImageUrl="~/images/icons2/arrow_newTab.png" AlternateText="Go to Shipment Record" />
                                    </asp:HyperLink>
                                </div>
                                <div class="fieldgroup">
                                    <label class="wlabel">Status</label>
                                    <eShip:CustomTextBox runat="server" ID="txtStatus" CssClass="w200 disabled" ReadOnly="True" />
                                </div>
                            </div>
                            <div class="row mt20">
                                <div class="fieldgroup mr20">
                                    <label class="wlabel">Customer Number</label>
                                    <eShip:CustomTextBox runat="server" ID="txtCustomerNumber" CssClass="w150 disabled" ReadOnly="True" />
                                </div>
                                <div class="fieldgroup ">
                                    <label class="wlabel">Customer Name</label>
                                    <eShip:CustomTextBox runat="server" ID="txtCustomerName" CssClass="w280 disabled" ReadOnly="True" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="fieldgroup mr20">
                                    <label class="wlabel">Primary Acct Bucket</label>
                                    <eShip:CustomTextBox runat="server" ID="txtPrimaryAccountBucket" CssClass="w150 disabled" ReadOnly="True" />
                                </div>
                                <div class="fieldgroup ">
                                    <label class="wlabel">Primary Acct Bucket Description</label>
                                    <eShip:CustomTextBox runat="server" ID="txtPrimaryAccountBucketDescription" CssClass="w280 disabled" ReadOnly="True" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="fieldgroup mr20">
                                    <label class="wlabel">Prefix</label>
                                    <eShip:CustomTextBox runat="server" ID="txtPrefixCode" CssClass="w150 disabled" ReadOnly="True" />
                                </div>
                                <div class="fieldgroup ">
                                    <label class="wlabel">Prefix Description</label>
                                    <eShip:CustomTextBox runat="server" ID="txtPrefixDescription" CssClass="w280 disabled" ReadOnly="True" />
                                </div>
                            </div>
                        </div>
                        <div class="col_1_2 bbox vlinedarkleft">
                            <h5>Origin - Destination</h5>
                            <div class="row">
                                <div class="fieldgroup mr20">
                                    <label class="wlabel">Origin</label>
                                    <eShip:CustomTextBox runat="server" ID="txtOrigin" CssClass="w200 disabled" ReadOnly="True" TextMode="MultiLine" />
                                </div>
                                <div class="fieldgroup">
                                    <label class="wlabel">Destination</label>
                                    <eShip:CustomTextBox runat="server" ID="txtDestination" CssClass="w200 disabled" ReadOnly="True" TextMode="MultiLine" />
                                </div>
                            </div>
                            <h5 class="pt10">Dates</h5>
                            <div class="row">
                                <div class="fieldgroup">
                                    <label class="wlabel">Created</label>
                                    <eShip:CustomTextBox runat="server" ID="txtDateCreated" CssClass="w200 disabled" ReadOnly="True" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="fieldgroup mr20">
                                    <label class="wlabel">Desired Pickup</label>
                                    <eShip:CustomTextBox runat="server" ID="txtDesiredPickupDate" CssClass="w200 disabled" ReadOnly="True" />
                                </div>
                                <div class="fieldgroup">
                                    <label class="wlabel">Actual Pickup</label>
                                    <eShip:CustomTextBox runat="server" ID="txtActualPickupDate" CssClass="w200 disabled" ReadOnly="True" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="fieldgroup mr20">
                                    <label class="wlabel">Estimated Delivery</label>
                                    <eShip:CustomTextBox runat="server" ID="txtEstimatedDeliveryDate" CssClass="w200 disabled" ReadOnly="True" />
                                </div>
                                <div class="fieldgroup">
                                    <label class="wlabel">Actual Delivery</label>
                                    <eShip:CustomTextBox runat="server" ID="txtActualDeliveryDate" CssClass="w200 disabled" ReadOnly="True" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="dark" />
                    <div class="rowgroup">
                        <div class="col_1_3 bbox">
                            <h5>Finance Record Counts</h5>
                            <table class="stripe">
                                <tr>
                                    <th>Document</th>
                                    <th class="text-center">Count</th>
                                </tr>
                                <tr>
                                    <td>Invoices
                                    </td>
                                    <td class="text-center">
                                        <asp:Literal runat="server" ID="litInvoiceCount" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Credit Invoices
                                    </td>
                                    <td class="text-center">
                                        <asp:Literal runat="server" ID="litCreditInvoicesCount" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Supplemental Invoices
                                    </td>
                                    <td class="text-center">
                                        <asp:Literal runat="server" ID="litSupplementalInvoicesCount" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Vendor Bills
                                    </td>
                                    <td class="text-center">
                                        <asp:Literal runat="server" ID="litVendorBillsCount" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Credit Vendor Bills
                                    </td>
                                    <td class="text-center">
                                        <asp:Literal runat="server" ID="litCreditVendorBillsCount" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col_2_3 bbox pl20">
                            <h5>Job Financial Summary</h5>
                            <div class="row">
                                <table class="stripe">
                                    <tr>
                                        <th>Category/Group
                                        </th>
                                        <th class="text-right">Estimate
                                        </th>
                                        <th class="text-right">Actual
                                        </th>
                                        <th class="text-right pr30">(Act. - Est.)
                                        </th>
                                    </tr>
                                    <tr>
                                        <td class="pl40">Total Cost
                                        </td>
                                        <td class="text-right">
                                            <asp:Literal runat="server" ID="litEstimateCost" />
                                        </td>
                                        <td class="text-right">
                                            <asp:Literal runat="server" ID="litActualCost" />
                                        </td>
                                        <td class="text-right pr30">
                                            <asp:Literal runat="server" ID="litDiffCost" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="pl40">Total Revenue
                                        </td>
                                        <td class="text-right">
                                            <asp:Literal runat="server" ID="litEstimateRevenue" />
                                        </td>
                                        <td class="text-right">
                                            <asp:Literal runat="server" ID="litActualRevenue" />
                                        </td>
                                        <td class="text-right pr30">
                                            <asp:Literal runat="server" ID="litDiffRevenue" />
                                        </td>

                                    </tr>
                                    <tr>
                                        <td class="pl40">Total Profit
                                        </td>
                                        <td class="text-right">
                                            <asp:Literal runat="server" ID="litEstimateProfit" />
                                        </td>
                                        <td class="text-right">
                                            <asp:Literal runat="server" ID="litActualProfit" />
                                        </td>
                                        <td class="text-right pr30">
                                            <asp:Literal runat="server" ID="litDiffProfit" />
                                        </td>

                                    </tr>
                                    <tr>
                                        <td class="pl40">Profit %
                                        </td>
                                        <td class="text-right">
                                            <asp:Literal runat="server" ID="litGPMEstimate" />
                                        </td>
                                        <td class="text-right">
                                            <asp:Literal runat="server" ID="litGPMActual" />
                                        </td>
                                        <td>&nbsp;
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>Total Outstanding
                                        </td>
                                        <td></td>
                                        <td class="text-right">
                                            <asp:Literal runat="server" ID="litTotalOutstanding" />
                                        </td>
                                        <td></td>

                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel ID="tabItems" runat="server" HeaderText="Items">
                <ContentTemplate>
                    <eShip:TableFreezeHeaderExtender runat="server" ID="tfheItemsTable" TableId="itemsTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                    <table class="stripe" id="itemsTable">
                        <tr>
                            <th style="text-align: center; width: 2%;">&nbsp;
                            </th>
                            <th style="width: 9%;">Package Type
                            </th>
                            <th style="width: 5%;" class="text-center">Package Qty.
                            </th>
                            <th style="width: 10%;" class="text-right">Weight (lb)
                            </th>
                            <th style="width: 10%;" class="text-right">Length (in)
                            </th>
                            <th style="width: 10%;" class="text-right">Width (in)
                            </th>
                            <th style="width: 10%;" class="text-right">Height (in)
                            </th>
                            <th style="width: 5%;" class="text-center">Stackable
                            </th>
                            <th style="width: 5%;" class="text-center">Pieces
                            </th>
                            <th style="width: 5%;" class="text-center">Actual Class
                            </th>
                            <th style="width: 5%;" class="text-center">Rated Class
                            </th>
                            <th style="width: 10%">Value
                            </th>
							 <th style="width: 5%">IsHazmat
                            </th>
                            <th style="width: 5%;">
                                <abbr title="National Motor Freight Classification">
                                    NMFC</abbr>
                            </th>
                            <th style="width: 5%">
                                <abbr title="Harmonized Tariff Schedule">
                                    HTS</abbr>
                            </th>
                        </tr>
                        <asp:ListView ID="lstItems" runat="server" ItemPlaceholderID="itemPlaceHolder">
                            <LayoutTemplate>
                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="top" rowspan="2">
                                        <%# Container.DataItemIndex + 1 %>.
                                    </td>
                                    <td>
                                        <%# GetPackageTypeDescription(Eval("PackageTypeId")) %>
                                    </td>
                                    <td class="text-center">
                                        <%# Eval("Quantity") %>
                                    </td>

                                    <td class="text-right">
                                        <%# Eval("ActualWeight", "{0:n2}") %>
                                    </td>
                                    <td class="text-right">
                                        <%# Eval("ActualLength", "{0:n2}") %>
                                    </td>
                                    <td class="text-right">
                                        <%# Eval("ActualWidth", "{0:n2}") %>        
                                    </td>
                                    <td class="text-right">
                                        <%# Eval("ActualHeight", "{0:n2}") %>
                                    </td>
                                    <td class="text-center">
                                        <%# Eval("IsStackable").ToBoolean() ? WebApplicationConstants.HtmlCheck : string.Empty %>
                                    </td>
                                    <td class="text-center">
                                        <%# Eval("PieceCount") %>
                                    </td>
                                    <td class="text-center">
                                        <%# Eval("ActualFreightClass") %>
                                    </td>
                                    <td class="text-center">
                                        <%# Eval("RatedFreightClass") %>
                                    </td>
                                    <td>
                                        <%# Eval("Value", "{0:c2}") %>
                                    </td>
									 <td class="text-center">
                                         <%# Eval("HazardousMaterial").ToBoolean() ? WebApplicationConstants.HtmlCheck : string.Empty %>
                                    </td>
                                    <td>
                                        <%# Eval("NMFCCode") %>
                                    </td>
                                    <td>
                                        <%# Eval("HTSCode") %>
                                    </td>
                                </tr>
                                <tr class="hidden">
                                    <td></td>
                                </tr>
                                <tr class="bottom_shadow">
                                    <td colspan="13">
                                        <div class="row">
                                            <div class="fieldgroup mr20 vlinedarkright">
                                                <label class="wlabel">Item Description</label>
                                                <eShip:CustomTextBox runat="server" ID="txtItemDescription" Text='<%# Eval("Description") %>' CssClass="w420 disabled" ReadOnly="True" TextMode="MultiLine" />
                                            </div>
                                            <div class="fieldgroup pl40">
                                                <label class="wlabel">Comments</label>
                                                <eShip:CustomTextBox runat="server" ID="txtComments" Text='<%# Eval("Comment") %>' CssClass="w420 disabled" ReadOnly="True" TextMode="MultiLine" />
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel ID="tabCharges" runat="server" HeaderText="Charges">
                <ContentTemplate>
                    <div class="row mb10">
                        <div class="right">
                            <eShip:ChargeStatistics2 runat="server" ID="chargeStatistics" />
                        </div>
                    </div>
                    <eShip:TableFreezeHeaderExtender runat="server" ID="tfheShipmentChargesTable" TableId="shipmentChargesTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                    <table class="stripe" id="shipmentChargesTable">
                        <tr>
                            <th style="width: 5%;">Index</th>
                            <th style="width: 30%;">Charge Code
                            </th>
                            <th style="width: 5%;" class="text-center">Quantity
                            </th>
                            <th style="width: 10%;" class="text-right">Unit Buy ($)
                            </th>
                            <th style="width: 10%;" class="text-right">Unit Sell ($)
                            </th>
                            <th style="width: 10%;" class="text-right">Unit Discount ($)
                            </th>
                            <th style="width: 10%;" class="text-right">Final Buy ($)
                            </th>
                            <th style="width: 10%;" class="text-right">Final Sell ($)
                            </th>
                            <th style="width: 10%;" class="text-right">Amount Due ($)
                            </th>
                        </tr>
                        <asp:ListView ID="lstCharges" runat="server" ItemPlaceholderID="itemPlaceHolder">
                            <LayoutTemplate>
                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td rowspan="2" class="top"><%# Container.DataItemIndex + 1 %>.</td>
                                    <td>
                                        <%# GetChargeCodeDescription(Eval("ChargeCodeId"))%>
                                    </td>
                                    <td class="text-center">
                                        <%# Eval("Quantity") %>
                                    </td>
                                    <td class="text-right">
                                        <%# Eval("UnitBuy").ToDecimal().ToString("f4") %>
                                    </td>
                                    <td class="text-right">
                                        <%# Eval("UnitSell").ToDecimal().ToString("f4") %>
                                    </td>
                                    <td class="text-right">
                                        <%# Eval("UnitDiscount").ToDecimal().ToString("f4") %>
                                    </td>
                                    <td class="text-right">
                                        <%# Eval("FinalBuy").ToDecimal().ToString("f4") %>
                                    </td>
                                    <td class="text-right">
                                        <%# Eval("FinalSell").ToDecimal().ToString("f4") %>
                                    </td>
                                    <td class="text-right">
                                        <%# Eval("AmountDue").ToDecimal().ToString("f4") %>
                                    </td>
                                </tr>
                                <tr class="hidden">
                                    <td></td>
                                </tr>
                                <tr class="bottom_shadow">
                                    <td colspan="8">
                                        <div class="row">
                                            <label class="wlabel">Charge Comment</label>
                                            <eShip:CustomTextBox runat="server" Text='<%# Eval("Comment") %>' TextMode="MultiLine" ID="txtShipmentChargeComments" CssClass="w740 disabled" ReadOnly="True" />
                                        </div>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel ID="tabVendors" runat="server" HeaderText="Vendors">
                <ContentTemplate>
                    <eShip:TableFreezeHeaderExtender runat="server" ID="tfheVendorsTable" TableId="vendorsTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                    <table class="stripe" id="vendorsTable">
                        <tr>
                            <th style="width: 25%;">Name
                            </th>
                            <th style="width: 10%;">Vendor Number
                            </th>
                            <th style="width: 10%;">SCAC
                            </th>
                            <th style="width: 20%;">Pro Number
                            </th>
                            <th style="width: 30%;">Failure Code
                            </th>
                            <th style="width: 5%;" class="text-center">Primary
                            </th>
                        </tr>
                        <asp:ListView ID="lstVendors" runat="server" ItemPlaceholderID="itemPlaceHolder">
                            <LayoutTemplate>
                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <%# Eval("Name") %>
                                    </td>
                                    <td>
                                        <%# Eval("VendorNumber") %>
                                    </td>
                                    <td>
                                        <%# Eval("SCAC") %>
                                    </td>
                                    <td>
                                        <%# Eval("ProNumber") %>
                                    </td>
                                    <td>
                                        <%# Eval("FailureCode") %><br />
                                        <%# Eval("FailureComments") %>
                                    </td>
                                    <td class="text-center">
                                        <%# Eval("Primary").ToBoolean() ? WebApplicationConstants.HtmlCheck : string.Empty %>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel ID="tabInvoices" runat="server" HeaderText="Invoices">
                <ContentTemplate>
                    <eShip:TableFreezeHeaderExtender runat="server" ID="tfheInvoicesTable" TableId="invoicesTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                    <table class="stripe" id="invoicesTable">
                        <tr>
                            <th style="width: 10%;">Invoice Number
                            </th>
                            <th style="width: 8%;">Type
                            </th>
                            <th style="width: 20%;">Charge Code
                            </th>
                            <th style="width: 8%;">Invoice Date
                            </th>
                            <th style="width: 8%;">Post Date
                            </th>
                            <th style="width: 10%;" class="text-right">Unit Sell
                            </th>
                            <th style="width: 10%;" class="text-right">Unit Discount
                            </th>
                            <th style="width: 5%;" class="text-center">Quantity
                            </th>
                            <th style="width: 21%;">Acct. Bucket
                            </th>
                        </tr>
                        <asp:ListView ID="lstInvoices" runat="server" ItemPlaceholderID="itemPlaceHolder">
                            <LayoutTemplate>
                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <%# ActiveUser.HasAccessTo(ViewCode.Invoice) 
                                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Invoice Record'>{2}</a>", 
                                                    ResolveUrl(InvoiceView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    Eval("InvoiceNumber"))  
                                                : Eval("InvoiceNumber") %>
                                    </td>
                                    <td>
                                        <%# Eval("InvoiceType").FormattedString()%>
                                    </td>
                                    <td>
                                        <%# Eval("ChargeCodeDescription") %> (<%# Eval("ChargeCode") %>)
                                    </td>
                                    <td>
                                        <%# Eval("InvoiceDate").FormattedShortDate() %>
                                    </td>
                                    <td>
                                        <%# Eval("PostDate").FormattedShortDateSuppressEarliestDate() %>
                                    </td>
                                    <td class="text-right">
                                        <%# Eval("UnitSell").ToDecimal().ToString("c4") %>
                                    </td>
                                    <td class="text-right">
                                        <%# Eval("UnitDiscount").ToDecimal().ToString("c4")%>
                                    </td>
                                    <td class="text-center">
                                        <%# Eval("Quantity") %>
                                    </td>
                                    <td>
                                        <%# Eval("AccountBucketDescription")%> (<%# Eval("AccountBucketCode")%>)
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel ID="tabVendorBills" runat="server" HeaderText="Vendor Bills">
                <ContentTemplate>
                    <eShip:TableFreezeHeaderExtender runat="server" ID="tfheVendorBillsTable" TableId="vendorBillsTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                    <table class="stripe" id="vendorBillsTable">
                        <tr>
                            <th style="width: 10%;">Document Number
                            </th>
                            <th style="width: 10%;">Document Date
                            </th>
                            <th style="width: 8%;">Type
                            </th>
                            <th style="width: 8%;">Post Date
                            </th>
                            <th style="width: 25%;">Charge Code
                            </th>
                            <th style="width: 10%;" class="text-right">Unit Buy
                            </th>
                            <th style="width: 5%;" class="text-center">Quantity
                            </th>
                            <th style="width: 24%;">Acct. Bucket
                            </th>
                        </tr>
                        <asp:ListView ID="lstVendorBills" runat="server" ItemPlaceholderID="itemPlaceHolder">
                            <LayoutTemplate>
                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <%# ActiveUser.HasAccessTo(ViewCode.VendorBill) 
                                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Vendor Bill Record'>{3}</a>", 
                                                    ResolveUrl(VendorBillView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    Eval("VendorBillId").GetString().UrlTextEncrypt(),
                                                    Eval("DocumentNumber"))  
                                                : Eval("DocumentNumber") %>
                                    </td>
                                    <td>
                                        <%# Eval("DocumentDate").FormattedShortDate()%>
                                    </td>
                                    <td>
                                        <%# Eval("BillType").FormattedString()%>
                                    </td>
                                    <td>
                                        <%# Eval("PostDate").FormattedShortDateSuppressEarliestDate()%>
                                    </td>
                                    <td>
                                        <%# Eval("ChargeCodeDescription") %> (<%# Eval("ChargeCode") %>)
                                    </td>
                                    <td class="text-right">
                                        <%# Eval("UnitBuy").ToDecimal().ToString("c4") %>
                                    </td>
                                    <td class="text-center">
                                        <%# Eval("Quantity") %>
                                    </td>
                                    <td>
                                        <%# Eval("AccountBucketDescription")%> (<%# Eval("AccountBucketCode")%>)
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </ContentTemplate>
            </ajax:TabPanel>
        </ajax:TabContainer>
        <asp:Panel runat="server" ID="pnlNoAccess" Visible="False">
            <div class="popheader">
                <h4>Access Restriction
                </h4>
            </div>
            <table class="stripe ml10">
                <tr>
                    <td class="errorMsgLit">INADEQUATE PERMISSION
                    </td>
                </tr>
                <tr>
                    <td>Employees must have access to ShipmentAudits to view page content
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlRecordNotFound" Visible="False">
            <div class="popheader">
                <h4>Record Not Found
                </h4>
            </div>
            <table class="stripe ml10">
                <tr>
                    <td class="errorMsgLit">MISSING RECORD
                    </td>
                </tr>
                <tr>
                    <td>The record you are attempting to access was not found!
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <eShip:CustomHiddenField runat="server" ID="hidShipmentId" />
    </div>
</asp:Content>
