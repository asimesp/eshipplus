﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="CustomerView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.CustomerView" EnableEventValidation="false" %>
<%@ Register TagPrefix="asp" Namespace="LogisticsPlus.Eship.WebApplication.Members.Controls" Assembly="Pacman" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core.Registry" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Administration" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        ShowEdit="true" ShowDelete="true" ShowSave="true" ShowFind="true" OnFind="OnFindClicked" OnUnlock="OnUnlockClicked"
        OnSave="OnSaveClicked" OnDelete="OnDeleteClicked" OnNew="OnNewClicked" OnEdit="OnEditClicked" OnCommand="OnToolbarCommandClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                CssClass="pageHeaderIcon" />
            <h3>Customers
                <eShip:RecordIdentityDisplayControl runat="server" ID="ridcRecordIdentity" TargetControlId="txtCustomerName" />
            </h3>

            <div class="lockMessage">
                <asp:Literal ID="litMessage" runat="server" />
            </div>
        </div>

        <hr class="dark mb5" />

        <div class="errorMsgLit">
            <asp:Literal runat="server" ID="litErrorMessages" />
        </div>

        <eShip:CustomerFinderControl ID="customerFinder" Visible="false" runat="server" OpenForEditEnabled="true"
            EnableMultiSelection="false" OnItemSelected="OnCustomerFinderItemSelected" OnSelectionCancel="OnCustomerFinderSelectionCancel" />
        <eShip:SalesRepresentativeFinderControl runat="server" Visible="False" ID="salesRepFinder"
            OnItemSelected="OnSalesRepFinderItemSelected" OnSelectionCancel="OnSalesRepFinderItemCancelled" />
        <eShip:TierFinderControl ID="tierFinder" Visible="False" runat="server" OpenForEditEnabled="false"
            OnItemSelected="OnTierFinderItemSelected" OnSelectionCancel="OnTierFinderSelectionCancelled" />
        <eShip:PrefixFinderControl ID="prefixFinder" Visible="False" runat="server" ShowActiveRecordsOnly="true"
            OnItemSelected="OnPrefixFinderItemSelected" OnSelectionCancel="OnPrefixFinderSelectionCancelled" />
        <eShip:AccountBucketFinderControl ID="accountBucketFinder" Visible="False" runat="server"
            ShowActiveRecordsOnly="true" OnItemSelected="OnAccountBucketFinderItemSelected"
            OnSelectionCancel="OnAccountBucketFinderSelectionCancelled" EnableMultiSelection="False" />
        <eShip:UserFinderControl ID="userFinder" runat="server" Visible="false" EnableMultiSelection="true"
            OnMultiItemSelected="OnUserFinderMultiItemSelected" OnSelectionCancel="OnUserFinderSelectionCancelled" />
        <eShip:DocumentTagFinderControl ID="documentTagFinder" runat="server" EnableMultiSelection="true"
            Visible="false" OnMultiItemSelected="OnDocumentTagFinderMultiItemSelected" OnSelectionCancel="OnDocumentTagFinderSelectionCancelled" />
        <script type="text/javascript">
            function ClearHidFlag() {
                jsHelper.SetEmpty('<%= hidFlag.ClientID %>');
            }
        </script>

        <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
            Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" />
        <eShip:AjaxTabHeaderTextUpdater runat="server" ID="athtuTabUpdater" />


        <eShip:CustomHiddenField runat="server" ID="hidCustomerId" />
        <ajax:TabContainer ID="TabContainer1" runat="server" CssClass="ajaxCustom">
            <ajax:TabPanel runat="server" ID="tabDetails" HeaderText="Details">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlDetails">

                        <div class="rowgroup">
                            <div class="row">
                                <div class="fieldgroup mr10">
                                    <label class="wlabel blue">Date Created</label>
                                    <eShip:CustomTextBox ID="txtDateCreated" runat="server" ReadOnly="True" CssClass="w110 disabled" />
                                </div>
                                <div class="fieldgroup mr10 vlinedarkright">
                                    <label class="wlabel blue">Customer Number</label>
                                    <eShip:CustomTextBox ID="txtCustomerNumber" runat="server" MaxLength="50" ReadOnly="true" CssClass="w200 disabled" />
                                </div>
                                <div class="fieldgroup mr10">
                                    <label class="wlabel blue">Logo Image <span class="fs85em">(max: 75px by 400px)</span></label>
                                    <asp:FileUpload ID="fupLogoImage" runat="server" CssClass="jQueryUniform" />
                                    <asp:Button ID="btnClearLogo" runat="server" Text="CLEAR LOGO" OnClick="OnClearLogoClicked" CausesValidation="false" />
                                </div>
                                <div class="fieldgroup">
                                    <asp:Image runat="server" ID="imgLogo" CssClass="right Img60MaxHeight" Visible="False" />
                                    <eShip:CustomHiddenField runat="server" ID="hidLogoUrl" />
                                </div>
                            </div>
                        </div>

                        <hr class="dark" />

                        <div class="rowgroup">
                            <div class="col_1_2 bbox vlinedarkright">
                                <h5>General Details</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Name</label>
                                        <eShip:CustomTextBox ID="txtCustomerName" runat="server" CssClass="w200" MaxLength="50" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Type</label>
                                        <asp:DropDownList ID="ddlCustomerType" DataTextField="Value" DataValueField="Key" runat="server" CssClass="w200" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Payment Terms <span class="fs85em">(days)</span></label>
                                        <eShip:CustomTextBox ID="txtInvoiceTerms" runat="server" CssClass="w200" MaxLength="9" />
                                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtInvoiceTerms" FilterType="Numbers" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Credit Limit ($)</label>
                                        <eShip:CustomTextBox ID="txtCreditLimit" runat="server" CssClass="w200" />
                                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtCreditLimit"
                                            FilterType="Custom, Numbers" ValidChars="." />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Required Mileage Source</label>
                                        <eShip:CachedObjectDropDownList Type="MileageSources" ID="ddlMileageSources" runat="server" CssClass="w200" EnableNotApplicable="True"
                                            DefaultValue="0"/>
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">&nbsp;</label>
                                        <asp:CheckBox ID="chkActive" runat="server" CssClass="jQueryUniform" />
                                        <label class="mr10">Active</label>
                                        <asp:CheckBox runat="server" ID="chkCareOfAdddressFormatEnabled" CssClass="jQueryUniform" />
                                        <label>Customer name in BOL c/o</label>
                                    </div>
                                </div>

                                <div class="row mt20">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Default Account Bucket</label>
                                        <eShip:CustomTextBox ID="txtAccountBucketCode" runat="server" CssClass="w100" AutoPostBack="true" OnTextChanged="OnAccountBucketCodeChanged" />
                                        <asp:ImageButton runat="server" ID="ImageButton2" Enabled="true" OnClick="OnFindAccountBucket"
                                            ImageUrl="~/images/icons2/search_dark.png" CausesValidation="false" />
                                        <eShip:CustomTextBox ID="txtAccountBucketDescription" runat="server" ReadOnly="true" CssClass="w260 disabled" />
                                        <eShip:CustomHiddenField runat="server" ID="hidAccountBucketId" />
                                        <ajax:AutoCompleteExtender runat="server" ID="aceAccountBucketCode" TargetControlID="txtAccountBucketCode" ServiceMethod="GetAccountBucketList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                            EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Tier
                                            <%= hidTierId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.Tier) 
                                                    ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Tier Record'><img src={3} width='20' class='middle'/></a>", 
                                                                            ResolveUrl(TierView.PageAddress),
                                                                            WebApplicationConstants.TransferNumber, 
                                                                            hidTierId.Value.UrlTextEncrypt(),
                                                                            ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                    : string.Empty %>
                                        </label>
                                        <eShip:CustomTextBox ID="txtTierNumber" runat="server" CssClass="w100" AutoPostBack="true" OnTextChanged="OnTierNumberChanged" />
                                        <asp:ImageButton runat="server" ID="ibtnOpenTierFinder" Enabled="true" OnClick="OnFindTier"
                                            ImageUrl="~/images/icons2/search_dark.png" CausesValidation="false" />
                                        <eShip:CustomTextBox ID="txtTierName" runat="server" CssClass="w260 disabled" ReadOnly="true" />
                                        <eShip:CustomHiddenField runat="server" ID="hidTierId" />
                                        <ajax:AutoCompleteExtender runat="server" ID="aceTier" TargetControlID="txtTierNumber" ServiceMethod="GetTierList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                            EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Default Prefix</label>
                                        <eShip:CustomTextBox ID="txtPrefixCode" runat="server" CssClass="w100" AutoPostBack="true" OnTextChanged="OnPrefixCodeChanged" />
                                        <asp:ImageButton runat="server" ID="ImageButton1" Enabled="true" OnClick="OnFindPrefix"
                                            ImageUrl="~/images/icons2/search_dark.png" CausesValidation="false" />
                                        <eShip:CustomTextBox ID="txtPrefixDescription" runat="server" CssClass="w260 disabled" ReadOnly="true" />
                                        <eShip:CustomHiddenField runat="server" ID="hidPrefixId" />
                                        <ajax:AutoCompleteExtender runat="server" ID="acePrefixCode" TargetControlID="txtPrefixCode" ServiceMethod="GetPrefixList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                            EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                                        <asp:CheckBox ID="chkHidePrefix" runat="server" CssClass="jQueryUniform" />
                                        <label>Hide Prefix</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Sales Representative
                                             <%= hidSalesRepId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.SalesRepresentative) 
                                                    ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Sales Representative Record'><img src={3} width='20' class='middle'/></a>", 
                                                                            ResolveUrl(SalesRepresentativeView.PageAddress),
                                                                            WebApplicationConstants.TransferNumber, 
                                                                            hidSalesRepId.Value.UrlTextEncrypt(),
                                                                            ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                    : string.Empty %>
                                        </label>
                                        <eShip:CustomTextBox ID="txtSalesRepNumber" runat="server" CssClass="w100" AutoPostBack="true" OnTextChanged="OnSalesRepNumberChanged" />
                                        <asp:ImageButton runat="server" ID="ibtnOpenServiceRepFinder" Enabled="true" OnClick="OnFindServiceRepresentative"
                                            ImageUrl="~/images/icons2/search_dark.png" CausesValidation="false" />
                                        <eShip:CustomTextBox ID="txtSalesRepName" runat="server" CssClass="w260 disabled" ReadOnly="true" />
                                        <eShip:CustomHiddenField runat="server" ID="hidSalesRepId" />
                                        <ajax:AutoCompleteExtender runat="server" ID="aceSalesRep" TargetControlID="txtSalesRepNumber" ServiceMethod="GetSalesRepresentativesList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                            EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                                    </div>
                                </div>

                                <div class="row mt20">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Notes <span class="fs85em">[Internal Notes Only]</span>
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleNotes" MaxLength="1000" TargetControlId="txtNotes" />
                                        </label>
                                        <eShip:CustomTextBox ID="txtNotes" TextMode="MultiLine" runat="server" CssClass="w420 h150" />
                                    </div>
                                </div>
                                <div class="row mt10">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Additional BOL Text
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleAdditionalBolText" MaxLength="500" TargetControlId="txtAdditionBolText" />
                                        </label>
                                        <eShip:CustomTextBox ID="txtAdditionBolText" runat="server" TextMode="MultiLine" CssClass="w420 h150" />
                                    </div>
                                </div>

                                <h5 class="mt20">Shipper BOL Settings</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Shipper BOL Seed</label>
                                        <eShip:CustomTextBox ID="txtShipperBol" runat="server" CssClass="w200" />
                                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtShipperBol" FilterType="Numbers" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">&nbsp;</label>
                                        <asp:CheckBox ID="chkEnableBolSuffix" runat="server" CssClass="jQueryUniform" />
                                        <label>Enable Shipper BOL</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Shipper BOL Prefix <span class="fs85em">(if applicable)</span></label>
                                        <eShip:CustomTextBox ID="txtShipperBolPrefix" runat="server" CssClass="w200" MaxLength="10" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Shiipper BOL Suffix <span class="fs85em">(if applicable)</span> </label>
                                        <eShip:CustomTextBox ID="txtShipperBolSuffix" runat="server" CssClass="w200" MaxLength="10" />
                                    </div>
                                </div>

                                <h5 class="mt20">Custom Fields</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Custom Field 1</label>
                                        <eShip:CustomTextBox runat="server" ID="txtCustomField1" CssClass="w200" MaxLength="50" />
                                    </div>
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Custom Field 2</label>
                                        <eShip:CustomTextBox runat="server" ID="txtCustomField2" CssClass="w200" MaxLength="50" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Custom Field 3</label>
                                        <eShip:CustomTextBox runat="server" ID="txtCustomField3" CssClass="w200" MaxLength="50" />
                                    </div>
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Custom Field 4</label>
                                        <eShip:CustomTextBox runat="server" ID="txtCustomField4" CssClass="w200" MaxLength="50" />
                                    </div>
                                </div>
                            </div>
                            <div class="col_1_2 bbox pl40">
                                <h5>FTL Tolerance Settings</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Pickup <span class="fs85em">(minutes)</span></label>
                                        <eShip:CustomTextBox ID="txtFTLPickupTolerance" runat="server" CssClass="w100" MaxLength="4" />
                                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="txtFTLPickupTolerance" FilterType="Numbers" />
                                    </div>
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Delivery <span class="fs85em">(minutes)</span></label>
                                        <eShip:CustomTextBox ID="txtFTLDeliveryTolerance" runat="server" CssClass="w100" MaxLength="4" />
                                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" TargetControlID="txtFTLDeliveryTolerance" FilterType="Numbers" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">&nbsp;</label>
                                        <asp:CheckBox ID="chkCustomerToleranceEnabled" runat="server" CssClass="jQueryUniform" />
                                        <label>Enable FTL Tolerance</label>
                                    </div>
                                </div>

                                <h5 class="mt20">Operations Processing Settings</h5>
                                <div class="row">
                                    <ul class="twocol_list">
                                        <li>
                                            <asp:CheckBox ID="chkPurchaseOrderRequired" runat="server" CssClass="jQueryUniform" />
                                            <label>Purchase Order Required</label>
                                        </li>
                                        <li>
                                            <asp:CheckBox ID="chkValidatePurchaseOrder" runat="server" CssClass="jQueryUniform" />
                                            <label>Validate Purchase Order</label>
                                        </li>
                                        <li>
                                            <asp:CheckBox ID="chkNMFCRequired" runat="server" CssClass="jQueryUniform" />
                                            <label>NMFC Required</label>
                                        </li>
                                        <li>
                                            <asp:CheckBox ID="chkRateAndScheduleInDemo" runat="server" CssClass="jQueryUniform" />
                                            <label>Rate And Schedule In Demo</label>
                                        </li>
                                        <li>
                                            <asp:CheckBox ID="chkPurgeExpiredQuotes" runat="server" CssClass="jQueryUniform" />
                                            <label>Purge Expired Quotes</label>
                                        </li>
                                    </ul>
                                </div>

                                <div class="row mt20">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Invalid Purchase Order Message
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleInvalidPurchaseOrderMessage" MaxLength="200" TargetControlId="txtInvalidPurchaseOrderMessage" />
                                        </label>
                                        <eShip:CustomTextBox ID="txtInvalidPurchaseOrderMessage" runat="server" CssClass="w420 h150" TextMode="MultiLine" />
                                    </div>
                                </div>
                                <div class="row mt10">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Custom Vendor Selection Message
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleCustomVendorSelectionMessage" MaxLength="500" TargetControlId="txtCustomVendorSelectionMessage" />
                                        </label>
                                        <eShip:CustomTextBox ID="txtCustomVendorSelectionMessage" runat="server" TextMode="MultiLine" CssClass="w420 h150" />
                                    </div>
                                </div>

                                <h5 class="mt20">Finance Processing Settings</h5>
                                <div class="row">
                                    <ul class="twocol_list">
                                        <li>
                                            <asp:CheckBox ID="chkCarrierProNumber" runat="server" CssClass="jQueryUniform" />
                                            <label>Vendor/Carrier PRO Required</label>
                                        </li>
                                        <li>
                                            <asp:CheckBox ID="chkShipperReference" runat="server" CssClass="jQueryUniform" />
                                            <label>Shipper Reference Required</label>
                                        </li>
                                        <li>
                                            <asp:CheckBox ID="chkCustomerConrolAccountRequired" runat="server" CssClass="jQueryUniform" />
                                            <label>Customer Control Account Required</label>
                                        </li>
                                        <li>
                                            <asp:CheckBox ID="chkCanInvoiceNonDeliveredShipment" runat="server" CssClass="jQueryUniform" />
                                            <label>Can Invoice Non-Delivered Shipment</label>
                                        </li>
                                    </ul>
                                </div>
                                <div class="row mt20">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Audit Instructions
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleAuditInstructions" MaxLength="100" TargetControlId="txtAuditInstructions" />
                                        </label>
                                        <eShip:CustomTextBox runat="server" ID="txtAuditInstructions" TextMode="MultiLine" CssClass="w420 h150" />
                                    </div>
                                </div>
                                <h5 class="mt20">Payment Settings</h5>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Payment Gateway Key</label>
                                        <eShip:CustomTextBox runat="server" ID="txtPaymentGatewayKey" CssClass="w280 disabled" />
                                        <asp:Button runat="server" ID="btnCreateNewPaymentGatewayKey" OnClick="OnCreateNewPaymentGatewayClicked" Text="Create New Key" />
                                        <asp:Button runat="server" ID="btnDeletePaymentGatewayKey" OnClick="OnDeletePaymentGatewayKeyClicked" Text="Delete Key" Visible="False" />
                                    </div>
                                </div>
                                <div class="row pt10">
                                    <ul class="twocol_list">
                                        <li>
                                            <asp:CheckBox runat="server" ID="chkIsCashOnly" CssClass="jQueryUniform" />
                                            <label>Is Cash Only</label>
                                        </li>
                                        <li>
                                            <asp:CheckBox runat="server" ID="chkCanPayByCreditCard" CssClass="jQueryUniform" />
                                            <label>Can Pay By Credit Card</label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabLocations" HeaderText="Locations">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlLocations">
                        <div class="row mb10 bottom_shadow pb5">
                            <div class="fieldgroup right">
                                <asp:Button runat="server" ID="btnAddLocation" Text="Add Location" CssClass="right"
                                    CausesValidation="false" OnClick="OnAddLocationClicked" />

                            </div>
                        </div>

                        <asp:ListView ID="lstLocations" runat="server" OnItemDataBound="BindLocationLineItem"
                            ItemPlaceholderID="itemPlaceHolder">
                            <LayoutTemplate>
                                <table class="stripe">
                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                </table>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="bottom_shadow">
                                        <eShip:LocationListingInputControl ID="llicLocation" runat="server" LocationItemIndex='<%# Container.DataItemIndex %>' OnDeleteLocation="OnDeleteLocation" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabRequiredDocumentTags" HeaderText="Req'd Doc. Tags">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlRequiredDocumentTags" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlRequiredDocumentTags">
                                <div class="row mb20">
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddDocumentTag" Text="Add Document Tags" OnClick="OnAddDocumentTagClicked" />
                                        <asp:Button runat="server" ID="btnClearDocumentTags" Text="Clear Document Tags" OnClick="OnClearDocumentTagsClicked" />
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheRequiredDocumentTagsTable" TableId="requiredDocumentTagsTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table class="stripe" id="requiredDocumentTagsTable">
                                    <tr>
                                        <th style="width: 20%;">Code
                                        </th>
                                        <th style="width: 75%;">Description
                                        </th>
                                        <th style="width: 5%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstRequiredDocumentTags" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidDocumentTagId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <asp:Literal ID="litInvReqDocTagCode" Text='<%# Eval("Code") %>' runat="server" />
                                                </td>
                                                <td>
                                                    <asp:Literal ID="litInvReqDocTagDescription" Text='<%# Eval("Description") %>' runat="server" />
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnReqInvDocTagDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        OnClick="OnDeleteInvReqDocTagClicked" CausesValidation="false" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnAddDocumentTag" />
                        </Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabAssociatedUsers" HeaderText="Assoc. Users">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlAssociatedUsers" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlAssociatedUsers">
                                <div class="row mb20">
                                    <div class="fieldgroup">
                                        <label class="upper blue fs120em">
                                            Customer Service Representative Users
                                            <asp:Literal runat="server" ID="litCustomerServiceRepCount" />
                                        </label>
                                    </div>
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddServiceRep" Text="Add Service Reps"
                                            CausesValidation="False" OnClick="OnAddServiceRepClicked" />
                                        <asp:Button runat="server" ID="btnClearServiceReps" Text="Clear Service Reps"
                                            CausesValidation="False" OnClick="OnClearServiceRepsClicked" />
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheCustomerServiceRepTable" TableId="customerServiceRepTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table class="stripe" id="customerServiceRepTable">
                                    <tr>
                                        <th style="width: 15%;">First Name
                                        </th>
                                        <th style="width: 15%;">Last Name
                                        </th>
                                        <th style="width: 20%;">Phone
                                        </th>
                                        <th style="width: 20%;">Fax
                                        </th>
                                        <th style="width: 25%;">Email
                                        </th>
                                        <th style="width: 5%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstCSR" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidUserId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <asp:Literal ID="litSalesRepFirstName" Text='<%# Eval("FirstName") %>' runat="server" />
                                                    <%# ActiveUser.HasAccessTo(ViewCode.User) 
                                                        ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To User Record'><img src='{3}' alt='' width='16' align='absmiddle'/></a>", 
                                                            ResolveUrl(UserView.PageAddress),
                                                            WebApplicationConstants.TransferNumber, 
                                                            Eval("Id").GetString().UrlTextEncrypt(),
                                                            ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                        : string.Empty  %>
                                                </td>
                                                <td>
                                                    <asp:Literal ID="litSalesRepLastName" Text='<%# Eval("LastName") %>' runat="server" />
                                                </td>
                                                <td>
                                                    <asp:Literal ID="litSalesRepPhone" Text='<%# Eval("Phone") %>' runat="server" />
                                                </td>
                                                <td>
                                                    <asp:Literal ID="litSalesRepFax" Text='<%# Eval("Fax") %>' runat="server" />
                                                </td>
                                                <td>
                                                    <a href='mailto:<%# Eval("Email") %>' class="blue">
                                                        <asp:Literal ID="litSalesRepEmail" Text='<%# Eval("Email") %>' runat="server" /></a>
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnLocationDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        OnClick="OnDeleteCsrClicked" CausesValidation="false" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="pnlNonEmployeeUsers">
                                <div class="row mb20 pt40">
                                    <div class="fieldgroup">
                                        <label class="upper blue fs120em">
                                            Non-Employee Users
                                            <asp:Literal runat="server" ID="litNonEmployeeUsersCount" />
                                        </label>
                                    </div>
                                </div>
                                <table class="stripe">
                                    <tr>
                                        <th style="width: 25%;">Username
                                        </th>
                                        <th style="width: 25%;">User First Name
                                        </th>
                                        <th style="width: 25%;">User Last Name
                                        </th>
                                        <th style="width: 25%;">User Email
                                        </th>
                                    </tr>
                                    <asp:Literal runat="server" ID="litUsers" />
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnAddServiceRep" />
                        </Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabCustomFields" HeaderText="Cust. Settings">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlCustomFields" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlCustomFields">
                                <div class="row mb20">
	                                 <div class="fieldgroup">
                                        <label class="upper blue fs120em">
                                            Custom Fields
                                    <asp:Literal runat="server" ID="litCustomFieldsCount" />
                                        </label>
                                    </div>
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddCustomField" Text="Add Custom Field"
                                            CausesValidation="False" OnClick="OnAddCustomFieldClicked" />
                                        <asp:Button runat="server" ID="btnClearCustomFields" Text="Clear Custom Fields"
                                            CausesValidation="False" OnClick="OnClearCustomFieldsClicked" />
                                    </div>
                                </div>

                                <script type="text/javascript">
                                    function EnforceMutuallyExclusiveCustomerCustomFieldCheckBox(control) {
                                        $(control).parentsUntil("tbody").find("input[id*='_chkDisplayOn'][type='checkbox']").not($(control))
                                            .each(function () {
                                                jsHelper.UnCheckBox($(this).attr("id"));
                                                SetAltUniformCheckBoxClickedStatus($(this));
                                            });
                                    }

                                </script>

                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheCustomFieldsTable" TableId="customFieldsTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table class="stripe" id="customFieldsTable">
                                    <tr>
                                        <th style="width: 35%;">Name</th>
                                        <th style="width: 15%;" class="text-center">Display On Origin</th>
                                        <th style="width: 15%;" class="text-center">Display on Destination</th>
                                        <th style="width: 15%;" class="text-center">Show on Documents</th>
                                        <th style="width: 15%;" class="text-center">Required</th>
                                        <th style="width: 5%;" class="text-center">Action</th>
                                    </tr>
                                    <asp:ListView ID="lstCustomFields" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidCustomFieldId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <eShip:CustomHiddenField runat="server" ID="hidCustomFieldIndex" Value='<%# Container.DataItemIndex %>' />
                                                    <eShip:CustomTextBox ID="txtName" runat="server" Text='<%# Eval("Name") %>' CssClass="w360" MaxLength="50" />
                                                </td>
                                                <td class="text-center">
                                                    <eShip:AltUniformCheckBox ID="chkDisplayOnOrigin" runat="server" Checked='<%# Eval("DisplayOnOrigin") %>' OnClientSideClicked="EnforceMutuallyExclusiveCustomerCustomFieldCheckBox(this);" />
                                                </td>
                                                <td class="text-center">
                                                    <eShip:AltUniformCheckBox ID="chkDisplayOnDestination" runat="server" Checked='<%# Eval("DisplayOnDestination") %>' OnClientSideClicked="EnforceMutuallyExclusiveCustomerCustomFieldCheckBox(this);" />
                                                </td>
                                                <td class="text-center">
                                                    <eShip:AltUniformCheckBox ID="chkShowOnDocuments" runat="server" Checked='<%# Eval("ShowOnDocuments") %>' />
                                                </td>
                                                <td class="text-center">
                                                    <eShip:AltUniformCheckBox ID="chkRequired" runat="server" Checked='<%# Eval("Required") %>' />
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        CausesValidation="false" OnClick="OnCustomFieldDeleteClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
	                <br/>
					   <hr class="dark" />
					  <asp:UpdatePanel runat="server" ID="upPnlCustChargeCodeMap" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlCustChargeCodeMap">
                                <div class="row mb20">
	                                <div class="fieldgroup">
                                        <label class="upper blue fs120em">
                                            Charge Code Map
                                    <asp:Literal runat="server" ID="litChargeCodeMapCount" />
                                        </label>
                                    </div>
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddChargeCodeMap" Text="Add Charge Code Map"
                                            CausesValidation="False" OnClick="OnAddChargeCodeMap" />
                                        <asp:Button runat="server" ID="btnClearChargeCodeMap" Text="Clear Charge Code Map"
                                            CausesValidation="False" OnClick="OnClearCustChargeCodeMap" />
                                    </div>
                                </div>
								

                                <eShip:TableFreezeHeaderExtender runat="server" ID="TableFreezeHeaderExtender1" TableId="custChargeCodeTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table class="stripe" id="custChargeCodeTable">
                                    <tr>
                                        <th style="width: 25%;">Charge Code</th>
                                        <th style="width: 25%;">External Chareg Code</th>
                                        <th style="width: 45%;">External Description</th>
                                        <th style="width: 5%;" class="text-center">Action</th>
                                    </tr>
                                    <asp:ListView ID="lstCustChargeCodeMap" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                 <td>
                                                    <eShip:CustomHiddenField ID="hidChargeCodeIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <eShip:CustomHiddenField ID="hidCustChargeCodeMapId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <asp:CachedObjectDropDownList runat="server" Type="ChargeCodes" EnableChooseOne="True" ID="ddlChargeCode" DataValueField="Value" DataTextField="Text"  DefaultValue="0"  CssClass="w180" SelectedValue='<%# Eval("ChargeCodeId") %>'/>
                                                </td>
                                                <td>
													  <eShip:CustomTextBox ID="txtExternalChargeCode" runat="server" Text='<%# Eval("ExternalCode") %>' CssClass="w180" />
                                                </td>
                                                <td>
                                                      <eShip:CustomTextBox ID="txtExternalChargeCodeDescription" runat="server" Text='<%# Eval("ExternalDescription") %>' CssClass="w280" />
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        CausesValidation="false" OnClick="OnCustomerChargeCodeMapDeleteClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel ID="tabDocuments" runat="server" HeaderText="Documents">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlDocuments">

                        <div class="row mb20">
                            <div class="fieldgroup right">
                                <asp:Button runat="server" ID="btnAddDocument" Text="Add Document"
                                    CausesValidation="false" OnClick="OnAddDocumentClicked" />
                                <asp:Button runat="server" ID="btnClearDocuments" Text="Clear Documents"
                                    CausesValidation="false" OnClick="OnClearDocumentsClicked" />
                            </div>
                        </div>

                        <script type="text/javascript">
                            $(document).ready(function () {
                                $("a[id$='lnkCustomerDocumentLocationPath']").each(function () {
                                    jsHelper.SetDoPostBackJsLink($(this).prop('id'));
                                });
                            });
                        </script>

                        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheDocumentsTable" TableId="documentsTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                        <table class="stripe" id="documentsTable">
                            <tr>
                                <th style="width: 5%;" class="text-center">Internal
                                </th>
                                <th style="width: 20%;">Name
                                </th>
                                <th style="width: 25%;">Description
                                </th>
                                <th style="width: 22%;">Document Tag
                                </th>
                                <th style="width: 20%;">File
                                </th>
                                <th style="width: 8%;" class="text-center">Action
                                </th>
                            </tr>

                            <asp:ListView ID="lstDocuments" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                <LayoutTemplate>
                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td class="text-center top">
                                            <eShip:CustomHiddenField runat="server" ID="hidIsInternal" Value='<%# Eval("IsInternal") %>' />
                                            <%# Eval("IsInternal").ToBoolean() ? WebApplicationConstants.HtmlCheck : string.Empty %>
                                        </td>
                                        <td class="top">
                                            <eShip:CustomHiddenField ID="hidDocumentIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                            <eShip:CustomHiddenField ID="hidDocumentId" runat="server" Value='<%# Eval("Id") %>' />
                                            <asp:Literal ID="litDocumentName" runat="server" Text='<%# Eval("Name") %>' />
                                        </td>
                                        <td class="top">
                                            <asp:Literal ID="litDocumentDescription" runat="server" Text='<%# Eval("Description") %>' />
                                        </td>
                                        <td class="top">
                                            <eShip:CustomHiddenField ID="hidDocumentTagId" runat="server" Value='<%# Eval("DocumentTagId") %>' />
                                            <asp:Literal ID="litDocumentTag" runat="server" Text='<%# new DocumentTag(Eval("DocumentTagId").ToLong()).Description %>' />
                                        </td>
                                        <td class="top">
                                            <eShip:CustomHiddenField ID="hidLocationPath" runat="server" Value='<%# Eval("LocationPath") %>' />
                                            <asp:LinkButton runat="server" ID="lnkCustomerDocumentLocationPath" Text='<%# GetLocationFileName(Eval("LocationPath")) %>'
                                                OnClick="OnLocationPathClicked" CausesValidation="false" CssClass="blue" />
                                        </td>
                                        <td class="text-center top">
                                            <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png"
                                                CausesValidation="false" OnClick="OnEditDocumentClicked" Enabled='<%# Access.Modify %>' />
                                            <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                CausesValidation="false" OnClick="OnDeleteDocumentClicked" Enabled='<%# Access.Modify %>' />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:ListView>

                        </table>

                        <asp:Panel runat="server" ID="pnlEditDocument" Visible="False" CssClass="popup popupControlOverW500">
                            <div class="popheader mb10">
                                <h4>Add/Modify Document</h4>
                                <asp:ImageButton ID="ImageButton5" ImageUrl="~/images/icons2/close.png" CssClass="close"
                                    CausesValidation="false" OnClick="OnCloseEditDocumentClicked" runat="server" />
                            </div>
                            <table class="poptable">
                                <tr>
                                    <td class="text-right">
                                        <label class="upper">Name:</label>
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtDocumentName" CssClass="w200" MaxLength="50" />
                                        <asp:CheckBox runat="server" ID="chkDocumentIsInternal" CssClass="jQueryUniform ml10" />
                                        <label>Is Internal</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right">
                                        <label class="upper">Document Tag:</label>
                                    </td>
                                    <td>
                                        <eShip:CachedObjectDropDownList Type="DocumentTag" ID="ddlDocumentTag" runat="server" CssClass="w300" EnableChooseOne="True"
                                            DefaultValue="0"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right top">
                                        <label class="upper">Description:</label>
                                        <br />
                                        <label class="lhInherit">
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDocumentDescription" MaxLength="500" TargetControlId="txtDocumentDescription" />
                                        </label>
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtDocumentDescription" CssClass="w300 h150" TextMode="MultiLine" />
                                    </td>
                                </tr>

                                <tr>
                                    <td class="text-right">
                                        <label class="upper">Location Path:</label>
                                    </td>
                                    <td>
                                        <eShip:CustomHiddenField ID="hidLocationPath" runat="server" />
                                        <asp:FileUpload runat="server" ID="fupLocationPath" CssClass="jQueryUniform" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right">
                                        <label class="upper">Current File:</label>
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox ID="txtLocationPath" runat="server" ReadOnly="True" CssClass="w300 disabled" />
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Button ID="btnEditDocumentDone" Text="Done" OnClick="OnEditDocumentDoneClicked" runat="server" CausesValidation="false" />
                                        <asp:Button ID="btnCloseEditDocument" Text="Cancel" OnClick="OnCloseEditDocumentClicked" runat="server" CausesValidation="false" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabPaymentProfiles" HeaderText="Payment Prof." Visible="False">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlPaymentProfiles">
                        <eShip:PaymentProfileManagerControl runat="server" ID="ppmPaymentProfileManager" OnProcessingMessages="OnPaymentProfileManagerProcessingMessages"/>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabAuditLog" HeaderText="Audit Log">
                <ContentTemplate>
                    <eShip:AuditLogControl runat="server" ID="auditLogs" />
                </ContentTemplate>
            </ajax:TabPanel>
        </ajax:TabContainer>

        <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
        <eShip:CustomHiddenField runat="server" ID="hidCustomFieldEditItemIndex" />
        <eShip:CustomHiddenField runat="server" ID="hidChargeCodeMapItemIndex" />
        <eShip:CustomHiddenField runat="server" ID="hidEditingIndex" />
        <eShip:CustomHiddenField runat="server" ID="hidFlag" />
        <eShip:CustomHiddenField runat="server" ID="hidFilesToDelete" />
        <eShip:CustomHiddenField runat="server" ID="hidCommunicationId" />
        <eShip:CustomHiddenField runat="server" ID="hidRatingId" />
    </div>
</asp:Content>
