﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="CommissionsToBePaidView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.CommissionsToBePaidView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Operations" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>
<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowSave="True" ShowImport="True" OnSave="OnToolbarSave" OnImport="OnToolbarImport" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Commissions To Be Paid<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>

        <hr class="dark mb10" />

        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="fieldgroup right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="CommissionsToBePaid" ShowAutoRefresh="False"
                            OnProcessingError="OnSearchProfilesProcessingError" OnSearchProfileSelected="OnSearchProfilesProfileSelected" />
                    </div>
                </div>
            </div>

            <hr class="dark mb10" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                        OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>

            <hr class="fat" />

            <div class="rowgroup">
                <div class="row mb10">
                    <div class="fieldgroup">
                        <asp:Button ID="btnSearch" CssClass="mr10" runat="server" Text="SEARCH" OnClick="OnSearchClicked" CausesValidation="False" />
                        <asp:Button ID="btnExport" runat="server" Text="EXPORT" OnClick="OnExportClicked" CausesValidation="False" />
                    </div>
                    <div class="fieldgroup right">
                        <asp:CheckBox ID="chkFilterPaid" runat="server" CssClass="jQueryUniform" />
                        <label>Commissions With No Payments Only</label>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <script type="text/javascript">
            function SetCheckAllCommissionsToBePaid(control) {
                $("#commissionListingTable input:checkbox[id*='chkSelected']").each(function () {
                    this.checked = control.checked && this.disabled == false;
                    SetAltUniformCheckBoxClickedStatus(this);
                });

                // necessary to keep frozen header and header checkbox states the same
                $('[id*=' + control.id + ']').each(function () {
                    this.checked = control.checked;
                    SetAltUniformCheckBoxClickedStatus(this);
                });
            }
        </script>
        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheCommissionListingTable" TableId="commissionListingTable" HeaderZIndex="2" />
        <div class="rowgroup">
            <table id="commissionListingTable" class="line2 pl2">
                <tr>
                    <th style="width: 5%;">
                        <eShip:AltUniformCheckBox ID="chkSelectAllRecords" runat="server" OnClientSideClicked="javascript:SetCheckAllCommissionsToBePaid(this);" />
                    </th>
                    <th style="width: 10%">
                        <abbr title="Shipment Number">Shipment #</abbr>
                    </th>
                    <th style="width: 21%">Sales Representative
                    </th>
                    <th style="width: 19%">Sales Representative Company
                    </th>
                    <th style="width: 7%" class="text-center">All Invoices Paid
                    </th>
                    <th style="width: 10%">Payment $
                    </th>
                    <th style="width: 11%">
                        <abbr title="Additional Entity">Add'l Entity</abbr>
                        Payment $
                    </th>
                    <th style="width: 5%" class="text-center">Reversal
                    </th>
                    <th style="width: 12%">Payment Date
                    </th>
                </tr>
                <asp:ListView runat="server" ID="lstCommissions" ItemPlaceholderID="itemPlaceHolder">
                    <LayoutTemplate>
                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="top" rowspan="2">
                                <eShip:AltUniformCheckBox runat="server" ID="chkSelected" OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'commissionListingTable', 'chkSelectAllRecords');" />
                            </td>
                            <td>
                                <asp:Literal runat="server" ID="litRefNumber" Text='<%# Eval("ReferenceNumber") %>'/>
                                        <%# ActiveUser.HasAccessTo(ViewCode.Shipment)
                                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Shipment Record'><img src='{3}' alt='' width='16' align='absmiddle'/></a>", 
                                                    ResolveUrl(ShipmentAuditView.PageAddress),
                                                    WebApplicationConstants.ShipmentAuditId, 
                                                    Eval("ReferenceId").GetString().UrlTextEncrypt(),
                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                : string.Empty  %>
                            </td>
                            <td>
                                <eShip:CustomHiddenField ID="hidSalesRepresentativeId" runat="server" Value='<%# Eval("SalesRepId")%> ' />
                                <%# string.Format("{0} - {1}", Eval("SalesRepNumber"), Eval("SalesRepName")) %>
                                <%# ActiveUser.HasAccessTo(ViewCode.SalesRepresentative)
                                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Sales Representative Record'><img src='{3}' alt='' width='16' align='absmiddle'/></a>", 
                                                    ResolveUrl(SalesRepresentativeView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    Eval("SalesRepId").GetString().UrlTextEncrypt(),
                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                : string.Empty  %>
                            </td>
                            <td>
                                <%# Eval("SalesRepCompanyName") %>
                            </td>
                            <td class="text-center">
                                <%# Eval("InvoicesPaid").ToBoolean() ? WebApplicationConstants.Yes : WebApplicationConstants.No %>
                            </td>
                            <td>
                                <eShip:CustomTextBox runat="server" ID="txtAmountPaid" CssClass="w80" />
                                <ajax:FilteredTextBoxExtender ID="filterAmountPaid" runat="server" TargetControlID="txtAmountPaid"
                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                <asp:RangeValidator runat="server" ControlToValidate="txtAmountPaid"
                                    MinimumValue='<%# Eval("SalesRepCommCeiling").ToDecimal() < Eval("SalesRepCommFloor").ToDecimal() ? Eval("SalesRepCommCeiling").ToDecimal() : Eval("SalesRepCommFloor").ToDecimal()  %>'
                                    MaximumValue='<%# Eval("SalesRepCommCeiling").ToDecimal() %>'
                                    Type="Double" Text="*" ErrorMessage="Amount must be between commission minimum and maximum values"
                                    Display="Dynamic" />
                            </td>
                            <td>
                                <eShip:CustomTextBox runat="server" ID="txtAdditionalEntityAmountPaid" CssClass="w80" />
                                <ajax:FilteredTextBoxExtender runat="server" TargetControlID="txtAdditionalEntityAmountPaid" FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                <asp:RangeValidator runat="server" ControlToValidate="txtAdditionalEntityAmountPaid" ErrorMessage="Amount must be between commission minimum and maximum values"
                                    MinimumValue='<%# Eval("SalesRepCommCeiling").ToDecimal() < Eval("SalesRepCommFloor").ToDecimal() ? Eval("SalesRepCommCeiling").ToDecimal() : Eval("SalesRepCommFloor").ToDecimal()  %>'
                                    MaximumValue='<%# Eval("SalesRepCommCeiling").ToDecimal() %>'
                                    Type="Double" Text="*" Display="Dynamic" />
                            </td>
                            <td class="text-center">
                                <eShip:AltUniformCheckBox runat="server" ID="chkReversal" />
                            </td>
                            <td class="AjaxCalendarTable">
                                <eShip:CustomTextBox runat="server" ID="txtPaymentDate" CssClass="w100" Type="Date" placeholder="99/99/9999" Text='<%# DateTime.Now.ToShortDateString() %>' />
                            </td>
                        </tr>
                        <tr class="f9">
                            <td class="p_m0 forceLeftBorder" colspan="8">
                                <div class="col_1_3 no-right-border pb10">
                                    <div class="row">
                                        <div class="fieldgroup">
                                            <label class="text-right blue  w180">Rep Comm. %:</label>
                                            <label class="pl5"><%# Eval("SalesRepCommPercent", "{0:n2}") %></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="fieldgroup">
                                            <label class="text-right blue  w180">Rep Comm. Max $:</label>
                                            <label class="pl5"><%# Eval("SalesRepCommCeiling", "{0:n2}") %></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="fieldgroup">
                                            <label class="text-right blue  w180">Rep Comm. Min $:</label>
                                            <label class="pl5"><%# Eval("SalesRepCommFloor", "{0:n2}") %></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="fieldgroup">
                                            <label class="text-right blue  w180">Add'l Entity Comm. %:</label>
                                            <label class="pl5"><%# Eval("SalesRepAddlEntityCommPercent", "{0:n2}") %></label>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="row pt26">
                                        <div class="fieldgroup">
                                            <label class="text-right blue  w180">Net. Add'l Ent. Comm. Paid $:</label>
                                            <label class="pl5"><%# Eval("NetAdditionalEntityCommPayments", "{0:n2}") %></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="fieldgroup">
                                            <label class="text-right blue  w180">Net. Comm. Paid $:</label>
                                            <label class="pl5"><%# Eval("NetCommPayments", "{0:n2}") %></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col_1_3 no-right-border pb10">
                                    <div class="row">
                                        <div class="fieldgroup mr20">
                                            <label class="text-right blue  w180">Est. Rev $:</label>
                                            <label class="pl5"><%# Eval("EstimatedRevenue", "{0:n2}")%></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="fieldgroup">
                                            <label class="text-right blue  w180">Est. Cost $:</label>
                                            <label class="pl5"><%# Eval("EstimatedCost", "{0:n2}") %></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="fieldgroup">
                                            <label class="text-right blue  w180">Est. Rep Comm. $:</label>
                                            <label class="pl5"><%# Eval("EstSalesRepComm", "{0:n2}") %></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="fieldgroup">
                                            <label class="text-right blue  w180">Est. Add'l Ent. Comm. $:</label>
                                            <label class="pl5"><%# Eval("EstSalesRepAddlEntityComm", "{0:n2}") %></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="fieldgroup">
                                            <label class="text-right blue  w180">Est. Overall Comm. $:</label>
                                            <label class="pl5"><%# Eval("EstOveralComm", "{0:n2}") %></label>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="fieldgroup">
                                            <label class="text-right blue  w180">Add'l Ent. Comm. Paid $:</label>
                                            <label class="pl5"><%# Eval("AdditionalEntityTotalCommission", "{0:n2}") %></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="fieldgroup">
                                            <label class="text-right blue  w180">Comm. Paid $:</label>
                                            <label class="pl5"><%# Eval("TotalCommission", "{0:n2}") %></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col_1_3 no-right-border pb10">
                                    <div class="row">
                                        <div class="fieldgroup mr20">
                                            <label class="text-right blue  w180">Act. Rev $:</label>
                                            <label class="pl5"><%# Eval("NetActualRevenue", "{0:n2}")%></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="fieldgroup">
                                            <label class="text-right blue  w180">Act. Cost $:</label>
                                            <label class="pl5"><%# Eval("NetActualCost", "{0:n2}") %></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="fieldgroup">
                                            <label class="text-right blue  w180">Act. Rep Comm. $:</label>
                                            <label class="pl5"><%# Eval("ActSalesRepComm", "{0:n2}") %></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="fieldgroup">
                                            <label class="text-right blue  w180">Act. Add'l Ent. Comm. $:</label>
                                            <label class="pl5"><%# Eval("ActSalesRepAddlEntityComm", "{0:n2}") %></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="fieldgroup">
                                            <label class="text-right blue  w180">Act. Overall Comm. $:</label>
                                            <label class="pl5"><%# Eval("ActOveralComm", "{0:n2}") %></label>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="fieldgroup">
                                            <label class="text-right blue  w180">Add'l Ent. Comm. Reversed $:</label>
                                            <label class="pl5">(<%# Eval("AdditionalEntityTotalReversals", "{0:n2}") %>)</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="fieldgroup">
                                            <label class="text-right blue  w180">Comm. Reversed $:</label>
                                            <label class="pl5">(<%# Eval("TotalReversals", "{0:n2}") %>)</label>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        function ClearHidFlag() {
            jsHelper.SetEmpty('<%= hidFlag.ClientID %>');
        }
    </script>
    <asp:Panel runat="server" Visible="False" ID="pnlImportComplete">
        <div class="popup popupControlOverW500">
            <div class="popheader">
                <h4>Messages</h4>
                 <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                        CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
            </div>
            <div class="row pb10">
                <table class="poptable">
                    <tr>
                        <td>
                            Import is complete. Click the following link if you would like to download the results of the import.
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">
                            <asp:LinkButton runat="server" ID="lbtnDownloadImportResultsFile" OnClick="OnDownloadImportResultsFile" Text="Download Import Results" CssClass="text-center blue"/>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
        Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" />
    <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
    <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
        OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
</asp:Content>
