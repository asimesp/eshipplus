﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="DocumentPackDownloadView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.DocumentPackDownloadView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Operations" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor.Searches.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="False" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Document Pack Download<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" Text="" /></small>
            </h3>
        </div>

        <hr class="dark mb10" />

        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row mb10 pb10 bottom_shadow">
                    <script type="text/javascript">
                        $(function () {
                            $(jsHelper.AddHashTag('divRecordTypes input:checkbox')).each(function () {
                                $(this).click(function () {
                                    if (jsHelper.IsChecked($(this).attr('id'))) {
                                        $(jsHelper.AddHashTag('divRecordTypes input:checkbox')).not(jsHelper.AddHashTag($(this).attr('id'))).each(function () {
                                            jsHelper.UnCheckBox($(this).attr('id'));
                                        });
                                    } else {
                                        jsHelper.CheckBox($(this).attr('id'));
                                    }
                                    $.uniform.update();
                                });
                            });
                        });
                    </script>
                    <div class="fieldgroup" id="divRecordTypes">
                        <label class="upper blue mr10">Record Type: </label>
                        <asp:CheckBox runat="server" ID="chkShipmentRecordType" CssClass="jQueryUniform" AutoPostBack="True" Checked="True" OnCheckedChanged="OnRecordTypeCheckedChanged" />
                        <label class="blue mr10">Shipments</label>
                        <asp:CheckBox runat="server" ID="chkInvoiceRecordType" CssClass="jQueryUniform" AutoPostBack="True" OnCheckedChanged="OnRecordTypeCheckedChanged" />
                        <label class="blue">Invoices</label>
                    </div>
                </div>
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="CustomerControlAccounts" ShowAutoRefresh="False"
                            OnProcessingError="OnSearchProfilesProcessingError" OnSearchProfileSelected="OnSearchProfilesProfileSelected" />
                    </div>
                </div>
                <hr class="dark mb10" />
                <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                    OnItemDataBound="OnParameterItemDataBound">
                    <LayoutTemplate>
                        <table class="mb10">
                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                            OnRemoveParameter="OnParameterRemove" />
                    </ItemTemplate>
                </asp:ListView>
            </div>

            <hr class="dark mb10" />
            <div class="rowgroup">
                <div class="col_1_3 bbox no-right-border" id="divDownloadDocumentTypeChoices">
                    <h5>Document Type</h5>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            // enforce mutual exclusion on checkboxes in divDownloadDocumentTypeChoices
                            $(jsHelper.AddHashTag('divDownloadDocumentTypeChoices input:checkbox')).click(function () {
                                if (jsHelper.IsChecked($(this).attr('id'))) {
                                    $(jsHelper.AddHashTag('divDownloadDocumentTypeChoices input:checkbox')).not(jsHelper.AddHashTag($(this).attr('id'))).each(function () {
                                        jsHelper.UnCheckBox($(this).attr('id'));
                                    });
                                    $.uniform.update();
                                }
                            });
                        });
                    </script>
                    <div class="row">
                        <div class="fieldgroup">
                            <asp:CheckBox runat="server" ID="chkDownloadAll" Text=" " ToolTip="download all shipment documents, bill of lading and/or invoice" CssClass="jQueryUniform" />
                            <label>All Documents</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="fieldgroup pt10">
                            <asp:CheckBox runat="server" ID="chkDownloadNonInternal" Checked="True" ToolTip="download all shipment documents not marked internal, bill of lading and/or invoice" CssClass="jQueryUniform" />
                            <label>Non-Internal Documents</label>
                        </div>
                    </div>
                    <asp:Panel ID="pnlInvoiceOnly" runat="server">
                        <div class="row">
                            <div class="fieldgroup pt10">
                                <asp:CheckBox runat="server" ID="chkDownloadInvoiceOnly" ToolTip="download invoice documents only" CssClass="jQueryUniform" />
                                <label>Invoices Only</label>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <div class="col_2_3 pl46 bbox vlinedarkleft">
                    <h5>Document Tags To Include</h5>
                    <div class="row">
                        <ul class="twocol_list chk_list">
                            <asp:Repeater runat="server" ID="rptDocTagsToDownload">
                                <ItemTemplate>
                                    <li>
                                        <asp:CheckBox runat="server" ID="chkSelected" CssClass="jQueryUniform" Checked='<%# Eval("Selected") %>' />
                                        <eShip:CustomHiddenField runat="server" ID="hidDocumentTagId" Value='<%# Eval("Value") %>' />
                                        <label>
                                            <asp:Literal ID="litDocumentTagName" runat="server" Text='<%# Eval("Text") %>'></asp:Literal></label>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </div>
                </div>
            </div>
            <hr class="fat" />

            <div class="rowgroup">
                <div class="row mb10">
                    <asp:Button ID="btnSearch" runat="server" Text="SEARCH" OnClick="OnSearchClicked" CausesValidation="true" CssClass="mr10" />
                    <asp:Button ID="btnDownload" runat="server" Text="DOWNLOAD" OnClick="OnDownloadClicked" CausesValidation="true" />
                </div>
            </div>
        </asp:Panel>
        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheDocumentPackDownloadResultsTable" TableId="documentPackDownloadResultsTable" HeaderZIndex="2" />
        <table class="line2 pl2" id="documentPackDownloadResultsTable">
            <tr>
                <th style="width: 5%;" class="text-center">
                    <eShip:AltUniformCheckBox ID="chkSelectAllRecords" runat="server" OnClientSideClicked="javascript:SetSelectedCheckBoxesStatus(this, 'documentPackDownloadResultsTable');" />
                </th>
                <th style="width: 20%;">Invoice / Shipment Number
                </th>
                <th style="width: 25%;" class="text-center">Invoice Post / Shipment Date
                </th>
                <th style="width: 50%;">Customer
                </th>
            </tr>
            <asp:ListView ID="lstDocumentPackEntities" runat="server" ItemPlaceholderID="itemPlaceHolder">
                <LayoutTemplate>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </LayoutTemplate>
                <ItemTemplate>
                    <tr>
                        <td class="text-center">
                            <eShip:CustomHiddenField ID="hidEntityId" runat="server" Value='<%# Eval("Id") %>' />
                            <eShip:CustomHiddenField ID="hidEntityType" runat="server" Value='<%# Eval("Type") %>' />
                            <eShip:AltUniformCheckBox runat="server" ID="chkSelected" OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'documentPackDownloadResultsTable', 'chkSelectAllRecords');" />
                        </td>
                        <td>
                            <%# Eval("Number") %> 
                            <%# ActiveUser.HasAccessTo(ViewCode.Shipment) && chkShipmentRecordType.Checked
                                                        ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Shipment Record'><img src={3} width='16' class='middle'/></a>", 
                                                            ResolveUrl(ShipmentView.PageAddress),
                                                            WebApplicationConstants.TransferNumber,
                                                            Eval("Number"),
                                                            ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                        : string.Empty %>
                            
                            <%# ActiveUser.HasAccessTo(ViewCode.Invoice) && chkInvoiceRecordType.Checked
                                                        ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Invoice Record'><img src={3} width='16' class='middle'/></a>", 
                                                            ResolveUrl(InvoiceView.PageAddress),
                                                            WebApplicationConstants.TransferNumber,
                                                            Eval("Number"),
                                                            ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                        : string.Empty %>
                        </td>
                        <td class="text-center">
                            <%# Eval("Date").FormattedShortDate() %>
                        </td>
                        <td>
                            <%# string.Format("{0} - {1}", Eval("CustomerNumber"), Eval("CustomerName"))%>
                            <%# ActiveUser.HasAccessTo(ViewCode.Customer)
                                                        ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Customer Record'><img src={3} width='16' class='middle'/></a>", 
                                                            ResolveUrl(CustomerView.PageAddress),
                                                            WebApplicationConstants.TransferNumber,
                                                            new CustomerSearch().FetchCustomerByNumber(Eval("CustomerNumber").ToString(), ActiveUser.TenantId).Id.GetString().UrlTextEncrypt(),
                                                            ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                        : string.Empty %> 
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </table>
    </div>
    <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
</asp:Content>
