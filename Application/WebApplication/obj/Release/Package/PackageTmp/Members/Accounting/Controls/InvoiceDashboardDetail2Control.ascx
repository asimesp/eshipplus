﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InvoiceDashboardDetail2Control.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls.InvoiceDashboardDetail2Control" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>
<eShip:CustomHiddenField ID="hidItemIndex" runat="server" />
<eShip:CustomHiddenField ID="hidInvoiceId" runat="server" />
<tr>
    <td rowspan="2" class="top" <%# EnableMultiSelect ? string.Empty : "style='display:none;'" %>>
         <eShip:AltUniformCheckBox runat="server" ID="chkSelection" Visible='<%# EnableMultiSelect %>' />
    </td>
    <td rowspan="2" class="text-left top no-left-border">
        <asp:Literal runat="server" ID="litInvoiceNumber" />
        <asp:ImageButton runat="server" Id="ibtnInvoiceNumber" OnClick="OnInvoiceNumberClicked" CausesValidation="False" ImageUrl="~/images/icons2/downloadDocument.png" Width="14"/>
    </td>
    <td>
        <asp:Literal runat="server" ID="litInvoiceDate" /></td>
    <td>
        <asp:Literal runat="server" ID="litPosted" /></td>
    <td>
        <asp:Literal runat="server" ID="litDue" /></td>
    <td class="text-right">
        <asp:Literal runat="server" ID="litAmountDue" /></td>
    <td class="text-right">
        <asp:Literal runat="server" ID="litPaidAmount" /></td>
    <td>
        <asp:Literal runat="server" ID="litUser" />
        <asp:HyperLink ID="hypUser" Target="_blank" runat="server" Visible="False">
                <img src='<%= ResolveUrl("~/images/icons2/arrow_newTab.png") %>' alt="" align="absmiddle" width="16" title="Go To User Record"/>
        </asp:HyperLink>
    </td>
    <td>
        <asp:Literal runat="server" ID="litType" /></td>
     <td rowspan="2" class="top text-center">
         <asp:HyperLink runat="server" ID="hypInvoiceView" Target="_blank" ToolTip="Go To Invoice Record">
            <img src='<%# ResolveUrl("~/images/icons2/arrow_newTab.png") %>' alt="" class='middle'/>
        </asp:HyperLink>
     </td>
</tr>
<tr class="f9">
    <td colspan="7" class="forceLeftBorder">
        <div class="rowgroup">
            <div class="col_1_3 no-right-border">
                <div class="row">
                    <label class="wlabel blue">
                        Billing
                        
                    </label>
                    <label>
                        <asp:Literal runat="server" ID="litCustomer" />
                        <asp:HyperLink ID="hypCustomer" runat="server" Target="_blank" Visible='<%# ActiveUser.HasAccessTo(ViewCode.Customer) %>' ToolTip="Go To Customer Record">
                            <img src='<%# ResolveUrl("~/images/icons2/arrow_newTab.png") %>' alt="" width="16" class="middle"/>
                        </asp:HyperLink>
                    </label>
                </div>
                <div class="row">
                    <label><asp:Literal runat="server" ID="litAddress" /></label>
                </div>
                <div class="row">
                    <label><asp:Literal runat="server" ID="litInstructions" /></label>
                </div>
            </div>
            <div class="col_1_3 no-right-border">
                <div class="row">
                    <label class="wlabel blue">Shipments</label>
                    <label><asp:Literal runat="server" ID="litShipments" /></label>
                </div>
            </div>
            <div class="col_1_3">
                <div class="row">
                    <label class="wlabel blue">Service Tickets</label>
                    <label><asp:Literal runat="server" ID="litServiceTickets" /></label>
                </div>
            </div>
        </div>
    </td>
</tr>
