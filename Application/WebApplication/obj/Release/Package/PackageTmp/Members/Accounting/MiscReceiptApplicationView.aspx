﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="MiscReceiptApplicationView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.MiscReceiptApplicationView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Administration" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Operations" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>
<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
                Misc Receipt Application
            </h3>
        </div>
        <hr class="dark mb10" />
        
        <eShip:InvoiceFinderControl runat="server" ID="invoiceFinder" Visible="false" EnableMultiSelection="true"
            OnMultiItemSelected="OnInvoiceFinderMultiItemSelected" OnSelectionCancel="OnInvoiceFinderItemCancelled" OpenForEditEnabled="true" />

        <asp:Panel runat="server" ID="pnlMiscReceipt" Visible="False">
            <h5>Misc Receipt Information</h5>
            <div class="rowgroup">
                <div class="col_1_2 bbox vlinedarkright">
                    <div class="row">
                        <div class="fieldgroup mr20">
                            <label class="wlabel">Amount Paid</label>
                            <eShip:CustomTextBox runat="server" ID="txtAmountPaid" CssClass="w200 disabled" ReadOnly="True" />
                        </div>
                        <div class="fieldgroup">
                            <label class="wlabel">Amount Left To Be Applied</label>
                            <eShip:CustomTextBox runat="server" ID="txtAmountLeftToBeApplied" CssClass="w200 disabled" ReadOnly="True" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="fieldgroup mr20">
                            <label class="wlabel">Gateway Transaction Id</label>
                            <eShip:CustomTextBox runat="server" ID="txtGatewayTransactionId" CssClass="w200 disabled" ReadOnly="True" />
                        </div>
                        <div class="fieldgroup">
                            <label class="wlabel">Payment Gateway Type</label>
                            <eShip:CustomTextBox runat="server" ID="txtPaymentGatewayType" CssClass="w200 disabled" ReadOnly="True" />
                        </div>
                    </div>
                </div>
                <div class="col_1_2 bbox pl20">
                    <div class="row">
                        <div class="fieldgroup mr20">
                            <label class="wlabel">
                                Shipment 
                            <%= ActiveUser.HasAccessTo(ViewCode.Shipment) && !string.IsNullOrEmpty(txtShipmentNumber.Text)
                                    ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Shipment Record'><img width='20' src='{3}' class='middle' /></a>", 
                                        ResolveUrl(ShipmentView.PageAddress),
                                        WebApplicationConstants.TransferNumber, 
                                        txtShipmentNumber.Text,
                                        ResolveUrl("~/images/icons2/arrow_newTab.png"))  
                                    : string.Empty %>
                            </label>
                            <eShip:CustomTextBox runat="server" ID="txtShipmentNumber" CssClass="w200 disabled" ReadOnly="True" />
                        </div>
                        <div class="fieldgroup">
                            <label class="wlabel">
                                User
                            <%= ActiveUser.HasAccessTo(ViewCode.User) 
                                    ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To User Record'><img width='20' src='{3}' class='middle'/></a>", 
                                        ResolveUrl(UserView.PageAddress),
                                        WebApplicationConstants.TransferNumber, 
                                        hidUserId.Value.UrlTextEncrypt(),
                                        ResolveUrl("~/images/icons2/arrow_newTab.png"))  
                                    : string.Empty %>
                            </label>
                            <eShip:CustomHiddenField runat="server" ID="hidUserId" />
                            <eShip:CustomTextBox runat="server" ID="txtUser" CssClass="w200 disabled" ReadOnly="True" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="fieldgroup mr20">
                            <label class="wlabel">
                                Customer
                            <%= ActiveUser.HasAccessTo(ViewCode.Customer) 
                                    ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Customer Record'><img width='20' src='{3}' class='middle'/></a>", 
                                        ResolveUrl(CustomerView.PageAddress),
                                        WebApplicationConstants.TransferNumber, 
                                        hidCustomerId.Value.UrlTextEncrypt(),
                                        ResolveUrl("~/images/icons2/arrow_newTab.png"))  
                                    : string.Empty %>
                            </label>
                            <eShip:CustomHiddenField runat="server" ID="hidCustomerId" />
                            <eShip:CustomTextBox runat="server" ID="txtCustomer" CssClass="w200 disabled" ReadOnly="True" />
                        </div>
                        <div class="fieldgroup">
                            <label class="wlabel">Payment Date</label>
                            <eShip:CustomTextBox runat="server" ID="txtPaymentDate" CssClass="w200 disabled" ReadOnly="True" />
                        </div>
                    </div>
                </div>
            </div>
            <eShip:CustomHiddenField runat="server" ID="hidMiscReceiptId" />
            <h5>Applications to Invoices</h5>
            <eShip:TableFreezeHeaderExtender runat="server" ID="tfheApplicationsToInvoiceTable" TableId="applicationsToInvoiceTable" HeaderZIndex="2" />
            <div class="rowgroup">
                <table class="stripe" id="applicationsToInvoiceTable">
                    <tr>
                        <th style="width: 60%">Invoice Number
                        </th>
                        <th style="width: 20%">Application Date
                        </th>
                        <th style="width: 10%" class="text-right">Amount
                        </th>
                        <th style="width: 10%" class="text-center">Actions
                        </th>
                    </tr>
                    <asp:Repeater runat="server" ID="rptMiscReceiptApplications">
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <%# ActiveUser.HasAccessTo(ViewCode.Invoice)
                                                                ? string.Format("{0} <a href='{1}?{2}={3}' class='blue' target='_blank' title='Go To Invoice Record'><img src='{4}' alt='' width='16' align='absmiddle'/></a>",
                                                                                Eval("Invoice.InvoiceNumber"),
                                                                                ResolveUrl(InvoiceView.PageAddress),
                                                                                WebApplicationConstants.TransferNumber,
                                                                                Eval("Invoice.InvoiceNumber").GetString(),
                                                                                ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                                : Eval("Invoice.InvoiceNumber") %>
                                    <eShip:CustomHiddenField runat="server" ID="hidMiscReceiptApplicationId" Value='<%# Eval("Id") %>' />
                                </td>
                                <td>
                                    <%# Eval("ApplicationDate").FormattedLongDate() %>
                                </td>
                                <td class="text-right">
                                    <%# Eval("Amount", "{0:c4}") %>
                                </td>
                                <td class="text-center">
                                    <asp:ImageButton runat="server" ID="ibtnModify" ImageUrl="~/images/icons2/editBlue.png"
                                        ToolTip="Modify Application" OnClick="OnModifyMiscReceiptApplicationClicked" Visible='<%# Access.Modify %>' />
                                    <asp:ImageButton runat="server" ID="ibtnRefundReceipt" ImageUrl="~/images/icons2/deleteX.png"
                                        ToolTip="Delete Application" OnClick="OnDeleteMiscReceiptApplicationClicked" Visible='<%# Access.Remove %>' />
                                    <ajax:ConfirmButtonExtender runat="server" ID="cbeRefundReceipt" TargetControlID="ibtnRefundReceipt"
                                        ConfirmText="Are you sure you want to delete application?" />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </table>
            </div>
            <div class="row">
                <div class="fieldgroup">
                    <h5>Invoices That Misc Receipt May Be Applied To</h5>
                </div>
                <div class="fieldgroup right pt16">
                    <asp:Button runat="server" ID="btnAddInvoicesToBeAppliedTo" Text="Add Invoices" OnClick="OnAddInvoicesToBeAppliedToClicked"/>
                </div>
            </div>
            <eShip:TableFreezeHeaderExtender runat="server" ID="tfheInvoicesToBeAppliedToTable" TableId="invoicesToBeAppliedToTable" HeaderZIndex="2" />
            <div class="rowgroup">
                <table class="stripe sm_chk" id="invoicesToBeAppliedToTable">
                    <tr>
                        <th style="width: 3%;">
                            <eShip:AltUniformCheckBox ID="chkSelectAllRecords" runat="server" OnClientSideClicked="javascript:SetSelectedCheckBoxesStatus(this, 'invoicesToBeAppliedToTable');" />
                        </th>
                        <th style="width: 42%">Invoice Number
                        </th>
                        <th style="width: 10%">Invoice Type
                        </th>
                        <th style="width: 15%">Amount Paid
                        </th>
                        <th style="width: 15%">Total Amount Due
                        </th>
                        <th style="width: 15%" class="text-right">Amount to Apply
                        </th>
                    </tr>
                    <asp:Repeater runat="server" ID="rptInvoices">
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <eShip:AltUniformCheckBox runat="server" ID="chkSelected" OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'invoicesToBeAppliedToTable', 'chkSelectAllRecords');" />
                                </td>
                                <td>
                                    <eShip:CustomHiddenField runat="server" ID="hidInvoiceId" Value='<%# Eval("Id") %>' />
                                    <%# ActiveUser.HasAccessTo(ViewCode.Invoice)
                                                                ? string.Format("{0} <a href='{1}?{2}={3}' class='blue' target='_blank' title='Go To Invoice Record'><img src='{4}' alt='' width='16' align='absmiddle'/></a>",
                                                                                Eval("InvoiceNumber"),
                                                                                ResolveUrl(InvoiceView.PageAddress),
                                                                                WebApplicationConstants.TransferNumber,
                                                                                Eval("InvoiceNumber").GetString(),
                                                                                ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                                : Eval("InvoiceNumber") %>
                                </td>
                                <td>
                                    <%# Eval("InvoiceTypeText") %>
                                </td>
                                <td>
                                    <%# Eval("PaidAmount") %>
                                </td>
                                <td>
                                    <%# Eval("AmountDue") %>
                                </td>
                                <td class="text-right">
                                    <eShip:CustomTextBox runat="server" ID="txtAmountToApply" CssClass="w80" Type="FloatingPointNumbers" />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </table>
                <div class="row pt10">
                    <div class="fieldgroup right">
                        <asp:Button runat="server" ID="btnCreateApplications" Text="Apply" OnClick="OnApplyMiscReceiptClicked" Visible='<%# Access.Modify %>' />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlError" Visible="False">
            <table class="line2 pl2">
                <tr class="header">
                    <td>
                        <h5 class="red">Error Loading Record</h5>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p class="pb40 mt20 fs120em">
                            The misc. receipt you are attempting to access was not found.
                        </p>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlEditMiscReceiptApplication" CssClass="popup popupControlOverW500" Visible="False">
            <div class="popheader">
                <h4>Modify Misc Receipt Application</h4>
                <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                    CausesValidation="false" OnClick="OnCancelClicked" runat="server" />
            </div>
            <table class="poptable">
                <tr>
                    <td class="text-right pt10">
                        <label class="upper">Amount:</label>
                    </td>
                    <td class="pt10">
                        <eShip:CustomHiddenField runat="server" ID="hidEditMiscReceiptApplicationId" />
                        <eShip:CustomTextBox runat="server" ID="txtEditMiscReceiptApplicationAmount" CssClass="w80" Type="FloatingPointNumbers" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button runat="server" ID="btnSaveMiscReceiptApplication" OnClick="OnSaveMiscReceiptApplicationClicked" Text="Save" />
                        <asp:Button runat="server" ID="btnCancel" OnClick="OnCancelClicked" Text="Cancel" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
    </div>
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
</asp:Content>
