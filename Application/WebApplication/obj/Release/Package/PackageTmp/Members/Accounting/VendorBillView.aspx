﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="VendorBillView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.VendorBillView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.Operations" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.Registry" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Operations" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        ShowEdit="true" ShowDelete="true" ShowSave="true" ShowFind="true" ShowMore="true"
        ShowImport="false" OnFind="OnToolbarFindClicked" OnSave="OnToolbarSaveClicked" OnUnlock="OnToolbarUnlockClicked"
        OnDelete="OnToolbarDeleteClicked" OnNew="OnToolbarNewClicked" OnEdit="OnToolbarEditClicked"
        OnCommand="OnToolbarCommand" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Vendor Bills<eShip:RecordIdentityDisplayControl runat="server" ID="ridcVendorBillIdentity" TargetControlId="txtDocumentNumber" />
            </h3>

            <div class="lockMessage">
                <asp:Literal ID="litMessage" runat="server" />
            </div>
        </div>

        <hr class="dark mb5" />

        <div class="errorMsgLit">
            <asp:Literal runat="server" ID="litErrorMessages" />
        </div>

        <eShip:VendorBillFinderControl runat="server" ID="vendorBillFinder" Visible="false" OpenForEditEnabled="false"
            EnableMultiSelection="false" OnItemSelected="OnVendorBillFinderItemSelected"
            OnSelectionCancel="OnVendorBillFinderItemCancelled" />
        <eShip:ServiceTicketFinderControl runat="server" ID="serviceTicketFinder" Visible="false"
            EnableMultiSelection="true" OnMultiItemSelected="OnServiceTicketFinderMultiItemSelected"
            OnSelectionCancel="OnServiceTicketFinderItemCancelled" OpenForEditEnabled="true" />
        <eShip:ShipmentFinderControl runat="server" ID="shipmentFinder" Visible="false" EnableMultiSelection="true"
            OnMultiItemSelected="OnShipmentFinderMultiItemSelected" OnSelectionCancel="OnShipmentFinderItemCancelled"
            OpenForEditEnabled="true" />
        <eShip:VendorFinderControl ID="vendorFinder" runat="server" EnableMultiSelection="false"
            Visible="False" OpenForEditEnabled="false" OnSelectionCancel="OnVendorFinderSelectionCancelled"
            OnItemSelected="OnVendorFinderItemSelected" />

        <script type="text/javascript">
            function ClearHidFlag() {
                jsHelper.SetEmpty('<%= hidFlag.ClientID %>');
            }
        </script>
        <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
            Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" />

        <eShip:AjaxTabHeaderTextUpdater runat="server" ID="athtuTabUpdater" />

        <eShip:CustomHiddenField ID="hidVendorBillId" runat="server" />

        <ajax:TabContainer ID="tabInvoice" runat="server" CssClass="ajaxCustom">
            <ajax:TabPanel runat="server" ID="tabDetails" HeaderText="Details">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlDetails">
                        <div class="rowgroup">
                            <div class="fieldgroup mr10">
                                <label class="wlabel blue">Document Number</label>
                                <eShip:CustomTextBox ID="txtDocumentNumber" runat="server" CssClass="w200" />
                            </div>
                            <div class="fieldgroup vlinedarkright mr30">
                                <label class="wlabel blue">Type</label>
                                <eShip:CustomTextBox ID="txtBillType" runat="server" ReadOnly="True" CssClass="w150 disabled" />
                            </div>
                            <div class="fieldgroup vlinedarkright pl10 mr30">
                                <label class="wlabel blue">User</label>
                                <eShip:CustomTextBox runat="server" ID="txtUser" ReadOnly="True" CssClass="w240 disabled" />
                            </div>
                            <div class="fieldgroup pl10">
                                <label class="wlabel blue">Original Invoice Number</label>
                                <eShip:CustomHiddenField runat="server" ID="hidApplyToDocumentId" />
                                <eShip:CustomTextBox runat="server" ID="txtApplyToDocument" ReadOnly="true" CssClass="w240 disabled" />
                                <asp:ImageButton ID="imgApplyToDocumentSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png" CausesValidation="False" OnClick="OnVendorBillSearchClicked" />
                            </div>
                        </div>
                        <hr class="dark" />
                        <div class="rowgroup">
                            <div class="col_1_2 bbox vlinedarkright">
                                <h5>Billing Information</h5>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Vendor
                                            <%= hidVendorId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.Vendor) 
                                                    ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Vendor Record'><img src={3} width='20' class='middle'/></a>", 
                                                                            ResolveUrl(VendorView.PageAddress),
                                                                            WebApplicationConstants.TransferNumber, 
                                                                            hidVendorId.Value.UrlTextEncrypt(),
                                                                            ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                    : string.Empty %>
                                        </label>
                                        <eShip:CustomHiddenField runat="server" ID="hidVendorId" />
                                        <eShip:CustomTextBox runat="server" ID="txtVendorNumber" CssClass="w110" OnTextChanged="OnVendorNumberEntered"
                                            AutoPostBack="True" />
                                        <asp:RequiredFieldValidator runat="server" ID="rfvVendorNumber" Text="*" ControlToValidate="txtVendorNumber"
                                            ErrorMessage="all fields marked with an ' * ' are required" Display="Dynamic" />
                                        <asp:ImageButton ID="imgVendorSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                                            CausesValidation="False" OnClick="OnVendorSearchClicked" />
                                        <eShip:CustomTextBox runat="server" ID="txtVendorName" CssClass="w280 disabled" ReadOnly="True" />
                                        <ajax:AutoCompleteExtender runat="server" ID="aceVendor" TargetControlID="txtVendorNumber" ServiceMethod="GetVendorList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                            EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Vendor Location</label>

                                        <script type="text/javascript">
                                            $(document).ready(function () {
                                                $(jsHelper.AddHashTag("<%= ddlVendorLocation.ClientID %>")).change(function () {
                                                    $(jsHelper.AddHashTag(this.value)).show().siblings().hide();
                                                });
                                                $(jsHelper.AddHashTag("<%= ddlVendorLocation.ClientID %>")).change();
                                            })
                                        </script>
                                        <asp:DropDownList runat="server" ID="ddlVendorLocation" DataTextField="Text" DataValueField="Value" CssClass="w420" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Address</label>
                                    </div>
                                </div>
                                <div class="row pb20">
                                    <div class="fieldgroup">
                                        <div>
                                            <eShip:CustomTextBox ID="TextBox1" runat="server" TextMode="MultiLine" CssClass="w420 disabled" ReadOnly="True" />
                                        </div>
                                        <asp:Repeater runat="server" ID="rptVendorLocations">
                                            <ItemTemplate>
                                                <div id='<%# Eval("Id") %>' style="display: none;">
                                                    <eShip:CustomTextBox ID="TextBox2" runat="server" TextMode="MultiLine" CssClass="w420 disabled" ReadOnly="True"
                                                        Text='<%# string.Format("{0} {1} {2}{3}{4} {5} {6}{3}{7}", Eval("RemitTo"), Eval("Street1"), Eval("Street2"), Environment.NewLine, 
                                                                Eval("City"), Eval("State"), Eval("PostalCode"), Eval("CountryName")) %>' />
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                                <h5>Financial Information</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Vendor Bill Total</label>
                                        <asp:UpdatePanel runat="server" UpdateMode="Always">
                                            <ContentTemplate>
                                                <eShip:CustomTextBox runat="server" ID="txtVendorBillTotal" CssClass="w200 disabled" ReadOnly="True" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Total Credit Adjustments</label>
                                        <eShip:CustomTextBox runat="server" ID="txtAdjustments" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                </div>
                            </div>
                            <div class="col_1_2 bbox pl46">
                                <h5>Milestones</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Date Created</label>
                                        <eShip:CustomTextBox runat="server" ID="txtDateCreated" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Document Date</label>
                                        <eShip:CustomTextBox runat="server" ID="txtDocumentDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Posted</label>
                                        <eShip:CustomTextBox runat="server" ID="txtPostDate" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                    <div class="fieldgroup_s">
                                        <asp:CheckBox runat="server" ID="chkPosted" Enabled="False" CssClass="jQueryUniform" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Exported</label>
                                        <eShip:CustomTextBox runat="server" ID="txtExportDate" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                    <div class="fieldgroup_s">
                                        <asp:CheckBox runat="server" ID="chkExported" Enabled="False" CssClass="jQueryUniform" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabVendorBillDetails" HeaderText="Vendor Bill Details">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlDetails" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlVendorBillDetails">
                                <div class="row pb10">
                                    <div class="fieldgroup">
                                        <label class="wlabel blue">Total Amount Due</label>
                                        <asp:UpdatePanel runat="server">
                                            <ContentTemplate>
                                                <eShip:CustomTextBox ID="txtTotalAmount" runat="server" CssClass="w200 disabled" ReadOnly="True" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="fieldgroup_s pl20">
                                        <asp:Panel runat="server" ID="pnlVendorFiltering">
                                            <eShip:AltUniformCheckBox runat="server" ID="chkDisableVendorFiltering" />
                                            <label class="mr10">Disable Vendor Filtering When Adding Shipment/Service Ticket</label>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="row pb10">
                                    <div class="fieldgroup">
                                        <eShip:PaginationExtender runat="server" ID="peLstVendorBillDetails" TargetControlId="lstVendorBillDetails" PageSize="50" UseParentDataStore="True" OnPageChanging="OnVendorBillDetailsIndexChange" />
                                    </div>
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddShipment" Text="Add Shipment" CssClass="mr10" CausesValidation="false" OnClick="OnAddShipmentClicked" />
                                        <asp:Button runat="server" ID="btnAddServiceTicket" Text="Add Service Ticket" CssClass="mr10" CausesValidation="false" OnClick="OnAddServiceTicketClicked" />
                                        <asp:Button runat="server" ID="btnRemoveDetails" Text="Show/Remove Details" CssClass="mr10" CausesValidation="false" OnClick="OnRemoveDetailsClicked" />
                                        <asp:Button runat="server" ID="btnClearDetails" Text="Clear Details" CausesValidation="False" OnClick="OnClearDetailsClicked" />
                                    </div>
                                </div>
                                
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheVendorBillDetailsTable" TableId="vendorBillDetailsTable" HeaderZIndex="2" IgnoreHeaderBackgroundFill="True" />
                                <script type="text/javascript">
                                    $(function () {
                                        SetVendorBillDetailControlFocus();
                                        
                                        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                                            SetVendorBillDetailControlFocus();
                                        });
                                    });

                                    function SetVendorBillDetailControlFocus() {
                                        var controlId = $('#<%= hidControlToFocus.ClientID %>').val();
                                        if (!jsHelper.IsNullOrEmpty(controlId)) {
                                            $(jsHelper.AddHashTag(controlId)).focus();
                                            $('#<%= hidControlToFocus.ClientID %>').val(jsHelper.EmptyString);
                                        }
                                    }
                                </script>
                                <table class="stripe" id="vendorBillDetailsTable">
                                    <tr>
                                        <th style="width: 15%;">Reference Number
                                        </th>
                                        <th style="width: 12%;">Reference Type
                                        </th>
                                        <th style="width: 10%;" class="text-center">Quantity
                                        </th>
                                        <th style="width: 13%;" class="text-right">Unit Buy ($)
                                        </th>
                                        <th style="width: 12%;" class="text-right">Total Amount ($)
                                        </th>
                                        <th style="width: 30%;">Account Bucket
                                        </th>
                                        <th style="width: 8%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstVendorBillDetails" runat="server" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnVendorBillDetailsItemDataBound">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td class="top">
                                                    <eShip:CustomHiddenField ID="hidVendorBillDetailIndex" runat="server" Value='<%# Container.DataItemIndex + (peLstVendorBillDetails.PageSize*(peLstVendorBillDetails.CurrentPage-1)) %>' />
                                                    <eShip:CustomHiddenField ID="hidVendorDetailBillId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <eShip:CustomTextBox ID="txtRefNumber" runat="server" Text='<%# Eval("ReferenceNumber")%>' ReadOnly="True" CssClass="w200 disabled" />
                                                </td>
                                                <td class="top">
                                                    <asp:DropDownList ID="ddlReferenceType" DataTextField="Text" DataValueField="Value" runat="server" CssClass="w130" Enabled="False" />
                                                </td>
                                                <td class="top text-center">
                                                    <eShip:CustomTextBox ID="txtQuantity" runat="server" Text='<%# Eval("Quantity") %>' CssClass="w60" Type="NumbersOnly" OnTextChanged="OnVendorBillDetailsTextChanged" AutoPostBack="True"/>
                                                </td>
                                                <td class="top text-center">
                                                    <eShip:CustomTextBox ID="txtUnitBuy" runat="server" Text='<%# Eval("UnitBuy", "{0:f4}") %>' CssClass="w80" Type="FloatingPointNumbers" OnTextChanged="OnVendorBillDetailsTextChanged" AutoPostBack="True"/>
                                                </td>
                                                <td class="text-right top">
                                                    <eShip:CustomTextBox ID="txtTotalAmount" runat="server" Text='<%# Eval("TotalAmount", "{0:f4}") %>' CssClass="w80 disabled" ReadOnly="True" />
                                                </td>
                                                <td class="top">
                                                    <eShip:CachedObjectDropDownList runat="server" ID ="ddlAccountBucket" CssClass="w230" Type="AccountBuckets"
                                                         EnableChooseOne="True" DefaultValue="0" SelectedValue='<%# Eval("AccountBucketId") %>' />
                                                </td>
                                                <td class="text-center top">
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        CausesValidation="false" OnClick="OnDeleteVendorBillDetailClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                            <tr class="hidden">
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td colspan="10" class="pt0 bottom_shadow">
                                                    <div class="col_1_3">
                                                        <div class="row">
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue">Charge Code</label>
                                                                <eShip:CachedObjectDropDownList runat="server" ID="ddlChargeCode" CssClass="w300" Type="ChargeCodes" 
                                                                    DefaultValue="0" SelectedValue='<%# Eval("ChargeCodeId") %>'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col_1_3 no-right-border">
                                                        <div class="row">
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue">Note</label>
                                                                <eShip:CustomTextBox ID="txtNote" runat="server" Text='<%# Eval("Note") %>' CssClass="w300" MaxLength="50" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                                <asp:Panel runat="server" ID="pnlRemoveDetails" Visible="false" CssClass="popupControl">
                                    <div class="popheader">
                                        <h4>Vendor Bill Details
                                        </h4>
                                        <asp:ImageButton ID="ImageButton2" ImageUrl="~/images/icons2/close.png" CssClass="close"
                                            CausesValidation="false" OnClick="OnCloseRemoveDetailsClicked" runat="server" />
                                    </div>
                                    <table class="poptable">
                                        <tr>
                                            <td colspan="2" class="p_m0">
                                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheVendorBillRemoveDetailTable" TableId="vendorBillRemoveDetailTable" ScrollerContainerId="vendorBillRemoveDetailScrollSection"
                                                    ScrollOffsetControlId="pnlRemoveDetails" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2050" />
                                                <div class="finderScroll" id="vendorBillRemoveDetailScrollSection">
                                                    <table class="stripe sm_chk" id="vendorBillRemoveDetailTable">
                                                        <tr>
                                                            <th>
                                                                <eShip:AltUniformCheckBox ID="chkRemoveDetailSelectAll" runat="server" OnClientSideClicked="javascript:SetSelectedCheckBoxesStatus(this, 'vendorBillRemoveDetailTable');" />
                                                            </th>
                                                            <th style="width: 10%;">Reference Number
                                                            </th>
                                                            <th style="width: 10%;">Reference Type
                                                            </th>
                                                            <th style="width: 10%;">Quantity
                                                            </th>
                                                            <th style="width: 10%;" class="text-right">Unit Buy
                                                            </th>
                                                            <th style="width: 10%;" class="text-right">Total Amount
                                                            </th>
                                                            <th style="width: 24%;">Account Bucket
                                                            </th>
                                                            <th style="width: 23%;">Charge Code
                                                            </th>
                                                        </tr>
                                                        <asp:ListView ID="lstRemoveDetails" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                                            <LayoutTemplate>
                                                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                                            </LayoutTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <eShip:AltUniformCheckBox runat="server" ID="chkSelection" OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'vendorBillRemoveDetailTable', 'chkRemoveDetailSelectAll');" />
                                                                    </td>
                                                                    <td>
                                                                        <eShip:CustomHiddenField ID="hidDetailIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                                        <eShip:CustomHiddenField ID="hidDetailId" runat="server" Value='<%# Eval("Id") %>' />
                                                                        <%# Eval("ReferenceNumber") %>
                                                                    </td>
                                                                    <td>
                                                                        <%# Eval("ReferenceType") %>
                                                                    </td>
                                                                    <td class="text-center">
                                                                        <%# Eval("Quantity") %>
                                                                    </td>
                                                                    <td class="text-right">
                                                                        <%# Eval("UnitBuy") %>
                                                                    </td>
                                                                    <td class="text-right">
                                                                        <%# Eval("TotalAmount") %>
                                                                    </td>
                                                                    <td>
                                                                        <%# new AccountBucket(Eval("AccountBucketId").ToLong()).FormattedString() %>
                                                                    </td>
                                                                    <td>
                                                                        <%# new ChargeCode(Eval("ChargeCodeId").ToLong()).FormattedString() %>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:ListView>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Button Text="Remove Selected" OnClick="OnRemoveSelectedDetailsClicked" runat="server" CausesValidation="false" CssClass="mr10" />
                                                <asp:Button Text="Cancel" OnClick="OnCloseRemoveDetailsClicked" runat="server" CausesValidation="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnAddShipment" />
                            <asp:PostBackTrigger ControlID="btnAddServiceTicket" />
                        </Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
			  <ajax:TabPanel ID="tabDocuments" runat="server" HeaderText="Documents">
                <ContentTemplate>
	                 <table class="stripe" id="Table1">
                                    <tr>
                                        <th style="width: 50%;">Entity Reference
                                        </th>
                                        <th style="width: 50%;">File
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstDocuments" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                
                                                <td class="top">
                                                  <%# Eval("ReferenceType") %> -   <%# Eval("ReferenceNumber") %> 
													        <%# ActiveUser.HasAccessTo(ViewCode.Shipment)?
                                                        (string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To {3} Record'><img src={4} width='20' class='middle'/></a>",
                                                        ResolveUrl(
	                                                        (string) (Eval("ReferenceType")) == "Shipment"
		                                                        ? ShipmentView.PageAddress
		                                                        : (string) (Eval("ReferenceType")) == "ServiceTicket"
			                                                          ? ServiceTicketView.PageAddress
			                                                          : ShipmentView.PageAddress
	                                                        ),
                                                        WebApplicationConstants.TransferNumber,
                                                        Eval("ReferenceNumber"),
                                                        Eval("ReferenceType"),
                                                        ResolveUrl("~/images/icons2/arrow_newTab.png")) ):string.Empty
                                                            %>
                                                </td>
												<td class="top">
                                                   <eShip:CustomHiddenField ID="hidLocationPath" runat="server" Value='<%# Eval("LocationPath") %>' />
                                            <asp:LinkButton runat="server" ID="lnkShipmentDocumentLocationPath" Text='<%# GetLocationFileName(Eval("LocationPath")) %>'
                                                OnClick="OnLocationPathClicked" CausesValidation="false" CssClass="blue" />
                                                </td>
                                          
                                            </tr>
                                          </ItemTemplate>
                                    </asp:ListView>
                                </table>
				 </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabAuditLog" HeaderText="Audit Log">
                <ContentTemplate>
                    <eShip:AuditLogControl runat="server" ID="auditLogs" />
                </ContentTemplate>
            </ajax:TabPanel>
        </ajax:TabContainer>
    </div>
    <asp:Panel runat="server" ID="pnlNewVendorBillSelection" Visible="false">
        <div class="popupControl popupControlOverW500">
            <div class="popheader">
                <h4>New Vendor Bill Type Selection
                </h4>
                <asp:ImageButton ID="ImageButton3" ImageUrl="~/images/icons2/close.png" CssClass="close" CausesValidation="false" OnClick="OnCloseNewVendorBillSelectionTypeClicked" runat="server" />
            </div>
            <div class="row text-center pt10 pb10">
                <asp:Button runat="server" ID="btnInvoiceSelect" Text="INVOICE" OnClick="OnVendorBillTypeSelectClicked" Width="110px" CausesValidation="false" CssClass="mr20" />
                <asp:Button runat="server" ID="btnCreditSelect" Text="CREDIT" OnClick="OnVendorBillTypeSelectClicked" Width="110px" CausesValidation="false" CssClass="mr20" />
                <asp:Button runat="server" ID="btnCancelSelect" Text="CANCEL" OnClick="OnCloseNewVendorBillSelectionTypeClicked" Width="110px" CausesValidation="False" />
            </div>
        </div>
    </asp:Panel>

    <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
        OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
    <asp:UpdatePanel runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Panel ID="pnlDimScreen" CssClass="dimBackgroundControl" runat="server" Visible="false" />
            <eShip:CustomHiddenField runat="server" ID="hidFlag" />
            <eShip:CustomHiddenField runat="server" ID="hidEditStatus" />
            <eShip:CustomHiddenField runat="server" ID="hidControlToFocus"/>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
