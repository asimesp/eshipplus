﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VendorInformationControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls.VendorInformationControl" %>
<%@ Register TagPrefix="pc" TagName="VendorInformationControlDetailControl" Src="~/Members/Accounting/Controls/VendorInformationControlDetailControl.ascx" %>
<%@ Register TagPrefix="pc" TagName="ChartGenerator" Src="~/Members/BusinessIntelligence/Controls/ChartGenerator.ascx" %>
<asp:Panel runat="server" ID="pnlLocation" CssClass="popupControlOver">
    <div class="popheader">
        <h4>
            <asp:Literal runat="server" ID="litVendorName" />
            <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/icons2/close.png" CssClass="close"
                CausesValidation="false" OnClick="OnCancelClicked" runat="server" />
        </h4>
    </div>
    <ajax:TabContainer runat="server" ID="tabVendorInfo" CssClass="ajaxCustom">
        <ajax:TabPanel ID="tabVendorCostChart" runat="server" HeaderText="Shipment Cost Chart">
            <ContentTemplate>
                <div class="rowgroup">
                    <div class="row">
                        <asp:Panel runat="server" ID="pnlChartContainer" CssClass="text-center">
                            <pc:ChartGenerator ID="chartGenerator" runat="server" />
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlNoChart" Visible="False">
                            <label class="pl20">No Cost Chart Available for Vendor Selected</label>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlChartDataContainer">
                            <table class="stripe">
                                <asp:ListView runat="server" ItemPlaceholderID="itemPlaceHolder" ID="lstChartDataYearMonth">
                                    <LayoutTemplate>
                                        <tr>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </tr>
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <th>
                                            <%# Eval("YearMonth") %></th>
                                    </ItemTemplate>
                                </asp:ListView>
                                <asp:ListView runat="server" ItemPlaceholderID="itemPlaceHolder" ID="lstChartDataAverageCost">
                                    <LayoutTemplate>
                                        <tr>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </tr>
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <td>
                                            <%# Eval("AverageCost") %></td>
                                    </ItemTemplate>
                                </asp:ListView>
                            </table>
                        </asp:Panel>
                    </div>
                </div>
            </ContentTemplate>
        </ajax:TabPanel>
        <ajax:TabPanel runat="server" ID="tabLocations" HeaderText="Locations">
            <ContentTemplate>
                <table class="poptable">
                    <asp:ListView ID="lstLocations" runat="server" ItemPlaceholderID="itemPlaceHolder"
                        OnItemDataBound="OnVendorLocationsDataBound">
                        <LayoutTemplate>
                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <th colspan="20" class="text-left">
                                    <label class="upper">
                                        <%# string.Format("{0}. ", Container.DataItemIndex + 1) %>&nbsp;<%#Eval("FullAddress")%>&nbsp;
                                        <asp:Literal ID="Literal1" runat="server" Visible='<%#Eval("Primary") %>' Text="-  Primary Location" />
                                    </label>
                                </th>
                            </tr>
                            <tr>
                                <td>
                                    <pc:VendorInformationControlDetailControl runat="server" ID="vendorDetail" />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </table>
            </ContentTemplate>
        </ajax:TabPanel>
        <ajax:TabPanel ID="tabVendorStatistics" runat="server" HeaderText=" Shipment Statistics">
            <ContentTemplate>
                <table class="stripe">
                    <tr>
                        <th style="width: 25%;" class="text-right">Shipment Counts</th>
                        <th style="width: 15%;">LTL</th>
                        <th style="width: 15%;">TL</th>
                        <th style="width: 15%;">Air</th>
                        <th style="width: 15%;">Rail</th>
                        <th style="width: 15%;">Small Pack</th>
                    </tr>
                    <tr>
                        <td class="text-right">Total:</td>
                        <td>
                            <asp:Literal runat="server" ID="litLTLCount" />
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="litTLCount" />
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="litAirCount" />
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="litRailCount" />
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="litSmallPackCount" />
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">Total Considered:</td>
                        <td>
                            <asp:Literal runat="server" ID="litLTLConsideredCount" />
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="litTLConsideredCount" />
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="litAirConsideredCount" />
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="litRailConsideredCount" />
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="litSmallPackConsideredCount" />
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">Late Pickups:
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="litLTLLatePickupCount" />
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="litTLLatePickupCount" />
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="litAirLatePickupCount" />
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="litRailLatePickupCount" />
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="litSmallPackLatePickupCount" />
                        </td>
                    </tr>
                    <tr class="content">
                        <td class="text-right">Late Deliveries:
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="litLTLLateDeliveryCount" />
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="litTLLateDeliveryCount" />
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="litAirLateDeliveryCount" />
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="litRailLateDeliveryCount" />
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="litSmallPackLateDeliveryCount" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </ajax:TabPanel>
    </ajax:TabContainer>
</asp:Panel>
<asp:Panel ID="pnlFinderDimScreen" CssClass="dimBackgroundControlOver" runat="server" Visible="False" />

