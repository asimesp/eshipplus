﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="CustomerControlAccountView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.CustomerControlAccountView" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        OnNew="OnNewClick" OnImport="OnImportClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Customer Control Accounts<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" Text="" /></small>
            </h3>
        </div>

        <hr class="dark mb10" />

        <asp:Panel runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row mb10 pb10 bottom_shadow">
                    <div class="fieldgroup">
                        <label class="upper blue">Customer: </label>
                        <eShip:CustomHiddenField runat="server" ID="hidCustomerId" />
                        <eShip:CustomTextBox runat="server" ID="txtCustomerNumber" CssClass="w110" OnTextChanged="OnCustomerNumberEntered" AutoPostBack="true" />
                        <asp:RequiredFieldValidator runat="server" ID="rfvCustomerNumber" ControlToValidate="txtCustomerNumber" ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/icons2/search_dark.png" CausesValidation="false" OnClick="OnCustomerSearchClicked" />
                        <eShip:CustomTextBox runat="server" ID="txtCustomerName" CssClass="w280 disabled" ReadOnly="true" />
                        <ajax:AutoCompleteExtender runat="server" ID="aceCustomer" TargetControlID="txtCustomerNumber" ServiceMethod="GetCustomerList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                            EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                        <%= hidCustomerId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.Customer) 
                            ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Customer Record'><img src={3} width='20' class='middle'/></a>", 
                                                    ResolveUrl(CustomerView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    hidCustomerId.Value.UrlTextEncrypt(),
                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
                            : string.Empty %>
                    </div>
                </div>
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="CustomerControlAccounts" ShowAutoRefresh="False"
                            OnProcessingError="OnSearchProfilesProcessingError" OnSearchProfileSelected="OnSearchProfilesProfileSelected" />
                    </div>
                </div>
            </div>

            <hr class="dark mb10" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                        OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>

            <hr class="fat" />
            <div class="rowgroup">
                <div class="row mb10">
                    <div class="left">
                        <asp:Button ID="btnSearch" CssClass="mr10" runat="server" Text="SEARCH" OnClick="OnSearchClicked"
                            CausesValidation="False" />
                        <asp:Button ID="btnExport" runat="server" Text="EXPORT" OnClick="OnExportClicked" />
                    </div>
                </div>
            </div>
        </asp:Panel>

        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheCustomerControlAccountTable" TableId="customerControlAccountTable" HeaderZIndex="2"/>
        <div class="rowgroup">
            <table class="line2 pl2" id="customerControlAccountTable">
                <tr>
                    <th style="width: 15%;">Account Number
                    </th>
                    <th style="width: 15%;">Description
                    </th>
                    <th style="width: 62%;">Address
                    </th>
                    <th style="width: 8%;">Action
                    </th>
                </tr>
                <asp:ListView ID="lvwRegistry" runat="server" ItemPlaceholderID="itemPlaceHolder">
                    <LayoutTemplate>
                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <eShip:CustomHiddenField ID="customerControlAccountId" runat="server" Value='<%# Eval("Id") %>' />
                                <%# Eval("AccountNumber") %>
                            </td>
                            <td>
                                <%# Eval("Description") %>
                            </td>
                            <td>
                                <%# Eval("FullAddress")%>
                            </td>
                            <td class="imageButtons">
                                <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png"
                                    CausesValidation="false" OnClick="OnEditClicked" Enabled='<%# Access.Modify %>' />
                                <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                    CausesValidation="false" OnClick="OnDeleteClicked" Enabled='<%# Access.Remove %>' />
                                <ajax:ConfirmButtonExtender runat="server" ID="cbeDelete" TargetControlID="ibtnDelete"
                                    ConfirmText="Are you sure you want to delete record?" />
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </table>
        </div>
        <asp:Panel ID="pnlEdit" runat="server" Visible="false">
            <div class="popupControl popupControlOverW500">
                <div class="popheader">
                    <h4>
                        <asp:Literal ID="litTitle" runat="server" />
                    </h4>
                    <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                        CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
                </div>
                <div class="row">
                    <table class="poptable">
                        <tr>
                            <td colspan="20" class="red">
                                <asp:Literal ID="litMessage" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Account Number:</label>
                            </td>
                            <td>
                                <eShip:CustomHiddenField ID="hidCustomerControlAccountId" runat="server" />
                                <eShip:CustomTextBox ID="txtAccountNumber" CssClass="w200" MaxLength="100" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Customer Category:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtCustomerCategory" CssClass="w200" MaxLength="50" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Description:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtDescription" CssClass="w200" MaxLength="50" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Street 1:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtStreet1" CssClass="w200" MaxLength="50" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Street 2:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtStreet2" CssClass="w200" MaxLength="50" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">City:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtCity" CssClass="w200" MaxLength="50" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">State:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtState" CssClass="w200" MaxLength="50" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Postal Code:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtPostalCode" CssClass="w200" MaxLength="10" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Country:</label>
                            </td>
                            <td>
                                <eShip:CachedObjectDropDownList runat="server" ID ="ddlEditCountry" CssClass="w200" Type="Countries" EnableChooseOne="True" DefaultValue="0"/>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="pt10">
                                <asp:Button ID="btnSave" Text="Save" OnClick="OnSaveClick" runat="server" />
                                <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnCloseClicked" runat="server"
                                    CausesValidation="false" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>
    <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information"
        OnOkay="OnOkayProcess" />
    <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
        OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
    <eShip:CustomerFinderControl ID="customerFinder" runat="server" OnItemSelected="OnCustomerFinderItemSelected"
        OnSelectionCancel="OnCustomerFinderSelectionCancelled" Visible="false" OpenForEditEnabled="false" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackgroundControl" runat="server" Visible="false" />
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
</asp:Content>
