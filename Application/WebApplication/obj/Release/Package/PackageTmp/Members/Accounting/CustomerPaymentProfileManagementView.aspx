﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="CustomerPaymentProfileManagementView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.CustomerPaymentProfileManagementView" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>
<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
                Customer Payment Profile Management
            </h3>
        </div>
        <hr class="dark mb5" />
         <div class="errorMsgLit">
            <asp:Literal runat="server" ID="litErrorMessages" />
        </div>
         <eShip:CustomerFinderControl ID="customerFinder" runat="server" EnableMultiSelection="false"
            OnlyActiveResults="True" Visible="False" OpenForEditEnabled="false" OnSelectionCancel="OnCustomerFinderSelectionCancelled"
            OnItemSelected="OnCustomerFinderItemSelected" />

        <div class="rowgroup">
            <div class="row">
                <div class="fieldgroup mr20">
                    <label class="label upper blue">Customer:</label>
                    <eShip:CustomHiddenField runat="server" ID="hidCustomerId" />
                    <eShip:CustomTextBox runat="server" ID="txtCustomerNumber" OnTextChanged="OnCustomerNumberEntered" AutoPostBack="True" CssClass="w110" />
                    <asp:ImageButton ID="imgCustomerSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png" CausesValidation="False" OnClick="OnCustomerSearchClicked" />
                    <eShip:CustomTextBox runat="server" ID="txtCustomerName" ReadOnly="True" CssClass="w260 disabled" />
                    <ajax:AutoCompleteExtender runat="server" ID="aceCustomer" TargetControlID="txtCustomerNumber" ServiceMethod="GetCustomerList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                        EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                    <%= hidCustomerId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.Customer) 
                            ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Customer Record'><img src={3} width='20' class='middle'/></a>", 
                                                    ResolveUrl(CustomerView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    hidCustomerId.Value.UrlTextEncrypt(),
                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
                            : string.Empty %>
                </div>
            </div>
        </div>

        <hr class="fat" />

        <eShip:PaymentProfileManagerControl runat="server" ID="ppmPaymentProfileManager" OnProcessingMessages="OnPaymentProfileManagerProcessingMessages"/>
    </div>
    
    <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false" Button="Ok" Icon="Information" />
</asp:Content>
