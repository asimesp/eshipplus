﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VendorInformationControlDetailControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls.VendorInformationControlDetailControl" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<asp:Panel runat="server" ID="pnlVendorInformationContact">
    <asp:ListView runat="server" ID="lstServices" ItemPlaceholderID="itemPlaceHolder">
        <LayoutTemplate>
            <table class="stripe">
                <tr>
                    <th style="width: 20%;">
                        Name
                    </th>
                    <th style="width: 25%;">
                        Contact Type
                    </th>
                    <th style="width: 15%;">
                        Phone
                    </th>
                    <th style="width: 30%;">
                        Email
                    </th>
                    <th style="width: 10%;" class="text-center">
                        Primary
                    </th>
                </tr>
                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
            </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <%#Eval("Name")%>
                </td>
                <td>
                    <%#Eval("Type")%>
                </td>
                <td>
                    <%#Eval("Phone")%>
                </td>
                <td >
                    <%#Eval("Email")%>
                </td>
                <td class="text-center">
                    <%# Eval("Primary").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                </td>
            </tr>
        </ItemTemplate>
    </asp:ListView>
</asp:Panel>
