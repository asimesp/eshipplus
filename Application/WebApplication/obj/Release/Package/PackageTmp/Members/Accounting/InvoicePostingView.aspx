﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="InvoicePostingView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.InvoicePostingView" EnableEventValidation="false"%>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowMore="True" OnCommand="OnToolbarCommand" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:UpdatePanel runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
                        Invoice Posting
                        <small class="ml10">
                            <asp:Literal runat="server" ID="litRecordCount" />
                        </small>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </h3>
            <div class="right pr5">
                <label>
                    <asp:CheckBox runat="server" ID="chkDoNotSendNotifications" Text="DO NOT SEND POSTING NOTIFICATIONS" CssClass="jQueryUniform" />
                </label>
            </div>
        </div>
        <span class="clearfix"></span>
        <hr class="dark mb10" />
        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="Invoices" ShowAutoRefresh="True"
                            OnProcessingError="OnSearchProfilesProcessingError" OnSearchProfileSelected="OnSearchProfilesProfileSelected"
                            OnAutoRefreshChanged="OnSearchProfilesAutoRefreshChanged" />
                    </div>
                </div>
            </div>
            <hr class="dark mb10" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                        OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>
            <hr class="fat" />
            <div class="rowgroup">
                <div class="row mb10">
                    <div class="left">
                        <asp:Button ID="btnSearch" CssClass="left" runat="server" Text="SEARCH" OnClick="OnSearchClicked"
                            CausesValidation="False" />
                    </div>
                    <div class="right">
                        <div class="fieldgroup">
                            <label>Sort By:</label>
                            <asp:DropDownList runat="server" ID="ddlSortBy" DataTextField="DisplayName" DataValueField="Name"
                                OnSelectedIndexChanged="OnSortBySelectedIndexChanged" AutoPostBack="True" />
                        </div>
                        <div class="fieldgroup mr10">
                            <asp:RadioButton runat="server" ID="rbAsc" Text="ASC" GroupName="rbSortBy" AutoPostBack="True"
                                ToolTip="Ascending" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                        </div>
                        <div class="fieldgroup">
                            <asp:RadioButton runat="server" ID="rbDesc" Text="DESC" GroupName="rbSortBy" Checked="True"
                                ToolTip="Descending" AutoPostBack="True" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>

        <script type="text/javascript">

            function SetCheckAllInvoicePosting(control) {
                $("#invoiceTable input:checkbox").each(function () {
                    this.checked = control.checked && this.disabled == false;
                    SetAltUniformCheckBoxClickedStatus(this);
                });
            }
        </script>

        <asp:UpdatePanel runat="server" ID="upDataUpdate">
            <ContentTemplate>
                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheViewRegistryTable" TableId="invoiceTable" HeaderZIndex="2" />
                <asp:Timer runat="server" ID="tmRefresh" Interval="30000" OnTick="OnTimerTick" />
                <div class="rowgroup">
                    <table id="invoiceTable" class="line2 pl2">
                        <tr>
                            <th style="width: 5%; <%= HasAccessToModifyInvoicePosting ? string.Empty : "display: none;"%>">
                                <eShip:AltUniformCheckBox ID="chkSelectAllRecords" runat="server"
                                    OnClientSideClicked="javascript:SetCheckAllInvoicePosting(this); " />
                            </th>
                            <th style="width: 15%;" class="no-left-border">
                                <asp:LinkButton runat="server" ID="lbtnSortInvoiceNumber" CssClass="link_nounderline blue middle"
                                    CausesValidation="False" OnCommand="OnSortData">
                                    Invoice Number
                                </asp:LinkButton>
                            </th>
                            <th style="width: 13%;">
                                <asp:LinkButton runat="server" ID="lbtnSortInvoiceDate" CssClass="link_nounderline blue"
                                    CausesValidation="False" OnCommand="OnSortData">
                            Invoice Date
                                </asp:LinkButton>
                            </th>
                            <th style="width: 21%;">
                                <asp:LinkButton runat="server" ID="lbtnSortUsername" CssClass="link_nounderline blue"
                                    CausesValidation="False" OnCommand="OnSortData">
                            User
                                </asp:LinkButton>
                            </th>
                            <th style="width: 13%;">
                                <asp:LinkButton runat="server" ID="lbtnSortTypeText" CssClass="link_nounderline blue"
                                    CausesValidation="False" OnCommand="OnSortData">
                            Type
                                </asp:LinkButton>
                            </th>
                            <th style="width: 13%;">
                                <asp:LinkButton runat="server" ID="lbtnSortAmountDue" CssClass="link_nounderline blue"
                                    CausesValidation="False" OnCommand="OnSortData">
                            Amount Due
                                </asp:LinkButton>
                            </th>
                            <th style="width: 13%;">
                                <asp:LinkButton runat="server" ID="lbtnSortPrefix" CssClass="link_nounderline blue"
                                    CausesValidation="False" OnCommand="OnSortData">
                            Prefix
                                </asp:LinkButton>
                            </th>
                            <th style="width: 7%;" class="text-center">Action</th>
                        </tr>
                        <asp:ListView runat="server" ID="lstInvoiceDetails" ItemPlaceholderID="itemPlaceHolder"
                            OnItemDataBound="OnInvoiceDetailsItemDataBound">
                            <LayoutTemplate>
                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <eShip:CustomHiddenField runat="server" ID="hidInvoiceId" Value='<%# Eval("Id") %>' />
                                <eShip:InvoicePostingDetailControl ID="invoicePostingDetail2" ItemIndex='<%# Container.DataItemIndex %>'
                                    runat="server" EnableMultiSelect="<%# HasAccessToModifyInvoicePosting %>" />
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lbtnSortInvoiceNumber" />
                <asp:PostBackTrigger ControlID="lbtnSortInvoiceDate" />
                <asp:PostBackTrigger ControlID="lbtnSortUsername" />
                <asp:PostBackTrigger ControlID="lbtnSortTypeText" />
                <asp:PostBackTrigger ControlID="lbtnSortAmountDue" />
                <asp:PostBackTrigger ControlID="lbtnSortPrefix" />
            </Triggers>
        </asp:UpdatePanel>
        <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
        <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information"
            OnOkay="OnOkayProcess" OnYes="OnYesProcess" OnNo="OnNoProcess" />
        <asp:Panel ID="pnlDimScreen" CssClass="dimBackgroundControl" runat="server" Visible="False" />
        <asp:Panel runat="server" ID="pnlDownloadDocument" Visible="False">
            <div class="popupControl popupControlOverW500">
                <div class="popheader">
                    <h4>Document Download
                    </h4>
                </div>
                <div class="row">
                    <table class="poptable">
                        <tr>
                            <td class="text-center">Documents are ready for download. Click Download to continue.
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">
                                <asp:Button runat="server" ID="btnDownloadDocuments" Text="Download" OnClick="OnDownloadDocumentsClicked" />
                                <asp:Button runat="server" ID="btnDownloadDocumentsClose" Text="Close" OnClick="OnDownloadDocumentsCloseClicked" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
        <eShip:CustomHiddenField runat="server" ID="hidFilePath" />
        <eShip:CustomHiddenField runat="server" ID="hidFileName" />
    </div>
</asp:Content>
