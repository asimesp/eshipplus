﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ApplyMiscReceiptsToInvoiceControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls.ApplyMiscReceiptsToInvoiceControl" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Administration" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Operations" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Panel runat="server" ID="pnlApplyMiscReceiptInvoiceToInvoice" CssClass="popupControl" Visible="False">
    <div class="popheader">
        <h4>Apply Misc Receipts To Invoice</h4>
        <asp:ImageButton runat="server" ID="ibtnCloseApplyMiscReceiptToInvoice" CausesValidation="False" CssClass="close" ImageUrl="~/images/icons2/close.png" OnClick="OnCloseApplyMiscReceiptToInvoiceClicked" />
    </div>
    <table class="stripe sm_chk" id="miscReceiptsToApplyTable">
        <tr>
            <th style="width: 3%">
                <eShip:AltUniformCheckBox ID="chkSelectAllRecords" runat="server" OnClientSideClicked="javascript:SetSelectedCheckBoxesStatus(this, 'miscReceiptsToApplyTable');" />
            </th>
            <th style="width: 13%;">Shipment Number
            </th>
            <th style="width: 18%;">User
            </th>
            <th style="width: 11%;" class="text-right">Appliable Amount
            </th>
            <th style="width: 20%;">Payment Date
            </th>
            <th style="width: 15%;">Gateway Transaction Id
            </th>
            <th style="width: 12%;">Payment Gateway Type
            </th>
            <th style="width: 8%" class="text-right">Amount To Apply
            </th>
        </tr>
        <asp:Repeater runat="server" ID="rptMiscReceipts">
            <ItemTemplate>
                <tr>
                    <td>
                        <eShip:AltUniformCheckBox runat="server" ID="chkSelected" OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'miscReceiptsToApplyTable', 'chkSelectAllRecords');" />
                    </td>
                    <td>
                        <eShip:CustomHiddenField runat="server" ID="hidMiscReceiptId" Value='<%# Eval("Id") %>' />
                        <%# Eval("ShipmentNumber") %>
                        <%# ActiveUser.HasAccessTo(ViewCode.Shipment) && !string.IsNullOrEmpty(Eval("ShipmentNumber").ToString()) 
                                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Shipment Record'><img src='{3}' alt='' width='16' align='absmiddle'/></a>", 
                                                    ResolveUrl(ShipmentView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    Eval("ShipmentNumber").GetString(),
                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                : string.Empty  %>
                    </td>
                    <td>
                        <%# Eval("UserUsername").GetString() %>
                        <%# ActiveUser.HasAccessTo(ViewCode.User) 
                                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To User Record'><img src='{3}' alt='' width='16' align='absmiddle'/></a>", 
                                                    ResolveUrl(UserView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    Eval("UserId").GetString().UrlTextEncrypt(),
                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                : string.Empty  %>
                    </td>
                    <td class="text-right">
                        <%# Eval("AppliableAmount").ToDecimal().ToString("c2") %>
                    </td>
                    <td>
                        <%# Eval("PaymentDate").FormattedLongDate() %>
                    </td>
                    <td>
                        <%# Eval("GatewayTransactionId") %>
                    </td>
                    <td>
                        <%# Eval("PaymentGatewayType") %>
                    </td>
                    <td class="text-right">
                        <eShip:CustomTextBox runat="server" ID="txtAmountToApplyFromMiscReceipt" CssClass="w80" Type="FloatingPointNumbers" Text='<%# Eval("AppliableAmount").ToDecimal().ToString("f4") %>' />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
    <table class="poptable">
        <tr>
            <td style="width: 75%" class="pt10 pb10"></td>
            <td class="pt10 pb10">
                <asp:Button runat="server" ID="btnApplyMiscReceiptsToInvoice" Text="Update Invoice Amount Paid" OnClick="OnApplyMiscReceiptsToInvoiceClicked" CausesValidation="False" />
                <asp:Button runat="server" ID="btnCloseApplyMiscReceiptToInvoice" Text="Cancel" OnClick="OnCloseApplyMiscReceiptToInvoiceClicked" CausesValidation="False" />
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="pnlApplyMiscReceiptInvoiceToInvoiceDimScreen" CssClass="dimBackgroundControl" runat="server" Visible="false" />

<eShip:CustomHiddenField runat="server" ID="hidInvoiceId" />
<eShip:CustomHiddenField runat="server" ID="hidFlag"/> 
