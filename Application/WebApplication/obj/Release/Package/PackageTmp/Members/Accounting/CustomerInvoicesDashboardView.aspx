﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="CustomerInvoicesDashboardView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.CustomerInvoicesDashboardView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Operations" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowNew="false" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Customer Invoices Dashboard
                <small class="ml10">
                    <asp:Literal runat="server" ID="litRecordCount" Text="" /></small>
            </h3>
        </div>
        <span class="clearfix"></span>
        <hr class="dark mb10" />

        <asp:Panel runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="CustomerInvoices" OnProcessingError="OnSearchProfilesProcessingError"
                            OnSearchProfileSelected="OnSearchProfilesProfileSelected" />
                    </div>
                </div>
            </div>
            <hr class="dark mb10" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                        OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>
            <hr class="fat" />
            <div class="rowgroup">
                <div class="row mb10">
                    <div class="fieldgroup">
                        <asp:Button ID="btnSearch" CssClass="left" runat="server" Text="SEARCH" OnClick="OnSearchClicked"
                            CausesValidation="False" />
                    </div>
                    <div class="fieldgroup right">
                        <label>Sort By:</label>
                        <asp:DropDownList runat="server" ID="ddlSortBy" DataTextField="DisplayName" DataValueField="Name"
                            OnSelectedIndexChanged="OnSortBySelectedIndexChanged" AutoPostBack="True" />
                        <asp:RadioButton runat="server" ID="rbAsc" Text="ASC" GroupName="rbSortBy" AutoPostBack="True"
                            ToolTip="Ascending" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform mr10" />
                        <asp:RadioButton runat="server" ID="rbDesc" Text="DESC" GroupName="rbSortBy" Checked="True"
                            ToolTip="Descending" AutoPostBack="True" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                    </div>
                </div>
                <hr class="dotted" />

                <div class="col_1_3 no-right-border">
                    <div class="row">
                        <div class="fieldgroup">
                            <label>Include documents with Invoice as document pack (*.zip):</label>
                        </div>
                    </div>
                </div>
                <div class="col_2_3">
                    <div class="row">
                        <ul class="chk_list twocol_list">
                            <asp:Repeater runat="server" ID="rptDocTagsToDownload">
                                <ItemTemplate>
                                    <li>
                                        <asp:CheckBox runat="server" ID="chkSelected" CssClass="jQueryUniform" />
                                        <eShip:CustomHiddenField runat="server" ID="hidDocumentTagId" Value='<%# Eval("Value") %>' />
                                        <label><%# Eval("Text") %></label>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:UpdatePanel runat="server" ID="upDataUpdate">
            <ContentTemplate>
                <eShip:ContinuousScrollExtender ID="upcseDataUpdate" runat="server" BindableControlId="lstInvoiceDetails" UpdatePanelToExtendId="upDataUpdate" 
                    SelectionCheckBoxControlId="chkSelected" SelectionUniqueRefHiddenFieldId="hidInvoiceId" PreserveRecordSelection="True"/>
                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheInvoiceDashboardTable" TableId="customerInvoiceDashboardTable" HeaderZIndex="2" />

                <script type="text/javascript">
                    $(function() {
                        UpdateTotal();
                        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                            UpdateTotal();
                        });
                    });

                    function UpdateTotal() {
                        var total = 0;
                        $("[id='trInvoice'] input:checkbox").each(function () {
                            if (this.checked) total += Number($(this).closest('#trInvoice').find('[id$="hidBalance"]').val());
                        });
                        SetTotalAndDisplayVisibility(total);
                    }
                    
                    function ClearSelectedInvoices() {
                        $("#customerInvoiceDashboardTable input:checkbox").each(function () {
                            this.checked = false;
                            SetAltUniformCheckBoxClickedStatus(this);
                        });

                        SetTotalAndDisplayVisibility(0);
                    }
                    
                    function SetTotalAndDisplayVisibility(total) {
                        $("#spanSelectedTotal").prop('innerHTML', total.toFixed(2));
                        if (total <= 0) $("#divRunningTotal").hide();
                        else $("#divRunningTotal").show();
                    }
                </script>
                <div class="rowgroup">
                    <table class="line2 pl2" id="customerInvoiceDashboardTable">
                        <tr>
                            <th style="width: 5%;">
                                <eShip:AltUniformCheckBox ID="chkSelectAllRecords" runat="server" OnClientSideClicked="javascript:SetSelectedCheckBoxesStatus(this, 'customerInvoiceDashboardTable'); UpdateTotal();" />
                            </th>
                            <th style="width: 10%;">
                                <asp:LinkButton runat="server" ID="lbtnSortInvoiceNumber" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
								Invoice
                                </asp:LinkButton>
                            </th>
                            <th style="width: 13%;">
                                <asp:LinkButton runat="server" ID="lbtnSortPostDate" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                            Posted Date
                                </asp:LinkButton>
                            </th>
                            <th style="width: 13%;">
                                <asp:LinkButton runat="server" ID="lbtnSortDueDate" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                            Due Date
                                </asp:LinkButton>
                            </th>
                            <th style="width: 13%;">
                                <asp:LinkButton runat="server" ID="lbtnSortPaidAmount" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                            Paid Amount
                                </asp:LinkButton>
                            </th>
                            <th style="width: 13%;">
                                <asp:LinkButton runat="server" ID="lbtnSortAmountDue" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                            <abbr title="Amount">Amt.</abbr>
                            Due /Credit
                                </asp:LinkButton>
                            </th>
                            <th style="width: 23%;">Customer
				            <asp:LinkButton runat="server" ID="lbtnSortCustomerNumber" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                                        Number
                            </asp:LinkButton>/
				            <asp:LinkButton runat="server" ID="lbtnSortCustomerName" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                                        Name
                            </asp:LinkButton>
                            </th>
                            <th style="width: 8%;">
                                <asp:LinkButton runat="server" ID="lbtnSortTypeText" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                            Type
                                </asp:LinkButton>
                            </th>
                            <th style="width: 7%;" class="text-center">Action</th>
                        </tr>
                        <asp:ListView runat="server" ID="lstInvoiceDetails" ItemPlaceholderID="itemPlaceHolder">
                            <LayoutTemplate>
                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr id="trInvoice">
                                    <td class="top" rowspan="2">
                                        <eShip:AltUniformCheckBox runat="server" ID="chkSelected" OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'customerInvoiceDashboardTable', 'chkSelectAllRecords'); UpdateTotal();"
                                            Visible='<%# ActiveUser.HasAccessTo(ViewCode.InvoicePay) && new Customer(Eval("CustomerId").ToLong()).CanPayByCreditCard && Eval("InvoiceType").ToInt().ToEnum<InvoiceType>() != InvoiceType.Credit &&  Eval("AmountDue").ToDecimal() - Eval("PaidAmount").ToDecimal() > 0 %>' />
                                    </td>
                                    <td class="text-left top">
                                        <%# Eval("InvoiceNumber") %>
                                        <eShip:CustomHiddenField ID="hidBalance" runat="server" Value='<%# Eval("AmountDue").ToDecimal() - Eval("PaidAmount").ToDecimal() %>' />
                                        <eShip:CustomHiddenField ID="hidInvoiceId" runat="server" Value='<%# Eval("Id") %>' />
                                        <asp:ImageButton runat="server" ID="ibtnInvoiceNumber" OnClick="OnGenerateInvoiceRequest" CausesValidation="False" ImageUrl="~/images/icons2/downloadDocument.png" Width="14" />
                                    </td>
                                    <td>
                                        <%# Eval("PostDate").ToDateTime().FormattedShortDateSuppressEarliestDate() %>
                                    </td>
                                    <td>
                                        <%# Eval("DueDate").ToDateTime().FormattedShortDateSuppressEarliestDate() %>
                                    </td>
                                    <td>
                                        <%# Eval("InvoiceType").ToInt().ToEnum<InvoiceType>() == InvoiceType.Credit ? string.Empty : Eval("PaidAmount", "{0:c2}") %>
                                    </td>
                                    <td>
                                        <%# Eval("AmountDue", "{0:c2}") %>
                                    </td>
                                    <td>
                                        <%# Eval("CustomerNumber") %> - <%# Eval("CustomerName") %>
                                        <%# ActiveUser.HasAccessTo(ViewCode.Customer) 
                                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Customer Record'><img src='{3}' alt='' width='16' align='absmiddle'/></a>", 
                                                    ResolveUrl(CustomerView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    Eval("CustomerId").GetString().UrlTextEncrypt(),
                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                : string.Empty  %>
                                    </td>
                                    <td>
                                        <asp:Literal runat="server" ID="litType" />
                                        <%# Eval("InvoiceType").ToInt().ToEnum<InvoiceType>().FormattedString() %>
                                    </td>
                                    <td rowspan="2" class="top text-center">
                                        <%# string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='View Documents'><img src='{3}' alt='' width='16' align='absmiddle'/></a>", 
                                                    ResolveUrl(CustomerInvoiceDocumentView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    Eval("InvoiceNumber").GetString().UrlTextEncrypt(),
                                                    ResolveUrl("~/images/icons2/document.png")) %>
                                    </td>
                                </tr>
                                <tr class="f9">
                                    <td colspan="7" class="forceLeftBorder">
                                        <div class="rowgroup">
                                            <div class="col_1_3 no-right-border">
                                                <div class="row">
                                                    <label class="wlabel blue">Address</label>
                                                    <label>
                                                        <%# string.Format("{0}{1}{2}{3}{4}, {5} {6} {7}",
                                                                Eval("LocationStreet1"),
                                                                WebApplicationConstants.HtmlBreak, Eval("LocationStreet2"),
                                                                string.IsNullOrEmpty(Eval("LocationStreet2").ToString())
                                                                    ? string.Empty
                                                                    : WebApplicationConstants.HtmlBreak,
                                                                Eval("LocationCity"), Eval("LocationState"), Eval("LocationPostalCode"),
                                                                Eval("LocationCountryName")) %>
                                                    </label>
                                                </div>
                                                <div class="row">
                                                    <label>
                                                        <%# string.IsNullOrEmpty(Eval("SpecialInstruction").ToString()) 
                                                            ? string.Empty
                                                            : string.Format("<span class='blue'><abbr title='Special Instructions'>I</abbr>:</span> {0}", Eval("SpecialInstruction")) %>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col_1_3 no-right-border">
                                                <div class="row">
                                                    <label class="wlabel blue">Shipments</label>
                                                    <label>
                                                        <%#  ActiveUser.HasAccessTo(ViewCode.Shipment)
                                                            ? ShipmentView.BuildShipmentLinks(Eval("Shipments").ToString()).Replace("~", Request.ResolveSiteRootWithHttp())
                                                            : Eval("Shipments") %>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col_1_3 no-right-border">
                                                <div class="row">
                                                    <label class="wlabel blue">Service Tickets</label>
                                                    <label>
                                                        <%#  ActiveUser.HasAccessTo(ViewCode.ServiceTicket)
                                                            ? ServiceTicketView.BuildServiceTicketLinks(Eval("ServiceTickets").ToString()).Replace("~", Request.ResolveSiteRootWithHttp())
                                                            : Eval("ServiceTickets") %>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lbtnSortInvoiceNumber" />
                <asp:PostBackTrigger ControlID="lbtnSortPostDate" />
                <asp:PostBackTrigger ControlID="lbtnSortDueDate" />
                <asp:PostBackTrigger ControlID="lbtnSortPaidAmount" />
                <asp:PostBackTrigger ControlID="lbtnSortAmountDue" />
                <asp:PostBackTrigger ControlID="lbtnSortCustomerNumber" />
                <asp:PostBackTrigger ControlID="lbtnSortCustomerName" />
                <asp:PostBackTrigger ControlID="lbtnSortTypeText" />
                <asp:PostBackTrigger ControlID="lstInvoiceDetails" />
            </Triggers>
        </asp:UpdatePanel>
    </div>

    <div class="invoiceTotalSection top_shadow " id="divRunningTotal" style="display:none; ">
        <div align="center" class="blue pt10 pb5 fs150em" >
            Selected Total: $ <span id="spanSelectedTotal" class="w100">0</span> 
            
            <asp:Button runat="server" ID="btnPayForInvoices" Text="Pay For Invoices" OnClick="OnPayForInvoicesClicked" CssClass="ml20 mb5"/>
            <asp:Button runat="server" ID="btnClearSelection" Text="Clear Selected" OnClientClick="ClearSelectedInvoices(); return false;" CssClass="mb5"/>
        </div>
    </div>

    <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
    <eShip:DocumentDisplayControl runat="server" ID="documentDisplay" Visible="false" OnClose="OnDocumentDisplayClosed" />
    <eShip:CustomHiddenField runat="server" ID="hidLastInvoiceId" />
</asp:Content>
