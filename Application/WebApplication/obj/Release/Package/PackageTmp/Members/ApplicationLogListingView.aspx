﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="ApplicationLogListingView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.ApplicationLogListingView" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Application Log Listing<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>

        <hr class="fat" />

        <div class="rowgroup mb0">
            <div class="row mb10">
                <div class="fieldgroup">
                    <label class="upper blue">PURGE LOGS EARLIER THAN:</label>
                    <eShip:CustomTextBox runat="server" ID="txtCutOffDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                    <asp:Button ID="btnPurge" runat="server" Text="PURGE" OnClick="OnPurgeClicked" CausesValidation="true" />
                </div>
            </div>
        </div>
        
        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheAppLogListingTable" TableId="applicationLogListingTable" HeaderZIndex="2"/>
        <asp:ListView ID="lvwApplicationLog" runat="server" ItemPlaceholderID="itemPlaceHolder">
            <LayoutTemplate>
                <table id="applicationLogListingTable" class="line2 pl2">
                    <tr>
                        <th style="width: 50%">File Name
                        </th>
                        <th style="width: 50%">Date Created
                        </th>
                    </tr>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <asp:LinkButton ID="lnkDisplayApplicationLog" runat="server" CommandArgument='<%# Eval("FileName") %>' Text='<%# Eval("FileName") %>' OnCommand="OnDisplayApplicationLogClicked"
                            CausesValidation="False" CssClass="blue"/>
                    </td>
                    <td>
                        <%# Eval("DateCreated") %>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
        <asp:Panel runat="server" ID="pnlApplicationLogDisplay" Visible="False" CssClass="popupControl">
            <script type="text/javascript">
                // maintains scroll position over partial postbacks
                $(document).ready(function () {
                    $(jsHelper.AddHashTag('divApplicationLogContent')).scroll(function () {
                        $(jsHelper.AddHashTag('<%= hidScrollPosition.ClientID%>')).val($(this).scrollTop());
                    });

                    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                        $(jsHelper.AddHashTag('divApplicationLogContent')).scroll(function () {
                            $(jsHelper.AddHashTag('<%= hidScrollPosition.ClientID%>')).val($(this).scrollTop());
                        });
                        $(jsHelper.AddHashTag('divApplicationLogContent')).scrollTop(Number(($(jsHelper.AddHashTag('<%= hidScrollPosition.ClientID %>')).val())));
                    });
                });
            </script>
            <eShip:CustomHiddenField runat="server" ID="hidScrollPosition" />
            <asp:UpdatePanel runat="server" ID="udpApplicationLogDisplay">
                <ContentTemplate>
                    <div class="popheader">
                        <h4>Application Log:
                            <asp:Literal runat="server" ID="litApplicationLogTitle" />
                            <asp:LinkButton runat="server" ID="btnRefresh" CssClass="ml10" OnClick="OnRefreshClicked" CausesValidation="False">
                                <asp:Image ID="imgRefresh" runat="server" ImageUrl="~/images/icons2/refresh.png" ToolTip="Refresh"
                                    AlternateText="Refresh" />
                            </asp:LinkButton>
                        </h4>
                        <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                            CausesValidation="false" OnClick="OnCloseApplicationLogDisplayClicked" runat="server" />
                    </div>
                    <div class="finderScroll" id="divApplicationLogContent">
                        <table>
                            <tr class="text-left mb10">
                                <td class="pl10 pr10">
                                    <asp:Literal runat="server" ID="litApplicationLogContent" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="ibtnClose" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
        <asp:Panel ID="pnlApplicationLogDisplayDimsScreen" CssClass="dimBackground" runat="server" Visible="false" />
    </div>
</asp:Content>
