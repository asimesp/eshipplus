﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RateNoticeControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.RateNoticeControl" %>
<script type="text/javascript">
    $(document).ready(function () {
        $(jsHelper.AddHashTag('<%= btnRatingNoticeClose.ClientID %>')).click(function (e) {
            e.preventDefault();
            $("div[id$='pnlRateNotice']").hide("slow");
            $("div[id$='pnlDimScreenJS']").hide();
        });
    });
</script>

<asp:Panel runat="server" ID="pnlRateNotice" CssClass="popupControl popupControlOverW750" Style="display: none;">
    <div class="popheader">
        <h4>Rate Notice</h4>
    </div>
    <div class="row pl20 pt10 pb20 pr20 wAuto">
        <asp:Literal ID="litPhone" runat="server" /><br />
        <asp:Label ID="lblRatingNotice" runat="server" />
    </div>
    <div class="pb10">
        <hr class="w100p ml-inherit p_m0" />
    </div>
    <div class="row pl20 fs85em">
        To disable this notice:
        <ul>
            <li>Go to your User Profile by following this link : 
                <asp:LinkButton runat="server" Text="Click Here" CausesValidation="false" CssClass="blue" OnClick="OnDisableNoticeClicked" />
            </li>
            <li>Uncheck the checkbox entitled 'Always Show Rating Notice'
            </li>
            <li>Click the 'Save' button to confirm the change
            </li>
        </ul>
    </div>
    <div class="row pb10">
        <div class="fieldgroup right">
            <asp:Button ID="btnRatingNoticeClose" runat="server" Text="Close" CausesValidation="False" />
        </div>
    </div>
</asp:Panel>
