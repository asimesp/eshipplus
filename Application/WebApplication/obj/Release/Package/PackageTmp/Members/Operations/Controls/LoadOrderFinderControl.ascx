﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoadOrderFinderControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.LoadOrderFinderControl" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<asp:Panel runat="server" ID="pnlLoadOrderFinderContent" CssClass="popupControl" DefaultButton="btnSearch">
    <div class="popheader">
        <h4>Load Order Finder<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
            <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                CausesValidation="false" OnClick="OnCancelClicked" runat="server" />
        </h4>
    </div>
    <script type="text/javascript">
        $(function () {
            if (jsHelper.IsTrue($('#<%= hidAreFiltersShowing.ClientID%>').val())) {
                $('#<%= pnlSearch.ClientID%>').show();
                $('#<%= pnlSearchShowFilters.ClientID%>').hide();
            } else {
                $('#<%= pnlSearch.ClientID%>').hide();
                $('#<%= pnlSearchShowFilters.ClientID%>').show();
            }
        });

        function ToggleFilters() {
            $('#<%= hidAreFiltersShowing.ClientID%>').val(jsHelper.IsTrue($('#<%= hidAreFiltersShowing.ClientID%>').val()) ? false : true);
            $('#<%= pnlSearch.ClientID%>').toggle();
            $('#<%= pnlSearchShowFilters.ClientID%>').toggle();
            var top = $('#<%= pnlLoadOrderFinderContent.ClientID%> div[class="finderScroll"]').offset().top - $('#<%= pnlLoadOrderFinderContent.ClientID%>').offset().top - 2;
            $('#<%= pnlLoadOrderFinderContent.ClientID%> div[class="finderScroll"] > div[id^="hd"]').css('top', top + 'px');
        }
    </script>
    <table class="poptable">
        <tr>
            <td>
                <asp:Panel runat="server" ID="pnlSearchShowFilters" class="rowgroup mb0" style="display:none;">
                    <div class="row">
                        <div class="fieldgroup">
                            <asp:Button ID="btnProcessSelected2" runat="server" Text="ADD SELECTED" OnClick="OnProcessSelectedClicked" CssClass="ml10" CausesValidation="false" />
                        </div>
                        <div class="fieldgroup right">
                            <button onclick="ToggleFilters(); return false;">Show Filters</button>
                        </div>
                        <div class="fieldgroup right mb5 mt5 mr10">
                            <span class="fs75em  ">*** If you would like to show parameters when searching change the 'Always Show Finder Parameters On Search' option on your <asp:HyperLink ID="hypGoToUserProfile" Target="_blank" CssClass="blue" runat="server" Text="User Profile"/></span>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlSearch" DefaultButton="btnSearch">
                    <div class="mt5">
                        <div class="rowgroup mb0">
                            <div class="row">
                                <div class="fieldgroup">
                                    <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                                        CausesValidation="False" CssClass="add" />
                                </div>
                                <div class="fieldgroup right">
                                    <button onclick="ToggleFilters(); return false;">Hide Filters</button>
                                </div>
                                <div class="right">
                                    <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="LoadOrders" ShowAutoRefresh="False"
                                        OnProcessingError="OnSearchProfilesProcessingError" OnSearchProfileSelected="OnSearchProfilesProfileSelected" />
                                </div>
                            </div>
                        </div>


                        <hr class="mb5" />

                        <script type="text/javascript">$(window).load(function () { SetControlFocus(); });</script>
                        <table class="mb5" id="parametersTable">
                            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                                OnItemDataBound="OnParameterItemDataBound">
                                <LayoutTemplate>
                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                                        OnRemoveParameter="OnParameterRemove" />
                                </ItemTemplate>
                            </asp:ListView>
                        </table>
                        <div class="pl10 row mb5">
                            <asp:Button ID="btnSearch" runat="server" Text="SEARCH" OnClick="OnSearchClicked" CausesValidation="False" />
                            <asp:Button ID="btnCancel" OnClick="OnCancelClicked" runat="server" Text="CANCEL" CssClass="ml10" CausesValidation="false" />
                            <asp:Button ID="btnProcessSelected" runat="server" Text="ADD SELECTED" OnClick="OnProcessSelectedClicked" CssClass="ml10" CausesValidation="false" />
                            <label class="right pr10">
                                Sort:
                                <asp:Literal runat="server" ID="litSortOrder" Text='<%# WebApplicationConstants.Default %>' />
                            </label>
                        </div>
                    </div>
                </asp:Panel>
            </td>
        </tr>
    </table>

    <asp:UpdatePanel runat="server" ID="upDataUpdate">
        <ContentTemplate>
            <eShip:ContinuousScrollExtender ID="upcseDataUpdate" runat="server" BindableControlId="lstLoadOrders" UpdatePanelToExtendId="upDataUpdate" ScrollableDivId="loadOrderFinderScrollSection"
                SelectionCheckBoxControlId="chkSelected" SelectionUniqueRefHiddenFieldId="hidLoadOrderId" />
            <eShip:TableFreezeHeaderExtender runat="server" ID="tfheUsersFinderTable" TableId="loadOrderFinderTable" ScrollerContainerId="loadOrderFinderScrollSection" ScrollOffsetControlId="pnlLoadOrderFinderContent"
                IgnoreHeaderBackgroundFill="True" HeaderZIndex="2050" />
            <div class="finderScroll" id="loadOrderFinderScrollSection">
                <table id="loadOrderFinderTable" class="stripe sm_chk">
                    <tr>
                        <th style="width: 15%">
                            <eShip:AltUniformCheckBox ID="chkSelectAllRecords" runat="server" OnClientSideClicked="javascript:SetSelectedCheckBoxesStatus(this, 'loadOrderFinderTable');" />
                        </th>
                        <th style="width: 9%;">
                            <asp:LinkButton runat="server" ID="lbtnSortShipmentNumber" CssClass="link_nounderline white" CausesValidation="False" OnCommand="OnSortData">
												<abbr title="Shipment Number">Load #</abbr>
                            </asp:LinkButton>
                        </th>
                        <th style="width: 10%;">
                            <asp:LinkButton runat="server" ID="lbtnSortDesiredPickup" CssClass="link_nounderline white" CausesValidation="False" OnCommand="OnSortData">
												<abbr title="Desired Pickup Date">D. Pickup</abbr>
                            </asp:LinkButton>
                        </th>
                        <th style="width: 11%;">
                            <asp:LinkButton runat="server" ID="lbtnSortActualPickup" CssClass="link_nounderline white" CausesValidation="False" OnCommand="OnSortData">
											  <abbr title="Actual Pickup Date">A. Pickup</abbr>
                            </asp:LinkButton>
                        </th>
                        <th style="width: 10%;">
                            <asp:LinkButton runat="server" ID="lbtnSortEstimatedDelivery" CssClass="link_nounderline white" CausesValidation="False" OnCommand="OnSortData">
											 <abbr title="Estimated Delivery Date">Est. Del.</abbr>
                            </asp:LinkButton>
                        </th>
                        <th style="width: 26%;">Customer
								<asp:LinkButton runat="server" ID="lbtnSortCustomerNumber" Text="Number" CssClass="link_nounderline white" CausesValidation="False" OnCommand="OnSortData" />
                            /<asp:LinkButton runat="server" ID="lbtnSortCustomerName" CausesValidation="False" Text="Name" CssClass="link_nounderline white" OnCommand="OnSortData" />
                        </th>
                        <th style="width: 16%;">
                            <asp:LinkButton runat="server" ID="lbtnSortStatus" Text="Status" CssClass="link_nounderline white" CausesValidation="False" OnCommand="OnSortData" />
                        </th>
                    </tr>
                    <asp:ListView ID="lstLoadOrders" runat="server" ItemPlaceholderID="itemPlaceHolder">
                        <LayoutTemplate>
                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Button ID="btnSelect" runat="server" Text="Select" OnClick="OnSelectClicked"
                                        Visible='<%# !EnableMultiSelection %>' CausesValidation="false" />
                                    <asp:Button ID="btnEditSelect" runat="server" Text="Edit" OnClick="OnEditSelectClicked"
                                        Visible='<%# !EnableMultiSelection && OpenForEditEnabled %>' CausesValidation="false" />
                                    <eShip:AltUniformCheckBox runat="server" ID="chkSelected" Visible='<%# EnableMultiSelection %>'
                                        OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'loadOrderFinderTable', 'chkSelectAllRecords');" />
                                    <eShip:CustomHiddenField ID="hidLoadOrderId" Value='<%# Eval("Id") %>' runat="server" />
                                </td>
                                <td>
                                    <%# Eval("ShipmentNumber")  %>
                                </td>
                                <td>
                                    <%# Eval("DesiredPickupDate").ToDateTime().TimeToMinimum() != DateUtility.SystemEarliestDateTime ?  Eval("DesiredPickupDate").FormattedShortDate() : string.Empty  %>
                                </td>
                                <td>
                                    <%# Eval("ActualPickupDate").ToDateTime().TimeToMinimum() != DateUtility.SystemEarliestDateTime ?  Eval("ActualPickupDate").FormattedShortDate() : string.Empty  %>
                                </td>
                                <td>
                                    <%# Eval("EstimatedDeliveryDate").ToDateTime().TimeToMinimum() != DateUtility.SystemEarliestDateTime ?  Eval("EstimatedDeliveryDate").FormattedShortDate() : string.Empty  %>
                                </td>
                                <td>
                                    <%# Eval("CustomerNumber")  %> - <%# Eval("CustomerName")  %>
                                </td>
                                <td>
                                    <%# Eval("StatusText") %>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnSortShipmentNumber" />
            <asp:PostBackTrigger ControlID="lbtnSortDesiredPickup" />
            <asp:PostBackTrigger ControlID="lbtnSortActualPickup" />
            <asp:PostBackTrigger ControlID="lbtnSortEstimatedDelivery" />
            <asp:PostBackTrigger ControlID="lbtnSortCustomerNumber" />
            <asp:PostBackTrigger ControlID="lbtnSortCustomerName" />
            <asp:PostBackTrigger ControlID="lbtnSortStatus" />
            <asp:PostBackTrigger ControlID="lstLoadOrders" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Panel>
<asp:Panel ID="pnlLoadOrderFinderDimScreen" CssClass="dimBackgroundControl" runat="server"
    Visible="false" />
<eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
<eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information"/>
<eShip:CustomHiddenField ID="hidEditSelected" runat="server" />
<eShip:CustomHiddenField ID="hidOpenForEditEnabled" runat="server" />
<eShip:CustomHiddenField ID="hidOnlyActiveResults" runat="server" />
<eShip:CustomHiddenField ID="hidLoadOrderFinderEnableMultiSelection" runat="server" />
<eShip:CustomHiddenField ID="hidLocationItemIndex" runat="server" />
<eShip:CustomHiddenField ID="hidLoadOrderToExludeAttachedToJob" runat="server" />
<eShip:CustomHiddenField runat="server" ID="hidSortAscending" />
<eShip:CustomHiddenField runat="server" ID="hidSortField" />
<eShip:CustomHiddenField runat="server" ID="hidAreFiltersShowing" Value="true" />