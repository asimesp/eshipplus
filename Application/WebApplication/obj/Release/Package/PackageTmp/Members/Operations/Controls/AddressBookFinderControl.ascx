﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddressBookFinderControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.AddressBookFinderControl" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>

<asp:Panel runat="server" ID="pnlAddressBookFinderContent" CssClass="popupControl" DefaultButton="btnSearch">
    <div class="popheader">
        <h4>Address Book Finder <small class="ml10">
            <asp:Literal runat="server" ID="litRecordCount" Text="" /></small>
            <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                CausesValidation="false" OnClick="OnCancelClicked" runat="server" /></h4>
    </div>
    <script type="text/javascript">
        $(function () {
            if (jsHelper.IsTrue($('#<%= hidAreFiltersShowing.ClientID%>').val())) {
                $('#<%= pnlSearch.ClientID%>').show();
                $('#<%= pnlSearchShowFilters.ClientID%>').hide();
            } else {
                $('#<%= pnlSearch.ClientID%>').hide();
                $('#<%= pnlSearchShowFilters.ClientID%>').show();
            }
        });

        function ToggleFilters() {
            $('#<%= hidAreFiltersShowing.ClientID%>').val(jsHelper.IsTrue($('#<%= hidAreFiltersShowing.ClientID%>').val()) ? false : true);
            $('#<%= pnlSearch.ClientID%>').toggle();
            $('#<%= pnlSearchShowFilters.ClientID%>').toggle();
            var top = $('#<%= pnlAddressBookFinderContent.ClientID%> div[class="finderScrollFixed"]').offset().top - $('#<%= pnlAddressBookFinderContent.ClientID%>').offset().top - 2;
            $('#<%= pnlAddressBookFinderContent.ClientID%> div[class="finderScrollFixed"] > div[id^="hd"]').css('top', top + 'px');
        }
    </script>
    <table id="finderTable" class="poptable">
        <tr>
            <td>
                <asp:Panel runat="server" ID="pnlSearchShowFilters" class="rowgroup mb0" style="display:none;">
                    <div class="row">
                        <div class="fieldgroup right">
                            <button onclick="ToggleFilters(); return false;">Show Filters</button>
                        </div>
                        <div class="fieldgroup right mb5 mt5 mr10">
                            <span class="fs75em  ">*** If you would like to show parameters when searching change the 'Always Show Finder Parameters On Search' option on your <asp:HyperLink ID="hypGoToUserProfile" Target="_blank" CssClass="blue" runat="server" Text="User Profile"/></span>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
                    <div class="mt5">
                        <div class="rowgroup mb0">
                            <div class="row">
                                <div class="fieldgroup">
                                    <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                                        CausesValidation="False" CssClass="add" />
                                </div>
                                <div class="fieldgroup right">
                                    <button onclick="ToggleFilters(); return false;">Hide Filters</button>
                                </div>
                                <div class="right">
                                    <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="AddressBooks" ShowAutoRefresh="False"
                                        OnProcessingError="OnSearchProfilesProcessingError" OnSearchProfileSelected="OnSearchProfilesProfileSelected" />
                                </div>
                            </div>
                        </div>
                        <hr class="mb5" />
                        <script type="text/javascript">$(window).load(function () { SetControlFocus(); });</script>
                        <table class="mb5" id="parametersTable">
                            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                                OnItemDataBound="OnParameterItemDataBound">
                                <LayoutTemplate>
                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                                        OnRemoveParameter="OnParameterRemove" />
                                </ItemTemplate>
                            </asp:ListView>
                        </table>
                        <table class="poptable mb5">
                            <tr>
                                <td style="width: 40%">
                                    <asp:Button ID="btnSearch" CssClass="left" runat="server" Text="SEARCH" OnClick="OnSearchClicked"
                                        CausesValidation="False" />
                                    <asp:Button ID="btnCancel" OnClick="OnCancelClicked" runat="server" Text="CANCEL" CssClass="ml10"
                                        CausesValidation="false" />
                                    <asp:Button ID="btnDeleteSelected" OnClick="OnDeleteSelectedClicked" runat="server" CssClass="ml10"
                                        Text="DELETE SELECTED" CausesValidation="false" />
                                </td>
                                <td>
                                    <span class="note green">&#8226; Result set is first 100 matches</span>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <script type="text/javascript">
        function ToggleContactsVisibility(index) {
            $("#divContacts" + index).slideToggle("fast");
        }
    </script>
    <asp:UpdatePanel runat="server" ID="upDataUpdate">
        <ContentTemplate>
            <eShip:ContinuousScrollExtender ID="upcseDataUpdate" runat="server" BindableControlId="lstAddressBooks" UpdatePanelToExtendId="upDataUpdate" ScrollableDivId="addressFinderScrollSection"
                SelectionCheckBoxControlId="chkMarkForDelete" />
            <eShip:TableFreezeHeaderExtender runat="server" ID="tfheAddressTable" TableId="addressBookTable" ScrollerContainerId="addressFinderScrollSection" ScrollOffsetControlId="pnlAddressBookFinderContent"
                IgnoreHeaderBackgroundFill="True" HeaderZIndex="2050" />
            <div class="finderScrollFixed" id="addressFinderScrollSection">
                <asp:ListView ID="lstAddressBooks" runat="server" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnAddressBooksItemDataBound">
                    <LayoutTemplate>
                        <table id="addressBookTable" class="stripe sm_chk">
                            <tr>
                                <th style="width: 12%">&nbsp;</th>
                                <th style="width: 5%" class="text-left">
                                    <eShip:AltUniformCheckBox ID="chkSelectAllRecords" runat="server" OnClientSideClicked="javascript:SetSelectedCheckBoxesStatus(this, 'addressBookTable');" />
                                </th>
                                <th style="width: 15%;">Customer
                                </th>
                                <th style="width: 20%">Name/Description
                                </th>
                                <th style="width: 44%;">Address
                                </th>
                                <th style="width: 4%;"></th>
                            </tr>
                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <eShip:CustomHiddenField ID="hidAddressBookId" runat="server" Value='<%# Eval("Id")  %>' />
                                <asp:Button ID="btnSelect" runat="server" Text="Select" OnClick="OnSelectClicked"
                                    CausesValidation="false" Visible="true" />
                                <asp:Button ID="btnEditSelect" runat="server" Text="Edit" OnClick="OnEditSelectClicked"
                                    Visible='<%# OpenForEditEnabled %>' CausesValidation="false" />
                            </td>
                            <td class="text-left">
                                <eShip:AltUniformCheckBox ToolTip="Check to mark for deletion" runat="server" ID="chkMarkForDelete" Visible='<%# CanDelete %>'
                                    OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'addressBookTable', 'chkSelectAllRecords');" />
                            </td>
                            <td>
                                <%# Eval("CustomerName")  %>
                                    -
                                <%# Eval("CustomerNumber")  %>
                            </td>
                            <td>
                                <%# Eval("Description")  %>
                            </td>
                            <td>
                                <%# Eval("FullAddress")%>
                            </td>
                            <td>
                                <asp:LinkButton runat="server" ID="lnkContactDisplay" ToolTip="See Contacts" Text="[+]" CssClass="blue link_nounderline" />
                            </td>
                        </tr>
                        <tr class="hidden">
                            <td colspan="6"></td>
                        </tr>
                        <tr>
                            <td colspan="6" class="p_m0">
                                <div id="divContacts<%# Container.DataItemIndex %>" style="display: none;">
                                    <asp:ListView ID="lstContactInfo" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <div class="rowgroup mb10">
                                                <h6 class="mt0 mb0 text-center">Contacts</h6>
                                                <table class="contain">
                                                    <tr>
                                                        <td style="width: 3%"></td>
                                                        <td style="width: 21%" class="blue">Name
                                                        </td>
                                                        <td style="width: 16%" class="blue">Phone
                                                        </td>
                                                        <td style="width: 16%" class="blue">Mobile
                                                        </td>
                                                        <td style="width: 16%" class="blue">Fax
                                                        </td>
                                                        <td style="width: 20%" class="blue">Email
                                                        </td>
                                                        <td style="width: 5%" class="blue text-center">Primary
                                                        </td>
                                                    </tr>
                                                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                                </table>
                                            </div>
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr class="border-top">
                                                <td class="border-right">
                                                    <label><%# Container.DataItemIndex + 1 %></label>
                                                </td>
                                                <td class="border-right">
                                                    <label><%# Eval("Name")%></label>
                                                </td>
                                                <td class="border-right">
                                                    <label><%# Eval("Phone")%></label>
                                                </td>
                                                <td class="border-right">
                                                    <label><%# Eval("Mobile")%></label>
                                                </td>
                                                <td class="border-right">
                                                    <label><%# Eval("Fax")%></label>
                                                </td>
                                                <td class="border-right">
                                                    <label><%# Eval("Email")%></label>
                                                </td>
                                                <td class="text-center">
                                                    <label>
                                                        <%# Eval("Primary").ToBoolean() ? WebApplicationConstants.HtmlCheck : "&nbsp;"%>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </div>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lstAddressBooks" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Panel>
<asp:Panel ID="pnlAddressBookFinderDimScreen" CssClass="dimBackgroundControl" runat="server"
    Visible="false" />
<eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
<eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
<eShip:CustomHiddenField runat="server" ID="hidEditSelected" />
<eShip:CustomHiddenField runat="server" ID="hidOpenForEditEnabled" />
<eShip:CustomHiddenField runat="server" ID="hidAddressBookFinderEnableMultiSelection" />
<eShip:CustomHiddenField ID="hidLocationItemIndex" runat="server" />
<eShip:CustomHiddenField ID="hidCustomerIdSpecificFilter" runat="server" />
<eShip:CustomHiddenField runat="server" ID="hidAreFiltersShowing" Value="true" />