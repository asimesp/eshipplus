﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="JobView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.JobView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.Operations" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Operations" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.Registry" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true" ShowUnlock="False"
        ShowEdit="true" ShowDelete="true" ShowSave="true" ShowFind="true"
        OnFind="OnToolbarFindClicked" OnSave="OnToolbarSaveClicked" OnDelete="OnToolbarDeleteClicked" OnUnlock="OnToolbarUnlockClicked"
        OnNew="OnToolbarNewClicked" OnEdit="OnToolbarEditClicked" OnCommand="OnToolbarCommand" />
</asp:Content>

<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Job
				<eShip:RecordIdentityDisplayControl runat="server" ID="ridcJobNumberIdentity" TargetControlId="txtJobNumber" />
                <eShip:RecordIdentityDisplayControl runat="server" ID="ridcJobCustomerNameIdentity" TargetControlId="txtCustomerName" />
            </h3>
            <div class="shipmentAlertSection">
                <asp:Panel ID="pnlResolvedAlert" runat="server" CssClass="flag-shipment" Visible="false">
                    Closed
                </asp:Panel>
            </div>
            <div class="lockMessage">
                <asp:Literal ID="litMessage" runat="server" />
            </div>
        </div>

        <hr class="dark mb5" />

        <div class="errorMsgLit">
            <asp:UpdatePanel runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <asp:Literal runat="server" ID="litErrorMessages" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <eShip:CustomHiddenField ID="hidJobId" runat="server" />
        <eShip:JobFinderControl runat="server" ID="jobFinder" Visible="false" OnItemSelected="OnJobFinderItemSelected"
            OnSelectionCancel="OnJobFinderItemCancelled" OpenForEditEnabled="True" />
        <eShip:CustomerFinderControl ID="customerFinder" runat="server" EnableMultiSelection="false"
            OnlyActiveResults="True" Visible="False" OpenForEditEnabled="false" OnSelectionCancel="OnCustomerFinderSelectionCancelled"
            OnItemSelected="OnCustomerFinderItemSelected" />
        <eShip:ShipmentFinderControl runat="server" EnableMultiSelection="true" ID="shipmentFinder" Visible="false" OnMultiItemSelected="OnShipmentFinderMultiItemSelected"
            OnSelectionCancel="OnShipmentFinderItemCancelled" OpenForEditEnabled="false" FilterForShipmentsToExludeAttachedToJob="true" />
        <eShip:ServiceTicketFinderControl EnableMultiSelection="true" runat="server" ID="serviceTicketFinder" Visible="false" OnMultiItemSelected="OnServiceTicketFinderMultiItemSelected"
            OnSelectionCancel="OnServiceTicketFinderItemCancelled" OpenForEditEnabled="false" FilterForServiceTicketsToExludeAttachedToJob="true" />
        <eShip:LoadOrderFinderControl runat="server" ID="loadOrderFinder" Visible="false" OnMultiItemSelected="OnLoadOrderFinderMultiItemSelected"
            OnSelectionCancel="OnLoadOrderFinderItemCancelled" OpenForEditEnabled="false" EnableMultiSelection="true" FilterForLoadOrderToExludeAttachedToJob="true" />


        <ajax:TabContainer ID="tabJob" runat="server" CssClass="ajaxCustom">
            <ajax:TabPanel runat="server" ID="tabDetails" HeaderText="Details">
                <ContentTemplate>
                    <asp:Panel ID="pnlDetails" runat="server">
                        <div class="rowgroup">
                            <div class="row">
                                <div class="row mb10">
                                    <div class="rowgroup">
                                        <div class="col_1_2 bbox vlinedarkright">
                                            <h5>General Details</h5>
                                            <div class="row">
                                                <div class="fieldgroup">
                                                    <label class="wlabel">Job Number</label>
                                                    <eShip:CustomTextBox runat="server" ID="txtJobNumber" CssClass="w160 disabled" MaxLength="50" Type="NotSet" ReadOnly="true" />
                                                </div>
                                                <div class="fieldgroup">
                                                    <label class="wlabel w130">Created by User</label>
                                                    <eShip:CustomTextBox runat="server" ID="txtCreatedByUser" CssClass="w240 disabled" MaxLength="50" Type="NotSet" ReadOnly="true" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="fieldgroup">
                                                    <label class="wlabel">
                                                        Customer Details
														<%= hidCustomerId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.Customer)
															    ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Customer Record'><img src={3} width='16' class='middle'/></a>",
															                    ResolveUrl(CustomerView.PageAddress),
															                    WebApplicationConstants.TransferNumber,
															                    hidCustomerId.Value.UrlTextEncrypt(),
															                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
															    : string.Empty %>
                                                    </label>
                                                    <eShip:CustomHiddenField runat="server" ID="hidCustomerId" />
                                                    <eShip:CustomTextBox runat="server" ID="txtCustomerNumber" CssClass="w150" OnTextChanged="OnCustomerNumberEntered"
                                                        AutoPostBack="True" />
                                                    <asp:RequiredFieldValidator runat="server" ID="rfvCustomer" ControlToValidate="txtCustomerNumber"
                                                        ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                                    <asp:ImageButton ID="btnFindCustomer" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                                                        CausesValidation="false" OnClick="OnCustomerSearchClicked" />
                                                    <eShip:CustomTextBox runat="server" ID="txtCustomerName" CssClass="w330 disabled" ReadOnly="True" />
                                                    <ajax:AutoCompleteExtender runat="server" ID="aceCustomer" TargetControlID="txtCustomerNumber" ServiceMethod="GetCustomerList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                                        EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="fieldgroup">
                                                    <label class="wlabel w130">Status</label>
                                                    <asp:DropDownList runat="server" ID="ddlJobStatus" DataValueField="Value" DataTextField="Text" />
                                                </div>
                                                <div class="fieldgroup">
                                                    <label class="wlabel w130">Date Created</label>
                                                    <eShip:CustomTextBox runat="server" ID="txtDateCreated" CssClass="w100" Type="Date" placeholder="99/99/9999" ReadOnly="true" Enabled="false" />
                                                </div>
                                            </div>
                                            <h5>References</h5>
                                            <div class="row">
                                                <div class="fieldgroup mr20 fieldgroup">
                                                    <label class="wlabel">External Reference 1</label>
                                                    <eShip:CustomTextBox ID="txtExternalReference1" runat="server" CssClass="w420" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="fieldgroup mr20 fieldgroup">
                                                    <label class="wlabel">External Reference 2</label>
                                                    <eShip:CustomTextBox ID="txtExternalReference2" runat="server" CssClass="w420" MaxLength="100" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col_1_2 bbox pl40">
                                            <h5>Financial Summary</h5>
                                            <div class="row">
                                                <div class="row">
                                                    <label class="blue upper w130">Total Costs</label>
                                                </div>
                                                <div class="pl40">
                                                    <div class="fieldgroup">
                                                        <label class="wlabel w130">Estimate</label>
                                                        <eShip:CustomTextBox runat="server" ID="txtEstimateCost" CssClass="w110 disabled" ReadOnly="True" />
                                                    </div>
                                                    <div class="fieldgroup">
                                                        <label class="wlabel w130">Actual</label>
                                                        <eShip:CustomTextBox runat="server" ID="txtActualCost" CssClass="w110 disabled" ReadOnly="True" />
                                                    </div>
                                                    <div class="fieldgroup">
                                                        <label class="wlabel w150">(Act. - Est.)</label>
                                                        <eShip:CustomTextBox runat="server" ID="txtDiffCost" CssClass="w110 disabled" ReadOnly="True" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <br />
                                                <div class="row">
                                                    <label class="blue upper w130">Total Revenue</label>
                                                </div>
                                                <div class="pl40">
                                                    <div class="fieldgroup">
                                                        <label class="wlabel w130">Estimate</label>
                                                        <eShip:CustomTextBox runat="server" ID="txtEstimateRevenue" CssClass="w110 disabled" ReadOnly="True" />
                                                    </div>
                                                    <div class="fieldgroup">
                                                        <label class="wlabel w130">Actual</label>
                                                        <eShip:CustomTextBox runat="server" ID="txtActualRevenue" CssClass="w110 disabled" ReadOnly="True" />
                                                    </div>
                                                    <div class="fieldgroup">
                                                        <label class="wlabel w150">(Act. - Est.)</label>
                                                        <eShip:CustomTextBox runat="server" ID="txtDiffRevenue" CssClass="w110 disabled" ReadOnly="True" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <br />
                                                <div class="row">
                                                    <label class="blue upper w130">Total Profit</label>
                                                </div>
                                                <div class="pl40">
                                                    <div class="fieldgroup">
                                                        <label class="wlabel w130">Estimate</label>
                                                        <eShip:CustomTextBox runat="server" ID="txtEstimateProfit" CssClass="w110 disabled" ReadOnly="True" />
                                                    </div>
                                                    <div class="fieldgroup">
                                                        <label class="wlabel w130">Actual</label>
                                                        <eShip:CustomTextBox runat="server" ID="txtActualProfit" CssClass="w110 disabled" ReadOnly="True" />
                                                    </div>
                                                    <div class="fieldgroup">
                                                        <label class="wlabel w150">(Act. - Est.)</label>
                                                        <eShip:CustomTextBox runat="server" ID="txtDiffProfit" CssClass="w110 disabled" ReadOnly="True" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <br />
                                                <div class="row">
                                                    <label class="blue upper w130">Profit %</label>
                                                    <label class="blue upper w130"></label>
                                                    <label class="blue upper w130 pl40">Total Outstanding</label>
                                                </div>
                                                <div class="pl40">
                                                    <div class="fieldgroup">
                                                        <label class="wlabel w130">Estimate</label>
                                                        <eShip:CustomTextBox runat="server" ID="txtGPMEstimate" CssClass="w110 disabled" ReadOnly="True" />
                                                    </div>
                                                    <div class="fieldgroup">
                                                        <label class="wlabel w130">Actual</label>
                                                        <eShip:CustomTextBox runat="server" ID="txtGPMActual" CssClass="w110 disabled" ReadOnly="True" />
                                                    </div>
                                                    <div class="fieldgroup pl20">
                                                        <label class="wlabel w130">Actual</label>
                                                        <eShip:CustomTextBox runat="server" ID="txtTotalOutstanding" CssClass="w110 disabled" ReadOnly="True" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div>
                                                <br />
                                                <h5>Finance Record Counts</h5>
                                                <div class="row">
                                                    <div class="fieldgroup">
                                                        <label class="wlabel w130">Invoices</label>
                                                        <eShip:CustomTextBox runat="server" ID="txtInvoiceCount" CssClass="w110 disabled" ReadOnly="True" />
                                                    </div>
                                                    <div class="fieldgroup">
                                                        <label class="wlabel w130">Credit Invoices</label>
                                                        <eShip:CustomTextBox runat="server" ID="txtCreditInvoicesCount" CssClass="w110 disabled" ReadOnly="True" />
                                                    </div>
                                                    <div class="fieldgroup">
                                                        <label class="wlabel w150">Supplemental Invoices</label>
                                                        <eShip:CustomTextBox runat="server" ID="txtSupplementalInvoicesCount" CssClass="w110 disabled" ReadOnly="True" />
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="fieldgroup">
                                                        <label class="wlabel w130">Vendor Bills</label>
                                                        <eShip:CustomTextBox runat="server" ID="txtVendorBillsCount" CssClass="w110 disabled" ReadOnly="True" />
                                                    </div>
                                                    <div class="fieldgroup">
                                                        <label class="wlabel w130">Credit Vendor Bills</label>
                                                        <eShip:CustomTextBox runat="server" ID="txtCreditVendorBillsCount" CssClass="w110 disabled" ReadOnly="True" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <br />
                                                <h5>Job Component Entity Counts</h5>
                                                <div class="row">
                                                    <div class="fieldgroup">
                                                        <label class="wlabel w130">Shipments</label>
                                                        <eShip:CustomTextBox runat="server" ID="txtShipmentCount" CssClass="w110 disabled" ReadOnly="True" />
                                                    </div>
                                                    <div class="fieldgroup">
                                                        <label class="wlabel w130">Service Tickets</label>
                                                        <eShip:CustomTextBox runat="server" ID="txtServiceTicketCount" CssClass="w110 disabled" ReadOnly="True" />
                                                    </div>
                                                    <div class="fieldgroup">
                                                        <label class="wlabel w130">Load Orders</label>
                                                        <eShip:CustomTextBox runat="server" ID="txtLoadOrderCount" CssClass="w110 disabled" ReadOnly="True" />
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabJobComponents" HeaderText="Job Components">
                <ContentTemplate>
                    <asp:Panel ID="pnlJobComponents" runat="server">
                        <div class="rowgroup">
                            <div class="row">
                                <div class="rowgroup mb10">
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddServiceTicket" Text="Add Service Ticket" CausesValidation="false" CssClass="mr10" OnClick="OnAddServiceTicketClicked" />
                                        <asp:Button runat="server" ID="btnAddShipment" Text="Add Shipment" CausesValidation="false" CssClass="mr10" OnClick="OnAddShipmentClicked" />
                                        <asp:Button runat="server" ID="btnAddLoadOrder" Text="Add Load Order" CausesValidation="false" CssClass="mr10" OnClick="OnAddLoadOrderClicked" />
                                        <asp:Button runat="server" ID="btnClearAll" Text="Clear All" CausesValidation="false" OnClick="OnClearAllComponentsClicked" />
                                    </div>
                                </div>

                                <asp:UpdatePanel runat="server" ID="upJobComponents">
                                    <ContentTemplate>
                                        <table class="stripe">
                                            <tr>
                                                <th style="width: 20%;">Component Type</th>
                                                <th style="width: 20%;">Component Number</th>
                                                <th style="width: 20%;">Status</th>
                                                <th style="width: 20%;">Step</th>
                                                <th style="width: 20%;" class="text-center">Action</th>
                                            </tr>

                                            <asp:ListView ID="lstJobComponents" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <asp:Literal ID="litComponentType" runat="server" Text='<%# Eval("ComponentType") %>' />
                                                            <eShip:CustomHiddenField ID="hidJobComponentIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                            <eShip:CustomHiddenField ID="hidComponentId" runat="server" Value='<%# Eval("ComponentId") %>' />
                                                        </td>
                                                        <td>
                                                            <asp:Label runat="server" ID="lblJobComponentNumber" Text='<%# Eval("ComponentNumber") %>' />
                                                        </td>
                                                        <td>
                                                            <asp:Label runat="server" ID="lblJobComponentStatus" Text='<%# Eval("ComponentStatus") %>' />
                                                        </td>
                                                        <td>
                                                            <eShip:CustomTextBox runat="server" ID="txtComponentJobStep" CssClass="w40" MaxLength="5"
                                                                Type="NumbersOnly" ReadOnly="false" Text='<%# Eval("ComponentJobStep") %>' AutoPostBack="true" OnTextChanged="OnResortJobComponentsAccordingToStep" />
                                                        </td>
                                                        <td class="text-center top">
                                                            <asp:ImageButton ID="ibtnDelete" runat="server" CssClass="mr10" ImageUrl="~/images/icons2/deleteX.png" ToolTip="Remove job component"
                                                                CausesValidation="false" OnClick="OnDeleteComponentClicked" Enabled='<%# Access.Modify %>' />
                                                            <%# 
                                                        string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To {3} Record'><img src={4} width='24' class='middle'/></a>",
                                                        ResolveUrl(
	                                                        ((ComponentType) Eval("ComponentType")) == ComponentType.Shipment
		                                                        ? ShipmentView.PageAddress
		                                                        : ((ComponentType) Eval("ComponentType")) == ComponentType.ServiceTicket
			                                                          ? ServiceTicketView.PageAddress
			                                                          : LoadOrderView.PageAddress
	                                                        ),
                                                        WebApplicationConstants.TransferNumber,
                                                        Eval("ComponentNumber"),
                                                        Eval("ComponentType"),
                                                        ResolveUrl("~/images/icons2/arrow_general.png")) 
                                                            %>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabVendorBills" HeaderText="Vendor Bill Details">
                <ContentTemplate>
                    <asp:Panel ID="pnlVendorBills" runat="server">
                        <div class="rowgroup">
                            <div class="row">
                                <table class="stripe">
                                    <tr>

                                        <th style="width: 15%;">Document Number</th>
                                        <th style="width: 10%;">Bill Type</th>
                                        <th style="width: 20%;">Vendor</th>
                                        <th style="width: 5%;">Reference Type</th>
                                        <th style="width: 10%;">Reference Number</th>
                                        <th style="width: 10%;">Charge Code</th>
                                        <th style="width: 10%;">Unit Buy</th>
                                        <th style="width: 10%;">Quantity</th>
                                        <th style="width: 10%;">Amount Due</th>
                                    </tr>

                                    <asp:ListView ID="lstVendorBillDetails" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <%# Eval("DocumentNumber") %>
                                                    <%# ActiveUser.HasAccessTo(ViewCode.VendorBill) && Eval("VendorBillId").ToLong() != default(long)
														    ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Vendor Bill Record'><img src={3} width='16' class='middle'/></a>",
														                    ResolveUrl(VendorBillView.PageAddress),
														                    WebApplicationConstants.TransferNumber,
														                    Eval("VendorBillId").GetString().UrlTextEncrypt(),
														                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
														    : string.Empty %>
                                                </td>
                                                <td>
                                                    <%# Eval("BillType") %>
                                                </td>
                                                <td>
                                                    <%# Eval("VendorName") %>
                                                    <%# ActiveUser.HasAccessTo(ViewCode.Vendor) && Eval("VendorId").ToLong() != default(long)
														    ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Vendor Record'><img src={3} width='16' class='middle'/></a>",
														                    ResolveUrl(VendorView.PageAddress),
														                    WebApplicationConstants.TransferNumber,
														                    Eval("VendorId").GetString().UrlTextEncrypt(),
														                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
														    : string.Empty %>
                                                </td>
                                                <td>
                                                    <%# Eval("ReferenceType").ToString() %>
                                                </td>
                                                <td>
                                                    <%# Eval("ReferenceNumber").ToString() %>
                                                    <%# !string.IsNullOrEmpty(Eval("ReferenceNumber").ToString()) &&
													    ((Eval("ReferenceType").ToInt().ToEnum<DetailReferenceType>() == DetailReferenceType.Shipment && ActiveUser.HasAccessTo(ViewCode.Shipment)) ||
													     (Eval("ReferenceType").ToInt().ToEnum<DetailReferenceType>() == DetailReferenceType.ServiceTicket && ActiveUser.HasAccessTo(ViewCode.ServiceTicket)))
														    ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To {3} Record'><img src={4} width='16' class='middle'/></a>",
														                    Eval("ReferenceType").ToInt().ToEnum<DetailReferenceType>() == DetailReferenceType.Shipment ? ResolveUrl(ShipmentView.PageAddress) : ResolveUrl(ServiceTicketView.PageAddress),
														                    WebApplicationConstants.TransferNumber,
														                    Eval("ReferenceNumber").GetString(),
														                    Eval("ReferenceType"),
														                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
														    : string.Empty %>
                                                </td>
                                                <td>
                                                    <%# Eval("ChargeCode") %>
                                                </td>
                                                <td>
                                                    <asp:Literal runat="server" ID="litUnitBuy" Text='<%# Eval("UnitBuy") %>' />
                                                </td>
                                                <td>
                                                    <asp:Literal runat="server" ID="litQuantity" Text='<%# Eval("Quantity") %>' />
                                                </td>
                                                <td>
                                                    <asp:Literal runat="server" ID="litAmountDue" Text='<%# Eval("AmountDue") %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabInvoices" HeaderText="Invoice Details">
                <ContentTemplate>
                    <asp:Panel ID="pnlInvoices" runat="server">
                        <div class="rowgroup">
                            <div class="row">
                                <table class="stripe">
                                    <tr>
                                        <th style="width: 10%;">Invoice Number</th>
                                        <th style="width: 5%;">Invoice Type</th>
                                        <th style="width: 25%;">Customer</th>
                                        <th style="width: 5%;">Reference Type</th>
                                        <th style="width: 10%;">Reference Number</th>
                                        <th style="width: 10%;">Charge Code</th>
                                        <th style="width: 10%;">Unit Sell</th>
                                        <th style="width: 10%;">Unit Discount</th>
                                        <th style="width: 5%;">Quantity</th>
                                        <th style="width: 10%;">Amount Due</th>
                                    </tr>

                                    <asp:ListView ID="lstInvoiceDetails" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <%# Eval("InvoiceNumber") %>
                                                    <%# ActiveUser.HasAccessTo(ViewCode.Invoice) && !string.IsNullOrEmpty(Eval("InvoiceNumber").ToString())
														    ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Invoice Record'><img src={3} width='16' class='middle'/></a>",
														                    ResolveUrl(InvoiceView.PageAddress),
														                    WebApplicationConstants.TransferNumber,
														                    Eval("InvoiceNumber").GetString(),
														                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
														    : string.Empty %>
                                                </td>
                                                <td>
                                                    <%# Eval("InvoiceType") %>
                                                </td>
                                                <td>
                                                    <%# Eval("CustomerName") %>
                                                    <%# ActiveUser.HasAccessTo(ViewCode.Customer) && Eval("CustomerId").ToLong() != default(long)
														    ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Customer Record'><img src={3} width='16' class='middle'/></a>",
														                    ResolveUrl(CustomerView.PageAddress),
														                    WebApplicationConstants.TransferNumber,
														                    Eval("CustomerId").GetString().UrlTextEncrypt(),
														                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
														    : string.Empty %>
                                                </td>
                                                <td>
                                                    <%# Eval("ReferenceType").ToString() %>
                                                </td>
                                                <td>
                                                    <%# Eval("ReferenceNumber").ToString() %>
                                                    <%# !string.IsNullOrEmpty(Eval("ReferenceNumber").ToString()) &&
													    ((Eval("ReferenceType").ToInt().ToEnum<DetailReferenceType>() == DetailReferenceType.Shipment && ActiveUser.HasAccessTo(ViewCode.Shipment)) ||
													     (Eval("ReferenceType").ToInt().ToEnum<DetailReferenceType>() == DetailReferenceType.ServiceTicket && ActiveUser.HasAccessTo(ViewCode.ServiceTicket)))
														    ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To {3} Record'><img src={4} width='16' class='middle'/></a>",
														                    Eval("ReferenceType").ToInt().ToEnum<DetailReferenceType>() == DetailReferenceType.Shipment ? ResolveUrl(ShipmentView.PageAddress) : ResolveUrl(ServiceTicketView.PageAddress),
														                    WebApplicationConstants.TransferNumber,
														                    Eval("ReferenceNumber").GetString(),
														                    Eval("ReferenceType"),
														                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
														    : string.Empty %>
                                                </td>
                                                <td>
                                                    <%# Eval("ChargeCode") %>
                                                </td>
                                                <td>
                                                    <asp:Literal runat="server" ID="litUnitSell" Text='<%# Eval("UnitSell") %>' />
                                                </td>
                                                <td>
                                                    <asp:Literal runat="server" ID="litUnitDiscount" Text='<%# Eval("UnitDiscount") %>' />
                                                </td>
                                                <td>
                                                    <asp:Literal runat="server" ID="litQuantity" Text='<%# Eval("Quantity") %>' />
                                                </td>
                                                <td>
                                                    <asp:Literal runat="server" ID="litAmountDue" Text='<%# Eval("AmountDue") %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabDocuments" HeaderText="Documents">
                <ContentTemplate>
                    <asp:Panel ID="pnlDocuments" runat="server">
                        <div class="rowgroup">
                            <div class="row">
                                <div class="rowgroup mb10">
                                    <div class="row">
                                        <div class="fieldgroup right">
                                            <asp:Button runat="server" ID="btnAddDocument" Text="Add Document" CssClass="mr10" CausesValidation="false" OnClick="OnAddDocumentClicked" />
                                            <asp:Button runat="server" ID="btnClearDocuments" Text="Clear Documents" OnClick="OnClearDocumentsClicked" CausesValidation="False" />
                                        </div>
                                    </div>
                                </div>

                                <table class="stripe">
                                    <tr>
                                        <th style="width: 22%;">Name</th>
                                        <th style="width: 32%;">Description</th>
                                        <th style="width: 21%;">Document Tag</th>
                                        <th style="width: 15%;">File</th>
                                        <th style="width: 8%;" class="text-center">Action</th>
                                    </tr>
                                    <asp:ListView ID="lstDocuments" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <script type="text/javascript">
                                                $(document).ready(function () {
                                                    $("a[id$='lnkJobsDocumentLocationPath']").each(function () {
                                                        jsHelper.SetDoPostBackJsLink($(this).prop('id'));
                                                    });
                                                });
                                            </script>

                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidDocumentIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <eShip:CustomHiddenField ID="hidDocumentId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <asp:Literal ID="litDocumentName" runat="server" Text='<%# Eval("Name") %>' />
                                                    <eShip:CustomHiddenField ID="hidDateCreated" runat="server" Value='<%# Eval("DateCreated") %>' />
                                                </td>
                                                <td>
                                                    <asp:Literal ID="litDocumentDescription" runat="server" Text='<%# Eval("Description") %>' />
                                                </td>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidDocumentTagId" runat="server" Value='<%# Eval("DocumentTagId") %>' />
                                                    <asp:Literal ID="litDocumentTag" runat="server" Text='<%# new DocumentTag(Eval("DocumentTagId").ToLong()).Description %>' />
                                                </td>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidLocationPath" runat="server" Value='<%# Eval("LocationPath") %>' />
                                                    <asp:LinkButton runat="server" ID="lnkJobsDocumentLocationPath" Text='<%# GetLocationFileName(Eval("LocationPath")) %>'
                                                        OnClick="OnLocationPathClicked" CausesValidation="False" CssClass="blue" />
                                                </td>
                                                <td class="text-center top">
                                                    <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png" ToolTip="Edit document"
                                                        CausesValidation="false" OnClick="OnEditDocumentClicked" Enabled='<%# Access.Modify %>' />
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png" ToolTip="Remove document"
                                                        CausesValidation="false" OnClick="OnDeleteDocumentClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>

                                <asp:Panel runat="server" ID="pnlEditDocument" Visible="false" CssClass="popup popupControlOverW500">
                                    <div class="popheader mb10">
                                        <h4>Add/Modify Document
                                        </h4>
                                        <asp:ImageButton ID="ImageButton5" ImageUrl="~/images/icons2/close.png" CssClass="close"
                                            CausesValidation="false" OnClick="OnCloseEditDocumentClicked" runat="server" />
                                    </div>
                                    <table class="poptable">
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Name:</label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox runat="server" ID="txtDocumentName" CssClass="w260" MaxLength="50" />
                                                <eShip:CustomHiddenField runat="server" ID="hiddenDateCreated" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right top">
                                                <label class="upper">Description:</label>
                                                <br />
                                                <label class="lhInherit">
                                                    <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDocumentDescription" MaxLength="500" TargetControlId="txtDocumentDescription" />
                                                </label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox runat="server" ID="txtDocumentDescription" CssClass="w260 h150" TextMode="MultiLine" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Document Tag:</label>
                                            </td>
                                            <td>
                                                <eShip:CachedObjectDropDownList Type="DocumentTag" ID="ddlDocumentTag" runat="server" CssClass="w260" EnableChooseOne="True" DefaultValue="0" />
                                                <eShip:CustomHiddenField ID="hidDocumentTagId" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Location Path:</label>
                                            </td>
                                            <td>
                                                <eShip:CustomHiddenField ID="hidLocationPath" runat="server" />
                                                <asp:FileUpload runat="server" ID="fupLocationPath" CssClass="jQueryUniform" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Current File:</label>
                                            </td>
                                            <td>
                                                <asp:Literal ID="litLocationPath" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>
                                                <asp:Button Text="Done" OnClick="OnEditDocumentDoneClicked" runat="server"
                                                    CausesValidation="false" />
                                                <asp:Button Text="Cancel" OnClick="OnCloseEditDocumentClicked" runat="server"
                                                    CausesValidation="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabAuditLog" HeaderText="Audit Log">
                <ContentTemplate>
                    <eShip:AuditLogControl ID="auditLogs" runat="server" />
                </ContentTemplate>
            </ajax:TabPanel>
        </ajax:TabContainer>

        <asp:Panel runat="server" ID="pnlSelectShipments" Visible="false" CssClass="popupControlOver popupControlOverW500">
            <div class="popheader">
                <h4>
                    <asp:Label runat="server" ID="lblSelectShipmentsPopupTitle"></asp:Label></h4>
                <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                    CausesValidation="false" OnClick="OnCloseSelectShipmentClicked" runat="server" />
            </div>
            <table class="poptable" id="shipmentsTableDiv">
                <tr>
                    <td colspan="2" class="p_m0">
                        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheSelectShipmentsTable" TableId="selectShipmentsTable"
                            ScrollerContainerId="selectShipmentsScrollSection" ScrollOffsetControlId="pnlSelectShipments" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2050" />
                        <div class="finderScroll" id="selectShipmentsScrollSection">
                            <table class="stripe sm_chk" id="selectShipmentsTable">
                                <tr>
                                    <th style="width: 10%;">
                                        <eShip:AltUniformCheckBox ID="chkSelectAllRecords" runat="server" OnClientSideClicked="javascript:SetSelectedCheckBoxesStatus(this, 'selectShipmentsTable');" />
                                    </th>
                                    <th style="width: 30%;" class="text-left">Shipment Number</th>
                                    <th style="width: 30%;" class="text-left">Status</th>
                                    <th style="width: 30%;" class="text-left">Mode</th>
                                </tr>
                                <asp:ListView runat="server" ID="lstShipments" ItemPlaceholderID="itemPlaceHolder">
                                    <LayoutTemplate>
                                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <eShip:CustomHiddenField ID="hidShipmentId" runat="server" Value='<%# Eval("Id") %>' />
                                                <eShip:AltUniformCheckBox runat="server" ID="chkSelection" CssClass="jQueryUniform" OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'selectShipmentsTable', 'chkSelectAllRecords');" />
                                            </td>
                                            <td>
                                                <%# Eval("ShipmentNumber") %>
                                            </td>
                                            <td>
                                                <%# Eval("Status") %>
                                            </td>
                                            <td>
                                                <%# Eval("ServiceMode") %>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="btnSelectShipmentsAction" OnClick="OnSelectShipmentsActionButtonClicked" runat="server" CausesValidation="false" OnClientClick="ShowDontTouchDivBlock();" />
                        <script>
                            $(window).load(function () {
                                $('#divDontRefresh').hide();
                                $('#shipmentsTableDiv').show();
                            });
                            function ShowDontTouchDivBlock() {
                                $('#divDontRefresh').show();
                                $('#shipmentsTableDiv').hide();
                            }
                        </script>
                        <asp:Button ID="btnEditItemClose" Text="Cancel" OnClick="OnCloseSelectShipmentClicked" runat="server" CausesValidation="false" />
                    </td>
                </tr>
            </table>
            <div id="divDontRefresh" class="text-center middle" style="display: none; cursor: wait;">
                <table>
                    <tr>
                        <td class="text-center middle">
                            <asp:Image ID="imgWarning" ImageUrl="~/images/icons/messageIcons/warning.png" Width="32"
                                Height="32" runat="server" />
                        </td>
                        <td>
                            <asp:Literal ID="message" runat="server" />
                            <h4>This may take a long time, please don't refresh the page! ...</h4>
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>


        <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />

        <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
            Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" />

        <asp:UpdatePanel runat="server" ID="upHiddens" UpdateMode="Always">
            <ContentTemplate>
                <eShip:CustomHiddenField runat="server" ID="hidFlag" />
                <eShip:CustomHiddenField runat="server" ID="hidEditingIndex" />
                <eShip:CustomHiddenField runat="server" ID="hidFilesToDelete" />
                <eShip:CustomHiddenField runat="server" ID="hidNotificationsRequired" />
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
</asp:Content>
