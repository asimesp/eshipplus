﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DatLoadboardAssetControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.DatLoadboardAssetControl" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.Operations" %>
<%@ Import Namespace="LogisticsPlus.Eship.Dat" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>


<asp:Panel runat="server" ID="pnlDatLoadboardAssetContent" CssClass="popupControl">

    <script type="text/javascript">
        $(function () {
            UpdateDatButtonVisibilities();
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                UpdateDatButtonVisibilities();
            });
        });

        function UpdateDatButtonVisibilities() {
            // based on existing DAT posting and permissions, handle visibility of buttons
            var datPostingExists = !jsHelper.IsNullOrEmpty($('#<%= txtDatAssetId.ClientID %>').val());

            if (jsHelper.IsTrue('<%= ActiveUser.HasAccessToModify(ViewCode.DatLoadboard) %>') && datPostingExists) {
                $('#<%= btnUpdateAssetAtDat.ClientID %>').show();
            } else {
                $('#<%= btnUpdateAssetAtDat.ClientID %>').hide();
            }

            if (jsHelper.IsTrue('<%= ActiveUser.HasAccessToDelete(ViewCode.DatLoadboard) %>') && datPostingExists) {
                $('#<%= btnDeleteAssetFromDat.ClientID %>').show();
            } else {
                $('#<%= btnDeleteAssetFromDat.ClientID %>').hide();
            }

            if (datPostingExists) {
                $('#<%= btnEnterRatePostAssetToDat.ClientID %>').hide();
            } else {
                $('#<%= btnEnterRatePostAssetToDat.ClientID %>').show();
            }
        }

        function DisplayRateEntryForDatPosting(show, isUpdate) {
            if (show) {
                $('#divRateEntryForDat').show();

                $('#<%= txtMileageForAssetPost.ClientID %>').val($('#<%= hidLoadOrderMileage.ClientID %>').val());
                $('#<%= ddlDatRateTypeForAssetPost.ClientID %>').val('<%= RateBasedOnType.Flat.ToInt() %>');
                if (isUpdate) {
                    $('#<%= txtBaseRateForAssetPost.ClientID %>').val($('#<%= txtDatBaseRate.ClientID %>').val());
        	        $('#<%= btnPostAssetToDat.ClientID %>').hide();
        	        $('#<%= btnUpdateAssetOnDat.ClientID %>').show();
        	    } else {
        	        $('#<%= txtBaseRateForAssetPost.ClientID %>').val($('#<%= hidLoadOrderUnitBuyTotal.ClientID %>').val());
        	        $('#<%= btnPostAssetToDat.ClientID %>').show();
        	        $('#<%= btnUpdateAssetOnDat.ClientID %>').hide();
        	        UpdateTotalRateForAssetPost();
        	    }

            } else {
                $('#divRateEntryForDat').hide();
            }
        }

        function CallPostAssetToDatLoadboard() {
            var userId = $('#<%= hidUserId.ClientID %>').val();
            var recordId = $('#<%= hidLoadOrderId.ClientID %>').val();
            var rate = $('#<%= txtBaseRateForAssetPost.ClientID %>').val();
            var rateType = $('#<%= ddlDatRateTypeForAssetPost.ClientID %>').val();
            var comments = new Array();
            $('[id$="txtDatAssetComment"]').each(function () {
                comments.push($(this).val());
            });

            var ids = new ControlIds();
            ids.DatAssetId = '<%= txtDatAssetId.ClientID %>';
            ids.DatExpirationDate = '<%= txtDatExpirationDate.ClientID %>';
            ids.DatDateCreated = '<%= txtDatDateCreated.ClientID %>';
            ids.DatBaseRate = '<%= txtDatBaseRate.ClientID %>';
            ids.DatRateType = '<%= txtDatRateType.ClientID %>';
            ids.DatMileage = '<%= txtDatMileage.ClientID %>';
            ids.DatWasSentAsHazMat = '<%= chkWasSentAsHazMat.ClientID %>';
            ids.DatPostedByUser = '<%= txtDatPostedByUser.ClientID %>';
            ids.BtnUpdateAsset = '<%= btnUpdateAssetAtDat.ClientID %>';
            ids.BtnRemoveAsset = '<%= btnDeleteAssetFromDat.ClientID %>';
            ids.BtnPostAsset = '<%= btnEnterRatePostAssetToDat.ClientID %>';
            ids.HidDatAssetId = '<%= hidAssetId.ClientID %>';
            ids.DatLoadboardErrors = '<%= lblDatLoadboardErrorMessages.ClientID %>';

            PostAssetToDatLoadboard(userId, recordId, rate, rateType, comments, ids);

            $('#divRateEntryForDat').hide();
            $('#<%= hidTenantId.ClientID %>').val("<%= ActiveUser.TenantId %>");
            CallLookupRatesFromDatLoadboard();
        }

        function CallUpdateAssetToDatLoadboard() {
            var userId = $('#<%= hidUserId.ClientID %>').val();
            var recordId = $('#<%= hidLoadOrderId.ClientID %>').val();
            var tenantId = $('#<%= hidTenantId.ClientID %>').val();
            var assetId = $('#<%= txtDatAssetId.ClientID %>').val();
            var rate = $('#<%= txtBaseRateForAssetPost.ClientID %>').val();
            var rateType = $('#<%= ddlDatRateTypeForAssetPost.ClientID %>').val();
            var comments = new Array();
            $('[id$="txtDatAssetComment"]').each(function () {
                comments.push($(this).val());
            });

            var ids = new ControlIds();
            ids.DatBaseRate = '<%= txtDatBaseRate.ClientID %>';
            ids.DatRateType = '<%= txtDatRateType.ClientID %>';
            ids.DatLoadboardErrors = '<%= lblDatLoadboardErrorMessages.ClientID %>';

            UpdateAssetToDatLoadboard(userId, recordId, tenantId, rate, rateType, comments, assetId, ids);

            $('#divRateEntryForDat').hide();

        }

        function CallDeleteAssetFromDatLoadboard() {
            if (!confirm("Are you sure you would like to remove this asset from the DAT Loadboard?")) return;

            var userId = $('#<%= hidUserId.ClientID %>').val();
            var recordId = $('#<%= hidLoadOrderId.ClientID %>').val();

            var ids = new ControlIds();
            ids.DatAssetId = '<%= txtDatAssetId.ClientID %>';
            ids.DatExpirationDate = '<%= txtDatExpirationDate.ClientID %>';
            ids.DatDateCreated = '<%= txtDatDateCreated.ClientID %>';
            ids.DatBaseRate = '<%= txtDatBaseRate.ClientID %>';
            ids.DatRateType = '<%= txtDatRateType.ClientID %>';
            ids.DatMileage = '<%= txtDatMileage.ClientID %>';
            ids.DatWasSentAsHazMat = '<%= chkWasSentAsHazMat.ClientID %>';
            ids.DatPostedByUser = '<%= txtDatPostedByUser.ClientID %>';
            ids.BtnUpdateAsset = '<%= btnUpdateAssetAtDat.ClientID %>';
            ids.BtnRemoveAsset = '<%= btnDeleteAssetFromDat.ClientID %>';
            ids.BtnPostAsset = '<%= btnEnterRatePostAssetToDat.ClientID %>';
            ids.DatLoadboardErrors = '<%= lblDatLoadboardErrorMessages.ClientID %>';
            ids.DatRatesContainer = '<%= ratesContainer.ClientID  %>';

            DeleteAssetFromDatLoadboard(userId, recordId, ids);
            $(jsHelper.AddHashTag(ids.DatRatesContainer)).hide();

        }

        function CallLookupRatesFromDatLoadboard() {
            var userId = $('#<%= hidUserId.ClientID %>').val();
            var recordId = $('#<%= hidLoadOrderId.ClientID %>').val();
            var assetId = $('#<%= txtDatAssetId.ClientID %>').val();

            var ids = new ControlIds();
            ids.DatLoadboardErrors = '<%= lblDatLoadboardErrorMessages.ClientID %>';
            ids.DatRatesContainer = '<%= ratesContainer.ClientID  %>';
            ids.DatRatesDiv = '<%= ratesDiv.ClientID  %>';

            LookupRateFromDatLoadboard(userId, recordId, assetId, ids);
        }

    </script>


    <div class="popheader">
        <h4>DAT Loadboard Asset Management
        <asp:ImageButton runat="server" ID="ibtnCloseManageAssetsOnLoadboard" OnClick="OnCloseManageAssetsOnLoadboardClicked"
            CausesValidation="False" CssClass="close" ImageUrl="~/images/icons2/close.png" /></h4>
    </div>
    <div class="pl10 red pt10 pb10" id="divErrors">
        <asp:Label runat="server" ID="lblDatLoadboardErrorMessages" />
    </div>
    <div class="rowgroup">
        <div class="row">
            <table class="poptable">
                <tr>
                    <td class="text-right">
                        <label class="upper">
                            <asp:Literal runat="server" ID="litShipmentOrLoadOrderNumberLabel" />
                            <abbr title="Number">#</abbr>:
                        </label>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtLoadNumber" CssClass="w200 disabled" ReadOnly="True" />

                    </td>
                    <td class="text-right">
                        <label class="upper">Mileage:</label>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtDatMileage" CssClass="w200 disabled" ReadOnly="True" />
                    </td>
                </tr>
                <tr>
                    <td class="text-right">
                        <label class="upper">DAT Asset Id:</label>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtDatAssetId" CssClass="w200 disabled" ReadOnly="True" />
                    </td>
                    <td class="text-right">
                        <label class="upper">Rate Type:</label>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtDatRateType" CssClass="w200 disabled" ReadOnly="True" />
                    </td>
                </tr>
                <tr>
                    <td class="text-right">
                        <label class="upper">Date Posted:</label>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtDatDateCreated" CssClass="w200 disabled" ReadOnly="True" />
                    </td>
                    <td class="text-right">
                        <label class="upper">Expiration Date:</label>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtDatExpirationDate" CssClass="w200 disabled" ReadOnly="True" />
                    </td>
                </tr>
                <tr>
                    <td class="text-right">
                        <label class="upper">Base Rate:</label>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtDatBaseRate" CssClass="w200 disabled" ReadOnly="True" />
                    </td>

                </tr>
                <tr>
                    <td class="text-right">
                        <label class="upper">Posted By User:</label>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtDatPostedByUser" CssClass="w200 disabled" ReadOnly="True" />
                    </td>
                    <td class="text-right"></td>
                    <td>
                        <eShip:AltUniformCheckBox runat="server" ID="chkWasSentAsHazMat" Enabled="False" />
                        <label class="upper">Was Posted as Haz Mat</label>
                    </td>
                </tr>
            </table>
        </div>

        <div id="ratesContainer" style="display: none" runat="server" class="finderScrollFixed">
            <hr class="w100p ml-inherit dark dotted mb10" />
            <div class="rowgroup">
                <h5 class="pl10 dark">Rates
                <%= string.Format("<a href='{0}' class='blue' target='_blank' title='View Rates on DAT'><img src={1} width='20' class='middle'/></a>", 
                                                        hidLoadOrderId.Value.ToLong().GetDatRateViewUrl(),
                                                        ResolveUrl("~/images/icons2/arrow_newTab.png")) %></h5>

                <div id="ratesDiv" runat="server"></div>
            </div>
        </div>

        <asp:UpdatePanel runat="server" UpdateMode="Always">
            <ContentTemplate>
                <div class="row pt10">
                    <div class="fieldgroup right pr25">
                        <asp:Button runat="server" ID="btnEnterRatePostAssetToDat" Text="Post Asset"
                            OnClientClick="DisplayRateEntryForDatPosting(true, false); return false;" />

                        <asp:Button runat="server" ID="btnUpdateAssetAtDat" Text="Update Asset" OnClick="OnUpdateClicked"
                            OnClientClick="DisplayRateEntryForDatPosting(true, true); return true;" />

                        <asp:Button runat="server" ID="btnDeleteAssetFromDat" Text="Remove Asset" OnClick="DeleteAsset"
                            OnClientClick="CallDeleteAssetFromDatLoadboard(); return true;" />

                        <asp:Button runat="server" ID="btnLookupRatesForAsset" Text="Search For Rates"
                            OnClientClick="CallLookupRatesFromDatLoadboard(); return false;" />

                        <asp:Button runat="server" CausesValidation="False" ID="btnClose" Text="Close" OnClick="OnCloseManageAssetsOnLoadboardClicked" />
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnUpdateAssetAtDat" />
                <asp:PostBackTrigger ControlID="btnClose" />
            </Triggers>
        </asp:UpdatePanel>
    </div>

    <div class="popupControlOver popupControlOverW500 top50" id="divRateEntryForDat" style="display: none;">
        <div class="popheader mb10">
            <h4>Modify DAT Asset</h4>
            <img title="Close" align="AbsMiddle" width="20" src='<%= ResolveUrl("~/images/icons2/close.png") %>' class="close"
                onclick="DisplayRateEntryForDatPosting(false);" />
        </div>
        <script type="text/javascript">
            function UpdateTotalRateForAssetPost() {
                var baseRate = Number($('#<%= txtBaseRateForAssetPost.ClientID %>').val());
                if ($('#<%= ddlDatRateTypeForAssetPost.ClientID %>').val() == '<%= RateBasedOnType.PerMile.ToInt() %>') {
                    $('#<%= txtDatRateEntryTotalForAssetPost.ClientID %>').val((baseRate * Number($('#<%= txtMileageForAssetPost.ClientID %>').val())).toFixed(2));
            } else {
                $('#<%= txtDatRateEntryTotalForAssetPost.ClientID %>').val(baseRate.toFixed(2));
            }
        }
        </script>

        <table class="poptable">
            <tr>
                <td class="text-right" style="width: 20%">
                    <label class="upper">Base Rate:</label>
                </td>
                <td>
                    <eShip:CustomTextBox runat="server" ID="txtBaseRateForAssetPost" CssClass="w200" Type="FloatingPointNumbers"
                        onchange="UpdateTotalRateForAssetPost();" />
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    <label class="upper">Rate Type:</label>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlDatRateTypeForAssetPost" DataTextField="Text" DataValueField="Value" CssClass="w200"
                        onchange="UpdateTotalRateForAssetPost();" />
                </td>
            </tr>
            <tr id="trShipmentMileage">
                <td class="text-right">
                    <label class="upper">Mileage:</label>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtMileageForAssetPost" CssClass="w200 disabled" ReadOnly="True" />
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    <label class="upper">Total: </label>
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtDatRateEntryTotalForAssetPost" CssClass="w200 disabled" ReadOnly="True" />

                </td>
            </tr>
        </table>

        <asp:UpdatePanel runat="server" ID="udpDatComments" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="finderScrollFixed" style="max-height: 160px;">
                    <table class="poptable">
                        <tr>
                            <td colspan="2">

                                <hr class="w100p ml-inherit" />
                                <h5 class="ml10 dark">Comments
                                            <asp:ImageButton runat="server" ID="ibtnAddComment" ToolTip="Add Comment" ImageUrl="~/images/icons2/add.png"
                                                CausesValidation="false" OnClick="OnAddDatCommentClicked" Visible="True" />
                                </h5>
                                <hr class="w100p ml-inherit dark dotted mb10" />
                                <div class="row">
                                    <div class="fieldgroup right">
                                        <table class="poptable">
                                            <asp:Repeater runat="server" ID="rptDatAssetComments">
                                                <HeaderTemplate>
                                                    <tr>
                                                        <th style="width: 85%;" class="text-left blue">
                                                            <label class="upper">Note</label>
                                                        </th>
                                                        <th style="width: 15%;" class="text-center blue">
                                                            <label class="upper">Delete</label>
                                                        </th>
                                                    </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <eShip:CustomTextBox runat="server" ID="txtDatAssetComment" CssClass='<%# Eval("Exists").ToBoolean() ? "w420 disabled" : "w420" %>'
                                                                Text='<%# Eval("Comment") %>' MaxLength="70" ReadOnly='<%# Eval("Exists").ToBoolean() %>' />

                                                            <eShip:CustomHiddenField runat="server" ID="hidDatCommentIndex" Value='<%# Container.ItemIndex %>' />
                                                            <eShip:CustomHiddenField runat="server" ID="hidDatCommentExists" Value='<%# Eval("Exists") %>' />
                                                        </td>
                                                        <td class="text-center">
                                                            <asp:ImageButton ID="ibtnDelete" runat="server" ToolTip="Delete" ImageUrl="~/images/icons2/deleteX.png"
                                                                CausesValidation="false" OnClick="OnDeleteDatCommentClicked" Visible='<%# !Eval("Exists").ToBoolean() %>' />
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>

                                        </table>
                                    </div>
                                </div>

                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ibtnAddComment" />
            </Triggers>
        </asp:UpdatePanel>
        <div class="row mt20 mb10">
            <div class="fieldgroup right">
                <asp:Button runat="server" ID="btnPostAssetToDat" Text="Post Asset To DAT" OnClientClick="CallPostAssetToDatLoadboard();return false;"
                    Visible='<%# hidAssetId.Value == string.Empty %>' />
                <asp:Button runat="server" ID="btnUpdateAssetOnDat" Text="Update Asset" OnClientClick="CallUpdateAssetToDatLoadboard();return false;"
                    Visible='<%# hidAssetId.Value != string.Empty %>' />
                <asp:Button runat="server" Text="Cancel" OnClientClick="DisplayRateEntryForDatPosting(false); return false;" />
            </div>
        </div>

    </div>

</asp:Panel>

<asp:UpdatePanel runat="server" ID="udpHiddenFields" UpdateMode="Always">
    <ContentTemplate>
        <eShip:CustomHiddenField runat="server" ID="hidUserId" />

        <eShip:CustomHiddenField runat="server" ID="hidLoadOrderId" />
        <eShip:CustomHiddenField runat="server" ID="hidLoadOrderMileage" />
        <eShip:CustomHiddenField runat="server" ID="hidLoadOrderUnitBuyTotal" />

        <eShip:CustomHiddenField runat="server" ID="hidAssetId" />
        <eShip:CustomHiddenField runat="server" ID="hidTenantId" />
    </ContentTemplate>
</asp:UpdatePanel>

<asp:Panel ID="pnlDatLoadboardAssetDimScreen" CssClass="dimBackgroundControl" runat="server" Visible="false" />
<eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
