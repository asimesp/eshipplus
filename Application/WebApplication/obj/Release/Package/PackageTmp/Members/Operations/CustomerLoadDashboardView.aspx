﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="CustomerLoadDashboardView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.CustomerLoadDashboardView"
    EnableEventValidation="false" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowNew="True" ShowMore="True"
        OnNew="OnToolbarNewClicked" OnCommand="OnToolbarCommand" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:UpdatePanel runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
                        Customer Loads Dashboard<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </h3>
        </div>
        <span class="clearfix"></span>
        <hr class="dark mb10" />
        <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="CustomerLoads" OnProcessingError="OnSearchProfilesProcessingError"
                            OnSearchProfileSelected="OnSearchProfilesProfileSelected" ShowAutoRefresh="True" OnAutoRefreshChanged="OnSearchProfilesAutoRefreshChanged" />
                    </div>
                </div>
            </div>
            <hr class="dark mb10" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                        OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>
            <hr class="fat" />
            <div class="rowgroup">
                <div class="row mb10">
                    <div class="left">
                        <asp:Button ID="btnSearch" CssClass="left" runat="server" Text="SEARCH" OnClick="OnSearchClicked"
                            CausesValidation="False" />
                    </div>
                    <div class="right">
                        <div class="fieldgroup">
                            <label>Sort By:</label>
                            <asp:DropDownList runat="server" ID="ddlSortBy" DataTextField="DisplayName" DataValueField="Name"
                                OnSelectedIndexChanged="OnSortBySelectedIndexChanged" AutoPostBack="True" />
                        </div>
                        <div class="fieldgroup">
                            <asp:RadioButton runat="server" ID="rbAsc" Text="ASC" GroupName="rbSortBy" AutoPostBack="True"
                                ToolTip="Ascending" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                        </div>
                        <div class="fieldgroup">
                            <asp:RadioButton runat="server" ID="rbDesc" Text="DESC" GroupName="rbSortBy" Checked="True"
                                ToolTip="Descending" AutoPostBack="True" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:UpdatePanel runat="server" ID="upDataUpdate">
            <ContentTemplate>
                <eShip:ContinuousScrollExtender ID="upcseDataUpdate" runat="server" BindableControlId="lstShipmentDetails" UpdatePanelToExtendId="upDataUpdate" />
                <eShip:ShippingLabelGeneratorControl runat="server" ID="shippingLabelGenerator" Visible="false" OnClose="OnShippingLabelGeneratorCloseClicked" />
                <asp:Timer runat="server" ID="tmRefresh" Interval="30000" OnTick="OnTimerTick" />
                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheCustomerLoadDashboardTable" TableId="customerLoadDashboardTable" HeaderZIndex="2" />
                <table class="line2 pl2" id="customerLoadDashboardTable">
                    <tr>
                        <th style="width: 9%;">
                            <asp:LinkButton runat="server" ID="lbtnSortShipmentNumber" CssClass="link_nounderline blue" OnCommand="OnSortData">
                                <abbr title="Shipment Number">Shipment #</abbr>
                            </asp:LinkButton>
                        </th>
                        <th style="width: 9%;">
                            <asp:LinkButton runat="server" ID="lbtnSortDateCreated" OnCommand="OnSortData" CssClass="link_nounderline blue">
                                Date Created
                            </asp:LinkButton>
                        </th>
                        <th style="width: 8%;">
                            <asp:LinkButton runat="server" ID="lbtnSortDesiredPickup" CssClass="link_nounderline blue" OnCommand="OnSortData">
                                Desired Pickup
                            </asp:LinkButton>
                        </th>
                        <th style="width: 8%;">
                            <asp:LinkButton runat="server" ID="lbtnSortActualPickup" CssClass="link_nounderline blue" OnCommand="OnSortData">
                                Actual Pickup
                            </asp:LinkButton>
                        </th>
                        <th style="width: 8%;">
                            <asp:LinkButton runat="server" ID="lbtnSortEstimatedDelivery" CssClass="link_nounderline blue" OnCommand="OnSortData">
                                Estimated Delivery
                            </asp:LinkButton>
                        </th>
                        <th style="width: 8%;">
                            <asp:LinkButton runat="server" ID="lbtnSortActualDelivery" CssClass="link_nounderline blue" OnCommand="OnSortData">
                                Actual Delivery
                            </asp:LinkButton>
                        </th>
                        <th style="width: 10%;" class="blue">
                            <abbr title="Amount Due">Amt. Due</abbr>
                        </th>
                        <th style="width: 15%;">
                            <asp:LinkButton runat="server" ID="lbtnSortServiceMode" Text="Service Mode" CssClass="link_nounderline blue" OnCommand="OnSortData" />
                        </th>
                        <th style="width: 10%;">
                            <asp:LinkButton runat="server" ID="lbtnSortStatus" Text="Status" CssClass="link_nounderline blue" OnCommand="OnSortData" />
                        </th>
                        <th style="width: 7%;" class="text-center">Action</th>
                    </tr>
                    <asp:ListView runat="server" ID="lstShipmentDetails" ItemPlaceholderID="itemPlaceHolder"
                        OnItemDataBound="OnShipmentDetailsItemDataBound">
                        <LayoutTemplate>
                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                        </LayoutTemplate>
                        <ItemTemplate>
                            <eShip:CustomerLoadDashboardDetailControl ID="customerLoadDashboardDetailControl" ItemIndex='<%# Container.DataItemIndex %>'
                                runat="server" OnGenerateShippingLabel="OnGenerateShippingLabel" />
                        </ItemTemplate>
                    </asp:ListView>
                </table>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lbtnSortShipmentNumber" />
                <asp:PostBackTrigger ControlID="lbtnSortDateCreated" />
                <asp:PostBackTrigger ControlID="lbtnSortDesiredPickup" />
                <asp:PostBackTrigger ControlID="lbtnSortActualPickup" />
                <asp:PostBackTrigger ControlID="lbtnSortEstimatedDelivery" />
                <asp:PostBackTrigger ControlID="lbtnSortActualDelivery" />
                <asp:PostBackTrigger ControlID="lbtnSortServiceMode" />
                <asp:PostBackTrigger ControlID="lbtnSortStatus" />
              </Triggers>
        </asp:UpdatePanel>

        <asp:Panel runat="server" ID="pnlRequestShipentCancellation" Visible="False" DefaultButton="btnContinue">
            <div class="popup">
                <div class="popheader">
                    <h4>Request Shipment Cancellation
                    </h4>
                    <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/icons2/close.png" CssClass="close"
                        CausesValidation="false" OnClick="OnCloseShipmentCancellationClicked" runat="server" />
                </div>
                <div class="row">
                    <table>
                        <tr>
                            <td class="text-right">
                                <label class="upper blue">Shipment Number:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox runat="server" ID="txtVoidShipmentNumber" CssClass="w200" />
                            </td>
                            <td>
                                <asp:Button runat="server" ID="btnVoidShipment" Text="CANCEL SHIPMENT" CausesValidation="False" Visible="False" OnClick="OnShipmentRequestCancellationVoidShipmentClicked" />
                                <asp:Button runat="server" ID="btnContinue" Text="FETCH SHIPMENT" CausesValidation="False" OnClick="OnShipmentRequestCancellationContinueClicked" />
                                <asp:Button runat="server" ID="btnCancel" Text="CLOSE" CausesValidation="False" OnClick="OnShipmentCancellationRequestCancelClicked" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="row">
                    <asp:Literal runat="server" ID="litBillofLading" />
                </div>
            </div>
        </asp:Panel>

        <asp:Panel runat="server" ID="pnlRebookShipment" Visible="False" DefaultButton="btnRebook">
            <div class="popup popupControlOverW500">
                <div class="popheader">
                    <h4>Rebook Shipment
                    </h4>
                    <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                        CausesValidation="false" OnClick="OnCloseRebookShipmentClicked" runat="server" />
                </div>
                <div class="row">
                    <table class="poptable">
                        <tr>
                            <td colspan="2">
                                <ul>
                                    <li class="note">Please review all information on next screen before submission.
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Shipment Number:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox runat="server" ID="txtRebookShipmentNumber" CssClass="w200" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="pt10">
                                <asp:Button runat="server" ID="btnRebook" Text="REBOOK SHIPMENT" CausesValidation="False" OnClick="OnRebookShipmentClicked" />
                                <asp:Button runat="server" ID="btnCancelRebook" Text="CANCEL" CausesValidation="False" OnClick="OnCancelRebookShipmentClicked" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>
    <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information"
        OnOkay="OnOkayProcess" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
</asp:Content>
