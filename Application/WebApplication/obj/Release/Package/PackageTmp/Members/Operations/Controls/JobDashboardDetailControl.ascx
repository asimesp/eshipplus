﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="JobDashboardDetailControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.JobDashboardDetailControl" %>
<eShip:CustomHiddenField ID="hidItemIndex" runat="server" />
<eShip:CustomHiddenField ID="hidJobId" runat="server" />
<tr>
    <td rowspan="2" class="top">
        <asp:Literal runat="server" ID="litJobNumber" />
    </td>
    <td>
        <asp:Literal runat="server" ID="litDateCreated" />
    </td>
    <td>
        <asp:Literal runat="server" ID="litCustomerNameNumber" />
        <asp:HyperLink ID="hypCustomer" Target="_blank" runat="server" ToolTip="Go To Customer Record" Visible="False">
            <img src='<%= ResolveUrl("~/images/icons2/arrow_newTab.png") %>' alt="" align="absmiddle" width="16"/>
        </asp:HyperLink>
    </td> 
    <td>
        <asp:Literal runat="server" ID="litUser" />
        <asp:HyperLink ID="hypUser" Target="_blank" runat="server" ToolTip="Go To Customer Record" Visible="False">
            <img src='<%= ResolveUrl("~/images/icons2/arrow_newTab.png") %>' alt="" align="absmiddle" width="16"/>
        </asp:HyperLink>
    </td>

    
    <td>
        <asp:Literal runat="server" ID="litExternalReference1" />
    </td>
    <td>
        <asp:Literal runat="server" ID="litExternalReference2" />
    </td>
    <td>
        <asp:Literal runat="server" ID="litStatus" />
    </td>
    <td rowspan="2" class="top text-center">
        <asp:HyperLink runat="server" ID="hypJobView" Target="_blank">
            <asp:Image runat="server" ID="imgGoToJobView" ImageUrl="~/images/icons2/arrow_newTab.png" AlternateText="Go to Job Record" />
        </asp:HyperLink>
    </td>
</tr>
<tr>
    <td colspan="6" class="top forceLeftBorder">
        <div class="rowgroup">
            <div class="col_1_3 no-right-border">
                <div class="row">
                    <div class="fieldgroup">
                        <label class="wlabel blue">Shipments</label>
                        <label>
                            <asp:Literal runat="server" ID="litShipments" />
                            <asp:HyperLink ID="hypShipment" Target="_blank" runat="server" ToolTip="Go To Shipment Record" Visible="False">
                                <asp:Image ImageUrl="~/images/icons2/arrow_newTab.png" runat="server" Width="16px" CssClass="middle"/>
                            </asp:HyperLink>
                        </label>
                    </div>
                </div>
                </div>
            <div class="col_1_3 no-right-border">
                <div class="row">
                    <div class="fieldgroup">
                        <label class="wlabel blue">Service Tickets</label>
                        <label>
                            <asp:Literal runat="server" ID="litServiceTickets" />
                            <asp:HyperLink ID="hypServiceTicket" Target="_blank" runat="server" ToolTip="Go To ServiceTicket Record" Visible="False">
                                <asp:Image ImageUrl="~/images/icons2/arrow_newTab.png" runat="server" Width="16px" CssClass="middle"/>
                            </asp:HyperLink>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col_1_3 no-right-border">
                <div class="row">
                    <div class="fieldgroup">
                        <label class="wlabel blue">Load Orders</label>
                        <label>
                            <asp:Literal runat="server" ID="litLoadOrders" />
                            <asp:HyperLink ID="hypLoadOrder" Target="_blank" runat="server" ToolTip="Go To LoadOrder Record" Visible="False">
                                <asp:Image ImageUrl="~/images/icons2/arrow_newTab.png" runat="server" Width="16px" CssClass="middle"/>
                            </asp:HyperLink>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="fieldgroup">
                    </div>
                </div>
            </div>
        </div>
    </td>
</tr>