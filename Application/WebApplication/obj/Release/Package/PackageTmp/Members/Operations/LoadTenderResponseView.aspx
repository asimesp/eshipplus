﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="LoadTenderResponseView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.LoadTenderResponseView" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
	<eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="false"
	                   ShowEdit="false" ShowDelete="false" ShowSave="false" ShowFind="false" ShowMore="false" />
</asp:Content>

<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
	<eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information" OnOkay="OnOkayProcess"  />
	<div class="clearfix">
		<div class="pageHeader">
			<asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
			           CssClass="pageHeaderIcon" />
			<h3>Response to Load Tender</h3>
		</div>
		<div align="center">
			<div class="row pb20">
				<asp:Panel ID="pnlVoidResponsePage" runat="server" CssClass="flag-shipment"   Visible="false">
					Response options are inactive now! This load tender is either expired, cancelled or already covered.
				</asp:Panel>
			</div>
			<div class="row">
				<asp:Button ID="responseYes" Text="Yes, can cover this load." OnClick="OnYesClicked" class="mr20" runat="server" CausesValidation="False" />
				<asp:Button ID="responseNo" Text="No, cannot cover this load." OnClick="OnNoClicked" class="mr20" runat="server" CausesValidation="False" />
			</div>
		</div>
	</div>
	<asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
  
</asp:Content>

