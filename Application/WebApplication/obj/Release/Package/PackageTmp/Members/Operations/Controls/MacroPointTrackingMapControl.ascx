﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MacroPointTrackingMapControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.MacroPointTrackingMapControl" %>
<asp:Panel runat="server" ID="pnlTrackingMap" CssClass="popupControl" Visible="false">
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA0ymfFetmLi9eu0snHyX5p5kqn4bFhH5U">
    </script>
    <script type='text/javascript'>
        function initialize() {
            //Set center to North America and zoom out
            var mapOptions = {
                center: { lat: 37.062778, lng: -95.676944 },
                zoom: 3
            };
            var map = new google.maps.Map(document.getElementById('map-canvas'),
                mapOptions);
            
            var coordinatePairs = '<%= hidCoordinatesString.Value %>'.split(';');
            var markers = [];
            var points = [];
            var i;
            
            for (i = 0; i < coordinatePairs.length - 1; i++) {
                var coordinatePair = coordinatePairs[i].split(',');
                var marker = CreateMarkerWithLabel(i + 1, map, Number(coordinatePair[0]), Number(coordinatePair[1]));
                markers.push(marker);
                points.push(marker.position);
            }

           

            if (markers.length > 0) {
                var bounds = new google.maps.LatLngBounds();
                for (i = 0; i < markers.length; i++) {
                    bounds.extend(markers[i].getPosition());
                }

                map.setCenter(bounds.getCenter());
                map.fitBounds(bounds);
                map.setZoom(map.getZoom() - 1);

                if (map.getZoom() > 15) {
                    map.setZoom(15);
                }

                for (i = 0; i < points.length - 1; i++) {
                    new google.maps.Polyline({
                        path: [points[i], points[i + 1]],
                        icons: [{
                            icon: { path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW },
                            offset: '52%'
                        }],
                        strokeColor: "#c3524f",
                        strokeOpacity: 1.0,
                        strokeWeight: 2,
                        map: map
                    });
                }
            }
        }

        function CreateMarkerWithLabel(text, map, x, y) {
            return new google.maps.Marker({
                position: { lat: x, lng: y },
                map: map,
                icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' + text + '|FF4719|000000'
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>

    <div class="popheader">
        <h4>Macro Point Tracking Map</h4>
        <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close" CausesValidation="false" OnClick="OnCloseTrackingMapClicked" runat="server" />
    </div>
    <asp:Panel runat="server" ID="pnlRequestLocationUpdate" Visible="False">
        <div class="row pt5 pb5">
            <div class="fieldgroup pl20">
                <asp:Button runat="server" ID="btnRequestLocationUpdate" Text="Request Location Update" OnClick="OnRequestLocationUpdateClicked" />
            </div>
        </div>
    </asp:Panel>
    <div id="map-canvas" class="h700"></div>
</asp:Panel>

<asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
<eShip:CustomHiddenField runat="server" ID="hidCoordinatesString" />
