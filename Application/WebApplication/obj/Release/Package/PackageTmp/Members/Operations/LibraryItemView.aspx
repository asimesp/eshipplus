﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="LibraryItemView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.LibraryItemView" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.Registry" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>
<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        ShowEdit="false" ShowDelete="false" ShowSave="false" ShowFind="false" ShowImport="true"
        ShowExport="true" OnNew="OnNewLibraryItemClicked" OnExport="OnExportClicked"
        OnImport="OpenLibraryItemImport" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                CssClass="pageHeaderIcon" />
            <h3>Library Item <small class="ml10">
                <asp:Literal runat="server" ID="litRecordCount" /></small></h3>
            <div class="lockMessage">
                <asp:Literal ID="litMessage" runat="server" />
            </div>
        </div>
        <span class="clearfix"></span>
        <hr class="dark mb10" />
        <asp:Panel runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row mb10 pb10 bottom_shadow">
                    <div class="fieldgroup">
                        <label class="upper blue">Customer: </label>
                        <eShip:CustomHiddenField runat="server" ID="hidCustomerId" />
                       
                        <eShip:CustomTextBox runat="server" ID="txtCustomerNumber" CssClass="w110" OnTextChanged="OnCustomerNumberEntered"
                            AutoPostBack="true" />
                        <asp:RequiredFieldValidator runat="server" ID="rfvCustomerNumber" ControlToValidate="txtCustomerNumber"
                            ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                        <asp:ImageButton ID="btnCustomerSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                            CausesValidation="false" OnClick="OnCustomerSearchClicked"/>
                        <eShip:CustomTextBox runat="server" ID="txtCustomerName" CssClass="w280 disabled" ReadOnly="true" />
                        <ajax:AutoCompleteExtender runat="server" ID="aceCustomer" TargetControlID="txtCustomerNumber" ServiceMethod="GetCustomerList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                            EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                         <%= hidCustomerId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.Customer) 
                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Customer Record'><img src={3} width='20' class='middle'/></a>", 
                                                        ResolveUrl(CustomerView.PageAddress),
                                                        WebApplicationConstants.TransferNumber, 
                                                        hidCustomerId.Value.UrlTextEncrypt(),
                                                        ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                : string.Empty %>
                    </div>
                </div>
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="LibraryItems" ShowAutoRefresh="False"
                            OnProcessingError="OnSearchProfilesProcessingError" OnSearchProfileSelected="OnSearchProfilesProfileSelected" />
                    </div>
                </div>
            </div>
            <hr class="dark mb10" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                        OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>
            <hr class="fat" />
            <div class="rowgroup">
                <div class="row mb10">
                    <div class="left">
                        <asp:Button ID="btnSearch" CssClass="mr10" runat="server" Text="SEARCH" OnClick="OnSearchClicked" CausesValidation="False" />
                        <asp:Button ID="btnExport" runat="server" Text="EXPORT" OnClick="OnExportClicked" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheLibraryItemTable" TableId="libraryItemTable" HeaderZIndex="2" />
        <div class="rowgroup">
            <table class="line2 pl2" id="libraryItemTable">
                <tr>
                    <th style="width: 15%;">Description
                    </th>
                    <th style="width: 7%;" class="text-right">Piece Count
                    </th>
                    <th style="width: 8%;">Package Type
                    </th>
                    <th style="width: 8%;" class="text-right">Freight Class
                    </th>
                    <th style="width: 9%;" class="text-right">Weight
                        <abbr title="Pounds">(lb)</abbr>
                    </th>
                    <th style="width: 9%;" class="text-right">Length
                        <abbr title="Inches">(in)</abbr>
                    </th>
                    <th style="width: 8%;" class="text-right">Width
                        <abbr title="Inches">(in)</abbr>
                    </th>
                    <th style="width: 9%;" class="text-right">Height
                        <abbr title="Inches">(in)</abbr>
                    </th>
                    <th style="width: 5%;">Stackable
                    </th>
                    <th style="width: 5%;"><abbr title="Hazardous Material">HazMat</abbr>
                    </th>
                    <th style="width: 9%;">
                        <abbr title="National Motor Freight Classification">
                            NMFC</abbr>
                        <br />
                        <abbr title="Harmonized Tariff Schedule">
                            HTS</abbr>
                    </th>
                    <th style="width: 8%;" class="text-center">Action
                    </th>
                </tr>
                <asp:ListView ID="lstLibraryItems" runat="server" ItemPlaceholderID="itemPlaceHolder">
                    <LayoutTemplate>
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td rowspan="2" class="top">
                                <asp:Literal ID="litDescription" runat="server" Text='<%# Eval("Description") %>' />
                                <eShip:CustomHiddenField ID="hidLibraryItemId" runat="server" Value='<%# Eval("Id") %>' />
                            </td>
                            <td class="text-right">
                                <asp:Literal ID="litPieceCount" runat="server" Text='<%# Eval("PieceCount") %>' />
                            </td>
                            <td>
                                <%# new PackageType(Eval("PackageTypeId").ToLong()).TypeName %>
                            </td>
                            <td class="text-right">
                                <asp:Literal ID="litFreightClass" runat="server" Text='<%# ddlFreightClass.GetValueText(Eval("FreightClass").ToString()) %>' />
                            </td>
                            <td class="text-right">
                                <asp:Literal ID="litWeight" runat="server" Text='<%# Eval("Weight") %>' />
                            </td>
                            <td class="text-right">
                                <asp:Literal ID="litLength" runat="server" Text='<%# Eval("Length") %>' />
                            </td>
                            <td class="text-right">
                                <asp:Literal ID="litWidth" runat="server" Text='<%# Eval("Width") %>' />
                            </td>
                            <td class="text-right">
                                <asp:Literal ID="litHeight" runat="server" Text='<%# Eval("Height") %>' />
                            </td>
                            <td class="text-center">
                                <%# Eval("IsStackable").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                            </td>
                            <td class="text-center">
                                <%# Eval("HazardousMaterial").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                            </td>
                            <td>
                                <asp:Literal ID="litNMFCCode" runat="server" Text='<%# Eval("NMFCCode") %>' />
                                <br />
                                <asp:Literal ID="litHTSCode" runat="server" Text='<%# Eval("HTSCode") %>' />
                            </td>
                            <td rowspan="2" class="top text-center">
                                <asp:ImageButton ID="ibtnEdit" ToolTip="Edit" runat="server" ImageUrl="~/images/icons2/editBlue.png"
                                    CausesValidation="false" OnClick="OnEditLibraryItemClicked" Enabled='<%# Access.Modify %>' />
                                <asp:ImageButton ID="ibtnDelete" ToolTip="Delete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                    CausesValidation="false" OnClick="OnDeleteLibraryItemClicked" Enabled='<%# Access.Remove %>' 
                                    OnClientClick="if (!confirm('Are you sure you want to delete record?')) return false;" />
                            </td>
                        </tr>
                        <tr class="f9">
                            <td colspan="9" class="forceLeftBorder">
                                <div class="fieldgroup">
                                    <span class="blue mr10"><b>Comment: </b></span>
                                    <asp:Literal runat="server" ID="litComment" Text='<%# Eval("Comment") %>' />
                                </div>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </table>
        </div>
        <asp:Panel ID="pnlEdit" class="popupControl popupControlOverW750" runat="server" Visible="False">
            <div class="popheader">
                <h4>
                    <asp:Literal ID="litTitle" runat="server" />
                </h4>
                <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                    CausesValidation="False" OnClick="OnEditCloseClicked" runat="server" />
            </div>
            <div class="row mt20">
                <table class="poptable center">
                    <tr>
                        <td colspan="20" class="red">
                            <asp:Literal ID="litLibraryItemFailedLock" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
            <div class="row mt10">
                <div class="col_1_2 bbox pl20">
                    <div class="row">
                        <table class="poptable">
                            <tr>
                                <td class="text-right top">
                                    <label class="upper">
                                        Description:
                                    </label>
                                </td>
                                <td>
                                    <eShip:CustomHiddenField ID="hidEditLibraryItemId" runat="server" />
                                    <eShip:CustomTextBox ID="txtDescription" CssClass="w200" MaxLength="100" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <label class="upper">
                                        Package
                                    <abbr title="Quantity">Qty.</abbr>:</label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox ID="txtQuantity" Text="1" runat="server" ReadOnly="true" CssClass="w100 disabled" MaxLength="50" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <label class="upper">Piece Count:</label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox runat="server" ID="txtPieceCount" CssClass="w100" MaxLength="50" Type="NumbersOnly"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <label class="upper">
                                        <abbr title="National Motor Freight Classification">
                                            NMFC</abbr>
                                        Code:
                                    </label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox ID="txtNmfcCode" runat="server" CssClass="w200" MaxLength="50" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <label class="upper">
                                        <abbr title="Harmonized Tariff Schedule">
                                            HTS</abbr>
                                        Code:
                                    </label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox ID="txtHtsCode" runat="server" CssClass="w200" MaxLength="50" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right top">
                                    <label class="upper">Comment:</label>
                                    <br />
                                    <label class="lhInherit">
                                        <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleComment" TargetControlId="txtComment" MaxLength="50" />
                                    </label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox ID="txtComment" runat="server" TextMode="MultiLine" Rows="3" CssClass="w200" MaxLength="50" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col_1_2 bbox vlinedarkleft pl20">
                    <div class="row">
                        <table class="poptable">
                            <tr>
                                <td class="text-right">
                                    <label class="upper">Package Type:</label>
                                </td>
                                <td>
                                    <eShip:CachedObjectDropDownList Type="PackageType" ID="ddlPackageTypes" runat="server" CssClass="w200"
                                        EnableChooseOne="True"  DefaultValue="0" />
                                    <asp:ImageButton runat="server" ID="imgSetPackageDimensions" ImageUrl="~/images/icons2/cog.png"
                                        ToolTip="Set Package Type Dimensions" OnClick="OnSetPackageDimensionsClicked" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <label class="upper">Freight Class:</label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlFreightClass" runat="server" CssClass="w130" DataTextField="Text"
                                        DataValueField="Value" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <label class="upper">
                                        Weight
                                    <abbr title="Pounds">(lb)</abbr>:</label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox ID="txtWeight" runat="server" CssClass="w130" MaxLength="8" />
                                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtWeight"
                                        FilterType="Custom, Numbers" ValidChars="." />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <label class="upper">
                                        Length
                                    <abbr title="Inches">(in)</abbr>:</label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox ID="txtLength" runat="server" CssClass="w130" MaxLength="8" />
                                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtLength"
                                        FilterType="Custom, Numbers" ValidChars="." />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <label class="upper">
                                        Width
                                    <abbr title="Inches">(in)</abbr>:</label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox ID="txtWidth" runat="server" CssClass="w130" MaxLength="8" />
                                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtWidth"
                                        FilterType="Custom, Numbers" ValidChars="." />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <label class="upper">
                                        Height
                                    <abbr title="Inches">(in)</abbr>:</label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox ID="txtHeight" runat="server" CssClass="w130" MaxLength="8" />
                                    <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtHeight"
                                        FilterType="Custom, Numbers" ValidChars="." />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox runat="server" ID="chkIsHazmat" CssClass="jQueryUniform" />
                                    <label class="active"><abbr title="Hazardous Material">HazMat</abbr></label>
                                </td>
                                <td> 
                                    <asp:CheckBox runat="server" ID="chkIsStackable" CssClass="jQueryUniform" />
                                    <label class="active">Stackable</label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row mt20">
                <div class="row">
                    <table class="poptable center">
                        <tr>
                            <td class="pt10">
                                <asp:Button ID="btnSave" Text="Save" OnClick="OnSaveLibraryItemClicked" runat="server" />
                                <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnEditCloseClicked" runat="server"
                                     CausesValidation="False" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>
    <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
    <eShip:CustomerFinderControl ID="customerFinder" runat="server" OnItemSelected="OnCustomerFinderItemSelected"
        OnSelectionCancel="OnCustomerFinderSelectionCancelled" Visible="false" OpenForEditEnabled="false" EnableMultiSelection="false" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information" OnOkay="OnOkayProcess" />
    <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
        OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
</asp:Content>
