﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="AddressBookView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.AddressBookView" %>
<%@ Import Namespace="System.IdentityModel.Metadata" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true" ShowUnlock="False"
        ShowEdit="true" ShowDelete="true" ShowSave="true" ShowFind="true"
        OnFind="OnToolbarFindClicked" OnSave="OnToolbarSaveClicked" OnDelete="OnToolbarDeleteClicked"
        OnNew="OnToolbarNewClicked" OnEdit="OnToolbarEditClicked" OnUnlock="OnToolbarUnlockClicked"
        OnImport="OnToolbarImportClicked" OnExport="OnToolbarExportClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Address Book
                 <eShip:RecordIdentityDisplayControl runat="server" ID="ridcAddressBookIdentity" TargetControlId="txtAddressBookDescription" />
            </h3>
            <div class="lockMessage">
                <asp:Literal ID="litMessage" runat="server" />
            </div>
        </div>
        <hr class="dark mb5" />
        <div class="errorMsgLit">
            <asp:Literal runat="server" ID="litErrorMessages" />
        </div>

        <eShip:CustomerFinderControl ID="customerFinder" runat="server" EnableMultiSelection="false"
            Visible="False" OpenForEditEnabled="false" OnSelectionCancel="OnCustomerFinderSelectionCancelled" OnItemSelected="OnCustomerFinderItemSelected" />
        <eShip:PostalCodeFinderControl runat="server" ID="postalCodeFinder" EnableMultiSelection="false" Visible="false" OnItemSelected="OnPostalCodeFinderItemSelected"
            OnSelectionCancel="OnPostalCodeFinderItemSelectionCancelled" />
        <eShip:AddressBookFinderControl runat="server" ID="addressBookFinder" Visible="false" OnItemSelected="OnAddressBookFinderItemSelected"
            OnSelectionCancel="OnAddresBookItemSelectionCancelled" OpenForEditEnabled="true" CustomerIdSpecificFilter="0" />
        <eShip:ServiceFinderControl ID="serviceFinder" runat="server" EnableMultiSelection="true" Visible="false"
            OnMultiItemSelected="OnServiceFinderMultiItemSelected" OnSelectionCancel="OnServiceFinderSelectionCancelled" />
        <eShip:AjaxTabHeaderTextUpdater runat="server" ID="athtuTabUpdater" />

        <eShip:CustomHiddenField runat="server" ID="hidAddressBookId" />
        <eShip:CustomHiddenField runat="server" ID="hidCustomerId" />

        <ajax:TabContainer ID="tabAddressBook" runat="server" CssClass="ajaxCustom">
            <ajax:TabPanel runat="server" ID="tabDetails" HeaderText="Details">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlDetails">
                        <div class="col_1_3">
                            <div class="rowgroup">
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Customer
                                            <%= hidCustomerId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.Customer) 
                                                    ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Customer Record'><img src={3} width='20' class='middle'/></a>", 
                                                                            ResolveUrl(CustomerView.PageAddress),
                                                                            WebApplicationConstants.TransferNumber, 
                                                                            hidCustomerId.Value.UrlTextEncrypt(),
                                                                            ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                    : string.Empty %>
                                        </label>
                                        <eShip:CustomTextBox runat="server" ID="txtCustomerNumber" CssClass="w80" OnTextChanged="OnCustomerNumberEntered"
                                            AutoPostBack="True" />
                                        <asp:ImageButton ID="imgCustomerSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                                            CausesValidation="False" OnClick="OnCustomerSearchClicked" />
                                        <eShip:CustomTextBox runat="server" ID="txtCustomerName" CssClass="w200 disabled" ReadOnly="True" BackColor="LightGray" />
                                        <ajax:AutoCompleteExtender runat="server" ID="aceCustomer" TargetControlID="txtCustomerNumber" ServiceMethod="GetCustomerList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                            EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Name/Description
                                        </label>
                                        <eShip:CustomTextBox runat="server" ID="txtAddressBookDescription" MaxLength="50" CssClass="w310" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Street 1 </label>
                                        <eShip:CustomTextBox ID="txtStreetOne" MaxLength="50" runat="server" CssClass="w310" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Street 2 </label>
                                        <eShip:CustomTextBox ID="txtStreetTwo" MaxLength="50" runat="server" CssClass="w310" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">City </label>
                                        <eShip:CustomTextBox ID="txtCity" MaxLength="100" runat="server" CssClass="w310" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">State </label>
                                        <eShip:CustomTextBox ID="txtState" MaxLength="50" runat="server" CssClass="w310" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Postal Code </label>
                                        <eShip:CustomTextBox ID="txtPostalCode" MaxLength="10" runat="server" CssClass="w200" />
                                        <asp:ImageButton ID="imgPostalCodeSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                                            CausesValidation="False" OnClick="OnPostalCodeSearchClicked" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Country </label>
                                        <eShip:CachedObjectDropDownList Type="Countries" ID="ddlCountries" runat="server" DefaultValue="0"
                                            CssClass="w200"  EnableChooseOne="True"  />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col_1_3 no-right-border">
                            <div class="rowgroup pt40">
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Origin Instructions
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleAddressOriginInstructions" MaxLength="500" TargetControlId="txtOringinInstructions" />
                                        </label>
                                    </div>
                                    <div class="fieldgroup">
                                        <eShip:CustomTextBox ID="txtOringinInstructions" TextMode="MultiLine" CssClass="w330 h150" runat="server" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Destination Instructions
                                               <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleAddressDestinationInstructions" MaxLength="500" TargetControlId="txtDestinationInstructions" />
                                        </label>
                                    </div>
                                    <div class="fieldgroup">
                                        <eShip:CustomTextBox ID="txtDestinationInstructions" TextMode="MultiLine" CssClass="w330 h150" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col_1_3">
                            <div class="rowgroup pt40">
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            General Information
                                                  <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleAddressGeneralInformation" MaxLength="2000" TargetControlId="txtGeneralInfo" />
                                        </label>
                                    </div>
                                    <div class="fieldgroup">
                                        <eShip:CustomTextBox ID="txtGeneralInfo" TextMode="MultiLine" CssClass="w330 h150" runat="server" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Directions
                                                  <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleAddressDirections" MaxLength="2000" TargetControlId="txtDirections" />
                                        </label>
                                    </div>
                                    <div class="fieldgroup">
                                        <eShip:CustomTextBox ID="txtDirections" TextMode="MultiLine" CssClass="w330 h150" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabContacts" HeaderText="Contacts">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlContacts" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlContacts">
                                <div class="rowgroup mb10">
                                    <div class="row">
                                        <div class="fieldgroup right">
                                            <asp:Button runat="server" ID="btnAddMailingContact" Text="Add Contact" CssClass="mr10" CausesValidation="False" OnClick="OnAddContactClicked" />
                                            <asp:Button runat="server" ID="btnClearContacts" Text="Clear Contacts" OnClick="OnClearContactsClicked" CausesValidation="False" />
                                        </div>
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheAddressContactTable" TableId="addressContactTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table class="stripe" id="addressContactTable">
                                    <tr class="header">
                                        <th style="width: 22%;">Name </th>
                                        <th style="width: 13%;">Phone </th>
                                        <th style="width: 13%;">Mobile </th>
                                        <th style="width: 13%;">Fax </th>
                                        <th style="width: 25%;">Email </th>
                                        <th style="width: 6%;">Primary </th>
                                        <th style="width: 8%;" class="text-center">Action </th>
                                    </tr>
                                    <asp:ListView ID="lstContacts" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td class="top">
                                                    <eShip:CustomHiddenField ID="hidContactId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <eShip:CustomHiddenField runat="server" ID="hidContactIndex" Value='<%# Container.DataItemIndex %>' />
                                                    <eShip:CustomTextBox ID="txtName" runat="server" Text='<%# Eval("Name") %>' CssClass="w240" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtPhone" runat="server" Text='<%# Eval("Phone") %>' CssClass="w110" MaxLength="50" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtMobile" runat="server" Text='<%# Eval("Mobile") %>' CssClass="w110" MaxLength="50" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtFax" runat="server" Text='<%# Eval("Fax") %>' CssClass="w110" MaxLength="50" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtEmail" runat="server" Text='<%# Eval("Email") %>' CssClass="w260" MaxLength="100" />
                                                </td>
                                                <td class="text-center">
                                                    <eShip:AltUniformCheckBox runat="server" ID="chkPrimary" Checked='<%# Eval("Primary") %>' />
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnDelete" ToolTip="Delete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        CausesValidation="false" OnClick="OnDeleteContactClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                            <tr class="hidden">
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td colspan="9" class="pt0">
                                                    <div class="row">
                                                        <div class="fieldgroup vlinedarkright">
                                                            <label class="wlabel blue">Contact Type</label>
                                                            <eShip:CachedObjectDropDownList Type="ContactTypes" ID="ddlContactType" runat="server" CssClass="w200" 
                                                                EnableChooseOne="True" DefaultValue="0" SelectedValue='<%# Eval("ContactTypeId") %>' />
                                                        </div>
                                                        <div class="fieldgroup pl20">
                                                            <label class="blue wlabel">Comment</label>
                                                            <eShip:CustomTextBox runat="server" ID="txtComment" Text='<%# Eval("Comment") %>' MaxLength="200" CssClass="w740" />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabServices" HeaderText="Services">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlServices" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlServices">
                                <div class="rowgroup mb10">
                                    <div class="row">
                                        <div class="fieldgroup right">
                                            <asp:Button runat="server" ID="btnAddService" Text="Add Service" CssClass="mr10"
                                                CausesValidation="False" OnClick="OnAddServiceClicked" />
                                            <asp:Button runat="server" ID="btnClearServices" Text="Clear Services" OnClick="OnClearServicesClicked" CausesValidation="False" />
                                        </div>
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheAddressServicesTable" TableId="addressServicesTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table class="stripe" id="addressServicesTable">
                                    <tr>
                                        <th style="width: 7%;">Code </th>
                                        <th style="width: 40%;">Description </th>
                                        <th style="width: 10%;">Category </th>
                                        <th style="width: 41%;">Charge Code </th>
                                        <th style="width: 2%;">Action </th>
                                    </tr>
                                    <asp:ListView ID="lstServices" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidServiceId" Value='<%#Eval("Id") %>' runat="server" />
                                                    <eShip:CustomHiddenField runat="server" ID="hidServiceIndex" Value='<%# Container.DataItemIndex %>' />
                                                    <asp:Literal ID="litServiceCode" Text='<%# Eval("Code") %>' runat="server" />
                                                </td>
                                                <td>
                                                    <asp:Literal ID="litServiceDescription" Text='<%# Eval("Description") %>' runat="server" />
                                                </td>
                                                <td>
                                                    <asp:Literal ID="litServiceCategory" Text='<%# Eval("Category") %>' runat="server" />
                                                </td>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidServiceChargeCodeId" Value='<%# Eval("ChargeCodeId") %>'
                                                        runat="server" />
                                                    <asp:Literal ID="litServiceChargeCode" Text='<%# Eval("ChargeCode") %>' runat="server" />
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="ibtnServiceDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        OnClick="OnDeleteServiceClicked" CausesValidation="false" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnAddService"/>
                        </Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabAuditLog" HeaderText="Audit Log">
                <ContentTemplate>
                    <eShip:AuditLogControl ID="auditLogs" runat="server" />
                </ContentTemplate>
            </ajax:TabPanel>
        </ajax:TabContainer>
        <asp:Panel runat="server" ID="pnlExportCustomerSelection" Visible="false" CssClass="popup popupControlOverW500">
            <div class="popheader mb10">
                <h4>Export Customer Selection</h4>
                <asp:ImageButton ID="btnExportClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                    CausesValidation="false" OnClick="OnExportCloseClick" runat="server" />
            </div>
            <table class="poptable">
                <tr>
                    <td class="text-right">
                        <label class="upper">Customer:</label></td>
                    <td class="text-left">
                        <asp:DropDownList ID="ddlCustomerSelection" DataTextField="Text" DataValueField="Value"
                            runat="server" CssClass="w230" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="btnExportCont" Text="Continue" OnClick="OnContinueExportClick" runat="server" />
                        <asp:Button ID="btnExportCancel" Text="Cancel" OnClick="OnExportCancelClick" runat="server" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <script type="text/javascript">
            function ClearHidFlag() {
                jsHelper.SetEmpty('<%= hidFlag.ClientID %>');
            }
        </script>

        <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
            Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" />
        <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
        <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
            OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
        <eShip:CustomHiddenField runat="server" ID="hidFlag" />
        <eShip:CustomHiddenField runat="server" ID="hidContactEditingIndex" />
    </div>
</asp:Content>
