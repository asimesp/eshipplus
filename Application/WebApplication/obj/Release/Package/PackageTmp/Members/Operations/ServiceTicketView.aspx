﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="ServiceTicketView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.ServiceTicketView" EnableEventValidation="false" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.Registry" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Operations" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        ShowEdit="true" ShowDelete="true" ShowSave="true" ShowFind="true" ShowMore="true"
        OnFind="OnToolbarFindClicked" OnSave="OnToolbarSaveClicked" OnDelete="OnToolbarDeleteClicked"
        OnNew="OnToolbarNewClicked" OnEdit="OnToolbarEditClicked" OnCommand="OnToolbarCommand" OnUnlock="OnToolbarUnlockClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Service Tickets
                <eShip:RecordIdentityDisplayControl runat="server" ID="ridcRecordIdentity" TargetControlId="txtServiceTicketNumber" />
            </h3>
            <div class="shipmentAlertSection">
                <asp:Panel ID="pnlPrimaryVendorInsuranceAlert" runat="server" CssClass="flag-shipment" Visible="False">
                    <asp:Literal runat="server" ID="litPrimaryVendorInsuranceAlert" />
                </asp:Panel>
                <asp:Panel ID="pnlCreditAlert" runat="server" CssClass="flag-shipment" Visible="false">
                    Credit Alert
                </asp:Panel>
            </div>
            <div class="lockMessage">
                <asp:Literal ID="litMessage" runat="server" />
            </div>
        </div>

        <hr class="dark mb5" />

        <div class="errorMsgLit">
            <asp:Literal runat="server" ID="litErrorMessages" />
        </div>

        <eShip:ServiceTicketFinderControl runat="server" ID="serviceTicketFinder" Visible="false"
            OnItemSelected="OnServiceTicketFinderItemSelected" OnSelectionCancel="OnServiceTicketFinderItemCancelled"
            OpenForEditEnabled="true" EnableMultiSelection="False" />
        <eShip:CustomerFinderControl ID="customerFinder" runat="server" EnableMultiSelection="false"
            OnlyActiveResults="True" Visible="False" OpenForEditEnabled="false" OnSelectionCancel="OnCustomerFinderSelectionCancelled"
            OnItemSelected="OnCustomerFinderItemSelected" />
        <eShip:PrefixFinderControl ID="prefixFinder" runat="server" EnableMultiSelection="false"
            Visible="False" OnSelectionCancel="OnPrefixFinderSelectionCancelled" OnItemSelected="OnPrefixFinderItemSelected" />
        <eShip:VendorFinderControl EnableMultiSelection="true" ID="vendorFinder" OnItemSelected="OnVendorFinderItemSelected"
            OnlyActiveResults="true" OnMultiItemSelected="OnVendorFinderMultiItemSelected"
            OnSelectionCancel="OnVendorFinderItemCancelled" OpenForEditEnabled="false" runat="server"
            Visible="false" />
        <eShip:AccountBucketFinderControl EnableMultiSelection="false" ID="accountBucketFinder"
            OnItemSelected="OnAccountBucketFinderItemSelected" OnSelectionCancel="OnAccountBucketFinderSelectionCancelled"
            runat="server" ShowActiveRecordsOnly="True" Visible="False" />
		<eShip:AjaxTabHeaderTextUpdater runat="server" ID="athtuTabUpdater" />
        <eShip:JobFinderControl runat="server" ID="jobFinder" Visible="false" OnItemSelected="OnJobFinderItemSelected"
            OnSelectionCancel="OnJobFinderItemCancelled" OpenForEditEnabled="False" />

        <eShip:CustomHiddenField ID="hidServiceTicketId" runat="server" />

        <ajax:TabContainer ID="tabServiceTicket" runat="server" CssClass="ajaxCustom">
            <ajax:TabPanel runat="server" ID="tabDetails" HeaderText="Details">
                <ContentTemplate>
                    <asp:Panel ID="pnlDetails" runat="server">
                        <script type="text/javascript">
                            $(document).ready(function () {
                                $("#<%= btnEditServices.ClientID %>").click(function (e) {
                                    e.preventDefault();
                                    ShowPanel($("#<%= pnlServiceList.ClientID %>"));
                                });

                                $("#<%= btnEditEquipment.ClientID %>").click(function (e) {
                                    e.preventDefault();
                                    ShowPanel($("#<%= pnlEquipmentTypeList.ClientID %>"));
                                });

                                $("#<%= btnEquipmentTypesDone.ClientID %>, #<%= btnServicesDone.ClientID %>").click(function (e) {
                                    e.preventDefault();
                                    HidePanels($("#<%= pnlEquipmentTypeList.ClientID %>, #<%= pnlServiceList.ClientID %>"));
                                });
                            });

                            function ShowPanel($panel) {
                                $($panel).fadeIn('fast');
                                $("#<%= pnlDimScreenJS.ClientID %>").fadeIn('fast');
                            }

                            function HidePanels($panel) {
                                $($panel).fadeOut('fast');
                                $("#<%= pnlDimScreenJS.ClientID %>").fadeOut('fast');
                            }

                            function getSelected(list, lblResults, lblValueText) {
                                var checkboxes = $('[id$=' + list + '] input[id$="chkSelected"]');
                                var checkedText = jsHelper.EmptyString;

                                for (var i = 0; i < checkboxes.length; i++) {
                                    if (jsHelper.IsChecked(checkboxes[i].id)) {
                                        checkedText += ($(jsHelper.AddHashTag(checkboxes[i].id)).parent().parent().parent().parent().find('[id$="' + lblValueText + '"]').text() + ",");
                                    }
                                }

                                $('[id$=hid' + lblResults + ']').val(checkedText);
                                $('[id$=lbl' + lblResults + ']').text(checkedText);
                            }
                        </script>

                        <div class="rowgroup">
                            <div class="row">
                                <div class="fieldgroup vlinedarkright">
                                    <label class="wlabel blue">Service Ticket Number</label>
                                    <eShip:CustomTextBox ID="txtServiceTicketNumber" runat="server" ReadOnly="True" CssClass="disabled w200" />
                                </div>
                                <div class="fieldgroup pl20 vlinedarkright">
                                    <label class="wlabel blue">
                                        Customer
                                        <%= hidCustomerId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.Customer) 
                                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Customer Record'><img src={3} width='20' class='middle'/></a>", 
                                                                        ResolveUrl(CustomerView.PageAddress),
                                                                        WebApplicationConstants.TransferNumber, 
                                                                        hidCustomerId.Value.UrlTextEncrypt(),
                                                                        ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                : string.Empty %>
                                    </label>
                                    <eShip:CustomHiddenField runat="server" ID="hidCustomerId" />
                                    <eShip:CustomTextBox runat="server" ID="txtCustomerNumber" CssClass="w80" OnTextChanged="OnCustomerNumberEntered" AutoPostBack="True" />
                                    <asp:ImageButton ID="imgCustomerSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png" CausesValidation="False" OnClick="OnCustomerSearchClicked" />
                                    <eShip:CustomTextBox runat="server" ID="txtCustomerName" CssClass="w180 disabled" ReadOnly="True" />
                                    <ajax:AutoCompleteExtender runat="server" ID="aceCustomer" TargetControlID="txtCustomerNumber" ServiceMethod="GetCustomerList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                        EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                                </div>
                                <div class="fieldgroup pl20 vlinedarkright">
                                    <label class="wlabel blue">Status</label>
                                    <eShip:CustomTextBox runat="server" ID="txtStatus" ReadOnly="true" CssClass="w150 disabled" />
									<eShip:CustomHiddenField runat="server" ID="hidStatus" />
                                </div>
                                <div class="fieldgroup pl20">
                                    <label class="wlabel blue">User</label>
                                    <eShip:CustomTextBox runat="server" ID="txtUser" ReadOnly="True" CssClass="w200 disabled" />
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div class="rowgroup">
                            <div class="col_1_2 bbox vlinedarkright">
                                <h5>General Information</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Date Created</label>
                                        <eShip:CustomTextBox runat="server" ID="txtDateCreated" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Ticket Date</label>
                                        <eShip:CustomTextBox runat="server" ID="txtTicketDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <eShip:CustomHiddenField runat="server" ID="hidPrimaryVendorId" />
                                        <eShip:CustomHiddenField runat="server" ID="hidPrimaryServiceTicketVendorId" />
                                        <label class="wlabel">
                                            Primary Vendor
                                              <%= hidPrimaryVendorId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.Vendor) 
                                                    ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Vendor Record'><img src={3} width='20' class='middle'/></a>", 
                                                                            ResolveUrl(VendorView.PageAddress),
                                                                            WebApplicationConstants.TransferNumber, 
                                                                            hidPrimaryVendorId.Value.UrlTextEncrypt(),
                                                                            ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                    : string.Empty %>
                                        </label>
                                        <asp:Panel runat="server" DefaultButton="imgVendorSearch">
                                            <eShip:CustomTextBox runat="server" ID="txtVendorNumber" CssClass="w110" OnTextChanged="OnVendorNumberEntered" AutoPostBack="True" />
                                            <asp:ImageButton ID="imgVendorSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png" CausesValidation="False" OnClick="OnVendorSearchClicked" />
                                            <eShip:CustomTextBox runat="server" ID="txtVendorName" CssClass="w330 disabled" ReadOnly="True" />
                                            <ajax:AutoCompleteExtender runat="server" ID="aceVendor" TargetControlID="txtVendorNumber" ServiceMethod="GetVendorList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                                UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" DelimiterCharacters="" Enabled="True" />
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <eShip:CustomHiddenField runat="server" ID="hidAccountBucketId" />
                                        <label class="wlabel">Account Bucket</label>
                                        <eShip:CustomTextBox runat="server" ID="txtAccountBucketCode" CssClass="w110" OnTextChanged="OnAccountBucketCodeEntered" AutoPostBack="True" />
                                        <asp:ImageButton ID="imgAccountBucketSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png" CausesValidation="False" OnClick="OnAccountBucketSearchClicked" />
                                        <eShip:CustomTextBox runat="server" ID="txtAccountBucketDescription" CssClass="w180 disabled" ReadOnly="True" />
                                        <ajax:AutoCompleteExtender runat="server" ID="aceAccountBucketCode" TargetControlID="txtAccountBucketCode" ServiceMethod="GetAccountBucketList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                            UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" DelimiterCharacters="" Enabled="True" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Account Bucket Unit</label>
                                        <eShip:CachedObjectDropDownList runat="server" ID="ddlAccountBucketUnit" CssClass="w140" Type="AccountBucketUnits" EnableChooseOne="True" DefaultValue="0" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <eShip:CustomHiddenField runat="server" ID="hidPrefixId" />
                                        <label class="wlabel">Prefix</label>
                                        <eShip:CustomTextBox runat="server" ID="txtPrefixCode" CssClass="w110" OnTextChanged="OnPrefixCodeEntered" AutoPostBack="True" />
                                        <asp:ImageButton ID="imgPrefixSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png" CausesValidation="False" OnClick="OnPrefixSearchClicked" />
                                        <eShip:CustomTextBox runat="server" ID="txtPrefixDescription" CssClass="w180 disabled" ReadOnly="True" />
                                        <ajax:AutoCompleteExtender runat="server" ID="acePrefixCode" TargetControlID="txtPrefixCode" ServiceMethod="GetPrefixList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                            UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" DelimiterCharacters="" Enabled="True" />
                                    </div>
                                    <div class="fieldgroup_s">
                                        <asp:CheckBox runat="server" ID="chkHidePrefix" CssClass="jQueryUniform" />
                                        <label>Hide Prefix</label>
                                    </div>
                                </div>
                                <h5 class="pt20">Customer Billing Address</h5>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Customer Location</label>
                                        <script type="text/javascript">
                                            $(document).ready(function () {
                                                $(jsHelper.AddHashTag('<%= ddlCustomerLocation.ClientID %>')).change(function () {
                                                    $(jsHelper.AddHashTag(this.value)).show().siblings().hide();
                                                });
                                                $(jsHelper.AddHashTag('<%= ddlCustomerLocation.ClientID %>')).change();
                                            })
                                        </script>
                                        <asp:DropDownList runat="server" ID="ddlCustomerLocation" DataTextField="Text" DataValueField="Value" CssClass="w420" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Address</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <div>
                                            <eShip:CustomTextBox ID="CustomTextBox1" runat="server" TextMode="MultiLine" CssClass="w420 disabled" ReadOnly="True" />
                                        </div>
                                        <asp:Repeater runat="server" ID="rptCustomerLocations">
                                            <ItemTemplate>
                                                <div id='<%# Eval("Id") %>' style="display: none;">
                                                    <eShip:CustomTextBox ID="CustomTextBox2" runat="server" TextMode="MultiLine" CssClass="w420 disabled" ReadOnly="True"
                                                        Text='<%# string.Format("{0} {1} {2}{3}{4} {5} {6}{3}{7}", Eval("BillTo"), Eval("Street1"), Eval("Street2"), Environment.NewLine, 
                                                                Eval("City"), Eval("State"), Eval("PostalCode"), Eval("CountryName")) %>' />
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                                <h5>Job Link</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Job Number
                                            <%= txtJobNumber.Text.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.Job) 
                                                            ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Job Record'><img src={3} width='20' class='middle'/></a>", 
                                                                                    ResolveUrl(JobView.PageAddress),
                                                                                    WebApplicationConstants.TransferNumber, 
                                                                                    txtJobNumber.Text,
                                                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                            : string.Empty %>
                                        </label>
                                        <eShip:CustomTextBox runat="server" ID="txtJobNumber" Type="NumbersOnly" CssClass="w200" MaxLength="50" OnTextChanged="OnJobNumberTextChanged"
                                            AutoPostBack="True"/>
                                        <asp:ImageButton ID="btnFindJob" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                                                        CausesValidation="false" OnClick="OnFindJobClicked" />
                                        <asp:ImageButton ID="btnClearJob" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                            CausesValidation="false" OnClick="OnClearJobClicked" Width="20px" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Job Step</label>
                                        <eShip:CustomTextBox runat="server" ID="txtJobStep" CssClass="w70" MaxLength="50" Type="NumbersOnly" />
                                    </div>
                                </div>
                            </div>
                            <div class="col_1_2 pl46 bbox">
                                <h5>External References</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">External Reference 1</label>
                                        <eShip:CustomTextBox runat="server" ID="txtExternalReference1" CssClass="w200"  MaxLength="50"/>
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">External Reference 2</label>
                                        <eShip:CustomTextBox runat="server" ID="txtExternalReference2" CssClass="w200" MaxLength="50" />
                                    </div>
                                </div>
                                <h5 class="pt20">Sales Representative Information</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Name</label>
                                        <eShip:CustomTextBox runat="server" ID="txtSalesRepresentative" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Commission (%)</label>
                                        <eShip:CustomHiddenField runat="server" ID="hidSalesRepresentativeCommissionPercent" />
                                        <eShip:CustomTextBox runat="server" ID="txtSalesRepresentativeCommissionPercent" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Minimum Commission ($)</label>
                                        <eShip:CustomHiddenField runat="server" ID="hidSalesRepresentativeMinCommValue" />
                                        <eShip:CustomTextBox runat="server" ID="txtSalesRepresentativeMinCommValue" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Maximum Commission ($)</label>
                                        <eShip:CustomHiddenField runat="server" ID="hidSalesRepresentativeMaxCommValue" />
                                        <eShip:CustomTextBox runat="server" ID="txtSalesRepresentativeMaxCommValue" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Additional Entity Name</label>
                                        <eShip:CustomTextBox runat="server" ID="txtSalesRepresentativeAddlEntityName" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Additional Entity Commission(%)</label>
                                        <eShip:CustomHiddenField runat="server" ID="hidSalesRepresentativeAddlEntityCommPercent" />
                                        <eShip:CustomTextBox runat="server" ID="txtSalesRepresentativeAddlEntityCommPercent" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                </div>
                                <div class="row pt20">
                                    <div class="col_1_2 bbox">
                                        <h5>Services
                                            <asp:Button ID="btnEditServices" Text="Edit Services" CssClass="right" runat="server" CausesValidation="False" />
                                        </h5>
                                        <div class="row" id="services">
                                            <eShip:CustomHiddenField ID="hidServices" runat="server" />
                                            <label>
                                                <asp:Label ID="lblServices" runat="server" /></label>
                                        </div>
                                    </div>
                                    <div class="col_1_2 bbox pl10">
                                        <h5>Equipment
                                            <asp:Button ID="btnEditEquipment" CssClass="right" Text="Edit Equipment" runat="server" CausesValidation="False" />
                                        </h5>
                                        <div class="row" id="equipment">
                                            <eShip:CustomHiddenField ID="hidEquipment" runat="server" />
                                            <label>
                                                <asp:Label ID="lblEquipment" runat="server" /></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlServiceList" runat="server" CssClass="popup popup-position-fixed top50" Style="display: none;">
                        <div class="popheader">
                            <h4>Services</h4>
                        </div>
                        <div class="row finderScroll">
                            <div class="fieldgroup pl20 pr20 pt20">
                                <asp:Repeater runat="server" ID="rptServices">
                                    <HeaderTemplate>
                                        <ul class="twocol_list sm_chk" id="ulServices">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <li>
                                            <eShip:CustomHiddenField runat="server" ID="hidServiceId" Value='<%# Eval("Value") %>' />
                                            <asp:CheckBox runat="server" ID="chkSelected" CssClass="jQueryUniform" />
                                            <label class="lhInherit">
                                                <asp:Label runat="server" ID="lblServiceCode" Text='<%# Eval("Text") %>' CssClass="fs85em" />
                                            </label>
                                        </li>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </ul>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                        <div class="row text-right">
                            <div class="fieldgroup pb10 pt10 right">
                                <asp:Button ID="btnServicesDone" runat="server" Text="Done" CausesValidation="False" OnClientClick="getSelected('ulServices', 'Services', 'lblServiceCode');" />
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlEquipmentTypeList" runat="server" CssClass="popup popupControlOverW750 popup-position-fixed top50" Style="display: none;">
                        <div class="popheader">
                            <h4>Equipment Types</h4>
                        </div>
                        <div class="row finderScroll">
                            <div class="fieldgroup pl20 pr20 pt20">
                                <asp:Repeater runat="server" ID="rptEquipmentTypes">
                                    <HeaderTemplate>
                                        <ul class="twocol_list sm_chk" id="ulEquipmentTypes">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <li>
                                            <asp:CheckBox runat="server" ID="chkSelected" CssClass="jQueryUniform" />
                                            <eShip:CustomHiddenField runat="server" ID="hidEquipmentTypeId" Value='<%# Eval("Value") %>' />
                                            <label class="lhInherit">
                                                <asp:Label runat="server" ID="lblEquipmentTypeCode" Text='<%# Eval("Text") %>' CssClass="fs85em" />
                                            </label>
                                        </li>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </ul>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                        <div class="row">
                            <div class="fieldgroup pb10 pt10 right">
                                <asp:Button ID="btnEquipmentTypesDone" runat="server" Text="Done" CausesValidation="False"
                                    OnClientClick="getSelected('ulEquipmentTypes', 'Equipment', 'lblEquipmentTypeCode');" />
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabItems" HeaderText="Items">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlItems" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlItems">
                                <div class="row mb10">
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddItem" Text="Add Item" CssClass="mr10" CausesValidation="false" OnClick="OnAddItemClicked" />
                                        <asp:Button runat="server" ID="btnClearAllItems" Text="Clear Items" CausesValidation="False" OnClick="OnClearAllItemsClicked" />
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheServiceTicketItemsTable" TableId="serviceTicketItemsTable" HeaderZIndex="2" IgnoreHeaderBackgroundFill="True" />
                                <table class="stripe" id="serviceTicketItemsTable">
                                    <tr>
                                        <th style="width: 40%;">Description
                                        </th>
                                        <th style="width: 55%;">Comment
                                        </th>
                                        <th style="width: 5%;">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstItems" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidItemIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <eShip:CustomHiddenField ID="hidItemId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <eShip:CustomTextBox ID="txtDescription" runat="server" Text='<%# Eval("Description") %>' CssClass="w360" MaxLength="50" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtComment" runat="server" Text='<%# Eval("Comment") %>' CssClass="w360" MaxLength="50" />
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        CausesValidation="false" OnClick="OnDeleteItemClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabCharges" HeaderText="Charges">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlCharges" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlCharges" DefaultButton="btnAddCharge">
                                <div class="row mb10">
                                    <div class="fieldgroup">
                                        <eShip:AltUniformCheckBox runat="server" ID="chkAuditedForInvoicing" />
                                        <label>Audited For Invoicing</label>
                                    </div>
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddCharge" Text="Add Charge" CssClass="mr10" CausesValidation="false" OnClick="OnAddChargeClicked" OnClientClick="ShowProcessingDivBlock();" />
                                        <asp:Button runat="server" ID="btnClearAllCharges" Text="Clear Charges" CssClass="mr10" CausesValidation="false" OnClick="OnClearAllChargesClicked" OnClientClick="ShowProcessingDivBlock();" />
									</div>
                                </div>
                                <div class="row mb10 mt10" id="chargeStatisticsDiv">
                                    <eShip:ChargeStatistics2 ID="chargeStatistics" runat="server" />
                                    <asp:ImageButton runat="server" ID="ibtnRefreshChargeStatistics" CausesValidation="False" AlternateText="Refresh Charge Statistics" ImageUrl="~/images/icons2/cog.png"
                                        ToolTip="Refresh Charge Statistics" OnClick="OnRefreshChargeStatisticsClicked" OnClientClick="ResetChargeStatisticsColor(); ShowProcessingDivBlock();" />
                                </div>

                                <script type="text/javascript">
                                    $(window).load(function () {
                                        $(jsHelper.AddHashTag('serviceTicketChargesTable [id$="txtAmountDue"]')).each(function () {
                                            UpdateChargeInformation(this);
                                        });
                                        $('#chargeStatisticsDiv').removeClass('red');

                                        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                                            $(jsHelper.AddHashTag('serviceTicketChargesTable [id$="txtAmountDue"]')).each(function () {
                                                UpdateChargeInformation(this);
                                            });
                                            $('#chargeStatisticsDiv').removeClass('red');
                                        });
                                    });

                                    function ResetChargeStatisticsColor() {
                                        $('#chargeStatisticsDiv').removeClass('red');
                                    }

                                    function UpdateChargeInformation(control) {
                                        var unitSell = Number($(control).parent().parent().find('[id$="txtUnitSell"]').val());
                                        var unitDiscount = Number($(control).parent().parent().find('[id$="txtUnitDiscount"]').val());
                                        var qty = Number($(control).parent().parent().find('[id$="txtQuantity"]').val());
                                        if (qty < 1) qty = 1;
                                        $(control).parent().parent().find('[id$="txtAmountDue"]').val(((unitSell - unitDiscount) * qty).toFixed(4));
                                        $('#chargeStatisticsDiv').addClass('red');
                                    }
                                    
                                    function SetCheckAllServiceTicketCharges(control) {
                                    	$("#serviceTicketChargesTable input:checkbox[id*='chkSelected']").each(function () {
                                    		this.checked = control.checked && this.disabled == false;
                                    		SetAltUniformCheckBoxClickedStatus(this);
                                    	});

                                    	// necessary to keep frozen header and header checkbox states the same
                                    	$('[id*=' + control.id + ']').each(function () {
                                    		this.checked = control.checked;
                                    		SetAltUniformCheckBoxClickedStatus(this);
                                    	});
                                    }
                                </script>

                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheServiceTicketChargesTable" TableId="serviceTicketChargesTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table class="stripe" id="serviceTicketChargesTable">
                                    <tr>
	                                    <th style="width: 3%;">
                                        <eShip:AltUniformCheckBox ID="chkSelectAllCharges" runat="server" OnClientSideClicked="javascript:SetCheckAllServiceTicketCharges(this);" />
                                         </th>
                                        <th style="width: 27%;">Charge Code
                                        </th>
                                        <th style="width: 5%;">
                                            <abbr title="Quantity">Qty</abbr>
                                        </th>
                                        <th style="width: 15%;" class="text-right">Unit Buy
										    <abbr title="Dollars">($)</abbr>
                                        </th>
                                        <th style="width: 15%;" class="text-right">Unit Sell
										    <abbr title="Dollars">($)</abbr>
                                        </th>
                                        <th style="width: 15%;" class="text-right">Unit Discount
										    <abbr title="Dollars">($)</abbr>
                                        </th>
                                        <th style="width: 15%;" class="text-right">Amount Due
										    <abbr title="Dollars">($)</abbr>
                                        </th>
                                        <th style="width: 5%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstCharges" runat="server" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnAddVendorsToChargesList">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
	                                            <td class="top">
		                                            <eShip:AltUniformCheckBox runat="server" ID="chkSelected" OnClientSideClicked=" SetCheckAllCheckBoxUnChecked(this, 'serviceTicketChargesTable', 'chkSelectAllRecords'); " />
	                                            </td>
                                                <td>
                                                    <eShip:CachedObjectDropDownList runat="server" ID="ddlChargeCode" CssClass="w260" Type="ChargeCodes"
                                                        EnableChooseOne="True" DefaultValue="0" SelectedValue='<%# Eval("ChargeCodeId") %>' />
                                                </td>
                                                <td class="text-center">
                                                    <eShip:CustomHiddenField ID="hidChargeIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <eShip:CustomHiddenField ID="hidChargeId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <eShip:CustomTextBox ID="txtQuantity" runat="server" Text='<%# Eval("Quantity") %>' CssClass="w40" onblur="UpdateChargeInformation(this);" Type="NumbersOnly" />
                                                </td>
                                                <td class="text-right">
                                                    <eShip:CustomTextBox ID="txtUnitBuy" runat="server" Text='<%# Eval("UnitBuy", "{0:f4}") %>' CssClass="w75" onblur="UpdateChargeInformation(this);" Type="FloatingPointNumbers" />
                                                </td>
                                                <td class="text-right">
                                                    <eShip:CustomTextBox ID="txtUnitSell" runat="server" Text='<%# Eval("UnitSell", "{0:f4}") %>' CssClass="w75" onblur="UpdateChargeInformation(this);" Type="FloatingPointNumbers" />
                                                </td>
                                                <td class="text-right">
                                                    <eShip:CustomTextBox ID="txtUnitDiscount" runat="server" Text='<%# Eval("UnitDiscount", "{0:f4}") %>' CssClass="w75" onblur="UpdateChargeInformation(this);" Type="FloatingPointNumbers" />
                                                </td>
                                                <td class="text-right">
                                                    <eShip:CustomTextBox runat="server" ID="txtAmountDue" CssClass="w80 disabled" ReadOnly="True" />
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png" OnClientClick="ShowProcessingDivBlock();"
                                                        CausesValidation="false" OnClick="OnDeleteChargeClicked" Enabled='<%# Access.Modify %>' Visible='<%# (Eval("VendorBillId").ToLong() == default(long)) %>' />
                                                    <eShip:CustomHiddenField ID="hidVendorBillId" runat="server" Value='<%# Eval("VendorBillId") %>' />
                                                </td>
                                            </tr>
                                            <tr class="hidden">
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td colspan="13" class="pt0 mt0 bottom_shadow">
                                                    <div class="row mb10">
                                                        <div class="col_1_3">
                                                            <div class="fieldgroup middle">
                                                                <label class="wlabel blue">Vendor</label>
                                                                <asp:DropDownList runat="server" ID="ddlVendors" OnSelectedIndexChanged="OnChangeVendorInCharge"
                                                                    AutoPostBack="True" DataTextField="Text" DataValueField="Value" AppendDataBoundItems="true" />
                                                                <eShip:CustomHiddenField ID="hidSelectedVendorId" runat="server" Value='<%# Eval("VendorId") %>' />
                                                            </div>
                                                        </div>
                                                        <div class="col_1_3">
                                                            <div class="fieldgroup middle">
                                                                <label class="wlabel blue">Vendor Bill Number
                                                                    <%# ActiveUser.HasAccessTo(ViewCode.VendorBill) && Eval("VendorBillId").ToLong() != default(long)
                                                                        ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Vendor Bill Record'><img src={3} width='16' class='middle'/></a>", 
                                                                            ResolveUrl(VendorBillView.PageAddress),
                                                                            WebApplicationConstants.TransferNumber, 
                                                                            Eval("VendorBillId").GetString().UrlTextEncrypt(),
                                                                            ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                                        : string.Empty %>
                                                                </label>
                                                                <eShip:CustomTextBox runat="server" ID="txtVendorBillNumber" Text='<%# Eval("VendorBillDocumentNumber") %>' CssClass="w340 disabled" ReadOnly="True" />
                                                            </div>
                                                        </div>
                                                        <div class="col_1_3">
                                                            <div class="fieldgroup top">
                                                                <label class="wlabel blue">
                                                                    Comment
                                                                    <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDescription" MaxLength="100" TargetControlId="txtComment" />
                                                                </label>
                                                                <eShip:CustomTextBox ID="txtComment" runat="server" Text='<%# Eval("Comment") %>' TextMode="MultiLine" MaxLength="100" CssClass="w315" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
					</asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabNotes" HeaderText="Notes">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlNotes">
                        <div class="row mb10">
                            <div class="fieldgroup right">
                                <asp:Button runat="server" ID="btnAddNote" Text="Add Note" CausesValidation="false" OnClick="OnAddNoteClicked" />
                            </div>
                        </div>
                        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheServiceTicketNotesTable" TableId="serviceTicketNotesTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                        <table class="stripe" id="serviceTicketNotesTable">
                            <tr>
                                <th style="width: 20%;">Type
                                </th>
                                <th style="width: 55%;">Message
                                </th>
                                <th style="width: 10%;" class="text-center">Archived
                                </th>
                                <th style="width: 10%;" class="text-center">Classified
                                </th>
                                <th style="width: 5%;" class="text-center">Action
                                </th>
                            </tr>
                            <asp:ListView ID="lstNotes" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                <LayoutTemplate>
                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td class="top">
                                            <eShip:CustomHiddenField ID="hidNoteIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                            <eShip:CustomHiddenField ID="hidNoteId" runat="server" Value='<%# Eval("Id") %>' />
                                            <eShip:CustomHiddenField ID="hidType" runat="server" Value='<%# Eval("Type").ToInt() %>' />
                                            <asp:Literal ID="litType" runat="server" Text='<%# Eval("Type").FormattedString() %>' />
                                        </td>
                                        <td class="top">
                                            <asp:Literal ID="litMessage" runat="server" Text='<%# Eval("Message") %>' />
                                        </td>
                                        <td class="text-center top">
                                            <%# Eval("Archived").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                                            <eShip:CustomHiddenField runat="server" ID="hidArchived" Value='<%# Eval("Archived").ToBoolean().GetString() %>' />
                                        </td>
                                        <td class="text-center top">
                                            <%# Eval("Classified").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                                            <eShip:CustomHiddenField runat="server" ID="hidClassified" Value='<%# Eval("Classified").ToBoolean().GetString() %>' />
                                        </td>
                                        <td class="text-center top">
                                            <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png"
                                                CausesValidation="false" OnClick="OnEditNoteClicked" Enabled='<%# Access.Modify %>' />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:ListView>
                        </table>

                        <asp:Panel runat="server" ID="pnlEditNote" Visible="false">
                            <div class="popup popupControlOverW500">
                                <div class="popheader">
                                    <h4>Add/ Modify Note</h4>
                                    <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                                        CausesValidation="false" OnClick="OnCloseEditNoteClicked" runat="server" />
                                </div>
                                <div class="row">
                                    <table class="poptable">
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Type</label>
                                            </td>
                                            <td>
                                                <asp:DropDownList runat="server" ID="ddlType" DataValueField="Value" DataTextField="Text" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="chkArchived" CssClass="jQueryUniform" />
                                                <label class="upper mr10">Archived</label>

                                                <asp:CheckBox runat="server" ID="chkClassified" CssClass="jQueryUniform" />
                                                <label class="upper">Classified</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right top">
                                                <label class="upper">Message</label>
                                                <br />
                                                <label class="lhInherit">
                                                    <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleMessage" TargetControlId="txtMessage" MaxLength="500" />
                                                </label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox runat="server" ID="txtMessage" TextMode="MultiLine" CssClass="w300 h150" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <asp:Button ID="btnEditNoteDone" Text="Done" OnClick="OnEditNoteDoneClicked" runat="server" CausesValidation="false" />
                                                <asp:Button ID="btnEditNoteCancel" Text="Cancel" OnClick="OnCloseEditNoteClicked" runat="server" CausesValidation="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </asp:Panel>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel ID="tabAdditionalDetails" HeaderText="Reseller Additions" runat="server">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlAdditionalDetails">
                        <div class="row mb10">
                            <div class="fieldgroup mr20">
                                <label class="upper blue">
                                    <asp:Literal runat="server" ID="litBillReseller" />
                                    <asp:Literal runat="server" ID="litResellerAdditionName" />
                                </label>
                            </div>
                            <div class="fieldgroup right">
                                <asp:Button runat="server" ID="btnRetrieveDetails" Text="Retrieve" CssClass="mr10" CausesValidation="false" OnClick="OnRetrieveResellerAdditionDetailsClicked" />
                                <asp:Button runat="server" ID="btnClearDetails" Text="Clear" CausesValidation="false" OnClick="OnClearResellerAdditionDetailsClicked" />
                            </div>
                        </div>
                        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheServiceTicketResellerAdditionsTable" TableId="serviceTicketResellerAdditionsTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                        <table class="stripe" id="serviceTicketResellerAdditionsTable">
                            <tr class="header">
                                <th style="width: 20%;" class="text-center">Category</th>
                                <th style="width: 15%;" class="text-center">Type
                                </th>
                                <th style="width: 20%;" class="text-right">Value ($)
                                </th>
                                <th style="width: 24%;" class="text-right">Percent (%)
                                </th>
                                <th style="width: 20%;" class="text-center">Use Lower
                                </th>
                            </tr>
                            <asp:Repeater runat="server" ID="rptResellerAdditions">
                                <ItemTemplate>
                                    <tr>
                                        <td class="text-center">
                                            <%# Eval("Header") %>
                                        </td>
                                        <td class="text-center">
                                            <%# Eval("Type") %>
                                        </td>
                                        <td class="text-right">
                                            <%# Eval("Value") %>
                                        </td>
                                        <td class="text-right">
                                            <%# Eval("Percent") %>
                                        </td>
                                        <td class="text-center">
                                            <%# Eval("UseLower").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel ID="tabVendors" HeaderText="Vendors" runat="server">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlVendors" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel ID="pnlVendors" runat="server">
                                <div class="row mb10">
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddVendorFromFinder" Text="Add Vendor" CssClass="mr10" CausesValidation="false" OnClick="OnAddVendorClicked" />
                                        <asp:Button runat="server" ID="btnClearAllVendors" Text="Clear Vendors" CausesValidation="false" OnClick="OnClearAllVendorsClicked" />
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheServiceTicketVendorsTable" TableId="serviceTicketVendorsTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table class="stripe" id="serviceTicketVendorsTable">
                                    <tr class="header">
                                        <th style="width: 4%;" class="text-center"></th>
                                        <th style="width: 16%;">Vendor Number
                                        </th>
                                        <th style="width: 70%;">Name
                                        </th>
                                        <th style="width: 10%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstVendors" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td class="text-center">
                                                    <eShip:CustomHiddenField runat="server" ID="hidInsuranceAlert" Value='<%# Eval("InsuranceAlert") %>' />
                                                    <eShip:CustomHiddenField runat="server" ID="hidInsuranceTooltip" Value='<%# Eval("InsuranceTooltip") %>' />
                                                    <asp:Image CssClass="shipmentAlert" ID="imgInsuranceAlert" ImageAlign="AbsMiddle"
                                                        ImageUrl="~/images/icons/Insurance.png" runat="server" Width="16px" Visible='<%# Eval("InsuranceAlert").ToBoolean() %>'
                                                        ToolTip='<%# Eval("InsuranceTooltip") %>' />
                                                </td>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidVendorIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <eShip:CustomHiddenField ID="hidServiceTicketVendorId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <eShip:CustomHiddenField ID="hidVendorId" runat="server" Value='<%# Eval("VendorId") %>' />

                                                    <asp:Label runat="server" ID="lblVendorNumber" Text='<%# Eval("VendorNumber") %>' />
                                                    <%# ActiveUser.HasAccessTo(ViewCode.Vendor) && GetDataItem().HasGettableProperty("VendorId")
                                                            ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Vendor Record'><img src={3} width='16' class='middle'/></a>", 
                                                                ResolveUrl(VendorView.PageAddress),
                                                                WebApplicationConstants.TransferNumber,
                                                                Eval("VendorId").GetString().UrlTextEncrypt(),
                                                                ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                            : string.Empty %>
                                                </td>
                                                <td>
                                                    <asp:Literal ID="litName" runat="server" Text='<%# Eval("Name") %>' />
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        CausesValidation="false" OnClick="OnDeleteVendorClicked" Enabled='<%# Access.Modify %>'
                                                        Visible='<%# !(bool)Eval("IsRelationBetweenVendorAndVendorBill") %>' />
                                                    <eShip:CustomHiddenField ID="hidIsRelationBetweenVendorAndVendorBill" runat="server" Value='<%# Eval("IsRelationBetweenVendorAndVendorBill") %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnAddVendorFromFinder" />
                        </Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel ID="tabDocuments" runat="server" HeaderText="Documents">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlDocuments" DefaultButton="btnAddDocument">

                        <div class="row mb10">
                            <div class="fieldgroup right">
                                <asp:Button runat="server" ID="btnAddDocument" Text="Add Document" CausesValidation="false" OnClick="OnAddDocumentClicked" />
                                <asp:Button runat="server" ID="btnClearDocuments" Text="Clear Documents" CausesValidation="false" OnClick="OnClearDocumentsClicked" />
                            </div>
                        </div>

                        <script type="text/javascript">
                            $(document).ready(function () {
                                $("a[id$='lnkServiceTicketDocumentLocationPath']").each(function () {
                                    jsHelper.SetDoPostBackJsLink($(this).prop('id'));
                                });
                            });
                        </script>
                        <table class="stripe">
                            <tr>
                                <th style="width: 5%;" class="text-center">Internal
                                </th>
                                <th style="width: 17%;">Name
                                </th>
                                <th style="width: 25%;">Description
                                </th>
                                <th style="width: 20%;">Document Tag
                                </th>
                                <th style="width: 25%;">File
                                </th>
                                <th style="width: 8%;" class="text-center">Action
                                </th>
                            </tr>
                            <asp:ListView ID="lstDocuments" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                <LayoutTemplate>
                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td class="text-center">
                                            <eShip:CustomHiddenField runat="server" ID="hidIsInternal" Value='<%# Eval("IsInternal") %>' />
                                            <%# Eval("IsInternal").ToBoolean() ? WebApplicationConstants.HtmlCheck : string.Empty %>
                                        </td>
                                        <td>
                                            <eShip:CustomHiddenField ID="hidDocumentIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                            <eShip:CustomHiddenField ID="hidDocumentId" runat="server" Value='<%# Eval("Id") %>' />
                                            <asp:Literal ID="litDocumentName" runat="server" Text='<%# Eval("Name") %>' />
                                        </td>
                                        <td>
                                            <asp:Literal ID="litDocumentDescription" runat="server" Text='<%# Eval("Description") %>' />
                                        </td>
                                        <td>
                                            <eShip:CustomHiddenField ID="hidDocumentTagId" runat="server" Value='<%# Eval("DocumentTagId") %>' />
                                            <asp:Literal ID="litDocumentTag" runat="server" Text='<%# new DocumentTag(Eval("DocumentTagId").ToLong()).FormattedString() %>' />
                                        </td>
                                        <td>
                                            <eShip:CustomHiddenField ID="hidLocationPath" runat="server" Value='<%# Eval("LocationPath") %>' />
                                            <asp:LinkButton runat="server" ID="lnkServiceTicketDocumentLocationPath" Text='<%# GetLocationFileName(Eval("LocationPath")) %>'
                                                OnClick="OnLocationPathClicked" CausesValidation="false" CssClass="blue" />
                                        </td>
                                        <td class="text-center">
                                            <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png"
                                                CausesValidation="false" OnClick="OnEditDocumentClicked" Enabled='<%# Access.Modify %>' />
                                            <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                CausesValidation="false" OnClick="OnDeleteDocumentClicked" Enabled='<%# Access.Modify %>' />
                                        </td>
                                    </tr>
	                                <tr class="hidden">
		                                <td>&nbsp;</td>
	                                </tr>
	                                <tr>
		                                <td colspan="13" class="pt0 mt0 bottom_shadow pl20">
			                                <eShip:CustomHiddenField ID="hidVendorBillId" runat="server" Value='<%# Eval("VendorBill.Id") %>' />
			                                <div class="col_1_2 ">
				                                <label class="wlabel blue">Vendor</label>
				                                <eShip:CustomTextBox runat="server" ID="txtVendor" Text='<%# Eval("VendorBill.VendorLocation.Vendor.name")%>' CssClass="w240 disabled" ReadOnly="True" />
			                                </div>
			                                <div class="col_1_2 ">
				                                <label class="wlabel blue">Vendor Bill Number </label>
				                                <eShip:CustomTextBox runat="server" ID="txtVendorBillNumber" Text='<%#  Eval("VendorBill.DocumentNumber") %>' CssClass="w240 disabled" ReadOnly="True" />
			                                </div>
		                                </td>
	                                </tr>
                                </ItemTemplate>
                            </asp:ListView>
                        </table>

                        <asp:Panel runat="server" ID="pnlEditDocument" Visible="false" CssClass="popupControl popupControlOverW500">
                            <div class="popheader mb10">
                                <h4>Add/Modify Document</h4>
                                <asp:ImageButton ID="ibtnCloseEditDocument" ImageUrl="~/images/icons2/close.png"
                                    CssClass="close" CausesValidation="false" OnClick="OnCloseEditDocumentClicked" runat="server" />
                            </div>

                            <table class="poptable">
                                <tr>
                                    <td class="text-right">
                                        <label class="upper">Name:</label>
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtDocumentName" CssClass="w200" MaxLength="50" />
                                        <asp:CheckBox runat="server" ID="chkDocumentIsInternal" CssClass="jQueryUniform ml10" />
                                        <label>Is Internal</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right">
                                        <label class="upper">Document Tag:</label>
                                    </td>
                                    <td>
                                        <eShip:CachedObjectDropDownList runat="server" ID="ddlDocumentTag" CssClass="w300" Type="DocumentTag" EnableChooseOne="True" DefaultValue="0" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right top">
                                        <label class="upper">Description:</label>
                                        <br />
                                        <label class="lhInherit">
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDocumentDescription" MaxLength="500" TargetControlId="txtDocumentDescription" />
                                        </label>
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtDocumentDescription" CssClass="w300 h150" TextMode="MultiLine" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right">
                                        <label class="upper">Location Path:</label>
                                    </td>
                                    <td>
                                        <eShip:CustomHiddenField ID="hidLocationPath" runat="server" />
                                        <asp:FileUpload runat="server" ID="fupLocationPath" CssClass="jQueryUniform" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right">
                                        <label class="upper">Current File:</label>
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox ID="txtLocationPath" runat="server" ReadOnly="True" CssClass="w300 disabled" />
                                    </td>
	                                <eShip:CustomHiddenField ID="hidVendorBillId" runat="server" />
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <asp:Button ID="btnEditDocumentDone" Text="Done" OnClick="OnEditDocumentDoneClicked" runat="server" CausesValidation="false" />
                                        <asp:Button ID="btnCloseEditDocument" Text="Cancel" OnClick="OnCloseEditDocumentClicked" runat="server" CausesValidation="false" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel ID="tabAssets" runat="server" HeaderText="Assets">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlAssets" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlAssets">
                                <div class="row mb10">
                                    <div class="fieldgroup right">
                                        <asp:Button ID="btnAddAsset" runat="server" Text="Add Asset" CssClass="mr10" CausesValidation="false" OnClick="OnAddAssetClicked" />
                                        <asp:Button runat="server" ID="btnClearAllAssets" CausesValidation="False" Text="Clear Assets" OnClick="OnClearAllAssetsClicked" />
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheServiceTicketAssetsTable" TableId="serviceTicketAssetsTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table class="stripe" id="serviceTicketAssetsTable">
                                    <tr>
                                        <th style="width: 25%">Driver Asset
                                        </th>
                                        <th style="width: 25%">Tractor Asset
                                        </th>
                                        <th style="width: 25%">Trailer Asset
                                        </th>
                                        <th style="width: 11%">Miles Run
                                        </th>
                                        <th style="width: 6%;">Primary
                                        </th>
                                        <th style="width: 8%;">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstAssets" runat="server" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnServiceTicketAssetsItemDataBound">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidAssetIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <eShip:CustomHiddenField ID="hidAssetId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <asp:DropDownList runat="server" ID="ddlDriverAsset" DataTextField="Text" DataValueField="Value" CssClass="w200" />
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="ddlTractorAsset" DataTextField="Text" DataValueField="Value" CssClass="w200" />
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="ddlTrailerAsset" DataTextField="Text" DataValueField="Value" CssClass="w200" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtMilesRun" runat="server" Text='<%# Eval("MilesRun", "{0:f4}") %>' CssClass="w100" Type="FloatingPointNumbers" />
                                                </td>
                                                <td class="text-center">
                                                    <eShip:AltUniformCheckBox runat="server" ID="chkPrimary" Checked='<%# Eval("Primary") %>' />
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        CausesValidation="false" OnClick="OnDeleteAssetClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabAuditLog" HeaderText="Audit Log">
                <ContentTemplate>
                    <eShip:AuditLogControl runat="server" ID="auditLogs" />
                </ContentTemplate>
            </ajax:TabPanel>
        </ajax:TabContainer>
    </div>


    <script type="text/javascript">
        function ClearHidFlag() {
            jsHelper.SetEmpty('<%= hidFlag.ClientID %>');
        }
    </script>

    <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
        Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" />
    <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" Instructions="**Please refer to help menu for import file format." OnFileUploadCancelled="OnFileUploaderUploadCancelled"
        OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />

    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
    <asp:Panel ID="pnlDimScreenJS" CssClass="dimBackground" runat="server" Style="display: none;" />


    <asp:UpdatePanel runat="server" ID="upHiddens" UpdateMode="Always">
        <ContentTemplate>

            <eShip:CustomHiddenField runat="server" ID="hidFlag" />
            <eShip:CustomHiddenField runat="server" ID="hidCustomerOutstandingBalance" />
            <eShip:CustomHiddenField runat="server" ID="hidLoadedShipmentTotalDue" />
            <eShip:CustomHiddenField runat="server" ID="hidCustomerCredit" />
            <eShip:CustomHiddenField runat="server" ID="hidLoadedServiceTicketTotalDue" />
            <eShip:CustomHiddenField runat="server" ID="hidEditingIndex" />
            <eShip:CustomHiddenField runat="server" ID="hidSalesRepresentativeId" />
            <eShip:CustomHiddenField runat="server" ID="hidResellerAdditionId" />
            <eShip:CustomHiddenField runat="server" ID="hidBillReseller" />
            <eShip:CustomHiddenField runat="server" ID="hidFilesToDelete" />
			<eShip:CustomHiddenField runat="server" ID="hidEditStatus" />
            <eShip:CustomHiddenField runat="server" ID="hidJobId" />
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
