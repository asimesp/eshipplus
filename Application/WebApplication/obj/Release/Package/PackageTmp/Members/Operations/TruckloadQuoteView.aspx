﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="TruckloadQuoteView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.TruckloadQuoteView" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="false"
        ShowEdit="false" ShowDelete="false" ShowSave="false" ShowFind="false" ShowMore="false" />
</asp:Content>

<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                CssClass="pageHeaderIcon" />
            <h3>Request A Truckload Quote
            <small class="ml10">
                <asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>
        <span class="clearfix"></span>
        <hr class="dark mb5" />
        <div class="errorMsgLit">
            <asp:Literal runat="server" ID="litErrorMessages" />
        </div>
        <asp:Panel runat="server" ID="pnlQuoteRequestForm">
            <eShip:QuoteRequestFormControl ID="quoteRequestForm" runat="server" QuoteRequestServiceMode="Truckload" EnableSubmitQuoteButton="True"
                OnProcessingError="OnQuoteRequestFormError" OnCustomerSearch="OnQuoteRequestFormCustomerSearch" OnOpenLibraryItemFinder="OnQuoteRequestOpenLibraryItemFinder"
                OnOpenCustomerFinder="OnQuoteRequestFormOpenCustomerFinder" OnOpenPostalCodeFinder="OnQuoteRequestFormOpenPostalCodeFinder" OnSubmitQuote="OnSubmitQuoteRequestClicked" />
        </asp:Panel>
    </div>
    <eShip:CustomerFinderControl ID="customerFinder" runat="server" EnableMultiSelection="false"
        OnlyActiveResults="True" Visible="False" OpenForEditEnabled="false" OnSelectionCancel="OnCustomerFinderSelectionCancelled"
        OnItemSelected="OnCustomerFinderItemSelected" />
    <eShip:LibraryItemFinderControl runat="server" ID="libraryItemFinder" Visible="false" EnableMultiSelection="True"
        OnMultiItemSelected="OnLibraryItemFinderMultiItemSelected" OnSelectionCancel="OnLibraryItemFinderItemCancelled"
        OpenForEditEnabled="false" />
    <eShip:PostalCodeFinderControl runat="server" ID="postalCodeFinder" Visible="False" EnableMultiSelection="False"
        OnItemSelected="OnPostalCodeFinderItemSelected" OnSelectionCancel="OnPostalCodeFinderItemCancelled" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
    <eShip:CustomHiddenField runat="server" ID="hidTransferToConfirmation" />
</asp:Content>

