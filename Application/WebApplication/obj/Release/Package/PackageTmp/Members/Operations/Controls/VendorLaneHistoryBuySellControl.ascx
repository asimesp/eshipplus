﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VendorLaneHistoryBuySellControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.VendorLaneHistoryBuySellControl" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<eShip:VendorInformationControl runat="server" ID="vendorInfoControl" Visible="False" OnCancel="OnVendorInfoCancel" />
<asp:Panel runat="server" ID="pnlVendorLaneHistory" CssClass="popupControl" DefaultButton="btnSearch">
    <div class="popheader">
        <h4>Vendor Lane Buy/Sell</h4>
        <asp:ImageButton ImageUrl="~/images/icons2/close.png" CssClass="close" CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
    </div>

    
    
    <asp:Panel runat="server" ID="pnlNoRates" Visible="False">
        <div class="row">
            <div class="fieldgroup pl20 pt20 pr20 red">
                There is no rate history for the lane configuration. Please check your lane configuration and try again.
            </div>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlError" Visible="False">
        <div class="row">
            <div class="fieldgroup pl20 pr20 pt20">
                There was an error in the search process for lane history rates. Our IT department
                        has been contacted with the problem.<br />
                <br />
                Thank you.
            </div>
        </div>
    </asp:Panel>

    <div class="row">
        <div class="fieldgroup pl20 pt20 pb20">
            <label class="blue upper">Within Mileage Radius: </label>
            <eShip:CustomTextBox runat="server" ID="txtMileageRadius" CssClass="w100 mr20" />
            <asp:Button runat="server" Text="SEARCH" ID="btnSearch" OnClick="OnSearchClicked" CausesValidation="False"/>
        </div>
    </div>
    <script type="text/javascript">
        function ToggleChargeBreakdownVisibility(index, serviceMode) {
            $(jsHelper.AddHashTag('div' + serviceMode + 'Charges' + index)).slideToggle("fast");
        }
    </script>
    <eShip:TableFreezeHeaderExtender runat="server" ID="tfheVendorLaneHistoryLtlGroupTable" TableId="vendorLaneHistoryLtlGroupTable" ScrollerContainerId="vendorLaneHistoryLtlGroupScrollSection"
        ScrollOffsetControlId="pnlVendorLaneHistory" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2050" />
    <eShip:TableFreezeHeaderExtender runat="server" ID="tfheVendorLaneHistoryTlGroupTable" TableId="vendorLaneHistoryTlGroupTable" ScrollerContainerId="vendorLaneHistoryTlGroupScrollSection" 
        ScrollOffsetControlId="pnlVendorLaneHistory" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2050" />
    <eShip:TableFreezeHeaderExtender runat="server" ID="tfheVendorLaneHistoryAirGroupTable" TableId="vendorLaneHistoryAirGroupTable" ScrollerContainerId="vendorLaneHistoryAirGroupScrollSection"
        ScrollOffsetControlId="pnlVendorLaneHistory" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2050" />
    <eShip:TableFreezeHeaderExtender runat="server" ID="tfheVendorLaneHistoryRailGroupTable" TableId="vendorLaneHistoryRailGroupTable" ScrollerContainerId="vendorLaneHistoryRailGroupScrollSection"
        ScrollOffsetControlId="pnlVendorLaneHistory" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2050" />
    <eShip:TableFreezeHeaderExtender runat="server" ID="tfheVendorLaneHistorySmallPackGroupTable" TableId="vendorLaneHistorySmallPackGroupTable" ScrollerContainerId="vendorLaneHistorySmallPackGroupScrollSection"
        ScrollOffsetControlId="pnlVendorLaneHistory" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2050" />
    <ajax:TabContainer runat="server" ID="tabRates" CssClass="ajaxCustom">
        <ajax:TabPanel runat="server" ID="tabLTL">
            <ContentTemplate>
                <div class="finderScroll" id="vendorLaneHistoryLtlGroupScrollSection">
                    <table class="stripe" id="vendorLaneHistoryLtlGroupTable">
                        <tr>
                            <th style="width: 10%;">Vendor Number </th>
                            <th style="width: 34%;">Name </th>
                            <th style="width: 8%;" class="text-center">Contract Rate </th>
                            <th style="width: 8%;" class="text-right">
                                <abbr title="Contract Rate/Quote/Shipment record count">Record Count</abbr></th>
                            <th style="width: 10%;" class="text-right">Lowest Buy ($) </th>
                            <th style="width: 10%;" class="text-right">Highest Buy ($) </th>
                            <th style="width: 10%;" class="text-right">Lowest Sell ($) </th>
                            <th style="width: 10%;" class="text-right">Highest Sell ($) </th>
                        </tr>
                        <asp:ListView runat="server" ID="lstLTLGroup" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnLTLGroupItemDataBound">
                            <LayoutTemplate>
                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="top">
                                        <%# Eval("VendorNumber") %>
                                    </td>
                                    <td class="top">
                                        <eShip:CustomHiddenField runat="server" ID="hidVendorId" Value='<%# Eval("VendorId") %>' />
                                        <div class="row">
                                            <asp:LinkButton runat="server" ID="lbtnVendorName" Text='<%# Eval("VendorName") %>' OnClick="OnVendorNameClick" CssClass="blue"/>
                                        </div>
                                        <div class="row pt10">
                                            <asp:LinkButton runat="server" ID="lnkChargeBreakdown" Text="[See Charge Breakdown]" CssClass="blue link_nounderline" />
                                        </div>
                                    </td>
                                    <td class="text-center top">
                                        <%# Eval("ContractRate").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                                    </td>
                                    <td class="text-right top">
                                        <%# Eval("RecordCount") %>
                                    </td>
                                    <td class="text-right top">
                                        <%# Eval("TotalLowestBuy","{0:n4}")%>
                                    </td>
                                    <td class="text-right top">
                                        <%# Eval("TotalHighestBuy","{0:n4}")%>
                                    </td>
                                    <td class="text-right top">
                                        <%# Eval("TotalLowestSell","{0:n4}")%>
                                    </td>
                                    <td class="text-right top">
                                        <%# Eval("TotalHighestSell","{0:n4}")%>
                                    </td>
                                </tr>
                                <tr class="hidden">
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="20" class="p_m0 bottom_shadow">
                                        <div id="divLtlCharges<%# Container.DataItemIndex %>" style="display: none;" class="pb10 pl10 pr10">
                                            <h6 class="text-center pb5">Charge Breakdown</h6>
                                            <div class="row">
                                                <div class="left" style="width: 8%">
                                                    <label class="blue pl10 pr10">Code</label>
                                                </div>
                                                <div class="left" style="width: 40%">
                                                    <label class="blue pl10 pr10">Description</label>
                                                </div>
                                                <div class="left text-right" style="width: 13%">
                                                    <label class="blue pl10 pr10">Lowest Buy ($)</label>
                                                </div>
                                                <div class="left text-right" style="width: 13%">
                                                    <label class="blue pl10 pr10">Highest Buy ($)</label>
                                                </div>
                                                <div class="left text-right" style="width: 13%">
                                                    <label class="blue pl10 pr10">Lowest Sell ($)</label>
                                                </div>
                                                <div class="left text-right" style="width: 13%">
                                                    <label class="blue pl10 pr10">Highest Sell ($)</label>
                                                </div>
                                            </div>
                                            <asp:ListView ID="lstCharges" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <div class="row border-top">
                                                        <div class="left border-right" style="width: 8%">
                                                            <label class="pl10 pr10"><%# Eval("ChargeCode") %></label>
                                                        </div>
                                                        <div class="left border-right" style="width: 40%">
                                                            <label class="pl10 pr10"><%# Eval("ChargeCodeDescription")%></label>
                                                        </div>
                                                        <div class="left border-right text-right" style="width: 13%">
                                                            <label class="pl10 pr10"><%# Eval("LowestBuy","{0:n4}")%></label>
                                                        </div>
                                                        <div class="left border-right text-right" style="width: 13%">
                                                            <label class="pl10 pr10"><%# Eval("HighestBuy","{0:n4}")%></label>
                                                        </div>
                                                        <div class="left border-right text-right" style="width: 13%">
                                                            <label class="pl10 pr10"><%# Eval("LowestSell","{0:n4}")%></label>
                                                        </div>
                                                        <div class="left text-right" style="width: 12%">
                                                            <label class="pl10 pr10"><%# Eval("HighestSell","{0:n4}")%></label>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </div>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </div>
            </ContentTemplate>
        </ajax:TabPanel>
        <ajax:TabPanel runat="server" ID="tabTL">
            <ContentTemplate>
                <div class="finderScroll" id="vendorLaneHistoryTlGroupScrollSection">
                    <table class="stripe" id="vendorLaneHistoryTlGroupTable">
                        <tr>
                            <th style="width: 8%;">Vendor Number </th>
                            <th style="width: 31%;">Name </th>
                            <th style="width: 8%;" class="text-center">Contract Rate </th>
                            <th style="width: 8%;" class="text-right">
                                <abbr title="Contract Rate/Quote/Shipment record count">Record Count</abbr></th>
                            <th style="width: 10%;" class="text-right">Lowest Buy ($) </th>
                            <th style="width: 10%;" class="text-right">Highest Buy ($) </th>
                            <th style="width: 10%;" class="text-right">Lowest Sell ($) </th>
                            <th style="width: 10%;" class="text-right">Highest Sell ($) </th>
                        </tr>
                        <asp:ListView runat="server" ID="lstTLGroup" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnTLGroupItemDataBound">
                            <LayoutTemplate>
                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="top">
                                        <%# Eval("VendorNumber") %>
                                    </td>
                                    <td class="top">
                                        <eShip:CustomHiddenField runat="server" ID="hidVendorId" Value='<%# Eval("VendorId") %>' />
                                        <div class="row">
                                            <asp:LinkButton runat="server" ID="lbtnVendorName" Text='<%# Eval("VendorName") %>' OnClick="OnVendorNameClick" CssClass="blue"/>
                                        </div>
                                        <div class="row pt10">
                                            <asp:LinkButton runat="server" ID="lnkChargeBreakdown" Text="[See Charge Breakdown]" CssClass="blue link_nounderline" />
                                        </div>
                                    </td>
                                    <td class="text-center top">
                                        <%# Eval("ContractRate").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                                    </td>
                                    <td class="text-right top">
                                        <%# Eval("RecordCount") %>
                                    </td>
                                    <td class="text-right top">
                                        <%# Eval("TotalLowestBuy").ToDecimal().ToString("f4") %>
                                    </td>
                                    <td class="text-right top">
                                        <%# Eval("TotalHighestBuy").ToDecimal().ToString("f4") %>
                                    </td>
                                    <td class="text-right top">
                                        <%# Eval("TotalLowestSell").ToDecimal().ToString("f4") %>
                                    </td>
                                    <td class="text-right top">
                                        <%# Eval("TotalHighestSell").ToDecimal().ToString("f4") %>
                                    </td>
                                </tr>
                                <tr class="hidden">
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="20" class="p_m0 bottom_shadow">
                                        <div id="divTlCharges<%# Container.DataItemIndex %>" style="display: none;" class="pb10 pl10 pr10">
                                            <h6 class="text-center pb5">Charge Breakdown</h6>
                                            <div class="row">
                                                <div class="left" style="width: 8%">
                                                    <label class="blue pl10 pr10">Code</label>
                                                </div>
                                                <div class="left" style="width: 40%">
                                                    <label class="blue pl10 pr10">Description</label>
                                                </div>
                                                <div class="left text-right" style="width: 13%">
                                                    <label class="blue pl10 pr10">Lowest Buy ($)</label>
                                                </div>
                                                <div class="left text-right" style="width: 13%">
                                                    <label class="blue pl10 pr10">Highest Buy ($)</label>
                                                </div>
                                                <div class="left text-right" style="width: 13%">
                                                    <label class="blue pl10 pr10">Lowest Sell ($)</label>
                                                </div>
                                                <div class="left text-right" style="width: 13%">
                                                    <label class="blue pl10 pr10">Highest Sell ($)</label>
                                                </div>
                                            </div>
                                            <asp:ListView ID="lstCharges" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <div class="row border-top">
                                                        <div class="left border-right" style="width: 8%">
                                                            <label class="pl10 pr10"><%# Eval("ChargeCode") %></label>
                                                        </div>
                                                        <div class="left border-right" style="width: 40%">
                                                            <label class="pl10 pr10"><%# Eval("ChargeCodeDescription")%></label>
                                                        </div>
                                                        <div class="left border-right text-right" style="width: 13%">
                                                            <label class="pl10 pr10"><%# Eval("LowestBuy","{0:n4}")%></label>
                                                        </div>
                                                        <div class="left border-right text-right" style="width: 13%">
                                                            <label class="pl10 pr10"><%# Eval("HighestBuy","{0:n4}")%></label>
                                                        </div>
                                                        <div class="left border-right text-right" style="width: 13%">
                                                            <label class="pl10 pr10"><%# Eval("LowestSell","{0:n4}")%></label>
                                                        </div>
                                                        <div class="left text-right" style="width: 12%">
                                                            <label class="pl10 pr10"><%# Eval("HighestSell","{0:n4}")%></label>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </div>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </div>
            </ContentTemplate>
        </ajax:TabPanel>
        <ajax:TabPanel runat="server" ID="tabAir">
            <ContentTemplate>
                <div class="finderScroll" id="vendorLaneHistoryAirGroupScrollSection">
                    <table class="stripe" id="vendorLaneHistoryAirGroupTable">
                        <tr>
                            <th style="width: 8%;">Vendor Number </th>
                            <th style="width: 31%;">Name </th>
                            <th style="width: 8%;" class="text-center">Contract Rate </th>
                            <th style="width: 8%;" class="text-right">
                                <abbr title="Contract Rate/Quote/Shipment record count">Record Count</abbr></th>
                            <th style="width: 10%;" class="text-right">Lowest Buy ($) </th>
                            <th style="width: 10%;" class="text-right">Highest Buy ($) </th>
                            <th style="width: 10%;" class="text-right">Lowest Sell ($) </th>
                            <th style="width: 10%;" class="text-right">Highest Sell ($) </th>
                        </tr>
                        <asp:ListView runat="server" ID="lstAirGroup" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnAirGroupItemDataBound">
                            <LayoutTemplate>
                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="top">
                                        <%# Eval("VendorNumber") %>
                                    </td>
                                    <td class="top">
                                        <eShip:CustomHiddenField runat="server" ID="hidVendorId" Value='<%# Eval("VendorId") %>' />
                                        <div class="row">
                                            <asp:LinkButton runat="server" ID="lbtnVendorName" Text='<%# Eval("VendorName") %>' OnClick="OnVendorNameClick" CssClass="blue"/>
                                        </div>
                                        <div class="row pt10">
                                            <asp:LinkButton runat="server" ID="lnkChargeBreakdown" Text="[See Charge Breakdown]" CssClass="blue link_nounderline" />
                                        </div>
                                    </td>
                                    <td class="text-center top">
                                        <%# Eval("ContractRate").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                                    </td>
                                    <td class="text-right top">
                                        <%# Eval("RecordCount") %>
                                    </td>
                                    <td class="text-right top">
                                        <%# Eval("TotalLowestBuy").ToDecimal().ToString("f4") %>
                                    </td>
                                    <td class="text-right top">
                                        <%# Eval("TotalHighestBuy").ToDecimal().ToString("f4") %>
                                    </td>
                                    <td class="text-right top">
                                        <%# Eval("TotalLowestSell").ToDecimal().ToString("f4") %>
                                    </td>
                                    <td class="text-right top">
                                        <%# Eval("TotalHighestSell").ToDecimal().ToString("f4") %>
                                    </td>
                                </tr>
                                <tr class="hidden">
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="20" class="p_m0 bottom_shadow">
                                        <div id="divAirCharges<%# Container.DataItemIndex %>" style="display: none;" class="pb10 pl10 pr10">
                                            <h6 class="text-center pb5">Charge Breakdown</h6>
                                            <div class="row">
                                                <div class="left" style="width: 8%">
                                                    <label class="blue pl10 pr10">Code</label>
                                                </div>
                                                <div class="left" style="width: 40%">
                                                    <label class="blue pl10 pr10">Description</label>
                                                </div>
                                                <div class="left text-right" style="width: 13%">
                                                    <label class="blue pl10 pr10">Lowest Buy ($)</label>
                                                </div>
                                                <div class="left text-right" style="width: 13%">
                                                    <label class="blue pl10 pr10">Highest Buy ($)</label>
                                                </div>
                                                <div class="left text-right" style="width: 13%">
                                                    <label class="blue pl10 pr10">Lowest Sell ($)</label>
                                                </div>
                                                <div class="left text-right" style="width: 13%">
                                                    <label class="blue pl10 pr10">Highest Sell ($)</label>
                                                </div>
                                            </div>
                                            <asp:ListView ID="lstCharges" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <div class="row border-top">
                                                        <div class="left border-right" style="width: 8%">
                                                            <label class="pl10 pr10"><%# Eval("ChargeCode") %></label>
                                                        </div>
                                                        <div class="left border-right" style="width: 40%">
                                                            <label class="pl10 pr10"><%# Eval("ChargeCodeDescription")%></label>
                                                        </div>
                                                        <div class="left border-right text-right" style="width: 13%">
                                                            <label class="pl10 pr10"><%# Eval("LowestBuy","{0:n4}")%></label>
                                                        </div>
                                                        <div class="left border-right text-right" style="width: 13%">
                                                            <label class="pl10 pr10"><%# Eval("HighestBuy","{0:n4}")%></label>
                                                        </div>
                                                        <div class="left border-right text-right" style="width: 13%">
                                                            <label class="pl10 pr10"><%# Eval("LowestSell","{0:n4}")%></label>
                                                        </div>
                                                        <div class="left text-right" style="width: 12%">
                                                            <label class="pl10 pr10"><%# Eval("HighestSell","{0:n4}")%></label>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </div>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </div>
            </ContentTemplate>
        </ajax:TabPanel>
        <ajax:TabPanel runat="server" ID="tabRail">
            <ContentTemplate>
                <div class="finderScroll" id="vendorLaneHistoryRailGroupScrollSection">
                    <table class="stripe" id="vendorLaneHistoryRailGroupTable">
                        <tr>
                            <th style="width: 8%;">Vendor Number </th>
                            <th style="width: 31%;">Name </th>
                            <th style="width: 8%;" class="text-center">Contract Rate </th>
                            <th style="width: 8%;" class="text-right">
                                <abbr title="Contract Rate/Quote/Shipment record count">Record Count</abbr></th>
                            <th style="width: 10%;" class="text-right">Lowest Buy ($) </th>
                            <th style="width: 10%;" class="text-right">Highest Buy ($) </th>
                            <th style="width: 10%;" class="text-right">Lowest Sell ($) </th>
                            <th style="width: 10%;" class="text-right">Highest Sell ($) </th>
                        </tr>
                        <asp:ListView runat="server" ID="lstRailGroup" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnRailGroupItemDataBound">
                            <LayoutTemplate>
                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="top">
                                        <%# Eval("VendorNumber") %>
                                    </td>
                                    <td class="top">
                                        <eShip:CustomHiddenField runat="server" ID="hidVendorId" Value='<%# Eval("VendorId") %>' />
                                        <div class="row">
                                            <asp:LinkButton runat="server" ID="lbtnVendorName" Text='<%# Eval("VendorName") %>' OnClick="OnVendorNameClick" CssClass="blue"/>
                                        </div>
                                        <div class="row pt10">
                                            <asp:LinkButton runat="server" ID="lnkChargeBreakdown" Text="[See Charge Breakdown]" CssClass="blue link_nounderline" />
                                        </div>
                                    </td>
                                    <td class="text-center top">
                                        <%# Eval("ContractRate").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                                    </td>
                                    <td class="text-right top">
                                        <%# Eval("RecordCount") %>
                                    </td>
                                    <td class="text-right top">
                                        <%# Eval("TotalLowestBuy").ToDecimal().ToString("f4") %>
                                    </td>
                                    <td class="text-right top">
                                        <%# Eval("TotalHighestBuy").ToDecimal().ToString("f4") %>
                                    </td>
                                    <td class="text-right top">
                                        <%# Eval("TotalLowestSell").ToDecimal().ToString("f4") %>
                                    </td>
                                    <td class="text-right top">
                                        <%# Eval("TotalHighestSell").ToDecimal().ToString("f4") %>
                                    </td>
                                </tr>
                                <tr class="hidden">
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="20" class="p_m0 bottom_shadow">
                                        <div id="divRailCharges<%# Container.DataItemIndex %>" style="display: none;" class="pb10 pl10 pr10">
                                            <h6 class="text-center pb5">Charge Breakdown</h6>
                                            <div class="row">
                                                <div class="left" style="width: 8%">
                                                    <label class="blue pl10 pr10">Code</label>
                                                </div>
                                                <div class="left" style="width: 40%">
                                                    <label class="blue pl10 pr10">Description</label>
                                                </div>
                                                <div class="left text-right" style="width: 13%">
                                                    <label class="blue pl10 pr10">Lowest Buy ($)</label>
                                                </div>
                                                <div class="left text-right" style="width: 13%">
                                                    <label class="blue pl10 pr10">Highest Buy ($)</label>
                                                </div>
                                                <div class="left text-right" style="width: 13%">
                                                    <label class="blue pl10 pr10">Lowest Sell ($)</label>
                                                </div>
                                                <div class="left text-right" style="width: 13%">
                                                    <label class="blue pl10 pr10">Highest Sell ($)</label>
                                                </div>
                                            </div>
                                            <asp:ListView ID="lstCharges" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <div class="row border-top">
                                                        <div class="left border-right" style="width: 8%">
                                                            <label class="pl10 pr10"><%# Eval("ChargeCode") %></label>
                                                        </div>
                                                        <div class="left border-right" style="width: 40%">
                                                            <label class="pl10 pr10"><%# Eval("ChargeCodeDescription")%></label>
                                                        </div>
                                                        <div class="left border-right text-right" style="width: 13%">
                                                            <label class="pl10 pr10"><%# Eval("LowestBuy","{0:n4}")%></label>
                                                        </div>
                                                        <div class="left border-right text-right" style="width: 13%">
                                                            <label class="pl10 pr10"><%# Eval("HighestBuy","{0:n4}")%></label>
                                                        </div>
                                                        <div class="left border-right text-right" style="width: 13%">
                                                            <label class="pl10 pr10"><%# Eval("LowestSell","{0:n4}")%></label>
                                                        </div>
                                                        <div class="left text-right" style="width: 12%">
                                                            <label class="pl10 pr10"><%# Eval("HighestSell","{0:n4}")%></label>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </div>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </div>
            </ContentTemplate>
        </ajax:TabPanel>
        <ajax:TabPanel runat="server" ID="tabSmallPack">
            <ContentTemplate>

                <div class="finderScroll" id="vendorLaneHistorySmallPackGroupScrollSection">
                    <table class="stripe" id="vendorLaneHistorySmallPackGroupTable">
                        <tr>
                            <th style="width: 8%;">Vendor Number </th>
                            <th style="width: 31%;">Name </th>
                            <th style="width: 8%;" class="text-center">Contract Rate </th>
                            <th style="width: 8%;" class="text-right">
                                <abbr title="Contract Rate/Quote/Shipment record count">Record Count</abbr></th>
                            <th style="width: 10%;" class="text-right">Lowest Buy ($) </th>
                            <th style="width: 10%;" class="text-right">Highest Buy ($) </th>
                            <th style="width: 10%;" class="text-right">Lowest Sell ($) </th>
                            <th style="width: 10%;" class="text-right">Highest Sell ($) </th>
                        </tr>
                        <asp:ListView runat="server" ID="lstSmallPackGroup" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnSmallPackGroupItemDataBound">
                            <LayoutTemplate>
                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="top">
                                        <%# Eval("VendorNumber") %>
                                    </td>
                                    <td class="top">
                                        <eShip:CustomHiddenField runat="server" ID="hidVendorId" Value='<%# Eval("VendorId") %>' />
                                        <div class="row">
                                            <asp:LinkButton runat="server" ID="lbtnVendorName" Text='<%# Eval("VendorName") %>' OnClick="OnVendorNameClick" CssClass="blue"/>
                                        </div>
                                        <div class="row pt10">
                                            <asp:LinkButton runat="server" ID="lnkChargeBreakdown" Text="[See Charge Breakdown]" CssClass="blue link_nounderline" />
                                        </div>
                                    </td>
                                    <td class="text-center top">
                                        <%# Eval("ContractRate").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                                    </td>
                                    <td class="text-right top">
                                        <%# Eval("RecordCount") %>
                                    </td>
                                    <td class="text-right top">
                                        <%# Eval("TotalLowestBuy").ToDecimal().ToString("f4") %>
                                    </td>
                                    <td class="text-right top">
                                        <%# Eval("TotalHighestBuy").ToDecimal().ToString("f4") %>
                                    </td>
                                    <td class="text-right top">
                                        <%# Eval("TotalLowestSell").ToDecimal().ToString("f4") %>
                                    </td>
                                    <td class="text-right top">
                                        <%# Eval("TotalHighestSell").ToDecimal().ToString("f4") %>
                                    </td>
                                </tr>
                                <tr class="hidden">
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="20" class="p_m0 bottom_shadow">
                                        <div id="divSmallPackCharges<%# Container.DataItemIndex %>" style="display: none;" class="pb10 pl10 pr10">
                                            <h6 class="text-center pb5">Charge Breakdown</h6>
                                            <div class="row">
                                                <div class="left" style="width: 8%">
                                                    <label class="blue pl10 pr10">Code</label>
                                                </div>
                                                <div class="left" style="width: 40%">
                                                    <label class="blue pl10 pr10">Description</label>
                                                </div>
                                                <div class="left text-right" style="width: 13%">
                                                    <label class="blue pl10 pr10">Lowest Buy ($)</label>
                                                </div>
                                                <div class="left text-right" style="width: 13%">
                                                    <label class="blue pl10 pr10">Highest Buy ($)</label>
                                                </div>
                                                <div class="left text-right" style="width: 13%">
                                                    <label class="blue pl10 pr10">Lowest Sell ($)</label>
                                                </div>
                                                <div class="left text-right" style="width: 13%">
                                                    <label class="blue pl10 pr10">Highest Sell ($)</label>
                                                </div>
                                            </div>
                                            <asp:ListView ID="lstCharges" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <div class="row border-top">
                                                        <div class="left border-right" style="width: 8%">
                                                            <label class="pl10 pr10"><%# Eval("ChargeCode") %></label>
                                                        </div>
                                                        <div class="left border-right" style="width: 40%">
                                                            <label class="pl10 pr10"><%# Eval("ChargeCodeDescription")%></label>
                                                        </div>
                                                        <div class="left border-right text-right" style="width: 13%">
                                                            <label class="pl10 pr10"><%# Eval("LowestBuy","{0:n4}")%></label>
                                                        </div>
                                                        <div class="left border-right text-right" style="width: 13%">
                                                            <label class="pl10 pr10"><%# Eval("HighestBuy","{0:n4}")%></label>
                                                        </div>
                                                        <div class="left border-right text-right" style="width: 13%">
                                                            <label class="pl10 pr10"><%# Eval("LowestSell","{0:n4}")%></label>
                                                        </div>
                                                        <div class="left text-right" style="width: 12%">
                                                            <label class="pl10 pr10"><%# Eval("HighestSell","{0:n4}")%></label>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </div>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </div>
            </ContentTemplate>
        </ajax:TabPanel>
    </ajax:TabContainer>
    
</asp:Panel>
<asp:Panel ID="pnlVendorLaneHistoryDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
<eShip:CustomHiddenField ID="hidOriginPostalCode" runat="server" />
<eShip:CustomHiddenField ID="hidOriginCountryId" runat="server" />
<eShip:CustomHiddenField ID="hidDestinationPostalCode" runat="server" />
<eShip:CustomHiddenField ID="hidDestinationCountryId" runat="server" />
<eShip:CustomHiddenField ID="hidTotalWeight" runat="server" />
<eShip:CustomHiddenField ID="hidDesiredPickupDate" runat="server" />
