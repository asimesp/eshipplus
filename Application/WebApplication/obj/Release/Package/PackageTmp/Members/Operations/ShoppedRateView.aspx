﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="ShoppedRateView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.ShoppedRateView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Administration" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Shopped Rates
                <small class="ml10">
                    <asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>
        <span class="clearfix"></span>
        <hr class="dark mb10" />
        <asp:Panel runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="ShoppedRates" OnProcessingError="OnSearchProfilesProcessingError"
                            OnSearchProfileSelected="OnSearchProfilesProfileSelected" />
                    </div>
                </div>
            </div>
            <hr class="dark mb10" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                        OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>
            <hr class="fat" />
            <div class="rowgroup">
                <div class="row mb10">
                    <div class="left">
                        <asp:Button ID="btnSearch" CssClass="left" runat="server" Text="SEARCH" OnClick="OnSearchClicked"
                            CausesValidation="False" />
                    </div>
                    <div class="right">
                        <div class="fieldgroup">
                            <label>Sort By:</label>
                            <asp:DropDownList runat="server" ID="ddlSortBy" DataTextField="DisplayName" DataValueField="Name"
                                OnSelectedIndexChanged="OnSortBySelectedIndexChanged" AutoPostBack="True" />
                        </div>
                        <div class="fieldgroup">
                            <asp:RadioButton runat="server" ID="rbAsc" Text="ASC" GroupName="rbSortBy" AutoPostBack="True"
                                ToolTip="Ascending" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                        </div>
                        <div class="fieldgroup">
                            <asp:RadioButton runat="server" ID="rbDesc" Text="DESC" GroupName="rbSortBy" Checked="True"
                                ToolTip="Descending" AutoPostBack="True" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheShoppedRatesTable" TableId="shoppedRatesTable" HeaderZIndex="2" />
        <table class="line2 pl2" id="shoppedRatesTable">
            <tr>
                <th style="width: 9%;">
                    <asp:LinkButton runat="server" ID="lbtnSortReferenceNumber" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                            <abbr title="Reference Number">Ref #</abbr>
                    </asp:LinkButton>
                </th>
                <th style="width: 12%;">
                    <asp:LinkButton runat="server" ID="lbtnSortType" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                           Type
                    </asp:LinkButton>
                </th>
                <th style="width: 15%;">
                    <asp:LinkButton runat="server" ID="lbtnSortDateCreated" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                            Date Created
                    </asp:LinkButton>
                </th>
                <th style="width: 20%;">
                    <asp:LinkButton runat="server" ID="lbtnSortUser" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                            User
                    </asp:LinkButton>
                </th>
                <th style="width: 6%;" class="text-right">
                    <asp:LinkButton runat="server" ID="lbtnSortNumberLineItems" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                           <abbr title="Number of Line Items">#</abbr>
                    </asp:LinkButton>
                </th>
                <th style="width: 8%;" class="text-right">
                    <asp:LinkButton runat="server" ID="lbnSortQuantity" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                            Quantity
                    </asp:LinkButton>
                </th>
                <th style="width: 10%;" class="text-right">
                    <asp:LinkButton runat="server" ID="lbtnSortWeight" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                             Weight<abbr title="Pounds">(lb)</abbr>
                    </asp:LinkButton>
                </th>
                <th style="width: 10%;" class="text-right">
                    <asp:LinkButton runat="server" ID="lbtnSortHighestFreightClass" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                             <abbr title="Highest freight class">HFC</abbr>
                    </asp:LinkButton>/
                 <asp:LinkButton runat="server" ID="lbtnMostSignificantClass" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                            <abbr title="Most significant freight class">SFC</abbr>
                 </asp:LinkButton>
                </th>
                <th style="width: 10%;" class="text-right">
                    <asp:LinkButton runat="server" ID="lbtnLowestTotalCharge" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                              <abbr title="Lowest total charge">LTC ($)</abbr>
                    </asp:LinkButton>
                </th>
            </tr>
            <asp:ListView ID="lvwShoppedRates" runat="server" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnShoppedRateItemDataBound">
                <LayoutTemplate>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </LayoutTemplate>
                <ItemTemplate>
                    <tr>
                        <td rowspan="2" class="top">
                            <eShip:CustomHiddenField ID="hidShoppedRateId" runat="server" Value='<%# Eval("Id") %>' />
                            <eShip:CustomHiddenField ID="hidLoadOrderId" runat="server" Value='<%# Eval("LoadOrderId") %>' />
                            <eShip:CustomHiddenField ID="hidShipmentId" runat="server" Value='<%# Eval("ShipmentId") %>' />
                            <asp:Literal runat="server" Text='<%# Eval("ReferenceNumber") %>' />
                            <asp:HyperLink runat="server" ID="hypTransferLink" Target="_blank" Visible='<%# !ShowLiteralReferenceNumber(Container.DataItem) %>' CssClass="blue">
                                <img src='<%= ResolveUrl("~/images/icons2/arrow_newTab.png") %>' alt="" align="absmiddle" width="16"/>
                            </asp:HyperLink>
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("Type") %>' ID="litType" />
                        </td>
                        <td>
                            <%# Eval("DateCreated").FormattedLongDateAlt()%>
                        </td>
                        <td>
                            <%# Eval("Username") %>
                            <%# ActiveUser.HasAccessTo(ViewCode.User) 
                                    ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To User Record'><img src='{3}' alt='' width='16' align='absmiddle'/></a>", 
                                        ResolveUrl(UserView.PageAddress),
                                        WebApplicationConstants.TransferNumber, 
                                        Eval("UserId").GetString().UrlTextEncrypt(),
                                        ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                    : string.Empty  %>
                        </td>
                        <td class="text-right">
                            <%# Eval("NumberLineItems") %>
                        </td>
                        <td class="text-right">
                            <%# Eval("Quantity") %>
                        </td>
                        <td class="text-right">
                            <%# Eval("Weight", "{0:f2}")%>
                        </td>
                        <td class="text-right">
                            <%# Eval("HighestFreightClass")%>/<%# Eval("MostSignificantFreightClass")%>
                        </td>
                        <td class="text-right">
                            <%# Eval("LowestTotalCharge", "{0:n2}") %>
                        </td>
                    </tr>
                    <tr class="f9">
                        <td colspan="8" class="forceLeftBorder">
                            <div class="rowgroup mb10 <%# Eval("HasServices").ToBoolean() ? "bottom_shadow" : string.Empty %>">
                                <div class="col_1_2">
                                    <div class="col_1_2">
                                        <div class="row">
                                            <label class="wlabel blue">
                                                Customer <%# Eval("CustomerRateAndScheduleInDemo").ToBoolean() ? "[In Demo] " : string.Empty %>
                                            </label>
                                            <label class="mr10">
                                                <%# Eval("CustomerNumber") %> - <%# Eval("CustomerName") %>
                                                <asp:HyperLink runat="server" ID="hypCustomerNumber" Target="_blank" ToolTip="Go To Customer Record" Visible='<%# ActiveUser.HasAccessTo(ViewCode.Customer) %>'
                                                    NavigateUrl='<%# string.Format("{0}?{1}={2}", CustomerView.PageAddress, WebApplicationConstants.TransferNumber, Eval("CustomerId").GetString().UrlTextEncrypt()) %>'>
                                                    <img src='<%# ResolveUrl("~/images/icons2/arrow_newTab.png") %>' alt="" width="16" class="middle"/>
                                                </asp:HyperLink>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col_1_2">
                                        <div class="row">
                                            <label class="wlabel blue">
                                                Vendor
                                            </label>
                                            <label class="mr10">
                                                <%# string.IsNullOrEmpty(Eval("VendorNumber").GetString()) ? string.Empty : string.Format("{0} - {1}", Eval("VendorNumber"), Eval("VendorName")) %>
                                                <asp:HyperLink runat="server" ID="hypVendorNumber" Target="_blank" ToolTip="Go To Vendor Record"
                                                    Visible='<%#  !string.IsNullOrEmpty(Eval("VendorNumber").GetString()) && ActiveUser.HasAccessTo(ViewCode.Vendor) %>'>
                                                    <img src='<%# ResolveUrl("~/images/icons2/arrow_newTab.png") %>' alt="" width="16" class="middle"/>
                                                </asp:HyperLink>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col_1_2">
                                    <div class="col_1_2">
                                        <div class="row">
                                            <label class="wlabel blue">Origin</label>
                                            <label class="mr10">
                                                <%# string.IsNullOrEmpty(Eval("OriginCity").GetString()) ? string.Empty : string.Format("{0},", Eval("OriginCity"))%> <%#Eval("OriginState")%> <%#Eval("OriginPostalCode")%> <%#Eval("OriginCountryName")%>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col_1_2">
                                        <div class="row">
                                            <label class="wlabel blue">Destination</label>
                                            <label class="mr10">
                                                <%# string.IsNullOrEmpty(Eval("DestinationCity").GetString()) ? string.Empty : string.Format("{0},", Eval("DestinationCity"))%> <%#Eval("DestinationState")%> <%#Eval("DestinationPostalCode")%> <%#Eval("DestinationCountryName")%>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <asp:ListView runat="server" ID="lstShoppedRateServices" ItemPlaceholderID="itemPlaceHolder">
                                <LayoutTemplate>
                                    <h6 class="text-center upper">Services</h6>
                                    <div class="row pb10">
                                        <table class="contain pl2">
                                            <tr class="bbb1">
                                                <th style="width: 10%" class="no-top-border pt5">Service Code
                                                </th>
                                                <th style="width: 30%" class="no-top-border no-left-border pt5">Service Description
                                                </th>
                                                <th style="width: 15%" class="no-top-border no-left-border pt5">Category
                                                </th>
                                                <th style="width: 10%" class="no-top-border no-left-border pt5">Charge Code
                                                </th>
                                                <th style="width: 35%" class="no-top-border no-left-border pt5">Charge Code Description
                                                </th>
                                            </tr>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </table>
                                    </div>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr class="no-bottom-border">
                                        <td>
                                            <%# Eval("Code") %>
                                        </td>
                                        <td>
                                            <%# Eval("Description") %>
                                        </td>
                                        <td>
                                            <%# Eval("Category") %>
                                        </td>
                                        <td>
                                            <%# Eval("ChargeCode") %>
                                        </td>
                                        <td>
                                            <%# Eval("ChargeCodeDescription") %>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:ListView>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </table>
    </div>
    <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
</asp:Content>
