﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="DispatchDashboardView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.DispatchDashboardView" EnableEventValidation="false" %>

<%@ Import Namespace="LogisticsPlus.Eship.Dat" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor.Dto.Operations" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Operations" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" ImageUrl="~/images/operationsBlue.png" />
                Dispatch Dashboard
            </h3>
        </div>

        <eShip:PostalCodeFinderControl runat="server" ID="postalCodeFinder" Visible="False" OnItemSelected="OnPostalCodeFinderItemSelected"
            OnSelectionCancel="OnPostalCodeFinderItemCancelled" />
        <eShip:DatLoadboardAssetControl runat="server" ID="datLoadboardAssetControl" Visible="false" OnDoneModifying="OnCancelDatAssetModifications" />
        <eShip:AddressBookFinderControl runat="server" ID="addressBookFinder" Visible="False" OnItemSelected="OnAddressBookFinderItemSelected"
            OnSelectionCancel="OnAddressBookFinderItemCancelled" OpenForEditEnabled="false" />
        <eShip:LibraryItemFinderControl EnableMultiSelection="True" runat="server" ID="libraryItemFinder" OpenForEditEnabled="false"
            Visible="false" OnMultiItemSelected="OnLibraryItemFinderMultiItemSelected" OnSelectionCancel="OnLibraryItemFinderItemCancelled" />
        <eShip:VendorFinderControl EnableMultiSelection="False" ID="vendorFinder" OnItemSelected="OnVendorFinderItemSelected" OnlyActiveResults="true"
            OnSelectionCancel="OnVendorFinderItemCancelled" OpenForEditEnabled="false" runat="server" Visible="false" />
        <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information" OnOkay="OnOkayProcess" OnNo="OnNoProcess" />
        <div class="rowgroup mb0">
            <div class="row">
                <div class="fieldgroup right">
                    <script type="text/javascript">
                        $(function () {
                            ReloadSectionVisibilities();
                            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                                ReloadSectionVisibilities();
                            });
                        });

                        function ReloadSectionVisibilities() {
                            if (jsHelper.IsTrue($('#<%= hidAreFiltersShowing.ClientID%>').val())) {
                                $('#<%= pnlSearch.ClientID%>').show();
                            } else {
                                $('#<%= pnlSearch.ClientID%>').hide();
                            }

                            if ($('#<%= hidRecordTypeShowing.ClientID%>').val() == '<%= DashboardDtoType.LoadOrder.ToString()%>') {
                                $('#divShipments').hide();
                                $('#divLoadOrders').show();
                            } else {
                                $('#divShipments').show();
                                $('#divLoadOrders').hide();
                            }


                            if ($('#<%= hidEditingRecordPanelClientId.ClientID %>').val() != '') {
                                $('#<%= pnlClientSideDimScreen.ClientID%>').show();
                                $($('#<%= hidEditingRecordPanelClientId.ClientID %>').val()).show();
                            } else {
                                $('#<%= pnlClientSideDimScreen.ClientID%>').hide();
                            }
                        }

                        function ToggleFilters() {
                            $('#<%= hidAreFiltersShowing.ClientID%>').val(jsHelper.IsTrue($('#<%= hidAreFiltersShowing.ClientID%>').val()) ? false : true);
                            $('#<%= pnlSearch.ClientID%>').slideToggle();
                        }

                        function DisplayLoadOrders() {
                            $('#divShipments').hide();
                            $('#divLoadOrders').show();
                            $('#<%= hidRecordTypeShowing.ClientID%>').val('<%= DashboardDtoType.LoadOrder.ToString()%>');
                        }

                        function DisplayShipments() {
                            $('#divShipments').show();
                            $('#divLoadOrders').hide();
                            $('#<%= hidRecordTypeShowing.ClientID%>').val('<%= DashboardDtoType.Shipment.ToString()%>');
                        }
                    </script>
                    <button onclick="ToggleFilters(); return false;" class="mr20">Filters</button>
                    <button onclick="DisplayLoadOrders(); return false;">Load Orders</button>
                    <button onclick="DisplayShipments(); return false;">Shipments</button>
                </div>
            </div>
        </div>

        <hr class="fat" />

        <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="Shipments" OnProcessingError="OnSearchProfilesProcessingError"
                            OnSearchProfileSelected="OnSearchProfilesProfileSelected" ShowAutoRefresh="False" />
                    </div>
                </div>
            </div>
            <hr class="dark mb10" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                        OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>
            <hr class="fat" />
            <div class="rowgroup">
                <div class="row mb10">
                    <div class="left">
                        <asp:Button ID="btnSearch" CssClass="left" runat="server" Text="SEARCH" OnClick="OnSearchClicked" CausesValidation="False" />
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>

    <script type="text/javascript">
        var app = angular.module('DispatchDashboardMainApp', []);

        app.controller('DispatchDashboardMainAppControl', function ($scope, $http, $interval, $filter) {
            if ($('#<%= hidSerializedParameters.ClientID %>').val() != '') {
                DoSearch($http, $scope, $filter);
                $interval(function () { DoSearch($http, $scope, $filter); }, 10000);
            }

            $scope.DisplayModifyPopupForShipment = function (shipmentId, updatePanelClientId, updateArgument) {
                $('#<%= hidEditShipmentId.ClientID%>').val(shipmentId);
        	    $('#<%= hidEditLoadOrderId.ClientID%>').val(jsHelper.EmptyString);
        	    SetCurrentScrollAndGoToTop();
        	    window.__doPostBack(updatePanelClientId, updateArgument);
        	};

            $scope.DisplayModifyPopupForLoadOrder = function (loadOrderId, updatePanelClientId, updateArgument) {

                $('#<%= hidEditShipmentId.ClientID%>').val(jsHelper.EmptyString);
        	    $('#<%= hidEditLoadOrderId.ClientID%>').val(loadOrderId);
        	    SetCurrentScrollAndGoToTop();
        	    window.__doPostBack(updatePanelClientId, updateArgument);
        	};
			
        	$scope.SortShipments = function (sortByField) {
        	    var hidShipmentSortReverse = $('#<%= hidShipmentSortReverse.ClientID %>');
                var hidShipmentSortByField = $('#<%= hidShipmentSortByField.ClientID %>');

                hidShipmentSortReverse.val(hidShipmentSortByField.val() == sortByField && !jsHelper.IsTrue(hidShipmentSortReverse.val()));
                hidShipmentSortByField.val(sortByField);
                $scope.shipments = $filter('orderBy')($scope.shipments, sortByField, jsHelper.IsTrue(hidShipmentSortReverse.val()));
            };

            $scope.SortLoadOrders = function (sortByField) {
                var hidLoadOrderSortReverse = $('#<%= hidLoadOrderSortReverse.ClientID %>');
                var hidLoadOrderSortByField = $('#<%= hidLoadOrderSortByField.ClientID %>');

                hidLoadOrderSortReverse.val(hidLoadOrderSortByField.val() == sortByField && !jsHelper.IsTrue(hidLoadOrderSortReverse.val()));
                hidLoadOrderSortByField.val(sortByField);
                $scope.loadOrders = $filter('orderBy')($scope.loadOrders, sortByField, jsHelper.IsTrue(hidLoadOrderSortReverse.val()));
            };

            // bind event to handle resettting scroll position and reloading search results
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                if (jsHelper.IsTrue($('#<%= hidGoToCurrentScrollPosition.ClientID%>').val())) {
                    $(document).scrollTop($('#<%= hidCurrentScrollPosition.ClientID%>').val());
                    $('#<%= hidCurrentScrollPosition.ClientID%>').val(0);
                    $('#<%= hidGoToCurrentScrollPosition.ClientID%>').val('False');
                    DoSearch($http, $scope, $filter);
                }
            });
        });

        function SetCurrentScrollAndGoToTop() {
            $('#<%= hidCurrentScrollPosition.ClientID %>').val($(document).scrollTop());
            $(document).scrollTop(0);
        }

        function DoSearch($http, $scope, $filter) {
            $http({
                method: "POST",
                url: "DispatchDashboardView.aspx/GetData",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: '{ "p":"' + $('#<%= hidSerializedParameters.ClientID %>').val() + '", "tenant":"' + $('#<%= hidTenantUser.ClientID %>').val() + '" }',
            }).then(function (response) {
                $scope.shipments = new Array();
                $scope.loadOrders = new Array();

                for (var i = 0; i < response.data.d.length; i++) {
                    if (response.data.d[i].Type == '<%= DashboardDtoType.Shipment.GetString() %>') {
                        var shipment = response.data.d[i];
                        shipment.DesiredPickupDateAsDate = shipment.DesiredPickupDate == '' ? new Date('01/01/1753') : new Date(shipment.DesiredPickupDate);
                        shipment.ActualPickupDateAsDate = shipment.ActualPickupDate == '' ? new Date('01/01/1753') : new Date(shipment.ActualPickupDate);
                        shipment.EstimatedDeliveryDateAsDate = shipment.EstimatedDeliveryDate == '' ? new Date('01/01/1753') : new Date(shipment.EstimatedDeliveryDate);
                        $scope.shipments.push(shipment);
                    } else {
                        var loadOrder = response.data.d[i];
                        loadOrder.DesiredPickupDateAsDate = loadOrder.DesiredPickupDate == '' ? new Date('01/01/1753') : new Date(loadOrder.DesiredPickupDate);
                        loadOrder.EstimatedDeliveryDateAsDate = loadOrder.EstimatedDeliveryDate == '' ? new Date('01/01/1753') : new Date(loadOrder.EstimatedDeliveryDate);
                        $scope.loadOrders.push(loadOrder);
                    }
                }
                $scope.shipments = $filter('orderBy')($scope.shipments, $('#<%= hidShipmentSortByField.ClientID %>').val(), jsHelper.IsTrue($('#<%= hidShipmentSortReverse.ClientID %>').val()));
                $scope.loadOrders = $filter('orderBy')($scope.loadOrders, $('#<%= hidLoadOrderSortByField.ClientID %>').val(), jsHelper.IsTrue($('#<%= hidLoadOrderSortReverse.ClientID %>').val()));

                $scope.ShipmentCountLabel = $scope.shipments.length > 0 ? "(" + $scope.shipments.length + " record" + ($scope.shipments.length > 1 ? "s" : "") + " found)" : jsHelper.EmptyString;
                $scope.LoadOrderCountLabel = $scope.loadOrders.length > 0 ? "(" + $scope.loadOrders.length + " record" + ($scope.loadOrders.length > 1 ? "s" : "") + " found)" : jsHelper.EmptyString;
            });
        }
    </script>

    <div ng-app="DispatchDashboardMainApp" ng-controller="DispatchDashboardMainAppControl">
        <input type="hidden" runat="server" id="hidSerializedParameters" />
        <input type="hidden" runat="server" id="hidTenantUser" value="" />

        <div id="divShipments">
            <h5>Shipments<small class="ml10" ng-bind="ShipmentCountLabel"></small></h5>
            <eShip:TableFreezeHeaderExtender runat="server" ID="tfheShipmentDashboardTable" TableId="shipmentDashboardTable" HeaderZIndex="2" IgnoreHeaderBackgroundFill="False" />
            <table class="line2 pl3" id="shipmentDashboardTable">
                <thead>
                    <tr>
                        <th style="width: 8%">
                            <a href="" ng-click="SortShipments('ShipmentNumber')" class="link_nounderline blue">Shipment #</a>
                        </th>
                        <th style="width: 10%">
                            <a href="" ng-click="SortShipments('CustomerName')" class="link_nounderline blue">Customer</a>
                        </th>
                        <th style="width: 10%">
                            <a href="" ng-click="SortShipments('VendorName')" class="link_nounderline blue">Vendor</a>
                        </th>
                        <th style="width: 9%">
                            <a href="" ng-click="SortShipments('OriginCity')" class="link_nounderline blue">Origin</a>
                        </th>
                        <th style="width: 9%">
                            <a href="" ng-click="SortShipments('DestinationCity')" class="link_nounderline blue">Destination</a>
                        </th>
                        <th style="width: 10%">
                            <a href="" ng-click="SortShipments('DesiredPickupDateAsDate')" class="link_nounderline blue">Desired<br />
                                Pickup</a>
                        </th>
                        <th style="width: 10%">
                            <a href="" ng-click="SortShipments('ActualPickupDateAsDate')" class="link_nounderline blue">Actual<br />
                                Pickup</a>
                        </th>
                        <th style="width: 10%">
                            <a href="" ng-click="SortShipments('EstimatedDeliveryDateAsDate')" class="link_nounderline blue">Estimated<br />
                                Delivery</a>
                        </th>
                        <th style="width: 8%" class="text-right">
                            <a href="" ng-click="SortShipments('TotalWeight')" class="link_nounderline blue">Total<br />
                                Weight</a>
                        </th>
                        <th style="width: 8%" class="text-right">
                            <a href="" ng-click="SortShipments('TotalAmountDue')" class="link_nounderline blue">Total<br />
                                Sell</a>
                        </th>
                        <th style="width: 8%">
                            <a href="" ng-click="SortShipments('EquipmentCode')" class="link_nounderline blue">Equipment</a>
                        </th>
                        <th style="width: 8%">
                            <a href="" ng-click="SortShipments('StatusText')" class="link_nounderline blue">Status</a>
                        </th>
                    </tr>
                </thead>
                <tr ng-repeat="x in shipments">
                    <td>{{x.ShipmentNumber}} 
                        <%= ActiveUser.HasAccessTo(ViewCode.Shipment) 
                                        ? string.Format("<a href='{0}?{1}={{{{x.ShipmentNumber}}}}' class='blue' target='_blank' title='Go To Shipment Record'><img src='{2}' alt='' width='16' align='absmiddle'/></a>", 
                                            ResolveUrl(ShipmentView.PageAddress),
                                            WebApplicationConstants.TransferNumber,
                                            ResolveUrl("~/images/icons2/arrow_newTab.png")) 
                                        : string.Empty %>
                    </td>
                    <td>{{x.CustomerName}}
                        <%= ActiveUser.HasAccessTo(ViewCode.Customer) 
                            ? string.Format("<a href='{0}?{1}={{{{x.CustomerIdEncrypted}}}}' class='blue' target='_blank' title='Go To Customer Record'><img src='{2}' alt='' width='1' align='absmiddle'/></a>", 
                                ResolveUrl(CustomerView.PageAddress),
                                WebApplicationConstants.TransferNumber,
                                ResolveUrl("~/images/icons2/arrow_newTab.png")) 
                            : string.Empty %>
                    </td>
                    <td>
                        <a href="" ng-click="DisplayModifyPopupForShipment(x.Id, '<%= udpModifyVendor.ClientID %>', '')" class="link_nounderline link_bodytextcolor">{{x.VendorName}}</a>
                        <input type="image" ng-show="x.VendorId == 0" onclick="return false;" ng-click="DisplayModifyPopupForShipment(x.Id, '<%= udpModifyVendor.ClientID %>', '')" src="<%= ResolveUrl("~/images/icons2/downloadDocument.png") %>" align="absmiddle" width="10" />
                        <%= ActiveUser.HasAccessTo(ViewCode.Vendor) 
                                ? string.Format("<a ng-show='{{{{x.VendorId>0}}}}'  href='{0}?{1}={{{{x.VendorIdEncrypted}}}}' class='blue' target='_blank' title='Go To Vendor Record'><img src='{2}' alt='' width='16' align='absmiddle'/></a>", 
                                    ResolveUrl(VendorView.PageAddress),
                                    WebApplicationConstants.TransferNumber,
                                    ResolveUrl("~/images/icons2/arrow_newTab.png")) 
                                : string.Empty %>
                    </td>
                    <td>
                        <a href="" ng-click="DisplayModifyPopupForShipment(x.Id, '<%= udpModifyLocation.ClientID %>', '<%= OriginStopText %>')" class="link_nounderline link_bodytextcolor">{{x.OriginCity}}
                            <br />
                            {{x.OriginState}} {{x.OriginPostalCode}}</a>
                    </td>
                    <td>
                        <a href="" ng-click="DisplayModifyPopupForShipment(x.Id, '<%= udpModifyLocation.ClientID %>', '<%= DestinationStopText %>')" class="link_nounderline link_bodytextcolor">{{x.DestinationCity}} {{x.DestinationState}} {{x.DestinationPostalCode}}</a>
                    </td>
                    <td>
                        <a href="" ng-click="DisplayModifyPopupForShipment(x.Id, '<%= udpModifyDates.ClientID %>', '')" class="link_nounderline link_bodytextcolor">{{x.DesiredPickupDate}}</a>
                        <input type="image" ng-show="x.DesiredPickupDate == ''" onclick="return false;" ng-click="DisplayModifyPopupForShipment(x.Id, '<%= udpModifyDates.ClientID %>', '')" src="<%= ResolveUrl("~/images/icons2/downloadDocument.png") %>" align="absmiddle" width="10" />
                    </td>
                    <td>
                        <a href="" ng-click="DisplayModifyPopupForShipment(x.Id, '<%= udpModifyDates.ClientID %>', '')" class="link_nounderline link_bodytextcolor">{{x.ActualPickupDate}}</a>
                        <input type="image" ng-show="x.ActualPickupDate == ''" onclick="return false;" ng-click="DisplayModifyPopupForShipment(x.Id, '<%= udpModifyDates.ClientID %>', '')" src="<%= ResolveUrl("~/images/icons2/downloadDocument.png") %>" align="absmiddle" width="10" />
                    </td>
                    <td>
                        <a href="" ng-click="DisplayModifyPopupForShipment(x.Id, '<%= udpModifyDates.ClientID %>', '')" class="link_nounderline link_bodytextcolor">{{x.EstimatedDeliveryDate}}</a>
                        <input type="image" ng-show="x.EstimatedDeliveryDate == ''" onclick="return false;" ng-click="DisplayModifyPopupForShipment(x.Id, '<%= udpModifyDates.ClientID %>', '')" src="<%= ResolveUrl("~/images/icons2/downloadDocument.png") %>" align="absmiddle" width="10" />
                    </td>
                    <td class="text-right">
                        <a href="" ng-click="DisplayModifyPopupForShipment(x.Id, '<%= udpModifyItems.ClientID %>', '')" class="link_nounderline link_bodytextcolor">{{x.TotalWeightAsString}}</a>
                    </td>
                    <td class="text-right">
                        <a href="" ng-click="DisplayModifyPopupForShipment(x.Id, '<%= udpModifyCharges.ClientID %>', '')" class="link_nounderline link_bodytextcolor">{{x.TotalAmountDueAsString}}</a>
                    </td>
                    <td>
                        <a href="" ng-click="DisplayModifyPopupForShipment(x.Id, '<%= udpModifyEquipment.ClientID %>', '')" class="link_nounderline link_bodytextcolor">{{x.Equipments.replace(",","")}}</a>
                        <input type="image" ng-show="x.Equipments == ''" onclick="return false;" ng-click="DisplayModifyPopupForShipment(x.Id, '<%= udpModifyEquipment.ClientID %>', '')" src="<%= ResolveUrl("~/images/icons2/downloadDocument.png") %>" align="absmiddle" width="10" />
                    </td>
                    <td>{{x.StatusText}}
                    </td>
                </tr>
            </table>
        </div>
        <div id="divLoadOrders">
            <h5>Load Orders<span class="ml10" ng-bind="LoadOrderCountLabel"></span></h5>
            <eShip:TableFreezeHeaderExtender runat="server" ID="tfheLoadOrderDashboardTable" TableId="loadOrderDashboardTable" HeaderZIndex="2" IgnoreHeaderBackgroundFill="False" />
            <table class="line2 pl3" id="loadOrderDashboardTable">
                <thead>
                    <tr>
                        <th style="width: 8%">
                            <a href="" ng-click="SortLoadOrders('ShipmentNumber')" class="blue link_nounderline">Load Order #</a>
                        </th>
                        <th style="width: 10%">
                            <a href="" ng-click="SortLoadOrders('CustomerName')" class="link_nounderline blue">Customer</a>
                        </th>
                        <th style="width: 10%">
                            <a href="" ng-click="SortLoadOrders('VendorName')" class="link_nounderline blue">Vendor</a>
                        </th>
                        <th style="width: 11%">
                            <a href="" ng-click="SortLoadOrders('OriginCity')" class="blue link_nounderline">Origin</a>
                        </th>
                        <th style="width: 11%">
                            <a href="" ng-click="SortLoadOrders('DestinationCity')" class="blue link_nounderline">Destination</a>
                        </th>
                        <th style="width: 8%">
                            <a href="" ng-click="SortLoadOrders('DesiredPickupDateAsDate')" class="blue link_nounderline">Desired<br />
                                Pickup</a>
                        </th>
                        <th style="width: 8%">
                            <a href="" ng-click="SortLoadOrders('EstimatedDeliveryDateAsDate')" class="blue link_nounderline">Estimated<br />
                                Delivery</a>
                        </th>
                        <th style="width: 6%" class="text-right">
                            <a href="" ng-click="SortLoadOrders('TotalWeight')" class="blue link_nounderline">Total<br />
                                Weight</a>
                        </th>
                        <th style="width: 8%" class="text-right">
                            <a href="" ng-click="SortLoadOrders('TotalAmountDue')" class="blue link_nounderline">Total<br />
                                Sell</a>
                        </th>
                        <th style="width: 8%">
                            <a href="" ng-click="SortLoadOrders('EquipmentCode')" class="link_nounderline blue">Equipment</a>
                        </th>
                        <th style="width: 6%">
                            <a href="" ng-click="SortLoadOrders('StatusText')" class="blue link_nounderline">Status</a>
                        </th>
                       <th style="width: 6%" class="text-center">Actions
                        </th>
                    </tr>
                </thead>
                <tr ng-repeat-start="x in loadOrders">
                    <td rowspan="2" class="top">{{x.ShipmentNumber}}
                        <%= ActiveUser.HasAccessTo(ViewCode.LoadOrder) 
                                        ? string.Format("<a href='{0}?{1}={{{{x.ShipmentNumber}}}}' class='blue' target='_blank' title='Go To Load Order Record'><img src='{2}' alt='' width='16' align='absmiddle'/></a>", 
                                            ResolveUrl(LoadOrderView.PageAddress),
                                            WebApplicationConstants.TransferNumber,
                                            ResolveUrl("~/images/icons2/arrow_newTab.png")) 
                                        : string.Empty %>
                        <img ng-show="x.DatPostingIsExpired" src='<%= ResolveUrl("~/images/icons2/alert.png") %>' width="16" align="absmiddle" title="DAT posting has expired" />
                    </td>
                    <td>{{x.CustomerName}}
                        <%= ActiveUser.HasAccessTo(ViewCode.Customer) 
                            ? string.Format("<a href='{0}?{1}={{{{x.CustomerIdEncrypted}}}}' class='blue' target='_blank' title='Go To Customer Record'><img src='{2}' alt='' width='16' align='absmiddle'/></a>", 
                                ResolveUrl(CustomerView.PageAddress),
                                WebApplicationConstants.TransferNumber,
                                ResolveUrl("~/images/icons2/arrow_newTab.png")) 
                            : string.Empty %>
                    </td>
                    <td>
                        <a href="" ng-click="DisplayModifyPopupForLoadOrder(x.Id, '<%= udpModifyVendor.ClientID %>', '')" class="link_nounderline link_bodytextcolor">{{x.VendorName}}</a>
                        <input type="image" ng-show="x.VendorId == 0" onclick="return false;" ng-click="DisplayModifyPopupForLoadOrder(x.Id, '<%= udpModifyVendor.ClientID %>', '')" src="<%= ResolveUrl("~/images/icons2/downloadDocument.png") %>" align="absmiddle" width="10" />
                        <%= ActiveUser.HasAccessTo(ViewCode.Vendor) 
                            ? string.Format("<a ng-show='{{{{x.VendorId>0}}}}'  href='{0}?{1}={{{{x.VendorIdEncrypted}}}}' class='blue' target='_blank' title='Go To Vendor Record'><img src='{2}' alt='' width='16' align='absmiddle'/></a>", 
                                ResolveUrl(VendorView.PageAddress),
                                WebApplicationConstants.TransferNumber,
                                ResolveUrl("~/images/icons2/arrow_newTab.png")) 
                            : string.Empty %>
                    </td>
                    <td>
                        <a href="" ng-click="DisplayModifyPopupForLoadOrder(x.Id, '<%= udpModifyLocation.ClientID %>', '<%= OriginStopText %>')" class="link_nounderline link_bodytextcolor"
                            ng-style="{cursor: x.HasDatPosting ? 'default' : 'pointer'}">{{x.OriginCity}} {{x.OriginState}} {{x.OriginPostalCode}}</a>
                        <input type="image" ng-show="x.OriginCity == '' && x.OriginState == '' && x.OriginPostalCode == ''" onclick="return false;" ng-click="DisplayModifyPopupForLoadOrder(x.Id, '<%= udpModifyLocation.ClientID %>', '<%= OriginStopText %>')" src="<%= ResolveUrl("~/images/icons2/downloadDocument.png") %>" align="absmiddle" width="10" />
                    </td>
                    <td>
                        <a href="" ng-click="DisplayModifyPopupForLoadOrder(x.Id, '<%= udpModifyLocation.ClientID %>', '<%= DestinationStopText %>')" class="link_nounderline link_bodytextcolor"
                            ng-style="{cursor: x.HasDatPosting ? 'default' : 'pointer'}">{{x.DestinationCity}} {{x.DestinationState}} {{x.DestinationPostalCode}}</a>
                        <input type="image" ng-show="x.DestinationCity == '' && x.DestinationState == '' && x.DestinationPostalCode == ''" onclick="return false;" ng-click="DisplayModifyPopupForLoadOrder(x.Id, '<%= udpModifyLocation.ClientID %>', '<%= DestinationStopText %>')" src="<%= ResolveUrl("~/images/icons2/downloadDocument.png") %>" align="absmiddle" width="10" />
                    </td>
                    <td>
                        <a href="" ng-click="DisplayModifyPopupForLoadOrder(x.Id, '<%= udpModifyDates.ClientID %>', '')" class="link_nounderline link_bodytextcolor">{{x.DesiredPickupDate}}</a>
                        <input type="image" ng-show="x.DesiredPickupDate == ''" onclick="return false;" ng-click="DisplayModifyPopupForLoadOrder(x.Id, '<%= udpModifyDates.ClientID %>', '')" src="<%= ResolveUrl("~/images/icons2/downloadDocument.png") %>" align="absmiddle" width="10" />
                    </td>
                    <td>
                        <a href="" ng-click="DisplayModifyPopupForLoadOrder(x.Id, '<%= udpModifyDates.ClientID %>', '')" class="link_nounderline link_bodytextcolor">{{x.EstimatedDeliveryDate}}</a>
                        <input type="image" ng-show="x.EstimatedDeliveryDate == ''" onclick="return false;" ng-click="DisplayModifyPopupForLoadOrder(x.Id, '<%= udpModifyDates.ClientID %>', '')" src="<%= ResolveUrl("~/images/icons2/downloadDocument.png") %>" align="absmiddle" width="10" />
                    </td>
                    <td class="text-right">
                        <a href="" ng-click="DisplayModifyPopupForLoadOrder(x.Id, '<%= udpModifyItems.ClientID %>', '')" class="link_nounderline link_bodytextcolor"
                            ng-style="{cursor: x.HasDatPosting ? 'default' : 'pointer'}">{{x.TotalWeightAsString}}</a>
                    </td>
                    <td class="text-right">
                        <a href="" ng-click="DisplayModifyPopupForLoadOrder(x.Id, '<%= udpModifyCharges.ClientID %>', '')" class="link_nounderline link_bodytextcolor">{{x.TotalAmountDueAsString}}</a>
                    </td>
                    <td>
                        <a href="" ng-click="DisplayModifyPopupForLoadOrder(x.Id, '<%= udpModifyEquipment.ClientID %>', '')" class="link_nounderline link_bodytextcolor">{{x.Equipments.replace(",","")}}</a>
                        <input type="image" ng-show="x.Equipments == ''" onclick="return false;" ng-click="DisplayModifyPopupForLoadOrder(x.Id, '<%= udpModifyEquipment.ClientID %>', '')" src="<%= ResolveUrl("~/images/icons2/downloadDocument.png") %>" align="absmiddle" width="10" />
                    </td>
                    <td>
                        <a href="" ng-click="DisplayModifyPopupForLoadOrder(x.Id, '<%= udpModifyLoadOrderStatus.ClientID %>', '')" class="link_nounderline link_bodytextcolor">{{x.StatusText}}</a>
                    </td>

                    <td class="text-center">
                        <img ng-show="x.HasDatPosting" src='<%= ResolveUrl("~/images/icons2/DATIcon.png") %>' width="32" align="absmiddle" title="Modify DAT Posting"
                            ng-style="{cursor: 'pointer'}" onclick="return false;" ng-click="DisplayModifyPopupForLoadOrder(x.Id, '<%= udpManageAssetsOnLoadboard.ClientID %>', '')" />
                        <img ng-show="!x.HasDatPosting" src='<%= ResolveUrl("~/images/icons2/DATIconBlack.png") %>' width="32" align="absmiddle" title="Post To DAT"
                            ng-style="{cursor: 'pointer'}" onclick="return false;" ng-click="DisplayModifyPopupForLoadOrder(x.Id, '<%= udpManageAssetsOnLoadboard.ClientID %>', '')" />
                    </td>

                </tr>
                <tr class="hidden">
                    <td></td>
                </tr>
                <tr ng-repeat-end class="f9">
                    <td colspan="11" class="pt0 forceLeftBorder">
                        <div class="rowgroup mb0 ml20">
                            <div class="row">
                                <div class="fieldgroup red">
                                    <a href="" ng-click="DisplayModifyPopupForLoadOrder(x.Id, '<%= udpModifyLoadOrderDescription.ClientID %>', '')" class="link_nounderline link_bodytextcolor">{{x.Description}}</a>
                                    <input type="image" ng-show="x.Description == ''" onclick="return false;" ng-click="DisplayModifyPopupForLoadOrder(x.Id, '<%= udpModifyLoadOrderDescription.ClientID %>', '')" src="<%= ResolveUrl("~/images/icons2/downloadDocument.png") %>" align="absmiddle" width="10" />
                                </div>
                                <div class="fieldgroup right pr20 top">
                                    <label class="blue w40 lhInherit">Mileage:</label>
                                    <label class="w50 lhInherit text-right">{{x.Mileage}}</label>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>

        <asp:UpdatePanel runat="server" ID="udpModifyLocation" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlModifyLocation" CssClass="popup popupControlOverW500 hidden" DefaultButton="btnDoneModifyingLocation">
                    <div class="popheader">
                        <h4>
                            <asp:Literal runat="server" ID="litModifyLocationTitle" />
                        </h4>
                        <asp:ImageButton runat="server" ID="ibtnCloseModifyLocation" OnClick="OnCloseModifyLocationClicked" CssClass="close" ImageUrl="~/images/icons2/close.png" OnClientClick="ShowProcessingDivBlock();" />
                    </div>
                    <div class="pl10 red pt10 pb10">
                        <asp:Literal runat="server" ID="litLocationErrorMessages" />
                    </div>
                    <asp:Panel runat="server" ID="pnlModifyLocationBody">
                        <div class="row">
                            <div class="pl40 pr30">
                                <eShip:OperationsAddressInputControl ID="oaiModifyLocation" runat="server"
                                    OnFindPostalCode="OnAddressInputFindPostalCode" OnAddAddressFromAddressBook="OnAddressInputAddAddressFromAddressBook" />
                            </div>
                        </div>
                        <div class="row pb10">
                            <div class="fieldgroup right">
                                <asp:Button runat="server" ID="btnDoneModifyingLocation" OnClick="OnDoneModifyingLocationClicked" OnClientClick="ShowProcessingDivBlock();" CausesValidation="False" Text="Save" />
                                <asp:Button runat="server" ID="btnCloseModifyingLocation" OnClick="OnCloseModifyLocationClicked" OnClientClick="ShowProcessingDivBlock();" CausesValidation="False" Text="Cancel" />
                            </div>
                        </div>
                    </asp:Panel>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="oaiModifyLocation" />
            </Triggers>
        </asp:UpdatePanel>

        <asp:UpdatePanel runat="server" ID="udpModifyDates" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlModifyDates" CssClass="popup popupControlOverW500" Visible="False" DefaultButton="btnDoneModifyingDates">
                    <div class="popheader">
                        <h4>
                            <asp:Literal runat="server" ID="litModifyDatesTitle" /></h4>
                        <asp:ImageButton runat="server" ID="ibtnCloseModifyDates" OnClick="OnCloseModifyDatesClicked" CssClass="close" ImageUrl="~/images/icons2/close.png" OnClientClick="ShowProcessingDivBlock();" />
                    </div>
                    <asp:Panel runat="server" ID="pnlModifyDatesBody">
                        <table class="poptable">
                            <tr>
                                <td colspan="2" class="red">
                                    <asp:Literal ID="litDatesErrorMessages" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <label class="upper">Desired Pickup Date:</label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox runat="server" ID="txtDesiredPickupDate" placeholder="99/99/9999" Type="Date" />
                                </td>
                            </tr>
                            <tr runat="server" id="trActualPickupDate">
                                <td class="text-right">
                                    <label class="upper">Actual Pickup Date:</label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox runat="server" ID="txtActualPickupDate" placeholder="99/99/9999" Type="Date" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <label class="upper">Estimated Delivery Date:</label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox runat="server" ID="txtEstimatedDeliveryDate" placeholder="99/99/9999" Type="Date" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td class="pt10">
                                    <asp:Button runat="server" ID="btnDoneModifyingDates" OnClick="OnDoneModifyingDatesClicked" OnClientClick="ShowProcessingDivBlock();" CausesValidation="False" Text="Save" />
                                    <asp:Button runat="server" ID="btnCloseModifyingDates" OnClick="OnCloseModifyDatesClicked" OnClientClick="ShowProcessingDivBlock();" CausesValidation="False" Text="Cancel" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>

        <script type="text/javascript">
            $(function () {
                $(jsHelper.AddHashTag('itemsTable [id$="txtEstimatedPcf"]')).each(function () {
                    UpdateDensity(this);
                });

                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                    $(jsHelper.AddHashTag('itemsTable [id$="txtEstimatedPcf"]')).each(function () {
                        UpdateDensity(this);
                    });
                });
            });

            function UpdateDensity(control) {
                var ids = new ControlIds();
                ids.DensityWeight = $(control).parent().parent().find('[id$="txtWeight"]').attr('id');
                ids.DensityLength = $(control).parent().parent().find('[id$="txtLength"]').attr('id');
                ids.DensityWidth = $(control).parent().parent().find('[id$="txtWidth"]').attr('id');
                ids.DensityHeight = $(control).parent().parent().find('[id$="txtHeight"]').attr('id');
                ids.DensityCalc = $(control).parent().parent().find('[id$="txtEstimatedPcf"]').attr('id');
                ids.HidDensityCalc = jsHelper.EmptyString;
                ids.DensityQty = $(control).parent().parent().find('[id$="txtQuantity"]').attr('id');
                CalculateDensity(jsHelper.EnglishUnits, ids);
            }
        </script>
        <asp:UpdatePanel runat="server" ID="udpModifyItems" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlModifyItems" CssClass="popup hidden" DefaultButton="btnDoneModifyingItems">
                    <div class="popheader">
                        <h4>
                            <asp:Literal runat="server" ID="litModifyItemsTitle" />
                        </h4>
                        <asp:ImageButton runat="server" ID="ibtnCloseModifyItems" OnClick="OnCloseModifyItemsClicked" CssClass="close" ImageUrl="~/images/icons2/close.png" OnClientClick="ShowProcessingDivBlock();" />
                    </div>
                    <div class="pl10 red pt10 pb10">
                        <asp:Literal runat="server" ID="litItemsErrorMessages" />
                    </div>
                    <asp:Panel runat="server" ID="pnlModifyItemsBody">
                        <div class="row mb10">
                            <div class="fieldgroup right">
                                <asp:Button runat="server" ID="btnAddItem" Text="Add Item" CssClass="mr10" CausesValidation="false" OnClick="OnAddItemClicked" OnClientClick="ShowProcessingDivBlock();" />
                                <asp:Button runat="server" ID="btnAddLibraryItem" Text="Add Library Item" CssClass="mr10" CausesValidation="false" OnClick="OnAddLibraryItemClicked" />
                                <asp:Button runat="server" ID="btnClearItems" Text="Clear Items" CausesValidation="False" OnClick="OnClearItemsClicked" OnClientClick="ShowProcessingDivBlock();" />
                            </div>
                        </div>
                        <table class="stripe" id="itemsTable">
                            <tr>
                                <th style="width: 17%;">Package Type
                                </th>
                                <th style="width: 7%;" class="text-center">Quantity
                                </th>
                                <th style="width: 9%;">Weight<abbr title="Pounds">(lb)</abbr>
                                </th>
                                <th style="width: 9%;">Length(in)
                                </th>
                                <th style="width: 9%;">Width(in)
                                </th>
                                <th style="width: 9%;">Height(in)
                                </th>
                                <th style="width: 8%;" class="text-center">
                                    <abbr title="Actual Pounds per Cubic Feet">Pcf</abbr>
                                </th>
                                <th style="width: 8%;" class="text-center">Stackable
                                </th>
                                <th style="width: 8%;" class="text-center">Pieces
                                </th>
                                <th style="width: 8%">Value
                                </th>
                                <th style="width: 8%;" class="text-center">Action
                                </th>
                            </tr>
                            <asp:ListView ID="lstItems" runat="server" ItemPlaceholderID="itemPlaceHolder" OnDataBinding="OnItemsDataBinding" OnDataBound="OnItemsDataBound" OnItemDataBound="OnItemsItemDataBound">
                                <LayoutTemplate>
                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <eShip:CustomHiddenField ID="hidItemIndex" runat="server" Value='<%# Container.DataItemIndex%>' />
                                            <eShip:CustomHiddenField ID="hidItemId" runat="server" Value='<%# Eval("Id")%>' />
                                            <eShip:CachedObjectDropDownList runat="server" ID="ddlPackageType" CssClass="w130" Type="PackageType"
                                                EnableChooseOne="True" DefaultValue="0" SelectedValue='<%# Eval("PackageTypeId") %>' />
                                            <asp:ImageButton runat="server" ID="imgSetPackageDimensions" ImageUrl="~/images/icons2/cog.png" Width="16px"
                                                CausesValidation="False" ToolTip="Set Package Type Dimensions" OnClick="OnSetPackageDimensionsClicked" OnClientClick="ShowProcessingDivBlock();" />
                                        </td>
                                        <td class="text-center">
                                            <eShip:CustomTextBox ID="txtQuantity" runat="server" Text='<%# Eval("Quantity")%>' CssClass="w55" onblur="UpdateDensity(this);" Type="NumbersOnly" />
                                        </td>

                                        <td>
                                            <eShip:CustomTextBox ID="txtWeight" runat="server" Text='<%# Eval("ActualWeight")%>' CssClass="w70" onblur="UpdateDensity(this);" Type="FloatingPointNumbers" />
                                        </td>
                                        <td>
                                            <eShip:CustomTextBox ID="txtLength" runat="server" Text='<%# Eval("ActualLength")%>' CssClass="w70" onblur="UpdateDensity(this);" Type="FloatingPointNumbers" />
                                        </td>
                                        <td>
                                            <eShip:CustomTextBox ID="txtWidth" runat="server" Text='<%# Eval("ActualWidth")%>' CssClass="w70" onblur="UpdateDensity(this);" Type="FloatingPointNumbers" />
                                        </td>
                                        <td>
                                            <eShip:CustomTextBox ID="txtHeight" runat="server" Text='<%# Eval("ActualHeight")%>' CssClass="w70" onblur="UpdateDensity(this);" Type="FloatingPointNumbers" />
                                        </td>
                                        <td class="text-center">
                                            <eShip:CustomTextBox ID="txtEstimatedPcf" runat="server" CssClass="w55 disabled" ReadOnly="True" />
                                        </td>
                                        <td class="text-center">
                                            <eShip:AltUniformCheckBox runat="server" ID="chkIsStackable" Checked='<%# Eval("IsStackable")%>' />
                                        </td>
                                        <td class="text-center">
                                            <eShip:CustomTextBox ID="txtPieceCount" runat="server" Text='<%# Eval("PieceCount")%>' CssClass="w40" Type="NumbersOnly" />
                                        </td>
                                        <td>
                                            <eShip:CustomTextBox ID="txtItemValue" runat="server" Text='<%# Eval("Value")%>' CssClass="w70" Type="FloatingPointNumbers" />
                                        </td>
                                        <td class="text-center top" rowspan="2">
                                            <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png" OnClientClick="ShowProcessingDivBlock();"
                                                CausesValidation="false" OnClick="OnDeleteItemClicked" />
                                        </td>
                                    </tr>
                                    <tr class="hidden">
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="13" class="pt0 mt0 bottom_shadow">
                                            <div class="row mb10">
                                                <div class="col_1_3">
                                                    <div class="fieldgroup top">
                                                        <label class="wlabel blue pb10">
                                                            Description
                                                    <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDescription" MaxLength="200" TargetControlId="txtDescription" />
                                                        </label>
                                                        <eShip:CustomTextBox ID="txtDescription" runat="server" Text='<%# Eval("Description")%>' TextMode="MultiLine" CssClass="w280" />
                                                    </div>
                                                </div>
                                                <div class="col_1_3">
                                                    <div class="row">
                                                        <div class="fieldgroup">
                                                            <label class="wlabel blue">Comment</label>
                                                            <eShip:CustomTextBox ID="txtComment" runat="server" Text='<%# Eval("Comment")%>' CssClass="w310" MaxLength="50" />
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="fieldgroup mr10">
                                                            <label class="wlabel blue">Pickup</label>
                                                            <asp:DropDownList runat="server" ID="ddlItemPickup" DataTextField="Text" DataValueField="Value" CssClass="w100" />
                                                        </div>
                                                        <div class="fieldgroup">
                                                            <label class="wlabel blue">Delivery</label>
                                                            <asp:DropDownList runat="server" ID="ddlItemDelivery" DataTextField="Text" DataValueField="Value" CssClass="w100" />
                                                        </div>
                                                        <div class="fieldgroup">
                                                                <label class="wlabel blue"><abbr title="Hazardous Material">HazMat</abbr></label>
                                                                <eShip:AltUniformCheckBox runat="server" ID="chkIsHazmat" Checked='<%# Eval("HazardousMaterial") %>'
                                                                     Enabled="False"  />
                                                            </div>
                                                    </div>
                                                </div>
                                                <div class="col_1_3">
                                                    <div class="row">
                                                        <div class="fieldgroup mr10">
                                                            <label class="wlabel blue">NMFC Code</label>
                                                            <eShip:CustomTextBox ID="txtNMFCCode" runat="server" Text='<%# Eval("NMFCCode")%>' CssClass="w130" />
                                                        </div>
                                                        <div class="fieldgroup">
                                                            <label class="wlabel blue">HTS Code</label>
                                                            <eShip:CustomTextBox ID="txtHTSCode" runat="server" Text='<%# Eval("HTSCode")%>' CssClass="w130" />
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="fieldgroup mr10">
                                                            <label class="wlabel blue">Actual Class</label>
                                                            <asp:DropDownList runat="server" ID="ddlActualFreightClass" DataTextField="Text" DataValueField="Value" CssClass="w130" onchange="UncheckAutorated();" />
                                                        </div>
                                                        <div class="fieldgroup">
                                                            <label class="wlabel blue">Rated Class</label>
                                                            <eShip:CustomTextBox runat="server" ID="txtRatedFreightClass" Text='<%# Eval("RatedFreightClass")%>' CssClass="w130 disabled" ReadOnly="True" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:ListView>
                        </table>
                        <div class="row pb10 pt10">
                            <div class="fieldgroup right">
                                <asp:Button runat="server" ID="btnDoneModifyingItems" OnClick="OnDoneModifyingItemsClicked" OnClientClick="ShowProcessingDivBlock();" CausesValidation="False" Text="Save" />
                                <asp:Button runat="server" ID="btnCloseModifyingItems" OnClick="OnCloseModifyItemsClicked" OnClientClick="ShowProcessingDivBlock();" CausesValidation="False" Text="Cancel" />
                            </div>
                        </div>
                    </asp:Panel>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnAddLibraryItem" />
            </Triggers>
        </asp:UpdatePanel>

        <asp:UpdatePanel runat="server" ID="udpModifyCharges" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlModifyCharges" CssClass="popup hidden" DefaultButton="btnDoneModifyingCharges">
                    <div class="popheader">
                        <h4>
                            <asp:Literal runat="server" ID="litModifyChargesTitle" /></h4>
                        <asp:ImageButton runat="server" ID="ibtnCloseModifyCharges" OnClick="OnCloseModifyChargesClicked" CssClass="close" ImageUrl="~/images/icons2/close.png" OnClientClick="ShowProcessingDivBlock();" />
                    </div>
                    <div class="pl10 red pt10 pb10">
                        <asp:Literal runat="server" ID="litChargesErrorMessages" />
                    </div>
                    <asp:Panel runat="server" ID="pnlModifyChargesBody">
                        <div class="row pb10">
                            <div class="fieldgroup right">
                                <asp:Button runat="server" ID="btnAddCharge" Text="Add Charge" CssClass="mr10" CausesValidation="false" OnClick="OnAddChargeClicked" OnClientClick="ShowProcessingDivBlock();" />
                                <asp:Button runat="server" ID="btnClearCharges" Text="Clear Charges" CausesValidation="False" OnClick="OnClearChargesClicked" OnClientClick="ShowProcessingDivBlock();" />
                            </div>
                        </div>
                        <script type="text/javascript">
                            $(function () {
                                $(jsHelper.AddHashTag('chargesTable [id$="txtAmountDue"]')).each(function () {
                                    UpdateChargeInformation(this);
                                });

                                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                                    $(jsHelper.AddHashTag('chargesTable [id$="txtAmountDue"]')).each(function () {
                                        UpdateChargeInformation(this);
                                    });
                                });
                            });

                            function UpdateChargeInformation(control) {
                                var unitSell = Number($(control).parent().parent().find('[id$="txtUnitSell"]').val());
                                var unitDiscount = Number($(control).parent().parent().find('[id$="txtUnitDiscount"]').val());
                                var qty = Number($(control).parent().parent().find('[id$="txtQuantity"]').val());
                                if (qty < 1) qty = 1;
                                $(control).parent().parent().find('[id$="txtAmountDue"]').val(((unitSell - unitDiscount) * qty).toFixed(4));
                            }
                        </script>
                        <table class="stripe" id="chargesTable">
                            <tr>
                                <th style="width: 30%;">Charge Code
                                </th>
                                <th style="width: 20%;">Comment
                                </th>
                                <th style="width: 5%;">
                                    <abbr title="Quantity">Qty</abbr>
                                </th>
                                <th style="width: 10%;" class="text-right">Unit Buy
										    <abbr title="Dollars">($)</abbr>
                                </th>
                                <th style="width: 10%;" class="text-right">Unit Sell
										    <abbr title="Dollars">($)</abbr>
                                </th>
                                <th style="width: 10%;" class="text-right">Unit Discount
										    <abbr title="Dollars">($)</abbr>
                                </th>
                                <th style="width: 10%;" class="text-right">Amount Due
										    <abbr title="Dollars">($)</abbr>
                                </th>
                                <th style="width: 5%;" class="text-center">Action
                                </th>
                            </tr>
                            <asp:ListView ID="lstCharges" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                <LayoutTemplate>
                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <eShip:CachedObjectDropDownList runat="server" ID="ddlChargeCode" CssClass="w260" Type="ChargeCodes"
                                                EnableChooseOne="True" DefaultValue="0" SelectedValue='<%# Eval("ChargeCodeId") %>' />
                                        </td>
                                        <td>
                                            <eShip:CustomTextBox ID="txtComment" runat="server" Text='<%# Eval("Comment")%>' MaxLength="50" CssClass="w260" />
                                        </td>
                                        <td class="text-center">
                                            <eShip:CustomHiddenField ID="hidChargeIndex" runat="server" Value='<%# Container.DataItemIndex%>' />
                                            <eShip:CustomHiddenField ID="hidChargeId" runat="server" Value='<%# Eval("Id")%>' />
                                            <eShip:CustomTextBox ID="txtQuantity" runat="server" Text='<%# Eval("Quantity")%>' CssClass="w40" onblur="UpdateChargeInformation(this);" Type="NumbersOnly" />
                                        </td>
                                        <td class="text-right">
                                            <eShip:CustomTextBox ID="txtUnitBuy" runat="server" Text='<%# Eval("UnitBuy", "{0:f4}")%>' CssClass="w75" onblur="UpdateChargeInformation(this);" Type="FloatingPointNumbers" />
                                        </td>
                                        <td class="text-right">
                                            <eShip:CustomTextBox ID="txtUnitSell" runat="server" Text='<%# Eval("UnitSell", "{0:f4}")%>' CssClass="w75" onblur="UpdateChargeInformation(this);" Type="FloatingPointNumbers" />
                                        </td>
                                        <td class="text-right">
                                            <eShip:CustomTextBox ID="txtUnitDiscount" runat="server" Text='<%# Eval("UnitDiscount", "{0:f4}")%>' CssClass="w75" onblur="UpdateChargeInformation(this);" Type="FloatingPointNumbers" />
                                        </td>
                                        <td class="text-right">
                                            <eShip:CustomTextBox runat="server" ID="txtAmountDue" CssClass="w80 disabled" ReadOnly="True" />
                                        </td>
                                        <td class="text-center">
                                            <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png" OnClientClick="ShowProcessingDivBlock();"
                                                CausesValidation="false" OnClick="OnDeleteChargeClicked" Enabled='<%# Access.Modify%>' />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:ListView>
                        </table>
                        <div class="row pb10 pt10">
                            <div class="fieldgroup right">
                                <asp:Button runat="server" ID="btnDoneModifyingCharges" OnClick="OnDoneModifyingChargesClicked" OnClientClick="ShowProcessingDivBlock();" CausesValidation="False" Text="Save" />
                                <asp:Button runat="server" ID="btnCloseModifyingCharges" OnClick="OnCloseModifyChargesClicked" OnClientClick="ShowProcessingDivBlock();" CausesValidation="False" Text="Cancel" />
                            </div>
                        </div>
                    </asp:Panel>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>

        <script type="text/javascript">
            $(document).ready(function () {
                // enforce mutual exclusion on checkboxes in customerLoginPageSection
                $(jsHelper.AddHashTag('ulEquipmentTypes input:checkbox')).click(function () {
                    if (jsHelper.IsChecked($(this).attr('id'))) {
                        $(jsHelper.AddHashTag('ulEquipmentTypes input:checkbox')).not(jsHelper.AddHashTag($(this).attr('id'))).each(function () {
                            jsHelper.UnCheckBox($(this).attr('id'));
                            SetAltUniformCheckBoxClickedStatus(this);
                        });
                    }
                });

                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                    $(jsHelper.AddHashTag('ulEquipmentTypes input:checkbox')).click(function () {
                        if (jsHelper.IsChecked($(this).attr('id'))) {
                            $(jsHelper.AddHashTag('ulEquipmentTypes input:checkbox')).not(jsHelper.AddHashTag($(this).attr('id'))).each(function () {
                                jsHelper.UnCheckBox($(this).attr('id'));
                                SetAltUniformCheckBoxClickedStatus(this);
                            });
                        }
                    });
                });
            });
        </script>
        <asp:UpdatePanel runat="server" ID="udpModifyEquipment" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="pnlModifyEquipment" runat="server" CssClass="popup popupControlOverW750 popup-position-fixed top50" Visible="False" DefaultButton="btnDoneModifyingEquipment">
                    <div class="popheader">
                        <h4>
                            <asp:Literal runat="server" ID="litModifyEquipmentTitle" /></h4>
                        <asp:ImageButton runat="server" ID="ibtnCloseModifyEquipment" OnClick="OnCloseModifyEquipmentClicked" CssClass="close" ImageUrl="~/images/icons2/close.png" OnClientClick="ShowProcessingDivBlock();" />
                    </div>
                    <div class="pl10 red pt10 pb10">
                        <asp:Literal runat="server" ID="litEquipmentErrorMessages" />
                    </div>
                    <asp:Panel runat="server" ID="pnlModifyEquipmentBody">
                        <div class="row finderScroll">
                            <div class="fieldgroup pl20 pr20 pt20">
                                <asp:Repeater runat="server" ID="rptEquipmentTypes">
                                    <HeaderTemplate>
                                        <ul class="twocol_list sm_chk" id="ulEquipmentTypes">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <li>
                                            <eShip:AltUniformCheckBox runat="server" ID="chkSelected" />
                                            <eShip:CustomHiddenField runat="server" ID="hidEquipmentTypeId" Value='<%# Eval("Value")%>' />
                                            <label class="lhInherit">
                                                <asp:Label runat="server" ID="lblEquipmentTypeCode" Text='<%# Eval("Text")%>' CssClass="fs85em" />
                                            </label>
                                        </li>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </ul>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                        <div class="row">
                            <div class="fieldgroup pb10 pt10 right">
                                <asp:Button runat="server" ID="btnDoneModifyingEquipment" OnClick="OnDoneModifyingEquipmentClicked" OnClientClick="ShowProcessingDivBlock();" CausesValidation="False" Text="Save" />
                                <asp:Button runat="server" ID="btnCloseModifyingEquipment" OnClick="OnCloseModifyEquipmentClicked" OnClientClick="ShowProcessingDivBlock();" CausesValidation="False" Text="Cancel" />
                            </div>
                        </div>
                    </asp:Panel>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdatePanel runat="server" ID="udpModifyVendor" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlModifyVendor" CssClass="popup popupControlOverW500" Visible="False" DefaultButton="btnDoneModifyingVendor">
                    <div class="popheader">
                        <h4>
                            <asp:Literal runat="server" ID="litModifyVendorTitle" />
                        </h4>
                        <asp:ImageButton runat="server" ID="ibtnCloseModifyVendor" OnClick="OnCloseModifyVendorClicked" CssClass="close" ImageUrl="~/images/icons2/close.png" OnClientClick="ShowProcessingDivBlock();" />
                    </div>
                    <div class="pl10 red pt10 pb10">
                        <asp:Literal runat="server" ID="litVendorErrorMessages" />
                    </div>
                    <asp:Panel runat="server" ID="pnlModifyVendorBody">
                        <table class="poptable">
                            <tr>
                                <td class="text-right">
                                    <label class="upper">
                                        Primary Vendor: 
                                        <%= hidPrimaryVendorId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.Vendor)
                                                    ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Vendor Record'><img src={3} width='20' class='middle' /></a>",
                                                                            ResolveUrl(VendorView.PageAddress),
                                                                            WebApplicationConstants.TransferNumber,
                                                                            hidPrimaryVendorId.Value.UrlTextEncrypt(),
                                                                            ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                    : string.Empty%>
                                    </label>
                                </td>
                                <td>
                                    <eShip:CustomHiddenField runat="server" ID="hidPrimaryVendorId" />
                                    <eShip:CustomHiddenField runat="server" ID="hidPrimaryVendorRelationId" />
                                    <asp:Panel runat="server" DefaultButton="imgVendorSearch">
                                        <eShip:CustomTextBox runat="server" ID="txtVendorNumber" CssClass="w80" OnTextChanged="OnVendorNumberEntered" AutoPostBack="True" />
                                        <asp:ImageButton ID="imgVendorSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png" CausesValidation="False" OnClick="OnVendorSearchClicked" />
                                        <eShip:CustomTextBox runat="server" ID="txtVendorName" CssClass="w220 disabled" ReadOnly="True" />
                                        <ajax:AutoCompleteExtender runat="server" ID="aceVendor" TargetControlID="txtVendorNumber" ServiceMethod="GetVendorList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                            EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                        <div class="row pb10 pt10">
                            <div class="fieldgroup right">
                                <asp:Button runat="server" ID="btnDoneModifyingVendor" OnClick="OnDoneModifyingVendorClicked" OnClientClick="ShowProcessingDivBlock();" CausesValidation="False" Text="Save" />
                                <asp:Button runat="server" ID="btnCloseModifyingVendor" OnClick="OnCloseModifyVendorClicked" OnClientClick="ShowProcessingDivBlock();" CausesValidation="False" Text="Cancel" />
                            </div>
                        </div>
                    </asp:Panel>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="imgVendorSearch" />
            </Triggers>
        </asp:UpdatePanel>

        <asp:UpdatePanel runat="server" ID="udpModifyLoadOrderStatus" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlModifyLoadOrderStatus" CssClass="popup popupControlOverW500" Visible="False" DefaultButton="btnDoneModifyingLoadOrderStatus">
                    <div class="popheader">
                        <h4>
                            <asp:Literal runat="server" ID="litModifyLoadOrderStatusTitle" /></h4>
                        <asp:ImageButton runat="server" ID="ibtnCloseModifyLoadOrderStatus" OnClick="OnCloseModifyLoadOrderStatusClicked" CssClass="close" ImageUrl="~/images/icons2/close.png" OnClientClick="ShowProcessingDivBlock();" />
                    </div>
                    <div class="pl10 red pt10 pb10">
                        <asp:Literal runat="server" ID="litLoadOrderStatusErrorMessages" />
                    </div>
                    <asp:Panel runat="server" ID="pnlModifyLoadOrderStatusBody">
                        <table class="poptable">
                            <tr>
                                <td class="text-right">
                                    <label class="upper">Load Order Status: </label>
                                </td>
                                <td>
                                    <asp:DropDownList runat="server" ID="ddlLoadOrderStatus" DataTextField="Text" DataValueField="Value" />
                                </td>
                            </tr>
                        </table>
                        <div class="row">
                            <div class="fieldgroup pb10 pt10 right">
                                <asp:Button runat="server" ID="btnDoneModifyingLoadOrderStatus" OnClick="OnDoneModifyingLoadOrderStatusClicked" OnClientClick="ShowProcessingDivBlock();" CausesValidation="False" Text="Save" />
                                <asp:Button runat="server" ID="btnCloseModifyingLoadOrderStatus" OnClick="OnCloseModifyLoadOrderStatusClicked" OnClientClick="ShowProcessingDivBlock();" CausesValidation="False" Text="Cancel" />
                            </div>
                        </div>
                    </asp:Panel>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdatePanel runat="server" ID="udpModifyLoadOrderDescription" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlModifyLoadOrderDescription" CssClass="popup popupControlOverW500" Visible="False" DefaultButton="btnDoneModifyingLoadOrderDescription">
                    <div class="popheader">
                        <h4>
                            <asp:Literal runat="server" ID="litModifyLoadOrderDescriptionTitle" /></h4>
                        <asp:ImageButton runat="server" ID="ibtnCloseModifyLoadOrderDescription" OnClick="OnCloseModifyLoadOrderDescriptionClicked" CssClass="close" ImageUrl="~/images/icons2/close.png" OnClientClick="ShowProcessingDivBlock();" />
                    </div>
                    <div class="pl10 red pt10 pb10">
                        <asp:Literal runat="server" ID="litLoadOrderDescriptionErrorMessages" />
                    </div>
                    <asp:Panel runat="server" ID="pnlModifyLoadOrderDescriptionBody">
                        <table class="poptable">
                            <tr>
                                <td class="text-right top">
                                    <label class="upper">
                                        Loadboard Comments: 
                                    </label>
                                    <br />
                                    <label class="lhInherit">
                                        <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDescription" TargetControlId="txtLoadOrderDescription" MaxLength="250" />
                                    </label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox ID="txtLoadOrderDescription" runat="server" TextMode="MultiLine" CssClass="w300 h150" />
                                </td>
                            </tr>
                        </table>
                        <div class="row">
                            <div class="fieldgroup pb10 pt10 right">
                                <asp:Button runat="server" ID="btnDoneModifyingLoadOrderDescription" OnClick="OnDoneModifyingLoadOrderDescriptionClicked" OnClientClick="ShowProcessingDivBlock();" CausesValidation="False" Text="Save" />
                                <asp:Button runat="server" ID="btnCloseModifyingLoadOrderDescription" OnClick="OnCloseModifyLoadOrderDescriptionClicked" OnClientClick="ShowProcessingDivBlock();" CausesValidation="False" Text="Cancel" />
                            </div>
                        </div>
                    </asp:Panel>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel runat="server" ID="udpManageAssetsOnLoadboard">
            <ContentTemplate>
                <asp:UpdatePanel runat="server" ID="udpModifyLoadOrderDatPosting" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel runat="server" ID="pnlModifyLoadOrderDatPosting" CssClass="popup popupControlOverW500" Visible="False" DefaultButton="btnDoneModifyingVendor">
                            <div class="popheader">
                                <h4>
                                    <asp:Literal runat="server" ID="litModifyDatLoadOrder" />
                                </h4>
                                <asp:ImageButton runat="server" ID="ImageButton1" OnClick="OnCloseDatAssetClicked" CssClass="close" ImageUrl="~/images/icons2/close.png" OnClientClick="ShowProcessingDivBlock();" />
                            </div>
                            <asp:Panel runat="server" ID="pnlModifyLoadOrderDatPostingBody">
                                <table class="poptable">
                                    <tr>
                                        <td class="text-right">
                                            <label class="upper">
                                                DAT Asset Id:
                                            </label>
                                        </td>
                                        <td>
                                            <eShip:CustomTextBox runat="server" ID="txtAssetId" CssClass="w200 disabled" ReadOnly="True" />
                                            <asp:Button runat="server" ID="btnModifyDatAssetForLoadOrder" CausesValidation="False" OnClick="OnModifyDatAssetClicked" Text="Modify DAT Asset" />
                                        </td>
                                    </tr>
                                </table>
                                <div class="row pb10 pt10">
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="Button2" OnClick="OnCloseDatAssetClicked" OnClientClick="ShowProcessingDivBlock();" CausesValidation="False" Text="Cancel" />
                                    </div>
                                </div>
                            </asp:Panel>
                        </asp:Panel>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnModifyDatAssetForLoadOrder" />
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdatePanel runat="server" ID="udpHiddenFields" UpdateMode="Always">
            <ContentTemplate>
                <eShip:CustomHiddenField runat="server" ID="hidEditingRecordPanelClientId" />
                <eShip:CustomHiddenField runat="server" ID="hidEditLoadOrderId" />
                <eShip:CustomHiddenField runat="server" ID="hidEditShipmentId" />

                <eShip:CustomHiddenField runat="server" ID="hidCurrentScrollPosition" />
                <eShip:CustomHiddenField runat="server" ID="hidGoToCurrentScrollPosition" />

                <eShip:CustomHiddenField runat="server" ID="hidShipmentSortByField" Value="ShipmentNumber" />
                <eShip:CustomHiddenField runat="server" ID="hidShipmentSortReverse" />
                <eShip:CustomHiddenField runat="server" ID="hidLoadOrderSortByField" />
                <eShip:CustomHiddenField runat="server" ID="hidLoadOrderSortReverse" />
                <eShip:CustomHiddenField runat="server" ID="hidDatAssetExists" />

                <eShip:CustomHiddenField runat="server" ID="hidUserId" />
                <eShip:CustomHiddenField runat="server" ID="hidFlag" />
                <div id="pnlClientSideDimScreen" class="dimBackground" runat="server" style="display: none;"></div>
                <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />

            </ContentTemplate>
        </asp:UpdatePanel>


        <div id="pnlClientSideDimScreenControl" class="dimBackgroundControl" runat="server" style="display: none;"></div>
        <eShip:CustomHiddenField runat="server" ID="hidAreFiltersShowing" Value="true" />
        <eShip:CustomHiddenField runat="server" ID="hidRecordTypeShowing" />


        <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
    </div>

</asp:Content>
