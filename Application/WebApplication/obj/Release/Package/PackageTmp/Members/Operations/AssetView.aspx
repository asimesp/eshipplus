﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="AssetView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.AssetView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        OnNew="OnNewClick" OnImport="OnImportClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" ImageUrl="" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Assets<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" Text="" /></small>
            </h3>
        </div>

        <hr class="dark mb10" />

        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="Assets" ShowAutoRefresh="False"
                            OnProcessingError="OnSearchProfilesProcessingError" OnSearchProfileSelected="OnSearchProfilesProfileSelected" />
                    </div>
                </div>
            </div>

            <hr class="dark mb10" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                        OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>

            <hr class="fat" />
            <div class="rowgroup">
                <div class="row mb10">
                    <div class="left">
                        <asp:Button ID="btnSearch" CssClass="mr10" runat="server" Text="SEARCH" OnClick="OnSearchClicked"
                            CausesValidation="False" />
                        <asp:Button ID="btnExport" runat="server" Text="EXPORT" OnClick="OnExportClicked" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheAssetTable" TableId="assetTable" HeaderZIndex="2"/>
        <div class="rowgroup">
            <table class="line2 pl2" id="assetTable">
                <tr>
                    <th style="width: 10%;">Asset Number
                    </th>
                    <th style="width: 15%;">Asset Type
                    </th>
                    <th style="width: 50%;">Description
                    </th>
                    <th style="width: 12%;">Date Created
                    </th>
                    <th style="width: 5%;" class="text-center">Active
                    </th>
                    <th style="width: 8%;" class="text-center">Action
                    </th>
                </tr>
                <asp:ListView ID="lstAssets" runat="server" ItemPlaceholderID="itemPlaceHolder">
                    <LayoutTemplate>
                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <%# Eval("AssetNumber") %>
                                <eShip:CustomHiddenField ID="hidAssetId" runat="server" Value='<%# Eval("Id") %>' />
                            </td>
                            <td>
                                <%# ddlAssetType.GetValueText(Eval("AssetType").ToInt().GetString())%>
                            </td>
                            <td>
                                <%# Eval("Description")%>
                            </td>
                            <td>
                                <%# Eval("DateCreated").FormattedShortDate()%>
                            </td>
                            <td class="text-center">
                                <%# Eval("Active").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                            </td>
                            <td class="text-center">
                                <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png"
                                    ToolTip="edit" CausesValidation="false" OnClick="OnEditClicked" Enabled='<%# Access.Modify %>' />
                                <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                    ToolTip="delete" CausesValidation="false" OnClick="OnDeleteClicked" Enabled='<%# Access.Remove %>' />
                                <ajax:ConfirmButtonExtender runat="server" ID="cbeDelete" TargetControlID="ibtnDelete"
                                    ConfirmText="Are you sure you want to delete record?" />
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </table>
        </div>

        <asp:Panel ID="pnlEdit" runat="server" Visible="false" DefaultButton="btnSave">
            <div class="popup popupControlOverW500">
                <div class="popheader">
                    <h4>Add/Modify Asset
                    </h4>
                    <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                        CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
                </div>
                <div class="row">
                    <table class="poptable">
                        <tr>
                            <td colspan="20" class="red">
                                <asp:Literal ID="litMessage" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Date Created</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox runat="server" ID="txtDateCreated" CssClass="w200 disabled" ReadOnly="True" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Asset Number</label>
                            </td>
                            <td>
                                <eShip:CustomHiddenField ID="hidEditAssetId" runat="server" />
                                <eShip:CustomTextBox ID="txtAssetNumber" runat="server" MaxLength="50" ReadOnly="true" CssClass="w200 disabled" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Asset Type</label>
                            </td>
                            <td>
                                <asp:DropDownList DataTextField="Value" DataValueField="Key" ID="ddlAssetType" runat="server" CssClass="w200" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right top">
                                <label class="upper">Description</label>
                                <br />
                                <label class="lhInherit">
                                    <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDescription" TargetControlId="txtDescription" MaxLength="100" />
                                </label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtDescription" runat="server" TextMode="MultiLine" CssClass="w300" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:CheckBox ID="chkActive" runat="server" CssClass="jQueryUniform" />
                                <label>Active</label>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="pt10">
                                <asp:Button ID="btnSave" Text="Save" OnClick="OnSaveClick" runat="server"/>
                                <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnCloseClicked" runat="server" CausesValidation="false" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>
    <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information"
        OnOkay="OnOkayProcess" />
    <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
        OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
</asp:Content>
