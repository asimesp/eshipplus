﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="CarrierPreferredLaneView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.CarrierPreferredLaneView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        OnNew="OnNewClick" OnImport="OnImportClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Carrier Preferred Lanes<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" Text="" /></small>
            </h3>
        </div>

        <hr class="dark mb10" />



        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="CarrierPreferredLanes" ShowAutoRefresh="False"
                            OnProcessingError="OnSearchProfilesProcessingError" OnSearchProfileSelected="OnSearchProfilesProfileSelected" />
                    </div>
                </div>
            </div>
            <hr class="dark mb10" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                        OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>

            <hr class="fat" />
            <div class="rowgroup">
                <div class="row mb10">
                    <div class="left">
                        <asp:Button ID="btnSearch" CssClass="mr10" runat="server" Text="SEARCH" OnClick="OnSearchClicked"
                            CausesValidation="False" />
                        <asp:Button ID="btnExport" runat="server" Text="EXPORT" OnClick="OnExportClicked" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:UpdatePanel runat="server" ID="upCarrierPreferredLane">
            <ContentTemplate>
                <eShip:ContinuousScrollExtender ID="upcseDataUpdate" runat="server" BindableControlId="lvwPreferredLanes" UpdatePanelToExtendId="upCarrierPreferredLane" />
                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheCarrierPreferredLaneTable" TableId="carrierPreferredLaneTable" HeaderZIndex="2" />
                <div class="rowgroup">
                    <table class="line2 pl2" id="carrierPreferredLaneTable">
                        <tr>
                            <th style="width: 10%;">Vendor
                            </th>
                            <th style="width: 11%;">Origin City
                            </th>
                            <th style="width: 11%;">Origin State
                            </th>
                            <th style="width: 8%;">Origin Postal Code
                            </th>
                            <th style="width: 10%;">Origin Country
                            </th>
                            <th style="width: 11%;">Destination City
                            </th>
                            <th style="width: 11%;">Destination State
                            </th>
                            <th style="width: 8%;">Destination Postal Code
                            </th>
                            <th style="width: 10%;">Destination Country
                            </th>
                            <th style="width: 12%;" class="text-center">Action
                            </th>
                        </tr>
                        <asp:ListView ID="lvwPreferredLanes" runat="server" ItemPlaceholderID="itemPlaceHolder">
                            <LayoutTemplate>
                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="top">
                                        <%# string.Format("{0} - {1}", Eval("VendorNumber"),Eval("VendorName"))%>
                                        <%# ActiveUser.HasAccessTo(ViewCode.Vendor) 
                                                        ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Vendor Record'><img src={3} width='16' class='middle'/></a>", 
                                                            ResolveUrl(VendorView.PageAddress),
                                                            WebApplicationConstants.TransferNumber, 
                                                            Eval("VendorId").GetString().UrlTextEncrypt(),
                                                            ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                        : string.Empty %>
                                    </td>
                                    <td class="top">
                                        <eShip:CustomHiddenField ID="hidCarrierPrefferedLaneId" runat="server" Value='<%# Eval("Id") %>' />
                                        <%# Eval("OriginCity")%>
                                    </td>
                                    <td class="top">
                                        <%# Eval("OriginState") %>
                                    </td>
                                    <td class="top">
                                        <%# Eval("OriginPostalCode") %>
                                    </td>
                                    <td class="top">
                                        <%# Eval("OriginCountryName")%>
                                    </td>
                                    <td class="top">
                                        <%# Eval("DestinationCity")%>
                                    </td>
                                    <td class="top">
                                        <%# Eval("DestinationState")%>
                                    </td>
                                    <td class="top">
                                        <%# Eval("DestinationPostalCode")%>
                                    </td>
                                    <td class="top">
                                        <%#Eval("DestinationCountryName") %>
                                    </td>
                                    <td class="text-center top">
                                        <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png"
                                            CausesValidation="false" OnClick="OnEditClicked" Enabled='<%# Access.Modify %>' />
                                        <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                            CausesValidation="false" OnClick="OnDeleteClicked" Enabled='<%# Access.Remove %>'
                                            OnClientClick="if (!confirm('Are you sure you want to delete record?')) return false;" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lvwPreferredLanes" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:Panel ID="pnlEditCarrierPreferredLane" CssClass="popup popupControlOverW750" runat="server" Visible="false">
            <div class="popheader">
                <h4>
                    <asp:Literal ID="litTitle" runat="server" />
                </h4>
                <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                    CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
            </div>
            <div class="row mt20">
                <table class="poptable center">
                    <tr>
                        <td colspan="20" class="red">
                            <asp:Literal ID="litMessage" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="upper">Vendor: </label>
                            <eShip:CustomHiddenField ID="hidEditVendorId" runat="server" />
                            <eShip:CustomTextBox runat="server" ID="txtVendorNumber" CssClass="w80" OnTextChanged="OnVendorNumberEntered"
                                AutoPostBack="true" />
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                                CausesValidation="false" OnClick="OnVendorSearchClicked" />
                            <ajax:AutoCompleteExtender runat="server" ID="aceVendor" TargetControlID="txtVendorNumber" ServiceMethod="GetVendorList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                            <eShip:CustomTextBox runat="server" ID="txtVendorName" CssClass="w200 disabled" ReadOnly="true" />
                        </td>
                    </tr>
                </table>
            </div>
            <div class="row mt10">
                <div class="col_1_2 bbox vlinedarkright pl20">
                    <div class="row">
                        <h5 class="pl40">Origin</h5>
                        <table class="poptable">
                            <tr>
                                <td class="text-right">
                                    <label class="upper">City:</label>
                                    <eShip:CustomHiddenField ID="hidEditCarrierPreferredLaneId" runat="server" />
                                </td>
                                <td>
                                    <eShip:CustomTextBox ID="txtEditOriginCity" CssClass="w200" MaxLength="50" runat="server" /></td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <label class="upper">State:</label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox ID="txtEditOriginState" CssClass="w200" MaxLength="50" runat="server" /></td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <label class="upper">Postal Code:</label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox ID="txtEditOriginPostalCode" CssClass="w200" MaxLength="10" runat="server" /></td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <label class="upper">Country:</label>
                                </td>
                                <td>
                                    <eShip:CachedObjectDropDownList Type="Countries" ID="ddlEditOriginCountries" runat="server" CssClass="w200" EnableChooseOne="True" DefaultValue="0" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col_1_2 bbox pl40">
                    <div class="row">
                        <h5 class="pl20">Destination</h5>
                        <table class="poptable">
                            <tr>
                                <td class="text-right">
                                    <label class="upper">City:</label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox ID="txtEditDestCity" runat="server" MaxLength="50" CssClass="w200" /></td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <label class="upper">State:</label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox ID="txtEditDestState" runat="server" MaxLength="50" CssClass="w200" /></td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <label class="upper">Postal Code:</label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox ID="txtEditDestPostalCode" CssClass="w200" MaxLength="10" runat="server" /></td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <label class="upper">Country:</label>
                                </td>
                                <td>
                                    <eShip:CachedObjectDropDownList Type="Countries" ID="ddlEditDestCountries" runat="server" CssClass="w200" EnableChooseOne="True" DefaultValue="0" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row mt20">
                <div class="row">
                    <table class="poptable center">
                        <tr>
                            <td class="pt10">
                                <asp:Button ID="btnSave" Text="Save" OnClick="OnSaveClick" runat="server" />
                                <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnCloseClicked" runat="server"
                                    CausesValidation="false" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>
    <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information"
        OnOkay="OnOkayProcess" />
    <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
        OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
    <eShip:VendorFinderControl ID="vendorFinder" runat="server" OnItemSelected="OnVendorFinderItemSelected" EnableMultiSelection="False"
        OnSelectionCancel="OnVendorFinderSelectionCancelled" Visible="false" OpenForEditEnabled="false" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
</asp:Content>
