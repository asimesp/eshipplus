﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="LoadOrderView.aspx.cs" EnableEventValidation="false"
    Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.LoadOrderView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core.Registry" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Operations" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>
<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        ShowEdit="true" ShowDelete="true" ShowSave="true" ShowFind="true" ShowMore="true"
        OnFind="OnToolbarFindClicked" OnSave="OnToolbarSaveClicked" OnDelete="OnToolbarDeleteClicked"
        OnNew="OnToolbarNewClicked" OnEdit="OnToolbarEditClicked" OnCommand="OnToolbarCommand" OnUnlock="OnToolbarUnlockClicked" />

</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Load Orders</h3>
            <div class="shipmentAlertSection">
                <asp:Panel ID="pnlCreditAlert" runat="server" CssClass="flag-shipment" Visible="false">
                    Credit Alert
                </asp:Panel>
                <asp:Panel ID="pnlPrimaryVendorInsuranceAlert" runat="server" CssClass="flag-shipment" Visible="false">
                    <asp:Literal runat="server" ID="litPrimaryVendorInsuranceAlert" Text="Vendor Insurance" />
                </asp:Panel>
                <asp:Panel ID="pnlCashOnlyCustomer" runat="server" CssClass="flag-shipment" Visible="false">
                    Cash-Only Customer
                </asp:Panel>
                <asp:Panel ID="pnlDATAssetPosted" runat="server" CssClass="flag-shipment" Visible="false">
                    Posted To DAT
                </asp:Panel>
                <asp:Panel ID="pnlDATAssetExpired" runat="server" CssClass="flag-shipment" Visible="false">
                    DAT Posting Expired
                </asp:Panel>
                <asp:Panel ID="pnlTruckloadBid" runat="server" CssClass="flag-shipment" Visible="false">
                    Truckload Bid Initiated
                </asp:Panel>
	            <asp:Panel ID="pnlGuaranteedDeliveryServices" runat="server" CssClass="flag-shipment" Visible="False">
		            Guaranteed Delivery Services
	            </asp:Panel>
            </div>
            <div class="lockMessage">
                <asp:Literal ID="litMessage" runat="server" />
            </div>
        </div>

        <hr class="dark mb5" />

        <div class="errorMsgLit">

            <asp:UpdatePanel runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <asp:Literal runat="server" ID="litErrorMessages" />
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>

        <eShip:LoadOrderFinderControl runat="server" ID="loadOrderFinder" Visible="false" OnItemSelected="OnLoadOrderFinderItemSelected"
            OnSelectionCancel="OnLoadOrderFinderItemCancelled" OpenForEditEnabled="true" EnableMultiSelection="False" />
        <eShip:ShipmentFinderControl runat="server" ID="shipmentFinder" Visible="false" OnItemSelected="OnShipmentFinderItemSelected"
            OnSelectionCancel="OnShipmentFinderItemCancelled" OpenForEditEnabled="false" EnableMultiSelection="False" />
        <eShip:AddressBookFinderControl runat="server" ID="addressBookFinder" Visible="false" OnItemSelected="OnAddressBookFinderItemSelected"
            OnSelectionCancel="OnAddressBookFinderItemCancelled" OpenForEditEnabled="false" />
        <eShip:CustomerFinderControl ID="customerFinder" runat="server" EnableMultiSelection="false"
            OnlyActiveResults="True" Visible="False" OpenForEditEnabled="false" OnSelectionCancel="OnCustomerFinderSelectionCancelled"
            OnItemSelected="OnCustomerFinderItemSelected" />
        <eShip:PrefixFinderControl ID="prefixFinder" runat="server" EnableMultiSelection="false"
            Visible="False" OnSelectionCancel="OnPrefixFinderSelectionCancelled" OnItemSelected="OnPrefixFinderItemSelected" />
        <eShip:LibraryItemFinderControl EnableMultiSelection="True" runat="server" ID="libraryItemFinder"
            Visible="false" OnMultiItemSelected="OnLibraryItemFinderMultiItemSelected" OnSelectionCancel="OnLibraryItemFinderItemCancelled"
            OpenForEditEnabled="false" />
        <eShip:VendorFinderControl EnableMultiSelection="true" ID="vendorFinder" OnItemSelected="OnVendorFinderItemSelected"
            OnlyActiveResults="true" OnSelectionCancel="OnVendorFinderItemCancelled" OpenForEditEnabled="false"
            runat="server" Visible="false" OnMultiItemSelected="OnVendorFinderMultiItemSelected" />
        <eShip:AccountBucketFinderControl EnableMultiSelection="true" ID="accountBucketFinder" OnItemSelected="OnAccountBucketFinderItemSelected"
            OnSelectionCancel="OnAccountBucketFinderSelectionCancelled" runat="server" ShowActiveRecordsOnly="True"
            Visible="False" OnMultiItemSelected="OnAccountBucketFinderMultiItemSelected" />
        <eShip:RateSelectionDisplayControl ID="rateSelectionDisplay" runat="server" Visible="False"
            OnRateSelectionCancelled="OnRateSelectionDisplaySelectionCancelled" OnRateSelected="OnRateSelectionDisplayRateSelected" />
        <eShip:UserFinderControl ID="userFinder" runat="server" Visible="false" EnableMultiSelection="false"
            FilterForEmployees="False" MatchNonEmployeeDeptToActiveUser="True" OpenForEditEnabled="False" OnItemSelected="OnUserFinderItemSelected"
            OnSelectionCancel="OnUserFinderSelectionCancelled" />
        <eShip:VendorLaneHistoryBuySellControl runat="server" ID="vendorLaneHistoryBuySell" Visible="False"
            OnVendorLaneHistoryClose="OnVendorLaneHistoryClose" />
        <eShip:VendorRateAgreementControl runat="server" ID="vendorRateAgreement" OnClose="OnVendorRateAgreementClose"
            OnGenerate="OnVendorRateAgreementGenerate" Visible="false" />
        <eShip:ShippingLabelGeneratorControl runat="server" ID="shippingLabelGenerator" Visible="false"
            OnClose="OnShippingLabelGeneratorCloseClicked" />
        <eShip:DocumentDisplayControl runat="server" ID="documentDisplay" Visible="false" OnClose="OnDocumentDisplayClosed" />
        <eShip:PostalCodeFinderControl runat="server" ID="postalCodeFinder" Visible="False" OnItemSelected="OnPostalCodeFinderItemSelected"
            OnSelectionCancel="OnPostalCodeFinderItemCancelled" />
        <eShip:PaymentEntryControl runat="server" ID="pecPayment" Visible="False" CanModifyPaymentAmount="True" OnClose="OnPaymentEntryCloseClicked" OnProcessingMessages="OnPaymentEntryProcessingMessages"
            OnPaymentSuccessfullyProcessed="OnPaymentEntryPaymentSuccessfullyProcessed" />
        <eShip:PaymentRefundOrVoidControl runat="server" ID="prcRefundPayments" Visible="False" OnClose="OnPaymentRefundCloseClicked" OnProcessingMessages="OnPaymentRefundProcessingMessages"
            OnPaymentRefundProcessed="OnRefundPaymentsPaymentRefundProcessed" />
        <eShip:DatLoadboardAssetControl runat="server" ID="datLoadboardAssetControl" Visible="false"
            OnDoneModifying="OnCloseDatAssetClicked" />
        <eShip:AjaxTabHeaderTextUpdater runat="server" ID="athtuTabUpdater" />
        <eShip:JobFinderControl runat="server" ID="jobFinder" Visible="false" OnItemSelected="OnJobFinderItemSelected"
            OnSelectionCancel="OnJobFinderItemCancelled" OpenForEditEnabled="False" />

        <asp:UpdatePanel runat="server" UpdateMode="Always">
            <ContentTemplate>
                <eShip:VendorPerformanceSummaryControl runat="server" ID="vpscVendorPerformance" Visible="False" OnCancel="OnVendorPerformanceClose" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="vpscVendorPerformance" />
            </Triggers>
        </asp:UpdatePanel>

        <eShip:CustomHiddenField ID="hidLoadOrderId" runat="server" />

        <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information" OnOkay="OnOkayProcess" OnYes="OnYesProcess" OnNo="OnNoProcess" />


        <asp:Panel ID="pnlAboveDetails" runat="server" CssClass="rowgroup" DefaultButton="imgCustomerSearch">
            <div class="row">
                <div class="fieldgroup mr20 vlinedarkright">
                    <label class="label upper blue">Load Order Number: </label>
                    <eShip:CustomTextBox ID="txtLoadOrderNumber" runat="server" ReadOnly="True" CssClass="w130 disabled" />
                </div>
                <div class="fieldgroup mr20 vlinedarkright">
                    <label class="label blue upper">Customer: </label>
                    <eShip:CustomHiddenField runat="server" ID="hidCustomerId" />

                    <eShip:CustomTextBox runat="server" ID="txtCustomerNumber" OnTextChanged="OnCustomerNumberEntered" AutoPostBack="True" CssClass="w110" />
                    <asp:ImageButton ID="imgCustomerSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png" CausesValidation="False" OnClick="OnCustomerSearchClicked" />
                    <eShip:CustomTextBox runat="server" ID="txtCustomerName" ReadOnly="True" CssClass="w260 disabled" />
                    <ajax:AutoCompleteExtender runat="server" ID="aceCustomer" TargetControlID="txtCustomerNumber" ServiceMethod="GetCustomerList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                        EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                    <%= hidCustomerId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.Customer) 
                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Customer Record'><img src={3} width='20' class='middle'/></a>", 
                                                        ResolveUrl(CustomerView.PageAddress),
                                                        WebApplicationConstants.TransferNumber, 
                                                        hidCustomerId.Value.UrlTextEncrypt(),
                                                        ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                : string.Empty %>
                </div>
                <div class="fieldgroup">
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $(jsHelper.AddHashTag('<%= imgChangeStatus.ClientID %>')).on('mouseover mouseenter', function () {
                                $(jsHelper.AddHashTag('loadOrderStatusDiv')).slideDown();
                            });
                            $(jsHelper.AddHashTag('<%= imgChangeStatus.ClientID %>')).parent().mouseleave(function () {
                                $(jsHelper.AddHashTag('loadOrderStatusDiv')).slideUp();
                            });
                        });
                    </script>
                    <label class="label blue upper">Status: </label>
                    <asp:Image runat="server" ID="imgChangeStatus" ToolTip="Change Status" ImageAlign="AbsMiddle" ImageUrl="~/images/icons2/cog.png" Width="20px" />
                    <eShip:CustomTextBox runat="server" ID="txtStatus" ReadOnly="true" CssClass="w130 disabled" />
                    <eShip:CustomHiddenField runat="server" ID="hidStatus" />
                    <div id="loadOrderStatusDiv" style="display: none;" class="shadow">
                        <div class="popheader">
                            <h4>Change Status To</h4>
                        </div>
                        <table class="contain">
                            <tr>
                                <td>
                                    <label>
                                        <asp:LinkButton runat="server" ID="lbtnOffered" Text="Offered" OnCommand="OnStatusChanged" CssClass="link_nounderline blue" CausesValidation="False" /></label>
                                </td>
                                <td>
                                    <label>
                                        <asp:LinkButton runat="server" ID="lbtnQuoted" Text="Quoted" OnCommand="OnStatusChanged" CssClass="link_nounderline blue" CausesValidation="False" /></label>
                                </td>
                                <td>
                                    <label>
                                        <asp:LinkButton runat="server" ID="lbtnAccepted" Text="Accepted" OnCommand="OnStatusChanged" CssClass="link_nounderline blue" CausesValidation="False" /></label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>
                                        <asp:LinkButton runat="server" ID="lbtnCovered" Text="Covered" OnCommand="OnStatusChanged" CssClass="link_nounderline blue" CausesValidation="False" /></label>
                                </td>
                                <td>
                                    <label>
                                        <asp:LinkButton runat="server" ID="lbtnCancelled" Text="Cancelled" OnCommand="OnStatusChanged" CssClass="link_nounderline blue" CausesValidation="False" /></label>
                                </td>
                                <td>
                                    <label>
                                        <asp:LinkButton runat="server" ID="lbtnLost" Text="Lost" OnCommand="OnStatusChanged" CssClass="link_nounderline blue" CausesValidation="False" /></label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlLoadTenderDisplay" runat="server" Visible="false">
            <div class="popup">
                <div class="popheader">
                    <h4>
                        <asp:Literal ID="litTitle" runat="server" />
                        <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close" CausesValidation="false" OnClick="OnLoadTenderDisplayCloseClicked" runat="server" />
                    </h4>
                </div>
                <table class="poptable">
                    <tr>
                        <td>
                            <div runat="server" id="contentDiv" class="finderScroll">
                                [content]
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnQueueMessage" Text="Queue to send after saving record" OnClick="OnLoadTenderDisplayProcessClicked" class="mr20" runat="server" CausesValidation="False" />
                            <asp:CheckBox runat="server" ID="chkTruckLoadBid" Text="Initiate Truckload Bid"
                                Checked="False" CssClass="jQueryUniform" />
                        </td>

                    </tr>
                </table>
            </div>
        </asp:Panel>

        <ajax:TabContainer ID="tabLoadOrder" runat="server" CssClass="ajaxCustom" OnClientActiveTabChanged="HandleAddItemButtonVisibility">
            <ajax:TabPanel runat="server" ID="tabDetails" HeaderText="Details">
                <ContentTemplate>
                    <asp:Panel ID="pnlDetails" runat="server">
                        <div class="rowgroup">
                            <div class="row">
                                <div class="fieldgroup mr10">
                                    <script type="text/javascript">

                                        $(document).ready(function () {
                                            $('#<%= txtAccountBucketCode.ClientID %>').change(function () {

                                                var ids = new ControlIds();
                                                ids.AcctBucketCode = $('#<%= txtAccountBucketCode.ClientID %>').attr('id');
                                                ids.AcctBucketDesc = $('#<%= txtAccountBucketDescription.ClientID %>').attr('id');
                                                ids.AcctBucketId = $('#<%= hidAccountBucketId.ClientID %>').attr('id');
                                                ids.AcctBucketUnits = $('#<%= ddlAccountBucketUnit.ClientID %>').attr('id');
                                                ids.AcctBucketUnitId = $('#<%= hidAccountBucketUnitId.ClientID %>').attr('id');

                                                FindActiveAcctBucket('<%= ActiveUserTenantId %>', $('#<%= txtAccountBucketCode.ClientID %>').val(), ids);
                                            });
                                        });

                                    </script>

                                    <eShip:CustomHiddenField runat="server" ID="hidAccountBucketId" />
                                    <eShip:CustomHiddenField runat="server" ID="hidLoadOrderAccountBucketId" />
                                    <label class="wlabel blue">Account Bucket</label>
                                    <asp:Panel runat="server" DefaultButton="imgAccountBucketSearch">
                                        <eShip:CustomTextBox runat="server" ID="txtAccountBucketCode" CssClass="w100" />
                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtAccountBucketCode"
                                            ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                        <asp:ImageButton ID="imgAccountBucketSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png" CausesValidation="False" OnClick="OnAccountBucketSearchClicked" />
                                        <eShip:CustomTextBox runat="server" ID="txtAccountBucketDescription" CssClass="w180 disabled" Type="ReadOnly" />
                                        <ajax:AutoCompleteExtender runat="server" ID="aceAccountBucketCode" TargetControlID="txtAccountBucketCode" ServiceMethod="GetAccountBucketList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                            EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                                    </asp:Panel>
                                </div>
                                <div class="fieldgroup vlinedarkright mr10">
                                    <script type="text/javascript">
                                        $(document).ready(function () {
                                            $('#<%= ddlAccountBucketUnit.ClientID %>').change(function () {
                                                $('#<%= hidAccountBucketUnitId.ClientID %>').val($('#<%= ddlAccountBucketUnit.ClientID %>').val());
                                            });
                                        });
                                    </script>
                                    <label class="wlabel blue">Account Bucket Unit</label>
                                    <eShip:CustomHiddenField runat="server" ID="hidAccountBucketUnitId" />
                                    <eShip:CachedObjectDropDownList runat="server" ID="ddlAccountBucketUnit" CssClass="w130" EnableChooseOne="True" Type="AccountBucketUnits" DefaultValue="0" />
                                </div>
                                <div class="fieldgroup vlinedarkright mr10">
                                    <label class="wlabel blue">Service Mode</label>
                                    <asp:DropDownList ID="ddlServiceMode" runat="server" CssClass="w160" DataTextField="Text"
                                        DataValueField="Value" AutoPostBack="True" OnSelectedIndexChanged="OnServiceModeSelectedIndexChanged" />
                                </div>
                                <div class="fieldgroup">
                                    <script type="text/javascript">

                                        $(document).ready(function () {
                                            $('#<%= txtPrefixCode.ClientID %>').change(function () {

                                                var ids = new ControlIds();
                                                ids.PrefixCode = $('#<%= txtPrefixCode.ClientID %>').attr('id');
                                                ids.PrefixDesc = $('#<%= txtPrefixDescription.ClientID %>').attr('id');
                                                ids.PrefixId = $('#<%= hidPrefixId.ClientID %>').attr('id');

                                                FindActivePrefix('<%= ActiveUserTenantId %>', $('#<%= txtPrefixCode.ClientID %>').val(), ids);
                                            });
                                        });
                                    </script>

                                    <eShip:CustomHiddenField runat="server" ID="hidPrefixId" />
                                    <label class="wlabel blue">Prefix</label>
                                    <asp:Panel runat="server" DefaultButton="imgPrefixSearch">
                                        <eShip:CustomTextBox runat="server" ID="txtPrefixCode" CssClass="w100" />
                                        <asp:ImageButton ID="imgPrefixSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                                            CausesValidation="False" OnClick="OnPrefixSearchClicked" />
                                        <eShip:CustomTextBox runat="server" ID="txtPrefixDescription" CssClass="w150 disabled" Type="ReadOnly" />
                                        <ajax:AutoCompleteExtender runat="server" ID="acePrefixCode" TargetControlID="txtPrefixCode" ServiceMethod="GetPrefixList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                            EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                                    </asp:Panel>
                                </div>
                                <div class="fieldgroup_s">
                                    <asp:CheckBox runat="server" ID="chkHidePrefix" CssClass="jQueryUniform" />
                                    <label class="comlabel blue">
                                        Hide<br />
                                        Prefix</label>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div class="rowgroup">
                            <div class="col_1_3">
                                <h5>Service Information</h5>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <eShip:CustomHiddenField runat="server" ID="hidPrimaryVendorId" />
                                        <eShip:CustomHiddenField runat="server" ID="hidPrimaryLoadOrderVendorId" />
                                        <script type="text/javascript">
                                            $(document).ready(function () {
                                                var controlId = $("[id$='ibtnViewVendorPerformanceStatistics']").attr('id');
                                                if (controlId != null) jsHelper.SetDoPostBackJsLink(controlId);
                                            });
                                        </script>
                                        <label class="wlabel">
                                            Primary Vendor&nbsp;
                                            <asp:ImageButton runat="server" ID="ibtnLogVendorRejection" ImageUrl="~/images/icons2/cog.png" ImageAlign="AbsMiddle"
                                                OnClick="OnLogPrimaryVendorRejectionClicked" CausesValidation="False" ToolTip="Clear vendor and log vendor rejection" Width="20px" />
                                            <asp:ImageButton runat="server" ID="ibtnViewVendorPerformanceStatistics" ImageUrl="~/images/icons2/statsBlue.png" OnClick="OnViewVendorPerformanceStatisticsClicked"
                                                ToolTip="See Vendor Performance Statistics" Visible="False" Width="20px" />
                                            <%= hidPrimaryVendorId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.Vendor) 
                                                    ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Vendor Record'><img src={3} width='20' class='middle'/></a>", 
                                                                            ResolveUrl(VendorView.PageAddress),
                                                                            WebApplicationConstants.TransferNumber, 
                                                                            hidPrimaryVendorId.Value.UrlTextEncrypt(),
                                                                            ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                    : string.Empty %>
                                        </label>
                                        <asp:Panel runat="server" DefaultButton="imgVendorSearch">
                                            <eShip:CustomTextBox runat="server" ID="txtVendorNumber" CssClass="w80" OnTextChanged="OnVendorNumberEntered" AutoPostBack="True" />
                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtVendorNumber"
                                                ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                            <asp:ImageButton ID="imgVendorSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png" CausesValidation="False" OnClick="OnVendorSearchClicked" />
                                            <eShip:CustomTextBox runat="server" ID="txtVendorName" ReadOnly="True" CssClass="w220 disabled" />
                                            <ajax:AutoCompleteExtender runat="server" ID="aceVendor" TargetControlID="txtVendorNumber" ServiceMethod="GetVendorList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                                EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Vendor Pro Number</label>
                                        <eShip:CustomTextBox runat="server" ID="txtPrimaryProNumber" CssClass="w160" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Edi Status</label>
                                        <eShip:CustomTextBox runat="server" ID="txtEdiStatus" CssClass="w160 disabled" ReadOnly="True" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Priority</label>
                                        <eShip:CachedObjectDropDownList Type="ShipmentPriority" ID="ddlPriority" runat="server" CssClass="w160" EnableChooseOne="True"
                                            DefaultValue="0" />
                                    </div>
                                    <div class="fieldgroup_s">
                                        <asp:Panel runat="server" ID="pnlPartialTruckload">
                                            <asp:CheckBox runat="server" ID="chkIsPartialTruckLoad" CssClass="jQueryUniform" />
                                            <label>Is Partial TruckLoad</label>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">PO Number</label>
                                        <eShip:CustomTextBox ID="txtPurchaseOrderNumber" runat="server" CssClass="w160" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Shipper Reference</label>
                                        <eShip:CustomTextBox ID="txtShipperReference" runat="server" CssClass="w160" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Shipper BOL</label>
                                        <eShip:CustomTextBox ID="txtShipperBol" runat="server" ReadOnly="True" CssClass="w160 disabled" />
                                    </div>
                                    <div class="fieldgroup">
                                        <script type="text/javascript">
                                            $(document).ready(function () {
                                                var mileage = $("#<%=txtMileage.ClientID%>").val();
                                                var emptyMileage = $("#<%=txtEmptyMileage.ClientID%>").val();

                                                $("#<%=txtTotalMiles.ClientID%>").val(+mileage + +emptyMileage);
                                            });
                                        </script>
                                        <label class="wlabel">Total Miles</label>
                                        <eShip:CustomTextBox ID="txtTotalMiles" runat="server" CssClass="w160 disabled" ReadOnly="True" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup_s">
                                        <asp:CheckBox runat="server" ID="chkHideFromLoadboards" CssClass="jQueryUniform" />
                                        <label>Hide From Loadboards</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Loadboards Comment
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDescription" TargetControlId="txtDescription" MaxLength="250" />
                                        </label>
                                        <eShip:CustomTextBox runat="server" ID="txtDescription" CssClass="w330 h150" TextMode="MultiLine" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            User&nbsp;
                                            <asp:Image runat="server" ID="imgContactInformation" ToolTip="Contact Information" ImageAlign="AbsMiddle"
                                                Width="20px" ImageUrl="~/images/icons2/eye-open.png" />
                                        </label>
                                        <eShip:CustomTextBox ID="txtUser" runat="server" ReadOnly="True" CssClass="w160 disabled" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Date Created</label>
                                        <eShip:CustomTextBox runat="server" ID="txtDateCreated" ReadOnly="True" CssClass="w160 disabled" />
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    $(document).ready(function () {
                                        $(jsHelper.AddHashTag('<%= imgContactInformation.ClientID %>')).on('mouseover mouseenter', function () {
                                            $(jsHelper.AddHashTag('contactInformationDiv')).slideDown();
                                        });

                                        $(jsHelper.AddHashTag('<%= imgCloseContactInformation.ClientID %>')).click(function () {
                                            $(jsHelper.AddHashTag('contactInformationDiv')).slideUp();
                                        });

                                    });
                                </script>
                                <div class="row mt20" id="contactInformationDiv" style="display: none;">
                                    <div class="wAuto mr20 shadow">
                                        <div class="popheader">
                                            <h4>Contact Information
                                            <asp:Image ID="imgCloseContactInformation" ImageUrl="~/images/icons2/close.png" CssClass="close"
                                                runat="server" />
                                            </h4>
                                        </div>
                                        <table class="contain">
                                            <tr>
                                                <td class="text-right p_m0" style="width: 20%">
                                                    <label class="upper blue">Phone: </label>
                                                </td>
                                                <td class="p_m0">
                                                    <label>
                                                        <a id="lnkUserPhone" runat="server" />
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right p_m0">
                                                    <label class="upper blue">Mobile: </label>
                                                </td>
                                                <td class="p_m0">
                                                    <label>
                                                        <a id="lnkUserMobile" runat="server" />
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right p_m0">
                                                    <label class="upper blue">Fax: </label>
                                                </td>
                                                <td class="p_m0">
                                                    <label>
                                                        <a id="lnkUserFax" runat="server" />
                                                    </label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right p_m0">
                                                    <label class="upper blue">Email: </label>
                                                </td>
                                                <td class="p_m0">
                                                    <label>
                                                        <a id="lnkUserEmail" runat="server" />
                                                    </label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <h5>Pickup And Delivery Dates</h5>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel w130">Desired Pickup Date</label>
                                        <eShip:CustomTextBox runat="server" ID="txtDesiredPickupDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                                    </div>
                                    <div class="fieldgroup">

                                        <script type="text/javascript">
                                            function OffsetLatePickupTimes() {
                                                var ids = new ControlIds();
                                                ids.TimesOfDayWindowStart = $(jsHelper.AddHashTag('<%= ddlEarlyPickup.ClientID %>')).attr('id');
                                                ids.TimesOfDayWindowEnd = $(jsHelper.AddHashTag('<%= ddlLatePickup.ClientID %>')).attr('id');

                                                OffsetDropDownListByTimeOfDay(false, ids);
                                            }
                                        </script>
                                        <label class="wlabel shrink mr10">Earliest Pickup</label>
                                        <asp:DropDownList ID="ddlEarlyPickup" runat="server" CssClass="w75" onchange="OffsetLatePickupTimes();" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel shrink">Latest Pickup</label>
                                        <asp:DropDownList ID="ddlLatePickup" runat="server" CssClass="w75" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel w130">
                                            <abbr title="Estimated">Est.</abbr>
                                            Delivery Date
                                        </label>
                                        <eShip:CustomTextBox runat="server" ID="txtEstimatedDeliveryDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                                    </div>
                                    <asp:Panel runat="server" ID="pnlDelivery">

                                        <div class="fieldgroup">
                                            <script type="text/javascript">
                                                function OffsetLateDeliveryTimes() {
                                                    var ids = new ControlIds();
                                                    ids.TimesOfDayWindowStart = $(jsHelper.AddHashTag('<%= ddlEarlyDelivery.ClientID %>')).attr('id');
                                                    ids.TimesOfDayWindowEnd = $(jsHelper.AddHashTag('<%= ddlLateDelivery.ClientID %>')).attr('id');

                                                    OffsetDropDownListByTimeOfDay(false, ids);
                                                }
                                            </script>
                                            <label class="wlabel shrink">Earliest Delivery</label>
                                            <asp:DropDownList ID="ddlEarlyDelivery" runat="server" CssClass="w75" onchange="OffsetLateDeliveryTimes();" />
                                        </div>
                                        <div class="fieldgroup">
                                            <label class="wlabel shrink">Latest Delivery</label>
                                            <asp:DropDownList ID="ddlLateDelivery" runat="server" CssClass="w75" />
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div class="row mt10">
                                    <script type="text/javascript">
                                        $(document).ready(function () {
                                            $("#<%= btnEditServices.ClientID %>").click(function (e) {
                                                e.preventDefault();
                                                ShowPanel($("#<%= pnlServiceList.ClientID %>"));
                                            });

                                            $("#<%= btnEditEquipment.ClientID %>").click(function (e) {
                                                e.preventDefault();
                                                ShowPanel($("#<%= pnlEquipmentTypeList.ClientID %>"));
                                            });


                                            $("#<%= btnEquipmentTypesDone.ClientID %>").click(function (e) {
                                                e.preventDefault();
                                                HidePanels($("#<%= pnlEquipmentTypeList.ClientID %>"));
                                            });

                                            $("#<%= btnServicesDone.ClientID %>").click(function () {
                                                HidePanels($("#<%= pnlServiceList.ClientID %>"));
                                            });

                                            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                                                $("#<%= btnEditServices.ClientID %>").click(function (e) {
                                                    e.preventDefault();
                                                    ShowPanel($("#<%= pnlServiceList.ClientID %>"));
                                                });

                                                $("#<%= btnServicesDone.ClientID %>").click(function () {
                                                    HidePanels($("#<%= pnlServiceList.ClientID %>"));
                                                });
                                            });
                                        });

                                        function ShowPanel($panel) {
                                            $($panel).fadeIn('fast');
                                            $("#<%= pnlDimScreenJS.ClientID %>").fadeIn('fast');
                                        }

                                        function HidePanels($panel) {
                                            $($panel).fadeOut('fast');
                                            $("#<%= pnlDimScreenJS.ClientID %>").fadeOut('fast');
                                        }

                                        function getSelected(list, lblResults, lblValueText) {
                                            var checkboxes = $('[id$=' + list + '] input[id$="chkSelected"]');
                                            var checkedText = jsHelper.EmptyString;

                                            for (var i = 0; i < checkboxes.length; i++) {
                                                if (jsHelper.IsChecked(checkboxes[i].id)) {
                                                    checkedText += ($(jsHelper.AddHashTag(checkboxes[i].id)).parent().parent().parent().parent().find('[id$="' + lblValueText + '"]').text() + ",");
                                                }
                                            }
                                            if (!jsHelper.IsNullOrEmpty(checkedText)) checkedText = checkedText.substr(0, checkedText.length - 1);
                                            $('[id*=hid' + lblResults + '_txtHid]').val(checkedText);
                                            $('[id$=lbl' + lblResults + ']').text(checkedText);
                                        }
                                    </script>
                                    <h5>Equipment
                                        <asp:Button ID="btnEditEquipment" Text="Edit Equipment" runat="server" CausesValidation="False" CssClass="mr17 right" />
                                    </h5>
                                    <div class="row mb10">
                                        <eShip:CustomHiddenField ID="hidEquipment" runat="server" />
                                        <label>
                                            <asp:Label ID="lblEquipment" runat="server" />
                                        </label>
                                    </div>
                                </div>

                                <asp:UpdatePanel runat="server" ID="upPnlServices" UpdateMode="Always">
                                    <ContentTemplate>
                                        <h5>Services
                                            <asp:Button ID="btnEditServices" Text="Edit Services" runat="server" CausesValidation="False" CssClass="mr17 right" />
                                        </h5>
                                        <div class="row">
                                            <eShip:CustomHiddenField ID="hidServices" runat="server" />
                                            <label>
                                                <asp:Label ID="lblServices" runat="server" />
                                            </label>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>

                            <div class="col_1_3">
                                <div class="row">
                                    <eShip:CustomHiddenField ID="hidOriginKey" runat="server" />
                                    <asp:Panel runat="server" ID="pnlOriginLocation">
                                        <eShip:OperationsAddressInputControl ID="operationsAddressInputOrigin" runat="server" Type="Origin"
                                            OnFindPostalCode="OnAddressInputFindPostalCode" OnAddAddressFromAddressBook="OnAddressInputAddAddressFromAddressBook" />
                                    </asp:Panel>
                                </div>
                                <div class="row">
                                    <asp:CheckBox runat="server" ID="chkSaveOrigin" CssClass="jQueryUniform" />
                                    <label>Save origin location to address book on shipment save</label>
                                </div>
                            </div>
                            <div class="col_1_3 wd">
                                <div class="row">
                                    <asp:HiddenField ID="hidDestinationKey" runat="server" />
                                    <asp:Panel runat="server" ID="pnlDestinationLocation">
                                        <eShip:OperationsAddressInputControl ID="operationsAddressInputDestination" runat="server"
                                            Type="Destination" OnFindPostalCode="OnAddressInputFindPostalCode" OnAddAddressFromAddressBook="OnAddressInputAddAddressFromAddressBook" />
                                    </asp:Panel>
                                </div>
                                <div class="row">
                                    <asp:CheckBox runat="server" ID="chkSaveDestination" CssClass="jQueryUniform" />
                                    <label>Save destination location to address book on shipment save</label>
                                </div>
                            </div>
                    </asp:Panel>
                    </div>
                    <asp:UpdatePanel runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:Panel ID="pnlServiceList" runat="server" CssClass="popup popup-position-fixed top50" Style="display: none;">
                                <div class="popheader">
                                    <h4>Services</h4>
                                </div>
                                <div class="row finderScroll">
                                    <div class="fieldgroup pl20 pr20 pt20">
                                        <asp:Repeater runat="server" ID="rptServices">
                                            <HeaderTemplate>
                                                <ul class="twocol_list sm_chk" id="ulServices">
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <li>
                                                    <eShip:CustomHiddenField runat="server" ID="hidServiceId" Value='<%# Eval("Value") %>' />
                                                    <eShip:AltUniformCheckBox runat="server" ID="chkSelected" />
                                                    <label class="lhInherit">
                                                        <asp:Label runat="server" ID="lblServiceCode" Text='<%# Eval("Text") %>' CssClass="fs85em" />
                                                    </label>
                                                </li>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </ul>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                                <div class="row text-right">
                                    <div class="fieldgroup pb10 pt10 right">
                                        <asp:Button ID="btnServicesDone" runat="server" Text="Done" CausesValidation="False"
                                            OnClientClick="ShowProcessingDivBlock();" OnClick="OnServicesSelectionDoneClicked" />
                                    </div>
                                </div>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:Panel ID="pnlEquipmentTypeList" runat="server" CssClass="popup popupControlOverW750 popup-position-fixed top50" Style="display: none;">
                        <div class="popheader">
                            <h4>Equipment Types</h4>
                        </div>
                        <div class="row finderScroll">
                            <div class="fieldgroup pl20 pr20 pt20">
                                <script type="text/javascript">
                                    $(document).ready(function () {
                                        // enforce mutual exclusion on checkboxes in ulEquipmentTypes
                                        $(jsHelper.AddHashTag('ulEquipmentTypes input:checkbox')).click(function () {
                                            if (jsHelper.IsChecked($(this).attr('id'))) {
                                                $(jsHelper.AddHashTag('ulEquipmentTypes input:checkbox')).not(jsHelper.AddHashTag($(this).attr('id'))).each(function () {
                                                    jsHelper.UnCheckBox($(this).attr('id'));
                                                });
                                                $.uniform.update();
                                            }
                                        });
                                    });
                                </script>
                                <asp:Repeater runat="server" ID="rptEquipmentTypes">
                                    <HeaderTemplate>
                                        <ul class="twocol_list sm_chk" id="ulEquipmentTypes">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <li>
                                            <asp:CheckBox runat="server" ID="chkSelected" CssClass="jQueryUniform" />
                                            <eShip:CustomHiddenField runat="server" ID="hidEquipmentTypeId" Value='<%# Eval("Value") %>' />
                                            <label class="lhInherit">
                                                <asp:Label runat="server" ID="lblEquipmentTypeCode" Text='<%# Eval("Text") %>' CssClass="fs85em" />
                                            </label>
                                        </li>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </ul>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                        <div class="row">
                            <div class="fieldgroup pb10 pt10 right">
                                <asp:Button ID="btnEquipmentTypesDone" runat="server" Text="Done" CausesValidation="False"
                                    OnClientClick="getSelected('ulEquipmentTypes', 'Equipment', 'lblEquipmentTypeCode');" />
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabItems" HeaderText="Items">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlItems" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlItems" DefaultButton="btnAddItem">
                                <div class="row mb10">
                                    <div class="fieldgroup">
                                        <eShip:AltUniformCheckBox runat="server" ID="chkBypassLinearFootRuleForRating" />
                                        <label>Bypass Linear Foot Rule</label>
                                    </div>
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddItem" Text="Add Item" CausesValidation="false" OnClick="OnAddItemClicked" OnClientClick="ShowProcessingDivBlock();" />
                                        <asp:Button runat="server" ID="btnAddLibraryItem" Text="Add Library Item" CausesValidation="false" OnClick="OnAddLibraryItemClicked" />
                                        <asp:Button runat="server" ID="btnClearItems" Text="Clear Items" CausesValidation="False" OnClick="OnClearItemsClicked" OnClientClick="ShowProcessingDivBlock();" />
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    $(window).load(function () {
                                        $(jsHelper.AddHashTag('loadOrderItemsTable [id$="txtEstimatedPcf"]')).each(function () {
                                            UpdateDensity(this);
                                        });

                                        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                                            $(jsHelper.AddHashTag('loadOrderItemsTable [id$="txtEstimatedPcf"]')).each(function () {
                                                UpdateDensity(this);
                                            });
                                        });
                                    });


                                    function UpdateDensity(control) {
                                        var ids = new ControlIds();
                                        ids.DensityWeight = $(control).parent().parent().find('[id$="txtWeight"]').attr('id');
                                        ids.DensityLength = $(control).parent().parent().find('[id$="txtLength"]').attr('id');
                                        ids.DensityWidth = $(control).parent().parent().find('[id$="txtWidth"]').attr('id');
                                        ids.DensityHeight = $(control).parent().parent().find('[id$="txtHeight"]').attr('id');
                                        ids.DensityCalc = $(control).parent().parent().find('[id$="txtEstimatedPcf"]').attr('id');
                                        ids.HidDensityCalc = jsHelper.EmptyString;
                                        ids.DensityQty = $(control).parent().parent().find('[id$="txtQuantity"]').attr('id');
                                        CalculateDensity(jsHelper.EnglishUnits, ids);
                                    }
                                </script>

                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheShipmentsItemsTable" TableId="loadOrderItemsTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table class="stripe" id="loadOrderItemsTable">
                                    <tr>
                                        <th style="width: 17%;">Package Type
                                        </th>
                                        <th style="width: 7%;" class="text-center">Quantity
                                        </th>
                                        <th style="width: 9%;">Weight
                                    <abbr title="Pounds">(lb)</abbr>
                                        </th>
                                        <th style="width: 9%;">Length
                                    <abbr title="Inches">(in)</abbr>
                                        </th>
                                        <th style="width: 9%;">Width
                                    <abbr title="Inches">(in)</abbr>
                                        </th>
                                        <th style="width: 9%;">Height
                                    <abbr title="Inches">(in)</abbr>
                                        </th>
                                        <th style="width: 8%;">
                                            <abbr title="Actual Pounds per Cubic Feet">Pcf</abbr>
                                        </th>
                                        <th style="width: 8%;" class="text-center">Stackable
                                        </th>
                                        <th style="width: 8%;" class="text-center">Pieces
                                        </th>
                                        <th style="width: 8%">Value
                                        </th>
                                        <th style="width: 8%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstItems" runat="server" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnLoadOrderItemDataBound">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidItemIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <eShip:CustomHiddenField ID="hidItemId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <eShip:CachedObjectDropDownList Type="PackageType" ID="ddlPackageType" runat="server" CssClass="w130"
                                                        EnableChooseOne="True" DefaultValue="0" SelectedValue='<%# Eval("PackageTypeId") %>' />
                                                    <asp:ImageButton runat="server" ID="imgSetPackageDimensions" ImageUrl="~/images/icons2/cog.png" Width="16px"
                                                        CausesValidation="False" ToolTip="Set Package Type Dimensions" OnClick="OnSetPackageDimensionsClicked" OnClientClick="ShowProcessingDivBlock();" />
                                                </td>
                                                <td class="text-center">
                                                    <eShip:CustomTextBox ID="txtQuantity" runat="server" Text='<%# Eval("Quantity") %>' CssClass="w55" onblur="UpdateDensity(this);" Type="NumbersOnly" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtWeight" runat="server" Text='<%# Eval("Weight") %>' CssClass="w70" onblur="UpdateDensity(this);" Type="FloatingPointNumbers" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtLength" runat="server" Text='<%# Eval("Length") %>' CssClass="w70" onblur="UpdateDensity(this);" Type="FloatingPointNumbers" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtWidth" runat="server" Text='<%# Eval("Width") %>' CssClass="w70" onblur="UpdateDensity(this);" Type="FloatingPointNumbers" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtHeight" runat="server" Text='<%# Eval("Height") %>' CssClass="w70" onblur="UpdateDensity(this);" Type="FloatingPointNumbers" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtEstimatedPcf" runat="server" CssClass="w80 disabled" ReadOnly="True" />
                                                </td>
                                                <td class="text-center">
                                                    <eShip:AltUniformCheckBox runat="server" ID="chkIsStackable" Checked='<%# Eval("IsStackable") %>' />
                                                </td>
                                                <td class="text-center">
                                                    <eShip:CustomTextBox ID="txtPieceCount" runat="server" Text='<%# Eval("PieceCount") %>' CssClass="w40" Type="NumbersOnly" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtItemValue" runat="server" Text='<%# Eval("Value") %>' CssClass="w70" Type="FloatingPointNumbers" />
                                                </td>
                                                <td class="text-center top" rowspan="2">
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png" OnClientClick="ShowProcessingDivBlock();"
                                                        CausesValidation="false" OnClick="OnDeleteItemClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                            <tr class="hidden">
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td colspan="13" class="pt0 bottom_shadow">
                                                    <div class="col_1_3">
                                                        <div class="row">
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue pb10">
                                                                    Description
                                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDescription" MaxLength="200" TargetControlId="txtDescription" />
                                                                </label>
                                                                <eShip:CustomTextBox ID="txtDescription" runat="server" Text='<%# Eval("Description") %>' TextMode="MultiLine" CssClass="w280" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col_1_3">
                                                        <div class="row">
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue">Comment</label>
                                                                <eShip:CustomTextBox ID="txtComment" runat="server" Text='<%# Eval("Comment") %>' CssClass="w310" MaxLength="50" />
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="fieldgroup mr10">
                                                                <label class="wlabel blue">Pickup</label>
                                                                <asp:DropDownList runat="server" ID="ddlItemPickup" DataTextField="Text" DataValueField="Value" CssClass="w100" />
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue">Delivery</label>
                                                                <asp:DropDownList runat="server" ID="ddlItemDelivery" DataTextField="Text" DataValueField="Value" CssClass="w100" />
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue"><abbr title="Hazardous Material">HazMat</abbr></label>
                                                                <eShip:AltUniformCheckBox runat="server" ID="chkIsHazmat" Checked='<%# Eval("HazardousMaterial") %>' />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col_1_3">
                                                        <div class="row">
                                                            <div class="fieldgroup mr10">
                                                                <label class="wlabel blue">NMFC Code</label>
                                                                <eShip:CustomTextBox ID="txtNMFCCode" runat="server" Text='<%# Eval("NMFCCode") %>' CssClass="w130" />
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue">HTS Code</label>
                                                                <eShip:CustomTextBox ID="txtHTSCode" runat="server" Text='<%# Eval("HTSCode") %>' CssClass="w130" />
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue">Freight Class</label>
                                                                <asp:DropDownList runat="server" ID="ddlFreightClass" DataTextField="Text" DataValueField="Value" CssClass="w130" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                                <script type="text/javascript">
                                    $(function () {
                                        HandleAddItemButtonVisibility();

                                        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function () {
                                            HandleAddItemButtonVisibility();
                                        });
                                    });

                                    function HandleAddItemButtonVisibility() {
                                        if ($(document).height() > $(window).height()) {
                                            $("#divSecondAddItemButton").show();
                                        } else {
                                            $("#divSecondAddItemButton").hide();
                                        }
                                    }
                                </script>
                                <div class="row pt20" id="divSecondAddItemButton">
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddItemTabBottom" Text="Add Item" CssClass="mr10" CausesValidation="false" OnClick="OnAddItemClicked" OnClientClick="ShowProcessingDivBlock();" />
                                    </div>
                                </div>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnAddLibraryItem" />
                        </Triggers>
                    </asp:UpdatePanel>

                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabCharges" HeaderText="Charges">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlCharges" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlCharges" DefaultButton="btnAddCharge">
                                <div class="row">
                                    <div class="fieldgroup">
                                        <eShip:AltUniformCheckBox runat="server" ID="chkDeclineInsurance" CssClass="jQueryUniform" />
                                        <label>Declined Additional Insurance</label>
                                    </div>
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddCharge" Text="Add Charge" CausesValidation="false" OnClick="OnAddChargeClicked" OnClientClick="ShowProcessingDivBlock();" />
                                        <asp:Button runat="server" ID="btnAutoRate" Text="Auto Rate Shipment" CausesValidation="false" OnClick="OnAutoRateClicked" OnClientClick="ShowProcessingDivBlock();" />
                                        <asp:Button runat="server" ID="btnClearCharges" Text="Clear Charges" CausesValidation="False" OnClick="OnClearChargesClicked" OnClientClick="ShowProcessingDivBlock();" />
                                    </div>
                                </div>
                                <div class="row mb10 mt10">
                                    <div class="fieldgroup">
                                        <label class="upper blue">Collected Payment Amount:</label>
                                        <eShip:CustomTextBox runat="server" ID="txtCollectedPaymentAmount" CssClass="w100 disabled" ReadOnly="True" />
                                    </div>
                                    <div class="fieldgroup" id="chargeStatisticsDiv">
                                        <eShip:ChargeStatistics2 ID="chargeStatistics" runat="server" />
                                        <asp:ImageButton runat="server" ID="ibtnRefreshChargeStatistics" CausesValidation="False" AlternateText="Refresh Charge Statistics" ImageUrl="~/images/icons2/cog.png"
                                            ToolTip="Refresh Charge Statistics" OnClick="OnRefreshChargeStatisticsClicked" OnClientClick="ResetChargeStatisticsColor(); ShowProcessingDivBlock();" />
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    $(window).load(function () {

                                        $(jsHelper.AddHashTag('shipmentChargesTable [id$="txtAmountDue"]')).each(function () {
                                            UpdateChargeInformation(this);
                                        });
                                        $('#chargeStatisticsDiv').removeClass('red');

                                        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                                            $(jsHelper.AddHashTag('shipmentChargesTable [id$="txtAmountDue"]')).each(function () {
                                                UpdateChargeInformation(this);
                                            });
                                            $('#chargeStatisticsDiv').removeClass('red');
                                        });
                                    });

                                    function ResetChargeStatisticsColor() {
                                        $('#chargeStatisticsDiv').removeClass('red');
                                    }

                                    function UpdateChargeInformation(control) {
                                        var unitSell = Number($(control).parent().parent().find('[id$="txtUnitSell"]').val());
                                        var unitDiscount = Number($(control).parent().parent().find('[id$="txtUnitDiscount"]').val());
                                        var qty = Number($(control).parent().parent().find('[id$="txtQuantity"]').val());
                                        if (qty < 1) qty = 1;
                                        $(control).parent().parent().find('[id$="txtAmountDue"]').val(((unitSell - unitDiscount) * qty).toFixed(4));
                                        $('#chargeStatisticsDiv').addClass('red');
                                    }
                                </script>

                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheShipmentChargesTable" TableId="shipmentChargesTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table class="stripe" id="shipmentChargesTable">
                                    <tr>
                                        <th style="width: 25%;">Charge Code
                                        </th>
                                        <th style="width: 10%;">
                                            <abbr title="Quantity">Qty</abbr>
                                        </th>
                                        <th style="width: 15%;" class="text-right">Unit Buy
										    <abbr title="Dollars">($)</abbr>
                                        </th>
                                        <th style="width: 15%;" class="text-right">Unit Sell
										    <abbr title="Dollars">($)</abbr>
                                        </th>
                                        <th style="width: 15%;" class="text-right">Unit Discount
										    <abbr title="Dollars">($)</abbr>
                                        </th>
                                        <th style="width: 15%;" class="text-right">Amount Due
										    <abbr title="Dollars">($)</abbr>
                                        </th>
                                        <th style="width: 5%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstCharges" runat="server" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnChargesItemDataBound">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CachedObjectDropDownList Type="ChargeCodes" ID="ddlChargeCode" runat="server" CssClass="w220"
                                                        EnableChooseOne="True" DefaultValue="0" SelectedValue='<%# Eval("ChargeCodeId") %>' />
                                                </td>
                                                <td class="text-center">
                                                    <eShip:CustomHiddenField ID="hidChargeIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <eShip:CustomHiddenField ID="hidChargeId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <eShip:CustomTextBox ID="txtQuantity" runat="server" Text='<%# Eval("Quantity") %>' CssClass="w60" onblur="UpdateChargeInformation(this);" Type="NumbersOnly" />
                                                </td>
                                                <td class="text-right">
                                                    <eShip:CustomTextBox ID="txtUnitBuy" runat="server" Text='<%# Eval("UnitBuy", "{0:f4}") %>' CssClass="w80" onblur="UpdateChargeInformation(this);" Type="FloatingPointNumbers" />
                                                </td>
                                                <td class="text-right">
                                                    <eShip:CustomTextBox ID="txtUnitSell" runat="server" Text='<%# Eval("UnitSell", "{0:f4}") %>' CssClass="w80" onblur="UpdateChargeInformation(this);" Type="FloatingPointNumbers" />
                                                </td>
                                                <td class="text-right">
                                                    <eShip:CustomTextBox ID="txtUnitDiscount" runat="server" Text='<%# Eval("UnitDiscount", "{0:f4}") %>' CssClass="w80" onblur="UpdateChargeInformation(this);" Type="FloatingPointNumbers" />
                                                </td>
                                                <td class="text-right">
                                                    <eShip:CustomTextBox runat="server" ID="txtAmountDue" CssClass="w80 disabled" ReadOnly="True" />
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        CausesValidation="false" OnClick="OnDeleteChargeClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                            <tr class="hidden">
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td colspan="13" class="pt0 mt0 bottom_shadow">
                                                    <div class="row mb10">
                                                        <div class="col_1_3">
                                                            <div class="fieldgroup middle">
                                                                <label class="wlabel blue">Vendor</label>
                                                                <asp:DropDownList runat="server" ID="ddlVendors" OnSelectedIndexChanged="OnChangeVendorInCharge"
                                                                    AutoPostBack="True" DataTextField="Text" DataValueField="Value" AppendDataBoundItems="true" />
                                                                <eShip:CustomHiddenField ID="hidSelectedVendorId" runat="server" Value='<%# Eval("VendorId") %>' />
                                                            </div>
                                                        </div>
                                                        <div class="col_2_3">
                                                            <div class="fieldgroup top">
                                                                <label class="wlabel blue">
                                                                    Comment
                                                                    <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDescription" MaxLength="100" TargetControlId="txtComment" />
                                                                </label>
                                                                <eShip:CustomTextBox ID="txtComment" runat="server" Text='<%# Eval("Comment") %>' TextMode="MultiLine" MaxLength="100" CssClass="w420" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>

                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnAutoRate" />
                        </Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabAdditionalDetails" HeaderText="Add. Details" Style="display: none;">
                <ContentTemplate>
                    <asp:Panel ID="pnlAdditionalDetails" runat="server">
                        <div class="rowgroup">
                            <div class="col_1_2 bbox vlinedarkright">
                                <h5>Coordinator Information</h5>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <script type="text/javascript">

                                            $(document).ready(function () {

                                                $('#<%= txtLoadOrderCoordinator.ClientID %>').change(function () {

                                                    var ids = new ControlIds();
                                                    ids.Username = $('#<%= txtLoadOrderCoordinator.ClientID %>').attr('id');
                                                    ids.UserId = $('#<%= hidLoadOrderCoordUserId.ClientID %>').attr('id');
                                                    ids.UserFullName = $('#<%= txtLoadOrderCoordName.ClientID %>').attr('id');

                                                    FindActiveShipmentCoordinator('<%= ActiveUserTenantCode %>', '<%= ActiveUsername %>', $('#<%= txtLoadOrderCoordinator.ClientID %>').val(), ids);
                                                });


                                                $('#<%= txtCarrierCoordUsername.ClientID %>').change(function () {

                                                    var ids = new ControlIds();
                                                    ids.Username = $('#<%= txtCarrierCoordUsername.ClientID %>').attr('id');
                                                    ids.UserId = $('#<%= hidCarrierCoordUserId.ClientID %>').attr('id');
                                                    ids.UserFullName = $('#<%= txtCarrierCoordName.ClientID %>').attr('id');

                                                    FindActiveCarrierCoordinator('<%= ActiveUserTenantCode %>', '<%= ActiveUsername %>', $('#<%= txtCarrierCoordUsername.ClientID %>').val(), ids);
                                                });
                                            });

                                        </script>
                                        <label class="wlabel">Shipment Coordinator</label>
                                        <eShip:CustomTextBox runat="server" ID="txtLoadOrderCoordinator" MaxLength="50" CssClass="w110" />
                                        <asp:ImageButton runat="server" ID="imgShipmentCoordUsernameSearch" ToolTip="Find Shipment Coordinator"
                                            ImageUrl="~/images/icons2/search_dark.png" CausesValidation="False" OnClick="OnFindLoadOrderCoordUserClicked" />
                                        <eShip:CustomTextBox runat="server" ID="txtLoadOrderCoordName" Type="ReadOnly" CssClass="w280 disabled" />
                                        <eShip:CustomHiddenField runat="server" ID="hidLoadOrderCoordUserId" />
                                        <ajax:AutoCompleteExtender runat="server" ID="aceLoadOrderCoordinator" TargetControlID="txtLoadOrderCoordinator" ServiceMethod="GetUserList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                            EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                                    </div>
                                </div>
                                <div class="row pb20">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Carrier Coordinator</label>
                                        <eShip:CustomTextBox runat="server" ID="txtCarrierCoordUsername" MaxLength="50" CssClass="w110" />
                                        <asp:ImageButton runat="server" ID="imgCarrierCoordUsernameSearch" ToolTip="Find Carrier Coordinator"
                                            ImageUrl="~/images/icons2/search_dark.png" CausesValidation="False" OnClick="OnFindCarrierCoordUserClicked" />
                                        <eShip:CustomTextBox runat="server" ID="txtCarrierCoordName" Type="ReadOnly" CssClass="w280 disabled" />
                                        <eShip:CustomHiddenField runat="server" ID="hidCarrierCoordUserId" />
                                        <ajax:AutoCompleteExtender runat="server" ID="aceCarrierCoordinator" TargetControlID="txtCarrierCoordUsername" ServiceMethod="GetUserList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                            EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                                    </div>
                                </div>
                                <h5>Mileage</h5>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Mileage Source</label>
                                        <eShip:CachedObjectDropDownList Type="MileageSources" ID="ddlMileageSource" runat="server" CssClass="w200" EnableChooseOne="True"
                                            DefaultValue="0" />
                                    </div>
                                </div>
                                <div class="row pb20">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Empty Mileage</label>
                                        <eShip:CustomTextBox ID="txtEmptyMileage" runat="server" CssClass="w200" Type="FloatingPointNumbers" />
                                    </div>
                                    <div class="fieldgroup">

                                        <asp:UpdatePanel runat="server" ID="upPnlMileage" UpdateMode="Conditional">
                                            <ContentTemplate>

                                                <label class="wlabel">Mileage</label>
                                                <eShip:CustomTextBox ID="txtMileage" runat="server" CssClass="w200" Type="FloatingPointNumbers" />
                                                <asp:ImageButton ID="btnGetMiles" runat="server" ImageUrl="~/images/icons2/cog.png" OnClientClick="ShowProcessingDivBlock();"
                                                    CausesValidation="false" ToolTip="Get Mileage" OnClick="OnGetMilesClicked" />

                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                                <asp:Panel ID="pnlHazMat" runat="server">
                                    <h5>Hazardous Material Contact</h5>
                                    <div class="row">
                                        <div class="fieldgroup">
                                            <asp:CheckBox ID="chkHazardousMaterial" runat="server" CssClass="jQueryUniform" />
                                            <label>Hazardous Material</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="fieldgroup mr20">
                                            <label class="wlabel">Name</label>
                                            <eShip:CustomTextBox ID="txtHazardousContactName" runat="server" CssClass="w200" MaxLength="50" />
                                        </div>
                                        <div class="fieldgroup">
                                            <label class="wlabel">Email</label>
                                            <eShip:CustomTextBox ID="txtHazardousContactEmail" runat="server" CssClass="w200" MaxLength="100" />
                                        </div>
                                    </div>
                                    <div class="row pb20">
                                        <div class="fieldgroup mr20">
                                            <label class="wlabel">Phone</label>
                                            <eShip:CustomTextBox ID="txtHazardousContactPhone" runat="server" CssClass="w200" MaxLength="50" />
                                        </div>
                                        <div class="fieldgroup">
                                            <label class="wlabel">Mobile</label>
                                            <eShip:CustomTextBox ID="txtHazardousContactMobile" runat="server" CssClass="w200" MaxLength="50" />
                                        </div>
                                    </div>
                                </asp:Panel>
                                <h5>Miscellaneous Fields</h5>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Miscellaneous Field 1
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleMiscField1" TargetControlId="txtMiscField1" MaxLength="100" />
                                        </label>
                                        <eShip:CustomTextBox ID="txtMiscField1" runat="server" TextMode="MultiLine" CssClass="w420" />
                                    </div>
                                </div>
                                <div class="row pb20">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Miscellaneous Field 2
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleMiscField2" TargetControlId="txtMiscField2" MaxLength="100" />
                                        </label>
                                        <eShip:CustomTextBox ID="txtMiscField2" runat="server" TextMode="MultiLine" CssClass="w420" />
                                    </div>
                                </div>
                                <h5>Driver Information</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Driver Name</label>
                                        <eShip:CustomTextBox runat="server" ID="txtDriverName" CssClass="w200" MaxLength="50" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Driver Phone Number</label>
                                        <eShip:CustomTextBox runat="server" ID="txtDriverPhoneNumber" CssClass="w200" MaxLength="50" />
                                    </div>
                                </div>
                                <div class="row pb20">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Driver Truck Number</label>
                                        <eShip:CustomTextBox runat="server" ID="txtDriverTrailerNumber" CssClass="w200" MaxLength="50" />
                                    </div>
                                </div>
                                <asp:Panel runat="server" ID="pnlDat" Visible="false">
                                    <h5>DAT</h5>
                                    <div class="row">
                                        <div class="fieldgroup mr20">
                                            <label class="wlabel">Asset Id</label>
                                            <eShip:CustomTextBox runat="server" ID="txtDatAssetId" CssClass="w200 disabled"
                                                ReadOnly="True" MaxLength="50" />
                                            <asp:Button runat="server" CausesValidation="False" ID="btnModifyDatPosting" Text="Post to DAT"
                                                OnClick="DisplayManageLoadOrderOnDatLoadboardClicked" />
                                        </div>
                                    </div>
                                </asp:Panel>
                                <h5>Job Link</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">
                                            Job Number
                                            <%= txtJobNumber.Text.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.Job) 
                                                            ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Job Record'><img src={3} width='20' class='middle'/></a>", 
                                                                                    ResolveUrl(JobView.PageAddress),
                                                                                    WebApplicationConstants.TransferNumber, 
                                                                                    txtJobNumber.Text,
                                                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                            : string.Empty %>
                                        </label>
                                        <eShip:CustomTextBox runat="server" ID="txtJobNumber" Type="NumbersOnly" CssClass="w200" MaxLength="50" OnTextChanged="OnJobNumberTextChanged"
                                            AutoPostBack="True" />
                                        <asp:ImageButton ID="btnFindJob" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                                            CausesValidation="false" OnClick="OnFindJobClicked" />
                                        <asp:ImageButton ID="btnClearJob" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                            CausesValidation="false" OnClick="OnClearJobClicked" Width="20px" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Job Step</label>
                                        <eShip:CustomTextBox runat="server" ID="txtJobStep" CssClass="w70" MaxLength="50" Type="NumbersOnly" />
                                    </div>
                                </div>
                            </div>
                            <div class="col_1_2 pl46 bbox">
                                <h5>Comments</h5>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            General Comments
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleGeneralBolComments" TargetControlId="txtGeneralBolComments" MaxLength="200" />
                                        </label>
                                        <eShip:CustomTextBox ID="txtGeneralBolComments" runat="server" TextMode="MultiLine" CssClass="w420 h150" />
                                    </div>
                                </div>
                                <div class="row pb20">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Critical Comments
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleCriticalBolComments" TargetControlId="txtCriticalBolComments" MaxLength="200" />
                                        </label>
                                        <eShip:CustomTextBox ID="txtCriticalBolComments" runat="server" TextMode="MultiLine" CssClass="w420 h150" />
                                    </div>
                                </div>
                                <h5>Sales Representative Information</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Name</label>
                                        <eShip:CustomHiddenField runat="server" ID="hidSalesRepresentativeId" />
                                        <eShip:CustomTextBox runat="server" ID="txtSalesRepresentative" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Commission (%)</label>
                                        <eShip:CustomHiddenField runat="server" ID="hidSalesRepresentativeCommissionPercent" />
                                        <eShip:CustomTextBox runat="server" ID="txtSalesRepresentativeCommissionPercent" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Minimum Commission ($)</label>
                                        <eShip:CustomHiddenField runat="server" ID="hidSalesRepresentativeMinCommValue" />
                                        <eShip:CustomTextBox runat="server" ID="txtSalesRepresentativeMinCommValue" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Maximum Commission ($)</label>
                                        <eShip:CustomHiddenField runat="server" ID="hidSalesRepresentativeMaxCommValue" />
                                        <eShip:CustomTextBox runat="server" ID="txtSalesRepresentativeMaxCommValue" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                </div>
                                <div class="row pb20">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Additional Entity Name</label>
                                        <eShip:CustomTextBox runat="server" ID="txtSalesRepresentativeAddlEntityName" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Additional Entity Commission (%)</label>
                                        <eShip:CustomHiddenField runat="server" ID="hidSalesRepresentativeAddlEntityCommPercent" />
                                        <eShip:CustomTextBox runat="server" ID="txtSalesRepresentativeAddlEntityCommPercent" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                </div>
                                <asp:UpdatePanel runat="server" ID="upResellerAdditions" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <h5>Reseller Additions</h5>
                                        <div class="row mb10">
                                            <div class="fieldgroup mr20">
                                                <label class="upper blue">
                                                    <asp:Literal runat="server" ID="litBillReseller" />
                                                    <asp:Literal runat="server" ID="litResellerAdditionName" />
                                                </label>
                                            </div>
                                            <div class="fieldgroup right">
                                                <asp:Button runat="server" ID="btnRetrieveDetails" Text="Retrieve" CausesValidation="false" OnClick="OnRetrieveResellerAdditionDetailsClicked" OnClientClick="ShowProcessingDivBlock();" />
                                                <asp:Button runat="server" ID="btnClearDetails" Text="Clear" CausesValidation="false" OnClick="OnClearResellerAdditionDetailsClicked" OnClientClick="ShowProcessingDivBlock();" />
                                            </div>
                                        </div>
                                        <table class="stripe" id="shipmentResellerAdditionsTable">
                                            <tr>
                                                <th style="width: 20%;">Category
                                                </th>
                                                <th style="width: 15%;" class="text-center">Type
                                                </th>
                                                <th style="width: 20%;" class="text-center">Value ($)
                                                </th>
                                                <th style="width: 24%;" class="text-center">Percent (%)
                                                </th>
                                                <th style="width: 20%;" class="text-center">Use Lower
                                                </th>
                                            </tr>
                                            <asp:Repeater runat="server" ID="rptResellerAdditions">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td class="text-right">
                                                            <%# Eval("Header") %>
                                                        </td>
                                                        <td class="text-center">
                                                            <%# Eval("Type") %>
                                                        </td>
                                                        <td class="text-center">
                                                            <%# Eval("Value") %>
                                                        </td>
                                                        <td class="text-center">
                                                            <%# Eval("Percent") %>
                                                        </td>
                                                        <td class="text-center">
                                                            <%# Eval("UseLower").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabCollections" HeaderText="Misc. Items">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upReferences" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlReferences" DefaultButton="btnAddReference" CssClass="rowgroup">
                                <div class="row mb10">
                                    <div class="fieldgroup">
                                        <label class="upper blue fs120em">
                                            Customer References
                                        <asp:Literal runat="server" ID="litReferenceCount" />
                                        </label>
                                    </div>
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddReference" Text="Add Reference" CausesValidation="false" OnClick="OnAddReferenceClicked" OnClientClick="ShowProcessingDivBlock();" />
                                        <asp:Button runat="server" ID="btnClearAllReferences" Text="Clear References" CausesValidation="False" OnClick="OnClearAllReferencesClicked" OnClientClick="ShowProcessingDivBlock();" />
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    function EnforceMutuallyExclusiveCustomerCustomFieldCheckBox(control) {
                                        $(control).parentsUntil("tbody").find("input[id*='_chkDisplayOn'][type='checkbox']").not($(control))
                                            .each(function () {
                                                jsHelper.UnCheckBox($(this).attr("id"));
                                                SetAltUniformCheckBoxClickedStatus($(this));
                                            });
                                    }
                                </script>
                                <table class="stripe">
                                    <tr>
                                        <th style="width: 25%;">Name
                                        </th>
                                        <th style="width: 36%;">Value
                                        </th>
                                        <th style="width: 12%;" class="text-center">Display On Origin
                                        </th>
                                        <th style="width: 12%;" class="text-center">Display On
										        <abbr title="Destination">Dest.</abbr>
                                        </th>
                                        <th style="width: 10%;" class="text-center">Required
                                        </th>
                                        <th style="width: 5%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstReferences" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidReferenceIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <eShip:CustomHiddenField ID="hidReferenceId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <eShip:CustomTextBox ID="txtName" runat="server" Text='<%# Eval("Name") %>' CssClass="w260" MaxLength="50" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtValue" runat="server" Text='<%# Eval("Value") %>' CssClass="w330" MaxLength="50" />
                                                </td>
                                                <td class="text-center">
                                                    <eShip:AltUniformCheckBox ID="chkDisplayOnOrigin" runat="server" Checked='<%# Eval("DisplayOnOrigin") %>' OnClientSideClicked="EnforceMutuallyExclusiveCustomerCustomFieldCheckBox(this);" />
                                                </td>
                                                <td class="text-center">
                                                    <eShip:AltUniformCheckBox ID="chkDisplayOnDestination" runat="server" Checked='<%# Eval("DisplayOnDestination") %>' OnClientSideClicked="EnforceMutuallyExclusiveCustomerCustomFieldCheckBox(this);" />
                                                </td>
                                                <td class="text-center">
                                                    <%# Eval("Required").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                                                    <eShip:CustomHiddenField runat="server" ID="hidRequired" Value='<%# Eval("Required").ToBoolean().GetString() %>' />
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        CausesValidation="false" OnClick="OnDeleteReferenceClicked" Enabled='<%# Access.Modify %>' OnClientClick="ShowProcessingDivBlock();" />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <hr class="dark" />
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $("[id$='ibtnViewAdditionalVendorPerformanceStatistics']").each(function () {
                                jsHelper.SetDoPostBackJsLink($(this).prop('id'));
                            });

                            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                                $("[id$='ibtnViewAdditionalVendorPerformanceStatistics']").each(function () {
                                    jsHelper.SetDoPostBackJsLink($(this).prop('id'));
                                });
                            });
                        });
                    </script>
                    <asp:UpdatePanel runat="server" ID="upVendors" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel ID="pnlVendors" runat="server" DefaultButton="btnAddVendorFromFinder" CssClass="rowgroup">
                                <div class="row mb10">
                                    <div class="fieldgroup">
                                        <label class="upper blue fs120em">
                                            Additional Vendors
                                            <asp:Literal runat="server" ID="litVendorsCount" />
                                        </label>
                                    </div>
                                    <div class="fieldgroup pl20">
                                        <label>
                                            Primary Vendor:
                                            <eShip:CustomTextBox runat="server" ID="txtPrimaryVendor" CssClass="disabled w280" />
                                        </label>
                                    </div>
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddVendorFromFinder" Text="Add Vendor" CausesValidation="false" OnClick="OnAddVendorClicked" />
                                        <asp:Button runat="server" ID="btnClearAllVendors" Text="Clear Vendors" CausesValidation="False" OnClick="OnClearAllVendorsClicked" OnClientClick="ShowProcessingDivBlock();" />
                                    </div>
                                </div>

                                <div class="errorMsgLit">
                                    <asp:Literal runat="server" ID="litVendorsErrorMsg" />
                                </div>

                                <table class="stripe">
                                    <tr>
                                        <th style="width: 5%;">Alerts
                                        </th>
                                        <th style="width: 10%;">Vendor #
                                        </th>
                                        <th style="width: 30%;">Name
                                        </th>
                                        <th style="width: 35%;">Pro Number
                                        </th>
                                        <th style="width: 10%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstVendors" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField runat="server" ID="hidInsuranceAlert" Value='<%# Eval("InsuranceAlert") %>' />
                                                    <eShip:CustomHiddenField runat="server" ID="hidInsuranceTooltip" Value='<%# Eval("InsuranceTooltip") %>' />
                                                    <asp:Image CssClass="shipmentAlert" ID="imgInsuranceAlert" ImageAlign="AbsMiddle" ImageUrl="~/images/icons/Insurance.png"
                                                        runat="server" Width="16px" Visible='<%# Eval("InsuranceAlert").ToBoolean() %>' ToolTip='<%# Eval("InsuranceTooltip") %>' />
                                                </td>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidVendorIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <eShip:CustomHiddenField ID="hidLoadOrderVendorId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <eShip:CustomHiddenField ID="hidVendorId" runat="server" Value='<%# Eval("VendorId") %>' />
                                                    <eShip:CustomTextBox ID="txtVendorNumber" runat="server" Text='<%# Eval("VendorNumber") %>' CssClass="w70 disabled" ReadOnly="True" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtName" runat="server" Text='<%# Eval("Name") %>' CssClass="w300 disabled" ReadOnly="True" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtProNumber" runat="server" Text='<%# Eval("ProNumber") %>' CssClass="w200" MaxLength="50" />
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton runat="server" ID="ibtnLogVendorRejection" ImageUrl="~/images/icons2/cog.png" ToolTip="Delete line and log vendor rejection"
                                                        CausesValidation="False" OnClick="OnLogNonPrimaryVendorRejection" Enabled='<%# Access.Modify %>' OnClientClick="ShowProcessingDivBlock();" />
                                                    <asp:ImageButton runat="server" ID="ibtnViewAdditionalVendorPerformanceStatistics" ImageUrl="~/images/icons2/statsBlue.png" OnClick="OnViewAdditionalVendorPerformanceStatisticsClicked"
                                                        ToolTip="See Vendor Performance Statistics" Width="20px" OnClientClick="ShowProcessingDivBlock();" />
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png" ToolTip="Delete line"
                                                        CausesValidation="false" OnClick="OnDeleteVendorClicked" Enabled='<%# Access.Modify %>' OnClientClick="ShowProcessingDivBlock();" />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnAddVendorFromFinder" />
                        </Triggers>
                    </asp:UpdatePanel>

                    <hr class="dark" />

                    <asp:UpdatePanel runat="server" ID="upAccountBuckets" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel ID="pnlAccountBuckets" runat="server" DefaultButton="btnAddAccountbucket" CssClass="rowgroup">
                                <div class="row mb10">
                                    <div class="fieldgroup">
                                        <label class="upper blue fs120em">
                                            Additional Account Buckets<asp:Literal runat="server" ID="litAccountBucketCount" />
                                        </label>
                                    </div>
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddAccountbucket" Text="Add Account Bucket" CausesValidation="false" OnClick="OnAddAccountBucketClicked" />
                                        <asp:Button runat="server" ID="btnClearAllAccountBuckets" Text="Clear Account Buckets" CausesValidation="False" OnClick="OnClearAllAccountBucketsClicked" OnClientClick="ShowProcessingDivBlock();" />
                                    </div>
                                </div>
                                <table class="stripe">
                                    <tr>
                                        <th style="width: 20%;">Code
                                        </th>
                                        <th style="width: 70%;">Description
                                        </th>
                                        <th style="width: 10%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstAccountBuckets" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidAccountBucketIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <eShip:CustomHiddenField ID="hidLoadOrderAccountBucketId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <eShip:CustomHiddenField ID="hidAccountBucketId" runat="server" Value='<%# Eval("AccountBucketId") %>' />
                                                    <eShip:CustomTextBox ID="txtAccountBucketCode" runat="server" Text='<%# Eval("AccountBucketCode") %>' ReadOnly="True" CssClass="w150 disabled" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtAccountBucketDescription" runat="server" Text='<%# Eval("AccountBucketDescription") %>' ReadOnly="True" CssClass="w300 disabled" />
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png" OnClientClick="ShowProcessingDivBlock();"
                                                        CausesValidation="false" OnClick="OnDeleteAccountBucketClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnAddAccountbucket" />
                        </Triggers>
                    </asp:UpdatePanel>

                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabNotes" HeaderText="Notes">
                <ContentTemplate>

                    <asp:UpdatePanel ID="upPnlNotes" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>

                            <asp:Panel ID="pnlNotes" runat="server" DefaultButton="btnAddNote" CssClass="rowgroup">
                                <div class="row mb10">
                                    <div class="fieldgroup left">
                                        <eShip:PaginationExtender runat="server" ID="peNotes" TargetControlId="lstNotes" PageSize="5" OnPageChanging="OnPeNotesPageChanging" />
                                    </div>
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddNote" Text="Add Note" OnClick="OnAddNoteClicked"
                                            ToolTip="Add Note" OnClientClick="ShowProcessingDivBlock();" CausesValidation="False" />
                                    </div>
                                </div>
                                <div class="row">
                                    <eShip:TableFreezeHeaderExtender runat="server" ID="tfheShipmentNotesTable" TableId="shipmentNotesTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                    <table class="stripe" id="shipmentNotesTable">
                                        <tr>
                                            <th style="width: 15%;">Type
                                            </th>
                                            <th style="width: 15%;">User
                                            </th>
                                            <th style="width: 10%;" class="text-center">Archived
                                            </th>
                                            <th style="width: 10%;" class="text-center">Classified
                                            </th>
                                            <th style="width: 40%;">Message
                                            </th>
                                            <th style="width: 10%" class="text-center">Action
                                            </th>
                                        </tr>
                                        <asp:ListView ID="lstNotes" runat="server" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnNotesItemDataBound">
                                            <LayoutTemplate>
                                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                            </LayoutTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td class="top">
                                                        <eShip:CustomHiddenField ID="hidNoteIndex" runat="server" Value='<%# Container.DataItemIndex + (peNotes.PageSize*(peNotes.CurrentPage-1)) %>' />
                                                        <eShip:CustomHiddenField ID="hidNoteId" runat="server" Value='<%# Eval("Id") %>' />
                                                        <eShip:CustomHiddenField ID="hidLoadOrderNote" runat="server" Value='<%# Eval("IsLoadOrderNote") %>' />
                                                        <asp:DropDownList runat="server" ID="ddlNoteType" DataValueField="Value" DataTextField="Text" CssClass="w110" />
                                                    </td>
                                                    <td class="top">
                                                        <eShip:CustomTextBox ID="txtUser" runat="server" Text='<%# Eval("Username") %>' ReadOnly="True" CssClass="w150 disabled" />
                                                    </td>
                                                    <td class="text-center top">
                                                        <eShip:AltUniformCheckBox runat="server" ID="chkArchived" Checked='<%# Eval("Archived") %>' />
                                                    </td>
                                                    <td class="text-center top">
                                                        <eShip:AltUniformCheckBox runat="server" ID="chkClassified" Checked='<%# Eval("Classified") %>' />
                                                    </td>
                                                    <td class="top">
                                                        <eShip:CustomTextBox ID="txtMessage" runat="server" Text='<%# Eval("Message") %>' TextMode="MultiLine" CssClass='<%# Eval("Id").ToLong() == default(long) ? "w430" : "w430 disabled" %>' ReadOnly='<%# Eval("Id").ToLong() != default(long) %>' />
                                                        <label class="lhInherit mb5">
                                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleMessage" MaxLength="500" TargetControlId="txtMessage" />
                                                        </label>
                                                    </td>
                                                    <td class="text-center top">
                                                        <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png" CausesValidation="false" OnClick="OnDeleteUnsavedNoteClicked" Enabled='<%# Access.Modify %>'
                                                            Visible='<%# Eval("Id").ToLong() == default(long) %>' OnClientClick="ShowProgressNotice(true);" />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </table>
                                </div>
                            </asp:Panel>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabStops" HeaderText="Stops">
                <ContentTemplate>

                    <asp:UpdatePanel runat="server" ID="upPnlStops" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlStops">
                                <div class="row mb20">
                                    <div class="fieldgroup left" id="stopsInstructionOptionDiv">

                                        <script type="text/javascript">
                                            function EnforceMutuallyStopsInstructionCheckBox(control) {

                                                $("#stopsInstructionOptionDiv").find("input[type='checkbox']").not($(control))
                                                    .each(function () {
                                                        jsHelper.UnCheckBox($(this).attr("id"));
                                                        SetAltUniformCheckBoxClickedStatus($(this));
                                                    });
                                            }

                                        </script>

                                        <eShip:AltUniformCheckBox runat="server" ID="chkOriginInstruction" Checked="true" OnClientSideClicked="EnforceMutuallyStopsInstructionCheckBox(this);" />
                                        <label class="mr20">Use origin Instruction</label>
                                        <eShip:AltUniformCheckBox runat="server" ID="chkDestinationInstruction" OnClientSideClicked="EnforceMutuallyStopsInstructionCheckBox(this);" />
                                        <label>Use destination Instruction</label>
                                    </div>
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddStop" Text="Add Stop" CausesValidation="false" OnClick="OnAddLocationClicked" OnClientClick="ShowProcessingDivBlock();" />
                                        <asp:Button runat="server" ID="btnAddStopFromAddressBook" Text="Add Stop from Address Book"
                                            CausesValidation="false" OnClick="OnAddStopFromAddressBookClicked" />
                                    </div>
                                </div>

                                <asp:ListView ID="lstStops" runat="server" OnItemDataBound="OnStopsItemDataBound"
                                    ItemPlaceholderID="itemPlaceHolder">
                                    <LayoutTemplate>
                                        <table class="stripe">
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </table>
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tr class="bottom_shadow">
                                            <td class="pt100 text-center top">
                                                <div class="border-right pr5">
                                                    <asp:ImageButton ID="ibtnMoveUp" runat="server" ImageUrl="~/images/icons2/arrowUp.png"
                                                        OnClick="OnMoveUpClicked" CausesValidation="false" OnClientClick="ShowProcessingDivBlock();" />
                                                    <br />
                                                    <b class="mt10 mb10 fs200em"><%# Container.DataItemIndex + 1 %></b>
                                                    <br />
                                                    <asp:ImageButton ID="ibtnMoveDown" runat="server" ImageUrl="~/images/icons2/arrowDown.png"
                                                        OnClick="OnMoveDownClicked" CausesValidation="false" OnClientClick="ShowProcessingDivBlock();" />
                                                </div>
                                            </td>
                                            <td>
                                                <eShip:LocationListingInputControl ID="llicLocation" runat="server" LocationItemIndex='<%# Container.DataItemIndex %>' OnDeleteLocation="OnDeleteLocation" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnAddStopFromAddressBook" />
                        </Triggers>
                    </asp:UpdatePanel>

                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel ID="tabDocuments" runat="server" HeaderText="Documents">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlDocuments" DefaultButton="btnAddDocument">
                        <div class="row mb10">
                            <div class="fieldgroup right">
                                <asp:Button runat="server" ID="btnAddDocument" Text="Add Document" CausesValidation="false" OnClick="OnAddDocumentClicked" />
                                <asp:Button runat="server" ID="btnClearAllDocuments" Text="Clear Documents" CausesValidation="False" OnClick="OnClearAllDocumentsClicked" />
                            </div>
                        </div>
                        <script type="text/javascript">
                            $(document).ready(function () {
                                $("a[id$='lnkLoadOrderDocumentLocationPath']").each(function () {
                                    jsHelper.SetDoPostBackJsLink($(this).prop('id'));
                                });
                            });
                        </script>
                        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheLoadOrderDocumentTable" TableId="loadOrderDocumentTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                        <table class="stripe" id="loadOrderDocumentTable">
                            <tr>
                                <th style="width: 5%;" class="text-center">Internal
                                </th>
                                <th style="width: 18%;">Name
                                </th>
                                <th style="width: 22%;">Description
                                </th>
                                <th style="width: 28%;">Document Tag
                                </th>
                                <th style="width: 19%;">File
                                </th>
                                <th style="width: 8%;" class="text-center">Action
                                </th>
                            </tr>
                            <asp:ListView ID="lstDocuments" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                <LayoutTemplate>
                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td class="text-center">
                                            <%# Eval("IsInternal").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                                            <eShip:CustomHiddenField runat="server" ID="hidIsInternal" Value='<%# Eval("IsInternal").ToBoolean().GetString() %>' />
                                        </td>
                                        <td>
                                            <eShip:CustomHiddenField ID="hidDocumentIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                            <eShip:CustomHiddenField ID="hidDocumentId" runat="server" Value='<%# Eval("Id") %>' />
                                            <asp:Literal ID="litDocumentName" runat="server" Text='<%# Eval("Name") %>' />
                                        </td>
                                        <td>
                                            <asp:Literal ID="litDocumentDescription" runat="server" Text='<%# Eval("Description") %>' />
                                        </td>
                                        <td>
                                            <eShip:CustomHiddenField ID="hidDocumentTagId" runat="server" Value='<%# Eval("DocumentTagId") %>' />
                                            <asp:Literal ID="litDocumentTag" runat="server" Text='<%# new DocumentTag(Eval("DocumentTagId").ToLong()).Description %>' />
                                        </td>
                                        <td>
                                            <eShip:CustomHiddenField ID="hidLocationPath" runat="server" Value='<%# Eval("LocationPath") %>' />
                                            <asp:LinkButton runat="server" ID="lnkLoadOrderDocumentLocationPath" Text='<%# GetLocationFileName(Eval("LocationPath")) %>'
                                                OnClick="OnLocationPathClicked" CausesValidation="false" CssClass="blue" />
                                        </td>
                                        <td class="text-center">
                                            <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png"
                                                CausesValidation="false" OnClick="OnEditDocumentClicked" Enabled='<%# Access.Modify %>' />
                                            <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                CausesValidation="false" OnClick="OnDeleteDocumentClicked" Enabled='<%# Access.Modify %>' />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:ListView>
                        </table>
                        <asp:Panel runat="server" ID="pnlEditDocument" Visible="false">
                            <div class="popup popupControlOverW500">
                                <div class="popheader">
                                    <h4>Add/Modify Document
                                        <asp:ImageButton ID="ibtnCloseEditDocument" ImageUrl="~/images/icons2/close.png" CssClass="close"
                                            CausesValidation="false" OnClick="OnCloseEditDocumentClicked" runat="server" />
                                    </h4>
                                </div>
                                <div class="row pt10">
                                    <table class="poptable">
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Name:</label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox runat="server" ID="txtDocumentName" CssClass="w200" MaxLength="50" />
                                                <asp:CheckBox runat="server" ID="chkDocumentIsInternal" CssClass="jQueryUniform ml10" />
                                                <label>Is Internal</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Document Tag:</label>
                                            </td>
                                            <td>
                                                <eShip:CachedObjectDropDownList Type="DocumentTag" ID="ddlDocumentTag" runat="server" CssClass="w300"
                                                    EnableChooseOne="True" DefaultValue="0" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right top">
                                                <label class="upper">Description:</label>
                                                <br />
                                                <label class="lhInherit">
                                                    <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDocumentDescription" TargetControlId="txtDocumentDescription" MaxLength="500" />
                                                </label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox runat="server" ID="txtDocumentDescription" CssClass="w300 h150" TextMode="MultiLine" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Location Path:</label>
                                            </td>
                                            <td>
                                                <eShip:CustomHiddenField ID="hidLocationPath" runat="server" />
                                                <asp:FileUpload runat="server" ID="fupLocationPath" CssClass="jQueryUniform" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Current File:</label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox ID="txtLocationPath" runat="server" ReadOnly="True" CssClass="w300 disabled" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <asp:Button ID="btnEditDocumentDone" Text="Done" OnClick="OnEditDocumentDoneClicked" runat="server" CausesValidation="false" />
                                                <asp:Button ID="btnCloseEditDocument" Text="Cancel" OnClick="OnCloseEditDocumentClicked" runat="server" CausesValidation="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </asp:Panel>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabAuditLog" HeaderText="Audit Log">
                <ContentTemplate>
                    <eShip:AuditLogControl ID="auditLogs" runat="server" />
                </ContentTemplate>
            </ajax:TabPanel>
        </ajax:TabContainer>
    </div>

    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
    <asp:Panel ID="pnlDimScreenJS" CssClass="dimBackground" runat="server" Style="display: none;" />
    <asp:UpdatePanel runat="server" ID="upHiddens" UpdateMode="Always">
        <ContentTemplate>
            <eShip:CustomHiddenField runat="server" ID="hidFlag" />
            <eShip:CustomHiddenField runat="server" ID="hidCustomerOutstandingBalance" />
            <eShip:CustomHiddenField runat="server" ID="hidCustomerCredit" />
            <eShip:CustomHiddenField runat="server" ID="hidEditingIndex" />
            <eShip:CustomHiddenField runat="server" ID="hidNotificationsRequired" />
            <eShip:CustomHiddenField runat="server" ID="hidFilesToDelete" />
            <eShip:CustomHiddenField runat="server" ID="hidResellerAdditionId" />
            <eShip:CustomHiddenField runat="server" ID="hidBillReseller" />
            <eShip:CustomHiddenField runat="server" ID="hidEditStatus" />
            <eShip:CustomHiddenField runat="server" ID="hidLoadedShipmentTotalDue" />
            <eShip:CustomHiddenField runat="server" ID="hidLoadTenderDisplayFlag" />
            <eShip:CustomHiddenField runat="server" ID="hidEdiStatus" />
            <eShip:CustomHiddenField runat="server" ID="hidLoadOrderStatusChangedToCancelled" />
            <eShip:CustomHiddenField runat="server" ID="hidMiscReceiptsRelatedToLoadOrderExist" />
            <eShip:CustomHiddenField runat="server" ID="hidDatAssetId" />
            <eShip:CustomHiddenField runat="server" ID="hidJobId" />
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
