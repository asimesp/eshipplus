﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LibraryItemFinderControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Operations.Controls.LibraryItemFinderControl" %>

<asp:Panel runat="server" ID="pnlLibraryItemFinderContent" CssClass="popupControl" DefaultButton="btnSearch">
    <div class="popheader">
        <h4>Library Item Finder <small class="ml10">
            <asp:Literal runat="server" ID="litRecordCount" Text="" /></small>
            <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/icons2/close.png" CssClass="close"
                CausesValidation="false" OnClick="OnCancelClicked" runat="server" /></h4>
    </div>
    <script type="text/javascript">
        $(function () {
            if (jsHelper.IsTrue($('#<%= hidAreFiltersShowing.ClientID%>').val())) {
                $('#<%= pnlSearch.ClientID%>').show();
                $('#<%= pnlSearchShowFilters.ClientID%>').hide();
            } else {
                $('#<%= pnlSearch.ClientID%>').hide();
                $('#<%= pnlSearchShowFilters.ClientID%>').show();
            }
        });

        function ToggleFilters() {
            $('#<%= hidAreFiltersShowing.ClientID%>').val(jsHelper.IsTrue($('#<%= hidAreFiltersShowing.ClientID%>').val()) ? false : true);
            $('#<%= pnlSearch.ClientID%>').toggle();
            $('#<%= pnlSearchShowFilters.ClientID%>').toggle();
            var top = $('#<%= pnlLibraryItemFinderContent.ClientID%> div[class="finderScroll"]').offset().top - $('#<%= pnlLibraryItemFinderContent.ClientID%>').offset().top - 2;
            $('#<%= pnlLibraryItemFinderContent.ClientID%> div[class="finderScroll"] > div[id^="hd"]').css('top', top + 'px');
        }
    </script>
    <table id="finderTable" class="poptable">
        <tr>
            <td>
                <asp:Panel runat="server" ID="pnlSearchShowFilters" class="rowgroup mb0" style="display:none;">
                    <div class="row">
                        <div class="fieldgroup">
                            <asp:Button ID="btnSelectAll2" runat="server" Text="ADD SELECTED" OnClick="OnSelectAllClicked" CssClass="ml10" CausesValidation="false" />
                        </div>
                        <div class="fieldgroup right">
                            <button onclick="ToggleFilters(); return false;">Show Filters</button>
                        </div>
                        <div class="fieldgroup right mb5 mt5 mr10">
                            <span class="fs75em  ">*** If you would like to show parameters when searching change the 'Always Show Finder Parameters On Search' option on your <asp:HyperLink ID="hypGoToUserProfile" Target="_blank" CssClass="blue" runat="server" Text="User Profile"/></span>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
                    <div class="mt5">
                        <div class="rowgroup mb0">
                            <div class="row">
                                <div class="fieldgroup">
                                    <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                                        CausesValidation="False" CssClass="add" />
                                </div>
                                <div class="fieldgroup right">
                                    <button onclick="ToggleFilters(); return false;">Hide Filters</button>
                                </div>
                                <div class="right">
                                    <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="LibraryItemsFinder" ShowAutoRefresh="False"
                                        OnProcessingError="OnSearchProfilesProcessingError" OnSearchProfileSelected="OnSearchProfilesProfileSelected" />
                                </div>
                            </div>
                        </div>
                        <hr class="mb5" />
                        <script type="text/javascript">$(window).load(function () { SetControlFocus(); });</script>
                        <table class="mb5" id="parametersTable">
                            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                                OnItemDataBound="OnParameterItemDataBound">
                                <LayoutTemplate>
                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                                        OnRemoveParameter="OnParameterRemove" />
                                </ItemTemplate>
                            </asp:ListView>
                            <tr>
                                <td colspan="4">
                                    <asp:Button ID="btnSearch" CssClass="left" runat="server" Text="SEARCH" OnClick="OnSearchClicked" CausesValidation="False" />
                                    <asp:Button ID="btnCancel" OnClick="OnCancelClicked" runat="server" Text="CANCEL" CssClass="ml10" CausesValidation="false" />
                                    <asp:Button ID="btnSelectAll" runat="server" Text="ADD SELECTED" OnClick="OnSelectAllClicked" CssClass="ml10" CausesValidation="false" />
                                    <asp:CheckBox runat="server" ID="chkConvertPieceCountToQuantity" CssClass="jQueryUniform ml10" />
                                    <label class="blue">Convert Piece Count To Quantity</label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel runat="server" ID="upDataUpdate">
        <ContentTemplate>
            <eShip:ContinuousScrollExtender ID="upcseDataUpdate" runat="server" BindableControlId="lstSearchResults" UpdatePanelToExtendId="upDataUpdate" ScrollableDivId="libraryItemfinderScrollSection"
                SelectionCheckBoxControlId="chkSelected" SelectionUniqueRefHiddenFieldId="hidLibraryItemId" />
            <eShip:TableFreezeHeaderExtender runat="server" ID="tfheGroupsTable" TableId="libraryItemFinderTable" ScrollerContainerId="libraryItemfinderScrollSection" ScrollOffsetControlId="pnlLibraryItemFinderContent"
                IgnoreHeaderBackgroundFill="True" HeaderZIndex="2050" />
            <div class="finderScroll" id="libraryItemfinderScrollSection">
                <asp:ListView ID="lstSearchResults" runat="server" ItemPlaceholderID="itemPlaceHolder">
                    <LayoutTemplate>
                        <table id="libraryItemFinderTable" class="stripe sm_chk">
                            <tr>
                                <th style="width: 15%">
                                    <eShip:AltUniformCheckBox ID="chkSelectAllRecords" runat="server" OnClientSideClicked="javascript:SetSelectedCheckBoxesStatus(this, 'libraryItemFinderTable');" />
                                </th>
                                <th style="width: 8%;">Weight (lb)
                                </th>
                                <th style="width: 8%;">Length (in)
                                </th>
                                <th style="width: 8%;">Width (in)
                                </th>
                                <th style="width: 8%;">Height (in)
                                </th>
                                <th style="width: 5%;">Package Qty.
                                </th>
                                <th style="width: 5%;">Pieces
                                </th>
                                <th style="width: 8%;">NMFC
                                </th>
                                <th style="width: 12%;">Packaging
                                </th>
                                <th style="width: 33%;">Customer
                                </th>
                            </tr>
                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td rowspan="2" class="top">
                                <asp:Button ID="btnSelect" runat="server" Text="Select" OnClick="OnSelectClicked"
                                    CausesValidation="false" Visible='<%# !EnableMultiSelection %>' />
                                <asp:Button ID="btnEditSelect" runat="server" Text="Edit" OnClick="OnEditSelectClicked"
                                    CausesValidation="false" Visible='<%# !EnableMultiSelection && OpenForEditEnabled %>' />
                                <eShip:AltUniformCheckBox runat="server" ID="chkSelected" Visible='<%# EnableMultiSelection %>' OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'libraryItemFinderTable', 'chkSelectAllRecords');" />
                                <eShip:CustomHiddenField ID="hidLibraryItemId" Value='<%# Eval("Id") %>' runat="server" />
                            </td>
                            <td>
                                <%# Eval("Weight")  %>
                            </td>
                            <td>
                                <%# Eval("Length")  %>
                            </td>
                            <td>
                                <%# Eval("Width")  %>
                            </td>
                            <td>
                                <%# Eval("Height")  %>
                            </td>
                            <td>
                                <%# Eval("Quantity")  %>
                            </td>
                            <td>
                                <%# Eval("PieceCount")  %>
                            </td>
                            <td>
                                <%# Eval("NMFCCode") %>
                            </td>
                            <td>
                                <%# Eval("PackageTypeName") %>
                            </td>
                            <td>
                                <%# Eval("CustomerNumber") + " - " + Eval("CustomerName") %>
                            </td>
                        </tr>
                        <tr class="hidden">
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="9">
                                <div class="col_1_2 bbox">
                                    <div class="row">
                                        <div class="fieldgroup">
                                            <label class="wlabel blue">Description:</label>
                                            <%# Eval("Description") %>
                                        </div>
                                    </div>
                                </div>
                                <div class="col_1_2 bbox">
                                    <div class="row">
                                        <div class="fieldgroup">
                                            <label class="wlabel blue">
                                                <abbr title="Harmonized Tariff Schedule">
                                                    HTS</abbr>
                                                Code:
                                            </label>
                                            <%# Eval("HTSCode") %>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lstSearchResults" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Panel>
<asp:Panel runat="server" ID="pnlConvertPieceCountToQuantity" CssClass="popupControl popupControlOverW500" Visible="False">
    <div class="popheader">
        <h4>Convert Piece Count To Quantity For Selected Items</h4>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('input:text[id$="txtPieceCount"]').change(function () {
                var txtQuantity = $(this).parent().parent().find('input:text[id$="txtQuantity"]');
                var originalPieceCount = $(this).parent().find('input:text[id*="hidOriginalPieceCount"]').val();
                var originalQuantity = $(this).parent().parent().find('input:text[id*="hidOriginalQuantity"]').val();
                var pieceCount = $(this).val();
                if (originalPieceCount == 0 || originalQuantity == 0 || pieceCount == 0) {
                    txtQuantity.val('1');
                    return;
                }
                txtQuantity.val(Math.ceil(pieceCount / (originalPieceCount / originalQuantity)));
            });

            $('input:text[id$="txtQuantity"]').change(function () {
                var txtPieceCount = $(this).parent().parent().find('input:text[id$="txtPieceCount"]');
                var originalPieceCount = $(this).parent().parent().find('input:text[id*="hidOriginalPieceCount"]').val();
                var originalQuantity = $(this).parent().find('input:text[id*="hidOriginalQuantity"]').val();
                if (originalPieceCount == 0 || originalQuantity == 0) {
                    txtPieceCount.val('0');
                    return;
                }

                txtPieceCount.val($(this).val() * (originalPieceCount / originalQuantity));
            });
        });
    </script>
    <div class="finderScroll">
        <table class="stripe">
            <tr>
                <th style="width: 60%;">Description
                </th>
                <th style="width: 20%;">Pieces
                </th>
                <th style="width: 20%;">Package Qty.
                </th>
            </tr>
            <asp:Repeater runat="server" ID="rptConvertPieceCountToQuantityItems">
                <ItemTemplate>
                    <tr>
                        <td class="top">
                            <eShip:CustomHiddenField ID="hidLibraryItemId" Value='<%# Eval("Id") %>' runat="server" />
                            <%# Eval("Description") %>
                        </td>
                        <td class="top">
                            <eShip:CustomHiddenField runat="server" ID="hidOriginalPieceCount" Value='<%# Eval("PieceCount") %>' />
                            <eShip:CustomTextBox runat="server" ID="txtPieceCount" CssClass="w60" Type="NumbersOnly" Text='<%# Eval("PieceCount")  %>' />
                        </td>
                        <td class="top">
                            <eShip:CustomHiddenField runat="server" ID="hidOriginalQuantity" Value='<%# Eval("Quantity") %>' />
                            <eShip:CustomTextBox runat="server" ID="txtQuantity" CssClass="w60" Type="NumbersOnly" Text='<%# Eval("Quantity")  %>' />
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
    <table class="poptable">
        <tr>
            <td style="width: 70%"></td>
            <td class="pt10 pb10">
                <asp:Button runat="server" ID="btnDoneConvertingPieceCountToQuantity" Text="Done" OnClick="OnDoneConvertingPieceCountToQuantityClicked" CausesValidation="False"/>
                <asp:Button runat="server" ID="btnCancelConvertingPieceCountToQuantity" Text="Cancel" OnClick="OnCancelConvertingPieceCountToQuantityClicked" CausesValidation="False"/>
            </td>
        </tr>
    </table>
</asp:Panel>
<eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
<asp:Panel ID="pnlLibraryItemFinderDimScreen" CssClass="dimBackgroundControl" runat="server"
    Visible="false" />
<eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
<eShip:CustomHiddenField runat="server" ID="hidEditSelected" />
<eShip:CustomHiddenField runat="server" ID="hidOpenForEditEnabled" />
<eShip:CustomHiddenField runat="server" ID="hidCustomerIdSpecificFilter" />
<eShip:CustomHiddenField runat="server" ID="hidLibraryItemFinderEnableMultiSelection" />
<eShip:CustomHiddenField runat="server" ID="hidAreFiltersShowing" Value="true" />
