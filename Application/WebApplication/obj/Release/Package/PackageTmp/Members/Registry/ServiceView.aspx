﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="ServiceView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Registry.ServiceView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.Registry" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.Project44" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        OnNew="OnNewClick" OnImport="OnImportClicked" OnExport="OnExportClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Services<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>
        <hr class="fat" />
        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheServiceTable" TableId="serviceTable" HeaderZIndex="2"/>
        <asp:ListView ID="lvwRegistry" runat="server" ItemPlaceholderID="itemPlaceHolder">
            <LayoutTemplate>
                <div class="rowgroup">
                    <table id="serviceTable" class="line2 pl2">
                        <tr>
                            <th style="width: 8%;">Code
                            </th>
                            <th style="width: 28%;">Description
                            </th>
                            <th style="width: 8%;">Category
                            </th>
                            <th style="width: 12%;">Project 44 Code
                            </th>
                            <th style="width: 8%;">Dispatch Flag
                            </th>
                            <th style="width: 3%;" class="text-center">Pickup
                            </th>
                            <th style="width: 3%;" class="text-center">Delivery
                            </th>
                            <th style="width: 20%;">Charge Code
                            </th>
                            <th style="width: 10%;" class="text-center">Action
                            </th>
                        </tr>
                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                    </table>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <eShip:CustomHiddenField ID="serviceId" runat="server" Value='<%# Eval("Id") %>' />
                        <asp:Literal ID="litCode" runat="server" Text='<%# Eval("Code") %>' />
                    </td>
                    <td>
                        <asp:Literal ID="litDescription" runat="server" Text='<%# Eval("Description") %>' />
                    </td>
                    <td>
                        <asp:Literal ID="litCategory" runat="server" Text='<%# Eval("Category") %>' />
                    </td>
                    <td>
                        <%#  Eval("Project44Code").ToInt().ToEnum<Project44ServiceCode>().GetServiceCodeDescription() %>
                    </td>
                    <td>
                        <%# Eval("DispatchFlag").FormattedString() %>
                        <eShip:CustomHiddenField ID="hidDispatchFlag" runat="server" Value='<%# Eval("DispatchFlag") %>' />
                    </td>
                     <td class="text-center">
                         <%# Eval("ApplicableAtPickup").ToBoolean() ? WebApplicationConstants.HtmlCheck : string.Empty %>
                        <eShip:CustomHiddenField ID="hidPickup" runat="server" Value='<%# Eval("ApplicableAtPickup") %>' />
                    </td>
                     <td class="text-center">
                         <%# Eval("ApplicableAtDelivery").ToBoolean() ? WebApplicationConstants.HtmlCheck : string.Empty %>
                        <eShip:CustomHiddenField ID="hidDelivery" runat="server" Value='<%# Eval("ApplicableAtDelivery") %>' />
                    </td>
                    <td>
                        <asp:Literal ID="litChargeCode" runat="server" Text='<%# new ChargeCode(Eval("ChargeCodeId").ToLong()).Description %>' />
                    </td>
                    <td class="text-center">
                        <asp:ImageButton ID="ibtnEdit" runat="server" ToolTip="Edit" ImageUrl="~/images/icons2/editBlue.png"
                            CausesValidation="false" OnClick="OnEditClicked" Enabled='<%# Access.Modify %>' />
                        <asp:ImageButton ID="ibtnDelete" runat="server" ToolTip="Delete" ImageUrl="~/images/icons2/deleteX.png"
                            CausesValidation="false" OnClick="OnDeleteClicked" Enabled='<%# Access.Remove %>' />
                        <ajax:ConfirmButtonExtender runat="server" ID="cbeDelete" TargetControlID="ibtnDelete"
                            ConfirmText="Are you sure you want to delete record?" />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
        <asp:Panel ID="pnlEdit" runat="server" Visible="false" DefaultButton="btnSave">
            <div class="popup popupControlOverW500">
                <div class="popheader">
                    <h4>
                        <asp:Literal ID="litTitle" runat="server" />
                    </h4>
                    <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                        CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
                </div>
                <div class="row">
                    <table class="poptable">
                        <tr>
                            <td colspan="20" class="red">
                                <asp:Literal ID="litMessage" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Code:</label>
                            </td>
                            <td>
                                <eShip:CustomHiddenField ID="hidServiceId" runat="server" />
                                <eShip:CustomTextBox ID="txtCode" CssClass="w170" MaxLength="50" runat="server" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvCode" ControlToValidate="txtCode"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right top">
                                <label class="upper">
                                    Description:
                                </label>
                                <br />
                                <label class="lhInherit">
                                    <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDescription" TargetControlId="txtDescription" MaxLength="200" />
                                </label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtDescription" runat="server" CssClass="w300" TextMode="MultiLine" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvDescription" ControlToValidate="txtDescription"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Category:</label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlCategories" DataTextField="Value" DataValueField="Key" runat="server" CssClass="w300" />
                            </td>
                        </tr>
                         <tr>
                            <td class="text-right">
                                <label class="upper">Applies At:</label>
                            </td>
                            <td>
                                <asp:CheckBox runat="server" ID="chkPickup" CssClass="jQueryUniform" CausesValidation="False" />
                                <label class="upper mr20">Pickup</label>
                                <asp:CheckBox runat="server" ID="chkDelivery" CssClass="jQueryUniform" CausesValidation="False" />
                                <label class="upper">Delivery</label>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Dispatch Flag:</label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlDispatchFlag" DataTextField="Value" DataValueField="Key" runat="server" CssClass="w300" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Charge Code:</label>
                            </td>
                            <td>
                                <eShip:CachedObjectDropDownList Type="ChargeCodes" ID="ddlChargeCodes" runat="server" CssClass="w300"  DefaultValue="0"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Project 44 Code:</label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlProject44Code" DataTextField="Value" DataValueField="Key" runat="server" CssClass="w300" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="pt10">
                                <asp:Button ID="btnSave" Text="Save" OnClick="OnSaveClick" runat="server" />
                                <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnCloseClicked" runat="server"
                                    CausesValidation="false" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
        <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information" OnOkay="OnOkayProcess" />
        <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
            OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
        <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
    </div>
</asp:Content>
