﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="PackageTypeView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Registry.PackageTypeView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        OnNew="OnNewClick" OnImport="OnImportClicked" OnExport="OnExportClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
                Package Types<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>
        <hr class="fat" />
        <eShip:TableFreezeHeaderExtender runat="server" ID="tfhePackageTypeTable" TableId="packageTypeTable" HeaderZIndex="2"/>
        <asp:ListView ID="lvwRegistry" runat="server" ItemPlaceholderID="itemPlaceHolder">
            <LayoutTemplate>
                <div class="rowgroup">
                    <table id="packageTypeTable" class="line2 pl2">
                        <tr>
                            <th style="width: 25%;">Type Name
                            </th>
                            <th style="width: 5%;">
                                <abbr title="EDI Order Identification Detail Code">EDI OID</abbr>
                            </th>
                            <th style="width: 13%;" class="text-right">Default Length
                            <abbr title="Inches">(in)</abbr>
                            </th>
                            <th style="width: 13%;" class="text-right">Default Width
                            <abbr title="Inches">(in)</abbr>
                            </th>
                            <th style="width: 13%;" class="text-right">Default Height
                            <abbr title="Inches">(in)</abbr>
                            </th>
                            <th style="width: 10%;" class="text-right">Sort Weight
                            </th>
                            <th style="width: 8%;" class="text-center">Can Edit
                            <abbr title="Dimensions">Dims</abbr>
                            </th>
                            <th style="width: 5%;" class="text-center">Active
                            </th>
                            <th style="width: 8%;">Action
                            </th>
                        </tr>
                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                    </table>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <eShip:CustomHiddenField ID="packageTypeId" runat="server" Value='<%# Eval("Id") %>' />
                        <asp:Literal ID="litTypeName" runat="server" Text='<%# Eval("TypeName") %>' />
                    </td>
                    <td>
                        <asp:Literal ID="litEdiOid" runat="server" Text='<%# Eval("EdiOid") %>' />
                    </td>
                    <td class="text-right">
                        <asp:Literal ID="litDefaultLength" runat="server" Text='<%# Eval("DefaultLength") %>' />
                    </td>
                    <td class="text-right">
                        <asp:Literal ID="litDefaultWidth" runat="server" Text='<%# Eval("DefaultWidth") %>' />
                    </td>
                    <td class="text-right">
                        <asp:Literal ID="litDefaultHeight" runat="server" Text='<%# Eval("DefaultHeight") %>' />
                    </td>
                    <td class="text-right">
                        <asp:Literal ID="litSortWeight" runat="server" Text='<%# Eval("SortWeight") %>' />
                    </td>
                    <td class="text-center">
                        <%# Eval("EditableDimensions").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                        <eShip:CustomHiddenField runat="server" ID="hidEditableDimensions" Value='<%# Eval("EditableDimensions").ToBoolean().GetString() %>' />
                    </td>
                    <td class="text-center">
                        <%# Eval("Active").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                        <eShip:CustomHiddenField runat="server" ID="hidActive" Value='<%# Eval("Active").ToBoolean().GetString() %>' />
                    </td>
                    <td>
                        <asp:ImageButton ID="ibtnEdit" runat="server" ToolTip="Edit" ImageUrl="~/images/icons2/editBlue.png"
                            CausesValidation="false" OnClick="OnEditClicked" Enabled='<%# Access.Modify %>' />
                        <asp:ImageButton ID="ibtnDelete" runat="server" ToolTip="Delete" ImageUrl="~/images/icons2/deleteX.png"
                            CausesValidation="false" OnClick="OnDeleteClicked" Enabled='<%# Access.Remove %>' />
                        <ajax:ConfirmButtonExtender runat="server" ID="cbeDelete" TargetControlID="ibtnDelete"
                            ConfirmText="Are you sure you want to delete record?" />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
        <asp:Panel ID="pnlEdit" runat="server" Visible="false" DefaultButton="btnSave">
            <div class="popup popupControlOverW750">
                <div class="popheader">
                    <h4>
                        <asp:Literal ID="litTitle" runat="server" />
                    </h4>
                    <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                        CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
                </div>
                <div class="row">
                    <table class="poptable">
                        <tr>
                            <td colspan="2" class="red">
                                <asp:Literal ID="litMessage" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Type Name:</label>
                            </td>
                            <td>
                                <eShip:CustomHiddenField ID="hidPackageTypeId" runat="server" />
                                <eShip:CustomTextBox ID="txtTypeName" runat="server" CssClass="w300" MaxLength="50" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvTypeName" ControlToValidate="txtTypeName"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                <asp:CheckBox ID="chkActive" runat="server" CssClass="jQueryUniform ml10" />
                                <label class="upper w90">Active</label>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">
                                    <abbr title="EDI Order Identification Detail Code">EDI OID</abbr>:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtEdiOid" runat="server" CssClass="w75" MaxLength="2" />
                                <label class="upper w90 text-right">
                                    <abbr title="Placement weight of package type in list. 10000 carries more weight than 1.">Sort Weight</abbr>:</label>
                                <eShip:CustomTextBox ID="txtSortWeight" runat="server" CssClass="w75" MaxLength="50"/>
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtSortWeight"
                                    FilterType="Custom, Numbers" ValidChars="." />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Project 44 Code:</label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlProject44Code" CssClass="w170" DataTextField="Value" DataValueField="Key"
                                                  runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                               <p class="pl10 pt10">Default Dimensions</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper text-right">Length (in):</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtDefaultLength" runat="server" CssClass="w75" MaxLength="50"/>
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtDefaultLength"
                                    FilterType="Custom, Numbers" ValidChars="." />
                                <asp:RequiredFieldValidator runat="server" ID="rfvDefaultLength" ControlToValidate="txtDefaultLength"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />

                                <label class="upper text-right w70">Width (in):</label>
                                <eShip:CustomTextBox ID="txtDefaultWidth" runat="server" CssClass="w75" MaxLength="50"/>
                                <asp:RequiredFieldValidator runat="server" ID="rfvDefaultWidth" ControlToValidate="txtDefaultWidth"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtDefaultWidth"
                                    FilterType="Custom, Numbers" ValidChars="." />

                                <label class="upper text-right w70">Height (in):</label>
                                <eShip:CustomTextBox ID="txtDefaultHeight" runat="server" CssClass="w75" MaxLength="50"/>
                                <asp:RequiredFieldValidator runat="server" ID="rfvDefaultHeight" ControlToValidate="txtDefaultHeight"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtDefaultHeight"
                                    FilterType="Custom, Numbers" ValidChars="." />
                                <asp:CheckBox ID="chkEditableDimensions" runat="server" CssClass="jQueryUniform ml10" />
                                <label class="upper w90">Editable</label>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="pt10">
                                <asp:Button ID="btnSave" Text="Save" OnClick="OnSaveClick" runat="server"/>
                                <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnCloseClicked" runat="server" 
                                    CausesValidation="false" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information" OnOkay="OnOkayProcess" />
    <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
        OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
</asp:Content>
