﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="SettingView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Registry.SettingView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        OnNew="OnNewClick" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Settings<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>

            </h3>
        </div>
        <hr class="fat" />
        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheSettingTable" TableId="settingTable" HeaderZIndex="2" />
        <asp:ListView ID="lvwRegistry" runat="server" ItemPlaceholderID="itemPlaceHolder">
            <LayoutTemplate>
                <div class="rowgroup">
                    <table id="settingTable" class="line2 pl2">
                        <tr>
                            <th style="width: 35%;">Code
                            </th>
                            <th style="width: 57%;">Value
                            </th>
                            <th style="width: 8%;" class="text-center">Action
                            </th>
                        </tr>
                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                    </table>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <eShip:CustomHiddenField ID="settingId" runat="server" Value='<%# Eval("Id") %>' />
                        <%# Eval("Code").FormattedString()%>
                    </td>
                    <td>
                        <%# Eval("Value") %>
                    </td>
                    <td class="text-center">
                        <asp:ImageButton ID="ibtnEdit" runat="server" ToolTip="Edit" ImageUrl="~/images/icons2/editBlue.png"
                            CausesValidation="false" OnClick="OnEditClicked" Enabled='<%# Access.Modify %>' />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
        <asp:Panel ID="pnlEdit" runat="server" Visible="false" DefaultButton="btnSave">
            <div class="popup popupControlOverW500">
                <div class="popheader">
                    <h4>
                        <asp:Literal ID="litTitle" runat="server" />
                    </h4>
                    <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                        CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
                </div>
                <div class="row">
                    <table class="poptable">
                        <tr>
                            <td colspan="20" class="red">
                                <asp:Literal ID="litMessage" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Code:</label>
                            </td>
                            <td>
                                <eShip:CustomHiddenField ID="hidSettingId" runat="server" />
                                <asp:DropDownList DataTextField="Value" DataValueField="Key" ID="ddlSettingCodes"
                                    Enabled="false" runat="server" CssClass="w300" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right top">
                                <label class="upper">
                                    Value:
                                </label>
                                <br />
                                <label class="lhInherit">
                                    <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleValue" TargetControlId="txtValue" MaxLength="200" />
                                </label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtValue" CssClass="w300" TextMode="MultiLine" runat="server" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvValue" ControlToValidate="txtValue"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="pt10">
                                <asp:Button ID="btnSave" Text="Save" OnClick="OnSaveClick" runat="server" />
                                <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnCloseClicked" runat="server"
                                    CausesValidation="false" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information" OnOkay="OnOkayProcess" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
</asp:Content>
