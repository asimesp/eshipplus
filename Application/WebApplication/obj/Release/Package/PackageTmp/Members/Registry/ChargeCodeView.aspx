﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="ChargeCodeView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Registry.ChargeCodeView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.Registry" %>
<%@ Import Namespace="LogisticsPlus.Eship.Project44" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        OnNew="OnNewClick" OnImport="OnImportClicked" OnExport="OnExportClicked" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Charge Codes<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>
        <hr class="fat" />
        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheChargeCodeTable" TableId="chargeCodeTableTable" HeaderZIndex="2" />
        <asp:ListView ID="lvwRegistry" runat="server" ItemPlaceholderID="itemPlaceHolder">
            <LayoutTemplate>
                <div class="rowgroup">
                    <table id="chargeCodeTableTable" class="line2 pl2">
                        <tr>
                            <th style="width: 18%">Code</th>
                            <th style="width: 32%">Description</th>
                            <th style="width: 10%">Category</th>
                            <th style="width: 10%">Project 44 Code</th>
                            <th style="width: 10%" class="text-center">Active</th>
                            <th style="width: 12%" class="text-center">Suppress on Rate Confirmation</th>
                            <th style="width: 8%">Action</th>
                        </tr>
                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                    </table>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <eShip:CustomHiddenField ID="chargeCodeId" runat="server" Value='<%# Eval("Id") %>' />
                        <asp:Literal ID="litCode" runat="server" Text='<%# Eval("Code") %>' />
                    </td>
                    <td>
                        <asp:Literal ID="litDescription" runat="server" Text='<%# Eval("Description") %>' />
                    </td>
                    <td>
                        <asp:Literal ID="litCategory" runat="server" Text='<%# Eval("Category") %>' />
                    </td>
                    <td>
                        <%# Eval("Project44Code").ToInt().ToEnum<Project44ChargeCode>().GetChargeCodeDescription() %>
                    </td>
                    <td class="text-center">
                        <%# Eval("Active").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                        <eShip:CustomHiddenField runat="server" ID="hidActive" Value='<%# Eval("Active").ToBoolean().GetString() %>' />
                    </td>
                    <td class="text-center">
                        <%# Eval("SurpressOnCarrierRateAgreement").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                        <eShip:CustomHiddenField runat="server" ID="hidSuppress" Value='<%# Eval("SurpressOnCarrierRateAgreement").ToBoolean().GetString() %>' />
                    </td>
                    <td>
                        <asp:ImageButton ID="ibtnEdit" runat="server" ToolTip="Edit" ImageUrl="~/images/icons2/editBlue.png"
                            CausesValidation="false" OnClick="OnEditClicked" Enabled='<%# Access.Modify %>' />
                        <asp:ImageButton ID="ibtnDelete" runat="server" ToolTip="Delete`" ImageUrl="~/images/icons2/deleteX.png"
                            CausesValidation="false" OnClick="OnDeleteClicked" Enabled='<%# Access.Remove %>' />
                        <ajax:ConfirmButtonExtender runat="server" ID="cbeDelete" TargetControlID="ibtnDelete"
                            ConfirmText="Are you sure you want to delete record?" />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
        <asp:Panel ID="pnlEdit" runat="server" Visible="false" DefaultButton="btnSave">
            <div class="popup popupControlOverW500">
                <div class="popheader">
                    <h4>
                        <asp:Literal ID="litTitle" runat="server" />
                    </h4>
                    <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                        CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
                </div>
                <div class="row">
                    <table class="poptable">
                        <tr>
                            <td colspan="2" class="red">
                                <asp:Literal ID="litMessage" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Code:</label>
                            </td>
                            <td>
                                <eShip:CustomHiddenField ID="hidChargeCodeId" runat="server" />
                                <eShip:CustomTextBox ID="txtCode" MaxLength="50" runat="server" CssClass="w300" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvCode" ControlToValidate="txtCode"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right top">
                                <label class="upper">
                                    Description:
                                </label>
                                <br />
                                <label class="lhInherit">
                                    <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDescription" TargetControlId="txtDescription" MaxLength="200" />
                                </label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtDescription" runat="server" TextMode="MultiLine"
                                    Rows="3" CssClass="w300" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvDescription" ControlToValidate="txtDescription"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Category:</label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlCategories" CssClass="w300" DataTextField="Value" DataValueField="Key"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Project 44 Code:</label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlProject44Code" CssClass="w300" DataTextField="Value" DataValueField="Key"
                                                  runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:CheckBox ID="chkActive" runat="server" CssClass="jQueryUniform" />
                                <label class="upper pr10">Active</label>
                                <asp:CheckBox ID="chkSuppress" runat="server" CssClass="jQueryUniform" />
                                <label class="upper">Suppress on Rate Confirmation</label>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="pt10">
                                <asp:Button ID="btnSave" Text="Save" OnClick="OnSaveClick" runat="server" />
                                <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnCloseClicked" runat="server" CausesValidation="false" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information" OnOkay="OnOkayProcess" />
    <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
        OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
</asp:Content>
