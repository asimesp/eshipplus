﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PostalCodeFinderControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Registry.Controls.PostalCodeFinderControl" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<asp:Panel runat="server" ID="pnlPostalCodeFinderContent" CssClass="popupControl" DefaultButton="btnSearch">
    <script type="text/javascript">
        $(document).ready(function () {
            $(jsHelper.AddHashTag('divPostalCodeProcessing')).hide();
        });

        function ShowPostalCodeProcessingDivBlock() {
            $(jsHelper.AddHashTag('divPostalCodeProcessing')).show();
        }
    </script>
    <div class="popheader">
        <h4>Postal Code Finder<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" Text="" /></small>
            <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/icons2/close.png" CssClass="close"
                CausesValidation="false" OnClick="OnCancelClicked" runat="server" />
        </h4>
    </div>
    <script type="text/javascript">
        $(function () {
            if (jsHelper.IsTrue($('#<%= hidAreFiltersShowing.ClientID%>').val())) {
                $('#<%= pnlSearch.ClientID%>').show();
                $('#<%= pnlSearchShowFilters.ClientID%>').hide();
            } else {
                $('#<%= pnlSearch.ClientID%>').hide();
                $('#<%= pnlSearchShowFilters.ClientID%>').show();
            }
        });

        function ToggleFilters() {
            $('#<%= hidAreFiltersShowing.ClientID%>').val(jsHelper.IsTrue($('#<%= hidAreFiltersShowing.ClientID%>').val()) ? false : true);
            $('#<%= pnlSearch.ClientID%>').toggle();
            $('#<%= pnlSearchShowFilters.ClientID%>').toggle();
            var top = $('#<%= pnlPostalCodeFinderContent.ClientID%> div[class="finderScroll"]').offset().top - $('#<%= pnlPostalCodeFinderContent.ClientID%>').offset().top - 2;
            $('#<%= pnlPostalCodeFinderContent.ClientID%> div[class="finderScroll"] > div[id^="hd"]').css('top', top + 'px');
        }
    </script>
    <table id="finderTable" class="poptable">
        <tr>
            <td>
                <asp:Panel runat="server" ID="pnlSearchShowFilters" class="rowgroup mb0" style="display:none;">
                    <div class="row">
                        <div class="fieldgroup">
                            <asp:Button ID="btnSelectAll2" runat="server" Text="ADD SELECTED" OnClick="OnSelectAllClicked" CssClass="ml10" CausesValidation="false" />
                        </div>
                        <div class="fieldgroup right">
                            <button onclick="ToggleFilters(); return false;">Show Filters</button>
                        </div>
                        <div class="fieldgroup right mb5 mt5 mr10">
                            <span class="fs75em  ">*** If you would like to show parameters when searching change the 'Always Show Finder Parameters On Search' option on your <asp:HyperLink ID="hypGoToUserProfile" Target="_blank" CssClass="blue" runat="server" Text="User Profile"/></span>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlSearch" DefaultButton="btnSearch">
                    <div class="mt5">
                        <div class="rowgroup mb0">
                            <div class="row">
                                <div class="fieldgroup">
                                    <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                                        CausesValidation="False" CssClass="add" />
                                </div>
                                <div class="fieldgroup right">
                                    <button onclick="ToggleFilters(); return false;">Hide Filters</button>
                                </div>
                                <div class="right">
                                    <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="PostalCodes" ShowAutoRefresh="False"
                                        OnProcessingError="OnSearchProfilesProcessingError" OnSearchProfileSelected="OnSearchProfilesProfileSelected" />
                                </div>
                            </div>
                        </div>

                        <hr class="mb5" />

                        <script type="text/javascript">$(window).load(function () {SetControlFocus();});</script>
                        <table class="mb5" id="parametersTable">
                            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                                OnItemDataBound="OnParameterItemDataBound">
                                <LayoutTemplate>
                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                                        OnRemoveParameter="OnParameterRemove" />
                                </ItemTemplate>
                            </asp:ListView>
                        </table>
                        <table class="poptable mb5">
                            <tr>
                                <td style="width:40%">
                                    <asp:Button ID="btnSearch" CssClass="left" runat="server" Text="SEARCH" OnClick="OnSearchClicked"
                                        CausesValidation="False" OnClientClick="ShowPostalCodeProcessingDivBlock();"/>
                                    <asp:Button ID="btnCancel" OnClick="OnCancelClicked" runat="server" Text="CANCEL"
                                        CssClass="ml10" CausesValidation="false" />
                                    <asp:Button ID="btnSelectAll" runat="server" Text="ADD SELECTED" OnClick="OnSelectAllClicked"
                                        CssClass="ml10" CausesValidation="false" />
                                </td>
                                <td>
                                    <span class="note green">&#8226; Result set is first 10,000 matches</span>
                                </td>
                                <td>
                                    <label class="right pr10">
                                        Sort: <asp:Literal runat="server" ID="litSortOrder" Text='<%# WebApplicationConstants.Default %>'/>
                                    </label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel runat="server" ID="upDataUpdate">
        <ContentTemplate>
            <eShip:ContinuousScrollExtender ID="upcseDataUpdate" runat="server" BindableControlId="lstSearchResults" UpdatePanelToExtendId="upDataUpdate" ScrollableDivId="postalCodeFinderScrollSection"
                SelectionCheckBoxControlId="chkSelected" SelectionUniqueRefHiddenFieldId="hidPostalCodeId" />
            <eShip:TableFreezeHeaderExtender runat="server" ID="tfheUsersFinderTable" TableId="postalCodeFinderTable" ScrollerContainerId="postalCodeFinderScrollSection" ScrollOffsetControlId="pnlPostalCodeFinderContent"
                IgnoreHeaderBackgroundFill="True" HeaderZIndex="2050" />

            <div class="finderScroll" id="postalCodeFinderScrollSection">
                <table id="postalCodeFinderTable" class="stripe sm_chk">
                    <tr>
                        <th style="width: 5%;">
                            <eShip:AltUniformCheckBox ID="chkSelectAllRecords" runat="server" Visible='<%# EnableMultiSelection %>' OnClientSideClicked="javascript:SetSelectedCheckBoxesStatus(this, 'postalCodeFinderTable');" />
                        </th>
                        <th style="width: 8%;">
                            <asp:LinkButton runat="server" ID="lbtnSortPostalCode" CssClass="link_nounderline white"
                                CausesValidation="False" OnCommand="OnSortData">
								 Postal Code
                            </asp:LinkButton>
                        </th>
                        <th style="width: 20%;">
                            <asp:LinkButton runat="server" ID="lbtnSortCity" CssClass="link_nounderline white" CausesValidation="False"
                                OnCommand="OnSortData">
								 City
                            </asp:LinkButton>
                        </th>
                        <th style="width: 20%;">
                            <asp:LinkButton runat="server" ID="lbtnSortCityAlias" CssClass="link_nounderline white"
                                CausesValidation="False" OnCommand="OnSortData">
								 City Alias
                            </asp:LinkButton>
                        </th>
                        <th style="width: 10%;">
                            <asp:LinkButton runat="server" ID="lbtnSortState" CssClass="link_nounderline white" CausesValidation="False"
                                OnCommand="OnSortData">
								 State
                            </asp:LinkButton>
                        </th>
                        <th style="width: 18%;">
                            <asp:LinkButton runat="server" ID="lbtnSortCountryName" CssClass="link_nounderline white"
                                CausesValidation="False" OnCommand="OnSortData">
								 Country
                            </asp:LinkButton>
                        </th>
                        <th style="width: 5%;" class="text-center">Primary
                        </th>
                        <th style="width: 7%;">Latitude
                        </th>
                        <th style="width: 7%;">Longitude
                        </th>
                    </tr>
                    <asp:ListView ID="lstSearchResults" runat="server" ItemPlaceholderID="itemPlaceHolder">
                        <LayoutTemplate>
                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:Button ID="btnPostalCodeSelect" runat="server" Text="Select" OnClick="OnSelectClicked"
                                        Visible='<%# !EnableMultiSelection %>' CausesValidation="false" />
                                    <eShip:AltUniformCheckBox runat="server" ID="chkSelected" Visible='<%# EnableMultiSelection %>'
                                        OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'postalCodeFinderTable', 'chkSelectAllRecords');" />
                                    <eShip:CustomHiddenField ID="hidPostalCodeId" runat="server" Value='<%# Eval("Id") %>' />
                                </td>
                                <td>
                                    <%# Eval("Code") %>
                                </td>
                                <td>
                                    <%# Eval("City") %>
                                </td>
                                <td>
                                    <%# Eval("CityAlias") %>
                                </td>
                                <td>
                                    <%# Eval("State") %>
                                </td>
                                <td>
                                    <%# Eval("CountryName") %>
                                </td>
                                <td class="text-center">
                                    <%# Eval("Primary").ToBoolean() ? WebApplicationConstants.HtmlCheck : string.Empty %>
                                </td>
                                <td class="fs75em">
                                    <%# Eval("Latitude") %>
                                </td>
                                <td class="fs75em">
                                    <%# Eval("Longitude") %>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnSortPostalCode" />
            <asp:PostBackTrigger ControlID="lbtnSortCity" />
            <asp:PostBackTrigger ControlID="lbtnSortCityAlias" />
            <asp:PostBackTrigger ControlID="lbtnSortState" />
            <asp:PostBackTrigger ControlID="lbtnSortCountryName" />
            <asp:PostBackTrigger ControlID="lstSearchResults" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Panel>
<asp:Panel ID="pnlPostalCodeFinderDimScreen" CssClass="dimBackgroundControl" runat="server" Visible="false" />
<eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
<eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
<eShip:CustomHiddenField runat="server" ID="hidSortAscending" />
<eShip:CustomHiddenField runat="server" ID="hidSortField" />
<eShip:CustomHiddenField runat="server" ID="hidPostalCodeFinderEnableMultiSelection" />
<eShip:CustomHiddenField runat="server" ID="hidAreFiltersShowing" Value="true" />
<div id="divPostalCodeProcessing" class="dimBackgroundControlOver" style="display:none;">
    <div class="text-center" style="color: white; font-style: italic; font-weight: bold; font-size: 3em; margin: auto; display: block; width: 300px; padding-top: 200px">
        Processing ...
    </div>
</div>
