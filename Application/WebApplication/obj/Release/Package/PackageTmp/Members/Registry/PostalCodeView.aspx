﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="PostalCodeView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Registry.PostalCodeView" EnableEventValidation="false" %>

<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        ShowImport="true" ShowExport="True" OnNew="OnNewClicked" OnImport="OnImportClicked" OnExport="OnExportClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Postal Codes<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>

        <hr class="dark mb10" />
        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                </div>
            </div>

            <hr class="dark mb10" />

            <div class="rowgroup">
                <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                    OnItemDataBound="OnParameterItemDataBound">
                    <LayoutTemplate>
                        <table class="mb10">
                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                            OnRemoveParameter="OnParameterRemove" />
                    </ItemTemplate>
                </asp:ListView>
                <p class="note">NOTE: ** results set is first 10,000 matches.</p>
            </div>

            <hr class="fat" />

            <div class="rowgroup">
                <div class="row mb10">
                    <div class="left">
                        <asp:Button ID="btnSearch" CssClass="left" runat="server" Text="SEARCH" OnClick="OnSearchClicked"
                            CausesValidation="False" />
                    </div>
                    <div class="right">
                        <div class="fieldgroup">
                            <label>Sort by</label>
                            <asp:DropDownList runat="server" ID="ddlSortBy" DataTextField="DisplayName" DataValueField="Name"
                                OnSelectedIndexChanged="OnSortBySelectedIndexChanged" AutoPostBack="True" />
                        </div>
                        <div class="fieldgroup mr10">
                            <asp:RadioButton runat="server" ID="rbAsc" Text="ASC" GroupName="rbSortBy" AutoPostBack="True"
                                ToolTip="Ascending" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                        </div>
                        <div class="fieldgroup">
                            <asp:RadioButton runat="server" ID="rbDesc" Text="DESC" GroupName="rbSortBy" Checked="True"
                                ToolTip="Descending" AutoPostBack="True" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <div class="rowgroup">
            <asp:UpdatePanel runat="server" ID="upDataUpdate">
                <ContentTemplate>
                    <eShip:ContinuousScrollExtender ID="upcseDataUpdate" runat="server" BindableControlId="lvwPostalCode" UpdatePanelToExtendId="upDataUpdate" />
                    <eShip:TableFreezeHeaderExtender runat="server" ID="tfhePostalCodeTable" TableId="postalCodeTable" HeaderZIndex="2" />
                    <table class="line2 pl2" id="postalCodeTable">
                        <tr>
                            <th style="width: 10%;">
                                <asp:LinkButton runat="server" ID="lbtnSortPostalCode" CssClass="link_nounderline blue"
                                    CausesValidation="False" OnCommand="OnSortData">
								 Postal Code
                                </asp:LinkButton>
                            </th>
                            <th style="width: 18%;">
                                <asp:LinkButton runat="server" ID="lbtnSortCity" CssClass="link_nounderline blue"
                                    CausesValidation="False" OnCommand="OnSortData">
								 City
                                </asp:LinkButton>
                            </th>
                            <th style="width: 18%;">
                                <asp:LinkButton runat="server" ID="lbtnSortCityAlias" CssClass="link_nounderline blue"
                                    CausesValidation="False" OnCommand="OnSortData">
								 City Alias
                                </asp:LinkButton>
                            </th>
                            <th style="width: 8%;">
                                <asp:LinkButton runat="server" ID="lbtnSortState" CssClass="link_nounderline blue"
                                    CausesValidation="False" OnCommand="OnSortData">
								 State
                                </asp:LinkButton>
                            </th>
                            <th style="width: 15%;">
                                <asp:LinkButton runat="server" ID="lbtnSortCountryName" CssClass="link_nounderline blue"
                                    CausesValidation="False" OnCommand="OnSortData">
								 Country
                                </asp:LinkButton>
                            </th>
                            <th style="width: 5%;" class="text-center">Primary
                            </th>
                            <th style="width: 8%;">Latitude
                            </th>
                            <th style="width: 8%;">Longitude
                            </th>
                            <th style="width: 8%;" class="text-center">Action
                            </th>
                        </tr>
                        <asp:ListView ID="lvwPostalCode" runat="server" ItemPlaceholderID="itemPlaceHolder">
                            <LayoutTemplate>
                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <eShip:CustomHiddenField ID="hidPostalCodeId" runat="server" Value='<%# Eval("Id") %>' />
                                        <asp:Literal runat="server" ID="litCode" Text='<%# Eval("Code") %>' />
                                    </td>
                                    <td>
                                        <asp:Literal runat="server" ID="litCity" Text='<%# Eval("City") %>' />
                                    </td>
                                    <td>
                                        <asp:Literal runat="server" ID="litCityAlias" Text='<%# Eval("CityAlias") %>' />
                                    </td>
                                    <td>
                                        <asp:Literal runat="server" ID="litState" Text='<%# Eval("State") %>' />
                                    </td>
                                    <td>
                                        <eShip:CustomHiddenField runat="server" ID="hidCountryCode" Value='<%# Eval("CountryCode") %>' />
                                        <%# Eval("CountryName") %>
                                    </td>
                                    <td class="text-center">
                                        <eShip:CustomHiddenField runat="server" ID="hidPrimary" Value='<%# Eval("Primary") %>' />
                                        <%# Eval("Primary").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                                    </td>
                                    <td>
                                        <asp:Literal runat="server" ID="litLatitude" Text='<%# Eval("Latitude") %>' />
                                    </td>
                                    <td>
                                        <asp:Literal runat="server" ID="litLongitude" Text='<%# Eval("Longitude") %>' />
                                    </td>
                                    <td class="text-center pr5">
                                        <asp:ImageButton ID="ibtnEdit" runat="server" ToolTip="Edit" ImageUrl="~/images/icons2/editBlue.png"
                                            CausesValidation="false" OnClick="OnEditClicked" Enabled='<%# Access.Modify %>' />
                                        <asp:ImageButton ID="ibtnDelete" runat="server" ToolTip="Delete" ImageUrl="~/images/icons2/deleteX.png"
                                            CausesValidation="false" OnClick="OnDeleteClicked" Enabled='<%# Access.Remove %>' />
                                        <ajax:ConfirmButtonExtender runat="server" ID="cbeDelete" TargetControlID="ibtnDelete"
                                            ConfirmText="Are you sure you want to delete record?" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="lbtnSortPostalCode" />
                    <asp:PostBackTrigger ControlID="lbtnSortCity" />
                    <asp:PostBackTrigger ControlID="lbtnSortCityAlias" />
                    <asp:PostBackTrigger ControlID="lbtnSortState" />
                    <asp:PostBackTrigger ControlID="lbtnSortCountryName" />
                    <asp:PostBackTrigger ControlID="lvwPostalCode" />
                </Triggers>
            </asp:UpdatePanel>
        </div>

        <asp:Panel ID="pnlEdit" runat="server" Visible="false" DefaultButton="btnSave">
            <div class="popupControl popupControlOverW500">
                <div class="popheader">
                    <h4>
                        <asp:Literal ID="litTitle" runat="server" />
                    </h4>
                    <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                        CausesValidation="false" OnClick="OnCloseEditPopupClicked" runat="server" />
                </div>
                <div class="row">
                    <table class="poptable">
                        <tr>
                            <td colspan="2" class="red">
                                <asp:Literal ID="litMessage" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Postal Code</label>
                            </td>
                            <td class="text-left">
                                <eShip:CustomHiddenField ID="hidPostalCodeId" runat="server" />
                                <eShip:CustomTextBox ID="txtCode" MaxLength="10" runat="server" CssClass="w75" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvCode" ControlToValidate="txtCode"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                <asp:CheckBox ID="chkPrimary" runat="server" CssClass="jQueryUniform ml10" />
                                <label>Primary</label>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">City</label>
                            </td>
                            <td class="text-left">
                                <eShip:CustomTextBox ID="txtCity" MaxLength="50" runat="server" CssClass="w130" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvCity" ControlToValidate="txtCity"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />

                                <label class="upper w90 text-right">City Alias</label>
                                <eShip:CustomTextBox ID="txtCityAlias" MaxLength="50" runat="server" CssClass="w130" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvCityAlias" ControlToValidate="txtCityAlias"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">State</label>
                            </td>
                            <td class="text-left">
                                <eShip:CustomTextBox ID="txtState" MaxLength="50" runat="server" CssClass="w130" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvState" ControlToValidate="txtState"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />

                                <label class="upper w90 text-right">Country</label>
                                <asp:DropDownList DataTextField="Name" DataValueField="Id" ID="ddlCountries" runat="server"
                                    CssClass="w130" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Latitude</label>
                            </td>
                            <td class="text-left">
                                <eShip:CustomTextBox ID="txtLatitude" MaxLength="50" runat="server" CssClass="w130" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvLatitude" ControlToValidate="txtLatitude"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                <asp:CompareValidator runat="server" ID="cvLatitude" ControlToValidate="txtLatitude"
                                    Type="Double" ErrorMessage="Latitude must be numeric" Text="*" Display="Dynamic" />

                                <label class="upper w90 text-right">Longitude</label>
                                <eShip:CustomTextBox ID="txtLongitude" MaxLength="50" runat="server" CssClass="w130" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvLongitude" ControlToValidate="txtLongitude"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                <asp:CompareValidator runat="server" ID="cvLongitude" ControlToValidate="txtLongitude"
                                    Type="Double" ErrorMessage="Latitude must be numeric" Text="*" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Button ID="btnSave" Text="Save" OnClick="OnSaveClicked" runat="server" />
                                <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnCloseEditPopupClicked" runat="server"
                                    CausesValidation="false" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlExport" runat="server" Visible="false" DefaultButton="btnExport">
            <script type="text/javascript">
                $(document).ready(function () {
                    // enforce mutual exclusion on checkboxes in pnlExport
                    $(jsHelper.AddHashTag('ulExportOptions input:checkbox')).click(function () {
                        if (jsHelper.IsChecked($(this).attr('id'))) {
                            $(jsHelper.AddHashTag('ulExportOptions input:checkbox')).not(jsHelper.AddHashTag($(this).attr('id'))).each(function () {
                                jsHelper.UnCheckBox($(this).attr('id'));
                            });
                            $.uniform.update();
                        }
                    });
                });
								</script>
            <div class="popupControl popupControlOverW500">
                <div class="popheader">
                    <h4>
                        Export Postal Codes
                    </h4>
                    <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/icons2/close.png" CssClass="close"
                        CausesValidation="false" OnClick="OnCloseExportClicked" runat="server" />
                </div>
                <h5 class="pl10">Options</h5>
                <div class="rowgroup">
                    <div class="row bline mb10">
                        <ul class="twocol_list pl10" id="ulExportOptions">
                            <li>
                                <asp:CheckBox runat="server" ID="chkExportCurrentSelection" CssClass="jQueryUniform"/>
                                <label>Export Current Search Results</label>
                            </li>
                            <li>
                                <asp:CheckBox runat="server" ID="chkExportAllPostalCodes" CssClass="jQueryUniform"/>
                                <label>Export All Postal Codes</label>
                            </li>
                        </ul>
                    </div>
                    <div class="row">
                        <div class="fieldgroup right">
                            <asp:Button runat="server" ID="btnExport" Text="Export" OnClick="OnExportPostalCodesClicked" CssClass="mr10" />
                            <asp:Button ID="btnClose" OnClick="OnCloseExportClicked" runat="server" CssClass="mr10" Text="CLOSE" CausesValidation="false" />
                        </div>
                    </div>
                </div>
                
            </div>
        </asp:Panel>
    </div>
    <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information"
        OnOkay="OnOkayProcess" OnYes="OnForcePrimaryPostalCode" OnNo="OnDoNotForcePrimaryPostalCode" />
    <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
        OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
</asp:Content>
