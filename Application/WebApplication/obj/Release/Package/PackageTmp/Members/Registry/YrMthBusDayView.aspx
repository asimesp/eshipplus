﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="YrMthBusDayView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Registry.YrMthBusDayView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        OnNew="OnNewClick" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
                Year Monthly Business Days<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>
        <hr class="fat" />
        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheYrMthBusDayTable" TableId="yrMthBusDayTable" HeaderZIndex="2"/>
        <asp:ListView ID="lvwRegistry" runat="server" ItemPlaceholderID="itemPlaceHolder">
            <LayoutTemplate>
                <div class="rowgroup">
                    <table id="yrMthBusDayTable" class="line2 pl2">
                        <tr>
                            <th class="text-center" style="width: 24%;">Year
                            </th>
                            <th class="text-center" style="width: 5%;">Jan
                            </th>
                            <th class="text-center" style="width: 5%;">Feb
                            </th>
                            <th class="text-center" style="width: 5%;">Mar
                            </th>
                            <th class="text-center" style="width: 5%;">Apr
                            </th>
                            <th class="text-center" style="width: 5%;">May
                            </th>
                            <th class="text-center" style="width: 5%;">Jun
                            </th>
                            <th class="text-center" style="width: 5%;">Jul
                            </th>
                            <th class="text-center" style="width: 5%;">Aug
                            </th>
                            <th class="text-center" style="width: 5%;">Sep
                            </th>
                            <th class="text-center" style="width: 5%;">Oct
                            </th>
                            <th class="text-center" style="width: 5%;">Nov
                            </th>
                            <th class="text-center" style="width: 5%;">Dec
                            </th>
                            <th class="text-center" style="width: 15%;">Action
                            </th>
                        </tr>
                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                    </table>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td class="text-center">
                        <eShip:CustomHiddenField ID="yrMthBusDayId" runat="server" Value='<%# Eval("Id") %>' />
                        <%# Eval("Year") %>
                    </td>
                    <td class="text-center">
                        <%# Eval("January") %>
                    </td>
                    <td class="text-center">
                        <%# Eval("February") %>
                    </td>
                    <td class="text-center">
                        <%# Eval("March") %>
                    </td>
                    <td class="text-center">
                        <%# Eval("April") %>
                    </td>
                    <td class="text-center">
                        <%# Eval("May") %>
                    </td>
                    <td class="text-center">
                        <%# Eval("June") %>
                    </td>
                    <td class="text-center">
                        <%# Eval("July") %>
                    </td>
                    <td class="text-center">
                        <%# Eval("August") %>
                    </td>
                    <td class="text-center">
                        <%# Eval("September") %>
                    </td>
                    <td class="text-center">
                        <%# Eval("October") %>
                    </td>
                    <td class="text-center">
                        <%# Eval("November") %>
                    </td>
                    <td class="text-center">
                        <%# Eval("December") %>
                    </td>
                    <td class="text-center">
                        <asp:ImageButton ID="ibtnEdit" ToolTip="Edit" runat="server" ImageUrl="~/images/icons2/editBlue.png"
                            CausesValidation="false" OnClick="OnEditClicked" Enabled='<%# Access.Modify %>' />
                        <asp:ImageButton ID="ibtnDelete" ToolTip="Delete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                            CausesValidation="false" OnClick="OnDeleteClicked" Enabled='<%# Access.Remove %>' />
                        <ajax:ConfirmButtonExtender runat="server" ID="cbeDelete" TargetControlID="ibtnDelete"
                            ConfirmText="Are you sure you want to delete record?" />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>

        <asp:Panel ID="pnlEdit" runat="server" Visible="false" DefaultButton="btnSave">
            <div class="popup popupControlOverW500">
                <div class="popheader">
                    <h4>
                        <asp:Literal ID="litTitle" runat="server" />
                    </h4>
                    <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                        CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
                </div>
                <div class="row">
                    <table class="poptable">
                        <tr>
                            <td colspan="6" class="red">
                                <asp:Literal ID="litMessage" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Year:</label>
                            </td>
                            <td colspan="3">
                                <eShip:CustomHiddenField ID="hidYrMthBusDayId" runat="server" />
                                <eShip:CustomTextBox ID="txtYear" CssClass="w130" runat="server" MaxLength="4"/>
                                <asp:RequiredFieldValidator runat="server" ID="rfvCode" ControlToValidate="txtYear"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" TargetControlID="txtYear"
                                    FilterType="Numbers" Enabled="True" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">January:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtJanuary" CssClass="w60" runat="server" MaxLength="2"/>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtJanuary"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtJanuary"
                                    FilterType="Numbers" Enabled="True" />
                                <label class="upper w90 text-right">February:</label>

                                <eShip:CustomTextBox ID="txtFebruary" CssClass="w60" runat="server" MaxLength="2"/>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="txtFebruary"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtFebruary"
                                    FilterType="Numbers" Enabled="True" />
                                <label class="upper w90 text-right">March:</label>

                                <eShip:CustomTextBox ID="txtMarch" CssClass="w60" runat="server" MaxLength="2"/>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator7" ControlToValidate="txtMarch"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="txtMarch"
                                    FilterType="Numbers" Enabled="True" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">April:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtApril" CssClass="w60" runat="server" MaxLength="2"/>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator10" ControlToValidate="txtApril"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" TargetControlID="txtApril"
                                    FilterType="Numbers" Enabled="True" />
                                <label class="upper w90 text-right">May:</label>

                                <eShip:CustomTextBox ID="txtMay" CssClass="w60" runat="server" MaxLength="2"/>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtMay"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtMay"
                                    FilterType="Numbers" Enabled="True" />
                                <label class="upper w90 text-right">June:</label>
                                <eShip:CustomTextBox ID="txtJune" CssClass="w60" runat="server" MaxLength="2"/>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator5" ControlToValidate="txtJune"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtJune"
                                    FilterType="Numbers" Enabled="True" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">July:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtJuly" CssClass="w60" runat="server" MaxLength="2"/>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ControlToValidate="txtJuly"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="txtJuly"
                                    FilterType="Custom, Numbers" Enabled="True" />

                                <label class="upper w90 text-right">August:</label>

                                <eShip:CustomTextBox ID="txtAugust" CssClass="w60" runat="server" MaxLength="2"/>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator11" ControlToValidate="txtAugust"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" TargetControlID="txtAugust"
                                    FilterType="Custom, Numbers" Enabled="True" />

                                <label class="upper w90 text-right">September:</label>
                                <eShip:CustomTextBox ID="txtSeptember" CssClass="w60" runat="server" MaxLength="2"/>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="txtSeptember"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtSeptember"
                                    FilterType="Custom, Numbers" Enabled="True" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">October:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtOctober" runat="server" CssClass="w60" MaxLength="2"/>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server"
                                    ControlToValidate="txtOctober" Display="Dynamic"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server"
                                    Enabled="True" FilterType="Custom, Numbers" TargetControlID="txtOctober" />
                                <label class="upper w90 text-right">November:</label>

                                <eShip:CustomTextBox ID="txtNovember" runat="server" CssClass="w60" MaxLength="2"/>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server"
                                    ControlToValidate="txtNovember" Display="Dynamic"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server"
                                    Enabled="True" FilterType="Custom, Numbers" TargetControlID="txtNovember" />
                                <label class="upper w90 text-right">December:</label>

                                <eShip:CustomTextBox ID="txtDecember" runat="server" MaxLength="2" CssClass="w60" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server"
                                    ControlToValidate="txtDecember" Display="Dynamic"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server"
                                    Enabled="True" FilterType="Custom, Numbers" TargetControlID="txtDecember" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="pt10">
                                <asp:Button ID="btnSave" runat="server" OnClick="OnSaveClick" Text="Save" />
                                <asp:Button ID="btnCancel" runat="server" CausesValidation="false"
                                    OnClick="OnCloseClicked" Text="Cancel" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information" OnOkay="OnOkayProcess" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
</asp:Content>
