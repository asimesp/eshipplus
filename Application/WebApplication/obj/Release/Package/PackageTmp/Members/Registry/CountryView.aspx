﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="CountryView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Registry.CountryView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true" ShowExport="True"
        OnNew="OnNewClick" OnImport="OnImportClicked" OnExport="OnExportClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Countries<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>

        <hr class="fat" />
        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheCountryTable" TableId="countryTable" />
        <asp:ListView ID="lvwCountry" runat="server" ItemPlaceholderID="itemPlaceHolder">
            <LayoutTemplate>
                <div class="rowgroup">
                    <table id="countryTable" class="line2 pl2">
                        <tr>
                            <th style="width: 10%;">Code
                            </th>
                            <th style="width: 32%;">Name
                            </th>
                            <th style="width: 10%;" class="text-center">Employs Postal Code
                            </th>
                            <th style="width:22%;">Postal Code Validation
                            </th>
                            <th style="width: 10%;" class="text-right">Sort Weight
                            </th>
                            <th style="width: 8%;"><abbr title="Project 44">P44</abbr> Code
                            </th>
                            <th style="width: 8%;">Action
                            </th>
                        </tr>
                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                    </table>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <eShip:CustomHiddenField ID="countryId" runat="server" Value='<%# Eval("Id") %>' />
                        <asp:Literal runat="server" ID="litCode" Text='<%# Eval("Code") %>' />
                    </td>
                    <td>
                        <asp:Literal runat="server" ID="litName" Text='<%# Eval("Name") %>' />
                    </td>
                    <td class="text-center">
                        <eShip:CustomHiddenField ID="hidEmploysPostalCodes" runat="server" Value='<%# Eval("EmploysPostalCodes") %>' />
                        <%# Eval("EmploysPostalCodes").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                    </td>
                    <td>
                        <asp:Literal runat="server" ID="litPostalCodeValidation" Text='<%# Eval("PostalCodeValidation") %>' />
                    </td>
                    <td class="text-right">
                        <asp:Literal runat="server" ID="litSortWeight" Text='<%# Eval("SortWeight") %>' />
                    </td>
                    <td class="text-right">
                        <asp:Literal runat="server" ID="litProject44CountryCode" Text='<%# Eval("Project44CountryCode").ToString() %>' />
                    </td>
                    <td>
                        <asp:ImageButton ID="ibtnEdit" runat="server" ToolTip="Edit" ImageUrl="~/images/icons2/editBlue.png"
                            CausesValidation="false" OnClick="OnEditClicked" Enabled='<%# Access.Modify %>' />
                        <asp:ImageButton ID="ibtnDelete" runat="server" ToolTip="Delete" ImageUrl="~/images/icons2/deleteX.png"
                            CausesValidation="false" OnClick="OnDeleteClicked" Enabled='<%# Access.Remove %>' />
                        <ajax:ConfirmButtonExtender runat="server" ID="cbeDelete" TargetControlID="ibtnDelete"
                            ConfirmText="Are you sure you want to delete record?" />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>

        <asp:Panel ID="pnlEdit" runat="server" Visible="false" DefaultButton="btnSave">
            <div class="popupControl popupControlOverW500">
                <div class="popheader">
                    <h4>
                        <asp:Literal ID="litTitle" runat="server" />
                    </h4>
                    <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                        CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
                </div>
                <div class="row">
                    <table class="poptable">
                        <tr>
                            <td colspan="4" class="red">
                                <asp:Literal ID="litMessage" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Code</label>
                            </td>
                            <td>
                                <eShip:CustomHiddenField ID="hidCountryId" runat="server" />
                                <eShip:CustomTextBox ID="txtCode" MaxLength="2" runat="server" CssClass="w40" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvCode" ControlToValidate="txtCode"
                                    Display="Dynamic" ErrorMessage="All fields marked with an ' * ' are required"
                                    Text="*" />
                                <label class="upper">Sort Weight</label>
                                <eShip:CustomTextBox runat="server" ID="txtSortWeight" CssClass="w75" MaxLength="50" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtSortWeight"
                                    FilterType="Numbers" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:CheckBox ID="chkEmploysPostalCode" runat="server" CssClass="jQueryUniform ml5" />
                                <label>Employs Postal Code</label>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Postal Code Validation</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtPostalCodeValidation" MaxLength="50" runat="server" CssClass="w310" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Name</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtName" MaxLength="50" runat="server" CssClass="w310" />
                                <asp:RequiredFieldValidator runat="server" ID="rfvName" ControlToValidate="txtName"
                                    Display="Dynamic" ErrorMessage="All fields marked with an ' * ' are required"
                                    Text="*" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Project 44 Country Code:</label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlProject44CountryCode" DataTextField="Value" DataValueField="Key" runat="server" CssClass="w300" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Button ID="btnSave" Text="Save" OnClick="OnSaveClick" runat="server" />
                                <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnCloseClicked" runat="server"
                                    CausesValidation="false" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information" OnOkay="OnOkayProcess" />
    <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
        OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackgroundControl" runat="server" Visible="false" />
</asp:Content>
