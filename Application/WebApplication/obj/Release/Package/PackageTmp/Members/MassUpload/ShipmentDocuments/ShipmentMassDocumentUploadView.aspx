﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="ShipmentMassDocumentUploadView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.MassUpload.ShipmentDocuments.ShipmentMassDocumentUploadView" %>

<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Shipment Mass Document Upload<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>
        <hr class="fat" />
        <div id="divErrorMessages" class="red">
            <asp:Literal ID="litErrorMessages" runat="server" Visible="True" />
        </div>
        <div class="rowgroup mb0">
            <div class="row mb10">
                <div class="fieldgroup">
                    <label class="upper blue">Document Tag: </label>
                    <eShip:CachedObjectDropDownList Type="DocumentTag" ID="ddlDocumentTags" runat="server" CssClass="w240"/>
                    <label class="upper ml10 blue">Is Internal: </label>
                    <asp:DropDownList runat="server" ID="ddlIsInternal" CssClass="w75" DataTextField="Text" DataValueField="Value" />
                </div>
            </div>
        </div>
        <hr class="dark mb20" />
        <script type="text/javascript">
            function OnFileUploadClientCompleteAll() {
                $.ajax({
                    type: "post",
                    url: "ShipmentMassDocumentUploadView.aspx/FileUploadCompletedProcesses",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        if (result.d != null)
                            DisplayErrors(result.d);
                    },
                    error: function (xhr, status, error) {
                        alert(error);
                    }
                });
            }

            function DisplayErrors(msgs) {
                for (var i = 0; i < msgs.length; i++) {
                    $("span[class='filename']:contains('" + msgs[i].Filename + "')").each(function () {
                        var pcontrol = $(this).parent().parent();
                        var scontrol = $(pcontrol).find("span[class='uploadstatus']");
                        $(pcontrol).css("color", "red");
                        $(scontrol).html($(scontrol).html() + " - Err: " + msgs[i].Errors);
                    });
                }
            }

            function OnFileUploadClientUploadStart() {
                SetServerSideUploadOptions();

                $("span[class='uploadstatus']:contains('(Uploaded)')").each(function () {
                    $(this).parent().parent().remove();
                });
            }

            function CheckFileSizes(showMsg) {
                var kb = 1024;
                var mb = kb * kb;
                var gb = mb * kb;
                var maxSize = Number('<%= WebApplicationSettings.SystemFileUploadMaxSize %>');
                var msg = jsHelper.EmptyString;

                $("span[class='filesize']").each(function () {
                    var html = $(this).html().toLowerCase();
                    var idx = html.indexOf(' ');
                    var num = idx == -1 ? html.substr(0, html.length) : html.substr(0, idx);
                    var unit = idx == -1 ? html.substr(0, html.length) : html.substr(idx, html.length - idx).trim();
                    var fs;

                    switch (unit) {
                        case 'kb':
                            fs = num * kb;
                            break;
                        case 'mb':
                            fs = num * mb;
                            break;
                        case 'bytes':
                            fs = num;
                            break;
                        default:
                        case 'gb':
                            fs = num * gb;
                            break;
                    }

                    if (fs > maxSize) {
                        var fnc = $(this).parent().find("span[class='filename']").html();
                        msg += "'" + fnc + "', ";
                        $(this).parent().parent().css("background-color", "red");
                    }

                });

                if (msg.length != 0) {
                    $("div [id^='<%= fupMassUpload.ClientID  %>_UploadOrCancelButton']").css("visibility", "hidden");
                    msg = "The following files exceed upload limit of " + maxSize + " bytes (or " + maxSize / kb + " kb, " + maxSize / mb + " mb): " + msg;
                    if (showMsg) alert(msg);
                } else {
                    if (!jsHelper.IsTrue($(jsHelper.AddHashTag('<%= hidOptionsFailed.ClientID%>')).val()) && $("span[class='filesize']").length > 0)
                        $("div [id^='<%= fupMassUpload.ClientID  %>_UploadOrCancelButton']").css("visibility", "visible");
                }
            }

            function SetServerSideUploadOptions() {
                $.ajax({
                    type: "post",
                    url: "ShipmentMassDocumentUploadView.aspx/SetOptions",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: '{"tagId":"' + $(jsHelper.AddHashTag('<%= ddlDocumentTags.ClientID%>')).val() + '", "isInternal":"' + $(jsHelper.AddHashTag('<%= ddlIsInternal.ClientID%>')).val() + '"}',
                    success: function (result) {
                        if (!result.d) {
                            alert('setting options failed');
                            $("div [id^='<%= fupMassUpload.ClientID  %>_UploadOrCancelButton']").css("visibility", "hidden");
                            $(jsHelper.AddHashTag('<%= hidOptionsFailed.ClientID%>')).val(true);
                        } else {
                            $("div [id^='<%= fupMassUpload.ClientID  %>_UploadOrCancelButton']").css("visibility", "visible");
                            $(jsHelper.AddHashTag('<%= hidOptionsFailed.ClientID%>')).val(false);
                        }
                    },
                    error: function (xhr, status, error) {
                        alert(error);
                    }
                });
            }

            $(document).ready(function () {
                $(".ajax__fileupload_dropzone").bind("drop", function () {
                    CheckFileSizes(false);
                });

                $(".ajax__fileupload_queueContainer").bind("click", function () {
                    CheckFileSizes(false);
                });

                $(".ajax__fileupload_uploadbutton").bind("mouseenter", function () {
                    CheckFileSizes(true);
                });

                // styling corrections to file upload control
                $(jsHelper.AddHashTag('<%= fupMassUpload.ClientID %>')).change(function () {
                    if ($("div [id^='<%= fupMassUpload.ClientID  %>_FileItemDeleteButton_'][class='removeButton']").length > 0) {

                        $("div [id^='<%= fupMassUpload.ClientID  %>_FileItemDeleteButton_']").each(function () {
                            $(this).addClass('OverrideMultiFileDelDiv ');
                            $(this).attr('style', 'background-color: transparent !important;');
                            $(this).html("<%= DeleteImageElement %>");
                        });

                        $("div [id^='<%= fupMassUpload.ClientID  %>_UploadOrCancelButton']").addClass("button");
                        $("div [id^='<%= fupMassUpload.ClientID  %>_UploadOrCancelButton']").css('height', 'auto');
                        $("div [id^='<%= fupMassUpload.ClientID  %>_UploadOrCancelButton']").removeClass("ajax__fileupload_uploadbutton");
                    }
                });

                $("span [id^='<%= fupMassUpload.ClientID  %>_SelectFileButton']").addClass("button");
                $("span [id^='<%= fupMassUpload.ClientID  %>_SelectFileButton']").css('height', 'auto');
                $("span [id^='<%= fupMassUpload.ClientID  %>_SelectFileButton']").removeClass("ajax__fileupload_selectFileButton");
                $("div [id^='<%= fupMassUpload.ClientID  %>_SelectFileContainer']").addClass("pb10");
                $("div [id^='<%= fupMassUpload.ClientID  %>_SelectFileContainer']").css('width', '150px');
            });
        </script>
        <ajax:AjaxFileUpload ID="fupMassUpload" runat="server" OnUploadComplete="OnFileUploadComplete" OnClientUploadCompleteAll="OnFileUploadClientCompleteAll" OnClientUploadStart="OnFileUploadClientUploadStart" />
    </div>
    <eShip:CustomHiddenField runat="server" ID="hidOptionsFailed" />
</asp:Content>
