﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="FuelTableView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Rating.FuelTableView" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        ShowExport="true" ShowImport="true" ShowEdit="true" ShowDelete="true" ShowSave="true"
        ShowFind="true" OnFind="OnFindClicked" OnSave="OnSaveClicked" OnDelete="OnDeleteClicked" OnUnlock="OnUnlockClicked"
        OnNew="OnNewClicked" OnEdit="OnEditClicked" OnImport="OnImportClicked" OnExport="OnExportClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">

        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Fuel Tables
                <eShip:RecordIdentityDisplayControl runat="server" ID="ridcRecordIdentity" TargetControlId="txtName" />
            </h3>

            <div class="lockMessage">
                <asp:Literal ID="litMessage" runat="server" />
            </div>
        </div>

        <hr class="dark mb5" />

        <div class="errorMsgLit">
            <asp:Literal runat="server" ID="litErrorMessages" />
        </div>

        <eShip:FuelTableFinderControl ID="fuelTableFinder" runat="server" Visible="false" OpenForEditEnabled="true"
            OnItemSelected="OnFuelTableItemSelected" OnSelectionCancel="OnFuelTableSelectionCancel" />

        <script type="text/javascript">
            function ClearHidFlag() {
                jsHelper.SetEmpty('<%= hidFlag.ClientID %>');
            }
        </script>

        <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
            Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" />

        <eShip:CustomHiddenField runat="server" ID="hidFuelTableId" />
        <ajax:TabContainer ID="tabFuelTables" runat="server" CssClass="ajaxCustom">
            <ajax:TabPanel runat="server" ID="tabDetails" HeaderText="Details">
                <ContentTemplate>
                    <asp:Panel ID="pnlFuelTable" runat="server">
                        <div class="rowgroup">
                            <div class="row">
                                <div class="fieldgroup">
                                    <label class="wlabel blue">Name</label>
                                    <eShip:CustomTextBox ID="txtName" runat="server" CssClass="w300" MaxLength="50" />
                                </div>
                            </div>
                        </div>
                        <div class="row mb10">
                            <div class="fieldgroup">
                                <h5 class="p_m0">Price Indices</h5>
                            </div>
                            <div class="fieldgroup right">
                                <asp:Button runat="server" ID="btnAddFuelIndex" Text="Add Fuel Index" CssClass="mr10" CausesValidation="false" OnClick="OnAddFuelIndexClicked" />
                                <asp:Button runat="server" ID="btnClearFuelIndices" Text="Clear Fuel Indices" CausesValidation="false" OnClick="OnClearFuelIndicesClicked" />
                            </div>
                        </div>
                        <eShip:TableFreezeHeaderExtender runat="server" ID="tfhePriceIndicesTable" TableId="priceIndicesTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                        <table class="stripe" id="priceIndicesTable">
                            <tr>
                                <th style="width: 32%;">Lower Bound ($)
                                </th>
                                <th style="width: 32%;">Upper Bound ($)
                                </th>
                                <th style="width: 31%;">Surcharge %
                                </th>
                                <th style="width: 5%">Action
                                </th>
                            </tr>
                            <asp:ListView ID="lstPriceIndices" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                <LayoutTemplate>
                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <eShip:CustomHiddenField runat="server" ID="hidPriceItemIndex" Value='<%# Container.DataItemIndex %>' />
                                            <eShip:CustomHiddenField ID="hidPriceIndexId" runat="server" Value='<%# Eval("Id") %>' />
                                            <eShip:CustomTextBox ID="txtLowerBound" Text='<%# Eval("LowerBound") %>' runat="server" MaxLength="9" CssClass="w200" />
                                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtLowerBound"
                                                FilterType="Custom, Numbers" ValidChars="." />
                                        </td>
                                        <td>
                                            <eShip:CustomTextBox ID="txtUpperBound" Text='<%# Eval("UpperBound") %>' runat="server" MaxLength="9" CssClass="w200" />
                                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtUpperBound"
                                                FilterType="Custom, Numbers" ValidChars="." />
                                        </td>
                                        <td>
                                            <eShip:CustomTextBox ID="txtSurcharge" Text='<%# Eval("Surcharge") %>' runat="server" MaxLength="9" CssClass="w200" />
                                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtSurcharge"
                                                FilterType="Custom, Numbers" ValidChars="." />
                                        </td>
                                        <td class="text-center">
                                            <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                CausesValidation="false" OnClick="OnPriceIndexDeleteClicked" Enabled='<%# Access.Modify %>' />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:ListView>
                        </table>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabAuditLog" HeaderText="Audit Log">
                <ContentTemplate>
                    <eShip:AuditLogControl runat="server" ID="auditLogs" />
                </ContentTemplate>
            </ajax:TabPanel>
        </ajax:TabContainer>
    </div>
    <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
        OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackgroundControl" runat="server" Visible="false" />
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
</asp:Content>
