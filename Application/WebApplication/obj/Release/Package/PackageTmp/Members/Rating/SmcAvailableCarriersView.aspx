﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="SmcAvailableCarriersView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Rating.SmcAvailableCarriersView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="False" ShowExport="True" OnExport="OnToolbarExportClicked" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="pageHeader">
        <asp:Image runat="server" ID="imgPageImageLogo" ImageUrl="" AlternateText="page icon" CssClass="pageHeaderIcon" />
        <h3>SMC<sup>3</sup> Available Carriers </h3>
    </div>
    <div style="width: 100%; color: red; padding: 3px;">
        <asp:Literal runat="server" ID="litErrorMessages" />
    </div>

    <table class="line2 pl2" id="availableCarriersTable">
        <tr>
            <th style="width: 40%;">Carrier Name
            </th>
            <th style="width: 10%;">SCAC
            </th>
            <th style="width: 40%;">Service
            </th>
            <th style="width: 10%;" class="text-center">Action</th>
        </tr>
        <asp:Repeater runat="server" ID="rptAvailableCarriers">
            <ItemTemplate>
                <tr>
                    <td><%# Eval("Name") %></td>
                    <td><%# Eval("Scac") %></td>
                    <td><%# Eval("Services") %></td>
                    <td>
                        <asp:HiddenField runat="server" ID="hidScac" Value='<%# Eval("Scac") %>' />
                        <asp:ImageButton runat="server" ID="ibtnDownloadTerminals" ImageUrl="~/images/icons2/downloadDocument.png"
                            ToolTip="Download Carrier Terminals" OnClick="OnDownloadTerminalsClicked" />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>

    </table>

</asp:Content>
