﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VendorRatingMatchingRegionSearchControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Rating.Controls.VendorRatingMatchingRegionSearchControl" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<asp:Panel runat="server" ID="pnlVendorRatingMatchingRegionSearch" CssClass="popupControl popupControlOverW750" DefaultButton="btnFindMatchingLanes">
    <div class="popheader">
        <h4>Vendor Rating LTL Discount Tiers Lanes
             <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                 CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
        </h4>
    </div>
    <table class="poptable">
        <tr>
            <td class="text-right">
                <label class="upper">Origin Postal Code:</label>
            </td>
            <td>
                <eShip:CustomTextBox runat="server" ID="txtOriginPostalCode" CssClass="w100" MaxLength="10" />
            </td>
            <td class="text-right">
                <label class="upper">Origin Country: </label>
            </td>
            <td>
                <eShip:CachedObjectDropDownList Type="Countries" ID="ddlOriginCountry" runat="server" 
                    CssClass="w150" EnableNotApplicable="True" DefaultValue="0" />
            </td>
        </tr>
        <tr>
            <td class="text-right">
                <label class="upper">Destination Postal Code</label>
            </td>
            <td>
                <eShip:CustomTextBox runat="server" ID="txtDestinationPostalCode" CssClass="w100" MaxLength="10" />
            </td>
            <td class="text-right">
                <label class="upper">Destination Country</label>
            </td>
            <td>
                <eShip:CachedObjectDropDownList Type="Countries" ID="ddlDestinationCountry" runat="server" 
                    CssClass="w150" EnableNotApplicable="True" DefaultValue="0" />
            </td>
        </tr>
        <tr>
            <td></td>
            <td class="pt10">
                <asp:Button runat="server" ID="btnFindMatchingLanes" Text="FIND MATCH" OnClick="OnFindMatchingLanesClicked" CausesValidation="false" />
            </td>
        </tr>
        <tr>
            <td colspan="4" class="p_m0">
                <asp:ListView runat="server" ID="lstMatchingRegions">
                    <LayoutTemplate>
                        <hr class="w100p ml-inherit" />
                        <h5 class="pl10 dark">Results</h5>
                        <div class="finderScroll">
                            <table class="stripe">
                                <tr>
                                    <th style="width: 5%" class="pl10">Index
                                    </th>
                                    <th style="width: 45%">Origin
                                    </th>
                                    <th style="width: 45%">Destination
                                    </th>
                                    <th style="width: 5%">Go To</th>
                                </tr>
                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                            </table>
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="pl10">
                                <%# Eval("Index") %>.
                            </td>
                            <td>
                                <%# Eval("Origin") %>
                            </td>
                            <td>
                                <%# Eval("Destination") %>
                            </td>
                            <td>
                                <asp:ImageButton runat="server" OnClick="OnIndexClicked" ImageUrl="~/images/icons2/arrow_general.png" AlternateText="goto page" ToolTip='<%# GotoPage + Eval("Index").GetString() %>' />
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </td>
        </tr>
    </table>
</asp:Panel>
<eShip:CustomHiddenField runat="server" ID="hidVendorRatingId" />
