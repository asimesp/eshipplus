﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="RegionView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Rating.RegionView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true" ShowExport="true" ShowImport="true"
        ShowEdit="true" ShowDelete="true" ShowSave="true" ShowFind="true" ShowMore="True" OnFind="OnFindClicked"
        OnSave="OnSaveClicked" OnDelete="OnDeleteClicked" OnNew="OnNewClicked" OnEdit="OnEditClicked"
        OnImport="OnImportAreasClicked" OnExport="OnExportAreasClicked" OnUnlock="OnUnlockClicked"
        OnCommand="OnCommandClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">

        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Regions
                <eShip:RecordIdentityDisplayControl runat="server" ID="ridcRecordIdentity" TargetControlId="txtName" />
            </h3>

            <div class="lockMessage">
                <asp:Literal ID="litMessage" runat="server" />
            </div>
        </div>

        <hr class="dark mb5" />

        <div class="errorMsgLit">
            <asp:Literal runat="server" ID="litErrorMessages" />
        </div>

        <eShip:RegionFinderControl ID="regionFinder" runat="server" Visible="false" OpenForEditEnabled="true" EnableMultiSelection="false"
            OnItemSelected="OnRegionFinderItemSelected" OnSelectionCancel="OnRegionFinderSelectionCancelled"
            OnMultiItemSelected="OnRegionFinderMultiItemSelected" />
        <eShip:PostalCodeFinderControl ID="postalCodeFinder" runat="server" Visible="false" EnableMultiSelection="true"
            OnSelectionCancel="OnPostalCodeFinderSelectionCancelled" OnMultiItemSelected="OnPostalCodeFinderMultiItemSelected" />

        <script type="text/javascript">
            function ClearHidFlag() {
                jsHelper.SetEmpty('<%= hidFlag.ClientID %>');
            }
        </script>

        <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
            Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" />
        <eShip:CustomHiddenField runat="server" ID="hidRegionId" />

        <ajax:TabContainer ID="tabRegions" runat="server" CssClass="ajaxCustom">
            <ajax:TabPanel runat="server" ID="tabDetails" HeaderText="Details">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlRegions" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel ID="pnlRegions" runat="server">
                                <div class="rowgroup">
                                    <div class="row">
                                        <div class="fieldgroup">
                                            <label class="wlabel blue">Region Name</label>
                                            <eShip:CustomTextBox ID="txtName" runat="server" CssClass="w300" MaxLength="50" />
                                        </div>
                                    </div>
                                </div>
                                <h5>Areas <span class="note fs75em pl20">*** maximum 10000 areas per region</span>
                                </h5>
                                <div class="row mb10">
                                    <div class="fieldgroup left">
                                        <eShip:PaginationExtender runat="server" ID="peLstAreas" TargetControlId="lstAreas" UnCheckControlIdOnPageChange="chkSelectAllRecords" PageSize="200" />
                                    </div>
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddSubRegion" Text="Add Subregion" CssClass="mr10" CausesValidation="false" OnClick="OnAddSubRegionClicked" />
                                        <asp:Button runat="server" ID="btnAddPostalCodes" Text="Add Postal Code(s)" CssClass="mr10" CausesValidation="false" OnClick="OnAddPostalCodesClicked" />
                                        <asp:Button runat="server" ID="btnRemoveAreas" Text="Remove Area(s)" CssClass="mr10" CausesValidation="false" OnClick="OnRemoveAreasClicked" />
                                        <asp:Button runat="server" ID="btnRemoveSelected" Text="Remove Selected" CausesValidation="false" OnClick="OnRemoveSelectedClicked" />
                                    </div>
                                </div>

                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheRegionAreasTable" TableId="regionAreasTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table id="regionAreasTable" class="stripe">
                                    <tr>
                                        <th style="width: 3%">
                                            <eShip:AltUniformCheckBox ID="chkSelectAllRecords" runat="server" OnClientSideClicked="javascript:SetSelectedCheckBoxesStatus(this, 'regionAreasTable');" />
                                        </th>
                                        <th style="width: 8%">Postal Code
                                        </th>
                                        <th style="width: 19%">City
                                        </th>
                                        <th style="width: 19%">City Alias
                                        </th>
                                        <th style="width: 8%">State
                                        </th>
                                        <th style="width: 9%">Country
                                        </th>
                                        <th style="width: 9%;" class="text-center">Use Sub Region
                                        </th>
                                        <th style="width: 20%;">Sub Region
                                        </th>
                                        <th style="width: 5%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstAreas" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:AltUniformCheckBox runat="server" ID="chkSelected" OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'regionAreasTable', 'chkSelectAllRecords');" />
                                                </td>
                                                <td>
                                                    <eShip:CustomHiddenField runat="server" ID="hidAreaItemIndex" Value='<%# Container.DataItemIndex %>' />
                                                    <%# Eval("PostalCode") %>
                                                </td>
                                                <td>
                                                    <%# Eval("City") %>
                                                </td>
                                                <td>
                                                    <%# Eval("CityAlias") %>
                                                </td>
                                                <td>
                                                    <%# Eval("State") %>
                                                </td>
                                                <td>
                                                    <%# Eval("CountryName") %>
                                                </td>
                                                <td class="text-center">
                                                    <%# Eval("UseSubRegion").ToBoolean() ? WebApplicationConstants.HtmlCheck : string.Empty %>
                                                </td>
                                                <td>
                                                    <%# ActiveUser.HasAccessTo(ViewCode.Region) && Eval("UseSubRegion").ToBoolean()
                                                            ? string.Format("{0} <a href='{1}?{2}={3}' class='blue' target='_blank' title='Go To Region Record'><img src={4} width='16' class='middle' /></td>", 
                                                                Eval("SubRegionName"),
                                                                ResolveUrl(PageAddress),
                                                                WebApplicationConstants.TransferNumber, 
                                                                Eval("SubRegionId").GetString().UrlTextEncrypt(),
                                                                ResolveUrl("~/images/icons2/arrow_newTab.png"))  
                                                            : Eval("SubRegionName") %>
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        CausesValidation="false" OnClick="OnDeleteAreaClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnRemoveAreas" />
                            <asp:PostBackTrigger ControlID="btnAddPostalCodes" />
                            <asp:PostBackTrigger ControlID="btnAddSubRegion" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <asp:Panel ID="pnlRemoveAreas" runat="server" Visible="false">
                        <div class="popupControl popupControlOverW500">
                            <div class="popheader">
                                <h4>Remove Area(s)
                                </h4>
                                <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                                    CausesValidation="false" OnClick="OnRemoveAreasCloseClicked" runat="server" />
                            </div>
                            <div class="row">
                                <table>
                                    <tr>
                                        <td class="text-right top">
                                            <label class="upper blue">Criteria:</label>
                                        </td>
                                        <td>
                                            <eShip:CustomTextBox runat="server" ID="txtRemovePostalCodeCriteria" CssClass="w300" />
                                            <br />
                                            <label class="fs85em note">*** case sensitive match, matching beginning of search item</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-right">
                                            <label class="upper blue">Match:</label>
                                        </td>
                                        <td>
                                            <asp:CheckBox runat="server" ID="chkRemoveAreaPostalCodeCodeMatch" CssClass="jQueryUniform" /><label class="mr10 blue">Code</label>
                                            <asp:CheckBox runat="server" ID="chkRemoveAreaCityMatch" CssClass="jQueryUniform" /><label class="mr10 blue">City</label>
                                            <asp:CheckBox runat="server" ID="chkRemoveAreaStateMatch" CssClass="jQueryUniform" /><label class="mr10 blue">State</label>
                                            <asp:CheckBox runat="server" ID="chkRemoveAreaCountryMatch" CssClass="jQueryUniform" /><label class="mr10 blue">Country</label>
                                            <asp:CheckBox runat="server" ID="chkRemoveAreaSubRegionMatch" CssClass="jQueryUniform" /><label class="mr10 blue">Sub Region</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <asp:Button ID="btnRemovePostalCode" Text="Remove" OnClick="OnRemoveAreasRemoveClicked"
                                                runat="server" CausesValidation="false" />
                                            <asp:Button ID="btnRemovePostalCodeCancel" Text="Cancel" OnClick="OnRemoveAreasCloseClicked"
                                                runat="server" CausesValidation="false" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabAuditLog" HeaderText="Audit Log">
                <ContentTemplate>
                    <eShip:AuditLogControl runat="server" ID="auditLogs" />
                </ContentTemplate>
            </ajax:TabPanel>
        </ajax:TabContainer>
    </div>
    <asp:Panel runat="server" ID="pnlLocatePostalCode" CssClass="popupControl popupControlOverW750" DefaultButton="btnLocatePostalCode" Visible="False">
        <div class="popheader">
            <h4>Locate Postal Code
                 <asp:ImageButton ImageUrl="~/images/icons2/close.png" CssClass="close"
                     CausesValidation="false" OnClick="OnCloseLocatePostalCodeClicked" runat="server" />
            </h4>
        </div>
        <div class="row pt10">
            <table class="poptable">
                <tr>
                    <td class="text-right pb10">
                        <label class="upper">Postal Code:</label>
                    </td>
                    <td>
                        <eShip:CustomTextBox runat="server" ID="txtPostalCode" CssClass="w100" MaxLength="10" />
                        <label class="upper ml10">Country:</label>
                        <eShip:CachedObjectDropDownList Type="Countries" ID="ddlCountry" runat="server" 
                                        CssClass="w150 mr10" EnableChooseOne="True" />
                        <asp:Button runat="server" ID="btnLocatePostalCode" Text="FIND MATCH" OnClick="OnLocatePostalCodeClicked" CausesValidation="false" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="p_m0">
                        <asp:ListView runat="server" ID="lstPageLocations">
                            <LayoutTemplate>
                                <hr class="w100p ml-inherit" />
                                <h5 class="pl10 dark">Results</h5>
                                <div class="finderScroll">
                                    <table class="stripe">
                                        <tr>
                                            <th style="width: 5%" class="pl10">Index
                                            </th>
                                            <th style="width: 45%">Postal Code / Region
                                            </th>
                                            <th style="width: 5%">Go To</th>
                                        </tr>
                                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                    </table>
                                </div>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="pl10">
                                        <%# Eval("Index") %>.
                                    </td>
                                    <td>
                                        <%# Eval("Text") %>
                                    </td>
                                    <td>
                                        <asp:ImageButton runat="server" OnClick="OnLocatePostalCodeIndexClicked" ImageUrl="~/images/icons2/arrow_general.png"
                                            AlternateText="goto page" ToolTip='<%# GotoPage + Eval("Index").GetString() %>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
        OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
    <eShip:CustomHiddenField runat="server" ID="hidLastFinderUse" />
</asp:Content>
