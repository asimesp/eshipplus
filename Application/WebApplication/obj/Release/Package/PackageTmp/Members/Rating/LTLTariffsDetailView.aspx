﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="LTLTariffsDetailView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Rating.LTLTariffsDetailView" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="False" ShowImport="false" ShowExport="True"
        OnExport="OnToolbarExport" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                CssClass="pageHeaderIcon" />
            <h3>LTL Tariff Details
            <small class="ml10">
                <asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>

        <hr class="fat" />

        <div class="rowgroup">
            <h5>Available Tariffs</h5>
            <eShip:TableFreezeHeaderExtender runat="server" ID="tfheTariffTable" TableId="tariffTable" HeaderZIndex="2"/>
            <table class="line2 pl2" id="tariffTable">
                <tr>
                    <th style="width: 2%">&nbsp;
                    </th>
                    <th style="width: 20%">Friendly Name
                    </th>
                    <th style="width: 25%">Description
                    </th>
                    <th style="width: 15%">Tariff Name
                    </th>
                    <th style="width: 15%">Effective Date
                    </th>
                    <th style="width: 15%">SMC3 Product Number
                    </th>
                    <th style="width: 8%">SMC3 Release
                    </th>
                </tr>
                <asp:Repeater runat="server" ID="rptTariffs">
                    <ItemTemplate>
                        <tr>
                            <td><%# Container.ItemIndex+1 %>.</td>
                            <td><%# Eval("ReaderFriendlyTariff")%></td>
                            <td><%# Eval("Description")%></td>
                            <td><%# Eval("TariffName")%></td>
                            <td><%# Eval("EffectiveDate")%></td>
                            <td><%# Eval("ProductNumber")%></td>
                            <td><%# Eval("Release")%></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </div>
    </div>
</asp:Content>
