﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="LTLRateAnalysisView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Rating.LTLRateAnalysisView" %>
<%@ Register TagPrefix="eShip1" Namespace="LogisticsPlus.Eship.WebApplication.Members.Controls" Assembly="Pacman" %>

<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Administration" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Rating" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
	<eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
	<div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                CssClass="pageHeaderIcon" />
            <h3>LTL Rate Analysis
            <small class="ml10">
                <asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>

        <script type="text/javascript">

            $(document).ready(function () {
                
                $("div[id$='instructionDiv']").hide();
                $("div[id$='pnlDimScreenJS']").hide();

                $(jsHelper.AddHashTag("importantInformation")).click(function (e) {
                    e.preventDefault();
                    $("div[id$='instructionDiv']").show("slow");
                    $("div[id$='pnlDimScreenJS']").show();
                });

                $(jsHelper.AddHashTag("instructionDiv")).click(function () {
                    $(this).hide("slow", function () { $("div[id$='pnlDimScreenJS']").hide(); });
                });

                CheckBoxChanged();
                $(jsHelper.AddHashTag("profiles input[type='checkbox']")).change(CheckBoxChanged);

                $("input[id$='ibtnDownloadOutputGroupOption']").click(function () {
                    var answer = confirm("Click okay to merge and download all records in group. Click cancel to download only this record.");
                    if (answer == true) {
                        $(jsHelper.AddHashTag('<%= hidGroupProcessFlag.ClientID %>')).val("<%= DownloadForGroup %>");
                    } else {
                        $(jsHelper.AddHashTag('<%= hidGroupProcessFlag.ClientID %>')).val(jsHelper.EmptyString);
                    }
                });
            });

            function CheckBoxChanged() {
                
                var chkFilter = jsHelper.AddHashTag("profiles input[type='checkbox']");
                var selectedCount = $(chkFilter).filter(":checked").length;
                
                // set count display
                $(jsHelper.AddHashTag('selectedRatingCountSpan')).text('( selected: ' + selectedCount + ' of 20 )');

                // check to disable
                if (selectedCount >= 20) {
                    $(chkFilter).not(":checked").attr("disabled", "disabled");
                } else
                    $(chkFilter).removeAttr("disabled");
            }
            
            function CheckFileSize(fileControl) {
                if (fileControl.files.length == 0) return;

                var maxSize = '<%= WebApplicationSettings.AnalysisMaximumFileUploadSize %>';
                var mb = '<%= WebApplicationConstants.MegaByte %>';

                if (fileControl.files[0].size <= maxSize) return;

                // alert and disable upload button
                alert(fileControl.files[0].name + " is too large. Maximum file size is: " + maxSize + " bytes [or " + maxSize / mb + " MB]");
                fileControl.value = jsHelper.EmptyString;
            }

        </script>
      
        <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information"
            OnOkay="OnOkayProcess" OnYes="OnYesProcess" OnNo="OnNoProcess" />
        <div class="imp_note mb10">
            <label class=" note">
                &#8226; Enter all required parameters then submit analysis. <span style="color: red;">*</span> = Required. 
                    <a href="#" id="importantInformation" style="color: blue;">Click here</a> for lane input file specifications. 
                <span style="color: red;">+</span> = Analysis Set being processed.
            </label>
        </div>
        <div class="rowgroup">
            <div class="col_1_3 bbox no-right-border">

                <h5>Submission Settings</h5>
                <div class="row">
                    <div class="fieldgroup mr20">
                        <label class="wlabel"><span class="red">*</span>Name</label>
                        <eShip:CustomTextBox runat="server" ID="txtName" CssClass="w190" MaxLength="50" />
                    </div>
                </div>
                <div class="row">
                    <div class="fieldgroup mr20">
                        <label class="wlabel">Group Code</label>
                        <eShip:CustomTextBox runat="server" ID="txtGroupCode" CssClass="w190" MaxLength="50" />
                    </div>
                </div>
                <div class="row">
                    <div class="fieldgroup mr20">
                        <label class="wlabel"><span class="red">*</span>Analysis Effective Date</label>
                        <eShip:CustomTextBox runat="server" ID="txtAnalysisEffectiveDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                    </div>
                </div>

                <div class="row">
                    <div class="fieldgroup pt10">
                        <label class="wlabel"><span class="red">*</span>Lanes Input File</label>
                        <asp:FileUpload runat="server" ID="fupFileUpload" CssClass="jQueryUniform" onchange="CheckFileSize(this);"/>
                    </div>
                </div>
                <div class="row">
                    <div class="fieldgroup pt10">
                        <asp:CheckBox runat="server" ID="chkLtlIndirectPointEnabled" CssClass="jQueryUniform" />
                        <label>Allow Indirect Points</label>
                    </div>
                </div>
            </div>

            <div class="col_2_3 bbox pl46 vlinedarkleft pr10">
                
				<asp:UpdatePanel runat="server" UpdateMode="Always" ID="upnlVendors">
					<ContentTemplate>
						<div>
							<h5 class="col_1_2">Vendor Rating Profiles 
								<span id="selectedRatingCountSpan" class="fs75em pl20">( selected: 0 of 20 )</span>
							</h5>
							<div class="col_1_2">
								<span class="visible-lg-block">
									<span class="fs75em pl20 sm_chk chk_list">
										<eShip:AltUniformCheckBox runat="server" ID="chkShowInactive" OnCheckedChanged="OnShowVendorsCheckChanged" AutoPostBack="True"
											Checked="False" />
										<label>Show Inactive Vendor Rating Profiles</label>
									</span>
									<br />
									<span class="fs75em pl20 sm_chk chk_list">
										<eShip:AltUniformCheckBox runat="server" ID="chkShowExpired" OnCheckedChanged="OnShowVendorsCheckChanged" AutoPostBack="True"
											Checked="False" />
										<label>Show Expired Vendor Rating Profiles</label>
									</span>
								</span>
							</div>
						</div>
						<div class="finderScroll h300 pt5">
							<ul class="sm_chk chk_list twocol_list" id="profiles">
								<asp:Repeater runat="server" ID="rptVendorRatingProfiles">
									<ItemTemplate>
										<asp:Panel runat="server" ID="pnlVendor">
											<li>
												<eShip:AltUniformCheckBox runat="server" ID="chkSelected" />
												<eShip:CustomHiddenField runat="server" ID="hidVendorRatingId" Value='<%# Eval("Id") %>' />
												<label class="pr10">
													<%# Eval("Name") %><%# (Eval("Active").ToBoolean()) ? string.Empty : "<span class='red'>*</span>" %>
													<%# Eval("ExpirationDate").ToDateTime() >= DateTime.Now ? string.Empty : "<span class='red'>+</span>" %>
												</label>
											</li>
										</asp:Panel>
									</ItemTemplate>
								</asp:Repeater>
							</ul>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
            </div>
        </div>
        <hr class="dark" />
        <div class="rowgroup">
            <div class="row mb10 right">
                <asp:Button runat="server" ID="btnRefreshSetList" Text="Refresh Analysis Sets" CssClass="mr10"
                    CausesValidation="False" OnClick="OnRefreshAnalysisSetList" />
                <asp:Button runat="server" ID="btnSubmit" Text="Submit For Analysis" OnClick="OnSubmitBatchRateDataForAnalysis" />
            </div>
        </div>

        <hr class="fat" />

        <div class="row">
            <h5>LTL Rate Analysis Sets</h5>
        </div>
        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheLtlRateAnalysisSetsTable" TableId="ltlRateAnalysisSetsTable" HeaderZIndex="2"/>
        <div class="rowgroup">
            <table class="line2 pl2" id="ltlRateAnalysisSetsTable">
                <tr>
                    <th style="width: 15%">Name</th>
                    <th style="width: 20%">Vendor Rating Profile</th>
                    <th style="width: 4%">Allow Indirect Point</th>
                    <th style="width: 8%">Analysis Effective Date</th>
                    <th style="width: 8%">Date Submitted</th>
                    <th style="width: 8%">Date Completed</th>
                    <th style="width: 14%">Submitted By</th>
                    <th style="width: 10%">Group Code</th>
                    <th style="width: 13%">Action</th>
                </tr>
                <asp:Repeater runat="server" ID="rptRateAnalysisSets" OnItemDataBound="OnRateAnalysisSetsItemDataBound">
                    <ItemTemplate>
                        <tr>
                            <td>
                                <eShip:CustomHiddenField runat="server" ID="hidBatchRateDataId" Value='<%# Eval("Id") %>' />
                                <%# Eval("Name") %> <span class="red"><asp:Literal runat="server" ID="litProcessing" Text="+" Visible="False" /></span>
                            </td>
                            <td>
                                <%# ActiveUser.HasAccessTo(ViewCode.VendorRating) 
                                                ? string.Format("{0} <a href='{1}?{2}={3}' class='blue' target='_blank' title='Go To Vendor Rating Record'><img src={4} width='16' class='middle'/></a>", 
                                                    Eval("VendorRatingProfile"),
                                                    ResolveUrl(VendorRatingView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    Eval("VendorRatingId").GetString().UrlTextEncrypt(),
                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))  
                                                : Eval("VendorRatingProfile") %>
                            </td>
                            <td class="text-center">
                                <%# Eval("AllowIndirectPoint").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                            </td>
                            <td>
                                <%# Eval("AnalysisEffectiveDate") %>
                            </td>
                            <td>
                                <%# Eval("DateSubmitted") %>
                            </td>
                            <td>
                                <%# Eval("DateCompleted") %>
                            </td>
                            <td>
                                <%# ActiveUser.HasAccessTo(ViewCode.User) 
                                                ? string.Format("{0} <a href='{1}?{2}={3}' class='blue' target='_blank' title='Go To User Record'><img src={4} width='16' class='middle'/></a>", 
                                                    Eval("SubmittedBy"),
                                                    ResolveUrl(UserView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    Eval("SubmittedByUserId").GetString().UrlTextEncrypt(),
                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))  
                                                : Eval("SubmittedBy") %>
                            </td>
                            <td>
                                <asp:Literal runat="server" ID="litGroupCode" Text='<%# Eval("GroupCode") %>' />
                            </td>
                            <td>
                                <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                    ToolTip="Delete Entry" CausesValidation="false" OnClick="OnBatchRateDataDelete" Enabled='<%# Access.Remove %>' />
                                <asp:ImageButton ID="ibtnEnqueue" runat="server" ImageUrl="~/images/icons2/arrow_general.png"
                                    ToolTip="Queue for immediate processing" CausesValidation="false" OnClick="OnBatchRateDataEnqueue" Visible='<%# Access.Modify %>' />
                                <asp:ImageButton ID="ibtnDownloadInput" runat="server" ImageUrl="~/images/icons2/document.png"
                                    CausesValidation="false" OnClick="OnBatchRateDataInputDownload" ToolTip="Download Input Set" />
                                <asp:ImageButton ID="ibtnDownloadOutput" runat="server" ImageUrl="~/images/icons2/exportBlue.png"
                                    CausesValidation="false" OnClick="OnBatchRateDataOutputDownload" ToolTip="Download Result Set"
                                    Visible='<%# !string.IsNullOrEmpty(Eval("DateCompleted").GetString()) && string.IsNullOrEmpty(Eval("GroupCode").GetString())  %>' />
                                <asp:ImageButton ID="ibtnDownloadOutputGroupOption" runat="server" ImageUrl="~/images/icons2/exportBlue.png"
                                    CausesValidation="false" OnClick="OnBatchRateDataOutputDownload" ToolTip="Download Result Set"
                                    Visible='<%# !string.IsNullOrEmpty(Eval("DateCompleted").GetString()) && !string.IsNullOrEmpty(Eval("GroupCode").GetString()) %>' />
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </div>
        <div id="instructionDiv" style="display: none;" class="popupControl">
            <div class="popheader">
                <h4>Instructions</h4>
            </div>
            <div class="rowgroup pl20">
                <div class="row">
                    <div class="fieldgroup">
                        <h5>Lane Input File Specification</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="fieldgroup">
                        <label class=" note">Prepare a tab or comma delimited file with the structure defined below.</label>
                    </div>
                </div>
                <div class="row">
                    <div class="fieldgroup">
                        <label class=" note">
                            NOTE: Header row must be present. Detail lines must not exceed
                        <asp:Literal runat="server" ID="litMaxLineCount" />.
                        </label>
                    </div>
                </div>
            </div>
            <table class="norm">
                <tr>
                    <td>
                        <label class="lhInherit">Line ID</label>
                    </td>
                    <td>
                        <label class="lhInherit">Origin Postal Code</label>
                    </td>
                    <td>
                        <label class="lhInherit">Origin Country</label>
                    </td>
                    <td>
                        <label class="lhInherit">Destination Postal Code</label>
                    </td>
                    <td>
                        <label class="lhInherit">Destination Country</label>
                    </td>
                    <td>
                        <label class="lhInherit">Weight (lb)</label>
                    </td>
                    <td>
                        <label class="lhInherit">Freight Class</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="lhInherit">
                            User defined
                            <br />
                            line identifier</label>
                    </td>
                    <td class="top">
                        <label class="lhInherit">12345</label>
                    </td>
                    <td>
                        <label class="lhInherit">
                            Country IATA code e.g.
                            <br />
                            US = USA, CA = Canada, MX = mexico</label>
                    </td>
                    <td class="top">
                        <label class="comlabel">16501</label>
                    </td>
                    <td>
                        <label class="lhInherit">
                            Country IATA code e.g.
                            <br />
                            US = USA, CA = Canada, MX = mexico</label>
                    </td>
                    <td class="top">
                        <label class="lhInherit">1000</label>
                    </td>
                    <td>
                        <label class="lhInherit">
                            50 - 500 e.g.
                            <br />
                            50, 55, 60, 250, 500</label>
                    </td>
                </tr>
            </table>
            <div class="row pl20 pt10 mb10">
                <div class="fieldgroup">
                    <label class=" note">*** click anywhere in this box to close pop-up.</label>
                </div>
            </div>
        </div>
    </div>
    
    <asp:UpdatePanel runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Panel ID="pnlDimScreenJS" CssClass="dimBackground" runat="server" Style="display: none;" />
            <eShip:CustomHiddenField runat="server" ID="hidGroupProcessFlag" />
            <eShip:CustomHiddenField runat="server" ID="hidProcessRecordId" />
            <eShip:CustomHiddenField runat="server" ID="hidShowInactiveVendors" />
        </ContentTemplate>
    </asp:UpdatePanel>

    
</asp:Content>
