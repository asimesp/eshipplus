﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="VendorRatingView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Rating.VendorRatingView"
    EnableEventValidation="false" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.Rating" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.Registry" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Rating" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        ShowEdit="true" ShowDelete="true" ShowSave="true" ShowFind="true" ShowExport="true"
        ShowMore="True" ShowImport="true" OnFind="OnToolbarFindClicked" OnSave="OnToolbarSaveClicked" OnUnlock="OnUnlockClicked"
        OnDelete="OnToolbarDeleteClicked" OnNew="OnToolbarNewClicked" OnEdit="OnToolbarEditClicked"
        OnImport="OpenVendorImportExport" OnExport="OpenVendorImportExport" OnCommand="OnToolbarCommandClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Vendor Rating
                <eShip:RecordIdentityDisplayControl runat="server" ID="ridcRecordIdentity" TargetControlId="txtName" />
            </h3>
            <div class="lockMessage">
                <asp:Literal ID="litMessage" runat="server" />
            </div>
        </div>

        <hr class="dark mb5" />

        <div class="errorMsgLit">
            <asp:Literal runat="server" ID="litErrorMessages" />
        </div>

        <eShip:VendorRatingFinderControl ID="vendorRatingFinder" runat="server" Visible="false" OpenForEditEnabled="true"
            OnItemSelected="OnVendorRatingFinderItemSelected" OnSelectionCancel="OnVendorRatingFinderSelectionCancelled" />
        <eShip:VendorRatingImportExportControl ID="vendorRatingImportExport" runat="server" Visible="false"
            OnClose="OnVendorRatingImportExportClose" OnProcessingError="OnVendorRatingImportExportProcessingError"
            OnLTLDiscountTiersImported="OnVendorRatingImportExportLTLDiscTierImported"
            OnLTLAccessorialsImported="OnVendorRatingImportExportLTLAccessorialsImported"
            OnLTLCFCRulesImported="OnVendorRatingImportExportLtlcfcRulesImported"
            OnLTLAdditionalChargesImported="OnVendorRatingImportExportLTLAddtnlChargesImported"
            OnLTLOverlengthRulesImported="OnVendorRatingImportExportLTLOverlengthRulesImported"
            OnLTLPackageSpecificRatesImported="OnVendorRatingImportExportLTLPackageSpecificRatesImported"
            OnLTLGuaranteedChargesImported="OnVendorRatingImportExportLTLGuaranteedChargesImported" />
        <eShip:VendorRatingMatchingRegionSearchControl runat="server" ID="vendorRatingMatchingRegionSearch"
            Visible="false" OnClose="OnVendorRatingMatchingRegionSearchClose" OnSelectedIndex="OnVendorRatingMathcingRegionSearchSelectedIndex" />
        <eShip:AjaxTabHeaderTextUpdater runat="server" ID="athtuTabUpdater" />

        <script type="text/javascript">
            function ClearHidFlag() {
                jsHelper.SetEmpty('<%= hidFlag.ClientID %>');
            }
        </script>

        <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
            Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" />

        <eShip:CustomHiddenField runat="server" ID="hidVendorRatingId" />
        <ajax:TabContainer ID="tabContainerMain" runat="server" BorderWidth="0" CssClass="ajaxCustom">
            <ajax:TabPanel runat="server" ID="tabDetails" HeaderText="Details">
                <ContentTemplate>
                    <eShip:VendorFinderControl ID="vendorFinder" runat="server" EnableMultiSelection="false"
                        Visible="False" OpenForEditEnabled="false" OnSelectionCancel="OnVendorFinderSelectionCancelled"
                        OnItemSelected="OnVendorFinderItemSelected" />
                    <asp:Panel runat="server" ID="pnlDetails">
                        <div class="rowgroup">
                            <div class="col_1_2 bbox">
                                <h5>Profile Details</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Name</label>
                                        <eShip:CustomTextBox ID="txtName" runat="server" MaxLength="50" CssClass="w200" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Display Name</label>
                                        <eShip:CustomTextBox ID="txtDisplayName" runat="server" MaxLength="50" CssClass="w200" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Vendor</label>
                                        <asp:HiddenField runat="server" ID="hidVendorId" />
                                        <eShip:CustomTextBox runat="server" ID="txtVendorNumber" OnTextChanged="OnVendorNumberEntered" CssClass="w110" AutoPostBack="True" />
                                        <asp:RequiredFieldValidator runat="server" ID="rfvVendor" ControlToValidate="txtVendorNumber"
                                            ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                        <asp:ImageButton ID="imgVendorSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                                            CausesValidation="False" OnClick="OnVendorSearchClicked" />
                                        <eShip:CustomTextBox runat="server" ID="txtVendorName" ReadOnly="True" CssClass="w280 disabled" />
                                        <ajax:AutoCompleteExtender runat="server" ID="aceVendor" TargetControlID="txtVendorNumber" ServiceMethod="GetVendorList" ServicePath="~/Services/eShipPlusWSv4P.asmx" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" DelimiterCharacters="" Enabled="True" />
                                    </div>
                                </div>
                                <h5 class="pt20">Options</h5>
                                <div class="row">
                                    <ul class="twocol_list pt5">
                                        <li>
                                            <asp:CheckBox runat="server" ID="chkEnablePackageSpecificRatePackageType" AutoPostBack="True" CssClass="jQueryUniform"
                                                OnCheckedChanged="OnchkEnablePackageSpecificRatePackageTypeCheckChanged" />
                                            <label>Enable Package Specific Rates</label>
                                        </li>
                                        <li>
                                            <asp:CheckBox runat="server" ID="chkEnablePcfToFcConversion" AutoPostBack="True" CssClass="jQueryUniform"
                                                OnCheckedChanged="OnchkEnablePcfToFcConversionCheckChanged" />
                                            <label>
                                                Enable LTL
                                                <abbr title="Pounds per cubic foot">PCF</abbr>
                                                to
                                                <abbr title="Freight Class">FC</abbr>
                                                <abbr title="Conversion">Conv.</abbr></label>
                                        </li>
                                        <li>
                                            <asp:CheckBox runat="server" ID="chkProject44Profile" AutoPostBack="True" CssClass="jQueryUniform" OnCheckedChanged="OnProject44ProfileCheckChanged" />
                                            <label>Project 44 Profile</label>
                                        </li>
                                    </ul>
                                </div>
                                <h5 class="pt20">General LTL Settings</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">LTL Tariff</label>
                                        <asp:DropDownList ID="ddlLTLTariff" DataTextField="Text" DataValueField="Value" runat="server" CssClass="w200" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Max LTL
                                            <abbr title="Cubic Foot">CF</abbr>
                                            for
                                            <abbr title="Pounds per cubic foot">PCF</abbr>
                                            <abbr title="Conversion">Conv.</abbr></label>
                                        <eShip:CustomTextBox runat="server" ID="txtLTLMaxCfForPcfConv" MaxLength="9" CssClass="w200" />
                                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" TargetControlID="txtLTLMaxCfForPcfConv"
                                            FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">SMC3 Service Level</label>
                                        <asp:DropDownList runat="server" ID="ddlSmc3ServiceLevel" DataTextField="Text" DataValueField="Value" CssClass="w200"/>
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Project44 Trading Partner Code</label>
                                        <eShip:CustomTextBox runat="server" ID="txtProject44TradingPartnerCode" CssClass="w200" MaxLength="50" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel"><abbr title="This text will be added to the critical BOL notes upon selection of a rate">Critical BOL Notes</abbr></label>
                                        <eShip:CustomTextBox runat="server" ID="txtCriticalBOLNotes" CssClass="w420" MaxLength="50" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            LTL Override Address
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleOverrideAddress" TargetControlId="txtOverrideAddress" MaxLength="100" />
                                        </label>
                                        <eShip:CustomTextBox runat="server" ID="txtOverrideAddress" TextMode="MultiLine" CssClass="w420 h150" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            <abbr title="This text will be displayed on the Rate Selection Display">Rating Detail Notes</abbr>
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleRatingDetailsNotes" TargetControlId="txtRatingDetailNotes" MaxLength="500" />
                                        </label>
                                        <eShip:CustomTextBox runat="server" ID="txtRatingDetailNotes" TextMode="MultiLine" CssClass="w420 h150" />
                                    </div>
                                </div>
                            </div>
                            <div class="col_1_2 bbox pl46 vlinedarkleft">
                                <h5>Profile Validity</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Expiration Date</label>
                                        <eShip:CustomTextBox ID="txtExpirationDate" runat="server" CssClass="w200" Type="Date" />
                                    </div>
                                    <div class="fieldgroup_s">
                                        <asp:CheckBox runat="server" ID="chkActive" CssClass="jQueryUniform" />
                                        <label class="w180">Active</label>
                                    </div>
                                </div>
                                <h5 class="pt20">LTL Fuel Settings</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <label class="wlabel">
                                                    Fuel Table
                                                    <a runat="server" id="hypFuelTable" target="_blank" title="Go To Fuel Table Record" visible="False">
                                                        <img src='<%= ResolveUrl("~/images/icons2/arrow_newTab.png") %>' alt="" width="16" class="middle" />
                                                    </a>
                                                </label>
                                                <asp:DropDownList ID="ddlFuelTable" DataTextField="Text" DataValueField="Value" runat="server" CssClass="w200"
                                                    AutoPostBack="True" OnSelectedIndexChanged="OnFuelTableSelectedIndexChanged" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Fuel Index Region</label>
                                        <asp:DropDownList ID="ddlFuelIndexRegion" DataValueField="Key" DataTextField="Value" runat="server" CssClass="w200" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Fuel Charge Code</label>
                                        <eShip:CachedObjectDropDownList Type="ChargeCodes" ID="ddlFuelChargeCode" runat="server" CssClass="w200" 
                                            EnableChooseOne="True" DefaultValue="0" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Current LTL Fuel Surcharge (%)</label>
                                        <eShip:CustomTextBox ID="txtCurrentLTLFuelMarkUp" runat="server" MaxLength="9" CssClass="w200" />
                                        <asp:RequiredFieldValidator runat="server" ID="rfvCurrentLTLFuelMarkUp" ControlToValidate="txtCurrentLTLFuelMarkUp"
                                            ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtCurrentLTLFuelMarkUp"
                                            FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Fuel Surcharge Floor (%)</label>
                                        <eShip:CustomTextBox ID="txtFuelMarkUpFloor" MaxLength="9" runat="server" CssClass="w200" />
                                        <asp:RequiredFieldValidator runat="server" ID="rfvFuelMarkUpFloor" ControlToValidate="txtFuelMarkUpFloor"
                                            ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="txtFuelMarkUpFloor"
                                            FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Fuel Surcharge Ceiling (%)</label>
                                        <eShip:CustomTextBox ID="txtFuelMarkUpCeiling" runat="server" MaxLength="9" CssClass="w200" />
                                        <asp:RequiredFieldValidator runat="server" ID="rfvFuelMarkUpCeiling" ControlToValidate="txtFuelMarkUpCeiling"
                                            ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtFuelMarkUpCeiling"
                                            FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Fuel Updates On</label>
                                        <asp:DropDownList ID="ddlFuelUpdatesOn" DataTextField="Value" DataValueField="Key" runat="server" CssClass="w200" />
                                    </div>
                                </div>
                                <h5>LTL Truck Dimension Limits</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Max Rating Length (in)</label>
                                        <eShip:CustomTextBox ID="txtLTLTruckLength" runat="server" MaxLength="9" CssClass="w200" />
                                        <asp:RequiredFieldValidator runat="server" ID="rfvLTLTruckLength" ControlToValidate="txtLTLTruckLength"
                                            ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtLTLTruckLength"
                                            FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Max Rating Width (in)</label>
                                        <eShip:CustomTextBox ID="txtLTLTruckWidth" runat="server" MaxLength="9" CssClass="w200" />
                                        <asp:RequiredFieldValidator runat="server" ID="rfvLTLTruckWidth" ControlToValidate="txtLTLTruckWidth"
                                            ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtLTLTruckWidth"
                                            FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Max Rating Height (in)</label>
                                        <eShip:CustomTextBox ID="txtLTLTruckHeight" runat="server" MaxLength="9" CssClass="w200" />
                                        <asp:RequiredFieldValidator runat="server" ID="rfvLTLTruckHeight" ControlToValidate="txtLTLTruckHeight"
                                            ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtLTLTruckHeight"
                                            FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Max Weight (lb)</label>
                                        <eShip:CustomTextBox ID="txtLTLTruckWeight" runat="server" MaxLength="19" CssClass="w200" />
                                        <asp:RequiredFieldValidator runat="server" ID="rfvLTLTruckWeight" ControlToValidate="txtLTLTruckWeight"
                                            ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtLTLTruckWeight"
                                            FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Min Pickup Weight (lb)</label>
                                        <eShip:CustomTextBox runat="server" ID="txtLTLMinPickupWeight" MaxLength="9" CssClass="w200" />
                                        <asp:RequiredFieldValidator runat="server" ID="rfvLTLMinPickupWeight" ControlToValidate="txtLTLMinPickupWeight"
                                            ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender33" runat="server" TargetControlID="txtLTLMinPickupWeight"
                                            FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Max Unit Weight (lb)</label>
                                        <eShip:CustomTextBox ID="txtLTLTruckUnitWeight" runat="server" MaxLength="9" CssClass="w200" />
                                        <asp:RequiredFieldValidator runat="server" ID="rfvLTLTruckUnitWeight" ControlToValidate="txtLTLTruckUnitWeight"
                                            ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="txtLTLTruckUnitWeight"
                                            FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Max Package Quantity</label>
                                        <eShip:CustomTextBox runat="server" ID="txtMaxPackageQuantity" MaxLength="50" CssClass="w200" />
                                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" TargetControlID="txtMaxPackageQuantity"
                                            FilterType="Numbers" Enabled="True" />
                                    </div>
                                    <div class="fieldgroup_s">
                                        <asp:CheckBox runat="server" ID="chkMaxPackageQuantityApplies" CssClass="jQueryUniform" />
                                        <label>Max Package
                                            <abbr title="Quantity">Qty.</abbr>
                                            Applies</label>
                                    </div>
                                </div>
                                <h5 class="pt20">Individual Item Limits</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Max Item Length</label>
                                        <eShip:CustomTextBox runat="server" ID="txtMaxItemLength" CssClass="w200" />
                                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" TargetControlID="txtMaxItemLength"
                                            FilterType="Numbers" Enabled="True" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Max Item Width</label>
                                        <eShip:CustomTextBox runat="server" ID="txtMaxItemWidth" CssClass="w200" />
                                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" TargetControlID="txtMaxItemWidth"
                                            FilterType="Numbers" Enabled="True" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Max Item Height</label>
                                        <eShip:CustomTextBox runat="server" ID="txtMaxItemHeight" CssClass="w200" />
                                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" TargetControlID="txtMaxItemHeight"
                                            FilterType="Numbers" Enabled="True" />
                                    </div>
                                    <div class="fieldgroup_s">
                                        <asp:CheckBox runat="server" ID="chkIndividualItemLimitsApply" CssClass="jQueryUniform" />
                                        <label>Individual Item Limits Apply</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabLTLPcfToFcConvTab" HeaderText="<abbr title='pounds per cubic feet'>PCF</abbr> to <abbr title='freight class'>FC</abbr> Conv." Visible="False">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlLTLPcfToFcConvTab" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlLTLPcfToFcConvTab" DefaultButton="btnAddLTLPcfToFcConvTab">
                                <div class="row mb10">
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddLTLPcfToFcConvTab" Text="Add LTL Pcf to Fc Conv."
                                            CssClass="mr10" CausesValidation="false" OnClick="OnAddLTLPcfToFcConvTabClicked" />
                                        <asp:Button runat="server" ID="btnClearLTLPcfToFcConvTab" Text="Clear LTL PCF to FC Conv."
                                            CausesValidation="false" OnClick="OnClearLTLPcfToFcConvTabClicked" />
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheLTLpcfToFcConvTabTable" TableId="LTLpcfToFcConvTabTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <asp:ListView ID="lstLTLPcfToFcConvTab" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                    <LayoutTemplate>
                                        <table id="LTLpcfToFcConvTabTable" class="stripe">
                                            <tr>
                                                <th style="width: 15%;">Effective
                                                </th>
                                                <th style="width: 75%;">Conversion Table
                                                </th>
                                                <th style="width: 10%;" class="text-center">Action
                                                </th>
                                            </tr>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </table>
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tr class="bottom_shadow">
                                            <td class="top">
                                                <eShip:CustomHiddenField ID="hidLTLPcfToFcConvTabIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                <%# Eval("EffectiveDate").FormattedShortDate() %>
                                            </td>
                                            <td>
                                                <%# LTLPcfToFcConversion.GetConversionTable(Eval("ConversionTableData").GetString()).GetHtmlTable()%>
                                            </td>
                                            <td class="text-center top">
                                                <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png" OnClientClick="$(window).scrollTop(0);"
                                                    CausesValidation="false" OnClick="OnEditLTLPcfToFcConvTabClicked" Enabled='<%# Access.Modify %>' />
                                                <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                    CausesValidation="false" OnClick="OnDeleteLTLPcfToFcConvTabClicked" Enabled='<%# Access.Modify %>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                                <asp:Panel runat="server" ID="pnlEditLTLPcfToFcConvTab" Visible="False" CssClass="popup popupControlOverW500" DefaultButton="btnEditLTLPcfToFcConvTabDone">
                                    <div class="popheader mb10">
                                        <h4>Add/Modify LTL
                                    <abbr title='Pounds per Cubic Feet'>PCF</abbr>
                                            to
                                    <abbr title='Freight Class'>FC</abbr>
                                            Conversion Table
                                        </h4>
                                        <asp:ImageButton ID="ibtnLTLPcfToFcConvTabClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                                            CausesValidation="false" OnClick="OnCloseEditLTLPcfToFcConvTabClicked" runat="server" />
                                    </div>
                                    <table class="poptable" style="width: 500px;">
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Effective Date:</label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox runat="server" ID="txtLTLPcfToFcConvTabEffectiveDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>
                                                <div class="note">
                                                    <p>Sample conversion table format (data can be separated by comma, tab, pipe, colon or semi-colon):</p>
                                                    <table>
                                                        <tr>
                                                            <td>Lower Pcf</td>
                                                            <td>Upper Pcf</td>
                                                            <td>Freight Class</td>
                                                        </tr>
                                                        <tr>
                                                            <td>0.0000</td>
                                                            <td>1.9999</td>
                                                            <td>125</td>
                                                        </tr>
                                                        <tr>
                                                            <td>2.0000</td>
                                                            <td>4.9999</td>
                                                            <td>250</td>
                                                        </tr>
                                                    </table>
                                                    <p>
                                                        * Sample above seperated by tab
                                                <br />
                                                        * Header row is required
                                                <br />
                                                        * Cut and paste data from excel or notepad
                                                    </p>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right top">
                                                <label class="upper">Table:</label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox runat="server" ID="txtLTLPcfToFcConvTabConvTable" TextMode="MultiLine" CssClass="w360 h150" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="text-right pr25">
                                                <asp:Button ID="btnEditLTLPcfToFcConvTabDone" Text="Done" OnClick="OnEditLTLPcfToFcConvTabDoneClicked"
                                                    runat="server" CausesValidation="false" />
                                                <asp:Button ID="btnEditLTLPcfToFcConvTabCancel" Text="Cancel" OnClick="OnCloseEditLTLPcfToFcConvTabClicked"
                                                    runat="server" CausesValidation="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabLTLPackageSpecificRates" HeaderText="Pkg. Spec. Rates" Visible="False">
                <ContentTemplate>
                    <asp:UpdatePanel ID="upPnlPackageSpecificRates" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlLTLPackageSpecificRatesTab" DefaultButton="btnAddLTLPackageSpecificRate">
                                <div class="rowgroup mb10">
                                    <div class="row">
                                        <div class="fieldgroup mr20">
                                            <label class="wlabel blue">Rate Package Type</label>
                                            <eShip:CachedObjectDropDownList Type="PackageType" ID="ddlPackageSpecificRatePackageType" runat="server" CssClass="w260" 
                                                EnableChooseOne="True" DefaultValue="0" />
                                        </div>
                                        <div class="fieldgroup">
                                            <label class="wlabel blue">Rate Freight Charge Code</label>
                                            <eShip:CachedObjectDropDownList Type="ChargeCodes" ID="ddlLTLPackageSpecificRateFreightChargeCode" runat="server" CssClass="w260" 
                                                EnableChooseOne="True" DefaultValue="0" />
                                        </div>
                                    </div>
                                    <h5>Rate Tiers</h5>

                                    <div class="row pb10">
                                        <div class="fieldgroup left">
                                            <eShip:PaginationExtender runat="server" ID="peLstPackageSpecificRates" TargetControlId="lstPackageSpecificRates" PageSize="10" UseParentDataStore="True" />
                                        </div>
                                        <div class="fieldgroup right">
                                            <asp:Button runat="server" ID="btnAddLTLPackageSpecificRate" Text="Add LTL Package Specific Rate"
                                                CssClass="mr10" CausesValidation="false" OnClick="OnAddLTLPackageSpecificRateClicked" />
                                            <asp:Button runat="server" ID="btnClearLTLPackageSpecificRates" Text="Clear LTL Package Specific Rates"
                                                CausesValidation="false" OnClick="OnClearLTLPackageSpecificRatesClicked" />
                                        </div>
                                    </div>
                                </div>
                                <table class="stripe">
                                    <tr>
                                        <th style="width: 3%">Index</th>
                                        <th style="width: 27%;">Origin Region
                                        </th>
                                        <th style="width: 27%;">Destination Region
                                        </th>
                                        <th style="width: 15%;">Effective Date
                                        </th>
                                        <th style="width: 5%" class="text-center">Rate Priority
                                        </th>
                                        <th style="width: 5%;" class="text-center">
                                            <abbr title="Package Quantity">Pkg. Qty.</abbr>
                                        </th>
                                        <th style="width: 8%;" class="text-right">Flat Rate ($)
                                        </th>
                                        <th style="width: 10%;" class="text-center">Action
                                        </th>
                                    </tr>

                                    <asp:ListView ID="lstPackageSpecificRates" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <%# string.Format("{0}. ", (Container.DataItemIndex+1) + (peLstPackageSpecificRates.CurrentPage-1) * peLstPackageSpecificRates.PageSize) %>
                                                </td>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidPackageSpecificRateTabIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <%# ddlLTLPackageSpecificRateOriginRegion.GetValueText(Eval("OriginRegionId").GetString()) %>
                                                </td>
                                                <td>
                                                    <%# ddlLTLPackageSpecificRateDestinationRegion.GetValueText(Eval("DestinationRegionId").GetString())%>
                                                </td>
                                                <td>
                                                    <%# Eval("EffectiveDate").Equals(DateUtility.SystemEarliestDateTime) ? string.Empty : Eval("EffectiveDate").FormattedLongDateAlt() %>
                                                </td>
                                                <td class="text-center">
                                                    <%# Eval("RatePriority") %>
                                                </td>
                                                <td class="text-center">
                                                    <%# Eval("PackageQuantity") %>
                                                </td>
                                                <td class="text-right">
                                                    <%# Eval("Rate", "{0:n2}") %>
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png" OnClientClick="$(window).scrollTop(0);"
                                                        CausesValidation="false" OnClick="OnEditLTLPackageSpecificRateClicked" Enabled='<%# Access.Modify %>' />
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        CausesValidation="false" OnClick="OnDeleteLTLPackageSpecificRateClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                                <asp:Panel runat="server" ID="pnlEditPackageSpecificRate" Visible="False" CssClass="popup popupControlOverW500" DefaultButton="btnEditPackageSpecificRateDone">
                                    <div class="popheader">
                                        <h4>Add/Modify Package Specific Rate</h4>
                                        <asp:ImageButton ID="ibtnCloseEditPackageSpecificRate" ImageUrl="~/images/icons2/close.png" CssClass="close"
                                            CausesValidation="false" OnClick="OnCloseEditLTLPackageSpecificRateClicked" runat="server" />
                                    </div>
                                    <div class="row mt10">
                                        <table class="poptable">
                                            <tr>
                                                <td class="text-right">
                                                    <label class="upper">Origin Region:</label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlLTLPackageSpecificRateOriginRegion" DataTextField="Text" DataValueField="Value"
                                                        CssClass="w200" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right pl10">
                                                    <label class="upper">Destination Region:</label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlLTLPackageSpecificRateDestinationRegion" DataTextField="Text" DataValueField="Value"
                                                        CssClass="w200" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right; width: 125px;">
                                                    <label class="upper">Effective Date:</label>
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox runat="server" ID="txtPackageSpecificRateEffectiveDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right">
                                                    <label class="upper">Rate Priority:</label>
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtLTLPackageSpecificRateRatePriority" runat="server" CssClass="w200" Type="NumbersOnly" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right">
                                                    <label class="upper">Package Quantity:</label>
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtLTLPackageSpecificRateQuantity" runat="server" CssClass="w200" Type="NumbersOnly" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right">
                                                    <label class="upper">Flat Rate:</label>
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtLTLPackageSpecificRateFlatRate" runat="server" CssClass="w200" Type="FloatingPointNumbers" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>
                                                    <script type="text/javascript">
                                                        function CheckForPackageSpecificRateRegions() {

                                                            if ($(jsHelper.AddHashTag('<%= ddlLTLPackageSpecificRateOriginRegion.ClientID %>')).val() == jsHelper.DefaultId ||
                                                        $(jsHelper.AddHashTag('<%= ddlLTLPackageSpecificRateDestinationRegion.ClientID %>')).val() == jsHelper.DefaultId) {
                                                                alert("Please select an Origin and/or Destination Region");
                                                                return false;
                                                            }
                                                            return true;

                                                        }
                                                    </script>
                                                    <asp:Button ID="btnEditPackageSpecificRateDone" Text="Done" OnClientClick="javascript: return CheckForPackageSpecificRateRegions();"
                                                        OnClick="OnEditLTLPackageSpecificRateDoneClicked" runat="server" CausesValidation="false" />
                                                    <asp:Button ID="btnEditPackageSpecificRateCancel" Text="Cancel" OnClick="OnCloseEditLTLPackageSpecificRateClicked"
                                                        runat="server" CausesValidation="false" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabLTLDiscountTiers" HeaderText="Disc. Tiers">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlDiscountTiers" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel ID="pnlLTLDiscountTiers" runat="server" DefaultButton="btnAddLTLDiscountTier">
                                <div class="rowgroup mb10">
                                    <div class="row">
                                        <div class="fieldgroup">
                                            <eShip:PaginationExtender runat="server" ID="peLstLTLDiscountTiers" TargetControlId="lstLTLDiscountTiers" PageSize="10" UseParentDataStore="True" />
                                        </div>
                                        <div class="fieldgroup right">
                                            <asp:Button runat="server" ID="btnAddLTLDiscountTier" Text="Add LTL Discount Tier"
                                                CssClass="mr10" CausesValidation="false" OnClick="OnAddLTLDiscountTierClicked" />
                                            <asp:Button runat="server" ID="btnClearLTLDiscountTiers" Text="Clear LTL Discount Tiers"
                                                CausesValidation="false" OnClick="OnClearLTLDiscountTierClicked" />
                                        </div>
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheLTLDiscountTiersTable" TableId="LTLDiscountTiersTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table class="stripe" id="LTLDiscountTiersTable">
                                    <tr>
                                        <th style="width: 4%">Index
                                        </th>
                                        <th style="width: 12%;">Rate Basis
                                        </th>
                                        <th style="width: 10%;" class="text-right">Discount (%)
                                        </th>
                                        <th style="width: 8%;">
                                            <abbr title="Tariff Cut-off weight break in SMC3 price return">
                                                Stop Alt. Wgt.</abbr>
                                        </th>
                                        <th style="width: 9%;" class="text-right">Floor Value ($)
                                        </th>
                                        <th style="width: 9%;" class="=text-right">Ceiling Value ($)
                                        </th>
                                        <th style="width: 25%;">Freight Charge Code
                                        </th>
                                        <th style="width: 5%;" class="text-center">Tier Priority
                                        </th>
                                        <th style="width: 10%;">Effective
                                        </th>
                                        <th style="width: 8%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstLTLDiscountTiers" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td rowspan="2" class="top">
                                                    <%# string.Format("{0}. ", (Container.DataItemIndex+1) + (peLstLTLDiscountTiers.CurrentPage-1) * peLstLTLDiscountTiers.PageSize) %>
                                                    <eShip:CustomHiddenField ID="hidDiscountTierIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                </td>
                                                <td>
                                                    <%# Eval("RateBasis").FormattedString() %>
                                                </td>
                                                <td class="text-right">
                                                    <%# Eval("DiscountPercent", "{0:n2}") %>
                                                </td>
                                                <td>
                                                    <%# Eval("WeightBreak") %>
                                                </td>
                                                <td class="text-right">
                                                    <%# Eval("FloorValue", "{0:n2}") %>
                                                </td>
                                                <td class="text-right">
                                                    <%# Eval("CeilingValue", "{0:n2}") %>
                                                </td>
                                                <td>
                                                    <%# new ChargeCode(Eval("FreightChargeCodeId").ToLong()).Description %>
                                                </td>
                                                <td class="text-center">
                                                    <%# Eval("TierPriority")%>
                                                </td>
                                                <td>
                                                    <%# Eval("EffectiveDate").FormattedShortDate() %>
                                                </td>
                                                <td rowspan="2" class="text-center top">
                                                    <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png" OnClientClick="$(window).scrollTop(0);"
                                                        CausesValidation="false" OnClick="OnEditDiscountTierClicked" Enabled='<%# Access.Modify %>' />
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        CausesValidation="false" OnClick="OnDeleteDiscountTierClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                            <tr class="hidden">
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr class="bottom_shadow">
                                                <td colspan="8">
                                                    <div class="col_1_3">
                                                        <div class="rowgroup">
                                                            <div class="row">
                                                                <div class="fieldgroup">
                                                                    <label class="wlabel blue">Origin Region</label>
                                                                    <label>
                                                                        <%# ddlDiscountTierOriginRegion.GetValueText(Eval("OriginRegionId").GetString()) %>
                                                                        <%# ActiveUser.HasAccessTo(ViewCode.Region) 
                                                                            ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Region Record'><img src='{3}' alt='' width='16' class='middle'/></a>", 
                                                                                ResolveUrl(RegionView.PageAddress),
                                                                                WebApplicationConstants.TransferNumber, 
                                                                                Eval("OriginRegionId").GetString().UrlTextEncrypt(),
                                                                                ResolveUrl("~/images/icons2/arrow_newTab.png"))  
                                                                            : string.Empty %>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="row mt10">
                                                                <div class="fieldgroup">
                                                                    <label class="wlabel blue">Destination Region</label>
                                                                    <label>
                                                                        <%# ddlDiscountTierDestinationRegion.GetValueText(Eval("DestinationRegionId").GetString())%>
                                                                        <%# ActiveUser.HasAccessTo(ViewCode.Region) 
                                                                            ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Region Record'><img src='{3}' alt='' width='16' class='middle'/></a>", 
                                                                                ResolveUrl(RegionView.PageAddress),
                                                                                WebApplicationConstants.TransferNumber, 
                                                                                Eval("DestinationRegionId").GetString().UrlTextEncrypt(),
                                                                                ResolveUrl("~/images/icons2/arrow_newTab.png"))  
                                                                            : string.Empty %>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col_2_3">
                                                        <h6>FAK</h6>
                                                        <div class="row bottom_shadow pl46">
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue w50">50</label>
                                                                <label><%# Eval("FAK50") %></label>
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue w50">55</label>
                                                                <label><%# Eval("FAK55") %></label>
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue w50">60</label>
                                                                <label><%# Eval("FAK60") %></label>
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue w50">65</label>
                                                                <label><%# Eval("FAK65") %></label>
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue w50">70</label>
                                                                <label><%# Eval("FAK70") %></label>
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue w50">77.5</label>
                                                                <label><%# Eval("FAK775") %></label>
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue w50">85</label>
                                                                <label><%# Eval("FAK85") %></label>
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue w50">92.5</label>
                                                                <label><%# Eval("FAK925") %></label>
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue w50">100</label>
                                                                <label><%# Eval("FAK100") %></label>
                                                            </div>
                                                        </div>
                                                        <div class="row pl46">
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue w50">110</label>
                                                                <label><%# Eval("FAK110") %></label>
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue w50">125</label>
                                                                <label><%# Eval("FAK125") %></label>
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue w50">150</label>
                                                                <label><%# Eval("FAK150") %></label>
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue w50">175</label>
                                                                <label><%# Eval("FAK175") %></label>
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue w50">200</label>
                                                                <label><%# Eval("FAK200") %></label>
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue w50">250</label>
                                                                <label><%# Eval("FAK250") %></label>
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue w50">300</label>
                                                                <label><%# Eval("FAK300") %></label>
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue w50">400</label>
                                                                <label><%# Eval("FAK400") %></label>
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue w50">500</label>
                                                                <label><%# Eval("FAK500") %></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>

                                </table>
                                <script type="text/javascript">

                                    function CheckForRegions() {
                                        if ($(jsHelper.AddHashTag('<%= ddlDiscountTierOriginRegion.ClientID %>')).val() == jsHelper.DefaultId || $(jsHelper.AddHashTag('<%= ddlDiscountTierDestinationRegion.ClientID %>')).val() == jsHelper.DefaultId) {
                                            alert("Please select an Origin and/or Destination Region");
                                            return false;
                                        }
                                        return true;
                                    }
                                </script>

                                <asp:Panel ID="pnlEditDiscountTier" runat="server" Visible="False" CssClass="popup" DefaultButton="btnDone">
                                    <div class="popheader">
                                        <h4>Add/Modify LTL Discount Tier</h4>
                                        <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                                            CausesValidation="false" OnClick="OnCloseEditDiscountTierClicked" runat="server" />
                                    </div>
                                    <div class="row mt20">
                                        <div class="col_1_2 bbox vlinedarkright pl20">
                                            <div class="row">
                                                <h5 class="pl40">Tier Attributes</h5>
                                                <table class="poptable">
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">Origin Region:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlDiscountTierOriginRegion" DataTextField="Text" DataValueField="Value"
                                                                CssClass="w200" runat="server" />
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">Destination Region:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlDiscountTierDestinationRegion" DataTextField="Text" DataValueField="Value"
                                                                CssClass="w200" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">Rate Basis:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlDiscountTierRateBasis" DataTextField="Value" DataValueField="Key"
                                                                CssClass="w200" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">Freight Charge Code:</label>
                                                        </td>
                                                        <td>
                                                            <eShip:CachedObjectDropDownList Type="ChargeCodes" ID="ddlDiscountTierFreightChargeCode" runat="server" CssClass="w200" 
                                                                EnableChooseOne="True" DefaultValue="0" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">Tier Priority:</label>
                                                        </td>
                                                        <td>
                                                            <eShip:CustomTextBox ID="txtDiscountTierTierPriority" runat="server" CssClass="w200" Type="NumbersOnly" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">Effective Date:</label>
                                                        </td>
                                                        <td>
                                                            <eShip:CustomTextBox runat="server" ID="txtDiscountTierEffectiveDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">Max. Weight Break:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlDiscountTierWeightBreak" DataTextField="Value" DataValueField="Key"
                                                                CssClass="w200" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">Discount (%):</label>
                                                        </td>
                                                        <td>
                                                            <eShip:CustomTextBox ID="txtDiscountTierDiscountPercent" runat="server" MaxLength="9" CssClass="w200" Type="FloatingPointNumbers" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">Tier Ceiling Value ($):</label>
                                                        </td>
                                                        <td>
                                                            <eShip:CustomTextBox ID="txtDiscountTierDiscountCeilingValue" runat="server" MaxLength="9" CssClass="w200" Type="FloatingPointNumbers" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">Tier Floor Value ($):</label>
                                                        </td>
                                                        <td>
                                                            <eShip:CustomTextBox ID="txtDiscountTierDiscountFloorValue" runat="server" MaxLength="9" CssClass="w200" Type="FloatingPointNumbers" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col_1_2 bbox pl40">
                                            <div class="row">
                                                <h5 class="pl20">FAK</h5>
                                                <table class="poptable">
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">FAK 50:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlDiscountTierFak50" CssClass="w90" runat="server" />
                                                        </td>
                                                        <td class="text-right">
                                                            <label class="upper">FAK 110:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlDiscountTierFak110" CssClass="w90" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">FAK 55:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlDiscountTierFak55" CssClass="w90" runat="server" />
                                                        </td>
                                                        <td class="text-right">
                                                            <label class="upper">FAK 125:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlDiscountTierFak125" CssClass="w90" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">FAK 60:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlDiscountTierFak60" CssClass="w90" runat="server" />
                                                        </td>
                                                        <td class="text-right">
                                                            <label class="upper">FAK 150:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlDiscountTierFak150" CssClass="w90" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">FAK 65:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlDiscountTierFak65" CssClass="w90" runat="server" />
                                                        </td>
                                                        <td class="text-right">
                                                            <label class="=upper">FAK 175:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlDiscountTierFak175" CssClass="w90" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">FAK 70:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlDiscountTierFak70" CssClass="w90" runat="server" />
                                                        </td>
                                                        <td class="text-right">
                                                            <label class="upper">FAK 200:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlDiscountTierFak200" CssClass="w90" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">FAK 77.5:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlDiscountTierFak775" CssClass="w90" runat="server" />
                                                        </td>
                                                        <td class="text-right">
                                                            <label class="upper">FAK 250:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlDiscountTierFak250" CssClass="w90" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">FAK 85:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlDiscountTierFak85" CssClass="w90" runat="server" />
                                                        </td>
                                                        <td class="text-right">
                                                            <label class="upper">FAK 300:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlDiscountTierFak300" CssClass="w90" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">FAK 92.5:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlDiscountTierFak925" CssClass="w90" runat="server" />
                                                        </td>
                                                        <td class="text-right">
                                                            <label class="upper">FAK 400:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlDiscountTierFak400" CssClass="w90" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr class="bottom_shadow">
                                                        <td class="text-right">
                                                            <label class="upper">FAK 100:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlDiscountTierFak100" CssClass="w90" runat="server" />
                                                        </td>
                                                        <td class="text-right">
                                                            <label class="upper">FAK 500:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlDiscountTierFak500" CssClass="w90" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="20" class="pt10">
                                                            <asp:Button ID="btnDone" Text="Done" OnClick="OnEditDiscountTierDoneClicked" OnClientClick="javascript: return CheckForRegions();" runat="server" />
                                                            <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnCloseEditDiscountTierClicked" runat="server" CausesValidation="false" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt10 mb10 pl46">
                                        <div class="fieldgroup">
                                        </div>
                                    </div>
                                </asp:Panel>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabLTLAccessorials" HeaderText="Accessorials">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlAccessorials" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel ID="pnlLTLAccessorials" runat="server" DefaultButton="btnAddLTLAccessorial">
                                <div class="rowgroup mb10">
                                    <div class="row">
                                        <div class="fieldgroup right">
                                            <asp:Button runat="server" ID="btnAddLTLAccessorial" Text="Add LTL Accessorial" CssClass="mr10"
                                                CausesValidation="false" OnClick="OnAddLTLAccessorialClicked" />
                                            <asp:Button runat="server" ID="btnClearLTLAccessorials" Text="Clear LTL Accessorials"
                                                CausesValidation="false" OnClick="OnClearLTLAccessorialsClicked" />
                                        </div>
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheLtlAccessorialsTable" TableId="ltlAccessorialsTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table class="stripe" id="ltlAccessorialsTable">
                                    <tr>
                                        <th style="width: 37%;">Service
                                        </th>
                                        <th style="width: 15%;">Rate Type
                                        </th>
                                        <th style="width: 10%;" class="text-right">Rate ($)
                                        </th>
                                        <th style="width: 10%;" class="text-right">Floor Value ($)
                                        </th>
                                        <th style="width: 10%;" class="text-right">Ceiling Value ($)
                                        </th>
                                        <th style="width: 10%">Effective
                                        </th>
                                        <th style="width: 8%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstLTLAccessorials" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidLTLAccessorialIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <%# new Service(Eval("ServiceId").ToLong()).Description %>
                                                </td>
                                                <td>
                                                    <%# Eval("RateType").FormattedString() %>
                                                </td>
                                                <td class="text-right">
                                                    <%# Eval("Rate", "{0:n2}") %>
                                                </td>
                                                <td class="text-right">
                                                    <%# Eval("FloorValue", "{0:n2}") %>
                                                </td>
                                                <td class="text-right">
                                                    <%# Eval("CeilingValue", "{0:n2}") %>
                                                </td>
                                                <td>
                                                    <%# Eval("EffectiveDate").FormattedShortDate() %>
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png" OnClientClick="$(window).scrollTop(0);"
                                                        CausesValidation="false" OnClick="OnEditLTLAccssorialClicked" Enabled='<%# Access.Modify %>' />
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        CausesValidation="false" OnClick="OnDeleteLTLAccssorialClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>

                                <asp:Panel runat="server" ID="pnlEditLTLAccessorial" Visible="false" CssClass="popup popupControlOverW500" DefaultButton="btnEditLTLAccessorialDone">
                                    <div class="popheader mb10">
                                        <h4>Add/Modify LTL Accessorial</h4>
                                        <asp:ImageButton ImageUrl="~/images/icons2/close.png" CssClass="close" CausesValidation="false" OnClick="OnCloseEditLTLAccessorialClicked" runat="server" />
                                    </div>
                                    <table class="poptable">
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Service:</label>
                                            </td>
                                            <td>
                                                <eShip:CachedObjectDropDownList Type="Services" ID="ddlLTLAccessorialService" runat="server" CssClass="w200" 
                                                    EnableChooseOne="True" DefaultValue="0" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Rate Type:</label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlLTLAccessorialRateType" DataTextField="Value" DataValueField="Key" CssClass="w200" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Rate ($):</label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox ID="txtLTLAccessorialRate" CssClass="w200" MaxLength="19" runat="server" Type="FloatingPointNumbers" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Rate Floor Value ($):</label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox ID="txtLTLAccessorialRateFloorValue" CssClass="w200" MaxLength="19" runat="server" Type="FloatingPointNumbers" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Rate Ceiling Value ($):</label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox ID="txtLTLAccessorialRateCeilingValue" CssClass="w200" MaxLength="19" runat="server" Type="FloatingPointNumbers" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Effective Date:</label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox runat="server" ID="txtLTLAccessorialEffectiveDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <asp:Button ID="btnEditLTLAccessorialDone" Text="Done" OnClick="OnEditLTLAccessorialDoneClicked" runat="server" CausesValidation="False" />
                                                <asp:Button ID="btnEditLTLAccessorialCancel" Text="Cancel" OnClick="OnCloseEditLTLAccessorialClicked" runat="server" CausesValidation="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabLTLGuaranteedCharges" HeaderText="Guar. Chg.">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="upPnlGuaranteedCharges">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlLTLGuaranteedCharges">
                                <div class="rowgroup mb10">
                                    <div class="row">
                                        <div class="fieldgroup right">
                                            <asp:Button runat="server" ID="btnLTLAddGuaranteedCharge" Text="Add LTL Guaranteed Charge"
                                                CssClass="mr10" CausesValidation="false" OnClick="OnAddLTLGuaranteedChargeClicked" />
                                            <asp:Button runat="server" ID="btnClearLTLGuaranteedCharges" Text="Clear LTL Guaranteed Charges"
                                                CausesValidation="false" OnClick="OnClearLTLGuaranteedChargesClicked" />
                                        </div>
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheLtlGuaranteedChargesTable" TableId="ltlGuaranteedChargesTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table class="stripe" id="ltlGuaranteedChargesTable">
                                    <tr>
                                        <th style="width: 20%;">Description
                                        </th>
                                        <th style="width: 6%;">Time
                                        </th>
                                        <th style="width: 14%;">Rate Type
                                        </th>
                                        <th style="width: 8%;" class="text-right">Rate
                                        </th>
                                        <th style="width: 8%;" class="text-right">Floor Value ($)
                                        </th>
                                        <th style="width: 8%;" class="text-right">Ceiling Value ($)
                                        </th>
                                        <th style="width: 8%">Effective
                                        </th>
                                        <th style="width: 20%">Charge Code
                                        </th>
                                        <th style="width: 8%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstLTLGuaranteedCharges" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td rowspan="2" class="top">
                                                    <eShip:CustomHiddenField ID="hidLTLGuaranteedChargeIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <%# Eval("Description") %>
                                                </td>
                                                <td class="top">
                                                    <%# Eval("Time") %>
                                                </td>
                                                <td class="top">
                                                    <%# Eval("RateType").FormattedString() %>
                                                </td>
                                                <td class="text-right top">
                                                    <%# Eval("Rate", "{0:n2}") %>
                                                </td>
                                                <td class="text-right top">
                                                    <%# Eval("FloorValue", "{0:n2}") %>
                                                </td>
                                                <td class="text-right top">
                                                    <%# Eval("CeilingValue", "{0:n2}") %>
                                                </td>
                                                <td class="top">
                                                    <%# Eval("EffectiveDate").FormattedShortDate() %>
                                                </td>
                                                <td class="top">
                                                    <%# new ChargeCode(Eval("ChargeCodeId").ToLong()).Description %>
                                                </td>
                                                <td class="text-center top">
                                                    <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png" OnClientClick="$(window).scrollTop(0);"
                                                        CausesValidation="false" OnClick="OnEditLTLGuaranteedChargeClicked" Enabled='<%# Access.Modify %>' />
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        CausesValidation="false" OnClick="OnDeleteLTLGuaranteedChargeClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                            <tr class="hidden">
                                                <td></td>
                                            </tr>
                                            <tr class="bottom_shadow">
                                                <td colspan="8" class="forceLeftBorder pt0">
                                                    <div class="row">
                                                        <div class="fieldgroup">
                                                            <label class="blue">Critical Notes:</label>
                                                            <%# Eval("CriticalNotes") %>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                                <asp:Panel runat="server" ID="pnlEditLTLGuaranteedCharge" Visible="false" CssClass="popup popupControlOverW500" DefaultButton="btnEditLTLGuaranteedChargeDone">
                                    <div class="popheader mb10">
                                        <h4>Add/Modify LTL GuaranteedCharge</h4>
                                        <asp:ImageButton ID="ibtnCloseEditLTLGuaranteedCharge" ImageUrl="~/images/icons2/close.png" CssClass="close" CausesValidation="false" OnClick="OnCloseEditLTLGuaranteedChargeClicked" runat="server" />
                                    </div>
                                    <table class="poptable">
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Description:</label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox runat="server" ID="txtGuaranteedChargeDescription" CssClass="w200" MaxLength="50" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Time:</label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox runat="server" ID="txtGuaranteedChargeTime" CssClass="w55" MaxLength="5" placeholder="99:99" Type="Time" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Rate Type:</label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlGuarateedChargeRateType" DataTextField="Value" DataValueField="Key" CssClass="w200" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Rate:</label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox ID="txtGuaranteedChargeRate" CssClass="w200" MaxLength="19" runat="server" Type="FloatingPointNumbers" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Rate Floor Value ($):</label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox ID="txtGuaranteedChargeFloorValue" CssClass="w200" MaxLength="19" runat="server" Type="FloatingPointNumbers" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Rate Ceiling Value ($):</label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox ID="txtGuaranteedChargeCeilingValue" CssClass="w200" MaxLength="19" runat="server" Type="FloatingPointNumbers" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Effective Date:</label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox runat="server" ID="txtGuaranteedChargeEffectiveDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Charge Code:</label>
                                            </td>
                                            <td>
                                                <eShip:CachedObjectDropDownList Type="ChargeCodes" ID="ddlGuarateedChargeChargeCode" runat="server" CssClass="w200" 
                                                    EnableChooseOne="True" DefaultValue="0" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right top">
                                                <label class="upper">Critical Notes:</label>
                                                <br />
                                                <label class="lhInherit">
                                                    <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleGuaranteedChargeCriticalNotes" TargetControlId="txtGuaranteedChargeCriticalNotes" MaxLength="50" />
                                                </label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox runat="server" ID="txtGuaranteedChargeCriticalNotes" CssClass="w200" MaxLength="50" TextMode="MultiLine" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <asp:Button ID="btnEditLTLGuaranteedChargeDone" Text="Done" OnClick="OnEditLTLGuaranteedChargeDoneClicked" runat="server" CausesValidation="False" />
                                                <asp:Button ID="btnEditLTLGuaranteedChargeCancel" Text="Cancel" OnClick="OnCloseEditLTLGuaranteedChargeClicked" runat="server" CausesValidation="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>

                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabLTLAdditionalCharges" HeaderText="Add'l Chg.">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlAdditionalCharges" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel ID="pnlLTLAdditionalCharges" runat="server" DefaultButton="btnAddLTlAddiontalCharge">
                                <div class="rowgroup mb10">
                                    <div class="row">
                                        <div class="fieldgroup right">
                                            <asp:Button runat="server" ID="btnAddLTlAddiontalCharge" Text="Add LTL Additional Charge"
                                                CssClass="mr10" CausesValidation="false" OnClick="OnAddLTLAdditionalChargeClicked" />
                                            <asp:Button runat="server" ID="btnClearLTLAdditionalCharges" Text="Clear LTL Additional Charges"
                                                CausesValidation="false" OnClick="OnClearLTLAdditionalChargesClicked" />
                                        </div>
                                    </div>
                                </div>

                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheltlAdditionalChargesTable" TableId="ltlAdditionalChargesTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table class="stripe" id="ltlAdditionalChargesTable">
                                    <tr>
                                        <th style="width: 5%">Index</th>
                                        <th style="width: 38%;">Name
                                        </th>
                                        <th style="width: 37%;">Charge Code
                                        </th>
                                        <th style="width: 10%">Effective
                                        </th>
                                        <th style="width: 10%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstLTLAdditionalCharges" runat="server" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnLTLAdditionalChargesItemDataBound" OnDataBound="OnLTLAdditionChargesDataBound">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td rowspan="2" class="top">
                                                    <%# Container.DataItemIndex + 1 %>.
                                                </td>
                                                <td class="bottom_shadow">
                                                    <eShip:CustomHiddenField ID="hidLTLAdditionalChargeIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <%# Eval("Name") %>
                                                </td>
                                                <td class="bottom_shadow">
                                                    <%# new ChargeCode(Eval("ChargeCodeId").ToLong()).Description%>
                                                </td>
                                                <td class="bottom_shadow">
                                                    <%# Eval("EffectiveDate").FormattedShortDate() %>
                                                </td>
                                                <td class="text-center top" rowspan="2">
                                                    <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png" OnClientClick="$(window).scrollTop(0);"
                                                        CausesValidation="false" OnClick="OnEditLTLAdditionalChargeClicked" Enabled='<%# Access.Modify %>' />
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        CausesValidation="false" OnClick="OnDeleteLTLAdditionalChargeClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                            <tr class="hidden">
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr class="bottom_shadow">
                                                <td colspan="3">
                                                    <asp:Panel runat="server" ID="pnlAdditionalChargeIndices">
                                                        <div class="rowgroup mb10">
                                                            <div class="row">
                                                                <div class="fieldgroup left">
                                                                    <eShip:PaginationExtender runat="server" ID="peLstLTLAdditionalChargeIndices" TargetControlId="lstLTLAdditionalChargeIndices" PageSize="10" UseParentDataStore="True"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <table class="stripe">
                                                            <tr>
                                                                <td colspan="20" class="upper text-center blue">
                                                                    <h6>Indices</h6>
                                                                </td>
                                                            </tr>
                                                            <tr class="hidden">
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                            <tr class="upper blue">
                                                                <td style="width: 19%;">Region
                                                                </td>
                                                                <td style="width: 5%;" class="text-center">Use Postal Code
                                                                </td>
                                                                <td style="width: 11%;">Postal Code
                                                                </td>
                                                                <td style="width: 10%;" class="text-center">Apply On Origin
                                                                </td>
                                                                <td style="width: 10%;" class="text-center">Apply On Destination
                                                                </td>
                                                                <td style="width: 15%;">Rate Type
                                                                </td>
                                                                <td style="width: 10%;" class="text-right">Rate ($)
                                                                </td>
                                                                <td style="width: 10%;" class="text-right">Charge Floor ($)
                                                                </td>
                                                                <td style="width: 10%;" class="text-right">Charge Ceiling ($)
                                                                </td>
                                                            </tr>
                                                            <asp:ListView ID="lstLTLAdditionalChargeIndices" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                                                <LayoutTemplate>
                                                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                                                </LayoutTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <%# ActiveUser.HasAccessTo(ViewCode.Region)  && !Eval("UsePostalCode").ToBoolean() && Eval("RegionId").ToLong() != default(long)
                                                                                    ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Region Record'>{3}</a>", 
                                                                                        ResolveUrl(RegionView.PageAddress),
                                                                                        WebApplicationConstants.TransferNumber, 
                                                                                        Eval("RegionId").GetString().UrlTextEncrypt(),
                                                                                        RegionCollection.GetValueText(Eval("RegionId").GetString()))  
                                                                                    : RegionCollection.GetValueText(Eval("RegionId").GetString()) %>
                                                                        </td>
                                                                        <td class="text-center">
                                                                            <%# Eval("UsePostalCode").ToBoolean() ? WebApplicationConstants.HtmlCheck : string.Empty %>
                                                                        </td>
                                                                        <td>
                                                                            <%# !Eval("UsePostalCode").ToBoolean() ? string.Empty : Eval("PostalCode") + " <i>(" + new Country(Eval("CountryId").ToLong()).Name + ")</i>" %>
                                                                        </td>
                                                                        <td class="text-center">
                                                                            <%# Eval("ApplyOnOrigin").ToBoolean() ? WebApplicationConstants.HtmlCheck : string.Empty %>
                                                                        </td>
                                                                        <td class="text-center">
                                                                            <%# Eval("ApplyOnDestination").ToBoolean() ? WebApplicationConstants.HtmlCheck : string.Empty %>
                                                                                                                                        
                                                                        </td>
                                                                        <td>
                                                                            <%# Eval("RateType").FormattedString() %>
                                                                        </td>
                                                                        <td class="text-right">
                                                                            <%# Eval("Rate", "{0:n2}") %>
                                                                        </td>
                                                                        <td class="text-right">
                                                                            <%# Eval("ChargeFloor", "{0:n2}") %>
                                                                        </td>
                                                                        <td class="text-right">
                                                                            <%# Eval("ChargeCeiling", "{0:n2}") %>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:ListView>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>

                                <asp:Panel runat="server" ID="pnlEditLTLAdditionalCharge" Visible="false" CssClass="popup" DefaultButton="btnEditLTLAdditionalChargeDone">
                                    <div class="popheader mb10">
                                        <h4>Add/Modify LTL Additional Charge</h4>
                                        <asp:ImageButton ID="ImageButton5" ImageUrl="~/images/icons2/close.png" CssClass="close"
                                            CausesValidation="false" OnClick="OnEditLTLAdditionalChargeCancelClicked" runat="server" />
                                    </div>
                                    <div class="rowgroup pl10 mb5">
                                        <div class="row">
                                            <table class="poptable wAuto">
                                                <tr>
                                                    <td>
                                                        <label class="upper">Name:</label>
                                                    </td>
                                                    <td>
                                                        <eShip:CustomTextBox runat="server" ID="txtLTLAdditionalChargeName" CssClass="w200" MaxLength="50" />
                                                    </td>
                                                    <td class="text-right">
                                                        <label class="upper">Charge Code:</label>
                                                    </td>
                                                    <td>
                                                        <eShip:CachedObjectDropDownList Type="ChargeCodes" ID="ddlLTLAdditionalChargeChargeCode" runat="server" CssClass="w200" 
                                                            EnableChooseOne="True" DefaultValue="0" />
                                                    </td>
                                                    <td class="text-right">
                                                        <label class="upper">Effective Date:</label>
                                                    </td>
                                                    <td>
                                                        <eShip:CustomTextBox runat="server" ID="txtLTLAdditionalChargeEffectiveDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                                                    </td>
                                                    <td class="text-left pl10">
                                                        <asp:Button runat="server" ID="btnEditLTLAdditionalChargeDone" Text="DONE" OnClick="OnEditLTLAdditionalChargeDoneClicked" CausesValidation="False" />
                                                        <asp:Button runat="server" ID="btnEditLTlAdditionalChargeCancel" Text="CANCEL" OnClick="OnEditLTLAdditionalChargeCancelClicked" CausesValidation="false" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                    <table class="stripe" id="ltlAdditionalChargeIndicesEditTable">
                                        <tr class="header">
                                            <td colspan="4">
                                                <eShip:PaginationExtender runat="server" ID="pelstEditLTLAdditionalChargeIndices" TargetControlId="lstEditLTLAdditionalChargeIndices" PageSize="10" OnPageChanging="OnEditLTLAdditionalChargeIndexChange" />
                                            </td>
                                            <td colspan="3" class="upper pl100">
                                                <h6>Indices</h6>
                                            </td>
                                            <td colspan="4" class="text-right pr10">
                                                <asp:Button runat="server" ID="btnAddLTLAddtionalChargeIndex" Text="ADD INDEX" OnClick="OnAddLTLAddtionalChargeIndexClicked" CausesValidation="false" />
                                                <asp:Button runat="server" ID="btnResortEditLTLAdditionalChargeIndex" Text="SORT INDICES" OnClick="OnResortEditLTLAddtionalChargeIndexClicked" CausesValidation="false" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="width: 16%;">Region
                                            </th>
                                            <th style="width: 5%;" class="text-center">Use Postal Code
                                            </th>
                                            <th style="width: 9%;">Postal Code
                                            </th>
                                            <th style="width: 10%;">Country
                                            </th>
                                            <th style="width: 5%;" class="text-center">Apply On Origin
                                            </th>
                                            <th style="width: 5%;" class="text-center">Apply On Destination
                                            </th>
                                            <th style="width: 15%;">Rate Type
                                            </th>
                                            <th style="width: 10%;" class="text-center">Rate ($)
                                            </th>
                                            <th style="width: 10%;" class="text-center">Charge Floor ($)
                                            </th>
                                            <th style="width: 10%;" class="text-center">Charge Ceiling ($)
                                            </th>
                                            <th style="width: 5%;">Action
                                            </th>
                                        </tr>
                                        <asp:ListView runat="server" ID="lstEditLTLAdditionalChargeIndices" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnEditLTLAdditionalChargeIndexItemDataBound">
                                            <LayoutTemplate>
                                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                            </LayoutTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <eShip:CustomHiddenField runat="server" ID="hidAdditionalChargeIndexItemIndex" Value='<%# Container.DataItemIndex + (pelstEditLTLAdditionalChargeIndices.PageSize*(pelstEditLTLAdditionalChargeIndices.CurrentPage-1)) %>' />
                                                        <eShip:CustomHiddenField ID="hidAdditionalChargeIndexId" runat="server" Value='<%# Eval("Id") %>' />
                                                        <asp:DropDownList runat="server" DataTextField="Text" DataValueField="Value" ID="ddlAdditionalChargeRegion" CssClass="w100" />
                                                    </td>
                                                    <td class="text-center">
                                                        <eShip:AltUniformCheckBox runat="server" ID="chkAdditionalChargeIndexUsePostalCode" Checked='<%# Eval("UsePostalCode") %>' />
                                                    </td>
                                                    <td>
                                                        <eShip:CustomTextBox ID="txtAdditionalChargeIndexPostalCode" Text='<%# Eval("PostalCode") %>' runat="server" CssClass="w65" MaxLength="10" />
                                                    </td>
                                                    <td>
                                                        <eShip:CachedObjectDropDownList Type="Countries" ID="ddlAdditionalChargeCountries" runat="server" CssClass="w80" 
                                                            EnableNotApplicable="True" DefaultValue="0" SelectedValue='<%# Eval("CountryId") %>' />
                                                    </td>
                                                    <td class="text-center">
                                                        <eShip:AltUniformCheckBox runat="server" ID="chkAdditionalChargeIndexApplyOnOrigin" Checked='<%# Eval("ApplyOnOrigin") %>' />
                                                    </td>
                                                    <td class="text-center">
                                                        <eShip:AltUniformCheckBox runat="server" ID="chkAdditionalChargeIndexApplyOnDestination" Checked='<%# Eval("ApplyOnDestination") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlAdditionalChargeIndexRateType" DataTextField="Value" DataValueField="Key" CssClass="w65" runat="server" />
                                                    </td>
                                                    <td class="text-center">
                                                        <eShip:CustomTextBox ID="txtAdditionalChargeIndexRate" CssClass="w70" MaxLength="19" runat="server" Type="FloatingPointNumbers"
                                                            Text='<%# Eval("Rate", "{0:f4}") %>' />
                                                    </td>
                                                    <td class="text-center">
                                                        <eShip:CustomTextBox ID="txtAdditionalChargeIndexChargeFloor" CssClass="w70" MaxLength="19" Type="FloatingPointNumbers"
                                                            runat="server" Text='<%# Eval("ChargeFloor", "{0:f4}") %>' />
                                                    </td>
                                                    <td class="text-center">
                                                        <eShip:CustomTextBox ID="txtAdditionalChargeIndexChargeCeiling" CssClass="w70" MaxLength="19" Type="FloatingPointNumbers"
                                                            runat="server" Text='<%# Eval("ChargeCeiling", "{0:f4}") %>' />
                                                    </td>
                                                    <td class="text-center">
                                                        <asp:ImageButton ID="ibtnContactDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                            CausesValidation="false" OnClick="OnDeleteAdditionalChargeIndexClicked" />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </table>
                                </asp:Panel>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabLTLCubicFootCapacityRules" HeaderText="C.F.C. Rules">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlCubicFoorCapacityRules" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel ID="pnlLTLCubicFootCapacityRules" runat="server" DefaultButton="btnAddCFCRule">

                                <div class="rowgroup mb10">
                                    <div class="row">
                                        <div class="fieldgroup mr20">
                                            <label class="wlabel blue">Cubic Capacity Penalty Width</label>
                                            <eShip:CustomTextBox runat="server" ID="txtCubicCapacityPenaltyWidth" CssClass="w200" MaxLength="9" Type="FloatingPointNumbers" />
                                        </div>
                                        <div class="fieldgroup">
                                            <label class="wlabel blue">Cubic Capacity Penalty Height</label>
                                            <eShip:CustomTextBox runat="server" ID="txtCubicCapacityPenaltyHeight" CssClass="w200" MaxLength="9" Type="FloatingPointNumbers" />
                                        </div>
                                    </div>
                                    <h5>Rules</h5>
                                    <div class="row mt10">
                                        <p class="left note">
                                            * lb = Pounds, CF = Cubic Foot, FAK = Freight All Kinds, Pkg = Package 
                                        </p>
                                        <div class="fieldgroup right">
                                            <asp:Button runat="server" ID="btnAddCFCRule" Text="Add LTL C.F. Capacity Rule" CssClass="mr10" CausesValidation="false" OnClick="OnAddCFCRuleClicked" />
                                            <asp:Button runat="server" ID="btnClearCFCRules" Text="Clear LTL C.F. Capacity Rules" CausesValidation="false" OnClick="OnClearCFCRulesClicked" />
                                        </div>
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheLtlCFCRulesTable" TableId="ltlCFCRulesTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table class="stripe" id="ltlCFCRulesTable">
                                    <tr>
                                        <th style="width: 14%;" class="text-right">Lower Bound
                                    <abbr title="Cubic Foot">CF</abbr>
                                        </th>
                                        <th style="width: 14%;" class="text-right">Upper Bound
                                    <abbr title="Cubic Foot">CF</abbr>
                                        </th>
                                        <th style="width: 8%;" class="text-center">Apply Discount
                                        </th>
                                        <th style="width: 10%;" class="text-center">Applied
                                    <abbr title="Freight Class">FC</abbr>
                                        </th>
                                        <th style="width: 5%;" class="text-center">Apply FAK
                                        </th>
                                        <th style="width: 10%;" class="text-right">Average
                                    <abbr title="Pounds">lb</abbr>/<abbr title="Cubic Foot">CF</abbr>
                                            Limit
                                        </th>
                                        <th style="width: 9%;" class="text-center">Average
                                    <abbr title="Pounds">lb</abbr>/<abbr title="Cubic Foot">CF</abbr>
                                            Limit Applies
                                        </th>
                                        <th style="width: 10%;" class="text-right">Penalty
                                    <abbr title="Pounds">lb</abbr>/<abbr title="Cubic Foot">CF</abbr>
                                        </th>
                                        <th style="width: 10%;" class="text-center">Effective
                                        </th>
                                        <th style="width: 10%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstCFCRules" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td class="text-right">
                                                    <eShip:CustomHiddenField ID="hidLTLCFCRuleIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <%# Eval("LowerBound", "{0:n4}") %>
                                                </td>
                                                <td class="text-right">
                                                    <%# Eval("UpperBound", "{0:n4}") %>
                                                </td>
                                                <td class="text-center">
                                                    <%# Eval("ApplyDiscount").ToBoolean() ? WebApplicationConstants.HtmlCheck : string.Empty %>
                                                </td>
                                                <td class="text-center">
                                                    <%# Eval("AppliedFreightClass").ToString() == 0.ToString() ? WebApplicationConstants.NotApplicableShort : Eval("AppliedFreightClass") %>
                                                </td>
                                                <td class="text-center">
                                                    <%# Eval("ApplyFAK").ToBoolean() ? WebApplicationConstants.HtmlCheck : string.Empty %>
                                                </td>
                                                <td class="text-right">
                                                    <%# Eval("AveragePoundPerCubicFootLimit", "{0:n4}") %>
                                                </td>
                                                <td class="text-center">
                                                    <%# Eval("AveragePoundPerCubicFootApplies").ToBoolean() ? WebApplicationConstants.HtmlCheck : string.Empty %>
                                                </td>
                                                <td class="text-right">
                                                    <%# Eval("PenaltyPoundPerCubicFoot", "{0:n4}") %>
                                                </td>
                                                <td class="text-center">
                                                    <%# Eval("EffectiveDate").FormattedShortDate() %>
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png" OnClientClick="$(window).scrollTop(0);"
                                                        CausesValidation="false" OnClick="OnEditLtlcfcClicked" Enabled='<%# Access.Modify %>' />
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        CausesValidation="false" OnClick="OnDeleteLtlcfcClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                                <script type="text/javascript">
                                    function UncheckFAKApplies(control) {
                                        if ($(control).val() == 0) {
                                            jsHelper.UnCheckBox('<%= chkLTLCFCFAKIsApplicable.ClientID %>');
                                            SetAltUniformCheckBoxClickedStatus(document.getElementById('<%= chkLTLCFCFAKIsApplicable.ClientID %>'));
                                        }
                                    }
                                </script>
                                <asp:Panel runat="server" ID="pnlEditLTLCFC" Visible="false" CssClass="popup popupControlOverW750" DefaultButton="btnEditLTLCFCDone">
                                    <div class="popheader mb10">
                                        <h4>Add/Modify LTL Cubic Foot Capacity Rule</h4>
                                        <asp:ImageButton ID="ImageButton3" ImageUrl="~/images/icons2/close.png" CssClass="close"
                                            CausesValidation="false" OnClick="OnCloseEditLtlcfcClicked" runat="server" />
                                    </div>
                                    <table class="poptable">
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Lower Bound (CF):</label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox ID="txtLTLCFCLowerBound" CssClass="w200 mr10" MaxLength="9" runat="server" Type="FloatingPointNumbers" />
                                                <eShip:AltUniformCheckBox runat="server" ID="chkLTLCFCDiscountIsApplicable" />
                                                <label>Discount Applies</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Upper Bound (cf):</label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox ID="txtLTLCFCUpperBound" CssClass="w200" MaxLength="9" runat="server" Type="FloatingPointNumbers" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Applied Freight Class:</label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlLTLCFCAppliedFreightClass" CssClass="w200 mr10" runat="server" DataTextField="Text" DataValueField="Value" onchange="javascript:UncheckFAKApplies(this);" />
                                                <eShip:AltUniformCheckBox runat="server" ID="chkLTLCFCFAKIsApplicable" />
                                                <label>FAK Applies</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Avg. lb/Cubic Foot Limit:</label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox ID="txtLTLCFCAvgLbPerCFLimit" CssClass="w200 mr10" MaxLength="9" runat="server" Type="FloatingPointNumbers" />
                                                <eShip:AltUniformCheckBox runat="server" ID="chkLTLCFCAVgLbPerCFLimitIsApplicable" />
                                                <label>Apply in Calculations</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Penalty lb/Cubic Foot:</label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox ID="txtLTLCFCPenaltyLbPerCF" CssClass="w200" MaxLength="9" runat="server" Type="FloatingPointNumbers" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Effective Date:</label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox runat="server" ID="txtLTLCFCEffectiveDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <asp:Button ID="btnEditLTLCFCDone" Text="Done" OnClick="OnEditLtlcfcDoneClicked" CausesValidation="false" runat="server" />
                                                <asp:Button ID="btnEditLTLCFCCancel" Text="Cancel" OnClick="OnCloseEditLtlcfcClicked" runat="server" CausesValidation="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabLTLOverlengthChargeRules" HeaderText="Over. Rules">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlOverlengthRules" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel ID="pnlLTLOverlengthRules" runat="server" DefaultButton="btnAddLTLOverLengthRule">
                                <div class="row mb10">
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddLTLOverLengthRule" Text="Add LTL Overlength Rule" CssClass="mr10" CausesValidation="false" OnClick="OnAddLTLOverlengthRuleClicked" />
                                        <asp:Button runat="server" ID="btnClearLTLOverLengthRules" Text="Clear LTL Overlength Rules" CausesValidation="false" OnClick="OnClearLTLOverlengthRulesClicked" />
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheLtlOverlengthRulesTable" TableId="ltlOverlengthRulesTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table class="stripe" id="ltlOverlengthRulesTable">
                                    <tr>
                                        <th style="width: 16%;" class="text-right">Lower Bound (<abbr title="Inches">in</abbr>)
                                        </th>
                                        <th style="width: 16%;" class="text-right">Upper Bound (<abbr title="Inches">in</abbr>)
                                        </th>
                                        <th style="width: 15%;" class="text-right">Charge ($)
                                        </th>
                                        <th style="width: 35%;">Charge Code
                                        </th>
                                        <th style="width: 10%">Effective
                                        </th>
                                        <th style="width: 8%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstLTLOverlengthRules" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td class="text-right">
                                                    <eShip:CustomHiddenField ID="hidLTLOverlengthRuleIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <%# Eval("LowerLengthBound", "{0:n4}") %>
                                                </td>
                                                <td class="text-right">
                                                    <%# Eval("UpperLengthBound", "{0:n4}") %>
                                                </td>
                                                <td class="text-right">
                                                    <%# Eval("Charge", "{0:n2}") %>
                                                </td>
                                                <td>
                                                    <%# new ChargeCode(Eval("ChargeCodeId").ToLong()).Description %>
                                                </td>
                                                <td>
                                                    <%# Eval("EffectiveDate").FormattedShortDate() %>
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png" OnClientClick="$(window).scrollTop(0);"
                                                        CausesValidation="false" OnClick="OnEditLTLOverlengthRuleClicked" Enabled='<%# Access.Modify %>' />
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        CausesValidation="false" OnClick="OnDeleteLTLOverlengthRuleClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>

                                <asp:Panel runat="server" ID="pnlEditLTLOverlengthRule" Visible="false" CssClass="popup popupControlOverW500" DefaultButton="btnEditLTLOverlengthRuleDone">
                                    <div class="popheader mb10">
                                        <h4>Add/Modify LTL Overlength Rule</h4>
                                        <asp:ImageButton ImageUrl="~/images/icons2/close.png" CssClass="close" CausesValidation="false" OnClick="OnCloseEditLTLOverlengthRuleClicked" runat="server" />
                                    </div>
                                    <table class="poptable">
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Lower Bound (in):</label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox ID="txtLTLOverlengthRuleLowerBound" CssClass="w200" MaxLength="9" runat="server" Type="FloatingPointNumbers" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Upper Bound (in):</label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox ID="txtLTLOverlengthRuleUpperBound" CssClass="w200" MaxLength="9" runat="server" Type="FloatingPointNumbers" />

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Charge ($):</label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox ID="txtLTLOverlengthRuleCharge" CssClass="w200" MaxLength="19" runat="server" Type="FloatingPointNumbers" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Charge Code:</label>
                                            </td>
                                            <td>
                                                <eShip:CachedObjectDropDownList Type="ChargeCodes" ID="ddlLTLOverlengthRuleChargeCode" runat="server" CssClass="w200" 
                                                    EnableChooseOne="True" DefaultValue="0" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">
                                                <label class="upper">Effective Date:</label>
                                            </td>
                                            <td>
                                                <eShip:CustomTextBox runat="server" ID="txtLTLOverlengthRuleEffectiveDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <asp:Button ID="btnEditLTLOverlengthRuleDone" Text="Done" OnClick="OnEditLTLOverlengthRuleDoneClicked" CausesValidation="False" runat="server" />
                                                <asp:Button ID="btnEditLTLOverlengthRuleCancel" Text="Cancel" OnClick="OnCloseEditLTLOverlengthRuleClicked" runat="server" CausesValidation="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabAuditLog" HeaderText="Audit Log">
                <ContentTemplate>
                    <eShip:AuditLogControl runat="server" ID="auditLogs" />
                </ContentTemplate>
            </ajax:TabPanel>
        </ajax:TabContainer>
        <asp:UpdatePanel runat="server" UpdateMode="Always">
            <ContentTemplate>
                <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
                <div class="dimBackground" id="jqueryDimBackground" style="display: none;"></div>
                <eShip:CustomHiddenField runat="server" ID="hidFlag" />
                <eShip:CustomHiddenField runat="server" ID="hidEditingIndex" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
