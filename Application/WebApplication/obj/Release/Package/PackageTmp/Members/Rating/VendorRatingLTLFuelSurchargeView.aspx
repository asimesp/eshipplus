﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="VendorRatingLTLFuelSurchargeView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Rating.VendorRatingLTLFuelSurchargeView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Rating" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>
<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="false"
        ShowEdit="false" ShowDelete="false" ShowSave="true" ShowFind="false" ShowExport="false"
        ShowImport="false" OnSave="OnSaveClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                CssClass="pageHeaderIcon" />
            <h3>Vendor Rating LTL Fuel Surcharge Mass Update
            <small class="ml10">
                <asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>
        <span class="clearfix"></span>
        <hr class="dark mb10" />

        <asp:Panel runat="server" DefaultButton="btnSearch">
            <div class="rowgroup">
                <div class="row">
                    <div class="fieldgroup">
                        <label class="upper blue">Surcharge Effective Date:</label>
                        <eShip:CustomTextBox runat="server" ID="txtSurchargeEffectiveDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                    </div>
                    <div class="fieldgroup">
                        <asp:Button ID="btnSearch" runat="server" Text="SEARCH" OnClick="OnSearchClicked" CausesValidation="false" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        
        <hr class="dark mb10" />
        
        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheLtlFuelUpdateTable" TableId="ltlFuelUpdateTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
        <table class="stripe" id="ltlFuelUpdateTable">
            <tr class="header">
                <th style="width: 8%; <%= HasAccessToModifyVendorRatingFuelMassUpdateLTL ? string.Empty : "display: none;"%>">
                    <eShip:AltUniformCheckBox ID="chkSelectAllRecords" runat="server" OnClientSideClicked="javascript:SetSelectedCheckBoxesStatus(this, 'ltlFuelUpdateTable');" />
                </th>
                <th style="width: 40%">Vendor Rating Name
                </th>
                <th style="width: 12%">Fuel Index Region
                </th>
                <th style="width: 8%;" class="text-right">Current Surcharge (%)
                </th>
                <th style="width: 8%;" class="text-right">Surcharge Floor (%)
                </th>
                <th style="width: 8%;" class="text-right">Surcharge Ceiling (%)
                </th>
                <th style="width: 8%">Updates On
                </th>
                <th style="width: 8%">Update To Surcharge (%)
                </th>
            </tr>
            <asp:ListView ID="lstSurcharges" runat="server" ItemPlaceholderID="itemPlaceHolder">
                <LayoutTemplate>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </LayoutTemplate>
                <ItemTemplate>
                    <tr>
                        <td style='<%# HasAccessToModifyVendorRatingFuelMassUpdateLTL ? string.Empty : "display:none;"%>'>
                            <eShip:AltUniformCheckBox runat="server" ID="chkSelected" Checked='<%# Eval("FuelUpdatesOn").GetString().ToEnum<DayOfWeek>() == txtSurchargeEffectiveDate.Text.ToDateTime().DayOfWeek %>' 
                                OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'ltlFuelUpdateTable', 'chkSelectAllRecords');"/>
                        </td>
                        <td>
                            <eShip:CustomHiddenField ID="hidVendorRatingId" runat="server" Value='<%# Eval("Id") %>' />
                            <%# ActiveUser.HasAccessTo(ViewCode.VendorRating) 
                                                ? string.Format("{0} <a href='{1}?{2}={3}' class='blue' target='_blank' title='Go To Vendor Rating Record'><img src={4} width='16' class='middle'/></a></td>", 
                                                    Eval("Name"),
                                                    ResolveUrl(VendorRatingView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    Eval("Id").GetString().UrlTextEncrypt(),
                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))  
                                                : Eval("Name") %>
                        </td>
                        <td>
                            <%# Eval("FuelIndexRegion").FormattedString() %>
                        </td>
                        <td class="text-right">
                            <%# Eval("CurrentLTLFuelMarkup").ToDecimal().ToString("f4") %>
                        </td>
                        <td class="text-right">
                            <%# Eval("FuelMarkupFloor").ToDecimal().ToString("f4")%>
                        </td>
                        <td class="text-right">
                            <%# Eval("FuelMarkupCeiling").ToDecimal().ToString("f4")%>
                        </td>
                        <td>
                            <%# Eval("FuelUpdatesOn").FormattedString() %>
                        </td>
                        <td>
                            <eShip:CustomTextBox ID="txtNewSurcharge" runat="server" Text='<%# Eval("NewSurcharge").ToDecimal() == 0 ?  Eval("FuelMarkupFloor").ToDecimal().ToString("f4") :  Eval("NewSurcharge").ToDecimal().ToString("f4")%>'
                                CssClass='<%# HasAccessToModifyVendorRatingFuelMassUpdateLTL ? "w90" : "w90 disabled" %>' ReadOnly='<%# !HasAccessToModifyVendorRatingFuelMassUpdateLTL %>'/>
                            <asp:RangeValidator runat="server" ID="rvNewSurcharge" ControlToValidate="txtNewSurcharge"
                                MinimumValue='<%# Eval("FuelMarkupFloor").ToDecimal().ToString("f4")%>' MaximumValue='<%# Eval("FuelMarkupCeiling").ToDecimal().ToString("f4")%>'
                                Type="Double" ErrorMessage='<%# Eval("Name") +  " surcharge value must be between " + Eval("FuelMarkupFloor").ToDecimal().ToString("f4") + " and "  + Eval("FuelMarkupCeiling").ToDecimal().ToString("f4") %>'
                                Text="*" Display="Dynamic" />
                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtNewSurcharge"
                                FilterType="Custom, Numbers" ValidChars="." />
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </table>
    </div>
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information"/>
</asp:Content>
