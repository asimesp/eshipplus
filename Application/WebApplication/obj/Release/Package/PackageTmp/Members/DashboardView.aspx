﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="DashboardView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.DashboardView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>

<%@ Register Src="~/Members/Controls/DashboardItemControl.ascx" TagName="DashboardItem" TagPrefix="uc1" %>

<asp:Content ID="ToolBar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="false" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">

    <!-- script to fix the column width and height when less dynamic content is added -->
    <script type="text/javascript">
        $(document).ready(function () {
            var n = $(".column").length;
            if (n < 4) {
                $(".column").css("width", 366);
            }
            else {
                if (n < 5) {
                    $(".column").css("width", 275);
                }
                else {
                    $(".column").css("width", 220);
                }
            }

            $('#wrapper').each(function () {

                var highestBox = 0;
                $('.column', this).each(function () {

                    if ($(this).height() > highestBox)
                        highestBox = $(this).height();
                });

                $('.column', this).height(highestBox);

            });

        });
    </script>

    <div id="wrapper" class="clearfix">
        <asp:ListView runat="server" ID="lstOperationsDashboard" ItemPlaceholderID="itemPlaceHolder">
            <LayoutTemplate>
                <div class="column">
                    <div id="operationsDashboardMenu" class="listgrey text-left pl20">
                        <h3 class="menu_operations mr10">OPERATIONS</h3>
                        <ul>
                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                        </ul>
                    </div>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <uc1:DashboardItem ID="items" runat="server" IconName='<%# Eval("DisplayName") %>' OpenInNewWindow='<%# Eval("OpenInNewWindow") %>'
                    ViewCode='<%# Eval("ViewCode") %>' IconUrl='<%# Eval("IconUrl") %>' />
            </ItemTemplate>
        </asp:ListView>
        <asp:ListView runat="server" ID="lstFinanceDashboard" ItemPlaceholderID="itemPlaceHolder">
            <LayoutTemplate>
                <div class="column">
                    <div id="financeDashboardMenu" class="listgrey text-left pl20">
                        <h3 class="menu_finance mr10">FINANCE</h3>
                        <ul>
                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                        </ul>
                    </div>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <uc1:DashboardItem ID="items" runat="server" IconName='<%# Eval("DisplayName") %>' OpenInNewWindow='<%# Eval("OpenInNewWindow") %>'
                    ViewCode='<%# Eval("ViewCode") %>' IconUrl='<%# Eval("IconUrl") %>' />
            </ItemTemplate>
        </asp:ListView>
        <asp:ListView runat="server" ID="lstRegistryDashboard" ItemPlaceholderID="itemPlaceHolder">
            <LayoutTemplate>
                <div class="column">
                    <div id="registryDashboardMenu" class="listgrey text-left pl20">
                        <h3 class="menu_registry mr10">Registry</h3>
                        <ul>
                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                        </ul>
                    </div>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <uc1:DashboardItem ID="items" runat="server" IconName='<%# Eval("DisplayName") %>' OpenInNewWindow='<%# Eval("OpenInNewWindow") %>'
                    ViewCode='<%# Eval("ViewCode") %>' IconUrl='<%# Eval("IconUrl") %>' />
            </ItemTemplate>
        </asp:ListView>

        <asp:Panel runat="server" ID="pnlGroup2" CssClass="column">
            <asp:ListView runat="server" ID="lstRatingDashboard" ItemPlaceholderID="itemPlaceHolder">
                <LayoutTemplate>
                    <div id="ratingDashboardMenu" class="listgrey text-left pl20">
                        <h3 class="menu_rating mr10">RATING</h3>
                        <ul>
                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                        </ul>
                    </div>
                </LayoutTemplate>
                <ItemTemplate>
                    <uc1:DashboardItem ID="items" runat="server" IconName='<%# Eval("DisplayName") %>' OpenInNewWindow='<%# Eval("OpenInNewWindow") %>'
                        ViewCode='<%# Eval("ViewCode") %>' IconUrl='<%# Eval("IconUrl") %>' />
                </ItemTemplate>
            </asp:ListView>
            <asp:ListView runat="server" ID="lstUtilitiesDashboard" ItemPlaceholderID="itemPlaceHolder">
                <LayoutTemplate>
                    <div id="utilitiesDashboardMenu" class="listgrey text-left pl20">
                        <h3 class="menu_util mr10">UTILITIES</h3>
                        <ul>
                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                        </ul>
                    </div>
                </LayoutTemplate>
                <ItemTemplate>
                    <uc1:DashboardItem ID="items" runat="server" IconName='<%# Eval("DisplayName") %>' OpenInNewWindow='<%# Eval("OpenInNewWindow") %>'
                        ViewCode='<%# Eval("ViewCode") %>' IconUrl='<%# Eval("IconUrl") %>' />
                </ItemTemplate>
            </asp:ListView>
        </asp:Panel>

        <asp:Panel runat="server" ID="pnlGroup1" CssClass="column">
            <asp:ListView runat="server" ID="lstBusinessIntelligenceDashboard" ItemPlaceholderID="itemPlaceHolder">
                <LayoutTemplate>
                    <div id="businessIntelligenceDashboardMenu" class="listgrey text-left pl20">
                        <h3 class="menu_reports mr10">
                            REPORTING</h3>
                        <ul>
                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                        </ul>
                    </div>
                </LayoutTemplate>
                <ItemTemplate>
                    <uc1:DashboardItem ID="items" runat="server" IconName='<%# Eval("DisplayName") %>' OpenInNewWindow='<%# Eval("OpenInNewWindow") %>'
                        ViewCode='<%# Eval("ViewCode") %>' IconUrl='<%# Eval("IconUrl") %>' />
                </ItemTemplate>
            </asp:ListView>
            <asp:ListView runat="server" ID="lstConnectDashboard" ItemPlaceholderID="itemPlaceHolder">
                <LayoutTemplate>
                    <div id="connectDashboardMenu" class="listgrey text-left pl20">
                        <h3 class="menu_connect mr10">CONNECT</h3>
                        <ul>
                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                        </ul>
                    </div>
                </LayoutTemplate>
                <ItemTemplate>
                    <uc1:DashboardItem ID="items" runat="server" IconName='<%# Eval("DisplayName") %>' OpenInNewWindow='<%# Eval("OpenInNewWindow") %>'
                        ViewCode='<%# Eval("ViewCode") %>' IconUrl='<%# Eval("IconUrl") %>' />
                </ItemTemplate>
            </asp:ListView>
            <asp:ListView runat="server" ID="lstAdministrationDashboard" ItemPlaceholderID="itemPlaceHolder">
                <LayoutTemplate>
                    <div id="administrationDashboardMenu" class="listgrey text-left pl20">
                        <h3 class="menu_admin mr10">
                            <abbr title="Administration">ADMIN</abbr></h3>
                        <ul>
                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                        </ul>
                    </div>
                </LayoutTemplate>
                <ItemTemplate>
                    <uc1:DashboardItem ID="items" runat="server" IconName='<%# Eval("DisplayName") %>' OpenInNewWindow='<%# Eval("OpenInNewWindow") %>'
                        ViewCode='<%# Eval("ViewCode") %>' IconUrl='<%# Eval("IconUrl") %>' />
                </ItemTemplate>
            </asp:ListView>
        </asp:Panel>
    </div>
</asp:Content>
