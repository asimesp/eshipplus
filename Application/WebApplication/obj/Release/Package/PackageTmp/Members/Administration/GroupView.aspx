﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="GroupView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Administration.GroupView" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Administration" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true" ShowUnlock="False"
        ShowEdit="true" ShowDelete="true" ShowSave="true" ShowFind="true"  OnFind="OnFindClicked" OnUnlock="OnUnlockClicked"
        OnSave="OnSaveClicked" OnDelete="OnDeleteClicked" OnNew="OnNewClicked" OnEdit="OnEditClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">

        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Groups
                <eShip:RecordIdentityDisplayControl runat="server" ID="ridcRecordIdentity" TargetControlId="txtGroupName" />
            </h3>

            <div class="lockMessage">
                <asp:Literal ID="litMessage" runat="server" />
            </div>
        </div>

        <hr class="dark mb5" />

        <div class="errorMsgLit">
            <asp:Literal runat="server" ID="litErrorMessages" />
        </div>

        <eShip:GroupFinderControl ID="groupFinder" runat="server" Visible="false" EnableMultiSelection="False" OnItemSelected="OnGroupFinderItemSelected"
            OnSelectionCancel="OnGroupFinderSelectionCancelled" />
        <eShip:UserFinderControl ID="userFinder" runat="server" Visible="False" EnableMultiSelection="true"
            OpenForEditEnabled="false" OnMultiItemSelected="OnUserFinderMultiItemSelected"
            OnSelectionCancel="OnUserFinderSelectionCancelled" />
        <eShip:PermissionSelectorControl ID="permissionSelector" runat="server" Visible="false" OnItemsSelected="OnPermissionSelectorItemsSelected"
            OnSelectionCancel="OnPermissionSelectorSelectionCancelled" />
		<eShip:FindMatchingUserControl ID="matchingUser" runat="server" Visible="false"  OnClose="OnMatchingUserControlClose" OnSelectedIndex="OnMatchingUserControlSelectedIndex"  />
        <eShip:AjaxTabHeaderTextUpdater runat="server" ID="athtuTabUpdater" />

        <eShip:CustomHiddenField runat="server" ID="hidGroupId" />

        <ajax:TabContainer runat="server" CssClass="ajaxCustom" ID="tabContainerMain">
            <ajax:TabPanel runat="server" ID="tabDetails" HeaderText="Details">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlDetails">
                        <div class="rowgroup">
                            <div class="row mb20">
                                <div class="fieldgroup">
                                    <label class="wlabel blue">Group Name</label>
                                    <eShip:CustomTextBox ID="txtGroupName" runat="server" CssClass="w300" MaxLength="50" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="fieldgroup">
                                    <label class="wlabel blue">
                                        Description
                                        <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDescription" TargetControlId="txtDescription" MaxLength="200" />
                                    </label>
                                    <eShip:CustomTextBox ID="txtDescription" runat="server" TextMode="MultiLine" Rows="15" CssClass="w500 h150" />

                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabPermissions" HeaderText="Permissions">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlPermissions" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlPermissions">
                                <div class="row mb10">
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddPermission" Text="Add Permission" CssClass="mr10" OnClick="OnAddPermissionClicked" CausesValidation="False" />
                                        <asp:Button runat="server" ID="btnClearPermissions" Text="Clear Permissions" OnClick="OnClearPermissionsClicked" CausesValidation="False" />
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfhegroupPermissionTable" TableId="groupPermissionTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table id="groupPermissionTable" class="stripe">
                                    <tr>
                                        <th style="width: 25%;">Code</th>
                                        <th style="width: 30%;">Description</th>
                                        <th style="width: 10%;" class="text-center">Grant</th>
                                        <th style="width: 10%;" class="text-center">Deny</th>
                                        <th style="width: 10%;" class="text-center">Modify</th>
                                        <th style="width: 10%;" class="text-center">Delete</th>
                                        <th style="width: 5%;">Action</th>
                                    </tr>
                                    <asp:ListView ID="lstPermissions" OnItemDataBound="BindPermissionLineItem" runat="server"
                                        ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <eShip:PermissionLineItemControl ID="pcPermissionLineItem" runat="server" OnRemovePermission="OnPermissionItemRemovePermission"
                                                    EnableDelete='<%# Access.Modify %>' />
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnAddPermission" />
                        </Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabUsers" HeaderText="Users">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlUsers" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlUsers">
                                <div class="row mb10">
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddUser" Text="Add User" CssClass="mr10" OnClick="OnAddUserClicked" CausesValidation="False" />
										<asp:Button runat="server" ID="btnMatchingUser" CssClass="mr10" Text="Find User" OnClick="OnFindMatchingUserClicked" CausesValidation="False" />
                                        <asp:Button runat="server" ID="btnClearUsers" Text="Clear Users" OnClick="OnClearUsersClicked" CausesValidation="False" />
                                    </div>
									 <div class="fieldgroup left">
                                          <eShip:PaginationExtender runat="server" ID="peLstUsers" TargetControlId="lstUsers" PageSize="100" UseParentDataStore="True"   />
                                      </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfhegroupsUsersTable" TableId="groupsUsersTable" IgnoreHeaderBackgroundFill="True" />
                                <table id="groupsUsersTable" class="stripe">
                                    <tr>
                                        <th style="width: 25%">Username
                                        </th>
                                        <th style="width: 30%">Name
                                        </th>
                                        <th style="width: 35%">Email
                                        </th>
                                        <th style="width: 10%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstUsers" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField runat="server" ID="hidUserId" Value='<%# Eval("Id") %>' />
                                                    <asp:Literal runat="server" ID="litUsername" Text='<%# Eval("Username") %>'/>
                                                    <%# ActiveUser.HasAccessTo(ViewCode.User) 
                                                        ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To User Record'><img src='{3}' alt='' width='16' align='absmiddle'/></a>", 
                                                            ResolveUrl(UserView.PageAddress),
                                                            WebApplicationConstants.TransferNumber, 
                                                            Eval("Id").GetString().UrlTextEncrypt(),
                                                            ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                        : string.Empty  %>
                                                </td>
                                                <td>
                                                    <asp:Literal runat="server" ID="litFirstName" Text='<%# Eval("FirstName") %>' />
                                                    <asp:Literal runat="server" ID="litLastName" Text='<%# Eval("LastName") %>' />
                                                </td>
                                                <td>
                                                    <asp:Literal runat="server" ID="litEmail" Text='<%# Eval("Email") %>' />
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnDeleteUser" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        OnClick="OnDeleteUserClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnAddUser" />
							<asp:PostBackTrigger ControlID="btnMatchingUser" />
                        </Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabAuditLog" HeaderText="Audit Log">
                <ContentTemplate>
                    <eShip:AuditLogControl ID="auditLogs" runat="server" />
                </ContentTemplate>
            </ajax:TabPanel>
        </ajax:TabContainer>

        <script type="text/javascript">
            function ClearHidFlag() {
                jsHelper.SetEmpty('<%= hidFlag.ClientID %>');
            }
        </script>
    </div>

    <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
        Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" />

	   
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
              
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
</asp:Content>
