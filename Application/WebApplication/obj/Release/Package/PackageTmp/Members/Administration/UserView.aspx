﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="UserView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Administration.UserView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        ShowEdit="true" ShowDelete="true" ShowSave="true" ShowFind="true" ShowMore="true"
        OnFind="OnFindClicked" OnSave="OnSaveClicked" OnDelete="OnDeleteClicked" OnNew="OnNewClicked"
        OnEdit="OnEditClicked" OnCommand="OnCommand" OnUnlock="OnUnlockClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Users
               <eShip:RecordIdentityDisplayControl runat="server" ID="ridcRecordIdentity" TargetControlId="txtUsername" />
            </h3>

            <div class="lockMessage">
                <asp:Literal ID="litMessage" runat="server" />
            </div>
        </div>

        <hr class="dark mb5" />

        <div class="errorMsgLit">
            <asp:Literal runat="server" ID="litErrorMessages" />
        </div>

        <eShip:GroupFinderControl ID="groupFinder" runat="server" Visible="false" EnableMultiSelection="True" OnMultiItemSelected="OnGroupFinderMultiItemSelected"
            OnSelectionCancel="OnGroupFinderSelectionCancel" />
        <eShip:CustomerFinderControl ID="customerFinder" runat="server" Visible="false" OpenForEditEnabled="false"
            OnItemSelected="OnCustomerFinderItemSelected" OnMultiItemSelected="OnCustomerFinderMultiItemSelected"
            OnSelectionCancel="OnCustomerFinderSelectionCancelled" />
        <eShip:UserFinderControl ID="userFinder" runat="server" Visible="False" EnableMultiSelection="False"
            OpenForEditEnabled="false" OnItemSelected="OnUserFinderItemSelected" OnSelectionCancel="OnUserFinderSelectionCancelled" />
        <eShip:PermissionSelectorControl ID="permissionSelector" runat="server" Visible="false" OnItemsSelected="OnPermissionSelectorItemsSelected"
            OnSelectionCancel="OnPermissionSelectorSelectionCancelled" />
        <eShip:VendorFinderControl EnableMultiSelection="false" ID="vendorFinder" OnlyActiveResults="true"
            OnItemSelected="OnVendorFinderItemSelected" OnSelectionCancel="OnVendorFinderSelectionCancelled"
            OpenForEditEnabled="false" runat="server" Visible="false" />

        <script type="text/javascript">
            function ClearHidFlag() {
                jsHelper.SetEmpty('<%= hidFlag.ClientID %>');
            }
        </script>

        <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
            Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" />
        <eShip:AjaxTabHeaderTextUpdater runat="server" ID="athtuTabUpdater" />

        <eShip:CustomHiddenField runat="server" ID="hidUserId" />

        <ajax:TabContainer runat="server" CssClass="ajaxCustom">
            <ajax:TabPanel runat="server" ID="tabDetails" HeaderText="Details">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlDetails">
                        <div class="rowgroup">
                            <div class="col_1_2 bbox vlinedarkright">
                                <h5>Account Details</h5>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Default Customer Account
                                            <%= hidCustomerId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.Customer) 
                                                    ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Customer Record'><img src={3} width='20' class='middle'/></a>", 
                                                                            ResolveUrl(CustomerView.PageAddress),
                                                                            WebApplicationConstants.TransferNumber, 
                                                                            hidCustomerId.Value.UrlTextEncrypt(),
                                                                            ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                    : string.Empty %>
                                        </label>
                                        <eShip:CustomHiddenField ID="hidCustomerId" runat="server" />
                                        <eShip:CustomTextBox ID="txtCustomerNumber" runat="server" AutoPostBack="True" OnTextChanged="OnCustomerNumberEntered" CssClass="w110" />
                                        <asp:ImageButton ID="ibtnFindCustomer" runat="server" CausesValidation="False" ImageUrl="~/images/icons2/search_dark.png" OnClick="OnFindCustomerClicked" />
                                        <eShip:CustomTextBox ID="txtCustomerName" runat="server" CssClass="w280 disabled" ReadOnly="True" />
                                        <ajax:AutoCompleteExtender runat="server" ID="aceCustomer" TargetControlID="txtCustomerNumber" ServiceMethod="GetCustomerList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                            EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Username</label>
                                        <eShip:CustomTextBox ID="txtUsername" runat="server" MaxLength="50" CssClass="w200" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Password</label>
                                        <eShip:CustomTextBox ID="txtPassword" runat="server" MaxLength="500" TextMode="Password" CssClass="w200" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Active Directory User Name</label>
                                        <eShip:CustomTextBox ID="txtAdUserName" runat="server" MaxLength="100" CssClass="w200" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Failed Login Attempts</label>
                                        <script type="text/javascript">

                                            $(document).ready(function () {
                                                $('#<%= btnReset.ClientID %>').click(function (e) {
                                                    e.preventDefault();
                                                    $("#<%= hidFailedAttempts.ClientID %>").attr("value", "0");
                                                    $("#<%= txtFailedAttempts.ClientID %>").attr("value", "0");
                                                });
                                            });
                                        </script>
                                        <eShip:CustomTextBox ID="txtFailedAttempts" runat="server" ReadOnly="True" CssClass="w100 disabled" />
                                        <eShip:CustomHiddenField ID="hidFailedAttempts" runat="server" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Department</label>
                                        <asp:DropDownList ID="ddlDepartment" runat="server" DataTextField="Text" DataValueField="Value" CssClass="w200" />
                                    </div>
                                </div>

                                <h5 class="pt20">Contact Details</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">First Name</label>
                                        <eShip:CustomTextBox ID="txtFirstName" runat="server" MaxLength="50" CssClass="w200" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Last Name</label>
                                        <eShip:CustomTextBox ID="txtLastName" runat="server" MaxLength="50" CssClass="w200" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Phone</label>
                                        <eShip:CustomTextBox ID="txtPhone" runat="server" MaxLength="50" CssClass="w200" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Mobile</label>
                                        <eShip:CustomTextBox ID="txtMobile" runat="server" MaxLength="50" CssClass="w200" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Fax</label>
                                        <eShip:CustomTextBox ID="txtFax" runat="server" MaxLength="50" CssClass="w200" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Email</label>
                                        <eShip:CustomTextBox ID="txtEmail" runat="server" MaxLength="100" CssClass="w200" />
                                    </div>
                                </div>

                                <h5 class="pt20">Account Options</h5>
                                <div class="row mb5">
                                    <div class="fieldgroup mr20">
                                        <asp:CheckBox ID="chkForcePasswordReset" runat="server" CssClass="jQueryUniform" />
                                        <label class="w180">Force Password Reset</label>
                                    </div>
                                    <div class="fieldgroup">
                                        <asp:CheckBox ID="chkAlwaysShowRatingNotice" runat="server" CssClass="jQueryUniform" />
                                        <label class="w180">Always Show Rating Notice</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <asp:CheckBox ID="chkAlwaysShowFinderParametersOnSearch" runat="server" CssClass="jQueryUniform" />
                                        <label>Always Show Finder Parameters On Search</label>
                                    </div>
                                </div>

                                <h5 class="pt20">Vendor Portal Options</h5>
                                <div class="row mb5">
                                    <div class="fieldgroup">
                                        <asp:CheckBox ID="chkCanSeeAllVendorsInPortal" runat="server" CssClass="jQueryUniform" />
                                        <label class="w180">Can See All Vendors in Portal</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Vendor Account
                                                            <%= hidVendorId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.Vendor) 
                                                            ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Vendor Record'><img src={3} width='20' class='middle'/></a>", 
                                                                                    ResolveUrl(VendorView.PageAddress),
                                                                                    WebApplicationConstants.TransferNumber, 
                                                                                    hidVendorId.Value.UrlTextEncrypt(),
                                                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                            : string.Empty %>
                                        </label>
                                        <eShip:CustomHiddenField runat="server" ID="hidVendorId" />
                                        <eShip:CustomTextBox runat="server" ID="txtVendorNumber" CssClass="w150" OnTextChanged="OnVendorNumberEntered"
                                            AutoPostBack="True" />
                                        <asp:ImageButton ID="btnFindVendor" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                                            CausesValidation="false" OnClick="OnVendorSearchClicked" />
                                        <asp:ImageButton ID="btnClearVendor" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                            CausesValidation="false" OnClick="OnClearVendorClicked" Width="20px" />
                                        <eShip:CustomTextBox runat="server" ID="txtVendorName" CssClass="w240 disabled" ReadOnly="True" />
                                    </div>
                                </div>

                            </div>
                        <div class="col_1_2 bbox pl46">
                                <h5>User Location</h5>
                                <eShip:SimpleAddressInputControl ID="saicUserAddress" runat="server" CssClass="w300" />
                                <h5 class="pt20">Account Attributes</h5>
                                <div class="row mb5">
                                    <div class="fieldgroup mr20">
                                        <asp:CheckBox ID="chkEnabled" runat="server" CssClass="jQueryUniform" />
                                        <label class="w180">Enabled</label>
                                    </div>
                                    <div class="fieldgroup">
                                        <asp:CheckBox ID="chkEmployee" runat="server" CssClass="jQueryUniform" />
                                        <label class="w180">Is Employee</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <asp:CheckBox ID="chkIsCarrierCoordinator" runat="server" CssClass="jQueryUniform" />
                                        <label class="w180">Is Carrier Coordinator</label>
                                    </div>
                                    <div class="fieldgroup">
                                        <asp:CheckBox ID="chkIsShipmentCoordinator" runat="server" CssClass="jQueryUniform" />
                                        <label class="w180">Is Shipment Coordinator</label>
                                    </div>
                                </div>
                                <h5 class="pt20">Integration Settings</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Qlik User Id</label>
                                        <eShip:CustomTextBox ID="txtQlikUserId" runat="server" MaxLength="50" CssClass="w200" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Qlik User Directory</label>
                                        <eShip:CustomTextBox ID="txtQlikUserDirectory" runat="server" MaxLength="50" CssClass="w200" />
                                    </div>
                                </div>
                                <div class="row mb5">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">DAT Loadboard Username</label>
                                        <eShip:CustomTextBox ID="txtDatLoadboardUsername" runat="server" MaxLength="50" CssClass="w200" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">DAT Loadboard Password</label>
                                        <eShip:CustomTextBox ID="txtDatLoadboardPassword" runat="server" MaxLength="50" CssClass="w200" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabPermissions" HeaderText="Permissions">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlPermissions" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel ID="pnlPermissions" runat="server">
                                <div class="row mb10">
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddPermission" Text="Add Permission" CssClass="mr10" OnClick="OnAddPermissionClicked" CausesValidation="False" />
                                        <asp:Button runat="server" ID="btnClearPermissions" Text="Clear Permissions" OnClick="OnClearPermissionsClicked" CausesValidation="False" />
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheuserPermissionTable" TableId="userPermissionTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table id="userPermissionTable" class="stripe">
                                    <tr>
                                        <th style="width: 30%;">Code</th>
                                        <th style="width: 30%;">Description</th>
                                        <th style="width: 10%;" class="text-center">Grant</th>
                                        <th style="width: 10%;" class="text-center">Deny</th>
                                        <th style="width: 10%;" class="text-center">Modify</th>
                                        <th style="width: 10%;" class="text-center">Delete</th>
                                        <th>Action</th>
                                    </tr>
                                    <asp:ListView ID="lstPermissions" OnItemDataBound="BindPermissionLineItem" runat="server"
                                        ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <eShip:PermissionLineItemControl ID="pcPermissionLineItem" runat="server" OnRemovePermission="OnPermissionItemRemovePermission"
                                                    EnableDelete='<%# Access.Modify %>' />
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnAddPermission" />
                        </Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabShipAs" HeaderText="Ship As Customer">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlShipAs" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlShipAs">
                                <div class="row mb10">
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddShipAs" Text="Add Customers" CssClass="mr10" OnClick="OnAddShipAsClicked" CausesValidation="False" />
                                        <asp:Button runat="server" ID="btnClearAllShipAs" Text="Clear Ship As" CausesValidation="False" OnClick="OnClearShipAsClicked" />
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheuserCustomerShipAsTable" TableId="userCustomerShipAsTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table class="stripe" id="userCustomerShipAsTable">
                                    <tr>
                                        <th>Customer Name
                                        </th>
                                        <th>Customer Number
                                        </th>
                                        <th class="text-center">Active
                                        </th>
                                        <th class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstShipAs" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField runat="server" ID="hidCustomerItemIndex" Value='<%# Container.DataItemIndex %>' />
                                                    <eShip:CustomHiddenField ID="hidCustomerId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <asp:Literal ID="litCustomerName" Text='<%# Eval("Name") %>' runat="server" />
                                                </td>
                                                <td>
                                                    <asp:Literal runat="server" ID="litCustomerNumber" Text='<%# Eval("CustomerNumber") %>'/>
                                                    <%# ActiveUser.HasAccessTo(ViewCode.Customer) 
                                                        ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Customer Record'><img src='{3}' alt='' width='16' align='absmiddle'/></a>", 
                                                            ResolveUrl(CustomerView.PageAddress),
                                                            WebApplicationConstants.TransferNumber, 
                                                            Eval("Id").GetString().UrlTextEncrypt(),
                                                            ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                        : string.Empty  %>
                                                </td>
                                                <td class="text-center">
                                                    <%# Eval("Active").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                                                    <eShip:CustomHiddenField runat="server" ID="hidActive" Value='<%# Eval("Active").ToBoolean().GetString() %>' />
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnShipAsDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        OnClick="OnDeleteShipAsClicked" CausesValidation="false" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnAddShipAs" />
                        </Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel ID="tabPermissionGroups" runat="server" HeaderText="Permission Groups">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlUsers">
                        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheuserPermissionGroupTable" TableId="userPermissionGroupTable" IgnoreHeaderBackgroundFill="True" />
                        <table class="stripe" id="userPermissionGroupTable">
                            <tr>
                                <th>Group Name</th>
                            </tr>
                            <asp:ListView ID="lstGroups" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                <LayoutTemplate>
                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <eShip:CustomHiddenField ID="hidGroupId" runat="server" Value='<%# Eval("Id") %>' />
                                            <asp:Literal runat="server" ID="litName" Text='<%# Eval("Name") %>' />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:ListView>
                        </table>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabAuditLog" HeaderText="Audit Log">
                <ContentTemplate>
                    <eShip:AuditLogControl ID="auditLogs" runat="server" />
                </ContentTemplate>
            </ajax:TabPanel>
        </ajax:TabContainer>
        <asp:Panel runat="server" ID="pnlSelectGroupsToRemove" Visible="false" CssClass="popupControlOver popupControlOverW500">
            <div class="popheader">
                <h4>Remove Groups</h4>
                <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                    CausesValidation="false" OnClick="OnCloseEditGroupClicked" runat="server" />
            </div>
            <table class="poptable">
                <tr>
                    <td colspan="2" class="p_m0">
                        <eShip:TableFreezeHeaderExtender runat="server" ID="tfhePermissionSelectorPermissionsTable" TableId="userViewRemoveGroupTable"
                            ScrollerContainerId="userViewRemoveGroupScrollSection" ScrollOffsetControlId="pnlSelectGroupsToRemove" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2050" />
                        <div class="finderScroll" id="userViewRemoveGroupScrollSection">
                            <table class="stripe sm_chk" id="userViewRemoveGroupTable">
                                <tr>
                                    <th style="width: 5%;">&nbsp;</th>
                                    <th style="width: 95%;" class="text-left">Group Name</th>
                                </tr>
                                <asp:ListView runat="server" ID="lstRemoveGroups" ItemPlaceholderID="itemPlaceHolder">
                                    <LayoutTemplate>
                                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <eShip:CustomHiddenField ID="hidGroupId" runat="server" Value='<%# Eval("Id") %>' />
                                                <asp:CheckBox runat="server" ID="chkSelection" CssClass="jQueryUniform" />
                                            </td>
                                            <td>
                                                <%# Eval("Name") %>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="btnEditItemDone" Text="Done" OnClick="OnEditGroupDoneClicked" runat="server" CausesValidation="false" />
                        <asp:Button ID="btnEditItemClose" Text="Cancel" OnClick="OnCloseEditGroupClicked" runat="server" CausesValidation="false" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <eShip:FileUploaderControl runat="server" ID="fileUploader" Title="Import Users" Instructions="**Please refer to help menu for import file format." Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
            OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
        <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
        <eShip:CustomHiddenField runat="server" ID="hidFlag" />
    </div>
</asp:Content>
