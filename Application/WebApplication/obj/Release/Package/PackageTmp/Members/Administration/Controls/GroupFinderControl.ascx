﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GroupFinderControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Administration.Controls.GroupFinderControl" %>
<asp:Panel runat="server" ID="pnlGroupFinderContent" CssClass="popupControl" DefaultButton="btnSearch">
    <div class="popheader">
        <h4>Group Finder <small class="ml10"><asp:Literal runat="server" ID="litRecordCount" Text="" /></small>
        <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/icons2/close.png" CssClass="close"
            CausesValidation="false" OnClick="OnCancelClicked" runat="server" /></h4>
    </div>
    <script type="text/javascript">
        $(function () {
            if (jsHelper.IsTrue($('#<%= hidAreFiltersShowing.ClientID%>').val())) {
                $('#<%= pnlSearch.ClientID%>').show();
                $('#<%= pnlSearchShowFilters.ClientID%>').hide();
            } else {
                $('#<%= pnlSearch.ClientID%>').hide();
                $('#<%= pnlSearchShowFilters.ClientID%>').show();
            }
        });

        function ToggleFilters() {
            $('#<%= hidAreFiltersShowing.ClientID%>').val(jsHelper.IsTrue($('#<%= hidAreFiltersShowing.ClientID%>').val()) ? false : true);
            $('#<%= pnlSearch.ClientID%>').toggle();
            $('#<%= pnlSearchShowFilters.ClientID%>').toggle();
            var top = $('#<%= pnlGroupFinderContent.ClientID%> div[class="finderScroll"]').offset().top - $('#<%= pnlGroupFinderContent.ClientID%>').offset().top - 2;
            $('#<%= pnlGroupFinderContent.ClientID%> div[class="finderScroll"] > div[id^="hd"]').css('top', top + 'px');
        }
    </script>
    <table id="finderTable" class="poptable">
        <tr>
            <td>
                <asp:Panel runat="server" ID="pnlSearchShowFilters" class="rowgroup mb0" style="display:none;">
                    <div class="row">
                        <div class="fieldgroup">
                            <asp:Button ID="btnSelectAll2" runat="server" Text="ADD SELECTED" OnClick="OnSelectAllClicked" CssClass="ml10" CausesValidation="false" />
                        </div>
                        <div class="fieldgroup right">
                            <button onclick="ToggleFilters(); return false;">Show Filters</button>
                        </div>
                        <div class="fieldgroup right mb5 mt5 mr10">
                            <span class="fs75em  ">*** If you would like to show parameters when searching change the 'Always Show Finder Parameters On Search' option on your <asp:HyperLink ID="hypGoToUserProfile" Target="_blank" CssClass="blue" runat="server" Text="User Profile"/></span>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
                    <div class="mt5">
                        <div class="rowgroup mb0">
                            <div class="row">
                                <div class="fieldgroup">
                                    <asp:LinkButton ID="LinkButton1" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                                        CausesValidation="False" CssClass="add" />
                                </div>
                                <div class="fieldgroup right">
                                    <button onclick="ToggleFilters(); return false;">Hide Filters</button>
                                </div>
                                <div class="right">
                                    <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="Groups" ShowAutoRefresh="False"
                                        OnProcessingError="OnSearchProfilesProcessingError" OnSearchProfileSelected="OnSearchProfilesProfileSelected" />
                                </div>
                            </div>
                        </div>

                        <hr class="mb5" />
                        <script type="text/javascript">$(window).load(function () {SetControlFocus();});</script><table class="mb5" id="parametersTable">
                            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                                OnItemDataBound="OnParameterItemDataBound">
                                <LayoutTemplate>
                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                                        OnRemoveParameter="OnParameterRemove" />
                                </ItemTemplate>
                            </asp:ListView>
                            <tr>
                                <td colspan="4">
                                    <asp:Button ID="btnSearch" CssClass="left" runat="server" Text="SEARCH" OnClick="OnSearchClicked"
                                        CausesValidation="False" />
                                    <asp:Button ID="btnCancel" OnClick="OnCancelClicked" runat="server" Text="CANCEL" CssClass="ml10"
                                        CausesValidation="false" />
                                    <asp:Button ID="btnSelectAll" runat="server" Text="ADD SELECTED" OnClick="OnSelectAllClicked" CssClass="ml10"
                                        CausesValidation="false" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </td>
        </tr>
    </table>

    <asp:UpdatePanel runat="server" ID="upDataUpdate">
        <ContentTemplate>
            <eShip:ContinuousScrollExtender ID="cseDataUpdate" runat="server" BindableControlId="lstSearchResults" UpdatePanelToExtendId="upDataUpdate" ScrollableDivId="groupFinderScrollSection"
                SelectionCheckBoxControlId="chkSelected" SelectionUniqueRefHiddenFieldId="hidGroupId" PreserveRecordSelection="False" />
            <eShip:TableFreezeHeaderExtender runat="server" ID="tfheGroupsTable" TableId="groupFinderTable" ScrollerContainerId="groupFinderScrollSection" ScrollOffsetControlId="pnlGroupFinderContent"
                IgnoreHeaderBackgroundFill="True" HeaderZIndex="2050"/>
            <div class="finderScroll" id="groupFinderScrollSection">
                <asp:ListView ID="lstSearchResults" runat="server" ItemPlaceholderID="itemPlaceHolder">
                    <LayoutTemplate>
                        <table id="groupFinderTable" class="stripe sm_chk">
                            <tr>
                                <th style="width: 18%">
                                    <eShip:AltUniformCheckBox ID="chkSelectAllRecords" runat="server" OnClientSideClicked="javascript:SetSelectedCheckBoxesStatus(this, 'groupFinderTable');"  />
                                </th>
                                <th style="width: 30%">Name
                                </th>
                                <th style="width: 52%">Description
                                </th>
                            </tr>
                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:Button ID="btnSelect" runat="server" Text="Select" OnClick="OnSelectClicked"
                                    Visible='<%# !EnableMultiSelection %>' CausesValidation="false" />
                                <asp:Button ID="btnEditSelect" runat="server" Text="Edit" OnClick="OnEditSelectClicked"
                                    CausesValidation="false" Visible='<%# !EnableMultiSelection && OpenForEditEnabled %>' />
                                <eShip:AltUniformCheckBox ID="chkSelected" Visible='<%# EnableMultiSelection %>' runat="server" OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'groupFinderTable', 'chkSelectAllRecords');"  />
                                <eShip:CustomHiddenField ID="hidGroupId" Value='<%# Eval("Id") %>' runat="server" />
                            </td>
                            <td>
                                <%# Eval("Name") %>
                            </td>
                            <td>
                                <%# Eval("Description") %>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lstSearchResults" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Panel>
<eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
<asp:Panel ID="pnlGroupFinderDimScreen" CssClass="dimBackgroundControl" runat="server" Visible="false" />
<eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
<eShip:CustomHiddenField runat="server" ID="hidGroupFinderEnableMultiSelection" />
<eShip:CustomHiddenField runat="server" ID="hidEditSelected" />
<eShip:CustomHiddenField runat="server" ID="hidOpenForEditEnabled" />
<eShip:CustomHiddenField runat="server" ID="hidAreFiltersShowing" Value="true" />