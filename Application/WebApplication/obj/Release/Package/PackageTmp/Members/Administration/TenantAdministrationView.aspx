﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="TenantAdministrationView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Administration.TenantAdministrationView" 
    EnableEventValidation="false" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI, Version=2009.3.1208.20, Culture=neutral, PublicKeyToken=121fae78165ba3d4" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="false"
        ShowEdit="true" ShowDelete="false" ShowSave="true" ShowFind="false" OnSave="OnSaveClicked"
        OnEdit="OnEditClicked" OnUnlock="OnUnlockClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">

        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Tenant Administration</h3>

            <div class="lockMessage">
                <asp:Literal ID="litMessage" runat="server" />
            </div>
        </div>

        <hr class="dark mb5" />

        <div class="errorMsgLit">
            <asp:Literal runat="server" ID="litErrorMessages" />
        </div>

        <eShip:CustomHiddenField runat="server" ID="hidTenantId" />

        <ajax:TabContainer ID="tabTenantAdministration" runat="server" CssClass="ajaxCustom">
            <ajax:TabPanel runat="server" ID="tabDetails" HeaderText="Details">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlDetails">
                        <div class="rowgroup">
                            <div class="fieldgroup mr10">
                                <label class="wlabel blue">Code</label>
                                <eShip:CustomTextBox ID="txtCode" runat="server" CssClass="w100 disabled" Enabled="False" />
                            </div>
                            <div class="fieldgroup mr10">
                                <label class="wlabel blue">Name</label>
                                <eShip:CustomTextBox ID="txtName" runat="server" CssClass="w200 disabled" Enabled="False" />
                            </div>
                            <div class="fieldgroup_s mr10 vlinedarkright">
                                <asp:CheckBox runat="server" ID="chkActive" Enabled="False" CssClass="jQueryUniform" />
                                <label>Active</label>
                            </div>
                            <div class="fieldgroup mr10">
                                <label class="wlabel blue">Logo Image <span class="fs85em">(max: 75px by 400px)</span></label>
                                <asp:FileUpload ID="fupLogoImage" runat="server" CssClass="jQueryUniform" />
                                <asp:Button ID="btnClearLogo" runat="server" Text="CLEAR LOGO" OnClick="OnClearLogoClicked" />

                            </div>
                            <div class="fieldgroup">
                                <asp:Image runat="server" ID="imgLogo" CssClass="right Img60MaxHeight" />
                                <eShip:CustomHiddenField runat="server" ID="hidLogoUrl" />
                            </div>
                        </div>

                        <hr class="dark" />

                        <div class="rowgroup">

                            <div class="col_1_2 bbox vlinedarkright">
                                <h5>Tenant Defaults</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Default Country</label>
                                        <eShip:CachedObjectDropDownList runat="server" ID ="ddlDefaultCountryId" CssClass="w200" Type="Countries" EnableChooseOne="True" DefaultValue="0"/>
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Default Scheduling Contact Type</label>
                                        <eShip:CachedObjectDropDownList runat="server" ID="ddlDefaultSchedulingContactType" CssClass="w200" Type="ContactTypes" EnableNotApplicableLong="True" DefaultValue="0"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Default Packaging Type</label>
                                        <eShip:CachedObjectDropDownList runat="server" ID="ddlDefaultPackagingType" CssClass="w200" Type="PackageType" EnableNotApplicableLong="True" DefaultValue="0"/>
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Available Loads Contact Type</label>
                                        <eShip:CachedObjectDropDownList runat="server" ID="ddlAvailableLoadsContactType" CssClass="w200" Type="ContactTypes" EnableNotApplicableLong="True" DefaultValue="0"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup  mr20">
                                        <label class="wlabel">Auto MacroPoint Charge Code</label>
                                        <eShip:CachedObjectDropDownList runat="server" ID ="ddlMacroPointChargeCode" CssClass="w200" Type="ChargeCodes" EnableNotApplicableLong="True"/>
                                    </div>

                                    <div class="fieldgroup">
                                        <label class="wlabel">Auto LTL Hazardous Material Service</label>
                                        <eShip:CachedObjectDropDownList runat="server" ID ="ddlHarzadousMaterialService" CssClass="w200" Type="Services" EnableNotApplicableLong="True"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Default Mileage Engine</label>
                                        <asp:DropDownList runat="server" ID="ddlMileageEngine" CssClass="w200" DataTextField="Text" DataValueField="Value" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Auto LTL Border Crossing Service</label>
										<eShip:CachedObjectDropDownList runat="server" ID ="ddlBorderCrossingService" CssClass="w200" Type="Services" EnableNotApplicableLong="True"/>
                                    </div>
                                </div>
	                            <div class="row">
	                                <div class="fieldgroup">
	                                    <label class="wlabel">Default Unmapped Charge Code</label>
	                                    <eShip:CachedObjectDropDownList runat="server" ID ="ddlDefaultUnmappedChargeCode" CssClass="w200" Type="ChargeCodes" EnableNotApplicableLong="True"/>
	                                </div>
		                            <div class="fieldgroup">
			                            <label class="wlabel">Auto LTL Guaranteed Delivery Service</label>
			                            <eShip:CachedObjectDropDownList runat="server" ID ="ddlGuaranteedDeliveryService" CssClass="w200" Type="Services" EnableNotApplicableLong="True"/>
		                            </div>
	                            </div>
                                <div class="row pt20">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            <abbr title="Required for system automated processes that require a user account association.">Default System User</abbr>*</label>
                                        <eShip:CustomHiddenField runat="server" ID="hidDefaultSystemUserId" />
                                        <eShip:CustomTextBox runat="server" ID="txtDefaultSystemUsername" CssClass="w75" OnTextChanged="OnDefaultSystemUsernameTextChanged"
                                            AutoPostBack="True" />
                                        <asp:ImageButton runat="server" ID="imgDefaultSystemUsernameSearch" ToolTip="Find Default System User"
                                            ImageUrl="~/images/icons2/search_dark.png" CausesValidation="False" OnClick="OnFindDefaultSystemUserClicked" />
                                        <eShip:CustomTextBox runat="server" ID="txtDefaultSystemUser" CssClass="w230 disabled" ReadOnly="True" />
                                        <ajax:AutoCompleteExtender runat="server" ID="aceUser" TargetControlID="txtDefaultSystemUsername" ServiceMethod="GetUserList" ServicePath="~/Services/eShipPlusWSv4P.asmx" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" DelimiterCharacters="" Enabled="True" />
                                    </div>
                                </div>
                                <div class="row pt20">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Batch Rating Analysis Available Between
                                        </label>
                                        <script type="text/javascript">
                                            function OffsetEndTimes() {
                                                var ids = new ControlIds();
                                                ids.TimesOfDayWindowStart = $(jsHelper.AddHashTag('<%= ddlStartTime.ClientID%>')).attr('id');
                                                ids.TimesOfDayWindowEnd = $(jsHelper.AddHashTag('<%= ddlEndTime.ClientID%>')).attr('id');

                                                OffsetDropDownListByTimeOfDay(false, ids);
                                            }
                                        </script>
                                        <asp:DropDownList ID="ddlStartTime" runat="server" CssClass="w110" onchange="OffsetEndTimes();" />
                                        <asp:Image runat="server" ImageUrl="~/images/icons2/amper.png" CssClass="pl10 pr10" />
                                        <asp:DropDownList ID="ddlEndTime" runat="server" CssClass="w110" />
                                    </div>
                                </div>
                                <div class="row pt20">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">SCAC (if applicable)</label>
                                        <eShip:CustomTextBox ID="txtScac" runat="server" CssClass="w200" MaxLength="10" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Auto Notification Email Subject Prefix</label>
                                        <eShip:CustomTextBox ID="txtAutoNotificationSubjectPrefix" runat="server" CssClass="w200" MaxLength="50" />
                                    </div>
                                </div>
                                <div class="row pt20">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">FTL Pickup Tolerance (minutes)</label>
                                        <eShip:CustomTextBox ID="txtFTLPickupTolerance" runat="server" CssClass="w100" MaxLength="4" />
                                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="txtFTLPickupTolerance"
                                            FilterType="Numbers" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">FTL Delivery Tolerance (minutes)</label>
                                        <eShip:CustomTextBox ID="txtFTLDeliveryTolerance" runat="server" Width="100px" MaxLength="4" />
                                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" TargetControlID="txtFTLDeliveryTolerance"
                                            FilterType="Numbers" />
                                    </div>
                                </div>

                            </div>

                            <div class="col_1_2 bbox pl40">
                                <h5>Insurance Settings</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Insurance Cost Rate Per Dollar ($)</label>
                                        <eShip:CustomTextBox ID="txtInsuranceCostPerDollar" runat="server" CssClass="w200" MaxLength="9" />
                                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtInsuranceCostPerDollar"
                                            FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Insurance Cost Total (%)</label>
                                        <eShip:CustomTextBox ID="txtInsuranceCostTotalPercentage" runat="server" CssClass="w200" MaxLength="9" />
                                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtInsuranceCostTotalPercentage"
                                            FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Insurance Cost Purchase Floor ($)</label>
                                        <eShip:CustomTextBox ID="txtInsuranceCostPurchaseFloor" runat="server" CssClass="w200" MaxLength="9" />
                                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtInsuranceCostPurchaseFloor"
                                            FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Insurance Revenue Rate Per Dollar ($)</label>
                                        <eShip:CustomTextBox ID="txtInsuranceRevenueRatePerDollar" runat="server" CssClass="w200" MaxLength="9" />
                                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtInsuranceRevenueRatePerDollar"
                                            FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Insurance Revenue Total (%)</label>
                                        <eShip:CustomTextBox ID="txtInsuranceRevenueTotalPercentage" runat="server" CssClass="w200" MaxLength="9" />
                                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtInsuranceRevenueTotalPercentage"
                                            FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                    </div>
                                </div>

                                <h5 class="pt20">Document Retrieval Settings</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">POD Document Tag</label>
                                        <eShip:CachedObjectDropDownList runat="server" ID ="ddlPodDocTag" CssClass="w200" Type="DocumentTag" EnableNotApplicableLong="True" DefaultValue="0"/>
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">BOL Document Tag</label>
                                        <eShip:CachedObjectDropDownList runat="server" ID ="ddlBolDocTag" CssClass="w200" Type="DocumentTag" EnableNotApplicableLong="True" DefaultValue="0"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">WNI Document Tag</label>
                                        <eShip:CachedObjectDropDownList runat="server" ID ="ddlWniDocTag" CssClass="w200" Type="DocumentTag" EnableNotApplicableLong="True" DefaultValue="0"/>
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Image Retrieval Allowance Days</label>
                                        <eShip:CustomTextBox ID="txtImgRtrvAllowanceDays" runat="server" CssClass="w100" MaxLength="4" />
                                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtImgRtrvAllowanceDays"
                                            FilterType="Numbers" />
                                    </div>
                                </div>
                                <h5 class="pt20">Payment Gateway Settings</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Type</label>
                                        <asp:DropDownList runat="server" ID="ddlPaymentGatewayType" DataTextField="Text" DataValueField="Value" CssClass="w200"/>
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Login Id</label>
                                        <eShip:CustomTextBox runat="server" ID="txtPaymentGatewayLoginId" CssClass="w200" MaxLength="500"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Transaction Id</label>
                                        <eShip:CustomTextBox runat="server" ID="txtPaymentGatewayTransactionId" CssClass="w200" MaxLength="500"/>
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Secret</label>
                                        <eShip:CustomTextBox runat="server" ID="txtPaymentGatewaySecret" CssClass="w200" MaxLength="500"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup pt10">
                                        <asp:CheckBox runat="server" ID="chkPaymentGatewayInTestMode" CssClass="jQueryUniform"/>
                                        <label>Payment Gateway Is In Test Mode</label>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabLocation" HeaderText="Location">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlLocations">

                        <div class="rowgroup">
                            <div class="row">
                                <h5>Mailing Location</h5>
                                <div class="col_1_3 vlinedarkright">
                                    <h5 class="dark">Address</h5>
                                    <div class="rowgroup">
                                        <eShip:SimpleAddressInputControl ID="mailingAddressInput" runat="server" />
                                    </div>
                                </div>
                                <div class="col_2_3">
                                    <h5 class="dark">Contacts
                                        <asp:Button runat="server" ID="btnAddMailingContact" Text="Add Contact" OnClick="OnAddContactClicked" CssClass="right" />
                                    </h5>
                                    <eShip:CustomHiddenField ID="hidMailingLocationId" runat="server" />
                                    <asp:ListView ID="lstMailingContacts" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <table class="stripe">
                                                <tr>
                                                    <th style="width: 3%;">&nbsp;</th>
                                                    <th style="width: 15%;">Name
                                                    </th>
                                                    <th style="width: 15%;">Phone
                                                    </th>
                                                    <th style="width: 15%;">Mobile
                                                    </th>
                                                    <th style="width: 15%;">Fax
                                                    </th>
                                                    <th style="width: 20%;">Email
                                                    </th>
                                                    <th style="width: 10%; text-align: center;">Primary
                                                    </th>
                                                    <th style="width: 7%;">Action
                                                    </th>
                                                </tr>
                                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                            </table>
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td class="top">
                                                    <%# Container.DataItemIndex + 1%>.
                                                </td>
                                                <td class="text-left">
                                                    <eShip:CustomTextBox ID="txtName" runat="server" Text='<%# Eval("Name")%>' CssClass="w100" />
                                                    <eShip:CustomHiddenField ID="hidContactId" runat="server" Value='<%# Eval("Id")%>' />
                                                    <eShip:CustomHiddenField runat="server" ID="hidContactIndex" Value='<%# Container.DataItemIndex%>' />
                                                </td>
                                                <td class="text-left">
                                                    <eShip:CustomTextBox ID="txtPhone" runat="server" Text='<%# Eval("Phone")%>' CssClass="w80" />
                                                </td>
                                                <td class="text-left">
                                                    <eShip:CustomTextBox ID="txtMobile" runat="server" Text='<%# Eval("Mobile")%>' CssClass="w80" />
                                                </td>
                                                <td class="text-left">
                                                    <eShip:CustomTextBox ID="txtFax" runat="server" Text='<%# Eval("Fax")%>' CssClass="w80" />
                                                </td>
                                                <td class="text-left">
                                                    <eShip:CustomTextBox ID="txtEmail" runat="server" Text='<%# Eval("Email")%>' CssClass="w100" />
                                                </td>
                                                <td class="text-center">
                                                    <asp:CheckBox runat="server" ID="chkPrimary" Checked='<%# Eval("Primary")%>' CssClass="jQueryUniform" />
                                                </td>
                                                <td class="text-center top">
                                                    <eShip:CustomHiddenField runat="server" ID="hidEditingFlag" Value='<%# MailingLocation%>' />
                                                    <asp:ImageButton ID="ibtnDelete" ToolTip="Delete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        CausesValidation="false" OnClick="OnContactDeleteClicked" Enabled='<%# Access.Modify%>' />
                                                </td>
                                            </tr>
                                            <tr class="bottom_shadow">
                                                <td>&nbsp;</td>
                                                <td colspan="6" class="top">
                                                    <div class="rowgroup mb5">
                                                        <div class="row">
                                                            <div class="fieldgroup mr20">
                                                                <label class="wlabel">Contact Type</label>
                                                                <eShip:CachedObjectDropDownList runat="server" ID="ddlContactContactTypes" CssClass="w200" Type="ContactTypes"
                                                                     DefaultValue="0" EnableChooseOne="True" SelectedValue='<%# Eval("ContactTypeId") %>'/>
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel">
                                                                    Comment<eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDescription" TargetControlId="txtComments" MaxLength="200" />
                                                                </label>
                                                                <eShip:CustomTextBox ID="txtComments" runat="server" CssClass="w340" TextMode="MultiLine" Text='<%# Eval("Comment")%>' />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </div>
                            </div>

                            <hr class="dark" />

                            <div class="row">
                                <h5>Billing Location</h5>
                                <div class="col_1_3 vlinedarkright">
                                    <h5 class="dark">Address</h5>
                                    <div class="rowgroup">
                                        <eShip:SimpleAddressInputControl ID="billingAddressInput" runat="server" />
                                    </div>
                                </div>
                                <div class="col_2_3">
                                    <h5 class="dark">Contacts
                                         <asp:Button runat="server" ID="btnAddBillingContact" Text="Add Contact" CssClass="right" OnClick="OnAddContactClicked" />
                                    </h5>
                                    <eShip:CustomHiddenField ID="hidBillingLocationId" runat="server" />
                                    <asp:ListView ID="lstBillingContacts" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <table class="stripe">
                                                <tr>
                                                    <th style="width: 3%;">&nbsp;</th>
                                                    <th style="width: 15%;">Name
                                                    </th>
                                                    <th style="width: 15%;">Phone
                                                    </th>
                                                    <th style="width: 15%;">Mobile
                                                    </th>
                                                    <th style="width: 15%;">Fax
                                                    </th>
                                                    <th style="width: 20%;">Email
                                                    </th>
                                                    <th style="width: 10%; text-align: center;">Primary
                                                    </th>
                                                    <th style="width: 7%;">Action
                                                    </th>
                                                </tr>
                                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                            </table>
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <td class="top">
                                                <%# Container.DataItemIndex + 1%>.
                                            </td>
                                            <td class="text-left">
                                                <eShip:CustomTextBox ID="txtName" runat="server" Text='<%# Eval("Name")%>' CssClass="w100" />
                                                <eShip:CustomHiddenField ID="hidContactId" runat="server" Value='<%# Eval("Id")%>' />
                                                <eShip:CustomHiddenField runat="server" ID="hidContactIndex" Value='<%# Container.DataItemIndex%>' />
                                            </td>
                                            <td class="text-left">
                                                <eShip:CustomTextBox ID="txtPhone" runat="server" Text='<%# Eval("Phone")%>' CssClass="w80" />
                                            </td>
                                            <td class="text-left">
                                                <eShip:CustomTextBox ID="txtMobile" runat="server" Text='<%# Eval("Mobile")%>' CssClass="w80" />
                                            </td>
                                            <td class="text-left">
                                                <eShip:CustomTextBox ID="txtFax" runat="server" Text='<%# Eval("Fax")%>' CssClass="w80" />
                                            </td>
                                            <td class="text-left">
                                                <eShip:CustomTextBox ID="txtEmail" runat="server" Text='<%# Eval("Email")%>' CssClass="w100" />
                                            </td>
                                            <td class="text-center">
                                                <asp:CheckBox runat="server" ID="chkPrimary" Checked='<%# Eval("Primary")%>' CssClass="jQueryUniform" />
                                            </td>
                                            <td class="text-center top">
                                                <eShip:CustomHiddenField runat="server" ID="hidEditingFlag" Value='<%# BillingLocation%>' />
                                                <asp:ImageButton ID="ibtnDelete" ToolTip="Delete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                    CausesValidation="false" OnClick="OnContactDeleteClicked" Enabled='<%# Access.Modify%>' />
                                            </td>
                                            </tr>
                                              <tr class="bottom_shadow">
                                                  <td>&nbsp;</td>
                                                  <td colspan="6" class="top">
                                                      <div class="rowgroup mb5">
                                                          <div class="row">
                                                              <div class="fieldgroup mr20">
                                                                  <label class="wlabel">Contact Type</label>
                                                                <eShip:CachedObjectDropDownList runat="server" ID="ddlContactContactTypes" CssClass="w200" Type="ContactTypes" 
                                                                    EnableNotApplicableLong="True" DefaultValue="0" EnableChooseOne="True" SelectedValue='<%# Eval("ContactTypeId") %>'/>
                                                              </div>
                                                              <div class="fieldgroup">
                                                                  <label class="wlabel">
                                                                      Comment<eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDescription" TargetControlId="txtComments" MaxLength="200" />
                                                                  </label>
                                                                  <eShip:CustomTextBox ID="txtComments" runat="server" CssClass="w340" TextMode="MultiLine" Rows="5" Text='<%# Eval("Comment")%>' />
                                                              </div>
                                                          </div>
                                                      </div>
                                                  </td>
                                                  <td>&nbsp;</td>
                                              </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </div>
                            </div>
                        </div>

                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabIntegration" HeaderText="Integration">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlIntegration">

                        <script type="text/javascript">

                            $(document).ready(function () {
                                jsHelper.SetDoPostBackJsLink('<%= lbtnDownloadSmcRateWare.ClientID%>');
                                jsHelper.SetDoPostBackJsLink('<%= lbtnDownloadSmcCarrierConnect.ClientID%>');
                                jsHelper.SetDoPostBackJsLink('<%= lbtnDownloadFedExSmallPack.ClientID%>');
                                jsHelper.SetDoPostBackJsLink('<%= lbtnDownloadUpsSmallPack.ClientID%>');
                                jsHelper.SetDoPostBackJsLink('<%= lbtnDownloadMacroPoint.ClientID %>');
                                jsHelper.SetDoPostBackJsLink('<%= lBtnDownloadTermsAndConditions.ClientID%>');
                            });

                            function SetRequireIntegrationUpdate() {
                                $(jsHelper.AddHashTag('<%# hidRequireIntergrationUpdate.ClientID%>')).val("true");
                            }

                        </script>

                        <div class="rowgroup">
                            <ul class="twocol_list">
                                <li>
                                    <label class="wlabel">SMC RateWare Parameter File</label>
                                    <asp:FileUpload ID="fupSmcRateWare" runat="server" CssClass="jQueryUniform" />
                                    <asp:CheckBox runat="server" ID="chkSmcRateWareEnabled" onclick="javascript:SetRequireIntegrationUpdate();" CssClass="jQueryUniform" />
                                    <label class="pr10">Enabled</label>
                                    <asp:Button ID="btnSmcRateWareClear" runat="server" Text="CLEAR FILE" OnClick="OnSmcRateWareClearClicked" />
                                    <label class="w100p">
                                        Current File: 
                                        <span class="blue">
                                            <asp:LinkButton ID="lbtnDownloadSmcRateWare" runat="server" OnClick="OnDownloadSmcRatewareClicked" />
                                        </span>
                                    </label>
                                    <eShip:CustomHiddenField runat="server" ID="hidSmcRateWare" />
                                </li>
                                <li>
                                    <label class="wlabel">SMC Carrier Connect Parameter File</label>
                                    <asp:FileUpload ID="fupSmcCarrierConnect" runat="server" CssClass="jQueryUniform" />
                                    <asp:CheckBox runat="server" ID="chkSmcCarrierConnectEnabled" onclick="javascript:SetRequireIntegrationUpdate();" CssClass="jQueryUniform" />
                                    <label class="pr10">Enabled</label>
                                    <asp:Button ID="btnSmcCarrierConnectClear" runat="server" Text="CLEAR FILE" OnClick="OnSmcCarrierConnectClearClicked" />
                                    <label class="w100p">
                                        Current File:
                                        <span class="blue">
                                            <asp:LinkButton ID="lbtnDownloadSmcCarrierConnect" runat="server" OnClick="OnDownloadSmcCarrierConnectClicked" />
                                        </span>
                                        <eShip:CustomHiddenField runat="server" ID="hidSmcCarrierConnect" />
                                    </label>
                                </li>
                                <li>
                                    <label class="wlabel">FedEx Small Pack Parameter File</label>
                                    <asp:FileUpload ID="fupFedExSmallPack" runat="server" CssClass="jQueryUniform" />
                                    <asp:CheckBox runat="server" ID="chkFedExSmallPackEnabled" onclick="javascript:SetRequireIntegrationUpdate();" CssClass="jQueryUniform" />
                                    <label class="pr10">Enabled</label>
                                    <asp:Button ID="btnFedExSmallPack" runat="server" Text="CLEAR FILE" OnClick="OnFedExSmallPackClearClicked" />
                                    <label class="w100p">
                                        Current File:
                                        <span class="blue">
                                            <asp:LinkButton ID="lbtnDownloadFedExSmallPack" runat="server" OnClick="OnDownloadFedExSmallPackClicked" />
                                        </span>
                                    </label>
                                    <eShip:CustomHiddenField runat="server" ID="hidFedExSmallPack" />
                                </li>
                                <li>
                                    <label class="wlabel">UPS Small Pack Parameter File</label>
                                    <asp:FileUpload ID="fupUpsSmallPack" runat="server" CssClass="jQueryUniform" />
                                    <asp:CheckBox runat="server" ID="chkUpsSmallPackEnabled" onclick="javascript:SetRequireIntegrationUpdate();" CssClass="jQueryUniform" />
                                    <label class="pr10">Enabled</label>
                                    <asp:Button ID="btnUpsSmallPack" runat="server" Text="CLEAR FILE" OnClick="OnUpsSmallPackClearClicked" />
                                    <label class="w100p">
                                        Current File:
                                        <span class="blue">
                                            <asp:LinkButton ID="lbtnDownloadUpsSmallPack" runat="server" OnClick="OnDownloadUpsSmallPackClicked" />
                                        </span>
                                    </label>
                                    <eShip:CustomHiddenField runat="server" ID="hidUpsSmallPack" />
                                </li>
                                <li>
                                    <label class="wlabel">Macro Point Parameter File</label>
                                    <asp:FileUpload ID="fupMacroPoint" runat="server" CssClass="jQueryUniform" />
                                    <asp:CheckBox runat="server" ID="chkMacroPointEnabled" onclick="javascript:SetRequireIntegrationUpdate();" CssClass="jQueryUniform" />
                                    <label class="pr10">Enabled</label>
                                    <asp:Button ID="btnMacroPointClear" runat="server" Text="CLEAR FILE" OnClick="OnMacroPointClearClicked" />
                                    <label class="w100p">
                                        Current File:
                                        <span class="blue">
                                            <asp:LinkButton ID="lbtnDownloadMacroPoint" runat="server" OnClick="OnDownloadMacroPointClicked" />
                                        </span>
                                    </label>
                                    <eShip:CustomHiddenField runat="server" ID="hidMacroPoint" />
                                </li>
                            </ul>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabSeeds" HeaderText="Seeds">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlSeeds" Enabled="false">
                        <asp:ListView ID="lstSeeds" runat="server" ItemPlaceholderID="itemPlaceHolder">
                            <LayoutTemplate>
                                <table class="stripe">
                                    <tr>
                                        <th style="width: 35%;">Name</th>
                                        <th style="width: 65%;">Value</th>
                                    </tr>
                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                </table>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="text-left">
                                        <eShip:CustomHiddenField runat="server" ID="hidAutoNumberCode" Value='<%# Eval("Code")%>' />
                                        <%# Eval("Code").FormattedString()%>
                                    </td>
                                    <td class="text-left">
                                        <asp:Literal runat="server" ID="litSeedValue" Text='<%# Eval("NextNumber")%>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel ID="tabAutoRatingItems" runat="server" HeaderText="Auto Rating">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlAutoRatingDetails">

                        <div class="rowgroup">
                            <div class="row">
                                <label class="wlabel">Terms &amp; Conditions File</label>
                                <asp:FileUpload ID="fupTermsAndConditions" runat="server" CssClass="jQueryUniform" />
                                <asp:Button ID="btnClearTermsAndConditions" runat="server" Text="CLEAR FILE" OnClick="OnClearTermsAndConditionsClicked" CssClass="pl10" />
                                <label class="w100p">
                                    Current File:
                                    <span class="blue">
                                        <asp:LinkButton ID="lBtnDownloadTermsAndConditions" runat="server" OnClick="OnDownloadTermsAndConditionsClicked" />
                                    </span>
                                </label>
                                <eShip:CustomHiddenField runat="server" ID="hidTermsAndConditions" />
                            </div>
                            <div class="row pt20">
                                <label class="wlabel">Auto Rate Notice Text</label>
                                <telerik:RadEditor runat="server" Skin="Simple" ID="txtAutoRateNoticeText" Width="500px" Height="350px"
                                    ToolsFile="~/Telerik/Tools/AjaxImitationTools.xml" />
                                <asp:Literal runat="server" ID="litAutoRateNoticeText" />
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel ID="tabRatingNotice" runat="server" HeaderText="Rating Notice">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlRatingDetails">
                        <div class="rowgroup">
                            <div class="row">
                                <label class="wlabel">Rate Notice Text</label>
                                <telerik:RadEditor runat="server" Skin="Simple" ID="txtRateNoticeText" Width="500px" Height="350px"
                                    ToolsFile="~/Telerik/Tools/AjaxImitationTools.xml" />
                                <asp:Literal runat="server" ID="litRateNoticeText" />
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabAuditLog" HeaderText="Audit Log">
                <ContentTemplate>
                    <eShip:AuditLogControl ID="auditLogs" runat="server" />
                </ContentTemplate>
            </ajax:TabPanel>
        </ajax:TabContainer>

    </div>

    <script type="text/javascript">
        function ClearHidFlag() {
            jsHelper.SetEmpty('<%= hidFlag.ClientID%>');
        }
    </script>

    <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
        Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" />
    <eShip:UserFinderControl ID="userFinder" runat="server" Visible="false" EnableMultiSelection="false" FilterForEmployees="False"
        OpenForEditEnabled="False" OnItemSelected="OnUserFinderItemSelected" OnSelectionCancel="OnUserFinderSelectionCancelled" />

    <eShip:CustomHiddenField runat="server" ID="hidShowEdit" Value="false" />
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
    <eShip:CustomHiddenField runat="server" ID="hidFilesToDelete" />
    <eShip:CustomHiddenField runat="server" ID="hidRequireIntergrationUpdate" />
</asp:Content>
