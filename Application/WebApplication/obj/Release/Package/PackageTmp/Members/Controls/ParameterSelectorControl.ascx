﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ParameterSelectorControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Controls.ParameterSelectorControl" %>
<%@ Register TagPrefix="pc" TagName="AltUniformCheckBox" Src="~/Members/Controls/AltUniformCheckBox.ascx" %>
<asp:Panel runat="server" ID="pnlParameterSelector" Visible="false" CssClass="popupControlOver popupControlOverW500">
    <div class="popheader">
        <h4>Add Search Parameter</h4>
    </div>
    <table class="poptable">
        <tr>
            <td colspan="2" class="p_m0">
                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheParameterSelectorPermissionsTable" TableId="parameterSelectorTable" ScrollerContainerId="parameterSelectorScrollSection"
                    ScrollOffsetControlId="pnlParameterSelector" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2050" />
                <div class="finderScrollFixed" id="parameterSelectorScrollSection">
                    <table id="parameterSelectorTable" class="stripe sm_chk">
                        <tr>
                            <th style="width: 10%;">
                                <pc:AltUniformCheckBox ID="chkParameterSelectorSelectAll" runat="server" OnClientSideClicked="javascript:SetSelectedCheckBoxesStatus(this, 'parameterSelectorTable');" />
                            </th>
                            <th class="text-left">Name
                            </th>
                        </tr>
                        <asp:ListView runat="server" ID="lstAvailableParameters" ItemPlaceholderID="itemPlaceHolder">
                            <LayoutTemplate>
                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                         <pc:AltUniformCheckBox ID="chkSelection" runat="server" OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'parameterSelectorTable', 'chkParameterSelectorSelectAll');" />
                                    </td>
                                    <td class="text-left">
                                        <eShip:CustomHiddenField runat="server" ID="hidParameterDataType" Value='<%# Eval("DataType") %>' />
                                        <eShip:CustomHiddenField runat="server" ID="hidParameterReadOnly" Value='<%# Eval("ReadOnly") %>' />
                                        <eShip:CustomHiddenField runat="server" ID="hidParameterOperator" Value='<%# Eval("Operator") %>' />
                                        <asp:Literal runat="server" ID="litParameterName" Text='<%# Eval("Name") %>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td class="text-right">
                <asp:Button ID="btnParameterAddDone" Text="Done" OnClick="OnParameterAddDoneClicked"
                    runat="server" CausesValidation="false" />
                <asp:Button ID="btnParameterAddClose" Text="Cancel" OnClick="OnParameterAddCloseClicked"
                    runat="server" CausesValidation="false" />
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="pnlParameterSelectorDimScreen" CssClass="dimBackgroundControlOver" runat="server" Visible="false" />
