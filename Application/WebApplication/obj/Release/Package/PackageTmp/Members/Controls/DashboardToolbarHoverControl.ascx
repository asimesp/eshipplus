﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DashboardToolbarHoverControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Controls.DashboardToolbarHoverControl" %>
<%@ Register TagPrefix="uc1" TagName="DashboardHoverItem" Src="~/Members/Controls/DashboardHoverItemControl.ascx" %>

<script type="text/javascript">
    $(function () {
        $('#hoverContainer').on('mouseenter', function () {
            if (($("#dashboardHoverMenu").height() + jsHelper.PageHeaderHeight) >= $(window).height()) {
                $("#dashboardHoverMenu").css('overflow-y', 'auto');
                $("#dashboardHoverMenu").css('max-height', ($(window).height() - jsHelper.PageHeaderHeight) + 'px');
            } else {
                $("#dashboardHoverMenu").css('overflow-y', '');
                $("#dashboardHoverMenu").css('max-height', '');
            }
        });
    })
</script>

<div id="hoverContainer" class="hoverContainer clearfix" style="display: block;">
    <div id="dashboardHoverMenu" class="clearfix">
        <asp:ListView runat="server" ID="lstOperations" ItemPlaceholderID="itemPlaceHolder">
            <LayoutTemplate>
                <div class="column2">
                    <h3 class="menu_operations">OPERATIONS</h3>
                    <ul>
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </ul>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <li>
                    <uc1:DashboardHoverItem ID="items" runat="server" IconName='<%# Eval("DisplayName") %>' OpenInNewWindow='<%# Eval("OpenInNewWindow") %>'
                        ViewCode='<%# Eval("ViewCode") %>' IconUrl='<%# Eval("IconUrl") %>' IconImageLocation='<%# Eval("IconImageLocation") %>' />
                </li>
            </ItemTemplate>
        </asp:ListView>
        <asp:ListView runat="server" ID="lstFinance" ItemPlaceholderID="itemPlaceHolder">
            <LayoutTemplate>
                <div class="column2">
                    <h3 class="menu_finance">FINANCE</h3>
                    <ul>
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </ul>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <li>
                    <uc1:DashboardHoverItem ID="items" runat="server" IconName='<%# Eval("DisplayName") %>' OpenInNewWindow='<%# Eval("OpenInNewWindow") %>'
                        ViewCode='<%# Eval("ViewCode") %>' IconUrl='<%# Eval("IconUrl") %>' IconImageLocation='<%# Eval("IconImageLocation") %>' />
                </li>
            </ItemTemplate>
        </asp:ListView>
        <asp:ListView runat="server" ID="lstRegistry" ItemPlaceholderID="itemPlaceHolder">
            <LayoutTemplate>
                <div class="column2">
                    <h3 class="menu_registry">REGISTRY
                    </h3>
                    <ul>
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </ul>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <li>
                    <uc1:DashboardHoverItem ID="items" runat="server" IconName='<%# Eval("DisplayName") %>' OpenInNewWindow='<%# Eval("OpenInNewWindow") %>'
                        ViewCode='<%# Eval("ViewCode") %>' IconUrl='<%# Eval("IconUrl") %>' IconImageLocation='<%# Eval("IconImageLocation") %>' />
                </li>
            </ItemTemplate>
        </asp:ListView>

        <asp:Panel CssClass="column2" runat="server" ID="pnlGroup2">
            <asp:ListView runat="server" ID="lstRating" ItemPlaceholderID="itemPlaceHolder">
                <LayoutTemplate>
                    <h3 class="menu_rating">RATING
                    </h3>
                    <ul>
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </ul>
                </LayoutTemplate>
                <ItemTemplate>
                    <li>
                        <uc1:DashboardHoverItem ID="items" runat="server" IconName='<%# Eval("DisplayName") %>' OpenInNewWindow='<%# Eval("OpenInNewWindow") %>'
                            ViewCode='<%# Eval("ViewCode") %>' IconUrl='<%# Eval("IconUrl") %>' IconImageLocation='<%# Eval("IconImageLocation") %>' />
                    </li>
                </ItemTemplate>
            </asp:ListView>
            <asp:ListView runat="server" ID="lstUtilities" ItemPlaceholderID="itemPlaceHolder">
                <LayoutTemplate>
                    <h3 class="menu_util">UTILITIES
                    </h3>
                    <ul>
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </ul>
                </LayoutTemplate>
                <ItemTemplate>
                    <li>
                        <uc1:DashboardHoverItem ID="items" runat="server" IconName='<%# Eval("DisplayName") %>' OpenInNewWindow='<%# Eval("OpenInNewWindow") %>'
                            ViewCode='<%# Eval("ViewCode") %>' IconUrl='<%# Eval("IconUrl") %>' IconImageLocation='<%# Eval("IconImageLocation") %>' />
                    </li>
                </ItemTemplate>
            </asp:ListView>
        </asp:Panel>
        <asp:Panel CssClass="column2" runat="server" ID="pnlGroup1">
            <asp:ListView runat="server" ID="lstBusinessIntelligence" ItemPlaceholderID="itemPlaceHolder">
                <LayoutTemplate>
                    <h3 class="menu_reports">REPORTING
                    </h3>
                    <ul>
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </ul>
                </LayoutTemplate>
                <ItemTemplate>
                    <li>
                        <uc1:DashboardHoverItem ID="items" runat="server" IconName='<%# Eval("DisplayName") %>' OpenInNewWindow='<%# Eval("OpenInNewWindow") %>'
                            ViewCode='<%# Eval("ViewCode") %>' IconUrl='<%# Eval("IconUrl") %>' IconImageLocation='<%# Eval("IconImageLocation") %>' />
                    </li>
                </ItemTemplate>
            </asp:ListView>
            <asp:ListView runat="server" ID="lstConnect" ItemPlaceholderID="itemPlaceHolder">
                <LayoutTemplate>
                    <h3 class="menu_connect	">CONNECT
                    </h3>
                    <ul>
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </ul>
                </LayoutTemplate>
                <ItemTemplate>
                    <li>
                        <uc1:DashboardHoverItem ID="items" runat="server" IconName='<%# Eval("DisplayName") %>' OpenInNewWindow='<%# Eval("OpenInNewWindow") %>'
                            ViewCode='<%# Eval("ViewCode") %>' IconUrl='<%# Eval("IconUrl") %>' IconImageLocation='<%# Eval("IconImageLocation") %>' />
                    </li>
                </ItemTemplate>
            </asp:ListView>
            <asp:ListView runat="server" ID="lstAdministration" ItemPlaceholderID="itemPlaceHolder">
                <LayoutTemplate>
                    <h3 class="menu_admin">ADMIN
                    </h3>
                    <ul>
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </ul>
                </LayoutTemplate>
                <ItemTemplate>
                    <li>
                        <uc1:DashboardHoverItem ID="items" runat="server" IconName='<%# Eval("DisplayName") %>' OpenInNewWindow='<%# Eval("OpenInNewWindow") %>'
                            ViewCode='<%# Eval("ViewCode") %>' IconUrl='<%# Eval("IconUrl") %>' IconImageLocation='<%# Eval("IconImageLocation") %>' />
                    </li>
                </ItemTemplate>
            </asp:ListView>
        </asp:Panel>
    </div>
</div>
