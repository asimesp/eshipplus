﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DeveloperAccessRequestFinderControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Controls.DeveloperAccessRequestFinderControl" %>
<%@ Register TagPrefix="pc" TagName="UserSearchProfileControl" Src="~/Members/Controls/UserSearchProfileControl.ascx" %>
<%@ Register TagPrefix="pc" TagName="MessageBoxControl" Src="~/Members/Controls/MessageBoxControl.ascx" %>
<%@ Register TagPrefix="pc" TagName="ParameterSelectorControl" Src="~/Members/Controls/ParameterSelectorControl.ascx" %>
<%@ Register TagPrefix="pc" TagName="ContinuousScrollExtender" Src="~/Members/Controls/UpdatePanelContinuousScrollExtender.ascx" %>
<asp:Panel runat="server" ID="pnlDeveloperAccessRequestFinderContent" CssClass="popupControl" DefaultButton="btnSearch">
    <div class="popheader">
        <h4>Developer Access Request Finder <small class="ml10">
            <asp:Literal runat="server" ID="litRecordCount" Text="" /></small>
            <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/icons2/close.png" CssClass="close"
                CausesValidation="false" OnClick="OnCancelClicked" runat="server" /></h4>
    </div>
    <script type="text/javascript">
        $(function () {
            if (jsHelper.IsTrue($('#<%= hidAreFiltersShowing.ClientID%>').val())) {
                $('#<%= pnlSearch.ClientID%>').show();
                $('#<%= pnlSearchShowFilters.ClientID%>').hide();
            } else {
                $('#<%= pnlSearch.ClientID%>').hide();
                $('#<%= pnlSearchShowFilters.ClientID%>').show();
            }
        });

        function ToggleFilters() {
            $('#<%= hidAreFiltersShowing.ClientID%>').val(jsHelper.IsTrue($('#<%= hidAreFiltersShowing.ClientID%>').val()) ? false : true);
            $('#<%= pnlSearch.ClientID%>').toggle();
            $('#<%= pnlSearchShowFilters.ClientID%>').toggle();
            var top = $('#<%= pnlDeveloperAccessRequestFinderContent.ClientID%> div[class="finderScroll"]').offset().top - $('#<%= pnlDeveloperAccessRequestFinderContent.ClientID%>').offset().top - 2;
            $('#<%= pnlDeveloperAccessRequestFinderContent.ClientID%> div[class="finderScroll"] > div[id^="hd"]').css('top', top + 'px');
        }
    </script>
    <table id="finderTable" class="poptable">
        <tr>
            <td>
                <asp:Panel runat="server" ID="pnlSearchShowFilters" class="rowgroup mb0" style="display:none;">
                    <div class="row">
                        <div class="fieldgroup right">
                            <button onclick="ToggleFilters(); return false;">Show Filters</button>
                        </div>
                        <div class="fieldgroup right mb5 mt5 mr10">
                            <span class="fs75em  ">*** If you would like to show parameters when searching change the 'Always Show Finder Parameters On Search' option on your <asp:HyperLink ID="hypGoToUserProfile" Target="_blank" CssClass="blue" runat="server" Text="User Profile"/></span>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
                    <div class="mt5">
                        <div class="rowgroup mb0">
                            <div class="row">
                                <div class="fieldgroup">
                                    <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                                        CausesValidation="False" CssClass="add" />
                                </div>
                                <div class="fieldgroup right">
                                    <button onclick="ToggleFilters(); return false;">Hide Filters</button>
                                </div>
                                <div class="right">
                                    <pc:UserSearchProfileControl ID="searchProfiles" runat="server" Type="DeveloperAccessRequests" ShowAutoRefresh="False"
                                        OnProcessingError="OnSearchProfilesProcessingError" OnSearchProfileSelected="OnSearchProfilesProfileSelected" />
                                </div>
                            </div>
                        </div>

                        <hr class="mb5" />
                        <script type="text/javascript">$(window).load(function () {SetControlFocus();});</script><table class="mb5" id="parametersTable">
                            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                                OnItemDataBound="OnParameterItemDataBound">
                                <LayoutTemplate>
                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                                        OnRemoveParameter="OnParameterRemove" />
                                </ItemTemplate>
                            </asp:ListView>
                            <tr>
                                <td colspan="4">
                                    <asp:Button ID="btnSearch" CssClass="left" runat="server" Text="SEARCH" OnClick="OnSearchClicked"
                                        CausesValidation="False" />
                                    <asp:Button ID="btnCancel" OnClick="OnCancelClicked" runat="server" Text="CANCEL" CssClass="ml10"
                                        CausesValidation="false" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel runat="server" ID="upDataUpdate">
        <ContentTemplate>
            <pc:ContinuousScrollExtender ID="cseDataUpdate" runat="server" BindableControlId="lstSearchResults" UpdatePanelToExtendId="upDataUpdate" ScrollableDivId="groupFinderScrollSection" />
            <eShip:TableFreezeHeaderExtender runat="server" ID="tfheDevAccessRequestTable" TableId="devAccessRequestTable" ScrollerContainerId="devAccessRequestFinderScrollSection"
                ScrollOffsetControlId="pnlDeveloperAccessRequestFinderContent" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2050" />
            <div class="finderScroll" id="devAccessRequestFinderScrollSection">
                <asp:ListView ID="lstSearchResults" runat="server" ItemPlaceholderID="itemPlaceHolder">
                    <LayoutTemplate>
                        <table id="devAccessRequestTable" class="stripe sm_chk">
                            <tr>
                                <th style="width: 20%">&nbsp;</th>
                                <th style="width: 25%;">Customer</th>
                                <th style="width: 25%;">Contact Name</th>
                                <th style="width: 30%;">Contact Email</th>
                                <th style="width: 5%;">Date Created</th>
                            </tr>
                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:Button ID="btnSelect" runat="server" Text="Select" OnClick="OnSelectClicked"
                                    CausesValidation="false" />
                                <asp:Button ID="btnEditSelect" runat="server" Text="Edit" OnClick="OnEditSelectClicked"
                                    CausesValidation="false" Visible='<%# OpenForEditEnabled %>' />
                                <eShip:CustomHiddenField ID="hidRequestId" Value='<%# Eval("Id") %>' runat="server" />
                            </td>
                            <td>
                                <%# Eval("Customer") %></td>
                            <td>
                                <%# Eval("ContactName")%></td>
                            <td>
                                <%# Eval("ContactEmail") %></td>
                            <td>
                                <%# Eval("DateCreated") %></td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lstSearchResults" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Panel>
<pc:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
<asp:Panel ID="pnlFinderDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
<pc:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
<asp:HiddenField runat="server" ID="hidEditSelected" />
<asp:HiddenField runat="server" ID="hidOpenForEditEnabled" />
<eShip:CustomHiddenField runat="server" ID="hidAreFiltersShowing" Value="true" />