﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContinuousScrollExtender.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Controls.ContinuousScrollExtender" %>



<script type="text/javascript">
    $(document).ready(function () {


        if (jsHelper.IsNullOrEmpty('<%= ScrollableDivId %>')) BindWindowScroll();
        else BindDivScroll();

        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {

            if (!jsHelper.IsNullOrEmpty('<%= ScrollableDivId %>')) {
                BindDivScroll();
                ResetDivScrollTop();
            }

            $('#updateDivProcessing<%= ClientID %>').fadeOut();
        });



        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function () {
            if (Sys.WebForms.PageRequestManager.getInstance().get_isInAsyncPostBack() && !jsHelper.IsNullOrEmpty('<%= ScrollableDivId %>')) {
                $(jsHelper.AddHashTag('<%= ScrollableDivId %> input:checkbox')).each(function () {
                    SetAltUniformCheckBoxClickedStatus(this);
                });

                // uncheck select all button. NOTE: usually has 'All' in name!
                $(jsHelper.AddHashTag('<%= ScrollableDivId %> input:checkbox[id*="All"]')).each(function () {
                    this.checked = false;
                    SetAltUniformCheckBoxClickedStatus(this);
                });
            }

        });
    });

    function BindWindowScroll() {
        $(window).scroll(function () {
            if ($(document).height() <= $(window).scrollTop() + $(window).height() && !jsHelper.IsTrue($(jsHelper.AddHashTag('<%= hidAllRecordsLoaded.ClientID %>')).val())) {
                $('#updateDivProcessing<%= ClientID %>').fadeIn();
                $('#updateDivProcessing<%= ClientID %>').css('top', $(window).height() - $('#updateDivProcessing<%= ClientID %>').height());
                window.__doPostBack('<%= UpdatePanelClientId %>', '<%= ClientID %>');
            }
        });
    }

    function BindDivScroll() {
        $(jsHelper.AddHashTag('<%= ScrollableDivId %>')).scroll(function () {
            if (($(jsHelper.AddHashTag('<%= ScrollableDivId %>')).prop('scrollHeight') - $(jsHelper.AddHashTag('<%= ScrollableDivId %>')).scrollTop() == $(jsHelper.AddHashTag('<%= ScrollableDivId %>')).height()) && (Number('<%= DataSource != null ? DataSource.Cast<object>().Count() : 0 %>') > Number($(jsHelper.AddHashTag('<%= hidCurrentlyLoadedRecords.ClientID %>')).val()))) {
                $('#updateDivProcessing<%= ClientID %>').fadeIn();
                $('#updateDivProcessing<%= ClientID %>').css('top', $(jsHelper.AddHashTag('<%= ScrollableDivId %>')).offset().top + $(jsHelper.AddHashTag('<%= ScrollableDivId %>')).height() - $('#updateDivProcessing<%= ClientID %>').height() - $(window).scrollTop());
                $('#updateDivProcessing<%= ClientID %>').css('left', $(jsHelper.AddHashTag('<%= ScrollableDivId %>')).css('left'));
                $('#updateDivProcessing<%= ClientID %>').css('width', $(jsHelper.AddHashTag('<%= ScrollableDivId %>')).width());
                $(jsHelper.AddHashTag('<%= hidElementScrollTop.ClientID %>')).val($(jsHelper.AddHashTag('<%= ScrollableDivId %>')).scrollTop());
                window.__doPostBack('<%= UpdatePanelClientId %>', '<%= ClientID %>');
            }
        });

    }

    function ResetDivScrollTop() {
        $(jsHelper.AddHashTag('<%= ScrollableDivId %>')).scrollTop(Number(($(jsHelper.AddHashTag('<%= hidElementScrollTop.ClientID %>')).val())));
    }

</script>

<div class="dimBackgroundControlOver" id="updateDivProcessing<%= ClientID %>" style="display: none; cursor: wait; height: 35px !important;">
    <div align="center" style="color: white; font-style: italic; font-weight: bold; font-size: 1.5em; margin: auto; display: block; padding-top: 5px;">
        Loading more records ...
    </div>
</div>

<eShip:CustomHiddenField runat="server" ID="hidElementScrollTop" Value="0" />
<eShip:CustomHiddenField runat="server" ID="hidCurrentlyLoadedRecords" />
<eShip:CustomHiddenField runat="server" ID="hidAllRecordsLoaded" Value="True"/>

<eShip:CustomHiddenField runat="server" ID="hidBindableControlId" />
<eShip:CustomHiddenField runat="server" ID="hidUpdatePanelToExtendControlId" />
<eShip:CustomHiddenField runat="server" ID="hidScrollableDivId" />


<eShip:CustomHiddenField runat="server" ID="hidCheckBoxControlId"/>
<eShip:CustomHiddenField runat="server" ID="hidUniqueRecordRefControlId"/>
<eShip:CustomHiddenField runat="server" ID="hidPreserveRecordSelectionId"/>
