﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SortableExtender.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Controls.SortableExtender" %>
<script type="text/javascript">
    $(document).ready(function() {
        if (jsHelper.IsTrue($(jsHelper.AddHashTag('<%= hidEnabled.ClientID %>')).val())) {
            bindSortableToMilestoneListView();

            $("[id='" + $(jsHelper.AddHashTag('<%= hidDragHandlerId.ClientID %>')).val() + "']").mousedown(function() {
                $(jsHelper.AddHashTag($(jsHelper.AddHashTag('<%= hidSortableElementWrapperId.ClientID %>')).val())).sortable("enable");
            });

            $("[id='" + $(jsHelper.AddHashTag('<%= hidDragHandlerId.ClientID %>')).val() + "']").mouseup(function() {
                $(jsHelper.AddHashTag($(jsHelper.AddHashTag('<%= hidSortableElementWrapperId.ClientID %>')).val())).sortable("disable");
            });
        }

        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function() {
            if (jsHelper.IsTrue($(jsHelper.AddHashTag('<%= hidEnabled.ClientID %>')).val())) {
                bindSortableToMilestoneListView();

                $("[id='" + $(jsHelper.AddHashTag('<%= hidDragHandlerId.ClientID %>')).val() + "']").mousedown(function() {
                    $(jsHelper.AddHashTag($(jsHelper.AddHashTag('<%= hidSortableElementWrapperId.ClientID %>')).val())).sortable("enable");
                });

                $("[id='" + $(jsHelper.AddHashTag('<%= hidDragHandlerId.ClientID %>')).val() + "']").mouseup(function() {
                    $(jsHelper.AddHashTag($(jsHelper.AddHashTag('<%= hidSortableElementWrapperId.ClientID %>')).val())).sortable("disable");
                });
            }
        });

        function bindSortableToMilestoneListView() {
            $(jsHelper.AddHashTag($(jsHelper.AddHashTag('<%= hidSortableElementWrapperId.ClientID %>')).val())).sortable({
                items: "> " + $(jsHelper.AddHashTag('<%= hidSortableElementIdChildGroup.ClientID %>')).val(),
                update: function() {
                    <%= OnUpdateRunFunction %>
                    window.__doPostBack('<%= UpdatePanelClientId %>', $(jsHelper.AddHashTag($(jsHelper.AddHashTag('<%= hidSortableElementWrapperId.ClientID %>')).val())).sortable("toArray"));
                },
                helper: function(e, ui) { //function preserves the widths of objects being dragged
                    ui.children().each(function() {
                        $(this).width($(this).width());
                    });
                    return ui;
                }
            });
            $(jsHelper.AddHashTag($(jsHelper.AddHashTag('<%= hidSortableElementWrapperId.ClientID %>')).val())).sortable("disable");
        }
    });

    
</script>

<eShip:CustomHiddenField runat="server" ID="hidDragHandlerId"/>
<eShip:CustomHiddenField runat="server" ID="hidSortableElementWrapperId"/>
<eShip:CustomHiddenField runat="server" ID="hidEnabled"/>
<eShip:CustomHiddenField runat="server" ID="hidUpdatePanelToExtendControlId"/>  
<eShip:CustomHiddenField runat="server" ID="hidSortableElementIdChildGroup" Value="tr"/>
<eShip:CustomHiddenField runat="server" ID="hidOnUpdateFunction"/>