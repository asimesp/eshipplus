﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DocumentDisplayControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Controls.DocumentDisplayControl" %>
<%@ Register TagPrefix="pc" TagName="EmailItems" Src="~/Members/Controls/EmailItemControl.ascx" %>
<%@ Register TagPrefix="pc" TagName="MessageBox" Src="~/Members/Controls/MessageBoxControl.ascx" %>
<asp:Panel runat="server" ID="pnlDocumentViewer" DefaultButton="ibtnClose">
    <div class="popupControl">
        <div class="popheader">
            <h4>
                <asp:Literal runat="server" ID="litTitle" Text="Document Viewer" />
            </h4>
            <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
        </div>

        <div class="row">
            <table id="documentViewerTable" class="poptable">
                <tr class="header">
                    <th class="text-left">
                        <asp:ImageButton ID="ibtnHtml" runat="server" ImageUrl="~/images/icons/downloadHtml.png"
                            CausesValidation="False" OnClick="OnbtnHtmlClicked" ToolTip="download html" CssClass="middle" Width="25px"/>
                        <asp:ImageButton ID="ibtnPdf" runat="server" ImageUrl="~/images/icons/downloadPdf.png"
                            OnClick="OnbtnPdfClicked" CausesValidation="False" ToolTip="download pdf" CssClass="middle" Width="25px"/>|
                <asp:ImageButton ID="ibtnDownloadHtml" runat="server" ImageUrl="~/images/icons/EmailHtml.png"
                    CausesValidation="False" OnClientClick="" ToolTip="Email html" CssClass="middle" Width="25px"/>
                        <asp:ImageButton ID="ibtnDownloadPdf" runat="server" ImageUrl="~/images/icons/EmailPdf.png"
                            CausesValidation="False" OnClientClick="" ToolTip="Email Pdf" CssClass="middle" Width="25px" />
                        <script type="text/javascript">
                            $(document).ready(function () {
                                $("#<%= ibtnDownloadPdf.ClientID %>").click(function (e) {
                                    e.preventDefault();
                                    $("#<%= pnlEmail.ClientID %>").fadeIn('fast');
                                    $("#<%= hidHtmlPdfEmail.ClientID %>").val('<%= PdfFlag  %>');
                                    $("#<%= lblFormat.ClientID %>").text('( as pdf attachment)');
                                });

                                $("#<%= ibtnDownloadHtml.ClientID %>").click(function (e) {
                                    e.preventDefault();
                                    $("#<%= pnlEmail.ClientID %>").fadeIn('fast');
                                    $("#<%= hidHtmlPdfEmail.ClientID %>").val('<%= HtmlFlag  %>');
                                    $("#<%= lblFormat.ClientID %>").text('(as html in email body)');
                                });
                            });
                        </script>

                    </th>
                </tr>
                <tr>
                    <td class="pb10 pt10 pl10 pr10">
                        <div runat="server" id="contentDiv" class="finderScroll">
                            [content]
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="text-center middle pr10 black">
                        <hr class="dotted mb10" />
                        <a href="http://ardownload.adobe.com/pub/adobe/reader/win/9.x/9.2/enu/AdbeRdr920_en_US.exe"
                            target="_blank">
                            <img id="Img1" runat="server" src="~/images/icons/adobeAcrobatReader.jpg" alt="Adobe Acrobat Reader"
                                style="height: 16px; vertical-align: middle;" />
                        </a><a href="http://get.adobe.com/reader/" target="_blank">Adobe Acrobat Reader</a>
                        required to view some documents. Click image to download if necessary.
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Panel>
<asp:Panel runat="server" ID="pnlEmail" Style="display: none;">
    <div class="popupControl popupControlOverW750">
        <div class="popheader">
            <h4>
                <asp:Literal runat="server" Text="TITLE" ID="litEmailTitle" />
                <asp:Label runat="server" ID="lblFormat" Text="" />
            </h4>
        </div>
        <div class="row">
            <table id="editRegistryTable" class="poptable">
                <pc:EmailItems runat="server" ID="emailItems" Visible="true" Attachments="[Included]" />
                <tr>
                    <td colspan="2" class="text-right pb10">

                        <script type="text/javascript">
                            $(document).ready(function () {
                                $("#<%= btnCancel.ClientID %>").click(function (e) {
                                    e.preventDefault();
                                    $("#<%= pnlEmail.ClientID %>").fadeOut('fast');
                                });
                            });
                        </script>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <asp:Button runat="server" ID="btnSend" Text="SEND" OnClick="OnSendClicked" CausesValidation="False" />
                        <asp:Button runat="server" ID="btnCancel" Text="CANCEL" OnClientClick="" CausesValidation="False" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Panel>
<asp:Panel ID="pnlDocumentDisplayDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
<pc:MessageBox runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
<asp:HiddenField runat="server" ID="hidSaveHtmlWrapper" />
<asp:HiddenField runat="server" ID="hidHtmlPdfEmail" />
