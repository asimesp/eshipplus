﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FooterDetails.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Controls.FooterDetails.FooterDetails" %>
Copyright &copy; 2012 - <%: DateTime.Now.Year %> Logistics Plus, Inc. All rights reserved |
<asp:HyperLink runat="server" ID="hypToC">Terms And Conditions</asp:HyperLink>
| Powered by <a href="http://www.logisticsplus.net" target="_blank">Logistics Plus</a>, Inc | eShipPlus TMS
                <asp:Literal runat="server" ID="litVersion" />