﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmailItemControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Controls.EmailItemControl" %>
<tr>
    <td class="text-right top">
        <label class="upper">To:</label>
    </td>
    <td class="text-left">
        <eShip:CustomTextBox runat="server" ID="txtTo" CssClass="w500" TextMode="MultiLine" Rows="2" />
    </td>
</tr>
<asp:Panel runat="server" ID="pnlCc" Visible="true">
    <tr>
        <td class="text-right">
            <label class="upper">Cc:</label>
        </td>
        <td class="text-left">
            <eShip:CustomTextBox runat="server" ID="txtCc" CssClass="w500" TextMode="MultiLine" Rows="2" />
        </td>
    </tr>
</asp:Panel>
<asp:Panel runat="server" ID="pnlBcc" Visible="true">
    <tr>
        <td class="text-right top">
            <label class="upper">Bcc:</label>
        </td>
        <td class="text-left">
            <eShip:CustomTextBox runat="server" ID="txtBcc" CssClass="w500" TextMode="MultiLine" Rows="2" />
        </td>
    </tr>
</asp:Panel>
<tr>
    <td class="text-right top">
        <label class="upper">Subject:</label>
    </td>
    <td class="text-left">
        <eShip:CustomTextBox runat="server" ID="txtSubject" CssClass="w500"/>
    </td>
</tr>
<tr>
    <td class="text-right top">
        <label class="upper">Body:</label>
    </td>
    <td class="text-left">
        <ajax:Editor runat="server" ID="body" CssClass="w500 h150" />
    </td>
</tr>
<tr>
    <td class="text-right">
        <label class="upper">Attachments:</label>
    </td>
    <td class="text-left">
        <asp:Literal runat="server" ID="litAttachment" />
    </td>
</tr>
