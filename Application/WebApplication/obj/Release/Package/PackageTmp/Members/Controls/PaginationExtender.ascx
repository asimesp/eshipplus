﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaginationExtender.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Controls.PaginationExtender" %>

<script type="text/javascript">
    $(function() {
        jsHelper.Enable('<%= ddlPageNumber.ClientID %>');
        jsHelper.Enable('<%= ibtnNext.ClientID %>');
        jsHelper.Enable('<%= ibtnPrevious.ClientID %>');

        $(jsHelper.AddHashTag('<%= ibtnNext.ClientID %>')).click(function() { ShowProgressNotice(true); });
        $(jsHelper.AddHashTag('<%= ibtnPrevious.ClientID %>')).click(function() { ShowProgressNotice(true); });
        $(jsHelper.AddHashTag('<%= ddlPageNumber.ClientID %>')).change(function() { ShowProgressNotice(true); });

        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function() {

            jsHelper.Enable('<%= ddlPageNumber.ClientID %>');
            jsHelper.Enable('<%= ibtnNext.ClientID %>');
            jsHelper.Enable('<%= ibtnPrevious.ClientID %>');
            
            ShowProgressNotice(false);
            
            $(jsHelper.AddHashTag('<%= ibtnNext.ClientID %>')).click(function () { ShowProgressNotice(true); });
            $(jsHelper.AddHashTag('<%= ibtnPrevious.ClientID %>')).click(function () { ShowProgressNotice(true); });
            $(jsHelper.AddHashTag('<%= ddlPageNumber.ClientID %>')).change(function () { ShowProgressNotice(true); });
        });

    });
</script>


<div class="row middle" id="paginationDiv<%= ClientID %>">
    <div class="left middle">
        <label class="left lhInherit">
            <asp:ImageButton runat="server" ID="ibtnPrevious" AlternateText="Previous Record" ImageUrl="~/images/icons2/arrowLeft.png" CssClass="mr10" CausesValidation="False" OnClick="OnPreviousClicked" Width="22px" />
        </label>
        <label class="left lhInherit">
            Page
            <asp:DropDownList runat="server" ID="ddlPageNumber" CssClass="pl5 pr5 w60" AutoPostBack="True" DataTextField="Text" DataValueField="Value"
                CausesValidation="False" OnSelectedIndexChanged="OnPageNumberSelectedIndexChanged"/>
            of
            <asp:Label runat="server" ID="lbPageCount" CssClass="pl5" Text="n" />
        </label>
        <asp:ImageButton runat="server" ID="ibtnNext" AlternateText="Next Record" ImageUrl="~/images/icons2/arrowRight.png" CssClass="ml10" CausesValidation="False" OnClick="OnNextClicked" Width="22px" />

    </div>
</div>
<eShip:CustomHiddenField runat="server" ID="hidPageSize" />
<eShip:CustomHiddenField runat="server" ID="hidTargetControlId" />
<eShip:CustomHiddenField runat="server" ID="hidUnCheckControlIdOnPageChange" />
<eShip:CustomHiddenField runat="server" ID="hidUseParentPageDataStore" />
<eShip:CustomHiddenField runat="server" ID="hidParentPageDataStoreKey" />
