﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TableFreezeHeaderExtender.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Controls.FreeHeaderExtender.TableFreezeHeaderExtender" %>
<script type="text/javascript">
    $(document).ready(function() {

        jsHelper.SetTheadTbody('<%= TableId %>');
        $(jsHelper.AddHashTag('<%= TableId %>')).freezeHeader(
            {
                ScrollContainerId: '<%= ScrollerContainerId %>',
                ScrollOffsetControlId: '<%= ScrollOffsetControlId %>',
                DoNotFillHeaderBckg: '<%= IgnoreHeaderBackgroundFill %>',
                HeaderZIndex: '<%= HeaderZIndex %>'
            });

        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function() {

            jsHelper.SetTheadTbody('<%= TableId %>');
            $(jsHelper.AddHashTag('<%= TableId %>')).freezeHeader(
                {
                    ScrollContainerId: '<%= ScrollerContainerId %>',
                    ScrollOffsetControlId: '<%= ScrollOffsetControlId %>',
                    DoNotFillHeaderBckg: '<%= IgnoreHeaderBackgroundFill %>',
                    HeaderZIndex: '<%= HeaderZIndex %>'
                });
        });

    });
</script>

<eShip:CustomHiddenField runat="server" ID="hidTableId"/>
<eShip:CustomHiddenField runat="server" ID="hidZindex" Value="auto" />
<eShip:CustomHiddenField runat="server" ID="hidScrollerContainerId"/>
<eShip:CustomHiddenField runat="server" ID="hidScrollOffsetControlId"/>
<eShip:CustomHiddenField runat="server" ID="hidDoNotFillHeaderBackgroup"/>
