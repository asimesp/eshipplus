﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TextAreaMaxLengthExtender.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Controls.TextAreaMaxLengthExtender" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<asp:Label runat="server" ID="lbCharCount" CssClass="textAreaCharCount" >
    <%= string.Format("({0} of {1} char.)", Parent.FindControl(hidTargetControlId.Value) != null ? Parent.FindControl(hidTargetControlId.Value).ToTextBox().Text.Length : 0, hidMaxLength.Value) %>
</asp:Label>
<eShip:CustomHiddenField runat="server" ID="hidTargetControlClientlId" />
<eShip:CustomHiddenField runat="server" ID="hidTargetControlId" />
<eShip:CustomHiddenField runat="server" ID="hidMaxLength" Value="50" />