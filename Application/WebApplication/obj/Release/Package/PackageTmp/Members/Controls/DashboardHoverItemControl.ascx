﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DashboardHoverItemControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Controls.DashboardHoverItemControl" %>

<asp:HyperLink runat="server" ID="hypDashboardItem" NavigateUrl='<%# IconUrl %>' Target=<%# OpenInNewWindow ? "_blank" : "_self" %>>
	<asp:Literal runat="server" ID="litDashboardItem" Text='<%# IconName %>' />
</asp:HyperLink>
