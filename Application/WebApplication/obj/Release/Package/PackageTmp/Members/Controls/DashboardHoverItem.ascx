﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DashboardHoverItem.ascx.cs"
	Inherits="LogisticsPlus.Eship.WebApplication.Members.Controls.DashboardHoverItem" %>
<div style="text-align: left;">
	<asp:HyperLink runat="server" ID="hypDashboardItem" NavigateUrl='<%# IconUrl %>' Target=<%# OpenInNewWindow ? "_blank" : "_self" %>>
		<asp:Image runat="server" Width="12px" ID="imgDashboardItem" ImageUrl='<%# IconImageLocation %>' />
		<asp:Literal runat="server" ID="litDashboardItem" Text='<%# IconName %>' />
	</asp:HyperLink>
</div>
