﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AjaxTabHeaderTextUpdater.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Controls.AjaxTabHeaderTextUpdater" %>
<script type="text/javascript">
    $(function () {
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
            if (!jsHelper.IsNullOrEmpty($('#<%= hidTabPanelIdEndsWith.ClientID %>').val())) {
                $("a[id$='" + $('#<%= hidTabPanelIdEndsWith.ClientID %>').val() + "']").html($('#<%= hidTabPanelHeaderText.ClientID %>').val().replace(/&lt;/g, "<").replace(/&gt;/g, ">"));
                $('#<%= hidTabPanelIdEndsWith.ClientID %>').val(jsHelper.EmptyString);
                $('#<%= hidTabPanelHeaderText.ClientID %>').val(jsHelper.EmptyString);
            }
        });
    })
</script>

<asp:UpdatePanel runat="server" UpdateMode="Always">
    <ContentTemplate>
        <asp:HiddenField runat="server" ID="hidTabPanelIdEndsWith" />
        <asp:HiddenField runat="server" ID="hidTabPanelHeaderText" />
    </ContentTemplate>
</asp:UpdatePanel>
