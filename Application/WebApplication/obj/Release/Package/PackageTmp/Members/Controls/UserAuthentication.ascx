﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserAuthentication.ascx.cs"
    Inherits="LogisticsPlus.Eship.WebApplication.Members.Controls.UserAuthentication" %>

<script type="text/javascript">

    $(document).ready(function () {
        $('#forgot-password').click(function () {
            $('#divPwdLogin').hide();
            $('#divForgotPwd').fadeIn('slow');
        });

        $('#Return-login').click(function () {
            $('#divPwdLogin').fadeIn('slow');
            $('#divForgotPwd').hide();
        });


        $('#closeForgotPassword').click(function () { $('#forgotPasswordPopup').hide(); });

        retrieveCookies();
    });

    function retrieveCookies() {
        var username = jsHelper.RetrieveCookie(jsHelper.UsernameCookie);
        var usernameAutentificationType = jsHelper.RetrieveCookie(jsHelper.UserAuthenticateCookie);
        if (!jsHelper.IsNullOrEmpty(username)) {
            $("#<%= txtUsername.ClientID %>").val(username);
		    $('#remember').prop('checked', true);
		} else {
		    $('#remember').prop('checked', false);
        }
        if (!jsHelper.IsNullOrEmpty(usernameAutentificationType)) {
            $("#<%= ddlAutenticationType.ClientID %>").val(usernameAutentificationType);
        }
    }

    function setCookies() {
        if (jsHelper.IsChecked('remember')) {
            jsHelper.ClearCookie(jsHelper.UsernameCookie);
            var username = $("#<%= txtUsername.ClientID %>").val();
			jsHelper.SetCookie(jsHelper.UsernameCookie, username, jsHelper.CookieDaysToLive);
        } else {
            jsHelper.ClearCookie(jsHelper.UsernameCookie);
        }
        
        var usernameAutentificationType = $("#<%= ddlAutenticationType.ClientID %>").val();
        jsHelper.SetCookie(jsHelper.UserAuthenticateCookie, usernameAutentificationType, jsHelper.CookieDaysToLive);
    }
</script>

<div id="login_wrapper">
    <div id="login_box" class="clearfix;">
        <asp:Panel runat="server" ID="pnlLogo">
            <div id="login_heading">
                <h1>eShip Plus</h1>
                <h2 style="font-size: 16px;">
                    <a href="http://www.logisticsplus.net/" class="link_nounderline"><span class="blue">Logistics Plus<sup>&reg;</sup></span></a> Transportation Management System
                </h2>
                <br />
                &nbsp;
            </div>
        </asp:Panel>
        <div id="login_form">
            <!-- login panel -->
            <asp:Panel runat="server" ID="pnlLogin">
                <asp:HiddenField runat="server" ID="hidAccessCode" Value="TENANT1" />
                <eShip:CustomTextBox runat="server" ID="txtUsername" CssClass="username" placeholder="User Name"
                    autofocus="autofocus" />
                <!-- password entry for login -->
                <div id="divPwdLogin" style="display: block;">
                    <eShip:CustomTextBox runat="server" ID="txtPassword" CssClass="password" placeholder="Password"
                        TextMode="Password" />
                    <asp:DropDownList ID ="ddlAutenticationType" runat="server" DataValueField="Key" DataTextField="Value" CssClass="w240 mb20" />
                    <label class="biglabel">
                        User Login</label>
                    <asp:Button runat="server" ID="btnLogin" Text="Login" OnClientClick="setCookies();"
                        OnClick="OnLoginClicked" CausesValidation="False"/>
                    <p id="ForgotPassword-RememberMe" class="middle">
                        <a id="forgot-password" class="blue">Forgot Password</a> |
						<span class="blue">Remember Me
                        </span>
                        <input type="checkbox" id="remember" name="remember" class="jQueryUniform" />
                    </p>
                </div>
                <!-- forgot password retrieval -->
                <div id="divForgotPwd" style="display: none;">
                    <label class="biglabel">
                        Password</label>
                    <asp:Button runat="server" ID="btnRetrievePassword" Text="Retrieve" OnClick="OnRetrievePasswordClicked" CausesValidation="False"/>
                    <p class="blue">
                        <a id="Return-login" class="blue">Return to Login</a>
                    </p>
                </div>
            </asp:Panel>
            <!-- password reset panel -->
            <asp:Panel runat="server" ID="pnlPwdReset" Visible="False">
                <eShip:CustomTextBox runat="server" ID="txtNewPassword" CssClass="password" placeholder="New Password"
                    TextMode="Password" />
                <eShip:CustomTextBox runat="server" ID="txtConfirmPassword" CssClass="password" placeholder="Confirm Password"
                    TextMode="Password" />
                <asp:Button runat="server" ID="btnResetPassword" Text="Reset" OnClick="OnOkayResetPasswordClicked" CausesValidation="False"/>
                <asp:Button runat="server" ID="btnCancelResetPassword" Text="Cancel" OnClick="OnCancelResetPasswordClicked" CausesValidation="False"/>
            </asp:Panel>
            <!-- login/error/information messages -->
            <asp:Panel runat="server" ID="pnlErrors" Visible="False" CssClass="errorMsg">
                <asp:Literal runat="server" ID="litMessages" Text="Message!" />
            </asp:Panel>
        </div>
    </div>
</div>
<asp:Panel runat="server" ID="pnlFooter">
    <footer id="login_footer">
        <p>
            <eShip:FooterDetails ID="fdDetails" runat="server" />
        </p>
    </footer>
</asp:Panel>
