﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FileUploaderControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Controls.FileUploaderControl" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<asp:Panel runat="server" ID="pnlFileUpload">

    <script type="text/javascript">

        function CheckFileSize(fileControl) {

            $(jsHelper.AddHashTag('<%= btnUpload.ClientID %>')).show();

            if (fileControl.files.length == 0) return;

            var maxSize = '<%= WebApplicationSettings.SystemFileUploadMaxSize %>';
            var mb = '<%= WebApplicationConstants.MegaByte %>';

            if (fileControl.files[0].size <= maxSize) return;

            // alert and disable upload button
            alert(fileControl.files[0].name + " is too large. Maximum file size is: " + maxSize + " bytes [or " + maxSize / mb + " MB]");
            $(jsHelper.AddHashTag('<%= btnUpload.ClientID %>')).hide();
            fileControl.value = jsHelper.EmptyString;
        }

        function ClearFileUpload() {
            $(jsHelper.AddHashTag('<%= fupFileUpload.ClientID%>')).value = jsHelper.EmptyString;
        }

        $(window).load(function () {
            $('tbody.UploadPart').show();
            $('tbody.LoadingPart').hide();
        });

        function ShowDontTouchDivBlock() {
            $('tbody.UploadPart').fadeOut(2000, function() {
                $('tbody.LoadingPart').fadeIn(2000);
            });
            
        }
    </script>
    <div class="popupControl popupControlOverW500">
        <table class="poptable">
            <tr>
                <td class="p_m0">
                    <div class="popheader">
                        <h4>
                            <asp:Literal runat="server" ID="litTitle" />
                        </h4>
                        <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close" CausesValidation="false"
                            OnClick="OnCancelClicked" runat="server" />
                    </div>
                </td>
            </tr>
            <tbody class="UploadPart">
                <tr class="header">
                    <td>
                        <asp:Literal runat="server" ID="litInstructions" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="upper">INPUT FILE:</label>
                        <asp:FileUpload runat="server" CssClass="jQueryUniform" ID="fupFileUpload" onchange="CheckFileSize(this);" />
                    </td>
                </tr>
                <tr class="right">
                    <td>
                        <asp:Button runat="server" ID="btnUpload" Text="UPLOAD" OnClick="OnUploadClicked" CausesValidation="false" OnClientClick="ShowDontTouchDivBlock();" />
                        <asp:Button runat="server" ID="btnCancel" Text="CANCEL" OnClick="OnCancelClicked" CausesValidation="false" OnClientClick="ClearFileUpload();" />
                    </td>
                </tr>
            </tbody>
            <tbody class="LoadingPart" style="display: none; cursor: wait;">
                <tr>
                    <td class="text-center middle">
                        <asp:Image ID="imgWarning" ImageUrl="~/images/icons/messageIcons/warning.png" runat="server" CssClass="left"  Width="32"  Height="32"/>
                        <h4>This may take a long time, please don't refresh the page!</h4>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</asp:Panel>
<asp:Panel ID="pnlFileUploaderDimScreen" CssClass="dimBackgroundControl" runat="server" Visible="false" />
