﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SimpleAddressInputControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Controls.SimpleAddressInputControl" %>
<div class="row">
    <div class="fieldgroup">
        <label class="wlabel">Street Line 1</label>
        <eShip:CustomTextBox ID="txtStreet1" runat="server" MaxLength="50" CssClass="w200" />
    </div>
</div>
<div class="row">
    <div class="fieldgroup">
        <label class="wlabel">Street Line 2</label>
        <eShip:CustomTextBox ID="txtStreet2" runat="server" MaxLength="50" CssClass="w200" />
    </div>
</div>
<div class="row">
    <div class="fieldgroup">
        <label class="wlabel">City</label>
        <eShip:CustomTextBox ID="txtCity" runat="server" MaxLength="50" CssClass="w200" />
    </div>
</div>
<div class="row">
    <div class="fieldgroup">
        <label class="wlabel">State</label>
        <eShip:CustomTextBox ID="txtState" runat="server" MaxLength="50" CssClass="w200" />
    </div>
</div>
<div class="row">
    <div class="fieldgroup">
        <label class="wlabel">Postal Code</label>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#<%= txtPostalCode.ClientID %>').change(function () {
                GetCityAndState();
            });

            $(jsHelper.AddHashTag('<%= ddlCountries.ClientID %>')).change(function () {
                GetCityAndState();
            });

            function GetCityAndState() {
                var ids = new ControlIds();
                ids.City = $('#<%= txtCity.ClientID %>').attr('id');
                ids.State = $('#<%= txtState.ClientID %>').attr('id');
                FindCityAndState(
                    $('#<%= txtPostalCode.ClientID %>').val(),
						$('#<%= ddlCountries.ClientID %>').val(),
						ids
					);
                }
        });
        </script>

        <eShip:CustomTextBox ID="txtPostalCode" runat="server" MaxLength="50" CssClass="w200" />
    </div>
</div>
<div class="row">
    <div class="fieldgroup">
        <label class="wlabel">Country</label>
        <eShip:CachedObjectDropDownList Type="Countries" ID="ddlCountries" runat="server" CssClass="w200" EnableChooseOne="True" DefaultValue="0" />
    </div>
</div>
