﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="VendorProMassUpdateView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.VendorProMassUpdateView" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" howDashboard="true" ShowNew="false"
        ShowEdit="false" ShowDelete="false" ShowSave="false" ShowFind="false" ShowImport="true"
        ShowExport="false" OnImport="OnImportShipmentToUpdate" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Vendor Pro Mass Updates
            </h3>
            <div class="right">
                <eShip:AltUniformCheckBox runat="server" ID="chkOveride" />
                <label>Override Current Values</label>
            </div>
        </div>

        <hr class="fat" />

        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheVendorProMassTable" TableId="vendorProMassTable" HeaderZIndex="2"/>
        <asp:ListView ID="lstUpdatedRecords" runat="server" ItemPlaceholderID="itemPlaceHolder">
            <LayoutTemplate>
                <h5 >Vendor Pro Updates</h5>
                <table id="vendorProMassTable" class="line2 pl2">
                    <tr>
                        <th style="width: 10%;">Shipment Number
                        </th>
                        <th style="width: 5%;">Vendor SCAC
                        </th>
                        <th style="width: 9%;">Vendor Number
                        </th>
                        <th style="width: 10%;">Vendor PRO
                        </th>
                        <th style="width: 8%;">Pickup
                        </th>
                        <th style="width: 8%;">Del.
                        </th>
                        <th style="width: 50%;">Message
                        </th>
                    </tr>
                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr class="top">
                    <td>
                        <%# Eval("ShipmentNumber") %>
                    </td>
                    <td>
                        <%# Eval("VendorSCAC") %>
                    </td>
                    <td>
                        <%# Eval("VendorNumber") %>
                    </td>
                    <td>
                        <%# Eval("VendorPRO") %>
                    </td>
                    <td>
                        <%# Eval("Pickup") %>
                    </td>
                    <td>
                        <%# Eval("Delivery") %>
                    </td>
                    <td>
                        <%# Eval("Message") %>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
    </div>
    <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
        OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information"
        OnOkay="OnOkayProcess" />

</asp:Content>
