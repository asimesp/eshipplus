﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="XmlConnectRecordView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Connect.XmlConnectRecordView"
    EnableEventValidation="false" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.Connect" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>
<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" />
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:UpdatePanel runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
                        Xml Connect Record Listing<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" Text="" /></small>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </h3>
        </div>

        <hr class="dark mb10" />

        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="XmlConnectRecords" ShowAutoRefresh="True"
                            OnProcessingError="OnSearchProfilesProcessingError" OnSearchProfileSelected="OnSearchProfilesProfileSelected"
                            OnAutoRefreshChanged="OnSearchProfilesAutoRefreshChanged" />
                    </div>
                </div>
            </div>

            <hr class="dark mb10" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                        OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>

            <hr class="fat" />
            <div class="row mb10">
                <div class="fieldgroup">
                    <asp:Button ID="btnSearch" CssClass="left" runat="server" Text="SEARCH" OnClick="OnSearchClicked"
                        CausesValidation="False" />
                </div>
                <div class="right">
                    <label>Sort By:</label>
                    <asp:DropDownList runat="server" ID="ddlSortBy" DataTextField="DisplayName" DataValueField="Name"
                        OnSelectedIndexChanged="OnSortBySelectedIndexChanged" AutoPostBack="True" CssClass="mr10" />
                    <asp:RadioButton runat="server" ID="rbAsc" Text="ASC" GroupName="rbSortBy" AutoPostBack="True"
                        ToolTip="Ascending" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />&nbsp;
                    <asp:RadioButton runat="server" ID="rbDesc" Text="DESC" GroupName="rbSortBy" Checked="True"
                        ToolTip="Descending" AutoPostBack="True" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                </div>
            </div>
        </asp:Panel>

        <asp:UpdatePanel runat="server" ID="upDataUpdate">
            <ContentTemplate>
                <eShip:UpdatePanelContinuousScrollExtender ID="upcseDataUpdate" runat="server" BindableControlId="rptXmlConnectRecords" UpdatePanelToExtendId="upDataUpdate" />
                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheViewRegistryTable" TableId="viewRegistryTable" HeaderZIndex="2" />
                <asp:Timer runat="server" ID="tmRefresh" Interval="30000" OnTick="OnTimerTick" />
                <div class="rowgroup">
                    <table id="viewRegistryTable" class="line2 pl2 ">
                        <tr>
                            <th style="width: 9%;">
                                <asp:LinkButton runat="server" ID="lbtnSortShipmentIdNumber" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                                Shipment Id Number
                                </asp:LinkButton>
                            </th>
                            <th style="width: 14%;">
                                <asp:LinkButton runat="server" ID="lbtnSortExpirationDate" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                                Expiration
                                Date
                                </asp:LinkButton>
                            </th>
                            <th style="width: 14%;">
                                <asp:LinkButton runat="server" ID="lbtnSortDateCreated" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                                Date Created
                                </asp:LinkButton>
                            </th>
                            <th style="width: 9%;">
                                <asp:LinkButton runat="server" ID="lbtnSortDirection" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                                Direction
                                </asp:LinkButton>
                            </th>
                            <th style="width: 13%;">
                                <asp:LinkButton runat="server" ID="lbtnSortPurchaseOrderNumber" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                                Purchase Order Number
                                </asp:LinkButton>
                            </th>
                            <th style="width: 14%;">
                                <asp:LinkButton runat="server" ID="lbtnSortShipperReference" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                                Shipper Reference #
                                </asp:LinkButton>
                            </th>
                            <th style="width: 10%;">
                                <asp:LinkButton runat="server" ID="lbtnSortTotalWeight" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                                Total Weight
                                </asp:LinkButton>
                            </th>
                            <th style="width: 10%;">
                                <asp:LinkButton runat="server" ID="lbtnSortDocumentType" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                                Document
                                Type
                                </asp:LinkButton>
                            </th>
                            <th style="width: 7%;" class="text-center">Action</th>
                        </tr>
                        <asp:Repeater runat="server" ID="rptXmlConnectRecords" OnItemDataBound="OnXmlConnectRecordsItemDataBound">
                            <ItemTemplate>
                                <tr>
                                    <td rowspan="2" class="top">
                                        <eShip:CustomHiddenField ID="hidXmlConnectId" runat="server" Value='<%# Eval("Id") %>' />
                                        <%# Eval("ShipmentIdNumber") %>
                                    </td>
                                    <td>
                                        <%# Eval("ExpirationDate").Equals(DateUtility.SystemEarliestDateTime) ? string.Empty : Eval("ExpirationDate").FormattedLongDateAlt() %>
                                    </td>
                                    <td>
                                        <%# Eval("DateCreated").Equals(DateUtility.SystemEarliestDateTime) ? string.Empty : Eval("DateCreated").FormattedLongDateAlt() %>
                                    </td>
                                    <td>
                                        <%# Eval("DirectionText")%>
                                    </td>
                                    <td>
                                        <%# Eval("PurchaseOrderNumber")%>
                                    </td>
                                    <td>
                                        <%# Eval("ShipperReference")%> 
                                    </td>
                                    <td>
                                        <%# Eval("TotalWeight", "{0:n2}").ToDecimal() == default(decimal) ? string.Empty : Eval("TotalWeight", "{0:n2}") %>
                                    </td>
                                    <td>
                                        <%# Eval("DocumentTypeText")%>
                                    </td>
                                    <td rowspan="2" class="text-left top">
                                        <div>
                                            <asp:ImageButton ID="ImageButton3" ToolTip="Process/Edit" runat="server" ImageUrl="~/images/icons2/editBlue.png"
                                                CausesValidation="false" OnClick="OnXmlConnectProcessClicked"
                                                Visible='<%# Eval("Direction").ToInt().ToEnum<Direction>() == Direction.Inbound && Eval("ShipmentIdNumber").ToString() != string.Empty && Eval("DocumentTypeText").GetString() == EdiDocumentType.EDI204.GetString()  %>' />
                                            <asp:ImageButton ID="ibtnDownload" ToolTip="Download Xml Content" runat="server" ImageUrl="~/images/icons2/exportBlue.png"
                                                CausesValidation="false" OnClick="OnXmlConnectDownloadClicked" Visible='<%# CanDownloadXml %>' />
                                            <eShip:CustomHiddenField runat="server" ID="hidLoadOrderId" Value='<%# Eval("LoadOrderId") %>' />
                                            <asp:HyperLink runat="server" ID="hypLoadOrderView" Target="_blank" Visible='<%# Eval("LoadOrderId").ToLong() != default(long) %>'>
                                                <asp:Image runat="server" ID="imgGoToLoadOrderView" ImageUrl="~/images/icons2/arrow_newTab.png"
                                                    AlternateText="Go to Load Order" CssClass="mb5 middle" />
                                            </asp:HyperLink>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="f9">
                                    <td colspan="7" class="forceLeftBorder">
                                        <div class="rowgroup">
                                            <div class="col_1_3 no-right-border">
                                                <div class="row">
                                                    <label class="wlabel blue">Origin</label>
                                                    <label><%# Eval("OriginCity") %> <%# Eval("OriginState") %> <%# Eval("OriginPostalCode") %> <%# Eval("OriginCountryCode") %> &nbsp;</label>
                                                </div>
                                                <div class="row mt10">
                                                    <label class="wlabel blue">Destination</label>
                                                    <label><%# Eval("DestinationCity") %> <%# Eval("DestinationState") %> <%# Eval("DestinationPostalCode") %> <%# Eval("DestinationCountryCode") %></label>
                                                </div>
                                            </div>
                                            <div class="col_1_3 no-right-border">
                                                <div class="row">
                                                    <label class="wlabel blue">Customer</label>
                                                    <label>
                                                        <%# string.IsNullOrEmpty(Eval("CustomerNumber").GetString()) ? string.Empty : string.Format("{0} - ",Eval("CustomerNumber")) %> <%# Eval("CustomerName")%>
                                                        <asp:HyperLink runat="server" ID="hypCustomerNumber" Target="_blank" ToolTip="Go To Customer Record"
                                                            Visible='<%#  !string.IsNullOrEmpty(Eval("CustomerNumber").GetString()) && ActiveUser.HasAccessTo(ViewCode.Customer) %>'>
                                                            <asp:Image ImageUrl="~/images/icons2/arrow_newTab.png" runat="server" Width="16px" CssClass="middle"/>
                                                        </asp:HyperLink>
                                                        &nbsp;
                                                    </label>
                                                </div>
                                                <div class="row mt10">
                                                    <label class="wlabel blue">Vendor</label>
                                                    <label>
                                                        <%# string.IsNullOrEmpty(Eval("VendorNumber").GetString()) ? string.Empty : string.Format("{0} - {1}",Eval("VendorNumber"), Eval("VendorName")) %>
                                                        <asp:HyperLink runat="server" ID="hypVendorNumber" Target="_blank" ToolTip="Go To Vendor Record"
                                                            Visible='<%#  !string.IsNullOrEmpty(Eval("VendorNumber").GetString()) && ActiveUser.HasAccessTo(ViewCode.Vendor) %>'>
                                                            <asp:Image ImageUrl="~/images/icons2/arrow_newTab.png" runat="server" Width="16px" CssClass="middle"/>
                                                        </asp:HyperLink>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col_1_3">
                                                <div class="row">
                                                    <label class="wlabel blue">Processed/Declined</label>
                                                    <label>
                                                        <asp:Literal runat="server" Visible='<%# Eval("LoadOrderId").ToLong() == default(long) && Eval("LoadTenderResponseId").ToLong() != default(long) %>'>
                                                            <span class="red">Declined Load Tender</span>
                                                        </asp:Literal>

                                                        <asp:Literal runat="server" Visible='<%# Eval("LoadOrderId").ToLong() != default(long) && Eval("LoadTenderResponseId").ToLong() != default(long) %>'>
                                                            <span class="green">Accepted Load Tender</span>
                                                        </asp:Literal>
                                                        &nbsp;
                                                    </label>
                                                </div>
                                                <div class="row mt10">
                                                    <label class="wlabel blue">Status</label>
                                                    <label class="comlabel pt5">
                                                        <%# Eval("StatusText") %>
                                                        <br />
                                                        <span><%# Eval("StatusMessage") %></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lbtnSortShipmentIdNumber" />
                <asp:PostBackTrigger ControlID="lbtnSortExpirationDate" />
                <asp:PostBackTrigger ControlID="lbtnSortDateCreated" />
                <asp:PostBackTrigger ControlID="lbtnSortDirection" />
                <asp:PostBackTrigger ControlID="lbtnSortPurchaseOrderNumber" />
                <asp:PostBackTrigger ControlID="lbtnSortShipperReference" />
                <asp:PostBackTrigger ControlID="lbtnSortTotalWeight" />
                <asp:PostBackTrigger ControlID="lbtnSortDocumentType" />
                <asp:PostBackTrigger ControlID="rptXmlConnectRecords" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:Panel ID="pnlEditProcessRecord" runat="server" Visible="false">
            <div class="popupControl">
                <div class="popheader">
                    <h4>
                        <asp:Literal ID="litTitle" runat="server" />
                    </h4>
                    <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                        CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
                </div>

                <div class="row">
                    <table class="poptable">
                        <tr>
                            <td colspan="20" class="pb10 text-center">
                                <h6 class="red">
                                    <asp:Literal ID="litErrorMessage" runat="server" /></h6>

                            </td>
                        </tr>

                        <tr>
                            <td colspan="20" class="pb10 text-center">
                                <h6 class="green">
                                    <asp:Literal runat="server" ID="litProcessedMessage" /></h6>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="20" style="padding-bottom: 10px;">
                                <eShip:CustomHiddenField runat="server" ID="hidEditXmlConnectId" />
                                <%--                                 style="min-height: 300px; text-align: left; overflow: auto; display: block; padding-left: 25px; padding-right: 25px; font-size: .9em;" --%>
                                <div runat="server" id="contentDiv" class="ml10 mr10">
                                    [content]
                                </div>
                            </td>
                        </tr>

                        <asp:Panel runat="server" ID="pnlRequiredEquipmentType" Visible="False">
                            <tr>
                                <td colspan="20" class="pb20 text-center">
                                    <h6 class="red">*** Unable to match equipment description code to equipment type or equipment type is inactive. If you proceed, 
                       the resulting load order will not have an equipment type associated with it; you will have to manually select an equipment code for the load.
                                    </h6>
                                </td>
                            </tr>
                        </asp:Panel>

                        <asp:Panel runat="server" ID="pnlRequiredPackageType" Visible="False">
                            <tr>
                                <td colspan="20" class="pb20 text-center">
                                    <h6 class="red">*** Unable to match order identification detail package type code to package type type or package type is inactive. 
                        If you proceed, the resulting load order will utilize the default tenant package type if one exists. If a default tenant package type does not exist, the process will 
                        generate an error and fail.
                                    </h6>
                                </td>
                            </tr>
                        </asp:Panel>

                        <asp:Panel runat="server" ID="pnlProcessLoadTender" Visible="False">
                            <tr class="buttons">
                                <td class="text-left pl25">
                                    <asp:Literal runat="server" ID="litRejectionReason" Text="Rejection Reason:" />
                                    <asp:DropDownList runat="server" ID="ddlRejectionReason" DataTextField="Text" DataValueField="Value" />
                                </td>
                                <td class="text-right pr10">
                                    <asp:Button ID="btnAcceptLoadTender" Text="Accept" OnClick="OnAcceptLoadTender" runat="server" CausesValidation="False" />
                                    <asp:Button ID="btnRejectLoadTender" Text="Reject" OnClick="OnRejectLoadTender" runat="server" CausesValidation="false" />
                                </td>
                            </tr>
                        </asp:Panel>

                        <asp:Panel runat="server" ID="pnlGoToLoadOrder" Visible="False">
                            <tr class="buttons">
                                <td colspan="20" class="text-right pr10 pb10">
                                    <asp:Button ID="btnGoToLoadOrder" Text="Go To Load" OnClick="OnGotoLoadOrder" runat="server" CausesValidation="False" />
                                </td>
                            </tr>
                        </asp:Panel>

                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackgroundControl" runat="server" Visible="false" />
    <eShip:CustomHiddenField runat="server" ID="hidDocTypeProcessing" />
    <eShip:CustomHiddenField runat="server" ID="hidGoToRecordId" />
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />

    <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
</asp:Content>
