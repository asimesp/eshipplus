﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="VendorCommunicationView.aspx.cs" EnableEventValidation="false"
    Inherits="LogisticsPlus.Eship.WebApplication.Members.Connect.VendorCommunicationView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        ShowEdit="true" ShowDelete="true" ShowSave="true" ShowFind="true" ShowMore="True" OnFind="OnToolbarFindClicked"
        OnSave="OnToolbarSaveClicked" OnDelete="OnToolbarDeleteClicked" OnNew="OnToolbarNewClicked"
        OnEdit="OnToolbarEditClicked" OnUnlock="OnUnlockClicked" OnCommand="OnToolbarCommand" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Vendor Communication
               <eShip:RecordIdentityDisplayControl runat="server" ID="ridcRecordIdentity" TargetControlId="txtVendorName" />
            </h3>

            <div class="lockMessage">
                <asp:Literal ID="litMessage" runat="server" />
            </div>
        </div>
        <hr class="dark mb5" />
        <div class="errorMsgLit">
            <asp:Literal runat="server" ID="litErrorMessages" />
        </div>

        <eShip:VendorCommunicationFinderControl ID="vendorCommunicationFinder" runat="server" Visible="false"
            OpenForEditEnabled="true" OnItemSelected="OnVendorCommunicationFinderItemSelected"
            OnSelectionCancel="OnVendorCommunicationFinderSelectionCancelled" />
        <eShip:VendorFinderControl ID="vendorFinder" runat="server" EnableMultiSelection="false"
            Visible="False" OpenForEditEnabled="false" OnSelectionCancel="OnVendorFinderSelectionCancelled"
            OnItemSelected="OnVendorFinderItemSelected" />
        <eShip:CustomHiddenField runat="server" ID="hidVendorCommunicationId" />

        <script type="text/javascript">
            function ClearHidFlag() {
                jsHelper.SetEmpty('<%= hidFlag.ClientID %>');
            }
        </script>

        <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
            Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" />
        <eShip:AjaxTabHeaderTextUpdater runat="server" ID="athtuTabUpdater" />

        <ajax:TabContainer ID="TabContainer1" runat="server" CssClass="ajaxCustom">
            <ajax:TabPanel runat="server" ID="tabDetails" HeaderText="Details">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlDetails">
                        <div class="rowgroup">
                            <div class="col_1_2 bbox">
                                <h5>Account Information</h5>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Connect Guid</label>
                                        <eShip:CustomTextBox ID="txtConnectGuid" runat="server" CssClass="w420 disabled" ReadOnly="True" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Vendor
                                            <%= hidVendorId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.Vendor) 
                                                    ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Vendor Record'><img src={3} width='20' class='middle'/></a>", 
                                                                            ResolveUrl(VendorView.PageAddress),
                                                                            WebApplicationConstants.TransferNumber, 
                                                                            hidVendorId.Value.UrlTextEncrypt(),
                                                                            ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                    : string.Empty %>
                                        </label>
                                        <eShip:CustomHiddenField runat="server" ID="hidVendorId" />
                                        <eShip:CustomTextBox runat="server" ID="txtVendorNumber" CssClass="w110" OnTextChanged="OnVendorNumberChanged"
                                            AutoPostBack="True" />
                                        <asp:RequiredFieldValidator runat="server" ID="rfvVendor" ControlToValidate="txtVendorNumber"
                                            ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                        <asp:ImageButton ID="imgVendorSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                                            CausesValidation="False" OnClick="OnVendorSearchClicked" />
                                        <eShip:CustomTextBox runat="server" ID="txtVendorName" CssClass="w280 disabled" ReadOnly="True" />
                                        <ajax:AutoCompleteExtender runat="server" ID="aceVendor" TargetControlID="txtVendorNumber" ServiceMethod="GetVendorList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                            EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                                    </div>
                                </div>
                                <h5 class="pt10">General Options</h5>
                                <div class="row">
                                    <ul class="twocol_list">
                                        <li>
                                            <asp:CheckBox ID="chkEnabledWebServiceApiAccess" runat="server" CssClass="jQueryUniform" />
                                            <label>Enabled Web Service API Access</label>
                                        </li>
                                    </ul>
                                </div>

                                <h5 class="pt10">Document Pickup Options</h5>
                                <div class="row">
                                    <p class="note">
                                        * All pickups are selective <span style="font-style: italic">i.e. \[default folder\]XML.<b>type</b></span>
                                        where <b>type</b> is one of the support types 204, 210, 211, 214, 990 or 997. Default
                                        folder is optional and only utilized if present.
                                    </p>
                                    <ul class="twocol_list">
                                        <li>
                                            <asp:CheckBox runat="server" ID="chk210" Enabled="False" ToolTip="Currently Unavailable" CssClass="jQueryUniform" />
                                            <label class="comlabel">
                                                Pickup 210
                                                <br />
                                                <span>(Freight Invoice)</span>
                                            </label>
                                        </li>
                                        <li>
                                            <asp:CheckBox runat="server" ID="chkAck210" Enabled="False" ToolTip="Currently Unavailable" CssClass="jQueryUniform" />
                                            <label class="comlabel">
                                                Acknowledge 210
                                                <br />
                                                <span>(Freight Invoice)</span>
                                            </label>
                                        </li>
                                        <li>
                                            <asp:CheckBox runat="server" ID="chk214" CssClass="jQueryUniform" />
                                            <label class="comlabel">
                                                Pickup 214
                                                <br />
                                                <span>(Shipment Status Update)</span>
                                            </label>
                                        </li>
                                        <li>
                                            <asp:CheckBox runat="server" ID="chkAck214" CssClass="jQueryUniform" />
                                            <label class="comlabel">
                                                Acknowledge 214
                                                <br />
                                                <span>(Shipment Status Update)</span>
                                            </label>
                                        </li>
                                        <li>
                                            <asp:CheckBox runat="server" ID="chk990" CssClass="jQueryUniform" />
                                            <label class="comlabel">
                                                Pickup 990
                                                <br />
                                                <span>(Load Tender Response)</span>
                                            </label>
                                        </li>
                                        <li>
                                            <asp:CheckBox runat="server" ID="chkAck990" CssClass="jQueryUniform" />
                                            <label class="comlabel">
                                                Acknowledge 990
                                                <br />
                                                <span>(Load Tender Response)</span>
                                            </label>
                                        </li>
                                        <li>
                                            <asp:CheckBox runat="server" ID="chk997" CssClass="jQueryUniform" />
                                            <label class="comlabel">
                                                Pickup 997
                                                <br />
                                                <span>(Functional Acknowledgement)</span>
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                                <h5 class="pt10">Document Drop Off Options</h5>
                                <div class="row">
                                    <ul class="twocol_list">
                                        <li>
                                            <asp:CheckBox runat="server" ID="chk204" CssClass="jQueryUniform" />
                                            <label class="comlabel">
                                                Drop Off 204 
                                                <br />
                                                <span>(Load Tender)</span>
                                            </label>
                                        </li>
                                        <li>
                                            <asp:CheckBox runat="server" ID="chkUseSelectiveDropOff" CssClass="jQueryUniform" />
                                            <label class="comlabel">
                                                Use Selective Drop Off
                                                <br />
                                                <span>(excludes 997's)</span></label>
                                        </li>
                                        <li>
                                            <label class="wlabel">Load Tender Expires after (mins)</label>
                                            <eShip:CustomTextBox ID="txtLoadTenderExpAllowance" runat="server" MaxLength="4" CssClass="w200" />
                                            <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" TargetControlID="txtLoadTenderExpAllowance"
                                                FilterType="Numbers" Enabled="True" />
                                        </li>
                                    </ul>
                                </div>


                                <h5 class="pt10">Project 44 Integration Settings</h5>
                                <div class="row mb10">
                                    <div class="fieldgroup mr20">
                                        <asp:CheckBox ID="chkProject44Enabled" runat="server" CssClass="jQueryUniform" />
                                        <label>Project 44 Enabled</label>
                                    </div>
                                    <div class="fieldgroup">
                                        <asp:CheckBox ID="chkProject44TrackingEnabled" runat="server" CssClass="jQueryUniform" />
                                        <label>Tracking Enabled</label>
                                    </div>
                                </div>
                                <div class="row mb10">
                                    <div class="fieldgroup">
                                        <asp:CheckBox ID="chkProject44DispatchEnabled" runat="server" CssClass="jQueryUniform" />
                                        <label>Dispatch Enabled</label>
                                    </div>
                                </div>

                                <asp:Panel runat="server" ID="pnlEvaSettings" Visible="True">
                                    <h5 class="pt10">SMC3 EVA Integration Settings</h5>
                                    <div class="row mb10">
                                        <div class="fieldgroup mr20">
                                            <asp:CheckBox ID="chkSmc3EvaEnabled" runat="server" CssClass="jQueryUniform" />
                                            <label class="w130 mr40">EVA Enabled</label>
                                        </div>
                                        <div class="fieldgroup">
                                            <asp:CheckBox ID="chkSmc3EvaTest" runat="server" CssClass="jQueryUniform" />
                                            <label>Test Mode</label>
                                        </div>
                                    </div>
                                    <div class="row mb10">
                                        <div class="fieldgroup mr20">
                                            <asp:CheckBox ID="chkSmc3EvaDispatchEnabled" runat="server" CssClass="jQueryUniform" />
                                            <label class="w130 mr40">EVA Dispatch Enabled</label>
                                        </div>
                                        <div class="fieldgroup">
                                            <asp:CheckBox ID="chkSmc3EvaDocumentRetrievalEnabled" runat="server" CssClass="jQueryUniform" />
                                            <label>EVA Document Retrieval Enabled</label>
                                        </div>
                                    </div>
                                    <div class="row mb10">
                                        <div class="fieldgroup mr20">
                                            <asp:CheckBox ID="chkSmc3EvaTrackingEnabled" runat="server" CssClass="jQueryUniform" />
                                            <label class="w130 mr40">EVA Tracking Enabled</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="fieldgroup mr20">
                                            <label class="wlabel">Test Account Token</label>
                                            <eShip:CustomTextBox ID="txtSmc3EvaTestAccountToken" MaxLength="200" runat="server" CssClass="w200" />
                                        </div>
                                        <div class="fieldgroup">
                                            <label class="wlabel">Production Account Token</label>
                                            <eShip:CustomTextBox ID="txtSmc3EvaProductionAccountToken" MaxLength="200" runat="server" CssClass="w200" />
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div class="col_1_2 bbox pl46 vlinedarkleft">
                                <h5>EDI Settings</h5>
                                <div class="row mt20">
                                    <ul class="twocol_list">
                                        <li>
                                            <asp:CheckBox ID="chkEdiEnabled" runat="server" CssClass="jQueryUniform" />
                                            <label>Enabled</label>
                                        </li>
                                        <li>
                                            <asp:CheckBox ID="chkSecureEdiVan" runat="server" CssClass="jQueryUniform" />
                                            <label>Secure EDI (SSL)</label>
                                        </li>
                                        <li>
                                            <asp:CheckBox ID="chkRemoveFileAfterPickupEdi" runat="server" CssClass="jQueryUniform" />
                                            <label>Delete Remote File on Pickup</label>
                                        </li>
                                    </ul>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">VAN Url</label>
                                        <eShip:CustomTextBox ID="txtEdiVanUrl" MaxLength="100" runat="server" CssClass="w425" />
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">VAN Username</label>
                                        <eShip:CustomTextBox ID="txtEdiVanUsername" MaxLength="50" runat="server" CssClass="w200" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">VAN Password</label>
                                        <eShip:CustomTextBox ID="txtEdiVanPassword" MaxLength="50" runat="server" CssClass="w200" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">EDI Code</label>
                                        <eShip:CustomTextBox ID="txtEdiCode" MaxLength="50" runat="server" CssClass="w200" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">VAN Default Folder</label>
                                        <eShip:CustomTextBox ID="txtEdiVanDefaultFolder" MaxLength="50" runat="server" CssClass="w200" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">File Wrapper</label>
                                        <asp:FileUpload runat="server" CssClass="jQueryUniform" ID="fupEdiVANEnvelopePath" />
                                        <asp:Button ID="btnClearEdiFileWrapper" runat="server" Text="CLEAR FILE" OnClick="OnClearEdiFileWrapperClicked" CssClass="ml10" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <script type="text/javascript">
                                            $(document).ready(function () {
                                                jsHelper.SetDoPostBackJsLink('<%= lbtnDownloadEdiVANEnvelope.ClientID %>');
                                            });
                                        </script>
                                        <eShip:CustomHiddenField runat="server" ID="hidEdiVANEnvelopePath" />
                                        <label class="w100p">
                                            Current File:
                                        <span class="blue">
                                            <asp:LinkButton ID="lbtnDownloadEdiVANEnvelope" runat="server" OnClick="OnDownloadEdiVanEnvelopeClicked" /></span>
                                        </label>
                                    </div>
                                </div>
                                <h5 class="pt10">FTP Settings</h5>
                                <div class="row mt20">
                                    <div class="fieldgroup mr20">
                                        <asp:CheckBox ID="chkFtpEnabled" runat="server" CssClass="jQueryUniform" />
                                        <label class="w130 mr40">Enabled</label>
                                    </div>
                                    <div class="fieldgroup">
                                        <asp:CheckBox ID="chkSecureFtp" runat="server" CssClass="jQueryUniform" />
                                        <label>Secure FTP (SSL)</label>
                                    </div>
                                </div>
                                <div class="row mt20">
                                    <div class="fieldgroup">
                                        <asp:CheckBox ID="chkSFtp" runat="server" CssClass="jQueryUniform" />
                                        <label>SSH FTP(will take presedence over Secure FTP)</label>
                                    </div>
                                </div>
                                <div class="row mt20">

                                    <div class="fieldgroup">
                                        <asp:CheckBox ID="chkRemoveFileAfterPickupFtp" runat="server" CssClass="jQueryUniform" />
                                        <label>Remove File After Pickup FTP</label>
                                    </div>
                                </div>
                                <div class="row mt10">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Url</label>
                                        <eShip:CustomTextBox ID="txtFtpUrl" MaxLength="100" runat="server" CssClass="w200" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Username</label>
                                        <eShip:CustomTextBox ID="txtFtpUsername" MaxLength="50" runat="server" CssClass="w200" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Password</label>
                                        <eShip:CustomTextBox ID="txtFtpPassword" MaxLength="50" runat="server" CssClass="w200" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Default Folder</label>
                                        <eShip:CustomTextBox ID="txtFtpDefaultFolder" MaxLength="50" runat="server" CssClass="w200" />
                                    </div>
                                </div>
                                <h5 class="pt10">Carrier LTL Integration Settings</h5>
                                <div class="row mb10">
                                    <div class="fieldgroup mr20">
                                        <asp:CheckBox runat="server" ID="chkImgRtrvEnabled" CssClass="jQueryUniform" />
                                        <label class="w130 mr40">Enabled Image Retrieval</label>
                                    </div>
                                    <div class="fieldgroup">
                                        <asp:CheckBox runat="server" ID="chkShipmentDispatchEnabled" CssClass="jQueryUniform" />
                                        <label class="mr30">Enabled Shipment Dispatch</label>
                                    </div>
                                </div>
                                <div class="row mb20">
                                    <div class="fieldgroup mr20">
                                        <asp:CheckBox runat="server" ID="chkDisableGuaranteedServiceDispatch" CssClass="jQueryUniform" />
                                        <label class="mr40">Disable Guaranteed Service Dispatch</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Username</label>
                                        <eShip:CustomTextBox runat="server" ID="txtImgRtrvUsername" MaxLength="50" CssClass="w200" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Password</label>
                                        <eShip:CustomTextBox runat="server" ID="txtImgRtrvPassword" MaxLength="50" CssClass="w200" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Retrieval Engine</label>
                                        <asp:DropDownList ID="ddlImgRtrvEngine" DataTextField="Text" CssClass="w200" DataValueField="Value" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabNotifications" HeaderText="Notifications">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlNotifications" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlNotifications">
                                <div class="row mb10">
                                    <div class="fieldgroup">
                                        <p class="note">
                                            &#8226; Emails are delimited by a new line, comma, semi-colon, colon, tab, or vertical bar
                                        </p>
                                    </div>
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddNotification" Text="Add Notification" CssClass="mr10" CausesValidation="false" OnClick="OnAddNotificationClicked" />
                                        <asp:Button runat="server" ID="btnClearAllNotifications" Text="Clear Notifications" CausesValidation="false" OnClick="OnClearNotificationsClicked" />
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheVendorCommunicationTable" TableId="vendorCommunicationTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table class="stripe" id="vendorCommunicationTable">
                                    <tr>
                                        <th style="width: 20%;">Milestone
                                        </th>
                                        <th style="width: 10%;">Notification Method
                                        </th>
                                        <th style="width: 15%;">Fax
                                        </th>
                                        <th style="width: 40%">Email</th>
                                        <th style="width: 5%;" class="text-center">Enabled
                                        </th>
                                        <th style="width: 5%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstNotifications" runat="server" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnNotificationsItemDataBound">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidNotificationIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <eShip:CustomHiddenField ID="hidNotificationId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <asp:DropDownList ID="ddlMilestone" DataTextField="Value" CssClass="w200" DataValueField="Key" runat="server" />
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlNotificationMethod" DataTextField="Value" DataValueField="Key" CssClass="w100" runat="server" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtFax" runat="server" Text='<%# Eval("Fax") %>' CssClass="w150" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtEmail" CssClass="w420" MaxLength="500" runat="server" Text='<%# Eval("Email") %>' />
                                                </td>
                                                <td class="text-center top">
                                                    <eShip:AltUniformCheckBox ID="chkEnabled" runat="server" Checked='<%# Eval("Enabled") %>' />
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        CausesValidation="false" OnClick="OnDeleteNotificationClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabAuditLog" HeaderText="Audit Log">
                <ContentTemplate>
                    <eShip:AuditLogControl runat="server" ID="auditLogs" />
                </ContentTemplate>
            </ajax:TabPanel>
        </ajax:TabContainer>
    </div>
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
    <eShip:CustomHiddenField runat="server" ID="hidFilesToDelete" />
</asp:Content>
