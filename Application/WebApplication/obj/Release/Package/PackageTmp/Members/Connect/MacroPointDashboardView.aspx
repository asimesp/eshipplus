﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="MacroPointDashboardView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Connect.MacroPointDashboardView"
    EnableEventValidation="false" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Administration" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Operations" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>
<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:UpdatePanel runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
                        MacroPoint Dashboard<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </h3>
        </div>
        <hr class="dark mb10" />

        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="MacroPointOrders" ShowAutoRefresh="True" OnProcessingError="OnSearchProfilesProcessingError"
                            OnSearchProfileSelected="OnSearchProfilesProfileSelected" OnAutoRefreshChanged="OnSearchProfilesAutoRefreshChanged" />
                    </div>
                </div>
            </div>
            <hr class="dark mb10" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                        OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>

            <hr class="fat" />
            <div class="row mb10">
                <div class="fieldgroup">
                    <asp:Button ID="btnSearch" CssClass="left" runat="server" Text="SEARCH" OnClick="OnSearchClicked"
                        CausesValidation="False" />
                    <asp:Button ID="btnExport" CssClass="left ml10" runat="server" Text="EXPORT" OnClick="OnExportClicked"
                        CausesValidation="False" />
                </div>
                <div class="fieldgroup right">
                    <label>Sort By:</label>
                    <asp:DropDownList runat="server" ID="ddlSortBy" DataTextField="DisplayName" DataValueField="Name"
                        OnSelectedIndexChanged="OnSortBySelectedIndexChanged" AutoPostBack="True" CssClass="mr10" />
                    <asp:RadioButton runat="server" ID="rbAsc" Text="ASC" GroupName="rbSortBy" AutoPostBack="True"
                        ToolTip="Ascending" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />&nbsp;
                    <asp:RadioButton runat="server" ID="rbDesc" Text="DESC" GroupName="rbSortBy" Checked="True"
                        ToolTip="Descending" AutoPostBack="True" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                </div>
            </div>
        </asp:Panel>
        <asp:UpdatePanel runat="server" ID="upDataUpdate">
            <ContentTemplate>
                <eShip:UpdatePanelContinuousScrollExtender ID="upcseDataUpdate" runat="server" BindableControlId="lstMacroPointOrders" UpdatePanelToExtendId="upDataUpdate" />
                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheViewRegistryTable" TableId="macroPointDashboardTable" HeaderZIndex="2" />
                <asp:Timer runat="server" ID="tmRefresh" Interval="30000" OnTick="OnTimerTick" />
                <div class="rowgroup">
                    <table id="macroPointDashboardTable" class="line2 pl2">
                        <tr>
                            <th style="width: 9%;">
                                <asp:LinkButton runat="server" ID="lbtnSortIdNumber" CssClass="link_nounderline blue"
                                    CausesValidation="False" OnCommand="OnSortData">
                            Id <abbr title="Shipment Id Number">#</abbr>
                                </asp:LinkButton>
                            </th>
                            <th style="width: 3%;">
                                <asp:LinkButton runat="server" ID="lbtnSortNumberType" CssClass="link_nounderline blue"
                                    CausesValidation="False" OnCommand="OnSortData">
                            Number Type
                                </asp:LinkButton>
                            </th>
                            <th style="width: 8%;">
                                <asp:LinkButton runat="server" ID="lbtnSortNumber" CssClass="link_nounderline blue"
                                    CausesValidation="False" OnCommand="OnSortData">
                            Number
                                </asp:LinkButton>
                            </th>
                            <th style="width: 10%;">
                                <asp:LinkButton runat="server" ID="lbtnSortDateCreated" CssClass="link_nounderline blue"
                                    CausesValidation="False" OnCommand="OnSortData">
                            Date Created
                                </asp:LinkButton>
                            </th>
                            <th style="width: 10%;">
                                <asp:LinkButton runat="server" ID="lbtnSortStartTrackDateTime" CssClass="link_nounderline blue"
                                    CausesValidation="False" OnCommand="OnSortData">
                            Start Date
                                </asp:LinkButton>
                            </th>
                            <th style="width: 10%;">
                                <asp:LinkButton runat="server" ID="lbtnSortDateStopRequested" CssClass="link_nounderline blue"
                                    CausesValidation="False" OnCommand="OnSortData">
                            Stop Date
                                </asp:LinkButton>
                            </th>
                            <th style="width: 18%;">
                                <asp:LinkButton runat="server" ID="lbtnStatus" CssClass="link_nounderline blue"
                                    CausesValidation="False" OnCommand="OnSortData">
                            Status
                                </asp:LinkButton>
                            </th>
                            <th style="width: 15%;">
                                <asp:LinkButton runat="server" ID="lbtnSortUsername" CssClass="link_nounderline blue"
                                    CausesValidation="False" OnCommand="OnSortData">
                            User
                                </asp:LinkButton>
                            </th>
                            <th style="width: 5%;">
                                <asp:LinkButton runat="server" ID="lbtnSortTrackCost" CssClass="link_nounderline blue"
                                    CausesValidation="False" OnCommand="OnSortData">
                            Cost
                                </asp:LinkButton>
                            </th>
                            <th style="width: 12%;">
                                <asp:LinkButton runat="server" ID="lbtnSortTrackDurationHours" CssClass="link_nounderline blue"
                                    CausesValidation="False" OnCommand="OnSortData">
                            Track For
                                </asp:LinkButton>/
                         <asp:LinkButton runat="server" ID="lbtnSortTrackIntervalMinutes" CssClass="link_nounderline blue"
                             CausesValidation="False" OnCommand="OnSortData">
                            Update Every
                         </asp:LinkButton>
                            </th>
                        </tr>
                        <asp:ListView runat="server" ID="lstMacroPointOrders" ItemPlaceholderID="itemPlaceHolder">
                            <LayoutTemplate>
                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                         <%# ActiveUser.HasAccessTo(ViewCode.Shipment) 
                                                ? string.Format("{0} <a href='{1}?{2}={0}' class='blue' target='_blank' title='Go To Shipment Record'><img src={3} width='16' class='middle'/></a>", 
                                                    Eval("IdNumber"),
                                                    ResolveUrl(ShipmentView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))  
                                                : Eval("IdNumber") %>
                                    </td>
                                    <td>
                                        <%# Eval("NumberType") %>
                                    </td>
                                    <td>
                                        <%# Eval("Number") %>
                                    </td>
                                    <td>
                                        <%# Eval("DateCreated").Equals(DateUtility.SystemEarliestDateTime) ? string.Empty : Eval("DateCreated").FormattedLongDateAlt() %>
                                    </td>
                                    <td>
                                        <%# Eval("StartTrackDateTime").Equals(DateUtility.SystemEarliestDateTime) ? string.Empty : Eval("StartTrackDateTime").FormattedLongDateAlt() %>
                                    </td>
                                    <td>
                                        <%# Eval("DateStopRequested").Equals(DateUtility.SystemEarliestDateTime) ? string.Empty : Eval("DateStopRequested").FormattedLongDateAlt() %>
                                    </td>
                                    <td>
                                        <%# Eval("TrackingStatusDescription") %>
                                    </td>
                                    <td>
                                        <%# ActiveUser.HasAccessTo(ViewCode.User) 
                                                ? string.Format("{0} <a href='{1}?{2}={3}' class='blue' target='_blank' title='Go To User Record'><img src={4} width='16' class='middle'/></a>", 
                                                    Eval("Username"),
                                                    ResolveUrl(UserView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    Eval("UserId").GetString().UrlTextEncrypt(),
                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))  
                                                : Eval("Username") %>
                                    </td>
                                    <td>
                                        <%# Eval("TrackCost").ToDecimal().ToString("c2") %>
                                    </td>
                                    <td>
                                        <%# Eval("TrackDurationHours") %> hrs/ <%# Eval("TrackIntervalMinutes") %> min
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lbtnSortIdNumber" />
                <asp:PostBackTrigger ControlID="lbtnSortNumberType" />
                <asp:PostBackTrigger ControlID="lbtnSortNumber" />
                <asp:PostBackTrigger ControlID="lbtnSortDateCreated" />
                <asp:PostBackTrigger ControlID="lbtnSortStartTrackDateTime" />
                <asp:PostBackTrigger ControlID="lbtnSortDateStopRequested" />
                <asp:PostBackTrigger ControlID="lbtnStatus" />
                <asp:PostBackTrigger ControlID="lbtnSortUsername" />
                <asp:PostBackTrigger ControlID="lbtnSortTrackCost" />
                <asp:PostBackTrigger ControlID="lbtnSortTrackDurationHours" />
                <asp:PostBackTrigger ControlID="lbtnSortTrackIntervalMinutes" />
                <asp:PostBackTrigger ControlID="lstMacroPointOrders" />
            </Triggers>
        </asp:UpdatePanel>
        <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
        <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
        <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
    </div>
</asp:Content>
