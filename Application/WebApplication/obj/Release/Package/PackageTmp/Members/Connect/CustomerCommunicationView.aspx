﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="CustomerCommunicationView.aspx.cs" EnableEventValidation="false"
    Inherits="LogisticsPlus.Eship.WebApplication.Members.Connect.CustomerCommunicationView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        ShowEdit="true" ShowDelete="true" ShowSave="true" ShowFind="true" ShowMore="True"  OnFind="OnToolbarFindClicked"
        OnSave="OnToolbarSaveClicked" OnDelete="OnToolbarDeleteClicked" OnNew="OnToolbarNewClicked"
        OnEdit="OnToolbarEditClicked" OnUnlock="OnUnlockClicked" OnCommand="OnToolbarCommand" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Customer Communication
                <eShip:RecordIdentityDisplayControl runat="server" ID="ridcRecordIdentity" TargetControlId="txtCustomerName" />
            </h3>

            <div class="lockMessage">
                <asp:Literal ID="litMessage" runat="server" />
            </div>
        </div>
        <hr class="dark mb5" />
        <div class="errorMsgLit">
            <asp:Literal runat="server" ID="litErrorMessages" />
        </div>

        <eShip:CustomerCommunicationFinderControl ID="customerCommunicationFinder" runat="server" Visible="false"
            OpenForEditEnabled="true" OnItemSelected="OnCustomerCommunicationFinderItemSelected"
            OnSelectionCancel="OnCustomerCommunicationFinderSelectionCancelled" />
        <eShip:CustomerFinderControl ID="customerFinder" runat="server" EnableMultiSelection="false"
            Visible="False" OpenForEditEnabled="false" OnSelectionCancel="OnCustomerFinderSelectionCancelled"
            OnItemSelected="OnCustomerFinderItemSelected" />

        <script type="text/javascript">
            function ClearHidFlag() {
                jsHelper.SetEmpty('<%= hidFlag.ClientID %>');
            }
        </script>

        <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
            Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" />
        <eShip:AjaxTabHeaderTextUpdater runat="server" ID="athtuTabUpdater" />

        <eShip:CustomHiddenField runat="server" ID="hidCustomerCommunicationId" />

        <ajax:TabContainer ID="tabCommunication" runat="server" CssClass="ajaxCustom">
            <ajax:TabPanel runat="server" ID="tabDetails" HeaderText="Details">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlDetails">
                        <div class="rowgroup">
                            <div class="col_1_2 bbox">
                                <h5>Account Information</h5>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Connect Guid</label>
                                        <eShip:CustomTextBox ID="txtConnectGuid" runat="server" CssClass="w420 disabled" ReadOnly="True" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Customer
                                            <%= hidCustomerId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.Customer) 
                                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Customer Record'><img src={3} width='20' class='middle'/></a>", 
                                                                        ResolveUrl(CustomerView.PageAddress),
                                                                        WebApplicationConstants.TransferNumber, 
                                                                        hidCustomerId.Value.UrlTextEncrypt(),
                                                                        ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                : string.Empty %>
                                        </label>
                                        <eShip:CustomHiddenField runat="server" ID="hidCustomerId" />
                                        <eShip:CustomTextBox runat="server" ID="txtCustomerNumber" CssClass="w110" OnTextChanged="OnCustomerNumberChanged" AutoPostBack="True" />
                                        <asp:RequiredFieldValidator runat="server" ID="rfvCustomer" ControlToValidate="txtCustomerNumber"
                                            ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                        <asp:ImageButton ID="imgCustomerSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png" CausesValidation="False" OnClick="OnCustomerSearchClicked" />
                                        <eShip:CustomTextBox runat="server" ID="txtCustomerName" CssClass="w280 disabled" ReadOnly="True" />
                                        <ajax:AutoCompleteExtender runat="server" ID="aceCustomer" TargetControlID="txtCustomerNumber" ServiceMethod="GetCustomerList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                            EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                                    </div>
                                </div>

                                <h5 class="pt10">General Options</h5>
                                <div class="row">
                                    <ul class="twocol_list">
                                        <li>
                                            <asp:CheckBox ID="chkEnabledWebServiceApiAccess" runat="server" CssClass="jQueryUniform" />
                                            <label>Enabled Web Service API Access</label>
                                        </li>
                                        <li>
                                            <asp:CheckBox ID="chkStopVendorNotifications" runat="server" CssClass="jQueryUniform" />
                                            <label>Customer Handles Vendor Notifications</label>
                                        </li>
                                    </ul>
                                </div>
								   <div class="row">
                                    <ul class="twocol_list">
                                        <li>
                                            <asp:CheckBox ID="chkSendAveryLabel" runat="server" CssClass="jQueryUniform" />
                                            <label>Send Avery Label via Email</label>
                                        </li>
                                        <li>
                                           <asp:CheckBox ID="chkSendStandardLabel" runat="server" CssClass="jQueryUniform" />
                                            <label>Send Standard Label via Email</label>
                                        </li>
                                    </ul>
                                </div>

                                <h5 class="pt10">Document Options</h5>
                                <div class="row">
                                    <p class="note">
                                        * All pickups are selective <span style="font-style: italic">i.e. \[default folder\]XML.<b>type</b></span>
                                        where <b>type</b> is one of the support types 204, 210, 211, 214 or 997. Default
                                        folder is optional and only utilized if present.
                                    </p>
                                    <ul class="twocol_list">
                                        <li>
                                            <asp:CheckBox runat="server" ID="chkUseSelectiveDropOff" CssClass="jQueryUniform" />
                                            <label class="comlabel">
                                                Use Selective Drop Off
                                                <br />
                                                <span>(excludes 997's)</span></label>
                                        </li>
                                        <li>
                                            <asp:CheckBox runat="server" ID="chk997" CssClass="jQueryUniform" />
                                            <label class="comlabel">
                                                Pickup 997
                                                <br />
                                                <span>(Functional Acknowledgement)</span></label>
                                        </li>
                                        <li>
                                            <asp:CheckBox runat="server" ID="chk204" CssClass="jQueryUniform" />
                                            <label class="comlabel">
                                                Pickup 204
                                                <br />
                                                <span>(Load Tender)</span></label>
                                        </li>
                                        <li>
                                            <asp:CheckBox runat="server" ID="chkAck204" CssClass="jQueryUniform" />
                                            <label class="comlabel">
                                                Acknowledge 204
                                                <br />
                                                <span>(Load Tender)</span></label>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                            <div class="col_1_2 bbox pl46 vlinedarkleft">
                                <h5>EDI Settings</h5>
                                <div class="row mt20">
                                    <ul class="twocol_list">
                                        <li>
                                            <asp:CheckBox ID="chkEdiEnabled" runat="server" CssClass="jQueryUniform" />
                                            <label>Enabled</label>
                                        </li>
                                        <li>
                                            <asp:CheckBox ID="chkSecureEdiVan" runat="server" CssClass="jQueryUniform" />
                                            <label>Secure EDI (SSL)</label>
                                        </li>
                                        <li>
                                            <asp:CheckBox ID="chkRemoveFileAfterPickupEdi" runat="server" CssClass="jQueryUniform" />
                                            <label>Delete Remote File on Pickup</label>
                                        </li>
                                    </ul>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">VAN Url</label>
                                        <eShip:CustomTextBox ID="txtEdiVanUrl" MaxLength="100" runat="server" CssClass="w425" />
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">VAN Username</label>
                                        <eShip:CustomTextBox ID="txtEdiVanUsername" MaxLength="50" runat="server" CssClass="w200" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">VAN Password</label>
                                        <eShip:CustomTextBox ID="txtEdiVanPassword" MaxLength="50" runat="server" CssClass="w200" />
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">EDI Code</label>
                                        <eShip:CustomTextBox ID="txtEdiCode" MaxLength="50" runat="server" CssClass="w200" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">VAN Default Folder</label>
                                        <eShip:CustomTextBox ID="txtEdiVanDefaultFolder" MaxLength="50" runat="server" CssClass="w200" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">File Wrapper</label>
                                        <asp:FileUpload runat="server" CssClass="jQueryUniform" ID="fupEdiVANEnvelopePath" />
                                        <asp:Button ID="btnClearEdiFileWrapper" runat="server" Text="CLEAR FILE" OnClick="OnClearEdiFileWrapperClicked" CssClass="ml10" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <script type="text/javascript">
                                            $(document).ready(function () {
                                                jsHelper.SetDoPostBackJsLink('<%= lbtnDownloadEdiVANEnvelope.ClientID %>');
                                            });
                                        </script>
                                        <eShip:CustomHiddenField runat="server" ID="hidEdiVANEnvelopePath" />
                                        <label class="w100p">
                                            Current File:
                                        <span class="blue">
                                            <asp:LinkButton ID="lbtnDownloadEdiVANEnvelope" runat="server" OnClick="OnDownloadEdiVanEnvelopeClicked" /></span>
                                        </label>
                                    </div>
                                </div>
                                <h5 class="pt10">FTP Settings</h5>
                                <div class="row mt20">
                                    <ul class="twocol_list">
                                        <li>
                                            <asp:CheckBox ID="chkFtpEnabled" runat="server" CssClass="jQueryUniform" />
                                            <label>Enabled</label>
                                        </li>
                                        <li>
                                            <asp:CheckBox ID="chkSecureFtp" runat="server" CssClass="jQueryUniform" />
                                            <label>Secure FTP (SSL)</label>
                                        </li>
	                                    <li>
		                                    <asp:CheckBox ID="chkRemoveFileAfterPickupFtp" runat="server" CssClass="jQueryUniform" />
		                                    <label>Remove File After Pickup FTP</label>
	                                    </li>
                                    </ul>
	                            <div class="fieldgroup">
			                                <asp:CheckBox ID="chkSFtp" runat="server" CssClass="jQueryUniform" />
			                                <label>SSH FTP(will take presedence over Secure FTP)</label>
		                                </div>
	                             
                                </div>
                                <div class="row mt10">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Url</label>
                                        <eShip:CustomTextBox ID="txtFtpUrl" MaxLength="100" runat="server" CssClass="w200" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Username</label>
                                        <eShip:CustomTextBox ID="txtFtpUsername" MaxLength="50" runat="server" CssClass="w200" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Password</label>
                                        <eShip:CustomTextBox ID="txtFtpPassword" MaxLength="50" runat="server" CssClass="w200" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Default Folder</label>
                                        <eShip:CustomTextBox ID="txtFtpDefaultFolder" MaxLength="50" runat="server" CssClass="w200" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabNotifications" HeaderText="Notifications">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlNotifications" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlNotifications">
                                <div class="row mb10">
                                    <div class="fieldgroup">
                                        <p class="note">
                                            &#8226; Emails are delimited by a new line, comma, semi-colon, colon, tab, or vertical bar
                                        </p>
                                    </div>
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddNotification" Text="Add Notification" CssClass="mr10" CausesValidation="false" OnClick="OnAddNotificationClicked" />
                                        <asp:Button runat="server" ID="btnClearAllNotifications" Text="Clear Notifications" CausesValidation="false" OnClick="OnClearNotificationsClicked" />
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheCustomerCommunicationTable" TableId="customerCommunicationTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table class="stripe" id="customerCommunicationTable">
                                    <tr>
                                        <th style="width: 20%;">Milestone
                                        </th>
                                        <th style="width: 10%;">Notification Method
                                        </th>
                                        <th style="width: 15%;">Fax
                                        </th>
                                        <th style="width: 40%">Email</th>
                                        <th style="width: 5%;" class="text-center">Enabled
                                        </th>
                                        <th style="width: 5%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstNotifications" runat="server" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnNotificationsItemDataBound">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidNotificationIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <eShip:CustomHiddenField ID="hidNotificationId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <asp:DropDownList ID="ddlMilestone" DataTextField="Value" CssClass="w200" DataValueField="Key" runat="server" />
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlNotificationMethod" DataTextField="Value" DataValueField="Key" CssClass="w100" runat="server" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtFax" runat="server" Text='<%# Eval("Fax") %>' CssClass="w150" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtEmail" CssClass="w420" MaxLength="500" runat="server" Text='<%# Eval("Email") %>' />
                                                </td>
                                                <td class="text-center top">
                                                    <eShip:AltUniformCheckBox ID="chkEnabled" runat="server" Checked='<%# Eval("Enabled") %>' />
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        CausesValidation="false" OnClick="OnDeleteNotificationClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabDocDelivery" HeaderText="Document Delivery">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlDocDelivery">
                        <div class="rowgroup">
                            <div class="col_1_2 bbox vlinedarkright">
                                <h5>Options</h5>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <asp:CheckBox runat="server" ID="chkFtpDocDeliveryEnabled" OnCheckedChanged="OnDocDeliveryEnabledCheckedChanged" AutoPostBack="True" CssClass="jQueryUniform" />
                                        <label class="w180">FTP Enabled</label>
                                    </div>
                                    <div class="fieldgroup">
                                        <asp:CheckBox runat="server" ID="chkDocDeliverySecureFtp" CssClass="jQueryUniform" />
                                        <label class="w200">Secure FTP</label>
                                    </div>
                                </div>
                                <div class="row pt10">
                                    <div class="fieldgroup">
                                        <asp:CheckBox runat="server" ID="chkDeliverInternalDocs" CssClass="jQueryUniform" />
                                        <label class="w180">Deliver Internal Documents (FTP)</label>
                                    </div>
                                    <div class="fieldgroup">
                                        <asp:CheckBox runat="server" ID="chkHoldShipmentTillInvoiced" CssClass="jQueryUniform" />
                                        <label>Hold Shipment Documents Until Invoiced (FTP)</label>
                                    </div>
                                </div>
                                <div class="row pt40">
                                    <div class="fieldgroup">
                                        <asp:CheckBox runat="server" ID="chkEmailDocDeliveryEnabled" CssClass="jQueryUniform" />
                                        <label class="w180">Email Enabled</label>
                                    </div>
                                </div>
                                <h5 class="pt20">FTP Settings</h5>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Delivery Documents Created On/After</label>
                                        <eShip:CustomTextBox runat="server" ID="txtDocDeliveryStartDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                                    </div>
                                </div>
                                <div class="row mt20">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">FTP Url</label>
                                        <eShip:CustomTextBox runat="server" ID="txtDocDeliveryFtpUrl" MaxLength="50" CssClass="w200" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">FTP Default Folder</label>
                                        <eShip:CustomTextBox runat="server" ID="txtDocDeliveryFtpDefaultFolder" MaxLength="50" CssClass="w200" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">FTP Username</label>
                                        <eShip:CustomTextBox runat="server" ID="txtDocDeliveryFtpUsername" MaxLength="50" CssClass="w200" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">FTP Password</label>
                                        <eShip:CustomTextBox runat="server" ID="txtDocDeliveryFtpPassword" MaxLength="50" CssClass="w200" />
                                    </div>
                                </div>
                            </div>
                            <div class="col_1_2 bbox pl46">
                                <h5>Deliver System Documents</h5>
                                <div class="row">
                                    <ul class="twocol_list">
                                        <li>
                                            <asp:CheckBox runat="server" ID="chkDeliverBol" CssClass="jQueryUniform" />
                                            <label>Bill of Lading</label>
                                        </li>
                                        <li>
                                            <asp:CheckBox runat="server" ID="chkDeliverStatement" CssClass="jQueryUniform" />
                                            <label>Shipment Statement</label>
                                        </li>
                                        <li>
                                            <asp:CheckBox ID="chkDeliverInvoice" runat="server" CssClass="jQueryUniform" />
                                            <label>Invoice (FTP)</label>
                                        </li>
                                    </ul>
                                </div>
                                <h5 class="pt10">Deliver Shipment Documents Tagged As</h5>
                                <div class="row">
                                    <asp:Repeater runat="server" ID="rptDocumentTags">
                                        <HeaderTemplate>
                                            <ul class="twocol_list">
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <li>
                                                <table>
                                                    <tr>
                                                        <td class="top p_m0">
                                                            <asp:CheckBox ID="chkSelected" runat="server" CssClass="jQueryUniform" />
                                                        </td>
                                                        <td class="p_m0">
                                                            <label>
                                                                <asp:Literal runat="server" ID="litDocTagText" Text='<%# Eval("Text") %>' />
                                                            </label>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <eShip:CustomHiddenField ID="hidTagId" Value='<%# Eval("Value") %>' runat="server" />
                                            </li>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </ul>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabAuditLog" HeaderText="Audit Log">
                <ContentTemplate>
                    <eShip:AuditLogControl runat="server" ID="auditLogs" />
                </ContentTemplate>
            </ajax:TabPanel>
        </ajax:TabContainer>
    </div>
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
    <eShip:CustomHiddenField runat="server" ID="hidFilesToDelete" />
</asp:Content>
