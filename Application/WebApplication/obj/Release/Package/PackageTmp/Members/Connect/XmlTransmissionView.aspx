﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="XmlTransmissionView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Connect.XmlTransmissionView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.Connect" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Operations" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="pageHeader">
        <h3>
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                CssClass="pageHeaderIcon" />
            Xml Transmissions
            <small class="ml10">
                <asp:Literal runat="server" ID="litRecordCount" /></small>
        </h3>
    </div>
    <span class="clearfix"></span>
    <hr class="dark mb10" />
    <asp:Panel runat="server" DefaultButton="btnSearch">
        <table id="searchOptionTable contain">
            <tr>
                <td class="text-left">
                    <label class="upper blue">CRITERIA:</label>
                    <eShip:CustomTextBox ID="txtSearchCriteria" runat="server" CssClass="w340" />
                </td>
                <td class="text-right mr10">
                    <label class="upper blue">Send Date Range Start:</label>
                </td>
                <td class="text-left">
                    <eShip:CustomTextBox runat="server" ID="txtStartSendDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                </td>
                <td class="text-right">
                    <label class="upper blue">Send Date Range End:</label>
                </td>
                <td class="text-left">
                    <eShip:CustomTextBox runat="server" ID="txtEndSendDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                </td>
            </tr>
            <tr>
                <td id="tdSearchOperators">
                    <script type="text/javascript">
                        $(document).ready(function () {
                            // enforce mutual exclusion on checkboxes in customerLoginPageSection
                            $(jsHelper.AddHashTag('tdSearchOperators input:checkbox')).click(function () {
                                if (jsHelper.IsChecked($(this).attr('id'))) {
                                    $(jsHelper.AddHashTag('tdSearchOperators input:checkbox')).not(jsHelper.AddHashTag($(this).attr('id'))).each(function () {
                                        jsHelper.UnCheckBox($(this).attr('id'));
                                    });
                                    $.uniform.update();
                                }
                            });
                        });
                    </script>
                    <asp:CheckBox runat="server" ID="chkExact" CssClass="jQueryUniform" />
                    <label class="upper blue">Exact Match</label>
                    <asp:CheckBox runat="server" ID="chkBeginsWith" CssClass="jQueryUniform ml10" />
                    <label class="upper blue">Begins With</label>
                    <asp:CheckBox runat="server" ID="chkEndsWith" CssClass="jQueryUniform ml10" />
                    <label class="upper blue">Ends With</label>
                    <asp:CheckBox runat="server" ID="chkContains" CssClass="jQueryUniform ml10" />
                    <label class="upper blue">Contains</label>
                </td>
                <td class="text-right">
                    <label class="upper blue">Send Okay:</label>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlSendOkay" DataValueField="Key" DataTextField="Value" CssClass="w130" />
                </td>
                <td class="text-right">
                    <label class="upper blue">Direction:</label>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlDirection" DataValueField="Key" DataTextField="Value" CssClass="w130" />
                </td>
            </tr>
        </table>
        <hr class="fat" />
        <div class="row mb10">
            <div class="fieldgroup right">
                <asp:Button ID="btnSearch" runat="server" Text="SEARCH" OnClick="OnSearchClicked" CssClass="mr10" CausesValidation="False" />
                <asp:Button ID="btnExport" runat="server" Text="EXPORT" OnClick="OnExportClicked" CausesValidation="False" />
            </div>
        </div>
    </asp:Panel>
    <eShip:TableFreezeHeaderExtender runat="server" ID="tfheXmlTransmission" TableId="viewRegistryTable" HeaderZIndex="2"/>
    <div class="rowgroup">
        <table id="viewRegistryTable" class="line2">
            <tr>
                <th style="width: 7%;">Reference #
                </th>
                <th style="width: 9%;">Reference Type
                </th>
                <th style="width: 10%;">Transaction Date
                </th>
                <th style="width: 10%;">Acknowledgement Receipt Date
                </th>
                <th style="width: 19%;">Transmission Key
                </th>
                <th style="width: 5%;">Document Type
                </th>
                <th style="width: 5%;" class="text-center">Notify Method
                </th>
                <th style="width: 10%;" class="text-center">Direction
                </th>
                <th style="width: 5%;" class="text-center">Send Okay
                </th>
                <th style="width: 7%;" class="text-center">Source Key
                </th>
                <th style="width: 7%;" class="text-center">User
                </th>
                <th style="width: 6%;" class="text-center pr10">Options
                </th>
            </tr>
            <asp:ListView ID="lvwRegistry" runat="server" ItemPlaceholderID="itemPlaceHolder" >
                <LayoutTemplate>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </LayoutTemplate>
                <ItemTemplate>
                    <tr>
                        <td rowspan="2" class="top">
                            <eShip:CustomHiddenField ID="hidXmlTransmissionId" runat="server" Value='<%# Eval("Id") %>' />
                            
                            <%# Eval("ReferenceNumber") %>
                            <%# GetDataItem().HasGettableProperty("ReferenceNumber") && GetDataItem().HasGettableProperty("ReferenceNumberType")
                                    ? Eval("ReferenceNumberType").ToString().ToEnum<XmlTransmissionReferenceNumberType>() == XmlTransmissionReferenceNumberType.Shipment && ActiveUser.HasAccessTo(ViewCode.Shipment)
                                                ? string.Format(" <a href=' {0}?{1}={2}' class='blue' target='_blank' title='Go To Shipment Record'><img src={3} width='16' class='middle'/></a>", 
                                                    ResolveUrl(ShipmentView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    Eval("ReferenceNumber"),
                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))  
                                                : Eval("ReferenceNumberType").ToString().ToEnum<XmlTransmissionReferenceNumberType>() == XmlTransmissionReferenceNumberType.Invoice && ActiveUser.HasAccessTo(ViewCode.Invoice)
                                                        ? string.Format(" <a href=' {0}?{1}={2}' class='blue' target='_blank' title='Go To Invoice Record'><img src={3} width='16' class='middle'/></a>", 
                                                            ResolveUrl(InvoiceView.PageAddress),
                                                            WebApplicationConstants.TransferNumber, 
                                                            Eval("ReferenceNumber"),
                                                            ResolveUrl("~/images/icons2/arrow_newTab.png"))  
                                                        : string.Empty
                                    : string.Empty %>
                            
                            
                        </td>
                        <td>
                            <asp:Literal runat="server" ID="litReferenceNumberType" Text='<%# Eval("ReferenceNumberType")%>' />
                        </td>
                        <td>
                            <%# Eval("TransmissionDateTime", "{0:yyyy-MM-dd} ") + "<br />" + Eval("TransmissionDateTime", "{0:HH:mm:ss}")%>
                        </td>
                        <td>
                            <%# Eval("AcknowledgementDateTime").Equals(DateUtility.SystemEarliestDateTime)
									? string.Empty : Eval("AcknowledgementDateTime", "{0:yyyy-MM-dd} ") + "<br />" + Eval("AcknowledgementDateTime", "{0:HH:mm:ss}")%>
                        </td>
                        <td>
                            <%# Eval("TransmissionKey")%>
                        </td>
                        <td>
                            <%# Eval("DocumentType")%>
                        </td>
                        <td class="text-center">
                            <%# Eval("NotificationMethod")%>
                        </td>
                        <td class="text-center">
                            <%# Eval("Direction")%>
                        </td>
                        <td class="text-center">
                            <%# Eval("SendOkay").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                        </td>
                        <td class="text-center">

                            <%# Eval("Id")%>
                        </td>
                        <td>
                            <%# Eval("User")%>
                        </td>
                        <td class="text-center top pr10" rowspan="2">
                            <asp:ImageButton ID="ImageButton1" ToolTip="Download" runat="server" ImageUrl="~/images/icons2/exportBlue.png"
                                CausesValidation="false" OnClick="OnXmlTransmissionDownloadClicked" />
                            <asp:ImageButton ID="ImageButton2" ToolTip="Resend" runat="server" ImageUrl="~/images/icons2/redeliver.png"
                                CausesValidation="false" OnClick="OnXmlTransmissionResendClicked" Visible="False" />
                            <asp:ImageButton ID="ImageButton3" ToolTip="Process" runat="server" ImageUrl="~/images/icons2/arrow_general.png"
                                CausesValidation="false" OnClick="OnXmlTransmissionProcessClicked" Visible='<%# Eval("Direction").ToString() == Direction.Inbound.FormattedString() && Eval("AcknowledgementDateTime").Equals(DateUtility.SystemEarliestDateTime)  %>' />
                        </td>
                    </tr>
                    <tr class="f9">
                        <td colspan="10" class="forceLeftBorder">
                            <div class="left blue w200 mr10">Acknowledgement Message:</div>
                            <div class="left text-left"><%# Eval("AcknowledgementMessage")%></div>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </table>
    </div>
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
    <eShip:CustomHiddenField runat="server" ID="hidShipmentId" />
</asp:Content>
