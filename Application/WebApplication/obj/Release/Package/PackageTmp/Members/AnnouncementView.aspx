﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="AnnouncementView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.AnnouncementView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" OnNew="OnNewClick" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Announcements
                <small class="ml10">
                    <asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>
        <span class="clearfix"></span>
        <hr class="dark mb10" />

        <asp:Panel runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="Announcements"
                            ShowAutoRefresh="False" OnProcessingError="OnSearchProfilesProcessingError" OnSearchProfileSelected="OnSearchProfilesProfileSelected" />

                    </div>
                </div>
            </div>
            <hr class="dark mb10" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                        OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>

            <hr class="fat" />
            <div class="rowgroup">
                <div class="row ">
                    <div class="left">
                        <asp:Button ID="btnSearch" runat="server" Text="SEARCH" OnClick="OnSearchClicked"
                            CausesValidation="true" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheFailureCodeTable" TableId="announcmentTable" HeaderZIndex="2" />
        <div class="rowgroup">
            <table id="announcmentTable" class="line2 pl2">
                <tr>
                    <th style="width: 3%;">
                    </th>
                    <th style="width: 15%;" class="text-left pl10">Type
                    </th>
                    <th style="width: 69%;" class="text-left pl10">Name
                    </th>
                    <th style="width: 5%;" class="text-center">Enabled
                    </th>
                    <th style="width: 8%;" class="text-center">Action
                    </th>
                </tr>
                <asp:ListView ID="lvwRegistry" runat="server" ItemPlaceholderID="itemPlaceHolder">
                    <LayoutTemplate>
                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td rowspan="2" class="top">
                                <%# string.Format("{0}.", Container.DataItemIndex + 1)%>
                            </td>
                            <td>
                                <%# Eval("Type").FormattedString() %>
                            </td>
                            <td>
                                <eShip:CustomHiddenField ID="announcementId" runat="server" Value='<%# Eval("Id") %>' />
                                <%# Eval("Name").FormattedString() %>
                            </td>
                            <td class="text-center">
                                <%# Eval("Enabled").ToBoolean() ? WebApplicationConstants.HtmlCheck : string.Empty %>
                            </td>
                            <td class="text-center top" rowspan="2">
                                <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png"
                                    CausesValidation="false" OnClick="OnEditClicked" Enabled='<%# Access.Modify %>' />
                                <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                    CausesValidation="false" OnClick="OnDeleteClicked" Enabled='<%# Access.Remove %>' />
                                <ajax:ConfirmButtonExtender runat="server" ID="cbeDelete" TargetControlID="ibtnDelete"
                                    ConfirmText="Are you sure you want to delete record?" />
                            </td>
                        </tr>
                        <tr class="f9">
                            <td colspan="3" class="forceLeftBorder">
                                <table class="contain pl2">
                                    <tr class="no-bottom-border">
                                        <td class="blue no-border" style="width: 3%">Message:</td>
                                        <td class="no-border" style="width: 97%"><%# Eval("Message") %></td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </table>
        </div>
    </div>

    <asp:Panel ID="pnlEdit" runat="server" Visible="false">
        <div class=" popupControl popupControlOverW500">
            <div class="popheader">
                <h4>
                    <asp:Literal ID="litTitle" runat="server" />
                </h4>
                <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                    CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
            </div>
            <div class="row">
                <table class="poptable">
                    <tr>
                        <td colspan="20" class="failedLockLiteral">
                            <asp:Literal ID="litMessage" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right top">
                            <label class="upper">Name:</label>
                        </td>
                        <td>
                            <eShip:CustomTextBox ID="txtName" MaxLength="50" runat="server" CssClass="w200" />
                            <asp:RequiredFieldValidator runat="server" ID="rfvName" ControlToValidate="txtName"
                                ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right top">
                            <label class="upper">Announcement Type:</label>
                        </td>
                        <td>
                            <asp:HiddenField ID="hidAnnouncementId" runat="server" />
                            <asp:DropDownList DataTextField="Value" DataValueField="Key" ID="ddlAnnouncementTypes"
                                runat="server" CssClass="w200" />
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right top">
                            <label class="upper">
                                Message:
                            </label>
                            <br />
                            <label class="lhInherit">
                                <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDescription" TargetControlId="txtMessage" MaxLength="500" />
                            </label>
                        </td>
                        <td>
                            <eShip:CustomTextBox ID="txtMessage" MaxLength="500" runat="server" CssClass="w330 h150" TextMode="MultiLine" />
                            <asp:RequiredFieldValidator runat="server" ID="rfvCode" ControlToValidate="txtMessage"
                                ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:CheckBox ID="chkEnabled" runat="server" CssClass="jQueryUniform" />
                            <label class="upper">Enabled</label>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="pt10">
                            <asp:Button ID="btnSave" Text="Save" OnClick="OnSaveClick" runat="server" />
                            <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnCloseClicked" runat="server" CausesValidation="false" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
    <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information" OnOkay="OnOkayProcess" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackgroundControl" runat="server" Visible="false" />
</asp:Content>
