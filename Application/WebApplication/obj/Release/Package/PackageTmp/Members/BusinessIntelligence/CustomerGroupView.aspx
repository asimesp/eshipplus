﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="CustomerGroupView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.CustomerGroupView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %> 
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        ShowImport="false" ShowEdit="true" ShowDelete="true" ShowSave="true" ShowFind="true"
        OnFind="OnFindClicked" OnSave="OnSaveClicked" OnDelete="OnDeleteClicked" OnNew="OnNewClicked"
        OnEdit="OnEditClicked" OnImport="OnImportClicked" OnUnlock="OnUnlockClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="pageHeader">
        <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
        <h3>Customer Groups
            <eShip:RecordIdentityDisplayControl runat="server" ID="ridcRecordIdentity" TargetControlId="txtGroupName" />
        </h3>
        <div class="lockMessage">
            <asp:Literal ID="litMessage" runat="server" />
        </div>
    </div>

    <hr class="dark mb5" />
    <div class="errorMsgLit">
        <asp:Literal runat="server" ID="litErrorMessages" />
    </div>
    <eShip:CustomerGroupFinderControl ID="customerGroupFinder" runat="server" Visible="false" OpenForEditEnabled="true"
        OnItemSelected="OnCustomerGroupFinderItemSelected" OnSelectionCancel="OnCustomerGroupFinderSelectionCancelled"
        EnableMultiSelection="false" />
    <eShip:CustomerFinderControl ID="customerFinder" runat="server" Visible="false" OpenForEditEnabled="false"
        EnableMultiSelection="true" OnSelectionCancel="OnCustomerFinderSelectionCancelled"
        OnMultiItemSelected="OnCustomerFinderMultiItemSelected" />
    <eShip:AjaxTabHeaderTextUpdater runat="server" ID="athtuTabUpdater" />

    <eShip:CustomHiddenField runat="server" ID="hidCustomerGroupId" />
    <ajax:TabContainer ID="tabCustomerGroups" runat="server" CssClass="ajaxCustom">
        <ajax:TabPanel runat="server" ID="tabDetails" HeaderText="Details">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlDetails">
                    <div class="row">
                        <div class="fieldgroup mr20">
                            <label class="wlabel blue">Group Name</label>
                            <eShip:CustomTextBox ID="txtGroupName" runat="server" CssClass="w300" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="fieldgroup">
                            <label class="wlabel blue">
                                Description<eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDescription" TargetControlId="txtDescription" MaxLength="200" />
                            </label>
                            <eShip:CustomTextBox ID="txtDescription" runat="server" TextMode="MultiLine" CssClass="w500 h150" />
                        </div>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </ajax:TabPanel>
        <ajax:TabPanel runat="server" ID="tabCustomers" HeaderText="Customers">
            <ContentTemplate>
                <asp:UpdatePanel runat="server" ID="upPnlCustomers" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel runat="server" ID="pnlCustomers">
                            <div class="row mb10">
                                <div class="fieldgroup right">
                                    <asp:Button runat="server" ID="btnAddCustomer" CausesValidation="False" OnClick="OnAddCustomerClicked" Text="Add Customers" CssClass="mr10" />
                                    <asp:Button runat="server" ID="btnClearAllShipAs" Text="Clear Customers" CausesValidation="False" OnClick="OnClearCustomersClicked" />
                                </div>
                            </div>
                            <eShip:TableFreezeHeaderExtender runat="server" ID="tfhecustomerGroupTable" TableId="customerGroupTable" IgnoreHeaderBackgroundFill="True" />
                            <table id="customerGroupTable" class="stripe">
                                <tr>
                                    <th style="width: 50%;">Name
                                    </th>
                                    <th style="width: 30%;">Customer Number
                                    </th>
                                    <th style="width: 10%;" class="text-center">Active
                                    </th>
                                    <th style="width: 10%;" class="text-center">Action
                                    </th>
                                </tr>
                                <asp:ListView ID="lstCustomers" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                    <LayoutTemplate>
                                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <eShip:CustomHiddenField runat="server" ID="hidCustomerIndex" Value='<%# Container.DataItemIndex %>' />
                                            <td class="text-left">
                                                <eShip:CustomHiddenField ID="hidCustomerId" runat="server" Value='<%# Eval("Id") %>' />
                                                <asp:Literal ID="litCustomerName" Text='<%# Eval("Name") %>' runat="server" />
                                            </td>
                                            <td class="text-left">
                                                
                                                <asp:Label runat="server" ID="lblCustomerNumber" Text='<%# Eval("CustomerNumber") %>'/>
                                                 <%# ActiveUser.HasAccessTo(ViewCode.Customer) 
                                                        ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Customer Record'><img src={3} width='16' class='middle'/></a>", 
                                                                                ResolveUrl(CustomerView.PageAddress),
                                                                                WebApplicationConstants.TransferNumber, 
                                                                                Eval("Id").GetString().UrlTextEncrypt(),
                                                                                ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                        : string.Empty %>
                                            </td>
                                            <td class="text-center">
                                                <%# Eval("Active").ToBoolean() ?  WebApplicationConstants.HtmlCheck  : string.Empty%>
                                                <eShip:CustomHiddenField runat="server" ID="hidActive" Value='<%# Eval("Active").ToBoolean().GetString() %>' />
                                            </td>
                                            <td class="text-center">
                                                <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                    CausesValidation="false" OnClick="OnRemoveCustomerClicked" Enabled='<%# Access.Modify %>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnAddCustomer" />
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajax:TabPanel>
        <ajax:TabPanel runat="server" ID="tabAuditLog" HeaderText="Audit Log">
            <ContentTemplate>
                <eShip:AuditLogControl runat="server" ID="auditLogs" />
            </ContentTemplate>
        </ajax:TabPanel>
    </ajax:TabContainer>

    <script type="text/javascript">
        function ClearHidFlag() {
            jsHelper.SetEmpty('<%= hidFlag.ClientID %>');
        }
    </script>

    <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
        Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" />

    <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
        OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
</asp:Content>
