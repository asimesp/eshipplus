﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChartGenerator.ascx.cs"
    Inherits="LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls.ChartGenerator" %>

<%@ Import Namespace="System.Web.UI.DataVisualization.Charting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence" %>

<script type="text/javascript">
    $(document).ready(function() {
        (function(container) {
            var d = <%= Data.GetData() %>;
            var options = {
                selection: {
                    mode: '<%= Data.GetSelectionMode() %>',
                    fps: 60
                },
                title: '<%= Title %>',
                mouse: {
                    track: <%= (Data.CanMouseTrack()).ToString().ToLower() %>,
                    position: 'ne',
                    relative: true
                },
                pie: {
                    show: <%= (Data.ChartType == SeriesChartType.Pie).ToString().ToLower() %>,
                    explode: 6,
                    fill: true
                },
                xaxis: {
                    title: '<%= Data.ChartType != SeriesChartType.Pie ? XAxisTitle : string.Empty %>',
                    showLabels: <%= (Data.ChartType != SeriesChartType.Pie).ToString().ToLower() %>,
                    labelsAngle: 45,
                    mode: '<%= Data.GetXAxisMode() %>',
                    ticks: <%= Data.GenerateAxisTicks(ChartAxis.XAxis) %>,
                    tickDecimals: <%= Data.GetAxisTickDecimalPlaces(ChartAxis.XAxis) %>,
                },
                yaxis: {
                    title: '<%= Data.ChartType != SeriesChartType.Pie ? YAxisTitle : string.Empty %>',
                    showLabels: <%= (Data.ChartType != SeriesChartType.Pie).ToString().ToLower() %>,
                    ticks: <%= Data.GenerateAxisTicks(ChartAxis.YAxis) %>,
                    tickDecimals: <%= Data.GetAxisTickDecimalPlaces(ChartAxis.YAxis) %>,
                },
                grid: {
                    verticalLines: <%= (Data.ChartType != SeriesChartType.Pie).ToString().ToLower() %>,
                    horizontalLines: <%= (Data.ChartType != SeriesChartType.Pie).ToString().ToLower() %>
                    },
                legend: {
                    show: true,
                    position: 'ne',
                    backgroundColor: '#D2E8FF'
                },
                HtmlText: false
            };

            // Draw graph with default options, overwriting with passed options
            function drawGraph(opts) {

                // Clone the options, so the 'options' variable always keeps intact.
                var o = window.Flotr._.extend(window.Flotr._.clone(options), opts || {});

                // Return a new graph.
                return window.Flotr.draw(container, d, o);
            }

            // Actually draw the graph.
            drawGraph();

            // Hook into the 'flotr:select' event.
            window.Flotr.EventAdapter.observe(container, 'flotr:select', function(area) {

                // Draw graph with new area
                drawGraph({
                    xaxis: {
                        min: area.x1,
                        max: area.x2,
                        labelsAngle: 45,
                        mode: '<%= Data.GetXAxisMode() %>',
                        ticks: <%= Data.GenerateAxisTicks(ChartAxis.XAxis) %>,
                        tickDecimals: <%= Data.GetAxisTickDecimalPlaces(ChartAxis.XAxis) %>
                        },
                    yaxis: {
                        min: area.y1,
                        max: area.y2,
                        ticks: <%= Data.GenerateAxisTicks(ChartAxis.YAxis) %>,
                        tickDecimals: <%= Data.GetAxisTickDecimalPlaces(ChartAxis.YAxis) %>
                    }
                });
            });

            // When graph is clicked, draw the graph with default area.
            window.Flotr.EventAdapter.observe(container, 'flotr:click', function() {
                drawGraph();
            });
        })(document.getElementById('<%= chart.ClientID %>'));
    });
</script>

<div id="chart" class="flotr2Chart" runat="server"></div>


