﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="VendorPerformanceSummaryView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.VendorPerformanceSummaryView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.Registry" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %> 
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="false"
        ShowEdit="false" ShowDelete="false" ShowSave="false" ShowFind="false" ShowMore="false"
        ShowExport="false" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">

    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                CssClass="pageHeaderIcon" />
            <h3>Vendor Performance Summary
            <small class="ml10">
                <asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>
        <span class="clearfix"></span>
        <hr class="dark mb10" />

        <div class="errorMsgLit">
            <asp:Literal runat="server" ID="litErrorMessages" />
        </div>

        <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
        <eShip:VendorFinderControl EnableMultiSelection="False" ID="vendorFinder" OnItemSelected="OnVendorFinderItemSelected"
            OnlyActiveResults="False" OnSelectionCancel="OnVendorFinderItemCancelled" OpenForEditEnabled="false"
            runat="server" Visible="false" />
        <eShip:CustomerFinderControl ID="customerFinder" runat="server" EnableMultiSelection="false"
            OnlyActiveResults="False" Visible="False" OpenForEditEnabled="false" OnSelectionCancel="OnCustomerFinderSelectionCancelled"
            OnItemSelected="OnCustomerFinderItemSelected" />

        <asp:Panel runat="server" ID="pnlReportParameters" DefaultButton="btnGetVendorStatistics">
            <div class="rowgroup">
                <div class="col_1_2 bbox vlinedarkright">
                    <table>
                        <tr>
                            <td class="text-right">
                                <label class="upper blue">Customer: </label>
                            </td>
                            <td class="text-left">
                                <eShip:CustomHiddenField ID="hidCustomerId" runat="server" />
                                <eShip:CustomTextBox ID="txtCustomerNumber" runat="server" AutoPostBack="True" OnTextChanged="OnCustomerNumberEntered" CssClass="w100" />
                                <asp:ImageButton ID="ibtnFindCustomer" runat="server" CausesValidation="False" ImageUrl="~/images/icons2/search_dark.png" OnClick="OnCustomerSearchClicked" />
                                <eShip:CustomTextBox ID="txtCustomerName" runat="server" CssClass="w200 disabled" ReadOnly="True" />
                                <ajax:AutoCompleteExtender runat="server" ID="aceCustomer" TargetControlID="txtCustomerNumber" ServiceMethod="GetCustomerList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                    EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                            </td>
                        </tr>
                        <asp:Panel runat="server" ID="pnlVendor">
                            <tr>
                                <td class="text-right">
                                    <label class="upper blue">Vendor: </label>
                                </td>
                                <td class="text-left">
                                    <eShip:CustomHiddenField runat="server" ID="hidVendorId" />
                                    <eShip:CustomTextBox runat="server" ID="txtVendorNumber" CssClass="w100" OnTextChanged="OnVendorNumberEntered" AutoPostBack="True" />
                                    <asp:ImageButton ID="imgVendorSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png" CausesValidation="False" OnClick="OnVendorSearchClicked" />
                                    <eShip:CustomTextBox runat="server" ID="txtVendorName" CssClass="w200 disabled" ReadOnly="True" />
                                    <ajax:AutoCompleteExtender runat="server" ID="aceVendor" TargetControlID="txtVendorNumber" ServiceMethod="GetVendorList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                        EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                                </td>
                            </tr>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlVendorBlank" Visible="False">
                            <tr>
                                <td colspan="2">&nbsp;
                                </td>
                            </tr>
                        </asp:Panel>
                        <tr>
                            <td class="text-right">
                                <label class="upper blue">Start Shipment Date: </label>
                            </td>
                            <td class="AjaxCalendarTable">
                                <eShip:CustomTextBox runat="server" ID="txtStartShipmentDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper blue">End Shipment Date: </label>
                            </td>
                            <td class="AjaxCalendarTable">
                                <eShip:CustomTextBox runat="server" ID="txtEndShipmentDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col_1_2 bbox pl46">
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $(jsHelper.AddHashTag('<%= chkAllModes.ClientID%>')).click(function () {
                                if (jsHelper.IsChecked('<%= chkAllModes.ClientID %>')) {
                                    jsHelper.UnCheckBox('<%= chkLTL.ClientID %>');
                                jsHelper.UnCheckBox('<%= chkFTL.ClientID %>');
                                jsHelper.UnCheckBox('<%= chkAir.ClientID %>');
                                jsHelper.UnCheckBox('<%= chkRail.ClientID %>');
                                jsHelper.UnCheckBox('<%= chkSP.ClientID %>');
                                $.uniform.update();
                            }
                            });

                            $(jsHelper.AddHashTag('<%= chkLTL.ClientID%>')).click(function () {
                                if (jsHelper.IsChecked('<%= chkLTL.ClientID %>')) {
                                jsHelper.UnCheckBox('<%= chkAllModes.ClientID %>');
                                $.uniform.update();
                            }
                        });

                            $(jsHelper.AddHashTag('<%= chkFTL.ClientID%>')).click(function () {
                                if (jsHelper.IsChecked('<%= chkFTL.ClientID %>')) {
                                jsHelper.UnCheckBox('<%= chkAllModes.ClientID %>');
                                $.uniform.update();
                            }
                        });

                            $(jsHelper.AddHashTag('<%= chkAir.ClientID%>')).click(function () {
                                if (jsHelper.IsChecked('<%= chkAir.ClientID %>')) {
                                jsHelper.UnCheckBox('<%= chkAllModes.ClientID %>');
                                $.uniform.update();
                            }
                        });

                            $(jsHelper.AddHashTag('<%= chkRail.ClientID%>')).click(function () {
                                if (jsHelper.IsChecked('<%= chkRail.ClientID %>')) {
                                jsHelper.UnCheckBox('<%= chkAllModes.ClientID %>');
                                $.uniform.update();
                            }
                        });

                            $(jsHelper.AddHashTag('<%= chkSP.ClientID%>')).click(function () {
                                if (jsHelper.IsChecked('<%= chkSP.ClientID %>')) {
                                jsHelper.UnCheckBox('<%= chkAllModes.ClientID %>');
                                $.uniform.update();
                            }
                        });
                        });
                    </script>
                    <div class="row mb20">
                        <div class="fieldgroup">
                            <asp:CheckBox runat="server" ID="chkAllModes" CssClass="jQueryUniform" />
                            <label>All Modes</label>
                        </div>
                    </div>
                    <ul class="twocol_list">
                        <li>
                            <asp:CheckBox runat="server" ID="chkLTL" CssClass="jQueryUniform" />
                            <label>Less Than Truck Load</label>
                        </li>
                        <li>
                            <asp:CheckBox runat="server" ID="chkFTL" CssClass="jQueryUniform" />
                            <label>Truckload</label>
                        </li>
                        <li>
                            <asp:CheckBox runat="server" ID="chkSP" CssClass="jQueryUniform" />
                            <label>Small Package</label>
                        </li>
                        <li>
                            <asp:CheckBox runat="server" ID="chkRail" CssClass="jQueryUniform" />
                            <label>Rail</label>
                        </li>
                        <li>
                            <asp:CheckBox runat="server" ID="chkAir" CssClass="jQueryUniform" />
                            <label>Air</label>
                        </li>
                    </ul>
                </div>
            </div>
            <hr class="fat" />
            <div class="row mb20">
                <div class="row">
                    <div class="right">
                        <asp:Button runat="server" ID="btnGetVendorStatistics" Text="Load Vendors Performance Summary" OnClick="OnGetVendorStatisticsClicked" CssClass="mr10" />
                        <asp:Button runat="server" ID="btnExportVendorStatistics" Text="Export Vendors Performance Summary" OnClick="OnExportVendorStatisticsClicked" />
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
    <eShip:TableFreezeHeaderExtender runat="server" ID="tfheVendorPerformanceSummaryTable" TableId="vendorPerformanceSummaryTable" HeaderZIndex="2" />
    <asp:Repeater runat="server" ID="rptData" OnItemDataBound="OnDataItemDataBound">
        <ItemTemplate>
            <h5><%# Eval("Key").ToInt().ToEnum<ServiceMode>() == ServiceMode.NotApplicable ? AllModes :  Eval("Key").FormattedString() %></h5>
            <eShip:CustomHiddenField runat="server" Id="hidServiceMode" Value='<%# Eval("Key") %>'/>
            <table class="line2" id="vendorPerformanceSummaryTable">
                <tr>
                    <th style="width: 20%" class="pl10">Vendor Name
                    </th>
                    <th>Vendor
					<abbr title="Number">#</abbr>
                    </th>
                    <th>Vendor Scac
                    </th>
                    <th class="text-right">Total Loads
                    </th>
                    <th class="text-right">Considered
                    </th>
                    <th class="text-right">Overall On-Time
					<abbr title="Percentage">%</abbr>
                    </th>
                    <th class="text-right">Pickup On-Time
					<abbr title="Percentage">%</abbr>
                    </th>
                    <th class="text-right">Delivery On-Time
					<abbr title="Percentage">%</abbr>
                    </th>
                    <th class="text-right">Accepted Tender
					<abbr title="Percentage">%</abbr>
                    </th>
                    <th class="text-right <%# ActiveUser.TenantEmployee ? string.Empty : "hidden" %>">
                        <abbr title="Estimated cost used when actual cost is not yet available">Total Cost ($)</abbr>
                    </th>
                    <th class="text-right">Mileage
                    </th>
                </tr>
                <asp:Repeater runat="server" ID="rptModeDetails">
                    <ItemTemplate>
                        <tr>
                            <td class="pl10">
                                <%# Eval("Name") %>
                                <asp:ImageButton runat="server" ID="ibtnDisplayVendorPerformance" OnClick="OnDisplayVendorPerformanceClicked" ImageUrl="~/images/icons2/statsBlue.png" ToolTip="See Vendor Performance Statistics" />
                            </td>
                            <td>
                                <eShip:CustomHiddenField runat="server" Id="hidVendorId" Value='<%# Eval("VendorId") %>'/>

                                <%# Eval("VendorNumber") %>
                                <%# ActiveUser.HasAccessTo(ViewCode.Vendor) 
                                        ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Vendor Record'><img src={3} width='16' class='middle'/></a>", 
                                            ResolveUrl(VendorView.PageAddress),
                                            WebApplicationConstants.TransferNumber, 
                                            Eval("VendorId").GetString().UrlTextEncrypt(),
                                            ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                        : string.Empty %>
                            </td>
                            <td>
                                <%# Eval("Scac")%>
                            </td>
                            <td class="text-right">
                                <%# Eval("TotalOverallLoadCount", "{0:n0}")%>
                            </td>
                            <td class="text-right">
                                <%# Eval("TotalNonExcludedShipments", "{0:n0}")%>
                            </td>
                            <td class="text-right">
                                <%# Eval("PercentOnTimeOverall", "{0:n2}")%>
                            </td>
                            <td class="text-right">
                                <%# Eval("PercentOnTimePickup", "{0:n2}")%>
                            </td>
                            <td class="text-right">
                                <%# Eval("PercentOnTimeDelivery", "{0:n2}")%>
                            </td>
                            <td class="text-right">
                                <%# Eval("PercentAcceptedTenders", "{0:n2}")%>
                            </td>
                            <td class="text-right <%# ActiveUser.TenantEmployee ? string.Empty : "hidden" %>">
                                <%# Eval("TotalAmountPaid", "{0:n2}")%>
                            </td>
                            <td class="text-right">
                                <%# Eval("TotalMileage", "{0:n2}")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </ItemTemplate>
    </asp:Repeater>
    <eShip:VendorPerformanceSummaryControl runat="server" ID="vpscVendorPerformance" Visible="False" OnCancel="OnVendorPerformanceClose" CanGetInDepthAnalysis="False"/>
</asp:Content>
