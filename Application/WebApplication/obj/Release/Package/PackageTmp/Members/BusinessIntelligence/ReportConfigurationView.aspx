﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="ReportConfigurationView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.ReportConfigurationView"
    EnableEventValidation="false" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.BusinessIntelligence" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor.Reports" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Administration" %> 
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        ShowEdit="true" ShowDelete="true" ShowSave="true" ShowFind="true" ShowMore="false"
        OnFind="OnToolbarFindClicked" OnSave="OnToolbarSaveClicked" OnDelete="OnToolbarDeleteClicked"
        OnNew="OnToolbarNewClicked" OnEdit="OnToolbarEditClicked" OnCommand="OnToolbarCommand" OnUnlock="OnToolbarUnlockClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Report Configurations
                <eShip:RecordIdentityDisplayControl runat="server" ID="ridcRecordIdentity" TargetControlId="txtConfigurationName" />
            </h3>

            <div class="lockMessage">
                <asp:Literal ID="litMessage" runat="server" />
            </div>
        </div>
        <hr class="dark mb5" />
        <div class="errorMsgLit">
            <asp:Literal runat="server" ID="litErrorMessages" />
        </div>
        <eShip:ReportConfigurationFinderControl runat="server" ID="reportConfigurationFinder" Visible="false"
            OpenForEditEnabled="true" EnableMultiSelection="false" OnItemSelected="OnReportConfigurationFinderItemSelected"
            OnSelectionCancel="OnReportConfigurationFinderItemCancelled" />
        <eShip:UserFinderControl ID="userFinder" runat="server" Visible="false" OpenForEditEnabled="false" OnMultiItemSelected="OnUserFinderMultiItemSelected"
            OnSelectionCancel="OnUserFinderSelectionCancelled" OnItemSelected="OnUserFinderSingleItemSelected" />
        <eShip:GroupFinderControl ID="groupFinder" runat="server" Visible="false" OpenForEditEnabled="false" OnMultiItemSelected="OnGroupFinderMultiItemSelected"
                             OnSelectionCancel="OnGroupFinderSelectionCancelled"/>

        <script type="text/javascript">
            function ClearHidFlag() {
                jsHelper.SetEmpty('<%= hidFlag.ClientID %>');
            }
        </script>

        <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
            Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" OnYes="OnMessageBoxYesClicked" OnNo="OnMessageBoxNoClicked" />
        <eShip:AjaxTabHeaderTextUpdater runat="server" ID="athtuTabUpdaterMain" />
        <eShip:AjaxTabHeaderTextUpdater runat="server" ID="athtuTabUpdaterSorting" />
        <eShip:AjaxTabHeaderTextUpdater runat="server" ID="athtuTabUpdaterPivots" />
        <eShip:AjaxTabHeaderTextUpdater runat="server" ID="athtuTabUpdaterSummaries" />
        <eShip:AjaxTabHeaderTextUpdater runat="server" ID="athtuTabUpdaterParameters" />
        <eShip:AjaxTabHeaderTextUpdater runat="server" ID="athtuTabUpdaterCharts" />

        <eShip:CustomHiddenField ID="hidReportTemplateId" runat="server" />
        <eShip:CustomHiddenField ID="hidReportConfigurationId" runat="server" />

        <ajax:TabContainer ID="tabReportConfiguration" runat="server" CssClass="ajaxCustom">
            <ajax:TabPanel runat="server" ID="tabDetails" HeaderText="Details">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlDetails">
                        <div class="rowgroup">
                            <div class="col_1_2 bbox vlinedarkright">

                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Configuration Name</label>
                                        <eShip:CustomTextBox runat="server" ID="txtConfigurationName" MaxLength="50" CssClass="w420" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Configuration Description
                                        <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDescription" TargetControlId="txtConfigurationDescription" MaxLength="500" />
                                        </label>
                                        <eShip:CustomTextBox runat="server" ID="txtConfigurationDescription" CssClass="w420 h150"
                                            TextMode="MultiLine" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Last Run Date</label>
                                        <eShip:CustomTextBox runat="server" ID="txtLastRunDate" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Visibility</label>
                                        <asp:DropDownList runat="server" ID="ddlReportConfigurationVisibility" CssClass="w200"
                                            DataTextField="Text" DataValueField="Value" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Owner</label>
                                        <eShip:CustomTextBox runat="server" ID="txtCreatedBy" CssClass="w200 disabled" Enabled="False" />
                                        <eShip:CustomHiddenField runat="server" ID="hidReportUserId" />
                                        <asp:ImageButton runat="server" ID="imgFindUser" ToolTip="Find new user." Enabled="false"
                                            ImageUrl="~/images/icons2/search_dark.png" CausesValidation="False" OnClick="OnFindNewReportUserClicked" />
                                    </div>
                                </div>
                                <div class="row pt20 pb20">
                                    <div class="fieldgroup mr20">
                                        <asp:CheckBox ID="chkQlikConfiguration" runat="server" CssClass="jQueryUniform" AutoPostBack="True" OnCheckedChanged="OnQlikConfigurationCheckChanged" />
                                        <label>Qlik Configuration</label>
                                    </div>
                                    <div class="fieldgroup mr20">
                                        <asp:CheckBox ID="chkHideRawDataTable" runat="server" CssClass="jQueryUniform" />
                                        <label>Hide Raw Data Table</label>
                                    </div>
                                </div>
                                <asp:Panel runat="server" ID="pnlCustomerGroups" Visible="False">
                                    <div class="row">
                                        <div class="fieldgroup mr20">
                                            <label class="wlabel">Customer Group</label>
                                            <asp:DropDownList ID="ddlCustomerGroup" runat="server" CssClass="w200" DataValueField="Value"
                                                DataTextField="Text" />
                                        </div>
                                        <div class="fieldgroup pt26">
                                            <asp:CheckBox runat="server" ID="chkReadonlyCustomerGroupFilter" CssClass="jQueryUniform" />
                                            <label>Read Only</label>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:Panel runat="server" ID="pnlVendorGroups" Visible="False">
                                    <div class="row">
                                        <div class="fieldgroup mr20">
                                            <label class="wlabel">Vendor Group</label>
                                            <asp:DropDownList ID="ddlVendorGroup" runat="server" CssClass="w200" DataValueField="Value"
                                                DataTextField="Text" />
                                        </div>
                                        <div class="fieldgroup pt26">
                                            <asp:CheckBox runat="server" ID="chkReadonlyVendorGroupFilter" CssClass="jQueryUniform" />
                                            <label>Read Only</label>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                            <asp:Panel ID="pnlTemplateInfo" runat="server" CssClass="col_1_2 bbox pl46">
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Template Name</label>
                                        <eship:customtextbox runat="server" id="txtReportTemplateName" cssclass="w360 disabled" readonly="true" />
                                        <asp:ImageButton runat="server" ID="imgFindTemplate" ToolTip="Find new template." Enabled="true"
                                            ImageUrl="~/images/icons2/search_dark.png" CausesValidation="False" OnClick="OnFindReportTemplateClicked" />
                                        <asp:ImageButton runat="server" ID="imgClearTemplate" ToolTip="Clear data." Enabled="true"
                                            ImageUrl="~/images/icons2/deleteX.png" CausesValidation="False" OnClick="OnClearTemplateClicked"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Template Description</label>
                                        <eship:customtextbox runat="server" id="txtReportTemplateDescription" cssclass="w420 h150 disabled" textmode="MultiLine" readonly="True" />
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabUsersAndGroups" HeaderText="Users and Groups">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlUsers" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlUsers">
                                <div class="row pb10">
                                    <div class="fieldgroup">
                                        <h5 class="p_m0">Users</h5>
                                    </div>
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddUser" Text="  Add Users   " CssClass="mr10 w100" OnClick="OnAddUserClicked" CausesValidation="False" />
                                        <asp:Button runat="server" ID="btnClearAllUsers" Text="  Clear Users   " CausesValidation="false" OnClick="OnClearUsersClicked"
                                                    CssClass="w100" />
                                    </div>
                                </div>

                                <table class="stripe" id="reportConfigurationUsersTable">
                                    <tr>
                                        <th style="width: 25%">Username
                                        </th>
                                        <th style="width: 30%">Name
                                        </th>
                                        <th style="width: 35%">Email
                                        </th>
                                        <th style="width: 10%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstUsers" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField runat="server" ID="hidUserId" Value='<%# Eval("Id") %>' />
                                                    
                                                    <asp:Label runat="server" ID="lblUsername" Text='<%# Eval("Username") %>' />
                                                    <%# ActiveUser.HasAccessTo(ViewCode.User) 
                                                        ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To User Record'><img src={3} width='16' class='middle'/></a>", 
                                                                                ResolveUrl(UserView.PageAddress),
                                                                                WebApplicationConstants.TransferNumber, 
                                                                                Eval("Id").GetString().UrlTextEncrypt(),
                                                                                ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                        : string.Empty %>
                                                
                                                </td>
                                                <td>
                                                    <asp:Literal runat="server" ID="litFirstName" Text='<%# Eval("FirstName") %>' />
                                                    <asp:Literal runat="server" ID="litLastName" Text='<%# Eval("LastName") %>' />
                                                </td>
                                                <td>
                                                    <asp:Literal runat="server" ID="litEmail" Text='<%# Eval("Email") %>' />
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnDeleteUser" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        OnClick="OnDeleteUserClicked" CausesValidation="False" />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                                
                                <div class="row pb10 pt40">
                                    <div class="fieldgroup">
                                        <h5 class="p_m0">Groups</h5>
                                    </div>
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddGroup" Text="Add Groups" CssClass="mr10 w100" OnClick="OnAddGroupClicked" CausesValidation="False" />
                                        <asp:Button runat="server" ID="btnClearAllGroups" Text="Clear Groups" CausesValidation="false" OnClick="OnClearGroupsClicked"
                                                    CssClass="w100"/>
                                    </div>
                                </div>
                                
                                <table class="stripe" id="reportConfigurationGroupsTable">
                                    <tr>
                                        <th style="width: 90%">Name
                                        </th>
                                        <th style="width: 10%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstGroups" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField runat="server" ID="hidGroupId" Value='<%# Eval("Id") %>' />
                                                    <asp:Literal runat="server" ID="litGroupName" Text='<%# Eval("Name") %>' />
                                                    <%# ActiveUser.HasAccessTo(ViewCode.Group) 
                                                            ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Group Record'><img src={3} width='16' class='middle'/></a>", 
                                                                ResolveUrl(GroupView.PageAddress),
                                                                WebApplicationConstants.TransferNumber, 
                                                                Eval("Id").GetString().UrlTextEncrypt(),
                                                                ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                            : string.Empty %>
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnDeleteUser" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        OnClick="OnDeleteGroupClicked" CausesValidation="False" />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnAddUser" />
                            <asp:PostBackTrigger ControlID="btnAddGroup" />
                        </Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabQlikSheets" HeaderText="Qlik Sheets" Visible="False">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlQlikSheets" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlQlikSheets">
                                <div class="row mb10">
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddQlikSheet" Text="Add Sheet" CssClass="mr10" CausesValidation="false" OnClick="OnAddQlikSheetClicked" />
                                        <asp:Button runat="server" ID="btnClearQlikSheets" Text="Clear Sheets" CausesValidation="false" OnClick="OnClearQlikSheetsClicked" />
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheQlikSheets" TableId="reportConfigurationDataQlikSheetTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <eShip:SortableExtender ID="seQlikSheet" runat="server" SortableElement="tr" SortableElementWrapperId="sortableQlikSheetTbody"
                                    DragHandleElementId="dataDrag" UpdatePanelToExtendId="upQlikSheets" Enabled="True" OnItemsReordered="OnQlikSheetsReordered" />
                                <asp:UpdatePanel runat="server" ID="upQlikSheets">
                                    <ContentTemplate>
                                        <table class="stripe" id="reportConfigurationDataQlikSheetTable">
                                            <thead>
                                                <tr>
                                                    <th style="width: 5%"></th>
                                                    <th style="width: 30%;">Name
                                                    </th>
                                                    <th style="width: 60%;">Link
                                                    </th>
                                                    <th style="width: 5%;" class="text-center">Action
                                                    </th>
                                            </thead>
                                            <asp:ListView ID="lstQlikSheets" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                                <LayoutTemplate>
                                                    <tbody id="sortableQlikSheetTbody">
                                                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                                    </tbody>
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <tr id='<%# Container.DataItemIndex + 1%>'>
                                                        <td class="top">
                                                            <div id="dataDrag" class="dragHandle"><%# Container.DataItemIndex + 1 %></div>
                                                            <eship:CustomHiddenField runat="server" ID="hidQlikSheetId" Value='<%# Eval("Id") %>'/>
                                                        </td>
                                                         <td class="top">
                                                            <eShip:CustomHiddenField ID="hidQlikSheetIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                            <eShip:CustomTextBox ID="txtQlikSheetName" runat="server" Text='<%# Eval("Name") %>' CssClass='w260' />
                                                        </td>
                                                        <td class="top">
                                                            <eShip:CustomTextBox runat="server" CssClass="w420" ID="txtQlikSheetLink" TextMode="MultiLine" Text='<%# Eval("Link") %>' />
                                                        </td>
                                                        <td class="text-center top">
                                                            <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                                CausesValidation="false" OnClick="OnDeleteQlikSheetClicked" Enabled='<%# Access.Modify %>' />
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabDataColumns" HeaderText="Columns">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlDataColumns" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlDataColumns">
                                <div class="row mb10">
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddSystemColumns" Text="Add System Columns" CssClass="mr10" CausesValidation="false" OnClick="OnAddSystemColumnsClicked" />
                                        <asp:Button runat="server" ID="btnAddCustomColumn" Text="Add Custom Column" CssClass="mr10" CausesValidation="false" OnClick="OnAddCustomColumnClicked" />
                                        <asp:Button runat="server" ID="btnClearAllColumns" Text="Clear Columns" CausesValidation="false" OnClick="OnClearDataColumnsClicked" />
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheReportConfigurationDataColumnsTable" TableId="reportConfigurationDataColumnsTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <eShip:SortableExtender ID="seDataColumns" runat="server" SortableElement="tr" SortableElementWrapperId="sortableDataColumnsTbody"
                                    DragHandleElementId="dataDrag" UpdatePanelToExtendId="udpDataColumns" Enabled="True" OnItemsReordered="OnDataColumnsItemsReordered" />
                                <asp:UpdatePanel runat="server" ID="udpDataColumns">
                                    <ContentTemplate>
                                        <table class="stripe" id="reportConfigurationDataColumnsTable">
                                            <thead>
                                                <tr>
                                                    <th style="width: 5%"></th>
                                                    <th style="width: 30%;">Name
                                                    </th>
                                                    <th style="width: 45%;">Column
                                                    </th>
                                                    <th style="width: 10%;">Data Type
                                                    </th>
                                                    <th style="width: 5%;">Custom
                                                    </th>
                                                    <th style="width: 5%;" class="text-center">Action
                                                    </th>
                                            </thead>
                                            <asp:ListView ID="lstDataColumns" runat="server" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnDataColumnsItemDataBound">
                                                <LayoutTemplate>
                                                    <tbody id="sortableDataColumnsTbody">
                                                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                                    </tbody>
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <tr id='<%# Container.DataItemIndex + 1%>'>
                                                        <td class="top">
                                                            <div id="dataDrag" class="dragHandle"><%# Container.DataItemIndex + 1 %></div>
                                                        </td>
                                                        <td class="top">
                                                            <eShip:CustomHiddenField ID="hidDataColumnIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                            <eShip:CustomTextBox ID="txtDataColumnName" runat="server" Text='<%# Eval("Name") %>' CssClass='<%# Eval("Custom").ToBoolean() ? "w260" : "w260 disabled" %>'
                                                                ReadOnly='<%# !Eval("Custom").ToBoolean() %>'  AutoPostBack="True" OnTextChanged="LoadCurrentViewPivots"  />
                                                        </td>
                                                        <td class="top">
                                                            <eShip:CustomTextBox runat="server" CssClass="w420" ID="txtDataColumnColumn" TextMode="MultiLine" Text='<%# Eval("Column") %>' Visible='<%# Eval("Custom").ToBoolean() %>' />
                                                        </td>
                                                        <td class="top">
                                                            <asp:DropDownList ID="ddlDataColumnDataType" DataTextField="Value" CssClass="w150" DataValueField="Key" runat="server" />
                                                        </td>
                                                        <td class="text-center top">
                                                            <%# Eval("Custom").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                                                            <eShip:CustomHiddenField ID="hidDataColumnCustom" runat="server" Value='<%# Eval("Custom").ToBoolean().GetString() %>' />
                                                        </td>
                                                        <td class="text-center top">
                                                            <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                                CausesValidation="false" OnClick="OnDeleteDataColumnClicked" Enabled='<%# Access.Modify %>' />
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnAddSystemColumns" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <asp:Panel runat="server" ID="pnlAddSystemColumns" Visible="false" CssClass="popup popupControlOverW500">
                        <div class="popheader">
                            <h4>System Columns<asp:ImageButton ID="ImageButton1" ImageUrl="~/images/icons2/close.png" CssClass="close"
                                CausesValidation="false" OnClick="OnCloseAddSystemColumnsClicked" runat="server" />
                            </h4>
                        </div>
                        <table class="poptable">
                            <tr>
                                <td colspan="2" class="p_m0">
                                    <eShip:TableFreezeHeaderExtender runat="server" ID="tfheParameterSelectorPermissionsTable" TableId="reportConfigurationSystemColumnTable" ScrollerContainerId="reportConfigurationSystemColumnScrollSection"
                                        ScrollOffsetControlId="pnlAddSystemColumns" IgnoreHeaderBackgroundFill="True" HeaderZIndex="1050" />
                                    <div class="finderScroll" id="reportConfigurationSystemColumnScrollSection">
                                        <table id="reportConfigurationSystemColumnTable" class="stripe sm_chk">
                                            <tr>
                                                <th style="width: 4%;">
                                                    <eShip:AltUniformCheckBox ID="chkSystemColumnSelectAll" runat="server" OnClientSideClicked="javascript:SetSelectedCheckBoxesStatus(this, 'reportConfigurationSystemColumnTable');" />
                                                </th>
                                                <th style="width: 70%;">Name
                                                </th>
                                                <th style="width: 26%;">Data Type
                                                </th>
                                            </tr>
                                            <asp:ListView ID="lstAddSystemColumns" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <eShip:AltUniformCheckBox ID="chkSelection" runat="server" OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'reportConfigurationSystemColumnTable', 'chkSystemColumnSelectAll');" />
                                                        </td>
                                                        <td>
                                                            <eShip:CustomHiddenField ID="hidSystemColumnIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                            <asp:Literal ID="litSystemColumnName" runat="server" Text='<%# Eval("Name") %>' />
                                                            <eShip:CustomHiddenField ID="hidSystemColumnColumnName" runat="server" Value='<%# Eval("Column") %>' />
                                                            <eShip:CustomHiddenField ID="hidSystemColumnCustom" runat="server" Value='<%# Eval("Custom") %>' />
                                                        </td>
                                                        <td>
                                                            <eShip:CustomHiddenField ID="hidSystemColumnDataType" runat="server" Value='<%# Eval("DataType") %>' />
                                                            <asp:Literal ID="litSystemColumnDataType" runat="server" Text='<%# Eval("DataType").FormattedString() %>' />
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <asp:Button Text="Add Selected" OnClick="OnAddSelectedSystemColumnsDoneClicked" runat="server" CausesValidation="false" />
                                    <asp:Button Text="Cancel" OnClick="OnCloseAddSystemColumnsClicked" runat="server" CausesValidation="false" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabSortColumns" HeaderText="Sorting">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlSortColumns" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlSortColumns">
                                <div class="row mb10">
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddSortColumns" Text="Add Sort Columns" CssClass="mr10" OnClick="OnAddSortColumnsClicked" CausesValidation="False" />
                                        <asp:Button runat="server" ID="btnClearAllSortColumns" Text="Clear Sort Columns" CausesValidation="false" OnClick="OnClearSortColumnsClicked" />
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheReportConfigurationSortColumnsTable" TableId="reportConfigurationSortColumnsTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <eShip:SortableExtender ID="seSortColumns" runat="server" SortableElement="tr" SortableElementWrapperId="sortableSortColumnsTbody"
                                    DragHandleElementId="sortDrag" UpdatePanelToExtendId="upSortColumns" Enabled="True" OnItemsReordered="OnSortColumnsItemsReordered" />
                                <asp:UpdatePanel runat="server" ID="upSortColumns">
                                    <ContentTemplate>
                                        <table class="stripe" id="reportConfigurationSortColumnsTable">
                                            <thead>
                                                <tr>
                                                    <th style="width: 5%">Priority</th>
                                                    <th style="width: 70%;">Name
                                                    </th>
                                                    <th style="width: 20%;">Sort Direction
                                                    </th>
                                                    <th style="width: 5%;" class="text-center">Action
                                                    </th>
                                                </tr>
                                            </thead>
                                            <asp:ListView ID="lstSortColumns" runat="server" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnSortColumnsItemDataBound">
                                                <LayoutTemplate>
                                                    <tbody id="sortableSortColumnsTbody">
                                                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                                    </tbody>
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <tr id='<%# Container.DataItemIndex + 1%>'>
                                                        <td class="top">
                                                            <div id="sortDrag" class="dragHandle"><%# Container.DataItemIndex + 1 %></div>
                                                        </td>
                                                        <td>
                                                            <eShip:CustomHiddenField ID="hidSortColumnIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                            <asp:Literal ID="litSortColumnName" runat="server" Text='<%# Eval("ReportColumnName") %>' />
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlSortColumnSortDirection" DataTextField="Value" CssClass="w150" DataValueField="Key" runat="server" />
                                                        </td>
                                                        <td class="text-center">
                                                            <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                                CausesValidation="false" OnClick="OnDeleteSortColumnClicked" Enabled='<%# Access.Modify %>' />
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnAddSortColumns" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <asp:Panel runat="server" ID="pnlEditSortColumns" Visible="false" CssClass="popup popupControlOverW500">
                        <div class="popheader">
                            <h4>Sort Columns<asp:ImageButton ID="ImageButton2" ImageUrl="~/images/icons2/close.png" CssClass="close"
                                CausesValidation="false" OnClick="OnCloseEditSortColumnsClicked" runat="server" />
                            </h4>
                        </div>
                        <table class="poptable">
                            <tr>
                                <td colspan="2" class="p_m0">
                                    <eShip:TableFreezeHeaderExtender runat="server" ID="TableFreezeHeaderExtender1" TableId="reportConfigurationSortColumnEditTable" ScrollerContainerId="reportConfigurationSortColumnEditScrollSection"
                                        ScrollOffsetControlId="pnlEditSortColumns" IgnoreHeaderBackgroundFill="True" HeaderZIndex="1050" />
                                    <div class="finderScroll" id="reportConfigurationSortColumnEditScrollSection">
                                        <table id="reportConfigurationSortColumnEditTable" class="stripe sm_chk">
                                            <tr>
                                                <th style="width: 4%;">
                                                    <eShip:AltUniformCheckBox ID="chkSortColumnSelectAll" runat="server" OnClientSideClicked="javascript:SetSelectedCheckBoxesStatus(this, 'reportConfigurationSortColumnEditTable');" />
                                                </th>
                                                <th style="width: 96%;">Name
                                                </th>
                                            </tr>
                                            <asp:ListView ID="lstEditSortColumns" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <eShip:AltUniformCheckBox ID="chkSelection" runat="server" OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'reportConfigurationSortColumnEditTable', 'chkSortColumnSelectAll');" />
                                                        </td>
                                                        <td>
                                                            <eShip:CustomHiddenField ID="hidSortColumnIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                            <asp:Literal ID="litSortColumnName" runat="server" Text='<%# Eval("Name") %>' />
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td class="text-left">
                                    <asp:Button ID="Button3" Text="Add Selected" OnClick="OnAddSelectedSortColumnsDoneClicked"
                                        runat="server" CausesValidation="false" />
                                    <asp:Button ID="Button4" Text="Cancel" OnClick="OnCloseEditSortColumnsClicked" runat="server"
                                        CausesValidation="false" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabSummaryColumns" HeaderText="Summaries">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlSummaryColumns" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlSummaryColumns">
                                <div class="row mb10">
                                    <div class="fieldgroup right">
                                        <asp:Button ID="btnAddSummaryColumn" runat="server" Text="Add Summary Columns" CssClass="mr10" CausesValidation="False" OnClick="OnAddSummaryColumnsClicked" />
                                        <asp:Button runat="server" ID="btnClearAllSummaryColumns" Text="Clear Summary Columns" CausesValidation="false" OnClick="OnClearSummaryColumnsClicked" />
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheReportConfigurationSummaryColumnsTable" TableId="reportConfigurationSummaryColumnsTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <eShip:SortableExtender ID="seSummaryColumns" runat="server" SortableElement="tr" SortableElementWrapperId="sortableSummaryColumnsTbody"
                                    DragHandleElementId="summaryDrag" UpdatePanelToExtendId="upSummaryColumns" Enabled="True" OnItemsReordered="OnSummaryColumnsItemsReordered" />
                                <asp:UpdatePanel runat="server" ID="upSummaryColumns">
                                    <ContentTemplate>
                                        <table class="stripe" id="reportConfigurationSummaryColumnsTable">
                                            <thead>
                                                <tr>
                                                    <th style="width: 5%">Order</th>
                                                    <th style="width: 90%;">Name
                                                    </th>
                                                    <th style="width: 5%;" class="text-center">Action
                                                    </th>
                                                </tr>
                                            </thead>
                                            <asp:ListView ID="lstSummaryColumns" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                                <LayoutTemplate>
                                                    <tbody id="sortableSummaryColumnsTbody">
                                                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                                    </tbody>
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <tr id='<%# Container.DataItemIndex + 1%>'>
                                                        <td class="top">
                                                            <div id="summaryDrag" class="dragHandle"><%# Container.DataItemIndex + 1 %></div>
                                                        </td>
                                                        <td>
                                                            <eShip:CustomHiddenField ID="hidSummaryColumnIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                            <asp:Literal ID="litSummaryColumnName" runat="server" Text='<%# Eval("Name") %>' />
                                                            <eShip:CustomHiddenField ID="hidSummaryColumnColumnName" runat="server" Value='<%# Eval("ReportColumnName") %>' />
                                                        </td>
                                                        <td class="text-center">
                                                            <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                                CausesValidation="false" OnClick="OnDeleteSummaryColumnClicked" Enabled='<%# Access.Modify %>' />
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnAddSummaryColumn" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <asp:Panel runat="server" ID="pnlEditSummaryColumns" Visible="false" CssClass="popup popupControlOverW500">
                        <div class="popheader">
                            <h4>Summary Columns<asp:ImageButton ID="ImageButton3" ImageUrl="~/images/icons2/close.png" CssClass="close"
                                CausesValidation="false" OnClick="OnCloseEditSummaryColumnsClicked" runat="server" />
                            </h4>
                        </div>
                        <table class="poptable">
                            <tr>
                                <td colspan="2" class="p_m0">
                                    <eShip:TableFreezeHeaderExtender runat="server" ID="TableFreezeHeaderExtender3" TableId="reportConfigurationSummaryColumnEditTable" ScrollerContainerId="reportConfigurationSummaryColumnEditScrollSection"
                                        ScrollOffsetControlId="pnlEditSummaryColumns" IgnoreHeaderBackgroundFill="True" HeaderZIndex="1050" />
                                    <div class="finderScroll" id="reportConfigurationSummaryColumnEditScrollSection">
                                        <table id="reportConfigurationSummaryColumnEditTable" class="stripe sm_chk">
                                            <tr>
                                                <th style="width: 4%;">
                                                    <eShip:AltUniformCheckBox ID="chkSummaryColumnSelectAll" runat="server" OnClientSideClicked="javascript:SetSelectedCheckBoxesStatus(this, 'reportConfigurationSummaryColumnEditTable');" />
                                                </th>
                                                <th style="width: 96%;">Name
                                                </th>
                                            </tr>
                                            <asp:ListView ID="lstEditSummaryColumns" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <eShip:AltUniformCheckBox ID="chkSelection" runat="server" OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'reportConfigurationSummaryColumnEditTable', 'chkSummaryColumnSelectAll');" />
                                                        </td>
                                                        <td>
                                                            <eShip:CustomHiddenField ID="hidSummaryColumnIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                            <asp:Literal ID="litSummaryColumnName" runat="server" Text='<%# Eval("Name") %>' />
                                                            <eShip:CustomHiddenField ID="hidSummaryColumnColumn" runat="server" Value='<%# Eval("ReportColumnName") %>' />
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td class="text-left">
                                    <asp:Button ID="Button5" Text="Add Selected" OnClick="OnAddSelectedSummaryColumnsDoneClicked"
                                        runat="server" CausesValidation="false" />
                                    <asp:Button ID="Button6" Text="Cancel" OnClick="OnCloseEditSummaryColumnsClicked" runat="server"
                                        CausesValidation="false" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabPivots" HeaderText="Pivots">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlPivots" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlPivots">
                                <div class="row mb10">
                                    <div class="rowgroup mb10">
                                        <div class="row">
                                            <div class="fieldgroup left">
                                                <eShip:PaginationExtender runat="server" ID="peLstPivots" TargetControlId="lstPivots"
                                                    PageSize="1" UseParentDataStore="False" OnPageChanging="LoadCurrentViewPivots" />
                                            </div>
                                            <div class="fieldgroup right">
                                                <asp:Button runat="server" ID="btnAddPivot" Text="Add Pivot Table" CssClass="mr10"
                                                    CausesValidation="false" OnClick="OnAddPivotClicked" />
                                                <asp:Button runat="server" ID="btnClearPivots" Text="Clear Pivot Tables" CausesValidation="false"
                                                    OnClick="OnClearPivotsClicked" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    // Enforce mutual exclusion of Pivot and Aggregate checkboxes
                                    function UncheckAggregateColumns(control) {
                                        $(control).parentsUntil("tbody").find("input[id*='_chkTypeAggregate'][type='checkbox']").not($(control))
                                            .each(function () {
                                                jsHelper.UnCheckBox($(this).attr("id"));
                                                SetAltUniformCheckBoxClickedStatus($(this));
                                            });
                                    }

                                    function UnCheckPivot(control) {
                                        $(control).parentsUntil("tbody").find("input[id*='_chkTypePivot'][type='checkbox']").not($(control))
                                            .each(function () {
                                                jsHelper.UnCheckBox($(this).attr("id"));
                                                SetAltUniformCheckBoxClickedStatus($(this));
                                            });
                                    }
                                </script>
                                <table class="stripe" id="reportConfigurationPivots">
                                    <tr>
                                        <th style="width: 96%">Pivot
                                        </th>
                                        <th style="width: 4%">Actions
                                        </th>
                                    </tr>
                                    <asp:ListView runat="server" ID="lstPivots" OnItemDataBound="OnPivotsItemDataBound">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidGuidId" runat="server" Value='<%# Eval("GuidId") %>'/>
                                                    <eShip:CustomHiddenField ID="hidPivotIndex" runat="server" Value='<%# Container.DataItemIndex + (peLstPivots.PageSize*(peLstPivots.CurrentPage-1)) %>' />
                                                    <label>Pivot Table Name: </label>
                                                    <eShip:CustomTextBox runat="server" ID="txtPivotTableName" MaxLength="32" CssClass="w220"
                                                        Text='<%# Eval("TableName") %>' OnTextChanged="OnChartsPageChanging" AutoPostBack="True" />
                                                </td>
                                                <td class="text-center top">
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        CausesValidation="false" OnClick="OnDeletePivotClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                            <tr class="hidden">
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <asp:UpdatePanel runat="server">
                                                    <ContentTemplate>
                                                        <td colspan="2">
                                                            <div class="row mt20 mb20">
                                                                <div class="col_1_3 bbox pl10">
                                                                    <div class="row pb16">
                                                                        <div class="fieldgroup right">
                                                                            <asp:Button ID="btnAddSelectedColumns" Text="Add Selected Columns" OnClick="OnAddSelectedPivotColumnsClicked"
                                                                                runat="server" CausesValidation="false" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="row" id="divPivotColumnsToAdd">
                                                                        <eShip:TableFreezeHeaderExtender runat="server" ID="tfhePivotColumnsToAdd"
                                                                            TableId="reportConfigurationPivotColumnsToAdd" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" ScrollerContainerId="pivotColumnsToAddScrollSection"
                                                                            ScrollOffsetControlId="divPivotColumnsToAdd" />
                                                                        <div class="finderScrollFixed" id="pivotColumnsToAddScrollSection">
                                                                            <table class="stripe sm_chk" id="reportConfigurationPivotColumnsToAdd">
                                                                                <thead>
                                                                                    <tr>
                                                                                        <th style="width: 5%;">
                                                                                            <eShip:AltUniformCheckBox ID="chkSelectAllPivotColumns" runat="server"
                                                                                                OnClientSideClicked="javascript:SetSelectedCheckBoxesStatus(this, 'reportConfigurationPivotColumnsToAdd');" />
                                                                                        </th>
                                                                                        <th style="width: 85%">Column Name
                                                                                        </th>
                                                                                        <th style="width: 10%">Data Type
                                                                                        </th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <asp:ListView ID="lstPivotColumnsToAdd" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                                                                    <LayoutTemplate>
                                                                                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                                                                    </LayoutTemplate>
                                                                                    <ItemTemplate>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <eShip:AltUniformCheckBox runat="server" ID="chkSelection" OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'reportConfigurationPivotColumnsToAdd', 'chkSelectAllPivotColumns');" />
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:Literal runat="server" ID="litColumnName" Text='<%# Eval("ReportColumnName") %>' />
                                                                                            </td>
                                                                                            <td>
                                                                                                <eShip:CustomHiddenField runat="server" ID="hidDataType" Value='<%# Eval("DataType").ToInt() %>' />
                                                                                                <asp:Literal runat="server" ID="litDataType" Text='<%# Eval("DataType").ToInt().ToEnum<SqlDbType>().ToString() %>' />
                                                                                            </td>
                                                                                        </tr>
                                                                                    </ItemTemplate>
                                                                                </asp:ListView>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col_2_3 bbox pl20 pr20">
                                                                    <div class="row">
                                                                        <eShip:SortableExtender ID="sePivotColumns" runat="server" SortableElement="tr" SortableElementWrapperId="sortablePivotColumnsTbody"
                                                                            DragHandleElementId="pivotDrag" UpdatePanelToExtendId="upPivotColumns" Enabled="True" OnItemsReordered="OnPivotColumnsItemsReordered" />

                                                                        <div class="row">
                                                                            <div class="fieldgroup">
                                                                                <h3 class="pl40 pb16">Pivot Table Columns</h3>
                                                                            </div>
                                                                            <div class="fieldgroup right">
                                                                                <asp:Button runat="server" ID="btnClearPivotColumns" Text="Clear All" CausesValidation="false"
                                                                                    OnClick="OnClearPivotColumnsClicked" />
                                                                            </div>
                                                                        </div>

                                                                        <asp:UpdatePanel runat="server" ID="upPivotColumns" UpdateMode="Always">
                                                                            <ContentTemplate>
                                                                                <table class="stripe" id="reportConfigurationPivotColumnsTable">
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <th style="width: 5%">Order
                                                                                            </th>
                                                                                            <th style="width: 70%">Name
                                                                                            </th>
                                                                                            <th style="width: 3%">Data Type
                                                                                            </th>
                                                                                            <th style="width: 3%">
                                                                                                <abbr title="Pivot On">Pvt</abbr>
                                                                                            </th>
                                                                                            <th style="width: 3%">Min
                                                                                            </th>
                                                                                            <th style="width: 3%">Max
                                                                                            </th>
                                                                                            <th style="width: 3%">Sum
                                                                                            </th>
                                                                                            <th style="width: 3%">
                                                                                                <abbr title="Average">Avg</abbr>
                                                                                            </th>
                                                                                            <th style="width: 3%">
                                                                                                <abbr title="Count">Cnt</abbr>
                                                                                            </th>
                                                                                            <th style="width: 4%">Action
                                                                                            </th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <asp:ListView ID="lstPivotColumns" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                                                                        <LayoutTemplate>
                                                                                            <tbody id="sortablePivotColumnsTbody">
                                                                                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                                                                            </tbody>
                                                                                        </LayoutTemplate>
                                                                                        <ItemTemplate>

                                                                                            <tr id='<%# Container.DataItemIndex + 1%>'>
                                                                                                <td class="top">
                                                                                                    <div id="pivotDrag" class="dragHandle"><%# Container.DataItemIndex + 1 %></div>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <eShip:CustomHiddenField runat="server" ID="hidColumnType" Value='<%# Eval("ColumnType").ToInt() %>' />
                                                                                                    <eShip:CustomHiddenField runat="server" ID="hidColumnIndex" Value='<%# Container.DataItemIndex %>' />
                                                                                                    <asp:Literal runat="server" ID="litColumnName" Text='<%# Eval("ReportColumnName") %>' />
                                                                                                </td>
                                                                                                <td>
                                                                                                    <eShip:CustomHiddenField runat="server" ID="hidDataType" Value='<%# Eval("DataType").ToInt() %>' />
                                                                                                    <asp:Literal runat="server" ID="litDataType" Text='<%# Eval("DataType").ToInt().ToEnum<SqlDbType>().ToString() %>' />
                                                                                                </td>
                                                                                                <td>
                                                                                                    <eShip:AltUniformCheckBox runat="server" ID="chkTypePivot" OnClientSideClicked="UncheckAggregateColumns(this);"
                                                                                                        Checked='<%# Eval("ColumnType").ToInt().ToEnum<ColumnType>() == ColumnType.Pivot %>' OnCheckedChanged="OnPivotCheckboxChanged" AutoPostBack="True" />
                                                                                                </td>
                                                                                                <td>
                                                                                                    <eShip:AltUniformCheckBox runat="server" ID="chkTypeAggregateMin" OnClientSideClicked="UnCheckPivot(this);" Checked='<%# Eval("MinAggregate").ToBoolean() %>'
                                                                                                        Enabled='<%# Eval("DataType").ToInt().IsNumericOrDateTime() %>' OnCheckedChanged="OnAggregateCheckboxChanged" AutoPostBack="True" />
                                                                                                </td>
                                                                                                <td>
                                                                                                    <eShip:AltUniformCheckBox runat="server" ID="chkTypeAggregateMax" OnClientSideClicked="UnCheckPivot(this);" Checked='<%# Eval("MaxAggregate").ToBoolean() %>'
                                                                                                        Enabled='<%# Eval("DataType").ToInt().IsNumericOrDateTime() %>' OnCheckedChanged="OnAggregateCheckboxChanged" AutoPostBack="True" />
                                                                                                </td>

                                                                                                <td>
                                                                                                    <eShip:AltUniformCheckBox runat="server" ID="chkTypeAggregateSum" OnClientSideClicked="UnCheckPivot(this);" Checked='<%# Eval("SumAggregate").ToBoolean() %>'
                                                                                                        Enabled='<%# Eval("DataType").ToInt().IsNumeric() %>' OnCheckedChanged="OnAggregateCheckboxChanged" AutoPostBack="True" />
                                                                                                </td>
                                                                                                <td>
                                                                                                    <eShip:AltUniformCheckBox runat="server" ID="chkTypeAggregateAverage" OnClientSideClicked="UnCheckPivot(this);" Checked='<%# Eval("AvgAggregate").ToBoolean() %>'
                                                                                                        Enabled='<%# Eval("DataType").ToInt().IsNumeric() %>' OnCheckedChanged="OnAggregateCheckboxChanged" AutoPostBack="True" />
                                                                                                </td>
                                                                                                <td>
                                                                                                    <eShip:AltUniformCheckBox runat="server" ID="chkTypeAggregateCount" OnClientSideClicked="UnCheckPivot(this);"
                                                                                                        Checked='<%# Eval("CntAggregate").ToBoolean() %>' OnCheckedChanged="OnAggregateCheckboxChanged" AutoPostBack="True" />
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                                                                        CausesValidation="false" OnClick="OnDeletePivotColumnClicked" Enabled='<%# Access.Modify %>' />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </ItemTemplate>
                                                                                    </asp:ListView>
                                                                                </table>
                                                                            </ContentTemplate>
                                                                        </asp:UpdatePanel>
                                                                    </div>
                                                                </div>
                                                        </td>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="btnAddSelectedColumns"  />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabParameters" HeaderText="Parameters">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlParameterColumns" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlParameterColumns">
                                <div class="row mb10">
                                    <div class="fieldgroup">
                                        <p class="note">
                                            &#8226; Note: You can only setup parameter filters for selected data columns. Removal of a data column removes the corresponding parameter.
                                        </p>
                                    </div>
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddParameterColumns" Text="Add Parameter" CssClass="mr10" CausesValidation="False" OnClick="OnAddParameterColumnsClicked" />
                                        <asp:Button runat="server" ID="btnClearAllParameters" Text="Clear Parameters" CausesValidation="false" OnClick="OnClearParametersClicked" />
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheReportConfigurationParametersTable" TableId="reportConfigurationParametersTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table class="stripe" id="reportConfigurationParametersTable">
                                    <tr>
                                        <th style="width: 22%;">Name
                                        </th>
                                        <th style="width: 13%;">Operator
                                        </th>
                                        <th style="width: 55%;">Default Value
                                        </th>
                                        <th style="width: 5%;" class="text-center">Read Only
                                        </th>
                                        <th style="width: 5%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstParameterColumns" runat="server" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnParameterColumnsItemDataBound">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <eShip:ReportConfigurationParameterControl runat="server" ID="reportConfigurationParameterControl"
                                                ItemIndex='<%# Container.DataItemIndex %>' OnRemoveParameter="OnDeleteParameterColumnClicked" />
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnAddParameterColumns" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <asp:Panel runat="server" ID="pnlEditParameterColumns" Visible="false" CssClass="popup popupControlOverW500">
                        <div class="popheader">
                            <h4>Parameter Columns<asp:ImageButton ID="ImageButton4" ImageUrl="~/images/icons2/close.png" CssClass="close"
                                CausesValidation="false" OnClick="OnCloseEditParametersClicked" runat="server" />
                            </h4>
                        </div>
                        <table class="poptable">
                            <tr>
                                <td colspan="2" class="p_m0">
                                    <eShip:TableFreezeHeaderExtender runat="server" ID="TableFreezeHeaderExtender5" TableId="reportConfigurationParameterEditTable" ScrollerContainerId="reportConfigurationParameterEditScrollSection"
                                        ScrollOffsetControlId="pnlEditParameterColumns" IgnoreHeaderBackgroundFill="True" HeaderZIndex="1050" />
                                    <div class="finderScroll" id="reportConfigurationParameterEditScrollSection">
                                        <table id="reportConfigurationParameterEditTable" class="stripe sm_chk">
                                            <tr>
                                                <th style="width: 4%;">
                                                    <eShip:AltUniformCheckBox ID="chkParameterSelectAll" runat="server" OnClientSideClicked="javascript:SetSelectedCheckBoxesStatus(this, 'reportConfigurationParameterEditTable');" />
                                                </th>
                                                <th style="width: 70%;">Name
                                                </th>
                                                <th style="width: 26%;">Data Type
                                                </th>
                                            </tr>
                                            <asp:ListView ID="lstEditParameters" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <eShip:AltUniformCheckBox ID="chkSelection" runat="server" OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'reportConfigurationParameterEditTable', 'chkParameterSelectAll');" />
                                                        </td>
                                                        <td>
                                                            <eShip:CustomHiddenField ID="hidParameterIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                            <asp:Literal ID="litParameterName" runat="server" Text='<%# Eval("Name") %>' />
                                                        </td>
                                                        <td>
                                                            <eShip:CustomHiddenField ID="hidParameterOperator" runat="server" Value='<%# Eval("Operator") %>' />
                                                            <eShip:CustomHiddenField ID="hidParameterDataType" runat="server" Value='<%# Eval("DataType") %>' />
                                                            <asp:Literal ID="litParameterDataType" runat="server" Text='<%# Eval("DataType").FormattedString() %>' />
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td class="text-left">
                                    <asp:Button ID="Button7" Text="Add Selected" OnClick="OnAddSelectedParametersDoneClicked"
                                        runat="server" CausesValidation="false" />
                                    <asp:Button ID="Button8" Text="Cancel" OnClick="OnCloseEditParametersClicked" runat="server"
                                        CausesValidation="false" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabRequiredParameters" HeaderText="Req'd Params">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlRequiredParameterColumns">
                        <div class="row mb10">
                            <div class="fieldgroup pb10">
                                <p class="note">
                                    &#8226; Note: These parameters are required to run the report. You cannot run the report
                                    without appropriate values for these.
                                </p>
                            </div>
                        </div>
                        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheReportConfigurationRequiredParametersTable" TableId="reportConfigurationRequiredParametersTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                        <table class="stripe" id="reportConfigurationRequiredParametersTable">
                            <tr>
                                <th style="width: 35%;">Name
                                </th>
                                <th style="width: 40%;">Default Value
                                </th>
                                <th style="width: 15%;">Data Type
                                </th>
                                <th style="width: 10%;" class="text-center">Read Only
                                </th>
                            </tr>
                            <asp:ListView ID="lstRequiredParameters" runat="server" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnLstRequiredParametersItemDataBound">
                                <LayoutTemplate>
                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <eShip:ReportConfigurationParameterControl runat="server" ID="reportConfigurationRequiredParameterControl" ItemIndex='<%# Container.DataItemIndex %>' />
                                </ItemTemplate>
                            </asp:ListView>
                        </table>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabChartChartConfiguration" HeaderText="Charts">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlChartConfiguration" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlChartConfiguration">
                                <div class="row mb10">
                                    <div class="rowgroup mb10">
                                        <div class="row">
                                            <div class="fieldgroup left">
                                                <eShip:PaginationExtender runat="server" ID="peLstCharts" TargetControlId="lstCharts"
                                                    PageSize="1" UseParentDataStore="False" OnPageChanging="OnChartsPageChanging" />
                                            </div>
                                            <div class="fieldgroup right">
                                                <asp:Button runat="server" ID="btnAddChart" Text="Add Chart" CssClass="mr10"
                                                    CausesValidation="false" OnClick="OnAddChartClicked" />
                                                <asp:Button runat="server" ID="btnClearCharts" Text="Clear Charts" OnClick="OnClearChartsClicked" CausesValidation="false" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <table class="stripe" id="reportConfigurationCharts">
                                    <tr>
                                        <th style="width: 95%">Chart</th>
                                        <th style="width: 5%">Actions</th>
                                    </tr>
                                    <asp:ListView runat="server" ID="lstCharts" OnItemDataBound="OnChartsItemDataBound">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td colspan="2">
                                                    <eShip:CustomHiddenField ID="hidPivotGuidId" runat="server" Value='<%# Eval("PivotGuidId") %>'  />
                                                    <eShip:CustomHiddenField ID="hidPivotTableName" runat="server" Value='<%# Eval("PivotTableName") %>'  />
                                                    <eShip:CustomHiddenField ID="hidChartIndex" runat="server" Value='<%# Container.DataItemIndex + (peLstCharts.PageSize*(peLstCharts.CurrentPage-1)) %>' />
                                                    <div class="rowgroup">
                                                        <div class="col_1_2 bbox">
                                                            <h5>Chart Area Options</h5>
                                                            <div class="row">
                                                                <div class="fieldgroup">
                                                                    <label class="wlabel">
                                                                        <abbr title="For optimum viewing results limit length of title to 70 - 100 characters">Title</abbr>
                                                                    </label>
                                                                    <eShip:CustomTextBox runat="server" ID="txtChartTitle" CssClass="w425" Text='<%# Eval("Title") %>' MaxLength="32" />
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="fieldgroup mr20">
                                                                    <label class="wlabel">
                                                                        <abbr title="For optimum viewing results limit length of title to 70 - 100 characters">X-Axis Title</abbr>
                                                                    </label>
                                                                    <eShip:CustomTextBox runat="server" ID="txtXAxisTitle" CssClass="w200" Text='<%# Eval("XAxisTitle") %>' />
                                                                </div>
                                                                <div class="fieldgroup">
                                                                    <label class="wlabel">
                                                                        <abbr title="For optimum viewing results limit length of title to 70 - 100 characters">Y-Axis Title</abbr>
                                                                    </label>
                                                                    <eShip:CustomTextBox runat="server" ID="txtYAxisTitle" CssClass="w200" Text='<%# Eval("YAxisTitle") %>' />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col_1_2 bbox pl46 vlinedarkleft">
                                                            <asp:UpdatePanel runat="server" ID="upDataSourceOptions">
                                                                <ContentTemplate>
                                                                    <div class="row">
                                                                        <div class="fieldgroup">
                                                                            <h5>Data Source Options</h5>
                                                                        </div>
                                                                        <div class="fieldgroup right">
                                                                            <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                                                CausesValidation="false" OnClick="OnDeleteChartClicked" Enabled='<%# Access.Modify %>' />
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="fieldgroup">
                                                                            <label class="wlabel">
                                                                                Data Source
                                                                            </label>
                                                                            <asp:DropDownList ID="ddlDataSource" runat="server" DataTextField="Value" DataValueField="Key"
                                                                                OnSelectedIndexChanged="OnDataSourceSelectedIndexChanged" AutoPostBack="True" />
                                                                        </div>
                                                                        <asp:Panel runat="server" ID="pnlPivotOptions">
                                                                            <div class="fieldgroup">
                                                                                <label class="wlabel">
                                                                                    Pivot Table
                                                                                </label>
                                                                                <asp:DropDownList ID="ddlPivotTable" runat="server" DataTextField="Text" DataValueField="Value"
                                                                                    OnSelectedIndexChanged="OnPivotTableSelectedIndexChanged" AutoPostBack="True" />
                                                                            </div>
                                                                        </asp:Panel>
                                                                    </div>
                                                                    <asp:Panel runat="server" ID="pnlXSeriesOptions">
                                                                        <h5>X Series</h5>
                                                                        <div class="row">
                                                                            <div class="fieldgroup mr20">
                                                                                <label class="wlabel">X-Axis Data Column</label>
                                                                                <asp:DropDownList runat="server" ID="ddlXAxisDataColumn" DataValueField="Key" DataTextField="Value" />
                                                                           </div>
                                                                        </div>
                                                                    </asp:Panel>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="fieldgroup">
                                                            <h5 class="mb10">Y Series</h5>
                                                        </div>
                                                        <div class="fieldgroup right pt10">
                                                            <asp:UpdatePanel runat="server" ID="upYAxisColumnButtons">
                                                                <ContentTemplate>
                                                                    <asp:Button ID="btnAddYAxisColumn" runat="server" Text="Add Y-Axis Columns" CssClass="mr10" OnClick="OnAddYAxisColumnsClicked" CausesValidation="False" />
                                                                    <asp:Button runat="server" ID="btnClearAllYAxisColums" Text="Clear Y-Axis Columns" OnClick="OnClearAllYAxisColumnsClicked" CausesValidation="False" />
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:PostBackTrigger ControlID="btnAddYAxisColumn" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </div>
                                                    <eShip:TableFreezeHeaderExtender runat="server" ID="tfheReportConfigurationYAxisColumnTable" TableId="reportConfigurationYAxisColumnTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                                    <asp:UpdatePanel runat="server" ID="udpYAxisColumns">
                                                        <ContentTemplate>
                                                            <table class="stripe" id="reportConfigurationYAxisColumnTable">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="width: 74%;">Name
                                                                        </th>
                                                                        <th style="width: 21%;">Chart Type
                                                                        </th>
                                                                        <th style="width: 5%;">Action
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <asp:ListView ID="lstYAxisColumns" runat="server" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnYAxisColumnsItemDataBound">
                                                                    <LayoutTemplate>
                                                                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                                                    </LayoutTemplate>
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td>
                                                                                <eShip:CustomHiddenField ID="hidYAxisColumnIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                                                <asp:Literal ID="litYAxisColumnName" runat="server" Text='<%# Eval("ReportColumnName") %>' />
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList runat="server" ID="ddlYAxisColumnChartType" DataTextField="Value" DataValueField="Key" />
                                                                            </td>
                                                                            <td class="text-center">
                                                                                <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                                                    CausesValidation="false" OnClick="OnDeleteYAxisColumnClicked" Enabled='<%# Access.Modify %>' />
                                                                            </td>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:ListView>
                                                            </table>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:Panel runat="server" ID="pnlEditYAxisColumns" Visible="false" CssClass="popup popupControlOverW500">
                        <div class="popheader">
                            <h4>Y Axis Columns<asp:ImageButton ID="ImageButton5" ImageUrl="~/images/icons2/close.png" CssClass="close"
                                CausesValidation="false" OnClick="OnCloseEditYAxisColumnsClicked" runat="server" />
                            </h4>
                        </div>
                        <table class="poptable">
                            <tr>
                                <td colspan="2" class="p_m0">
                                    <eShip:TableFreezeHeaderExtender runat="server" ID="tfheReportConfigurationYAxisColumnEditTable" TableId="reportConfigurationYAxisColumnEditTable" ScrollerContainerId="reportConfigurationYAxisColumnEditScrollSection"
                                        ScrollOffsetControlId="pnlEditYAxisColumns" IgnoreHeaderBackgroundFill="True" HeaderZIndex="1050" />
                                    <div class="finderScroll" id="reportConfigurationYAxisColumnEditScrollSection">
                                        <table id="reportConfigurationYAxisColumnEditTable" class="stripe sm_chk">
                                            <tr>
                                                <th style="width: 4%;">
                                                    <eShip:AltUniformCheckBox ID="chkYAxisColumnSelectAll" runat="server" OnClientSideClicked="javascript:SetSelectedCheckBoxesStatus(this, 'reportConfigurationYAxisColumnEditTable');" />
                                                </th>
                                                <th style="width: 96%;">Name
                                                </th>
                                            </tr>
                                            <asp:ListView ID="lstEditYAxisColumns" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <eShip:AltUniformCheckBox ID="chkSelection" runat="server" OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'reportConfigurationYAxisColumnEditTable', 'chkYAxisColumnSelectAll');" />
                                                        </td>
                                                        <td>
                                                            
                                                            <eShip:CustomHiddenField ID="hidYAxisColumnIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                            <asp:Literal ID="litYAxisColumnName" runat="server" Text='<%# Eval("Name") %>' />
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td class="text-left">
                                    <asp:Button ID="Button9" Text="Add Selected" OnClick="OnAddSelectedYAxisColumnsDoneClicked" runat="server"
                                        CausesValidation="false" />
                                    <asp:Button ID="Button10" Text="Cancel" OnClick="OnCloseEditYAxisColumnsClicked" runat="server"
                                        CausesValidation="false" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabAuditLog" HeaderText="Audit Log">
                <ContentTemplate>
                    <eShip:AuditLogControl ID="auditLogs" runat="server" />
                </ContentTemplate>
            </ajax:TabPanel>
        </ajax:TabContainer>
        <asp:Panel ID="Panel1" runat="server" Visible="False">
            <asp:ListView ID="lstSystemColumns" runat="server" ItemPlaceholderID="itemPlaceHolder">
                <LayoutTemplate>
                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:CustomHiddenField ID="hidSystemColumnName" runat="server" Value='<%# Eval("Name") %>' />
                    <eShip:CustomHiddenField ID="hidSystemColumnColumn" runat="server" Value='<%# Eval("Column") %>' />
                    <eShip:CustomHiddenField ID="hidSystemColumnDataType" runat="server" Value='<%# Eval("DataType") %>' />
                    <eShip:CustomHiddenField ID="hidSystemColumnCustom" runat="server" Value='<%# Eval("Custom") %>' />
                </ItemTemplate>
            </asp:ListView>
        </asp:Panel>
    </div>

    <asp:Panel runat="server" ID="pnlReportTemplates" Visible="false" CssClass="popup popupControlOverW1000" DefaultButton="btnFilter">
        <div class="popheader">
            <h4>Report Templates</h4>
        </div>
        <div class="row pt20 pb20 pl10">
            <div class="fieldgroup">
                <label class="upper blue">Filter:</label>
                <eShip:CustomTextBox runat="server" ID="txtFilter" CssClass="w200" />
            </div>
            <div class="fieldgroup ml10">
                <asp:Button runat="server" ID="btnFilter" Text="Filter Results" OnClick="OnFilterResultsClick" />
            </div>
            <div class="fieldgroup right pr10">
                <asp:Button runat="server" ID="btnNoReports" Text="Cancel" OnClick="OnNoReportsClicked" CausesValidation="False" />
            </div>
        </div>
        <div class="row">
            <hr class="w100p ml-inherit dark p_m0" />
            <div class="finderScroll">
                <table class="stripe">
                    <asp:Repeater runat="server" ID="rptReportTemplates">
                        <HeaderTemplate>
                            <ul class="twocol_list pl10 pt10">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <eShip:CustomHiddenField runat="server" ID="hidReportConfigurationId" Value='<%# Eval("Id") %>' />
                            <li class="pb10">
                                <div class="fieldgroup shadow w90p pb10 pt10 pl10 pr10 h150" style="overflow: auto">
                                    <label class="wlabel">
                                        <asp:LinkButton runat="server" Text='<%# Eval("Name") %>' OnClick="OnSelectReportTemplateClicked" CssClass="blue" />
                                    </label>
                                    <div class="clearfix"></div>
                                    <div class ="pl10 mt10">
                                        <label class="pl10"><%# Eval("Description") %></label>
                                    </div>
                                  </div>
                                </div>
                            </li>
                        </ItemTemplate>
                        <FooterTemplate>
                            </ul>
                        </FooterTemplate>
                    </asp:Repeater>
                </table>
            </div>
        </div>

        <asp:Panel runat="server" ID="pnlNoReports" Visible="False">
            <div class="row">
                <div class="fieldgroup">
                    <label class="pt20 pl20 pb20">There are no reports available to run.</label>
                </div>
            </div>
        </asp:Panel>
    </asp:Panel>

    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <eShip:CustomHiddenField runat="server" ID="hidFlag" />
            <eShip:CustomHiddenField runat="server" ID="hidEditingIndex" />
            <eShip:CustomHiddenField runat="server" ID="hidEditStatus" />
        </ContentTemplate>
    </asp:UpdatePanel>
    
</asp:Content>
