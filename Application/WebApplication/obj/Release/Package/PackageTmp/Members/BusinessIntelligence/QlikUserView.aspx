﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="QlikUserView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.QlikUserView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true" OnNew="OnNewClick" OnImport="OnImportClicked" OnExport="OnExportClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
                Qlik Users <small class="ml10">
                    <asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>

        <hr class="fat" />

        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheQlikUsersTable" TableId="qlikUsersTable" HeaderZIndex="2" />
        <div class="rowgroup">
            <table id="qlikUsersTable" class="line2 pl2">
                <tr>
                    <th style="width: 20%;">User Id</th>
                    <th style="width: 70%;">Name</th>
                    <th style="width: 10%;">Action</th>
                </tr>
                <asp:Repeater ID="rptQlikUsers" runat="server" OnItemDataBound="OnQlikUserItemDataBound">
                    <ItemTemplate>
                        <tr>
                            <td <%# Eval("HasAttributes").ToBoolean() ? "rowspan='2' class='top'" : string.Empty %>>
                                <eShip:CustomHiddenField ID="hidQlikUserId" runat="server" Value='<%# Eval("UserId") %>' />
                                <asp:Literal ID="litUserId" runat="server" Text='<%# Eval("UserId") %>' />
                            </td>
                            <td>
                                <asp:Literal ID="litName" runat="server" Text='<%# Eval("Name") %>' />
                            </td>
                            <td <%# Eval("HasAttributes").ToBoolean() ? "rowspan='2'" : string.Empty %> class="top">
                                <asp:ImageButton ID="ibtnEdit" ToolTip="Edit" runat="server" ImageUrl="~/images/icons2/editBlue.png"
                                    CausesValidation="false" OnClick="OnEditClicked" Enabled='<%# Access.Modify %>' />
                                <asp:ImageButton ID="ibtnDelete" ToolTip="Delete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                    CausesValidation="false" OnClick="OnDeleteClicked" Enabled='<%# Access.Remove %>' />
                                <ajax:ConfirmButtonExtender runat="server" ID="cbeDelete" TargetControlID="ibtnDelete"
                                    ConfirmText="Are you sure you want to delete record?" />
                            </td>
                        </tr>

                        <asp:Repeater ID="rptQlikUserAttributes" runat="server" Visible="False">
                            <HeaderTemplate>
                                <tr class="f9">
                                    <td colspan="1" class="forceLeftBorder">
                                        <table class="contain pl2">
                                            <tr class="bbb1">
                                                <th style="width: 35%;" class="no-top-border">User Attribute Type
                                                </th>
                                                <th style="width: 65%;" class="no-top-border no-left-border">User Attribute Value
                                                </th>
                                            </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="no-bottom-border">
                                    <td class="text-left top">
                                        <%# Eval("Type")%>
                                    </td>
                                    <td class="text-left top">
                                        <%# Eval("Value")%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                                    </td>
                                </tr>
                            </FooterTemplate>
                        </asp:Repeater>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </div>
        <asp:Panel ID="pnlEditQlikUser" runat="server" Visible="false" DefaultButton="btnSave">
            <div class="popupControl popupControlOverW500">
                <div class="popheader">
                    <h4>
                        <asp:Literal ID="litTitle" runat="server" />
                    </h4>
                    <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close" CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
                </div>
                <div class="row">
                    <table class="poptable">
                        <tr>
                            <td colspan="2" class="red">
                                <asp:Literal ID="litMessage" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">User Id:</label>
                            </td>
                            <td class="text-left">
                                <eShip:CustomTextBox ID="txtUserId" CssClass="w300" MaxLength="50" runat="server" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtUserId"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Name:</label>
                            </td>
                            <td class="text-left">
                                <eShip:CustomTextBox ID="txtName" CssClass="w300" MaxLength="50" runat="server" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtName"
                                    ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td class="pt10">
                                <asp:Button ID="btnSave" Text="Save" OnClick="OnSaveClick" runat="server" />
                                <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnCloseClicked" runat="server" CausesValidation="false" />
                                <asp:Button runat="server" ID="btnAddQlikUserAttribute" Text="Add Qlik User Attribute" OnClick="OnAddQlikUserAttributeClicked" CausesValidation="false" />
                            </td>
                        </tr>
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <asp:Repeater runat="server" ID="rptEditQlikUserAttributes" Visible="False" OnItemDataBound="OnEditQlikUserAttributesItemDataBound">
                                    <HeaderTemplate>
                                        <tr>
                                            <td colspan="2">
                                                <hr class="w100p ml-inherit" />
                                                <h5 class="ml10 dark">Qlik User Attributes</h5>
                                                <hr class="w100p ml-inherit dark dotted mb0" />
                                                <table class="ml10 mr20 mb10">
                                                    <tr>
                                                        <th style="width: 30%;" class="text-left blue">
                                                            <label class="upper">Type</label>
                                                        </th>
                                                        <th style="width: 60%;" class="text-left blue">
                                                            <label class="upper">Value</label>
                                                        </th>
                                                        <th style="width: 10%;" class="text-center blue">
                                                            <label class="upper">Delete</label>
                                                        </th>
                                                    </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td class="text-left">
                                                <eShip:CustomHiddenField runat="server" ID="hidQlikUserAttributeItemIndex" Value='<%# Container.ItemIndex %>' />
                                                <eShip:CustomTextBox ID="txtType" MaxLength="50" runat="server" Text='<%# Eval("Type") %>' CssClass="w140" OnTextChanged="OnQlikUserAttributeTypeChanged" />
                                                <ajax:AutoCompleteExtender runat="server" ID="aceQlikUserAttributeType" TargetControlID="txtType" ServiceMethod="GetQlikUserAttributeTypeList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                                    EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                                            </td>
                                            <td class="text-left">
                                                <eShip:CustomTextBox ID="txtValue" MaxLength="200" runat="server" Text='<%# Eval("Value") %>' CssClass="w220" />
                                                <ajax:AutoCompleteExtender runat="server" ID="aceQlikUserAttributeValue" TargetControlID="txtValue" ServiceMethod="GetQlikUserAttributeValueListForType" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                                    EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                                            </td>
                                            <td class="text-center">
                                                <asp:ImageButton ID="ibtnAccountBucketUnitDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png" OnClick="OnQlikUserAttributeDeleteClicked" CausesValidation="False" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>
                                                <asp:Button ID="btnSave2" Text="Save" OnClick="OnSaveClick" runat="server" />
                                                <asp:Button ID="btnCancel2" Text="Cancel" OnClick="OnCloseClicked" runat="server" CausesValidation="false" />
                                            </td>
                                        </tr>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information" OnOkay="OnOkayProcess" />
    <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled" OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
</asp:Content>
