﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportConfigurationParameterControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls.ReportConfigurationParameterControl" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.BusinessIntelligence" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<eShip:CustomHiddenField ID="hidItemIndex" runat="server" />

<script type="text/javascript">
    $(function () {
        $('#<%= pnlDefaultValue2.ClientID %>').css('display', $('#<%= ddlParameterOperator.ClientID %>').val() == '<%= Operator.Between.ToInt() %>' || $('#<%= ddlParameterOperator.ClientID %>').val() == '<%= Operator.NotBetween.ToInt() %>' ? 'inline-block' : 'none');

        $('#<%= ddlParameterOperator.ClientID %>').change(function () {
            var val = $('#<%= ddlParameterOperator.ClientID %>').val();
            $('#<%= pnlDefaultValue2.ClientID %>').css('display', val == '<%= Operator.Between.ToInt() %>' || val == '<%= Operator.NotBetween.ToInt() %>' ? 'inline-block' : 'none');
        });

        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
            $('#<%= pnlDefaultValue2.ClientID %>').css('display', $('#<%= ddlParameterOperator.ClientID %>').val() == '<%= Operator.Between.ToInt() %>' || $('#<%= ddlParameterOperator.ClientID %>').val() == '<%= Operator.NotBetween.ToInt() %>' ? 'inline-block' : 'none');

            $('#<%= ddlParameterOperator.ClientID %>').change(function () {
                var val = $('#<%= ddlParameterOperator.ClientID %>').val();
                $('#<%= pnlDefaultValue2.ClientID %>').css('display', val == '<%= Operator.Between.ToInt() %>' || val == '<%= Operator.NotBetween.ToInt() %>' ? 'inline-block' : 'none');
            });
        });
    });
</script>
<tr>
    <td>
        <eShip:CustomHiddenField runat="server" ID="hidIsRequiredParameter" />
        <eShip:CustomHiddenField runat="server" ID="hidParameterDataType" />
        <eShip:CustomHiddenField runat="server" ID="hidRequiredParameterName" />
        <label>
            <asp:Literal runat="server" ID="litParameterName" />
        </label>
    </td>
    <td style='<%= IsRequiredParameter ? "display: none;": string.Empty %>'>
        <asp:DropDownList runat="server" ID="ddlParameterOperator" DataValueField="Value"
            DataTextField="Text" CssClass="w130" CausesValidation="False" />
    </td>
    <td class="AjaxCalendarTable">
        <asp:Panel runat="server" ID="pnlText" Visible="false" Style="display: inline-block;">
            <eShip:CustomTextBox ID="txtParameterText" runat="server" CssClass="w130" />
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlNumeric" Visible="false" Style="display: inline-block;">
            <eShip:CustomTextBox ID="txtParameterNumeric" runat="server" CssClass="w130" Type="NegativeNumbers" />
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlDateTime" Visible="false" Style="display: inline-block;">
            <eShip:CustomTextBox ID="txtParameterDateTime" runat="server" CssClass="w130 mr10" Type="Date" placeholder="99/99/9999"/>
            <asp:DropDownList runat="server" ID="ddlParameterTime" CssClass="w100" />
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlBoolean" Visible="false" Style="display: inline-block;">
            <asp:DropDownList runat="server" ID="ddlParameterBoolean" DataValueField="Value"
                DataTextField="Text" CssClass="w80" />
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlDefaultValue2" Style="display: inline-block;">
            <asp:Panel runat="server" ID="pnlText2" Visible="false">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icons2/amper.png" CssClass="middle" />
                <eShip:CustomTextBox ID="txtParameterText2" runat="server" CssClass="w130" />
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlNumeric2" Visible="false">
                <asp:Image ID="Image2" runat="server" ImageUrl="~/images/icons2/amper.png" CssClass="middle" />
                <eShip:CustomTextBox ID="txtParameterNumeric2" runat="server" CssClass="w130" Type="NegativeNumbers" />
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlDateTime2" Visible="false">
                <asp:Image ID="Image3" runat="server" ImageUrl="~/images/icons2/amper.png" CssClass="middle" />
                <eShip:CustomTextBox ID="txtParameterDateTime2" runat="server" CssClass="w130 mr10" Type="Date" placeholder="99/99/9999"/>
                <asp:DropDownList runat="server" ID="ddlParameterTime2" CssClass="w100" />
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlBoolean2" Visible="false">
                <asp:Image ID="Image4" runat="server" ImageUrl="~/images/icons2/amper.png" CssClass="middle" />
                <asp:DropDownList runat="server" ID="ddlParameterBoolean2" DataValueField="Value"
                    DataTextField="Text" CssClass="w80" />
            </asp:Panel>
        </asp:Panel>
    </td>
    <td style='<%= IsRequiredParameter ? string.Empty: "display: none;" %>'>
        <asp:Literal ID="litParameterColumnDataType" runat="server" />
    </td>
    <td class="text-center">
        <eShip:AltUniformCheckBox runat="server" ID="chkIsReadOnly" />
    </td>
    <td class="text-center" style='<%= IsRequiredParameter ? "display: none;": string.Empty %>'>
        <asp:ImageButton runat="server" ID="imgRemoveParameter" ToolTip="Remove Parameter"
            CausesValidation="False" ImageUrl="~/images/icons2/deleteX.png" OnClick="OnRemoveParameterClicked" />
    </td>
</tr>

