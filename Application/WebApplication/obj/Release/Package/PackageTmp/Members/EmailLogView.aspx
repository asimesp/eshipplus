﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="EmailLogView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.EmailLogView" EnableEventValidation="False" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
                Email Logs
                <small class="ml10">
                    <asp:Literal runat="server" ID="litRecordCount" Text="" /></small>
            </h3>
        </div>
        <span class="clearfix"></span>
        <hr class="dark mb10" />
        <asp:Panel runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb0">
                <div class="row mb0">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="EmailLogs" OnProcessingError="OnSearchProfilesProcessingError"
                            OnSearchProfileSelected="OnSearchProfilesProfileSelected" ShowAutoRefresh="True" OnAutoRefreshChanged="OnSearchProfilesAutoRefreshChanged"/>
                    </div>
                </div>
            </div>
            <hr class="dark mb20" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                        OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>
            <hr class="fat" />
            <div class="rowgroup">
                <div class="row mb10">
                    <div class="left">
                        <asp:Button ID="btnSearch" CssClass="left" runat="server" Text="SEARCH" OnClick="OnSearchClicked" CausesValidation="False" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:UpdatePanel runat="server" ID="upDataUpdate">
            <ContentTemplate>
                <eShip:ContinuousScrollExtender ID="cseDataUpdate" runat="server" BindableControlId="lvwEmailLogs" UpdatePanelToExtendId="upDataUpdate" />
                <asp:Timer runat="server" ID="tmRefresh" Interval="30000" OnTick="OnTimerTick" />
                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheEmailLogsTable" TableId="emailLogsTable" HeaderZIndex="2" />
                <asp:ListView ID="lvwEmailLogs" runat="server" ItemPlaceholderID="itemPlaceHolder">
                    <LayoutTemplate>
                        <table id="emailLogsTable" class="line2 pl2">
                            <tr>
                                <th style="width: 10%">From
                                </th>
                                <th style="width: 20%;">To
                                </th>
                                <th style="width: 15%;">Cc
                                </th>
                                <th style="width: 31%;">Subject
                                </th>
                                <th style="width: 12%;">Date Created
                                </th>
                                <th style="width:5%" class="text-center">Sent
                                </th>
                                <th style="width: 8%;">Options
                                </th>
                            </tr>
                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="top">
                                <eShip:CustomHiddenField runat="server" ID="hidEmailLogId" Value='<%# Eval("Id") %>' />
                                <%# Eval("From") %>
                            </td>
                            <td class="top">
                                <%# Eval("Tos") %>
                            </td>
                            <td class="top">
                                <%# Eval("Ccs") %>
                            </td>
                            <td class="top">
                                <%# Eval("Subject") %>
                            </td>
                            <td class="top">
                                <%# Eval("DateCreated") %>
                            </td>
                             <td class="top text-center">
                                <%# Eval("Sent").ToBoolean() ? WebApplicationConstants.HtmlCheck : string.Empty %>
                            </td>
                            <td class="text-center top">
                                <asp:ImageButton runat="server" ID="ibtnShippingLabel" ImageUrl="~/images/icons2/redeliver.png" ToolTip="View Email Details" OnClick="OnResendEmailClicked" />
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lvwEmailLogs" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:Panel runat="server" ID="pnlResendEmail" Visible="False">
            <div class="popupControl">
                <div class="popheader">
                    <h4>Resend Email</h4>
                    <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close" CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
                </div>
                <div class="row">
                    <table class="poptable">
                        <tr>
                            <td class="text-right pt10 top">
                                <label class="upper">To:</label>
                            </td>
                            <td class="pt10">
                                <eShip:CustomHiddenField runat="server" ID="hidEmailLogId" />
                                <eShip:CustomTextBox runat="server" ID="txtTo" CssClass="w650 disabled" TextMode="MultiLine" ReadOnly="True" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right top">
                                <label class="upper">Cc:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox runat="server" ID="txtCc" CssClass="w650 disabled" TextMode="MultiLine" ReadOnly="True" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right top">
                                <label class="upper">Bcc:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox runat="server" ID="txtBcc" CssClass="w650 disabled" TextMode="MultiLine" ReadOnly="True" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right top">
                                <label class="upper">Subject:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox runat="server" ID="txtSubject" CssClass="w650 disabled" ReadOnly="True" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right top">
                                <label class="upper">Body:</label>
                            </td>
                            <td class="pb10 pt10 pl10 pr10 top" colspan="2" >
                                <script type="text/javascript">
                                    $(function() {
                                        $('#<%= ifrEmailBody.ClientID %>').load(function() {
                                            $(this).height($(this).contents().height());
                                        });
                                    })
                                </script>
                                <iframe runat="server" ID="ifrEmailBody" class="finderScroll">
                                </iframe>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right top">
                                <label class="upper">Attachments:</label>
                            </td>
                            <td class="top">
                                <asp:Repeater runat="server" ID="rptAttachments">
                                    <HeaderTemplate>
                                        <ul class="mt0">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <li>
                                            <eShip:CustomHiddenField runat="server" ID="hidAttachmentId" Value='<%# Eval("Id") %>' />
                                            <label>
                                                <asp:LinkButton runat="server" ID="lnkAttachment" Text='<%# Eval("Name") %>' CssClass="blue" OnClick="OnDownloadAttachmentClicked" />
                                            </label>
                                        </li>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </ul>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="text-right">
                                <asp:Button runat="server" ID="btnResend" Text="Resend Email" OnClick="OnResendEmailResendClicked" />
                                <asp:Button runat="server" ID="btnCancel" Text="Cancel" CausesValidation="False" OnClick="OnCloseClicked" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>

    <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackgroundControl" runat="server" Visible="false" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
</asp:Content>
