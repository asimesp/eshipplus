﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="DeveloperAccessRequestListingView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.DeveloperAccessRequestListingView" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowDelete="True"
        OnDelete="OnToolbarDeleteClicked" ShowFind="True" OnFind="OnToolbarFindClicked" OnUnlock="OnUnlockClicked"
        ShowSave="True" OnSave="OnToolbarSaveClicked" ShowEdit="True" OnEdit="OnToolbarEditClicked" OnCommand="OnToolbarCommand" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">

        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" ImageUrl="" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Developer Access Request Listing
                <span class="pageHeaderRecDesc">
                    <asp:Literal runat="server" ID="litRecordIdentity" />
                </span>
            </h3>

            <div class="lockMessage">
                <asp:Literal ID="litMessage" runat="server" />
            </div>
        </div>

        <hr class="dark mb5" />

        <div class="errorMsgLit">
            <asp:Literal runat="server" ID="litErrorMessages" />
        </div>

        <eShip:DeveloperAccessRequestFinderControl runat="server" ID="developerAccessRequestFinder" Visible="false" OnItemSelected="OnFinderItemSelected"
            OnSelectionCancel="OnFinderItemCancelled" OpenForEditEnabled="True" />
        <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information"
            OnOkay="OnOkayProcess" OnNo="OnNoProcess" OnYes="OnYesProcess" />


        <eShip:CustomHiddenField runat="server" ID="hidRequestId" />
        <ajax:TabContainer ID="tabDeveloperAccessRequestListings" runat="server" CssClass="ajaxCustom">
            <ajax:TabPanel runat="server" ID="tabDetails" HeaderText="Details">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlRequest">
                        <div class="col_1_2 bbox vlinedarkright">
                            <h5>Request Details</h5>
                            <div class="rowgroup pb40">
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Customer Number</label>
                                        <eShip:CustomTextBox runat="server" ID="txtCustomerNumber" ReadOnly="True" CssClass="w200 disabled" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Customer Name</label>
                                        <eShip:CustomTextBox runat="server" ID="txtCustomerName" ReadOnly="True" CssClass="w200 disabled" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Contact Name</label>
                                        <eShip:CustomTextBox runat="server" ID="txtContactName" CssClass="w200" MaxLength="50" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Contact Email</label>
                                        <eShip:CustomTextBox runat="server" ID="txtContactEmail" CssClass="w200" MaxLength="100" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Date Created</label>
                                        <eShip:CustomTextBox runat="server" ID="txtDateCreated" ReadOnly="True" CssClass="w200 disabled" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col_1_2 bbox pl46">
                            <h5>Approvals</h5>
                            <div class="rowgroup">
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <asp:CheckBox runat="server" ID="chkAccess" CssClass="jQueryUniform" />
                                        <label class="w180">Approved for Development</label>
                                    </div>
                                    <div class="fieldgroup">
                                        <asp:CheckBox runat="server" ID="chkProductionAccess" CssClass="jQueryUniform" />
                                        <label class="w180">Approved for Production</label>
                                    </div>
                                </div>
                                <div class="row mt20">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Test Information
                                        <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleTestInfo" TargetControlId="txtTestInfo" MaxLength="500" />
                                        </label>
                                        <eShip:CustomTextBox runat="server" ID="txtTestInfo" CssClass="w480 h150" TextMode="MultiLine" />
                                    </div>
                                </div>
                            </div>
                        </div>

                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabAuditLog" HeaderText="Audit Log">
                <ContentTemplate>
                    <eShip:AuditLogControl ID="auditLogs" runat="server" />
                </ContentTemplate>
            </ajax:TabPanel>
        </ajax:TabContainer>

        <asp:Panel ID="pnlEditOriginalUser" runat="server" Visible="False" CssClass="popupControlOver popupControlOverW500" DefaultButton="btnSave">

            <div class="popheader">
                <h4>Original User Name [<asp:Literal runat="server" ID="litTitleQualifier" />]</h4>
                <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                    CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
            </div>

            <table class="poptable mt10 mr10 ml10 mb10">
                <tr>
                    <td>If you want to include the name of the user that requested access in the notification, please
		            enter it here:                       
                        <eShip:CustomTextBox ID="txtName" MaxLength="500" runat="server" CssClass="w200" />
                    </td>
                </tr>
                <tr>
                    <td class="pt10">
                        <asp:Button ID="btnSave" Text="Send Notification" OnClick="OnContinueClick" runat="server" />
                        <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnCloseClicked" runat="server"
                            CausesValidation="false" />
                    </td>
                </tr>
            </table>
        </asp:Panel>

    </div>

    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="False" />
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
</asp:Content>
