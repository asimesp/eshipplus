﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EvaTest.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.smc3.EvaTest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            BOL:
            <asp:TextBox runat="server" ID="txtBol"></asp:TextBox><br />
            Shipment Prefix:
            <asp:TextBox runat="server" ID="txtPrefix"></asp:TextBox><br />
            Origin Postal Code:
            <asp:TextBox runat="server" ID="txtOriginZip"></asp:TextBox><br />
            Destination Postal Code:
            <asp:TextBox runat="server" ID="txtDestinationZip"></asp:TextBox><br />
            Desired Pickup Date:
            <eShip:CustomTextBox runat="server" ID="txtDesiredPickupDate" Type="Date" /><br />
            SCAC:
            <asp:TextBox runat="server" ID="txtScac"></asp:TextBox><br />
            Account Token:
            <asp:TextBox runat="server" ID="txtAccountToken"></asp:TextBox><br />
            <asp:Button runat="server" ID="btnSubmit" OnClick="OnSubmitTrackingClicked" Text="Track"/>
            <asp:Button runat="server" ID="btnSubmitDispatch" OnClick="OnSubmitDispatchClicked" Text="Dispatch"/>
            <br/><br/>
            <asp:Literal runat="server" ID="litMsg"></asp:Literal>
        </div>
    </form>
</body>
</html>
