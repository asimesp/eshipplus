﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="PublicTrack.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Public.PublicTrack" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<asp:Content ID="PageContent" ContentPlaceHolderID="BaseContentPlaceHolder" runat="server">
    <asp:HiddenField runat="server" ID="hidAccessCode" Value="TENANT1" />
    <div id="mainContainer">
        <div id="header" class="shadow">
            <div class="header_top bottom_shadow">
                <h1 class="logo lhInherit">
                    <asp:Image ID="imgEshipLogo" runat="server" ImageUrl="~/images/eshiplogo.png" Height="53px" />

                </h1>
                <div class="classic_view_info" >
                    <div class="userinfo">
                        
                        <a class="blue link_nounderline pl20" target="_blank" href="https://www.surveymonkey.com/r/new-eshipplus">Provide Feedback</a>
                    </div>
                </div>
                <div id="alertsbar">
                    
                    <div class="dimBackground" id="annoucementDimBackgroud" style="display: none;">
                    </div>
                </div>
                <div class="logo2">
                    <h2>
                        <asp:Image ID="imgVendorLogo" runat="server" Height="50px" />
                    </h2>
                </div>
            </div>

            <!-- menu bar -->
            <div id="toolbar">
                
            </div>
        </div>



        <!-- main content -->
        <div id="content" class="shadow clearfix">
            <div id="wrapper" class="clearfix">
                <div class="row ml20 mb20">
                    <div class="fieldgroup">
                        <label class="upper ml20">Criteria:</label>
                    </div>
                    <div class="fieldgroup">
                        <asp:TextBox ID="txtCriteria" CssClass="w130 ml10" runat="server" MaxLength="50" />
                    </div>
                    <div class="fieldgroup">
                        <label class="wlabel ml20">Type:</label>
                    </div>
                    <div class="fieldgroup">
                        <asp:DropDownList ID="ddlCriteriaType" runat="server" CssClass="ml10">
                            <asp:ListItem Value="0">Shipment Number</asp:ListItem>
                            <asp:ListItem Value="1">PO Number</asp:ListItem>
                            <asp:ListItem Value="2">PRO Number</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="fieldgroup">
                        <label class="wlabel ml20">Origin Zip:</label>
                    </div>
                    <div class="fieldgroup">
                        <asp:TextBox ID="txtOriginPostalCode" CssClass="w100 ml10" runat="server" MaxLength="50" />
                    </div>
                    <div class="fieldgroup">
                        <asp:Button ID="btnTrack" runat="server" Text="TRACK" CssClass="ml20" OnClick="OnTrackClicked" />
                    </div>
                </div>
                <hr class="dark mb5" />
                <div class="row mt20">
                        <asp:Repeater ID="rptSearchResult" runat="server">
                            <HeaderTemplate>
                                <table class="table-bordered w100p table-striped stripe">
                            <thead>
                                <tr>
                                    <th scope="col">Shipment Number</th>
                                    <th scope="col">PO Number</th>
                                    <th scope="col">PRO Number</th>
                                    <th scope="col">Origin Zip</th>
                                    <th scope="col">Destination Zip</th>
                                    <th scope="col">Carrier Name</th>
                                    <th scope="col">Pickup Date</th>
                                    <th scope="col">Delivery Date</th>
                                    <th scope="col">Total Weight</th>
                                </tr>
                                </thead>
                            <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td><%# Eval("ShipmentNumber") %></td>
                                    <td><%# Eval("PurchaseOrderNumber") %></td>
                                    <td><%# Eval("VendorProNumber") %></td>
                                    <td><%# Eval("OriginPostalCode") %></td>
                                    <td><%# Eval("DestinationPostalCode") %></td>
                                    <td><%# Eval("VendorName") %></td>
                                    <td><%# Eval("ActualPickupDate").FormattedShortDateSuppressEarliestDate() %></td>
                                    <td><%# Eval("ActualDeliveryDate").FormattedShortDateSuppressEarliestDate() %></td>
                                    <td><%# Eval("TotalWeight", "{0:F2}") %></td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
            </div>
        </div>

        <div id="footer">
            <p>
                
            </p>
        </div>
    </div>
</asp:Content>
