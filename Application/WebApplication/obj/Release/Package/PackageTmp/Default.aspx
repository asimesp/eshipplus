﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication._Default" MasterPageFile="~/Base.Master" %>

<%@ Register TagPrefix="pc" TagName="UserAuthentication" Src="~/Members/Controls/UserAuthentication.ascx" %>


<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="BaseContentPlaceHolder">
    <pc:UserAuthentication ID="authentication" runat="server" OnSuccessfulLogin="OnSuccessfulAuthentication" />
</asp:Content>
