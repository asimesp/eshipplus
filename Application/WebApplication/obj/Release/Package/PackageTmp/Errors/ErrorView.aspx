﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.master" AutoEventWireup="true" CodeBehind="ErrorView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Errors.ErrorView" %>
<asp:Content ID="Content" ContentPlaceHolderID="BaseContentPlaceHolder" runat="server">
    <div style="width:100%;text-align: center;">
		<br/>
		<br/>
		<h1>There was an error processing your request.</h1>
		<br/>
		<b>Our staff has been notified of this error and will act on it as soon as possible.</b><br /><br />
		Click <a href="javascript:history.back(1);">here</a> to go back.<br />
	</div>
</asp:Content>
