﻿//-----------------------------------------------------------------------------------

/*
Application wide process div block
*/
$(window).load(function() {
	// hide processing block div. NOTE: div is in BaseMaster page.
    $('#divProcessing').hide();
    $('#stopClickingToQuickDiv').hide();
});

function ShowProcessingDivBlock() {
    //$('#divProcessing').show();
    ShowProgressNotice(true);
}

/*
    Show/hide Progress Notice
*/
function ShowProgressNotice(show) {
    if (show)
        $('#divProcessing').show();
    else
        $('#divProcessing').hide();
}

/*
    Set focus for parameterTable when visible (usually finders)

*/
function SetControlFocus() {
    var txt = $("table[id='parametersTable'] input[type='text']").not("input[id$='txtHid']").first();
    if (txt) {
        $(txt).focus();
        return;
    }
    var ddl = $("table[id='parametersTable'] select").not("select[id$='ddlParameterOperator']").first();
    if (ddl) {
        $(ddl).focus();
    }
}

//-----------------------------------------------------------------------------------

/*
    JQuery Uniform initializations
    NOTE: Requires JQuery and JQuery.Uniform js files loaded prior to call!!!
*/
function InitialJQueryUniform() {
    // asp checkboxes do not apply css class to checkbox, instead placing on the span containing checkbox
    $("input:checkbox, input:radio").each(function () {
        if ($(this).parent() != null
            && $(this).parent().attr('class') != null
            && $(this).parent().attr('class').indexOf("jQueryUniform") > -1) {
            var currentClass = $(this).attr('class') == null ? jsHelper.EmptyString : $(this).attr('class');
            $(this).attr('class', currentClass + ' jQueryUniform');
        }
    });

    $("input.jQueryUniform").uniform({ filenameClass: 'filename jQueryUniformFileUploaderSpanWidth', fileClass: 'uploader jQueryUniformFileUploaderDivWidth' });
}

/*
    JQuery uniform hack for AltUniformCheckBox control
    NOTE: issue using uniform checkbox within update panel so custom control required.
*/
function SetAltUniformCheckBoxClickedStatus(control) {
    if (control == null) return;
    $(control).parent().attr('class', control.checked ? 'checked' : '');
}

//-----------------------------------------------------------------------------------

// Miscellaneous Methods

/*
    Set all 'selected' indicating check boxes in table as checked or unchecked
*/
function SetSelectedCheckBoxesStatus(control, tableId) {
    if (control == null) return;
    $("#" + tableId + " input:checkbox").each(function() {
        this.checked = control.checked;
        SetAltUniformCheckBoxClickedStatus(this);
    });
    
    // necessary to keep frozen header and header checkbox states the same
    $('[id*=' + control.id + ']').each(function () {
        this.checked = control.checked;
        SetAltUniformCheckBoxClickedStatus(this);
    });
}

/*
    Set 'all check selected' indicating check box as unchecked if checkbox is unchecked
*/
function SetCheckAllCheckBoxUnChecked(control, tableId, checkAllCheckBoxId) {
    if (control == null) return;
    if (!jsHelper.IsChecked(control.id)) {
        $('[id$=' + tableId + '] input:checkbox[id*=' + checkAllCheckBoxId + ']').each(function() {
            $(this).prop('checked', '');
            SetAltUniformCheckBoxClickedStatus(this);
        });
    }
}

/*
Method controls max length on a text area.
*/
function CheckAndDisplayTextAreaLength(textAreaControl, maxLength, displayControlId) {
    if (textAreaControl == null) return;
    var l = $(textAreaControl).val().length;
    if (l >= maxLength) {
        $(textAreaControl).val($(textAreaControl).val().substr(0, maxLength));
        l = $(textAreaControl).val().length;
    }

    if (!jsHelper.IsNullOrEmpty(displayControlId))
        $(jsHelper.AddHashTag(displayControlId)).html("(" + l + " of " + maxLength + " char.)");
}

/*
    Set text of control
*/
function SetText(controlId, text) {
    if (jsHelper.IsNullOrEmpty(text)) $(jsHelper.AddHashTag(controlId)).text(jsHelper.EmptyString);
    else $(jsHelper.AddHashTag(controlId)).text(" [" + text + "] ");
}

//-----------------------------------------------------------------------------------


/*
Web Service for data calls
*/
function Ws() {
	this.eShipPlusWSv4P = LogisticsPlus.Eship.WebApplication.Services.eShipPlusWSv4P;
}

/*
Universal Helper function
*/
function JsHelper() {
	this.ChooseOne = '- Choose One -';
	this.DefaultId = 0;
	this.EmptyString = '';
	this.HashTag = '#';
	this.Zero = 0;

	this.EnglishUnits = '0';    // this matches Units.UnitType
	this.MetricUnits = '1';

	this.AccessCodeCookie = "eShipV4AccessCode";
	this.UsernameCookie = "eShipV4Username";
	this.UserAuthenticateCookie = "eShipV4UserAuthenticateCookie";
	this.RateAndScheduleInputPref = "eShipV4RateAndScheduleInputPref";
	this.CookieDaysToLive = 365;

    this.XYSet = "false";
	this.PageX = 0;
	this.PageY = 0;

	this.PageHeaderHeight = 173;
    this.PageWidth = 1200;

	// only adds hashtag to non empty string
	// value is string that requires hastag added as prefix
	this.AddHashTag = function(value) {
		if (value == this.EmptyString) return this.EmptyString;
		return this.HashTag + value;
	};

	this.ClearCookie = function(cookieName) {
		$.cookie(cookieName, null);
	};

	// set disabled attribute on a control
	this.Disable = function(controlId) {
		if (controlId == this.EmptyString) return;
		$(jsHelper.AddHashTag(controlId)).prop('disabled', 'disabled');
	};

	// reverse set of disabled attribute on a control
	this.Enable = function(controlId) {
		if (controlId == this.EmptyString) return;
		$(jsHelper.AddHashTag(controlId)).prop('disabled', null);
	};

	// checks for checked on checkbox
	this.IsChecked = function(checkBoxControlId) {
		return $(jsHelper.AddHashTag(checkBoxControlId)).prop('checked');
	};

	this.UnCheckBox = function(checkBoxControlId) {
		return $(jsHelper.AddHashTag(checkBoxControlId)).prop('checked', '');
	};

	this.CheckBox = function(checkBoxControlId) {
		return $(jsHelper.AddHashTag(checkBoxControlId)).prop('checked', 'checked');
	};
    
	this.IsNullOrEmpty = function(value) {
		return value == null || value == jsHelper.EmptyString;
	};

	// checks for true value
	this.IsTrue = function(value) {
		if (value == this.EmptyString) return false;
		return value.toLowerCase() == 'true';
	};

	this.RetrieveCookie = function(cookieName) {
		return $.cookie(cookieName);
	};

	this.SetCookie = function(cookieName, cookieValue, daysToLive) {
		$.cookie(cookieName, cookieValue, { expires: daysToLive });
	};

	this.SetEmptyDropDown = function(controlId) {
		$(jsHelper.AddHashTag(controlId))[0].options.length = 0;
		$(jsHelper.AddHashTag(controlId))[0].options[0] = new Option(jsHelper.ChooseOne, jsHelper.DefaultId);
		$(jsHelper.AddHashTag(controlId))[0].options[0].selected = true; // select first one i.e. '- Choose One -'
	};

	// set value empty. NOTE: control must support val()!
	this.SetEmpty = function(controlId) {
		if (controlId == this.EmptyString) return;
		$(this.AddHashTag(controlId)).val(this.EmptyString);
	};

    // set __doPostBack function for control in (usually inside of disabled panel)
    // NOTE: Will set only if control href is blank
    this.SetDoPostBackJsLink = function(controlId) {
        jsHelper.Enable(controlId);
        if (jsHelper.IsNullOrEmpty($(jsHelper.AddHashTag(controlId)).prop('href'))) {
            $(jsHelper.AddHashTag(controlId)).prop('href', "javascript:__doPostBack('" + controlId.replace(/_/g, '$') + "','')");
        }
    };
    
    // set thead and tbody on table taking first row as header
    this.SetTheadTbody = function (tableId) {
        if ($('#' + tableId + ' > thead').length != 0) return;

        var table = $(jsHelper.AddHashTag(tableId));
        table.children('tbody').children().unwrap();
        table.children('tr').not(':first').wrapAll('<tbody>');
        table.children('tr:first').wrapAll('<thead>');
    };
}
var jsHelper = new JsHelper();



/*
Control id's holds id's of control that must have values set in them on return from
web service call.  Applicable ids will depend on calling function
*/
function ControlIds() {

	this.City = jsHelper.EmptyString;
	this.State = jsHelper.EmptyString;

	this.Username = jsHelper.EmptyString;
	this.UserId = jsHelper.EmptyString;
	this.UserFullName = jsHelper.EmptyString;

	this.PrefixCode = jsHelper.EmptyString;
	this.PrefixDesc = jsHelper.EmptyString;
	this.PrefixId = jsHelper.EmptyString;

	this.AcctBucketCode = jsHelper.EmptyString; // textbox
	this.AcctBucketDesc = jsHelper.EmptyString; // textbox
	this.AcctBucketId = jsHelper.EmptyString;
	this.AcctBucketUnits = jsHelper.EmptyString; // drop down list
	this.AcctBucketUnitId = jsHelper.EmptyString; // hidden field

	this.CmsContentCurrentStatus = jsHelper.EmptyString;  // check box
	this.CmsContentId = jsHelper.EmptyString; // hidden field

	this.CmsNewsHiddenStatus = jsHelper.EmptyString;  // checkbox
	this.CmsNewsId = jsHelper.EmptyString; // hidden field

	this.DensityLength = jsHelper.EmptyString;
	this.DensityWidth = jsHelper.EmptyString;
	this.DensityHeight = jsHelper.EmptyString;
	this.DensityWeight = jsHelper.EmptyString;
	this.DensityCalc = jsHelper.EmptyString;
	this.HidDensityCalc = jsHelper.EmptyString; // hidden field
	this.DensityQty = jsHelper.EmptyString;

	this.TimeString = jsHelper.EmptyString; // textbox
	this.DateString = jsHelper.EmptyString; // textbox

	this.TimesOfDayWindowStart = jsHelper.EmptyString; // drop down list
	this.TimesOfDayWindowEnd = jsHelper.EmptyString; //drop down list

	this.DatAssetId = jsHelper.EmptyString; // textbox
	this.DatExpirationDate = jsHelper.EmptyString; // textbox
	this.DatDateCreated = jsHelper.EmptyString; // textbox
	this.DatBaseRate = jsHelper.EmptyString; // textbox
	this.DatRateType = jsHelper.EmptyString; // textbox
	this.DatMileage = jsHelper.EmptyString; // textbox
	this.DatWasSentAsHazMat = jsHelper.EmptyString; // alt uniform checkbox
	this.DatPostedByUser = jsHelper.EmptyString; // textbox
	this.DatLoadboardErrors = jsHelper.EmptyString; // label

    this.DatRatesContainer = jsHelper.EmptyString; // div
    this.DatRatesDiv = jsHelper.EmptyString; // div
    
	this.BtnPostAsset = jsHelper.EmptyString; // button
	this.BtnRemoveAsset = jsHelper.EmptyString; //button
	this.BtnUpdateAsset = jsHelper.EmptyString; //button
    
	this.HidDatAssetId = jsHelper.EmptyString; // hidden field
}

//-------//

/*
Method is particularly set up for RateAndSchedule and EstimateRate
Method expects an integer argument for unit type: 0 = english 1 = metric as per Js.helper
and the following control Ids (as textbox id's): 
DensityWeight, DensityLength, DensityWidth, DensityHeight, DensityCalculation, HidDensityCalculation, DensityQty
		
Method calculates density of an item based on its weight and dimensions. 
The result of the calculation is set into the following controls: DensityCalc and HidDensityCalc.

If any of the expected weight or dimension values are null or if any of the dimensions value is zero then
the value set into DensityCalc and HidDensity Calc controls values will be an empty string.

No results are returned with this method.
*/
function CalculateDensity(unitType, ids) {
	var wgt = $(jsHelper.AddHashTag(ids.DensityWeight)).val();
	var l = $(jsHelper.AddHashTag(ids.DensityLength)).val();
	var w = $(jsHelper.AddHashTag(ids.DensityWidth)).val();
	var h = $(jsHelper.AddHashTag(ids.DensityHeight)).val();
	var q = $(jsHelper.AddHashTag(ids.DensityQty)).val();

	var englishConversionFactor = 12.00;
	var metricConversionFactor = 100.00;
	var factor = unitType == jsHelper.EnglishUnits ? englishConversionFactor : metricConversionFactor;

	if (jsHelper.IsNullOrEmpty(q)) q = 1;
	if ((wgt != null && wgt != jsHelper.EmptyString)
        && (l != null && l != jsHelper.EmptyString && l > jsHelper.Zero)
        && (w != null && w != jsHelper.EmptyString && w > jsHelper.Zero)
        && (h != null && h != jsHelper.EmptyString && h > jsHelper.Zero)
		&& (q > jsHelper.Zero)) {
	
		var density = ((wgt / ((l / factor) * (w / factor) * (h / factor))) / q).toFixed(2);

		if ($(jsHelper.AddHashTag(ids.DensityCalc)).is('span')) {
	        $(jsHelper.AddHashTag(ids.DensityCalc)).html(density);
		} else if ($(jsHelper.AddHashTag(ids.DensityCalc)).is('input')) {
	        $(jsHelper.AddHashTag(ids.DensityCalc)).val(density);
	    }
		if(ids.HidDensityCalc) $(jsHelper.AddHashTag(ids.HidDensityCalc)).val(density);
	    
	} else {
	    if ($(jsHelper.AddHashTag(ids.DensityCalc)).is('span')) {
	        $(jsHelper.AddHashTag(ids.DensityCalc)).html(jsHelper.EmptyString);
	    } else if ($(jsHelper.AddHashTag(ids.DensityCalc)).is('input')) {
	        $(jsHelper.AddHashTag(ids.DensityCalc)).val(jsHelper.EmptyString);
	    }
	    if (ids.HidDensityCalc) $(jsHelper.AddHashTag(ids.HidDensityCalc)).val(jsHelper.EmptyString);
		
	}
}


/*
Method for finding active account bucket and associated unit
Expected control Ids: AcctBucketCode, AcctBucketDesc and AcctBucketId.  
AcctBucketUnits and AcctBucketUnit are optionals but if one is present,
the other must be present.
	
result will have:
Args			ControlId() class
Code			string	- account bucket code if present else string.empty
Description		string	- account bucket description if present else string.empty
Id				long	- account bucket Id if present else string.empty
Units			Array	- array of object structure: {{Id, Description},{Id, Description}}
ErrorMessage	string	- empty of no error message
*/
function FindActiveAcctBucket(tenantId, acctCode, ids) {

	if (acctCode == jsHelper.EmptyString) {
		$(jsHelper.AddHashTag(ids.AcctBucketCode)).val(jsHelper.EmptyString);
		$(jsHelper.AddHashTag(ids.AcctBucketId)).val(jsHelper.EmptyString);
		$(jsHelper.AddHashTag(ids.AcctBucketDesc)).val(jsHelper.EmptyString);

		// there will always be at least the default Instruction unit of '- Choose One -'
		if (ids.AcctBucketUnits != jsHelper.EmptyString) {
			jsHelper.SetEmptyDropDown(ids.AcctBucketUnits);
			jsHelper.SetEmpty(ids.AcctBucketUnitId);
		}

		return;
	}

	new Ws().eShipPlusWSv4P.GetActiveAccountBucket(tenantId, acctCode, ids,
		function(result) {
			if (result.ErrorMessage != jsHelper.EmptyString) {
				$(jsHelper.AddHashTag(result.Args.AcctBucketCode)).val(jsHelper.EmptyString);
				$(jsHelper.AddHashTag(result.Args.AcctBucketId)).val(jsHelper.EmptyString);
				$(jsHelper.AddHashTag(result.Args.AcctBucketDesc)).val(jsHelper.EmptyString);
				alert(result.ErrorMessage);
			} else {
				$(jsHelper.AddHashTag(result.Args.AcctBucketCode)).val(result.Code);
				$(jsHelper.AddHashTag(result.Args.AcctBucketId)).val(result.Id);
				$(jsHelper.AddHashTag(result.Args.AcctBucketDesc)).val(result.Description);
			}

			// there will always be at least the default Instruction unit of '- Choose One -'
			if (result.Args.AcctBucketUnits != jsHelper.EmptyString) {
				$(jsHelper.AddHashTag(result.Args.AcctBucketUnits))[0].options.length = 0;
				for (var i = 0; i < result.Units.length; i++) {
					$(jsHelper.AddHashTag(result.Args.AcctBucketUnits))[0].options[i] = new Option(result.Units[i].Name, result.Units[i].Id);
					if (i == 0) $(jsHelper.AddHashTag(result.Args.AcctBucketUnits))[0].options[i].selected = true; // select first one i.e. '- Choose One -'
				}
				jsHelper.SetEmpty(result.Args.AcctBucketUnitId);

			}
		});
}

/*
Method for finding active account bucket and associated unit
Expected control Ids: AcctBucketUnits and AcctBucketUnit.
	
result will have:
Args			ControlId() class
Units			Array	- array of object structure: {{Id, Description},{Id, Description}}
ErrorMessage	string	- empty of no error message
*/
function FindAcctBucketUnits(acctBucketId, ids) {

	if (acctBucketId == jsHelper.EmptyString || acctBucketId == jsHelper.DefaultId) {

		// there will always be at least the default Instruction unit of '- Choose One -'
		if (ids.AcctBucketUnits != jsHelper.EmptyString) {
			jsHelper.SetEmptyDropDown(ids.AcctBucketUnits);
			jsHelper.SetEmpty(ids.AcctBucketUnitId);
		}
		return;
	}

	new Ws().eShipPlusWSv4P.GetAccountBucketUnits(acctBucketId, ids,
		function(result) {
			if (result.ErrorMessage != jsHelper.EmptyString) {
				alert(result.ErrorMessage);
			}

			// there will always be at least the default Instruction unit of '- Choose One -'
			$(jsHelper.AddHashTag(result.Args.AcctBucketUnits))[0].options.length = 0;
			var valueSet = false;
			for (var i = 0; i < result.Units.length; i++) {
				$(jsHelper.AddHashTag(result.Args.AcctBucketUnits))[0].options[i] = new Option(result.Units[i].Name, result.Units[i].Id);
				if (result.Units[i].Id == $(jsHelper.AddHashTag(result.Args.AcctBucketUnitId)).val()) {
					$(jsHelper.AddHashTag(result.Args.AcctBucketUnits))[0].options[i].selected = true;
					valueSet = true;
				}
			}
			if (!valueSet) {
				$(jsHelper.AddHashTag(result.Args.AcctBucketUnits))[0].options[0].selected = true; // select first one i.e. '- Choose One -'
				jsHelper.SetEmpty(result.Args.AcctBucketUnitId); // remove previous value if not found!
			}
		});
}

/*
Method for finding user that is a carrier coordinator
Expected control Ids: Username, UserId and UserFullName
	
result will have:
Args			ControlId() class
Username		string	- username if present else string.empty
FullName		string	- user firstname lastname if present else string.empty
Id				long	- user Id if present else string.empty
ErrorMessage	string	- empty of no error message
*/
function FindActiveCarrierCoordinator(tenantCode, activeUserName, searchUsername, ids) {

	if (searchUsername == jsHelper.EmptyString) {
		$(jsHelper.AddHashTag(ids.Username)).val(jsHelper.EmptyString);
		$(jsHelper.AddHashTag(ids.UserId)).val(jsHelper.EmptyString);
		$(jsHelper.AddHashTag(ids.UserFullName)).val(jsHelper.EmptyString);
		return;
	}

	new Ws().eShipPlusWSv4P.GetActiveCarrierCoordinatorUser(tenantCode, activeUserName, searchUsername, ids,
		function(result) {
			if (result.ErrorMessage != jsHelper.EmptyString) {
				$(jsHelper.AddHashTag(result.Args.Username)).val(jsHelper.EmptyString);
				$(jsHelper.AddHashTag(result.Args.UserId)).val(jsHelper.EmptyString);
				$(jsHelper.AddHashTag(result.Args.UserFullName)).val(jsHelper.EmptyString);
				alert(result.ErrorMessage);
			} else {
				$(jsHelper.AddHashTag(result.Args.Username)).val(result.Username);
				$(jsHelper.AddHashTag(result.Args.UserId)).val(result.Id);
				$(jsHelper.AddHashTag(result.Args.UserFullName)).val(result.FullName);
			}
		});
}

/*
Method for finding user that is a shipment coordinator
Expected control Ids: Username, UserId and UserFullName
	
result will have:
Args			ControlId() class
Username		string	- username if present else string.empty
FullName		string	- user firstname lastname if present else string.empty
Id				long	- user Id if present else string.empty
ErrorMessage	string	- empty of no error message
*/
function FindActiveShipmentCoordinator(tenantCode, activeUserName, searchUsername, ids) {

	if (searchUsername == jsHelper.EmptyString) {
		$(jsHelper.AddHashTag(ids.Username)).val(jsHelper.EmptyString);
		$(jsHelper.AddHashTag(ids.UserId)).val(jsHelper.EmptyString);
		$(jsHelper.AddHashTag(ids.UserFullName)).val(jsHelper.EmptyString);
		return;
	}

	new Ws().eShipPlusWSv4P.GetActiveShipmentCoordinatorUser(tenantCode, activeUserName, searchUsername, ids,
		function(result) {
			if (result.ErrorMessage != jsHelper.EmptyString) {
				$(jsHelper.AddHashTag(result.Args.Username)).val(jsHelper.EmptyString);
				$(jsHelper.AddHashTag(result.Args.UserId)).val(jsHelper.EmptyString);
				$(jsHelper.AddHashTag(result.Args.UserFullName)).val(jsHelper.EmptyString);
				alert(result.ErrorMessage);
			} else {
				$(jsHelper.AddHashTag(result.Args.Username)).val(result.Username);
				$(jsHelper.AddHashTag(result.Args.UserId)).val(result.Id);
				$(jsHelper.AddHashTag(result.Args.UserFullName)).val(result.FullName);
			}
		});
}

/*
Method for finding prefix that is active for a tenant
Expected control Ids: PrefixCode, PrefixDesc and PrefixId
	
result will have:
Args			ControlId() class
Code			string	- prefix code if present else string.empty
Description		string	- prefix code description if present else string.empty
Id				long	- prefix Id if present else string.empty
ErrorMessage	string	- empty of no error message
*/
function FindActivePrefix(tenantId, prefixCode, ids) {

	if (prefixCode == jsHelper.EmptyString) {
		$(jsHelper.AddHashTag(ids.PrefixCode)).val(jsHelper.EmptyString);
		$(jsHelper.AddHashTag(ids.PrefixId)).val(jsHelper.EmptyString);
		$(jsHelper.AddHashTag(ids.PrefixDesc)).val(jsHelper.EmptyString);
		return;
	}

	new Ws().eShipPlusWSv4P.GetActivePrefix(tenantId, prefixCode, ids,
		function(result) {
			if (result.ErrorMessage != jsHelper.EmptyString) {
				$(jsHelper.AddHashTag(result.Args.PrefixCode)).val(jsHelper.EmptyString);
				$(jsHelper.AddHashTag(result.Args.PrefixId)).val(jsHelper.EmptyString);
				$(jsHelper.AddHashTag(result.Args.PrefixDesc)).val(jsHelper.EmptyString);
				alert(result.ErrorMessage);
			} else {
				$(jsHelper.AddHashTag(result.Args.PrefixCode)).val(result.Code);
				$(jsHelper.AddHashTag(result.Args.PrefixId)).val(result.Id);
				$(jsHelper.AddHashTag(result.Args.PrefixDesc)).val(result.Description);
			}
		});
}

/*
Method is particularly setup for address input controls
AccountingAddressInput
OperationsAddressInput
OperationsAddressInput2
		
Expected control Ids: City and State
		
result will have:
Args			ControlId() class
City			string	- city value if present else string.empty
State			string	- state value if present else string.empty
ErrorMessage	string	- empty if no error message
*/
function FindCityAndState(postalCodeValue, countryIdValue, ids) {
	if (postalCodeValue == jsHelper.EmptyString) return;
	new Ws().eShipPlusWSv4P.GetCityAndState(postalCodeValue, countryIdValue, ids,
		function(result) {
			$(jsHelper.AddHashTag(result.Args.City)).val(result.City);
			$(jsHelper.AddHashTag(result.Args.State)).val(result.State);
		});

}

/*
Method for offsetting TimesOfDayWindowEnd's available times by TimesOfDayWindowStart's selected value
Expected control Ids: TimesOfDayWindowStart, TimesOfDayWindowEnd.
	
No results are returned with this method.
*/
function OffsetDropDownListByTimeOfDay(hasEmptyStringOption, ids) {
    var ddlTimesOfDayWindowStart = $(jsHelper.AddHashTag(ids.TimesOfDayWindowStart));
    var ddlTimesOfDayWindowEnd = $(jsHelper.AddHashTag(ids.TimesOfDayWindowEnd));
    
    var offset = $(jsHelper.AddHashTag(ids.TimesOfDayWindowStart + " option:selected")).index();
    
    var previousValue = ddlTimesOfDayWindowEnd.val();
    ddlTimesOfDayWindowEnd[0].options.length = 0;

    var index = 0;
    if (hasEmptyStringOption) {
        ddlTimesOfDayWindowEnd[0].options[0] = new Option(jsHelper.EmptyString, jsHelper.EmptyString); // Add the empty string option
        index++;
        offset--; //account for empty string option
    }

    var valueSet = false;

    for (index; index + offset < ddlTimesOfDayWindowStart[0].options.length; index++) {
        ddlTimesOfDayWindowEnd[0].options[index] = new Option(ddlTimesOfDayWindowStart[0].options[index + offset].text, ddlTimesOfDayWindowStart[0].options[index + offset].value);
        
        if (ddlTimesOfDayWindowStart[0].options[index + offset].value == previousValue && previousValue != jsHelper.EmptyString) {
            ddlTimesOfDayWindowEnd[0].options[index].selected = true;
            valueSet = true;
        }
    }
    
    if (!valueSet) {
        ddlTimesOfDayWindowEnd[0].options[hasEmptyStringOption ? 1 : 0].selected = true;
    }
}

/*
Method for setting cms content as current content within cms content section code
Expected control Ids: CmsContentCurrentStatus and CmsContentId
	
result will have:
[string variable]	string	- message if applicable else blank
*/
function SetCmsContentCurrentStatus(adminUserId, ids) {
	new Ws().eShipPlusWSv4P.SetContentCurrentStatus(adminUserId, $(jsHelper.AddHashTag(ids.CmsContentId)).val(), jsHelper.IsChecked(ids.CmsContentCurrentStatus),
		function(result) {
			if (result != '') alert(result);
		});
}


/*
Method for setting news/event article as archived i.e. hidden
Expected control Ids: CmsNewsHiddenStatus and CmsNewsId
	
result will have:
[string variable]	string	- message if applicable else blank
*/
function SetCmsNewsHiddenStatus(adminUserId, ids) {
	new Ws().eShipPlusWSv4P.SetNewsHiddenStatus(adminUserId, $(jsHelper.AddHashTag(ids.CmsNewsId)).val(), jsHelper.IsChecked(ids.CmsNewsHiddenStatus),
		function(result) {
			if (result != '') alert(result);
		});
}



/*
Method for setting label units for dimensions and weights used in freight density calculations.
Expected controld Ids: DenistyWeight, DensityLength, DensityWidth, DensityHeight and DensityCalc as html span id's

No results are returned with this method.
*/
function SetUnitsLabel(unitType, ids) {
	switch (unitType) {
		case jsHelper.EnglishUnits:

			$(jsHelper.AddHashTag(ids.DensityWeight)).html('<abbr title="Pounds"> (lb)</abbr>');
			$(jsHelper.AddHashTag(ids.DensityLength)).html('<abbr title="Inches"> (in)</abbr>');
			$(jsHelper.AddHashTag(ids.DensityWidth)).html('<abbr title="Inches"> (in)</abbr>');
			$(jsHelper.AddHashTag(ids.DensityHeight)).html('<abbr title="Inches"> (in)</abbr>');
			$(jsHelper.AddHashTag(ids.DensityCalc)).html(' lb/ft<sup>3</sup>');
			break;

		default:

			$(jsHelper.AddHashTag(ids.DensityWeight)).html('<abbr title="Kilograms"> (kg)</abbr>');
			$(jsHelper.AddHashTag(ids.DensityLength)).html('<abbr title="Centimeters"> (cm)</abbr>');
			$(jsHelper.AddHashTag(ids.DensityWidth)).html('<abbr title="Centimeters"> (cm)</abbr>');
			$(jsHelper.AddHashTag(ids.DensityHeight)).html('<abbr title="Centimeters"> (cm)</abbr>');
			$(jsHelper.AddHashTag(ids.DensityCalc)).html(' kg/m<sup>3</sup>');
			break;
	}
}


/*
Method is particularly setup to valid date string
		
Expected control Ids: Date control (must implement val())
		
result will have:
Args			ControlId() class
Valid			bool	- true if valid else false
ErrorMessage	string	- empty if no error message
*/
function ValidateDateString(ids) {
    if ($(jsHelper.AddHashTag(ids.DateString)).val() == jsHelper.EmptyString) return;

    new Ws().eShipPlusWSv4P.DateStringIsValid($(jsHelper.AddHashTag(ids.DateString)).val(), ids,
		function (result) {
		    if (result.ErrorMessage != jsHelper.EmptyString) {
		        $(jsHelper.AddHashTag(ids.DateString)).val(jsHelper.EmptyString);
		        alert(result.ErrorMessage);
		    }
		});

}

/* 
Method is particularly setup to valid time string
		
Expected control Ids: Time control (must implement val())
		
result will have:
Args			ControlId() class
Valid			bool	- true if valid else false
ErrorMessage	string	- empty if no error message
*/
function ValidateTimeString(ids) {
	if ($(jsHelper.AddHashTag(ids.TimeString)).val() == jsHelper.EmptyString) return;

    new Ws().eShipPlusWSv4P.TimeStringIsValid($(jsHelper.AddHashTag(ids.TimeString)).val(), ids,
		function(result) {
			if (result.ErrorMessage != jsHelper.EmptyString) {
			    $(jsHelper.AddHashTag(ids.TimeString)).val(jsHelper.EmptyString);
			    $(jsHelper.AddHashTag(ids.TimeString)).focus();
				alert(result.ErrorMessage);
			}
		});

}


/*
Method setup to post an asset to DAT and return the result of the posting
		
Expected control Ids: DatAssetId, DatExpirationDate, DatDateCreated, DatWasSentAsHazMat, DatPostedByUser, BtnUpdateAsset
                                  BtnRemoveAsset, BtnPostAsset, DatLoadboardErrors
		
result will have:
Args			        ControlId() class
AssetId     			string	- empty if failure, value if success
ExpirationDate			string	- empty if failure, date as string value if success
DateCreated			    string	- empty if failure, date as string value if success
BaseRate			    decimal	- empty if failure, value if success
RateType			    string	- empty if failure, either 'Flat' or 'PerMile' if success
Mileage 			    string	- empty if failure, mileage of either the shipment or load order if success
WasSentAsHazMat			bool	- true if valid else false
PostedBy                string  - empty if failure, user full name who posted asset if success
ErrorMessage	        string	- empty if no error message
*/
function PostAssetToDatLoadboard(userId, recordId, rate, rateType, comments, ids) {
    new Ws().eShipPlusWSv4P.PostAssetToDat(userId, recordId, rate, rateType, comments, ids, function (result) {
		    $(jsHelper.AddHashTag(ids.DatAssetId)).val(result.AssetId);
		    $(jsHelper.AddHashTag(ids.DatExpirationDate)).val(result.ExpirationDate);
		    $(jsHelper.AddHashTag(ids.DatDateCreated)).val(result.DateCreated);
		    $(jsHelper.AddHashTag(ids.DatBaseRate)).val(result.BaseRate);
		    $(jsHelper.AddHashTag(ids.DatRateType)).val(result.RateType);
		    $(jsHelper.AddHashTag(ids.DatMileage)).val(result.Mileage);
		    $(jsHelper.AddHashTag(ids.DatPostedByUser)).val(result.PostedBy);
		    $(jsHelper.AddHashTag(ids.HidDatAssetId)).val(result.AssetId);
	    

            // if posting was succesful, disable the DAT comment text boxes & delete buttons and toggle the button visibilties 
		    if (result.ErrorMessage == jsHelper.EmptyString) {
		        
		        $(jsHelper.AddHashTag(ids.BtnUpdateAsset)).show();
		        $(jsHelper.AddHashTag(ids.BtnRemoveAsset)).show();
		        $(jsHelper.AddHashTag(ids.BtnPostAsset)).hide();
		        
		        $('[id$="txtDatAssetComment"]').each(function () {
		            $(this).addClass("disabled");
		            $(this).attr("readonly", true);
		        });

		        $('[id$="ibtnDelete"]').each(function () {
		            $(this).hide();
		        });
		        
		        $('[id$="hidDatCommentExists"]').each(function () {
		            $(this).val("true");
		        });
		    }
        
		    if (result.WasSentAsHazMat) {
                jsHelper.CheckBox(ids.DatWasSentAsHazMat);
                SetAltUniformCheckBoxClickedStatus($('#' + ids.DatWasSentAsHazMat));
		        }

		    if (result.ErrorMessage != jsHelper.EmptyString) {
		    	alert(result.ErrorMessage);
		    	$(jsHelper.AddHashTag(ids.BtnUpdateAsset)).hide();
		    	$(jsHelper.AddHashTag(ids.BtnRemoveAsset)).hide();
		    	$(jsHelper.AddHashTag(ids.BtnPostAsset)).show();
		    	$(jsHelper.AddHashTag(ids.DatLoadboardErrors)).html(result.ErrorMessage);
            }
        
    });
}

/*
Method setup to update an asset in DAT and return the update rate & rate type if it was successful

Expected control Ids: DatBaseRate, DatRateType, DatLoadboardErrors

result will have:
Args			            ControlId() class
BaseRate			        decimal - empty if failure, value if success
RateType			        string - empty if failure, value if success
ErrorMessage	            string - empty if failure, value if success
*/
function UpdateAssetToDatLoadboard(userId, recordId, tenantId, rate, rateType, comments, assetId, ids) {
    new Ws().eShipPlusWSv4P.UpdateAssetOnDat(userId, recordId, tenantId, rate, rateType, assetId, comments, ids, function (result) {
        
            if (result.Success) {
                $(jsHelper.AddHashTag(ids.DatBaseRate)).val(result.BaseRate);
                $(jsHelper.AddHashTag(ids.DatRateType)).val(result.RateType);
                
                // Disable any newly added comments & their delete buttons
                $('[id$="txtDatAssetComment"]').each(function () {
                    $(this).addClass("disabled");
                    $(this).attr("readonly",true);
                });

                $('[id$="hidDatCommentExists"]').each(function () {
                    $(this).val("true");
                });
                
                $('[id$="ibtnDelete"]').each(function () {
                    $(this).hide();
                });
                
                alert("DAT Posting Updated.");
            }

            if (result.ErrorMessage != jsHelper.EmptyString) {
                $(jsHelper.AddHashTag(ids.DatLoadboardErrors)).html(result.ErrorMessage);
                alert(result.ErrorMessage);
            }
    });
}

/*
Method setup to delete an asset from DAT and return the true/false
		
Expected control Ids: DatAssetId, DatExpirationDate, DatDateCreated, DatBaseRate, DatRateType, DatMileage, DatPostedByUser,
                                  BtnUpdateAsset, BtnPostAsset, BtnRemoveAsset, DatWasSentAsHazMat, DatLoadboardErrors
		
result will have:
Args			        ControlId() class
Success                 bool    - true if success, false if failure
ErrorMessage	        string	- empty if no error message
*/
function DeleteAssetFromDatLoadboard(userId, recordId, ids) {
    new Ws().eShipPlusWSv4P.DeleteAssetFromDat(userId, recordId, ids, function (result) {
        
		    if (result.Success) {
		        $(jsHelper.AddHashTag(ids.DatAssetId)).val(jsHelper.EmptyString);
		        $(jsHelper.AddHashTag(ids.DatExpirationDate)).val(jsHelper.EmptyString);
		        $(jsHelper.AddHashTag(ids.DatDateCreated)).val(jsHelper.EmptyString);
		        $(jsHelper.AddHashTag(ids.DatBaseRate)).val(jsHelper.EmptyString);
		        $(jsHelper.AddHashTag(ids.DatRateType)).val(jsHelper.EmptyString);
		        $(jsHelper.AddHashTag(ids.DatMileage)).val(jsHelper.EmptyString);
		        $(jsHelper.AddHashTag(ids.DatPostedByUser)).val(jsHelper.EmptyString);
		        $(jsHelper.AddHashTag(ids.BtnUpdateAsset)).hide();
		        $(jsHelper.AddHashTag(ids.BtnPostAsset)).show();
		        $(jsHelper.AddHashTag(ids.BtnRemoveAsset)).hide();
		        jsHelper.UnCheckBox(ids.DatWasSentAsHazMat);
		        SetAltUniformCheckBoxClickedStatus($('#' + ids.DatWasSentAsHazMat));
		    }
	    
		    if (result.ErrorMessage != jsHelper.EmptyString) {
			    
		    	$(jsHelper.AddHashTag(ids.BtnUpdateAsset)).show();
		    	$(jsHelper.AddHashTag(ids.BtnPostAsset)).hide();
		    	$(jsHelper.AddHashTag(ids.BtnRemoveAsset)).show();
		    	$(jsHelper.AddHashTag(ids.DatLoadboardErrors)).html(result.ErrorMessage);
		        alert(result.ErrorMessage);
		    }
    });

}

/*
Method setup to lookup rates for a DAT Asset
		
Expected control Ids: DatLoadboardErrors, DatRatesContainer, DatRatesDiv
		
result will have:
Args			        ControlId() class
Success                 bool    - true if success, false if failure
ErrorMessage	        string	- empty if no error message
Rates                   string[][]   - empty if failure, if success then JSON 2D array containing info with different rates returned
*/
function LookupRateFromDatLoadboard(userId, recordId, assetId, ids) {
    new Ws().eShipPlusWSv4P.LookupCurrentDatRate(userId, recordId, assetId, ids, function(result) {
        if (result.Success) {
            var table = '<table class="stripe">';
            var data = $.parseJSON(result.Rates);
            table += '<tr>' +
                '<th>Average Fuel Surcharge Rate</th>  ' +
                '<th>Estimated Line Haul Rate</th>' +
                '<th>Estimated Line Haul Total</th>' +
                '<th>High Line Haul Rate</th>' +
                '<th>High Line Haul Total</th>' +
                '<th>Low Line Haul Rate</th>' +
                '<th>Low Line Haul Total</th></tr>';

            $.each(data, function(index, item) {
                table += '<tr><td>$' + item[0] + '</td><td>$' + item[1] + '</td><td>$' + item[2] + '</td><td>$' + item[3] + '</td>' +
                    '<td>$' + item[4] + '</td>' + '<td>$' + item[5] + '</td>' + '<td>$' + item[6] + '</td></tr>';
            });

            table += '</table>';

            // bind this HTML table to the rates div contained in the DatLoadboardAssetControl
            $(jsHelper.AddHashTag(ids.DatRatesDiv)).html(table);
            $(jsHelper.AddHashTag(ids.DatRatesContainer)).show();
        }

        if (result.ErrorMessage != jsHelper.EmptyString) {
            alert(result.ErrorMessage);
            $(jsHelper.AddHashTag(ids.DatLoadboardErrors)).html(result.ErrorMessage);
        }
    });
}
//-----------------------------------------------------------------------------------