﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EmailTemplate.ascx.cs"
    Inherits="LogisticsPlus.Eship.WebApplication.Notifications.EmailTemplate" %>
<asp:Panel runat="server" ID="pnlEmail" Visible="false"  CssClass="popupControlOver">
    <table width="800px">
        <tr class="title">
            <td colspan="2" style="text-align: center;">
                <asp:Literal runat="server" Text="TITLE" ID="title" />
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <b>To:</b>
            </td>
            <td style="text-align: left;">
                <eShip:CustomTextBox runat="server" ID="txtTo" Width="500px"></eShip:CustomTextBox>
            </td>
        </tr>
        <tr>
            <asp:Panel runat="server" ID="pnlCc" Visible="true">
                <td style="text-align: right;">
                    <b>Cc:</b>
                </td>
                <td style="text-align: left;">
                    <eShip:CustomTextBox runat="server" ID="txtCc" Width="500px"></eShip:CustomTextBox>
                </td>
            </asp:Panel>
        </tr>
        <tr>
            <asp:Panel runat="server" ID="pnlBcc" Visible="true">
                <td style="text-align: right;">
                    Cc:
                </td>
                <td style="text-align: left;">
                    <eShip:CustomTextBox runat="server" ID="txtBcc" Width="500px"></eShip:CustomTextBox>
                </td>
            </asp:Panel>
        </tr>
        <tr>
            <td style="text-align: right;">
                <b>Subject:</b>
            </td>
            <td style="text-align: left;">
                <eShip:CustomTextBox runat="server" ID="txtSubject" Width="500px"></eShip:CustomTextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align: right; vertical-align: top;">
                <b>Body:</b>
            </td>
            <td style="text-align: left;">
                <eShip:CustomTextBox runat="server" ID="txtBody" TextMode="MultiLine" Rows="10" Width="500px" />
            </td>
        </tr>
        <tr>
            <td style="text-align: right; Width:500px;"><b>Attachments:</b></td>
            <td style="text-align:left;"><asp:Literal runat="server" ID="attachment"></asp:Literal></td>
        </tr>
        <tr>
            <td><asp:Button runat="server"  ID="btnSend" Text="SEND" OnClick="OnSendClicked"/></td>
            <td><asp:Button runat="server"  ID="btnCancel" Text="SEND" OnClick = OnCancelClicked/> </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="pnlEmailDimScreen" CssClass="dimBackground" runat="server"
	Visible="false" />
