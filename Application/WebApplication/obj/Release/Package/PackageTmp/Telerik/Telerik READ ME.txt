﻿When implementing Telerik, the following changes were made outside the Telerik folder this file sits in.

1. Web.Config
	- handlers for the WebResource, SpellChecker, and DialogHandler specifically
		<add name="Telerik_Web_UI_DialogHandler_aspx" verb="*" preCondition="integratedMode" path="Telerik.Web.UI.DialogHandler.aspx" type="Telerik.Web.UI.DialogHandler"/>
		<add name="Telerik_Web_UI_WebResource_axd" verb="*" preCondition="integratedMode" path="Telerik.Web.UI.WebResource.axd" type="Telerik.Web.UI.WebResource"/>
		<add name="Telerik_Web_UI_SpellCheckHandler_axd" verb="*" preCondition="integratedMode" path="Telerik.Web.UI.SpellCheckHandler.axd" type="Telerik.Web.UI.SpellCheckHandler"/>
	  in the handlers section
		<add path="Telerik.Web.UI.DialogHandler.aspx" type="Telerik.Web.UI.DialogHandler" verb="*" validate="false"/>
		<add path="Telerik.Web.UI.WebResource.axd" type="Telerik.Web.UI.WebResource" verb="*" validate="false"/>
		<add path="Telerik.Web.UI.SpellCheckHandler.axd" type="Telerik.Web.UI.SpellCheckHandler" verb="*" validate="false"/>
	  in the httphandlers section
	  
2. Stylesheet Telerik/Css/EditorStyles.css is referenced in Public/com/PublicInternal.master 
	so that any content managed in the CMS will retain custom classes set in the editor

3. CMS edit pages both use the RadEditor Control, back side sets some of the properties to reduce clutter on the front side

