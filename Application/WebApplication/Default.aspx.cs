﻿using System;
using System.Web.UI.HtmlControls;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.WebApplication.Members;

namespace LogisticsPlus.Eship.WebApplication
{
	public partial class _Default : PageBase
	{
		public static string PageAddress { get { return "~/Default.aspx"; } }

		protected void Page_Load(object sender, EventArgs e)
		{
			if (IsPostBack) return;

			// add meta for login
			var desc = new HtmlMeta
			{
				HttpEquiv = "description",
				Name = "description",
				Content = "Log in page for the Logistics Plus® eshipplus™ transportation management system (TMS). Manage your LTL, truckload, and more."
			};
			Page.Header.Controls.Add(desc);
		}

		protected void OnSuccessfulAuthentication(object sender, ViewEventArgs<User> e)
		{
			e.Argument.SetAuthenticatedUser(Request.QueryString[WebApplicationConstants.RedirectUrlPrefix] ?? DashboardView.PageAddress);
		}

	    public override string PageName
	    {
            get { return "eshipplus log in for Transportation Management – Logistics Plus"; }
	    }
	}
}