﻿using System;
using System.IO;
using System.Web.UI;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication
{
    public abstract class PageBase : Page
    {
        protected override object LoadPageStateFromPersistenceMedium()
        {
            var formatter = new LosFormatter();
            var bytes = Convert.FromBase64String(Request.Form["__VSTATE"]).ToDecompressedBytes();
            return formatter.Deserialize(Convert.ToBase64String(bytes));
        }

        protected override void SavePageStateToPersistenceMedium(object viewState)
        {
            var formatter = new LosFormatter();
            var writer = new StringWriter();
            formatter.Serialize(writer, viewState);
            var viewStateString = writer.ToString();
            var bytes = Convert.FromBase64String(viewStateString).ToCompressedBytes();
            
            var sm = ScriptManager.GetCurrent(this);
            if (sm != null && sm.IsInAsyncPostBack)
                ScriptManager.RegisterHiddenField(this, "__VSTATE", Convert.ToBase64String(bytes));
            else
                Page.ClientScript.RegisterHiddenField("__VSTATE", Convert.ToBase64String(bytes));
        }


        private readonly ViewStateCompressor _viewStateCompressor;

        protected override PageStatePersister PageStatePersister
        {
            get{ return _viewStateCompressor; }
        }


        public abstract string PageName { get; }

        protected override void OnInit(EventArgs e)
        {
            if (WebApplicationSettings.SiteSslEnabled && !Request.IsSecureConnection)
            {
                Response.Redirect(Request.ResolveSiteRootWithHttp() + Request.Url.Query);
                return;
            }


			if (Request.Url.AbsoluteUri.StartsWith("http://eshipplus.net") ||
			    Request.Url.AbsoluteUri.StartsWith("http://eshipplus.com") ||
				Request.Url.AbsoluteUri.StartsWith("https://eshipplus.net") ||
				Request.Url.AbsoluteUri.StartsWith("https://eshipplus.com"))
			{
				Response.Redirect(Request.ResolveSiteRootWithHttp() + Request.Url.Query); // redirection will have www. appended in resolve site root.
				return;
			}
			

            base.OnInit(e);
        }

        protected override void OnLoad(EventArgs e)
        {
            SetPageTitle(string.Empty);
            base.OnLoad(e);
        }

        protected void SetPageTitle(string prefix, bool withPageName = true)
        {
            var title = string.Format("eShipPlus v{0}", WebApplicationSettings.Version);
            if (withPageName) title = string.Format("{0} :: {1}", PageName, title);
            if (!string.IsNullOrEmpty(prefix)) title = string.Format("{0} :: {1}", prefix, title);
            Page.Header.Title = title;
        }


        protected PageBase()
        {
            _viewStateCompressor = new ViewStateCompressor(this);
        }
    }
}
