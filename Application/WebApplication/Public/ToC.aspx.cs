﻿using System;
using System.Web.UI;

namespace LogisticsPlus.Eship.WebApplication.Public
{
	public partial class ToC : Page
	{
		public static string PageAddress { get { return "~/Public/ToC.aspx"; } }

		protected void Page_Load(object sender, EventArgs e)
		{
			Page.Title = "eShip Plus v4.x :: Terms & Conditions";

			hypDefaultPage.NavigateUrl = _Default.PageAddress;
		}
	}
}