﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Base.Master" AutoEventWireup="true" CodeBehind="ToC.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Public.ToC" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BaseContentPlaceHolder" runat="server">
    <div id="mainContainer">
        <div id="header" class="shadow">
            <div class="header_top bottom_shadow">
                <h1 class="logo">
                    <asp:HyperLink runat="server" ID="hypDefaultPage">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/eshiplogo.png" AlternateText="Logistics+ eShipPlus"
                            Height="75px" />
                    </asp:HyperLink>
                </h1>
                <div id="alertsbar">
                    <!-- Add class .alerts to #alertsbar, or add div with class .alerts inside #alertsbar for the red text -->
                    <%--<div class="alerts right">
						Here comes the notification ticker with the updates and the annoncements that may
						turn to bright red if needed.
					</div>--%>
                </div>
                <div class="filterAndLoginInfo">
                    &nbsp;
                </div>
            </div>
            <div id="toolbar">
            </div>
        </div>
        <div id="content" class="shadow clearfix">
            <div class="clearfix">

                <div style="margin: 20px 100px; text-align: left;">
                    <p>
                        Upon logging into this web site, you will be shown confidential information that
				    is the property of Logistics Plus. By entering this site, we ask that you agree
				    to not disclose to any other person or entity the information provided to you. In
				    addition, as a user, you are agreeing to the following web site and shipment Terms
				    &amp; Conditions.
                    </p>

                    <div class="text-center pt10 mb30">
                        <h1>TERMS &amp; CONDITIONS for USE of eShipPlus WEB SITE</h1>
                    </div>

                    <p>
                        PLEASE REVIEW THESE TERMS AND CONDITIONS OF USE BEFORE USING THIS WEB SITE (THE
							&quot;WEB SITE&quot;). THESE WEB SITE TERMS AND CONDITIONS (THE &quot;TERMS OF USE&quot;)
							GOVERN YOUR ACCESS TO AND USE OF THE WEB SITE. THE WEB SITE IS AVAILABLE FOR YOUR
							USE ONLY ON THE CONDITION THAT YOU AGREE TO THE TERMS OF USE SET FORTH BELOW. IF
							YOU DO NOT AGREE TO ALL OF THE TERMS OF USE, DO NOT ACCESS OR USE THE WEB SITE.
							BY ACCESSING OR USING THE WEB SITE, YOU AND ANY ENTITY YOU ARE AUTHORIZED TO REPRESENT
							(&quot;YOU&quot; OR &quot;YOUR&quot;) SIGNIFY YOUR AGREEMENT TO BE BOUND BY THE
							TERMS OF USE.
                    </p>
                    <p>
                        The enrolled Customer, Shipper and/or Consignee (hereinafter collectively referred
							to as &quot;Customer&quot;) agrees to these TERMS AND CONDITIONS which no agent
							or employee of the parties may alter. These TERMS AND CONDITIONS shall apply to
							this and all future shipments scheduled by Customer, unless and until these TERMS
							AND CONDITIONS are altered or amended by EShip Plus (hereinafter referred to as
							“Broker”) issuance of new TERMS AND CONDITIONS which can be found at <a href="http://www.logisticsplus.net/">www.logisticsplus.net</a>.
                    </p>
                    <p>
                        The General Rules Tariffs, as set forth by the carriers, will in every instance
							take precedence in all legal proceedings and when applicable, will take precedence
							over the Broker's TERMS AND CONDITIONS stated herein. If not stated within the carrier's
							General Rules Tariff, the Broker's TERMS AND CONDITIONS as stated herein shall control.
							In the case of conflict between the TERMS AND CONDITIONS contained herein and those
							set forth by the individual selected carrier's General Rules Tariff, the selected
							carrier's General Rules Tariff shall control. All Terms, including, but not limited
							to, all the limitations of liability, shall apply to the selected carrier and their
							agents and contracted carriers.
                    </p>
                    <p>
                        The Broker is a freight broker and NOT a freight carrier. The Broker reserves the
							right, in its sole discretion, to refuse any shipment at any time.
                    </p>

                    <h5 class="dark mb5">1. Bills of Lading</h5>
                    <p>
                        All Bills of Lading are NON-NEGOTIABLE and have been prepared by the enrolled Customer
							or by Broker as Customer's agent on behalf of the Customer and shall be deemed,
							conclusively, to have been prepared by the Customer and to bind Customer. Any unauthorized
							alteration or use of Bills of Lading or tendering of shipments to any carrier other
							than that designated by the Broker, or the use of any Bill of Lading not authorized
							or issued by the Broker shall VOID the Broker's obligations to make any payments
							relating to this shipment and VOID all rate quotes.
                    </p>

                    <h5 class="dark mb5">2. Agreement to Terms</h5>
                    <p>
                        Customer agrees that international and domestic carriage by a Carrier of any shipment
							tendered using eShip shall be in accordance with the terms, conditions and limitation
							of liability set on the non- negotiable BOL, Air Waybill, Label, Manifest, or Pick-up
							record (collectively “shipping documentation”) and as appropriate any transportation
							agreement between customer and Broker and/or Carrier covering such shipment and
							in any applicable Tariff, Service Guide, or Standard Conditions of Carriage, which
							are incorporated into this agreement by reference. If there is a conflict between
							the shipping documentation and any such document then in effect or this agreement,
							the transportation agreement, Tariff Service Guide, Standard Conditions of Carriage,
							or this agreement will control in this order of priority.
                    </p>
                    <p>
                        If a shipment originated outside U.S., the contract of carriage is with the Broker’s
							subsidiary, branch, or independent contractor who originally accepts the shipment.
							Your use of this web site shall not alter your responsibility for the preparation
							and accuracy of shipping documentation including export/import.
                    </p>

                    <h5 class="dark mb5">3. Printed Signature</h5>
                    <p>
                        You acknowledge that if you process shipments to locations outside the country where
							your shipment originates, you must enter, to print in lieu of a manual signature
							on the Air Waybill, the name of the person completing the Air Waybill for all such
							shipments tendered using eShip. You further acknowledge that such printed name shall
							be sufficient to constitute signature of the Air Waybill on your behalf for purpose
							of the Warsaw Convention and for all other purposes, and your acceptance of the
							Carrier’s terms and conditions of carriage contained in the applicable Carrier’s
							Service Guide, Standard Conditions, Tariff Air Waybill or transportation agreement
							under which the shipment is accepted by the Carrier, Broker, or its independent
							contractor.
                    </p>

                    <h5 class="dark mb5">4. Customer Warranties</h5>
                    <p>
                        The Customer is responsible for and warrants their compliance with all applicable
							laws, rules, and regulations including but not limited to customs laws, import and
							export laws and governmental regulation of any country to, from, through or over
							which the shipment may be carried. The Customer agrees to furnish such information
							and complete and attach to this Bill of Lading such documents as are necessary to
							comply with such laws, rules and regulations. The Broker assumes no liability to
							the Customer or to any other person for any loss or expense due to the failure of
							the Customer to comply with this provision. Any individual or entity acting on behalf
							of the Customer in scheduling shipments hereunder warrants that it has the right
							to act on behalf of the Customer and the right to legally bind Customer.
                    </p>

                    <h5 class="dark mb5">5. Necessary Documentation</h5>
                    <p>
                        The Customer is required to use the Broker's system generated Bill of Lading. If
							the Customer does not complete all the documents required for carriage, or if the
							documents which they submit are not appropriate for the services, pick up or destination
							requested, the Customer hereby instructs the Broker, where permitted by law, to
							complete, correct or replace the documents for them at the expense of the Customer.
							However, the Broker is not obligated to do so. If a substitute form of Bill of Lading
							is needed to complete delivery of this shipment and the Broker completes that document,
							the terms of this Bill of Lading will govern. The Broker is not liable to the Customer
							or to any other person for any actions taken on behalf of the Customer under this
							provision.
                    </p>

                    <h5 class="dark mb5">6. Payment</h5>
                    <p>
                        All charges are payable in US Dollars and are due and payable fourteen (14) days
							from the date of billing, and any payment which is past due shall be subject to
							an additional charge at the rate of 1-1/2% per month of the average outstanding
							balance due, or the highest rate of interest permitted by applicable law, whichever
							is less. All funds received by the Broker will be applied to the oldest (based on
							pick-up date) invoiced Bill of Lading that is outstanding. Overpayments do not accrue
							interest and are subject to the Law of the
							<st1:placetype w:st="on"><st1:place w:st="on">Commonwealth</st1:place>
				 of <st1:PlaceName w:st="on">Pennsylvania</st1:PlaceName></st1:placetype>
                        . In the event the Broker retains an attorney or collection agency to collect unpaid
							charges or for the enforcement of these TERMS AND CONDITIONS, all unpaid charges
							will be subject to a late payment penalty of 33% and Customer shall also be liable
							for all attorneys and collection agency fees incurred, together with related costs
							and expenses. All shippers, consignors, consignees, freight forwarders or freight
							brokers are jointly and severally liable for the freight charges relating to this
							shipment.
                    </p>
                    <p>
                        All Customers are subject to credit approval. The Broker intends to perform a credit
							check based on the information provided at the time of enrollment by the Customer.
							The amount of credit, if any, granted to the Customer is at the sole discretion
							of the Broker. When paying by credit card or electronic funds, the Customer agrees
							they will be responsible for all charges payable, including any adjustments, on
							account of such Customer's shipment. These charges and adjustments, if any, will
							be automatically debited to the Customer's credit card or bank account.
                    </p>
                    <p>
                        The Customer shall be liable, jointly and severally, for all charges payable on
							account of such Customer's shipment, including but not limited to transportation,
							fuel and other applicable accessorial charges, including all adjustments issued
							by the carrier(s) after the shipment, and all duties, customs assessments, governmental
							penalties and fines, taxes, and Broker's attorney fees and legal costs allocable
							to this shipment and/or all disputes related thereto. The Broker shall have a lien
							on the shipment for all sums due it relating to this shipment or any other amounts
							owed by Customer. The Broker reserves the right to amend or adjust the original
							quoted amount or re-invoice the Customer if the original quoted amount was based
							upon incorrect information provided at the time of the original quote or if additional
							services by the carrier were required or otherwise authorized by the Customer to
							perform the pick up, transportation and delivery functions therein. Customer is
							permitted thirty (30) business days from the date of the invoice to dispute any
							invoiced charges. If the Broker does not receive a dispute within the allowable
							thirty (30) business days, the disputed item will be denied by the Broker.
                    </p>

                    <h5 class="dark mb5">7. Claims and Limitations of Liability</h5>
                    <p>
                        The individual carrier's governing General Rules Tariff determines the standard
							liability cargo insurance coverage offered by all carriers. If the shipment contains
							freight with a predetermined exception value, as determined by the selected carrier,
							the maximum exception liability will override the otherwise standard liability coverage.
							The filing of a claim does not relieve the responsible party for payment of freight
							charges. Freight payment is necessary in order for a carrier to process a claim.
							All freight cargo claims should be submitted immediately to the Broker to help ensure
							timely resolution. The Broker will attempt to assist in the resolution of freight
							claims, but has no responsibility or liability therefore. No claim will be reviewed
							until all shipping and related charges have been paid to Broker. All packaging and
							containers must be made available for inspection by Broker. Insurance claim payments,
							minus $100.00 USD deductible, will be made in U.S. dollars. Please contact the Broker
							for more details regarding carrier insurance or carrier liability.
                    </p>
                    <p>
                        Where Broker files damage claim with carrier on behalf of Customer and receives
							recovery funds, The Broker has a lien on such recovery amounts and reserves the
							right to apply recovery amounts to open past due invoices on account. This includes
							recovery amounts received from carrier for freight charges and/or product damage
							claim amounts.
                    </p>
                    <p>
                        The Broker is not liable for any loss, damage, mis-delivery or non-delivery caused
							by the act, default or omission of the Carrier. The Broker is not liable for any
							loss, mis-delivery or non-delivery caused by the act, default or omission of the
							Customer or any other party who claims interest in the shipment, or caused by the
							nature of the shipment or any defect thereof. The Broker is not liable for losses,
							mis-delivery or non-delivery caused by violation(s) by the Customer of any of the
							TERMS AND CONDITIONS contained in the Bill of Lading or of the carrier's General
							Rules Tariff including, but not limited to, improper or insufficient packing, securing,
							marking or addressing, or of failure to observe any of the rules relating to shipments
							not acceptable for transportation or shipments acceptable only under certain conditions.
							The Broker is not liable for losses, mis-delivery or non-delivery caused by the
							acts of God, perils of the air, public enemies, public authorities, acts or omissions
							of Customs or quarantine officials, war, riots, strikes, labor disputes, weather
							conditions or mechanical delay or failure of aircraft or other equipment. The Broker
							is not liable for failure to comply with delivery or other instructions from the
							Customer or for the acts or omissions of any person other than employees of the
							Broker.
                    </p>
                    <p>
                        Subject to the limitations of liability contained in the Bill of Lading and the
							carrier's General Rules Tariff, the Broker shall only be liable for loss, damage,
							mis-delivery or non-delivery caused by the Broker's own gross negligence. The Broker's
							liability therefore shall be limited to the fees that the Broker has earned with
							respect to the subject shipment.
                    </p>
                    <p>
                        THE BROKER MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, WITHOUT LIMITATION,
							WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, WITH REGARD TO
							DELIVERIES OR WITH REGARD TO THIS WEBSITE, INFORMATION PROVIDED ON THIS WEBSITE
							OR SERVICES RELATED TO TRANSACTIONS CONDUCTED ON THIS WEBSITE. THE BROKER CANNOT
							GUARANTEE DELIVERY BY ANY SPECIFIC TIME OR DATE. IN ANY EVENT, THE BROKER SHALL
							NOT BE LIABLE FOR ANY SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, INCLUDING BUT
							NOT LIMITED TO LOSS OF PROFITS OR INCOME, WHETHER OR NOT THE BROKER HAD KNOWLEDGE
							THAT SUCH DAMAGES MIGHT BE INCURRED.
                    </p>

                    <h5 class="dark mb5">8. Rates</h5>
                    <p>
                        Domestics LTL rates are based on the freight class as determined by the NMFC (National
							Motor Freight Classification) and are weight based. All displayed transit times
							are estimates only and do not include day of pickup. LTL pickup dates are not guaranteed.
							Broker will not process TL shipments through this service. Rates for TL shipments
							can be negotiated directly with Logistics Plus North America Division (1-866-564-7587).
                    </p>

                    <h5 class="dark mb5">9. Guaranteed Services</h5>
                    <p>
                        Guaranteed LTL Services are inclusive of transit times only as noted 
                        by the Carrier selected. Guaranteed LTL Service transit times do not include holiday 
                        and/or no service days as defined by the individual Carrier. Guaranteed LTL Service 
                        shipments must be scheduled before 3:00 pm the day of pickup, and must be picked up no 
                        later than 5:00 pm. Deliveries requiring special services, equipment, liftgates, COD, order 
                        notifications, or appointments are not eligible for Guaranteed LTL Service. Shipments not 
                        delivered within date/time specified on the bill of lading may not be considered a service 
                        failure when the reason for the delivery delay is deemed as no fault of the Carrier. These 
                        reasons could include, but are not limited to, the following conditions: acts of God; the 
                        existence of violence, riots, military action or such possible disturbance as creating reasonable 
                        apprehension of danger; acts or omissions by: shipper, consignee, owner of goods or public 
                        authority; delays due to customs clearance or documentation required for movement of shipment; 
                        closure of federal, state, city or local roads, streets, or highways resulting in travel 
                        delays by Carrier; shipments not accepted by the consignee when offered for delivery. Additional 
                        terms and conditions may apply according to the Carrier's published Rules Tariff. This service 
                        is not a guarantee for pickup. Pickup Day is not included in the qualification and calculation 
                        of transit time. In the event of Carrier failure to comply with the guaranteed service requested, 
                        the Broker will cancel 100% of shipment charges on customer request. If the shipment charges have 
                        already been paid, then a refund will be issued in the amount of the paid charges. In no event shall 
                        the Broker be liable nor will any account be credited if the customer does not use the broker's bill 
                        of lading.
                    </p>

                    <h5 class="dark mb5">10. Domestic Transit Insurance</h5>
                    <p>
                        Through the purchase of the quoted cargo insurance, underwritten by Continental
							Insurance Company and issued to Logistics Plus, Policy Number OC 24-2486, the enrolled
							Customer will receive an insurance policy equal to the declared value amount entered.
                    </p>
                    <p>
                        <b>FAILURE TO PURCHASE CARGO INSURANCE WILL RESULT IN CUSTOMER BEING LIMITED TO RECOVERY
								PURSUANT TO THE TERMS OF CARRIER'S GENERAL RULES TARIFF.</b>
                    </p>
                    <p>
                        Upon completion of the purchasing and approval process, including cargo insurance
							coverage acceptance and final shipment confirmation, a Certificate of Insurance
							will be issued to the indicated Certificate Holder by the end of the next business
							day. The Certificate of Insurance is issued by Continental Insurance Company and
							the purchaser is bound by the terms and conditions of the cargo insurance policy
							number indicated above. The full Policy is held by Logistics Plus.
                    </p>
                    <p>
                        The Broker has no responsibility, liability or involvement in the issuance of insurance,
							the denial of insurance, or in the payment of claims. In the event of cargo loss
							or damage, the Certificate Holder is to contact the Claim Agent noted on the Certificate
							of Insurance immediately. If the loss or damage is apparent, the consignee must
							note such loss or damage information on the Bill of Lading/delivery receipt. If
							the loss or damage is not apparent (concealed), the Certificate Holder must contact
							the Claims Agent noted on the certificate within 3 days after taking delivery.
                    </p>
                    <p>
                        <b><u>THE COVERAGE</u></b>
                    </p>
                    <p>
                        The Policy covers goods in transit within the Continental United States and/or
							<st1:place w:st="on"><st1:country-region w:st="on">Canada</st1:country-region></st1:place>
                        , subject to the terms and conditions contained therein. Coverage is from door to
							door during the ordinary course of transit.
                    </p>
                    <p>
                        <b><u>Insuring Conditions:</u></b> All Risks of Physical Loss or Damage from any
							external cause.
                    </p>
                    <p>
                        <b><u>Exclusions:</u></b> Inherent Vice, inventory shortages or mysterious disappearance,
							nuclear reaction or nuclear radiation or radioactive contamination or insufficient
							packing
                    </p>
                    <p>
                        <b><u>Limit any one conveyance</u></b> : $250,000.00
                    </p>
                    <p>
                        The general policy covers New General Merchandise only. Special quotes can be obtained
							for alcoholic beverages, tobacco, bagged goods, cell phones, firearms, jewelry,
							precious metals, bank notes, securities, works of art, valuable papers, computer
							chips, circuit boards, dangerous goods, antiques, used goods, glass, ceramics, marble,
							fresh and frozen foods, confectionary, asbestos, tiles, furs, live animals, bulk
							commodities perishable goods, paper, newsprint, lumber, logs and plywood.
                    </p>
                    <p>
                        The Customer warrants that the interest insured hereunder is in good condition at
							the commencement of coverage. No claim for loss and/or damage shall attach unless,
							immediately on the first discovery of any loss and/or damage to any part of the
							interest hereby insured, written notice shall have been given to Continental Insurance
							Company. In no event shall any claim be recoverable hereunder unless notice is given
							to Continental Insurance Company within thirty days of the termination of this insurance.
                    </p>
                    <p>
                        Disclaimer: The above is a general overview of the Inland Transit Endorsement that
							forms part of the Open Policy mentioned above
                    </p>

                    <h5 class="dark mb5">11. Right to Reject Requests for Shipping Services</h5>
                    <p>
                        Broker reserves the right to reject any request for shipping in its sole discretion.
							Without limitation, any shipment containing any item that is considered a restricted
							article or hazardous material by the Department of Transportation (DOT), International
							Air Transport Association (IATA), or the International Civil Aviation Broker (ICAO),
							will not be shipped by Broker. Shipments containing items that cannot be transported
							legally or safely, include, but are not limited to:
                    </p>
                    <ul>
                        <li>Animals</li>
                        <li>Plants</li>
                        <li>Chemicals</li>
                        <li>Perishables</li>
                        <li>Currency</li>
                        <li>Precious Metals</li>
                        <li>Explosives</li>
                        <li>Precious Stones</li>
                        <li>Liquor</li>
                        <li>Negotiable items in Bearer Form</li>
                    </ul>
                    <p>
                        For further information concerning items that cannot be shipped by Broker, please
							call 866-335-7623.
                    </p>

                    <h5 class="dark mb5">12. Obligations of Broker</h5>
                    <p>
                        Broker and its agents and carriers agree to use commercially reasonable efforts
							to, either directly or indirectly:
                    </p>
                    <p>
                        (a) match the item(s) of each shipment against the item(s) set forth on the shipping
							directions from Customer; (b) inspect each shipment and note all apparent damage
							on the appropriate freight bill, delivery receipt, or similar document evidencing
							delivery, and notify Customer of such damage; and (c) deliver all shipments to locations
							directed by Customer. Broker may ship the items by any means, including truck, air,
							vessel, or any other carrier, unless Customer gives specific electronic or written
							instructions to the contrary.
                    </p>

                    <h5 class="dark mb5">13. Customer Release of Liability</h5>
                    <p>
                        Broker shall not be held liable by Customer for the following:
                    </p>
                    <ol style="list-style-type: lower-alpha" class="mb20">
                        <li>Accuracy of item description, its contents, or its condition.</li>
                        <li>Any other limit of liability related to the item being shipped.</li>
                    </ol>

                    <h5 class="dark mb5">14. Independent Contractor</h5>
                    <p>
                        Broker shall be an independent contractor with respect to Customer, and nothing
							herein contained shall be construed to be inconsistent with such relationship or
							status.
                    </p>
                    <p>
                        Broker shall engage and/or subcontract with such entities and/or individuals as
							it may deem necessary or appropriate in connection herewith, it being understood
							and agreed that such entities or individuals shall be subcontractors of Broker only
							and shall be subject to discipline and control solely and exclusively by Broker.
                    </p>

                    <h5 class="dark mb5">15. Obligations of Customer</h5>
                    <p>
                        Customer represents and warrants to Broker as follows: (a) all items to be shipped
							will be completely and accurately marked to enable identification of the contents
							without opening any shipping or storage containers; (b) Customer will make every
							effort to accurately measure the dimensions and weights of all items and understands
							that the Broker rate depends upon the accuracy of this information (c) Customer's
							authorized representative(s) shall be identified to Broker's agent or coordinator
							and shall be available at all times at the point of origination to sign, and shall
							sign, all documents evidencing pick-up of the items to be shipped by Broker; and
							Customer is the legally documented owner of all property received by Broker, and/or
							is authorized to cause such property to be stored and otherwise controlled by Broker
							as provided in the applicable Bill of Lading.
                    </p>

                    <h5 class="dark mb5">16. Carrier’s and Warehouseman's Lien</h5>
                    <p>
                        Customer acknowledges that Broker and its subcontractors have both a carrier's and
							warehouseman's general lien on all of the tangible personal property being handled
							pursuant to any Bill of Lading. This lien may be enforced by Broker and its subcontractors
							at any time at either a public or private sale with or without a judicial hearing.
							Customer also grants Broker a security interest in the tangible personal property
							being handled under any Bill of Lading until Customer has fully satisfied all liabilities,
							whenever occurring, owed to Broker. Broker is authorized to file financing statements
							under the Uniform Commercial Code covering any such tangible personal property without
							Customer's signature, and Broker shall have all the rights and remedies of a secured
							party under the Uniform Commercial Code.
                    </p>

                    <h5 class="dark mb5">17. Limitation of Warranty</h5>
                    <p>
                        EXCEPT AS OTHERWISE PROVIDED HEREIN, Broker MAKES NO WARRANTIES FOR THE SERVICES
							AND HEREBY DISCLAIMS ALL WARRANTIES OR REPRESENTATIONS, EXPRESSED OR IMPLIED, INCLUDING
							ANY IMPLIED WARRANTIES OF TITLE, NON-INFRINGEMENT, MERCHANTABILITY, FITNESS FOR
							A PARTICULAR PURPOSE OR USE, AND ANY WARRANTIES ARISING FROM COURSE OF DEALING,
							USAGE OR TRADE PRACTICE.
                    </p>

                    <h5 class="dark mb5">18. Compliance with Law</h5>
                    <p>
                        Each party shall, at all times while these TERMS AND CONDITIONS are in effect and
							at its own expense, comply with all applicable federal, state, and local laws, rules
							and regulations, and shall maintain in full force and effect all license and permits
							required for performance under these TERMS AND CONDITIONS.
                    </p>

                    <h5 class="dark mb5">19. Force Majeure</h5>
                    <p>
                        Any delay or failure of performance of either party to these TERMS AND CONDITIONS
							shall not constitute a breach or default of these TERMS AND CONDITIONS or any Bill
							of Lading, or give rise to any claims for damages, if and to the extent that such
							delay or failure is caused by an occurrence beyond the control of the party affected,
							including, but not limited to, acts of governmental authorities, acts of God, the
							discovery of materially different site conditions, wars, riots, rebellions, sabotage,
							fire, explosions, accidents, floods, strikes, lockouts, or changes in laws, regulations,
							or ordinances. In the event that a party intends to invoke this force majeure provision,
							that party shall provide prompt notice to the other party as soon as possible after
							the occurrence of the event giving rise to the claim of force majeure.
                    </p>

                    <h5 class="dark mb5">20. Entire Agreement</h5>
                    <p>
                        These TERMS AND CONDITIONS together with all Bills of Lading entered into between
							the parties completely and exclusively states the agreement of the parties regarding
							the subject matter hereof and supersedes all prior negotiations, representations
							or agreements with respect to the subject matter hereof, written or oral, and may
							be amended only by written instruments signed by all parties hereto. If any part
							of these TERMS AND CONDITIONS is found unenforceable, it will not affect the validity
							or enforceability of any other provision of these TERMS AND CONDITIONS.
                    </p>

                    <h5 class="dark mb5">21. Governing Law</h5>
                    <p>
                        THE VALIDITY, PERFORMANCE AND CONSTRUCTION OF THESE TERMS AND CONDITIONS AND ALL
							BILLS OF LADING HEREUNDER SHALL BE GOVERNED AND INTERPRETED IN ACCORDANCE WITH THE
							LAWS OF THE
							<st1:placetype w:st="on"><st1:place
				 w:st="on">COMMONWEALTH</st1:place> OF <st1:PlaceName w:st="on">PENNSYLAVANIA</st1:PlaceName></st1:placetype>
                        .
                    </p>
                    <p>
                        Any claim, dispute or litigation relating to these TERMS AND CONDITIONS, any shipment
							scheduled or tendered hereunder or through the Broker's website, or relating to
							any and all disputes between the Broker and the enrolled Customer, shall be filed
							in the Court having jurisdiction within the
							<st1:placetype w:st="on"><st1:place w:st="on">Commonwealth</st1:place> of <st1:PlaceName
				 w:st="on">Pennsylvania</st1:PlaceName></st1:placetype>
                        .
                    </p>

                    <h5 class="dark mb5">22. Venue and Personal Jurisdiction</h5>
                    <p>
                        Any action arising out of or relating to theses TERMS AND CONDITIONS and all Bills
							of Lading contemplated hereunder will be exclusively venued in a state or federal
							court situated within the
							<st1:placetype w:st="on"><st1:place
				 w:st="on">Commonwealth</st1:place> of <st1:PlaceName w:st="on">Pennsylvania</st1:PlaceName></st1:placetype>
                        . Customer hereby irrevocably consents and submits to the personal jurisdiction
							of said courts for all such purposes.
                    </p>

                    <h5 class="dark mb5">23. Counterparts</h5>
                    <p>
                        Facsimile and Electronic Signatures: These TERMS AND CONDITIONS and all Bills of
							Lading entered into hereunder may be executed in two or more counterparts, each
							of which will be considered an original, but all of which together will constitute
							one and the same instrument. These TERMS AND CONDITIONS and all Bills of Lading
							entered into hereunder may be executed by facsimile signature or by any other electronic
							means and such signatures shall be deemed to be originals for all purposes under
							these TERMS AND CONDITIONS and any Bill of Lading.
                    </p>

                    <h5 class="dark mb5">24. Eligibility</h5>
                    <p>
                        The Web Site is provided by eShipPlus and available only to entities and persons
							over the legal age of majority who can form legally binding contract(s) under applicable
							law. If You do not qualify, You are not permitted to use the Web Site.
                    </p>

                    <h5 class="dark mb5">25. Scope</h5>
                    <p>
                        These Terms of Use govern Your use of the Web Site and all applications, software,
							and services (&quot;Services&quot;) available on the Web Site.
                    </p>


                    <h5 class="dark mb5">26. Modification</h5>
                    <p>
                        eShipPlus may revise and update these Terms of Use at any time. Your continued usage
							of the Web Site after any changes to these Terms of Use will mean You accept those
							changes. Any aspect of the Web Site may be changed, supplemented, deleted or updated
							without notice at the sole discretion of eShipPlus. eShipPlus may also change or
							impose fees for products and services provided through the Web Site at any time
							in its sole discretion.
                    </p>

                    <h5 class="dark mb5">27. License and Ownership</h5>
                    <p>
                        Any and all intellectual property rights (&quot;Intellectual Property&quot;) associated
						with the Web Site and its contents (the &quot;Content&quot;) are the sole property
						of eShipPlus, its affiliates or third parties. The Content is protected by copyright
						and other laws in both the
						<st1:place w:st="on"><st1:country-region w:st="on">United States</st1:country-region></st1:place>
                        and other countries. The Components of the Web Site are also protected by trade
						dress, trade secret, unfair competition, and other laws and may not be copied or
						imitated in whole or in part. All custom graphics, icons, and other items that appear
						on the Web Site are trademarks, service marks or trade dress (&quot;Marks&quot;)
						of eShipPlus, its affiliates or other entities that have granted eShipPlus the right
						and license to use such Marks and may not be used or interfered with in any manner
						without the express written consent of eShipPlus. Except as otherwise expressly
						authorized by these Terms of Use, You may not copy, reproduce, modify, lease, loan,
						sell, create derivative works from, upload, transmit, or distribute the Intellectual
						Property of the Web Site in any way without eShipPlus' or the appropriate third
						party's prior written permission. Except as expressly provided herein, eShipPlus
						does not grant to You any express or implied rights to eShipPlus' or any third party's
						Intellectual Property. eShipPlus grants You a limited, personal, nontransferable,
						nonassignable, revocable license to (a) access and use the Web Site, Content and
						Services only in the manner presented by eShipPlus, and (b) access and use the eShipPlus
						computer and network services offered within the Web Site (the &quot; eShipPlus
						Systems&quot;) only in the manner expressly permitted by eShipPlus. Except for this
						limited license, eShipPlus does not convey any interest in or to the eShipPlus Systems,
						information or data available via the eShipPlus Systems (the &quot;Information&quot;),
						Content, Services, Web Site or any other eShipPlus property by permitting You to
						access the Web Site. Except to the extent required by law or as expressly provided
						herein, none of the Content and/or Information may be modified, reproduced, republished,
						translated into any language or computer language, re-transmitted in any form or
						by any means, resold, redistributed, reverse-engineered without the prior written
						consent of eShipPlus. You may not make, sell, offer for sale, modify, reproduce,
						display, publicly perform, import, distribute, retransmit or otherwise use the Content
						in any way, unless expressly permitted to do so by eShipPlus. 
                    </p>

                    <h5 class="dark mb5">28. Prohibited Uses</h5>
                    <p>
                        In addition to other conditions set forth in these Terms of Use, You agree that:
                    </p>
                    <ol style="list-style-type: lower-alpha" class="mb20">
                        <li>You shall not disguise the origin of information transmitted through the Web Site.</li>
                        <li>You will not place false or misleading information on the Web Site.</li>
                        <li>You will not use or access any service, information, application or software available
								via the Web Site in a manner not expressly permitted by eShipPlus.</li>
                        <li>You will not input or upload to the Web Site any information which contains viruses,
								Trojan horses, worms, time bombs or other computer programming routines that are
								intended to damage, interfere with, intercept or expropriate any system, the Web
								Site or Information or that infringes the Intellectual Property (defined below)
								rights of another.</li>
                        <li>Certain areas of the Web Site are restricted to customers of eShipPlus.</li>
                        <li>You may not use or access the Web Site or the eShipPlus Systems or Services in any
								way that, in eShipPlus' judgment, adversely affects the performance or function
								of the eShipPlus Systems, Services or the Web Site or interferes with the ability
								of authorized parties to access the eShipPlus Systems, Services or the Web Site.</li>
                        <li>You may not frame or utilize framing techniques to enclose any portion or aspect
								of the Content or the Information, without the express written consent of eShipPlus.</li>
                    </ol>

                    <h5 class="dark mb5">29. Termination</h5>
                    <p>
                        You agree that eShipPlus, in its sole discretion, may terminate or suspend Your
							use of the Web Site, the eShipPlus Systems, Information, Services and Content at
							any time and for any or no reason in its sole discretion, even if access and use
							continues to be allowed to others. Upon such suspension or termination, You must
							immediately (a) discontinue use of the Web Site, and (b) destroy any copies You
							have made of any portion of the Content. Accessing the Web Site, the eShipPlus Systems,
							Information or Services after such termination, suspension or discontinuation shall
							constitute an act of trespass. Further, You agree that eShipPlus shall not be liable
							to You or any third party for any termination or suspension of Your access to the
							Web Site, the eShipPlus Systems, Information and/or the Services.
                    </p>

                    <h5 class="dark mb5">30. Disclaimer of Warranties</h5>
                    <p>
                        eShipPlus MAKES NO REPRESENTATIONS ABOUT THE RESULTS TO BE OBTAINED FROM USING THE
							WEB SITE, THE eShipPlus SYSTEMS, THE SERVICES, THE INFORMATION OR THE CONTENT. THE
							USE OF SAME IS AT YOUR OWN RISK.
                    </p>
                    <p>
                        THE WEB SITE, THE eShipPlus SYSTEMS, THE INFORMATION, THE SERVICES AND THE CONTENT
							ARE PROVIDED ON AN &quot;AS IS&quot; BASIS. eShipPlus, ITS LICENSORS, AND ITS SUPPLIERS,
							TO THE FULLEST EXTENT PERMITTED BY LAW, DISCLAIM ALL WARRANTIES, EITHER EXPRESS
							OR IMPLIED, STATUTORY OR OTHERWISE, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES
							OF MERCHANTABILITY, NON-INFRINGEMENT OF THIRD PARTIES' RIGHTS, AND FITNESS FOR A
							PARTICULAR PURPOSE. eShipPlus AND ITS AFFILIATES, LICENSORS AND SUPPLIERS MAKE NO
							REPRESENTATIONS OR WARRANTIES ABOUT THE ACCURACY, COMPLETENESS, SECURITY OR TIMELINESS
							OF THE CONTENT, INFORMATION OR SERVICES PROVIDED ON OR THROUGH THE USE OF THE WEB
							SITE OR THE eShipPlus SYSTEMS. NO INFORMATION OBTAINED BY YOU FROM THE WEB SITE
							SHALL CREATE ANY WARRANTY NOT EXPRESSLY STATED BY eShipPlus IN THESE TERMS OF USE.
                    </p>
                    <p>
                        SOME JURISDICTIONS DO NOT ALLOW LIMITATIONS ON IMPLIED WARRANTY, SO THE LIMITATIONS
							AND EXCLUSIONS IN THIS SECTION MAY NOT APPLY TO YOU. IF YOU ARE DEALING AS A CONSUMER,
							YOUR STATUTORY RIGHTS THAT CANNOT BE WAIVED, IF ANY, ARE NOT AFFECTED BY THESE PROVISIONS.
							YOU AGREE AND ACKNOWLEDGE THAT THE LIMITATIONS AND EXCLUSIONS OF LIABILITY AND WARRANTY
							PROVIDED IN THESE TERMS OF USE ARE FAIR AND REASONABLE.
                    </p>

                    <h5 class="dark mb5">31. Limitation of Liability</h5>
                    <p>
                        TO THE EXTENT PERMITTED BY APPLICABLE LAW AND TO THE EXTENT THAT eShipPlus IS OTHERWISE
							FOUND RESPONSIBLE FOR ANY DAMAGES, eShipPlus IS RESPONSIBLE FOR ACTUAL DAMAGES ONLY.
							TO THE EXTENT PERMITTED BY LAW, IN NO EVENT SHALL eShipPlus, ITS AFFILIATES, ITS
							LICENSORS, ITS SUPPLIERS OR ANY THIRD PARTIES MENTIONED AT THE WEB SITE BE LIABLE
							FOR ANY INCIDENTAL, INDIRECT, EXEMPLARY, PUNITIVE AND CONSEQUENTIAL DAMAGES, LOST
							PROFITS, OR DAMAGES RESULTING FROM LOST DATA OR BUSINESS INTERRUPTION RESULTING
							FROM THE USE OF OR INABILITY TO USE THE WEB SITE, THE eShipPlus SYSTEMS, INFORMATION,
							SERVICES OR THE CONTENT WHETHER BASED ON WARRANTY, CONTRACT, TORT, DELICT, OR ANY
							OTHER LEGAL THEORY, AND WHETHER OR NOT eShipPlus IS ADVISED OF THE POSSIBILITY OF
							SUCH DAMAGES. TO THE EXTENT PERMITTED BY LAW, THE REMEDIES STATED FOR YOU IN THESE
							TERMS OF USE ARE EXCLUSIVE AND ARE LIMITED TO THOSE EXPRESSLY PROVIDED FOR IN THESE
							TERMS OF USE.
                    </p>

                    <h5 class="dark mb5">32. Governing Law and Jurisdiction</h5>
                    <p>
                        To the fullest extent permitted by law, these Terms of Use are governed by the laws
							of the
							<st1:placetype w:st="on"><st1:place w:st="on">Commonwealth</st1:place>
				 of <st1:PlaceName w:st="on">Pennsylvania</st1:PlaceName></st1:placetype>
                        . To the fullest extent permitted by law, you hereby expressly agree that any proceeding
							arising out of or relating to your use of the web site, the eShipPlus systems, information,
							services and content shall be instituted in a state or federal court located in
							the county of Erie and the Commonwealth of Pennsylvania and you expressly waive
							any objection that you may have now or hereafter to the laying of the venue or to
							the jurisdiction of any such proceeding. You agree that any claim or cause of action
							arising out of or related to your use of the web site, the eShipPlus systems, information,
							services and/or content must be filed within one (1) year after such claim or cause
							of action arose.
                    </p>

                    <h5 class="dark mb5">33. General</h5>
                    <p>
                        You may not assign these Terms of Use or any of Your interests, rights or obligations
							under these Terms of Use. If any provision of these Terms of Use is found to be
							invalid by any court having competent jurisdiction, the invalidity of such provision
							shall not affect the validity of the remaining provisions of these Terms of Use,
							which shall remain in full force and effect. No waiver of any of these Terms of
							Use shall be deemed a further or continuing waiver of such term or condition or
							any other term or condition.
                    </p>

                    <h5 class="dark mb5">34. Complete Agreement</h5>
                    <p>
                        Except as expressly provided in a separate license, service or other written agreement
							between you and eShipPlus, these terms of use constitute the entire agreement between
							you and eShipPlus with respect to the use of the web site, the eShipPlus systems,
							and any software or service, information and content contained therein, and supersede
							all discussions, communications, conversations and agreements concerning the subject
							matter hereof.
                    </p>

                </div>
            </div>
        </div>
        <div id="footer">
            <p>
                <eShip:FooterDetails ID="fdDetails" runat="server" />
            </p>
        </div>
    </div>
</asp:Content>
