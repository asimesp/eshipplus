﻿using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using System;
using System.Collections.Generic;

namespace LogisticsPlus.Eship.WebApplication.Public
{
	public partial class PublicTrack : System.Web.UI.Page, IShipmentFinderView
	{
		private Tenant _tenant;
		private User _activeUser;

		private Tenant Tenant
		{
			get { return _tenant ?? (_tenant = new TenantSearch().FetchTenantByCode(hidAccessCode.Value)); }
		}

		public User ActiveUser
		{
			get
			{
				return _activeUser ??
					   (_activeUser =
						(Tenant == null || Tenant.DefaultSystemUser == null ? new User() : Tenant.DefaultSystemUser));
			}
		}

		public bool FilterForShipmentsToBeInvoicedOnly { get; set; }
		public bool FilterForShipmentsToExludeAttachedToJob { get; set; }

		public event EventHandler<ViewEventArgs<ShipmentViewSearchCriteria>> Search;

		public void DisplayMessages(IEnumerable<ValidationMessage> messages)
		{
		}

		public void DisplaySearchResult(List<ShipmentDashboardDto> shipments)
		{
			rptSearchResult.DataSource = shipments;
			rptSearchResult.DataBind();
		}



		private List<ParameterColumn> GetCurrentRunParameters()
		{

			var result = new List<ParameterColumn>();
			if (!string.IsNullOrEmpty(txtOriginPostalCode.Text))
			{
				result.Add(new ParameterColumn
				{
					Operator = Operator.Equal,
					DefaultValue = txtOriginPostalCode.Text,
					ReportColumnName = OperationsSearchFields.OriginPostalCode.DisplayName
				});
			}

			if (!string.IsNullOrEmpty(txtCriteria.Text))
			{
				switch (ddlCriteriaType.SelectedValue)
				{
					case "0":
						result.Add(new ParameterColumn
						{
							Operator = Operator.Equal,
							ReportColumnName = OperationsSearchFields.ShipmentNumber.DisplayName,
							DefaultValue = txtCriteria.Text
						});
						break;
					case "1":
						result.Add(new ParameterColumn
						{
							Operator = Operator.Equal,
							ReportColumnName = OperationsSearchFields.PurchaseOrderNumber.DisplayName,
							DefaultValue = txtCriteria.Text
						});
						break;
					case "2":
						result.Add(new ParameterColumn
						{
							Operator = Operator.Equal,
							ReportColumnName = OperationsSearchFields.VendorProNumber.DisplayName,
							DefaultValue = txtCriteria.Text
						});
						break;
				}
			}

			return result;
		}

		private void DoSearch(ShipmentViewSearchCriteria criteria)
		{
			if (Search != null)
				Search(this, new ViewEventArgs<ShipmentViewSearchCriteria>(criteria));
		}



		protected void Page_Load(object sender, EventArgs e)
		{
			new ShipmentFinderHandler(this).Initialize();

			if (IsPostBack) return;

			if (Tenant != null)
			{
				imgVendorLogo.ImageUrl = Tenant.LogoUrl;
				imgVendorLogo.Visible = !string.IsNullOrEmpty(imgVendorLogo.ImageUrl);
			}
		}

		protected void OnTrackClicked(object sender, EventArgs e)
		{
			DoSearch(new ShipmentViewSearchCriteria
			{
				Parameters = GetCurrentRunParameters(),
				ActiveUserId = ActiveUser.Id
			});
		}


	}
}