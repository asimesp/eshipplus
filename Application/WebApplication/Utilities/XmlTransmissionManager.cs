﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Web;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Extern;
using LogisticsPlus.Eship.Extern.Edi;
using LogisticsPlus.Eship.Extern.Edi.Segments;
using LogisticsPlus.Eship.Extern.Edi.Validators;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Handlers.Connect;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Connect;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.WebApplication.Documents;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.WebApplication.Utilities.Que;
using ShipmentStatus = LogisticsPlus.Eship.Core.Operations.ShipmentStatus;
using LogisticsPlus.Eship.Core.Accounting;

namespace LogisticsPlus.Eship.WebApplication.Utilities
{
	public class XmlTransmissionManager
	{
		private const string EdiUnknown = "Edi: Unknown";
		private const string FtpUnknown = "Ftp: Unknown";
		private const string InvalidDataInOneOrMoreSegments = "Invalid data in one or more segments";
		private const string ControlNumberMismatch = "Control number mismatch";
		private const string InvalidNumberOfSegments = "Invalid number of segments";
		private const string InternalError = "Internal Error";

		private readonly HttpServerUtility _server;
		private bool _shutdownThread;
		private Thread _managerThread;
		

		public bool IsRunning
		{
			get { return _managerThread != null; }
		}

		public string ErrorsFolder { get; set; }

		public XmlTransmissionManager(HttpServerUtility server)
		{
			ErrorsFolder = string.Empty;
			_server = server;
		}

		public XmlTransmissionManager(string errorsFolder, HttpServerUtility server)
		{
			ErrorsFolder = errorsFolder;
			_server = server;
		}

		~XmlTransmissionManager()
		{
			Shutdown();
		}

		public void Start()
		{
			try
			{
				if (_managerThread == null)
				{
					// start the scheduling thread
					_managerThread = new Thread(ManagerThread);
					_managerThread.Start();
				}
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, ErrorsFolder);
			}
		}

		public void Shutdown()
		{
			if (_managerThread != null)
			{
				_shutdownThread = true;

				// wait for the scheduler thread to shutdown for up to 60 sec
				// if the thread does not shutdown on time, try to kill it
				if (!_managerThread.Join(60000))
				{
					try
					{
						_managerThread.Abort();
					}
					catch (Exception e)
					{
						ErrorLogger.LogError(e, ErrorsFolder);
					}
				}

				_managerThread = null;
			}
		}

		private void ManagerThread()
		{
			// delay before starting the thread
			Thread.Sleep(250);

			// init some state variables
			var counter = 4;

			if (_shutdownThread) return;

			do
			{
				// each actual thread iteration occurs in 1 minute intervals
				// however to make the tread more responsive it will sleep 4 x 250ms
				if (counter != 0)
				{
					Thread.Sleep(250);
					counter--;
					continue;
				}

				// reset the counter
				counter = 4;

				try
				{
					ProcessCustomerXmlTransmissions();
					ProcessVendorXmlTransmissions();
					ProcessShipmentStatusUpdateQue();
				}
				catch (Exception ex)
				{
					Debug.WriteLine("eShipPlus:ediManager : " + ex.Message);
					ErrorLogger.LogError(ex, ErrorsFolder);
				}
			} while (!_shutdownThread);
		}




		private void ProcessCustomerXmlTransmissions()
		{
			// from customer setups
			var ccoms = new CustomerSearch().FetchCustomerCommunicationsWithEdiOrFtp();

			foreach (var ccom in ccoms)
			{
				// edi
				if (ccom.EdiEnabled)
				{
					// processing load tender
					if (ccom.Pickup204)
					{
						var files = ccom.GetEdiFilesListing(EdiDocumentType.EDI204);
						foreach (var file in files)
						{
							var content = ccom.DownloadEdi(file, EdiDocumentType.EDI204).FromUtf8Bytes();
							if (string.IsNullOrEmpty(content)) continue;
							var load = content.FromXml<Edi204>();
							if (load == null)
								_server.NotifyXmlTransmissionError(new UnknownExternObjectType(content.StripTags()), EdiUnknown, ccom.TenantId);
							else
								ProcessLoadTender(ccom, content, load, NotificationMethod.Edi);

							if (ccom.EdiDeleteFileAfterPickup) ccom.DeleteFileEdi(file, EdiDocumentType.EDI204);

							Thread.Sleep(250); // slow down
						}
					}

					// processing acknowledgement
					if (ccom.Pickup997)
					{
						var files = ccom.GetEdiFilesListing(EdiDocumentType.EDI997);
						foreach (var file in files)
						{
							var content = ccom.DownloadEdi(file, EdiDocumentType.EDI997).FromUtf8Bytes();
							var ack = string.IsNullOrEmpty(content) ? null : content.FromXml<Edi997>();
							if (ack == null)
								_server.NotifyXmlTransmissionError(new UnknownExternObjectType(content.StripTags()), EdiUnknown, ccom.TenantId);
							else
								ProcessAcknowledgement(ack, ccom);

							if (ccom.EdiDeleteFileAfterPickup) ccom.DeleteFileEdi(file, EdiDocumentType.EDI997);

							Thread.Sleep(250); // slow down
						}
					}
				}

				// ftp
				if (ccom.FtpEnabled)
				{
					// processing load tender
					if (ccom.Pickup204)
					{
						var files = ccom.GetFtpFilesListing(EdiDocumentType.EDI204);
						foreach (var file in files)
						{
							var content = ccom.DownloadFtp(file, EdiDocumentType.EDI204).FromUtf8Bytes();
							if (string.IsNullOrEmpty(content)) continue;
							var load = content.FromXml<Edi204>();
							if (load == null)
								_server.NotifyXmlTransmissionError(new UnknownExternObjectType(content.StripTags()), FtpUnknown, ccom.TenantId);
							else
								ProcessLoadTender(ccom, content, load, NotificationMethod.Ftp);

							if (ccom.FtpDeleteFileAfterPickup) ccom.DeleteFileFtp(file, EdiDocumentType.EDI204);

							Thread.Sleep(250); // slow down
						}
					}

					// processing acknowledgement
					if (ccom.Pickup997)
					{
						var files = ccom.GetFtpFilesListing(EdiDocumentType.EDI997);
						foreach (var file in files)
						{
							var content = ccom.DownloadFtp(file, EdiDocumentType.EDI997).FromUtf8Bytes();
							var ack = string.IsNullOrEmpty(content) ? null : content.FromXml<Edi997>();
							if (ack == null)
								_server.NotifyXmlTransmissionError(new UnknownExternObjectType(content.StripTags()), FtpUnknown, ccom.TenantId);
							else
								ProcessAcknowledgement(ack, ccom);

							if (ccom.FtpDeleteFileAfterPickup) ccom.DeleteFileFtp(file, EdiDocumentType.EDI997);

							Thread.Sleep(250); // slow down
						}
					}
				}

				if (_shutdownThread) break;
			}
		}

		private void ProcessVendorXmlTransmissions()
		{
			// from vendor setups
			var vcoms = new VendorSearch().FetchVendorCommunicationsWithEdiOrFtp();

			foreach (var vcom in vcoms)
			{
				// edi
				if (vcom.EdiEnabled)
				{
					// process invoices
					if (vcom.Pickup210)
					{
						var files = vcom.GetEdiFilesListing(EdiDocumentType.EDI210);
						foreach (var file in files)
						{
							var content = vcom.DownloadEdi(file, EdiDocumentType.EDI210).FromUtf8Bytes();
							var invoice = string.IsNullOrEmpty(content) ? null : content.FromXml<Edi210>();
							if (invoice == null)
								_server.NotifyXmlTransmissionError(new UnknownExternObjectType(content.StripTags()), EdiUnknown, vcom.TenantId);
							else
								ProcessCarrierInvoice(vcom, content, invoice, NotificationMethod.Edi);

							if (vcom.EdiDeleteFileAfterPickup) vcom.DeleteFileEdi(file, EdiDocumentType.EDI210);

							Thread.Sleep(250); // slow down
						}
					}

					// process shipment status update
					if (vcom.Pickup214)
					{
						var files = vcom.GetEdiFilesListing(EdiDocumentType.EDI214);
						foreach (var file in files)
						{
							var content = vcom.DownloadEdi(file, EdiDocumentType.EDI214).FromUtf8Bytes();
							var edi214 = string.IsNullOrEmpty(content) ? null : content.FromXml<Edi214>();
							if (edi214 == null)
								_server.NotifyXmlTransmissionError(new UnknownExternObjectType(content.StripTags()), EdiUnknown, vcom.TenantId);
							else
								ProcessShipmentStatusUpdate(vcom, content, edi214, NotificationMethod.Edi);

							if (vcom.EdiDeleteFileAfterPickup) vcom.DeleteFileEdi(file, EdiDocumentType.EDI214);

                            Thread.Sleep(250); // slow down
                        }
					}

					// process response to load tender
					if (vcom.Pickup990)
					{
						var files = vcom.GetEdiFilesListing(EdiDocumentType.EDI990);
						foreach (var file in files)
						{
						var content = vcom.DownloadEdi(file, EdiDocumentType.EDI990).FromUtf8Bytes();
						var edi990 = string.IsNullOrEmpty(content) ? null : content.FromXml<Edi990>();
						if (edi990 == null)
							_server.NotifyXmlTransmissionError(new UnknownExternObjectType(content.StripTags()), EdiUnknown, vcom.TenantId);
						else
							ProcessResponseToLoadTender(vcom, content, edi990, NotificationMethod.Edi);
						if (vcom.EdiDeleteFileAfterPickup) vcom.DeleteFileEdi(file, EdiDocumentType.EDI990);

						Thread.Sleep(250); // slow down
						}
					}

					// process functional acknowledge
					if (vcom.Pickup997)
					{
						var files = vcom.GetEdiFilesListing(EdiDocumentType.EDI997);
						foreach (var file in files)
						{
							var content = vcom.DownloadEdi(file, EdiDocumentType.EDI997).FromUtf8Bytes();
							var ack = string.IsNullOrEmpty(content) ? null : content.FromXml<Edi997>();
							if (ack == null)
								_server.NotifyXmlTransmissionError(new UnknownExternObjectType(content.StripTags()), EdiUnknown, vcom.TenantId);
							else
								ProcessAcknowledgement(ack, vcom);
							if (vcom.EdiDeleteFileAfterPickup) vcom.DeleteFileEdi(file, EdiDocumentType.EDI997);

							Thread.Sleep(250); // slow down
						}
					}
				}

				// ftp
				if (vcom.FtpEnabled)
				{
					// process invoices
					if (vcom.Pickup210)
					{
						var files = vcom.GetFtpFilesListing(EdiDocumentType.EDI210);
						foreach (var file in files)
						{
							var content = vcom.DownloadFtp(file, EdiDocumentType.EDI210).FromUtf8Bytes();
							var invoice = string.IsNullOrEmpty(content) ? null : content.FromXml<Edi210>();
							if (invoice == null)
								_server.NotifyXmlTransmissionError(new UnknownExternObjectType(content.StripTags()), FtpUnknown, vcom.TenantId);
							else
								ProcessCarrierInvoice(vcom, content, invoice, NotificationMethod.Ftp);

							if (vcom.EdiDeleteFileAfterPickup) vcom.DeleteFileFtp(file, EdiDocumentType.EDI210);

							Thread.Sleep(250); // slow down
						}
					}

					// process shipment status update
					if (vcom.Pickup214)
					{
						var files = vcom.GetFtpFilesListing(EdiDocumentType.EDI214);
						foreach (var file in files)
						{
							var content = vcom.DownloadFtp(file, EdiDocumentType.EDI214).FromUtf8Bytes();
							var edi214 = string.IsNullOrEmpty(content) ? null : content.FromXml<Edi214>();
							if (edi214 == null)
								_server.NotifyXmlTransmissionError(new UnknownExternObjectType(content.StripTags()), FtpUnknown, vcom.TenantId);
							else
								ProcessShipmentStatusUpdate(vcom, content, edi214, NotificationMethod.Ftp);

							if (vcom.EdiDeleteFileAfterPickup) vcom.DeleteFileFtp(file, EdiDocumentType.EDI214);

							Thread.Sleep(250); // slow down
						}
					}

					// process response to load tender
					if (vcom.Pickup990)
					{
						var files = vcom.GetFtpFilesListing(EdiDocumentType.EDI990);
						foreach (var file in files)
						{
							var content = vcom.DownloadFtp(file, EdiDocumentType.EDI990).FromUtf8Bytes();
							var edi990 = string.IsNullOrEmpty(content) ? null : content.FromXml<Edi990>();
							if (edi990 == null)
								_server.NotifyXmlTransmissionError(new UnknownExternObjectType(content.StripTags()), FtpUnknown, vcom.TenantId);
							else
								ProcessResponseToLoadTender(vcom, content, edi990, NotificationMethod.Ftp);

							if (vcom.EdiDeleteFileAfterPickup) vcom.DeleteFileFtp(file, EdiDocumentType.EDI990);

							Thread.Sleep(250); // slow down
						}
					}

					// process functional acknowledge
					if (vcom.Pickup997)
					{
						var files = vcom.GetFtpFilesListing(EdiDocumentType.EDI997);
						foreach (var file in files)
						{
							var content = vcom.DownloadFtp(file, EdiDocumentType.EDI997).FromUtf8Bytes();
							var ack = string.IsNullOrEmpty(content) ? null : content.FromXml<Edi997>();
							if (ack == null)
								_server.NotifyXmlTransmissionError(new UnknownExternObjectType(content.StripTags()), FtpUnknown, vcom.TenantId);
							else
								ProcessAcknowledgement(ack, vcom);

							if (vcom.EdiDeleteFileAfterPickup) vcom.DeleteFileFtp(file, EdiDocumentType.EDI997);

							Thread.Sleep(250); // slow down
						}
					}
				}

				if (_shutdownThread) break;
			}
		}

		private void ProcessShipmentStatusUpdateQue()
		{
			var objs = new ProcQueObjSearch()
				.FetchQue()
				.GroupBy(i => i.ObjType, i => i)
				.Select(g => new
					{
						Type = g.Key,
						Values = g.Select(x => x).ToList()
					})
				.ToList();


			if (!objs.Any()) return;

			foreach (var obj in objs)
				switch (obj.Type)
				{
					case ProcQueObjType.ShipmentStatusQue:
						ProcessQuedCheckCalls(obj.Values);
						break;

                    case ProcQueObjType.ShipmentDocumentQue:
                        ProcessQuedShipmentDocuments(obj.Values);
                        break;
					default:
						continue;
				}
		}

        private void ProcessQuedShipmentDocuments(IEnumerable<ProcQueObj> procQueObjs)
        {
            var phandler = new ProcQueObjProcessHandler();
            var shandler = new ShipmentUpdateHandler();

            var ques = procQueObjs
                .Select(p => new { Proc = p, Que = p.Xml.FromXml<ShipmentDocumentQue>() })
                .GroupBy(s => s.Que.ShipmentId, s => new { s.Proc, s.Que })
                .ToDictionary(g => g.Key, g => g.Select(x => x).OrderBy(x => x.Que.Id).ToList());

            foreach (var q in ques)
            {
                var shipment = new Shipment(q.Key, true);
                shipment.LoadCollections();
                Exception ex;

                if (!shipment.KeyLoaded)
                {
                    // delete que for record not found and continue
                    foreach (var v in q.Value)
                    {
                        phandler.Delete(v.Proc, out ex);
                        if (ex != null) ErrorLogger.LogError(ex, ErrorsFolder);
                    }
                    continue;
                }

                foreach (var document in q.Value.Select(v => v.Que.RetrieveShipmentDocument()))
                    shipment.Documents.Add(document);

                // save shipment
                var errs = shandler.UpdateShipmentTrackingInfo(shipment, out ex);
                if (ex != null)
                {
                    ErrorLogger.LogError(ex, ErrorsFolder);
                    continue;
                }
                if (errs.Any())
                {
                    continue;
                }
                foreach (var v in q.Value)
                {
                    phandler.Delete(v.Proc, out ex);
                    if (ex != null) ErrorLogger.LogError(ex, ErrorsFolder);
                }
            }
        }

	    private void ProcessQuedCheckCalls(IEnumerable<ProcQueObj> procQueObjs)
		{
			var phandler = new ProcQueObjProcessHandler();
			var shandler = new ShipmentUpdateHandler();

			var ques = procQueObjs
				.Select(p => new { Proc = p, Que = p.Xml.FromXml<ShipmentStatusUpdateQue>() })
				.GroupBy(s => s.Que.ShipmentId, s => new {s.Proc, s.Que})
				.ToDictionary(g => g.Key, g => g.Select(x => x).OrderBy(x => x.Que.Id).ToList());
			var ediCodes = ProcessorUtilities.GetAll<ShipStatMsgCode>().Values.ToList();

		    var checkCallsToUpdate = new List<CheckCall>();

			foreach (var q in ques)
			{
				

				var shipment = new Shipment(q.Key, true);
                shipment.LoadCollections();
				Exception ex;

				if (!shipment.KeyLoaded)
				{
					// delete que for record not found and continue
					foreach (var v in q.Value)
					{
						phandler.Delete(v.Proc, out ex);
						if (ex != null) ErrorLogger.LogError(ex, ErrorsFolder);
					}
					continue;
				}

				foreach (var v in q.Value)
				{
					var shipmentDocRtrvLogRequiredIfApplicable = false;

					var cc = v.Que.RetrieveCheckCall();
					shipment.CheckCalls.Add(cc);

                    if(!string.IsNullOrEmpty(cc.EdiStatusCode)) checkCallsToUpdate.Add(cc);
					// pros
					foreach (var pro in v.Que.Pros)
					{
						var sv = shipment.Vendors.FirstOrDefault(i => i.Vendor != null && i.Vendor.Scac == pro.Scac);
						if (sv != null && string.IsNullOrEmpty(sv.ProNumber))
						{
							sv.TakeSnapShot();
							if (sv.ProNumber != pro.Pro) shipmentDocRtrvLogRequiredIfApplicable = true; // updated pro
							sv.ProNumber = pro.Pro;
						}
					}

					// statuses
					if (ediCodes.Contains(cc.EdiStatusCode))
						switch (cc.EdiStatusCode.ToEnum<ShipStatMsgCode>())
						{
							case ShipStatMsgCode.AF:
								shipment.ActualPickupDate = cc.EventDate;
								shipment.Status = shipment.InferShipmentStatus(false);
								break;
							case ShipStatMsgCode.D1:
								if (shipment.ActualDeliveryDate != cc.EventDate) shipmentDocRtrvLogRequiredIfApplicable = true; // updated delivery date
								shipment.ActualDeliveryDate = cc.EventDate;
								if (shipment.ActualPickupDate == DateUtility.SystemEarliestDateTime)
									shipment.ActualPickupDate = shipment.DesiredPickupDate; // set actual pickup to desired pickup if auto setting actual delivery and still missing actual pickup.
								shipment.Status = shipment.InferShipmentStatus(false);
								break;
							case ShipStatMsgCode.CA:
								shipment.Status = ShipmentStatus.Void;
								break;
							case ShipStatMsgCode.AG:
								shipment.EstimatedDeliveryDate = cc.EventDate;
								break;
							case ShipStatMsgCode.AA:
								shipment.Origin.TakeSnapShot();
								shipment.Origin.AppointmentDateTime = cc.EventDate;
								break;
							case ShipStatMsgCode.AB:
								shipment.Destination.TakeSnapShot();
								shipment.Destination.AppointmentDateTime = cc.EventDate;
								break;
						}

					if (shipmentDocRtrvLogRequiredIfApplicable) shipment.InsertShipmentDocRtrvLogFor(shipment.Tenant.DefaultSystemUser);  // insert doc rtrv log
				}

				// save shipment
				var errs = shandler.UpdateShipmentTrackingInfo(shipment, out ex);
				if (ex != null)
				{
                    checkCallsToUpdate.RemoveAll(c => c.ShipmentId == shipment.Id); 
					ErrorLogger.LogError(ex, ErrorsFolder);
					continue;
				}
				if (errs.Any())
				{
                    checkCallsToUpdate.RemoveAll(c => c.ShipmentId == shipment.Id); 
				    continue;
				}

				// delete que for record not found and continue
				foreach (var v in q.Value)
				{
					phandler.Delete(v.Proc, out ex);
					if (ex != null) ErrorLogger.LogError(ex, ErrorsFolder);
				}
			}

            ProcessAutoClientUpdate(checkCallsToUpdate);
		}



		private void ProcessResponseToLoadTender(VendorCommunication vcom, string content, Edi990 edi990, NotificationMethod method)
		{
			var validator = new Edi990Validator();
			

			// validations
			if (!validator.SetControlNumbersMatch(edi990))
			{
				ProcessReponseLoadTenderRejectAck(vcom, content, edi990, ControlNumberMismatch, SyntaxErrorCode.E3, method);
				return;
			}

			if (!validator.NumberOfSegmentsMatch(edi990))
			{
				ProcessReponseLoadTenderRejectAck(vcom, content, edi990, InvalidNumberOfSegments, SyntaxErrorCode.E5, method);
				return;
			}

			if (!validator.HasValidData(edi990))
			{
				ProcessReponseLoadTenderRejectAck(vcom, content, edi990, InvalidDataInOneOrMoreSegments, SyntaxErrorCode.E5, method);
				return;
			}

			var loSearch = new LoadOrderSearch();
			var xcSearch = new XmlConnectSearch();
			var records = new object[0].Select(o => new {Load = new LoadOrder(), Connect = new XmlConnect()}).ToList();
			records.Clear();
			foreach (var response in edi990.Responses)
			{
				var shipIdNum = response.BeginningSegment.ShipmentIdentificationNumber;
				var load = loSearch.FetchLoadOrderByLoadNumber(shipIdNum, vcom.TenantId);
				if (load == null)
				{
					ProcessReponseLoadTenderRejectAck(vcom, content, edi990, InvalidDataInOneOrMoreSegments, SyntaxErrorCode.E6, method);
					return;
				}

				var vendorNumber = vcom.Vendor.VendorNumber;
				var ltdto = xcSearch.FetchOutBoundLoadTenderByVendShipIdNumAndDocType(vendorNumber, shipIdNum, vcom.TenantId, vcom.Tenant.DefaultSystemUserId);
				if (ltdto.TenderRecordExpired)
				{
					ProcessReponseLoadTenderRejectAck(vcom, content, edi990, "Tender expired", SyntaxErrorCode.NA, method);
					return;
				}


				var existing990 = xcSearch.FetchOutboundLoadTenderResponseByShipmentIdNumber(shipIdNum, vcom.TenantId, vcom.Tenant.DefaultSystemUserId, vendorNumber);
				if (existing990 != null)
				{
					ProcessReponseLoadTenderRejectAck(vcom, content, edi990, "Tender response already exists", SyntaxErrorCode.NA, method);
					return;
				}

				var xc = new XmlConnect();
				xc.SetEmpty();
				xc.ShipmentIdNumber = load.LoadOrderNumber;
				xc.TenantId = vcom.TenantId;
				xc.Direction = Direction.Inbound;
				xc.ControlNumber = edi990.TransactionSetHeader.TransactionSetControlNumber;
				xc.DocumentType = EdiDocumentType.EDI990;
				xc.Xml = content;
				xc.DateCreated = DateTime.Now;
				xc.ReceiptDate = response.BeginningSegment.Date.ToDateTime();
				xc.CustomerNumber = string.Empty;
				xc.VendorNumber = vendorNumber;
				xc.VendorScac = vcom.Vendor.Scac;
				xc.Status = response.BeginningSegment.ReservationActionCode == ReservationCode.A
					            ? XmlConnectStatus.Accepted
					            : XmlConnectStatus.Rejected;
				xc.StatusMessage = response.BeginningSegment.ReservationActionCode == ReservationCode.D
					                   ? response.BeginningSegment.DeclineReason
					                   : string.Empty;

				records.Add(new {Load = load, Connect = xc});
			}
			
			// save updates
			Exception ex;
			new XmlTransmissionProcessHandler().SaveTransmission(records.Select(r=>r.Connect).ToList(), vcom.Tenant.DefaultSystemUser, out ex);
			if (ex != null)
			{
				ErrorLogger.LogError(ex, ErrorsFolder);
				ProcessReponseLoadTenderRejectAck(vcom, content, edi990, InternalError, SyntaxErrorCode.NA, method);
			}
			else
			{
				foreach (var r in records)
					_server.NotifyOutgoingLoadTenderResponseEvent(r.Connect, r.Load);

				// Acknowledge batch
				if (!vcom.AcknowledgePickup990) return;

				var xmlConnect = records[0].Connect;
				AcknowledgeEdiDocument(vcom, xmlConnect, SyntaxErrorCode.NA, method, AcknowledgementCode.A, edi990.TransactionSetHeader.TransactionSetControlNumber, TransSetIdentifierCode.RLT);
				for (var i = 1; i < records.Count; i++)
				{
					records[i].Connect.TakeSnapShot();
					records[i].Connect.TransmissionOkay = xmlConnect.TransmissionOkay;
				}

				// save all
				new XmlTransmissionProcessHandler().SaveTransmission(records.Select(r=>r.Connect).ToList(), vcom.Tenant.DefaultSystemUser, out ex);
				if (ex != null) ErrorLogger.LogError(ex, ErrorsFolder);
			}

		}

		private void ProcessReponseLoadTenderRejectAck(VendorCommunication vcom, string content, Edi990 edi990, string statusMsg, SyntaxErrorCode syntaxErrCode, NotificationMethod method)
		{
			if (!vcom.AcknowledgePickup990) return; // acknowledge only if set to do so

			var xc = new XmlConnect();
			xc.SetEmpty();
			xc.TenantId = vcom.TenantId;
			xc.Direction = Direction.Inbound;
			xc.ControlNumber = edi990.TransactionSetHeader.TransactionSetControlNumber;
			xc.DocumentType = EdiDocumentType.EDI990;
			xc.Xml = content;
			xc.Status = XmlConnectStatus.Rejected;
			xc.StatusMessage = statusMsg;
			xc.DateCreated = DateTime.Now;
			xc.VendorNumber = vcom.Vendor.VendorNumber;
			xc.VendorScac = vcom.Vendor.Scac;

			Exception e;
			new XmlTransmissionProcessHandler().SaveTransmission(xc, vcom.Tenant.DefaultSystemUser, out e);
			if (e != null) ErrorLogger.LogError(e, ErrorsFolder);

			// send rejection acknowledgement
			AcknowledgeEdiDocument(vcom, xc, syntaxErrCode, method, AcknowledgementCode.R, edi990.TransactionSetHeader.TransactionSetControlNumber, TransSetIdentifierCode.RLT);
		}





		private void ProcessLoadTender(CustomerCommunication ccom, string content, Edi204 edi204, NotificationMethod method)
		{
			var validator = new Edi204Validator();
			var search = new XmlConnectSearch();

			// validations
			if (!validator.SetControlNumbersMatch(edi204))
			{
				ProcessLoadTenderRejectAck(ccom, content, edi204, ControlNumberMismatch, SyntaxErrorCode.E3, method);
				return;
			}

			if (!validator.NumberOfSegmentsMatch(edi204))
			{
				ProcessLoadTenderRejectAck(ccom, content, edi204, InvalidNumberOfSegments, SyntaxErrorCode.E5, method);
				return;
			}

			if (!validator.HasValidFreightClasses(edi204, WebApplicationSettings.FreightClasses.Select(c=>c.GetString()).ToList()))
			{
				ProcessLoadTenderRejectAck(ccom, content, edi204, "Invalid freight class", SyntaxErrorCode.E5, method);
				return;
			}

			if (!validator.HasValidCountries(edi204, ProcessorVars.RegistryCache.Countries.Values.ToList()))
			{
				ProcessLoadTenderRejectAck(ccom, content, edi204, "Invalid country code", SyntaxErrorCode.E5, method);
				return;
			}

			if (!validator.HasValidCounts(edi204))
			{
				ProcessLoadTenderRejectAck(ccom, content, edi204, "Shipment detail level data totals does not add up to summary level", SyntaxErrorCode.E5, method);
				return;
			}

			if (!validator.HasValidHanldingRequirements(edi204, new ServiceSearch().FetchAllServices(ccom.TenantId)))
			{
				ProcessLoadTenderRejectAck(ccom, content, edi204, "Invalid special handling code", SyntaxErrorCode.E5, method);
				return;
			}

			var reject = false;
			var rejectMsg = string.Empty;
			var connectRecords = new List<XmlConnect>();
			var updateChanges = new Dictionary<string, List<EdiDiff>>();
			foreach (var l in edi204.Loads)
			{
				var stops = l.Stops.OrderBy(s => s.StopOffDetails.StopSequenceNumber).ToList();
				var origin = stops[0];
				var dest = stops[stops.Count - 1];

				XmlConnect xc = null;

				if (l.SetPurpose.TransactionSetPurpose == SetPurposeTransType.CA ||
				    l.SetPurpose.TransactionSetPurpose == SetPurposeTransType.UP)
				{
					xc = search.FetchInboundLoadTenderByShipmentIdNumber(l.BeginningSegment.ShipmentIdNumber, ccom.TenantId, ccom.Tenant.DefaultSystemUserId, ccom.Customer.CustomerNumber);
					if (xc == null)
					{
						reject = true;
						rejectMsg = string.Format("Missing record for update or cancellation - {0}", l.BeginningSegment.ShipmentIdNumber);
						break;
					}
					xc.TakeSnapShot(); // for update later


					// log changes between new and old
					var ol = xc.Xml.FromXml<Edi204>().Loads.FirstOrDefault(i => i.BeginningSegment.ShipmentIdNumber == xc.ShipmentIdNumber);
					var diff = l.Diff(ol);
					if (diff.Any())
					{
						if(!updateChanges.ContainsKey(xc.ShipmentIdNumber)) updateChanges.Add(xc.ShipmentIdNumber, new List<EdiDiff>());
						updateChanges[xc.ShipmentIdNumber].AddRange(diff);
					}

					
				}
				if (l.SetPurpose.TransactionSetPurpose == SetPurposeTransType.OR)
				{
					xc = search.FetchInboundLoadTenderByShipmentIdNumber(l.BeginningSegment.ShipmentIdNumber, ccom.TenantId, ccom.Tenant.DefaultSystemUserId, ccom.Customer.CustomerNumber);
					if (xc != null)
					{
						reject = true;
						rejectMsg = string.Format("Record already exists but sent as new - {0}", l.BeginningSegment.ShipmentIdNumber);
						break;
					}
				}

				if(xc == null) xc = new XmlConnect();

				xc.TenantId = ccom.TenantId;
				xc.ShipmentIdNumber = l.BeginningSegment.ShipmentIdNumber;
				xc.Direction = Direction.Inbound;
				xc.ControlNumber = edi204.TransactionSetHeader.TransactionSetControlNumber;
				xc.ExpirationDate = l.ExpirationDate.ToDateTime();
				xc.EquipmentDescriptionCode = l.EquipmentDetail == null ? string.Empty : l.EquipmentDetail.EquipmentDescriptionCode;
				xc.CustomerNumber = ccom.Customer.CustomerNumber;
				xc.VendorNumber = string.Empty;
                xc.VendorPro = string.Empty;
				xc.VendorScac = string.Empty;
				xc.DateCreated = DateTime.Now;
				xc.ReceiptDate = DateTime.Now;
				xc.DestinationCity = dest.GeographicLocation.CityName;
				xc.DestinationCountryCode = dest.GeographicLocation.CountryCode;
				xc.DestinationCountryName = string.Empty;
				xc.DestinationPostalCode = dest.GeographicLocation.PostalCode;
				xc.DestinationState = dest.GeographicLocation.StateOrProvinceCode;
				xc.DestinationStreet1 = dest.LocationStreetInformation.AddressInfo1;
				xc.DestinationStreet2 = dest.LocationStreetInformation.AddressInfo2;
				xc.DocumentType = EdiDocumentType.EDI204;
				xc.OriginCity = origin.GeographicLocation.CityName;
				xc.OriginCountryCode = origin.GeographicLocation.CountryCode;
				xc.OriginCountryName = string.Empty;
				xc.OriginPostalCode = origin.GeographicLocation.PostalCode;
				xc.OriginState = origin.GeographicLocation.StateOrProvinceCode;
				xc.OriginStreet1 = origin.LocationStreetInformation.AddressInfo1;
				xc.OriginStreet2 = origin.LocationStreetInformation.AddressInfo2;
				xc.PurchaseOrderNumber = (l.LoadReferenceNumbers.FirstOrDefault(r => r.ReferenceIdentificationQualifier == RefIdQualifier.PO) ??
										  new ReferenceNumber()).ReferenceIdentification.GetString();
				xc.ShipperReference = (l.LoadReferenceNumbers.FirstOrDefault(r => r.ReferenceIdentificationQualifier == RefIdQualifier.SHR) ??
									   new ReferenceNumber()).ReferenceIdentification.GetString();
				xc.TotalPackages = l.ShipmentWeightAndTrackingData.LadingPackages;
				xc.TotalPieces = l.ShipmentWeightAndTrackingData.LadingPieces;
				xc.TotalStops = l.Stops.Count;
				xc.TotalWeight = l.ShipmentWeightAndTrackingData.Weight.ToDecimal();
				xc.VendorScac = l.BeginningSegment.StandardCarrierAlphaCode;
			    xc.Xml = content;
				xc.Status = l.SetPurpose.TransactionSetPurpose == SetPurposeTransType.CA
								? XmlConnectStatus.Cancelled
								: XmlConnectStatus.Accepted;
				xc.StatusMessage = string.Empty;
				xc.FtpTransmission = method == NotificationMethod.Ftp;

				connectRecords.Add(xc);
			}

			if (reject)
			{
				ProcessLoadTenderRejectAck(ccom, content, edi204, rejectMsg, SyntaxErrorCode.E5, method);
				return;
			}

			Exception ex;
			new XmlTransmissionProcessHandler().SaveTransmission(connectRecords, ccom.Tenant.DefaultSystemUser, out ex);
			if (ex != null)
			{
				ErrorLogger.LogError(ex, ErrorsFolder);
				ProcessLoadTenderRejectAck(ccom, content, edi204, InternalError, SyntaxErrorCode.NA, method);
			}
			else
			{
				foreach (var connect in connectRecords)
				{
					var knownUpdatesToRecord = updateChanges.ContainsKey(connect.ShipmentIdNumber) ? updateChanges[connect.ShipmentIdNumber] : null;
					_server.NotifyIncomingLoadTenderEvent(connect, ccom.Customer, knownUpdatesToRecord);
				}

				// Acknowledge batch
				if (!ccom.AcknowledgePickup204) return;

				var xmlConnect = connectRecords[0];
				AcknowledgeEdiDocument(ccom, xmlConnect, SyntaxErrorCode.NA, method, AcknowledgementCode.A, edi204.TransactionSetHeader.TransactionSetControlNumber, TransSetIdentifierCode.LTR);
				for (var i = 1; i < connectRecords.Count; i++)
				{
					connectRecords[i].TakeSnapShot();
					connectRecords[i].TransmissionOkay = xmlConnect.TransmissionOkay;
				}

				// save all
				new XmlTransmissionProcessHandler().SaveTransmission(connectRecords, ccom.Tenant.DefaultSystemUser, out ex);
				if (ex != null) ErrorLogger.LogError(ex, ErrorsFolder);
			}
		}

		private void ProcessLoadTenderRejectAck(CustomerCommunication ccom, string content, Edi204 edi204, string statusMsg, SyntaxErrorCode syntaxErrCode, NotificationMethod method)
		{
			if (!ccom.AcknowledgePickup204) return; // acknowledge only if set to do so

			var xc = new XmlConnect();
			xc.SetEmpty();
			xc.TenantId = ccom.TenantId;
			xc.Direction = Direction.Inbound;
			xc.ControlNumber = edi204.TransactionSetHeader.TransactionSetControlNumber;
			xc.DocumentType = EdiDocumentType.EDI204;
			xc.Xml = content;
			xc.Status = XmlConnectStatus.Rejected;
			xc.StatusMessage = statusMsg;
			xc.DateCreated = DateTime.Now;
			xc.CustomerNumber = ccom.Customer.CustomerNumber;

			Exception e;
			new XmlTransmissionProcessHandler().SaveTransmission(xc, ccom.Tenant.DefaultSystemUser, out e);
			if (e != null) ErrorLogger.LogError(e, ErrorsFolder);

			// send rejection acknowledgement
			AcknowledgeEdiDocument(ccom, xc, syntaxErrCode, method, AcknowledgementCode.R, edi204.TransactionSetHeader.TransactionSetControlNumber, TransSetIdentifierCode.LTR);
		}



		private void ProcessCarrierInvoice(VendorCommunication vcom, string content, Edi210 edi210, NotificationMethod method)
		{
			var validator = new Edi210Validator();

			// validations
			if (!validator.SetControlNumbersMatch(edi210))
			{
				ProcessCarrierInvoiceRejectAck(vcom, content, edi210, ControlNumberMismatch, SyntaxErrorCode.E3, method);
				return;
			}

			if (!validator.NumberOfSegmentsMatch(edi210))
			{
				ProcessCarrierInvoiceRejectAck(vcom, content, edi210, InvalidNumberOfSegments, SyntaxErrorCode.E5, method);
				return;
			}

            var chargeCodes = ProcessorVars.RegistryCache[vcom.TenantId].ChargeCodes;
			if (!validator.HasValidChargeCodes(edi210, chargeCodes))
			{
				ProcessCarrierInvoiceRejectAck(vcom, content, edi210, "Invalid charge code code", SyntaxErrorCode.E5, method);
				return;
			}

			if (!validator.HasValidData(edi210, vcom.Vendor))
			{
				ProcessCarrierInvoiceRejectAck(vcom, content, edi210, InvalidDataInOneOrMoreSegments, SyntaxErrorCode.E5, method);
				return;
			}

			var sSearch = new ShipmentSearch();
			var tSearch = new ServiceTicketSearch();
			var vbSearch = new VendorBillSearch();

			var records = new object[0].Select(o => new { Bill = new VendorBill(), Connect = new XmlConnect() }).ToList();
			records.Clear();
			foreach (var inv in edi210.Invoices)
			{
				var shipment = !string.IsNullOrEmpty(inv.BeginningSegment.ShipmentIdNumber)
					               ? sSearch.FetchShipmentByShipmentNumber(inv.BeginningSegment.ShipmentIdNumber, vcom.TenantId)
					               : null;

				var refTicket = inv.ReferenceNumbers.FirstOrDefault(r => r.ReferenceIdentificationQualifier == RefIdQualifier.BRQ);
				var ticket = shipment == null && refTicket != null
					             ? tSearch.FetchServiceTicketByServiceTicketNumber(refTicket.ReferenceIdentification, vcom.TenantId)
					             : null;

				if (shipment == null && ticket == null)
				{
					ProcessCarrierInvoiceRejectAck(vcom, content, edi210, InvalidDataInOneOrMoreSegments, SyntaxErrorCode.E6, method);
					return;
				}

				var remitTo = vcom.Vendor.Locations.FirstOrDefault(l => l.RemitToLocation && l.MainRemitToLocation);
				if (remitTo == null)
				{
					ProcessCarrierInvoiceRejectAck(vcom, content, edi210, InternalError, SyntaxErrorCode.E5, method);
					return;
				}

				if (vbSearch.FetchVendorBillByVendorBillNumber(inv.BeginningSegment.InvoiceNumber, remitTo.Id, vcom.TenantId) != null)
				{
					ProcessCarrierInvoiceRejectAck(vcom, content, edi210, "Duplicate Invoice", SyntaxErrorCode.E5, method);
					return;
				}


				var xc = new XmlConnect();
				xc.SetEmpty();
				xc.TenantId = vcom.TenantId;
				xc.ShipmentIdNumber = inv.BeginningSegment.InvoiceNumber;
				xc.Direction = Direction.Inbound;
				xc.ControlNumber = edi210.TransactionSetHeader.TransactionSetControlNumber;
				xc.ExpirationDate = DateUtility.SystemEarliestDateTime;
				xc.EquipmentDescriptionCode = shipment != null && shipment.Equipments.Any() ? shipment.Equipments[0].EquipmentType.Code : string.Empty;
				xc.CustomerNumber = string.Empty;
				xc.VendorNumber = vcom.Vendor.VendorNumber;
				xc.VendorScac = vcom.Vendor.Scac;
				xc.DateCreated = DateTime.Now;
				xc.ReceiptDate = DateTime.Now;
				xc.DestinationCity = shipment != null ? shipment.Destination.City : string.Empty;
				xc.DestinationCountryCode = shipment != null ? shipment.Destination.Country.Code : string.Empty;
				xc.DestinationCountryName = shipment != null ? shipment.Destination.Country.Name : string.Empty;
				xc.DestinationPostalCode = shipment != null ? shipment.Destination.PostalCode : string.Empty;
				xc.DestinationState = shipment != null ? shipment.Destination.State : string.Empty;
				xc.DestinationStreet1 = shipment != null ? shipment.Destination.Street1 : string.Empty;
				xc.DestinationStreet2 = shipment != null ? shipment.Destination.Street2 : string.Empty;
				xc.DocumentType = EdiDocumentType.EDI210;
				xc.OriginCity = shipment != null ? shipment.Origin.City : string.Empty;
				xc.OriginCountryCode = shipment != null ? shipment.Origin.Country.Code : string.Empty;
				xc.OriginCountryName = shipment != null ? shipment.Origin.Country.Name : string.Empty;
				xc.OriginPostalCode = shipment != null ? shipment.Origin.PostalCode : string.Empty;
				xc.OriginState = shipment != null ? shipment.Origin.State : string.Empty;
				xc.OriginStreet1 = shipment != null ? shipment.Origin.Street1 : string.Empty;
				xc.OriginStreet2 = shipment != null ? shipment.Origin.Street2 : string.Empty;
				xc.PurchaseOrderNumber = shipment != null ? shipment.PurchaseOrderNumber : string.Empty;
				xc.ShipperReference = shipment != null ? shipment.ShipperReference : string.Empty;
				xc.TotalPackages = shipment != null ? shipment.Items.Sum(i => i.Quantity) : ticket.Items.Count;
				xc.TotalPieces = shipment != null ? shipment.Items.Sum(i => i.PieceCount) : ticket.Items.Count;
				xc.TotalStops = shipment != null ? shipment.Stops.Count+2 : inv.Stops.Count == 0 ? ExternUtilities.InvalidCount : inv.Stops.Count;
				xc.TotalWeight = shipment != null ? shipment.Items.Sum(i => i.ActualWeight) : 0;
				xc.Xml = content;
				xc.Status = XmlConnectStatus.Accepted;
				xc.StatusMessage = string.Empty;
				xc.FtpTransmission = method == NotificationMethod.Ftp;

			    var documentDate = inv.BeginningSegment.InvoiceDate.ToDateTime() <= DateUtility.SystemEarliestDateTime
			                        ? DateTime.Now
			                        : inv.BeginningSegment.InvoiceDate.ToDateTime();

                var bill = new VendorBill
					{
						Tenant = vcom.Tenant,
						User = vcom.Tenant.DefaultSystemUser,
						DateCreated = DateTime.Now,
						ApplyToDocument = null,
						BillType = inv.BeginningSegment.CorrectionIndicator == CorrectionIndicator.CR
							           ? InvoiceType.Credit
							           : InvoiceType.Invoice,
						DocumentDate = documentDate,
						ExportDate = DateUtility.SystemEarliestDateTime,
						Exported = false,
						PostDate = DateUtility.SystemEarliestDateTime,
						Posted = false,
						DocumentNumber = inv.BeginningSegment.InvoiceNumber,
						VendorLocation = remitTo,
					};
				bill.Details = inv.FreightInvoiceDetails
				                  .Select(d => new VendorBillDetail
					                  {
						                  ReferenceNumber = shipment != null ? shipment.ShipmentNumber : ticket.ServiceTicketNumber,
						                  ReferenceType = shipment != null ? DetailReferenceType.Shipment : DetailReferenceType.ServiceTicket,
						                  Tenant = vcom.Tenant,
						                  VendorBill = bill,
						                  AccountBucket = shipment != null
								                  ? shipment.AccountBuckets.First(a => a.Primary).AccountBucket
								                  : ticket.AccountBucket,
						                  ChargeCode = chargeCodes.FirstOrDefault(cc => cc.Code == d.ChargeCode),
						                  Note = d.Remarks.FreeFormMessage,
						                  Quantity = 1,
						                  UnitBuy = d.Charge,
					                  })
				                  .ToList();

				records.Add(new {Bill = bill, Connect = xc});
			}

			// save vendor bills
			var handler = new VendorBillXmlTransHandler();
			var savedBills = new List<VendorBill>();
			foreach (var bill in records.Select(r => r.Bill).ToList())
			{
				Exception e;
				if (!handler.SaveNewVendorBills(bill, out e, vcom.Tenant.DefaultSystemUser))
				{
					ProcessCarrierInvoiceRejectAck(vcom, content, edi210, InternalError, SyntaxErrorCode.NA, method);
					foreach (var b in savedBills)
					{
						handler.DeleteVendorBills(b, out e, vcom.Tenant.DefaultSystemUser);
						if (e != null) ErrorLogger.LogError(e, ErrorsFolder);
					}
					return;
				}
				if (e != null)
				{
					ErrorLogger.LogError(e, ErrorsFolder);
					ProcessCarrierInvoiceRejectAck(vcom, content, edi210, InternalError, SyntaxErrorCode.NA, method);
					foreach (var b in savedBills)
					{
						handler.DeleteVendorBills(b, out e, vcom.Tenant.DefaultSystemUser);
						if (e != null) ErrorLogger.LogError(e, ErrorsFolder);
					}
					return;
				}
				savedBills.Add(bill);
			}

			// save connect records
			Exception ex;
			new XmlTransmissionProcessHandler().SaveTransmission(records.Select(r => r.Connect).ToList(), vcom.Tenant.DefaultSystemUser, out ex);
			if (ex != null)
			{
				ErrorLogger.LogError(ex, ErrorsFolder);
				ProcessCarrierInvoiceRejectAck(vcom, content, edi210, InternalError, SyntaxErrorCode.NA, method);
			}
			else
			{
				// Acknowledge batch
				if (!vcom.AcknowledgePickup210) return;

				var xmlConnect = records[0].Connect;
				AcknowledgeEdiDocument(vcom, xmlConnect, SyntaxErrorCode.NA, method, AcknowledgementCode.A, edi210.TransactionSetHeader.TransactionSetControlNumber, TransSetIdentifierCode.CSI);
				for (var i = 1; i < records.Count; i++)
				{
					records[i].Connect.TakeSnapShot();
					records[i].Connect.TransmissionOkay = xmlConnect.TransmissionOkay;
				}

				// save all
				new XmlTransmissionProcessHandler().SaveTransmission(records.Select(r => r.Connect).ToList(), vcom.Tenant.DefaultSystemUser, out ex);
				if (ex != null) ErrorLogger.LogError(ex, ErrorsFolder);
			}

		}

		private void ProcessCarrierInvoiceRejectAck(VendorCommunication vcom, string content, Edi210 edi210, string statusMsg, SyntaxErrorCode syntaxErrCode, NotificationMethod method)
		{
			if (!vcom.AcknowledgePickup210) return; // acknowledge only if set to do so

			var xc = new XmlConnect();
			xc.SetEmpty();
			xc.TenantId = vcom.TenantId;
			xc.Direction = Direction.Inbound;
			xc.ControlNumber = edi210.TransactionSetHeader.TransactionSetControlNumber;
			xc.DocumentType = EdiDocumentType.EDI210;
			xc.Xml = content;
			xc.Status = XmlConnectStatus.Rejected;
			xc.StatusMessage = statusMsg;
			xc.DateCreated = DateTime.Now;
			xc.VendorNumber = vcom.Vendor.VendorNumber;
			xc.VendorScac = vcom.Vendor.Scac;

			Exception e;
			new XmlTransmissionProcessHandler().SaveTransmission(xc, vcom.Tenant.DefaultSystemUser, out e);
			if (e != null) ErrorLogger.LogError(e, ErrorsFolder);

			// send rejection acknowledgement
			AcknowledgeEdiDocument(vcom, xc, syntaxErrCode, method, AcknowledgementCode.R, edi210.TransactionSetHeader.TransactionSetControlNumber, TransSetIdentifierCode.CSI);
		}




		private void ProcessShipmentStatusUpdate(VendorCommunication vcom, string content, Edi214 edi214, NotificationMethod method)
		{
			var validator = new Edi214Validator();

			// validations
			if (!validator.SetControlNumbersMatch(edi214))
			{
				ProcessShipmentStatusRejectAck(vcom, content, edi214, ControlNumberMismatch, SyntaxErrorCode.E3, method);
				return;
			}

			if (!validator.NumberOfSegmentsMatch(edi214))
			{
				ProcessShipmentStatusRejectAck(vcom, content, edi214, InvalidNumberOfSegments, SyntaxErrorCode.E5, method);
				return;
			}

			if (!validator.HasValidData(edi214))
			{
				ProcessShipmentStatusRejectAck(vcom, content, edi214, InvalidDataInOneOrMoreSegments, SyntaxErrorCode.E5, method);
				return;
			}

			try
			{
				// check shipments
				var shipments = new Dictionary<string, Shipment>();
				var connectRecords = new List<XmlConnect>();
				var checkCallsToUpdate = new List<CheckCall>();
				var search = new ShipmentSearch();
				var allValid = true;
				var allValidErrCode = SyntaxErrorCode.E5;
				foreach (var s in edi214.Statuses)
				{
					var shipmentDocRtrvLogRequiredIfApplicable = false;

					// find shipment
					var number = s.BeginningSegment.ShipmentIdentificationNumber.GetString().StripNonDigits();
					var shipment = shipments.ContainsKey(number) ? shipments[number] : search.FetchShipmentByShipmentNumber(number, vcom.TenantId);
					var scac = s.BeginningSegment.StandardCarrierAlphaCode.GetString();
					var pro = s.BeginningSegment.ReferenceIdentification.GetString();

					if (shipment == null && !string.IsNullOrEmpty(pro) && !string.IsNullOrEmpty(scac))
					{
						shipment = search.FetchShipmentByPrimaryVendorProAndScac(pro, scac, vcom.TenantId);
						if (shipment != null && shipments.ContainsKey(shipment.ShipmentNumber))
							shipment = shipments[shipment.ShipmentNumber];
					}

					if (shipment == null)
					{
						allValid = false;
						allValidErrCode = SyntaxErrorCode.E6;
						break;
					}

                    //if key is present, we have already preloaded collections
                    if (!shipments.ContainsKey(shipment.ShipmentNumber)) shipment.LoadCollections();
					
                    // get primary shipment vendor and secondary vendor
					var psv = shipment.Vendors.FirstOrDefault(v => v.Vendor != null && v.Vendor.Scac == scac && v.Primary);
					var sv = shipment.Vendors.FirstOrDefault(v => v.Vendor != null && v.Vendor.Scac == scac && !v.Primary);

					if (psv == null && sv == null)
					{
						allValid = false;
						allValidErrCode = SyntaxErrorCode.E6;
						break;
					}
                    
					// primary vendor processing
					if (psv != null)
					{
                        //if key is present, we have already taken a snapshot of this shipment
                        if (!shipments.ContainsKey(shipment.ShipmentNumber)) shipment.TakeSnapShot();
						var msg = string.Format("{0}. [Terminal: {1} {2}, Trailer #: {3}, Reason Code: {4}]",
						                        s.ShipmentStatusCode.GetDescription(),
						                        s.Location != null && s.Location.GeographicLocation != null
							                        ? s.Location.GeographicLocation.CityName.GetString()
							                        : string.Empty,
						                        s.Location != null && s.Location.GeographicLocation != null
							                        ? s.Location.GeographicLocation.StateOrProvinceCode.GetString()
							                        : string.Empty,
						                        s.EquipmentDetail != null ? s.EquipmentDetail.EquipmentNumber.GetString() : string.Empty,
						                        s.ShipmentStatusReasonCode);
						var checkCall = new CheckCall
							{
								Shipment = shipment,
								CallDate = DateTime.Now,
								EventDate = s.DateTime.ToDateTime(),
								CallNotes = msg,
								TenantId = shipment.TenantId,
								User = shipment.Tenant.DefaultSystemUser,
								EdiStatusCode = s.ShipmentStatusCode.GetString()
							};
						checkCallsToUpdate.Add(checkCall);
						shipment.CheckCalls.Add(checkCall);
						switch (s.ShipmentStatusCode)
						{
							case ShipStatMsgCode.AF:
								shipment.ActualPickupDate = s.DateTime.ToDateTime();
								shipment.Status = shipment.InferShipmentStatus(false);
								break;
							case ShipStatMsgCode.D1:
								if (shipment.ActualDeliveryDate != s.DateTime.ToDateTime()) shipmentDocRtrvLogRequiredIfApplicable = true;
								shipment.ActualDeliveryDate = s.DateTime.ToDateTime();
								if (shipment.ActualPickupDate == DateUtility.SystemEarliestDateTime)
									shipment.ActualPickupDate = shipment.DesiredPickupDate; // set actual pickup to desired pickup if auto setting actual delivery and still missing actual pickup.
								shipment.Status = shipment.InferShipmentStatus(false);
								break;
							case ShipStatMsgCode.CA:
								shipment.Status = ShipmentStatus.Void;
								break;
							case ShipStatMsgCode.AG:
								shipment.EstimatedDeliveryDate = s.DateTime.ToDateTime();
								break;
							case ShipStatMsgCode.AA:
								shipment.Origin.TakeSnapShot();
								shipment.Origin.AppointmentDateTime = s.DateTime.ToDateTime();
								break;
							case ShipStatMsgCode.AB:
								shipment.Destination.TakeSnapShot();
								shipment.Destination.AppointmentDateTime = s.DateTime.ToDateTime();
								break;
							case ShipStatMsgCode.A3:
							case ShipStatMsgCode.A7:
							case ShipStatMsgCode.A9:
							case ShipStatMsgCode.AH:
							case ShipStatMsgCode.AI:
							case ShipStatMsgCode.AJ:
							case ShipStatMsgCode.AM:
							case ShipStatMsgCode.AN:
							case ShipStatMsgCode.AP:
							case ShipStatMsgCode.AR:
							case ShipStatMsgCode.AV:
							case ShipStatMsgCode.B6:
							case ShipStatMsgCode.BA:
							case ShipStatMsgCode.BC:
							case ShipStatMsgCode.C1:
							case ShipStatMsgCode.CD:
							case ShipStatMsgCode.CL:
							case ShipStatMsgCode.CP:
							case ShipStatMsgCode.I1:
							case ShipStatMsgCode.J1:
							case ShipStatMsgCode.K1:
							case ShipStatMsgCode.L1:
							case ShipStatMsgCode.OA:
							case ShipStatMsgCode.OO:
							case ShipStatMsgCode.P1:
							case ShipStatMsgCode.PR:
							case ShipStatMsgCode.R1:
							case ShipStatMsgCode.RL:
							case ShipStatMsgCode.S1:
							case ShipStatMsgCode.SD:
							case ShipStatMsgCode.X1:
							case ShipStatMsgCode.X2:
							case ShipStatMsgCode.X3:
							case ShipStatMsgCode.X4:
							case ShipStatMsgCode.X5:
							case ShipStatMsgCode.X6:
							case ShipStatMsgCode.X8:
							case ShipStatMsgCode.XB:
								break;
							default:
								allValid = false;
								break;
						}

						// pro's don't match
						if (!string.IsNullOrEmpty(pro) && !string.IsNullOrEmpty(psv.ProNumber) && psv.ProNumber.ToLower() != pro.ToLower())
						{
							allValid = false;
							allValidErrCode = SyntaxErrorCode.E5;
							break;
						}
						if (string.IsNullOrEmpty(psv.ProNumber))
						{
							psv.TakeSnapShot();
							if (psv.ProNumber != pro) shipmentDocRtrvLogRequiredIfApplicable = true;
							psv.ProNumber = pro;
						}

						if(shipmentDocRtrvLogRequiredIfApplicable) shipment.InsertShipmentDocRtrvLogFor(shipment.Tenant.DefaultSystemUser); // insert doc rtrv log
					}

					// secondary vendor processing
					if (sv != null)
					{
						// pro's don't match
						if (!string.IsNullOrEmpty(pro) && !string.IsNullOrEmpty(sv.ProNumber) && sv.ProNumber.ToLower() != pro.ToLower())
						{
							allValid = false;
							allValidErrCode = SyntaxErrorCode.E5;
							break;
						}
						if (string.IsNullOrEmpty(sv.ProNumber))
						{
							sv.TakeSnapShot();
							sv.ProNumber = pro;
						}
					}
					if (shipments.ContainsKey(shipment.ShipmentNumber)) shipments[shipment.ShipmentNumber] = shipment;
					else shipments.Add(shipment.ShipmentNumber, shipment);

					// xml connect record for shipment status
					var xc = new XmlConnect();
					xc.SetEmpty();
					xc.TenantId = vcom.TenantId;
					xc.ShipmentIdNumber = s.BeginningSegment.ShipmentIdentificationNumber.GetString();
					xc.Direction = Direction.Inbound;
					xc.ControlNumber = edi214.TransactionSetHeader.TransactionSetControlNumber.GetString();
					xc.ExpirationDate = DateUtility.SystemEarliestDateTime;
					xc.EquipmentDescriptionCode = s.EquipmentDetail == null ? string.Empty : s.EquipmentDetail.EquipmentDescriptionCode.GetString();
					xc.VendorNumber = vcom.Vendor.VendorNumber;
				    xc.VendorPro = psv != null && !string.IsNullOrEmpty(psv.ProNumber)
				                       ? psv.ProNumber
				                       : sv != null && !string.IsNullOrEmpty(sv.ProNumber) 
                                            ? sv.ProNumber 
                                            : string.Empty;
					xc.VendorScac = vcom.Vendor.Scac;
					xc.DateCreated = DateTime.Now;
					xc.ReceiptDate = DateTime.Now;

					if (s.ShipmentStatusCode == ShipStatMsgCode.D1 && s.Location != null)
					{
						var dest = s.Location;
						xc.DestinationCity = dest.GeographicLocation == null ? string.Empty : dest.GeographicLocation.CityName.GetString();
						xc.DestinationCountryCode = dest.GeographicLocation == null ? string.Empty : dest.GeographicLocation.CountryCode.GetString();
						xc.DestinationCountryName = string.Empty;
						xc.DestinationPostalCode = dest.GeographicLocation == null ? string.Empty : dest.GeographicLocation.PostalCode.GetString();
						xc.DestinationState = dest.GeographicLocation == null ? string.Empty : dest.GeographicLocation.StateOrProvinceCode.GetString();
						xc.DestinationStreet1 = dest.LocationStreetInformation == null ? string.Empty : dest.LocationStreetInformation.AddressInfo1.GetString();
						xc.DestinationStreet2 = dest.LocationStreetInformation == null ? string.Empty : dest.LocationStreetInformation.AddressInfo2.GetString();
					}
					xc.DocumentType = EdiDocumentType.EDI214;

					if (s.ShipmentStatusCode == ShipStatMsgCode.AF && s.Location != null)
					{
						var origin = s.Location;
						xc.OriginCity = origin.GeographicLocation == null ? string.Empty : origin.GeographicLocation.CityName.GetString();
						xc.OriginCountryCode = origin.GeographicLocation == null ? string.Empty : origin.GeographicLocation.CountryCode.GetString();
						xc.OriginCountryName = string.Empty;
						xc.OriginPostalCode = origin.GeographicLocation == null ? string.Empty : origin.GeographicLocation.PostalCode.GetString();
						xc.OriginState = origin.GeographicLocation == null ? string.Empty : origin.GeographicLocation.StateOrProvinceCode.GetString();
						xc.OriginStreet1 = origin.LocationStreetInformation == null ? string.Empty : origin.LocationStreetInformation.AddressInfo1.GetString();
						xc.OriginStreet2 = origin.LocationStreetInformation == null ? string.Empty : origin.LocationStreetInformation.AddressInfo2.GetString();
					}

					xc.TotalPackages = s.ShipmentWghtPkgAndQtyData.LadingPackages;
					xc.TotalPieces = s.ShipmentWghtPkgAndQtyData.LadingPieces;
					xc.TotalStops = shipment.Stops.Count + 2;
					xc.TotalWeight = s.ShipmentWghtPkgAndQtyData.Weight.ToDecimal();
					xc.VendorScac = s.BeginningSegment.StandardCarrierAlphaCode.GetString();
					xc.Xml = content;
					xc.Status = XmlConnectStatus.Accepted;
					xc.StatusMessage = string.Empty;
					xc.FtpTransmission = method == NotificationMethod.Ftp;
					connectRecords.Add(xc);
				}

				if (!allValid)
				{
					ProcessShipmentStatusRejectAck(vcom, content, edi214, InvalidDataInOneOrMoreSegments, allValidErrCode, method);
					return;
				}

				// save updated shipments
				var cnt = 0;
				// should equipment number of shipment indicating all shipments successfully saved.  If not zero, then some shipments saved
				foreach (var shipment in shipments.Values)
				{
					Exception exCnt;
					var errs = new ShipmentUpdateHandler().UpdateShipmentTrackingInfo(shipment, out exCnt);
					if (exCnt != null) ErrorLogger.LogError(exCnt, ErrorsFolder);
					else cnt++;

					if (exCnt == null && errs.Any())
					{
						var handler = new ProcQueObjProcessHandler();
						var pros = shipment.Vendors
						                   .Where(v => v.HasChanges())
										   .Select(v => new ShipmentStatusUpdateQuePro(v.Vendor.Scac, v.ProNumber))
						                   .ToList();
						foreach (var cc in shipment.CheckCalls.Where(c => c.IsNew))
						{
							var obj = new ProcQueObj
								{
									ObjType = ProcQueObjType.ShipmentStatusQue,
									Xml = new ShipmentStatusUpdateQue(cc, pros).ToXml(),
									DateCreated = DateTime.Now,
								};
							handler.Save(obj, out exCnt);
							if (exCnt != null) ErrorLogger.LogError(exCnt, ErrorsFolder);
						}
						checkCallsToUpdate.RemoveAll(c => c.ShipmentId == shipment.Id); // remove check calls for failed shipment save!
					}
				}
				if (cnt == 0)
				{
					ProcessShipmentStatusRejectAck(vcom, content, edi214, InternalError, SyntaxErrorCode.NA, method);
					return;
				}

				// save xml connect records
				Exception ex;
				new XmlTransmissionProcessHandler().SaveTransmission(connectRecords, vcom.Tenant.DefaultSystemUser, out ex);
				if (ex != null)
				{
					ErrorLogger.LogError(ex, ErrorsFolder);
					ProcessShipmentStatusRejectAck(vcom, content, edi214, InternalError, SyntaxErrorCode.NA, method);
				}
				else
				{
					// Acknowledge batch
					if (!vcom.AcknowledgePickup214) return;

					var xmlConnect = connectRecords[0];
					var ackCode = cnt != shipments.Values.Count ? AcknowledgementCode.E : AcknowledgementCode.A;
					AcknowledgeEdiDocument(vcom, xmlConnect, SyntaxErrorCode.NA, method, ackCode,
					                       edi214.TransactionSetHeader.TransactionSetControlNumber, TransSetIdentifierCode.SSM);
					for (var i = 1; i < connectRecords.Count; i++)
					{
						connectRecords[i].TakeSnapShot();
						connectRecords[i].TransmissionOkay = xmlConnect.TransmissionOkay;
					}

					// save all
					new XmlTransmissionProcessHandler().SaveTransmission(connectRecords, vcom.Tenant.DefaultSystemUser, out ex);
					if (ex != null) ErrorLogger.LogError(ex, ErrorsFolder);
				}

				// send auto update for customers (EDI and FTP only)
				ProcessAutoClientUpdate(checkCallsToUpdate);
			}
			catch (Exception)
			{
				ProcessShipmentStatusRejectAck(vcom, content, edi214, "Invalid data in one or more segments - Possible missing required segment", SyntaxErrorCode.E5, method);
			}
		}

		private void ProcessAutoClientUpdate(IEnumerable<CheckCall> checkCallsToUpdate)
		{
			var milestones = new List<Milestone>
				{
					Milestone.ShipmentCheckCallUpdate,
					Milestone.ShipmentInTransit,
					Milestone.ShipmentDelivered
				};

			var items = checkCallsToUpdate
				.Where( c =>c.Shipment.Customer.Communication != null && (c.Shipment.Customer.Communication.EdiEnabled || c.Shipment.Customer.Communication.FtpEnabled))
				.Where(c => c.Shipment.Customer.Communication.Notifications.Any(n => milestones.Contains(n.Milestone) && n.Enabled))
				.GroupBy(c => c.Shipment.ShipmentNumber)
				.Select(g => new {g.Key, Values = g})
				.ToList();

			foreach (var item in items)
			{
				var shipment = item.Values.First().Shipment;
				_server.NotifyCheckCallUpdateEdiFtpOnly(shipment, item.Values.Select(n=>n).ToList(), shipment.Tenant.DefaultSystemUser);
			}
		}

		private void ProcessShipmentStatusRejectAck(VendorCommunication vcom, string content, Edi214 edi214, string statusMsg, SyntaxErrorCode syntaxErrCode, NotificationMethod method)
		{
			// acknowledge only if set to do so
			if (!vcom.AcknowledgePickup214) return;

		    var handler = new XmlTransmissionProcessHandler();
		    var connectRecords = new List<XmlConnect>();

            foreach (var status in edi214.Statuses)
            {
                var xc = new XmlConnect();
                xc.SetEmpty();
                xc.TenantId = vcom.TenantId;
                xc.Direction = Direction.Inbound;
                xc.ControlNumber = edi214.TransactionSetHeader.TransactionSetControlNumber;
                xc.DocumentType = EdiDocumentType.EDI214;
                xc.Xml = content;
                xc.Status = XmlConnectStatus.Rejected;
                xc.StatusMessage = statusMsg;
                xc.DateCreated = DateTime.Now;
                xc.VendorNumber = vcom.Vendor.VendorNumber;
                xc.VendorScac = vcom.Vendor.Scac;
                xc.VendorPro = status.BeginningSegment.ReferenceIdentificationQualifier ==
                                         RefIdQualifier.CN
                                             ? status.BeginningSegment.ReferenceIdentification.GetString()
                                             : string.Empty;
                xc.ShipperReference = status.BeginningSegment.ReferenceIdentificationQualifier ==
                                         RefIdQualifier.SHR
                                             ? status.BeginningSegment.ReferenceIdentification.GetString()
                                             : string.Empty;
                xc.PurchaseOrderNumber = status.BeginningSegment.ReferenceIdentificationQualifier ==
                                         RefIdQualifier.PO
                                             ? status.BeginningSegment.ReferenceIdentification.GetString()
                                             : string.Empty;
                xc.ShipmentIdNumber = !string.IsNullOrEmpty(status.BeginningSegment.ShipmentIdentificationNumber)
                                          ? status.BeginningSegment.ShipmentIdentificationNumber.GetString()
                                          : string.Empty;
                Exception e;
                handler.SaveTransmission(xc, vcom.Tenant.DefaultSystemUser, out e);
                if (e != null) ErrorLogger.LogError(e, ErrorsFolder);
                else connectRecords.Add(xc);
            }
			

			// send rejection acknowledgement
			AcknowledgeEdiDocument(vcom, connectRecords.First(), syntaxErrCode, method, AcknowledgementCode.R, edi214.TransactionSetHeader.TransactionSetControlNumber, TransSetIdentifierCode.SSM);

		    if (connectRecords.Count() <= 1) return;
		    
            for(var i =1; i < connectRecords.Count(); i++){
		        connectRecords[i].TakeSnapShot();
		        connectRecords[i].TransmissionOkay = connectRecords.First().TransmissionOkay;

		        Exception e;
		        handler.SaveTransmission(connectRecords[i], vcom.Tenant.DefaultSystemUser, out e);
		        if (e != null) ErrorLogger.LogError(e, ErrorsFolder);
		    }
		}



		private void AcknowledgeEdiDocument(Communication com, XmlConnect connect, SyntaxErrorCode syntaxErrCode, NotificationMethod method, AcknowledgementCode ackCode, string transSetCtrlNum, TransSetIdentifierCode transSetIdCode)
		{
			// send rejection acknowledgement
			var controlNumber = new AutoNumberProcessor()
				.NextNumber(AutoNumberCode.EdiTransactionSetControlNumber, com.Tenant.DefaultSystemUser)
				.GetString();
			var ack = new Edi997
				{
					TransactionSetHeader = new TransactionSetHeader
						{
							TransactionSetControlNumber = controlNumber,
							TransactionSetIdentifierCode = TransSetIdentifierCode.ACK
						},
					FunctionalIdentifierCode = FuncIdentifierCode.IM,
					SetAcknowledgementCode = ackCode,
					SetControlNumber = transSetCtrlNum,
					SetIdentifierCode = transSetIdCode,
					SetSyntaxErrorCode = syntaxErrCode,
					TransactionSetTrailer = new TransactionSetTrailer
						{
							NumberOfIncludedSegments = 1,
							TransactionSetControlNumber = controlNumber
						}
				};

			connect.TakeSnapShot();
			var ackContent = _server.GenerateEdiMessage(com, ack.ToXml(), EdiDocumentType.EDI997.GetDocTypeString(), transSetCtrlNum).ToUtf8Bytes();
			switch (method)
			{
				case NotificationMethod.Edi:
					connect.TransmissionOkay = com.SendEdi(string.Format("{0}.{1}", controlNumber, ReportExportExtension.xml), ackContent, EdiDocumentType.EDI997);
					break;
				case NotificationMethod.Ftp:
					connect.TransmissionOkay = com.SendFtp(string.Format("{0}.{1}", controlNumber, ReportExportExtension.xml), ackContent, EdiDocumentType.EDI997);
					break;
			}

			// save for transmission okay 
			Exception e;
			new XmlTransmissionProcessHandler().SaveTransmission(connect, com.Tenant.DefaultSystemUser, out e);
			if (e != null) ErrorLogger.LogError(e, ErrorsFolder);
		}




		private void ProcessAcknowledgement(Edi997 edi997, Communication com)
		{
			var search = new XmlConnectSearch();
			List<XmlConnect> records;
			switch (edi997.SetIdentifierCode)
			{
				case TransSetIdentifierCode.CSI:
					records = search.FetchOutBoundXmlConnectBySetCtrlNumAndDocType(edi997.SetControlNumber, EdiDocumentType.EDI210, com.TenantId, com.Tenant.DefaultSystemUserId);
					goto ProcessConnectAck;
				case TransSetIdentifierCode.LTR:
					records = search.FetchOutBoundXmlConnectBySetCtrlNumAndDocType(edi997.SetControlNumber, EdiDocumentType.EDI204, com.TenantId, com.Tenant.DefaultSystemUserId);
					goto ProcessConnectAck;
				case TransSetIdentifierCode.RLT:
					records = search.FetchOutBoundXmlConnectBySetCtrlNumAndDocType(edi997.SetControlNumber, EdiDocumentType.EDI990, com.TenantId, com.Tenant.DefaultSystemUserId);

			ProcessConnectAck:
					foreach (var record in records)
					{
						record.TakeSnapShot();
						if (record.Direction == Direction.Outbound) record.ReceiptDate = DateTime.Now;
						switch (edi997.SetAcknowledgementCode)
						{
							case AcknowledgementCode.A:
								record.Status = XmlConnectStatus.Accepted;
								record.StatusMessage = string.Empty;

								break;
							case AcknowledgementCode.E:
								record.Status = XmlConnectStatus.Accepted;
								record.StatusMessage = edi997.SetAcknowledgementCode.GetDescription();
								break;
							default:
								record.Status = XmlConnectStatus.Rejected;
								record.StatusMessage = edi997.SetAcknowledgementCode.GetDescription();
								break;
						}
						Exception e;
						new XmlTransmissionProcessHandler().SaveTransmission(record, com.Tenant.DefaultSystemUser, out e);
						if (e != null) ErrorLogger.LogError(e, ErrorsFolder);
					}
					break;
				default:
					ErrorLogger.LogError(new UnMatchedAcknowledgementException(edi997.ToXml().StripTags()), ErrorsFolder);
					break;
			}
		}
	}
}
