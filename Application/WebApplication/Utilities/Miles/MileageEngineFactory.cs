﻿using System;
using System.Web;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.PluginBase.Mileage;

namespace LogisticsPlus.Eship.WebApplication.Utilities.Miles
{
	public static class MileageEngineFactory
	{
		public static IMileagePlugin GetPlugin(this MileageEngine engine)
		{
			var typeName = WebApplicationSettings.GetTypeName(engine);
			var type = Type.GetType(typeName, false);
			try
			{
				return type == null ? null : (IMileagePlugin) Activator.CreateInstance(type);
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, HttpContext.Current);
				return null;
			}
		}
	}
}
