﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Core;
using LogisticsPlus.Eship.Processor.Searches.Core;

namespace LogisticsPlus.Eship.WebApplication.Utilities
{
    public class EmailProcessor
    {
        private bool _shutdownThread;
        private Thread _managerThread;


        public bool IsRunning
        {
            get { return _managerThread != null; }
        }

        public string ErrorsFolder { get; set; }

        public EmailProcessor(string errorsFolder)
        {
            ErrorsFolder = errorsFolder;
        }

        ~EmailProcessor()
        {
            Shutdown();
        }

        public void Start()
        {
            try
            {
                if (_managerThread != null) return;

                // start the scheduling thread
                _managerThread = new Thread(ManagerThread);
                _managerThread.Start();
            }
            catch (Exception e)
            {
                ErrorLogger.LogError(e, ErrorsFolder);
            }
        }

        public void Shutdown()
        {
            if (_managerThread == null) return;

            _shutdownThread = true;

            // wait for the scheduler thread to shutdown for up to 60 sec
            // if the thread does not shutdown on time, try to kill it
            if (!_managerThread.Join(60000))
            {
                try
                {
                    _managerThread.Abort();
                }
                catch (Exception e)
                {
                    ErrorLogger.LogError(e, ErrorsFolder);
                }
            }

            _managerThread = null;
        }

        private void ManagerThread()
        {
            // delay before starting the thread
            Thread.Sleep(250);

            // init some state variables
            var counter = 4;

            if (_shutdownThread) return;

            do
            {
                // each actual thread iteration occurs in 1 minute intervals
                // however to make the tread more responsive it will sleep 4 x 250ms
                if (counter != 0)
                {
                    Thread.Sleep(250);
                    counter--;
                    continue;
                }

                // reset the counter
                counter = 4;

                try
                {
                    ProcessEmails();
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Hrms:emailProcessor : " + ex.Message);
                    ErrorLogger.LogError(ex, ErrorsFolder);
                }
            } while (!_shutdownThread);
        }

        private void ProcessEmails()
        {
            var emails = new EmailLogSearch().FetchUnsentEmails();

            foreach (var emailLog in emails)
            {
                var msg = new MailMessage();

                foreach (var addr in emailLog.Tos.Split(WebApplicationUtilities.ImportSeperators(), StringSplitOptions.RemoveEmptyEntries).Distinct())
                    try
                    {
	                    var email = addr.Trim();

	                    if (!email.EmailIsValid())
                        {
	                        var message = string.Format("Incorrect email format [{0}]. Email Log Ref: {1}, Subject: {2}",
	                                                    email, emailLog.Id, emailLog.Subject);
	                        ErrorLogger.LogError(new Exception(message), WebApplicationSettings.ErrorsFolder);
							continue;
                        }

	                    msg.To.Add(email);
                    }
                    catch (Exception e)
                    {
                        ErrorLogger.LogError(e, ErrorsFolder);
                    }

                foreach (var addr in emailLog.Ccs.Split(WebApplicationUtilities.ImportSeperators(), StringSplitOptions.RemoveEmptyEntries).Distinct())
                    try
                    {
	                    var email = addr.Trim();

	                    if (!email.EmailIsValid())
                        {
							var message = string.Format("Incorrect email format [{0}]. Email Log Ref: {1}, Subject: {2}",
														email, emailLog.Id, emailLog.Subject);
							ErrorLogger.LogError(new Exception(message), WebApplicationSettings.ErrorsFolder);
							continue;
                        }

                        msg.CC.Add(email);
                    }
                    catch (Exception e)
                    {
                        ErrorLogger.LogError(e, ErrorsFolder);
                    }

                foreach (var addr in emailLog.Bbcs.Split(WebApplicationUtilities.ImportSeperators(), StringSplitOptions.RemoveEmptyEntries).Distinct())
                    try
                    {
	                    var email = addr.Trim();

	                    if (!email.EmailIsValid())
                        {
							var message = string.Format("Incorrect email format [{0}]. Email Log Ref: {1}, Subject: {2}",
														email, emailLog.Id, emailLog.Subject);
							ErrorLogger.LogError(new Exception(message), WebApplicationSettings.ErrorsFolder);
							continue;
                        }

                        msg.Bcc.Add(email);
                    }
                    catch (Exception e)
                    {
                        ErrorLogger.LogError(e, ErrorsFolder);
                    }

	            var sendOkay = false;
                try
                {
                    msg.From = new MailAddress(emailLog.From);
                    msg.Subject = emailLog.Subject;
                    msg.Body = emailLog.Body;
                    msg.IsBodyHtml = true;

                    var attachments = emailLog.EmailLogAttachments.Select(a => new Attachment(new MemoryStream(a.AttachmentBytes), a.Name));

                    foreach (var attachment in attachments)
                        msg.Attachments.Add(attachment);

                    var client = new SmtpClient(WebApplicationSettings.SmtpNetworkHost) { DeliveryMethod = SmtpDeliveryMethod.Network };
                    if (!string.IsNullOrEmpty(WebApplicationSettings.SmtpPort)) client.Port = WebApplicationSettings.SmtpPort.ToInt();
                    client.Send(msg);
	                sendOkay = true;
                }
                catch (Exception e)
                {
                    ErrorLogger.LogError(e, ErrorsFolder);
                }

	            if (!sendOkay) continue;

	            // Update email log as sent, save email log
	            emailLog.TakeSnapShot();
	            emailLog.Sent = true;

	            Exception ex;
	            new EmailProcessorHandler().UpdateEmailLog(emailLog, emailLog.Tenant.DefaultSystemUser, out ex);
	            if (ex != null) ErrorLogger.LogError(ex, ErrorsFolder);
            }
        }
    }
}