﻿namespace LogisticsPlus.Eship.WebApplication.Utilities
{
	public static class SystemTemplates
	{
		/* PLEASE KEEP A - Z */

	    public static string AvailableLoadsTemplate
	    {
            get { return string.Format("{0}AvailableLoadsTemplate.html", WebApplicationSettings.BaseFilePath); }
	    }

	    public static string BookingRequestTemplate
	    {
            get { return string.Format("{0}BookingRequestTemplate.html", WebApplicationSettings.BaseFilePath); }
	    }

		public static string CancelledShipmentEmailTemplate
		{
			get { return string.Format("{0}ShipmentVoided.html", WebApplicationSettings.BaseFilePath); }
		}

		public static string CancelledShipmentReversedEmailTemplate
		{
			get { return string.Format("{0}ShipmentVoidReversed.html", WebApplicationSettings.BaseFilePath); }
        }
        
        public static string CheckCallNotificationTemplate
        {
            get { return string.Format("{0}CheckCallNotification.html", WebApplicationSettings.BaseFilePath); }
        }

	    public static string ClaimNoteReminderNotification
	    {
            get { return string.Format("{0}ClaimNoteReminderNotification.html", WebApplicationSettings.BaseFilePath); }
	        
	    }

        public static string CustomerSupportEmailTemplate
	    {
            get { return string.Format("{0}CustomerSupportRequestTemplate.html", WebApplicationSettings.BaseFilePath); }
	    }

		public static string DeletedBookingRequestEmailTemplate
		{
			get { return string.Format("{0}BookingRequestDeleted.html", WebApplicationSettings.BaseFilePath); }
		}

        public static string DeveloperAccessRequestDevelopmentApprovalTemplate
        {
            get { return string.Format("{0}DevelopmentAccessApprovalNotification.html", WebApplicationSettings.BaseFilePath); }
        }

        public static string DeveloperAccessRequestProductionTemplate
        {
            get { return string.Format("{0}ProductionAccessRequestNotification.html", WebApplicationSettings.BaseFilePath); }
        }

        public static string DeveloperAccessRequestProductionApprovalTemplate
        {
            get { return string.Format("{0}ProductionAccessApprovalNotification.html", WebApplicationSettings.BaseFilePath); }
        }

        public static string DeveloperAccessRequestSupportTemplate
        {
            get { return string.Format("{0}DeveloperAccessSupportRequest.html", WebApplicationSettings.BaseFilePath); }
        }

		public static string DeveloperAccessRequestTemplate
		{
			get { return string.Format("{0}DeveloperAccessRequestNotification.html", WebApplicationSettings.BaseFilePath); }
		}

		public static string DeveloperAccessRequestUpdateTemplate
		{
			get { return string.Format("{0}DeveloperAccessRequestUpdate.html", WebApplicationSettings.BaseFilePath); }
		}

        public static string Edi204DetailTemplate
        {
            get { return string.Format("{0}EDI204Detail.html", WebApplicationSettings.BaseFilePath); }
        }

		public static string Edi990DetailTemplate
		{
			get { return string.Format("{0}EDI990Detail.html", WebApplicationSettings.BaseFilePath); }
		}

		public static string Edi204EmailTemplate
		{
			get { return string.Format("{0}EDI204Email.html", WebApplicationSettings.BaseFilePath); }
		}

		public static string EmailTenderResponseTemplate
		{
			get { return string.Format("{0}EmailTenderResponse.html", WebApplicationSettings.BaseFilePath); }
		}

		public static string GeneralEmailTemplate
		{
			get { return string.Format("{0}EmailTemplate.html", WebApplicationSettings.BaseFilePath); }
		}

	    public static string GenerateInvoicePostedTemplate
	    {
	        get { return string.Format("{0}InvoicePostedTemplate.html", WebApplicationSettings.BaseFilePath); }
	    }

		public static string GetAccountTemplatePath
		{
			get { return string.Format("{0}GetAccountTemplate.html", WebApplicationSettings.BaseFilePath); }
		}

		public static string LoadOrderEmailTemplate
		{
			get { return string.Format("{0}LoadOrderNotification.html", WebApplicationSettings.BaseFilePath); }
		}

	    public static string NewPendingVendorNotificationTemplate
	    {
            get { return string.Format("{0}NewPendingVendorNotification.html", WebApplicationSettings.BaseFilePath); }
	    }

		public static string NewShipmentEmailTemplate
		{
			get { return string.Format("{0}NewShipmentNotification.html", WebApplicationSettings.BaseFilePath); }
		}

	    public static string RateAndSchedulePreauthorization
	    {
            get { return string.Format("{0}RateAndSchedulePreauthorization.html", WebApplicationSettings.BaseFilePath); }	        
	    }

		public static string RateDownloadTemplate
		{
			get { return string.Format("{0}RateDownload.html", WebApplicationSettings.BaseFilePath); }
		}

		public static string ReportReadyNotificationTemplate
		{
			get { return string.Format("{0}ReportReadyNotification.html", WebApplicationSettings.BaseFilePath); }
		}

		public static string ShipmentDeliveredEmailTemplate
		{
			get { return string.Format("{0}ShipmentDeliveredNotification.html", WebApplicationSettings.BaseFilePath); }
		}

	    public static string ShipmentDetailsTemplate
	    {
            get { return string.Format("{0}ShipmentDetails.html", WebApplicationSettings.BaseFilePath); }	        
	    }

		public static string ShipmentInTransitEmailTemplate
		{
			get { return string.Format("{0}ShipmentInTransitNotification.html", WebApplicationSettings.BaseFilePath); }
		}

	    public static string ShipmentRouteSummaryTemplate
	    {
            get { return string.Format("{0}ShipmentRouteSummary.html", WebApplicationSettings.BaseFilePath); }
	    }

		public static string TenantLoginTemplate
		{
			get { return string.Format("{0}Index.html", WebApplicationSettings.BaseFilePath); }
		}

		public static string VendorChargeDisputeTemplate
		{
			get { return string.Format("{0}VendorChargeDisputeTemplate.html", WebApplicationSettings.BaseFilePath); }
		}

		public static string VendorChargeConfirmationTemplate
		{
			get { return string.Format("{0}VendorChargeConfirmationTemplate.html", WebApplicationSettings.BaseFilePath); }
		}

		public static string VendorDisputeResolvedTemplate
		{
			get { return string.Format("{0}VendorDisputeResolvedTemplate.html", WebApplicationSettings.BaseFilePath); }
		}


		public static string XmlTransmissionErrorTemplate
		{
			get { return string.Format("{0}XmlTransmissionError.html", WebApplicationSettings.BaseFilePath); }
		}

        public static string ProLabel4x6Template
        {
            get { return string.Format("{0}ProLabel4x6.html", WebApplicationSettings.BaseFilePath); }
        }

        public static string ProLabelAvery3x4Template
        {
            get { return string.Format("{0}ProLabelAvery3x4.html", WebApplicationSettings.BaseFilePath); }
        }

    }
}
