﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Extern.Ws;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Rates;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Project44;
using LogisticsPlus.Eship.WebApplication.Services;

namespace LogisticsPlus.Eship.WebApplication.Utilities
{
	public static class WSUtilitiesV4
	{
		private const string ContractCarrier = "[Contract]";

		public static User IsValidUser(this AuthenticationToken token, List<WSMessage> messages)
		{
			Customer customer;
			return IsValidUser(token, messages, out customer);
		}

		public static User IsValidUser(this AuthenticationToken token, List<WSMessage> messages, out Customer customer)
		{
			customer = null; // default set

			if (token == null)
			{
				messages.Add(WSMessage.ErrorMessage("Missing Credentials."));
				return null;
			}

            var clientIp = HttpContext.Current == null ? string.Empty : HttpContext.Current.Request.UserHostAddress;

            EnvironmentUtilities.PageAccessLogger.Info("Authentication\tAccess Key:{0}\tAccess Code:{1}\tUser:{2}\t IP:{3}", token.AccessKey, token.AccessCode, token.Username, clientIp);

			var user = new UserSearch().FetchUserByUsernameAndAccessCode(token.Username, token.AccessCode);
			if (user == null || user.Password != token.Password)
			{
				var msg = new WSMessage {Type = WSEnums.MessageType.Error, Value = "Invalid credentials! Check username, password and/or access code."};
				messages.Add(msg);
                EnvironmentUtilities.PageAccessLogger.Error("Authentication\tAccess Key:{0}\tAccess Code:{1}\tUser:{2}\tMessage:{3}\t IP:{4}", token.AccessKey, token.AccessCode, token.Username, msg.Value, clientIp);
				return null;
			}

			if (!user.Enabled || !user.Tenant.Active)
			{
				var msg = new WSMessage {Type = WSEnums.MessageType.Error, Value = "User account/Setup is disabled."};
				messages.Add(msg);
                EnvironmentUtilities.PageAccessLogger.Error("Authentication\tAccess Key:{0}\tAccess Code:{1}\tUser:{2}\tMessage:{3}\t IP:{4}", token.AccessKey, token.AccessCode, token.Username, msg.Value, clientIp);
			}

			if (user.FailedLoginAttempts >= ProcessorVars.MaxFailedLoginAttempts)
			{
				var msg = new WSMessage {Type = WSEnums.MessageType.Error, Value = "Suspected hacking activity. Max login attempts exceeded."};
				messages.Add(msg);
                EnvironmentUtilities.PageAccessLogger.Error("Authentication\tAccess Key:{0}\tAccess Code:{1}\tUser:{2}\tMessage:{3}\t IP:{4}", token.AccessKey, token.AccessCode, token.Username, msg.Value, clientIp);
			}

			customer = new CustomerSearch().FetchCustomerByCommunicationConnectGuid(token.AccessKey, user.TenantId);

			if (customer == null)
			{
				var msg = new WSMessage {Type = WSEnums.MessageType.Error, Value = "Access key account not found!"};
				messages.Add(msg);
                EnvironmentUtilities.PageAccessLogger.Error("Authentication\tAccess Key:{0}\tAccess Code:{1}\tUser:{2}\tMessage:{3}\t IP:{4}", token.AccessKey, token.AccessCode, token.Username, msg.Value, clientIp);
				return null;
			}

			if (!customer.Active || !customer.Tier.Active)
			{
				var msg = new WSMessage {Type = WSEnums.MessageType.Error, Value = "Access key account is disabled."};
				messages.Add(msg);
                EnvironmentUtilities.PageAccessLogger.Error("Authentication\tAccess Key:{0}\tAccess Code:{1}\tUser:{2}\tMessage:{3}\t IP:{4}", token.AccessKey, token.AccessCode, token.Username, msg.Value, clientIp);
			}

			if (customer.Communication == null || !customer.Communication.EnableWebServiceAPIAccess)
			{
				var msg = new WSMessage {Type = WSEnums.MessageType.Error, Value = "Access key account not web service API enabled."};
				messages.Add(msg);
                EnvironmentUtilities.PageAccessLogger.Error("Authentication\tAccess Key:{0}\tAccess Code:{1}\tUser:{2}\tMessage:{3}\t IP:{4}", token.AccessKey, token.AccessCode, token.Username, msg.Value, clientIp);
			}

			// ensure user has access to account.
			if(!user.TenantEmployee)
			{
				var customerId = customer.Id;
				if (user.DefaultCustomer.Id != customerId && user.UserShipAs.Count(usa => usa.CustomerId == customerId) == 0)
				{
					var msg = new WSMessage {Type = WSEnums.MessageType.Error, Value = "User is not authorized to access this account."};
					messages.Add(msg);
                    EnvironmentUtilities.PageAccessLogger.Error("Authentication\tAccess Key:{0}\tAccess Code:{1}\tUser:{2}\tMessage:{3}\t IP:{4}", token.AccessKey, token.AccessCode, token.Username, msg.Value, clientIp);
				}
			}

			return user;
		}

		public static User IsValidUser(this WSAuthentication auth, List<WSMessage> messages)
		{
			Customer customer;
			var token = new AuthenticationToken
				{
					AccessCode = auth.AccessCode,
					AccessKey = auth.AccessKey,
					Username = auth.UserName,
					Password = auth.Password
				};
			return IsValidUser(token, messages, out customer);
		}

		public static User IsValidUser(this WSAuthentication auth, List<WSMessage> messages, out Customer customer)
		{
			var token = new AuthenticationToken
			{
				AccessCode = auth.AccessCode,
				AccessKey = auth.AccessKey,
				Username = auth.UserName,
				Password = auth.Password
			};
			return IsValidUser(token, messages, out customer);
		}




		public static string ObscureRESTPassword(string streamData)
		{
			// remove password from logged data:
			var str = streamData.ToLower();
			const string pwdStr = "\"password\":\"";
			var pwdStrStartIndex = str.IndexOf(pwdStr, StringComparison.OrdinalIgnoreCase);
			var replaceStrStartPoint = pwdStrStartIndex + pwdStr.Length;
			var pwdEndIndex = str.IndexOf("\"", replaceStrStartPoint, StringComparison.Ordinal);
			var replaceData = string.Empty;
			for (var i = 0; i < pwdEndIndex - replaceStrStartPoint; i++) replaceData += "*";
			var logData = str.Replace(str.Substring(replaceStrStartPoint, pwdEndIndex - replaceStrStartPoint), replaceData);
			return logData;
		}



		public static WSMessage ToWSMessage(this ValidationMessage message)
		{
			return new WSMessage
			       	{
			       		Value = message.Message,
			       		Type = message.Type == ValidateMessageType.Error
			       		       	? WSEnums.MessageType.Error
			       		       	: message.Type == ValidateMessageType.Warning
			       		       	  	? WSEnums.MessageType.Warning
			       		       	  	: message.Type == ValidateMessageType.Information
			       		       	  	  	? WSEnums.MessageType.Information
			       		       	  	  	: WSEnums.MessageType.Null
			       	};
		}

		public static IEnumerable<WSMessage> ToWSMessages(this IEnumerable<ValidationMessage> messages)
		{
			return messages.Select(m => m.ToWSMessage());
		}



	    public static ValidationMessage ToValidationMessage(this WSMessage message)
	    {
	        return new ValidationMessage
            {
                Message = message.Value,
	            Type = message.Type == WSEnums.MessageType.Error
	                ? ValidateMessageType.Error
                    : message.Type == WSEnums.MessageType.Warning
                        ? ValidateMessageType.Warning
                        :  ValidateMessageType.Information
	        };
	    }

	    public static IEnumerable<ValidationMessage> ToValidationMessages(this IEnumerable<WSMessage> messages)
	    {
	        return messages.Select(m => m.ToValidationMessage());
	    }



        public static List<Rate> RateShipment(this Shipment s, HttpContext context)
		{
			try
			{
                var pmaps = ProcessorVars.RegistryCache[s.User.TenantId].SmallPackagingMaps;
                var smaps = ProcessorVars.RegistryCache[s.User.TenantId].SmallPackageServiceMaps;
				var variables = ProcessorVars.IntegrationVariables[s.User.TenantId];
				var rater = new Rater2(variables.RatewareParameters, variables.CarrierConnectParameters,
									  variables.FedExParameters, variables.UpsParameters, pmaps, smaps, variables.Project44Settings);
				var rates = rater.GetRates(s).ToList();

				return rates;

			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, context);
				return new List<Rate>();
			}
		}

		public static Rate FindMatchingRate(this WSRate2 rate, IEnumerable<Rate> rates)
		{
			return rates
				.Where(r =>
				       	{
				       		var vendor = r.Mode == ServiceMode.LessThanTruckload
				       		             	? r.LTLSellRate.VendorRating.Vendor
				       		             	: r.Vendor ?? new Vendor
				       		             	              	{
				       		             	              		Name = r.Mode == ServiceMode.Truckload ? ContractCarrier : string.Empty,
				       		             	              		VendorNumber = r.Mode == ServiceMode.Truckload ? ContractCarrier : string.Empty
				       		             	              	};
				       		return rate.CarrierScac == vendor.Scac && rate.CarrierKey == vendor.VendorNumber &&
				       		       rate.ServiceMode == r.Mode;
				       	})
				.FirstOrDefault();
		}

		public static WSRate2 ToWSRate(this Rate rate, RateCharge guarRateSelected = null)
		{
			var vendor = rate.Mode == ServiceMode.LessThanTruckload
			             	? rate.LTLSellRate.VendorRating.Vendor
			             	: rate.Vendor ?? new Vendor
			             	                 	{
			             	                 		Name = rate.Mode == ServiceMode.Truckload ? ContractCarrier : string.Empty,
			             	                 		VendorNumber = rate.Mode == ServiceMode.Truckload ? ContractCarrier : string.Empty
			             	                 	};
		    var wsRate = new WSRate2
		    {
		        BilledWeight = rate.BilledWeight,
		        RatedWeight = rate.RatedWeight,
		        RatedCubicFeet = rate.RatedCubicFeet,
		        CarrierKey = vendor.VendorNumber,
		        CarrierName = vendor.Name,
		        CarrierScac = vendor.Scac,
		        TransitTime = rate.TransitDays,
		        EstimatedDeliveryDate = rate.EstimatedDeliveryDate,
		        ServiceMode = rate.Mode,
		        BillingDetails = rate.BaseRateCharges
		            .Select(c => new WSBillingDetail
		            {
		                ReferenceType = DetailReferenceType.Miscellaneous,
		                ReferenceNumber = string.Empty,
		                Category = c.ChargeCode.Category,
		                BillingCode = c.ChargeCode.Code,
		                Description = c.ChargeCode.Description,
		                AmountDue = c.AmountDue
		            })
		            .ToArray(),
		        Mileage = rate.Mileage,
		        MileageSourceKey = rate.MileageSourceId,
		        MileageSourceDescription = rate.Mode == ServiceMode.Truckload
		            ? new MileageSource(rate.MileageSourceId).Description
		            : null,
		        SelectedGuarOption = guarRateSelected == null
		            ? null
		            : !rate.IsProject44Rate
		                ? new WSBillingDetail
		                {
		                    ReferenceType = DetailReferenceType.Miscellaneous,
		                    ReferenceNumber = guarRateSelected.LTLGuaranteedCharge.Time,
		                    Category = guarRateSelected.ChargeCode.Category,
		                    BillingCode = guarRateSelected.ChargeCode.Code,
		                    Description =
		                        guarRateSelected.ChargeCode.GetDescription(guarRateSelected.LTLGuaranteedCharge),
		                    AmountDue = guarRateSelected.AmountDue
		                }
		                : new WSBillingDetail
		                {
		                    ReferenceType = DetailReferenceType.Miscellaneous,
		                    ReferenceNumber = guarRateSelected.ChargeCode.Project44Code.ToString(),
		                    Category = guarRateSelected.ChargeCode.Category,
		                    BillingCode = guarRateSelected.ChargeCode.Code,
		                    Description = string.Format("{0} - {1}", guarRateSelected.ChargeCode.Description, guarRateSelected.ChargeCode.Project44Code.GetChargeCodeDescription()),
		                    AmountDue = guarRateSelected.AmountDue
		                },
		    };

		    var guaranteedCharges = rate.GuaranteedCharges
                .Where(c => !c.IsProject44GuaranteedCharge)
		        .OrderBy(c => c.LTLGuaranteedCharge.Time)
		        .Select(c => new WSBillingDetail
		        {
		            ReferenceType = DetailReferenceType.Miscellaneous,
		            ReferenceNumber = c.LTLGuaranteedCharge.Time,
		            Category = c.ChargeCode.Category,
		            BillingCode = c.ChargeCode.Code,
		            Description = c.ChargeCode.GetDescription(c.LTLGuaranteedCharge),
		            AmountDue = c.AmountDue
		        }).ToList();

            guaranteedCharges.AddRange(rate.GuaranteedCharges
                .Where(c => c.IsProject44GuaranteedCharge)
                .OrderBy(c => c.ChargeCode.Project44Code)
                .Select(c => new WSBillingDetail
                {
                    ReferenceType = DetailReferenceType.Miscellaneous,
                    ReferenceNumber = c.ChargeCode.Project44Code.ToString(),
                    Category = c.ChargeCode.Category,
                    BillingCode = c.ChargeCode.Code,
                    Description = string.Format("{0} - {1}", c.ChargeCode.Description, c.ChargeCode.Project44Code.GetChargeCodeDescription()),
                    AmountDue = c.AmountDue
                }));

		    wsRate.GuarOptions = guaranteedCharges.ToArray();

            // total up charges
            foreach (var detail in wsRate.BillingDetails)
			{
				wsRate.TotalCharges += detail.AmountDue;
				switch (detail.Category)
				{
					case ChargeCodeCategory.Fuel:
						wsRate.FuelCharges += detail.AmountDue;
						break;
					case ChargeCodeCategory.Freight:
						wsRate.FreightCharges += detail.AmountDue;
						break;
					case ChargeCodeCategory.Accessorial:
						wsRate.AccessorialCharges += detail.AmountDue;
						break;
					case ChargeCodeCategory.Service:
						wsRate.ServiceCharges += detail.AmountDue;
						break;
				}
			}
			if (wsRate.SelectedGuarOption != null)
			{
				wsRate.TotalCharges += wsRate.SelectedGuarOption.AmountDue;
				switch (wsRate.SelectedGuarOption.Category)
				{
					case ChargeCodeCategory.Fuel:
						wsRate.FuelCharges += wsRate.SelectedGuarOption.AmountDue;
						break;
					case ChargeCodeCategory.Freight:
						wsRate.FreightCharges += wsRate.SelectedGuarOption.AmountDue;
						break;
					case ChargeCodeCategory.Accessorial:
						wsRate.AccessorialCharges += wsRate.SelectedGuarOption.AmountDue;
						break;
					case ChargeCodeCategory.Service:
						wsRate.ServiceCharges += wsRate.SelectedGuarOption.AmountDue;
						break;
				}
			}

			return wsRate;
		}

		public static WSRate2[] ToWSRates(this IEnumerable<Rate> rates, HttpContext context)
		{
			try
			{
				return rates.Select(rate => rate.ToWSRate()).ToArray();
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, context);
				return new WSRate2[0];
			}
		}

		public static ShoppedRate GetShoppedRate(this Shipment shipment, WSRate2[] rates, ShoppedRateType shoppedRateType)
		{
			var rate = rates.Any() ? rates.ToList().OrderBy(r => r.TotalCharges).FirstOrDefault() : null;
			var shoppedRate = new ShoppedRate
			                  	{
			                  		TenantId = shipment.TenantId,
			                  		Shipment = shipment,
			                  		ReferenceNumber = shipment.ShipmentNumber.GetString(),
			                  		Type = shoppedRateType,
			                  		NumberLineItems = shipment.Items.Count,
			                  		Quantity = shipment.Items.Sum(i => i.Quantity),
			                  		Weight = shipment.Items.Sum(i => i.ActualWeight),
			                  		HighestFreightClass = shipment.Items.Any() ? shipment.Items.Max(i => i.ActualFreightClass) : 0,
			                  		MostSignificantFreightClass = shipment.Items.Any()
			                  		                              	? shipment.Items
			                  		                              	  	.GroupBy(i => i.ActualFreightClass)
			                  		                              	  	.Select(g => new {FreightClass = g.Key, Items = g})
			                  		                              	  	.OrderByDescending(a => a.Items.Count())
			                  		                              	  	.ThenByDescending(a => a.FreightClass)
			                  		                              	  	.First().FreightClass
			                  		                              	: 0,
			                  		LowestTotalCharge = rate == null ? 0 : rate.TotalCharges,
			                  		DateCreated = DateTime.Now,
			                  		Customer = shipment.Customer,
			                  		User = shipment.User,
			                  		OriginCountryId = shipment.Origin.CountryId,
			                  		OriginCity = shipment.Origin.City.GetString(),
			                  		OriginPostalCode = shipment.Origin.PostalCode.GetString(),
			                  		OriginState = shipment.Origin.State.GetString(),
			                  		DestinationCountryId = shipment.Destination.CountryId,
			                  		DestinationCity = shipment.Destination.City.GetString(),
			                  		DestinationPostalCode = shipment.Destination.PostalCode.GetString(),
			                  		DestinationState = shipment.Destination.State.GetString(),
									VendorName = rate == null ? string.Empty : rate.CarrierName,
									VendorNumber = rate == null ? string.Empty : rate.CarrierKey,
			                  		Services = new List<ShoppedRateService>()
			                  	};

			foreach (var service in shipment.Services)
				shoppedRate.Services.Add(new ShoppedRateService
				                         	{
				                         		TenantId = shipment.TenantId,
				                         		ServiceId = service.ServiceId,
				                         		ShoppedRate = shoppedRate,
				                         	});
			return shoppedRate;
		}

		private static string GetDescription(this ChargeCode code, LTLGuaranteedCharge guaranteedCharge)
		{
			return string.Format("{0} - by {1}", code.Description, guaranteedCharge.Time.ConvertAmPm());
		}

		public static RateCharge GetGuarRateCharge(this Rate rate, WSBillingDetail guarRateSelected = null)
		{
		    return guarRateSelected == null
		        ? null
		        : !rate.IsProject44Rate
		          ? rate.GuaranteedCharges.FirstOrDefault(c => c.LTLGuaranteedCharge.Time == guarRateSelected.ReferenceNumber)
		          : rate.GuaranteedCharges.FirstOrDefault(c => c.ChargeCode.Project44Code.ToString() == guarRateSelected.ReferenceNumber);
		}
	}
}
