﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Connect;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.WebApplication.Members;

namespace LogisticsPlus.Eship.WebApplication.Utilities
{
	public class FtpDocDeliveryProcessor
	{
		private static readonly TextEncryptor Encryptor = new TextEncryptor("E/FBigVVK3FQhYkX0pG0Y8kiRk3rLVfyopwGnq807wM=", "ob88dGazarud9L4WBwAJRQ==");

		private const string DocViewerPlus = "--plus--";
		private const string DocViewerAmpersand = "--amp--";
		private const string DocViewerQuestion = "--question--";
		private const string DocViewerEqual = "--Equal--";


		private readonly DocDeliveryProcessorHandler _handler;
		private bool _shutdownThread;
		private Thread _managerThread;
		private readonly HttpServerUtility _server;


		public bool IsRunning
		{
			get { return _managerThread != null; }
		}

		public string ErrorsFolder
		{
			get { return _server.MapPath(WebApplicationSettings.ErrorsFolder); }
		}

		public FtpDocDeliveryProcessor(HttpServerUtility server)
		{
			_server = server;
			_handler = new DocDeliveryProcessorHandler();
		}

		~FtpDocDeliveryProcessor()
		{
			Shutdown();
		}


		public static string EncryptKey(string keyValue)
		{
			return Encryptor.Encrypt(keyValue);
		}

		public static string DecryptKey(string encryptedKeyValue)
		{
			return Encryptor.Decrypt(encryptedKeyValue);
		}



		public static string EncodeForQueryString(string value)
		{
			return value.Replace("&", DocViewerAmpersand).Replace("+", DocViewerPlus).Replace("?", DocViewerQuestion).Replace("=", DocViewerEqual);
		}

		public static string DecodeFromQueryString(string value)
		{
			return value.Replace(DocViewerAmpersand, "&").Replace(DocViewerPlus, "+").Replace(DocViewerQuestion, "?").Replace(DocViewerEqual, "=");
		}



		public void Start()
		{
			try
			{
				if (_managerThread == null)
				{
					// start the scheduling thread
					_managerThread = new Thread(ManagerThread);
					_managerThread.Start();
				}
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, ErrorsFolder);
			}
		}

		public void Shutdown()
		{
			if (_managerThread != null)
			{
				_shutdownThread = true;

				// wait for the scheduler thread to shutdown for up to 60 sec
				// if the thread does not shutdown on time, try to kill it
				if (!_managerThread.Join(60000))
				{
					try
					{
						_managerThread.Abort();
					}
					catch (Exception e)
					{
						ErrorLogger.LogError(e, ErrorsFolder);
					}
				}

				_managerThread = null;
			}
		}

		private void ManagerThread()
		{
			// delay before starting the thread
			Thread.Sleep(250);

			// init some state variables
			var counter = 3600;

			if (_shutdownThread) return;

			do
			{
				// each actual thread iteration occurs in 1 minute intervals
				// however to make the tread more responsive it will sleep 3600 x 250ms
				if (counter != 0)
				{
					Thread.Sleep(250);
					counter--;
					continue;
				}

				// reset the counter
				counter = 3600;

				try
				{
					ProcessDeliveries();
				}
				catch (Exception ex)
				{
					Debug.WriteLine("eShipPlus:DocDelivery : " + ex.Message);
					ErrorLogger.LogError(ex, ErrorsFolder);
				}
			} while (!_shutdownThread);
		}



		public void RedeliverShipmentDocument(CustomerCommunication com, ShipmentDocument doc, User executingUser)
		{
			DeliverShipmentDocument(com, doc, false, executingUser);
		}

		public void RedeliverShipmentBol(CustomerCommunication com, Shipment shipment, byte[] docContent, User executingUser)
		{
			DeliverShipmentBol(com, shipment, docContent, false, executingUser);
		}

		public void RedeliverShipmentStatement(CustomerCommunication com, Shipment shipment, byte[] docContent, User executingUser)
		{
			DeliverShipmentStatement(com, shipment, docContent, false, executingUser);
		}

		public void RedeliverInvoice(CustomerCommunication com, Invoice invoice, byte[] docContent, User executingUser)
		{
			DeliverInvoice(com, invoice, docContent, false, executingUser);
		}

		

		private void ProcessDeliveries()
		{
			var coms = new CustomerSearch().FetchCustomerCommunicationsWithDocDelivery();

			foreach (var com in coms)
			{
				// process undelivered shipment documents
				if (com.DocDeliveryDocTags.Any()) ProcessShipmentForShipmentDocuments(com);

				// process undelivered shipment bol
				if (com.DeliverBolDoc) ProcessShipmentForBols(com);

				// process undelivered shipment statement
				if (com.DeliverShipmentStatementDoc) ProcessShipmentForStatements(com);

				// process undelivered invoices
				if (com.DeliverInvoiceDoc) ProcessInvoices(com);
			}
		}

		private void ProcessShipmentForShipmentDocuments(CustomerCommunication com)
		{
			var tagIds = com.DocDeliveryDocTags.Select(t => t.DocumentTagId).ToList();
			var docs = new ShipmentSearch().FetchShipmentDocumentsNotDelivered(com.StartDocDeliveriesFrom, com.Customer.Id, com.TenantId, com.HoldShipmentDocsTillInvoiced, com.DeliverInternalShipmentDocs, tagIds);
			foreach (var doc in docs)
				DeliverShipmentDocument(com, doc, true, com.Tenant.DefaultSystemUser);
		}

		private void ProcessShipmentForBols(CustomerCommunication com)
		{
			var shipments = new ShipmentSearch().FetchShipmentNotBolDelivered(com.StartDocDeliveriesFrom, com.Customer.Id, com.TenantId, com.HoldShipmentDocsTillInvoiced);
			foreach (var shipment in shipments)
				DeliverShipmentBol(com, shipment, GetBolContent(shipment, com.Tenant.DefaultSystemUserId.GetString()), true, com.Tenant.DefaultSystemUser);
		}

		private void ProcessShipmentForStatements(CustomerCommunication com)
		{
			var shipments = new ShipmentSearch().FetchShipmentNotShipmentStatementDelivered(com.StartDocDeliveriesFrom, com.Customer.Id, com.TenantId);
			foreach (var shipment in shipments)
				DeliverShipmentStatement(com, shipment, GetStatementContent(shipment, com.Tenant.DefaultSystemUserId.GetString()), true, com.Tenant.DefaultSystemUser);
		}

		private void ProcessInvoices(CustomerCommunication com)
		{
			var invoices = new InvoiceSearch().FetchInvoiceNotDocDelivered(com.StartDocDeliveriesFrom, com.Customer.Id, com.TenantId);
			foreach (var invoice in invoices)
				DeliverInvoice(com, invoice, GetInvoiceContent(invoice, com.Tenant.DefaultSystemUserId.GetString()), true, com.Tenant.DefaultSystemUser);
		}



		private void DeliverShipmentDocument(CustomerCommunication com, ShipmentDocument doc, bool logToEntity, User executingUser)
		{
			Lock @lock = null;
			try
			{
				if (logToEntity)
				{
					// obtain record lock; DO NOT PROCESS if lock cannot be obtained
					@lock = doc.Shipment.ObtainLock(executingUser, doc.Shipment.Id);
					if (!@lock.IsUserLock(executingUser)) return;
				}

				var name = string.Format("{0}_{1}_{2}{3}", doc.Shipment.ShipmentNumber, doc.DocumentTag.Code, doc.Name, _server.GetFileExtension(doc.LocationPath));
				var content = _server.ReadFromFile(doc.LocationPath);

				var err = this.SendDoc(com, name, content);
				var log = new DocDeliveryLog
				          	{
				          		TenantId = com.TenantId,
				          		DocumentName = name,
				          		DocumentTag = doc.DocumentTag,
				          		EntityId = doc.Id,
				          		EntityType = DocDeliveryEntityType.ShipmentDocument,
				          		LocationPath = doc.LocationPath,
				          		LogDateTime = DateTime.Now,
				          		DeliveryWasSuccessful = string.IsNullOrEmpty(err),
				          		FailedDeliveryMessage = err,
								User = executingUser
				          	};

				Exception e;
				_handler.SaveDocDeliveryLog(log, out e);
				if (e != null)
				{
					ErrorLogger.LogError(e, ErrorsFolder);
					return;
				}

				if (logToEntity)
				{
					_handler.LogShipmentDocumentDelivery(doc, executingUser, out e);
					if (e != null) ErrorLogger.LogError(e, ErrorsFolder);
				}
			}
			catch (Exception ex)
			{
				ErrorLogger.LogError(ex, ErrorsFolder);
				if (@lock != null) @lock.Delete();
			}
		}

		private void DeliverShipmentBol(CustomerCommunication com, Shipment shipment, byte[] docContent, bool logToEntity, User executingUser)
		{
			Lock @lock = null;
			try
			{
				if(logToEntity)
				{
					// obtain record lock; DO NOT PROCESS if lock cannot be obtained
					@lock = shipment.ObtainLock(executingUser, shipment.Id);
					if (!@lock.IsUserLock(executingUser)) return;
				}


				var name = string.Format("{0}_Bol.pdf", shipment.ShipmentNumber);

				var err = this.SendDoc(com, name, docContent);
				var log = new DocDeliveryLog
				{
					TenantId = com.TenantId,
					DocumentName = name,
					DocumentTag = null,
					EntityId = shipment.Id,
					EntityType = DocDeliveryEntityType.BillOfLading,
					LocationPath = string.Empty,
					LogDateTime = DateTime.Now,
					DeliveryWasSuccessful = string.IsNullOrEmpty(err),
					FailedDeliveryMessage = err,
					User = executingUser
				};

				Exception e;
				_handler.SaveDocDeliveryLog(log, out e);
				if (e != null)
				{
					ErrorLogger.LogError(e, ErrorsFolder);
					return;
				}

				if (logToEntity)
				{
					_handler.LogShipmentBolDelivery(shipment, executingUser, out e);
					if (e != null) ErrorLogger.LogError(e, ErrorsFolder);
				}
			}
			catch (Exception ex)
			{
				ErrorLogger.LogError(ex, ErrorsFolder);
				if (@lock != null) @lock.Delete();
			}
		}

		private void DeliverShipmentStatement(CustomerCommunication com, Shipment shipment, byte[] docContent, bool logToEntity, User executingUser)
		{
			Lock @lock = null;
			try
			{
				if (logToEntity)
				{
					// obtain record lock; DO NOT PROCESS if lock cannot be obtained
					@lock = shipment.ObtainLock(executingUser, shipment.Id);
					if (!@lock.IsUserLock(executingUser)) return;
				}

				var name = string.Format("{0}_Statement.pdf", shipment.ShipmentNumber);

				var err = this.SendDoc(com, name, docContent);
				var log = new DocDeliveryLog
				{
					TenantId = com.TenantId,
					DocumentName = name,
					DocumentTag = null,
					EntityId = shipment.Id,
					EntityType = DocDeliveryEntityType.ShipmentStatement,
					LocationPath = string.Empty,
					LogDateTime = DateTime.Now,
					DeliveryWasSuccessful = string.IsNullOrEmpty(err),
					FailedDeliveryMessage = err,
					User = executingUser
				};

				Exception e;
				_handler.SaveDocDeliveryLog(log, out e);
				if (e != null)
				{
					ErrorLogger.LogError(e, ErrorsFolder);
					return;
				}

				if (logToEntity)
				{
					_handler.LogShipmentStatementDelivery(shipment, executingUser, out e);
					if (e != null) ErrorLogger.LogError(e, ErrorsFolder);
				}
			}
			catch (Exception ex)
			{
				ErrorLogger.LogError(ex, ErrorsFolder);
				if (@lock != null) @lock.Delete();
			}
		}

		private void DeliverInvoice(CustomerCommunication com, Invoice invoice, byte[] docContent, bool logToEntity, User executingUser)
		{
			Lock @lock = null;
			try
			{
				if (logToEntity)
				{
					// obtain record lock; DO NOT PROCESS if lock cannot be obtained
					@lock = invoice.ObtainLock(executingUser, invoice.Id);
					if (!@lock.IsUserLock(executingUser)) return;
				}

				var name = string.Format("{0}_Invoice.pdf", invoice.InvoiceNumber);

				var err = this.SendDoc(com, name, docContent);
				var log = new DocDeliveryLog
				{
					TenantId = com.TenantId,
					DocumentName = name,
					DocumentTag = null,
					EntityId = invoice.Id,
					EntityType = DocDeliveryEntityType.Invoice,
					LocationPath = string.Empty,
					LogDateTime = DateTime.Now,
					DeliveryWasSuccessful = string.IsNullOrEmpty(err),
					FailedDeliveryMessage = err,
					User = executingUser
				};

				Exception e;
				_handler.SaveDocDeliveryLog(log, out e);
				if (e != null)
				{
					ErrorLogger.LogError(e, ErrorsFolder);
					return;
				}

				if (logToEntity)
				{
					_handler.LogInvoiceDelivery(invoice,executingUser, out e);
					if (e != null) ErrorLogger.LogError(e, ErrorsFolder);
				}
			}
			catch (Exception ex)
			{
				ErrorLogger.LogError(ex, ErrorsFolder);
				if (@lock != null) @lock.Delete();
			}
		}




		private static byte[] GetBolContent(Shipment shimpent, string ukey)
		{
			var url = DocumentViewer.GenerateShipmentBolLink(shimpent);

			// clean up url add base
			url = WebApplicationSettings.SiteRoot.EndsWith("/")
					? WebApplicationSettings.SiteRoot + url.Replace("~/", string.Empty)
					: WebApplicationSettings.SiteRoot + url.Replace("~", string.Empty);

			// add authentication user
			url = url + "&" + WebApplicationConstants.DocumentDeliveryUKey + "=" +
				  HttpUtility.HtmlEncode(EncodeForQueryString(Encryptor.Encrypt(ukey)));

			return GetContents(url);
		}

		private static byte[] GetStatementContent(Shipment shimpent, string ukey)
		{
			var url = DocumentViewer.GenerateShipmentStatementLink(shimpent);

			// clean up url add base
			url = WebApplicationSettings.SiteRoot.EndsWith("/")
					? WebApplicationSettings.SiteRoot + url.Replace("~/", string.Empty)
					: WebApplicationSettings.SiteRoot + url.Replace("~", string.Empty);

			// add authentication user
			url = url + "&" + WebApplicationConstants.DocumentDeliveryUKey + "=" +
				  HttpUtility.HtmlEncode(EncodeForQueryString(Encryptor.Encrypt(ukey)));

			return GetContents(url);
		}

		private static byte[] GetInvoiceContent(Invoice invoice, string ukey)
		{
			var url = DocumentViewer.GenerateInvoiceLink(invoice);

			// clean up url add base
			url = WebApplicationSettings.SiteRoot.EndsWith("/")
					? WebApplicationSettings.SiteRoot + url.Replace("~/", string.Empty)
					: WebApplicationSettings.SiteRoot + url.Replace("~", string.Empty);

			// add authentication user
			url = url + "&" + WebApplicationConstants.DocumentDeliveryUKey + "=" +
				  HttpUtility.HtmlEncode(EncodeForQueryString(Encryptor.Encrypt(ukey)));

			return GetContents(url);
		}

		private static byte[] GetContents(string url)
		{
			var request = (HttpWebRequest)WebRequest.Create(new Uri(url));
			request.Method = WebRequestMethods.Http.Get;
			byte[] contents;
			using (var response = (HttpWebResponse)request.GetResponse())
			using (var reader = new BinaryReader(response.GetResponseStream()))
			{
				contents = new byte[response.ContentLength];
				for (var i = 0; i < response.ContentLength; i++)
					reader.Read(contents, i, 1);
			}
			return contents;
		}


	}
}
