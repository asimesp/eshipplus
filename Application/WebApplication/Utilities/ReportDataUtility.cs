﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using Ionic.Zip;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Reports;
using OfficeOpenXml;
using OfficeOpenXml.Drawing.Chart;

namespace LogisticsPlus.Eship.WebApplication.Utilities
{
    public static class ReportDataUtility
    {
		private const string FloatingPointFormat = "###,###,##0.00";
        private const int DefaultChartWidth = 1024;
        private const int DefaultChartHeight = 768;

	    public static FileInfo TransformTabDelimited(this ReportData data, HttpServerUtility server)
		{
			var lines = new List<string>();
			if (data.HasSummaries)
			{
				lines.Add("Summaries:");
				lines.AddRange(data.Summaries.Select(summary => new[] { summary.Key, string.Format("{0}", summary.Value) }.ToArray().TabJoin()));
				lines.Add(string.Empty);
			}

	        if (data.HasPivotTable)
	        {
	            lines.Add("Pivot Tables:");
	            lines.Add(string.Empty);

	            foreach (var table in data.PivotTables)
	            {
	                if(table.TableName != string.Empty) lines.Add(table.TableName + ":");
	                var columns = table.Columns.Cast<DataColumn>().Select(c => c.ColumnName).ToList();
	                lines.Add(columns.Select(c => string.Format("\"{0}\"", c)).ToArray().TabJoin());
	                lines.AddRange(from DataRow row in table.Rows
	                               select columns
	                                   .Select(c => string.Format("{0}", row[c])
	                                                      .Replace("\"", "''")
	                                                      .Replace("\t", "     ")
	                                                      .Replace(Environment.NewLine, string.Empty)
	                                                      .Replace("\n", string.Empty)
	                                                      .Replace("\r", string.Empty))
	                                   .Select(c => string.Format("\"{0}\"", c))
	                                   .ToArray()
	                                   .TabJoin());
	                lines.Add(string.Empty);

	            }
	        }

	        if (data.HasTable)
			{
				if (data.HasSummaries || data.HasPivotTable) lines.Add("Data:");

				var columns = data.Table.Columns.Cast<DataColumn>().Select(c => c.ColumnName).ToList();
				lines.Add(columns.Select(c => string.Format("\"{0}\"", c)).ToArray().TabJoin());
				lines.AddRange(from DataRow row in data.Table.Rows
				               select columns
					               .Select(c => string.Format("{0}", row[c])
					                                  .Replace("\"", "''")
					                                  .Replace("\t", "     ")
					                                  .Replace(Environment.NewLine, string.Empty)
													  .Replace("\n", string.Empty)
													  .Replace("\r", string.Empty))
					               .Select(c => string.Format("\"{0}\"", c))
					               .ToArray()
					               .TabJoin());
			}


			var filename = server.MapPath(string.Format("{0}{1}.{2}", WebApplicationSettings.TempFolder,
														Guid.NewGuid().GetString(),
														ReportExportExtension.txt.GetString()));
			var info = new FileInfo(filename);
			using(var stream = info.OpenWrite())
			using (var writer = new StreamWriter(stream))
				foreach (var line in lines)
					writer.WriteLine(line);
			return info;
		}

		public static FileInfo TransformExcel(this ReportData data, HttpServerUtility server, List<ReportColumn> reportColumns, List<ChartConfiguration> chartConfigs = null, string fileName = "")
		{

		    var filename = fileName == string.Empty
		        ? server.MapPath(string.Format("{0}{1}.{2}", WebApplicationSettings.TempFolder,
		            Guid.NewGuid().GetString(),
		            ReportExportExtension.xlsx.GetString()))
		        : fileName;

            var info = new FileInfo(filename);
            
			DataTable table = null;
		    List<DataTable> pivotTables = null;
		    var pivotSheetIndices = new List<int>();

			using (var pck = new ExcelPackage { Compression = CompressionLevel.BestCompression })
			{
				var ws = pck.Workbook.Worksheets.Add(data.HasTable ? data.Table.TableName : "Report Data");


				var startRowIndex = 1;

				if (data.HasSummaries)
				{
					var collection = data.Summaries.Select(s => new {Key = s.Key.Trim(), s.Value}).ToList();
					collection.Insert(0, new {Key = string.Empty, Value = (object) string.Empty});
					collection.Insert(0, new {Key = "Summaries", Value = (object) string.Empty});

					collection.Add(new {Key = string.Empty, Value = (object) string.Empty});

					ws.Cells[startRowIndex, 1].LoadFromCollection(collection);
					ws.Cells[startRowIndex + 1, 1, collection.Count, 1].Style.Font.Bold = true;
					startRowIndex += collection.Count;

					for (var i = 0; i < data.Summaries.Count; i++)
					{
						var col = reportColumns.FirstOrDefault(c => data.Summaries[i].Key == c.Name);
						if (col != null && col.DataType.ToInt().IsFloatingPoint())
							ws.Cells[i + 3, 2].Style.Numberformat.Format = FloatingPointFormat;
					}
				}

				if (data.HasTable)
				{
					if (data.HasSummaries)
					{
						ws.Cells[startRowIndex, 1].LoadFromCollection(new[]
						                                              	{
						                                              		new {Key = "Data", Value = (object) string.Empty},
						                                              		new {Key = string.Empty, Value = (object) string.Empty}
						                                              	});
						startRowIndex += 2; // compensate for above two rows
					}

					for (var c = 0; c < data.Table.Columns.Count; c++)
						ws.Cells[startRowIndex, 1 + c].Value = data.Table.Columns[c].GetString().Trim();
					ws.Cells[startRowIndex, 1, startRowIndex, data.Table.Columns.Count < 1 ? 1 : data.Table.Columns.Count].Style.Font.Bold = true;

					table = data.Table; // hold to write later!
				}

			    if (chartConfigs != null)
			    {
			        foreach (var chart in chartConfigs.Where(c => c.DataSourceType == ChartDataSourceType.Original))
			        {
			            if (!chart.YAxisDataConfigurations.Any()) continue;

			            var chartType = chart.YAxisDataConfigurations.First().Type.ToEChartType();

			            var chartSheet = pck.Workbook.Worksheets.Add(chart.Title)
			                                .Drawings.AddChart(chart.Title, chartType);

			            chartSheet.SetSize(DefaultChartWidth, DefaultChartHeight);
			            chartSheet.Title.Text = chart.Title;
			            if (chartType != eChartType.Pie)
			            {
			                chartSheet.XAxis.Title.Text = chart.XAxisTitle;
			                chartSheet.YAxis.Title.Text = chart.YAxisTitle;
			            }
			            // get column index of what will serve as the x series
			            var xAxisColumn = 1 + ws.Cells[startRowIndex, 1, startRowIndex, ws.Cells.Columns]
			                                      .TakeWhile(cell => cell.Value.GetString() != chart.XAxisDataColumn)
			                                      .Count();

			            // get all header names from YAxisConfigurations for the first chart type being plotted
			            var ySeriesColumnNames = chart.YAxisDataConfigurations
			                                          .Where(y => y.Type.ToEChartType() == chartSheet.ChartType)
			                                          .Select(y => y.ReportColumnName);

			            // for each header, locate the column index of the y series data on the spreadsheet, then add y series
			            foreach (var series in ySeriesColumnNames)
			            {
			                var yAxisColumn = 1 + ws.Cells[startRowIndex, 1, startRowIndex, ws.Cells.Columns]
			                                          .TakeWhile(cell => cell.Value.GetString() != series)
			                                          .Count();

			                var newSeries = chartSheet.Series.Add(
			                    ws.Cells[startRowIndex + 1, yAxisColumn, data.Table.Rows.Count + startRowIndex, yAxisColumn],
			                    ws.Cells[startRowIndex + 1, xAxisColumn, data.Table.Rows.Count + startRowIndex, xAxisColumn]);
			                newSeries.Header = series;
			            }

			            // plot additional y series for chart types not equal to the first chart type if they exist
			            var aditionalYSeries = chart
			                .YAxisDataConfigurations
			                .Where(y => y.Type.ToEChartType() != chartSheet.ChartType);

			            if (aditionalYSeries.Any())
			            {
			                var seriesByType = aditionalYSeries
			                    .GroupBy(y => y.Type)
			                    .ToDictionary(y => y.Key, y => y.Select(s => s.ReportColumnName));

			                foreach (var type in seriesByType)
			                {
			                    var additionalChart = chartSheet.PlotArea.ChartTypes.Add(type.Key.ToEChartType());

			                    foreach (var series in type.Value)
			                    {
			                        var yAxisColumn = 1 + ws.Cells[startRowIndex, 1, startRowIndex, ws.Cells.Columns]
			                                                  .TakeWhile(cell => cell.Value.GetString() != series)
			                                                  .Count();
			                        // add series
			                        var newSeries = additionalChart.Series.Add(
			                            ws.Cells[
			                                startRowIndex + 1, yAxisColumn, data.Table.Rows.Count + startRowIndex,
			                                yAxisColumn],
			                            ws.Cells[
			                                startRowIndex + 1, xAxisColumn, data.Table.Rows.Count + startRowIndex,
			                                xAxisColumn]);
			                        newSeries.Header = series;
			                    }
			                }
			            }
			        }
			    }

			    if (data.HasPivotTable)
                {
                    pivotTables = new List<DataTable>();

                    foreach (var pivot in data.PivotTables)
                    {
                        pivotSheetIndices.Add(pck.Workbook.Worksheets.Count + 1);
                        startRowIndex = 1;
                        var pivotWs = pck.Workbook.Worksheets.Add(pivot.TableName != string.Empty
                                                                      ? pivot.TableName
                                                                      : "Pivot " + (1 + data.PivotTables.IndexOf(pivot)));

                        if (data.HasSummaries)
                        {
                            var collection = data.Summaries.Select(s => new { Key = s.Key.Trim(), s.Value }).ToList();
                            collection.Insert(0, new { Key = string.Empty, Value = (object)string.Empty });
                            collection.Insert(0, new { Key = "Summaries", Value = (object)string.Empty });

                            collection.Add(new { Key = string.Empty, Value = (object)string.Empty });

                            pivotWs.Cells[startRowIndex, 1].LoadFromCollection(collection);
                            pivotWs.Cells[startRowIndex + 1, 1, collection.Count, 1].Style.Font.Bold = true;
                            startRowIndex += collection.Count;

                            for (var i = 0; i < data.Summaries.Count; i++)
                            {
                                var col = reportColumns.FirstOrDefault(c => data.Summaries[i].Key == c.Name);
                                if (col != null && col.DataType.ToInt().IsFloatingPoint())
                                    pivotWs.Cells[i + 3, 2].Style.Numberformat.Format = FloatingPointFormat;
                            }

                            pivotWs.Cells[startRowIndex, 1].LoadFromCollection(new[]
						                                              	{
						                                              		new {Key = "Data", Value = (object) string.Empty},
						                                              		new {Key = string.Empty, Value = (object) string.Empty}
						                                              	});
                            startRowIndex += 2; // compensate for above two rows
                        }

                        for (var c = 0; c < pivot.Columns.Count; c++)
                            pivotWs.Cells[startRowIndex, 1 + c].Value = pivot.Columns[c].GetString().Trim();
                        pivotWs.Cells[startRowIndex, 1, startRowIndex, pivot.Columns.Count < 1 ? 1 : pivot.Columns.Count].Style.Font
                                                                                                                     .Bold = true;

                        pivotTables.Add(pivot); // hold to write later!

                        if (chartConfigs != null)
                        {
                            foreach (var chart in chartConfigs.Where(c => c.DataSourceType == ChartDataSourceType.Pivot && c.PivotTableName == pivot.TableName))
                            {
                                if (!chart.YAxisDataConfigurations.Any()) continue;

                                var chartType = chart.YAxisDataConfigurations.First().Type.ToEChartType();

                                var chartSheet = pck.Workbook.Worksheets.Add(chart.Title)
                                                    .Drawings.AddChart(chart.Title, chartType);

                                chartSheet.SetSize(DefaultChartWidth, DefaultChartHeight);
                                chartSheet.Title.Text = chart.Title;
                                if (chartType != eChartType.Pie)
                                {
                                    chartSheet.XAxis.Title.Text = chart.XAxisTitle;
                                    chartSheet.YAxis.Title.Text = chart.YAxisTitle;
                                }

                                // get column index of what will serve as the x series

                                // Hack: Aggregate column names will contain a colon, so every column leading up to the first occurance of a colon is a pivot column
                                var endPivotIndex =
                                    pivot.Columns.Cast<DataColumn>().First(c => c.ColumnName.Contains(":"));

                                var xAxisColumn =
                                    pivotWs.Cells[startRowIndex, 1, startRowIndex, pivot.Columns.IndexOf(endPivotIndex)];

                                // get all header names from YAxisConfigurations for the first chart type being plotted
                                var ySeriesColumnNames = chart.YAxisDataConfigurations
                                                              .Where(y => y.Type.ToEChartType() == chartSheet.ChartType)
                                                              .Select(y => y.ReportColumnName);

                                // for each header, locate the column index of the y series data on the spreadsheet, then add y series
                                foreach (var series in ySeriesColumnNames)
                                {
                                    var yAxisColumn = 1 + pivotWs.Cells[startRowIndex, 1, startRowIndex, pivotWs.Cells.Columns]
                                                              .TakeWhile(cell => cell.Value.GetString() != series)
                                                              .Count();

                                    var newSeries = chartSheet.Series.Add(
                                        pivotWs.Cells[startRowIndex + 1, yAxisColumn, pivot.Rows.Count + startRowIndex, yAxisColumn],
                                        pivotWs.Cells[startRowIndex + 1, xAxisColumn.Start.Column, pivot.Rows.Count + startRowIndex, xAxisColumn.End.Column]);
                                    newSeries.Header = series;
                                }

                                // plot additional y series for chart types not equal to the first chart type if they exist
                                var aditionalYSeries = chart
                                    .YAxisDataConfigurations
                                    .Where(y => y.Type.ToEChartType() != chartSheet.ChartType);

                                if (aditionalYSeries.Any())
                                {
                                    var seriesByType = aditionalYSeries
                                        .GroupBy(y => y.Type)
                                        .ToDictionary(y => y.Key, y => y.Select(s => s.ReportColumnName));

                                    foreach (var type in seriesByType)
                                    {
                                        var additionalChart = chartSheet.PlotArea.ChartTypes.Add(type.Key.ToEChartType());

                                        foreach (var series in type.Value)
                                        {
                                            var yAxisColumn = 1 + pivotWs.Cells[startRowIndex, 1, startRowIndex, pivotWs.Cells.Columns]
                                                                      .TakeWhile(cell => cell.Value.GetString() != series)
                                                                      .Count();
                                            // add series
                                            var newSeries = additionalChart.Series.Add(
                                                pivotWs.Cells[
                                                    startRowIndex + 1, yAxisColumn, pivot.Rows.Count + startRowIndex,
                                                    yAxisColumn],
                                                pivotWs.Cells[
                                                    startRowIndex + 1, xAxisColumn.Start.Column, pivot.Rows.Count + startRowIndex, xAxisColumn.End.Column]);
                                            newSeries.Header = series;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
				pck.SaveAs(info);
			}

			// write table data if table present
            if (table != null) server.WriteExcelData(table, info, reportColumns);
            if (pivotTables != null) server.GeneratePivotExcelSheets(pivotTables, info, pivotSheetIndices);

			return info;
		}
		
	    public static FileInfo TransformCsv(this ReportData data, HttpServerUtility server)
		{
			var lines = new List<string>();
			if (data.HasSummaries)
			{
				lines.Add("Summaries:");
				lines.AddRange(data.Summaries.Select(summary => new[] { summary.Key, string.Format("{0}", summary.Value) }.ToArray().TabJoin()));
				lines.Add(string.Empty);
			}

	        if (data.HasPivotTable)
	        {
	            lines.Add("Pivot Tables:");
	            lines.Add(string.Empty);

	            foreach (var table in data.PivotTables)
	            {
	                if(table.TableName != string.Empty) lines.Add(table.TableName + ":");
	                var columns = table.Columns.Cast<DataColumn>().Select(c => c.ColumnName).ToList();
	                lines.Add(columns.Select(c => string.Format("\"{0}\"", c.Replace(",", " "))).ToArray().CommaJoin());
	                lines.AddRange(from DataRow row in table.Rows
	                               select columns
	                                   .Select(c => string.Format("{0}", row[c])
	                                                      .Replace("\"", "''")
	                                                      .Replace("\t", "     ")
	                                                      .Replace(Environment.NewLine, string.Empty)
	                                                      .Replace("\n", string.Empty)
	                                                      .Replace("\r", string.Empty))
	                                   .Select(c => string.Format("\"{0}\"", c.Replace(",", string.Empty)))
	                                   .ToArray()
	                                   .CommaJoin());
	                lines.Add(string.Empty);

	            }
	        }

	        if (data.HasTable)
			{
				if (data.HasSummaries || data.HasPivotTable) lines.Add("Data:");

				var columns = data.Table.Columns.Cast<DataColumn>().Select(c => c.ColumnName).ToList();
				lines.Add(columns.Select(c => string.Format("\"{0}\"", c.Replace(",", " "))).ToArray().CommaJoin());
				lines.AddRange(from DataRow row in data.Table.Rows
				               select columns
					               .Select(c => string.Format("{0}", row[c])
					                                  .Replace("\"", "''")
					                                  .Replace("\t", "     ")
					                                  .Replace(Environment.NewLine, string.Empty)
													  .Replace("\n", string.Empty)
													  .Replace("\r", string.Empty))
					               .Select(c => string.Format("\"{0}\"", c.Replace(",", string.Empty)))
					               .ToArray()
					               .CommaJoin());
			}


			var filename = server.MapPath(string.Format("{0}{1}.{2}", WebApplicationSettings.TempFolder,
														Guid.NewGuid().GetString(),
														ReportExportExtension.txt.GetString()));
			var info = new FileInfo(filename);
			using(var stream = info.OpenWrite())
			using (var writer = new StreamWriter(stream))
				foreach (var line in lines)
					writer.WriteLine(line);
			return info;
		}

		private static eChartType ToEChartType(this ReportChartType chartType)
        {
            switch (chartType)
            {
                case ReportChartType.Line:
                    return eChartType.Line;
                case ReportChartType.Column:
                    return eChartType.ColumnClustered;
                case ReportChartType.Bar:
                    return eChartType.BarClustered;
                case ReportChartType.Pie:
                    return eChartType.Pie;
                case ReportChartType.StackedBar:
                    return eChartType.BarStacked;
                case ReportChartType.StackedColumn:
                    return eChartType.ColumnStacked;
                case ReportChartType.Point:
                    return eChartType.XYScatter;
                default:
                    return eChartType.Line;
            }
        }
		
		public static void WriteExcelData(this HttpServerUtility server, DataTable table, FileInfo info, List<ReportColumn> reportColumns)
		{
			/*
			 * String manipulation write of excel data to bypass memory upper limit issue with .Net and EPPlus.
			 */

			var sysTempFolder = server.MapPath(WebApplicationSettings.TempFolder);
			var uniqueTempFolder = sysTempFolder + Guid.NewGuid().GetString() + "/"; // unique temp folder needed for each run!
			Directory.CreateDirectory(uniqueTempFolder);
			var newFile = Path.Combine(uniqueTempFolder, info.Name.Replace(info.Extension, string.Empty) + ".zip");

			if (File.Exists(newFile)) File.Delete(newFile);

			info.CopyTo(newFile);

			using (var zip = new ZipFile(newFile))
			{
				// find shared string file
				var sharedStrings = zip.Entries.First(i => i.FileName.Contains("sharedStrings.xml"));
				var entries = zip.SelectEntries(string.Format("name={0}", sharedStrings.FileName));
				var sharedStringZipEntry = entries.First();
				sharedStringZipEntry.Extract(uniqueTempFolder, ExtractExistingFileAction.OverwriteSilently);

				//load shared strings content
				string sharedStringContent;
				var sharedStringFile = Path.Combine(uniqueTempFolder, "xl\\sharedStrings.xml");
				using (var stream = new FileStream(sharedStringFile, FileMode.Open))
				using (var reader = new StreamReader(stream))
					sharedStringContent = reader.ReadToEnd();


				// get count and unique count
				var startIdx = sharedStringContent.IndexOf(" count=\"", StringComparison.Ordinal);
				var endIdx = sharedStringContent.IndexOf("\" ", startIdx, StringComparison.Ordinal);
				var cellCount = sharedStringContent.Substring(startIdx, endIdx - startIdx + "\" ".Length)
												   .Replace("count=\"", string.Empty)
												   .Replace("\"", string.Empty)
												   .Trim()
												   .ToInt();

				// get shared strings
				var sharedString = new Dictionary<string, int>();
				startIdx = sharedStringContent.IndexOf("<t>", StringComparison.Ordinal);
				var lastSharedStringIdx = 0;
				while (startIdx != -1)
				{
					endIdx = sharedStringContent.IndexOf("</t>", startIdx, StringComparison.Ordinal);
					var sub = sharedStringContent.Substring(startIdx, endIdx - startIdx + "</t>".Length);
					sharedString.Add(sub.Replace("<t>", string.Empty).Replace("</t>", string.Empty), lastSharedStringIdx++);
					startIdx = sharedStringContent.IndexOf("<t>", endIdx, StringComparison.Ordinal);
				}

                // find style file
                var styles = zip.Entries.First(i => i.FileName.Contains("styles.xml"));
                entries = zip.SelectEntries(string.Format("name={0}", styles.FileName));
                var stylesZipEntry = entries.First();
                stylesZipEntry.Extract(uniqueTempFolder, ExtractExistingFileAction.OverwriteSilently);

                //load shared strings content
                string stylesContent;
                var stylesFile = Path.Combine(uniqueTempFolder, "xl\\styles.xml");
                using (var stream = new FileStream(stylesFile, FileMode.Open))
                using (var reader = new StreamReader(stream))
                    stylesContent = reader.ReadToEnd();

                // get styles
                startIdx = stylesContent.IndexOf("<cellXfs ", StringComparison.Ordinal);
			    endIdx = stylesContent.IndexOf(">", startIdx, StringComparison.Ordinal);
                var cellXfsStartTag = stylesContent.Substring(startIdx, endIdx - startIdx + ">".Length);
                
                startIdx = cellXfsStartTag.IndexOf("count=\"", StringComparison.Ordinal);
                endIdx = cellXfsStartTag.IndexOf("\"", startIdx + "count=\"".Length, StringComparison.Ordinal);
                var styleCount = cellXfsStartTag.Substring(startIdx + "count=\"".Length, endIdx - (startIdx + "count=\"".Length)).ToInt();
			    var newCellXfsStartTag = string.Format("<cellXfs count=\"{0}\">", styleCount + 2);
                stylesContent = stylesContent.Replace(cellXfsStartTag, newCellXfsStartTag);

                // find last style. numeric style format 4 (Numbers with commas) will be added after last style
			    startIdx = stylesContent.IndexOf(newCellXfsStartTag, StringComparison.Ordinal);
                endIdx = stylesContent.IndexOf("</cellXfs>", startIdx, StringComparison.Ordinal);
                var cellXfsSection = stylesContent.Substring(startIdx + newCellXfsStartTag.Length, endIdx - startIdx - newCellXfsStartTag.Length); // grab between the start and end tag, not including tag
                var newCellXfsSection = cellXfsSection + "<xf numFmtId=\"3\" xfId=\"0\" applyNumberFormat=\"1\" /><xf numFmtId=\"4\" xfId=\"0\" applyNumberFormat=\"1\" />";

			    stylesContent = stylesContent.Replace(cellXfsSection, newCellXfsSection);
                

                // find data worksheet
				var sheet1 = zip.Entries.First(i => i.FileName.Contains("worksheets/sheet1.xml"));
				entries = zip.SelectEntries(string.Format("name={0}", sheet1.FileName));
				var sheet1SipEntry = entries.First();
				sheet1SipEntry.Extract(uniqueTempFolder, ExtractExistingFileAction.OverwriteSilently);

				// load sheet content
				string sheetContent;
				var sheetFile = Path.Combine(uniqueTempFolder, "xl\\worksheets\\sheet1.xml");
				using (var stream = new FileStream(sheetFile, FileMode.Open))
				using (var reader = new StreamReader(stream))
					sheetContent = reader.ReadToEnd();

				// find last row.  Data will be written after last row!!!
				var rowStart = sheetContent.LastIndexOf("<row", StringComparison.Ordinal);
				var rowEnd = sheetContent.IndexOf("</row>", rowStart, StringComparison.Ordinal) + "</row>".Length;

				var rowTemplate = sheetContent.Substring(rowStart, rowEnd - rowStart);

				var contentHeaderSection = sheetContent.Substring(0, rowEnd);
				var contentFooterSection = sheetContent.Substring(contentHeaderSection.Length);

				// get row number of header
				startIdx = rowTemplate.IndexOf(" r=\"", StringComparison.Ordinal);
				endIdx = rowTemplate.IndexOf("\" ", startIdx, StringComparison.Ordinal);
				var rowCntSub = rowTemplate.Substring(startIdx, endIdx - startIdx + "\" ".Length);
				var rowNumber = rowCntSub.Replace("r=\"", string.Empty).Replace("\"", string.Empty).Trim().ToInt();

				// write sheet data
				var temp = uniqueTempFolder + DateTime.Now.ToString("yyyyMMddHHmmssffffff") + ".txt";
				var sheet1TempFile = server.MakeUniqueFilename(temp, true);
				using(var stream = new FileStream(sheet1TempFile, FileMode.CreateNew))
				using (var writer = new StreamWriter(stream))
				{
					const string rowh = "<row r=\"{0}\">";
					const string col = "<c r=\"{0}{1}\" t=\"s\"><v>{2}</v></c>";
					const string colInline = "<c r=\"{0}{1}\" t=\"n\" s=\"{2}\"><v>{3}</v></c>";
					const string rowf = "</row>";

					writer.Write(contentHeaderSection);
					var dataTypes = reportColumns.Select(c => c.DataType.ToInt()).ToList();

					for (var r = 0; r < table.Rows.Count; r++)
					{
						writer.Write(string.Format(rowh, ++rowNumber));

						var cols = new StringBuilder();
						for (var c = 0; c < table.Columns.Count; c++)
						{
							// get column prefix
							var divisible = c;
							var reduce = divisible > 25; // pre-first boundary break; account for excel crazy number scheme!
							var charStack = new Stack<char>();
							do
							{
								var remainder = divisible%26;
								divisible = (divisible/26);

								charStack.Push((char) (remainder + 'A'));

							} while (divisible > 0);
							if (reduce) charStack.Push((char) (charStack.Pop() - 1));

							// build column
							var cellData = dataTypes[c].IsDateTime()
								               ? table.Rows[r][c].FormattedLongDateAlt()
								               : dataTypes[c].IsFloatingPoint()
									                 ? table.Rows[r][c].ToDecimal().GetString()
									                 : dataTypes[c].IsString()
										                   ? string.Format("<![CDATA[{0}]]>", table.Rows[r][c])
										                   : table.Rows[r][c].GetString();
						    cellData = cellData.Trim();
							if (!dataTypes[c].IsNumeric())
							{
								if (!sharedString.ContainsKey(cellData)) sharedString.Add(cellData, lastSharedStringIdx++);
								cols.Append(string.Format(col, new string(charStack.ToArray()), rowNumber, sharedString[cellData]));
								cellCount++;
							}
							else if (dataTypes[c].IsFloatingPoint())
							    cols.Append(string.Format(colInline, new string(charStack.ToArray()), rowNumber, styleCount + 1, cellData));
							else
                                cols.Append(string.Format(colInline, new string(charStack.ToArray()), rowNumber, styleCount, cellData));
						}

						writer.Write(cols);
						writer.Write(rowf);
					}
					writer.Write(contentFooterSection);
				}

				

				// write shared string data
				var sharedStringsTempFile = server.MakeUniqueFilename(temp, true);
				using (var stream = new FileStream(sharedStringsTempFile, FileMode.Create))
				using (var writer = new StreamWriter(stream))
				{
					const string ssh ="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?><sst xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" count=\"{0}\" uniqueCount=\"{1}\">";
					const string si = "<si><t>{0}</t></si>";
					const string ssf = "</sst>";

					writer.Write(string.Format(ssh,cellCount, sharedString.Keys.Count));
					foreach (var key in sharedString.Keys)
						writer.Write(si, key);
					writer.Write(ssf);
				}

                // write style data
                var styleTempFile = server.MakeUniqueFilename(temp, true);
                using (var stream = new FileStream(styleTempFile, FileMode.Create))
                using (var writer = new StreamWriter(stream))
                {
                    writer.Write(stylesContent);
                }


				// update sheet1 & shared strings
				using (var sheet1Stream = new FileStream(sheet1TempFile, FileMode.OpenOrCreate))
                using (var sharedStringsStream = new FileStream(sharedStringsTempFile, FileMode.OpenOrCreate))
                using (var styleStream = new FileStream(styleTempFile, FileMode.OpenOrCreate))
				{
					zip.UpdateEntry(sheet1SipEntry.FileName, sheet1Stream);
                    zip.UpdateEntry(sharedStringZipEntry.FileName, sharedStringsStream);
                    zip.UpdateEntry(stylesZipEntry.FileName, styleStream);
					zip.Save();
				}

				// delete temp folder and temp files
				File.Delete(sheet1TempFile);
				File.Delete(sharedStringsTempFile);
				File.Delete(sheetFile);
                File.Delete(sharedStringFile);
                File.Delete(styleTempFile);
				var dir = uniqueTempFolder + "xl\\";
				var files = Directory.GetFiles(dir, "*.*", SearchOption.AllDirectories);
				foreach (var file in files) File.Delete(file);
				Directory.Delete(dir, true);
			}

			//return file and clean up
			var nInfo = new FileInfo(newFile);
			nInfo.CopyTo(info.FullName, true);
			nInfo.Delete();
			Directory.Delete(uniqueTempFolder);
		}

        public static void GeneratePivotExcelSheets(this HttpServerUtility server, List<DataTable> pivotTables, FileInfo info, List<int> pivotSheetIndices )
        {
            var n = 0;
            foreach (var pivot in pivotTables)
            {
                var sysTempFolder = server.MapPath(WebApplicationSettings.TempFolder);
                var uniqueTempFolder = sysTempFolder + Guid.NewGuid().GetString() + "/";
                    // unique temp folder needed for each run!
                Directory.CreateDirectory(uniqueTempFolder);
                var newFile = Path.Combine(uniqueTempFolder, info.Name.Replace(info.Extension, string.Empty) + ".zip");

                if (File.Exists(newFile)) File.Delete(newFile);

                info.CopyTo(newFile);

                using (var zip = new ZipFile(newFile))
                {
                    // find shared string file
                    var sharedStrings = zip.Entries.First(i => i.FileName.Contains("sharedStrings.xml"));
                    var entries = zip.SelectEntries(string.Format("name={0}", sharedStrings.FileName));
                    var sharedStringZipEntry = entries.First();
                    sharedStringZipEntry.Extract(uniqueTempFolder, ExtractExistingFileAction.OverwriteSilently);

                    //load shared strings content
                    string sharedStringContent;
                    var sharedStringFile = Path.Combine(uniqueTempFolder, "xl\\sharedStrings.xml");
                    using (var stream = new FileStream(sharedStringFile, FileMode.Open))
                    using (var reader = new StreamReader(stream))
                        sharedStringContent = reader.ReadToEnd();


                    // get count and unique count
                    var startIdx = sharedStringContent.IndexOf(" count=\"", StringComparison.Ordinal);
                    var endIdx = sharedStringContent.IndexOf("\" ", startIdx, StringComparison.Ordinal);
                    var cellCount = sharedStringContent.Substring(startIdx, endIdx - startIdx + "\" ".Length)
                                                       .Replace("count=\"", string.Empty)
                                                       .Replace("\"", string.Empty)
                                                       .Trim()
                                                       .ToInt();

                    // get shared strings
                    var sharedString = new Dictionary<string, int>();
                    startIdx = sharedStringContent.IndexOf("<t>", StringComparison.Ordinal);
                    var lastSharedStringIdx = 0;
                    while (startIdx != -1)
                    {
                        endIdx = sharedStringContent.IndexOf("</t>", startIdx, StringComparison.Ordinal);
                        var sub = sharedStringContent.Substring(startIdx, endIdx - startIdx + "</t>".Length);
                        sharedString.Add(sub.Replace("<t>", string.Empty).Replace("</t>", string.Empty),
                                         lastSharedStringIdx++);
                        startIdx = sharedStringContent.IndexOf("<t>", endIdx, StringComparison.Ordinal);
                    }

                    // find style file
                    var styles = zip.Entries.First(i => i.FileName.Contains("styles.xml"));
                    entries = zip.SelectEntries(string.Format("name={0}", styles.FileName));
                    var stylesZipEntry = entries.First();
                    stylesZipEntry.Extract(uniqueTempFolder, ExtractExistingFileAction.OverwriteSilently);

                    //load shared strings content
                    string stylesContent;
                    var stylesFile = Path.Combine(uniqueTempFolder, "xl\\styles.xml");
                    using (var stream = new FileStream(stylesFile, FileMode.Open))
                    using (var reader = new StreamReader(stream))
                        stylesContent = reader.ReadToEnd();

                    // get styles
                    startIdx = stylesContent.IndexOf("<cellXfs ", StringComparison.Ordinal);
                    endIdx = stylesContent.IndexOf(">", startIdx, StringComparison.Ordinal);
                    var cellXfsStartTag = stylesContent.Substring(startIdx, endIdx - startIdx + ">".Length);

                    startIdx = cellXfsStartTag.IndexOf("count=\"", StringComparison.Ordinal);
                    endIdx = cellXfsStartTag.IndexOf("\"", startIdx + "count=\"".Length, StringComparison.Ordinal);
                    var styleCount =
                        cellXfsStartTag.Substring(startIdx + "count=\"".Length, endIdx - (startIdx + "count=\"".Length))
                                       .ToInt();
                    var newCellXfsStartTag = string.Format("<cellXfs count=\"{0}\">", styleCount + 2);
                    stylesContent = stylesContent.Replace(cellXfsStartTag, newCellXfsStartTag);

                    // find last style. numeric style format 4 (Numbers with commas) will be added after last style
                    startIdx = stylesContent.IndexOf(newCellXfsStartTag, StringComparison.Ordinal);
                    endIdx = stylesContent.IndexOf("</cellXfs>", startIdx, StringComparison.Ordinal);
                    var cellXfsSection = stylesContent.Substring(startIdx + newCellXfsStartTag.Length,
                                                                 endIdx - startIdx - newCellXfsStartTag.Length);
                        // grab between the start and end tag, not including tag
                    var newCellXfsSection = cellXfsSection +
                                            "<xf numFmtId=\"3\" xfId=\"0\" applyNumberFormat=\"1\" /><xf numFmtId=\"4\" xfId=\"0\" applyNumberFormat=\"1\" />";

                    stylesContent = stylesContent.Replace(cellXfsSection, newCellXfsSection);


                    // find data worksheet

                    // the first worksheet is named "sheet1.xml". Additional sheets are named "sheet2.xml", "sheet3.xml", .... 
                    var sheet2 = zip.Entries.First(i => i.FileName.Contains("worksheets/sheet" + pivotSheetIndices[n] + ".xml"));
                    entries = zip.SelectEntries(string.Format("name={0}", sheet2.FileName));
                    var sheet2SipEntry = entries.First();
                    sheet2SipEntry.Extract(uniqueTempFolder, ExtractExistingFileAction.OverwriteSilently);

                    // load sheet content
                    string sheetContent;
                    var sheetFile = Path.Combine(uniqueTempFolder, "xl\\worksheets\\sheet" + pivotSheetIndices[n] + ".xml");
                    using (var stream = new FileStream(sheetFile, FileMode.Open))
                    using (var reader = new StreamReader(stream))
                        sheetContent = reader.ReadToEnd();

                    // find last row.  Data will be written after last row!!!
                    var rowStart = sheetContent.LastIndexOf("<row", StringComparison.Ordinal);
                    var rowEnd = sheetContent.IndexOf("</row>", rowStart, StringComparison.Ordinal) + "</row>".Length;

                    var rowTemplate = sheetContent.Substring(rowStart, rowEnd - rowStart);

                    var contentHeaderSection = sheetContent.Substring(0, rowEnd);
                    var contentFooterSection = sheetContent.Substring(contentHeaderSection.Length);

                    // get row number of header
                    startIdx = rowTemplate.IndexOf(" r=\"", StringComparison.Ordinal);
                    endIdx = rowTemplate.IndexOf("\" ", startIdx, StringComparison.Ordinal);
                    var rowCntSub = rowTemplate.Substring(startIdx, endIdx - startIdx + "\" ".Length);
                    var rowNumber = rowCntSub.Replace("r=\"", string.Empty).Replace("\"", string.Empty).Trim().ToInt();

                    // write sheet data
                    var temp = uniqueTempFolder + DateTime.Now.ToString("yyyyMMddHHmmssffffff") + ".txt";
                    var pivotTempFile = server.MakeUniqueFilename(temp, true);
                    using (var stream = new FileStream(pivotTempFile, FileMode.CreateNew))
                    using (var writer = new StreamWriter(stream))
                    {
                        const string rowh = "<row r=\"{0}\">";
                        const string col = "<c r=\"{0}{1}\" t=\"s\"><v>{2}</v></c>";
                        const string colInline = "<c r=\"{0}{1}\" t=\"n\" s=\"{2}\"><v>{3}</v></c>";
                        const string rowf = "</row>";

                        writer.Write(contentHeaderSection);

                        var dataTypes = pivot.Columns.Cast<DataColumn>().Select(c => c.DataType).ToList();

                        for (var r = 0; r < pivot.Rows.Count; r++)
                        {
                            writer.Write(string.Format(rowh, ++rowNumber));

                            var cols = new StringBuilder();
                            for (var c = 0; c < pivot.Columns.Count; c++)
                            {
                                // get column prefix
                                var divisible = c;
                                var reduce = divisible > 25;
                                    // pre-first boundary break; account for excel crazy number scheme!
                                var charStack = new Stack<char>();
                                do
                                {
                                    var remainder = divisible%26;
                                    divisible = (divisible/26);

                                    charStack.Push((char) (remainder + 'A'));

                                } while (divisible > 0);
                                if (reduce) charStack.Push((char) (charStack.Pop() - 1));

                                // build column
                                var cellData = dataTypes[c] == typeof (DateTime)
                                                   ? pivot.Rows[r][c].FormattedLongDateAlt()
                                                   : dataTypes[c] == typeof (decimal)
                                                         ? pivot.Rows[r][c].ToDecimal().GetString()
                                                         : dataTypes[c] == typeof (string)
                                                               ? string.Format("<![CDATA[{0}]]>", pivot.Rows[r][c])
                                                               : pivot.Rows[r][c].GetString();
                                cellData = cellData.Trim();
                                if (dataTypes[c] != typeof(decimal) && dataTypes[c] != typeof(int))
                                {
                                    if (!sharedString.ContainsKey(cellData))
                                        sharedString.Add(cellData, lastSharedStringIdx++);
                                    cols.Append(string.Format(col, new string(charStack.ToArray()), rowNumber,
                                                              sharedString[cellData]));
                                    cellCount++;
                                }
                                else if (dataTypes[c] == typeof(decimal))
                                    cols.Append(string.Format(colInline, new string(charStack.ToArray()), rowNumber,
                                                              styleCount + 1, cellData));
                                else
                                    cols.Append(string.Format(colInline, new string(charStack.ToArray()), rowNumber,
                                                              styleCount, cellData));
                            }

                            writer.Write(cols);
                            writer.Write(rowf);
                        }
                        writer.Write(contentFooterSection);
                    }

                    // write shared string data
                    var sharedStringsTempFile = server.MakeUniqueFilename(temp, true);
                    using (var stream = new FileStream(sharedStringsTempFile, FileMode.Create))
                    using (var writer = new StreamWriter(stream))
                    {
                        const string ssh =
                            "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?><sst xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" count=\"{0}\" uniqueCount=\"{1}\">";
                        const string si = "<si><t>{0}</t></si>";
                        const string ssf = "</sst>";

                        writer.Write(string.Format(ssh, cellCount, sharedString.Keys.Count));
                        foreach (var key in sharedString.Keys)
                            writer.Write(si, key);
                        writer.Write(ssf);
                    }

                    // write style data
                    var styleTempFile = server.MakeUniqueFilename(temp, true);
                    using (var stream = new FileStream(styleTempFile, FileMode.Create))
                    using (var writer = new StreamWriter(stream))
                    {
                        writer.Write(stylesContent);
                    }


                    // update sheet1 & shared strings
                    using (var pivotStream = new FileStream(pivotTempFile, FileMode.OpenOrCreate))
                    using (var sharedStringsStream = new FileStream(sharedStringsTempFile, FileMode.OpenOrCreate))
                    using (var styleStream = new FileStream(styleTempFile, FileMode.OpenOrCreate))
                    {
                        zip.UpdateEntry(sheet2SipEntry.FileName, pivotStream);
                        zip.UpdateEntry(sharedStringZipEntry.FileName, sharedStringsStream);
                        zip.UpdateEntry(stylesZipEntry.FileName, styleStream);
                        zip.Save();
                    }

                    // delete temp folder and temp files
                    File.Delete(pivotTempFile);
                    File.Delete(sharedStringsTempFile);
                    File.Delete(sheetFile);
                    File.Delete(sharedStringFile);
                    File.Delete(styleTempFile);
                    var dir = uniqueTempFolder + "xl\\";
                    var files = Directory.GetFiles(dir, "*.*", SearchOption.AllDirectories);
                    foreach (var file in files) File.Delete(file);
                    Directory.Delete(dir, true);
                }

                //return file and clean up
                var nInfo = new FileInfo(newFile);
                nInfo.CopyTo(info.FullName, true);
                nInfo.Delete();
                Directory.Delete(uniqueTempFolder);

                n++;
            }
        }
    }
}
