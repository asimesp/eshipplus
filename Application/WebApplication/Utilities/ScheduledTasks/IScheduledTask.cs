﻿using System;

namespace LogisticsPlus.Eship.WebApplication.Utilities.ScheduledTasks
{
    /// <summary>
    /// Scheduled tasks interface
    /// </summary>
    public interface IScheduledTask : IDisposable
    {
		/// <summary>
		/// initialization processes
		/// </summary>
        void Init();

		/// <summary>
		/// Task run processes
		/// </summary>
        void Run();
    }
}
