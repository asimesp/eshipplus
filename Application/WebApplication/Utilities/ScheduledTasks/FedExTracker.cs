﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.SmallPacks;
using LogisticssPlus.FedExPlugin.Core.Tracking;
using LogisticsPlus.Eship.Processor.Handlers.Operations;

namespace LogisticsPlus.Eship.WebApplication.Utilities.ScheduledTasks
{
    public class FedExTracker : IScheduledTask
    {
        private const int CallNotesLengthLimit = 490;
        private const int PickupDateRange = 3;

        private const string Ellipses = "...";
        private const string PickedUpStatus = "PU";
        private const string PoNumber = "PURCHASE_ORDER";
        private const string SuccessCode = "0";
        private const string ShipperReference = "SHIPPER_REFERENCE";
        

        private readonly string _errorLogFilesPath;

        public FedExTracker(string errorLogFilesPath)
        {
            _errorLogFilesPath = errorLogFilesPath;
        }

        /// <summary>
        /// Initializes the scheduled task.
        /// </summary>
        public void Init()
        {
        }

	    /// <summary>
	    /// Starts the scheduled task.
	    /// </summary>
	    public void Run()
	    {
		    var shipments = new ShipmentSearch().FetchSmallPackShipmentsForTracking(SmallPackageEngine.FedEx);

		    if (!shipments.Any()) return;

		    var shipmentGroup = shipments
			    .GroupBy(s => s.TenantId)
			    .ToDictionary(g => g.Key, g => g.Select(s => s).ToList())
			    .Select(i => new
				    {
					    Plugin = new FedExPluginHandler(ProcessorVars.IntegrationVariables[i.Key].FedExParameters, null, null),
					    Shipments = i.Value
				    })
				.SelectMany(i => i.Shipments.Select(x=>new {Shipment = x, i.Plugin}))
			    .ToList();

		    foreach (var group in shipmentGroup)
		    {
			    var shipment = group.Shipment;
			    var fedexPlugin = group.Plugin;

			    var primaryVendor = shipment.Vendors != null ? shipment.Vendors.FirstOrDefault(v => v.Primary) : null;
			    var trackingNumber = primaryVendor != null ? primaryVendor.ProNumber : string.Empty;

			    if (string.IsNullOrEmpty(trackingNumber)) continue;

			    var pickupDate = shipment.ActualPickupDate > DateUtility.SystemEarliestDateTime
				                     ? shipment.ActualPickupDate
				                     : shipment.DesiredPickupDate;

			    var trackingDetails = fedexPlugin
				    .GetTrackingInformation(trackingNumber, pickupDate, PickupDateRange)
				    .Where(td => td.Notification.Code == SuccessCode)
				    .ToList();

			    if (!trackingDetails.Any()) continue;

			    var trackingDetail = trackingDetails.Count() > 1
				                         ? GetBestMatch(trackingDetails, shipment)
				                         : trackingDetails.First();

			    ProcessTrackingDataForShipment(shipment, trackingDetail);
		    }
	    }

	    /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
        }


        private void ProcessTrackingDataForShipment(Shipment shipment, FedExTrackDetail trackingDetail)
        {
            shipment.LoadCollections();
            shipment.TakeSnapShot();

            //get dates and change the shipment's status if relevant
            var pickupEvent = trackingDetail.Events.FirstOrDefault(e => e.EventType == PickedUpStatus);
            if (pickupEvent != null)
                shipment.ActualPickupDate = pickupEvent.TimeStamp;
            

            if (trackingDetail.ActualDeliveryTimestamp > DateUtility.SystemEarliestDateTime && trackingDetail.ActualDeliveryTimestampSpecified)
            {
                if (trackingDetail.ShipTimestamp > DateUtility.SystemEarliestDateTime && trackingDetail.ShipTimestampSpecified)
                    shipment.ActualPickupDate = trackingDetail.ShipTimestamp;

                shipment.ActualDeliveryDate = trackingDetail.ActualDeliveryTimestamp;
            }
                
            if (trackingDetail.EstimatedDeliveryTimestamp > DateUtility.SystemEarliestDateTime && trackingDetail.EstimatedDeliveryTimestampSpecified)
                shipment.EstimatedDeliveryDate = trackingDetail.EstimatedDeliveryTimestamp;
            
            shipment.Status = shipment.InferShipmentStatus(false);

            //translate events into check calls
            var checkCalls = new List<CheckCall>();
            foreach (var shipmentEvent in trackingDetail.Events)
            {
                var callNotes = shipmentEvent.EventDescription;

                if(!string.IsNullOrEmpty(shipmentEvent.EventType)) callNotes += string.Format(" [Event Type: {0}]", shipmentEvent.EventType);

                if(!string.IsNullOrEmpty(shipmentEvent.Address.GetFormattedAddress()))
                    callNotes += string.Format(" [Address: {0}]", shipmentEvent.Address.GetFormattedAddress());
                
                if (!string.IsNullOrEmpty(shipmentEvent.ArrivalLocationType))
                    callNotes += string.Format(" [Arrival Location Type: {0}]", shipmentEvent.ArrivalLocationType);

                if(!string.IsNullOrEmpty(shipmentEvent.StatusExceptionCode))
                    callNotes += string.Format(" [Status Exception Code: {0}]", shipmentEvent.StatusExceptionCode);

                if(!string.IsNullOrEmpty(shipmentEvent.StatusExceptionDescription))
                    callNotes += string.Format(" [Status Exception Description: {0}]", shipmentEvent.StatusExceptionDescription);

                //Check for CheckCall call notes longer than 500 characters and make additional CheckCall to place remainder of notes in
                var callNotesSpillover = string.Empty;

                if (callNotes.Length >= CallNotesLengthLimit)
                {
                    var lastSpaceIndex = callNotes.Substring(0, CallNotesLengthLimit).LastIndexOf(" ", StringComparison.Ordinal);
                    callNotesSpillover = Ellipses + callNotes.Substring(lastSpaceIndex, callNotes.Length - lastSpaceIndex - 1);
                    callNotes = callNotes.Substring(0, lastSpaceIndex) + Ellipses;
                }

                var checkCall = new CheckCall
                {
                    CallDate = DateTime.Now,
                    CallNotes = callNotes,
                    EventDate = shipmentEvent.TimeStamp,
                    EdiStatusCode = string.Empty,
                    Shipment = shipment,
                    TenantId = shipment.TenantId,
                    User = shipment.Tenant.DefaultSystemUser 
                };

                if (!string.IsNullOrEmpty(callNotesSpillover))
                {
                    checkCalls.Add(new CheckCall
                    {
                        CallDate = DateTime.Now,
                        CallNotes = callNotesSpillover,
                        EventDate = shipmentEvent.TimeStamp,
                        EdiStatusCode = string.Empty,
                        Shipment = shipment,
                        TenantId = shipment.TenantId,
                        User = shipment.Tenant.DefaultSystemUser
                    });
                }

                checkCalls.Add(checkCall);
            }

            shipment.CheckCalls.AddRange(checkCalls.Where(cc => !shipment.CheckCalls.Any(scc => cc.EventDate == scc.EventDate && cc.CallNotes == scc.CallNotes)));

            //save the shipment
            Exception ex;
            new ShipmentUpdateHandler().UpdateShipmentTrackingInfo(shipment, out ex);
            if (ex != null) ErrorLogger.LogError(ex, _errorLogFilesPath);
        }

        private static FedExTrackDetail GetBestMatch(IEnumerable<FedExTrackDetail> trackingDetails, Shipment shipment)
        {
            var trackingDetail = new FedExTrackDetail();
            var highestMatchCount = default(int);
            var closestDateDistance = new TimeSpan();

            //whichever detail with the most common identifiers and closest date to the shipment timestamp is picked
            foreach (var detail in trackingDetails)
            {
                var matchCount = default(int);

                var shipperRefId = detail.OtherIdentifiers.FirstOrDefault(oi => oi.PackageIdentifier.IdentifierType == ShipperReference);
                if (shipperRefId != null && shipperRefId.PackageIdentifier.IdentifierValue == shipment.ShipperReference)
                    matchCount++;

                var poNumberId = detail.OtherIdentifiers.FirstOrDefault(oi => oi.PackageIdentifier.IdentifierType == PoNumber);
                if (poNumberId != null && poNumberId.PackageIdentifier.IdentifierValue == shipment.PurchaseOrderNumber)
                    matchCount++;

                if (matchCount >= highestMatchCount)
                {
                    if (matchCount > highestMatchCount)
                    {
                        highestMatchCount = matchCount;
                        closestDateDistance = new TimeSpan(); //reset closest date distance if we have a detail that matches more than others on po number and shipper ref
                    } 
                    
                    var dateDistance = (shipment.ActualPickupDate > DateUtility.SystemEarliestDateTime
                                                 ? shipment.ActualPickupDate
                                                 : shipment.DesiredPickupDate) - detail.ShipTimestamp;

                    if (dateDistance.Duration() < closestDateDistance || closestDateDistance == new TimeSpan())
                    {
                        closestDateDistance = dateDistance.Duration();
                        trackingDetail = detail;
                    }
                }
            }

            return trackingDetail;
        }
    }
}