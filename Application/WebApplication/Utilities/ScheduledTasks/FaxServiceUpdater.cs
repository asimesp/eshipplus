﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Web;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Fax;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches.Connect;

namespace LogisticsPlus.Eship.WebApplication.Utilities.ScheduledTasks
{
    public class FaxServiceUpdater
    {
        private int _errorCounter = 100;

        private readonly HttpServerUtility _server;

        private bool _shutdownThread;
        private Thread _updaterThread;
        private readonly FaxHandler _serviceHandler;
        private readonly ShipmentFaxTransmissionHandler _updateHandler;

        public bool IsRunning
        {
            get { return _updaterThread != null; }
        }

        public string ErrorsFolder
        {
            get { return _server.MapPath(WebApplicationSettings.ErrorsFolder); }
        }

        public FaxServiceUpdater(HttpServerUtility server, FaxHandler serviceHandler)
        {
            _server = server;
            _serviceHandler = serviceHandler;
            _updateHandler = new ShipmentFaxTransmissionHandler();
        }

        ~FaxServiceUpdater()
        {
            Shutdown();
        }


        public void Start()
        {
            try
            {
                if (_updaterThread == null)
                {
                    // start the scheduling thread
                    _updaterThread = new Thread(ManagerThread);
                    _updaterThread.Start();
                }
            }
            catch (Exception e)
            {
                ErrorLogger.LogError(e, ErrorsFolder);
            }
        }

        public void Shutdown()
        {
            if (_updaterThread != null)
            {
                _shutdownThread = true;

                // wait for the scheduler thread to shutdown for up to 60 sec
                // if the thread does not shutdown on time, try to kill it
                if (!_updaterThread.Join(60000))
                {
                    try
                    {
                        _updaterThread.Abort();
                    }
                    catch (Exception e)
                    {
                        ErrorLogger.LogError(e, ErrorsFolder);
                    }
                }

                _updaterThread = null;
            }
        }

        private void ManagerThread()
        {
            // delay before starting the thread
            Thread.Sleep(250);

            // init some state variables
            var counter = 600;

            if (_shutdownThread) return;
            var search = new FaxTransmissionSearch();

            do
            {
                // each actual thread iteration occurs in 2.5 minute intervals
                // however to make the tread more responsive it will sleep 600 x 250ms
                if (counter != 0)
                {
                    Thread.Sleep(250);
                    counter--;
                    continue;
                }

                // if 100 errors, shutdown and return
                if (_errorCounter <= 0)
                {
                    Shutdown();
                    return;
                }

                // reset the counter
                counter = 600;

                try
                {
                    var faxTransmissions = search.FetchAllFaxTransmissionWithNoResponse();

                    // update transmissions
                    foreach (var faxTransmission in faxTransmissions)
                    {
                        switch (faxTransmission.Status)
                        {
                            case FaxTransmissionStatus.Pending:
                                faxTransmission.TakeSnapShot();
                                faxTransmission.LastStatusCheckDateTime = DateTime.Now;
                                Exception exception = null;

                                // if fax transmission has no external reference, cannot get status
                                var faxStatus = !string.IsNullOrEmpty(faxTransmission.ExternalReference)
                                                    ? _serviceHandler.RetrieveFaxStatus(out exception, faxTransmission.ExternalReference, faxTransmission.ServiceProvider)
                                                    : null;
                                if (exception != null)
                                {
                                    ErrorLogger.LogError(exception, ErrorsFolder);
                                    _errorCounter--;
                                    continue;
                                }

                                // not found and out of allowable interval
                                if ((faxStatus == null || faxStatus.SendResult == FaxTransmissionResult.Queued) && (DateTime.Now - faxTransmission.SendDateTime).TotalMinutes > WebApplicationSettings.FaxServiceFailAfterInterval)
                                {
                                    faxTransmission.Status = FaxTransmissionStatus.Failed;
                                    faxTransmission.Message = string.Format("No Response In Allowed Interval of {0} minutes", WebApplicationSettings.FaxServiceFailAfterInterval);
                                    faxTransmission.ResponseDateTime = DateTime.Now;
                                }
                                else if (faxStatus != null)
                                {
                                    switch (faxStatus.SendResult)
                                    {
                                        case FaxTransmissionResult.SendingFailed:
                                            faxTransmission.Status = FaxTransmissionStatus.Failed;
                                            faxTransmission.Message = string.Format("Fax Failed! Fax #:{0}", faxStatus.FaxNumber);
                                            faxTransmission.ResponseDateTime = DateTime.Now;
                                            break;
                                        case FaxTransmissionResult.Sent:
                                            faxTransmission.Status = FaxTransmissionStatus.Success;
                                            faxTransmission.Message = string.Format("Fax Sent Successfully! Fax #:{0}", faxStatus.FaxNumber);
                                            faxTransmission.ResponseDateTime = DateTime.Now;
                                            break;
                                        case FaxTransmissionResult.Queued:
                                            faxTransmission.Message = string.Format("Fax Queued Successfully! Fax #:{0}", faxStatus.FaxNumber);
                                            break;
                                    }
                                }

                                // hold thread for 5 seconds to ensure we don't hit limit of request rate at Ring Central
                                Thread.Sleep(5000);
                                break;

                            case FaxTransmissionStatus.Queued:
                                faxTransmission.TakeSnapShot();
                                var faxWrapper = faxTransmission.FaxWrapper.FromXml<FaxWrapper>();

                                faxTransmission.ExternalReference = _serviceHandler.Send(faxWrapper, faxTransmission.ServiceProvider);

								// if we're using RingCentral and got an external reference back, fax was posted
								// if we're using Interfax and the external reference is not negitive, the fax was posted
		                        switch (faxTransmission.ServiceProvider)
		                        {
									case ServiceProvider.RingCentral:

				                        if (string.IsNullOrEmpty(faxTransmission.ExternalReference))
				                        {
					                        faxTransmission.Message = _serviceHandler.LastMessage;
					                        faxTransmission.Status = FaxTransmissionStatus.Failed;
				                        }
										else
										{
											faxTransmission.Status = FaxTransmissionStatus.Pending;
										}
				                        
										break;

									case ServiceProvider.Interfax:

				                        if(faxTransmission.ExternalReference.ToInt() < 0)
				                        {
					                        faxTransmission.Message = _serviceHandler.LastMessage;
											faxTransmission.Status = FaxTransmissionStatus.Failed;
				                        }
										else
										{
											faxTransmission.Status = FaxTransmissionStatus.Pending;
										}
										break;

		                        }
                                // hold thread for 10 seconds to ensure we don't hit limit of request rate at Ring Central
                                Thread.Sleep(10000);
                                break;
                        }

                        // save updates to fax transmission object
                        var msgs = _updateHandler.SaveFaxTransmission(faxTransmission);
                        if (!msgs.Any()) continue;

                        var err = string.Format("Error saving fax transmission. [Err: {0}]", msgs.Select(em => em.Message).ToArray().NewLineJoin());
                        ErrorLogger.LogError(new Exception(err), ErrorsFolder);
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("eShipPlus:FaxServiceUpdater : " + ex.Message);
                    ErrorLogger.LogError(ex, ErrorsFolder);
                }
            } while (!_shutdownThread);
        }

        public void Dispose()
        {

        }
    }
}