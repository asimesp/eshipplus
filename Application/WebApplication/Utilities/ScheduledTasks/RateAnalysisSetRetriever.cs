﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.WebApplication.Utilities.ScheduledTasks
{
	public class RateAnalysisSetRetriever : EntityBase, IScheduledTask
	{
		/// <summary>
        /// Initializes the scheduled task.
        /// </summary>
        public void Init()
        {
        }

        /// <summary>
        /// Starts the scheduled task.
        /// </summary>
		public void Run()
        {
        	var tenants = new TenantSearch().FetchTenants(new List<ParameterColumn>());
        	var checkDate = DateTime.Now;

        	var tenantsToCheck =
        		tenants.Where(tenant => checkDate.SetTime(tenant.BatchRatingAnalysisStartTime) <= checkDate &&
        		                        checkDate <= checkDate.SetTime(tenant.BatchRatingAnalysisEndTime)).ToList();
        	var search = new BatchRateDataSearch();
			foreach (var ids in tenantsToCheck.Select(tenant => search.FetchIncompleteBatchRateDataSetIds(tenant.Id)))
				RateAnalysisProcessor.Enqueue(ids);
        	
        }

	}
}
