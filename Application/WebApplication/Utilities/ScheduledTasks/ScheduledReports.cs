﻿using System;
using System.Linq;
using System.Threading;
using System.Web;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Reports;
using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.WebApplication.Utilities.Que;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.WebApplication.Utilities.ScheduledTasks
{
	public class ScheduledReports : IScheduledTask
	{
		private readonly HttpServerUtility _server;

		public void Init()
		{
			
		}

		public void Run()
		{
			new Thread(new ReportTask(_server).Run).Start();
		}

		public ScheduledReports(HttpServerUtility server)
		{
			_server = server;
		}

		public void Dispose()
		{
			
		}

		// internal class to handler report runs.
		internal class ReportTask : EntityBase
		{
			private ReportEngine _engine;
			private readonly HttpServerUtility _server;

			public ReportTask(HttpServerUtility server)
			{
				_server = server;
			}

			public void Run()
			{
				try
				{
					_engine = new ReportEngine();
				    var schedules = new ReportScheduleSearch().FetchReportScheduleForRun(DateTime.Now);
                    ReportQueue.AppendToListAndClear(schedules);
				    
					foreach (var schedule in schedules.Where(s => s.Notify || s.FtpEnabled))
						try
						{
							var data = _engine.GenerateReport(schedule.ReportConfiguration, schedule.User, schedule.CustomerGroupId,
							                                  schedule.VendorGroupId);

							// setup to send report data!
							string name = string.Empty;

							try
							{
								name = !string.IsNullOrEmpty(schedule.DeliveryFilename) ? string.Format("{0:" + schedule.DeliveryFilename + "}", DateTime.Now) : string.Empty;
							}
							catch(Exception exn)
							{
								ErrorLogger.LogError(exn,  _server.MapPath(WebApplicationSettings.ErrorsFolder));
							}

							if (string.IsNullOrEmpty(name))
								name = data.HasTable
									       ? data.Table.TableName + (data.Table.Rows != null && data.Table.Rows.Count > 0 ? string.Empty : " - No Data Rows")
									       : "eShipPlus_TMS_Report - No Data Table";

							var customization = schedule.ReportConfiguration.SerializedCustomization.FromXml<ReportCustomization>();

                            System.IO.FileInfo info;

                            switch (schedule.Extension)
                            {
                                case ReportExportExtension.txt:
                                    info = data.TransformTabDelimited(this._server);
                                    break;
                                case ReportExportExtension.xlsx:
                                    info = data.TransformExcel(this._server, customization.DataColumns, customization.Charts);
                                    break;
                                case ReportExportExtension.csv:
                                    info = data.TransformCsv(this._server);
                                    break;
                                default:
                                    info = data.TransformCsv(this._server);
                                    break;
                            }

						    // email
						    if (schedule.Notify)
						    {
						        // if no table (or no rows) and SuppressNotificationForEmptyReport is true, don't send an email notification
						        if ((data.HasTable && data.Table.Rows != null && data.Table.Rows.Count > 0) ||
						            (!data.HasTable && !schedule.SuppressNotificationForEmptyReport) ||
						            (data.HasTable && data.Table.Rows == null && !schedule.SuppressNotificationForEmptyReport) ||
						            (data.HasTable && data.Table.Rows != null && data.Table.Rows.Count == 0 && !schedule.SuppressNotificationForEmptyReport))
						        {
						            _server.NotifyReadyReport(info, name, schedule);
						        }
						    }

							// ftp
							AuditLog ftpAudit = null;
							if (schedule.FtpEnabled)
							{
								ftpAudit = new AuditLog
									{
										Description = schedule.SendFtp(string.Format("{0}.{1}", name, schedule.Extension), info)
											              ? "Successfully sent report via ftp"
											              : "Sending report via ftp failed",
										TenantId = schedule.User.TenantId,
										User = schedule.User,
										EntityCode = schedule.EntityName(),
										EntityId = schedule.Id.ToString(),
										LogDateTime = DateTime.Now
									};
							}

							// update last run times.
							Connection = DatabaseConnection.DefaultConnection;
							BeginTransaction();
							try
							{
								if (ftpAudit != null)
								{
									ftpAudit.Connection = Connection;
									ftpAudit.Transaction = Transaction;
									ftpAudit.Log();
								}

								schedule.Connection = Connection;
								schedule.Transaction = Transaction;

							    var lastRunDate = DateTime.Now;

								schedule.TakeSnapShot();
							    schedule.LastRun = lastRunDate;

								foreach (var change in schedule.Changes())
									new AuditLog
										{
											Connection = Connection,
											Transaction = Transaction,
											Description = change,
											TenantId = schedule.User.TenantId,
											User = schedule.User,
											EntityCode = schedule.EntityName(),
											EntityId = schedule.Id.ToString(),
											LogDateTime = DateTime.Now
										}.Log();
								schedule.Save();

							    var reportConfiguration = schedule.ReportConfiguration;
                                reportConfiguration.TakeSnapShot();
							    reportConfiguration.LastRun = lastRunDate;

                                // save run AuditLog for ReportConfiguration
                                new AuditLog
                                {
                                    Connection = Connection,
                                    Transaction = Transaction,
                                    Description = string.Format("Schedule Ran Report: '{0}'", reportConfiguration.Name),
                                    TenantId = schedule.TenantId,
                                    User = schedule.User,
                                    EntityCode = reportConfiguration.EntityName(),
                                    EntityId = reportConfiguration.Id.ToString(),
                                    LogDateTime = DateTime.Now
                                }.Log();

                                // log update for LastRun
                                foreach (var change in reportConfiguration.Changes())
                                    new AuditLog
                                    {
                                        Connection = Connection,
                                        Transaction = Transaction,
                                        Description = change,
                                        TenantId = schedule.User.TenantId,
                                        User = schedule.User,
                                        EntityCode = reportConfiguration.EntityName(),
                                        EntityId = reportConfiguration.Id.ToString(),
                                        LogDateTime = DateTime.Now
                                    }.Log();
                                reportConfiguration.Save();

								CommitTransaction();
							}
							catch (Exception e1)
							{
								RollBackTransaction();
								ErrorLogger.LogError(e1, _server.MapPath(WebApplicationSettings.ErrorsFolder));
							}
						}
						catch (Exception e2)
						{
							ErrorLogger.LogError(e2, _server.MapPath(WebApplicationSettings.ErrorsFolder));
						}
				}
				catch (Exception e3)
				{
					ErrorLogger.LogError(e3, _server.MapPath(WebApplicationSettings.ErrorsFolder));
				}
			}
		}
	}
}
