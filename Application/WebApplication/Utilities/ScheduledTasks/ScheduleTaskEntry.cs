﻿using System;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor;

namespace LogisticsPlus.Eship.WebApplication.Utilities.ScheduledTasks
{
	public class ScheduleTaskEntry
	{
		private readonly ScheduleTaskType _scheduleTaskType;
		private readonly int _interval;
		private readonly int _hour;
		private readonly int _minute;
		private readonly int _dayOfMonth;
		private readonly DayOfWeek _dayOfWeek;

		private readonly string _name;
		private readonly IScheduledTask _task;

		public string Name { get { return _name; } }
		public IScheduledTask Task { get { return _task; } }
		public DateTime NextRunTime { get; private set; }


		public ScheduleTaskEntry(string taskName, IScheduledTask task, ScheduleTaskType scheduleTaskType, int interval,
		                         int hour = 0, int minute = 0, int dayOfMonth = 1, DayOfWeek dayOfWeek = DayOfWeek.Sunday)
		{
			_name = taskName;
			_task = task;
			_scheduleTaskType = scheduleTaskType;
			_interval = interval;
			_hour = hour;
			_minute = minute;
			_dayOfMonth = dayOfMonth;
			_dayOfWeek = dayOfWeek;

			// properly seed the next run time!
			var time = string.Format("{0:00}:{1:00}", _hour, _minute);

			// this satisfy daily type
			NextRunTime = DateTime.Now.SetTime(time) <= DateTime.Now
				              ? DateTime.Now.AddDays(1).SetTime(time)
				              : DateTime.Now.SetTime(time);
			switch (_scheduleTaskType)
			{
				case ScheduleTaskType.Interval:
					NextRunTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, 0, 0);
                    while (NextRunTime <= DateTime.Now) NextRunTime = NextRunTime.AddMinutes(_interval);
					break;
				case ScheduleTaskType.Weekly:
					while (NextRunTime.DayOfWeek.ToInt() != _dayOfWeek.ToInt()) NextRunTime = NextRunTime.AddDays(1);
					break;
				case ScheduleTaskType.Monthly:
					var daysInMonth = DateTime.DaysInMonth(NextRunTime.Year, NextRunTime.Month);
					NextRunTime = new DateTime(NextRunTime.Year, NextRunTime.Month, daysInMonth < _dayOfMonth ? daysInMonth : _dayOfMonth, hour, minute, 0);
					while (NextRunTime <= DateTime.Now) NextRunTime = NextRunTime.AddMonths(1).SetTime(time);
					
                    daysInMonth = DateTime.DaysInMonth(NextRunTime.Year, NextRunTime.Month);
					while (NextRunTime.Day != _dayOfMonth && NextRunTime.Day < daysInMonth) NextRunTime = NextRunTime.AddDays(1);
					break;
			}
		}

		public bool HasTimeElapsed()
		{
			return NextRunTime <= DateTime.Now;
		}

		public DateTime Reschedule()
		{
			var time = string.Format("{0:00}:{1:00}", _hour, _minute);
			switch (_scheduleTaskType)
			{
				case ScheduleTaskType.Interval:
					NextRunTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, 0, 0);
                    while (NextRunTime <= DateTime.Now) NextRunTime = NextRunTime.AddMinutes(_interval);
					break;

				case ScheduleTaskType.Daily:
					while (NextRunTime <= DateTime.Now) NextRunTime = NextRunTime.AddDays(1).SetTime(time);
					break;

				case ScheduleTaskType.Weekly:
					while (NextRunTime <= DateTime.Now) NextRunTime = NextRunTime.AddDays(7).SetTime(time);
					break;

				case ScheduleTaskType.Monthly:
					// we need to make sure that the task will run if scheduled 31th and today is Feb 28
					while (NextRunTime <= DateTime.Now) NextRunTime = NextRunTime.AddMonths(1).SetTime(time);
					var daysInMonth = DateTime.DaysInMonth(NextRunTime.Year, NextRunTime.Month);
					while (NextRunTime.Day != _dayOfMonth && NextRunTime.Day < daysInMonth) NextRunTime = NextRunTime.AddDays(1);
					break;
			}

			return NextRunTime;
		}
	}
}
