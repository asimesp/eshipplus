﻿namespace LogisticsPlus.Eship.WebApplication.Utilities.ScheduledTasks
{
    /// <summary>
    /// Defines the type of a scheduled task
    /// </summary>
    public enum ScheduleTaskType
    {
        /// <summary>The task will be runned on a minute base interval (every 15 min, etc).</summary>
        Interval,

        /// <summary>The task will be runned once a day.</summary>
        Daily,

        /// <summary>The taks will be runned on the specified week day.</summary>
        Weekly,

        /// <summary>The taks will be runned once a month.</summary>
        Monthly
    }
}
