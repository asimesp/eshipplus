﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Reports;
using LogisticsPlus.Eship.WebApplication.Documents;
using LogisticsPlus.Eship.WebApplication.Notifications;
using Newtonsoft.Json;

namespace LogisticsPlus.Eship.WebApplication.Utilities.ScheduledTasks
{
    public class BackgroundReportRunProcessor
    {
        private const int MaxChildrenThreads = 2;

        private bool _shutdownThread;
        private Thread _managerThread;
        private List<Thread> _childrenThreads;
        private List<long> _reportIdsBeingProcessed;

        private readonly HttpServerUtility _server;
        private static readonly object LockObject = new object();



        public bool IsRunning
        {
            get { return _managerThread != null; }
        }

        private string ErrorsFolder { get; set; }

        public BackgroundReportRunProcessor(string errorsFolder, HttpServerUtility server)
        {
            ErrorsFolder = errorsFolder;
            _server = server;
        }

        ~BackgroundReportRunProcessor()
        {
            Shutdown();
        }

        public void Start()
        {
            try
            {
                if (_managerThread != null) return;

                _childrenThreads = new List<Thread>();
                _reportIdsBeingProcessed = new List<long>();

                // start the scheduling thread
                _managerThread = new Thread(ManagerThread);
                _managerThread.Start();
            }
            catch (Exception e)
            {
                ErrorLogger.LogError(e, ErrorsFolder);
            }
        }

        public void Shutdown()
        {
            if (_managerThread == null) return;

            _shutdownThread = true;

            // wait to kill child threads
            var w = new Stopwatch();
            w.Start();
            do
            {
                // loop for 35 seconds max or until all children threads are killed off!
            } while (_childrenThreads.Any(t => t.IsAlive) && w.Elapsed.Seconds <= 35);
            w.Stop();

            // wait for the scheduler thread to shutdown for up to 60 sec
            // if the thread does not shutdown on time, try to kill it
            if (!_managerThread.Join(60000))
            {
                try
                {
                    _managerThread.Abort();
                }
                catch (Exception e)
                {
                    ErrorLogger.LogError(e, ErrorsFolder);
                }
            }

            _managerThread = null;
        }

        private void ManagerThread()
        {
            // delay before starting the thread
            Thread.Sleep(250);

            // init some state variables
            var counter = 40;

            if (_shutdownThread) return;

            do
            {
                // however to make the tread more responsive it will sleep 40 x 250ms
                if (counter != 0)
                {
                    Thread.Sleep(250);
                    counter--;
                    continue;
                }

                // reset the counter
                counter = 40;

                try
                {
                    // maximum threads hit and all running
                    if (_childrenThreads.Count == MaxChildrenThreads && _childrenThreads.All(t => t.IsAlive))
                        continue;

                    // clean up dead/complete/abort threads
                    if (_childrenThreads.Any(t => !t.IsAlive))
                    {
                        _childrenThreads.RemoveAll(t => !t.IsAlive);
                    }

                    // Gradually force the queue to grab new records from the database
                    if (BackgroundReportQueue.Count == 0)
                    {
                        BackgroundReportQueue.Dequeue(true);
                        continue;
                    }


                    // spin off new threads to do work
                    for (var i = _childrenThreads.Count; i < MaxChildrenThreads; i++)
                    {
                        if (BackgroundReportQueue.Count == 0) break; // if no background reports to run, break out of loop

                        var t = new Thread(ProcessReportsToRun);
                        _childrenThreads.Add(t);
                        t.Start();

                        Thread.Sleep(100); // sleep for short period to allow thread kick off and deque report
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("eShip Plus Background Report Run Processor : " + ex.Message);
                    ErrorLogger.LogError(ex, ErrorsFolder);
                }
            } while (!_shutdownThread);

            // if we are here, we have broken out of about look because we are shutting down!
            foreach (var t in _childrenThreads.Where(t => !t.Join(2500)))
            {
                try
                {
                    t.Abort();
                }
                catch (Exception e)
                {
                    ErrorLogger.LogError(e, ErrorsFolder);
                }
            }
        }

        private void ProcessReportsToRun()
        {
            var backgroundReportManager = new BackgroundReportRunRecordManager();
            var reportId = default(long);

            do
            {
                try
                {
                    var report = BackgroundReportQueue.Dequeue();

                    if (report == null)
                        continue;

                    lock (LockObject)
                    {
                        // ensure that this report isn't being processed by another thread
                        if (_reportIdsBeingProcessed.Contains(report.Id))
                            continue;

                        _reportIdsBeingProcessed.Add(reportId);
                    }

                    reportId = report.Id;

                    var engine = new ReportEngine();

                    // reload load to refresh and validate that it hasn't already been processed by another thread or deleted
                    report.Reload(true);
                    if (report.ExpirationDate != DateUtility.SystemEarliestDateTime || !report.KeyLoaded) continue;

                    var customization = report.ReportCustomization.FromXml<ReportCustomization>();
                    var data = engine.GenerateReport(customization, report.Query, JsonConvert.DeserializeObject<Dictionary<string, object>>(report.Parameters), report.ReportName,
                        report.User);

                    var fileName = string.Format("{0}.{1}", Guid.NewGuid().GetString(), ReportExportExtension.xlsx.GetString());
                    var physicalFilePath = _server.MapPath(string.Format("{0}{1}",
                        WebApplicationSettings.BackgroundReportRunFolder, fileName));

                    data.TransformExcel(_server, customization.DataColumns, customization.Charts, physicalFilePath);

                    Exception ex;
                    report.ExpirationDate = DateTime.Now.AddHours(WebApplicationSettings.DefaultBackgroundReportRunExpirationHours);
                    report.CompletedReportFileName = fileName;
                    backgroundReportManager.SaveBackgroundReport(report, out ex);

                    if (ex != null)
                    {
                        ErrorLogger.LogError(ex, ErrorsFolder);
                        lock (LockObject)
                        {
                            _reportIdsBeingProcessed.Remove(reportId);
                        }
                        continue;
                    }

                    var body = _server.GenerateReportReadyNotification(report.ReportName, false, true);

                    var emails = new List<string> { report.User.Email };

                    var subject = $"eShip Plus Ready Report - {report.ReportName}";

                    emails.SendEmail(new string[0], ProcessorVars.MonitorEmails[report.TenantId], body, subject, report.TenantId); // monitor emails Bcc'ed.

                    lock (LockObject)
                    {
                        _reportIdsBeingProcessed.Remove(reportId);
                    }
                }
                catch (Exception ex)
                {
                    ErrorLogger.LogError(ex, ErrorsFolder);

                    lock (LockObject)
                    {
                        _reportIdsBeingProcessed.Remove(reportId);

                        // write error message to file and save against background report run
                        var fileName = $"ERR-{Guid.NewGuid().GetString()}.{ReportExportExtension.txt.GetString()}";
                        var physicalFilePath = _server.MapPath($"{WebApplicationSettings.BackgroundReportRunFolder}{fileName}");
                        File.WriteAllText(physicalFilePath, ex.ToString());

                        var report = new BackgroundReportRunRecord(reportId);
                        if (report.KeyLoaded)
                        {
                            report.ExpirationDate = DateTime.Now.AddHours(WebApplicationSettings.DefaultBackgroundReportRunExpirationHours);
                            report.CompletedReportFileName = fileName;
                            backgroundReportManager.SaveBackgroundReport(report, out var ex1);

                            if (ex1 != null)
                            {
                                ErrorLogger.LogError(ex1, ErrorsFolder);
                            }
                        }

                        // INTENTIONALLY DO NOT NOTIFY COMPLETION FOR AN ERROR
                    }
                }
            } while (BackgroundReportQueue.Count > 0);

        }
    }
}