﻿using System;
using System.Threading;
using System.Web;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.WebApplication.Notifications;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.WebApplication.Utilities.ScheduledTasks
{
    public class ClaimNoteReminder : EntityBase, IScheduledTask
    {
        private readonly HttpServerUtility _server;

        public void Init()
        {

        }

        public void Run()
        {
            new Thread(SendReminderEmails).Start();
        }

        public void SendReminderEmails()
        {
            try
            {
                var claimNotes = new ClaimSearch().FetchClaimNoteRemindersReadyForRun();

                foreach (var note in claimNotes)
                {
                    // email
                    _server.NotifyClaimNoteReminder(note);

                    // disable SendReminder
                    Connection = DatabaseConnection.DefaultConnection;
                    BeginTransaction();

                    note.Connection = Connection;
                    note.Transaction = Transaction;

                    note.TakeSnapShot();

                    note.SendReminder = false;

                    foreach (var change in note.Changes())
                        new AuditLog
                        {
                            Connection = Connection,
                            Transaction = Transaction,
                            Description = change,
                            TenantId = note.TenantId,
                            User = note.Tenant.DefaultSystemUser,
                            EntityCode = note.EntityName(),
                            EntityId = note.Id.ToString(),
                            LogDateTime = DateTime.Now
                        }.Log();

                    note.Save();

                    CommitTransaction();
                }
            }
            catch (Exception err)
            {
                RollBackTransaction();
                ErrorLogger.LogError(err, _server.MapPath(WebApplicationSettings.ErrorsFolder));
            }
        }

        public ClaimNoteReminder(HttpServerUtility server)
        {
            _server = server;
        }

        public new void Dispose()
        {

        }
    }
}
