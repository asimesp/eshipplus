﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Threading;

namespace LogisticsPlus.Eship.WebApplication.Utilities.ScheduledTasks
{
	/// <summary>
	/// A simple task scheduler.
	/// </summary>
	public class Scheduler : IDisposable
	{
		// internal constants
		private const int KeepAliveInterval = 15; // minutes

		// internal fields
		private bool _shutdownThread;
		private readonly string _keepAliveUrl;
		private readonly bool _useWASSiteRoot;
		private string _errorsFolder = string.Empty;
		private Thread _schedulerThread;
		private readonly Dictionary<string, ScheduleTaskEntry> _tasks = new Dictionary<string, ScheduleTaskEntry>();

	    private string KeepAliveUrl
	    {
	        get
	        {
	            return string.IsNullOrEmpty(_keepAliveUrl) && _useWASSiteRoot
	                ? WebApplicationSettings.SiteRoot
	                : _keepAliveUrl;
	        }
	    }

		public bool IsRunning
		{
			get { return _schedulerThread != null; }
		}

		public string ErrorsFolder
		{
			get { return _errorsFolder; }
			set { _errorsFolder = value; }
		}

		public Scheduler()
		{
		}

		public Scheduler(string errorsFolder,  string keepAliveUrl = "", bool useWASSiteRoot = true)
		{
			_keepAliveUrl = keepAliveUrl;
			_errorsFolder = errorsFolder;
		    _useWASSiteRoot = useWASSiteRoot;
		}

		~Scheduler()
		{
			Shutdown();
		}

		/// <summary>
		/// Adds a interval scheduled task.
		/// </summary>
		/// <param name="taskName">The task name</param>
		/// <param name="task">The scheduled task.</param>
		/// <param name="interval">The interval in minutes at which to execute the task. Must be between 1 and 1440</param>
		public void AddIntervalTask(string taskName, IScheduledTask task, int interval)
		{
			if (interval < 1 || interval > (1440))
			{
				throw new ArgumentException("Invalid interval value. Must be in the range of 1 - 1440 (24 hours)");
			}

			AddTask(taskName, task, ScheduleTaskType.Interval, interval, 0, 0, 1);
		}

		/// <summary>
		/// Adds a daily scheduled task.
		/// </summary>
		/// <param name="taskName">The task name.</param>
		/// <param name="task">The scheduled task.</param>
		/// <param name="hour">The hour in which to run the task.</param>
		/// <param name="minute">The minute in which to run the task.</param>
		public void AddDailyTask(string taskName, IScheduledTask task, int hour, int minute)
		{
			AddTask(taskName, task, ScheduleTaskType.Daily, 0, hour, minute, 1);
		}

		/// <summary>
		/// Adds the weekly task.
		/// </summary>
		/// <param name="taskName">The task name.</param>
		/// <param name="task">The task.</param>
		/// <param name="hour">The hour in which to run the task.</param>
		/// <param name="minute">The minute in which to run the task.</param>
		/// <param name="dayOfWeek">Day of the week the task should run</param>
		public void AddWeeklyTask(string taskName, IScheduledTask task, int hour, int minute, DayOfWeek dayOfWeek)
		{
			AddTask(taskName, task, ScheduleTaskType.Weekly, 0, hour, minute, 1, dayOfWeek);
		}

		/// <summary>
		/// Adds the monthly task.
		/// </summary>
		/// <param name="taskName">The task name.</param>
		/// <param name="task">The task.</param>
		/// <param name="hour">The hour in which to run the task.</param>
		/// <param name="minute">The minute in which to run the task.</param>
		/// <param name="dayOfMonth">Between 1 and 31 </param>
		public void AddMonthlyTask(string taskName, IScheduledTask task, int hour, int minute, int dayOfMonth)
		{
			AddTask(taskName, task, ScheduleTaskType.Monthly, 0, hour, minute, dayOfMonth);
		}

		/// <summary>
		/// Removes a task
		/// </summary>
		/// <param name="taskName">The name of the task</param>
		public void RemoveTask(string taskName)
		{
			lock (_tasks)
			{
				_tasks.Remove(taskName);
			}
		}

		/// <summary>
		/// Starts the task scheduler
		/// </summary>
		public void Start()
		{
			try
			{
				if (_schedulerThread == null)
				{
					lock (_tasks)
					{
						// reschedule all tasks
						foreach (string key in _tasks.Keys)
							_tasks[key].Reschedule();

						// start the scheduling thread
						_schedulerThread = new Thread(SchedulerThread);
						_schedulerThread.Start();
					}
				}
			}
			catch (Exception e)
			{
				Debug.WriteLine(e.ToString());
			}
		}

		/// <summary>
		/// Makes HTTP request in attempt to keep the application alive
		/// </summary>
		public void PingServer()
		{
			if (string.IsNullOrEmpty(KeepAliveUrl)) return;
			try
			{
				var http = new WebClient();
				http.DownloadString(KeepAliveUrl);
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, _errorsFolder);
			}
		}

		/// <summary>
		/// Stops the task scheduler
		/// </summary>
		public void Shutdown()
		{
			if (_schedulerThread != null)
			{
				_shutdownThread = true;

				// wait for the scheduler thread to shutdown for up to 60 sec
				// if the thread does not shutdown on time, try to kill it
				if (!_schedulerThread.Join(60000))
				{
					try
					{
						_schedulerThread.Abort();
					}
					catch (Exception e)
					{
						ErrorLogger.LogError(e, _errorsFolder);
					}
				}

				_schedulerThread = null;
			}
		}

		/// <summary>
		/// Stops the task scheduler and disposes all tasks
		/// </summary>
		public void Dispose()
		{
			// shutdown the scheduler thread first
			Shutdown();

			// call Dispose() for each task
			lock (_tasks)
			{
				foreach (string key in _tasks.Keys)
				{
					try
					{
						_tasks[key].Task.Dispose();
					}
					catch (Exception e)
					{
						ErrorLogger.LogError(e, _errorsFolder);
					}
				}

				// clear the tasks list
				_tasks.Clear();
			}
		}

		/// <summary>
		/// Adds a task to the list of scheduled tasks.
		/// </summary>
		/// <param name="taskName">Name of the task.</param>
		/// <param name="task">A reference to the task.</param>
		/// <param name="schedType">Type of the scheduled task.</param>
		/// <param name="interval">The interval.</param>
		/// <param name="hour">The hour.</param>
		/// <param name="minute">The minute.</param>
		/// <param name="dayOfMonth">between 1 and 31</param>
		/// <param name="dayOfWeek">Day of the week the task should run (weekly task only)</param>
		private void AddTask(string taskName, IScheduledTask task, ScheduleTaskType schedType, int interval, int hour, int minute, int dayOfMonth, DayOfWeek dayOfWeek = DayOfWeek.Sunday)
		{
			if (schedType != ScheduleTaskType.Interval)
			{
				if (hour < 0 || hour > 23) throw new ArgumentException("Invalid hour value. Must be in the range of 0 - 23");
				if (minute < 0 || minute > 59) throw new ArgumentException("Invalid minute. Must be in the range of 0 - 59");
			}

			lock (_tasks)
			{
				ScheduleTaskEntry t;
				if (_tasks.TryGetValue(taskName, out t)) throw new ArgumentException("The specified task aready exists.");
				_tasks.Add(taskName, new ScheduleTaskEntry(taskName, task, schedType, interval, hour, minute, dayOfMonth, dayOfWeek));
			}
		}

		/// <summary>
		/// The scheduling thread
		/// </summary>
		private void SchedulerThread()
		{
			// delay before starting the thread
			Thread.Sleep(250);

			// init some state variables
			var dtKeepAlive = DateTime.Now;
			var counter = 4;

			while (!_shutdownThread)
			{
				// each actual thread iteration occurs in 1 minute intervals
				// however to make the tread more responsive it will sleep 4 x 250ms
				if (counter == 0)
				{
					// reset the counter
					counter = 4;

					// loop thru the tasks
					lock (_tasks)
					{
						foreach (var key in _tasks.Keys)
						{
							if (_tasks[key].HasTimeElapsed())
							{
								// run the task and make sure there 
								// are no uncaught exceptions
								try
								{
									var task = _tasks[key].Task;
									task.Init();
									task.Run();
								}
								catch (Exception ex)
								{
									Debug.WriteLine("eShipPlus:Scheduler : " + ex.Message);
									ErrorLogger.LogError(ex, ErrorsFolder);
								}

								// re-schedule the task
								_tasks[key].Reschedule();
							}

							// check if the shutdown was requested
							if (_shutdownThread) break;
						}
					}

					// make a HTTP request to the specified URL if the KeepAlive Interval has expired
					if (!string.IsNullOrEmpty(KeepAliveUrl) && dtKeepAlive < DateTime.Now)
					{
						PingServer();
						dtKeepAlive = DateTime.Now.AddMinutes(KeepAliveInterval);
					}
				}
				else
				{
					Thread.Sleep(250);
					counter--;
				}
			}
		}
	}
}
