﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using LogisticsPlus.Eship.CarrierIntegrations;
using LogisticsPlus.Eship.CarrierIntegrations.ImgRtrv;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Connect;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;

namespace LogisticsPlus.Eship.WebApplication.Utilities.ScheduledTasks
{
	public class ImgRtrvProcessor : IScheduledTask
	{
		private readonly ShipmentDocRtrvLogProcessHandler _handler;
		private readonly HttpServerUtility _server;
		private readonly string _errorsFolder;

		public ImgRtrvProcessor(HttpServerUtility server, string errorsFolder)
		{
			_handler = new ShipmentDocRtrvLogProcessHandler();
			_server = server;
			_errorsFolder = errorsFolder;
		}


		public void Init()
		{
			// Intentional Empty
		}

		public void Run()
		{
			var vcoms = new VendorSearch().FetchVendorCommunicationsWithImgRtrv();
			var logs = new ShipmentSearch().RetrieveNonExpiredShipmentDocRtrvLogs();

			foreach (var vcom in vcoms)
			{
				var vlogs = logs.Where(l => l.CommunicationId == vcom.Id).ToList();

				switch (vcom.CarrierIntegrationEngine.Replace(" ", string.Empty).ToEnum<CarrierEngine>())
				{
					case CarrierEngine.Estes:
						ProcessEstesLogs(vlogs, vcom);
						break;
                    case CarrierEngine.DaytonFreight:
				        ProcessDaytonFreightLogs(vlogs, vcom);
				        break;
				}

			}
		}

	    
	    public void Dispose()
		{
			// Intentional Empty
		}




		private void ProcessEstesLogs(IEnumerable<ShipmentDocRtrvLog> logs, VendorCommunication vcom)
		{
			try
			{
				var estes = new EstesImgRtrv(new CarrierAuth {Username = vcom.CarrierIntegrationUsername, Password = vcom.CarrierIntegrationPassword});

				var requests = logs
					.Select(l =>
						{
							var s = l.RetrieveCorrespondingShipment();						
							if (s == null) return null;
							return new
								{
									Shipment = s,
									Log = l,
									Criteria = new ImgRtrvCriteria
										{
											BolNumber = l.ShipmentNumber,
											ProNumber = l.ProNumber,
											UniqueRequestKey = s.Id.GetString(),
											ExcludeDocTypes = GetTypesToExclude(l),
										},
								};
						})
					.Where(i => i != null)
					.ToList();

				var docs = estes.GetImages(requests.Select(r => r.Criteria).ToList());

				foreach (var r in requests)
				{
					var shipment = r.Shipment;
					var log = r.Log;
					var imgRtrvDocs = docs.Where(d => d.UniqueRequestKey == shipment.Id.GetString()).ToList();

					var sdocs = ProcessRetreivedDocuments(imgRtrvDocs, shipment, log);

					// save 
					if (!sdocs.Any()) continue;

					Exception ex;
					_handler.UpdateImgRtrvLogAndShipmentDocument(log, sdocs, shipment.Tenant.DefaultSystemUser, out ex);

					// if failed save, attempt to delete files from server
					if (ex != null)
					{
						ErrorLogger.LogError(ex, _errorsFolder);
						_server.RemoveFiles(sdocs.Select(d => d.LocationPath).ToArray());
					}
				}
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, _errorsFolder);
			}
		}

        private void ProcessDaytonFreightLogs(IEnumerable<ShipmentDocRtrvLog> logs, VendorCommunication vcom)
        {
            try
            {
                var daytonFreight = new DaytonFreightImgRtrv(new CarrierAuth {Username = vcom.CarrierIntegrationUsername, Password = vcom.CarrierIntegrationPassword});
                var requests = logs
                    .Select(l =>
                    {
                        var s = l.RetrieveCorrespondingShipment();
                        if (s == null) return null;
                        return new
                        {
                            Shipment = s,
                            Log = l,
                            Criteria = new ImgRtrvCriteria
                            {
                                BolNumber = l.ShipmentNumber,
                                ProNumber = l.ProNumber,
                                UniqueRequestKey = s.Id.GetString(),
                                ExcludeDocTypes = GetTypesToExclude(l),
                            },
                        };
                    })
                    .Where(i => i != null)
                    .ToList();

                var docs = daytonFreight.GetImages(requests.Select(r => r.Criteria).ToList());

                foreach (var r in requests)
                {
                    var shipment = r.Shipment;
                    var log = r.Log;
                    var imgRtrvDocs = docs.Where(d => d.UniqueRequestKey == shipment.Id.GetString()).ToList();

                    var sdocs = ProcessRetreivedDocuments(imgRtrvDocs, shipment, log);

                    // save 
                    if (!sdocs.Any()) continue;

                    Exception ex;
                    _handler.UpdateImgRtrvLogAndShipmentDocument(log, sdocs, shipment.Tenant.DefaultSystemUser, out ex);

                    // if failed save, attempt to delete files from server
                    if (ex != null)
                    {
                        ErrorLogger.LogError(ex, _errorsFolder);
                        _server.RemoveFiles(sdocs.Select(d => d.LocationPath).ToArray());
                    }
                }
            }
            catch (Exception e)
            {
                ErrorLogger.LogError(e, _errorsFolder);
            }
        }




		private List<ShipmentDocument> ProcessRetreivedDocuments(IEnumerable<ImgRtrvDoc> imgRtrvDocs, Shipment shipment, ShipmentDocRtrvLog log)
		{
			var sdocs = new List<ShipmentDocument>();

			foreach (var doc in imgRtrvDocs)
			{
				// create shipment document
				var shipmentFolder = WebApplicationSettings.ShipmentFolder(shipment.TenantId, shipment.Id);
				var filePath = Path.Combine(shipmentFolder, doc.Filename);
				var fi = new FileInfo(_server.MakeUniqueFilename(filePath)); // ensure unique filename

				var documentTag = GetTag(doc, shipment.Tenant, log);
				if (documentTag == null) continue;

				var document = new ShipmentDocument
					{
						TenantId = shipment.TenantId,
						Shipment = shipment,
						Description = string.Format("{0} {1}", shipment.ShipmentNumber, documentTag.Code),
						DocumentTag = documentTag,
						IsInternal = doc.Type == ImgRtrvDocType.WNI,
						LocationPath = Path.Combine(shipmentFolder, fi.Name),
						Name = fi.Name,
					};


				// write to server and add to list for save
				doc.FileContent.WriteToFile(_server.MapPath(shipmentFolder), fi.Name, string.Empty);
				sdocs.Add(document);
			}

			return sdocs;
		}

		private static DocumentTag GetTag(ImgRtrvDoc doc, Tenant tenant, ShipmentDocRtrvLog log)
		{
			switch (doc.Type)
			{
				case ImgRtrvDocType.POD:
					if (log.PodDate == DateUtility.SystemEarliestDateTime) log.PodDate = DateTime.Now;
					return tenant.PodDocumentTag;
				case ImgRtrvDocType.BOL:
					if (log.BolDate == DateUtility.SystemEarliestDateTime) log.BolDate = DateTime.Now;
					return tenant.BolDocumentTag;
				case ImgRtrvDocType.WNI:
					if (log.WniDate == DateUtility.SystemEarliestDateTime) log.WniDate = DateTime.Now;
					return tenant.WniDocumentTag;
				default:
					return null;
			}
		}

		private static List<ImgRtrvDocType> GetTypesToExclude(ShipmentDocRtrvLog log)
		{
			var types = new List<ImgRtrvDocType>();

			if(log.PodDate != DateUtility.SystemEarliestDateTime) types.Add(ImgRtrvDocType.POD);
			if(log.BolDate != DateUtility.SystemEarliestDateTime) types.Add(ImgRtrvDocType.BOL);
			if(log.WniDate != DateUtility.SystemEarliestDateTime) types.Add(ImgRtrvDocType.WNI);

			return types;
		}
	}
}
