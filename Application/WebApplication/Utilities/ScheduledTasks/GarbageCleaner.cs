﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Handlers.Core;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Handlers.Rating;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation.BusinessIntelligence;

namespace LogisticsPlus.Eship.WebApplication.Utilities.ScheduledTasks
{
    public class GarbageCleaner : IScheduledTask
	{
    	private readonly string _errorLogFilesPath;
    	private readonly string _tempFolder;
	    private readonly string _applicationLogsFolder;
	    private readonly string _backgroundReportsFolder;

    	/// <summary>
		/// 
		/// </summary>
		public GarbageCleaner(string errorLogFilesPath, string tempFolder, string applicationLogsFolder, string backgroundReportsFolder)
		{
			_errorLogFilesPath = errorLogFilesPath;
    		_tempFolder = tempFolder;
    		_applicationLogsFolder = applicationLogsFolder;
		    _backgroundReportsFolder = backgroundReportsFolder;
        }

    	/// <summary>
        /// Initializes the scheduled task.
        /// </summary>
        public void Init()
        {
        }

        /// <summary>
        /// Starts the scheduled task.
        /// </summary>
        public void Run()
        {
	        CleanApplicationLogs();
	        CleanErrorFiles();
	        CleanTempFiles();
			PurgeExpiredLocks();
        	PurgeRateAnalysisDataReadyForDelete();
            PurgeExpiredQuotes();
            PurgeExpiredBackgroundReportRunRecords();
        }

    	/// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            // do any clean-up
        }

		private void CleanApplicationLogs()
		{
			if (!string.IsNullOrEmpty(_applicationLogsFolder) && Directory.Exists(_applicationLogsFolder))
			{
				var dt = DateTime.Now.AddDays(-1 * WebApplicationSettings.ErrorLogDeleteAfterDays);
				var files = Directory.GetFiles(_applicationLogsFolder, "*.*", SearchOption.TopDirectoryOnly);
				foreach (var file in (from filePath in files let fi = new FileInfo(filePath) where fi.CreationTime < dt select filePath))
					try
					{
						File.Delete(file);
					}
					catch
					{
						//fall through;
						;
					}
			}
		}

	    private void CleanErrorFiles()
	    {
		    if (!string.IsNullOrEmpty(_errorLogFilesPath) && Directory.Exists(_errorLogFilesPath))
		    {
			    var dt = DateTime.Now.AddDays(-1*WebApplicationSettings.ErrorLogDeleteAfterDays);
			    var files = Directory.GetFiles(_errorLogFilesPath, "*.*", SearchOption.TopDirectoryOnly);
			    foreach (
				    var file in (from filePath in files let fi = new FileInfo(filePath) where fi.CreationTime < dt select filePath))
				    try
				    {
					    File.Delete(file);
				    }
				    catch
				    {
					    //fall through;
					    ;
				    }
		    }
	    }

	    private void CleanTempFiles()
		{
			if (!string.IsNullOrEmpty(_tempFolder) && Directory.Exists(_tempFolder))
			{
				var dt = DateTime.Now.AddMinutes(-5);
				var files = Directory.GetFiles(_tempFolder, "*.*", SearchOption.AllDirectories);
				foreach (var file in (from filePath in files let fi = new FileInfo(filePath) where fi.CreationTime < dt select filePath))
					try
					{
						File.Delete(file);
					}
					catch
					{
						//fall through;
					}
				var dirs = Directory.GetDirectories(_tempFolder);
				foreach(var dir in dirs.Where(d=> !Directory.GetFiles(d, "*.*", SearchOption.AllDirectories).Any()))
					try
					{
						Directory.Delete(dir, true);
					}
					catch
					{
						//fall through;
					}
			}
		}

    	private static void PurgeExpiredLocks()
		{
			var manager = new LocksCleanUpManager();
			manager.PurgeExpiredTenantLocks();
		}

    	private static void PurgeRateAnalysisDataReadyForDelete()
    	{
    		var manager = new BatchRateDataCleanUpManager();
			manager.PurgeCompletedReadyForDeleteAfter();
    	}

        private void PurgeExpiredQuotes()
        {
            var expiredLoads = new LoadOrderSearch().FetchExpiredLoads();
            var deletionHandler = new LoadOrderDeletionHandler();

            var messages = deletionHandler.DeleteLoadOrders(expiredLoads);

            if (!messages.Any()) return;

            var ex = new Exception(messages.ToArray().CommaJoin());
            ErrorLogger.LogError(ex, _errorLogFilesPath);
        }

	    private void PurgeExpiredBackgroundReportRunRecords()
	    {
	        var expiredRecords = new BackgroundReportRunSearch().FetchExpiredBackgroundReportRunRecords();

	        List<Exception> ex;
	        var backgroundManager = new BackgroundReportRunRecordManager();
	        var fileNamesToDelete = backgroundManager.DeleteBackgroundReports(expiredRecords, out ex);

	        if (ex.Any())
	        {
	            ex.ForEach(e => ErrorLogger.LogError(e, _errorLogFilesPath));
	        }

	        foreach (var fileName in fileNamesToDelete)
	        {
	            var physicalPath = string.Format("{0}{1}", _backgroundReportsFolder, fileName);

	            var file = new FileInfo(physicalPath);
	            try
	            {
	                file.Delete();
	            }
	            catch
	            {
	                //fall through;
	            }
	        }
	    }
    }
}
