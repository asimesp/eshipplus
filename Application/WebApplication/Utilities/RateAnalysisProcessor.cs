﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Web;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Calculations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Rates;
using LogisticsPlus.Eship.Project44;
using LogisticsPlus.Eship.Smc;
using P44SDK.V4.Model;

namespace LogisticsPlus.Eship.WebApplication.Utilities
{
    public class RateAnalysisProcessor
    {
        private const int MaxChildrenThreads = 5;

        private static readonly object LockObject = new object();
        private static readonly Queue<long> DataSetIds = new Queue<long>();

        const int BatchSize = 2000; // processing batches.

        private readonly HttpServerUtility _server;
        private bool _shutdownThread;
        private Thread _managerThread;
        private List<Thread> _project44ProcessingThreads;
        private readonly Project44Wrapper _project44Wrapper;

        public static Queue<Tuple<BatchRateData, Lane>> _queuedLanesForProject44Analysis;
        private static Dictionary<long, List<string>> _project44Results;
        public static Dictionary<string, Dictionary<string, List<RateQuote>>> _project44RatedLanes;

        public string ErrorsFolder { get; set; }

        public bool IsRunning
        {
            get { return _managerThread != null; }
        }

        public RateAnalysisProcessor(string errorsFolder, HttpServerUtility server, Project44ServiceSettings settings)
        {
            ErrorsFolder = errorsFolder;
            _server = server;

            _project44Wrapper = new Project44Wrapper(settings);
            _project44Results = new Dictionary<long, List<string>>();
            _queuedLanesForProject44Analysis = new Queue<Tuple<BatchRateData, Lane>>();
            _project44RatedLanes = new Dictionary<string, Dictionary<string, List<RateQuote>>>();
        }

        ~RateAnalysisProcessor()
        {
            Shutdown();
        }


        public static void Enqueue(long id)
        {
            lock (LockObject)
            {
                DataSetIds.Enqueue(id);
            }
        }

        public static void Enqueue(List<long> ids)
        {
            lock (LockObject)
            {
                foreach (var id in ids)
                    DataSetIds.Enqueue(id);
            }
        }

        public static Queue<long> GetQueuedIds()
        {
            return DataSetIds;
        }



        // Helper Methods for handling multi-threaded Project 44 Analysis
        public static void EnqueueProject44Lane(Tuple<BatchRateData, Lane> lane)
        {
            lock (LockObject)
            {
                _queuedLanesForProject44Analysis.Enqueue(lane);
            }
        }

        public static Tuple<BatchRateData, Lane> DequeueProject44Lane()
        {
            lock (LockObject)
            {
                return _queuedLanesForProject44Analysis.Dequeue();
            }
        }

        public static List<string> FetchProject44Results(long id)
        {
            lock (LockObject)
            {
                if (_project44Results.ContainsKey(id))
                {
                    var lanes = _project44Results.Where(l => l.Key == id).SelectMany(l => l.Value).ToList();
                    _project44Results.Remove(id);
                    return lanes;
                }

                return new List<string>();
            }
        }

        public static void AddProject44Result(long id, string lane)
        {
            lock (LockObject)
            {
                if (_project44Results.ContainsKey(id))
                {
                    _project44Results[id].Add(lane);
                }
                else
                {
                    _project44Results.Add(id, new List<string> { lane });
                }
            }
        }

        public static RateQuote GetProject44RatedLane(string groupCode, string lineId, string scac)
        {
            lock (LockObject)
            {
                if (_project44RatedLanes.ContainsKey(groupCode) && _project44RatedLanes[groupCode].ContainsKey(lineId))
                {
                    return _project44RatedLanes[groupCode][lineId].FirstOrDefault(r => r.CarrierCode == scac);
                }

                return null;
            }
        }

        public static void AddProject44RatedLanes(string groupCode, string lineId, List<RateQuote> rates)
        {
            lock (LockObject)
            {
                if (_project44RatedLanes.ContainsKey(groupCode) && _project44RatedLanes[groupCode].ContainsKey(lineId))
                {
                    _project44RatedLanes[groupCode][lineId].AddRange(rates.Where(r => !_project44RatedLanes[groupCode][lineId].Select(l => l.CarrierCode).Contains(r.CarrierCode)));
                }
                else if (_project44RatedLanes.ContainsKey(groupCode))
                {
                    _project44RatedLanes[groupCode].Add(lineId, rates);
                }
                else
                {
                    _project44RatedLanes.Add(groupCode, new Dictionary<string, List<RateQuote>>
                    {
                        {lineId, rates}
                    });
                }
            }
        }

        public static void ClearProject44RatedLanes()
        {
            lock (LockObject)
            {
                _project44RatedLanes.Clear();
            }
        }

        public static int GetCurrentProject44QueuedLanes()
        {
            lock (LockObject)
            {
                return _queuedLanesForProject44Analysis.Count;
            }
        }


        public void Start()
        {
            try
            {
                if (_managerThread == null)
                {
                    _project44ProcessingThreads = new List<Thread>();

                    // start the scheduling thread
                    _managerThread = new Thread(ManagerThread);
                    _managerThread.Start();
                }
            }
            catch (Exception e)
            {
                ErrorLogger.LogError(e, ErrorsFolder);
            }
        }

        public void Shutdown()
        {
            if (_managerThread != null)
            {
                _shutdownThread = true;

                // wait to kill child threads
                var w = new Stopwatch();
                w.Start();
                do
                {
                    // loop for 35 seconds max or until all children threads are killed off!
                } while (_project44ProcessingThreads.Any(t => t.IsAlive) && w.Elapsed.Seconds <= 35);
                w.Stop();

                // wait for the scheduler thread to shutdown for up to 60 sec
                // if the thread does not shutdown on time, try to kill it
                if (!_managerThread.Join(60000))
                {
                    try
                    {
                        _managerThread.Abort();
                    }
                    catch (Exception e)
                    {
                        ErrorLogger.LogError(e, ErrorsFolder);
                    }
                }

                _managerThread = null;
            }
        }

        private void ManagerThread()
        {
            // delay before starting the thread
            Thread.Sleep(250);

            // init some state variables
            var counter = 4;

            if (_shutdownThread) return;

            do
            {
                // each actual thread iteration occurs in 1 minute intervals
                // however to make the tread more responsive it will sleep 4 x 250ms
                if (counter != 0)
                {
                    Thread.Sleep(250);
                    counter--;
                    continue;
                }

                // reset the counter
                counter = 4;

                try
                {
                    if (!DataSetIds.Any())
                    {
                        if (WebApplicationSettings.EnableProject44RateProcessor)
                        {
                            ClearProject44RatedLanes();
                        }

                        continue;
                    }

                    long id;
                    lock (LockObject)
                    {
                        id = DataSetIds.Dequeue();
                    }
                    if (id == default(long)) continue;
                    var data = new BatchRateData(id);
                    if (!data.KeyLoaded) continue;
                    if (data.DateCompleted != DateUtility.SystemEarliestDateTime) continue; // i.e. completed

                    if (!WebApplicationSettings.EnableProject44RateProcessor)
                    {
                        switch (data.ServiceMode)
                        {
                            case ServiceMode.LessThanTruckload:
                                ProcessSmc3LtlAnalysis(data);
                                break;
                            case ServiceMode.SmallPackage:
                                ProcessSmc3SmallPackageAnalysis(data);
                                break;
                        }
                    }
                    else
                    {
                        ProcessProject44LtlAnalysis(data);
                    }

                    // set completed;
                    data.DateCompleted = DateTime.Now;
                    data.Save();
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("eShipPlus:ediManager : " + ex.Message);
                    ErrorLogger.LogError(ex, ErrorsFolder);
                }
            } while (!_shutdownThread);
        }

        private void ProcessSmc3SmallPackageAnalysis(BatchRateData data)
        {
            try
            {
                var countries = ProcessorVars.RegistryCache.Countries.Values;
                var lines = data.LaneData.Split(Environment.NewLine.ToArray(), StringSplitOptions.RemoveEmptyEntries).ToList();
                var lineBatches = lines.Batch(BatchSize);
                var oLines = new List<string>();
                foreach (var lineBatch in lineBatches)
                {
                    var lanes = lineBatch
                        .Select(l => l.Split(WebApplicationUtilities.ImportSeperators()))
                        .Where(p => p.Length >= 9)
                        .Select(p => new Lane
                        {
                            LineId = p[0],
                            OriginPostalCode = p[1],
                            OriginCountryId =
                                    (countries.FirstOrDefault(c => c.Code.ToLower() == p[2].ToLower()) ?? new Country()).Id,
                            DestinationPostalCode = p[3],
                            DestinationCountryId =
                                    (countries.FirstOrDefault(c => c.Code.ToLower() == p[4].ToLower()) ?? new Country()).Id,
                            ActualWeight = p[5].ToDecimal(),
                            Length = p[6].ToDecimal(),
                            Width = p[7].ToDecimal(),
                            Height = p[8].ToDecimal(),
                        })
                        .ToList();

                    var variables = ProcessorVars.IntegrationVariables[data.TenantId];
                    var pmaps = ProcessorVars.RegistryCache[data.TenantId].SmallPackagingMaps;
                    var rater = new VendorRateAnalysisSmallPack(variables.FedExParameters, variables.UpsParameters, pmaps);
                    rater.RunAnalysis(data.SmallPackageEngine, data.SmallPackageEngineType, data.PackageType, lanes,
                                      data.AnalysisEffectiveDate);

                    oLines.AddRange(lanes
                        .Select(l => new[]
                            {
                                l.LineId,
                                l.OriginPostalCode,
                                (countries.FirstOrDefault(c => c.Id == l.OriginCountryId) ?? new Country()).Code,
                                l.DestinationPostalCode,
                                (countries.FirstOrDefault(c => c.Id == l.DestinationCountryId) ?? new Country()).Code,
                                data.AnalysisEffectiveDate.FormattedShortDate(),
                                l.ActualWeight.GetString(), l.Length.GetString(), l.Width.GetString(), l.Height.GetString(),
                                data.SmallPackageEngine.GetString(), data.SmallPackageEngineType, data.PackageType.TypeName,
                                l.BilledWeight.GetString(), l.OriginalValue.GetString(),
                                l.CurrentTransitDays >= 1 ? l.CurrentTransitDays.GetString() : string.Empty,
                                l.Comment
                            }.TabJoin())
                        .ToList());
                }
                var header = new[]
                        {
                            "Line ID",
                            "Origin Postal Code", "Origin Country Code", "Destination Postal Code", "Destination Country Code",
                            "Analysis Effective Date", "Actual Weight (lb)", "Length (in)", "Width (in)", "Height (in)",
                            "Rate Engine",
                            "Service Type", "Package Type", "Billed Weight (lb)", "Cost ($)", "Current Transit Days", "Comment"
                        }.TabJoin();
                oLines.Insert(0, header);
                data.ResultData = oLines.ToArray().NewLineJoin();
            }
            catch (Exception e)
            {
                data.ResultData = "Error Processing: " + e.Message;
                ErrorLogger.LogError(e, ErrorsFolder);
            }
        }

        private void ProcessSmc3LtlAnalysis(BatchRateData data)
        {

            try
            {
                var countries = ProcessorVars.RegistryCache.Countries.Values;
                var lines = data.LaneData.Split(Environment.NewLine.ToArray(), StringSplitOptions.RemoveEmptyEntries).ToList();
                var lineBatches = lines.Batch(BatchSize);
                var oLines = new List<string>();
                foreach (var lineBatch in lineBatches)
                {
                    var lanes = lineBatch
                    .Select(l => l.Split(WebApplicationUtilities.ImportSeperators()))
                    .Where(p => p.Length >= 7)
                    .Select(p => new Lane
                    {
                        LineId = p[0],
                        OriginPostalCode = p[1],
                        OriginCountryId = (countries.FirstOrDefault(c => c.Code.ToLower() == p[2].ToLower()) ?? new Country()).Id,
                        DestinationPostalCode = p[3],
                        DestinationCountryId = (countries.FirstOrDefault(c => c.Code.ToLower() == p[4].ToLower()) ?? new Country()).Id,
                        ActualWeight = p[5].ToDecimal(),
                        ActualFreightClass = p[6].ToDouble()
                    })
                    .ToList();

                    var ratewareSettings = string.IsNullOrEmpty(data.Tenant.SMCRateWareParameterFile)
                                ? new SmcServiceSettings()
                                : _server.ReadFromFile(data.Tenant.SMCRateWareParameterFile).FromUtf8Bytes().FromXml<SmcServiceSettings>();
                    var carrierConnectSettings = string.IsNullOrEmpty(data.Tenant.SMCCarrierConnectParameterFile)
                                ? new SmcServiceSettings()
                                : _server.ReadFromFile(data.Tenant.SMCCarrierConnectParameterFile).FromUtf8Bytes().FromXml<SmcServiceSettings>();

                    var rater = new VendorRateAnalysisLTL(ratewareSettings, carrierConnectSettings);

                    rater.RunSmc3Analysis(data.VendorRating, data.AnalysisEffectiveDate, lanes, data.LTLIndirectPointEnabled);

                    oLines.AddRange(lanes
                        .Select(l => new[]
                        {
                                l.LineId,
                                l.OriginPostalCode,
                                (countries.FirstOrDefault(c => c.Id == l.OriginCountryId) ?? new Country()).Code,
                                l.DestinationPostalCode,
                                (countries.FirstOrDefault(c => c.Id == l.DestinationCountryId) ?? new Country()).Code,
                                data.VendorRating.Name, data.AnalysisEffectiveDate.FormattedShortDate(),
                                l.ActualWeight.GetString(), l.ActualFreightClass.GetString(), l.RatedFreightClass.GetString(),
                                l.BilledWeight.GetString(), l.BilledWeightBreak, l.OriginalValue.GetString(), l.DiscountPercent.GetString(),
                                l.FreightCostFloor.GetString(), l.FreightCostCeiling.GetString(),
                                l.FreightCost.GetString(), l.FuelPercent.GetString(), l.FuelCost.GetString(),
                                l.CurrentTransitDays >= 1 ? l.CurrentTransitDays.GetString() : string.Empty,
                                l.Comment
                        }.TabJoin())
                        .ToList());

                    var header = new[]
                    {
                            "Line ID",
                            "Origin Postal Code", "Origin Country Code", "Destination Postal Code", "Destination Country Code",
                            "Vendor Rating Profile", "Analysis Effective Date",
                            "Actual Weight (lb)", "Actual Freight Class", "Rated Freight Class", "Billed Weight (lb)", "Billed Weight Break",
                            "Original Value ($)",
                            "Discount Percent (%)", "Vendor Floor ($)", "Vendor Ceiling ($)", "Freight Cost ($)",
                            "Fuel Percent (%)",
                            "Fuel Cost ($)", "Current Transit Days", "Comment"
                        }.TabJoin();

                    oLines.Insert(0, header);
                }

                data.ResultData = oLines.ToArray().NewLineJoin();
            }
            catch (Exception e)
            {
                data.ResultData = "Error Processing: " + e.Message;
                ErrorLogger.LogError(e, ErrorsFolder);
            }
        }


        private void ProcessProject44LtlAnalysis(BatchRateData data)
        {
            try
            {
                PrepareBatchDataForProject44Analysis(data);
                var laneCount = GetCurrentProject44QueuedLanes();
                var results = new List<string>();

                // NOTE: We multi-thread Project 44 analysis as the single-threaded implementation takes too much time
                // spin off new threads to do work
                for (var i = _project44ProcessingThreads.Count; i < MaxChildrenThreads; i++)
                {
                    if (GetCurrentProject44QueuedLanes() == 0) break; // if no lanes to analyze, break out of loop

                    var t = new Thread(RunProject44Analysis);
                    _project44ProcessingThreads.Add(t);
                    t.Start();

                    Thread.Sleep(500); // sleep for short period to allow thread kick off and deque lane
                }

                while (results.Count != laneCount)
                {
                    results.AddRange(FetchProject44Results(data.Id));
                    Thread.Sleep(500);
                }

                var header = new[]
                {
                    "Line ID",
                    "Origin Postal Code",
                    "Origin Country Code",
                    "Destination Postal Code",
                    "Destination Country Code",
                    "Vendor Rating Profile",
                    "Analysis Effective Date",
                    "Actual Weight (lb)",
                    "Actual Freight Class",
                    "Original Value ($)",
                    "Freight Cost ($)",
                    "Fuel Cost ($)",
                    "Discount ($)",
                    "Current Transit Days",
                    "Comment"
                }.TabJoin();

                results.Insert(0, header);
                data.ResultData = results.ToArray().NewLineJoin();

                // clean up dead/complete/abort threads
                if (_project44ProcessingThreads.Any(t => !t.IsAlive))
                {
                    _project44ProcessingThreads.RemoveAll(t => !t.IsAlive);
                }
            }
            catch (Exception ex)
            {
                data.ResultData = "Error Processing: " + ex.Message;
                ErrorLogger.LogError(ex, ErrorsFolder);
            }
        }

        private void PrepareBatchDataForProject44Analysis(BatchRateData data)
        {
            var countries = ProcessorVars.RegistryCache.Countries.Values;
            var lines = data.LaneData.Split(Environment.NewLine.ToArray(), StringSplitOptions.RemoveEmptyEntries).ToList();
            var lineBatches = lines.Batch(BatchSize);
            foreach (var lineBatch in lineBatches)
            {
                var lanes = lineBatch
                    .Select(l => l.Split(WebApplicationUtilities.ImportSeperators()))
                    .Where(p => p.Length >= 7)
                    .Select(p => new Lane
                    {
                        LineId = p[0],
                        OriginPostalCode = p[1],
                        OriginCountryId =
                            (countries.FirstOrDefault(c => c.Code.ToLower() == p[2].ToLower()) ?? new Country()).Id,
                        DestinationPostalCode = p[3],
                        DestinationCountryId =
                            (countries.FirstOrDefault(c => c.Code.ToLower() == p[4].ToLower()) ?? new Country()).Id,
                        ActualWeight = p[5].ToDecimal(),
                        ActualFreightClass = p[6].ToDouble()
                    })
                    .ToList();

                lanes.ForEach(l => EnqueueProject44Lane(new Tuple<BatchRateData, Lane>(data, l)));
            }

        }

        private void RunProject44Analysis()
        {
            do
            {
                var laneBatch = DequeueProject44Lane();
                var batchRateData = laneBatch.Item1;
                var lane = laneBatch.Item2;
                var countries = ProcessorVars.RegistryCache.Countries.Values;

                try
                {
                    if (_project44Wrapper == null)
                    {
                        var failedLine = new[]
                        {
                            lane.LineId,
                            lane.OriginPostalCode,
                            (countries.FirstOrDefault(c => c.Id == lane.OriginCountryId) ?? new Country()).Code,
                            lane.DestinationPostalCode,
                            (countries.FirstOrDefault(c => c.Id == lane.DestinationCountryId) ?? new Country()).Code,
                            batchRateData.VendorRating.Name,
                            batchRateData.AnalysisEffectiveDate.FormattedShortDate(),
                            lane.ActualWeight.GetString(),
                            lane.ActualFreightClass.GetString(),
                            string.Empty,
                            "0",
                            "0",
                            "0",
                            "0",
                            "Project 44 Lane Rate failed."
                        }.TabJoin();
                        AddProject44Result(batchRateData.Id, failedLine);
                        continue;
                    }



                    // run rates
                    RateQuote p44Rate;

                    // See if we've rated this lane before,
                    var matchingStoredRate = GetProject44RatedLane(batchRateData.GroupCode, lane.LineId, batchRateData.VendorRating.Vendor.Scac);
                    if (matchingStoredRate != null)
                    {
                        // We have, so use it instead of having to wait an additional 2-3 seconds for a Project 44 rate response
                        p44Rate = matchingStoredRate;
                    }
                    else
                    {
                        // if not, grab it from project 44
                        LaneRateRequest p44Request;

                        try
                        {
                            // build p44 rate shipment request
                            p44Request = new LaneRateRequest
                            {
                                OriginPostalCode = lane.OriginPostalCode,
                                OriginCountry = Address.CountryEnum.US,
                                DestinationPostalCode = lane.DestinationPostalCode,
                                DestinationCountry = Address.CountryEnum.US,
                                FreightClass = lane.ActualFreightClass,
                                Height = lane.Height,
                                Length = lane.Length,
                                Weight = lane.ActualWeight,
                                Width = lane.Width,
                                Scac = batchRateData.VendorRating.Vendor.Scac
                            };

                            // lane values
                            lane.FuelPercent = batchRateData.VendorRating.CurrentLTLFuelMarkup.ApplyManipulations(
                                batchRateData.VendorRating.FuelMarkupFloor, batchRateData.VendorRating.FuelMarkupCeiling);
                        }
                        catch (Exception e)
                        {
                            var failedLine = new[]
                            {
                            lane.LineId,
                            lane.OriginPostalCode,
                            (countries.FirstOrDefault(c => c.Id == lane.OriginCountryId) ?? new Country()).Code,
                            lane.DestinationPostalCode,
                            (countries.FirstOrDefault(c => c.Id == lane.DestinationCountryId) ?? new Country()).Code,
                            batchRateData.VendorRating.Name,
                            batchRateData.AnalysisEffectiveDate.FormattedShortDate(),
                            lane.ActualWeight.GetString(),
                            lane.ActualFreightClass.GetString(),
                            string.Empty,
                            "0",
                            "0",
                            "0",
                            "0",
                            string.Format("Error processing lane: {0}", e.Message)
                        }.TabJoin();
                            AddProject44Result(batchRateData.Id, failedLine);

                            continue;
                        }

                        var p44Rates = _project44Wrapper.GetRates(p44Request);
                        AddProject44RatedLanes(batchRateData.GroupCode, lane.LineId, p44Rates);

                        p44Rate = p44Rates.FirstOrDefault(r => r.CarrierCode == batchRateData.VendorRating.Vendor.Scac);
                    }

                    lane.Comment = p44Rate != null && p44Rate.ErrorMessages == null
                        ? "Rate retrieved from Project 44."
                        : string.Format("Rate could not be retrieved from Project 44. {0}",
                            p44Rate != null && p44Rate.ErrorMessages != null
                                ? p44Rate.ErrorMessages.Select(m => m._Message).ToArray().NewLineJoin()
                                : string.Empty);

                    if (!batchRateData.LTLIndirectPointEnabled && p44Rate != null && p44Rate.LaneType != RateQuote.LaneTypeEnum.DIRECT)
                    {
                        // if invalid rate, set all return values to zero!
                        lane.Comment += " Indirect point rate not allowed.";

                        if (p44Rate.RateQuoteDetail != null)
                            p44Rate.RateQuoteDetail.Charges = new List<Charge>();
                    }


                    var fuelCost = p44Rate != null && p44Rate.RateQuoteDetail != null
                        ? p44Rate.RateQuoteDetail.Charges.Where(c =>
                                Project44Constants.FuelChargeCodes.Contains(
                                    c.Code.ToEnum<Project44ChargeCode>()))
                            .Sum(c => c.Amount)
                        : default(decimal);

                    var freightCost = p44Rate != null && p44Rate.RateQuoteDetail != null
                        ? p44Rate.RateQuoteDetail.Charges.Where(c =>
                                !Project44Constants.FuelChargeCodes.Contains(
                                    c.Code.ToEnum<Project44ChargeCode>()) &&
                                c.Code.ToEnum<Project44ChargeCode>() != Project44ChargeCode.DSC)
                            .Sum(c => c.Amount)
                        : default(decimal);

                    var discountCharge = p44Rate != null && p44Rate.RateQuoteDetail != null
                        ? p44Rate.RateQuoteDetail.Charges.FirstOrDefault(c =>
                            c.Code.ToEnum<Project44ChargeCode>() == Project44ChargeCode.DSC)
                        : null;

                    var rateLine = new[]
                    {
                        lane.LineId,
                        lane.OriginPostalCode,
                        (countries.FirstOrDefault(c => c.Id == lane.OriginCountryId) ?? new Country()).Code,
                        lane.DestinationPostalCode,
                        (countries.FirstOrDefault(c => c.Id == lane.DestinationCountryId) ?? new Country()).Code,
                        batchRateData.VendorRating.Name,
                        batchRateData.AnalysisEffectiveDate.FormattedShortDate(),
                        lane.ActualWeight.GetString(),
                        lane.ActualFreightClass.GetString(),
                        p44Rate != null && p44Rate.RateQuoteDetail != null
                            ? p44Rate.RateQuoteDetail.Total.ToString()
                            : "0",
                        freightCost.ToString(),
                        fuelCost.ToString(),
                        discountCharge != null ? (discountCharge.Amount * -1).ToString() : "0",
                        p44Rate != null && p44Rate.TransitDays >= 1 ? p44Rate.TransitDays.GetString() : "0",
                        lane.Comment
                    }.TabJoin();


                    AddProject44Result(batchRateData.Id, rateLine);
                }
                catch (Exception ex)
                {
                    ErrorLogger.LogError(ex, ErrorsFolder);
                    var failedLine = new[]
                    {
                        lane.LineId,
                        lane.OriginPostalCode,
                        (countries.FirstOrDefault(c => c.Id == lane.OriginCountryId) ?? new Country()).Code,
                        lane.DestinationPostalCode,
                        (countries.FirstOrDefault(c => c.Id == lane.DestinationCountryId) ?? new Country()).Code,
                        batchRateData.VendorRating.Name,
                        batchRateData.AnalysisEffectiveDate.FormattedShortDate(),
                        lane.ActualWeight.GetString(),
                        lane.ActualFreightClass.GetString(),
                        string.Empty,
                        "0",
                        "0",
                        "0",
                        "0",
                        "Project 44 Lane Rate failed."
                    }.TabJoin();
                    AddProject44Result(batchRateData.Id, failedLine);
                }
            } while (GetCurrentProject44QueuedLanes() > 0);

        }
    }
}
