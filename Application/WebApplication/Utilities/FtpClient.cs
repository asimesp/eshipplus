﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using Renci.SshNet;

namespace LogisticsPlus.Eship.WebApplication.Utilities
{
	///<summary>
	///		FTP Client class.  Implements IFtpClient
	///</summary>
	public class FtpClient
	{
		private const int BufferSize = 2048;

		private readonly string _user = string.Empty;
		private readonly string _password = string.Empty;
		private readonly string _ftpHost = string.Empty;
		private Exception _exception;

		private string _currentDirectory = string.Empty;

		/// <summary>
		/// Error messages from unsuccessful operations
		/// </summary>
		public Exception Exception
		{
			get { return _exception; }
		}

		/// <summary>
		/// Secure ftp connection setting
		/// </summary>
		public bool IsSslftp { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this instance is SSH FTP.
		/// </summary>
		/// <value>
		///   <c>true</c> if this instance is SSH FTP; otherwise, <c>false</c>.
		/// </value>
		public bool IsSSHFtp { get; set; }

		///<summary>
		///</summary>
		///<param name="ftpHost">ftp host ommitting "ftp://". This parameter must be supplied else an exception is thrown.</param>
		///<param name="user">ftp username</param>
		///<param name="password">ftp password</param>
		public FtpClient(string ftpHost, string user, string password)
		{
			if (string.IsNullOrEmpty(ftpHost))
				throw new ArgumentNullException("ftpHost");

			_ftpHost = ftpHost;
			_user = user;
			_password = password;

			_exception = null;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="directoryPath">
		///		Relative path to host root. Example 'MyDocments/MyPictures'. Exclude '/' on end of directoryPath
		/// </param>
		public void SetCurrentDirectory(string directoryPath)
		{
			_currentDirectory = directoryPath.EndsWith("/") ? directoryPath.Substring(0, directoryPath.LastIndexOf("/", StringComparison.Ordinal)) : directoryPath;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="filename">filename in current directory</param>
		/// <returns>true if successful</returns>
		public bool DeleteFile(string filename)
		{
			try
			{
				//SFTP
				if (IsSSHFtp)
				{
					using (var sftp = new SftpClient(_ftpHost, _user, _password))
					{
						sftp.Connect();
						sftp.ChangeDirectory(_currentDirectory);
						if (!sftp.Exists(filename))
						{
							_exception = new FileNotFoundException(string.Format("File does not exist! [filname = {0}]", filename));
							return false;
						}
						sftp.DeleteFile(filename);
						sftp.Disconnect();
						return true;
					}
				}

				if (!FileExists(filename))
				{
					_exception = new FileNotFoundException(string.Format("File does not exist! [filname = {0}]", filename));
					return false;
				}

				var request = GetRequest(filename);
				request.Method = WebRequestMethods.Ftp.DeleteFile;
				using (var response = (FtpWebResponse)request.GetResponse())
				using (var stream = response.GetResponseStream())
					stream.ReadByte();
				return true;
			}
			catch (Exception e)
			{
				_exception = new Exception(string.Format("Delete File Error: [{0}]", e), e);
				return false;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="filename">filename in current directory</param>
		/// <returns>byte[] contain contents if successful else null</returns>
		public byte[] DownloadFile(string filename)
		{
			try
			{
				//SFTP
				if (IsSSHFtp)
				{
					if (!FileExists(filename))
					{
						_exception = new FileNotFoundException(string.Format("File does not exist! [filname = {0}]", filename));
						return null;
					}
					using (var sftp = new SftpClient(_ftpHost, _user, _password))
					{
						byte[] result;
						sftp.Connect();
						sftp.ChangeDirectory(_currentDirectory);
						using (var ms = new MemoryStream())
						{
							sftp.DownloadFile(filename, ms);
							result = ms.ToArray();
						}
						sftp.Disconnect();
						return result;
					}
				}

				if (!FileExists(filename))
				{
					_exception = new FileNotFoundException(string.Format("File does not exist! [filname = {0}]", filename));
					return null;
				}

				byte[] contents;
				using (var stream = new MemoryStream())
				{
					var request = GetRequest(filename);
					request.Method = WebRequestMethods.Ftp.DownloadFile;
					request.UseBinary = true;
					using (var response = (FtpWebResponse)request.GetResponse())
					using (var reader = response.GetResponseStream())
						if (reader != null)
						{
							int readCount;
							var buffer = new byte[BufferSize];
							do
							{
								readCount = reader.Read(buffer, 0, BufferSize);
								if (readCount > 0)
									stream.Write(buffer, 0, readCount);
							} while (readCount > 0);
						}
					contents = stream.ToArray();
				}
				return contents;
			}
			catch (Exception e)
			{
				_exception = new Exception(string.Format("Download File Error: [{0}]", e), e);
				return null;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="filename">filename in current directory</param>
		/// <param name="saveToLocation"></param>
		/// <returns>true if successful</returns>
		public bool DownloadFile(string filename, string saveToLocation)
		{
			try
			{
				//SFTP
				if (IsSSHFtp)
				{
					if (!FileExists(filename))
					{
						_exception = new FileNotFoundException(string.Format("File does not exist! [filname = {0}]", filename));
						return false;
					}
					using (var sftp = new SftpClient(_ftpHost, _user, _password))
					{
						sftp.Connect();
						sftp.ChangeDirectory(_currentDirectory);
						using (var fs = new FileStream(saveToLocation + "\\" + filename, FileMode.Create))
						{
							sftp.DownloadFile(filename, fs);
						}
						sftp.Disconnect();
						return true;
					}
				}

				if (!FileExists(filename))
				{
					_exception = new FileNotFoundException(string.Format("File does not exist! [filname = {0}]", filename));
					return false;
				}

				using (var stream = new FileStream(string.Format("{0}\\{1}", saveToLocation, filename), FileMode.Create))
				{
					var request = GetRequest(filename);
					request.Method = WebRequestMethods.Ftp.DownloadFile;
					request.UseBinary = true;
					using (var response = (FtpWebResponse)request.GetResponse())
					using (var reader = response.GetResponseStream())
						if (reader != null)
						{
							int readCount;
							var buffer = new byte[BufferSize];
							do
							{
								readCount = reader.Read(buffer, 0, BufferSize);
								if (readCount > 0)
									stream.Write(buffer, 0, readCount);
							} while (readCount > 0);
						}
				}
				return true;
			}
			catch (Exception e)
			{
				_exception = new Exception(string.Format("Download File Error: [{0}]", e), e);
				return false;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="filename">filename in current directory</param>
		/// <returns>true if successful and file exists. false if error or files doesn't exist</returns>
		public bool FileExists(string filename)
		{
			try
			{
				//SFTP
				if (IsSSHFtp)
				{
					using (var sftp = new SftpClient(_ftpHost, _user, _password))
					{
						sftp.Connect();
						sftp.ChangeDirectory(_currentDirectory);
						var exists = sftp.Exists(filename);
						sftp.Disconnect();
						return exists;
					}
				}

				var directory = ListDirectory(false);
				return directory.Any(file => String.CompareOrdinal(file, filename) == 0);
			}
			catch (Exception e)
			{
				_exception = new Exception(string.Format("File Exists Error: [{0}]", e), e);
				return false;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="filename">filename in current directory</param>
		/// <returns>null if unsucessful</returns>
		public DateTime? GetDateTimeStamp(string filename)
		{
			try
			{
				//SFTP
				if (IsSSHFtp)
				{
					using (var sftp = new SftpClient(_ftpHost, _user, _password))
					{
						sftp.Connect();
						sftp.ChangeDirectory(_currentDirectory);

						if (!sftp.Exists(filename))
						{
							_exception = new FileNotFoundException(string.Format("File does not exist! [filname = {0}]", filename));
							return null;
						}

						var files = sftp.ListDirectory(_currentDirectory);
						foreach (var file in files)
						{
							if (string.Compare(file.Name, filename, StringComparison.InvariantCultureIgnoreCase) == 0)
							{
								return file.LastWriteTime;
							}
						}
						sftp.Disconnect();
					}
					return null;
				}

				if (!FileExists(filename))
				{
					_exception = new FileNotFoundException(string.Format("File does not exist! [filname = {0}]", filename));
					return null;
				}

				var request = GetRequest(filename);
				request.Method = WebRequestMethods.Ftp.GetDateTimestamp;
				using (var response = (FtpWebResponse)request.GetResponse())
				using (var stream = response.GetResponseStream())
				using (var reader = new StreamReader(stream))
				{
					var data = reader.ReadToEnd();
					DateTime dateTime;
					return DateTime.TryParse(data, out dateTime) ? dateTime : (DateTime?)null;
				}
			}
			catch (Exception e)
			{
				_exception = new Exception(string.Format("Get Date Time Stamp Error: [{0}]", e), e);
				return null;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="filename">filename in current directory</param>
		/// <returns>-1 if unsuccessful</returns>
		public long GetFileSize(string filename)
		{
			const int errorReturnValue = -1;
			
			try
			{
				//SFTP
				if (IsSSHFtp)
				{
					using (var sftp = new SftpClient(_ftpHost, _user, _password))
					{
						sftp.Connect();
						sftp.ChangeDirectory(_currentDirectory);
						if (!sftp.Exists(filename))
						{
							_exception = new FileNotFoundException(string.Format("File does not exist! [filname = {0}]", filename));
							return errorReturnValue;
						}

						var files = sftp.ListDirectory(_currentDirectory);
						foreach (var file in files)
						{
							if (string.Compare(file.Name, filename, StringComparison.InvariantCultureIgnoreCase) == 0)
							{
								return file.Length;
							}
						}
						sftp.Disconnect();
					}
					return errorReturnValue;
				}

				if (!FileExists(filename))
				{
					_exception = new FileNotFoundException(string.Format("File does not exist! [filname = {0}]", filename));
					return errorReturnValue;
				}

				var request = GetRequest(filename);
				request.Method = WebRequestMethods.Ftp.GetFileSize;
				using (var response = (FtpWebResponse)request.GetResponse())
					return response.ContentLength;
			}
			catch (Exception e)
			{
				_exception = new Exception(string.Format("Get File Size Error: [{0}]", e), e);
				return errorReturnValue;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="withDetails">get detail listing with contents</param>
		/// <returns>Gets directory contents</returns>
		public List<string> ListDirectory(bool withDetails)
		{
			try
			{
				var directoryContents = new List<string>();

				//SFTP
				if (IsSSHFtp)
				{
					using (var sftp = new SftpClient(_ftpHost, _user, _password))
					{
						sftp.Connect();
						var files = sftp.ListDirectory(_currentDirectory);
						foreach (var file in files)
						{
							if (file.Name != "." && file.Name != "..")
								directoryContents.Add(withDetails
									? string.Format("{0}\t{1}\t{2}", file.Name, file.Length, file.LastWriteTime)
									: file.Name);
						}
						sftp.Disconnect();
						return directoryContents;
					}
				}

				var request = GetRequest(string.Empty);
				request.UseBinary = true;
				request.Method = withDetails ? WebRequestMethods.Ftp.ListDirectoryDetails : WebRequestMethods.Ftp.ListDirectory;
				using (var response = (FtpWebResponse)request.GetResponse())
				using (var reader = new StreamReader(response.GetResponseStream()))
				{
					string line;
					while (!string.IsNullOrEmpty(line = reader.ReadLine()))
						directoryContents.Add(line);
					return directoryContents;
				}
			}
			catch (Exception e)
			{
				_exception = new Exception(string.Format("List Directory Error: [{0}]", e), e);
				return new List<string>();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="directoryPath">relative path to host root. Exclude '/' on the end of directoryPath</param>
		/// <returns>true if successful</returns>
		public bool MakeDirectory(string directoryPath)
		{
			try
			{
				//SFTP
				if (IsSSHFtp)
				{
					using (var sftp = new SftpClient(_ftpHost, _user, _password))
					{
						sftp.Connect();
						sftp.CreateDirectory(_currentDirectory);
						sftp.Disconnect();
						return true;
					}
				}

				SetCurrentDirectory(directoryPath);
				var request = GetRequest(string.Empty);
				request.Method = WebRequestMethods.Ftp.MakeDirectory;
				using (var response = (FtpWebResponse)request.GetResponse())
				using (var stream = response.GetResponseStream())
					stream.ReadByte();
				return true;
			}
			catch (Exception e)
			{
				_exception = new Exception(string.Format("Make Directory Error: [{0}]", e), e);
				return false;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns>null if unsuccessful</returns>
		public string GetWorkingDirectory()
		{
			try
			{
				//SFTP
				if (IsSSHFtp)
				{
					using (var sftp = new SftpClient(_ftpHost, _user, _password))
					{
						sftp.Connect();
						sftp.ChangeDirectory(_currentDirectory);
						var result = sftp.WorkingDirectory;
						sftp.Disconnect();
						return result;
					}
				}

				var request = GetRequest(string.Empty);
				request.Method = WebRequestMethods.Ftp.PrintWorkingDirectory;
				using (var response = (FtpWebResponse)request.GetResponse())
				using (var stream = response.GetResponseStream())
				using (var reader = new StreamReader(stream))
				{
					var data = reader.ReadToEnd();
					return string.IsNullOrEmpty(data) ? null : data;
				}
			}
			catch (Exception e)
			{
				_exception = new Exception(string.Format("Print Working Directory Error: [{0}]", e), e);
				return null;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="directoryPath">relative path to host root new directory. Exclude '/' on the end of directoryPath</param>
		/// <returns>true if successful</returns>
		public bool RemoveDirectory(string directoryPath)
		{
			try
			{
				//SFTP
				if (IsSSHFtp)
				{
					using (var sftp = new SftpClient(_ftpHost, _user, _password))
					{
						sftp.Connect();
						sftp.DeleteDirectory(_currentDirectory);
						sftp.Disconnect();
						return true;
					}
				}

				SetCurrentDirectory(directoryPath);
				var request = GetRequest(string.Empty);
				request.Method = WebRequestMethods.Ftp.RemoveDirectory;
				using (var response = (FtpWebResponse)request.GetResponse())
				using (var stream = response.GetResponseStream())
					stream.ReadByte();
				return true;
			}
			catch (Exception e)
			{
				_exception = new Exception(string.Format("Remove Directory Error: [{0}]", e), e);
				return false;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="oldFilename">old filename in current directory</param>
		/// <param name="newFilename">new filename in current directory</param>
		/// <returns>true if successful</returns>
		public bool RenameFile(string oldFilename, string newFilename)
		{
			try
			{


			//SFTP
			if (IsSSHFtp)
			{
				using (var sftp = new SftpClient(_ftpHost, _user, _password))
				{
					sftp.Connect();
					sftp.ChangeDirectory(_currentDirectory);
					if (!sftp.Exists(oldFilename))
					{
						_exception = new FileNotFoundException(string.Format("File does not exist! [filname = {0}]", oldFilename));
						return false;
					}

					if (sftp.Exists(newFilename))
					{
						_exception = new Exception(string.Format("File already exists on host! [filname = {0}]", newFilename));
						return false;
					}

					sftp.RenameFile(oldFilename, newFilename);
					sftp.Disconnect();
					return true;
				}
			}
			if (oldFilename.CompareTo(newFilename) == 0)
			{
				_exception = new Exception(string.Format("Old and new filenames are equal! [Old name = {0}, New name = {1}]",
												oldFilename, newFilename));
				return false;
			}
			if (!FileExists(oldFilename))
			{
				_exception = new FileNotFoundException(string.Format("File does not exist! [filname = {0}]", oldFilename));
				return false;
			}
			if (FileExists(newFilename))
			{
				_exception = new Exception(string.Format("File already exists on host! [filname = {0}]", newFilename));
				return false;
			}

			
				var request = GetRequest(oldFilename);
				request.Method = WebRequestMethods.Ftp.Rename;
				request.RenameTo = newFilename;
				using (var response = (FtpWebResponse)request.GetResponse())
				using (var stream = response.GetResponseStream())
					stream.ReadByte();
				return true;
			}
			catch (Exception e)
			{
				_exception = new Exception(string.Format("Rename File Error: [{0}]", e), e);
				return false;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="filename">filename on client computer (including path)</param>
		/// <returns>true if successful</returns>
		public bool UploadFile(string filename)
		{
			try
			{
				//SFTP
				if (IsSSHFtp)
				{
					using (var sftp = new SftpClient(_ftpHost, _user, _password))
					{
						sftp.Connect();
						var fi = new FileInfo(filename);
						if (!fi.Exists)
						{
							_exception = new Exception(string.Format("File do not exists! [filname = {0}]", filename));
							return false;
						}
						sftp.ChangeDirectory(_currentDirectory);
						if (sftp.Exists(fi.Name))
						{
							_exception = new Exception(string.Format("File already exists on host! [filname = {0}]", filename));
							return false;
						}
						sftp.UploadFile(fi.OpenRead(), fi.Name);
						sftp.Disconnect();
						return true;
					}
				}
				var fileInfo = new FileInfo(filename);

				if (FileExists(fileInfo.Name))
				{
					_exception = new Exception(string.Format("File already exists on host! [filname = {0}]", filename));
					return false;
				}

				var request = GetRequest(fileInfo.Name);
				request.Method = WebRequestMethods.Ftp.UploadFile;
				request.UseBinary = true;
				request.ContentLength = fileInfo.Length;
				using (var reader = fileInfo.OpenRead())
				using (var writer = request.GetRequestStream())
				{
					int readCount;
					var buffer = new byte[BufferSize];
					do
					{
						readCount = reader.Read(buffer, 0, BufferSize);
						if (readCount > 0)
							writer.Write(buffer, 0, readCount);
					} while (readCount > 0);
				}
				return true;
			}
			catch (Exception e)
			{
				_exception = new Exception(string.Format("Upload File Error: [{0}]", e), e);
				return false;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="contents">File contents</param>
		/// <param name="saveAs">Save file as on server once uploaded</param>
		/// <returns>true if successful</returns>
		public bool UploadFile(byte[] contents, string saveAs)
		{
			try
			{


			if(string.IsNullOrEmpty(saveAs))
			{
				_exception = new Exception("saveAs cannot be null or empty");
				return false;
			}


				//SFTP
				if (IsSSHFtp)
				{
					using (var sftp = new SftpClient(_ftpHost, _user, _password))
					{
						sftp.Connect();
						sftp.ChangeDirectory(_currentDirectory);
						if (sftp.Exists(saveAs))
						{
							_exception = new Exception(string.Format("File already exists on host! [filname = {0}]", saveAs));
							return false;
						}
						sftp.UploadFile(new MemoryStream(contents), saveAs);
						sftp.Disconnect();
						return true;
					}
				}
			
				var request = GetRequest(saveAs);
				request.Method = WebRequestMethods.Ftp.UploadFile;
				request.UseBinary = true;
				request.ContentLength = contents.Length;
				using (var reader = new MemoryStream(contents))
				using (var writer = request.GetRequestStream())
				{
					int readCount;
					var buffer = new byte[BufferSize];
					do
					{
						readCount = reader.Read(buffer, 0, BufferSize);
						if (readCount > 0)
							writer.Write(buffer, 0, readCount);
					} while (readCount > 0);
				}
				return true;
			}
			catch (Exception e)
			{
				_exception = new Exception(string.Format("Upload File Error: [{0}]", e), e);
				return false;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="info">File Info to file to upload</param>
		/// <param name="saveAs">Save file as on server once uploaded</param>
		/// <returns>true if successful</returns>
		public bool UploadFile(FileInfo info, string saveAs)
		{
			try
			{
				if (string.IsNullOrEmpty(saveAs))
				{
					_exception = new Exception("saveAs cannot be null or empty");
					return false;
				}


				//SFTP
				if (IsSSHFtp)
				{
					using (var sftp = new SftpClient(_ftpHost, _user, _password))
					{
						sftp.Connect();
						if (!info.Exists)
						{
							_exception = new Exception(string.Format("File do not exists! [filname = {0}]", info.Name));
							return false;
						}
						sftp.ChangeDirectory(_currentDirectory);
						if (sftp.Exists(saveAs))
						{
							_exception = new Exception(string.Format("File already exists on host! [filname = {0}]", saveAs));
							return false;
						}
						sftp.UploadFile(info.OpenRead(), saveAs);
						sftp.Disconnect();
						return true;
					}
				}
				var request = GetRequest(saveAs);
				request.Method = WebRequestMethods.Ftp.UploadFile;
				request.UseBinary = true;
				request.ContentLength = info.Length;			
				using (var reader = new BinaryReader(info.OpenRead()))
				using (var writer = request.GetRequestStream())
				{
					int readCount;
					var buffer = new byte[BufferSize];
					do
					{
						readCount = reader.Read(buffer, 0, BufferSize);
						if (readCount > 0)
							writer.Write(buffer, 0, readCount);
					} while (readCount > 0);
				}
				return true;
			}
			catch (Exception e)
			{
				_exception = new Exception(string.Format("Upload File Error: [{0}]", e), e);
				return false;
			}
		}


		private FtpWebRequest GetRequest(string filename)
		{
		    ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            var request = (FtpWebRequest)WebRequest.Create(new Uri(string.Format("ftp://{0}/{1}/{2}", _ftpHost, _currentDirectory,
																				  filename)));
			if (!string.IsNullOrEmpty(_user) && !string.IsNullOrEmpty(_password))
				request.Credentials = new NetworkCredential(_user, _password);
			request.EnableSsl = IsSslftp;
			return request;
		}

	}
}