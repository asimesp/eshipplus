﻿using System;
using System.Collections.Generic;
using System.Linq;
using AjaxControlToolkit;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Administration;
using LogisticsPlus.Eship.Processor.Dto.Operations;

namespace LogisticsPlus.Eship.WebApplication.Utilities
{
	public static class AutoCompleteUtility
	{
		private const char Seperator = ':';
		public const string AutoCompleteSeperator = " - ";

		// special prefix operators
		public const char Equal = '=';
		private const char EndsWith = '$';
		private const char BeginsWith = '^';

		private static readonly TextEncryptor Encryptor = new TextEncryptor("qjCnV+3DBy/MsFutwwy1QEJ0b9hr7eCIPCUfCXiUVvY=", "bEhQYuYtY8V3n0jehGoR0g==");

		public struct Keys
		{
            public const string ActiveSuperUser = "Suid";
            public const string ActiveUser = "Uid";
			public const string Tenant = "Tid";
			public const string Customer = "Cid";
		    public const string IsShipmentCoordinator = "Isc";
		    public const string IsCarrierCoordinator = "Icc";
		    public const string QlikUserAttributeType = "Quat";
		}

		public static void SetupExtender(this AutoCompleteExtender extender,  IDictionary<string, string> contextKeyValues, int minimumPrefixLength = 1)
		{
			extender.ContextKey = contextKeyValues.Keys
										.Select(k => string.Format("{0}{1}{2}", k, Seperator, Encryptor.Encrypt(contextKeyValues[k])))
			                            .ToArray()
			                            .CommaJoin();
			extender.MinimumPrefixLength = minimumPrefixLength;
			extender.CompletionSetCount = 100;
		}

		public static IDictionary<string, string> RetrieveContextKeyValues(this string contextKey)
		{
			return contextKey
			               .Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries)
			               .Select(i =>
				               {
					               var split1 = i.Split(new[] {Seperator});

					               return split1.Length < 2 ? null : new {Key = split1[0], Value = Encryptor.Decrypt(split1[1])};
				               })
			               .Where(x => x != null)
			               .ToDictionary(x => x.Key, x => x.Value);
		}



		public static string FormatForAutoComplete(this AccountBucket accountBucket)
		{
			return accountBucket == null
					   ? string.Empty
					   : string.Format("{0}{1}{2}", accountBucket.Code, AutoCompleteSeperator, accountBucket.Description);
		}

		public static string FormatForAutoComplete(this AddressBookViewSearchDto address)
		{
			return address == null
				       ? string.Empty
				       : string.Format("{0}{1}{2}{3}&nbsp;&nbsp;{4}{3}", address.Id, AutoCompleteSeperator, address.Description,
				                       WebApplicationConstants.HtmlBreak, address.FullAddress);
		}

		public static string FormatForAutoComplete(this Customer customer)
		{
			return customer == null
				       ? string.Empty
				       : string.Format("{0}{1}{2}", customer.CustomerNumber, AutoCompleteSeperator, customer.Name);
		}

		public static string FormatForAutoComplete(this Prefix prefix)
		{
			return prefix == null
					   ? string.Empty
					   : string.Format("{0}{1}{2}", prefix.Code, AutoCompleteSeperator, prefix.Description);
		}

		public static string FormatForAutoComplete(this SalesRepresentative rep)
		{
			return rep == null
					   ? string.Empty
					   : string.Format("{0}{1}{2}: {3}", rep.SalesRepresentativeNumber, AutoCompleteSeperator, rep.CompanyName,  rep.Name);
		}

		public static string FormatForAutoComplete(this Tier tier)
		{
			return tier == null
					   ? string.Empty
					   : string.Format("{0}{1}{2}", tier.TierNumber, AutoCompleteSeperator, tier.Name);
		}

		public static string FormatForAutoComplete(this UserDto user)
		{
			return user == null
					   ? string.Empty
					   : string.Format("{0}{1}{2} {3}", user.Username, AutoCompleteSeperator, user.FirstName, user.LastName);
		}

        public static string FormatForAutoComplete(this Vendor vendor)
        {
            return vendor == null
                       ? string.Empty
                       : string.Format("{0}{1}{2}", vendor.VendorNumber, AutoCompleteSeperator, vendor.Name);
        }


		public static string RetrieveSearchCodeFromAutoCompleteString(this string value)
		{
			var idx = value.IndexOf(AutoCompleteSeperator, StringComparison.Ordinal);
			return idx == -1 ? value : value.Substring(0, idx);
		}

		public static Operator GetOperator(this string value)
		{
			if (string.IsNullOrEmpty(value)) return Operator.Contains;
			switch (value[0])
			{
				case BeginsWith: return Operator.BeginsWith;
				case Equal: return Operator.Equal;
				case EndsWith: return Operator.EndsWith;
				default:
					return Operator.Contains;
			}
		}

		public static string CleanPrefixText(this string value)
		{
			if (string.IsNullOrEmpty(value)) return string.Empty;
			var includes = new[] {Equal, BeginsWith, EndsWith};
			return value.Length == 1 ? value : includes.Contains(value[0]) ? value.Substring(1) : value;
		}


        public static string EncryptText(this string value)
        {
            return Encryptor.Encrypt(value);
        }

        public static string DecryptText(this string encryptedValue)
        {
            return Encryptor.Decrypt(encryptedValue);
        }
	}
}