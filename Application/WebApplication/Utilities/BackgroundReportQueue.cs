﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation.BusinessIntelligence;

namespace LogisticsPlus.Eship.WebApplication.Utilities
{
    public static class BackgroundReportQueue
    {
        private const int DbRecheckValue = 10;

        private static readonly object LockObject = new object();

        private static readonly Queue<BackgroundReportRunRecord> _queue = new Queue<BackgroundReportRunRecord>();

        private static int _dbRecheck = DbRecheckValue; // after running empty 10 times, then try reload from db


        public static int Count
        {
            get { return _queue.Count; }
        }

        public static void Enqueue(BackgroundReportRunRecord log)
        {
            lock (LockObject)
            {
                _queue.Enqueue(log);
            }
        }

        public static BackgroundReportRunRecord Dequeue(bool isRefreshRequest = false)
        {
            BackgroundReportRunRecord record = null;
            lock (LockObject)
            {
                // shortcut to ensure we check DB for report record that was dequeue but never actually processed
                if (_queue.Count == 0) _dbRecheck--;
                if (_dbRecheck == 0)
                {
                    foreach (var l in new BackgroundReportRunSearch().FetchBackgroundReportsToRun())
                        _queue.Enqueue(l);
                    _dbRecheck = DbRecheckValue;

                }
                if (_queue.Count > 0 && !isRefreshRequest) record = _queue.Dequeue();
            }
            return record;
        }

        public static List<BackgroundReportRunRecord> CurrentRecords()
        {
            return _queue.ToList();
        }

        public static void Initialize()
        {
            lock (LockObject)
            {
                if (_queue.Count > 0) return;
                foreach (var log in new BackgroundReportRunSearch().FetchBackgroundReportsToRun())
                    _queue.Enqueue(log);
            }
        }
    }
}