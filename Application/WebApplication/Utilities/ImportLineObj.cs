﻿namespace LogisticsPlus.Eship.WebApplication.Utilities
{
	public class ImportLineObj
	{
		public string[] Line { get; set; }
		public int Index { get; set; }
		public int Length { get; set; }

		public ImportLineObj(string[] line, int index, int length)
		{
			Line = line;
			Index = index;
			Length = length;
		}
	}
}