﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Extern.Ws;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Rates;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Smc;
using LogisticsPlus.Eship.WebApplication.Utilities.ImmitationViews;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Utilities
{
	public static class WsUtilities
	{
		public static User CheckExistingValidUser(this WSCredentials credentials, List<WSMessage> messages)
		{
			if (credentials == null)
				messages.Add(WSMessage.ErrorMessage("Missing Credentials."));
			else
			{
				var user = new UserSearch().FetchUserByUsernameAndAccessCode(credentials.Username, credentials.AccessCode);
				if (user == null || user.Password != credentials.Password)
					messages.Add(new WSMessage { Type = WSEnums.MessageType.Error, Value = "Invalid username or password in credentials!" });
				else
				{
					if (!user.Enabled || !user.Tenant.Active)
						messages.Add(new WSMessage {Type = WSEnums.MessageType.Error, Value = "User account/Setup is disabled."});

					if (user.FailedLoginAttempts >= ProcessorVars.MaxFailedLoginAttempts)
						messages.Add(new WSMessage
						             	{
						             		Type = WSEnums.MessageType.Error,
						             		Value = "Suspected hacking activity. Max login attempts exceeded."
						             	});

					var customer = new CustomerSearch().FetchCustomerByCommunicationConnectGuid(credentials.AccessKey, user.TenantId);

					if (customer == null)
					{
						messages.Add(new WSMessage {Type = WSEnums.MessageType.Error, Value = "Access key account not found!"});
						return null;
					}

					if (!customer.Active || !customer.Tier.Active)
						messages.Add(new WSMessage {Type = WSEnums.MessageType.Error, Value = "Access key account is disabled."});

					if (customer.Communication == null || !customer.Communication.EnableWebServiceAPIAccess)
						messages.Add(new WSMessage
						             	{Type = WSEnums.MessageType.Error, Value = "Access key account not web service API enabled."});

					// ensure user has access to account.
					if (!user.TenantEmployee)
					{
						var customerId = customer.Id;
						if (user.DefaultCustomer.Id != customerId && user.UserShipAs.Count(usa => usa.CustomerId == customerId) == 0)
							messages.Add(new WSMessage
							             	{Type = WSEnums.MessageType.Error, Value = "User is not authorized to access this account."});
					}
				}
				return user ?? new User();
			}
			return new User();
		}

		public static Shipment GetShipmentToRate(this WSShipment shipment, List<WSMessage> messages)
		{
			if (shipment == null)
			{
				messages.Add(WSMessage.ErrorMessage("Missing shipment object."));
				return null;
			}

			shipment.ValidateRatingFields(messages);
			if (WSMessage.ContainsError(messages.ToArray())) return null;

			var credentials = shipment.Credentials;
			var user = new UserSearch().FetchUserByUsernameAndAccessCode(credentials.Username, credentials.AccessCode) ?? new User();
			

			var s = new Shipment {User = user, TenantId = user.TenantId};

			if (!s.User.HasAccessTo(ViewCode.EstimateRate, ViewCode.RateAndSchedule))
			{
				messages.Add(WSMessage.ErrorMessage("User does not have rating permission."));
				return s;
			}

			s.SetCustomerAndResellerAdditions(shipment, messages);
			s.SetTransitInfo(shipment);
			if (WSMessage.ContainsError(messages.ToArray())) return s;

			s.SetItems(shipment);
			s.SetShippingServices(shipment);

			if (shipment.AdditionalInsuranceRequested)
			{
				if (!s.Customer.Rating.InsuranceEnabled)
				{
					messages.Add(WSMessage.ErrorMessage("Insurance requested but client is not setup to buy additional insurance."));
					s.DeclineInsurance = true;
				}

				s.DeclineInsurance = false;
			}
			else s.DeclineInsurance = true;



			return s;
		}

		public static List<Rate> GetShippingRates(this Shipment s, WSEnums.Mode[] modes, HttpContext context)
		{
			try
			{
                var pmaps = ProcessorVars.RegistryCache[s.User.TenantId].SmallPackagingMaps;
                var smaps = ProcessorVars.RegistryCache[s.User.TenantId].SmallPackageServiceMaps;
				var variables = ProcessorVars.IntegrationVariables[s.User.TenantId];
				var rater = new Rater2(variables.RatewareParameters, variables.CarrierConnectParameters,
				                      variables.FedExParameters, variables.UpsParameters, pmaps, smaps);
				var rates = rater.GetRates(s)
					.Where(r => r.Mode == ServiceMode.SmallPackage || r.Mode == ServiceMode.LessThanTruckload)
					.ToList();

				return rates;

			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, context);
				return new List<Rate>();
			}
		}

		public static List<WSRate> GetWsRates(this Shipment s, List<Rate> rates, HttpContext context)
		{
			try
			{
				var avaiableRates = new List<WSRate>();
				var customerRating = s.Customer.Rating;
				foreach (var rate in rates)
				{
					var vendor = rate.Mode == ServiceMode.LessThanTruckload
					             	? rate.LTLSellRate.VendorRating.Vendor
					             	: rate.Vendor;
					var item = new WSRate
						{
							Accessorials = rate.BaseRateCharges
							                   .Where(c => c.ChargeCode.Category == ChargeCodeCategory.Accessorial ||
							                               c.ChargeCode.Category == ChargeCodeCategory.Service)
							                   .Sum(c => c.AmountDue),
							AdditionalInsuranceAvailable = customerRating.InsuranceEnabled,
							AdditionalInsuranceCost = customerRating.InsuranceEnabled
								                          ? (rate.Charges
								                                 .FirstOrDefault(
									                                 r => r.ChargeCodeId == customerRating.InsuranceChargeCodeId) ??
								                             new Charge()).AmountDue
								                          : 0m,
							BillWeight = rate.BilledWeight.ToDouble(),
							RatedWeight = rate.RatedWeight.ToDouble(),
							RatedCubicFeet = rate.RatedCubicFeet.ToDouble(),
							CarrierKey = vendor.VendorNumber.ToInt(),
							CarrierName = vendor.Name,
							Freight =
								rate.Charges.Where(c => c.ChargeCode.Category == ChargeCodeCategory.Freight).Sum(c => c.AmountDue),
							Fuel = rate.Charges.Where(c => c.ChargeCode.Category == ChargeCodeCategory.Fuel).Sum(c => c.AmountDue),
							Mode = rate.Mode == ServiceMode.SmallPackage ? WSEnums.Mode.SmallPack : WSEnums.Mode.LTL,
							TransitTime = rate.TransitDays
						};
					avaiableRates.Add(item);
				}
				return avaiableRates;

			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, context);
				return new List<WSRate>();
			}
		}

		public static ShoppedRate CreateAndShoppedRate(this Shipment shipment, List<WSRate> rates,
		                                               ShoppedRateType shoppedRateType, HttpContext context)
		{
			var rate = rates.Any() ? rates.ToList().OrderBy(r => r.TotalCharge).First() : null;
			var shoppedRate = shipment.GetShoppedRate(rate, shoppedRateType);
			var view = new RateAndScheduleImmitationView(shipment.User);
			view.Save(shoppedRate);
			return view.ShoppedRate;
		}

		public static void UpdateShipmentToBook(this Shipment s, WSShipment shipment, List<Rate> rates,
		                                        List<WSMessage> messages, HttpContext context)
		{
			if (!s.User.HasAccessTo(ViewCode.RateAndSchedule))
			{
				messages.Add(WSMessage.ErrorMessage("User does not have booking permission."));
				return;
			}
			// coming in, shipment s should already have all rating parameters set including transit info
			shipment.ValidateBookingFields(messages, s.Customer);

			if (WSMessage.ContainsError(messages.ToArray())) return;

			s.UpdateShipmentFindAndSetRate(shipment, rates, messages, context);
			if (WSMessage.ContainsError(messages.ToArray())) return;

			shipment.CheckVarianceInSelectedAndBookedRates(messages);
		}

		public static void BookShipment(this Shipment s, ShoppedRate rate, List<WSMessage> messages, HttpContext context)
		{
			var view = new RateAndScheduleImmitationView(s.User);

			view.Save(s);
			if (view.Messages.Any(m => m.Type == WSEnums.MessageType.Error))
			{
				messages.AddRange(view.Messages);
				return;
			}

			if (view.Shipment.ServiceMode == ServiceMode.SmallPackage) view.BookPackageShipment(s, context);
			if (view.Messages.Any(m => m.Type == WSEnums.MessageType.Error))
			{
				messages.AddRange(view.Messages);
				return;
			}

			rate.Shipment = view.Shipment;
			rate.ReferenceNumber = view.Shipment != null ? view.Shipment.ShipmentNumber : string.Empty;
			view.Save(rate);

			view.ProcessesNotification(s, context);
		}

		public static WSEnums.DocumentType GetDocumentType(this string extension)
		{
			switch (extension)
			{
				case ".pdf":
					return WSEnums.DocumentType.Pdf;
				case ".gif":
					return WSEnums.DocumentType.Gif;
				default:
					return WSEnums.DocumentType.Png;
			}
		}


		private static void CheckVarianceInSelectedAndBookedRates(this WSShipment shipment, ICollection<WSMessage> messages)
		{
			if (shipment.SelectedRate.Accessorials != shipment.BookedRate.Accessorials)
				messages.Add(WSMessage.WarningMessage("Change in accessorials cost."));
			if (shipment.SelectedRate.AdditionalInsuranceCost != shipment.BookedRate.AdditionalInsuranceCost)
				messages.Add(WSMessage.WarningMessage("Change in additional insurance cost."));
			if (shipment.SelectedRate.Freight != shipment.BookedRate.Freight)
				messages.Add(WSMessage.WarningMessage("Change in freight cost."));
			if (shipment.SelectedRate.Fuel != shipment.BookedRate.Fuel)
				messages.Add(WSMessage.WarningMessage("Change in fuel cost."));
			if (shipment.SelectedRate.BillWeight.ToDecimal() != shipment.BookedRate.BillWeight.ToDecimal())
				messages.Add(WSMessage.WarningMessage("Change in specified billed weight."));
			if (shipment.SelectedRate.TransitTime != shipment.BookedRate.TransitTime)
				messages.Add(WSMessage.WarningMessage("Change in transit time."));
		}

		private static void UpdateShipmentFindAndSetRate(this Shipment s, WSShipment shipment, IEnumerable<Rate> rates,
		                                                 ICollection<WSMessage> messages, HttpContext context)
		{
			var rateFound = false;
			foreach (var rate in rates)
			{
				var vendor = rate.Mode == ServiceMode.LessThanTruckload
				             	? rate.LTLSellRate.VendorRating.Vendor
				             	: rate.Vendor;
				if (vendor.VendorNumber == shipment.SelectedRate.CarrierKey.ToString())
				{
					s.CreatedInError = false;
					s.DateCreated = DateTime.Now;
					s.ActualDeliveryDate = DateUtility.SystemEarliestDateTime;
					s.ActualPickupDate = DateUtility.SystemEarliestDateTime;
					s.Status = ShipmentStatus.Scheduled;
					s.Notes = new List<ShipmentNote>();
					s.Stops = new List<ShipmentLocation>();
					s.Equipments = new List<ShipmentEquipment>();
					s.Documents = new List<ShipmentDocument>();
					s.Assets = new List<ShipmentAsset>();
					s.Vendors = new List<ShipmentVendor>();
					s.AccountBuckets = new List<ShipmentAccountBucket>();
					s.AutoRatingAccessorials = new List<ShipmentAutoRatingAccessorial>();
					s.CustomerReferences = new List<ShipmentReference>();
					s.GeneralBolComments = string.Empty;
					s.CriticalBolComments = string.Empty;

					s.HazardousMaterial = false;
					s.HazardousMaterialContactEmail = string.Empty;
					s.HazardousMaterialContactMobile = string.Empty;
					s.HazardousMaterialContactName = string.Empty;
					s.HazardousMaterialContactPhone = string.Empty;
					s.ShipperBol = string.Empty;

					s.MiscField1 = string.Empty;
					s.MiscField2 = string.Empty;
					s.AccountBucketUnitId = default(long);

					s.DriverName = string.Empty;
					s.DriverPhoneNumber = string.Empty;
					s.DriverTrailerNumber = string.Empty;

					s.EmptyMileage = 0m;
					s.Mileage = 0m;
					s.MileageSourceId = default(long);

					s.ResellerAdditionId = s.Customer.Rating.ResellerAdditionId;
					s.BillReseller = s.Customer.Rating.ResellerAdditionId != default(long) && s.Customer.Rating.BillReseller;

					s.AccountBuckets.Add(new ShipmentAccountBucket
					                     	{
					                     		AccountBucket = s.Customer.DefaultAccountBucket,
					                     		Shipment = s,
					                     		TenantId = s.TenantId,
					                     		Primary = true,
					                     	});
					s.Prefix = s.Customer.Prefix;
					s.HidePrefix = s.Customer.HidePrefix;



					s.EstimatedDeliveryDate = rate.EstimatedDeliveryDate;
					s.EarlyPickup = string.Format("{0:00}:{1:00}", shipment.EarliestPickup.Get24Hour(), shipment.EarliestPickup.Minute);
					s.EarlyDelivery = TimeUtility.DefaultOpen;
					s.LatePickup = string.Format("{0:00}:{1:00}", shipment.LatestPickup.Get24Hour(), shipment.LatestPickup.Minute);
					s.LateDelivery = TimeUtility.DefaultClose;
					s.PurchaseOrderNumber = shipment.PurchaseOrder;
					s.ShipperReference = shipment.ReferenceNumber;
					s.Vendors.Add(new ShipmentVendor
					              	{
					              		Shipment = s,
					              		TenantId = s.TenantId,
					              		Vendor = vendor,
					              		Primary = true,
					              		FailureComments = string.Empty,
					              		ProNumber = string.Empty,
					              	});
					s.ServiceMode = rate.Mode;

					// sales rep
					var srtier = s.Customer.SalesRepresentative == null
									 ? null
									 : s.Customer.SalesRepresentative.ApplicableTier(s.DateCreated, s.ServiceMode, s.Customer);
					s.SalesRepresentative = s.Customer.SalesRepresentative;
					s.SalesRepresentativeCommissionPercent = srtier != null ? srtier.CommissionPercent : 0m;
					s.SalesRepAddlEntityCommPercent = s.Customer.SalesRepresentative == null
														  ? 0m
														  : s.Customer.SalesRepresentative.AdditionalEntityCommPercent;
					s.SalesRepAddlEntityName = s.Customer.SalesRepresentative == null
												   ? string.Empty
												   : s.Customer.SalesRepresentative.AdditionalEntityName;
					s.SalesRepMinCommValue = srtier != null ? srtier.FloorValue : 0m;
					s.SalesRepMaxCommValue = srtier != null ? srtier.CeilingValue : 0m;

					// charges
					s.Charges = rate.BaseRateCharges
						.Select(i => new ShipmentCharge
						             	{
						             		TenantId = s.TenantId,
						             		Shipment = s,
						             		ChargeCodeId = i.ChargeCodeId,
						             		UnitBuy = i.UnitBuy,
						             		UnitSell = i.UnitSell,
						             		UnitDiscount = i.UnitDiscount,
						             		Quantity = i.Quantity,
						             		Comment = string.Empty,
						             	})
						.ToList();


					// auto rating details
					s.OriginalRateValue = rate.OriginalRateValue;
					s.ShipmentAutorated = true;
					s.LineHaulProfitAdjustmentRatio = rate.FreightProfitAdjustment;
					s.FuelProfitAdjustmentRatio = rate.FuelProfitAdjustment;
					s.ServiceProfitAdjustmentRatio = rate.ServiceProfitAdjustment;
					s.AccessorialProfitAdjustmentRatio = rate.AccessorialProfitAdjustment;
					s.ResellerAdditionalAccessorialMarkup = rate.ResellerAccessorialMarkup;
					s.ResellerAdditionalAccessorialMarkupIsPercent = rate.ResellerAccessorialMarkupIsPercent;
					s.ResellerAdditionalServiceMarkup = rate.ResellerServiceMarkup;
					s.ResellerAdditionalServiceMarkupIsPercent = rate.ResellerServiceMarkupIsPercent;
					s.ResellerAdditionalFuelMarkup = rate.ResellerFuelMarkup;
					s.ResellerAdditionalFuelMarkupIsPercent = rate.ResellerFuelMarkupIsPercent;
					s.ResellerAdditionalFreightMarkup = rate.ResellerFreightMarkup;
					s.ResellerAdditionalFreightMarkupIsPercent = rate.ResellerFreightMarkupIsPercent;
					s.DirectPointRate = rate.DirectPointRate;
					s.SmallPackType = rate.SmallPackServiceType ?? string.Empty;
					s.SmallPackageEngine = rate.SmallPackEngine;
					s.ApplyDiscount = rate.ApplyDiscount;
					s.BilledWeight = rate.BilledWeight;
					s.RatedCubicFeet = rate.RatedCubicFeet;
					s.RatedWeight = rate.RatedWeight;
					s.RatedPcf = rate.RatedPcf;
				    s.GuaranteedDeliveryServiceTime = string.Empty;
				    s.IsGuaranteedDeliveryService = false;
                    s.GuaranteedDeliveryServiceRateType = RateType.Flat;
                    s.GuaranteedDeliveryServiceRate = default(decimal);

					var tier = rate.DiscountTier ?? new DiscountTier();
					s.VendorDiscountPercentage = tier.DiscountPercent;
					s.VendorFreightFloor = tier.FloorValue;
					s.VendorFreightCeiling = tier.CeilingValue;
					s.VendorFuelPercent = rate.FuelMarkupPercent;

					var sellRate = rate.LTLSellRate ?? new LTLSellRate();

					s.CustomerFreightMarkupValue = rate.MarkupValue;
					s.CustomerFreightMarkupPercent = rate.MarkupPercent;
					s.UseLowerCustomerFreightMarkup = sellRate.UseMinimum;
					s.VendorRatingOverrideAddress = sellRate.VendorRating == null
					                                	? string.Empty
					                                	: sellRate.VendorRating.OverrideAddress;

					// update processing
					Rater2.UpdateShipmentAutoRatingAccessorilas(s, sellRate);
					if (s.ServiceMode == ServiceMode.LessThanTruckload)
					{
						var rule = rate.CFCRule ?? new LTLCubicFootCapacityRule();
						if (!rule.KeyLoaded) rule = null;
						Rater2.UpdateShipmentItemRatedCalculations(s, rule, tier);

						// update terminal information
						try
						{
							var variables = ProcessorVars.IntegrationVariables[s.TenantId];
							var info = new CarrierConnect(variables.CarrierConnectParameters); //NOTE: without country map as country id is set below on return

							var terminals = info.GetLTLTerminalInfo(vendor.Scac, new[] { rate.OriginTerminalCode, rate.DestinationTerminalCode });

							s.OriginTerminal = terminals.FirstOrDefault(t => t.Code == rate.OriginTerminalCode) ?? new LTLTerminalInfo();
							s.OriginTerminal.Shipment = s;
							s.OriginTerminal.TenantId = s.TenantId;
							s.OriginTerminal.CountryId = s.Origin.CountryId;
							s.DestinationTerminal = terminals.FirstOrDefault(t => t.Code == rate.DestinationTerminalCode) ?? new LTLTerminalInfo();
							s.DestinationTerminal.Shipment = s;
							s.DestinationTerminal.TenantId = s.TenantId;
							s.DestinationTerminal.CountryId = s.Destination.CountryId;
						}
						catch (Exception ex)
						{
							ErrorLogger.LogError(ex, context);
						}
					}

					rateFound = true;
					var customerRating = s.Customer.Rating;
					shipment.BookedRate =
						new WSRate
							{
								Accessorials = rate.BaseRateCharges
									.Where(c => c.ChargeCode.Category == ChargeCodeCategory.Accessorial ||
									            c.ChargeCode.Category == ChargeCodeCategory.Service)
									.Sum(c => c.AmountDue),
								AdditionalInsuranceAvailable = customerRating.InsuranceEnabled,
								AdditionalInsuranceCost = customerRating.InsuranceEnabled
								                          	? (rate.Charges
								                          	   	.FirstOrDefault(
								                          	   		r => r.ChargeCodeId == customerRating.InsuranceChargeCodeId) ??
								                          	   new Charge()).AmountDue
								                          	: 0m,
								BillWeight = rate.BilledWeight.ToDouble(),
								RatedWeight = rate.RatedWeight.ToDouble(),
								RatedCubicFeet = rate.RatedCubicFeet.ToDouble(),
								CarrierKey = vendor.VendorNumber.ToInt(),
								CarrierName = vendor.Name,
								Freight =
									rate.BaseRateCharges.Where(c => c.ChargeCode.Category == ChargeCodeCategory.Freight).Sum(c => c.AmountDue),
								Fuel = rate.BaseRateCharges.Where(c => c.ChargeCode.Category == ChargeCodeCategory.Fuel).Sum(c => c.AmountDue),
								Mode = rate.Mode == ServiceMode.SmallPackage ? WSEnums.Mode.SmallPack : WSEnums.Mode.LTL,
								TransitTime = rate.TransitDays
							};

					break;
				}
			}
			if (!rateFound)
				messages.Add(WSMessage.ErrorMessage("Selected Shipment Rate is unavailable at this time."));
		}

		private static void SetShippingServices(this Shipment s, WSShipment shipment)
		{
			var search = new ServiceSearch();
			s.Services = new List<ShipmentService>();
			if (shipment.RequestedServices != null)
				foreach (var option in shipment.RequestedServices)
				{
					s.Services.Add(new ShipmentService
					               	{
					               		TenantId = s.TenantId,
					               		ServiceId = search.FetchServiceIdByCode(option.Key, s.TenantId),
					               		Shipment = s,
					               	});
				}
		}

		private static void SetItems(this Shipment s, WSShipment shipment)
		{
			foreach (var item in shipment.Items)
			{
				var convertDims = item.DimsUnit == WSEnums.DimensionUnit.Cm;
				var convertWeight = item.WeightUnit == WSEnums.WeightUnit.Kg;
				var unit = new ShipmentItem
				           	{
				           		TenantId = s.TenantId,
				           		Shipment = s,
				           		ActualFreightClass = item.FreightClass.Key.GetDouble(),
				           		ActualHeight = item.Height.GetDimInInches(convertDims),
				           		ActualLength = item.Length.GetDimInInches(convertDims),
				           		ActualWidth = item.Length.GetDimInInches(convertDims),
				           		ActualWeight = item.Weight.GetWeightInLb(convertWeight).ToDecimal(),
				           		IsStackable = item.IsStackable,
				           		NMFCCode = item.NationalMotorFreightClassification,
				           		HTSCode = item.HarmonizedTariffSchedule,
				           		PackageTypeId = item.Packaging.Key.ToLong(),
				           		Quantity = item.Quantity,
				           		PieceCount = item.SaidToContain,
				           		Description = item.Description,
				           		Comment = string.Empty,
				           		Delivery = 1,
				           		Pickup = 0,
				           		RatedFreightClass = item.FreightClass.Key.GetDouble(),
				           		Value = item.DeclaredValue,
				           	};


				s.Items.Add(unit);
			}
		}

		private static void SetTransitInfo(this Shipment s, WSShipment shipment)
		{
			var search = new CountrySearch();

			s.Origin = new ShipmentLocation
			           	{
			           		Shipment = s,
			           		TenantId = s.TenantId,
			           		Description = shipment.Origin.Name,
			           		Street1 = shipment.Origin.Street,
			           		City = shipment.Origin.City,
			           		State = shipment.Origin.State,
			           		PostalCode = shipment.Origin.PostalCode,

			           		StopOrder = 0,

			           		SpecialInstructions = shipment.Origin.SpecialInstructions,
			           		GeneralInfo = string.Empty,
			           		Direction = string.Empty,
			           		CountryId = shipment.Origin.Country == null
			           		            	? default(long)
			           		            	: search.FetchCountryIdByCode(shipment.Origin.Country.Key),
			           		AppointmentDateTime = DateUtility.SystemEarliestDateTime,
			           		Contacts = new List<ShipmentContact>
			           		           	{
			           		           		new ShipmentContact
			           		           			{
			           		           				Primary = true,
			           		           				Phone = shipment.Origin.Phone,
			           		           				Fax = shipment.Origin.Fax,
			           		           				Email = shipment.Origin.Email,
			           		           				Name = shipment.Origin.Contact,
			           		           				TenantId = s.TenantId,
			           		           				ContactTypeId = s.Tenant.DefaultSchedulingContactTypeId
			           		           			}
			           		           	},
			           	};
			s.Origin.Contacts[0].Location = s.Origin;

			s.Destination = new ShipmentLocation
			                	{
			                		Shipment = s,
			                		TenantId = s.TenantId,
			                		Description = shipment.Destination.Name,
			                		Street1 = shipment.Destination.Street,
			                		City = shipment.Destination.City,
			                		State = shipment.Destination.State,
			                		PostalCode = shipment.Destination.PostalCode,

			                		StopOrder = 1,

			                		SpecialInstructions = shipment.Destination.SpecialInstructions,
									GeneralInfo = string.Empty,
									Direction = string.Empty,
			                		CountryId = shipment.Destination.Country == null
			                		            	? default(long)
			                		            	: search.FetchCountryIdByCode(shipment.Destination.Country.Key),
									AppointmentDateTime = DateUtility.SystemEarliestDateTime,
			                		Contacts = new List<ShipmentContact>
			                		           	{
			                		           		new ShipmentContact
			                		           			{
			                		           				Primary = true,
			                		           				Phone = shipment.Destination.Phone,
			                		           				Fax = shipment.Destination.Fax,
			                		           				Email = shipment.Destination.Email,
			                		           				Name = shipment.Destination.Contact,
			                		           				TenantId = s.TenantId,
			                		           				ContactTypeId = s.Tenant.DefaultSchedulingContactTypeId
			                		           			}
			                		           	},
			                	};
			s.Destination.Contacts[0].Location = s.Destination;


			s.DesiredPickupDate = shipment.ShipmentDate;
			s.EstimatedDeliveryDate = shipment.ShipmentDate;
		}

		private static void SetCustomerAndResellerAdditions(this Shipment s, WSShipment shipment,
		                                                    ICollection<WSMessage> messages)
		{
			var search = new CustomerSearch();
			var customer = search.FetchCustomerByCommunicationConnectGuid(shipment.Credentials.AccessKey, s.User.TenantId);
			if (customer == null)
				messages.Add(WSMessage.ErrorMessage("Invalid access key or access key client not found."));
			else if (!customer.Communication.EnableWebServiceAPIAccess)
				messages.Add(WSMessage.ErrorMessage("Client access key not authorized for creating shipments via web services."));

			s.Customer = customer;
			var rating = customer == null ? null : customer.Rating;
			s.ResellerAdditionId = rating == null ? default(long) : rating.ResellerAdditionId;
			s.BillReseller = rating != null && rating.ResellerAdditionId != default(long) && rating.BillReseller;
		}

		private static void ValidateRatingFields(this WSShipment shipment, ICollection<WSMessage> messages)
		{
			if (shipment.Credentials.AccessKey == Guid.Empty)
				messages.Add(WSMessage.ErrorMessage("Invalid Access Key."));

			if (shipment.Origin == null)
				messages.Add(WSMessage.ErrorMessage("Missing Origin Information."));
			else
			{
				if (string.IsNullOrEmpty(shipment.Origin.PostalCode))
					messages.Add(WSMessage.ErrorMessage("Missing Origin Postal Code."));
				if (shipment.Origin.Country == null)
					messages.Add(WSMessage.ErrorMessage("Missing Origin Country."));
			}

			if (shipment.Destination == null)
				messages.Add(WSMessage.ErrorMessage("Missing Destination Information."));
			else
			{
				if (string.IsNullOrEmpty(shipment.Destination.PostalCode))
					messages.Add(WSMessage.ErrorMessage("Missing Destination Postal Code."));
				if (shipment.Destination.Country == null)
					messages.Add(WSMessage.ErrorMessage("Missing Destination Country."));
			}

			if (shipment.Items == null)
				messages.Add(WSMessage.ErrorMessage("Item(s) object missing."));
			else if (!shipment.Items.Any())
				messages.Add(WSMessage.ErrorMessage("There must be at least one item to get a shipment rate."));
			else
			{
				var checkStc = shipment.Origin != null && shipment.Origin.Country != null && shipment.Destination != null
				               && shipment.Destination.Country != null && shipment.IsInternational();
				foreach (var item in shipment.Items)
				{
					if (item.FreightClass == null)
						messages.Add(WSMessage.ErrorMessage("Invalid item freight class."));
					if (item.Packaging == null || !(new PackageType(item.Packaging.Key.ToLong()).KeyLoaded))
						messages.Add(WSMessage.ErrorMessage("Invalid item Packaging."));
					//if (item.CommodityType == null)
					//    messages.Add(WSMessage.ErrorMessage("Invalid item commodity type."));
					if (item.Weight <= 0)
						messages.Add(WSMessage.ErrorMessage("Invalid item weight."));
					if (string.IsNullOrEmpty(item.Description))
						messages.Add(WSMessage.ErrorMessage("Invalid item description."));
					if (item.Height <= 0)
						messages.Add(WSMessage.ErrorMessage("Invalid item height."));
					if (item.Width <= 0)
						messages.Add(WSMessage.ErrorMessage("Invalid item width."));
					if (item.Length <= 0)
						messages.Add(WSMessage.ErrorMessage("Invalid item length."));
					if (item.Quantity <= 0 || item.Quantity > 20)
						messages.Add(WSMessage.ErrorMessage("Invalid item quantity."));
					if (checkStc && item.SaidToContain <= 0)
						messages.Add(WSMessage.ErrorMessage("Invalid item said to contain."));
					if (item.NationalMotorFreightClassification == null) item.NationalMotorFreightClassification = string.Empty;
					if (item.HarmonizedTariffSchedule == null) item.HarmonizedTariffSchedule = string.Empty;
				}
			}


			if (shipment.ShipmentDate.Date < DateTime.Now.Date || shipment.ShipmentDate.Date > DateTime.Now.AddDays(7).Date)
				messages.Add(WSMessage.ErrorMessage("Invalid shipment date. Date range Today to Today+7days"));

			if (shipment.RequestedModes == null)
				messages.Add(WSMessage.ErrorMessage("Requested modes cannot be null."));
			else
			{
				if (shipment.RequestedModes.Contains(WSEnums.Mode.SmallPack) && shipment.PackageShipmentDetails == null)
					messages.Add(WSMessage.ErrorMessage("Package Shipment Details missing."));
			}
		}

		private static void ValidateBookingFields(this WSShipment shipment, ICollection<WSMessage> messages, Customer customer)
		{
			shipment.ValidateRatingFields(messages);
			if (WSMessage.ContainsError(messages.ToArray())) return;

			if (shipment.SelectedRate == null)
				messages.Add(WSMessage.ErrorMessage("Selected shipping rate not set."));

			if (string.IsNullOrEmpty(shipment.Origin.Name))
				messages.Add(WSMessage.ErrorMessage("Origin company name is missing."));
			if (string.IsNullOrEmpty(shipment.Origin.Street))
				messages.Add(WSMessage.ErrorMessage("Origin street is missing."));
			if (string.IsNullOrEmpty(shipment.Origin.Phone))
				messages.Add(WSMessage.ErrorMessage("Origin phone is missing."));
			if (string.IsNullOrEmpty(shipment.Origin.Contact))
				messages.Add(WSMessage.ErrorMessage("Origin contact is missing."));
			if (string.IsNullOrEmpty(shipment.Destination.Name))
				messages.Add(WSMessage.ErrorMessage("Destination company name is missing."));
			if (string.IsNullOrEmpty(shipment.Destination.Street))
				messages.Add(WSMessage.ErrorMessage("Destination street is missing."));
			if (string.IsNullOrEmpty(shipment.Destination.Phone))
				messages.Add(WSMessage.ErrorMessage("Destination phone is missing."));
			if (string.IsNullOrEmpty(shipment.Destination.Contact))
				messages.Add(WSMessage.ErrorMessage("Destination contact is missing."));

			if (customer.ShipmentRequiresPurchaseOrderNumber && string.IsNullOrEmpty(shipment.PurchaseOrder))
				messages.Add(WSMessage.ErrorMessage("Purchase Order is missing."));

			if (customer.InvoiceRequiresShipperReference && string.IsNullOrEmpty(shipment.ReferenceNumber))
				messages.Add(WSMessage.ErrorMessage("Shipper Reference is missing."));

			if (shipment.EarliestPickup == null)
				messages.Add(WSMessage.ErrorMessage("Earliest pick up object missing."));
			else
			{
				if (shipment.EarliestPickup.Hour < 1 || shipment.EarliestPickup.Hour > 12)
					messages.Add(WSMessage.ErrorMessage("Invalid earliest pickup hour."));
				if (shipment.EarliestPickup.Minute < 0 || shipment.EarliestPickup.Minute > 59)
					messages.Add(WSMessage.ErrorMessage("Invalid earliest pickup minute."));
				if (shipment.EarliestPickup.Second < 0 || shipment.EarliestPickup.Second > 59)
					messages.Add(WSMessage.ErrorMessage("Invalid earliest pickup second."));
			}

			if (shipment.LatestPickup == null)
				messages.Add(WSMessage.ErrorMessage("Latest pick up object missing."));
			else
			{
				if (shipment.LatestPickup.Hour < 1 || shipment.LatestPickup.Hour > 12)
					messages.Add(WSMessage.ErrorMessage("Invalid latest pickup hour."));
				if (shipment.LatestPickup.Minute < 0 || shipment.LatestPickup.Minute > 59)
					messages.Add(WSMessage.ErrorMessage("Invalid latest pickup minute."));
				if (shipment.LatestPickup.Second < 0 || shipment.LatestPickup.Second > 59)
					messages.Add(WSMessage.ErrorMessage("Invalid latest pickup second."));
			}

			if (shipment.RequestedModes.Contains(WSEnums.Mode.SmallPack) &&
			    shipment.PackageShipmentDetails != null)
			{
				if (shipment.PackageShipmentDetails.DestinationControlSpecified &&
				    !shipment.PackageShipmentDetails.ExportDetailSpecified)
					messages.Add(WSMessage.ErrorMessage("Destination control cannot be specified independent of export details."));

				if (shipment.PackageShipmentDetails.DutiesPayor == WSEnums.DutiesPayor.RECIPIENT &&
				    string.IsNullOrEmpty(shipment.PackageShipmentDetails.PaymentAccountNumber))
					messages.Add(
						WSMessage.ErrorMessage(
							"A [carrier] applicable payment account is required when Recipient is responsible custom duties."));

			}
		}

		private static double GetDouble(this object value)
		{
			double x;
			return value == null ? 0 : double.TryParse(value.ToString(), out x) ? x : 0;
		}

		public static int GetInt(this object value)
		{
			int x;
			return value == null ? 0 : int.TryParse(value.ToString(), out x) ? x : 0;
		}

		private static int GetDimInInches(this int value, bool convertFromCm)
		{
			//NOTE: 2.20462262 cm = 1 inch
			return convertFromCm ? (int) (value/2.20462262) : value;
		}

		private static float GetWeightInLb(this double value, bool convertFromKg)
		{
			// NOTE: 2.20462262 kg = 1 lb
			return convertFromKg ? (float) (2.20462262*value) : (float) value.GetDouble();
		}

		private static bool IsInternational(this WSShipment shipment)
		{
			return shipment.Origin.Country.Key != shipment.Destination.Country.Key;
		}

		private static int Get24Hour(this WSTime time)
		{
			var hour = time.Hour;
			if (time.TimeUnit == WSEnums.TimeUnit.PM && hour < 12)
				hour += 12;
			if (time.TimeUnit == WSEnums.TimeUnit.AM && hour == 12)
				hour = 0;
			return hour;
		}

		private static ShoppedRate GetShoppedRate(this Shipment shipment, WSRate rate,
		                                          ShoppedRateType shoppedRateType)
		{
			var sr = new ShoppedRate
			         	{
			         		TenantId = shipment.TenantId,
			         		Shipment = shipment,
			         		ReferenceNumber = shipment.ShipmentNumber.GetString(),
			         		Type = shoppedRateType,
			         		NumberLineItems = shipment.Items.Count,
			         		Quantity = shipment.Items.Sum(i => i.Quantity),
			         		Weight = shipment.Items.Sum(i => i.ActualWeight),
			         		HighestFreightClass = shipment.Items.Any() ? shipment.Items.Max(i => i.ActualFreightClass) : 0,
			         		MostSignificantFreightClass = shipment.Items.Any()
			         		                              	? shipment.Items
			         		                              	  	.GroupBy(i => i.ActualFreightClass)
			         		                              	  	.Select(g => new {FreightClass = g.Key, Items = g})
			         		                              	  	.OrderByDescending(a => a.Items.Count())
			         		                              	  	.ThenByDescending(a => a.FreightClass)
			         		                              	  	.First().FreightClass
			         		                              	: 0,
			         		LowestTotalCharge = rate == null ? 0 : rate.TotalCharge,
			         		DateCreated = DateTime.Now,
			         		Customer = shipment.Customer,
			         		User = shipment.User,
			         		OriginCountryId = shipment.Origin.CountryId,
			         		OriginCity = shipment.Origin.City.GetString(),
			         		OriginPostalCode = shipment.Origin.PostalCode.GetString(),
			         		OriginState = shipment.Origin.State.GetString(),
			         		DestinationCountryId = shipment.Destination.CountryId,
			         		DestinationCity = shipment.Destination.City.GetString(),
			         		DestinationPostalCode = shipment.Destination.PostalCode.GetString(),
			         		DestinationState = shipment.Destination.State.GetString(),
							VendorName = rate == null ? string.Empty : rate.CarrierName,
							VendorNumber = rate == null ? string.Empty : rate.CarrierKey.ToString(),
			         		Services = new List<ShoppedRateService>()
			         	};

			foreach (var service in shipment.Services)
				sr.Services.Add(new ShoppedRateService
				                	{TenantId = shipment.TenantId, ServiceId = service.ServiceId, ShoppedRate = sr,});

			return sr;
		}
	}
}
