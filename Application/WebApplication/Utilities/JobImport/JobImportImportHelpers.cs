﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;

namespace LogisticsPlus.Eship.WebApplication.Utilities.JobImport
{
    // Job
    public struct JobHeaderRow
    {
        private List<string> _lines { get; set; }

        public JobHeaderRow(List<string> headerLine) : this()
        {
            _lines = headerLine;
        }

        public string JobIdentifier
        {
            get { return _lines[0]; }
        }

        public string ExternalReference1
        {
            get { return _lines[1]; }
        }

        public string ExternalReference2
        {
            get { return _lines[2]; }
        }

        public string CustomerCode
        {
            get { return _lines[3]; }
        }
    }


    // Shipment
    public struct ShipmentRow
    {
        private List<string> _lines { get; set; }

        public ShipmentRow(List<string> headerLine) : this()
        {
            _lines = headerLine;
        }

        public string JobIdentifier
        {
            get { return _lines[0]; }
        }

        public string ShipmentIdentifier
        {
            get { return _lines[1]; }
        }

        public string ShipperReference
        {
            get { return _lines[2]; }
        }

        public string PurchaseOrderNumber
        {
            get { return _lines[3]; }
        }

        public bool HazardousMaterial
        {
            get { return _lines[4].ToBoolean(); }
        }

        public string HazardousMaterialContactName
        {
            get { return _lines[5]; }
        }

        public string HazardousMaterialContactPhone
        {
            get { return _lines[6]; }
        }

        public string HazardousMaterialContactMobile
        {
            get { return _lines[7]; }
        }

        public string HazardousMaterialContactEmail
        {
            get { return _lines[8]; }
        }

        public DateTime DesiredPickupDate
        {
            get { return _lines[9].ToDateTime(); }
        }

        public string EarlyPickup
        {
            get { return _lines[10]; }
        }

        public string LatePickup
        {
            get { return _lines[11]; }
        }

        public DateTime EstimatedDeliveryDate
        {
            get { return _lines[12].ToDateTime(); }
        }

        public string EarlyDelivery
        {
            get { return _lines[13]; }
        }

        public string LateDelivery
        {
            get { return _lines[14]; }
        }

        public DateTime ActualPickupDate
        {
            get { return _lines[15].ToDateTime(); }
        }

        public string ActualPickupTime
        {
            get { return _lines[16]; }
        }

        public DateTime ActualDeliveryDate
        {
            get { return _lines[17].ToDateTime(); }
        }

        public string ActualDeliveryTime
        {
            get { return _lines[18]; }
        }

        public ServiceMode ServiceMode
        {
            get { return _lines[19].ToEnum<ServiceMode>(); }
        }

        public bool DeclineAdditionalInsurance
        {
            get { return _lines[20].ToBoolean(); }
        }

        public string MiscField1
        {
            get { return _lines[21]; }
        }

        public string MiscField2
        {
            get { return _lines[22]; }
        }

        public string OriginStreet1
        {
            get { return _lines[23]; }
        }

        public string OriginStreet2
        {
            get { return _lines[24]; }
        }

        public string OriginCity
        {
            get { return _lines[25]; }
        }

        public string OriginPostalCode
        {
            get { return _lines[26]; }
        }

        public string OriginState
        {
            get { return _lines[27]; }
        }

        public string OriginCountryCode
        {
            get { return _lines[28]; }
        }

        public string OriginName
        {
            get { return _lines[29]; }
        }

        public string OriginSpecialInstructions
        {
            get { return _lines[30]; }
        }

        public DateTime OriginAppointmentDate
        {
            get { return _lines[31].ToDateTime(); }
        }

        public string OriginAppointmentTime
        {
            get { return _lines[32]; }
        }

        public string OriginPrimaryContactName
        {
            get { return _lines[33]; }
        }

        public string OriginPrimaryContactPhone
        {
            get { return _lines[34]; }
        }

        public string OriginPrimaryContactMobile
        {
            get { return _lines[35]; }
        }

        public string OriginPrimaryContactFax
        {
            get { return _lines[36]; }
        }

        public string OriginPrimaryContactEmail
        {
            get { return _lines[37]; }
        }

        public string OriginPrimaryContactType
        {
            get { return _lines[38]; }
        }

        public string OriginPrimaryContactComment
        {
            get { return _lines[39]; }
        }

        public string DestinationStreet1
        {
            get { return _lines[40]; }
        }

        public string DestinationStreet2
        {
            get { return _lines[41]; }
        }

        public string DestinationCity
        {
            get { return _lines[42]; }
        }

        public string DestinationPostalCode
        {
            get { return _lines[43]; }
        }

        public string DestinationState
        {
            get { return _lines[44]; }
        }

        public string DestinationCountryCode
        {
            get { return _lines[45]; }
        }

        public string DestinationName
        {
            get { return _lines[46]; }
        }

        public string DestinationSpecialInstructions
        {
            get { return _lines[47]; }
        }

        public DateTime DestinationAppointmentDate
        {
            get { return _lines[48].ToDateTime(); }
        }

        public string DestinationAppointmentTime
        {
            get { return _lines[49]; }
        }

        public string DestinationPrimaryContactName
        {
            get { return _lines[50]; }
        }

        public string DestinationPrimaryContactPhone
        {
            get { return _lines[51]; }
        }

        public string DestinationPrimaryContactMobile
        {
            get { return _lines[52]; }
        }

        public string DestinationPrimaryContactFax
        {
            get { return _lines[53]; }
        }

        public string DestinationPrimaryContactEmail
        {
            get { return _lines[54]; }
        }

        public string DestinationPrimaryContactType
        {
            get { return _lines[55]; }
        }

        public string DestinationPrimaryContactComment
        {
            get { return _lines[56]; }
        }

        public bool IsPartialTruckload
        {
            get { return _lines[57].ToBoolean(); }
        }

        public int JobStep
        {
            get { return _lines[58].ToInt(); }
        }

        public string GeneralBolComments
        {
            get { return _lines[59]; }
        }

        public string CriticalBolComments
        {
            get { return _lines[60]; }
        }

        public string Prefix
        {
            get { return _lines[61]; }
        }
    }

    public struct ShipmentItemRow
    {
        private List<string> _lines { get; set; }

        public ShipmentItemRow(List<string> headerLine) : this()
        {
            _lines = headerLine;
        }

        public string ShipmentIdentifier
        {
            get { return _lines[0]; }
        }

        public string Description
        {
            get { return _lines[1]; }
        }

        public string Comment
        {
            get { return _lines[2]; }
        }

        public double FreightClass
        {
            get { return _lines[3].ToDouble(); }
        }

        public decimal ActualWeight
        {
            get { return _lines[4].ToDecimal(); }
        }

        public decimal ActualLength
        {
            get { return _lines[5].ToDecimal(); }
        }

        public decimal ActualWidth
        {
            get { return _lines[6].ToDecimal(); }
        }

        public decimal ActualHeight
        {
            get { return _lines[7].ToDecimal(); }
        }

        public int Quantity
        {
            get { return _lines[8].ToInt(); }
        }

        public int PieceCount
        {
            get { return _lines[9].ToInt(); }
        }

        public bool IsStackable
        {
            get { return _lines[10].ToBoolean(); }
        }

        public bool HazardousMaterial
        {
            get { return _lines[11].ToBoolean(); }
        }

        public string PackageTypeName
        {
            get { return _lines[12]; }
        }

        public string NMFCCode
        {
            get { return _lines[13]; }
        }

        public string HTSCode
        {
            get { return _lines[14]; }
        }

        public decimal Value
        {
            get { return _lines[15].ToDecimal(); }
        }
    }

    public struct ShipmentChargeRow
    {
        private List<string> _lines { get; set; }

        public ShipmentChargeRow(List<string> headerLine) : this()
        {
            _lines = headerLine;
        }

        public string ShipmentIdentifier
        {
            get { return _lines[0]; }
        }

        public string ChargeCode
        {
            get { return _lines[1]; }
        }

        public int Quantity
        {
            get { return _lines[2].ToInt(); }
        }

        public decimal UnitBuy
        {
            get { return _lines[3].ToDecimal(); }
        }

        public decimal UnitSell
        {
            get { return _lines[4].ToDecimal(); }
        }

        public decimal UnitDiscount
        {
            get { return _lines[5].ToDecimal(); }
        }

        public string Comment
        {
            get { return _lines[6]; }
        }
    }

    public struct ShipmentVendorRow
    {
        private List<string> _lines { get; set; }

        public ShipmentVendorRow(List<string> headerLine) : this()
        {
            _lines = headerLine;
        }

        public string ShipmentIdentifier
        {
            get { return _lines[0]; }
        }

        public string VendorNumber
        {
            get { return _lines[1]; }
        }

        public string ProNumber
        {
            get { return _lines[2]; }
        }

        public string FailureComments
        {
            get { return _lines[3]; }
        }

        public bool IsPrimary
        {
            get { return _lines[4].ToBoolean(); }
        }
    }

    public struct ShipmentCustomerReference
    {
        private List<string> _lines { get; set; }

        public ShipmentCustomerReference(List<string> headerLine) : this()
        {
            _lines = headerLine;
        }

        public string ShipmentIdentifier
        {
            get { return _lines[0]; }
        }

        public string Name
        {
            get { return _lines[1]; }
        }

        public string Value
        {
            get { return _lines[2]; }
        }

        public bool DisplayOnOrigin
        {
            get { return _lines[3].ToBoolean(); }
        }

        public bool DisplayOnDestination
        {
            get { return _lines[4].ToBoolean(); }
        }

    }

    public struct ShipmentEquipmentRow
    {
        private List<string> _lines { get; set; }

        public ShipmentEquipmentRow(List<string> headerLine) : this()
        {
            _lines = headerLine;
        }

        public string ShipmentIdentifier
        {
            get { return _lines[0]; }
        }

        public string EquipmentCode
        {
            get { return _lines[1]; }
        }
    }

    public struct ShipmentServiceRow
    {
        private List<string> _lines { get; set; }

        public ShipmentServiceRow(List<string> headerLine) : this()
        {
            _lines = headerLine;
        }

        public string ShipmentIdentifier
        {
            get { return _lines[0]; }
        }

        public string ServiceCode
        {
            get { return _lines[1]; }
        }
    }


    // Load Order
    public struct LoadOrderRow
    {
        private List<string> _lines { get; set; }

        public LoadOrderRow(List<string> headerLine) : this()
        {
            _lines = headerLine;
        }

        public string JobIdentifier
        {
            get { return _lines[0]; }
        }

        public string LoadOrderIdentifier
        {
            get { return _lines[1]; }
        }

        public string ShipperReference
        {
            get { return _lines[2]; }
        }

        public string PurchaseOrderNumber
        {
            get { return _lines[3]; }
        }

        public bool HazardousMaterial
        {
            get { return _lines[4].ToBoolean(); }
        }

        public string HazardousMaterialContactName
        {
            get { return _lines[5]; }
        }

        public string HazardousMaterialContactPhone
        {
            get { return _lines[6]; }
        }

        public string HazardousMaterialContactMobile
        {
            get { return _lines[7]; }
        }

        public string HazardousMaterialContactEmail
        {
            get { return _lines[8]; }
        }

        public DateTime DesiredPickupDate
        {
            get { return _lines[9].ToDateTime(); }
        }

        public string EarlyPickup
        {
            get { return _lines[10]; }
        }

        public string LatePickup
        {
            get { return _lines[11]; }
        }

        public DateTime EstimatedDeliveryDate
        {
            get { return _lines[12].ToDateTime(); }
        }

        public string EarlyDelivery
        {
            get { return _lines[13]; }
        }

        public string LateDelivery
        {
            get { return _lines[14]; }
        }

        public ServiceMode ServiceMode
        {
            get { return _lines[15].ToEnum<ServiceMode>(); }
        }

        public bool DeclineAdditionalInsurance
        {
            get { return _lines[16].ToBoolean(); }
        }

        public string MiscField1
        {
            get { return _lines[17]; }
        }

        public string MiscField2
        {
            get { return _lines[18]; }
        }

        public string OriginStreet1
        {
            get { return _lines[19]; }
        }

        public string OriginStreet2
        {
            get { return _lines[20]; }
        }

        public string OriginCity
        {
            get { return _lines[21]; }
        }

        public string OriginPostalCode
        {
            get { return _lines[22]; }
        }

        public string OriginState
        {
            get { return _lines[23]; }
        }

        public string OriginCountryCode
        {
            get { return _lines[24]; }
        }

        public string OriginName
        {
            get { return _lines[25]; }
        }

        public string OriginSpecialInstructions
        {
            get { return _lines[26]; }
        }

        public DateTime OriginAppointmentDate
        {
            get { return _lines[27].ToDateTime(); }
        }

        public string OriginAppointmentTime
        {
            get { return _lines[28]; }
        }

        public string OriginPrimaryContactName
        {
            get { return _lines[29]; }
        }

        public string OriginPrimaryContactPhone
        {
            get { return _lines[30]; }
        }

        public string OriginPrimaryContactMobile
        {
            get { return _lines[31]; }
        }

        public string OriginPrimaryContactFax
        {
            get { return _lines[32]; }
        }

        public string OriginPrimaryContactEmail
        {
            get { return _lines[33]; }
        }

        public string OriginPrimaryContactType
        {
            get { return _lines[34]; }
        }

        public string OriginPrimaryContactComment
        {
            get { return _lines[35]; }
        }

        public string DestinationStreet1
        {
            get { return _lines[36]; }
        }

        public string DestinationStreet2
        {
            get { return _lines[37]; }
        }

        public string DestinationCity
        {
            get { return _lines[38]; }
        }

        public string DestinationPostalCode
        {
            get { return _lines[39]; }
        }

        public string DestinationState
        {
            get { return _lines[40]; }
        }

        public string DestinationCountryCode
        {
            get { return _lines[41]; }
        }

        public string DestinationName
        {
            get { return _lines[42]; }
        }

        public string DestinationSpecialInstructions
        {
            get { return _lines[43]; }
        }

        public DateTime DestinationAppointmentDate
        {
            get { return _lines[44].ToDateTime(); }
        }

        public string DestinationAppointmentTime
        {
            get { return _lines[45]; }
        }

        public string DestinationPrimaryContactName
        {
            get { return _lines[46]; }
        }

        public string DestinationPrimaryContactPhone
        {
            get { return _lines[47]; }
        }

        public string DestinationPrimaryContactMobile
        {
            get { return _lines[48]; }
        }

        public string DestinationPrimaryContactFax
        {
            get { return _lines[49]; }
        }

        public string DestinationPrimaryContactEmail
        {
            get { return _lines[50]; }
        }

        public string DestinationPrimaryContactType
        {
            get { return _lines[51]; }
        }

        public string DestinationPrimaryContactComment
        {
            get { return _lines[52]; }
        }

        public bool IsPartialTruckload
        {
            get { return _lines[53].ToBoolean(); }
        }

        public int JobStep
        {
            get { return _lines[54].ToInt(); }
        }
    }

    public struct LoadOrderItemRow
    {
        private List<string> _lines { get; set; }

        public LoadOrderItemRow(List<string> headerLine) : this()
        {
            _lines = headerLine;
        }

        public string LoadOrderIdentifier
        {
            get { return _lines[0]; }
        }

        public string Description
        {
            get { return _lines[1]; }
        }

        public string Comment
        {
            get { return _lines[2]; }
        }

        public double FreightClass
        {
            get { return _lines[3].ToDouble(); }
        }

        public decimal ActualWeight
        {
            get { return _lines[4].ToDecimal(); }
        }

        public decimal ActualLength
        {
            get { return _lines[5].ToDecimal(); }
        }

        public decimal ActualWidth
        {
            get { return _lines[6].ToDecimal(); }
        }

        public decimal ActualHeight
        {
            get { return _lines[7].ToDecimal(); }
        }

        public int Quantity
        {
            get { return _lines[8].ToInt(); }
        }

        public int PieceCount
        {
            get { return _lines[9].ToInt(); }
        }

        public bool IsStackable
        {
            get { return _lines[10].ToBoolean(); }
        }

        public bool HazardousMaterial
        {
            get { return _lines[11].ToBoolean(); }
        }

        public string PackageTypeName
        {
            get { return _lines[12]; }
        }

        public string NMFCCode
        {
            get { return _lines[13]; }
        }

        public string HTSCode
        {
            get { return _lines[14]; }
        }

        public decimal Value
        {
            get { return _lines[15].ToDecimal(); }
        }
    }

    public struct LoadOrderChargeRow
    {
        private List<string> _lines { get; set; }

        public LoadOrderChargeRow(List<string> headerLine) : this()
        {
            _lines = headerLine;
        }

        public string LoadOrderIdentifier
        {
            get { return _lines[0]; }
        }

        public string ChargeCode
        {
            get { return _lines[1]; }
        }

        public int Quantity
        {
            get { return _lines[2].ToInt(); }
        }

        public decimal UnitBuy
        {
            get { return _lines[3].ToDecimal(); }
        }

        public decimal UnitSell
        {
            get { return _lines[4].ToDecimal(); }
        }

        public decimal UnitDiscount
        {
            get { return _lines[5].ToDecimal(); }
        }

        public string Comment
        {
            get { return _lines[6]; }
        }
    }

    public struct LoadOrderVendorRow
    {
        private List<string> _lines { get; set; }

        public LoadOrderVendorRow(List<string> headerLine) : this()
        {
            _lines = headerLine;
        }

        public string LoadOrderIdentifier
        {
            get { return _lines[0]; }
        }

        public string VendorNumber
        {
            get { return _lines[1]; }
        }

        public string ProNumber
        {
            get { return _lines[2]; }
        }

        public bool IsPrimary
        {
            get { return _lines[3].ToBoolean(); }
        }
    }

    public struct LoadOrderEquipmentRow
    {
        private List<string> _lines { get; set; }

        public LoadOrderEquipmentRow(List<string> headerLine) : this()
        {
            _lines = headerLine;
        }

        public string LoadOrderIdentifier
        {
            get { return _lines[0]; }
        }

        public string EquipmentCode
        {
            get { return _lines[1]; }
        }
    }

    public struct LoadOrderServiceRow
    {
        private List<string> _lines { get; set; }

        public LoadOrderServiceRow(List<string> headerLine) : this()
        {
            _lines = headerLine;
        }

        public string LoadOrderIdentifier
        {
            get { return _lines[0]; }
        }

        public string ServiceCode
        {
            get { return _lines[1]; }
        }
    }


    // Service Ticket
    public struct ServiceTicketRow
    {
        private List<string> _lines { get; set; }

        public ServiceTicketRow(List<string> headerLine) : this()
        {
            _lines = headerLine;
        }

        public string JobIdentifier
        {
            get { return _lines[0]; }
        }

        public string ServiceTicketIdentifier
        {
            get { return _lines[1]; }
        }

        public DateTime TicketDate
        {
            get { return _lines[2].ToDateTime(); }
        }

        public string ExternalReference1
        {
            get { return _lines[3]; }
        }

        public string ExternalReference2
        {
            get { return _lines[4]; }
        }

        public int JobStep
        {
            get { return _lines[5].ToInt(); }
        }
    }

    public struct ServiceTicketItemRow
    {
        private List<string> _lines { get; set; }

        public ServiceTicketItemRow(List<string> headerLine) : this()
        {
            _lines = headerLine;
        }

        public string ServiceTicketIdentifier
        {
            get { return _lines[0]; }
        }

        public string Description
        {
            get { return _lines[1]; }
        }

        public string Comment
        {
            get { return _lines[2]; }
        }
    }

    public struct ServiceTicketChargeRow
    {
        private List<string> _lines { get; set; }

        public ServiceTicketChargeRow(List<string> headerLine) : this()
        {
            _lines = headerLine;
        }

        public string ServiceTicketIdentifier
        {
            get { return _lines[0]; }
        }

        public string ChargeCode
        {
            get { return _lines[1]; }
        }

        public int Quantity
        {
            get { return _lines[2].ToInt(); }
        }

        public decimal UnitBuy
        {
            get { return _lines[3].ToDecimal(); }
        }

        public decimal UnitSell
        {
            get { return _lines[4].ToDecimal(); }
        }

        public decimal UnitDiscount
        {
            get { return _lines[5].ToDecimal(); }
        }

        public string Comment
        {
            get { return _lines[6]; }
        }
    }

    public struct ServiceTicketVendorRow
    {
        private List<string> _lines { get; set; }

        public ServiceTicketVendorRow(List<string> headerLine) : this()
        {
            _lines = headerLine;
        }

        public string ServiceTicketIdentifier
        {
            get { return _lines[0]; }
        }

        public string VendorNumber
        {
            get { return _lines[1]; }
        }

        public bool IsPrimary
        {
            get { return _lines[2].ToBoolean(); }
        }
    }

    public struct ServiceTicketEquipmentRow
    {
        private List<string> _lines { get; set; }

        public ServiceTicketEquipmentRow(List<string> headerLine) : this()
        {
            _lines = headerLine;
        }

        public string ServiceTicketIdentifier
        {
            get { return _lines[0]; }
        }

        public string EquipmentCode
        {
            get { return _lines[1]; }
        }
    }

    public struct ServiceTicketServiceRow
    {
        private List<string> _lines { get; set; }

        public ServiceTicketServiceRow(List<string> headerLine) : this()
        {
            _lines = headerLine;
        }

        public string ServiceTicketIdentifier
        {
            get { return _lines[0]; }
        }

        public string ServiceCode
        {
            get { return _lines[1]; }
        }
    }
}