﻿namespace LogisticsPlus.Eship.WebApplication.Utilities
{
    public static class Units
    {
        public enum UnitType
        {
            // DO NOT CHANGE ORDER! Required in Utilites.js file
            English = 0,
            Metric
        }

        public class Ratios
        {
            public const decimal CmMm = 10.00000000m;
            public const decimal MCm = 100.00000000m;
            public const decimal CmIn = 0.39370079m;
            public const decimal FtIn = 12.00000000m;

            public const decimal GMg = 1000.00000000m;
            public const decimal KgG = 1000.00000000m;
            public const decimal KgLb = 2.20462000m;
            public const decimal LbOz = 16.00000000m;
        }


        public enum Weight
        {
            Mg,
            G,
            Kg,
            Lb,
            Oz,
        }

        public enum Dims
        {
            Mm,
            Cm,
            M,
            In,
            Ft,
        }

        public static decimal CmToIn(this decimal value)
        {
            return value * Ratios.CmIn;
        }

        public static decimal InToCm(this decimal value)
        {
            return value / Ratios.CmIn;
        }

        public static decimal FtToIn(this decimal value)
        {
            return value * Ratios.FtIn;
        }

        public static decimal InToFt(this decimal value)
        {
            return value / Ratios.FtIn;
        }

        public static decimal KgToLb(this decimal value)
        {
            return value * Ratios.KgLb;
        }

        public static decimal LbToKg(this decimal value)
        {
            return value / Ratios.KgLb;
        }

        public static decimal LbToOz(this decimal value)
        {
            return value * Ratios.LbOz;
        }

        public static decimal OzToLb(this decimal value)
        {
            return value / Ratios.LbOz;
        }


        public static decimal GToMg(this decimal value)
        {
            return value * Ratios.GMg;
        }

        public static decimal MgToG(this decimal value)
        {
            return value / Ratios.GMg;
        }

        public static decimal KgToG(this decimal value)
        {
            return value * Ratios.KgG;
        }

        public static decimal GToKg(this decimal value)
        {
            return value / Ratios.KgG;
        }


        public static decimal CmToMm(this decimal value)
        {
            return value * Ratios.CmMm;
        }

        public static decimal MmToCm(this decimal value)
        {
            return value / Ratios.CmMm;
        }

        public static decimal MToCm(this decimal value)
        {
            return value * Ratios.MCm;
        }

        public static decimal CmToM(this decimal value)
        {
            return value / Ratios.MCm;
        }
    }
}
