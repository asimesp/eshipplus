﻿using System;
using System.Web;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor;

namespace LogisticsPlus.Eship.WebApplication.Utilities
{
	public static class PersistenceTokenUtility
	{
		private static readonly TextEncryptor Encryptor = new TextEncryptor("DhS+cOFpD725lpePJ3+fF2BLwGKtBSulxL7jnKdkNSk=", "al49OdI0vnrvnciBjj4s2w==");
		private const string LoginCookie = "eShipPlus_TMS_PLC";
		private const string PartnerReturnUrlCookie = "eTnT_Suite_PLC_PRU";
		private const string PartnerHomeUrlCookie = "eTnT_Suite_PLC_PHU";
		private const string PartnerAuthenticationErrorReturnUrlCookie = "eTnT_Suite_PLC_PAERU";


		private static bool Persist<T>(this HttpResponse response, T value, string name)
		{
			try
			{
				var cookie = new HttpCookie(name)
				{
					Value = Encryptor.Encrypt(value.GetString()),
					Expires = DateTime.Now.TimeToMinimum().AddDays(1)
				};
				response.Cookies.Add(cookie);
				return true;
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, HttpContext.Current);
				return false;
			}
		}

		private static void RemovePersisted(this HttpResponse response, string name)
		{
			response.Cookies.Add(new HttpCookie(name)
			{
				Value = string.Empty,
				Expires = DateTime.Now.AddMinutes(-1)
			});
		}



		public static bool PersistLogin(this HttpResponse response, User user)
		{
			return response.Persist(user.Id, LoginCookie);
		}

		public static bool PersistPartnerAuthenticationErrorReturnUrl(this HttpResponse response, string url)
		{
			return response.Persist(url, PartnerAuthenticationErrorReturnUrlCookie);
		}

		public static bool PersistPartnerHomeUrl(this HttpResponse response, string url)
		{
			return response.Persist(url, PartnerHomeUrlCookie);
		}

		public static bool PersistPartnerReturnUrl(this HttpResponse response, string url)
		{
			return response.Persist(url, PartnerReturnUrlCookie);
		}

		public static bool RemovePersistedLogin(this HttpResponse response)
		{
			try
			{
				response.RemovePersisted(LoginCookie);
				response.RemovePersisted(PartnerReturnUrlCookie);
				response.RemovePersisted(PartnerHomeUrlCookie);
				response.RemovePersisted(PartnerAuthenticationErrorReturnUrlCookie);

				return true;
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, HttpContext.Current);
				return false;
			}
		}



		public static bool HasPersistedLogin(this HttpRequest request)
		{
			try
			{
				return request.Cookies[LoginCookie] != null;
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, HttpContext.Current);
				return false;
			}
		}



		private static object RetrievePersisted(this HttpRequest request, string name)
		{
			try
			{
				var cookie = request.Cookies[name];
				return cookie == null ? null : Encryptor.Decrypt(cookie.Value);
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, HttpContext.Current);
				return null;
			}
		}

		public static User RetrievePersistedLogin(this HttpRequest request)
		{
			try
			{
				var user = new User(request.RetrievePersisted(LoginCookie).ToLong());
				return user.KeyLoaded ? user : null;
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, HttpContext.Current);
				return null;
			}
		}

		public static string RetrievePersistedPartnerAuthenticationErrorReturnUrl(this HttpRequest request)
		{
			return request.RetrievePersisted(PartnerAuthenticationErrorReturnUrlCookie).GetString();
		}

		public static string RetrievePersistedPartnerHomeUrl(this HttpRequest request)
		{
			return request.RetrievePersisted(PartnerHomeUrlCookie).GetString();
		}

		public static string RetrievePersistedPartnerReturnUrl(this HttpRequest request)
		{
			return request.RetrievePersisted(PartnerReturnUrlCookie).GetString();
		}
	}
}
