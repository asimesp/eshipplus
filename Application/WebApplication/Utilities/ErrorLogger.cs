using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using LogisticsPlus.Eship.WebApplication.Members;
using LogisticsPlus.Eship.WebApplication.Notifications;

namespace LogisticsPlus.Eship.WebApplication.Utilities
{
    /// <summary>
    /// Error loggign class
    /// </summary>
    public class ErrorLogger
    {
        private const string EmailTemplate = "<html><head><title>eShip Error Report</title>"
            + "<style type=\"text/css\">body, table, td, th { font: 10pt Verdana; } "
            + "th { padding-right: 10px; text-align: left; vertical-align: top; font-weight: bold; width: 1%; white-space: nowrap; } "
            + "td { padding-bottom: 3px; vertical-align: top; } "
            + "h1 { font-family: \"Trebuchet MS\"; font-size: 200%; padding-bottom: 5px; "
            + "border-bottom: 1px solid #ccc; color: #CF4D33; } "
            + "h2 { font-family: \"Trebuchet MS\"; font-size: 125%; color: #333; margin-top: 20px; margin-bottom: 0; } "
            + "label { font-weight: bold; padding-right: 10px; line-height: 150%; } "
            + ".errMsg { font-size: 115%; color: #CF4D33; font-weight: bold; } "
            + ".errMsg label { color: #000; padding-right: 10px; }</style></head>"
            + "<body><h1>eShip Error Report</h1><div class=\"errMsg\"><label>Error message:</label>[#ERROR_MESSAGE#]</div>"
            + "<label>Server: </label>[#SERVER_NAME#]<br />"
            + "<label>Timestamp: </label>[#TIME_STAMP#]<br /><label>Filename: </label>[#FILE_NAME#]<br />"
            + "<label>URL: </label>[#URL#]<br /><br />"
            + "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\">"
            + "<tr><td colspan=\"2\"><h2>User Details</h2></td></tr><tr><th>User ID:</th><td>[#USER_ID#]</td></tr>"
            + "<tr><th>User:</th><td>[#USER_NAME#]</td></tr><tr><th>Email:</th><td>[#USER_EMAIL#]</td></tr><tr><th>IP Address:</th><td>[#USER_IP#]</td></tr>"
            + "[#ERROR_DETAILS_BEGIN#]<tr><td colspan=\"2\"><h2>Error Details</h2></td></tr>"
            + "<tr><th>File:</th><td>[#FILE#]</td></tr><tr><th>Class:</th><td>[#CLASS#]</td></tr>"
            + "<tr><th>Method:</th><td>[#FUNCTION#]</td></tr><tr><th>Line Number:</th><td>[#LINE_NUMBER#]</td></tr>"
            + "<tr><th>Stack Trace:</th><td>[#STACK_TRACE#]</td></tr>[#ERROR_DETAILS_END#]"
            + "<tr><td colspan=\"2\"><h2>Server Request Data</h2></td></tr><tr><th>URL:</th><td>[#URL#]</td></tr>"
            + "<tr><th>Referer URL:</th><td>[#REFERER_URL#]</td></tr>"
            + "<tr><th>Form:</th><td>[#FORM_PARAMS#]</td></tr><tr><th>Query String:</th><td>[#QUERY_PARAMS#]</td></tr>"
            + "</table></body></html>";


        #region [ Static methods ]

		public static void LogError(Exception ex, string errorsFolder)
		{
			LogError(ex, HttpContext.Current, errorsFolder);
		}

    	public static void LogError(Exception ex, HttpContext context)
        {
    		LogError(ex, context, context.Server.MapPath(WebApplicationSettings.ErrorsFolder));
        }

    	private static void LogError(Exception ex, HttpContext context, string errorsFolder)
    	{
    		var baseException = ex.GetBaseException();

    		try
    		{
    			// make the file name
    			var errorFileName = DateTime.Now.ToString("yyyyMMddHHmmssff") + ".html";

    			// get user information if any
    			var user = context == null ? null : context.RetrieveAuthenticatedUser();
    			string userId = "-", userName = "-", userEmail = "-", tenantName = "-";
    			if (user != null)
    			{
    				userId = user.Id.ToString();
    				userName = string.Format("{0} {1} ({2}) {3}", user.FirstName, user.LastName, user.Username,
    				                         user.TenantId == default(long) ? "*** Super User" : string.Empty);
    				userEmail = user.Email;
    				if (user.TenantId != default(long)) tenantName = user.Tenant.Name;
    			}

    			var url = context == null ? string.Empty : context.Request.RawUrl;
    			var referer = context == null ? "[ process without context ]" : context.Request.ServerVariables["HTTP_REFERER"];
    			var clientIp = context == null ? string.Empty : context.Request.ServerVariables["REMOTE_ADDR"];
    			string className = string.Empty, methodName = string.Empty, lineNumber = "-", fileName = "-";
    			try
    			{
    				var trace = new StackTrace(baseException, true);
    				className = baseException.TargetSite.DeclaringType.ToString();
    				methodName = baseException.TargetSite.Name;

    				if (trace.FrameCount > 0)
    				{
    					lineNumber = trace.GetFrame(0).GetFileLineNumber().ToString();
    					fileName = trace.GetFrame(0).GetFileName();
    				}
    			}
    			catch
    			{
    			}

    			// prepare and send the email notification
    			string msg = EmailTemplate;
    			msg = msg.Replace("[#FILE_NAME#]", errorFileName);
    			msg = msg.Replace("[#ERROR_DETAILS_BEGIN#]", "");
    			msg = msg.Replace("[#ERROR_DETAILS_END#]", "");
    			msg = msg.Replace("[#ERROR_MESSAGE#]", baseException.Message);
    			msg = msg.Replace("[#SERVER_NAME#]", context == null ? string.Empty : context.Request.Url.Host);
    			msg = msg.Replace("[#TIME_STAMP#]", DateTime.Now.ToString("HH:mm:ss / dd MMMM, yyyy"));
    			msg = msg.Replace("[#FILE#]", fileName);
    			msg = msg.Replace("[#CLASS#]", className);
    			msg = msg.Replace("[#FUNCTION#]", methodName);
    			msg = msg.Replace("[#LINE_NUMBER#]", lineNumber);
    			msg = msg.Replace("[#STACK_TRACE#]", baseException.StackTrace == null
    			                                     	? string.Empty
    			                                     	: baseException.StackTrace.Replace("\r\n", "<br />"));
    			msg = msg.Replace("[#URL#]", url);
    			msg = msg.Replace("[#REFERER_URL#]", referer);
    			msg = msg.Replace("[#USER_ID#]", userId);
    			msg = msg.Replace("[#USER_NAME#]", userName);
    			msg = msg.Replace("[#USER_EMAIL#]", userEmail);
    			msg = msg.Replace("[#USER_IP#]", clientIp);
    			msg = msg.Replace("[#FORM_PARAMS#]", context == null ? string.Empty : RetrieveRequestParams(context));
    			msg = msg.Replace("[#QUERY_PARAMS#]", context == null ? string.Empty : RetrieveQueryParams(context));

    			// send an email
    			msg.SendErrorLogEmail();

    			// save the file
    			SaveFile(msg, errorFileName, errorsFolder);

				// log error to Application logger
    			EnvironmentUtilities.PageAccessLogger.Error("Tenant:{0}\tUser:{1}\tUrl:{2}\tErr:{3}\tIP:{4}", tenantName, userName, url, baseException.Message, clientIp);
    		}
    		catch
    		{
    			// sink all exceptions
    		}
    	}

    	#endregion

    	#region [ Internals ]

    	private static string RetrieveQueryParams(HttpContext context)
    	{
    		var queryParams = "";
    		if (context.Request.QueryString.Keys.Count > 0)
    		{
    			queryParams = context.Request.QueryString.Keys
    				.Cast<string>()
    				.Aggregate(queryParams, (current, key) => current + (key + ": " + context.Request.QueryString[key] + "<br />"));
    		}
    		else
    		{
    			queryParams = "[ no query string data ]";
    		}
    		return queryParams;
    	}

    	private static string RetrieveRequestParams(HttpContext context)
    	{
    		var requestParams = "";
    		if (context.Request.Form.Keys.Count > 0)
    		{
    			requestParams = context.Request.Form.Keys
    				.Cast<string>()
    				.Aggregate(requestParams, (current, key) => current + (key + ": " + context.Request.Form[key] + "<br />"));
    		}
    		else
    		{
    			requestParams = "[ no form data ]";
    		}
    		return requestParams;
    	}

    	private static void SaveFile(string html, string fileName, string path)
        {
            try
            {
                if (!Directory.Exists(path)) Directory.CreateDirectory(path);
				using (var writer = new StreamWriter(Path.Combine(path, fileName)))
					writer.Write(html);
            }
            catch 
            { 
            }
        }

        #endregion
    }
}