﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Extern.Ws;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Members;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Utilities.ImmitationViews
{
	public class CustomerPurchaseOrderImmitationView : MemberPageBase, ICustomerPurchaseOrderView, IHttpContextImmitationView
	{
		private List<WSMessage> _messages;

		public List<WSMessage> Messages { get { return _messages; } }

		public override ViewCode PageCode
		{
			get { return ViewCode.WebServices; }
		}

		public override string SetPageIconImage
		{
			set { }
		}

		public List<Country> Countries
		{
			set { }
		}

		public event EventHandler Loading;
		public event EventHandler<ViewEventArgs<CustomerPurchaseOrder>> Save;
		public event EventHandler<ViewEventArgs<CustomerPurchaseOrder>> Delete;
		public event EventHandler<ViewEventArgs<CustomerPurchaseOrder>> Lock;
		public event EventHandler<ViewEventArgs<CustomerPurchaseOrder>> UnLock;
		public event EventHandler<ViewEventArgs<PurchaseOrderViewSearchCriteria>> Search;
		public event EventHandler<ViewEventArgs<List<CustomerPurchaseOrder>>> BatchImport;
		public event EventHandler<ViewEventArgs<string>> CustomerSearch;

		

		public void DisplaySearchResult(List<CustomerPurchaseOrder> customerPurchaseOrders)
		{
		}

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, HttpContext.Current);
        }

		public void DisplayMessages(IEnumerable<ValidationMessage> messages)
		{
			_messages = messages.ToWSMessages().ToList();
		}

		public void DisplayCustomer(Customer customer)
		{
		}

		public void FailedLock(Lock @lock)
		{
			DisplayMessages(new[] {ValidationMessage.Error(@lock.BuildFailedLockMessage())});
		}


		public CustomerPurchaseOrderImmitationView(User user)
		{
			ActiveUser = user;
			_messages = new List<WSMessage>();
			new CustomerPurchaseOrderHandler(this).Initialize();
		}


		public void LockRecord(CustomerPurchaseOrder po)
		{
			if (po.IsNew) return;

			if (Lock != null)
				Lock(this, new ViewEventArgs<CustomerPurchaseOrder>(po));
		}

		public void UnLockRecord(CustomerPurchaseOrder po)
		{
			if (po.IsNew) return;

			if (UnLock != null)
				UnLock(this, new ViewEventArgs<CustomerPurchaseOrder>(po));
		}

		public void DeletePurchaseOrder(CustomerPurchaseOrder po)
		{
			if (Delete != null)
				Delete(this, new ViewEventArgs<CustomerPurchaseOrder>(po));

		}

		public void SavePurchaseOrder(CustomerPurchaseOrder po)
		{
			if (Save != null)
				Save(this, new ViewEventArgs<CustomerPurchaseOrder>(po));

		}

		public void BatchImportPurchaseOrders(List<CustomerPurchaseOrder> pos)
		{
			if (BatchImport != null)
				BatchImport(this, new ViewEventArgs<List<CustomerPurchaseOrder>>(pos));
		}
	}
}
