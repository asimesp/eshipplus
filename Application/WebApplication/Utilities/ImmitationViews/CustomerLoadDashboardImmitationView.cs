﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Extern.Ws;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.WebApplication.Members;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Utilities.ImmitationViews
{
	public class CustomerLoadDashboardImmitationView : MemberPageBase, ICustomerLoadDashboardView, IHttpContextImmitationView
	{
		private List<WSMessage> _messages;

		public List<WSMessage> Messages { get { return _messages; } }

		public override ViewCode PageCode
		{
			get { return ViewCode.WebServices; }
		}

		public override string SetPageIconImage
		{
			set {  }
		}

		public Dictionary<int, string> Statuses
		{
			set {  }
		}

		public List<Country> Countries
		{
			set { }
		}

		public List<ContactType> ContactTypes
		{
			set { }
		}

		public List<PackageType> PackageTypes
		{
			set { }
		}

		public List<ServiceViewSearchDto> Services
		{
			set { }
		}

		public List<EquipmentType> EquipmentTypes
		{
			set { }
		}

		public List<MileageSource> MileageSources
		{
			set { }
		}

		public List<DocumentTag> DocumentTags
		{
			set { }
		}

		public event EventHandler Loading;
		public event EventHandler<ViewEventArgs<ShipmentViewSearchCriteria>> Search;
		public event EventHandler<ViewEventArgs<Shipment>> VoidShipment;



		public void ProcessVoidComplete(Shipment shipment)
		{
			this.NotifyCancelledShipment(shipment);
			DisplayMessages(new[] { ValidationMessage.Information("Shipment Cancel Request Complete!") });
		}

		public void DisplaySearchResult(List<ShipmentDashboardDto> shipments)
		{
		}

		public void DisplayMessages(IEnumerable<ValidationMessage> messages)
		{
			_messages = messages.ToWSMessages().ToList();
		}

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, HttpContext.Current);
        }


		public CustomerLoadDashboardImmitationView(User user)
		{
			ActiveUser = user;
			_messages = new List<WSMessage>();
			new CustomerLoadDashboardHandler(this).Initialize();
		}

		public void CancelShipment(Shipment shipment)
		{
			if (shipment.Status != ShipmentStatus.Scheduled)
			{
				DisplayMessages(new[]
    			                	{
    			                		ValidationMessage.Error("Requested shipment cannot be cancel because it is in '{0}' status.",
    			                		                        shipment.Status.ToString())
    			                	});
				return;
			}

			if (VoidShipment != null)
				VoidShipment(this, new ViewEventArgs<Shipment>(shipment));
		}
	}
}
