﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Extern.Ws;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.WebApplication.Members;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Utilities.ImmitationViews
{
	public class LoadOrderImmitationView : MemberPageBase, ILoadOrderView, IHttpContextImmitationView
	{
		private List<WSMessage> _messages;
		private LoadOrder _loadOrder;

		private readonly List<Milestone> _notifications;

		public List<WSMessage> Messages { get { return _messages; } }
		public LoadOrder LoadOrder { get { return _loadOrder; } }

		public override ViewCode PageCode
		{
			get { return ViewCode.WebServices; }
		}
		public override string SetPageIconImage
		{
			set { }
		}

	    public bool RefundOrVoidMiscReceipts { get; private set; }

	    public bool SaveOriginToAddressBook
		{
			get { return false; }
		}
		public bool SaveDestinationToAddressBook
		{
			get { return false; }
		}
		public List<Country> Countries
		{
			set {}
		}
		public List<ContactType> ContactTypes
		{
			set { }
		}
		public List<PackageType> PackageTypes
		{
			set { }
		}
		public List<ServiceViewSearchDto> Services
		{
			set { }
		}
		public List<EquipmentType> EquipmentTypes
		{
			set { }
		}
		public List<ShipmentPriority> ShipmentPriorities
		{
			set { }
		}

	    public Dictionary<int, string> DateUnits
	    {
	        set{ }
	    }

	    public List<ChargeCode> ChargeCodes
		{
			set { }
		}
		public List<MileageSource> MileageSources
		{
			set { }
		}
		public List<DocumentTag> DocumentTags
		{
			set { }
		}
		public Dictionary<int, string> NoteTypes
		{
			set { }
		}
		public Dictionary<int, string> ServiceModes
		{
			set { }
		}

		public event EventHandler Loading;
		public event EventHandler<ViewEventArgs<LoadOrder>> Save;
		public event EventHandler<ViewEventArgs<LoadOrder>> Delete;
		public event EventHandler<ViewEventArgs<LoadOrder>> Lock;
		public event EventHandler<ViewEventArgs<LoadOrder>> UnLock;
		public event EventHandler<ViewEventArgs<LoadOrder>> LoadAuditLog;
		public event EventHandler<ViewEventArgs<string>> CustomerSearch;
		public event EventHandler<ViewEventArgs<string>> VendorSearch;
		public event EventHandler<ViewEventArgs<LoadOrder, Shipment>> SaveConvertToShipment;
	    public event EventHandler<ViewEventArgs<LoadOrder>> RefundMiscReceipts;

	    public void SetCustomerSalesRepresentative(Customer customer)
		{
		}

		public void DisplayVendor(Vendor vendor)
		{
		}

		public void DisplayCustomer(Customer customer, bool updateSalesRepAndResellers)
		{
		}

		public void DisplayMessages(IEnumerable<ValidationMessage> messages)
		{
			_messages = messages.ToWSMessages().ToList();

			if (!messages.HasErrors() && !messages.HasWarnings())
			{
				// unlock
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<LoadOrder>(_loadOrder));

				// send notifications
				if (_notifications.Any())
				{
					ProcessNotifications(_loadOrder);
					_notifications.Clear();
				}
			}
		}

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, HttpContext.Current);
        }

		public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
		{
		}

		public void FailedLock(Lock @lock)
		{
			DisplayMessages(new[] { ValidationMessage.Error(@lock.BuildFailedLockMessage()) });
		}

		public void Set(LoadOrder loadOrder)
		{
			_loadOrder = loadOrder;
		}


		private void ProcessNotifications(LoadOrder request)
		{
			foreach (var notification in _notifications)
				switch (notification)
				{
					case Milestone.NewLoadOrder:
					case Milestone.LoadOrderVoided:
						this.NotifyLoadOrderEvent(request);
						break;
				}
		}


		public LoadOrderImmitationView(User user)
		{
			_notifications = new List<Milestone>();
			ActiveUser = user;
			_messages = new List<WSMessage>();
			new LoadOrderHandler(this).Initialize();
		}



		public void LockRecord(LoadOrder request)
		{
			if (Lock == null || request.IsNew) return;
			Lock(this, new ViewEventArgs<LoadOrder>(request));
		}

		public void UnLockRecord(LoadOrder request)
		{
			if (UnLock == null || request.IsNew) return;
			UnLock(this, new ViewEventArgs<LoadOrder>(request));
		}

		public void CancelLoadOrder(LoadOrder request)
		{
			if (request.IsNew)
			{
				DisplayMessages(new[] { ValidationMessage.Error("Unable to cancel new load record that has not been previously saved.") });
				return;
			}
			if (request.Status == LoadOrderStatus.Cancelled)
			{
				DisplayMessages(new[] {ValidationMessage.Error("Unable to cancel as load is already covered.")});
				return;
			}

			request.LoadCollections();
			request.TakeSnapShot();
			request.Status = LoadOrderStatus.Cancelled;

			_notifications.Add(Milestone.LoadOrderVoided);
			_loadOrder = request; // for unlock

			if (Save != null)
				Save(this, new ViewEventArgs<LoadOrder>(request));
		}

		public void SaveLoadOrder(LoadOrder request)
		{
			if (request.IsNew) _notifications.Add(Milestone.NewLoadOrder);
			_loadOrder = request; // for unlock
			if (Save != null)
				Save(this, new ViewEventArgs<LoadOrder>(request));
		}

        private bool CustomerCreditCheckOkay(LoadOrder loadOrder)
        {
            var charges = loadOrder.Charges.Sum(c => c.AmountDue);
            var customer = loadOrder.Customer;
            var totalDue = loadOrder.Charges.Sum(c => c.AmountDue);

            var outstanding = customer.OutstandingBalance() - totalDue;
            var credit = (loadOrder.Customer ?? new Customer()).CreditLimit - outstanding;

            if (charges > credit && !ActiveUser.HasAccessTo(ViewCode.BypassCustomerCreditStop))
            {
                DisplayMessages(new[] { ValidationMessage.Error("Insufficient customer credit [{0:c2}] to cover order total [{1:c2}]", credit, charges) });
                return false;
            }

            return true;
        }

        public void ConvertLoadOrderToShipment(LoadOrder loadOrder)
        {
            loadOrder.Status = LoadOrderStatus.Shipment;

            if (!CustomerCreditCheckOkay(loadOrder)) return;

            var shipment = loadOrder.ToShipment();
            shipment.User = ActiveUser;

            _loadOrder = loadOrder;


            if (SaveConvertToShipment != null)
                SaveConvertToShipment(this, new ViewEventArgs<LoadOrder, Shipment>(loadOrder, shipment));
        }

        public void FinalizeConvertToShipment(Shipment shipment)
        {
            this.NotifyNewShipment(shipment);
        }
	}
}
