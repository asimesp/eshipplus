﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Extern.Ws;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.WebApplication.Members;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Utilities.ImmitationViews
{
    public class ShipmentImmitationView : MemberPageBase, IShipmentView, IHttpContextImmitationView
    {
        private List<WSMessage> _messages;
        private Shipment _shipment;

        public List<WSMessage> Messages { get { return _messages; } }
        public Shipment Shipment { get { return _shipment; } }

        public override ViewCode PageCode
        {
            get { return ViewCode.WebServices; }
        }
        public override string SetPageIconImage
        {
            set { }
        }

        public bool RefundOrVoidMiscReceipts { get; set; }
        public bool SaveOriginToAddressBook { get { return false; } }
        public bool SaveDestinationToAddressBook { get { return false; } }

        public List<ServiceViewSearchDto> Services { set { } }
        public List<EquipmentType> EquipmentTypes { set { } }
        public List<Asset> Assets { set { } }

        public Dictionary<int, string> DateUnits { set { } }
        public Dictionary<int, string> InDisputeReasons { set { } }
        public Dictionary<int, string> NoteTypes { set { } }
        public Dictionary<int, string> ServiceModes { set { } }
        public Dictionary<int, string> MileageEngines { set { } }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<Shipment>> Save;
        public event EventHandler<ViewEventArgs<Shipment>> Delete;
        public event EventHandler<ViewEventArgs<Shipment>> Lock;
        public event EventHandler<ViewEventArgs<Shipment>> UnLock;
        public event EventHandler<ViewEventArgs<Shipment>> LoadAuditLog;
        public event EventHandler<ViewEventArgs<string>> CustomerSearch;
        public event EventHandler<ViewEventArgs<string>> VendorSearch;
        public event EventHandler<ViewEventArgs<ShoppedRate>> SaveShoppedRate;
        public event EventHandler<ViewEventArgs<SMC3TrackRequestResponse>> SaveSmc3TrackRequestResponse;
        public event EventHandler<ViewEventArgs<Project44TrackingResponse>> SaveProject44TrackRequestResponse;

        public void SetCustomerSalesRepresentative(Customer customer)
        {
        }

        public void DisplayVendor(Vendor vendor)
        {
        }

        public void DisplayCustomer(Customer customer, bool updateSalesRepAndResellers)
        {
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            _messages = messages.ToWSMessages().ToList();

            if (!messages.HasErrors() && !messages.HasWarnings())
            {
                // unlock
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<Shipment>(_shipment));
            }
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, HttpContext.Current);
        }

        public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
        {
        }

        public void FailedLock(Lock @lock)
        {
            DisplayMessages(new[] { ValidationMessage.Error(@lock.BuildFailedLockMessage()) });
        }

        public void Set(Shipment Shipment)
        {
        }

        public void Set(ShoppedRate shoppedRate)
        {
        }


        public ShipmentImmitationView(User user)
        {
            ActiveUser = user;
            _messages = new List<WSMessage>();
            new ShipmentHandler(this).Initialize();
        }



        public void LockRecord(Shipment request)
        {
            if (Lock == null || request.IsNew) return;
            Lock(this, new ViewEventArgs<Shipment>(request));
        }

        public void UnLockRecord(Shipment request)
        {
            if (UnLock == null || request.IsNew) return;
            UnLock(this, new ViewEventArgs<Shipment>(request));
        }

        public void SaveShipment(Shipment request)
        {
            _shipment = request;
            if (Save != null)
                Save(this, new ViewEventArgs<Shipment>(request));
        }
    }
}
