﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Extern.Edi.Segments;
using LogisticsPlus.Eship.Extern.Ws;
using LogisticsPlus.Eship.PaymentGateway;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Rates;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.Project44;
using LogisticsPlus.Eship.Smc.Eva;
using LogisticsPlus.Eship.WebApplication.Documents;
using LogisticsPlus.Eship.WebApplication.Members;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.WebApplication.ViewCodes;
using Newtonsoft.Json;
using P44SDK.V4.Model;

namespace LogisticsPlus.Eship.WebApplication.Utilities.ImmitationViews
{
	internal class RateAndScheduleImmitationView : MemberPageBase, IRateAndScheduleView, IRateAndScheduleConfirmationView,
	                                               IHttpContextImmitationView
	{
		private ShoppedRate _shoppedRate;
		private Shipment _shipment;
		private LoadOrder _load;
		private List<WSMessage> _messages;

		public override ViewCode PageCode
		{
			get { return ViewCode.WebServices; }
		}

		public override string SetPageIconImage
		{
			set { }
		}

		public bool SaveOriginToAddressBook
		{
			get { return false; }
		}

		public bool SaveOriginWithServicesToAddressBook
		{
			get { return false; }
		}

		public bool SaveDestinationToAddressBook
		{
			get { return false; }
		}

		public bool SaveDestinationWithServicesToAddressBook
		{
			get { return false; }
		}

		public List<Country> Countries
		{
			set { }
		}

		public List<ContactType> ContactTypes
		{
			set { }
		}

		public List<EquipmentType> EquipmentTypes
		{
			set { }
		}

		public List<PackageType> PackageTypes
		{
			set { }
		}

		public List<ServiceViewSearchDto> Services
		{
			set { }
		}

		public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<Shipment, PaymentInformation>> SaveShipment;
        public event EventHandler<ViewEventArgs<LoadOrder, PaymentInformation>> SaveLoadOrder;
		public event EventHandler<ViewEventArgs<string>> CustomerSearch;
		public event EventHandler<ViewEventArgs<ShoppedRate>> SaveShoppedRate;
	    public event EventHandler<ViewEventArgs<Shipment>> UpdateShipmentDocumentsAndTracking;
	    public event EventHandler<ViewEventArgs<Shipment>> UpdateShipmentCheckCalls;
	    public event EventHandler<ViewEventArgs<Shipment>> UpdateShipmentNotes;
        public event EventHandler<ViewEventArgs<SMC3TrackRequestResponse>> SaveSmc3TrackRequestResponse;
		public event EventHandler<ViewEventArgs<SMC3DispatchRequestResponse>> SaveSmc3DispatchRequestResponse;
	    public event EventHandler<ViewEventArgs<Project44DispatchResponse>> SaveProject44DispatchRequestResponse;


        public List<WSMessage> Messages
		{
			get { return _messages; }
		}

		public ShoppedRate ShoppedRate
		{
			get { return _shoppedRate; }
		}

		public Shipment Shipment
		{
			get { return _shipment; }
		}

		public LoadOrder LoadOrder
		{
			get { return _load; }
		}

		
		public void DisplayCustomer(Customer customer)
		{

		}

		public void DisplayMessages(IEnumerable<ValidationMessage> messages)
		{
			_messages = messages.ToWSMessages().ToList();
		}

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, HttpContext.Current);
        }

		public void Set(Shipment shipment)
		{
			_shipment = shipment;
		}

	    public void Set(LoadOrder loadOrder)
	    {
	    	_load = loadOrder;
	    }

		public void Set(ShoppedRate shoppedRate)
		{
			_shoppedRate = shoppedRate;
		}

		public RateAndScheduleImmitationView(User user)
		{
			ActiveUser = user;
			_messages = new List<WSMessage>();
			new RateAndScheduleHandler(this).Initialize();
			new RateAndScheduleConfirmationHandler(this).Initialize();
		}

		public void Save(ShoppedRate rate)
		{
			if (SaveShoppedRate != null)
				SaveShoppedRate(this, new ViewEventArgs<ShoppedRate>(rate));
		}

		public void Save(Shipment shipment)
		{
			if (SaveShipment != null)
				SaveShipment(this, new ViewEventArgs<Shipment, PaymentInformation>(shipment, null));
		}

		public void Save(LoadOrder load)
		{
			if (SaveLoadOrder != null)
				SaveLoadOrder(this, new ViewEventArgs<LoadOrder, PaymentInformation>(load, null));
		}


		public void BookPackageShipment(Shipment shipment, HttpContext context)
		{
            var pmaps = ProcessorVars.RegistryCache[ActiveUser.TenantId].SmallPackagingMaps;
            var smaps = ProcessorVars.RegistryCache[ActiveUser.TenantId].SmallPackageServiceMaps;
			var variables = ProcessorVars.IntegrationVariables[ActiveUser.TenantId];
			var rater = new Rater2(variables.RatewareParameters, variables.CarrierConnectParameters,
			                      variables.FedExParameters, variables.UpsParameters, pmaps, smaps, variables.Project44Settings);
			foreach (var vendor in shipment.Vendors) vendor.TakeSnapShot();
			var errors = rater.BookShipment(shipment);

			if (errors.Any())
			{
				DisplayMessages(errors.Select(e => ValidationMessage.Error(e)));
				return;
			}

			var relativePath = WebApplicationSettings.ShipmentFolder(ActiveUser.TenantId, shipment.Id);
			var physicalPath = context.Server.MapPath(relativePath);
			if (!Directory.Exists(physicalPath)) Directory.CreateDirectory(physicalPath);
			foreach (var document in shipment.Documents)
				if (!document.LocationPath.StartsWith("~"))
				{
					var oldFilePath = document.LocationPath;
					var newFilePath = physicalPath + document.Name;
					document.LocationPath = relativePath + document.Name;
					if (!File.Exists(newFilePath))
					{
						File.Copy(oldFilePath, newFilePath);
						File.Delete(oldFilePath);
					}
				}

			if (UpdateShipmentDocumentsAndTracking != null)
				UpdateShipmentDocumentsAndTracking(this, new ViewEventArgs<Shipment>(shipment));
		}

		public void InitiateLTLShipmentDispatch(Shipment shipment)
		{
			// check for SMC3 EVA services
			var primaryVendor = shipment.Vendors.FirstOrDefault(v => v.Primary).Vendor;
			var vcom = primaryVendor.Communication;
			if (vcom != null && vcom.SMC3EvaEnabled && vcom.SMC3EvaSupportsDispatch && shipment.ServiceMode == ServiceMode.LessThanTruckload)
			{
				try
				{
					var dispatchRequest = vcom.GetDispatchRequest(shipment, primaryVendor);

					var evaServiceHandler = new EvaServiceHandler(WebApplicationSettings.SmcEvaSettings);
					var trackResponse = evaServiceHandler.InitiateDispatch(dispatchRequest);

					var dispatchRequestResponse = new SMC3DispatchRequestResponse
					{
						TransactionId = trackResponse.transactionID,
						ShipmentId = shipment.Id,
						RequestData = JsonConvert.SerializeObject(dispatchRequest),
						ResponseData = JsonConvert.SerializeObject(trackResponse),
						Scac = primaryVendor.Scac,
						DateCreated = DateTime.Now,
					};


					if (SaveSmc3DispatchRequestResponse != null)
						SaveSmc3DispatchRequestResponse(this, new ViewEventArgs<SMC3DispatchRequestResponse>(dispatchRequestResponse));
				}
				catch (Exception ex)
				{
					DisplayMessages(new List<ValidationMessage>
			            {
				            ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message)
			            });
				}
			}

		    if (vcom != null && vcom.Project44Enabled && vcom.Project44DispatchEnabled && shipment.ServiceMode == ServiceMode.LessThanTruckload)
		    {
		        try
		        {
		            var p44Settings = new Project44ServiceSettings
		            {
		                Hostname = WebApplicationSettings.Project44Host,
		                Username = WebApplicationSettings.Project44Username,
		                Password = WebApplicationSettings.Project44Password,
		                PrimaryLocationId = WebApplicationSettings.Project44PrimaryLocationId
		            };

		            var p44Wrapper = new Project44Wrapper(p44Settings);
		            var dispatchResponse = p44Wrapper.DispatchShipment(shipment, primaryVendor.Scac);

		            if (dispatchResponse == null)
		            {
		                var messages = p44Wrapper.ErrorMessages.Select(e => ValidationMessage.Error(e)).ToList();
		                DisplayMessages(messages);

		                // Add a note to inform operations that dispatch failed
		                var failedDispatchNote = new ShipmentNote
		                {
		                    Tenant = shipment.Tenant,
		                    Type = NoteType.Critical,
		                    Archived = false,
		                    Message = string.Format(
		                        "Automatic dispatch via Project 44 failed with message: {0}{0}{1}{0}{0} Please contact carrier directly!",
		                        Environment.NewLine, messages.Select(m => m.Message).ToArray().NewLineJoin()),
		                    Shipment = shipment,
		                    Classified = true,
		                    User = ActiveUser
		                };
		                shipment.Notes.Add(failedDispatchNote);

		                if (UpdateShipmentCheckCalls != null)
		                    UpdateShipmentCheckCalls(this, new ViewEventArgs<Shipment>(shipment));
                        return;
		            }

                    var project44DispatchResponse = new Project44DispatchResponse
		            {
		                Project44Id = dispatchResponse.Id.GetString(),
		                ShipmentId = shipment.Id,
		                UserId = ActiveUser.Id,
                        ResponseData = JsonConvert.SerializeObject(dispatchResponse),
		                Scac = primaryVendor.Scac,
		                DateCreated = DateTime.Now,
		            };

		            if (SaveProject44DispatchRequestResponse != null)
		                SaveProject44DispatchRequestResponse(this, new ViewEventArgs<Project44DispatchResponse>(project44DispatchResponse));

		            if (dispatchResponse.ShipmentIdentifiers != null &&
		                dispatchResponse.ShipmentIdentifiers.Any(i => i.Type == LtlShipmentIdentifier.TypeEnum.PICKUP))
		            {
		                // add pickup check call
		                var pickupCheckCall = new CheckCall
		                {
		                    Tenant = shipment.Tenant,
		                    CallDate = DateTime.Now,
		                    CallNotes = dispatchResponse.ShipmentIdentifiers
		                        .First(i => i.Type == LtlShipmentIdentifier.TypeEnum.PICKUP).Value,
		                    EdiStatusCode = ShipStatMsgCode.AA.GetString(),
		                    EventDate = dispatchResponse.PickupDateTime.ToDateTime(),
		                    Shipment = shipment,
		                    User = ActiveUser
		                };

		                shipment.CheckCalls.Add(pickupCheckCall);


		                if (UpdateShipmentCheckCalls != null)
		                    UpdateShipmentCheckCalls(this, new ViewEventArgs<Shipment>(shipment));
		            }
		        }
		        catch (Exception ex)
		        {
		            DisplayMessages(new List<ValidationMessage>
		            {
		                ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message)
		            });
		        }
		    }

        }

        public void InitiateLTLShipmentTracking(Shipment shipment)
		{
			// check for SMC3 EVA services
			var primaryVendor = shipment.Vendors.FirstOrDefault(v => v.Primary).Vendor;
			var vcom = primaryVendor.Communication;
			if (vcom != null && vcom.SMC3EvaEnabled && vcom.SMC3EvaSupportsTracking && shipment.ServiceMode == ServiceMode.LessThanTruckload)
			{
				try
				{
					var trackRequest = vcom.GetTrackRequest(shipment, primaryVendor);

					var evaServiceHandler = new EvaServiceHandler(WebApplicationSettings.SmcEvaSettings);
					var trackResponse = evaServiceHandler.InitiateTracking(trackRequest);

					var trackRequestResponse = new SMC3TrackRequestResponse
					{
						TransactionId = trackResponse.transactionID,
						ShipmentId = shipment.Id,
						RequestData = JsonConvert.SerializeObject(trackRequest),
						ResponseData = JsonConvert.SerializeObject(trackResponse),
						Scac = primaryVendor.Scac,
						DateCreated = DateTime.Now,
					};


					if (SaveSmc3TrackRequestResponse != null)
						SaveSmc3TrackRequestResponse(this, new ViewEventArgs<SMC3TrackRequestResponse>(trackRequestResponse));
				}
				catch (Exception ex)
				{
					DisplayMessages(new List<ValidationMessage>
			            {
				            ValidationMessage.Error(ProcessorVars.RecordSaveErrMsgFormat, ex.Message)
			            });
				}
			}
        }


        public void ProcessesNotification(Shipment shipment, HttpContext context)
		{
			try
			{
				this.NotifyNewShipment(shipment);
			}
			catch (Exception ex)
			{
				ErrorLogger.LogError(ex, context);
			}
		}

		public void ProcessesNotification(LoadOrder loadOrder, HttpContext context)
		{
			try
			{
				this.NotifyLoadOrderEvent(loadOrder);
			}
			catch (Exception ex)
			{
				ErrorLogger.LogError(ex, context);
			}
		}



		public byte[] GenerateQuoteDocument(LoadOrder load, HttpContext context)
		{
			try
			{
				return this.GenerateQuote(load, true).ToPdf(string.Format("{0}_Quote", load.LoadOrderNumber));
			}
			catch (Exception ex)
			{
				ErrorLogger.LogError(ex, context);
				return new byte[0];
			}
		}

		public byte[] GenerateBOLDocument(Shipment shipment, HttpContext context)
		{
			try
			{
				return this.GenerateBOL(shipment, true).ToPdf(string.Format("{0}_BillOfLading", shipment.ShipmentNumber));
			}
			catch (Exception ex)
			{
				ErrorLogger.LogError(ex, context);
				return new byte[0];
			}
		}
        public byte[] GenerateProLabels4x6Document(Shipment shipment, HttpContext context, string templateType)
        {
            try
            {
                return this.GenerateShipmentLabels(shipment, true,DocumentTemplateCategory.ProLabel4x6inch).ToPdf(templateType);
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex, context);
                return new byte[0];
            }
        }
        public byte[] GenerateProLabelAvery3x4Document(Shipment shipment, HttpContext context, string templateType)
        {
            try
            {
                return this.GenerateShipmentLabels(shipment, true, DocumentTemplateCategory.ProLabelAvery3x4inch).ToPdf(templateType);
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex, context);
                return new byte[0];
            }
        }
    }
}
