﻿using System;
using System.Collections.Generic;
using System.Web;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.Processor.Views.Rating;
using LogisticsPlus.Eship.WebApplication.Members;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Utilities.ImmitationViews
{
	internal class TruckloadTenderingPrcessorImmitationView : MemberPageBase, IHttpContextImmitationView, ICustomerTLTenderingProfile, ITruckloadBidView, ILoadOrderView
	{
		
		public override ViewCode PageCode
		{
			get { return ViewCode.TruckloadTenderingProfile; }
		}
		public override string SetPageIconImage
		{
			set { }
		}

		public TruckloadTenderingPrcessorImmitationView(User user, HttpServerUtility server)
		{
			ActiveUser = user;
			Server = server;
			TruckloadBidMsgs = new List<ValidationMessage>();
			new TruckloadBidHandler(this).Initialize();
			new LoadOrderHandler(this).Initialize();
		}
		
		public event EventHandler<ViewEventArgs<TruckloadBid, LoadOrder>> TruckloadBidSave;
		public event EventHandler<ViewEventArgs<TruckloadBid>> TruckloadBidDelete;
		public event EventHandler<ViewEventArgs<TruckloadBid>> TruckloadBidLock;
		public event EventHandler<ViewEventArgs<TruckloadBid>> TruckloadBidUnLock;
		public event EventHandler<ViewEventArgs<TruckloadBid>> TruckloadBidLoadAuditLog;

		public event EventHandler<ViewEventArgs<CustomerTLTenderingProfile>> TLProfileSave;
		public event EventHandler<ViewEventArgs<CustomerTLTenderingProfile>> TLProfileDelete;
		public event EventHandler<ViewEventArgs<CustomerTLTenderingProfile>> TLProfileLock;
		public event EventHandler<ViewEventArgs<CustomerTLTenderingProfile>> TLProfileUnLock;
		public event EventHandler<ViewEventArgs<CustomerTLTenderingProfile>> TLProfileLoadAuditLog;
		public event EventHandler<ViewEventArgs<long>> TLProfileCanDeleteLane;
		
		public event EventHandler Loading;
		public event EventHandler<ViewEventArgs<LoadOrder>> Save;
		public event EventHandler<ViewEventArgs<LoadOrder>> Delete;
		public event EventHandler<ViewEventArgs<LoadOrder>> Lock;
		public event EventHandler<ViewEventArgs<LoadOrder>> UnLock;
		public event EventHandler<ViewEventArgs<LoadOrder>> LoadAuditLog;
		public event EventHandler<ViewEventArgs<string>> CustomerSearch;
		public event EventHandler<ViewEventArgs<string>> VendorSearch;
		public event EventHandler<ViewEventArgs<LoadOrder, Shipment>> SaveConvertToShipment;

		public bool RefundOrVoidMiscReceipts { get; private set; }
		public bool SaveOriginToAddressBook { get; private set; }
		public bool SaveDestinationToAddressBook { get; private set; }
		public List<ServiceViewSearchDto> Services { set; private get; }
		public List<EquipmentType> EquipmentTypes { set; private get; }
		public Dictionary<int, string> DateUnits { set; private get; }
		public Dictionary<int, string> NoteTypes { set; private get; }
		public Dictionary<int, string> ServiceModes { set; private get; }

		public void Set(CustomerTLTenderingProfile profile)
		{
			// Not implemented
		}

		public void DisplayCustomer(Customer customer)
		{
			// Not implemented
		}

		public void DisplayVendorForLane(Vendor vendor)
		{
			// Not implemented
		}

		public void LogException(Exception ex)
		{
			// Not implemented
		}

		public void DisplayCustomer(Customer customer, bool updateSalesRepAndResellers)
		{
			// Not implemented
		}

		public void DisplayVendor(Vendor vendor)
		{
			// Not implemented
		}

		public void DisplayMessages(IEnumerable<ValidationMessage> messages)
		{
			// Not implemented
		}

		public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs)
		{
			// Not implemented
		}

		public void FailedLock(Lock @lock)
		{
			// Not implemented
		}

		public List<ValidationMessage> TruckloadBidMsgs { get; set; }

		public void Set(LoadOrder loadOrder)
		{
			// Not implemented
		}

		public void FinalizeConvertToShipment(Shipment shipment)
		{
			// Not implemented
		}
		
		public void SaveOrUpdateBid(TruckloadBid bid, LoadOrder loadOrder)
		{
			if (TruckloadBidSave != null)
					TruckloadBidSave(this, new ViewEventArgs<TruckloadBid, LoadOrder>(bid,loadOrder));
		}

		public void CancelBid(TruckloadBid bid)
		{
			if (TruckloadBidDelete != null)
				TruckloadBidDelete(this, new ViewEventArgs<TruckloadBid>(bid));
		}
		public void UpdateLoadOrder(LoadOrder loadOrder)
		{
			if (Save != null)
				Save(this, new ViewEventArgs<LoadOrder>(loadOrder));
		}
		public void LockLoadOrder(LoadOrder loadOrder)
		{
			if (Lock != null)
				Lock(this, new ViewEventArgs<LoadOrder>(loadOrder));
		}
		public void LockTruckloadBid(TruckloadBid bid)
		{
			if (TruckloadBidLock != null)
				TruckloadBidLock(this, new ViewEventArgs<TruckloadBid>(bid));
		}
		public void UnlockLoadOrder(LoadOrder loadOrder)
		{
			if (UnLock != null)
				UnLock(this, new ViewEventArgs<LoadOrder>(loadOrder));
		}
		public void UnlockTruckloadBid(TruckloadBid bid)
		{
			if (TruckloadBidUnLock != null)
				TruckloadBidUnLock(this, new ViewEventArgs<TruckloadBid>(bid));
		}
	}
}