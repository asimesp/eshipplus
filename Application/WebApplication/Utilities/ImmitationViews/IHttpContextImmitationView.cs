﻿using System.Web;
using LogisticsPlus.Eship.Core.Administration;

namespace LogisticsPlus.Eship.WebApplication.Utilities.ImmitationViews
{
	// interface for check whether or not a view should use HttpContext.Current
	public interface IHttpContextImmitationView
	{
		HttpRequest Request { get; }
		HttpServerUtility Server { get; }
		User ActiveUser { get; set; }
	}
}
