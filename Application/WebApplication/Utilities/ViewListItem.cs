﻿using System;

namespace LogisticsPlus.Eship.WebApplication.Utilities
{
	[Serializable]
	public class ViewListItem
	{
		public string Text { get; set; }
		public string Value { get; set; }

		public ViewListItem()
		{
		}

		public ViewListItem(string text, string value)
		{
			Text = text;
			Value = value;
		}
	}
}
