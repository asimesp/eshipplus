﻿using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Rating;

namespace LogisticsPlus.Eship.WebApplication.Utilities
{
	public class PageBoundary<T>
	{
		public string Name { get; private set; }

		public PageBoundary(string name)
		{
			Name = name;
		}

		public PageBoundary(T item)
		{
			if (item as AreaViewSearchDto != null)
			{
				var area = item as AreaViewSearchDto;
				Name = area.UseSubRegion ? area.SubRegionName : area.PostalCode;
				return;
			}

			if (item as DiscountTier != null)
			{
				var tier = item as DiscountTier;
				Name = string.Format("[{0}/{1}]", tier.OriginRegion.Name, tier.DestinationRegion.Name);
				return;
			}

            if (item as AdditionalChargeIndex != null)
            {
                var additionalChargeIndex = item as AdditionalChargeIndex;
                Name = additionalChargeIndex.UsePostalCode
                           ? additionalChargeIndex.PostalCode
                           : additionalChargeIndex.Region == null ? string.Empty : additionalChargeIndex.Region.Name;
                return;
            }
            if (item as LTLAdditionalCharge != null)
            {
                var additionalCharge = item as LTLAdditionalCharge;
                Name = additionalCharge.Name;
                return;
            }
            if (item as LTLPackageSpecificRate != null)
            {
                var psRate = item as LTLPackageSpecificRate;
                Name = string.Format("[{0}/{1}]", psRate.OriginRegion.Name, psRate.DestinationRegion.Name);
                return;
            }
            if (item as TLSellRate != null)
            {
                var tlSellRate = item as TLSellRate;
                Name = string.Format("[{0} {1} {2}/{3} {4} {5}]", tlSellRate.OriginCity, tlSellRate.OriginState,
                                     tlSellRate.OriginCountry != null ? tlSellRate.OriginCountry.Name : string.Empty,
                                     tlSellRate.DestinationCity, tlSellRate.DestinationState,
                                     tlSellRate.DestinationCountry != null
                                         ? tlSellRate.DestinationCountry.Name
                                         : string.Empty);
                return;
            }

			Name = item.GetString();
		}
	}
}