﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using Ionic.Zip;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Extern;
using LogisticsPlus.Eship.Extern.Edi.Segments;
using LogisticsPlus.Eship.PluginBase.Mileage;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Handlers.Connect;
using LogisticsPlus.Eship.Processor.Rates;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Project44;
using LogisticsPlus.Eship.Smc;
using LogisticsPlus.Eship.Smc.Eva;
using LogisticsPlus.Eship.WebApplication.Members;
using LogisticsPlus.Eship.WebApplication.Members.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.Administration;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence;
using LogisticsPlus.Eship.WebApplication.Members.Connect;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls.InputControls;
using LogisticsPlus.Eship.WebApplication.Members.Operations;
using LogisticsPlus.Eship.WebApplication.Members.Rating;
using LogisticsPlus.Eship.WebApplication.ViewCodes;
using OfficeOpenXml;
using WkHtmlToPdf.NET;
using Availability = LogisticsPlus.Eship.Dat.Availability;
using ClosestTruckStops = LogisticsPlus.Eship.Dat.ClosestTruckStops;
using CountryCode = LogisticsPlus.Eship.Dat.CountryCode;
using Dimensions = LogisticsPlus.Eship.Dat.Dimensions;
using ItemChoiceType = LogisticsPlus.Eship.Dat.ItemChoiceType;
using LookupRateOperation = LogisticsPlus.Eship.Dat.LookupRateOperation;
using LookupRateRequest = LogisticsPlus.Eship.Dat.LookupRateRequest;
using NamedPostalCode = LogisticsPlus.Eship.Dat.NamedPostalCode;
using Place = LogisticsPlus.Eship.Dat.Place;
using PostAssetOperation = LogisticsPlus.Eship.Dat.PostAssetOperation;
using PostAssetRequest = LogisticsPlus.Eship.Dat.PostAssetRequest;
using ShipmentRate = LogisticsPlus.Eship.Dat.ShipmentRate;
using ShipmentUpdate = LogisticsPlus.Eship.Dat.ShipmentUpdate;
using StateProvince = LogisticsPlus.Eship.Dat.StateProvince;
using TruckStops = LogisticsPlus.Eship.Dat.TruckStops;
using UpdateAssetOperation = LogisticsPlus.Eship.Dat.UpdateAssetOperation;
using UpdateAssetRequest = LogisticsPlus.Eship.Dat.UpdateAssetRequest;

namespace LogisticsPlus.Eship.WebApplication.Utilities
{
    public static class WebApplicationUtilities
    {
        private static readonly List<ViewCode> ExcludedViewCodes =
            new List<ViewCode>
				{
                    ViewCode.CustomerInvoiceDocument,
					ViewCode.CustomerShipmentDetail,
					ViewCode.Dashboard,
					ViewCode.DocDelivery,
					ViewCode.DocumentProcessorPage,
					ViewCode.SupportEmail,
					ViewCode.TariffsDetailLTL,
                    ViewCode.UserGuide,
					ViewCode.UserProfile,
					ViewCode.WebServices
				};

        public static bool HasErrors(this IEnumerable<ValidationMessage> messages)
        {
            return messages.Count(m => m.Type == ValidateMessageType.Error) > 0;
        }

        public static bool HasWarnings(this IEnumerable<ValidationMessage> messages)
        {
            return messages.Count(m => m.Type == ValidateMessageType.Warning) > 0;
        }

        public static MessageIcon GenerateIcon(this IEnumerable<ValidationMessage> messages)
        {
            return messages == null
                    ? MessageIcon.Error
                    : messages.HasErrors()
                        ? MessageIcon.Error
                        : messages.HasWarnings() ? MessageIcon.Warning : MessageIcon.Information;
        }



        public static List<ViewListItem> GetEdiStatusCodes()
        {
            return ProcessorUtilities
                .GetAll<ShipStatMsgCode>().Values
                .Select(v => new ViewListItem(string.Format("{0} ({1})", v.ToEnum<ShipStatMsgCode>().GetDescription(), v), v))
                .OrderBy(v => v.Text)
                .ToList();
        }


        public static List<T> PadDashboardItemConfiguration<T>(this List<T> configurations, int maxColumns) where T : class, new()
        {
            var needed = maxColumns - configurations.Count % maxColumns;
            if (needed == maxColumns) needed = 0;
            for (int i = needed; i > 0; i--)
                configurations.Add(new T());
            return configurations;
        }



        public static bool HasAccessToAll(this User user, params ViewCode[] codes)
        {
            return codes.All(user.HasAccessTo);
        }

        public static bool HasAccessToAll(this User user, params string[] codes)
        {
            return codes.All(user.HasAccessTo);
        }

        public static bool HasAccessToModifyAll(this User user, params ViewCode[] codes)
        {
            return codes.All(user.HasAccessToModify);
        }

        public static bool HasAccessToModifyAll(this User user, params string[] codes)
        {
            return codes.All(user.HasAccessToModify);
        }

        public static bool HasAccessToCustomer(this User user, long customerId)
        {
            return user.TenantEmployee ||
                   user.DefaultCustomer.Id == customerId ||
                   user.UserShipAs.Any(usa => usa.CustomerId == customerId);
        }

        public static bool HasAccessTo(this User user, params ViewCode[] codes)
        {
            return codes.Any(user.HasAccessTo);
        }

        public static bool HasAccessTo(this User user, params string[] codes)
        {
            return codes.Any(user.HasAccessTo);
        }

        public static bool HasAccessTo(this User user, ViewCode code)
        {
            var p = user.RetrievePermission(code);
            return p.Grant && !p.Deny;
        }

        public static bool HasAccessTo(this User user, string code)
        {
            var p = user.RetrievePermission(code);
            return p.Grant && !p.Deny;
        }

        public static bool HasAccessToModify(this User user, params ViewCode[] codes)
        {
            return codes.Any(user.HasAccessToModify);
        }

        public static bool HasAccessToModify(this User user, params string[] codes)
        {
            return codes.Any(user.HasAccessToModify);
        }

        public static bool HasAccessToModify(this User user, ViewCode code)
        {
            var p = user.RetrievePermission(code);
            return p.Grant && !p.Deny && p.Modify;
        }

        public static bool HasAccessToModify(this User user, string code)
        {
            var p = user.RetrievePermission(code);
            return p.Grant && !p.Deny && p.Modify;
        }

        public static bool HasAccessToDelete(this User user, ViewCode code)
        {
            var p = user.RetrievePermission(code);
            return p.Grant && !p.Deny && p.Remove;
        }

        public static bool HasAccessToDelete(this User user, string code)
        {
            var p = user.RetrievePermission(code);
            return p.Grant && !p.Deny && p.Remove;
        }

        public static bool ExcludePermission(this ViewCode code)
        {
            return ExcludedViewCodes.Contains(code);
        }

        public static Permission RetrievePermission(this User user, ViewCode code)
        {
            var permission = user.AllPermissions.FirstOrDefault(p => p.Code == code.ToString());
            return permission ?? new Permission { Code = code.ToString(), Deny = !code.ExcludePermission() };
        }

        public static Permission RetrievePermission(this User user, string code)
        {
            var permission = user.AllPermissions.FirstOrDefault(p => p.Code == code);
            return permission ?? new Permission { Code = code, Deny = false };
        }

        public static string RetrievePageLogoUrl(this User user)
        {
            return user.DefaultCustomer != null
                    ? user.DefaultCustomer.RetrievePageLogoUrl()
                    : user.Tenant != null
                        ? user.Tenant.LogoUrl
                        : WebApplicationConstants.AppDefaultLogoUrl;
        }

        public static string RetrievePageLogoUrl(this Customer customer)
        {
            if (!string.IsNullOrEmpty(customer.LogoUrl))
                return customer.LogoUrl;
            if (!string.IsNullOrEmpty(customer.Tier.LogoUrl))
                return customer.Tier.LogoUrl;
            return customer.Tenant.LogoUrl;
        }



        public static string ResolveSiteRoot(this HttpRequest request)
        {
            var serverName = request.ServerVariables["SERVER_NAME"];
            if (serverName.ToLower() == "eshipplus.com" || serverName.ToLower() == "eshipplus.net")
                serverName = "www." + serverName;
            var port = request.ServerVariables["SERVER_PORT"];
            var addPort = string.IsNullOrEmpty(port) || port.Equals("80") || port.Equals("443") ? string.Empty : ":" + port;
            var virtualDir = request.ApplicationPath ?? string.Empty; // if !ends with '/' then virtual dir root

            return virtualDir.EndsWith("/") ? serverName + addPort : serverName + addPort + virtualDir;
        }

        public static string ResolveSiteRootWithHttp(this HttpRequest request)
        {
            return string.Format("http{0}://{1}", WebApplicationSettings.SiteSslEnabled ? "s" : string.Empty, request.ResolveSiteRoot());
        }



        public static bool HasVisibleDashboardHoverItems(this IEnumerable<ListViewDataItem> items)
        {
            var dashboardItems = items.Select(i => i.FindControl("items") as DashboardHoverItem).Where(d => d != null).ToList();
            return dashboardItems.Count != 0 &&
                   dashboardItems.Count(d => d.Visible && d.ViewCode != ViewCode.DashboardPadding) > 0;
        }



        public static string BuildFailedLockMessage(this Lock @lock)
        {
            return string.Format("Record in use by: {0} Date: {1: yyyy-MM-dd hh:mm:ss}", @lock.User.Username,
                                 @lock.LockDateTime);
        }


        public static string BuildTabCount<T>(this IEnumerable<T> list)
        {
            return BuildTabCount(list, string.Empty);
        }

        public static string BuildTabCount<T>(this IEnumerable<T> list, string title)
        {
            return list != null && list.Any()
                    ? string.Format("{0} <span class='tabHeaderSuperscript'>{1}</span>", title, list.Count())
                    : title;
        }

        public static string BuildRecordCount<T>(this ICollection<T> list)
        {
            return list.Any()
                       ? string.Format("({0} record{1} found)", list.Count(), list.Count() > 1 ? "s" : string.Empty)
                       : string.Empty;
        }



        public static bool HasGettableProperty<T>(this T entity, string propertyName) where T : class
        {
            return entity.GetType().GetProperties().Count(p => p.Name == propertyName && p.CanRead) > 0;
        }

        public static object GetPropertyValue<T>(this T entity, string propertyName) where T : class
        {
            var property = entity.GetType().GetProperties().FirstOrDefault(p => p.Name == propertyName && p.CanRead);
            return property == null ? string.Empty : property.GetValue(entity, null);
        }

        public static bool HasSettableProperty<T>(this T entity, string propertyName) where T : class
        {
            return entity.GetType().GetProperties().Count(p => p.Name == propertyName && p.CanWrite) > 0;
        }

        public static bool SetPropertyValue<T>(this T entity, string propertyName, object value) where T : class
        {
            var property = entity.GetType().GetProperties().FirstOrDefault(p => p.Name == propertyName && p.CanWrite);
            if (property != null)
            {
                property.SetValue(entity, value, null);
                return true;
            }
            return false;
        }


        public static char[] ImportSeperators()
        {
            return new[] { '\t', ',', ';', ':' };
        }

        public static char[] Seperators()
        {
            var chars = ImportSeperators().ToList();
            chars.AddRange(Environment.NewLine.ToArray());
            return chars.ToArray();
        }


        public static void Export(this HttpResponse response, string data)
        {
            response.Export(data.ToUtf8Bytes());
        }

        public static void Export(this HttpResponse response, string data, string filename)
        {
            response.Export(data.ToUtf8Bytes(), filename);
        }

        public static void Export(this HttpResponse response, byte[] data)
        {
            response.Export(data, Guid.NewGuid().ToString() + ".txt");
        }

        public static void Export(this HttpResponse response, byte[] data, string filename)
        {
            var cleanFile = filename.Replace(" ", string.Empty);
            response.Clear();
            response.ContentType = "application/octet-stream";
            response.AddHeader("Content-Type", "binary/octet-stream");
            response.AddHeader("Content-Length", data.Length.ToString());
            response.AddHeader("Content-Disposition", string.Format("attachment; filename=\"{0}\"; size={1}", cleanFile, data.Length));
            response.BinaryWrite(data);
            response.Flush();
            //response.End();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }

        public static void Export(this HttpResponse response, FileInfo info, string filename = "")
        {
            var cleanFile = filename.Replace(" ", string.Empty);
            if (string.IsNullOrEmpty(cleanFile)) cleanFile = info.Name;
            response.Clear();
            response.ContentType = "application/octet-stream";
            response.AddHeader("Content-Type", "binary/octet-stream");
            response.AddHeader("Content-Length", info.Length.GetString());
            response.AddHeader("Content-Disposition", string.Format("attachment; filename=\"{0}\"; size={1}", cleanFile, info.Length));

            using (var reader = new BinaryReader(info.OpenRead()))
            {
                for (var i = 0; i < info.Length; i++)
                    response.BinaryWrite(new[] { reader.ReadByte() });
            }

            response.Flush();
            //response.End();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }



        public static string GetFileName(this HttpServerUtility server, string relativeFilePath)
        {
            return string.IsNullOrEmpty(relativeFilePath) ? string.Empty : new FileInfo(server.MapPath(relativeFilePath)).Name;
        }

        public static string GetFileExtension(this HttpServerUtility server, string relativeFilePath)
        {
            return string.IsNullOrEmpty(relativeFilePath) ? string.Empty : Path.GetExtension(server.MapPath(relativeFilePath));
        }

        public static void WriteToFile(this FileUpload fileUpload, string physicalPath, string oldFile)
        {
            fileUpload.WriteToFile(physicalPath, fileUpload.FileName, oldFile);
        }

        public static void WriteToFile(this FileUpload fileUpload, string physicalPath, string filename, string oldFile)
        {
            fileUpload.FileBytes.WriteToFile(physicalPath, filename, oldFile);
        }

        public static void WriteToFile(this byte[] contents, string physicalPath, string filename, string oldFile)
        {
            if (!string.IsNullOrEmpty(oldFile) && File.Exists(oldFile)) File.Delete(oldFile);

            if (!Directory.Exists(physicalPath)) Directory.CreateDirectory(physicalPath);
            using (var stream = new FileStream(Path.Combine(physicalPath, filename), FileMode.Create))
            using (var writer = new BinaryWriter(stream))
                writer.Write(contents);
        }

        public static byte[] ReadFromFile(this HttpServerUtility server, string relativeFilePath)
        {
            if (string.IsNullOrEmpty(relativeFilePath)) return new byte[0];

            var file = server.MapPath(relativeFilePath);
            if (!File.Exists(file)) return new byte[0];
            return file.ReadFromFile();
        }

        public static byte[] ReadFromFile(this string fullFilePath)
        {
            if (!File.Exists(fullFilePath)) return new byte[0];
            try
            {
                byte[] contents;

                using (var stream = new FileStream(fullFilePath, FileMode.Open, FileAccess.Read))
                {
                    contents = new byte[stream.Length];
                    for (var i = 0; i < stream.Length; i++)
                        stream.Read(contents, i, 1);
                }

                return contents;
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex, HttpContext.Current);
                return new byte[0];
            }
        }

        public static bool RemoveFiles(this HttpServerUtility server, string[] files)
        {
            try
            {
                foreach (var file in files.Select(server.MapPath).Where(File.Exists))
                    File.Delete(file);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool RemoveDirectory(this HttpServerUtility server, string directoryPath)
        {
            try
            {
                var path = server.MapPath(directoryPath);
                if (Directory.Exists(path)) Directory.Delete(path, true);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static string MakeUniqueFilename(this HttpServerUtility server, string filePath, bool filePathIsAbsolute = false)
        {
            var path = filePathIsAbsolute ? filePath : server.MapPath(filePath);
            var fi = new FileInfo(path);
            var cnt = 1;
            var newPath = path;
            while (File.Exists(newPath))
                newPath = string.Format("{0}({1}){2}", path.Replace(fi.Extension, string.Empty), cnt++, fi.Extension);
            return newPath;
        }



        public static List<string[]> GetImportData(this FileUploaderEventArgs arg, bool removeEmptyRow, bool removeHeader = true)
        {
            List<string[]> lines;

            if (arg.Filename.EndsWith(ReportExportExtension.xlsx.GetString()))
            {
                // reading from xlsx file
                var pck = new ExcelPackage(arg.Stream);
                lines = GetLinesFromPackage(removeEmptyRow, removeHeader, pck);
            }
            else
            {
                // reading from txt file
                lines = GetLinesFromReader(removeEmptyRow, removeHeader, arg.Reader);
            }

            return lines;
        }

        public static List<string[]> GetImportData(this FileUpload control, bool removeEmptyRow, bool removeHeader = true)
        {
            List<string[]> lines;

            if (control.FileName.EndsWith(ReportExportExtension.xlsx.GetString()))
            {
                // reading from xlsx file
                var pck = new ExcelPackage(control.FileContent);
                lines = GetLinesFromPackage(removeEmptyRow, removeHeader, pck);
            }
            else
            {
                // reading from txt file
                lines = GetLinesFromReader(removeEmptyRow, removeHeader, new StreamReader(control.FileContent));
            }

            return lines;
        }

        private static List<string[]> GetLinesFromReader(bool removeEmptyRow, bool removeHeader, StreamReader reader)
        {
            string contents;
            using (reader) contents = reader.ReadToEnd();
            var allLines = contents.Split(Environment.NewLine.ToArray(), StringSplitOptions.RemoveEmptyEntries).ToList();
            if (removeHeader && allLines.Count > 0) allLines.RemoveAt(0); // remove header
            List<string[]> lines = removeEmptyRow
                             ? allLines
                                   .Select(l => l.Split(ImportSeperators()))
                                   .Where(l => !string.IsNullOrEmpty(l.SpaceJoin().Trim()))
                                   .ToList()
                             : allLines
                                   .Select(l => l.Split(ImportSeperators()))
                                   .ToList();
            return lines;
        }

        private static List<string[]> GetLinesFromPackage(bool removeEmptyRow, bool removeHeader, ExcelPackage pck)
        {
            var ws = pck.Workbook.Worksheets.FirstOrDefault(s => s.Name.ToLower() == WebApplicationConstants.DataImportSheetName);
            if (ws == null)
                throw new NullReferenceException(string.Format("Missing import data work sheet. Sheet name must be {0}",
                                                               WebApplicationConstants.DataImportSheetName));

            var lines = new List<string[]>();
            var startIndex = removeHeader ? 2 : 1;
            for (var r = startIndex; r <= ws.Dimension.End.Row; r++)
            {
                var row = new string[ws.Dimension.End.Column];
                for (var c = 1; c <= ws.Dimension.End.Column; c++)
                    row[c - 1] = ws.Cells[r, c].Value.GetString();
                if (removeEmptyRow && !string.IsNullOrEmpty(row.SpaceJoin().Trim())) lines.Add(row);
            }
            return lines;
        }

        public static List<string[]> GetLinesFromPackage(this ExcelPackage pck, bool removeEmptyRow, bool removeHeader, bool forceDataSheet = true, string sheetName = "")
        {
            var sname = string.IsNullOrEmpty(sheetName) || forceDataSheet ? WebApplicationConstants.DataImportSheetName.ToLower() : sheetName;

            var ws = pck.Workbook.Worksheets.FirstOrDefault(s => s.Name.ToLower() == sname.ToLower())
                     ?? (forceDataSheet ? null : pck.Workbook.Worksheets.FirstOrDefault());
            if (ws == null)
                throw new NullReferenceException(String.Format("Missing import data work sheet. Sheet name must be {0}",
                    WebApplicationConstants.DataImportSheetName));

            var lines = new List<string[]>();
            var startIndex = removeHeader ? 2 : 1;
            for (var r = startIndex; r <= ws.Dimension.End.Row; r++)
            {
                var row = new string[ws.Dimension.End.Column];
                for (var c = 1; c <= ws.Dimension.End.Column; c++)
                    row[c - 1] = ws.Cells[r, c].Value.GetString();
                if (removeEmptyRow && !String.IsNullOrEmpty(row.SpaceJoin().Trim())) lines.Add(row);
            }
            return lines;
        }



        public static string AppendImportExportFolder(this string templateFile)
        {
            return Path.Combine(WebApplicationSettings.ImportExportTemplatesFolder, templateFile);
        }



        public static string RetrieveSystemAlerts(this HttpServerUtility server)
        {
            var file = Path.Combine(WebApplicationSettings.BaseFilePath, WebApplicationSettings.SystemAlertsFile);
            var content = string.Empty;
            if (File.Exists(server.MapPath(file)))
                content = server.ReadFromFile(file).FromUtf8Bytes().Replace(Environment.NewLine, "<br />");
            return content;
        }



        public static byte[] ToUtf8Bytes(this string value)
        {
            return string.IsNullOrEmpty(value) ? null : new UTF8Encoding().GetBytes(value);
        }

        public static string FromUtf8Bytes(this byte[] value)
        {
            return value == null ? string.Empty : new UTF8Encoding().GetString(value);
        }

        public static byte[] ToPdf(this string content, string title)
        {
            var pdfCreator = new WkHtmlToPdf.NET.WkHtmlToPdf
                                {
                                    PageSize = PageSize.Letter,
                                    PrintBackground = true,
                                    PrintInGrayscale = false,
                                    Title = title,
                                    TempDirectory = ProcessorVars.TempFolder
                                };

            byte[] data;
            using (var memoryStream = new MemoryStream(content.ToUtf8Bytes()))
            using (var stream = pdfCreator.PdfToStream(memoryStream))
            {
                data = new byte[stream.Length];
                using (var reader = new BinaryReader(stream))
                    for (var i = 0; i < data.Length; i++)
                        reader.Read(data, i, 1);
            }

            return data;
        }


        public static byte[] ToCompressedBytes(this byte[] data)
        {
            var output = new MemoryStream();
            using (var dstream = new DeflateStream(output, System.IO.Compression.CompressionLevel.Optimal))
            {
                dstream.Write(data, 0, data.Length);
            }
            return output.ToArray();
        }

        public static byte[] ToDecompressedBytes(this byte[] data)
        {
            var input = new MemoryStream(data);
            var output = new MemoryStream();
            using (var dstream = new DeflateStream(input, CompressionMode.Decompress))
            {
                dstream.CopyTo(output);
            }
            return output.ToArray();
        }


        public static string Encode(this HttpServerUtility server, string value)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return server.HtmlEncode(value);
        }

        public static string Decode(this HttpServerUtility server, string value)
        {
            if (string.IsNullOrEmpty(value)) return value;
            return server.HtmlDecode(value);
        }

        public static byte[] GetZippedBytes(this ZipFile zip)
        {
            if (zip == null) return new byte[0];
            if (zip.Count == 0) return new byte[0];

            byte[] data;
            using (var stream = zip.GetZipperBytesStream())
                data = stream.ToArray();
            return data;
        }

        public static MemoryStream GetZipperBytesStream(this ZipFile zip)
        {
            if (zip == null) return new MemoryStream();
            if (zip.Count == 0) return new MemoryStream();

            var stream = new MemoryStream();
            zip.Save(stream);
            return stream;
        }



        public static string GetValueText(this DropDownList ddl, string value)
        {
            for (var i = 0; i < ddl.Items.Count; i++)
                if (ddl.Items[i].Value == value)
                    return ddl.Items[i].Text;
            return string.Empty;
        }

        public static string GetValueText(this CheckBoxList cbl, string value)
        {
            for (var i = 0; i < cbl.Items.Count; i++)
                if (cbl.Items[i].Value == value)
                    return cbl.Items[i].Text;
            return string.Empty;
        }

        public static string GetValueText(this List<ViewListItem> items, string value)
        {
            foreach (var t in items.Where(t => t.Value == value))
                return t.Text;
            return string.Empty;
        }

        public static bool ContainsValue(this DropDownList ddl, string value)
        {
            return !string.IsNullOrEmpty(ddl.GetValueText(value));
        }

        public static string GetHtmlTable(this string[][] values)
        {
            if (values == null) return string.Empty;

            var tab = "<table class='contain'>";

            foreach (var t in values)
            {
                tab += "<tr>";
                tab = t.Aggregate(tab, (current, t1) => current + string.Format("<td style='padding: 1px 5px 1px 1px;'>{0}</td>", t1));
                tab += "</tr>";
            }
            tab += "</table>";

            return tab;
        }



        public static double EnsureCorrectFreightClass(this List<double> freightClasses, double intendedFreightClass, double testFreightClass)
        {
            return !freightClasses.Contains(testFreightClass) ? intendedFreightClass : testFreightClass;
        }



        public static string ItemsInvalidForLTLFreightClassRating(this Shipment shipment)
        {
            var freightClases = WebApplicationSettings.FreightClasses;

            var err = new List<string>();

            if (shipment.Items.Any(i => i.ActualWeight == 0)) err.Add(WebApplicationConstants.Weight);
            if (shipment.Items.Any(i => i.Quantity == 0)) err.Add(WebApplicationConstants.Quantity);
            if (string.IsNullOrEmpty(shipment.Origin.PostalCode)) err.Add(WebApplicationConstants.OriginPostalCode);
            if (shipment.Origin.CountryId == default(long)) err.Add(WebApplicationConstants.OriginCountry);
            if (string.IsNullOrEmpty(shipment.Destination.PostalCode)) err.Add(WebApplicationConstants.DestinationPostalCode);
            if (shipment.Destination.CountryId == default(long)) err.Add(WebApplicationConstants.DestinationCountry);
            if (shipment.Customer == null) err.Add(WebApplicationConstants.Customer);
            if (shipment.Items.Any(i => i.ActualLength == 0 || i.ActualWidth == 0 || i.ActualHeight == 0)) err.Add(WebApplicationConstants.Dimensions);
            if (shipment.Items.Any(i => !freightClases.Contains(i.ActualFreightClass))) err.Add(WebApplicationConstants.FreightClass);

            return err.Any() ? string.Format("LTL Freight Class: <br/><ul>{0}</ul>", err.Aggregate(string.Empty, (agg, str) => agg + string.Format("<li>{0}</li>", str))) : string.Empty;
        }

        public static string ItemsInvalidForLTLDensityRating(this Shipment shipment)
        {
            var err = new List<string>();

            if (shipment.Items.Any(i => i.ActualWeight == 0)) err.Add(WebApplicationConstants.Weight);
            if (shipment.Items.Any(i => i.Quantity == 0)) err.Add(WebApplicationConstants.Quantity);
            if (string.IsNullOrEmpty(shipment.Origin.PostalCode)) err.Add(WebApplicationConstants.OriginPostalCode);
            if (shipment.Origin.CountryId == default(long)) err.Add(WebApplicationConstants.OriginCountry);
            if (string.IsNullOrEmpty(shipment.Destination.PostalCode)) err.Add(WebApplicationConstants.DestinationPostalCode);
            if (shipment.Destination.CountryId == default(long)) err.Add(WebApplicationConstants.DestinationCountry);
            if (shipment.Customer == null) err.Add(WebApplicationConstants.Customer);
            if (shipment.Items.Any(i => i.ActualLength == 0 || i.ActualWidth == 0 || i.ActualHeight == 0)) err.Add(WebApplicationConstants.Dimensions);

            return err.Any() ? string.Format("LTL Density: <br/><ul>{0}</ul>", err.Aggregate(string.Empty, (agg, str) => agg + string.Format("<li>{0}</li>", str))) : string.Empty;
        }

        public static string ItemsInvalidForPackageRating(this Shipment shipment)
        {
            var err = new List<string>();

            if (shipment.Items.Any(i => i.ActualWeight == 0)) err.Add(WebApplicationConstants.Weight);
            if (shipment.Items.Any(i => i.Quantity == 0)) err.Add(WebApplicationConstants.Quantity);
            if (string.IsNullOrEmpty(shipment.Origin.PostalCode)) err.Add(WebApplicationConstants.OriginPostalCode);
            if (shipment.Origin.CountryId == default(long)) err.Add(WebApplicationConstants.OriginCountry);
            if (string.IsNullOrEmpty(shipment.Destination.PostalCode)) err.Add(WebApplicationConstants.DestinationPostalCode);
            if (shipment.Destination.CountryId == default(long)) err.Add(WebApplicationConstants.DestinationCountry);
            if (shipment.Customer == null) err.Add(WebApplicationConstants.Customer);
            if (shipment.Items.Any(i => i.ActualLength == 0 || i.ActualWidth == 0 || i.ActualHeight == 0)) err.Add(WebApplicationConstants.Dimensions);
            if (shipment.Items.Any(i => i.PackageTypeId == default(long))) err.Add(WebApplicationConstants.PackageType);

            return err.Any() ? string.Format("Package: <br/><ul>{0}</ul>", err.Aggregate(string.Empty, (agg, str) => agg + string.Format("<li>{0}</li>", str))) : string.Empty;
        }

        public static string ItemsInvalidForFTLRating(this Shipment shipment)
        {
            var err = new List<string>();

            if (shipment.Items.Any(i => i.ActualWeight == 0)) err.Add(WebApplicationConstants.Weight);
            if (shipment.Items.Any(i => i.Quantity == 0)) err.Add(WebApplicationConstants.Quantity);
            if (string.IsNullOrEmpty(shipment.Origin.PostalCode)) err.Add(WebApplicationConstants.OriginPostalCode);
            if (shipment.Origin.CountryId == default(long)) err.Add(WebApplicationConstants.OriginCountry);
            if (string.IsNullOrEmpty(shipment.Destination.PostalCode)) err.Add(WebApplicationConstants.DestinationPostalCode);
            if (shipment.Destination.CountryId == default(long)) err.Add(WebApplicationConstants.DestinationCountry);
            if (shipment.Customer == null) err.Add(WebApplicationConstants.Customer);

            return err.Any() ? string.Format("FTL: <br/><ul>{0}</ul>", err.Aggregate(string.Empty, (agg, str) => agg + string.Format("<li>{0}</li>", str))) : string.Empty;
        }



        private static string ToDocTypePath(this string docType)
        {
            return string.IsNullOrEmpty(docType) ? string.Empty : "XML." + docType;
        }

        public static string GetDocTypeString(this EdiDocumentType docType)
        {
            return docType.GetString().Replace("EDI", string.Empty);
        }

        public static bool SendEdi(this Communication com, string saveAs, byte[] fileContent, EdiDocumentType docType)
        {
            return com.SendEdi(saveAs, fileContent, docType.GetDocTypeString());
        }

        public static bool SendEdi(this Communication com, string saveAs, byte[] fileContent, string docType)
        {
            var useSelectiveDropOff = docType != EdiDocumentType.EDI997.GetDocTypeString() && com.UseSelectiveDropOff;
            var folder = Path.Combine(com.EdiVANDefaultFolder ?? string.Empty, useSelectiveDropOff ? docType.ToDocTypePath() : string.Empty);
            var client = new FtpClient(com.EdiVANUrl, com.EdiVANUsername, com.EdiVANPassword) { IsSslftp = com.SecureEdiVAN, IsSSHFtp = false};
            if (!string.IsNullOrEmpty(folder)) client.SetCurrentDirectory(folder);
            var okay = client.UploadFile(fileContent, saveAs);
            if (!okay && client.Exception != null) ErrorLogger.LogError(client.Exception, HttpContext.Current);
            return okay;
        }

        public static bool SendFtp(this Communication com, string saveAs, byte[] fileContent, EdiDocumentType docType)
        {
            return com.SendFtp(saveAs, fileContent, docType.GetDocTypeString());
        }

        public static bool SendFtp(this Communication com, string saveAs, byte[] fileContent, string docType)
        {
            var useSelectiveDropOff = docType != EdiDocumentType.EDI997.GetDocTypeString() && com.UseSelectiveDropOff;
            var folder = Path.Combine(com.FtpDefaultFolder ?? string.Empty, useSelectiveDropOff ? docType.ToDocTypePath() : string.Empty);
            var client = new FtpClient(com.FtpUrl, com.FtpUsername, com.FtpPassword) { IsSSHFtp = com.SSHFtp,IsSslftp = com.SecureFtp };
            if (!string.IsNullOrEmpty(folder)) client.SetCurrentDirectory(folder);
            var okay = client.UploadFile(fileContent, saveAs);
            if (!okay && client.Exception != null) ErrorLogger.LogError(client.Exception, HttpContext.Current);
            return okay;
        }

        public static string SendDoc(this FtpDocDeliveryProcessor processor, CustomerCommunication com, string saveAs, byte[] fileContent)
        {
            var client = new FtpClient(com.DocDeliveryFtpUrl, com.DocDeliveryFtpUsername, com.DocDeliveryFtpPassword) { IsSslftp = com.DocDeliverySecureFtp, IsSSHFtp = false };
            if (!string.IsNullOrEmpty(com.DocDeliveryFtpDefaultFolder)) client.SetCurrentDirectory(com.DocDeliveryFtpDefaultFolder);
            var okay = client.UploadFile(fileContent, saveAs);
            if (!okay && client.Exception != null) ErrorLogger.LogError(client.Exception, processor.ErrorsFolder);
            return client.Exception == null ? string.Empty : client.Exception.Message;
        }

        public static bool SendFtp(this ReportSchedule schedule, string saveAs, FileInfo info)
        {
            var client = new FtpClient(schedule.FtpUrl, schedule.FtpUsername, schedule.FtpPassword) { IsSslftp = schedule.SecureFtp, IsSSHFtp = false };
            if (!string.IsNullOrEmpty(schedule.FtpDefaultFolder)) client.SetCurrentDirectory(schedule.FtpDefaultFolder);
            var okay = client.UploadFile(info, saveAs);
            if (!okay && client.Exception != null) ErrorLogger.LogError(client.Exception, HttpContext.Current);
            return okay;
        }

        public static List<string> GetEdiFilesListing(this Communication com, EdiDocumentType docType)
        {
            return com.GetEdiFilesListing(docType.GetDocTypeString());
        }
        public static List<string> GetEdiFilesListing(this Communication com, string docType)
        {
            var folder = Path.Combine(com.EdiVANDefaultFolder ?? string.Empty, docType.ToDocTypePath());
            var client = new FtpClient(com.EdiVANUrl, com.EdiVANUsername, com.EdiVANPassword) { IsSslftp = com.SecureEdiVAN, IsSSHFtp = false };
            if (!string.IsNullOrEmpty(folder)) client.SetCurrentDirectory(folder);
            return client.ListDirectory(false);
        }

        public static List<string> GetFtpFilesListing(this Communication com, EdiDocumentType docType)
        {
            return com.GetFtpFilesListing(docType.GetDocTypeString());
        }

        public static List<string> GetFtpFilesListing(this Communication com, string docType)
        {
            var folder = Path.Combine(com.FtpDefaultFolder ?? string.Empty, docType.ToDocTypePath());
			var client = new FtpClient(com.FtpUrl, com.FtpUsername, com.FtpPassword) { IsSSHFtp = com.SSHFtp, IsSslftp = com.SecureFtp };
            if (!string.IsNullOrEmpty(folder)) client.SetCurrentDirectory(folder);
            return client.ListDirectory(false);
        }

        public static bool DeleteFileEdi(this Communication com, string filename, EdiDocumentType docType)
        {
            return com.DeleteFileEdi(filename, docType.GetDocTypeString());
        }

        public static bool DeleteFileEdi(this Communication com, string filename, string docType)
        {
            var folder = Path.Combine(com.EdiVANDefaultFolder ?? string.Empty, docType.ToDocTypePath());
            var client = new FtpClient(com.EdiVANUrl, com.EdiVANUsername, com.EdiVANPassword) { IsSslftp = com.SecureEdiVAN, IsSSHFtp = false };
            if (!string.IsNullOrEmpty(folder)) client.SetCurrentDirectory(folder);
            return client.DeleteFile(filename);
        }

        public static bool DeleteFileFtp(this Communication com, string filename, EdiDocumentType docType)
        {
            return com.DeleteFileFtp(filename, docType.GetDocTypeString());
        }

        public static bool DeleteFileFtp(this Communication com, string filename, string docType)
        {
            var folder = Path.Combine(com.FtpDefaultFolder ?? string.Empty, docType.ToDocTypePath());
			var client = new FtpClient(com.FtpUrl, com.FtpUsername, com.FtpPassword) { IsSSHFtp = com.SSHFtp, IsSslftp = com.SecureFtp };
            if (!string.IsNullOrEmpty(folder)) client.SetCurrentDirectory(folder);
            return client.DeleteFile(filename);
        }

        public static byte[] DownloadEdi(this Communication com, string filename, EdiDocumentType docType)
        {
            return com.DownloadEdi(filename, docType.GetDocTypeString());
        }

        public static byte[] DownloadEdi(this Communication com, string filename, string docType)
        {
            var folder = Path.Combine(com.EdiVANDefaultFolder ?? string.Empty, docType.ToDocTypePath());
            var client = new FtpClient(com.EdiVANUrl, com.EdiVANUsername, com.EdiVANPassword) { IsSslftp = com.SecureEdiVAN, IsSSHFtp = false };
            if (!string.IsNullOrEmpty(folder)) client.SetCurrentDirectory(folder);
            return client.DownloadFile(filename);
        }

        public static byte[] DownloadFtp(this Communication com, string filename, EdiDocumentType docType)
        {
            return com.DownloadFtp(filename, docType.GetDocTypeString());
        }

        public static byte[] DownloadFtp(this Communication com, string filename, string docType)
        {
            var folder = Path.Combine(com.FtpDefaultFolder ?? string.Empty, docType.ToDocTypePath());
			var client = new FtpClient(com.FtpUrl, com.FtpUsername, com.FtpPassword) { IsSSHFtp = com.SSHFtp, IsSslftp = com.SecureFtp };
            if (!string.IsNullOrEmpty(folder)) client.SetCurrentDirectory(folder);
            return client.DownloadFile(filename);
        }



        public static void LogInvoicePrint(this MemberPageBase page, Invoice invoice)
        {
            Exception ex;
            if (!new InvoicePrintLogger().LogPrint(invoice, page.ActiveUser, out ex))
                ErrorLogger.LogError(ex, HttpContext.Current);
        }



        public static List<ValidationMessage> FinalizePackageShipmentBooking(this MemberPageBase page, Shipment shipment)
        {
            var pmaps = ProcessorVars.RegistryCache[page.ActiveUser.TenantId].SmallPackagingMaps;
            var smaps = ProcessorVars.RegistryCache[page.ActiveUser.TenantId].SmallPackageServiceMaps;
            var variables = ProcessorVars.IntegrationVariables[page.ActiveUser.TenantId];
            var rater = new Rater2(variables.RatewareParameters, variables.CarrierConnectParameters,
                                  variables.FedExParameters, variables.UpsParameters, pmaps, smaps);
            foreach (var vendor in shipment.Vendors) vendor.TakeSnapShot();
            var errors = rater.BookShipment(shipment);
            if (errors.Any())
            {
                return errors.Select(e => ValidationMessage.Error(e)).ToList();
            }

            var relativePath = WebApplicationSettings.ShipmentFolder(page.ActiveUser.TenantId, shipment.Id);
            var physicalPath = page.Server.MapPath(relativePath);
            if (!Directory.Exists(physicalPath)) Directory.CreateDirectory(physicalPath);
            foreach (var document in shipment.Documents)
                if (!document.LocationPath.StartsWith("~"))
                {
                    var oldFilePath = document.LocationPath;
                    var newFilePath = Path.Combine(physicalPath, document.Name);
                    document.LocationPath = Path.Combine(relativePath, document.Name);
                    if (!File.Exists(newFilePath))
                    {
                        File.Copy(oldFilePath, newFilePath);
                        File.Delete(oldFilePath);
                    }
                }

            return new List<ValidationMessage>();
        }


        public static string GetBillToString(this CustomerLocation location)
        {
            return location.BillToLocation
                    ? string.Format("[Bill To{0}]", location.MainBillToLocation ? "*" : string.Empty)
                    : string.Empty;
        }

        public static string GetRemitToString(this VendorLocation location)
        {
            return location.RemitToLocation
                    ? string.Format("[Remit To{0}]", location.MainRemitToLocation ? "*" : string.Empty)
                    : string.Empty;
        }


        public static string BuildTerminalDisplay(this LTLTerminalInfo info)
        {
            var b = new StringBuilder();

            b.Append("<p>");
            b.AppendFormat("{0}{1}", info.Name, WebApplicationConstants.HtmlBreak);
            b.AppendFormat("{0}{1}", info.Street1, WebApplicationConstants.HtmlBreak);
            b.AppendFormat("{0}{1}", info.Street2, WebApplicationConstants.HtmlBreak);
            b.AppendFormat("{0} {1} {2}{3}", info.City, info.State, info.PostalCode, WebApplicationConstants.HtmlBreak);
            b.Append("</p>");
            b.Append("<p>");
            b.AppendFormat("{0}: {1}{2}", info.ContactTitle, info.ContactName, WebApplicationConstants.HtmlBreak);
            b.Append("</p>");
            b.Append("<p>");
            b.AppendFormat("P. {0}{1}", info.Phone, WebApplicationConstants.HtmlBreak);
            b.AppendFormat("F. {0}{1}", info.Fax, WebApplicationConstants.HtmlBreak);
            b.AppendFormat("T. {0}{1}", info.TollFree, WebApplicationConstants.HtmlBreak);
            b.AppendFormat("E. {0}{1}", info.Email, WebApplicationConstants.HtmlBreak);
            b.Append("</p>");

            return b.ToString();
        }


        public static SortableExtender ToSortableExtender(this Control control)
        {
            return control as SortableExtender;
        }

        public static CustomTextBox ToTextBox(this Control control)
        {
            return control as CustomTextBox;
        }

        public static LinkButton ToLinkButton(this Control control)
        {
            return control as LinkButton;
        }

        public static Image ToImage(this Control control)
        {
            return control as Image;
        }

        public static Panel ToPanel(this Control control)
        {
            return control as Panel;
        }

        public static ImageButton ToImageButton(this Control control)
        {
            return control as ImageButton;
        }

        public static CheckBox ToCheckBox(this Control control)
        {
            var alt = control as AltUniformCheckBox;
            if (alt != null) return alt.FindControl("chkControl") as CheckBox;
            return control as CheckBox;
        }

        public static AltUniformCheckBox ToAltUniformCheckBox(this Control control)
        {
            return control as AltUniformCheckBox;
        }

        public static DropDownList ToDropDownList(this Control control)
        {
            return control as DropDownList;
        }

        public static CachedObjectDropDownList ToCachedObjectDropDownList(this Control control)
        {
            return control as CachedObjectDropDownList;
        }

        public static Label ToLabel(this Control control)
        {
            return control as Label;
        }

        public static Literal ToLiteral(this Control control)
        {
            return control as Literal;
        }

        public static ListView ToListView(this Control control)
        {
            return control as ListView;
        }

        public static Repeater ToRepeater(this Control control)
        {
            return control as Repeater;
        }

        public static HyperLink ToHyperLink(this Control control)
        {
            return control as HyperLink;
        }

        public static HiddenField ToHiddenField(this Control control)
        {
            return control as HiddenField;
        }

        public static CustomTextBox ToCustomTextBox(this Control control)
        {
            return control as CustomTextBox;
        }

        public static CustomHiddenField ToCustomHiddenField(this Control control)
        {
            return control as CustomHiddenField;
        }

        public static PaginationExtender ToPaginationExtender(this Control control)
        {
            return control as PaginationExtender;
        }

        public static TextAreaMaxLengthExtender ToTextAreaMaxLengthExtender(this Control control)
        {
            return control as TextAreaMaxLengthExtender;
        }

        public static LocationListingInputControl ToLocationListingInputControl(this Control control)
        {
            return control as LocationListingInputControl;
        }

        public static FilteredTextBoxExtender ToFilteredTextBoxExtender(this Control control)
        {
            return control as FilteredTextBoxExtender;
        }

        public static AutoCompleteExtender ToAutoCompleteExtender(this Control control)
        {
            return control as AutoCompleteExtender;
        }

        public static HtmlAnchor ToHtmlAnchor(this Control control)
        {
            return control as HtmlAnchor;
        }

        public static UpdatePanel ToUpdatePanel(this Control control)
        {
            return control as UpdatePanel;
        }


        public static void SetTextBoxEnabled(this TextBox control, bool enabled)
        {
            if (enabled)
            {
                control.ReadOnly = false;
                control.CssClass = control.CssClass.Replace(WebApplicationConstants.CssClassDisabledWithSpace, string.Empty);
            }
            else
            {
                control.ReadOnly = true;
                if (!control.CssClass.Contains(WebApplicationConstants.CssClassDisabledWithSpace))
                    control.CssClass += WebApplicationConstants.CssClassDisabledWithSpace;
            }
        }


        public static List<ValidationMessage> LineLengthsAreValid(this IEnumerable<ImportLineObj> lines, int expectedLength)
        {
            return lines
                .Where(i => i.Length < expectedLength)
                .Select(i => ValidationMessage.Error(WebApplicationConstants.IncorrectImportLineColCntErrMsg, i.Index))
                .ToList();
        }

        public static List<ValidationMessage> EnumsAreValid<T>(this IEnumerable<ImportLineObj> lines, int lineIndex, bool validateAgainstValues = false) where T : struct
        {

            var types = validateAgainstValues
                            ? ProcessorUtilities.GetAll<T>().Values.Select(type => type).ToList()
                            : ProcessorUtilities.GetAll<T>().Keys.Select(type => type.GetString()).ToList();

            var name = typeof(T).Name.FormattedString();
            return lines
                .Where(i => !types.Contains(i.Line[lineIndex]))
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedEnumErrMsg, name, i.Index))
                .ToList();
        }

        public static List<ValidationMessage> CodesAreValid(this IEnumerable<ImportLineObj> lines, int lineIndex, List<string> codes, string codeName)
        {
            return lines
                .Where(i => !codes.Contains(i.Line[lineIndex]))
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedCodeErrMsg, codeName, i.Index))
                .ToList();
        }



        public static string HtmlBreakJoin(this string[] values)
        {
            return string.Join(WebApplicationConstants.HtmlBreak, values);
        }



        public static string GetPageLabel<T>(this ICollection<T> pages, int pageNumber = 1)
        {
            return string.Format("{0} of {1}", pageNumber, pages.Count == 0 ? 1 : pages.Count);
        }

        public static List<List<T>> ToCollectionPages<T>(this ICollection<T> areas, int pageSize)
        {
            var pages = new List<List<T>>();
            for (var i = 0; i < Math.Ceiling(areas.Count / pageSize.ToDouble()).ToInt(); i++)
                pages.Add(areas.Skip(i * pageSize).Take(pageSize).ToList());
            return pages;
        }

        public static List<ViewListItem> GetPageBoundaries<T>(this IEnumerable<List<T>> pages)
        {
            var idx = 0;
            return pages
                .Select(l =>
                    {
                        var st = new PageBoundary<T>(l[0]);
                        var en = new PageBoundary<T>(l[l.Count() - 1]);
                        return new ViewListItem(string.Format("{0}. {1} - {2}", idx + 1, st.Name, en.Name), idx++.GetString());
                    })
                .ToList();
        }



        public static bool CanSendLoadTender(this Vendor vendor, LoadOrderStatus status)
        {
            var invalidTenderStatuses = new List<LoadOrderStatus>
				{
					LoadOrderStatus.Cancelled,
					LoadOrderStatus.Lost,
					LoadOrderStatus.Shipment
				};

            return vendor != null
                   && vendor.Communication != null
                   && (vendor.Communication.EdiEnabled || vendor.Communication.FtpEnabled)
                   && vendor.Communication.DropOff204
                   && !invalidTenderStatuses.Contains(status);
        }



        public static List<double[]> GetCoordinatesFromCheckCalls(this IEnumerable<CheckCall> checkCalls)
        {
            var points = new List<double[]>();
            foreach (var cc in checkCalls.Where(cc => cc.CallNotes.Contains(WebApplicationConstants.MacroPointCheckCallCoordinateSection)).OrderBy(c => c.CallDate))
            {
                var txt = cc.CallNotes.Split(';').First(cn => cn.Contains(WebApplicationConstants.MacroPointCheckCallCoordinateSection));
                var indexOfColon = txt.IndexOf(":", StringComparison.Ordinal);
                var indexOfComma = txt.IndexOf(",", StringComparison.Ordinal);
                points.Add(new[]
						{
							txt.Substring(indexOfColon + 1, indexOfComma - indexOfColon - 1).ToDouble(),
							txt.Substring(indexOfComma + 1, txt.Length - indexOfComma - 1).ToDouble()
						});
            }
            return points;
        }


        public static void InsertShipmentDocRtrvLogFor(this Shipment shipment, User activeUser)
        {
            var primaryVendor = shipment.Vendors.First(v => v.Primary);
            if (shipment.ActualDeliveryDate != DateUtility.SystemEarliestDateTime &&
                !string.IsNullOrEmpty(primaryVendor.ProNumber) &&
                primaryVendor.Vendor.Communication != null && primaryVendor.Vendor.Communication.ImgRtrvEnabled &&
                !new ShipmentSearch().ShipmentDocRtrvLogExistsForShipment(shipment.ShipmentNumber, shipment.TenantId))
            {
                var log = new ShipmentDocRtrvLog
                    {
                        ShipmentNumber = shipment.ShipmentNumber,
                        ExpirationDate = DateTime.Now.AddDays(shipment.Tenant.ShipmentDocImgRtrvAllowance),
                        Tenant = shipment.Tenant,
                        ProNumber = primaryVendor.ProNumber,
                        DateCreated = DateTime.Now,
                        BolDate = DateUtility.SystemEarliestDateTime,
                        PodDate = DateUtility.SystemEarliestDateTime,
                        WniDate = DateUtility.SystemEarliestDateTime,
                        Communication = primaryVendor.Vendor.Communication,
                        User = activeUser,
                    };

                Exception ex;
                new ShipmentDocRtrvLogProcessHandler().InsertImgRtrvLog(log, activeUser, out ex, shipment);
                if (ex != null) ErrorLogger.LogError(ex, HttpContext.Current);
            }
        }


        public static void GoToRecord2(this MemberPageBase page, string entityCode, string entityId)
        {
            //Operations
            if (entityCode == new AddressBook().EntityName())
            {
                page.Session[WebApplicationConstants.TransferAddressBookId] = entityId;
                page.Response.Redirect(AddressBookView.PageAddress);
            }
            else if (entityCode == new Claim().EntityName())
            {
                page.Session[WebApplicationConstants.TransferClaimId] = entityId;
                page.Session[WebApplicationConstants.DisableEditModeOnTransfer] = true;
                page.Response.Redirect(ClaimView.PageAddress);
            }
            else if (entityCode == new Job().EntityName())
            {
                page.Session[WebApplicationConstants.TransferJobId] = entityId;
                page.Session[WebApplicationConstants.DisableEditModeOnTransfer] = true;
                page.Response.Redirect(JobView.PageAddress);
            }
            else if (entityCode == new LoadOrder().EntityName())
            {
                page.Session[WebApplicationConstants.TransferLoadOrderId] = entityId;
                page.Session[WebApplicationConstants.DisableEditModeOnTransfer] = true;
                page.Response.Redirect(LoadOrderView.PageAddress);
            }
            else if (entityCode == new ServiceTicket().EntityName())
            {
                page.Session[WebApplicationConstants.TransferServiceTicketId] = entityId;
                page.Response.Redirect(ServiceTicketView.PageAddress);
            }
            else if (entityCode == new Shipment().EntityName())
            {
                page.Session[WebApplicationConstants.TransferShipmentId] = entityId;
                page.Response.Redirect(ShipmentView.PageAddress);
            }

                //Finance
            else if (entityCode == new Customer().EntityName())
            {
                page.Session[WebApplicationConstants.TransferCustomerId] = entityId;
                page.Response.Redirect(CustomerView.PageAddress);
            }
            else if (entityCode == new Invoice().EntityName())
            {
                page.Session[WebApplicationConstants.TransferInvoiceId] = entityId;
                page.Response.Redirect(InvoiceView.PageAddress);
            }
            else if (entityCode == new Tier().EntityName())
            {
                page.Session[WebApplicationConstants.TransferTierId] = entityId;
                page.Response.Redirect(TierView.PageAddress);
            }
            else if (entityCode == new Vendor().EntityName())
            {
                page.Session[WebApplicationConstants.TransferVendorId] = entityId;
                page.Response.Redirect(VendorView.PageAddress);
            }
            else if (entityCode == new PendingVendor().EntityName())
            {
                page.Session[WebApplicationConstants.TransferPendingVendorId] = entityId;
                page.Response.Redirect(PendingVendorView.PageAddress);
            }
            else if (entityCode == new VendorBill().EntityName())
            {
                page.Session[WebApplicationConstants.TransferVendorBillId] = entityId;
                page.Response.Redirect(VendorBillView.PageAddress);
            }

                //Rating
            else if (entityCode == new CustomerRating().EntityName())
            {
                page.Session[WebApplicationConstants.TransferCustomerRatingId] = entityId;
                page.Response.Redirect(CustomerRatingView.PageAddress);
            }
            else if (entityCode == new Region().EntityName())
            {
                page.Session[WebApplicationConstants.TransferRegionId] = entityId;
                page.Response.Redirect(RegionView.PageAddress);
            }
            else if (entityCode == new ResellerAddition().EntityName())
            {
                page.Session[WebApplicationConstants.TransferResellerAdditionId] = entityId;
                page.Response.Redirect(ResellerAdditionView.PageAddress);
            }
            else if (entityCode == new VendorRating().EntityName())
            {
                page.Session[WebApplicationConstants.TransferVendorRatingId] = entityId;
                page.Response.Redirect(VendorRatingView.PageAddress);
            }
            else if (entityCode == new Group().EntityName())
            {
                page.Session[WebApplicationConstants.TransferGroupId] = entityId;
                page.Response.Redirect(GroupView.PageAddress);
            }
            else if (entityCode == new User().EntityName())
            {
                page.Session[WebApplicationConstants.TransferUserId] = entityId;
                page.Response.Redirect(UserView.PageAddress);
            }

                //Connect
            else if (entityCode == new CustomerCommunication().EntityName())
            {
                page.Session[WebApplicationConstants.TransferCustomerCommunicationId] = entityId;
                page.Response.Redirect(CustomerCommunicationView.PageAddress);
            }
            else if (entityCode == new VendorCommunication().EntityName())
            {
                page.Session[WebApplicationConstants.TransferVendorCommunicationId] = entityId;
                page.Response.Redirect(VendorCommunicationView.PageAddress);
            }
            else if (entityCode == new XmlConnect().EntityName())
            {
                var xc = new XmlConnect(entityId.ToLong());
                var queryString = string.Format("?{0}={1}&{2}={3}", WebApplicationConstants.XmlConnectRecordsDocTypeKey, xc.DocumentType, WebApplicationConstants.XmlConnectRecordsShipmentIdKey, xc.ShipmentIdNumber);
                page.Response.Redirect(string.Format("{0}{1}", XmlConnectRecordView.PageAddress, queryString));
            }

                //Utility
            else if (entityCode == new DeveloperAccessRequest().EntityName())
            {
                page.Session[WebApplicationConstants.TransferDeveloperAccessRequestId] = entityId;
                page.Response.Redirect(DeveloperAccessRequestListingView.PageAddress);
            }

                //BI
            else if (entityCode == new CustomerGroup().EntityName())
            {
                page.Session[WebApplicationConstants.TransferCustomerGroupId] = entityId;
                page.Response.Redirect(CustomerGroupView.PageAddress);
            }

            else if (entityCode == new ReportSchedule().EntityName())
            {
                page.Session[WebApplicationConstants.TransferReportScheduleId] = entityId;
                page.Response.Redirect(ReportSchedulerView.PageAddress);
            }
            else if (entityCode == new ReportConfiguration().EntityName())
            {
                page.Session[WebApplicationConstants.TransferReportConfigurationId] = entityId;
                page.Response.Redirect(ReportConfigurationView.PageAddress);
            }
            else if (entityCode == new VendorGroup().EntityName())
            {
                page.Session[WebApplicationConstants.TransferVendorGroupId] = entityId;
                page.Response.Redirect(VendorGroupView.PageAddress);
            }
        }


        public static void SetSortDisplay(this Literal control, SearchField field, bool sortAscending)
        {
            if (field == null)
            {
                control.Text = WebApplicationConstants.Default;
                return;
            }
            control.Text = string.Format("{0} {1}", field.DisplayName.FormattedString(), sortAscending ? WebApplicationConstants.Ascending : WebApplicationConstants.Descending);
        }


        public static string UrlTextEncrypt(this string value)
        {
            if (string.IsNullOrEmpty(value)) return string.Empty;
            var str = value.EncryptText();
            var num = string.Empty;
            for (var i = 0; i < str.Length; i++)
                num += char.ConvertToUtf32(str, i).ToString() + "-";
            return num;
        }

        public static string UrlTextDecrypt(this string value)
        {
            if (string.IsNullOrEmpty(value)) return string.Empty;
            var parts = value.Split(new[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
            return parts.Aggregate(string.Empty, (current, t) => current + char.ConvertFromUtf32(t.ToInt())).DecryptText();
        }



        public static List<ValidationMessage> CalculateShipmentMiles(this Shipment shipment, MileageEngine engine)
        {
            var msg = new List<ValidationMessage>();
            try
            {
                var plugin = engine.RetrieveEnginePlugin();
                if (plugin == null)
                {
                    msg.Add(ValidationMessage.Error("Unable to find implemented mileage engine for {0}.", engine));
                    return msg;
                }


                const string errorRetrievingMilesForToMessage = "Error retrieving miles for {0} to {1} [Message: {2}].";

                var p1 = shipment.Origin.GetPoint(engine);
                shipment.Origin.MilesFromPreceedingStop = 0m;
                foreach (var stop in shipment.Stops.OrderBy(s => s.StopOrder))
                {
                    var p2 = stop.GetPoint(engine);
                    stop.MilesFromPreceedingStop = plugin.GetMiles(p1, p2).ToDecimal();
                    if (stop.MilesFromPreceedingStop == PluginBase.PluginBaseConstants.ErrorMilesValue.ToInt())
                    {
                        msg.Add(ValidationMessage.Error(errorRetrievingMilesForToMessage, p1.PostalCode, p2.PostalCode,
                                                        plugin.LastErrorMessage));
                        stop.MilesFromPreceedingStop = 0;
                    }
                    p1 = p2;
                }
                var pd = shipment.Destination.GetPoint(engine);
                shipment.Destination.MilesFromPreceedingStop = plugin.GetMiles(p1, pd).ToDecimal();
                if (shipment.Destination.MilesFromPreceedingStop == PluginBase.PluginBaseConstants.ErrorMilesValue.ToInt())
                {
                    msg.Add(ValidationMessage.Error(errorRetrievingMilesForToMessage, p1.PostalCode, pd.PostalCode,
                                                    plugin.LastErrorMessage));
                    shipment.Destination.MilesFromPreceedingStop = 0;
                }

                shipment.Mileage = shipment.Stops.Sum(s => s.MilesFromPreceedingStop) + shipment.Destination.MilesFromPreceedingStop;

                return msg;
            }
            catch (Exception e)
            {
                ErrorLogger.LogError(e, ProcessorVars.ErrorFolder);
                return new List<ValidationMessage>
					{
						ValidationMessage.Error("Error calculating shipment miles. Error [{0}]", e.ToString())
					};
            }
        }

        public static List<ValidationMessage> CalculateShipmentMiles(this LoadOrder loadOrder, MileageEngine engine)
        {
            var msg = new List<ValidationMessage>();
            try
            {
                var plugin = engine.RetrieveEnginePlugin();
                if (plugin == null)
                {
                    msg.Add(ValidationMessage.Error("Unable to find implemented mileage engine for {0}.", engine));
                    return msg;
                }


                const string errorRetrievingMilesForToMessage = "Error retrieving miles for {0} to {1} [Message: {2}].";

                var p1 = loadOrder.Origin.GetPoint(engine);
                loadOrder.Origin.MilesFromPreceedingStop = 0m;
                foreach (var stop in loadOrder.Stops.OrderBy(s => s.StopOrder))
                {
                    var p2 = stop.GetPoint(engine);
                    stop.MilesFromPreceedingStop = plugin.GetMiles(p1, p2).ToDecimal();
                    if (stop.MilesFromPreceedingStop == PluginBase.PluginBaseConstants.ErrorMilesValue.ToInt())
                    {
                        msg.Add(ValidationMessage.Error(errorRetrievingMilesForToMessage, p1.PostalCode, p2.PostalCode,
                                                        plugin.LastErrorMessage));
                        stop.MilesFromPreceedingStop = 0;
                    }
                    p1 = p2;
                }
                var pd = loadOrder.Destination.GetPoint(engine);
                loadOrder.Destination.MilesFromPreceedingStop = plugin.GetMiles(p1, pd).ToDecimal();
                if (loadOrder.Destination.MilesFromPreceedingStop == PluginBase.PluginBaseConstants.ErrorMilesValue.ToInt())
                {
                    msg.Add(ValidationMessage.Error(errorRetrievingMilesForToMessage, p1.PostalCode, pd.PostalCode,
                                                    plugin.LastErrorMessage));
                    loadOrder.Destination.MilesFromPreceedingStop = 0;
                }

                loadOrder.Mileage = loadOrder.Stops.Sum(s => s.MilesFromPreceedingStop) + loadOrder.Destination.MilesFromPreceedingStop;

                return msg;
            }
            catch (Exception e)
            {
                ErrorLogger.LogError(e, ProcessorVars.ErrorFolder);
                return new List<ValidationMessage>
					{
						ValidationMessage.Error("Error calculating shipment miles. Error [{0}]", e.ToString())
					};
            }
        }

        public static Point GetPoint(this Location location, MileageEngine engine)
        {
            return new Point
                {
                    Type = PointType.Address,
                    PostalCode = location.PostalCode,
                    Country = engine.IsApplicationDefault()
                                  ? location.CountryId.GetString()
                                  : location.Country == null ? string.Empty : location.Country.Code
                };
        }



        public static PostAssetRequest ToPostAssetRequest(this LoadOrder loadOrder, ShipmentRate rate, Dat.EquipmentType equipmentType, List<string> comments)
        {
            var request = new PostAssetRequest
                {
                    postAssetOperations = new[]
                        {
                            new PostAssetOperation
                                {
                                    Item = new Dat.Shipment
                                        {
                                            origin = loadOrder.Origin.ToDatPlace(),
                                            destination = loadOrder.Destination.ToDatPlace(),
                                            rate = rate,
                                            equipmentType = equipmentType,
                                            truckStops = new TruckStops
                                                {
                                                    Item = new ClosestTruckStops()
                                                },
                                        },
                                        ltl = loadOrder.ServiceMode == ServiceMode.LessThanTruckload,
                                        ltlSpecified = true,
                                        postersReferenceId = loadOrder.LoadOrderNumber,
                                        availability = new Availability
                                            {
                                                earliest = loadOrder.DesiredPickupDate.SetTime(loadOrder.EarlyPickup),
                                                earliestSpecified = true,
                                                latest = loadOrder.DesiredPickupDate.SetTime(loadOrder.LatePickup),
                                                latestSpecified = true,
                                            },
                                        dimensions = loadOrder.ServiceMode == ServiceMode.LessThanTruckload 
                                                        ? new Dimensions
                                                            {
                                                                weightPounds = (int)loadOrder.Items.Sum(i => i.Weight),
                                                                weightPoundsSpecified = true,
                                                                volumeCubicFeet = (int)loadOrder.Items.Sum(i => (i.Length/12m) * (i.Width/12m) * (i.Height/12m)),
                                                                volumeCubicFeetSpecified = true
                                                            }
                                                        : null,
                                        includeAsset = true,
                                        includeAssetSpecified = true,
                                        stops = 1 + loadOrder.Stops.Count,
                                        stopsSpecified = true,
                                }
                        }
                };

            var commentList = new List<string>();
            if (comments != null && comments.Any()) commentList.AddRange(comments);
            if (loadOrder.HazardousMaterial) commentList.Add("Contains Hazardous Materials");
            if (commentList.Any()) request.postAssetOperations[0].comments = commentList.ToArray();

            return request;
        }

        public static UpdateAssetRequest ToUpdateAssetRequest(this LoadOrder loadOrder, ShipmentRate rate, List<string> comments, string assetId)
        {

            return new UpdateAssetRequest
                {
                    updateAssetOperation = new UpdateAssetOperation
                        {
                            Item = assetId,
                            ItemElementName = ItemChoiceType.assetId,
                            Item1 = new ShipmentUpdate
                                {
                                    ltl = loadOrder.ServiceMode == ServiceMode.LessThanTruckload,
                                    ltlSpecified = true,
                                    stops = 1 + loadOrder.Stops.Count,
                                    stopsSpecified = true,
                                    rate = rate,
                                    comments = comments.ToArray()
                                },

                        },
                };
        }

        public static LookupRateRequest ToLookupRateRequest(this LoadOrder loadOrder)
        {
            return new LookupRateRequest
            {
                lookupRateOperations = new[]
                        {
                            new LookupRateOperation
                                {
                                    origin = loadOrder.Origin.ToDatPlace(),
                                    destination = loadOrder.Destination.ToDatPlace(),
                                    equipment = loadOrder.GetDatRateEquipmentCategory(),
                                    includeSpotRate = true,
                                    includeSpotRateSpecified = true
                                }
                        }
            };
        }

        public static string GetDatRateViewUrl(this long loadOrderId)
        {
            var loadOrder = new LoadOrder(loadOrderId);

            if (!loadOrder.Equipments.Any()) return "#";

            var equipmentType = loadOrder.Equipments.First().EquipmentType.DatEquipmentType;

            if (equipmentType == DatEquipmentType.NotApplicable) return "#";

            var origin = string.Format("{0},{1}", loadOrder.Origin.City, loadOrder.Origin.State);
            var destination = string.Format("{0},{1}", loadOrder.Destination.City, loadOrder.Destination.State);

            return string.Format(WebApplicationSettings.DatRateViewHttpsUrl + "{0}&to={1}&equipment={2}&market={3}",
                          origin,
                          destination,
                          equipmentType,
                          "spot");
        }

        private static Place ToDatPlace(this Location location)
        {
            return new Place
                {
                    Item = new NamedPostalCode
                        {
                            city = location.City.ToDatCityName(),
                            postalCode = new Dat.PostalCode
                                {
                                    code = location.PostalCode,
                                    country = location.Country.ToDatCountryCode(),
                                },
                            stateProvince = location.State.ToDatStateOrProvince()
                        },
                };
        }

        private static CountryCode ToDatCountryCode(this Country country)
        {
            switch (country.Code)
            {
                case "US": return CountryCode.US;
                case "MX": return CountryCode.MX;
                case "CA": return CountryCode.CA;
                default: throw new Exception(string.Format("Cannot convert {0} to a DAT country code whose valid values must be 'US' for United States, 'CA' for Canada, or 'MX' for Mexico", country.Code));
            }
        }

        private static StateProvince ToDatStateOrProvince(this string stateOrProvince)
        {
            try
            {
                return stateOrProvince.ToEnum<StateProvince>();
            }
            catch (Exception)
            {
                throw new Exception(string.Format("Cannot convert {0} to a DAT state or province code.", stateOrProvince));
            }
        }

        private static Dat.RateEquipmentCategory GetDatRateEquipmentCategory(this LoadOrder loadOrder)
        {
            if (loadOrder.Equipments.Any())
                switch (loadOrder.Equipments.First().EquipmentType.DatEquipmentType)
                {
                    case DatEquipmentType.Flatbed:
                        return Dat.RateEquipmentCategory.Flatbeds;
                    case DatEquipmentType.Reefer:
                        return Dat.RateEquipmentCategory.Reefers;
                    case DatEquipmentType.Van:
                        return Dat.RateEquipmentCategory.Vans;
                }
            return Dat.RateEquipmentCategory.Flatbeds;
        }

        public static string ToDatCityName(this string city)
        {
            if (string.IsNullOrEmpty(city)) return string.Empty;

            var abbrCity = city.ToUpper();

            var words = new List<string>();
            while (abbrCity.IndexOf(" ", StringComparison.Ordinal) != -1)
            {
                var idx = abbrCity.IndexOf(" ", StringComparison.Ordinal);
                words.Add(abbrCity.Substring(0, idx));
                abbrCity = abbrCity.Substring(idx + 1, abbrCity.Length - idx - 1);
            }
            words.Add(abbrCity);

            var multiWordCity = words.Count > 1;
            var firstWord = words.First();
            var lastWord = words.Last();

            words.RemoveAt(0);
            if (words.Any()) words.RemoveAt(words.Count - 1);

            if (firstWord == "EAST") firstWord = "E";
            if (firstWord == "FORT") firstWord = "FT";
            if (firstWord == "GR") firstWord = "GRAND";
            if (firstWord == "MOUNT") firstWord = "MT";
            if (firstWord == "MOUNTAIN") firstWord = "MTN";
            if (firstWord == "NORTH") firstWord = "N";
            if (firstWord == "SAINT") firstWord = "ST";
            if (firstWord == "SAINTE") firstWord = "ST";
            if (firstWord == "SNTA") firstWord = "SANTA";
            if (firstWord == "SOUTH") firstWord = "S";
            if (firstWord == "STE") firstWord = "ST";
            if (firstWord == "WEST") firstWord = "W";

            for (var i = 0; i < words.Count; i++)
            {
                if (words[i] == "CHRSTI") words[i] = "CHRISTI";
                if (words[i] == "ISLAND") words[i] = "IS";
                if (words[i] == "PNT" || words[i] == "POINT" || words[i] == "POINTE") words[i] = "PT";
                if (words[i] == "PRAIRIEDUCHEIN") words[i] = "PRAIRIEDUCHIEN";
            }

            if (multiWordCity)
            {
                if (lastWord == "BEA" || lastWord == "BEAC" || lastWord == "BEACH") lastWord = "BCH";
                if (lastWord == "BLUFFS") lastWord = "BLFS";
                if (lastWord == "CENT" || lastWord == "CENTER" || lastWord == "CENTRE") lastWord = "CTR";
                if (lastWord == "CIT") lastWord = "CY";
                if (lastWord == "COMMER") lastWord = "CMMRCE";
                if (lastWord == "CREK" || lastWord == "CREEK") lastWord = "CK";
                if (lastWord == "CTH") lastWord = "CH";
                if (lastWord == "CUCAMON") lastWord = "CUCMNGA";
                if (lastWord == "FAL" || lastWord == "FALL" || lastWord == "FALLS") lastWord = "FLS";
                if (lastWord == "FURNA") lastWord = "FURNC";
                if (lastWord == "GATEW") lastWord = "GTWY";
                if (lastWord == "HARBO") lastWord = "HBR";
                if (lastWord == "HEIGH" || lastWord == "HEIGHT" || lastWord == "HEIGHTS") lastWord = "HTS";
                if (lastWord == "INDUST") lastWord = "INDUS";
                if (lastWord == "ISLE") lastWord = "IS";
                if (lastWord == "JUNCTION") lastWord = "JCT";
                if (lastWord == "KENSINGTON") lastWord = "KENSINGTN";
                if (lastWord == "MILLS") lastWord = "MLS";
                if (lastWord == "MOUNT") lastWord = "MOUNTAIN";
                if (lastWord == "PLAIN" || lastWord == "PLAINS") lastWord = "PLNS";
                if (lastWord == "PRUSSI") lastWord = "PRUSS";
                if (lastWord == "SPRIN" || lastWord == "SPRINGS" || lastWord == "SPRS" || lastWord == "SPS") lastWord = "SPGS";
                if (lastWord == "SPRING") lastWord = "SPG";
                if (lastWord == "STATIO" || lastWord == "STATION") lastWord = "STA";
                if (lastWord == "TOWNSHI" || lastWord == "TOWNSHIP") lastWord = "TWP";
                if (lastWord == "VAL" || lastWord == "VALLEY") lastWord = "VLY";
                if (lastWord == "VILG") lastWord = "VLG";
                if (lastWord == "VILL") lastWord = "VL";
            }

            return string.Format("{0} {1}{2}{3}", firstWord, words.ToArray().SpaceJoin(), words.Any() ? " " : string.Empty, multiWordCity ? lastWord : string.Empty);
        }




        public static DispatchRequest GetDispatchRequest(this VendorCommunication vcom, Shipment shipment, Vendor primaryVendor = null)
        {
            if (primaryVendor == null) primaryVendor = shipment.Vendors.FirstOrDefault(v => v.Primary).Vendor;

            var destContact = (shipment.Destination.Contacts.FirstOrDefault(c => c.Primary) ?? new ShipmentContact());
            var originContact = (shipment.Origin.Contacts.FirstOrDefault(c => c.Primary) ?? new ShipmentContact());

            var hasPickup = shipment.Origin.AppointmentDateTime != DateUtility.SystemEarliestDateTime;

            var dispatchRequest = new DispatchRequest
                {
                    accountToken = vcom.IsInSMC3Testing ? vcom.SMC3TestAccountToken : vcom.SMC3ProductionAccountToken,
                    billToAccount = null,
                    billToAddress1 = null,
                    billToAddress2 = null,
                    billToCity = null,
                    billToCompanyName = null,
                    billToCountry = null,
                    billToEmail = null,
                    billToName = null,
                    billToPhone = null,
                    billToPostalCode = null,
                    billToState = null,
                    destinationAccount = null,
                    destinationAddress1 = shipment.Destination.Street1,
                    destinationAddress2 = shipment.Destination.Street2.EmptyStringToNull(),
                    destinationCity = shipment.Destination.City,
                    destinationCloseTime = null,
                    destinationCompanyName = null,
                    destinationCountry = shipment.Destination.Country.Code,
                    destinationEmail = destContact.Email.EmptyStringToNull(),
                    destinationName = shipment.Destination.Description.EmptyStringToNull(),
                    destinationOpenTime = null,
                    destinationPhone = originContact.Phone.FormattedSmc3EvaPhone(),
                    destinationPostalCode = shipment.Destination.PostalCode,
                    destinationState = shipment.Destination.State,
                    // TODO EVA DISPATCH: Dispatch codes have changed. Refer to Appendix F in docs.
                    dispatchCode = vcom.IsInSMC3Testing ? DispatchCodes.Test : DispatchCodes.Create,
                    equipmentID = null,
                    equipmentType = null,
                    grossWeight = Math.Ceiling(shipment.Items.TotalActualWeight()).ToInt().GetString(),
                    grossWeightUnit = "LBS",
                    identifiers =
                        new List<Identifier>
							{
								new Identifier {referenceNumber = shipment.ShipmentNumber, referenceType = ReferenceTypes.BillOfLadingNumber}
							},
                    originEmail = originContact.Email.EmptyStringToNull(),
                    originAccount = null,
                    originAddress1 = shipment.Origin.Street1,
                    originAddress2 = shipment.Origin.Street2.EmptyStringToNull(),
                    originCity = shipment.Origin.City,
                    originCloseTime = DateTime.Now.SetTime(shipment.LatePickup).ToSmc3EvaTime(),
                    originCompanyName = null, 
                    originCountry = shipment.Origin.Country.Code,
                    originName = shipment.Origin.Description.EmptyStringToNull(),
                    originOpenTime = DateTime.Now.SetTime(shipment.EarlyPickup).ToSmc3EvaTime(),
                    originPhone = originContact.Phone.FormattedSmc3EvaPhone(),
                    originPostalCode = shipment.Origin.PostalCode,
                    originState = shipment.Origin.State,
                    paymentTerms = null,
                    pickupCallRequired = hasPickup ? CallRequiredValues.Yes : CallRequiredValues.No,
                    pickupCloseTime = hasPickup ? shipment.Origin.AppointmentDateTime.ToSmc3EvaTime() : DateTime.Now.SetTime(shipment.LatePickup).ToSmc3EvaTime(),
                    pickupContactEmail = shipment.User.Email,
                    pickupContactName = shipment.User.FullName,
                    pickupContactPhone = shipment.User.Phone.FormattedSmc3EvaPhone(),
                    pickupDate = hasPickup ? shipment.Origin.AppointmentDateTime.ToSmc3EvaDate() : shipment.DesiredPickupDate.ToSmc3EvaDate(),
                    pickupReadyTime = hasPickup ? shipment.Origin.AppointmentDateTime.ToSmc3EvaTime() : DateTime.Now.SetTime(shipment.EarlyPickup).ToSmc3EvaTime(),
                    pickupRequester = null, // not required so we will leave null for now as has multiple carrier variations
                    pickupSpecialNotes = shipment.Origin.SpecialInstructions.EmptyStringToNull(),
                    scac = primaryVendor.Scac,
                    serviceCode = null,
                    shipmentCommodity = shipment
                        .Items
                        .Select(i => new ShipmentCommodity
                            {
                                classification = i.ActualFreightClass.ToString(),
                                density = null,
                                densityUnit = null,
                                description = i.Description.EmptyStringToNull(),
                                dimensionUnits = Smc.Eva.Units.Dimension.Feet,
                                height = i.ActualHeight.InToFt().ToString("n4"),
                                itemNumber = null,
                                length = i.ActualLength.InToFt().ToString("n4"),
                                packagingType = "PT",  //TODO: need mapping functionality for this!!! i.PackageType.TypeName,
                                pieces = i.Quantity.GetString(),
                                subNumber = null,
                                weight = Math.Ceiling(i.ActualWeight).ToInt().GetString(),
                                weightUnit = Smc.Eva.Units.Weight.Pounds,
                                width = i.ActualWidth.InToFt().ToString("n4"),
                            })
                        .ToList(),
                    // TODO EVA DISPATCH: Investigate other codes. Not all vendors may support the same codes.
                    shipmentCommodityFreightCodes = shipment.HazardousMaterial
                                                        ? new List<ShipmentCommodityFreightCode>
							                                {
								                                new ShipmentCommodityFreightCode
									                                {
										                                code = "HAZ",
										                                details = new List<ShipmentCommodityFreightCodeDetail>
											                                {
												                                new ShipmentCommodityFreightCodeDetail
													                                {
														                                name = "Phone",
														                                value = shipment.HazardousMaterialContactPhone
													                                },
												                                new ShipmentCommodityFreightCodeDetail
													                                {
														                                name = "Name",
														                                value = shipment.HazardousMaterialContactName
													                                },
												                                new ShipmentCommodityFreightCodeDetail
													                                {
														                                name = "Mobile",
														                                value = shipment.HazardousMaterialContactMobile
													                                },
												                                new ShipmentCommodityFreightCodeDetail
													                                {
														                                name = "Email",
														                                value = shipment.HazardousMaterialContactEmail
													                                }
											                                }
									                                }
							                                }
                                                        : new List<ShipmentCommodityFreightCode>(),
                    shipmentID = shipment.ShipmentNumber,
                };
            return dispatchRequest;
        }

        public static TrackRequest GetTrackRequest(this VendorCommunication vcom, Shipment shipment, Vendor primaryVendor = null)
        {
            if (primaryVendor == null) primaryVendor = shipment.Vendors.FirstOrDefault(v => v.Primary).Vendor;

            var trackRequest = new TrackRequest
                {
                    accountToken = vcom.IsInSMC3Testing ? vcom.SMC3TestAccountToken : vcom.SMC3ProductionAccountToken,
                    eventTypes = new List<EventType> { new EventType { eventType = EventTypes.All } },
                    referenceNumbers = new List<Smc.Eva.ReferenceNumber>
						{
							new Smc.Eva.ReferenceNumber 
                            {
                                referenceNumber = shipment.ShipmentNumber,
                                originPostalCode = shipment.Origin.PostalCode,
                                destinationPostalCode = shipment.Destination.PostalCode,
                                expectedPickupDate = shipment.DesiredPickupDate.ToString("yyyyMMdd")
                            },
							new Smc.Eva.ReferenceNumber 
                            {
                                referenceNumber = shipment.Prefix.Code + shipment.ShipmentNumber,
                                originPostalCode = shipment.Origin.PostalCode,
                                destinationPostalCode = shipment.Destination.PostalCode,
                                expectedPickupDate = shipment.DesiredPickupDate.ToString("yyyyMMdd")
                            }
						},
                    referenceType = ReferenceTypes.BillOfLadingNumber,
                    scac = primaryVendor.Scac
                };
            return trackRequest;
        }


        // Expedited Rate extension methods
        public static decimal GetExpeditedRate(this Rate rate)
        {
            if (rate == null) return default(decimal);
            if (!rate.ExpeditedCharges.Any()) return default(decimal);

            var charge = rate.ExpeditedCharges.First();

            return charge.UnitBuy;
        }

        public static string GetExpeditedRateTime(this Rate rate)
        {
            if (rate == null) return string.Empty;
            if (!rate.ExpeditedCharges.Any()) return string.Empty;

            var charge = rate.ExpeditedCharges.First();

            return charge.ChargeCode.Project44Code.GetExpeditedServiceTime();
        }

        // Guaranteed Rate extension methods

        public static string GetGuaranteedRateTime(this Rate rate)
		{
			if (rate == null) return string.Empty;
			if (!rate.GuaranteedCharges.Any()) return string.Empty;

			var charge = rate.GuaranteedCharges.First();

			return charge.IsProject44GuaranteedCharge
				       ? charge.ChargeCode.Project44Code.GetGuaranteedServiceTime()
				       : charge.LTLGuaranteedCharge.Time;
		}

        public static decimal GetGuaranteedRate(this Rate rate)
        {
            if (rate == null) return default(decimal);
            if (!rate.GuaranteedCharges.Any()) return default(decimal);

            var charge = rate.GuaranteedCharges.First();

            return charge.IsProject44GuaranteedCharge
                ? charge.UnitBuy // No markups on UnitBuy, so that is the original P44 Guaranteed rate 
                : charge.LTLGuaranteedCharge.Rate;
        }

        public static RateType GetGuaranteedRateType(this Rate rate)
        {
            if (rate == null) return RateType.Flat;
            if (!rate.GuaranteedCharges.Any()) return RateType.Flat;

            var charge = rate.GuaranteedCharges.First();

            return charge.IsProject44GuaranteedCharge
                ? RateType.Flat
                : charge.LTLGuaranteedCharge.RateType;
        }

        public static string GetCriticalBolComments(this Rate rate, DateTime estimatedDeliveryDate)
        {
            if (rate == null) return string.Empty;
            if (!rate.GuaranteedCharges.Any()) return string.Empty;

            var charge = rate.GuaranteedCharges.First();

            return charge.IsProject44GuaranteedCharge
                ? string.Format(WebApplicationConstants.GuaranteedServiceCriticalBolComments, // Project 44 Guaranteed Service
                    charge.ChargeCode.Project44Code.GetGuaranteedServiceTime(), estimatedDeliveryDate.FormattedShortDate())
                : string.IsNullOrEmpty(charge.LTLGuaranteedCharge.CriticalNotes) // eShip Guaranteed Service
                    ? string.Format(WebApplicationConstants.GuaranteedServiceCriticalBolComments, charge.LTLGuaranteedCharge.Time, estimatedDeliveryDate.FormattedShortDate())
                    : charge.LTLGuaranteedCharge.CriticalNotes.Replace(WebApplicationConstants.DeliveryDateTag, estimatedDeliveryDate.FormattedShortDate());
        }
    }
}
