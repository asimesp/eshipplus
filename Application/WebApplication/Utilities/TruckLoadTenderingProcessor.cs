﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Web;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Extern.Edi;
using LogisticsPlus.Eship.Extern.Edi.Segments;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches.Connect;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Extern;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.WebApplication.Utilities.ImmitationViews;

namespace LogisticsPlus.Eship.WebApplication.Utilities
{
	public class TruckLoadTenderingProcessor
	{
		private readonly HttpServerUtility _server;
		private bool _shutdownThread;
		private Thread _processorThread;

		public bool IsRunning
		{
			get { return _processorThread != null; }
		}

		public string ErrorsFolder { get; set; }

		public TruckLoadTenderingProcessor(HttpServerUtility server)
		{
			ErrorsFolder = string.Empty;
			_server = server;
		}

		public TruckLoadTenderingProcessor(string errorsFolder, HttpServerUtility server)
		{
			ErrorsFolder = errorsFolder;
			_server = server;
		}

		~TruckLoadTenderingProcessor()
		{
			Shutdown();
		}

		public void Start()
		{
			try
			{
				if (_processorThread == null)
				{
					// start the scheduling thread
					_processorThread = new Thread(PrcessorThread);
					_processorThread.Start();
				}
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, ErrorsFolder);
			}
		}

		public void Shutdown()
		{
			if (_processorThread != null)
			{
				_shutdownThread = true;

				// wait for the scheduler thread to shutdown for up to 60sec
				// if the thread does not shutdown on time, try to kill it
				if (!_processorThread.Join(60000))
				{
					try
					{
						_processorThread.Abort();
					}
					catch (Exception e)
					{
						ErrorLogger.LogError(e, ErrorsFolder);
					}
				}

				_processorThread = null;
			}
		}

		private void PrcessorThread()
		{
			// delay before starting the thread
			Thread.Sleep(250);

			// init some state variables
			var counter = 4;

			if (_shutdownThread) return;

			do
			{
				// each actual thread iteration occurs in 1 second intervals
				// however to make the tread more responsive it will sleep 4 x 250ms
				if (counter != 0)
				{
					Thread.Sleep(250);
					counter--;
					continue;
				}

				// reset the counter
				counter = 4;
				try
				{
					ProcessTruckLoadBids();
				}
				catch (Exception ex)
				{
					Debug.WriteLine("eShipPlus:TruckloadTenderingProcessor : " + ex.Message);
					ErrorLogger.LogError(ex, ErrorsFolder);
				}
			} while (!_shutdownThread);
		}

		private void ProcessTruckLoadBids()
		{
			var columns = new List<ParameterColumn>();
			var tenants = new TenantSearch().FetchTenants(columns);
			var eligibleBids = new List<TruckloadBid>();
			foreach (var tenant in tenants)
			{
				eligibleBids.AddRange(new TruckloadBidSearch().FetchTruckloadBidByStatus(TruckloadBidStatus.Processing.ToInt(),
				                                                                         tenant.Id));
			}

			foreach (var bid in eligibleBids)
			{
				var view = new TruckloadTenderingPrcessorImmitationView(bid.Tenant.DefaultSystemUser, _server);
				var matched = false;
				var hasNoMatch = false;
				Edi204 edi204;

				var edi204DocFirstVendor =
					new XmlConnectSearch().FetchOutBoundLoadTenderByVendShipIdNumAndDocType(bid.FirstPreferredVendor.VendorNumber,
					                                                                        bid.LoadOrder.LoadOrderNumber,
					                                                                        bid.Tenant.Id,
					                                                                        bid.Tenant.DefaultSystemUserId);


				var controlNumber =
					new AutoNumberProcessor().NextNumber(AutoNumberCode.EdiTransactionSetControlNumber, bid.Tenant.DefaultSystemUser)
					                         .GetString();
				
				var loadOrderVendor = bid.LoadOrder.Vendors.FirstOrDefault(p => p.Primary);
				if (loadOrderVendor == null) continue;

				//Check if edi204 document was sent when communicatio type is edi
				if (edi204DocFirstVendor == null && bid.FirstPreferredVendor.CanSendLoadTender(bid.LoadOrder.Status))
					hasNoMatch = true;

				//Lock LoadOrder and truckloadBid
				view.LockLoadOrder(bid.LoadOrder);
				view.LockTruckloadBid(bid);


				//Check for edi204 sent for the first vendor
				if (loadOrderVendor.VendorId == bid.FirstPreferredVendorId)
				{
					var edi990DocFirstVendor =
						new XmlConnectSearch().FetchOutboundLoadTenderResponseByShipmentIdNumber(bid.LoadOrder.LoadOrderNumber,
						                                                                         bid.Tenant.Id,
						                                                                         bid.Tenant.DefaultSystemUserId,
						                                                                         bid.FirstPreferredVendor.VendorNumber);
					var edi990ExistsAndRejected = edi990DocFirstVendor != null &&
					                              edi990DocFirstVendor.Status == XmlConnectStatus.Rejected;

					//if edi2014 found check if it is expired or response is rejected
					if (DateTime.Now > bid.ExpirationTime1 || edi990ExistsAndRejected)
					{
						if (bid.SecondPreferredVendor != null)
						{
							var vendors = bid.LoadOrder.Vendors;
							bid.LoadOrder.LoadCollections();
							vendors.Remove(loadOrderVendor);
							vendors.Add(new LoadOrderVendor
								{
									VendorId = bid.SecondPreferredVendorId,
									LoadOrder = bid.LoadOrder,
									TenantId = bid.LoadOrder.TenantId,
									ProNumber = string.Empty,
									Primary = true,
								});

							bid.LoadOrder.Vendors = vendors;
							view.UpdateLoadOrder(bid.LoadOrder);

							bid.TimeLoadTendered2 = DateTime.Now;
							bid.ExpirationTime2 = DateTime.Now.AddMinutes(bid.TruckloadTenderingProfile.MaxWaitTime);

							edi204 = bid.LoadOrder.ToEdi204(controlNumber, bid.ExpirationTime2.ToString());

							view.SaveOrUpdateBid(bid, bid.LoadOrder);
							if (bid.SecondPreferredVendor.CanSendLoadTender(bid.LoadOrder.Status))
							{
								view.SendLoadTender(edi204, bid.SecondPreferredVendor.Communication, bid.ExpirationTime2.ToString());
							}
							else
							{
								view.Server.NotifyLoadTenderByEmail(edi204.Loads[0], bid.LoadOrder.Customer, bid.Tenant.DefaultSystemUser,
								                                    bid.SecondPreferredVendor, SetPurposeTransType.OR.ToString());
							}
						}
						else if (bid.ThirdPreferredVendor == null)
							hasNoMatch = true;
					}
					else if (edi990DocFirstVendor != null && edi990DocFirstVendor.Status == XmlConnectStatus.Accepted)
						matched = true;
				}
				else if (bid.SecondPreferredVendor != null)
				{
					if (loadOrderVendor.VendorId == bid.SecondPreferredVendorId)
					{
						var edi990DocSecondVendor =
							new XmlConnectSearch().FetchOutboundLoadTenderResponseByShipmentIdNumber(bid.LoadOrder.LoadOrderNumber,
							                                                                         bid.Tenant.Id,
							                                                                         bid.Tenant.DefaultSystemUserId,
							                                                                         bid.SecondPreferredVendor.VendorNumber);
						var edi990ExistsAndRejected = edi990DocSecondVendor != null &&
						                              edi990DocSecondVendor.Status == XmlConnectStatus.Rejected;
						if (DateTime.Now > bid.ExpirationTime2 || edi990ExistsAndRejected)
						{
							if (bid.ThirdPreferredVendor != null)
							{
								var vendors = bid.LoadOrder.Vendors;

								bid.LoadOrder.LoadCollections();
								vendors.Remove(loadOrderVendor);
								vendors.Add(new LoadOrderVendor
									{
										VendorId = bid.ThirdPreferredVendorId,
										LoadOrder = bid.LoadOrder,
										TenantId = bid.LoadOrder.TenantId,
										ProNumber = string.Empty,
										Primary = true,
									});

								bid.LoadOrder.Vendors = vendors;
								view.UpdateLoadOrder(bid.LoadOrder);

								bid.TimeLoadTendered3 = DateTime.Now;
								bid.ExpirationTime3 = DateTime.Now.AddMinutes(bid.TruckloadTenderingProfile.MaxWaitTime);

								edi204 = bid.LoadOrder.ToEdi204(controlNumber, bid.ExpirationTime3.ToString());

								view.SaveOrUpdateBid(bid, bid.LoadOrder);
								if (bid.ThirdPreferredVendor.CanSendLoadTender(bid.LoadOrder.Status))
								{
									view.SendLoadTender(edi204, bid.ThirdPreferredVendor.Communication, bid.ExpirationTime3.ToString());
								}
								else
								{
									view.Server.NotifyLoadTenderByEmail(edi204.Loads[0], bid.LoadOrder.Customer, bid.Tenant.DefaultSystemUser,
									                                    bid.ThirdPreferredVendor, SetPurposeTransType.OR.ToString());
								}
							}
							else
								hasNoMatch = true;
						}
						else if (edi990DocSecondVendor != null && edi990DocSecondVendor.Status == XmlConnectStatus.Accepted)
							matched = true;
					}
					else if (bid.ThirdPreferredVendor != null)
					{
						if (loadOrderVendor.VendorId == bid.ThirdPreferredVendorId)
						{
							var edi990DocThirdVendor =
								new XmlConnectSearch().FetchOutboundLoadTenderResponseByShipmentIdNumber(bid.LoadOrder.LoadOrderNumber,
								                                                                         bid.Tenant.Id,
								                                                                         bid.Tenant.DefaultSystemUserId,
								                                                                         bid.ThirdPreferredVendor.VendorNumber);
							var edi990ExistsAndRejected = edi990DocThirdVendor != null &&
							                              edi990DocThirdVendor.Status == XmlConnectStatus.Rejected;
							if (DateTime.Now > bid.ExpirationTime3 || edi990ExistsAndRejected)
							{
								hasNoMatch = true;
							}
							else if (edi990DocThirdVendor != null && edi990DocThirdVendor.Status == XmlConnectStatus.Accepted)
								matched = true;
						}
					}
				}


			
				if (hasNoMatch)
				{
					bid.BidStatus = TruckloadBidStatus.NoMatch;
					view.SaveOrUpdateBid(bid, bid.LoadOrder);

					var vendors = bid.LoadOrder.Vendors;
					bid.LoadOrder.LoadCollections();
					vendors.Remove(loadOrderVendor);
					bid.LoadOrder.Vendors = vendors;
					view.UpdateLoadOrder(bid.LoadOrder);
				}

				//if vendor accepted the load order through edi communication.(NOTE: Acceptance through email communication is handled in LoadTenderResponseView)
				if (matched)
				{
					bid.BidStatus = TruckloadBidStatus.Covered;
					view.SaveOrUpdateBid(bid, bid.LoadOrder);
				}

				//UnLock LoadOrder and truckloadBid
				view.UnlockLoadOrder(bid.LoadOrder);
				view.UnlockTruckloadBid(bid);
			}
		}

	}
}