﻿using System;
using System.Collections.Generic;
using System.Web;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.MacroPoint;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Cache;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Project44;
using LogisticsPlus.Eship.Smc;
using LogisticsPlus.UpsPlugin;
using LogisticssPlus.FedExPlugin.Services;
using NLog;

namespace LogisticsPlus.Eship.WebApplication.Utilities
{
	public static class EnvironmentUtilities
	{
		public static Logger PageAccessLogger;

		public static void LoadTenantProcessingSetup(this HttpServerUtility server)
		{
			var tenants = new TenantSearch().FetchTenants(new List<ParameterColumn>());
			var search = new SettingSearch();
			foreach (var tenant in tenants)
			{
				// Registry Cache
				ProcessorVars.RegistryCache[tenant.Id] = new CachedCollectionItem(tenant.Id);

				// record lock limit
				var setting = search.FetchSettingByCode(SettingCode.RecordLockLimit, tenant.Id);
				if (!setting.IsNew && !ProcessorVars.RecordLockLimit.ContainsKey(tenant.Id))
					ProcessorVars.RecordLockLimit.Add(tenant.Id, setting.Value.ToInt());
				else if (!ProcessorVars.RecordLockLimit.ContainsKey(tenant.Id))
					ProcessorVars.RecordLockLimit.Add(tenant.Id, ProcessorVars.DefaultRecordLockLimit);

				// monitor emails
				setting = search.FetchSettingByCode(SettingCode.MonitorEmail, tenant.Id);
				if (!setting.IsNew && !ProcessorVars.MonitorEmails.ContainsKey(tenant.Id))
					ProcessorVars.MonitorEmails
						.Add(tenant.Id, setting.Value.Split(WebApplicationUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries));
				else if (!ProcessorVars.MonitorEmails.ContainsKey(tenant.Id))
					ProcessorVars.MonitorEmails.Add(tenant.Id, new string[0]);

				// advanced customer ltl schedule limit
				setting = search.FetchSettingByCode(SettingCode.AdvancedCustomerLTLScheduleLimit, tenant.Id);
				if (!setting.IsNew && !ProcessorVars.AdvancedCustomerLTLScheduleLimit.ContainsKey(tenant.Id))
					ProcessorVars.AdvancedCustomerLTLScheduleLimit.Add(tenant.Id, setting.Value.ToInt());
				else if (!ProcessorVars.AdvancedCustomerLTLScheduleLimit.ContainsKey(tenant.Id))
					ProcessorVars.AdvancedCustomerLTLScheduleLimit.Add(tenant.Id, ProcessorVars.DefaultAdvancedCustomerLTLScheduleLimit);

				// Vendor Insurance Alert Threshold Days
				setting = search.FetchSettingByCode(SettingCode.VendorInsuranceAlertThresholdDays, tenant.Id);
				if (!setting.IsNew && !ProcessorVars.VendorInsuranceAlertThresholdDays.ContainsKey(tenant.Id))
					ProcessorVars.VendorInsuranceAlertThresholdDays.Add(tenant.Id, setting.Value.ToInt());
				else if (!ProcessorVars.VendorInsuranceAlertThresholdDays.ContainsKey(tenant.Id))
					ProcessorVars.VendorInsuranceAlertThresholdDays.Add(tenant.Id, ProcessorVars.DefaultVendorInsuranceAlertThresholdDays);

				// Customer Credit Alert Threshold
				setting = search.FetchSettingByCode(SettingCode.CustomerCreditAlertThreshold, tenant.Id);
				if (!setting.IsNew && !ProcessorVars.CustomerCreditAlertThreshold.ContainsKey(tenant.Id))
					ProcessorVars.CustomerCreditAlertThreshold.Add(tenant.Id, setting.Value.ToInt());
				else if (!ProcessorVars.CustomerCreditAlertThreshold.ContainsKey(tenant.Id))
					ProcessorVars.CustomerCreditAlertThreshold.Add(tenant.Id, ProcessorVars.DefaultCustomerCreditAlertThreshold);

				//Developer Access Request Support Emails
				setting = search.FetchSettingByCode(SettingCode.DeveloperAccessRequestSupportEmail, tenant.Id);
				if (!setting.IsNew && !ProcessorVars.DeveloperAccessRequestSupportEmails.ContainsKey(tenant.Id))
					ProcessorVars.DeveloperAccessRequestSupportEmails
						.Add(tenant.Id, setting.Value.Split(WebApplicationUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries));
				else if (!ProcessorVars.DeveloperAccessRequestSupportEmails.ContainsKey(tenant.Id))
					ProcessorVars.DeveloperAccessRequestSupportEmails.Add(tenant.Id, new string[0]);

				//tier Support Emails
				setting = search.FetchSettingByCode(SettingCode.SpecialistSupportEmail, tenant.Id);
				if (!setting.IsNew && !ProcessorVars.SpecialistSupportEmails.ContainsKey(tenant.Id))
					ProcessorVars.SpecialistSupportEmails
						.Add(tenant.Id, setting.Value.Split(WebApplicationUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries));
				else if (!ProcessorVars.SpecialistSupportEmails.ContainsKey(tenant.Id))
					ProcessorVars.SpecialistSupportEmails.Add(tenant.Id, new string[0]);

				//tenant admin notification Emails
				setting = search.FetchSettingByCode(SettingCode.TenantAdminNotificationEmail, tenant.Id);
				if (!setting.IsNew && !ProcessorVars.TenantAdminNotificationEmails.ContainsKey(tenant.Id))
					ProcessorVars.TenantAdminNotificationEmails
						.Add(tenant.Id, setting.Value.Split(WebApplicationUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries));
				else if (!ProcessorVars.TenantAdminNotificationEmails.ContainsKey(tenant.Id))
					ProcessorVars.TenantAdminNotificationEmails.Add(tenant.Id, new string[0]);

				//Batch Rating Record Delete limit
				setting = search.FetchSettingByCode(SettingCode.DeleteCompletedRateAnalysisRecordAfterDays, tenant.Id);
				if (!setting.IsNew && !ProcessorVars.DeleteCompletedRateAnalysisRecordAfterDays.ContainsKey(tenant.Id))
					ProcessorVars.DeleteCompletedRateAnalysisRecordAfterDays.Add(tenant.Id, setting.Value.ToInt());
				else if (!ProcessorVars.DeleteCompletedRateAnalysisRecordAfterDays.ContainsKey(tenant.Id))
					ProcessorVars.DeleteCompletedRateAnalysisRecordAfterDays.Add(tenant.Id, ProcessorVars.DefaultDeleteCompletedRateAnalysisRecordsAfterDays);

				server.UpdateIntegrationVariables(tenant);
				tenant.UpdateTenantAnnouncements();
			}
		}

		public static void UpdateIntegrationVariables(this HttpServerUtility server, Tenant tenant)
		{
			// integration variables
			if (!ProcessorVars.IntegrationVariables.ContainsKey(tenant.Id))
				ProcessorVars.IntegrationVariables.Add(tenant.Id, new IntegrationVariables());

			if (tenant.SMCCarrierConnectEnabled && !string.IsNullOrEmpty(tenant.SMCCarrierConnectParameterFile))
			{
				ProcessorVars.IntegrationVariables[tenant.Id].CarrierConnectParameters =
					server.ReadFromFile(tenant.SMCCarrierConnectParameterFile).FromUtf8Bytes().FromXml<SmcServiceSettings>();
			}

			if (tenant.SMCRateWareEnabled && !string.IsNullOrEmpty(tenant.SMCRateWareParameterFile))
			{
				ProcessorVars.IntegrationVariables[tenant.Id].RatewareParameters =
					server.ReadFromFile(tenant.SMCRateWareParameterFile).FromUtf8Bytes().FromXml<SmcServiceSettings>();
			}

			if (tenant.FedExSmallPackEnabled && !string.IsNullOrEmpty(tenant.FedExSmallPackParameterFile))
			{
				ProcessorVars.IntegrationVariables[tenant.Id].FedExParameters =
					server.ReadFromFile(tenant.FedExSmallPackParameterFile).FromUtf8Bytes().FromXml<ServiceParams>();
			}

			if (tenant.UpsSmallPackEnabled && !string.IsNullOrEmpty(tenant.UpsSmallPackParameterFile))
			{
				ProcessorVars.IntegrationVariables[tenant.Id].UpsParameters =
					server.ReadFromFile(tenant.UpsSmallPackParameterFile).FromUtf8Bytes().FromXml<UpsServiceParams>();
			}

			if (tenant.MacroPointEnabled && !string.IsNullOrEmpty(tenant.MacroPointParameterFile))
			{
				ProcessorVars.IntegrationVariables[tenant.Id].MacroPointParams =
					server.ReadFromFile(tenant.MacroPointParameterFile).FromUtf8Bytes().FromXml<MacroPointSettings>();
			}

		    ProcessorVars.IntegrationVariables[tenant.Id].Project44Settings = new Project44ServiceSettings
		    {
		        Hostname = WebApplicationSettings.Project44Host,
		        Username = WebApplicationSettings.Project44Username,
		        Password = WebApplicationSettings.Project44Password,
                PrimaryLocationId = WebApplicationSettings.Project44PrimaryLocationId
		    };
		}

		public static void UpdateTenantAnnouncements(this Tenant tenant)
		{
			if (!ProcessorVars.Announcements.ContainsKey(tenant.Id))
				ProcessorVars.Announcements.Add(tenant.Id, new List<Announcement>());
			ProcessorVars.Announcements[tenant.Id] =
				new AnnouncementSearch().FetchEnabledAnnouncements(tenant.Id);
		}

		public static void UpdateApplicableSetting(this Setting setting)
		{
			switch (setting.Code)
			{
				case SettingCode.RecordLockLimit:
					setting.UpdateTenantLockLimit();
					break;
				case SettingCode.AdvancedCustomerLTLScheduleLimit:
					setting.UpdateAdvancedCustomerLTLScheduleLimit();
					break;
				case SettingCode.VendorInsuranceAlertThresholdDays:
					setting.UpdateVendorInsuranceAlertThresholdDays();
					break;
				case SettingCode.CustomerCreditAlertThreshold:
					setting.UpdateCustomerCreditAlertThreshold();
					break;
				case SettingCode.MonitorEmail:
					setting.UpdateTenantMonitorEmails();
					break;
				case SettingCode.SpecialistSupportEmail:
					setting.UpdateTenantSpecialistSupportEmails();
					break;
				case SettingCode.TenantAdminNotificationEmail:
					setting.UpdateTenantAdminNotificationEmails();
					break;
				case SettingCode.DeleteCompletedRateAnalysisRecordAfterDays:
					setting.UpdateDeleteCompletedRateAnalysisRecordsAfterDays();
					break;
			}
		}

		private static void UpdateTenantLockLimit(this Setting setting)
		{
			if (!ProcessorVars.RecordLockLimit.ContainsKey(setting.TenantId))
				ProcessorVars.RecordLockLimit.Add(setting.TenantId, ProcessorVars.DefaultRecordLockLimit);
			ProcessorVars.RecordLockLimit[setting.TenantId] = setting.Value.ToInt();
		}

		private static void UpdateTenantMonitorEmails(this Setting setting)
		{
			if (!ProcessorVars.MonitorEmails.ContainsKey(setting.TenantId))
				ProcessorVars.MonitorEmails.Add(setting.TenantId, new string[0]);
			ProcessorVars.MonitorEmails[setting.TenantId] =
				setting.Value.Split(WebApplicationUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries);
		}

		private static void UpdateAdvancedCustomerLTLScheduleLimit(this Setting setting)
		{
			if (!ProcessorVars.AdvancedCustomerLTLScheduleLimit.ContainsKey(setting.TenantId))
				ProcessorVars.AdvancedCustomerLTLScheduleLimit.Add(setting.TenantId, ProcessorVars.DefaultAdvancedCustomerLTLScheduleLimit);
			ProcessorVars.AdvancedCustomerLTLScheduleLimit[setting.TenantId] = setting.Value.ToInt();
		}

		private static void UpdateVendorInsuranceAlertThresholdDays(this Setting setting)
		{
			if (!ProcessorVars.VendorInsuranceAlertThresholdDays.ContainsKey(setting.TenantId))
				ProcessorVars.VendorInsuranceAlertThresholdDays.Add(setting.TenantId, ProcessorVars.DefaultVendorInsuranceAlertThresholdDays);
			ProcessorVars.VendorInsuranceAlertThresholdDays[setting.TenantId] = setting.Value.ToInt();
		}

		private static void UpdateCustomerCreditAlertThreshold(this Setting setting)
		{
			if (!ProcessorVars.CustomerCreditAlertThreshold.ContainsKey(setting.TenantId))
				ProcessorVars.CustomerCreditAlertThreshold.Add(setting.TenantId, ProcessorVars.DefaultCustomerCreditAlertThreshold);
			ProcessorVars.CustomerCreditAlertThreshold[setting.TenantId] = setting.Value.ToDecimal();
		}

		private static void UpdateTenantSpecialistSupportEmails(this Setting setting)
		{
			if (!ProcessorVars.SpecialistSupportEmails.ContainsKey(setting.TenantId))
				ProcessorVars.SpecialistSupportEmails.Add(setting.TenantId, new string[0]);
			ProcessorVars.SpecialistSupportEmails[setting.TenantId] =
				setting.Value.Split(WebApplicationUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries);
		}

		private static void UpdateTenantAdminNotificationEmails(this Setting setting)
		{
			if (!ProcessorVars.TenantAdminNotificationEmails.ContainsKey(setting.TenantId))
				ProcessorVars.TenantAdminNotificationEmails.Add(setting.TenantId, new string[0]);
			ProcessorVars.TenantAdminNotificationEmails[setting.TenantId] =
				setting.Value.Split(WebApplicationUtilities.Seperators(), StringSplitOptions.RemoveEmptyEntries);
		}

		private static void UpdateDeleteCompletedRateAnalysisRecordsAfterDays(this Setting setting)
		{
			if (!ProcessorVars.DeleteCompletedRateAnalysisRecordAfterDays.ContainsKey(setting.TenantId))
				ProcessorVars.DeleteCompletedRateAnalysisRecordAfterDays
					.Add(setting.TenantId, ProcessorVars.DefaultDeleteCompletedRateAnalysisRecordsAfterDays);
			ProcessorVars.DeleteCompletedRateAnalysisRecordAfterDays[setting.TenantId] = setting.Value.ToInt();
		}

	}
}
