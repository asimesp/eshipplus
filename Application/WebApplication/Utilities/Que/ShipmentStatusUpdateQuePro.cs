﻿using System;

namespace LogisticsPlus.Eship.WebApplication.Utilities.Que
{
	[Serializable]
	public class ShipmentStatusUpdateQuePro
	{
		public string Scac { get; set; }
		public string Pro { get; set; }

		public ShipmentStatusUpdateQuePro()
		{
		}

		public ShipmentStatusUpdateQuePro(string scac, string pro)
		{
			Scac = scac;
			Pro = pro;
		}
	}
}