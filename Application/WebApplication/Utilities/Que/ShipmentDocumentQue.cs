﻿using System;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.WebApplication.Utilities.Que
{
    [Serializable]
    public class ShipmentDocumentQue
    {
        public long Id { get; set; }
        public long ShipmentId { get; set; }
        public long TenantId { get; set; }
        public bool IsInternal { get; set; }
        public bool FtpDelivered { get; set; }
        public string LocationPath { get; set; }
        public long DocumentTagId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }


        public ShipmentDocumentQue()
        {
        }

        public ShipmentDocumentQue(ShipmentDocument document)
        {
            if (document == null)
                throw new ArgumentNullException("document");

            Id = document.Id;
            ShipmentId = document.ShipmentId;
            TenantId = document.TenantId;
            IsInternal = document.IsInternal;
            FtpDelivered = document.FtpDelivered;
            LocationPath = document.LocationPath;
            DocumentTagId = document.DocumentTagId;
            Name = document.Name;
            Description = document.Description;
        }

        public ShipmentDocument RetrieveShipmentDocument()
        {
            return new ShipmentDocument(Id, Id != default(long))
            {
                ShipmentId = ShipmentId,
                TenantId = TenantId,
                IsInternal = IsInternal,
                FtpDelivered = FtpDelivered,
                LocationPath = LocationPath,
                DocumentTagId = DocumentTagId,
                Name = Name,
                Description = Description,
            };
        }
    }
}