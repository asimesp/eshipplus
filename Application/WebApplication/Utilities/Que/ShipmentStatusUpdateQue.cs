﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;

namespace LogisticsPlus.Eship.WebApplication.Utilities.Que
{
	[Serializable]
	public class ShipmentStatusUpdateQue
	{
		public long Id { get; set; }
		public long ShipmentId { get; set; }
		public long UserId { get; set; }
		public DateTime CallDate { get; set; }
		public DateTime EventDate { get; set; }
		public string CallNotes { get; set; }
		public string EdiStatusCode { get; set; }
		public long TenantId { get; set; }
		public List<ShipmentStatusUpdateQuePro> Pros { get; set; }

		public ShipmentStatusUpdateQue()
		{
		}

		public ShipmentStatusUpdateQue(CheckCall checkCall, List<ShipmentStatusUpdateQuePro> pros = null)
		{
			if(checkCall == null)
				throw new ArgumentNullException("checkCall");

			Id = checkCall.Id;
			ShipmentId = checkCall.ShipmentId;
			UserId = checkCall.User == null ? default(long) : checkCall.User.Id;
			CallDate = checkCall.CallDate;
			EventDate = checkCall.EventDate;
			CallNotes = checkCall.CallNotes;
			EdiStatusCode = checkCall.EdiStatusCode;
			TenantId = checkCall.TenantId;
			Pros = pros;
		}

		public CheckCall RetrieveCheckCall()
		{
			return new CheckCall(Id, Id != default(long))
				{
					ShipmentId = ShipmentId,
					User = new User(UserId),
					CallDate = CallDate,
					EventDate = EventDate,
					CallNotes = CallNotes,
					EdiStatusCode = EdiStatusCode,
					TenantId = TenantId
				};
		}
	}
}