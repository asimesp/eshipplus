﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.WebApplication.Utilities.Que
{
    public static class ReportQueue
    {
        private static readonly object LockObject = new object();
        private static List<ReportSchedule> _listOfQueuedSchedules;


        public static void Add(List<ReportSchedule> queSchedule)
        {
            lock (LockObject)
            {
                _listOfQueuedSchedules.AddRange(queSchedule.Where(s => !_listOfQueuedSchedules.Contains(s)).ToList()); 
            }
        }

        public static void AppendToListAndClear(List<ReportSchedule> schedulesToBeRun)
        {
            lock (LockObject)
            {
                var schedulesToAdd = _listOfQueuedSchedules.Where(i => schedulesToBeRun.All(x => x.Id != i.Id));
                schedulesToBeRun.AddRange(schedulesToAdd);
                _listOfQueuedSchedules.Clear();
            }
        }

        public static void Initialize()
        {
            _listOfQueuedSchedules = new List<ReportSchedule>();
        }

    }
}