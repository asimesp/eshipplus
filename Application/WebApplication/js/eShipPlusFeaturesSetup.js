﻿$(function () {

    // setup draggability
    setupDraggable('popup');
    setupDraggable('popover');
    setupDraggable('popupControl');
    setupDraggable('popupControlOver');
    setupDraggable('popupAnnouncement');

    function setupDraggable(classSelector) {
        $('div[class~="' + classSelector + '"]').draggable({ disabled: true });

        $('div[class~="' + classSelector + '"] div[class~="popheader"]').on('mouseenter', function () {
            $('div[class~="' + classSelector + '"]').draggable("option", "disabled", false);
        });

        $('div[class~="' + classSelector + '"] div[class~="popheader"]').on('mouseover', function () {
            $(this).css('cursor', 'move');
        });

        $('div[class~="' + classSelector + '"] div[class~="popheader"] .close').on('mouseover', function () {
            $(this).css('cursor', 'pointer');
        });


        $('div[class~="' + classSelector + '"] div[class~="popheader"]').on('mouseleave', function () {
            $('div[class~="' + classSelector + '"]').draggable("option", "disabled", true);
        });
    }

    // finder parameter folding
    if ($('div[finderPanel] div[class="finderScroll"] > table  tr').length > 1) {

        // if multiple selection enable (select buttons will not show in this case)
        if ($('div[finderPanel] div[class="finderScroll"] table tr td > input[type="submit"][value="Select"]').length == 0) {
            $('div[finderPanel] div[class="finderScroll"]').addClass('finderScrollFixed');
            $('div[finderPanel] div[class="finderScroll finderScrollFixed"]').removeClass('finderScroll');
            $('div[finderPanel]').attr('finderPanel', 'false');
        } else {
            $('div[finderPanel] div[class="finderScrollFixed"]').addClass('finderScroll');
            $('div[finderPanel] div[class="finderScrollFixed finderScroll"]').removeClass('finderScrollFixed');
            $('div[finderPanel]').attr('finderPanel', 'true');
        }

        // we have results, then fold parameters
        $('div[finderPanel="true"] > table[class="poptable"] div[onkeypress]').addClass('semi-fold-parameters-panel');
        $('div[finderPanel="true"] div[class="finderScroll"]').addClass('top_shadow');

        $('div[finderPanel="true"] > table[class="poptable"] div[onkeypress]').on('mouseenter', function () {
            // mouse enter remove fold
            $('div[finderPanel="true"] > table[class="poptable"] div[onkeypress]').removeClass('semi-fold-parameters-panel');
            $('div[finderPanel="true"] div[class="finderScroll top_shadow"]').removeClass('top_shadow');

            // find freezeheader if present then set top accordingly
            var top = $('div[finderPanel="true"] div[class="finderScroll"]').offset().top - $('div[finderPanel="true"]').offset().top - 2;
            $('div[finderPanel="true"] div[class="finderScroll"] > div[id^="hd"]').css('top', top + 'px');
        });
    } else {
        // no results, expand to full height

        $('div[finderPanel] > table[class="poptable"] div[onkeypress]').removeClass('semi-fold-parameters-panel');
        $('div[finderPanel] div[class="finderScroll top_shadow"]').removeClass('top_shadow');
    }

    // input filter
    $('input:text[numbersonly]').on("input", function () {
        var start = this.selectionStart,
            end = this.selectionEnd;
        $(this).val($(this).val().replace(/[^0-9]/g, ''));
        this.setSelectionRange(start, end);
    });

    $('input:text[floatingpointnumbersonly]').on("input", function () {
        var start = this.selectionStart,
            end = this.selectionEnd;
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        this.setSelectionRange(start, end);
    });

    $('input:text[negativenumbersonly]').on("input", function () {
        var start = this.selectionStart,
            end = this.selectionEnd;
        $(this).val($(this).val().replace(/[^0-9\-]/g, ''));
        this.setSelectionRange(start, end);
    });



    $('input:text[eShipTimeInput]').each(function () {
        $(this).on("input", function () {
            var start = this.selectionStart,
            end = this.selectionEnd;
            $(this).val($(this).val().replace(/[^0-9:]/g, ''));
            this.setSelectionRange(start, end);
            if ($(this).val().length > 5)
                $(this).val($(this).val().substring(0, 5));

            if ($(this).val().length == 2 && $(this).val().indexOf(":") == -1)
                $(this).val($(this).val() + ':');
            else if ($(this).val().length == 2 && $(this).val().indexOf(":") != -1)
                $(this).val($(this).val().replace(/[:]/g, ''));
        });
    });

    $('input:text[eShipTimeInput]').on("change", function () {
        var ids = new ControlIds();
        ids.TimeString = $(this).attr('id');
        ValidateTimeString(ids);
    });


    $('input:text[eShipDateInput]').each(function () {
        $(this).datepicker({
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            minDate: $(this).attr('eShipMinDate'),
            maxDate: $(this).attr('eShipMaxDate'),
        });

        $(this).keypress(function (e) {
            // fix for Chrome because in Chrome e.key is always null
            var character = e.key == null ? String.fromCharCode(e.which).toLowerCase() : e.key;
            if (e.which == 187 && e.shiftKey && e.key == null) character = '+';
            if (e.which == 189 && e.key == null) character = '-';

            var idx = "today+-1234567890m".indexOf(character);
            if (idx == -1) return;
            idx = $(this).val().indexOf(character);
            if (idx != -1) return;
            idx = "1234567890".indexOf(character);
            if (idx != -1) return;
            $(this).val($(this).val() + character);
        });
    });

    $('input:text[eShipDateInput]').change(function () {
        var ids = new ControlIds();
        ids.DateString = $(this).attr('id');
        ValidateDateString(ids);
    });

    $('input:text[eShipReadOnly]').each(function() {
        $(this).attr('readonly', 'readonly');
    });


    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
        ShowProgressNotice(false);

        $("div[id$='chkControlDiv'] input[type='checkbox']").each(function () {
            SetAltUniformCheckBoxClickedStatus(this);
        });
        $("div[id$='chkControlDiv'] input[type='radio']").each(function () {
            SetAltUniformCheckBoxClickedStatus(this);
        });


        // finder parameter folding
        if ($('div[finderPanel] div[class="finderScroll"] > table  tr').length > 1) {

            // if multiple selection enable (select buttons will not show in this case)
            if ($('div[finderPanel] div[class="finderScroll"] table tr td > input[type="submit"][value="Select"]').length == 0) {
                $('div[finderPanel] div[class="finderScroll"]').addClass('finderScrollFixed');
                $('div[finderPanel] div[class="finderScroll finderScrollFixed"]').removeClass('finderScroll');
                $('div[finderPanel]').attr('finderPanel', 'false');
            } else {
                $('div[finderPanel] div[class="finderScrollFixed"]').addClass('finderScroll');
                $('div[finderPanel] div[class="finderScrollFixed finderScroll"]').removeClass('finderScrollFixed');
                $('div[finderPanel]').attr('finderPanel', 'true');
            }

            // we have results, then fold parameters
            $('div[finderPanel="true"] > table[class="poptable"] div[onkeypress]').addClass('semi-fold-parameters-panel');
            $('div[finderPanel="true"] div[class="finderScroll"]').addClass('top_shadow');

            $('div[finderPanel="true"] > table[class="poptable"] div[onkeypress]').on('mouseenter', function () {
                // mouse enter remove fold
                $('div[finderPanel="true"] > table[class="poptable"] div[onkeypress]').removeClass('semi-fold-parameters-panel');
                $('div[finderPanel="true"] div[class="finderScroll top_shadow"]').removeClass('top_shadow');

                // find freezeheader if present then set top accordingly
                var top = $('div[finderPanel="true"] div[class="finderScroll"]').offset().top - $('div[finderPanel="true"]').offset().top - 2;
                $('div[finderPanel="true"] div[class="finderScroll"] > div[id^="hd"]').css('top', top + 'px');
            });

        } else {
            // no results, expand to full height

            $('div[finderPanel] > table[class="poptable"] div[onkeypress]').removeClass('semi-fold-parameters-panel');
            $('div[finderPanel] div[class="finderScroll top_shadow"]').removeClass('top_shadow');
        }

        // input filter
        $('input:text[numbersonly]').on("input", function () {
            var start = this.selectionStart,
                end = this.selectionEnd;
            $(this).val($(this).val().replace(/[^0-9]/g, ''));
            this.setSelectionRange(start, end);
        });

        $('input:text[floatingpointnumbersonly]').on("input", function () {
            var start = this.selectionStart,
                end = this.selectionEnd;
            $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
            this.setSelectionRange(start, end);
        });

        $('input:text[negativenumbersonly]').on("input", function () {
            var start = this.selectionStart,
                end = this.selectionEnd;
            $(this).val($(this).val().replace(/[^0-9\-]/g, ''));
            this.setSelectionRange(start, end);
        });



        $('input:text[eShipTimeInput]').each(function () {
            $(this).on("input", function () {
                var start = this.selectionStart,
                    end = this.selectionEnd;
                $(this).val($(this).val().replace(/[^0-9:]/g, ''));
                this.setSelectionRange(start, end);

                if ($(this).val().length > 5)
                    $(this).val($(this).val().substring(0, 5));

                if ($(this).val().length == 2 && $(this).val().indexOf(":") == -1)
                    $(this).val($(this).val() + ':');
                else if ($(this).val().length == 2 && $(this).val().indexOf(":") != -1)
                    $(this).val($(this).val().replace(/[:]/g, ''));
            });
        });

        $('input:text[eShipTimeInput]').on("change", function () {
            var ids = new ControlIds();
            ids.TimeString = $(this).attr('id');
            ValidateTimeString(ids);
        });

        $('input:text[eShipDateInput]').each(function () {
            $(this).datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                minDate: $(this).attr('eShipMinDate'),
                maxDate: $(this).attr('eShipMaxDate'),
            });

            $(this).keypress(function (e) {
                // fix for Chrome because in Chrome e.key is always null
                var character = e.key == null ? String.fromCharCode(e.which).toLowerCase() : e.key;
                if (e.which == 187 && e.shiftKey && e.key == null) character = '+';
                if (e.which == 189 && e.key == null) character = '-';

                var idx = "today+-1234567890m".indexOf(character);
                if (idx == -1) return;
                idx = $(this).val().indexOf(character);
                if (idx != -1) return;
                idx = "1234567890".indexOf(character);
                if (idx != -1) return;
                $(this).val($(this).val() + character);
            });
        });

        $('input:text[eShipDateInput]').change(function () {
            var ids = new ControlIds();
            ids.DateString = $(this).attr('id');
            ValidateDateString(ids);
        });
        
        $('input:text[eShipReadOnly]').each(function () {
            $(this).attr('readonly', 'readonly');
        });
    });

});