﻿
$(document).ready(function() {
	var msgs;

	// show message display
	msgs = $("#alertsbar").find('div[id^="message"]');

	// set alerts class
	$(msgs).attr("class", "normalAnnouncement");
	$("#alertsbar").find('div[id^="message1"]').attr("class", "alerts");

	// chain pop-out display for full message with dim backgroung
	$(msgs).each(function() {
		$(this).click(function() {
			var fullmsg = '#full' + $(this).attr("id");
			var dimDiv = '#annoucementDimBackgroud';

			$(fullmsg).click(function() {
				$(this).hide('slow');
				$(dimDiv).hide();
			});

			$(fullmsg).show('slow');
			$(dimDiv).show();
		});
	});

	// short message display
	if (msgs.length > 1)
		DivFade(0);
	else if (msgs.length == 1)
		$(msgs[0]).show();

	function DivFade(index) {
		if (msgs.length == 0)
			return;

		if (index >= msgs.length)
			index = 0;

		$(msgs[index]).fadeIn("slow").delay(5000)
			.fadeOut("slow", function() {
				DivFade(index + 1);
			});
	}
});

