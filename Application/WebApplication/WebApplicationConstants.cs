﻿namespace LogisticsPlus.Eship.WebApplication
{
    public class WebApplicationConstants
    {
		public const int KiloByte = 1024;
		public const int MegaByte = 1048576; // 1024 x 1024 where 1024 = 1 KB.

        

        public const string ActiveUser = "ActiveUser";
    	public const string ActiveSuperUser = "AdminActiveUser";
    	public const string IsSuperUserLogin = "IsSuperUserLogin";
		public const string UserSearchDefaults = "UserSearchDefaults";

		public const string SectionCode = "SectionCode";

    	public const string RedirectUrlPrefix = "ret";
    	public const string SuperAdminLoginPrefix = "eShipPlus_Admin_";

    	public const string BrandText = "eShipPlus TMS";

    	public const string DefaultRefreshInterval = "180000";

        public const string DataImportSheetName = "data";

        public const string Default = "[Default]";


	    public const string OriginTerminalCode = "ORC";
	    public const string DestinationTerminalCode = "DRC";
	    public const string CarrierScac = "Scac";
	    public const string CarrierName = "Name";

        public const string RedirectUrlPrefixQuestion = "---Question---";
        public const string RedirectUrlPrefixAmperSand = "---amp---";

    	public const string AppDefaultLogoUrl = "~/images/lplogo.png";
    	public const string ChooseOne = "- Choose One -";
    	public const string AutoDefault = "- Auto Default -";
    	public const string ChooseSearchProfile = "- Choose Search Profile -";
        public const string OnFile = "On File";
        public const string HtmlBreak = "<br />";
		public const string HtmlCheck = "&#x2713;";
		public const string Member2CssUrl = "../../css/member2.css";
		
    	public const string NotApplicable = "- Not Applicable -";
        public const string NotApplicableShort = "- N/A -";
    	public const string NotAvailable= "- Not Available -";
    	public const string ReadOnly = "Read-Only";
    	public const string Preview = "Preview";
    	public const string PreviousValueNotFound = "- Previous Value Not Found! -";

        public const string Ampersand = "&";
        public const string QuestionMark = "?";
        public const string Tab = "\t";
        public const string X = "X";

	    public const string Yes = "Yes";
	    public const string No = "No";

	    public const int Kb = 1024;
	    public const int Mb = Kb*Kb;
	    public const int Gb = Mb*Kb;

        public const int InvalidIndex = -1;

    	public const string PartnerReturnUrlKey = "PartnerReturnUrlKey";
        public const string PartnerHomeUrlKey = "PartnerHomeUrlKey";

    	public const string PartnerActionCodeIndicator = "Key";
    	public const string PartnerForgotPasswordKey = "pwd";

        public const string AccessCode = "AccessCode";
        public const string Username = "Username";
        public const string Password = "UserPassword";
        public const string ReturnUrl = "ReturnUrl";
        public const string HomeUrl = "HomeUrl";
        public const string AuthenticationErrorReturnUrl = "AuthenticationErrorReturnUrl";

    	public const string ShippingLabelShipmentId = "ShippingLabelShipmentId";

    	public const string DocumentViewerKey = "DocumentViewKey";

    	public const string DocumentViewerImageKey = "DocumentViewImageKey";
    	public const string DocumentViewerDocumentKey = "DocumentViewDocumentKey";
    	public const string DocumentViewerStatementKey = "DocumentViewStatementKey";
    	public const string DocumentViewerBolKey = "DocumentViewBolKey";
        public const string DocumentViewerInvoiceKey = "DocumentViewerInvoiceKey";
        public const string DocumentViewerEmailKey = "DocumentViewerEmailKey";

		public const string DocumentViewerDocumentId = "DocumentViewDocumentName";
    	public const string DocumentViewerShipmentId = "DocumentViewShipmentId";
		public const string DocumentViewerServiceTicketId = "DocumentViewerServiceTicketId";
		public const string DocumentViewerInvoiceId = "DocumentViewerInvoiceId";
        public const string DocumentViewerEmailLogId = "DocumentViewerEmailLogId";

    	public const string DocumentDeliveryUKey = "DocumentDeliveryUKey";

        public const string ClaimDashboardSearchCriteria = "ClaimDashboardSearchCriteria";
		public const string ChargeViewSearchCriteria = "ChargeViewSearchCriteria";
        public const string InvoiceDashboardSearchCriteria = "InvoiceDashboardSearchCriteria";
		public const string InvoicePostingSearchCriteria = "InvoicePostingSearchCriteria";
        public const string ShipmentDashboardSearchCriteria = "ShipmentDashboardSearchCriteria";
		public const string ShipmentsToBeInvoicedSearchCriteria = "ShipmentsToBeInvoicedSearchCriteria";
        public const string ServiceTicketSearchCriteria = "ServiceTicketSearchCriteria";
        public const string VendorBillDashboardSearchCriteria = "VendorBillDashboardSearchCriteria";
		public const string VendorBillPostingSearchCriteria = "VendorBillPostingSearchCriteria";

    	public const string ShipmentAuditId = "ShipmentAuditId";
    	public const string DownloadPdf = "DownloadPDF";

		public const string DisableEditModeOnTransfer = "DisableEditModeOnTransfer";

        public const string TransferAddressBookId = "TransferAddressBookId";
		public const string TransferClaimId = "TransferClaimId";
        public const string TransferJobId = "TransferJobId";
        public const string TransferCustomerCommunicationId = "CustomerCommunicationId";
        public const string TransferCustomerGroupId = "TransferCustomerGroupId";
        public const string TransferCustomerId = "CustomerId";
        public const string TransferCustomerRatingId = "CustomerRatingId";
        public const string TransferDeveloperAccessRequestId = "TransferDeveloperAccessRequestId";
        public const string TransferGroupId = "TransferGroupId";
		public const string TransferInvoiceId = "TransferInvoiceId";
        public const string TransferListOfInvoicesObject = "TransferListOfInvoicesObject";
        public const string TransferLoadOrderId = "TransferLoadOrderId";
        public const string TransferLoadOrderObject = "TransferLoadOrderObject";
    	public const string TransferShipmentId = "TransferShipmentId";
    	public const string TransferNumber = "Num";
	    public const string TransferPendingVendorId = "PendingVendorId";
	    public const string TransferReportConfigurationId = "TransferReportConfigurationId";
	    public const string TransferRegionId = "TransferRegionId";
	    public const string TransferReportScheduleId = "TransferReportScheduleId";
	    public const string TransferResellerAdditionId = "TransferResellerAdditionId";
	    public const string TransferServiceTicketId = "TransferServiceTicketId";
	    public const string TransferTierId = "TransferTierId";
	    public const string TransferUserId = "TransferUserId";
	    public const string TransferVendorBillId = "TransferVendorBillId";
	    public const string TransferVendorCommunicationId = "VendorCommunicationId";
	    public const string TransferVendorGroupId = "TransferVendorGroupId";
	    public const string TransferVendorId = "VendorId";
	    public const string TransferVendorRatingId = "VendorRatingId";
        public const string TransferDisputeVendorId = "TransferDisputeVendorId";


        public const string CsrCustId = "CsrCustId";

        public const string RateAndScheduleOfferedLoadTransfer = "RateAndScheduleOfferedLoadTransfer";
        public const string RateAndScheduleQuotedLoadTransfer = "RateAndScheduleQuotedLoadTransfer";
    	public const string RateAndScheduleShipmentTransfer = "RateAndScheduleShipmentTransfer";
    	public const string RateAndScheduleTransferId = "RateAndScheduleTransferId";
		public const string RateAndScheduleTransferType = "RateAndScheduleTransferType";

    	public const string EstimateRateShipmentTransfer = "EstimateRateShipmentTransfer";

		public const string CustomerLoadDashboardShipmentTransfer = "CustomerLoadDashboardShipmentTransfer";


    	public const string Customer = " Customer";
    	public const string DestinationCountry = " Destination Country";
    	public const string DestinationPostalCode = " Destination Postal Code";
		public const string Dimensions = " Dimensions";
		public const string FreightClass = " Freight Class";
    	public const string OriginPostalCode = " Origin Postal Code";
    	public const string OriginCountry = " Origin Country";
		public const string PackageType = " Package Type";
    	public const string Quantity = " Quantity";
    	public const string Weight = " Weight";


        public const string CustomLoginPageName = "Index.htm";
    	public const string CustomLoginAlertsPlaceHolder = "<div id='alerts' style='padding: 0px; margin: 0px; height: 0px;'></div>";
    	public const string CustomLoginAlertsFormat = "<div style='padding: 15px 15px 15px 50px !important; height: auto !important; text-align: left;' class='alerts'>{0}</div>";

    	public const string NoBillToAddressOnFile = "No Bill To Address On File!";
    	public const string NoRemitToAddressOnFile = "No Remit To Address On File!";

    	public const string BOLDescription = "Bill of Lading";



        public const string EdiStatusNotApplicable = "Not Applicable";
        public const string EdiStatusNoStatus = "No Status";
        public const string EdiStatus204Sent = "204 Sent";
        public const string EdiStatus204Accepted = "204 Accepted";
        public const string EdiStatus204Rejected = "204 Rejected";
        public const string EdiStatus204Cancelled = "204 Cancelled";
        public const string EdiStatus204Expired = "204 Expired";

        public const string XmlConnectRecordsDocTypeKey = "xcrdt";
        public const string XmlConnectRecordsShipmentIdKey = "xcrsin";

		public const string LoadTenderEmailResponseVendorIdKey = "ltervik";


    	public const string AccountBucketImportTemplate = "AccountBucketImportTemplate.txt";
        public const string AddressBookImportTemplate = "AddressBookImportTemplate.txt";
        public const string AssetsImportTemplate = "AssetsImportTemplate.txt";
        public const string AverageWeeklyFuelImportTemplate = "AverageWeeklyFuelImportTemplate.txt";
        public const string ChargeCodeImportTemplate = "ChargeCodeImportTemplate.txt";
        public const string CommissionsToBePaidImportTemplate = "CommissionsToBePaidImportTemplate.txt";
        public const string ContactTypesImportTemplate = "ContactTypesImportTemplate.txt";
		public const string CountryImportTemplate = "CountryImportTemplate.txt";
        public const string CustomerControlAccountImportTemplate = "CustomerControlAccountImportTemplate.txt";
        public const string CustomerGroupsImportTemplate = "CustomerGroupsImportTemplate.txt";
        public const string CustomerPurchaseOrderImportTemplate = "CustomerPurchaseOrderImportTemplate.txt";
        public const string CustomerRatingCustomerServiceMarkupImportTemplate = "CustomerRatingCustomerServiceMarkupImportTemplate.txt";

        public const string CustomerRatingBatchInsertCustomerServiceMarkupTemplate = "CustomerRatingBatchInsertCustomerServiceMarkupTemplate.txt";
        public const string CustomerRatingBatchInsertLTLSellRatesImportTemplate = "CustomerRatingBatchInsertLTLSellRatesTemplate.txt";
        public const string CustomerRatingBatchInsertSmallPackageRatesImportTemplate = "CustomerRatingBatchInsertSmallPackageRatesTemplate.txt";
        public const string CustomerRatingBatchInsertTLSellRatesImportTemplate = "CustomerRatingBatchInsertTLSellRatesTemplate.txt";
        public const string CustomerRatingBatchInsertProfitImportTemplate = "CustomerRatingBatchInsertProfitTemplate.txt";

        public const string CustomerRatingBatchRemoveCustomerServiceMarkupImportTemplate = "CustomerRatingBatchRemoveCustomerServiceMarkupTemplate.txt";
        public const string CustomerRatingBatchRemoveLTLSellRatesImportTemplate = "CustomerRatingBatchRemoveLTLSellRatesTemplate.txt";
        public const string CustomerRatingBatchRemoveSmallPackageRatesImportTemplate = "CustomerRatingBatchRemoveSmallPackRatesTemplate.txt";
        public const string CustomerRatingBatchRemoveTLSellRatesImportTemplate = "CustomerRatingBatchRemoveTLSellRatesTemplate.txt";
		
        public const string CarrierTerminalImportTemplate = "CarrierTerminalImportTemplate.txt";
        public const string CarrierPreferredLanesImportTemplate = "CarrierPreferredLanesImportTemplate.txt";
        public const string CustomerRatingLTLSellRateImportTemplate = "CustomerRatingLTLSellRateImportTemplate.txt";
        public const string CustomerRatingSmallPackageRatesImportTemplate = "CustomerRatingSmallPackageRatesImportTemplate.txt";
        public const string CustomerRatingTLSellRateImportTemplate = "CustomerRatingTLSellRateImportTemplate.txt";
        public const string TruckloadTenderingProfileLaneImportTemplate = "TruckloadTenderingProfileLaneImportTemplate.txt";
        public const string TruckloadTenderingProfileVendorImportTemplate = "TruckloadTenderingProfileVendorImportTemplate.txt";
        public const string DocumentTagsImportTemplate = "DocumentTagsImportTemplate.txt";
        public const string EquipmentTypesImportTemplate = "EquipmentTypesImportTemplate.txt";
        public const string FailureCodeImportTemplate = "FailureCodeImportTemplate.txt";
        public const string FuelTablesImportTemplate = "FuelTablesImportTemplate.txt";
        public const string InsuranceTypesImportTemplate = "InsuranceTypesImportTemplate.txt";
        public const string InvoiceDetailImportTemplate = "InvoiceDetailImportTemplate.txt";
        public const string InvoicePaidAmountUpdatesImportTemplate = "InvoicePaidAmountUpdatesImportTemplate.txt";
        public const string LibraryItemsImportTemplate = "LibraryItemsImportTemplate.txt";
        public const string LTLRateAnalysisImportTemplate = "LTLRateAnalysisImportTemplate.txt";
        public const string MileageSourcesImportTemplate = "MileageSourcesImportTemplate.txt";
        public const string NoServiceDaysImportTemplate = "NoServiceDaysImportTemplate.txt";
        public const string PackageTypesImportTemplate = "PackageTypesImportTemplate.txt";
        public const string PostalCodesImportTemplate = "PostalCodeImportTemplate.txt";
        public const string PrefixesImportTemplate = "PrefixesImportTemplate.txt";
        public const string QlikUserImportTemplate = "QlikUserImportTemplate.txt";
        public const string QuickPayOptionsImportTemplate = "QuickPayOptionsImportTemplate.txt";
        public const string RegionsImportTemplate = "RegionsImportTemplate.txt";
        public const string SalesRepresentativeImportTemplate = "SalesRepresentativeImportTemplate.txt";
        public const string ServicesImportTemplate = "ServicesImportTemplate.txt";
        public const string P44ChargeCodeMappingImportTemplate = "P44ChargeCodeMappingImportTemplate.txt";
        public const string P44ServiceMappingImportTemplate = "P44ServiceMappingImportTemplate.txt";
        public const string ServiceTicketImportTemplate = "ServiceTicketImportTemplate.txt";
        public const string ShipmentPrioritiesImportTemplate = "ShipmentPrioritiesImportTemplate.txt";
        public const string SmallPackageRateAnalysisImportTemplate = "SmallPackageRateAnalysisInputTemplate.txt";
        public const string TarrifRateAnalysisImportTemplate = "TarrifRateAnalysisImportTemplate.txt";
        public const string TLRateAnalysisImportTemplate = "TLRateAnalysisImportTemplate.txt";
        public const string UserDefinedPermissionDetailImportTemplate = "UserDefinedPermissionDetailImportTemplate.txt";
        public const string UserDepartmentImportTemplate = "UserDepartmentImportTemplate.txt";
        public const string UserImportTemplate = "UserImportTemplate.txt";
        public const string VendorBillsDetailsImportTemplate = "VendorBillsDetailsImportTemplate.txt";
        public const string VendorBillsMassUploadImportTemplate = "VendorBillsMassUploadTemplate.txt";
        public const string VendorGroupsImportTemplate = "VendorGroupsImportTemplate.txt";
        public const string VendorProMassUpdateImportTemplate = "VendorProMassUpdateImportTemplate.txt";
        public const string VendorRatingAdditionalChargesImportTemplate = "VendorRatingAdditionalChargesImportTemplate.txt";
        public const string VendorRatingDiscountTiersImportTemplate = "VendorRatingDiscountTiersImportTemplate.txt";
        public const string VendorRatingImportTLTiersTemplate = "VendorRatingImportTLTiersTemplate.txt";
        public const string VendorRatingLTLAccessorialsImportTemplate = "VendorRatingLTLAccessorialsImportTemplate.txt";
        public const string VendorRatingTLTierIndexImportTemplate = "VendorRatingTLTierIndexImportTemplate.txt";
        public const string VendorRatingLtlcfCapacityImportTemplate = "VendorRatingLTLCFCapacityImportTemplate.txt";
        public const string VendorRatingLTLOverlengthRulesImportExport = "VendorRatingLTLOverlengthRulesImportTemplate.txt";
        public const string VendorRatingLTLPackageSpecificRatesImportTemplate = "VendorRatingLTLPackageSpecificRatesImportTemplate.txt";
        public const string VendorRatingLTLGuaranteedChargesImportTemplate = "VendorRatingLTLGuaranteedChargesImportTemplate.txt";


    	public const string VendorProPlaceHolder = "{PRO}";

        public const string Ascending = "Ascending";
        public const string Descending = "Descending";

        public const string DispatchShipmentMissingUserPhoneNumberErrMsg = "User profile has no phone number and cannot dispatch shipments without a phone number. Please add a phone number to your user profile.";
        public const string IncorrectImportLineColCntErrMsg = "Incorrect column count on line: {0}";
        public const string InvalidImportedHeaderErrMsg = "Invalid Header found on line {0}";
	    public const string InvalidImportedEnumErrMsg = "Invalid {0} Type found on line {1}";
	    public const string InvalidImportedCodeErrMsg = "Invalid {0} found on line {1}";
        public const string InvalidFreightClassErrMsg = "Invalid Freight Class on line {0}";
	    public const string NoRecordsImported = "No records imported";
        public const string NoRecordsChanged = "No records have been changed";
	    public const string NoResults = "No Result(s)";

        public const string MacroPointCheckCallCoordinateSection = "Location Notification - Point:";
        public const int MacroPointIdNumberStringLength = 50;
        public const string MacroPointPickupCode = "Pickup";
        public const string MacroPointDropOffCode = "DropOff";

        public const string GuaranteedServiceCriticalBolComments = "GUARANTEED BY {0} DELIVERY SERVICE ON {1}";
        public const string ExpeditedCriticalBolComments = "EXPEDITED BY {0} DELIVERY SERVICE ON {1}";

        public const string ApplicationMode = "ApplicationMode";
        public const string ClassicView = "ClassicView";
        public const string NewView = "NewView";

        public const string DeliveryDateTag = "[#Date#]";

        public const string CssClassDisabledWithSpace = " disabled";
        public const string CssClassHiddenWithSpace = " hidden";

        public const int PaymentGatewayExpirationDateYearsFromNow = 10;

        public const string RequestEventTargetIndex = "__EVENTTARGET";
        public const string RequestEventArgumentIndex = "__EVENTARGUMENT";

        public const string OriginalUserId = "OriginalUserId";
        public const string IsImpersonating = "IsImpersonating";
    }
}
