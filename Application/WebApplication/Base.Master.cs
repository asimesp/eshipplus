﻿using System;
using System.Web.UI;

namespace LogisticsPlus.Eship.WebApplication
{
	public partial class Base : MasterPage
	{
		private void LoadJavaScript()
		{
			Page.ClientScript.RegisterClientScriptInclude("jquery.js", ResolveUrl("~/js/jquery-2.0.3.min.js"));
			Page.ClientScript.RegisterClientScriptInclude("AnnoucementsAndNews.js", ResolveUrl("~/js/AnnouncementsAndNews.js"));
			Page.ClientScript.RegisterClientScriptInclude("jquery.freezeheader.js", ResolveUrl("~/js/jquery.freezeheader.js"));
			Page.ClientScript.RegisterClientScriptInclude("Utilities.js", ResolveUrl("~/js/Utilities.js"));
			Page.ClientScript.RegisterClientScriptInclude("eShipPlusFeaturesSetup.js", ResolveUrl("~/js/eShipPlusFeaturesSetup.js"));
			Page.ClientScript.RegisterClientScriptInclude("placeholder.js", ResolveUrl("~/js/placeholder.js"));
			Page.ClientScript.RegisterClientScriptInclude("jquery.uniform.min.js", ResolveUrl("~/js/jquery.uniform.min.js"));
			Page.ClientScript.RegisterClientScriptInclude("jquery.cookie.js", ResolveUrl("~/js/jquery.cookie.js"));
			Page.ClientScript.RegisterClientScriptInclude("jquery-ui.js", ResolveUrl("~/js/jquery-ui.js"));
			Page.ClientScript.RegisterClientScriptInclude("angular.js", ResolveUrl("~/scripts/angular.js"));
			Page.ClientScript.RegisterClientScriptInclude("angular-mocks.js", ResolveUrl("~/scripts/angular-mocks.js"));
            Page.ClientScript.RegisterClientScriptInclude("flotr2.min.js", ResolveUrl("~/js/flotr2.min.js"));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			LoadJavaScript();
		}
	}
}