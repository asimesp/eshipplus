﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation.Operations;
using LogisticsPlus.Eship.Smc.Eva;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.WebApplication.Utilities;
using Newtonsoft.Json;

namespace LogisticsPlus.Eship.WebApplication.smc3
{
	public partial class EvaTest : EvaServiceProxyPageBase
	{
		public override EvaSettings GetSettings()
		{
			return WebApplicationSettings.SmcEvaSettings;
		}

		public override void QueTrackingDataAsCheckCalls(TrackingData data)
		{
			throw new NotImplementedException();
		}

		public override void SaveEvaTrackingInformation(TrackingData data)
		{
			throw new NotImplementedException();
		}

		public override void UpdateShipmentDispatchInformation(DispatchData data)
		{
			throw new NotImplementedException();
		}

		public override void SaveDispatchInformation(DispatchData data)
		{
			throw new NotImplementedException();
		}

		public override void LogErrors(List<string> errors)
		{
			ErrorLogger.LogError(new Exception(errors.ToArray().NewLineJoin()), Context);
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			litMsg.Text = string.Empty;
		}

		protected void OnSubmitTrackingClicked(object sender, EventArgs e)
		{
			try
			{
				var r = new TrackRequest
					{
						accountToken = txtAccountToken.Text,
						eventTypes = new List<EventType> {new EventType {eventType = EventTypes.All}},
                        referenceNumbers = new List<ReferenceNumber>
                            {
                                new ReferenceNumber
                                    {
                                        referenceNumber = txtBol.Text,
                                        originPostalCode = txtOriginZip.Text,
                                        destinationPostalCode = txtDestinationZip.Text,
                                        expectedPickupDate = txtDesiredPickupDate.Text.ToDateTime().ToString("yyyyMMdd")
                                    },
                                new ReferenceNumber
                                    {
                                        referenceNumber = txtPrefix.Text + txtBol.Text,
                                        originPostalCode = txtOriginZip.Text,
                                        destinationPostalCode = txtDestinationZip.Text,
                                        expectedPickupDate = txtDesiredPickupDate.Text.ToDateTime().ToString("yyyyMMdd")
                                    }
                            },
						referenceType = ReferenceTypes.BillOfLadingNumber,
						scac = txtScac.Text
					};
				var res = InitiateTracking(r);

                var body = JsonConvert.SerializeObject(r) + Environment.NewLine + Environment.NewLine + JsonConvert.SerializeObject(res);

				new[] {"kyle.grygo@logisticsplus.net", "chris.oyesiku@logisticsplus.net"}.SendEmail(body, "SMC3 EVA eShipPlus Test: Tracking Request", default(long));

			    var validator = new SMC3RequestResponseValidator();
			    var trackObj = new SMC3TrackRequestResponse()
			        {
			            ShipmentId = new ShipmentSearch().FetchShipmentByShipmentNumber(txtBol.Text, 4).Id,
			            RequestData = JsonConvert.SerializeObject(r),
			            TransactionId = res.transactionID,
			            ResponseData = JsonConvert.SerializeObject(res),
			            DateCreated = DateTime.Now,
			            Scac = txtScac.Text
			        };

			    var isValid = false;
                if (validator.IsValid(trackObj))
                {
                    trackObj.Save();
                    isValid = true;
                }

			    litMsg.Text = isValid
                                  ? string.Format(@"
                                        JSON Request:
                                        {0}

                                        JSON Response:
                                        {1}", JsonConvert.SerializeObject(r), JsonConvert.SerializeObject(res))
                                  : string.Format(
                                      @"JSON Request:
                                        {0}

                                        JSON Response:
                                        {1}

                                        Errors occurred while saving to database:

                                        {2}", JsonConvert.SerializeObject(r), JsonConvert.SerializeObject(res), validator.Messages.Select(m => m.Message).ToArray().NewLineJoin());
			}
			catch (Exception ex)
			{
				litMsg.Text = ex.ToString();
			}
		}

		protected void OnSubmitDispatchClicked(object sender, EventArgs e)
		{
			try
			{
				var tenant = new TenantSearch().FetchTenants(new List<ParameterColumn>()).First();
				var shipment = new ShipmentSearch().FetchShipmentByShipmentNumber(txtBol.Text, tenant.Id);
				var vendor = shipment.Vendors.First(v => v.Primary).Vendor;
				var r = vendor.Communication.GetDispatchRequest(shipment, vendor);
				var res = InitiateDispatch(r);

                var body = JsonConvert.SerializeObject(r) + Environment.NewLine + Environment.NewLine + JsonConvert.SerializeObject(res);

                new[] { "kyle.grygo@logisticsplus.net", "chris.oyesiku@logisticsplus.net"}.SendEmail(body, "SMC3 EVA eShipPlus Test: Dispatch Request", default(long));

                var validator = new SMC3RequestResponseValidator();
                var trackObj = new SMC3DispatchRequestResponse()
                {
                    ShipmentId = new ShipmentSearch().FetchShipmentByShipmentNumber(txtBol.Text, 4).Id,
                    RequestData = JsonConvert.SerializeObject(r),
                    TransactionId = res.transactionID,
                    ResponseData = JsonConvert.SerializeObject(res),
                    DateCreated = DateTime.Now,
                    Scac = txtScac.Text
                };

                var isValid = false;
                if (validator.IsValid(trackObj))
                {
                    trackObj.Save();
                    isValid = true;
                }

                litMsg.Text = isValid
                                  ? string.Format(@"
                                        JSON Request:
                                        {0}

                                        JSON Response:
                                        {1}", JsonConvert.SerializeObject(r), JsonConvert.SerializeObject(res))
                                  : string.Format(
                                      @"JSON Request:
                                        {0}

                                        JSON Response:
                                        {1}

                                        Errors occurred while saving to database:

                                        {2}", JsonConvert.SerializeObject(r), JsonConvert.SerializeObject(res), validator.Messages.Select(m => m.Message).ToArray().NewLineJoin());
			}
			catch (Exception ex)
			{
				litMsg.Text = ex.ToString();
			}
		}
	}
}