﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Extern.Edi.Segments;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Smc;
using LogisticsPlus.Eship.Smc.Eva;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.Utilities.Que;
using Newtonsoft.Json;

namespace LogisticsPlus.Eship.WebApplication.smc3
{
	public partial class dispatchresponse : EvaServiceProxyPageBase
	{
		public override EvaSettings GetSettings()
		{
			return WebApplicationSettings.SmcEvaSettings;
		}

		public override void QueTrackingDataAsCheckCalls(TrackingData trackingData)
		{
			throw new NotImplementedException();
		}

		public override void SaveEvaTrackingInformation(TrackingData data)
		{
			throw new NotImplementedException();
		}



		public override void UpdateShipmentDispatchInformation(DispatchData dispatchData)
		{
			try
			{
				var search = new SMC3DispatchRequestResponseSearch();
				var record = search.FetchDispatchRequestResponseByTransactionId(dispatchData.transactionID);

				if (record == null)
				{
					ErrorLogger.LogError(new Exception(string.Format("Dispatch data transaction record not found! [{0}]",
																	 JsonConvert.SerializeObject(dispatchData))), Context);
					return;
				}

				var shipment = record.Shipment;

				if (record.Shipment == null)
				{
					ErrorLogger.LogError(new Exception(string.Format("Dispatch data not associated with a shipment! [{0}]",
																	 JsonConvert.SerializeObject(dispatchData))), Context);
					return;
				}

				if (shipment.Vendors.First(v => v.Primary).Vendor.Scac != dispatchData.scac)
				{
					ErrorLogger
						.LogError(new Exception(string.Format("Dispatch data information key does not match shipment key [{0}]",
													JsonConvert.SerializeObject(dispatchData))), Context);
					return;
				}

				var checkCall = new CheckCall
					{
						CallNotes = dispatchData.pickupNumber,
						CallDate = DateTime.Now,
						Shipment = shipment,
						TenantId = shipment.TenantId,
						User = shipment.Tenant.DefaultSystemUser,
						EdiStatusCode = ShipStatMsgCode.AA.GetString(),
						EventDate = dispatchData.dateAccepted.DateTimeFromEvaDateAndTime(),
					};

				shipment.LoadCollections();
				shipment.CheckCalls.Add(checkCall);
				Exception ex;
				var errs = new ShipmentUpdateHandler().UpdateShipmentTrackingInfo(shipment, out ex);
				if (ex != null) ErrorLogger.LogError(ex, Context);
				if (ex == null && errs.Any())
				{
					var procHandler = new ProcQueObjProcessHandler();
					var obj = new ProcQueObj
						{
							ObjType = ProcQueObjType.ShipmentStatusQue,
							Xml = new ShipmentStatusUpdateQue(checkCall).ToXml(),
							DateCreated = DateTime.Now
						};
					procHandler.Save(obj, out ex);
					if (ex != null) ErrorLogger.LogError(ex, Context);
				}
			}
			catch (Exception e)
			{
				ErrorLogger.LogError(e, Context);
			}
		}

		public override void SaveDispatchInformation(DispatchData data)
		{
			try
			{
				var proc = new Smc3EvaDataProcessor();
				Exception ex;
				var messages = proc.SaveDispatchData(data, out ex);

				if (ex != null) ErrorLogger.LogError(ex, Context);
				if (messages.Any()) ErrorLogger.LogError(new Exception(messages.Select(m => m.Message).ToArray().NewLineJoin()), Context);
			}
			catch (Exception ex)
			{
				ErrorLogger.LogError(ex, Context);
			}
		}


		public override void LogErrors(List<string> errors)
		{
			ErrorLogger.LogError(new Exception(errors.ToArray().NewLineJoin()), Context);
		}


		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
				ProcessDispatchResponse();
			}
			catch (Exception ex)
			{
				ErrorLogger.LogError(ex, Context);
			}
		}


	}
}