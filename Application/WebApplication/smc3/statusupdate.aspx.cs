﻿using System.Collections.Generic;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Extern.Edi.Segments;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Smc.Eva;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.Utilities.Que;
using LogisticsPlus.Eship.Smc;
using System;
using System.Linq;
using Newtonsoft.Json;

namespace LogisticsPlus.Eship.WebApplication.smc3
{
    public partial class StatusUpdate : EvaServiceProxyPageBase
	{
		public override EvaSettings GetSettings()
		{
			return WebApplicationSettings.SmcEvaSettings;
		}

	    public override void QueTrackingDataAsCheckCalls(TrackingData trackingData)
	    {
		    try
		    {
			    var search = new SMC3TrackRequestResponseSearch();
			    var record = search.FetchTrackRequestResponseByTransactionId(trackingData.transactionID);

			    if (record == null)
			    {
				    ErrorLogger.LogError(new Exception(string.Format("Tracking data transaction record not found! [{0}]",
				                                                     JsonConvert.SerializeObject(trackingData))), Context);
				    return;
			    }

			    var shipment = record.Shipment;

			    if (record.Shipment == null)
			    {
				    ErrorLogger.LogError(new Exception(string.Format("Tracking data not associated with a shipment! [{0}]",
				                                                     JsonConvert.SerializeObject(trackingData))), Context);
				    return;
			    }

			    var number = trackingData.referenceNumber.StripNonDigits();
			    if (trackingData.referenceType != ReferenceTypes.BillOfLadingNumber || shipment.ShipmentNumber != number ||
			        shipment.Vendors.First(v => v.Primary).Vendor.Scac != trackingData.scac)
			    {
				    ErrorLogger.LogError(
					    new Exception(string.Format("Tracking data information keys do not match shipment keys [{0}]",
					                                JsonConvert.SerializeObject(trackingData))), Context);
				    return;
			    }

			    var codes = ProcessorUtilities.GetAll<ShipStatMsgCode>().Values.ToList();
			    if (trackingData.statusCode == "AD") trackingData.statusCode = "AB";  // temp hack to address SMC3 using AD as code for AB!
			    var checkCall = new CheckCall
				    {
					    CallNotes = string.IsNullOrEmpty(trackingData.statusCode)
						                ? trackingData.responseMessage
						                : trackingData.statusDescription,
					    CallDate = DateTime.Now,
					    Shipment = shipment,
					    TenantId = shipment.TenantId,
					    User = shipment.Tenant.DefaultSystemUser,
						EdiStatusCode = codes.Contains(trackingData.statusCode) ? trackingData.statusCode : string.Empty,
						EventDate = trackingData.statusDate.DateTimeFromEvaDateAndTime(trackingData.statusTime)
				    };

                if (trackingData.statusCode == ShipStatMsgCode.AF.ToString())
                {
                    checkCall.EventDate = trackingData.pickupDate.DateTimeFromEvaDateAndTime(trackingData.pickupTime);
                }
                if (trackingData.statusCode == ShipStatMsgCode.D1.ToString())
                {
                    checkCall.EventDate = trackingData.deliveryDate.DateTimeFromEvaDateAndTime(trackingData.deliveryTime);
                }
                if (trackingData.statusCode == ShipStatMsgCode.CA.ToString())
                {
                    checkCall.EventDate = trackingData.statusDate.DateTimeFromEvaDateAndTime(trackingData.statusTime);
                }
                if (trackingData.statusCode == ShipStatMsgCode.AG.ToString())
                {
                    checkCall.EventDate = trackingData.etaDestinationDate.DateTimeFromEvaDateAndTime(trackingData.etaDestinationTime);
                }
                if (trackingData.statusCode == ShipStatMsgCode.AA.ToString())
                {
                    checkCall.EventDate = trackingData.appointmentDate.DateTimeFromEvaDateAndTime(trackingData.appointmentTime);
                }
                if (trackingData.statusCode == ShipStatMsgCode.AB.ToString())
                {
                    checkCall.EventDate = trackingData.appointmentDate.DateTimeFromEvaDateAndTime(trackingData.appointmentTime);
                }


			    var proQue = new ShipmentStatusUpdateQuePro(trackingData.scac, trackingData.pro);
			    var proc = new ProcQueObj
				    {
					    ObjType = ProcQueObjType.ShipmentStatusQue,
					    Xml = new ShipmentStatusUpdateQue(checkCall, new[] {proQue}.ToList()).ToXml(),
					    DateCreated = DateTime.Now
				    };

			    Exception ex;
			    new ProcQueObjProcessHandler().Save(proc, out ex);
			    if (ex != null) ErrorLogger.LogError(ex, Context);
		    }
		    catch (Exception e)
		    {
			    ErrorLogger.LogError(e, Context);
		    }
	    }

	    public override void SaveEvaTrackingInformation(TrackingData data)
		{
			try
			{
				var proc = new Smc3EvaDataProcessor();
				Exception ex;
				var messages = proc.SaveTrackingData (data, out ex);

                if(ex != null) ErrorLogger.LogError(ex, Context);
				if (messages.Any()) ErrorLogger.LogError(new Exception(messages.Select(m => m.Message).ToArray().NewLineJoin()), Context);
			}
			catch (Exception ex)
			{
				ErrorLogger.LogError(ex, Context);
			}
		}

	    public override void UpdateShipmentDispatchInformation(DispatchData data)
	    {
		    throw new NotImplementedException();
	    }

	    public override void SaveDispatchInformation(DispatchData data)
	    {
		    throw new NotImplementedException();
	    }

	    public override void LogErrors(List<string> errors)
	    {
		    ErrorLogger.LogError(new Exception(errors.ToArray().NewLineJoin()), Context);
	    }

	    protected void Page_Load(object sender, EventArgs e)
		{
            try
            {
                ProcessTrackingResponse();
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex, Context);
            }
		}
	}
}