﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Fax;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Smc.Eva;
using LogisticsPlus.Eship.WebApplication.Utilities;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.WebApplication
{
	public static class WebApplicationSettings
	{
		public const string SystemAlertsFile = "Alerts.txt";
		public const string AnnouncementXmlFile = "Announcements.xml";

		private static List<Double> _freightClasses;

		// KEEP A-Z PLEASE!!!

		public static int AnalysisMaximumInputLineCount
		{
			get { return GetSetting("AnalysisMaximumInputLineCount").ToInt(); }
		}

		public static long AnalysisMaximumFileUploadSize
		{
			get { return GetSetting("AnalysisMaximumFileUploadSize").ToLong(); }
		}

		public static string ApplicationLogsFolder
		{
			get { return BaseFilePath + GetSetting("ApplicationLogs"); }
		}

		public static string BaseFilePath
		{
			get { return GetSetting("BaseFilePath"); }
		}

		public static string BaseLoginPath
		{
			get { return GetSetting("BaseLoginPath"); }
		}

		public static string Brand
		{
			get { return string.Format("*** {0} {1}  - {2:yyyy-MM-dd hh:mm:ss} ***", WebApplicationConstants.BrandText, Version, DateTime.Now); }
		}

        public static string DatLoadboardHttpUrl
        {
            get { return GetSetting("DatLoadboardHttpUrl"); }
        }

        public static string DatLoadboardHttpsUrl
        {
            get { return GetSetting("DatLoadboardHttpsUrl"); }
        }

        public static string DatRateViewHttpsUrl
        {
            get { return GetSetting("DatRateViewHttpsUrl"); }
        }

		public static string DefaultConnectionString
		{
			get { return GetConnectionString("DefaultConnectionString"); }
		}

		public static DatabaseType DefaultDatabaseType
		{
			get { return DatabaseType.MsSql; }
		}

		public static int DefaultQueryTimeout
		{
			get { return GetSetting("DefaultQueryTimeout").ToInt(); }
		}

        public static string Domain
        {
            get { return GetSetting("Domain"); }
        }

		public static string DoNotReplyEmail
		{
			get { return GetSetting("doNotReplyEmail"); }
		}

		public static bool EnableDocDeliveryProcessor
		{
			get { return GetSetting("EnableDocDeliveryProcessor").ToBoolean(); }
		}

        public static bool EnableEmailProcessor
        {
            get { return GetSetting("EnableEmailProcessor").ToBoolean(); }
        }

		public static bool EnableRateAnalysisProcessor
		{
			get { return GetSetting("EnableRateAnalysisProcessor").ToBoolean(); }
		}

		public static bool EnableScheduler
		{
			get { return GetSetting("EnableScheduler").ToBoolean(); }
		}

		public static bool EnableXmlTransmissionManager
		{
			get { return GetSetting("EnableXmlTransmissionManager").ToBoolean(); }
		}

		public static bool EnableTruckloadTenderingProcessor
		{
			get { return GetSetting("EnableTruckloadTenderingProcessor").ToBoolean(); }
		}

		public static int ErrorLogDeleteAfterDays
		{
			get { return GetSetting("ErrorLogDeleteAfterDays").ToInt(); }
		}

		public static string ErrorLogNotificationEmails
		{
			get { return GetSetting("ErrorLogNotificationEmails"); }
		}

		public static string ErrorsFolder
		{
			get { return BaseFilePath + GetSetting("ErrorsFolder"); }
		}

		public static bool FaxServiceEnabled
		{
			get { return GetSetting("FaxServiceEnabled").ToBoolean(); }
		}

		public static ServiceProvider FaxServiceProvider
		{
			get { return GetSetting("FaxServiceProvider").ToEnum<ServiceProvider>(); }
		}

		public static int FaxServiceFailAfterInterval
		{
			get { return GetSetting("FaxServiceFailAfterInterval").ToInt(); }
		}

		public static string FaxServiceEmail
		{
			get { return GetSetting("FaxServiceEmail"); }
		}

        public static bool FaxServiceModeIsTest
        {
            get { return GetSetting("FaxServiceMode").ToUpper() == "T"; }
        }

        public static string FaxServiceTestNumber
        {
            get { return GetSetting("FaxServiceTestNumber"); }
        }
        
        public static FaxSettings FaxSettings
		{
			get
			{
				return new FaxSettings
					{
						RingCentralUser = GetSetting("RingCentralServiceUser"),
						RingCentralUserPassword = GetSetting("RingCentralUserPassword"),
						RingCentralUserExtension = GetSetting("RingCentralUserExtension"),
						RingCentralAppKey = GetSetting("RingCentralAppKey"),
						RingCentralAppSecret = GetSetting("RingCentralAppSecret"),
						RingCentralBaseUrl = GetSetting("RingCentralBaseUrl"),
						InterfaxUser = GetSetting("InterfaxUser"),
						InterfaxUserPassword = GetSetting("InterfaxUserPassword"),
					};
			}
		}

		public static List<double> FreightClasses
		{
			get
			{
				return _freightClasses ??
				       (_freightClasses = GetSetting("FreightClasses")
				                          	.Split(WebApplicationUtilities.ImportSeperators(),
				                          	       StringSplitOptions.RemoveEmptyEntries)
				                          	.Select(s => s.ToDouble())
				                          	.ToList());
			}
		}

		public static string GetAccountEmail
		{
			get { return GetSetting("GetAccountEmail"); }
		}

		public static string ImportExportTemplatesFolder
		{
			get { return BaseFilePath + GetSetting("ImportExportTemplates"); }
		}

	    public static string BackgroundReportRunFolder
	    {
	        get { return BaseFilePath + GetSetting("BackgroundReportRunFolder"); }
	    }

        public static long MaxReportAttachmentSize
		{
			get { return GetSetting("MaxReportAttachmentSize").ToLong(); }
		}
	    

		public static bool OnlinePaymentEnabled
		{
			get { return GetSetting("OnlinePaymentEnabled") == "Yes"; }
		}

		public static string OnlinePaymentAmountParam
		{
			get { return GetSetting("OnlinePaymentAmountParam"); }
		}

		public static string OnlinePaymentReferenceParam
		{
			get { return GetSetting("OnlinePaymentReferenceParam"); }
		}

		public static string OnlinePaymentUri
		{
			get { return GetSetting("OnlinePaymentUri"); }
		}


		public static string PdfToHtmlConverterPath
		{
			get { return GetSetting("PdfToHtmlConverterPath"); }
		}

	    public static string Project44Host
        {
	        get { return GetSetting("Project44Host"); }
	    }

	    public static string Project44Password
        {
	        get { return GetSetting("Project44Password"); }
	    }

	    public static string Project44Username
        {
	        get { return GetSetting("Project44Username"); }
	    }

	    public static string Project44PrimaryLocationId
        {
	        get { return GetSetting("Project44PrimaryLocationId"); }
	    }

	    public static bool EnableProject44RateProcessor
        {
	        get { return GetSetting("EnableProject44RateProcessor").ToBoolean(); }
	    }

        public static int ReportEngineQueryTimeout
		{
			get { return GetSetting("ReportEngineQueryTimeout").ToInt(); }
		}


		public static string QlikClientCertificateName
		{
			get { return GetSetting("QlikClientCertificateName"); }
		}

		public static string QlikProxyRestUri
		{
			get { return GetSetting("QlikProxyRestUri"); }
		}


		public static bool ShipmentDispatchEnabled
		{
			get { return GetSetting("ShipmentDispatchEnabled").ToBoolean(); }
		}

	    public static int SystemFileUploadMaxSize
	    {
			get { return GetSetting("SystemFileUploadMaxSize").ToInt(); }
	    }

		// Must set at application init!
		public static string SiteRoot { get; set; }

		public static bool SiteSslEnabled
		{
			get { return GetSetting("SiteSslEnabled").ToBoolean(); }
		}

		public static EvaSettings SmcEvaSettings
		{
			get
			{
				return new EvaSettings
				{
					BaseUri = GetSetting("SmcEvaSettingBaseUri")
				};
			}
		}

		public static string SmtpNetworkHost
		{
			get { return GetSetting("smtpNetworkHost"); }
		}

		public static string SmtpPort
		{
			get { return GetSetting("smtpPort"); }
		}


		public static bool SubstituteParameters
		{
			get { return GetSetting("SubstituteParameters").ToBoolean(); }
		}

		public static string SuperUserPassword
		{
			get { return GetSetting("SuperUserPassword"); }
		}

		public static string SuperUserUsername
		{
			get { return GetSetting("SuperUserUsername"); }
		}


		public static string TempFolder
		{
			get { return BaseFilePath + GetSetting("TempFolder"); }
		}

        public static string UserGuidesFolder
        {
            get { return BaseFilePath + GetSetting("UserGuidesFolder"); }
        }


		// must set at application init!
		public static string Version { get; set; }


		public static string ClaimFolder(long tenantId, long claimId)
		{
			return string.Format("{0}{1}{2}/", TenantFolder(tenantId), GetSetting("ClaimFolder"), claimId);
		}

		public static string CustomerFolder(long tenantId, long customerId)
		{
			return string.Format("{0}{1}{2}/", TenantFolder(tenantId), GetSetting("CustomerFolder"), customerId);
		}

		public static string CustomerCommunicationFolder(long tenantId, long customerCommunicationId)
		{
			return string.Format("{0}{1}{2}/", TenantFolder(tenantId), GetSetting("CustomerCommunicationFolder"),
								 customerCommunicationId);
		}

		public static string DocumentTemplateFolder(long tenantId)
		{
			return string.Format("{0}{1}", TenantFolder(tenantId), GetSetting("DocumentTemplateFolder"));
		}

		public static string TenantFolder(long tenantId)
		{
			return string.Format("{0}{1}{2}/", BaseFilePath, GetSetting("TenantFolder"), tenantId);
		}

		public static string TierFolder(long tenantId, long tierId)
		{
			return string.Format("{0}{1}{2}/", TenantFolder(tenantId), GetSetting("TierFolder"), tierId);
		}

		public static string ServiceTicketFolder(long tenantId, long ticketId)
		{
			return string.Format("{0}{1}{2}/", TenantFolder(tenantId), GetSetting("ServiceTicketFolder"), ticketId);
		}

		public static string ShipmentFolder(long tenantId, long shipmentId)
		{
			return string.Format("{0}{1}{2}/", TenantFolder(tenantId), GetSetting("ShipmentFolder"), shipmentId);
		}

        public static string JobFolder(long tenantId, long jobId)
        {
            return string.Format("{0}{1}{2}/", TenantFolder(tenantId), GetSetting("JobFolder"), jobId);
        }

        public static string LoadOrderFolder(long tenantId, long shipmentId)
        {
            return string.Format("{0}{1}{2}/", TenantFolder(tenantId), GetSetting("LoadOrderFolder"), shipmentId);
        }

		public static string VendorFolder(long tenantId, long vendorId)
		{
			return string.Format("{0}{1}{2}/", TenantFolder(tenantId), GetSetting("VendorFolder"), vendorId);
		}

		public static string PendingVendorFolder(long tenantId, long pendingVendorId)
		{
			return string.Format("{0}{1}{2}/", TenantFolder(tenantId), GetSetting("PendingVendorFolder"), pendingVendorId);
		}

		public static string VendorCommunicationFolder(long tenantId, long vendorCommunicationId)
		{
			return string.Format("{0}{1}{2}/", TenantFolder(tenantId), GetSetting("VendorCommunicationFolder"),
								 vendorCommunicationId);
		}

		public static string UploadedImagesFolder(long tenantId)
		{
			return string.Format("{0}{1}", TenantFolder(tenantId), GetSetting("UploadedImagesFolder"));
		}



		public static string GetTypeName(MileageEngine engine)
		{
			return GetSetting(string.Format("{0}MileageEngine", engine.ToString()));
		}

	    public static int DefaultBackgroundReportRunExpirationHours
	    {
	        get { return GetSetting("DefaultBackgroundReportRunExpirationHours").ToInt(); }
	    }

	    public static int MaxCellThresholdForBackgroundReports
	    {
	        get { return GetSetting("MaxCellThresholdForBackgroundReports").ToInt(); }
	    }

	    public static bool EnableBackgroundReportRunProcessor
	    {
	        get { return GetSetting("EnableBackgroundReportRunProcessor").ToBoolean(); }
	    }



        private static string GetSetting(string key)
		{
            if(ConfigurationManager.AppSettings[key] == null)  
                throw new Exception(string.Format("Web Application Setting '{0}' does not exist in the web.config", key));
			return ConfigurationManager.AppSettings[key];
		}

		private static string GetConnectionString(string name)
		{
			return ConfigurationManager.ConnectionStrings[name].ConnectionString;
		}
	}
}
