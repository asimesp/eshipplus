Service Code	Markup Percent	Markup Value	Service Charge Ceiling	Service Charge Floor	Effective Date	Use Minimum	Active
See Notes #5	Decimal Value	Decimal Value	Decimal Value	Decimal Value	See Notes #6	'True' or 'False'	'True' or 'False'
							
Notes:
1. Input file must have a header row.
2. Input file can be Excel (extension xlsx) or delimited file (extension txt)
3. Acceptable file delimiters are tab, comma, semicolon and colon
4. If using excel file, input data must be on a worksheet called "data"	
5. Values for the Service Code field must exist and be valid Service Codes										
6. Values for the Effective Date field must in the following Date format: YYYY-MM-DD							

Tip: Excel formatting can be sometimes problematic. If using excel and having trouble upload files, consider formatting all your cells as text