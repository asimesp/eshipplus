Line ID	Origin Postal Code	Origin Country	Destination Postal Code	Destination Country	Weight(lbs)	Length(Inches)	Width(Inches)	Height(Inches)
See Notes #5	See Notes #7	See Notes #8	See Notes #7	See Notes #8	Decimal Value	Decimal Value	Decimal Value	Decimal Value					

Notes:
1. Input file must have a header row.
2. Input file can be Excel (extension xlsx) or delimited file (extension txt)
3. Acceptable file delimiters are tab, comma, semicolon and colon
4. If using excel file, input data must be on a worksheet called "data"
5. Values for Line ID are defined by the user
6. Only 10000 records may be imported at one time
7. Values for the Origin Postal Code and Destination Postal Code field must exist and be valid Postal Codes
8. Values for the Origin Country Code and Destination Country Code must exist and be valid Country Codes

Tip: Excel formatting can be sometimes problematic. If using excel and having trouble upload files, consider formatting all your cells as text