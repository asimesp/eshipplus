Line Type	Group Id	Primary Vendor Number	Customer Number	External Reference 1	External Reference 2	Ticket Date	Item Description	Item Comment	Charge Charge Code	Charge Comment	Charge Quantity	Charge Unit Buy	Charge Unit Sell	Charge Unit Discount	Note Type	Note Archived	Note Classified	Note Message	Service Code	Equipment Code	Audited for Invoicing
See Notes #5	See Notes #6	See Notes #7	See Notes #8	external reference 1	external reference 2	See Notes # 9	Item Description	Item Comment	See Notes #10	Charge Code Comment	Integer	Decimal	Decimal	Decimal	See Notes #11	'True' or 'False'	'True' or 'False'	Note Message	See Notes #13	See Notes #14	See Notes #16

Notes:
1. Input file should include header row																								
2. Input file can be Excel (extension xlsx) or delimited file (extension txt)																									
3. Acceptable file delimiters are tab, comma, semicolon and colon																									
4. If using excel file, input data must be on a worksheet called "data"																									
5. Values for Line Type must be 'Header', 'Item', 'Note', 'Service', 'Equipment' or 'Charge'.
6. The Group Id will link a Header line with it's corresponding Item, Charge, Note, Service, and Equipment Lines
7. Values for the 'Primary Vendor Number' must exist and be valid Vendor numbers
8. Values for the 'Customer Number' must exist and be valid Customer Numbers
9. Values for the 'Ticket Date' field must in the following Date format: YYYY-MM-DD
10. Values for the 'Charge Code' field must exist and be valid Charge Codes
11. Valid values for the 'Note Type' field are as follows:
	0 for Critical
	1 for Normal
	2 for Finance
	3 for Operations
13. Values for the 'Service Code' must exist and be valid Service Codes
14. Values for the 'Equipment Code' must exist and be valid Equipment Codes
15. Notes, Services, and Equipments are not required for import.
16. Values for 'Audited for Invoicing' must be 'True' or 'False'. The import process will assume this is 'False' if you do not provide a value. This column will only be processed when provided on a Header line.

Tip: Excel formatting can be sometimes problematic. If using excel and having trouble upload files, consider formatting all your cells as text

General Information																									
This import process allows both the creation of service tickets within the application. You can import multiple service tickets in one file by specifying different group ids. Each Service Ticket must have at least one charge and at least one item,
however notes, services, and equipments are optional. This import process does not allow the updating of existing service tickets.
The following lines are an example of having multiple service tickets in one import file:

Line Type	Group Id	Primary Vendor Number	Customer Number	External Reference 1	External Reference 2	Ticket Date	Item Description	Item Comment	Charge Charge Code	Charge Comment	Charge Quantity	Charge Unit Buy	Charge Unit Sell	Charge Unit Discount	Note Type	Note Archived	Note Classified	Note Message	Service Code	Equipment Code	Audited for Invoicing
Header	TICKET1	10000	10000	EXREF1	EXREF2	1/1/2017															FALSE
Item	TICKET1						Item 1	This is the first item.													
Item	TICKET1						Item 2	This is the second item.													
Charge	TICKET1								ADDI	This is a charge.	5	59.99	65.99	5							
Equipment	TICKET1																			SEMI	
Service	TICKET1																		IND		
Header	TICKET2	10000	10000	EXREF1	EXREF2	1/1/2017															
Item	TICKET2						Item 1	This is the first item for the second ticket..													
Charge	TICKET2								ADDI	This is a charge for the second ticket.	5	100	20.5	0							
Equipment	TICKET2																			RAIL	
Equipment	TICKET2																			DRYV	
Service	TICKET2																		CUP		
Service	TICKET2																		MSP		
