﻿Vendor Number	Vendor Name	Communication Type
See Notes #5	vendor name	See Notes #6
	
Notes:
1. Input file must have a header row.
2. Input file can be Excel (extension xlsx) or delimited file (extension txt)
3. Acceptable file delimiters are tab, comma, semicolon and colon
4. If using excel file, input data must be on a worksheet called "data"											
5. Values for the Vendor Number field must exist and be valid Vendor Numbers that refer to a Vendor
6. This is communication type with Vendor. This is defaulted to 0.
     a. Enter 0 for EDI (Vendor should have ediEnabled to use this).
     b. Enter 1 for Email.

Tip: Excel formatting can be sometimes problematic. If using excel and having trouble upload files, consider formatting all your cells as text