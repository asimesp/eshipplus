Description	Package Type	Weight	Length	Width	Height	Piece Count	Stackable	Freight Class	NMFC Code	HTS Code	Comment	Hazmat
See Notes #5	See Notes #6	Decimal Value	Decimal Value	Decimal Value	Decimal Value	Integer Value	'True' or 'False'	Decimal See Notes #7	NMFC Code	HTS Code	comments	'True' or 'False'

Notes:
1. Input file must have a header row.
2. Input file can be Excel (extension xlsx) or delimited file (extension txt)
3. Acceptable file delimiters are tab, comma, semicolon and colon
4. If using excel file, input data must be on a worksheet called "data"
5. The Description field may not be left blank
6. Values for the Package Type field must exist and be valid Package Types (Ex: 10KG BOX, ENVELOPE)
7. Valid values for the Applied Freight Class field are as follows:
	50
	55
	60
	65
	70
	77.5
	85
	92.5
	100
	110
	125
	150
	175
	200
	250
	300
	400
	500								

Tip: Excel formatting can be sometimes problematic. If using excel and having trouble upload files, consider formatting all your cells as text