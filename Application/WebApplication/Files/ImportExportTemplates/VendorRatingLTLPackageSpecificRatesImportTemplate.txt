Effective Date	Origin Region	Destination Region	Package Quantity	Flat Rate	Rate Priority
See Notes #5	See Notes #6	See Notes #6	Integer Value	Decimal Value	Integer Value

Notes:
1. Input file must have a header row.
2. Input file can be Excel (extension xlsx) or delimited file (extension txt)
3. Acceptable file delimiters are tab, comma, semicolon and colon
4. If using excel file, input data must be on a worksheet called "data"	
5. Values for the Effective Date field must in the following Date format: YYYY-MM-DD	
6. Values for the Origin Region and Destination Region columns must exist and be valid region names

Tip: Excel formatting can be sometimes problematic. If using excel and having trouble upload files, consider formatting all your cells as text