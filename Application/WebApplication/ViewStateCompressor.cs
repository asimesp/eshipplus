﻿using System;
using System.Globalization;
using System.IO;
using System.Web.UI;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication
{
    public class ViewStateCompressor : PageStatePersister
    {
        public ViewStateCompressor(Page page) : base(page)
        {
        }

        private LosFormatter _stateFormatter;

        protected new LosFormatter StateFormatter
        {
            get { return _stateFormatter ?? (_stateFormatter = new LosFormatter()); }
        }

        public override void Save()
        {
            using (var writer = new StringWriter(CultureInfo.InvariantCulture))
            {
                StateFormatter.Serialize(writer, new Pair(ViewState, ControlState));
                var bytes = Convert.FromBase64String(writer.ToString()).ToCompressedBytes();
                ScriptManager.RegisterHiddenField(Page, "__PIT", Convert.ToBase64String(bytes));
            }
        }

        public override void Load()
        {
            var bytes = Convert.FromBase64String(Page.Request.Form["__PIT"]).ToDecompressedBytes();

            var p = ((Pair)(StateFormatter.Deserialize(Convert.ToBase64String(bytes))));
            ViewState = p.First;
            ControlState = p.Second;
        }
    }
}