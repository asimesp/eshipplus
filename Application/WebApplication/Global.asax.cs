﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using LogisticsPlus.Eship.Core.Cache;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Dat;
using LogisticsPlus.Eship.Extern.Edi.Segments;
using LogisticsPlus.Eship.Fax;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Reports;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Connect;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Project44;
using LogisticsPlus.Eship.WebApplication.Documents;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.Utilities.Miles;
using LogisticsPlus.Eship.WebApplication.Utilities.Que;
using LogisticsPlus.Eship.WebApplication.Utilities.ScheduledTasks;
using ObjToSql.Core;

namespace LogisticsPlus.Eship.WebApplication
{
	public class Global : HttpApplication
	{
		private static bool _runThrough;
		private static Scheduler _scheduler;
		private static XmlTransmissionManager _xmlTransmissionManager;
		private static TruckLoadTenderingProcessor _truckLoadTenderingProcessor;
		private static RateAnalysisProcessor _rateAnalysisProcessor;
		private static FtpDocDeliveryProcessor _ftpDocDeliveryProcessor;
		private static FaxServiceUpdater _faxService;
        private static EmailProcessor _emailProcessor;
	    private static BackgroundReportRunProcessor _backgroundReportRunProcessor;

        private static readonly object LockObject = new object();

		public override void Init()
		{
			base.Init();

			// current version and site root
			WebApplicationSettings.Version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
           
            
			// database path and type setup
            DatabaseConnection.DatabaseType = WebApplicationSettings.DefaultDatabaseType;
			DatabaseConnection.DefaultConnectionString = WebApplicationSettings.DefaultConnectionString;
			DatabaseConnection.SubstituteParameters = WebApplicationSettings.SubstituteParameters;
			DatabaseConnection.DefaultCommandTimeout = WebApplicationSettings.DefaultQueryTimeout;

			// report Engine setup
			ReportEngine.QueryTimeout = WebApplicationSettings.ReportEngineQueryTimeout;

			// error handling
			Error += delegate
						{
							if (Context.AllErrors == null)
								return;

							foreach (var exception in Context.AllErrors)
								ErrorLogger.LogError(exception, Context);
						};

			// setup Tenants stuff & searches & etc
			lock (LockObject)
			{
				if (!_runThrough)
				{
					// clear/set cache(s)
					CoreCache.Initialize();
					ProcessorVars.RaterCache.Clear();
					DocumentProcessor.ClearCache();

					// initial page access logger
					EnvironmentUtilities.PageAccessLogger = NLog.LogManager.GetCurrentClassLogger();

					// load tenant processing setups
					Server.LoadTenantProcessingSetup();

					// mileage plugins
					MileageEngine.LongitudeLatitude.AddOrUpdatePlugin(MileageEngine.LongitudeLatitude.GetPlugin());
					var plugin = MileageEngine.PcMiler25.GetPlugin();
					if (plugin != null)
						if (!plugin.Initialize()) ErrorLogger.LogError(new Exception(plugin.LastErrorMessage), Context);
						else MileageEngine.PcMiler25.AddOrUpdatePlugin(plugin, false);
				}
			}

			// search fields
		    AccountingSearchFields.Initialize();
		    AdminSearchFields.Initialize();
		    BusinessIntelligenceSearchFields.Initialize();
		    ConnectSearchFields.Initialize();
		    CoreSearchFields.Initialize();
		    OperationsSearchFields.Initialize();
		    RegistrySearchFields.Initialize();
		    RatingSearchFields.Initialize();

            // report que
            ReportQueue.Initialize();

			// set pdf creator path
			WkHtmlToPdf.NET.WkHtmlToPdf.WkHtmlToPdfExecutablePath = Server.MapPath(WebApplicationSettings.PdfToHtmlConverterPath);

			// check temp and error folder path
			ProcessorVars.TempFolder = Server.MapPath(WebApplicationSettings.TempFolder);
			if (!Directory.Exists(ProcessorVars.TempFolder)) Directory.CreateDirectory(ProcessorVars.TempFolder);
			ProcessorVars.ErrorFolder = Server.MapPath(WebApplicationSettings.ErrorsFolder);
			if (!Directory.Exists(ProcessorVars.ErrorFolder)) Directory.CreateDirectory(ProcessorVars.ErrorFolder);

            // set EDI Status codes
		    ProcessorVars.EdiStatusCodeFinalDelivery = ShipStatMsgCode.D1.GetString();
		    ProcessorVars.EdiStatusCodeActualPickup = ShipStatMsgCode.AF.GetString();
		    ProcessorVars.EdiStatusCodeDeliveryAppointment = ShipStatMsgCode.AB.GetString();
		    ProcessorVars.EdiStatusCodePickupAppointment = ShipStatMsgCode.AA.GetString();
		    ProcessorVars.EdiStatusCodeEstimatedDelivery = ShipStatMsgCode.AG.GetString();

            // schedulers/managers/processors etc ...mo
            // Task schedule setup
            if (WebApplicationSettings.EnableScheduler && _scheduler == null)
            {
                var gc = new GarbageCleaner(ProcessorVars.ErrorFolder, ProcessorVars.TempFolder, Server.MapPath(WebApplicationSettings.ApplicationLogsFolder), Server.MapPath(WebApplicationSettings.BackgroundReportRunFolder));
                var sr = new ScheduledReports(HttpContext.Current.Server);
                var rasr = new RateAnalysisSetRetriever();
                var fedEx = new FedExTracker(ProcessorVars.ErrorFolder);
                var imgRtrv = new ImgRtrvProcessor(HttpContext.Current.Server, ProcessorVars.ErrorFolder);
                var clmReminder = new ClaimNoteReminder(HttpContext.Current.Server);
                _scheduler = new Scheduler(ProcessorVars.ErrorFolder);
                _scheduler.AddIntervalTask("GarbageCleaner", gc, 30);
                _scheduler.AddIntervalTask("ScheduledReports", sr, 15);
                _scheduler.AddIntervalTask("RateAnalysisSetRetriever", rasr, 60);
                _scheduler.AddIntervalTask("FedExTracker", fedEx, 60);
                _scheduler.AddIntervalTask("ImgRtrvProcessor", imgRtrv, 60);
                _scheduler.AddIntervalTask("ClaimNoteReminder", clmReminder, 15);
                _scheduler.Start();
            }

            // xml transmission manager
            if (WebApplicationSettings.EnableXmlTransmissionManager && _xmlTransmissionManager == null)
			{
				_xmlTransmissionManager = new XmlTransmissionManager(ProcessorVars.ErrorFolder, HttpContext.Current.Server);
				_xmlTransmissionManager.Start();
			}

			// xml transmission manager
			if (WebApplicationSettings.EnableTruckloadTenderingProcessor && _truckLoadTenderingProcessor == null)
			{
				_truckLoadTenderingProcessor = new TruckLoadTenderingProcessor(ProcessorVars.ErrorFolder, HttpContext.Current.Server);
				_truckLoadTenderingProcessor.Start();
			}

			// rate analysis processor
			if (WebApplicationSettings.EnableRateAnalysisProcessor && _rateAnalysisProcessor == null)
			{
			    var p44Settings = new Project44ServiceSettings
			    {
			        Hostname = WebApplicationSettings.Project44Host,
			        Username = WebApplicationSettings.Project44Username,
			        Password = WebApplicationSettings.Project44Password,
			        PrimaryLocationId = WebApplicationSettings.Project44PrimaryLocationId
			    };

                _rateAnalysisProcessor = new RateAnalysisProcessor(ProcessorVars.ErrorFolder, HttpContext.Current.Server, p44Settings);
				_rateAnalysisProcessor.Start();
			}

			// doc delivery processor
			if(WebApplicationSettings.EnableDocDeliveryProcessor && _ftpDocDeliveryProcessor == null)
			{
				_ftpDocDeliveryProcessor = new FtpDocDeliveryProcessor(HttpContext.Current.Server);
				_ftpDocDeliveryProcessor.Start();
			}

			// fax service
			if (WebApplicationSettings.FaxServiceEnabled && _faxService == null)
			{
				// Assign Fax Handler
				ProcessorVars.FaxHandler = new FaxHandler(WebApplicationSettings.FaxSettings);

				_faxService = new FaxServiceUpdater(Server, ProcessorVars.FaxHandler);
				_faxService.Start();
			}

            // email processor
            if (WebApplicationSettings.EnableEmailProcessor && _emailProcessor == null)
            {
                _emailProcessor = new EmailProcessor(ProcessorVars.ErrorFolder);
                _emailProcessor.Start();
            }

		    // Background Report Run Processor
		    if (WebApplicationSettings.EnableBackgroundReportRunProcessor && _backgroundReportRunProcessor == null)
		    {
		        BackgroundReportQueue.Initialize(); // initial queue with records from database

		        _backgroundReportRunProcessor = new BackgroundReportRunProcessor(ProcessorVars.ErrorFolder, HttpContext.Current.Server);
		        _backgroundReportRunProcessor.Start();
		    }


            Connexion.Initialize(WebApplicationSettings.DatLoadboardHttpUrl, WebApplicationSettings.DatLoadboardHttpsUrl);

			// set run through
			_runThrough = true;
		}

		protected void Application_Start(object sender, EventArgs e)
		{

		}

		protected void Session_Start(object sender, EventArgs e)
		{

		}

		protected void Application_BeginRequest(object sender, EventArgs e)
		{
            // set site root here
		    WebApplicationSettings.SiteRoot = Context.Request.ResolveSiteRootWithHttp();
        }

		protected void Application_AuthenticateRequest(object sender, EventArgs e)
		{

		}

		protected void Application_Error(object sender, EventArgs e)
		{

		}

		protected void Session_End(object sender, EventArgs e)
		{

		}

		protected void Application_End(object sender, EventArgs e)
		{
			// shutdown processors, managers, and schedulers
			if (_scheduler != null)
			{
				_scheduler.Shutdown();
				_scheduler.PingServer();
			}

		    if (_emailProcessor != null) _emailProcessor.Shutdown();
            if (_xmlTransmissionManager != null) _xmlTransmissionManager.Shutdown();
		    if (_rateAnalysisProcessor != null) _rateAnalysisProcessor.Shutdown();
		    if (_ftpDocDeliveryProcessor != null) _ftpDocDeliveryProcessor.Shutdown();
		    if (_faxService != null) _faxService.Shutdown();
		    if (_truckLoadTenderingProcessor != null) _truckLoadTenderingProcessor.Shutdown();
		    if (_backgroundReportRunProcessor != null) _backgroundReportRunProcessor.Shutdown();

            //dispose mileage engines
            foreach (var plugin in ProcessorVars.MileagePlugins.Values.Where(plugin => plugin != null))
				try
				{
					plugin.Dispose();
				}
				catch (Exception ex)
				{
					ErrorLogger.LogError(ex, Context);
				}
		}
	}
}