﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LogisticsPlus.Eship.WebApplication
{
	public partial class ReportBase : System.Web.UI.MasterPage
	{
		private void LoadJavaScript()
		{
			Page.ClientScript.RegisterClientScriptInclude("jquery.js", ResolveUrl("~/Scripts/jquery-1.9.1.js"));
			Page.ClientScript.RegisterClientScriptInclude("jquery.intellisense.js", ResolveUrl("~/Scripts/jquery-1.9.1.intellisense.js"));
			Page.ClientScript.RegisterClientScriptInclude("bootstrap.js", ResolveUrl("~/Scripts/bootstrap.js"));
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			LoadJavaScript();
		}
	}
}