﻿namespace LogisticsPlus.Eship.WebApplication.ViewCodes
{
	public enum SectionCodes
	{
		AboutUsLogisticsPlus,
		AboutUsPartners,
		AboutUsEshipPlus,
		News,
		ContactUs,
		FreightClaims,
		FreightClass,
		SeeHowItWorks,
		TermsAndConditions,
	}
}
