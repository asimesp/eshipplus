﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Core;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Core;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members
{
    public partial class EmailLogView : MemberPageBase, IEmailLogView
    {
        public static string PageAddress
        {
            get { return "~/Members/EmailLogView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.EmailLog; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.UtilitiesBlue; }
        }


        public event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;


        public void DisplaySearchResult(List<EmailLogViewSearchDto> emailLogs)
        {
            cseDataUpdate.DataSource = emailLogs;
            cseDataUpdate.DataBind();
            litRecordCount.Text = emailLogs.BuildRecordCount();
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak,
                                             messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems { Columns = GetCurrentRunParameters(true) };
        }

        
        private void DoSearch(List<ParameterColumn> criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<List<ParameterColumn>>(criteria));
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new EmailLogHandler(this);
            handler.Initialize();

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);

            var columns = profile != null && profile.Columns != null
                              ? profile.Columns
                              : CoreSearchFields.DefaultEmailLogs.Select(f => f.ToParameterColumn()).ToList();

            lstFilterParameters.DataSource = columns;
            lstFilterParameters.DataBind();
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            cseDataUpdate.ResetLoadCount();
            DoSearch(GetCurrentRunParameters(false));
        }

        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items.Select(i =>((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }


        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = CoreSearchFields.EmailLogs.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);

        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();
        }

        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(CoreSearchFields.EmailLogs);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();
        }

        protected void OnSearchProfilesAutoRefreshChanged(object sender, ViewEventArgs<int> e)
        {
            tmRefresh.Enabled = e.Argument > default(int);
            if (e.Argument > 0) tmRefresh.Interval = e.Argument;
        }

        protected void OnTimerTick(object sender, EventArgs e)
        {
            DoSearch(GetCurrentRunParameters(false));
        }


        protected void OnResendEmailClicked(object sender, ImageClickEventArgs e)
        {
            var emailLog = new EmailLog(((Control) sender).Parent.FindControl("hidEmailLogId").ToCustomHiddenField().Value.ToLong());

            hidEmailLogId.Value = emailLog.Id.ToString();
            txtTo.Text = emailLog.Tos;
            txtCc.Text = emailLog.Ccs;
            txtBcc.Text = emailLog.Bbcs;
            txtSubject.Text = emailLog.Subject;

            ifrEmailBody.Src = DocumentViewer.GenerateEmailLogLink(emailLog); 

            rptAttachments.DataSource = emailLog.EmailLogAttachments;
            rptAttachments.DataBind();
            
            pnlDimScreen.Visible = true;
            pnlResendEmail.Visible = true;
        }

        protected void OnDownloadAttachmentClicked(object sender, EventArgs e)
        {
            var attachmentId = ((Control)sender).Parent.FindControl("hidAttachmentId").ToCustomHiddenField().Value.ToLong();
            var attachment = new EmailLogAttachment(attachmentId);

            Response.Export(attachment.AttachmentBytes, attachment.Name);
        }
        
        protected void OnResendEmailResendClicked(object sender, EventArgs e)
        {
            var emailLog = new EmailLog(hidEmailLogId.Value.ToLong());

            var toEmails = emailLog.Tos.Split(WebApplicationUtilities.ImportSeperators(), StringSplitOptions.RemoveEmptyEntries);

            var attachments = emailLog.EmailLogAttachments.Select(a => new Attachment(new MemoryStream(a.AttachmentBytes), a.Name));

            toEmails.SendEmail(emailLog.Ccs.Split(WebApplicationUtilities.ImportSeperators(), StringSplitOptions.RemoveEmptyEntries),
                               emailLog.Bbcs.Split(WebApplicationUtilities.ImportSeperators(), StringSplitOptions.RemoveEmptyEntries), emailLog.Body,
                               emailLog.Subject, attachments, emailLog.TenantId);

            DisplayMessages(new []{ValidationMessage.Information("Email has been resent successfully")});
        }

        protected void OnCloseClicked(object sender, EventArgs eventArgs)
        {
            pnlResendEmail.Visible = false;
            pnlDimScreen.Visible = false;
        }
    }
}