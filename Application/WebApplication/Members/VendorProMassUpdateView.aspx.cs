﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Handlers.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Core;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members
{
    public partial class VendorProMassUpdateView : MemberPageBase, IVendorProMassUpdateView
    {
        public static string PageAddress { get { return "~/Members/VendorProMassUpdateView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.VendorProMassUpdate; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.UtilitiesBlue; } }


        public event EventHandler<ViewEventArgs<List<string[]>>> Save;
        public event EventHandler<ViewEventArgs<List<string[]>>> OverrideSave;

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join("<br />", messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void DisplayUpdatedRecords(List<string[]> records)
        {
            var data = records
                .Where(r => r.Length >= 4)
                .Select(r => new
                {
                    ShipmentNumber = r[0],
                    VendorScac = r[1],
                    VendorNumber = r[2],
                    VendorPro = r[3],
                    Pickup = r[4],
                    Delivery = r[5],
                    Message = r[6],
                })
                .ToList();

            lstUpdatedRecords.DataSource = data;
            lstUpdatedRecords.DataBind();
        }

        public void LogShipmentForDocImgRtrv(Shipment shipment)
        {
            // check for document retrieval log
            shipment.InsertShipmentDocRtrvLogFor(ActiveUser);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new VendorProMassUpdateHandler(this).Initialize();

            if (IsPostBack) return;
            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.VendorProMassUpdateImportTemplate });
        }


        protected void OnImportShipmentToUpdate(object sender, EventArgs e)
        {
            fileUploader.Title = "VENDOR PRO MASS UPDATE IMPORT";
            fileUploader.Instructions = @"**Please refer to user guide for import file format";
            fileUploader.Visible = true;
        }


        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }


            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 6;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            if (lines.Count > 5000)
            {
                var message = new[] { ValidationMessage.Error("Mass update is limited to 5000 shipments, please adjust your input file") };
                DisplayMessages(message);
            }

            if (chkOveride.Checked)
            {
                if (OverrideSave != null)
                    OverrideSave(this, new ViewEventArgs<List<string[]>>(lines));
            }
            else
            {
                if (Save != null)
                    Save(this, new ViewEventArgs<List<string[]>>(lines));
            }

            fileUploader.Visible = false;
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }


        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;
        }
    }
}
