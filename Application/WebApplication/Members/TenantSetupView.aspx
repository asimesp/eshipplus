﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="TenantSetupView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.TenantSetupView" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        ShowEdit="true" ShowDelete="true" ShowSave="true" ShowFind="true" OnFind="OnFindClicked"
        OnSave="OnSaveClicked" OnNew="OnNewClicked" OnDelete="OnDeleteClicked" OnEdit="OnEditClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" ImageUrl="" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Tenant Setups
                <span class="pageHeaderRecDesc" id="spIdentityDisplay">
                    <asp:Literal runat="server" ID="litRecordIdentity" />
                </span>
            </h3>
        </div>

        <hr class="dark mb5" />

        <eShip:TenantFinderControl ID="tenantFinder" runat="server" Visible="false" OnItemSelected="OnTenantFinderItemSelected"
            OnSelectionCancel="OnTenantFinderSelectionCancelled" />
        <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
            OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
        <eShip:CustomHiddenField runat="server" ID="hidTenantId" />

        <ajax:TabContainer ID="tabTenantAdministration" runat="server" CssClass="ajaxCustom">
            <ajax:TabPanel runat="server" ID="tabDetails" HeaderText="Details">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlDetails">
                        <div class="rowgroup">
                            <div class="row">
                                <div class="fieldgroup">
                                    <label class="wlabel">Code</label>
                                    <eShip:CustomTextBox ID="txtCode" runat="server" CssClass="w300" MaxLength="50"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="fieldgroup">
                                    <label class="wlabel">Name</label>
                                    <script type="text/javascript" language="javascript">
                                        function SetNameInTitle() {
                                            $('#spIdentityDisplay').text('- [' + $('#<%= txtName.ClientID %>').val() + ']');
                                        }
                                    </script>
                                    <eShip:CustomTextBox ID="txtName" runat="server" CssClass="w300" onchange="SetNameInTitle();" MaxLength="50"/>
                                </div>
                            </div>
                            <div class="row mt10">
                                <div class="fieldgroup">
                                    <asp:CheckBox runat="server" ID="chkActive" CssClass="jQueryUniform" />
                                    <label>Active</label>
                                </div>
                            </div>
                            <div class="row mt10">
                                <div class="fieldgroup" id="customerLoginPageSection">
                                    <script type="text/javascript">
                                        $(document).ready(function () {
                                            // enforce mutual exclusion on checkboxes in customerLoginPageSection
                                            $(jsHelper.AddHashTag('customerLoginPageSection input:checkbox')).click(function () {
                                                if (jsHelper.IsChecked($(this).attr('id'))) {
                                                    $(jsHelper.AddHashTag('customerLoginPageSection input:checkbox')).not(jsHelper.AddHashTag($(this).attr('id'))).each(function () {
                                                        jsHelper.UnCheckBox($(this).attr('id'));
                                                    });
                                                    $.uniform.update();
                                                }
                                            });
                                        });
                                    </script>
                                    <label class="wlabel">Custom Login Page</label>
                                    <asp:CheckBox runat="server" ID="chkWithoutCustomTemplate" Text=" With default template"
                                        Checked="True" CssClass="jQueryUniform"/>
                                    <asp:CheckBox runat="server" ID="chkWithCustomTemplate" Text=" With custom template" 
                                        CssClass="jQueryUniform ml10"/>
                                </div>
                            </div>
                            <div class="row mt10">
                                <div class="fieldgroup">
                                    <asp:Button runat="server" ID="btnGenerateCustomLoginFolder" CausesValidation="False"
                                        Text="Generate Custom Login Page" OnClick="OnGenerateCustomLoginFolderClicked" />
                                    <asp:Button runat="server" ID="btnClearCustomLogin" CausesValidation="False" Text="Clear Custom Login Page"
                                        OnClick="OnClearCustomLoginFolderClicked" />
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:HyperLink runat="server" ID="hypCustomLogin" Visible="False" Target="_blank"
                        Text="Preview Login Page" />
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabSeeds" HeaderText="Seeds">
                <ContentTemplate>
                    <asp:CheckBox ID="chkRetrieveSeeds" runat="server" Checked="false" AutoPostBack="true"
                        OnCheckedChanged="OnRetrieveSeedsCheckedChanged" CssClass="jQueryUniform" />
                    <label>Update Seeds</label>
                    <br/><br class="mt10"/>
                    <asp:Panel runat="server" ID="pnlSeeds" Enabled="false">
                        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheTenantSeedTable" TableId="tenantSeedTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                        <asp:ListView ID="lstSeeds" runat="server" ItemPlaceholderID="itemPlaceHolder">
                            <LayoutTemplate>
                                <table class="stripe" id="tenantSeedTable">
                                    <tr>
                                        <th style="width: 30%;" class="middle">Name
                                        </th>
                                        <th style="width: 70%;" class="middle">Value
                                        </th>
                                    </tr>
                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                </table>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="text-left">
                                        <eShip:CustomHiddenField runat="server" ID="hidAutoNumberCode" Value='<%# Eval("Code") %>' />
                                        <%# Eval("Code").FormattedString() %>:
                                    </td>
                                    <td class="text-left">
                                        <eShip:CustomTextBox runat="server" ID="txtSeedValue" Text='<%# Eval("NextNumber") %>'  MaxLength="50"/>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
        </ajax:TabContainer>
        <script language="javascript" type="text/javascript">
            function ClearHidFlag() {
                jsHelper.SetEmpty('<%= hidFlag.ClientID %>');
            }
        </script>
    </div>
    <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
        Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" />
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
    <eShip:CustomHiddenField runat="server" ID="hidDeleteFolder" />
</asp:Content>
