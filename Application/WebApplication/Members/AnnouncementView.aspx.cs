﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Core;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Core;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members
{
    public partial class AnnouncementView : MemberPageBase, IAnnouncementView
    {
        public static string PageAddress
        {
            get { return "~/Members/AnnouncementView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.Announcement; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.UtilitiesBlue; }
        }

        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public Dictionary<int, string> AnnouncementTypes
        {
            set
            {
                var searchAnnouncementTypes = value.ToList();

                searchAnnouncementTypes.Insert(0, new KeyValuePair<int, string>(-1, WebApplicationConstants.NotApplicable));

                ddlAnnouncementTypes.DataSource = value ?? new Dictionary<int, string>();
                ddlAnnouncementTypes.DataBind();
            }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<Announcement>> Save;
        public event EventHandler<ViewEventArgs<Announcement>> Delete;
        public event EventHandler<ViewEventArgs<AnnouncementViewSearchCriteria>> Search;
        public event EventHandler<ViewEventArgs<Announcement>> Lock;
        public event EventHandler<ViewEventArgs<Announcement>> UnLock;

        public void FaildedLock(Lock @lock)
        {
            btnSave.Enabled = false;
            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void DisplaySearchResult(List<Announcement> announcements)
        {
            lvwRegistry.DataSource = announcements.OrderBy(a => a.Name);
            lvwRegistry.DataBind();
            litRecordCount.Text = announcements.BuildRecordCount();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() || messages.HasWarnings();

            if (CloseEditPopUp)
            {

                //  update announcements list
                ActiveUser.Tenant.UpdateTenantAnnouncements();
            }

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Button = MessageButton.Ok;
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }


        private void GeneratePopup(string title)
        {
            litTitle.Text = title;

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = pnlEdit.Visible;

            txtName.Focus();
        }

        private void CloseEditPopup()
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<Announcement>(new Announcement(hidAnnouncementId.Value.ToLong(), false)));
        }

        private void ClearFields()
        {
            txtName.Text = string.Empty;
            txtMessage.Text = string.Empty;
            chkEnabled.Checked = false;
        }

        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems { Columns = GetCurrentRunParameters(true) };
        }

        private void DoSearchPreProcessingThenSearch()
        {
            var criteria = new AnnouncementViewSearchCriteria
            {
                Parameters = GetCurrentRunParameters(false)
            };
            
            DoSearch(criteria);
        }

        private void DoSearch(AnnouncementViewSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<AnnouncementViewSearchCriteria>(criteria));
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new AnnouncementHandler(this).Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowNew = Access.Modify;

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);

            var columns = profile != null && profile.Columns != null
                            ? profile.Columns
                            : CoreSearchFields.Announcements.Select(f => f.ToParameterColumn()).ToList();

            lstFilterParameters.DataSource = columns;
            lstFilterParameters.DataBind();

            if (Loading != null)
                Loading(this, new EventArgs());
        }


        protected void OnNewClick(object sender, EventArgs e)
        {
            ClearFields();
            hidAnnouncementId.Value = string.Empty;
            GeneratePopup("Add Announcement");
        }

        protected void OnSearchClicked(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch();
        }

        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            ClearFields();
            GeneratePopup("Modify Announcement");

            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("AnnouncementId").ToCustomHiddenField();

            var announcement = new Announcement(hidden.Value.ToLong(), false);

            hidAnnouncementId.Value = announcement.Id.ToString();
            txtName.Text = announcement.Name;
            txtMessage.Text = announcement.Message;
            chkEnabled.Checked = announcement.Enabled;
            ddlAnnouncementTypes.SelectedValue = ((int)announcement.Type).ToString();

            if (Lock != null)
                Lock(this, new ViewEventArgs<Announcement>(announcement));
        }

        protected void OnCloseClicked(object sender, object e)
        {
            CloseEditPopup();
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            var announcementId = hidAnnouncementId.Value.ToLong();

            var announcement = new Announcement(announcementId, announcementId != default(long))
            {

                Name = txtName.Text,
                Type = ddlAnnouncementTypes.SelectedValue.ToEnum<AnnouncementType>(),
                Message = txtMessage.Text,
                Enabled = chkEnabled.Checked,
            };

            if (announcement.IsNew) announcement.TenantId = ActiveUser.TenantId;

            if (Save != null)
                Save(this, new ViewEventArgs<Announcement>(announcement));

            DoSearchPreProcessingThenSearch();
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("announcementId").ToCustomHiddenField();

            if (Delete != null)
                Delete(this, new ViewEventArgs<Announcement>(new Announcement(hidden.Value.ToLong(), true)));

            DoSearchPreProcessingThenSearch();
        }


        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (CloseEditPopUp)
                CloseEditPopup();
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = CoreSearchFields.Announcements.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();
        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(CoreSearchFields.Announcements);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }




        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();

            DoSearchPreProcessingThenSearch();
        }
    }
}
