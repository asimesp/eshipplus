﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="ApplicationErrorListingView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.ApplicationErrorListingView" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Application Error Listing<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>

        <hr class="fat" />

        <div class="rowgroup mb0">
            <div class="row mb10">
                <div class="fieldgroup">
                    <label class="upper blue">PURGE LOGS EARLIER THAN:</label>
                    <eShip:CustomTextBox runat="server" ID="txtCutOffDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                    <asp:Button ID="btnPurge" runat="server" Text="PURGE" OnClick="OnPurgeClicked"
                        CausesValidation="true" />
                </div>
            </div>
        </div>
        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheErrorLogListingTable" TableId="errorLogListingTable" HeaderZIndex="2"/>
        <asp:ListView ID="lvwErrorLog" runat="server" ItemPlaceholderID="itemPlaceHolder">
            <LayoutTemplate>
                <table id="errorLogListingTable" class="line2 pl2">
                    <tr>
                        <th style="width: 75%">File Name
                        </th>
                        <th style="width: 25%">Date Created
                        </th>
                    </tr>
                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# Eval("Path") %>' Target="_blank" CssClass="blue"><%# Eval("FileName") %></asp:HyperLink>
                    </td>
                    <td>
                        <%# Eval("DateCreated") %>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
    </div>
</asp:Content>
