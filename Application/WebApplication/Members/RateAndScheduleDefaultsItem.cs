﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Extern.Ws;

namespace LogisticsPlus.Eship.WebApplication.Members
{
    [Serializable]
    public class RateAndScheduleDefaultsItem
    {
        public string Name { get; set; }
        public bool IsCurrentDefault { get; set; }
        public RateAndScheduleProfileDataDto Shipment { get; set; }
    }
}