﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="DocDeliveryLogView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Connect.DocDeliveryLogView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Document Delivery Logs
                <small class="ml10">
                    <asp:Literal runat="server" ID="litRecordCount" Text="" /></small>
            </h3>
        </div>
        <span class="clearfix"></span>
        <hr class="dark mb10" />

        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb0">
                <div class="row mb10">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="DocumentDeliveryLogs"
                            ShowAutoRefresh="False" OnProcessingError="OnSearchProfilesProcessingError" OnSearchProfileSelected="OnSearchProfilesProfileSelected" />
                    </div>
                </div>
            </div>

            <hr class="dark mb10 mt0" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                        OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>
            <hr class="fat" />
            <div class="row mb10">
                <div class="fieldgroup">
                    <asp:Button ID="btnSearch" CssClass="mr10" runat="server" Text="SEARCH" OnClick="OnSearchClicked" CausesValidation="False" />
                    <asp:Button ID="btnExport" runat="server" Text="EXPORT" OnClick="OnExportClicked" CausesValidation="False" />
                </div>
                <div class="fieldgroup right pt5">
                    <p class="note">&#8226; Flags: U = User, E = Error *** hover over letter for additional info</p>
                </div>
            </div>
        </asp:Panel>
        <asp:UpdatePanel runat="server" ID="upDataUpdate">
            <ContentTemplate>
                <div class="rowgroup">
                    <eShip:ContinuousScrollExtender ID="upcseDataUpdate" runat="server" BindableControlId="rptDocDeliveryLogs" UpdatePanelToExtendId="upDataUpdate" />
                    <eShip:TableFreezeHeaderExtender runat="server" ID="tfheDocDeliveryTable" TableId="docDeliveryTable" HeaderZIndex="2"/>
                    <table id="docDeliveryTable" class="line2 pl2">
                         <tr>
                                    <th style="width: 15%;" class="text-left">Entity Type
                                    </th>
                                    <th style="width: 25%;" class="text-left">Name
                                    </th>
                                    <th style="width: 15%;" class="text-left">Log Date
                                    </th>
                                    <th style="width: 10%;" class="text-left">
                                        <abbr title="Document Tag Code">
                                            Doc. Tag</abbr>
                                    </th>
                                    <th style="width: 22%;" class="text-left">
                                        <abbr title="Document Tag Description">
                                            Doc. Tag Desc.</abbr>
                                    </th>
                                    <th style="width: 8%;" class="text-center">Flags
                                    </th>
                                    <th style="width: 5%;" class="text-center">Action
                                    </th>
                                </tr>
                        <asp:Repeater ID="rptDocDeliveryLogs" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <%# Eval("EntityType").FormattedString()%>
                                        <eShip:CustomHiddenField runat="server" ID="hidEntityType" Value='<%# Eval("EntityType") %>' />
                                        <eShip:CustomHiddenField runat="server" ID="hidEntityId" Value='<%# Eval("EntityId") %>' />
                                    </td>
                                    <td class="text-left">
                                        <%# Eval("DocumentName")%>
                                    </td>
                                    <td class="text-left">
                                        <%# Eval("LogDateTime")%>
                                    </td>
                                    <td class="text-left">
                                        <%# Eval("DocumentTagCode")%>
                                    </td>
                                    <td class="text-left">
                                        <%# Eval("DocumentTagDescription")%>
                                    </td>
                                    <td class="text-center">
                                        <div title='<%# Eval("FailedDeliveryMessage") %>' class='red flag right <%# Eval("DeliveryWasSuccessful").ToBoolean() ?" hidden" : string.Empty %>'>
                                            <%# Eval("DeliveryWasSuccessful").ToBoolean() ? string.Empty : "E" %>
                                        </div>
                                        <div title='<%# string.Format("{0}: {1} {2}", Eval("Username"), Eval("UserFirstName"), Eval("UserLastName")) %>' class="flag right">
                                            U
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <asp:ImageButton ID="ibtnRedeliver" ToolTip="Redeliver" runat="server" ImageUrl="~/images/icons2/redeliver.png"
                                            CausesValidation="false" OnClick="OnRedeliverClicked" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="rptDocDeliveryLogs" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
        Button="Ok" Icon="Information" />
</asp:Content>
