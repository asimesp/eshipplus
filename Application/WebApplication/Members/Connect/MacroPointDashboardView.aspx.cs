﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Handlers.Connect;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Connect;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Connect;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Connect
{
    public partial class MacroPointDashboardView : MemberPageBase, IMacroPointDashboardView
    {
        private const string ExportFlag = "ExportFlag";

        private string ResultFlag
        {
            get { return ViewState["ResultFlag"].GetString(); }
            set { ViewState["ResultFlag"] = value; }
        }

        private SearchField SortByField
        {
            get { return ConnectSearchFields.MacroPointSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue); }
        }


        public override ViewCode PageCode { get { return ViewCode.MacroPointDashboard; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.ConnectBlue; } }

        public static string PageAddress { get { return "~/Members/Connect/MacroPointDashboardView.aspx"; } }

        public event EventHandler<ViewEventArgs<MacroPointOrderSearchCriteria>> Search;


        public void DisplaySearchResult(List<MacroPointOrderViewSearchDto> mpOrders)
        {
            switch (ResultFlag)
            {
                case ExportFlag:
                    DoExport(mpOrders);
                    break;
                default:
                    litRecordCount.Text = mpOrders.BuildRecordCount();
                    upcseDataUpdate.DataSource = mpOrders;
                    upcseDataUpdate.DataBind();
                    break;
            }
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        
        private void DoExport(IEnumerable<MacroPointOrderViewSearchDto> mpOrders)
        {
            var q = mpOrders.Select(a => a.ToString()).ToList();
            q.Insert(0, MacroPointOrderViewSearchDto.Header());
            Response.Export(q.ToArray().NewLineJoin());
        }


        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems
            {
                Columns = GetCurrentRunParameters(true),
                SortAscending = rbAsc.Checked,
                SortBy = SortByField,
            };
        }

        private void DoSearchPreProcessingThenSearch(SearchField field)
        {
            DoSearch(new MacroPointOrderSearchCriteria
            {
                Parameters = GetCurrentRunParameters(false),
                SortBy = field,
                SortAscending = rbAsc.Checked
            });
        }

        private void DoSearch(MacroPointOrderSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<MacroPointOrderSearchCriteria>(criteria));
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            new MacroPointDashboardHandler(this).Initialize();

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            ddlSortBy.DataSource = ConnectSearchFields.MacroPointSortFields.OrderBy(s => s.DisplayName);
            ddlSortBy.DataBind();
            ddlSortBy.SelectedValue = ConnectSearchFields.MacroPointSortFields[0].Name;

            //sort commands
            lbtnSortIdNumber.CommandName = ConnectSearchFields.IdNumber.Name;
            lbtnSortNumberType.CommandName = ConnectSearchFields.NumberType.Name;
            lbtnSortNumber.CommandName = ConnectSearchFields.Number.Name;
            lbtnSortDateCreated.CommandName = ConnectSearchFields.DateCreated.Name;
            lbtnSortStartTrackDateTime.CommandName = ConnectSearchFields.StartTrackDateTime.Name;
            lbtnSortDateStopRequested.CommandName = ConnectSearchFields.DateStopRequested.Name;
            lbtnStatus.CommandName = ConnectSearchFields.StatusDescription.Name;
            lbtnSortUsername.CommandName = ConnectSearchFields.Username.Name;
            lbtnSortTrackCost.CommandName = ConnectSearchFields.TrackCost.Name;
            lbtnSortTrackDurationHours.CommandName = ConnectSearchFields.TrackDurationHours.Name;
            lbtnSortTrackIntervalMinutes.CommandName = ConnectSearchFields.TrackIntervalMinutes.Name;

            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);

            var criteria = new MacroPointOrderSearchCriteria
            {
                Parameters = profile != null
                                 ? profile.Columns
                                 : ConnectSearchFields.DefaultMacroPoint.Select(f => f.ToParameterColumn()).ToList(),
                SortAscending = profile == null || profile.SortAscending,
                SortBy = profile == null ? null : profile.SortBy
            };

            lstFilterParameters.DataSource = criteria.Parameters;
            lstFilterParameters.DataBind();

            rbAsc.Checked = criteria.SortAscending;
            rbDesc.Checked = !criteria.SortAscending;

            if (criteria.SortBy != null && ddlSortBy.ContainsValue(criteria.SortBy.Name))
                ddlSortBy.SelectedValue = criteria.SortBy.Name;
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            upcseDataUpdate.ResetLoadCount();
            ResultFlag = string.Empty;
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            ResultFlag = ExportFlag;
            DoSearchPreProcessingThenSearch(SortByField);
        }

        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }


        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = ConnectSearchFields.MacroPoint.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(ConnectSearchFields.MacroPoint);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;

        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSortData(object sender, CommandEventArgs e)
        {
            var field = ConnectSearchFields.MacroPointSortFields.FirstOrDefault(c => c.Name == e.CommandName);
            if (field == null) return;
            if (ddlSortBy.ContainsValue(field.Name))
            {
                if (ddlSortBy.SelectedValue == field.Name && rbAsc.Checked)
                {
                    rbAsc.Checked = false;
                    rbDesc.Checked = true;
                }
                else if (ddlSortBy.SelectedValue == field.Name && rbDesc.Checked)
                {
                    rbAsc.Checked = true;
                    rbDesc.Checked = false;
                }

                ddlSortBy.SelectedValue = field.Name;
            }
            DoSearchPreProcessingThenSearch(field);

        }

        protected void OnSortBySelectedIndexChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnSortOrderCheckChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }


        protected void OnSearchProfilesAutoRefreshChanged(object sender, ViewEventArgs<int> e)
        {
            tmRefresh.Enabled = e.Argument > default(int);
            if (e.Argument > 0) tmRefresh.Interval = e.Argument;
        }

        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();

            rbAsc.Checked = e.Argument.SortAscending;
            rbDesc.Checked = !e.Argument.SortAscending;

            if (e.Argument.SortBy != null && ddlSortBy.ContainsValue(e.Argument.SortBy.Name))
                ddlSortBy.SelectedValue = e.Argument.SortBy.Name;
        }



        protected void OnTimerTick(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }
    }
}