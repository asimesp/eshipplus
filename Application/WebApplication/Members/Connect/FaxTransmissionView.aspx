﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="FaxTransmissionView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Connect.FaxTransmissionView"
    EnableEventValidation="false" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.Connect" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Operations" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" />
</asp:Content>

<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">

    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:UpdatePanel runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                            CssClass="pageHeaderIcon" />
                        Fax Transmissions
                
                        <small class="ml10">
                            <asp:Literal runat="server" ID="litRecordCount" />
                        </small>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </h3>
        </div>
        <span class="clearfix"></span>
        <hr class="dark mb10" />
        <asp:Panel runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="FaxTransmissions" ShowAutoRefresh="True"
                            OnProcessingError="OnSearchProfilesProcessingError" OnSearchProfileSelected="OnSearchProfilesProfileSelected" OnAutoRefreshChanged="OnSearchProfilesAutoRefreshChanged" />
                    </div>
                </div>
            </div>
            <hr class="dark mb10" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                        OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>
            <hr class="fat" />
            <div class="row mb10">
                <div class="fieldgroup">
                    <asp:Button ID="btnSearch" CssClass="mr10" runat="server" Text="SEARCH" OnClick="OnSearchClicked" CausesValidation="False" />
                    <asp:Button ID="btnExport" runat="server" Text="EXPORT" OnClick="OnExportClicked" CausesValidation="False" />
                </div>
                <div class="fieldgroup right">
                    <label>Sort By:</label>
                    <asp:DropDownList runat="server" ID="ddlSortBy" DataTextField="DisplayName" DataValueField="Name" CssClass="mr10"
                        OnSelectedIndexChanged="OnSortBySelectedIndexChanged" AutoPostBack="True" />
                    <asp:RadioButton runat="server" ID="rbAsc" Text="ASC" GroupName="rbSortBy" AutoPostBack="True"
                        ToolTip="Ascending" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                    <asp:RadioButton runat="server" ID="rbDesc" Text="DESC" GroupName="rbSortBy" Checked="True"
                        ToolTip="Descending" AutoPostBack="True" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                </div>
            </div>
        </asp:Panel>

        <asp:UpdatePanel runat="server" ID="upDataUpdate">
            <ContentTemplate>

                <eShip:UpdatePanelContinuousScrollExtender ID="upcseDataUpdate" runat="server" BindableControlId="lvwRegistry" UpdatePanelToExtendId="upDataUpdate" />
                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheViewRegistryTable" TableId="viewRegistryTable" />
                <asp:Timer runat="server" ID="tmRefresh" Interval="30000" OnTick="OnTimerTick" />
                <div class="rowgroup">
                    <table id="viewRegistryTable" class="line2 pl2">
                        <tr>
                            <th style="width: 10%;">
                                <asp:LinkButton runat="server" ID="lbtnShipmentNumber" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                            Shipment #
                                </asp:LinkButton>
                            </th>
                            <th style="width: 10%;">
                                <asp:LinkButton runat="server" ID="lbtnSendDate" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                            Send Date
                                </asp:LinkButton>
                            </th>
                            <th style="width: 10%;">
                                <asp:LinkButton runat="server" ID="lbtnResponseDate" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                            Response Date
                                </asp:LinkButton>
                            </th>
                            <th style="width: 10%;">
                                <asp:LinkButton runat="server" ID="lbtnLastCheckDate" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                            Last Check Date
                                </asp:LinkButton>
                            </th>
                            <th style="width: 21%;">Message
                            </th>
                            <th style="width: 22%;">Resolution Comment
                            </th>
                            <th style="width: 5%; text-align: left;">
                                <asp:LinkButton runat="server" ID="lbtnStatus" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                            Status
                                </asp:LinkButton>
                            </th>
                            <th style ="width: 5%; text-align: left;">
                                <asp:LinkButton runat="server" ID="lbtnServiceProvider" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData">
                             Service Provider
                                </asp:LinkButton>
                            </th>
                            <th style="width: 2%;" class="text-center">Resolved
                            </th>
                            <th style="width: 5%;" class="text-center">Action
                            </th>
                        </tr>
                        <asp:ListView ID="lvwRegistry" runat="server" ItemPlaceholderID="itemPlaceHolder">
                            <LayoutTemplate>
                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <eShip:CustomHiddenField ID="hidFaxTransmissionId" runat="server" Value='<%# Eval("Id") %>' />
                                        
                                        <%# Eval("ShipmentNumber") %>
                                        <%# ActiveUser.HasAccessTo(ViewCode.Shipment) 
                                                ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Shipment Record'><img src={3} width='16' class='middle'/></a>", 
                                                    ResolveUrl(ShipmentView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    Eval("ShipmentNumber"),
                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                : string.Empty %>
                                    </td>
                                    <td>
                                        <%# Eval("SendDateTime").FormattedLongDateAlt() %>
                                    </td>
                                    <td>
                                        <%# Eval("ResponseDateTime").Equals(DateUtility.SystemEarliestDateTime) ? string.Empty : Eval("ResponseDateTime").FormattedLongDateAlt() %>
                                    </td>
                                    <td>
                                        <%# Eval("LastStatusCheckDateTime").Equals(DateUtility.SystemEarliestDateTime) ? string.Empty : Eval("LastStatusCheckDateTime").FormattedLongDateAlt() %>
                                    </td>
                                    <td>
                                        <%# Eval("Message") %>
                                    </td>
                                    <td>
                                        <%# Eval("ResolutionComment") %>
                                    </td>
                                    <td>
                                        <%# Eval("Status") %>
                                    </td>
                                    <td>
                                        <%# Eval("ServiceProvider").FormattedString() %>
                                    </td>
                                    <td class="text-center">
                                        <%# Eval("Resolved").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                                    </td>
                                    <td class="text-center">
                                        <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png"
                                            ToolTip="edit" CausesValidation="false" OnClick="OnEditClicked" Enabled='<%# Access.Modify %>'
                                            Visible='<%# Eval("Status").Equals(FaxTransmissionStatus.Failed) %>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lbtnShipmentNumber" />
                <asp:PostBackTrigger ControlID="lbtnSendDate" />
                <asp:PostBackTrigger ControlID="lbtnResponseDate" />
                <asp:PostBackTrigger ControlID="lbtnLastCheckDate" />
                <asp:PostBackTrigger ControlID="lbtnStatus" />
                <asp:PostBackTrigger ControlID="lbtnServiceProvider" />
                <asp:PostBackTrigger ControlID="lvwRegistry" />

            </Triggers>
        </asp:UpdatePanel>
        <asp:Panel ID="pnlEdit" runat="server" Visible="false">
            <div class="popupControl popupControlOverW500">
                <div class="popheader">
                    <h4>Modify Fax Transmission
                    </h4>
                    <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                        CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
                </div>

                <div class="row">
                    <table class="poptable">
                        <tr>
                            <td colspan="2" class="red">
                                <asp:Literal ID="litMessage" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Shipment Number:</label>
                            </td>
                            <td class="text-left">
                                <eShip:CustomHiddenField ID="hidFaxTransmissionId" runat="server" />
                                <eShip:CustomTextBox ID="txtShipmentNumber" runat="server" CssClass="w300 disabled" ReadOnly="True" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Send Date:</label>
                            </td>
                            <td class="text-left">
                                <eShip:CustomTextBox ID="txtSendDate" runat="server" CssClass="w300 disabled" ReadOnly="True" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Response Date:</label>
                            </td>
                            <td class="text-left">
                                <eShip:CustomTextBox ID="txtResponseDate" runat="server" CssClass="w300 disabled" ReadOnly="True" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Last Check Date:</label>
                            </td>
                            <td class="text-left">
                                <eShip:CustomTextBox ID="txtLastStatusCheckDate" runat="server" CssClass="w300 disabled" ReadOnly="True" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right top">
                                <label class="upper">Message:</label>
                            </td>
                            <td class="text-left ">
                                <eShip:CustomTextBox ID="txtFaxMessage" runat="server" CssClass="w300 disabled" ReadOnly="True" TextMode="MultiLine" />
                            </td>
                        </tr>
                       <tr>
                            <td class ="text-right">
                               <label class="upper">Service Provider:</label>
                            </td>
                           <td class ="text-left">
                                <eShip:CustomTextBox ID="txtServiceProvider" runat="server" CssClass="w300 disabled" ReadOnly="True" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right top">
                                <label class="upper">
                                    Resolution Comment:
                                </label>
                                <br />
                                <label class="lhInherit">
                                    <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDescription" TargetControlId="txtResolutionComment" MaxLength="500" />
                                </label>
                            </td>
                            <td class="text-left middle">
                                <eShip:CustomTextBox ID="txtResolutionComment" CssClass="w300" TextMode="MultiLine" Rows="3"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="text-left">
                                <asp:CheckBox ID="chkResolved" runat="server" CssClass="jQueryUniform" />
                                <label class="upper">Resolved</label>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td class="pt10">
                                <asp:Button ID="btnSave" Text="Save" OnClick="OnSaveClick" runat="server" CausesValidation="false" />
                                <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnCloseClicked" runat="server"
                                    CausesValidation="false" CssClass="ml10" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>
    <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information"
        OnOkay="OnOkayProcess" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackgroundControl" runat="server" Visible="false" />
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
    <eShip:CustomHiddenField runat="server" ID="hidShipmentId" />
</asp:Content>
