﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.CarrierIntegrations;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Connect;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Connect;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Connect
{
    public partial class VendorCommunicationView : MemberPageBaseWithPageStore, IVendorCommunicationView
    {
        private const string MilestonesKey = "MilestonesKey";
        private const string NotificationMethodKey = "NotificationMethodKey";

        private const string NotificationsHeader = "Notifications";

        private const string SaveFlag = "S";
        private const string DeleteFlag = "D";

		private const string DownloadEdiTemplatesArgs = "DETA";

		const string EdiTemplatesPath = @"~/Members/Connect/EDITemplates.zip";

        public override ViewCode PageCode { get { return ViewCode.VendorCommunication; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.ConnectBlue; } }

        public static string PageAddress { get { return "~/Members/Connect/VendorCommunicationView.aspx"; } }

        public Dictionary<int, string> Milestones
        {
            set
            {
                if (!PageStore.ContainsKey(MilestonesKey)) PageStore.Add(MilestonesKey, value);
                else PageStore[MilestonesKey] = value;
            }
            private get { return PageStore[MilestonesKey] as Dictionary<int, string>; }
        }

        public Dictionary<int, string> NotificationMethods
        {
            set
            {
                if (!PageStore.ContainsKey(NotificationMethodKey)) PageStore.Add(NotificationMethodKey, value);
                else PageStore[NotificationMethodKey] = value;
            }
            private get { return PageStore[NotificationMethodKey] as Dictionary<int, string>; }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<VendorCommunication>> Save;
        public event EventHandler<ViewEventArgs<VendorCommunication>> Delete;
        public event EventHandler<ViewEventArgs<VendorCommunication>> Lock;
        public event EventHandler<ViewEventArgs<VendorCommunication>> UnLock;
        public event EventHandler<ViewEventArgs<VendorCommunication>> LoadAuditLog;
        public event EventHandler<ViewEventArgs<string>> VendorSearch;

        public void DisplayVendor(Vendor vendor)
        {
            txtVendorNumber.Text = vendor == null ? string.Empty : vendor.VendorNumber;
            txtVendorName.Text = vendor == null ? string.Empty : vendor.Name;
            hidVendorId.Value = vendor == null ? string.Empty : vendor.Id.ToString();
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == SaveFlag)
            {
                // unlock and return to read-only
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<VendorCommunication>(new VendorCommunication(hidVendorCommunicationId.Value.ToLong(), false)));
                SetEditStatus(false);
                memberToolBar.ShowUnlock = false;
                // clean up file cleared out!
                if (!string.IsNullOrEmpty(hidFilesToDelete.Value))
                {
                    Server.RemoveFiles(hidFilesToDelete.Value.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries));
                    hidFilesToDelete.Value = string.Empty;
                }
            }

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;

            litErrorMessages.Text = messages.HasErrors()
                                        ? string.Join(WebApplicationConstants.HtmlBreak,
                                                      messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
                                                        Select(m => m.Message).ToArray())
                                        : string.Empty;
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs)
        {
            auditLogs.Load(logs);
        }

        public void FailedLock(Lock @lock)
        {
            memberToolBar.ShowNew = false;
            memberToolBar.ShowSave = false;
            memberToolBar.ShowDelete = false;
            memberToolBar.ShowUnlock = false;

            SetEditStatus(false);

            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void Set(VendorCommunication communication)
        {
            if (communication.Id == default(long) && hidFlag.Value == DeleteFlag)
            {
                Server.RemoveDirectory(WebApplicationSettings.VendorCommunicationFolder(ActiveUser.TenantId, hidVendorCommunicationId.Value.ToLong()));
                hidFlag.Value = string.Empty;
            }

            LoadVendorCommunication(communication);
            SetEditStatus(!communication.IsNew);
        }


        private void UpdateNotificationsFromView(VendorCommunication vendorCommunication)
        {
            var notifications = lstNotifications.Items
                .Select(control =>
                {
                    var id = control.FindControl("hidNotificationId").ToCustomHiddenField().Value.ToLong();
                    return new VendorNotification(id, id != default(long))
                    {
                        Milestone = control.FindControl("ddlMilestone").ToDropDownList().SelectedValue.ToEnum<Milestone>(),
                        NotificationMethod = control.FindControl("ddlNotificationMethod").ToDropDownList().SelectedValue.ToEnum<NotificationMethod>(),
                        Email = control.FindControl("txtEmail").ToTextBox().Text.StripSpacesFromEmails(),
                        Fax = control.FindControl("txtFax").ToTextBox().Text,
                        Enabled = control.FindControl("chkEnabled").ToAltUniformCheckBox().Checked,
                        VendorCommunication = vendorCommunication,
                        TenantId = vendorCommunication.TenantId
                    };
                })
                .ToList();

            vendorCommunication.Notifications = notifications;
        }


        private void SetEditStatus(bool enabled)
        {
            var notNew = (hidVendorCommunicationId.Value.ToLong() != default(long));

            pnlDetails.Enabled = enabled;
            pnlNotifications.Enabled = enabled;

            fupEdiVANEnvelopePath.Enabled = notNew && enabled;
            btnClearEdiFileWrapper.Enabled = notNew && enabled;

            memberToolBar.EnableSave = enabled;

            litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;
        }

        private void LoadVendorCommunication(VendorCommunication v)
        {
            if (!v.IsNew) v.LoadCollections();

            hidVendorCommunicationId.Value = v.Id.ToString();

            txtConnectGuid.Text = v.ConnectGuid.ToString();
            chkEnabledWebServiceApiAccess.Checked = v.EnableWebServiceAPIAccess;
            chkUseSelectiveDropOff.Checked = v.UseSelectiveDropOff;
            chk204.Checked = v.DropOff204;
            chk997.Checked = v.Pickup997;
            chk210.Checked = v.Pickup210;
            chk214.Checked = v.Pickup214;
            chk990.Checked = v.Pickup990;
            chkAck210.Checked = v.AcknowledgePickup210;
            chkAck214.Checked = v.AcknowledgePickup214;
            chkAck990.Checked = v.AcknowledgePickup990;

            txtLoadTenderExpAllowance.Text = v.LoadTenderExpAllowance.GetString();

            DisplayVendor(v.Vendor);

            //EDI
            chkEdiEnabled.Checked = v.EdiEnabled;
            chkSecureEdiVan.Checked = v.SecureEdiVAN;
            chkRemoveFileAfterPickupEdi.Checked = v.EdiDeleteFileAfterPickup;
            txtEdiCode.Text = v.EdiCode;
            txtEdiVanUrl.Text = v.EdiVANUrl;
            txtEdiVanUsername.Text = v.EdiVANUsername;
            txtEdiVanPassword.Text = v.EdiVANPassword;
            txtEdiVanDefaultFolder.Text = v.EdiVANDefaultFolder;

            lbtnDownloadEdiVANEnvelope.Text = Server.GetFileName(v.EdiVANEnvelopePath);
            hidEdiVANEnvelopePath.Value = v.EdiVANEnvelopePath;

            //FTP
            chkFtpEnabled.Checked = v.FtpEnabled;
            chkSecureFtp.Checked = v.SecureFtp;
            chkSFtp.Checked = v.SSHFtp;
            chkRemoveFileAfterPickupFtp.Checked = v.FtpDeleteFileAfterPickup;
            txtFtpUrl.Text = v.FtpUrl;
            txtFtpUsername.Text = v.FtpUsername;
            txtFtpPassword.Text = v.FtpPassword;
            txtFtpDefaultFolder.Text = v.FtpDefaultFolder;

            lstNotifications.DataSource = v.Notifications.OrderBy(n => n.Milestone.ToString());
            lstNotifications.DataBind();

            tabNotifications.HeaderText = v.Notifications.BuildTabCount(NotificationsHeader);

            //Integrations
            chkImgRtrvEnabled.Checked = v.ImgRtrvEnabled;
	        chkShipmentDispatchEnabled.Checked = v.ShipmentDispatchEnabled;
            chkDisableGuaranteedServiceDispatch.Checked = v.DisableGuaranteedServiceDispatch;
            txtImgRtrvUsername.Text = v.CarrierIntegrationUsername;
            txtImgRtrvPassword.Text = v.CarrierIntegrationPassword;
            if (ddlImgRtrvEngine.ContainsValue(v.CarrierIntegrationEngine))
                ddlImgRtrvEngine.SelectedValue = v.CarrierIntegrationEngine;
            else
                ddlImgRtrvEngine.SelectedIndex = 0;

            memberToolBar.ShowUnlock = v.HasUserLock(ActiveUser, v.Id);

            // SMC3 EVA

            chkSmc3EvaEnabled.Checked = v.SMC3EvaEnabled;
            chkSmc3EvaTest.Checked = v.IsInSMC3Testing;
            chkSmc3EvaDispatchEnabled.Checked = v.SMC3EvaSupportsDispatch;
            chkSmc3EvaDocumentRetrievalEnabled.Checked = v.SMC3EvaSupportsDocumentRetrieval;
            chkSmc3EvaTrackingEnabled.Checked = v.SMC3EvaSupportsTracking;
            txtSmc3EvaProductionAccountToken.Text = v.SMC3ProductionAccountToken;
            txtSmc3EvaTestAccountToken.Text = v.SMC3TestAccountToken;

            // Project 44

            chkProject44Enabled.Checked = v.Project44Enabled;
            chkProject44DispatchEnabled.Checked = v.Project44DispatchEnabled;
            chkProject44TrackingEnabled.Checked = v.Project44TrackingEnabled;

        }

        private void ProcessTransferredRequest(VendorCommunication vendorCommunication)
        {
            if (vendorCommunication.IsNew) return;
            
            LoadVendorCommunication(vendorCommunication);

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<VendorCommunication>(vendorCommunication));
        }

		private void OnDownloadEdiTemplatesClicked()
		{
			Response.Export(new FileInfo(MapPath(EdiTemplatesPath)), "EDITemplates.zip");
		}

		private List<ToolbarMoreAction> ToolbarMoreActions(bool enabled)
		{
			var downloadEdiTemplates = new ToolbarMoreAction
			{
				CommandArgs = DownloadEdiTemplatesArgs,
				ImageUrl = IconLinks.Export,
				Name = "Download Edi Templates Folder",
				IsLink = false,
				NavigationUrl = string.Empty
			};

			var actions = new List<ToolbarMoreAction> { downloadEdiTemplates };


			return actions;
		}


        protected void Page_Load(object sender, EventArgs e)
        {
            new VendorCommunicationHandler(this).Initialize();

            memberToolBar.ShowSave = Access.Modify;
            memberToolBar.ShowDelete = Access.Remove;
            memberToolBar.ShowNew = Access.Modify;
            memberToolBar.ShowEdit = Access.Modify;

            vendorCommunicationFinder.OpenForEditEnabled = Access.Modify;
			memberToolBar.LoadAdditionalActions(ToolbarMoreActions(true));

            if (IsPostBack) return;

            // load Img Rtrv Engine
            var engines = ProcessorUtilities.GetAll<CarrierEngine>().Select(i => new ViewListItem(i.Value, i.Value)).ToList();
            engines.Insert(0, new ViewListItem(WebApplicationConstants.ChooseOne, string.Empty));
            ddlImgRtrvEngine.DataSource = engines;
            ddlImgRtrvEngine.DataBind();


            aceVendor.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString() } });

            if (Loading != null)
                Loading(this, new EventArgs());

            if (Session[WebApplicationConstants.TransferVendorCommunicationId] != null)
            {
                ProcessTransferredRequest(new VendorCommunication(Session[WebApplicationConstants.TransferVendorCommunicationId].ToLong()));
                Session[WebApplicationConstants.TransferVendorCommunicationId] = null;
            }

            if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.TransferNumber]))
                ProcessTransferredRequest(new VendorCommunication(Request.QueryString[WebApplicationConstants.TransferNumber].UrlTextDecrypt().ToLong()));

            SetEditStatus(false);
        }


		protected void OnToolbarCommand(object sender, ViewEventArgs<string> e)
		{
			switch (e.Argument)
			{
				case DownloadEdiTemplatesArgs:
					OnDownloadEdiTemplatesClicked();
					break;
			}
		}

        protected void OnToolbarNewClicked(object sender, EventArgs e)
        {
            litErrorMessages.Text = string.Empty;

            LoadVendorCommunication(new VendorCommunication());
            DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
            SetEditStatus(true);
        }

        protected void OnToolbarEditClicked(object sender, EventArgs e)
        {
            var vendorCommunication = new VendorCommunication(hidVendorCommunicationId.Value.ToLong(), false);

            if (Lock == null || vendorCommunication.IsNew) return;

            SetEditStatus(true);
            memberToolBar.ShowUnlock = true;
            Lock(this, new ViewEventArgs<VendorCommunication>(vendorCommunication));
            LoadVendorCommunication(vendorCommunication);
        }

        protected void OnUnlockClicked(object sender, EventArgs e)
        {
            var vendorCommunication = new VendorCommunication(hidVendorCommunicationId.Value.ToLong(), false);
            if (UnLock == null || vendorCommunication.IsNew) return;

            SetEditStatus(false);
            memberToolBar.ShowUnlock = false;
            UnLock(this, new ViewEventArgs<VendorCommunication>(vendorCommunication));
        }

        protected void OnToolbarSaveClicked(object sender, EventArgs e)
        {
            var vendorCommunicationId = hidVendorCommunicationId.Value.ToLong();
            var communication = new VendorCommunication(vendorCommunicationId, vendorCommunicationId != default(long));

            if (communication.IsNew)
            {
                communication.ConnectGuid = Guid.NewGuid();
                communication.TenantId = ActiveUser.TenantId;
            }
            else communication.LoadCollections();

            //details
            communication.EnableWebServiceAPIAccess = chkEnabledWebServiceApiAccess.Checked;
            communication.UseSelectiveDropOff = chkUseSelectiveDropOff.Checked;
            communication.DropOff204 = chk204.Checked;
            communication.Pickup210 = chk210.Checked;
            communication.Pickup214 = chk214.Checked;
            communication.Pickup990 = chk990.Checked;
            communication.AcknowledgePickup210 = chkAck210.Checked;
            communication.AcknowledgePickup214 = chkAck214.Checked;
            communication.AcknowledgePickup990 = chkAck990.Checked;
            communication.Pickup997 = chk997.Checked;
            communication.Vendor = new Vendor(hidVendorId.Value.ToLong(), false);
            communication.LoadTenderExpAllowance = txtLoadTenderExpAllowance.Text.ToInt();

            //EDI
            communication.EdiEnabled = chkEdiEnabled.Checked;
            communication.SecureEdiVAN = chkSecureEdiVan.Checked;
            communication.EdiDeleteFileAfterPickup = chkRemoveFileAfterPickupEdi.Checked;
            communication.EdiCode = txtEdiCode.Text;
            communication.EdiVANUrl = txtEdiVanUrl.Text;
            communication.EdiVANUsername = txtEdiVanUsername.Text;
            communication.EdiVANPassword = txtEdiVanPassword.Text;
            communication.EdiVANDefaultFolder = txtEdiVanDefaultFolder.Text;

            //EDI File Wrapper
            if (fupEdiVANEnvelopePath.HasFile)
            {
                var virtualPath = WebApplicationSettings.VendorCommunicationFolder(ActiveUser.TenantId, communication.Id);
                var physicalPath = Server.MapPath(virtualPath);

                if (!Directory.Exists(physicalPath)) Directory.CreateDirectory(physicalPath);

                //ensure unique filename
                var fi = new FileInfo(Server.MakeUniqueFilename(Path.Combine(physicalPath, fupEdiVANEnvelopePath.FileName), true));
                fupEdiVANEnvelopePath.WriteToFile(physicalPath, fi.Name, Server.MapPath(communication.EdiVANEnvelopePath));
                communication.EdiVANEnvelopePath = virtualPath + fi.Name;
            }
            else if (string.IsNullOrEmpty(hidEdiVANEnvelopePath.Value)) communication.EdiVANEnvelopePath = string.Empty;

            //FTP
            communication.FtpEnabled = chkFtpEnabled.Checked;
            communication.SecureFtp = chkSecureFtp.Checked;
            communication.SSHFtp = chkSFtp.Checked;
            communication.FtpDeleteFileAfterPickup = chkRemoveFileAfterPickupFtp.Checked;
            communication.FtpUrl = txtFtpUrl.Text;
            communication.FtpUsername = txtFtpUsername.Text;
            communication.FtpPassword = txtFtpPassword.Text;
            communication.FtpDefaultFolder = txtFtpDefaultFolder.Text;

            //NOTIFICATIONS TAB
            UpdateNotificationsFromView(communication);

            //Img Rtrv
            communication.ImgRtrvEnabled = chkImgRtrvEnabled.Checked;
	        communication.ShipmentDispatchEnabled = chkShipmentDispatchEnabled.Checked;
            communication.CarrierIntegrationUsername = txtImgRtrvUsername.Text;
            communication.CarrierIntegrationPassword = txtImgRtrvPassword.Text;
            communication.CarrierIntegrationEngine = ddlImgRtrvEngine.SelectedValue;
            communication.DisableGuaranteedServiceDispatch = chkDisableGuaranteedServiceDispatch.Checked;


            // SMC3 EVA
            communication.SMC3EvaEnabled = chkSmc3EvaEnabled.Checked;
            communication.IsInSMC3Testing = chkSmc3EvaTest.Checked;
            communication.SMC3ProductionAccountToken = txtSmc3EvaProductionAccountToken.Text;
            communication.SMC3TestAccountToken = txtSmc3EvaTestAccountToken.Text;

            communication.SMC3EvaSupportsDispatch = chkSmc3EvaDispatchEnabled.Checked;
            communication.SMC3EvaSupportsDocumentRetrieval = chkSmc3EvaDocumentRetrievalEnabled.Checked;
            communication.SMC3EvaSupportsTracking = chkSmc3EvaTrackingEnabled.Checked;

            // Project 44
            communication.Project44Enabled = chkProject44Enabled.Checked;
            communication.Project44TrackingEnabled = chkProject44TrackingEnabled.Checked;
            communication.Project44DispatchEnabled = chkProject44DispatchEnabled.Checked;

            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<VendorCommunication>(communication));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<VendorCommunication>(communication));
        }

        protected void OnToolbarDeleteClicked(object sender, EventArgs e)
        {
            var vendorCommunication = new VendorCommunication(hidVendorCommunicationId.Value.ToLong(), false);

            hidFlag.Value = DeleteFlag;

            if (Delete != null)
                Delete(this, new ViewEventArgs<VendorCommunication>(vendorCommunication));

            vendorCommunicationFinder.Reset();
        }

        protected void OnToolbarFindClicked(object sender, EventArgs e)
        {
            vendorCommunicationFinder.Visible = true;
        }


        protected void OnVendorCommunicationFinderItemSelected(object sender, ViewEventArgs<VendorCommunication> e)
        {
            litErrorMessages.Text = string.Empty;

            if (hidVendorCommunicationId.Value.ToLong() != default(long))
            {
                var oldVendorCommunication = new VendorCommunication(hidVendorCommunicationId.Value.ToLong(), false);
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<VendorCommunication>(oldVendorCommunication));
            }

            var vendorCommunication = e.Argument;

            LoadVendorCommunication(vendorCommunication);

            vendorCommunicationFinder.Visible = false;

            if (vendorCommunicationFinder.OpenForEdit && Lock != null)
            {
                SetEditStatus(true);
                memberToolBar.ShowUnlock = true;
                Lock(this, new ViewEventArgs<VendorCommunication>(vendorCommunication));
            }
            else
            {
                SetEditStatus(false);
            }

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<VendorCommunication>(vendorCommunication));
        }

        protected void OnVendorCommunicationFinderSelectionCancelled(object sender, EventArgs e)
        {
            vendorCommunicationFinder.Visible = false;
        }


        protected void OnVendorFinderSelectionCancelled(object sender, EventArgs e)
        {
            vendorFinder.Visible = false;
        }

        protected void OnVendorFinderItemSelected(object sender, ViewEventArgs<Vendor> e)
        {
            DisplayVendor(e.Argument);
            vendorFinder.Visible = false;
        }

        protected void OnVendorNumberChanged(object sender, EventArgs e)
        {
            if (VendorSearch != null)
                VendorSearch(this, new ViewEventArgs<string>(txtVendorNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
        }

        protected void OnVendorSearchClicked(object sender, ImageClickEventArgs e)
        {
            vendorFinder.Visible = true;
        }


        protected void OnAddNotificationClicked(object sender, EventArgs e)
        {
            var notifications = lstNotifications.Items
                 .Select(control => new
                 {
                     Id = control.FindControl("hidNotificationId").ToCustomHiddenField().Value.ToLong(),
                     Milestone = control.FindControl("ddlMilestone").ToDropDownList().SelectedValue.ToEnum<Milestone>(),
                     NotificationMethod = control.FindControl("ddlNotificationMethod").ToDropDownList().SelectedValue.ToEnum<NotificationMethod>(),
                     Email = control.FindControl("txtEmail").ToTextBox().Text.StripSpacesFromEmails(),
                     Fax = control.FindControl("txtFax").ToTextBox().Text,
                     Enabled = control.FindControl("chkEnabled").ToAltUniformCheckBox().Checked,
                 })
                 .ToList();

            notifications.Add(new
            {
                Id = default(long),
                Milestone = Milestone.NewLoadOrder,
                NotificationMethod = NotificationMethod.Ftp,
                Email = string.Empty,
                Fax = string.Empty,
                Enabled = false
            });

            lstNotifications.DataSource = notifications;
            lstNotifications.DataBind();

            tabNotifications.HeaderText = notifications.BuildTabCount(NotificationsHeader);
            athtuTabUpdater.SetForUpdate(tabNotifications.ClientID, notifications.BuildTabCount(NotificationsHeader));
        }

        protected void OnClearNotificationsClicked(object sender, EventArgs e)
        {
            lstNotifications.DataSource = new List<CustomerNotification>();
            lstNotifications.DataBind();
            tabNotifications.HeaderText = NotificationsHeader;
            athtuTabUpdater.SetForUpdate(tabNotifications.ClientID, NotificationsHeader);
        }

        protected void OnDeleteNotificationClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var notifications = lstNotifications.Items
                 .Select(control => new
                 {
                     Id = control.FindControl("hidNotificationId").ToCustomHiddenField().Value.ToLong(),
                     Milestone = control.FindControl("ddlMilestone").ToDropDownList().SelectedValue.ToEnum<Milestone>(),
                     NotificationMethod = control.FindControl("ddlNotificationMethod").ToDropDownList().SelectedValue.ToEnum<NotificationMethod>(),
                     Email = control.FindControl("txtEmail").ToTextBox().Text.StripSpacesFromEmails(),
                     Fax = control.FindControl("txtFax").ToTextBox().Text,
                     Enabled = control.FindControl("chkEnabled").ToAltUniformCheckBox().Checked,
                 })
                 .ToList();

            notifications.RemoveAt(imageButton.Parent.FindControl("hidNotificationIndex").ToCustomHiddenField().Value.ToInt());

            lstNotifications.DataSource = notifications;
            lstNotifications.DataBind();

            tabNotifications.HeaderText = notifications.BuildTabCount(NotificationsHeader);
            athtuTabUpdater.SetForUpdate(tabNotifications.ClientID, notifications.BuildTabCount(NotificationsHeader));
        }


        protected void OnClearEdiFileWrapperClicked(object sender, EventArgs e)
        {
            hidFilesToDelete.Value += string.Format(";{0}", hidEdiVANEnvelopePath.Value);
            hidEdiVANEnvelopePath.Value = string.Empty;
            lbtnDownloadEdiVANEnvelope.Text = string.Empty;
        }


        protected void OnDownloadEdiVanEnvelopeClicked(object sender, EventArgs e)
        {
            Response.Export(Server.ReadFromFile(hidEdiVANEnvelopePath.Value), lbtnDownloadEdiVANEnvelope.Text);
        }


        protected void OnNotificationsItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var data = e.Item.DataItem;

            var nddl = e.Item.FindControl("ddlNotificationMethod").ToDropDownList();
            nddl.DataSource = NotificationMethods;
            nddl.DataBind();
            if (data.HasGettableProperty("NotificationMethod")) nddl.SelectedValue = ((NotificationMethod)data.GetPropertyValue("NotificationMethod")).ToInt().GetString();

            var mddl = e.Item.FindControl("ddlMilestone").ToDropDownList();
            mddl.DataSource = Milestones;
            mddl.DataBind();
            if (data.HasGettableProperty("Milestone")) mddl.SelectedValue = ((Milestone)data.GetPropertyValue("Milestone")).ToInt().GetString();

        }
    }
}
