﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Extern;
using LogisticsPlus.Eship.Extern.Edi;
using LogisticsPlus.Eship.Extern.Edi.Segments;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Connect;
using LogisticsPlus.Eship.Processor.Handlers.Connect;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Connect;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Connect;
using LogisticsPlus.Eship.WebApplication.Documents;
using LogisticsPlus.Eship.WebApplication.Members.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Operations;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Connect
{
    public partial class XmlConnectRecordView : MemberPageBase, IXmlConnectRecordsView
    {
        private const string InvalidEdiDocumentType = "Invalid Edi document type";
        private const string MissingCustComSetupErrMsgFormat = "Customer missing communication setup EDI 990 response. [Customer #: {0}, Name: {1}]";

        private const string OpenIfOnlySingleRecordFoundFlag = "PIOSRF";

        protected bool CanDownloadXml = false;

        private SearchField SortByField
        {
            get { return ConnectSearchFields.XmlConnectSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue); }
        }

        public override ViewCode PageCode { get { return ViewCode.XmlConnectRecord; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.ConnectBlue; } }

        public static string PageAddress { get { return "~/Members/Connect/XmlConnectRecordView.aspx"; } }

        public event EventHandler<ViewEventArgs<XmlConnect, LoadOrder>> ProcessLoadTenderAccept;
        public event EventHandler<ViewEventArgs<XmlConnectSearchCriteria>> Search;


        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplaySearchResult(List<XmlConnectViewSearchDto> xmlConnectRecords)
        {
            litRecordCount.Text = xmlConnectRecords.BuildRecordCount();
            upcseDataUpdate.DataSource = xmlConnectRecords;
            upcseDataUpdate.DataBind();

            if (hidFlag.Value == OpenIfOnlySingleRecordFoundFlag && xmlConnectRecords.Count == 1 && xmlConnectRecords[0].DocumentType == EdiDocumentType.EDI204)
            {
                hidFlag.Value = string.Empty;
                ProcessEdi204Doc(new XmlConnect(xmlConnectRecords[0].Id));
                pnlDimScreen.Visible = true;
                pnlEditProcessRecord.Visible = true;
            }
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems { Columns = GetCurrentRunParameters(true) };
        }

        public void FinalizeLoadOrderAccepted(XmlConnect connect, LoadOrder loadOrder)
        {
            var ccom = loadOrder.Customer.Communication;
            if (ccom == null)
            {
                DisplayMessages(new[] { ValidationMessage.Error(MissingCustComSetupErrMsgFormat, loadOrder.Customer.CustomerNumber, loadOrder.Customer.Name) });
                return;
            }
            var controlNumber = new AutoNumberProcessor().NextNumber(AutoNumberCode.EdiTransactionSetControlNumber, ActiveUser).GetString();
            var edi990 = connect.GenerateEdi990(controlNumber, loadOrder);
            Server.SendLoadTenderResponse(edi990, ccom, ActiveUser);
        }




        private void DoSearchPreProcessingThenSearch(SearchField field)
        {
            DoSearch(new XmlConnectSearchCriteria
            {
                Parameters = GetCurrentRunParameters(false),
                ActiveUserId = ActiveUser.Id,
                SortBy = field,
                SortAscending = rbAsc.Checked
            });
        }

        private void DoSearch(XmlConnectSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<XmlConnectSearchCriteria>(criteria));
        }


        private void ProcessTransferredDocumentSearchRequest()
        {
            var shipmentIdParameterColumn = ConnectSearchFields.ShipmentIdNumber.ToParameterColumn();
            shipmentIdParameterColumn.DefaultValue = Request.QueryString[WebApplicationConstants.XmlConnectRecordsShipmentIdKey];
            shipmentIdParameterColumn.Operator = Operator.Equal;

            var documentTypeParameterColumn = ConnectSearchFields.DocumentType.ToParameterColumn();
            documentTypeParameterColumn.DefaultValue = Request.QueryString[WebApplicationConstants.XmlConnectRecordsDocTypeKey].ToEnum<EdiDocumentType>().GetString();
            documentTypeParameterColumn.Operator = Operator.Equal;

            var criteria = new XmlConnectSearchCriteria
            {
                ActiveUserId = ActiveUser.Id,
                Parameters = new List<ParameterColumn>
				        {
					        shipmentIdParameterColumn,
					        documentTypeParameterColumn
				        },
                SortBy = SortByField,
                SortAscending = rbAsc.Checked
            };
            hidFlag.Value = OpenIfOnlySingleRecordFoundFlag;
            DoSearch(criteria);

            lstFilterParameters.DataSource = criteria.Parameters;
            lstFilterParameters.DataBind();
        }



		private void ProcessEdi204Doc(XmlConnect xc)
		{
			var htmlContent = string.Empty;

			var edi204 = xc.Xml.FromXml<Edi204>();
			var tender = edi204.Loads.FirstOrDefault(l => l.BeginningSegment.ShipmentIdNumber == xc.ShipmentIdNumber);
			var customerNumber = string.Empty;
			switch (xc.Direction)
			{
				case Direction.Inbound:
					var customer = new CustomerSearch().FetchCustomerByNumber(xc.CustomerNumber, xc.TenantId);
					customerNumber = customer.CustomerNumber;
					htmlContent = Server.GenerateLoadTender(tender, customer, xc.Direction);
					break;
				case Direction.Outbound:
					var vendor = new VendorSearch().FetchVendorByNumber(xc.VendorNumber, xc.TenantId);
					htmlContent = Server.GenerateLoadTender(tender, vendor == null ? string.Empty : vendor.Name, xc.Direction);
					break;
			}

			contentDiv.InnerHtml = htmlContent.RetrieveWrapperSection(BaseFields.BodyWrapperStart, BaseFields.BodyWrapperEnd);

			hidEditXmlConnectId.Value = xc.Id.GetString();
			hidDocTypeProcessing.Value = EdiDocumentType.EDI204.GetString();
			litTitle.Text = "Process Load Tender";


			var expired = xc.ExpirationDate <= DateTime.Now;
			var rxc = new XmlConnectSearch().FetchInboundLoadTenderResponseByShipmentIdNumber(xc.ShipmentIdNumber, xc.TenantId, ActiveUser.Id, customerNumber);
			var responded = rxc != null;
			var responseMsg = string.Empty;
			if (rxc != null)
			{
				var edi990 = rxc.Xml.FromXml<Edi990>();
				var response = edi990.Responses.FirstOrDefault(r => r.BeginningSegment.ShipmentIdentificationNumber == rxc.ShipmentIdNumber);
				responseMsg = response == null
								  ? string.Empty
								  : string.Format(" - {0}{1}{2}",
												  response.BeginningSegment.ReservationActionCode.GetDescription(),
												  response.BeginningSegment.ReservationActionCode == ReservationCode.D ? ":" : string.Empty,
												  response.BeginningSegment.DeclineReason);
			}


			litErrorMessage.Text = expired ? string.Format("{0} Record Expired!", WebApplicationConstants.HtmlCheck) : string.Empty;
			litProcessedMessage.Text = responded ? string.Format("{0} Record Already Processed!{1}", WebApplicationConstants.HtmlCheck, responseMsg) : string.Empty;

			pnlProcessLoadTender.Visible = !expired && !responded;

			var load = xc.Direction == Direction.Inbound
						   ? new LoadOrderSearch().FetchLoadOrderByShipperBol(xc.ShipmentIdNumber, xc.TenantId)
						   : xc.Direction == Direction.Outbound
								 ? new LoadOrderSearch().FetchLoadOrderByLoadNumber(xc.ShipmentIdNumber, xc.TenantId)
								 : null;
			pnlGoToLoadOrder.Visible = load != null;
			hidGoToRecordId.Value = load == null ? string.Empty : load.Id.GetString();

			// equipment type test
			if (tender != null && tender.EquipmentDetail != null &&
				!string.IsNullOrEmpty(tender.EquipmentDetail.EquipmentDescriptionCode))
			{
				var et = new EquipmentTypeSearch().FetchEquipmentTypeByCode(tender.EquipmentDetail.EquipmentDescriptionCode,
																			ActiveUser.TenantId);
				pnlRequiredEquipmentType.Visible = et == null || !et.Active;
			}
			else
			{
				pnlRequiredEquipmentType.Visible = false;
			}

			//package type test
			var oids = tender != null
						   ? tender.Stops.SelectMany(s => s.OrderIdentificationDetails).ToList()
						   : new List<OrderIdentificationDetail>();
			var ediOids = ProcessorVars.RegistryCache[ActiveUser.TenantId].PackageTypes
				.Select(pt => pt.EdiOid)
				.ToList();
			pnlRequiredPackageType.Visible = oids.Any(oid => !ediOids.Contains(oid.PackageTypeCode));
		}



        protected void Page_Load(object sender, EventArgs e)
        {
            new XmlConnectRecordsHandler(this).Initialize();

            CanDownloadXml = ActiveUser.HasAccessTo(ViewCode.XmlConnectRecordXmlDownload);

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            // set auto referesh
            tmRefresh.Interval = WebApplicationConstants.DefaultRefreshInterval.ToInt();

            // set sort fields
            ddlSortBy.DataSource = ConnectSearchFields.XmlConnectSortFields.OrderBy(s => s.DisplayName);
            ddlSortBy.DataBind();
            ddlSortBy.SelectedValue = ConnectSearchFields.XmlConnectSortFields[0].Name;

            //set rejection reasons
            var rejectionReasons = ProcessorUtilities.GetAll<LoadTenderDeclineReasonCode>().Values
                                                     .Select(v => new ViewListItem(v, v))
                                                     .ToList();
            rejectionReasons.Insert(0, new ViewListItem(WebApplicationConstants.ChooseOne, string.Empty));
            ddlRejectionReason.DataSource = rejectionReasons;
            ddlRejectionReason.DataBind();

            // sort commands
            lbtnSortDateCreated.CommandName = ConnectSearchFields.DateCreated.Name;
            lbtnSortDirection.CommandName = ConnectSearchFields.Direction.Name;
            lbtnSortDocumentType.CommandName = ConnectSearchFields.DocumentType.Name;
            lbtnSortExpirationDate.CommandName = ConnectSearchFields.ExpirationDate.Name;
            lbtnSortPurchaseOrderNumber.CommandName = ConnectSearchFields.PurchaseOrderNumber.Name;
            lbtnSortShipmentIdNumber.CommandName = ConnectSearchFields.ShipmentIdNumber.Name;
            lbtnSortShipperReference.CommandName = ConnectSearchFields.ShipperReference.Name;
            lbtnSortTotalWeight.CommandName = ConnectSearchFields.TotalWeight.Name;

            //If the page has query string with a doc type and shipment id, run a search on both parameters else load normally
            if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.XmlConnectRecordsDocTypeKey]) &&
                !string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.XmlConnectRecordsShipmentIdKey]))
            {
                ProcessTransferredDocumentSearchRequest();
                return;
            }

            //get criteria from Session
            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);

            var criteria = new XmlConnectSearchCriteria
            {
                ActiveUserId = ActiveUser.Id,
                Parameters = profile != null
                                 ? profile.Columns
                                 : ConnectSearchFields.DefaultXmlConnect.Select(f => f.ToParameterColumn()).ToList(),
                SortAscending = profile == null || profile.SortAscending,
                SortBy = profile == null ? null : profile.SortBy
            };

            lstFilterParameters.DataSource = criteria.Parameters;
            lstFilterParameters.DataBind();
        }

        protected void OnSearchClicked(object sender, EventArgs e)
        {
            upcseDataUpdate.ResetLoadCount();
            DoSearchPreProcessingThenSearch(SortByField);
        }



        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = ConnectSearchFields.XmlConnect.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

        }

        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(ConnectSearchFields.XmlConnect);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;

        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }



        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();

            rbAsc.Checked = e.Argument.SortAscending;
            rbDesc.Checked = !e.Argument.SortAscending;

            if (e.Argument.SortBy != null && ddlSortBy.ContainsValue(e.Argument.SortBy.Name))
                ddlSortBy.SelectedValue = e.Argument.SortBy.Name;


            DoSearchPreProcessingThenSearch(e.Argument.SortBy);
        }

        protected void OnSearchProfilesAutoRefreshChanged(object sender, ViewEventArgs<int> e)
        {
            tmRefresh.Enabled = e.Argument > default(int);
            if (e.Argument > 0) tmRefresh.Interval = e.Argument;
        }



        protected void OnTimerTick(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }



        protected void OnSortData(object sender, CommandEventArgs e)
        {
            var field = ConnectSearchFields.XmlConnectSortFields.FirstOrDefault(c => c.Name == e.CommandName);
            if (field == null) return;
            if (ddlSortBy.ContainsValue(field.Name))
            {
                if (ddlSortBy.SelectedValue == field.Name && rbAsc.Checked)
                {
                    rbAsc.Checked = false;
                    rbDesc.Checked = true;
                }
                else if (ddlSortBy.SelectedValue == field.Name && rbDesc.Checked)
                {
                    rbAsc.Checked = true;
                    rbDesc.Checked = false;
                }

                ddlSortBy.SelectedValue = field.Name;
            }
            DoSearchPreProcessingThenSearch(field);
        }

        protected void OnSortBySelectedIndexChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnSortOrderCheckChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }




        protected void OnXmlConnectDownloadClicked(object sender, ImageClickEventArgs e)
        {
            var button = (ImageButton)sender;
            var id = button.Parent.FindControl("hidXmlConnectId").ToCustomHiddenField().Value.ToLong();
            var xc = new XmlConnect(id);
            Response.Export(xc.Xml.ToUtf8Bytes(), string.Format("{0}.{1}", Guid.NewGuid(), ReportExportExtension.xml));
        }

        protected void OnXmlConnectProcessClicked(object sender, ImageClickEventArgs e)
        {
            var button = (ImageButton)sender;
            var id = button.Parent.FindControl("hidXmlConnectId").ToCustomHiddenField().Value.ToLong();
            var xc = new XmlConnect(id);

            switch (xc.DocumentType)
            {
                case EdiDocumentType.EDI204:
                    ProcessEdi204Doc(xc);
                    break;
                default:
                    DisplayMessages(new[] { ValidationMessage.Error("Unknown document type for process") });
                    return; // do not continue!
            }

            pnlDimScreen.Visible = true;
            pnlEditProcessRecord.Visible = true;
        }

        




        protected void OnRejectLoadTender(object sender, EventArgs e)
        {
            if (hidDocTypeProcessing.Value.ToEnum<EdiDocumentType>() != EdiDocumentType.EDI204)
            {
                DisplayMessages(new[] { ValidationMessage.Error(InvalidEdiDocumentType) });
                return;
            }

            if (string.IsNullOrEmpty(ddlRejectionReason.SelectedValue))
            {
                DisplayMessages(new[] { ValidationMessage.Error("Rejection reason is required!") });
                return;
            }

            var id = hidEditXmlConnectId.Value.ToLong();
            var xc = new XmlConnect(id, false);
            var cust = new CustomerSearch().FetchCustomerByNumber(xc.CustomerNumber, ActiveUser.TenantId);
            if (cust.Communication == null)
            {
                DisplayMessages(new[] { ValidationMessage.Error(MissingCustComSetupErrMsgFormat, cust.CustomerNumber, cust.Name) });
                return;
            }

            var controlNumber = new AutoNumberProcessor().NextNumber(AutoNumberCode.EdiTransactionSetControlNumber, ActiveUser).GetString();
            var edi990 = xc.GenerateEdi990(controlNumber, null, ddlRejectionReason.SelectedValue);
            Server.SendLoadTenderResponse(edi990, cust.Communication, ActiveUser);

            pnlDimScreen.Visible = false;
            pnlEditProcessRecord.Visible = false;

            // refresh results
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnAcceptLoadTender(object sender, EventArgs e)
        {
            if (hidDocTypeProcessing.Value.ToEnum<EdiDocumentType>() != EdiDocumentType.EDI204)
            {
                DisplayMessages(new[] { ValidationMessage.Error(InvalidEdiDocumentType) });
                return;
            }

            var id = hidEditXmlConnectId.Value.ToLong();
            var xc = new XmlConnect(id, false);

            // create new load
            var edi204 = xc.Xml.FromXml<Edi204>();
            var customer = new CustomerSearch().FetchCustomerByNumber(xc.CustomerNumber, ActiveUser.TenantId);
            var equipments = ProcessorVars.RegistryCache[ActiveUser.TenantId].EquipmentTypes;
            var services = new ServiceSearch().FetchAllServices(ActiveUser.TenantId);
            var countries = ProcessorVars.RegistryCache.Countries.Values.ToList();
            var packageTypes = ProcessorVars.RegistryCache[ActiveUser.TenantId].PackageTypes;
            var load = edi204.FromIncomingEdi204(xc, customer, ActiveUser, equipments, services, countries, packageTypes, WebApplicationSettings.FreightClasses);

            if (ProcessLoadTenderAccept != null)
                ProcessLoadTenderAccept(this, new ViewEventArgs<XmlConnect, LoadOrder>(xc, load));

            pnlDimScreen.Visible = false;
            pnlEditProcessRecord.Visible = false;

            // refresh results
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnCloseClicked(object sender, ImageClickEventArgs e)
        {
            pnlDimScreen.Visible = false;
            pnlEditProcessRecord.Visible = false;
        }

        protected void OnGotoLoadOrder(object sender, EventArgs e)
        {
            if (hidDocTypeProcessing.Value.ToEnum<EdiDocumentType>() != EdiDocumentType.EDI204)
            {
                DisplayMessages(new[] { ValidationMessage.Error(InvalidEdiDocumentType) });
                return;
            }

            Session[WebApplicationConstants.TransferLoadOrderId] = hidGoToRecordId.Value;
            Session[WebApplicationConstants.DisableEditModeOnTransfer] = false;
            Response.Redirect(LoadOrderView.PageAddress);
        }

        protected void OnXmlConnectRecordsItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var item = e.Item;
            if (item == null) return;

            var xmlConnectDto = (XmlConnectViewSearchDto)item.DataItem;
            if (xmlConnectDto.LoadOrderId != default(long))
            {
                var loadOrder = new LoadOrder(xmlConnectDto.LoadOrderId);
                item.FindControl("hypLoadOrderView").ToHyperLink().NavigateUrl = string.Format("{0}?{1}={2}", LoadOrderView.PageAddress, WebApplicationConstants.TransferNumber, loadOrder.LoadOrderNumber);
            }

            if (!string.IsNullOrEmpty(xmlConnectDto.VendorNumber))
            {
                var hypVendorNumber = e.Item.FindControl("hypVendorNumber").ToHyperLink();
                var vendor = new VendorSearch().FetchVendorByNumber(xmlConnectDto.VendorNumber, ActiveUser.TenantId);
                hypVendorNumber.NavigateUrl = string.Format("{0}?{1}={2}", VendorView.PageAddress, WebApplicationConstants.TransferNumber, vendor.Id.GetString().UrlTextEncrypt());
            }

            if (!string.IsNullOrEmpty(xmlConnectDto.CustomerNumber))
            {
                var hypCustomerNumber = e.Item.FindControl("hypCustomerNumber").ToHyperLink();
                var customer = new CustomerSearch().FetchCustomerByNumber(xmlConnectDto.CustomerNumber, ActiveUser.TenantId);
                hypCustomerNumber.NavigateUrl = string.Format("{0}?{1}={2}", CustomerView.PageAddress, WebApplicationConstants.TransferNumber, customer.Id.GetString().UrlTextEncrypt());
            }
        }
    }
}