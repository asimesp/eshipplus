﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Dto.Connect;
using LogisticsPlus.Eship.Processor.Handlers.Connect;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Connect;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Connect;
using LogisticsPlus.Eship.WebApplication.Documents;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Connect
{
    public partial class DocDeliveryLogView : MemberPageBase, IDocDeliveryLogView
    {
        private const string ExportFlag = "ExportFlag";

        private string ResultFlag
        {
            get { return ViewState["ResultFlag"].GetString(); }
            set { ViewState["ResultFlag"] = value; }
        }


        public static string PageAddress { get { return "~/Members/Connect/DocDeliveryLogView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.DocDeliveryLog; } }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.ConnectBlue; }
        }

        public event EventHandler<ViewEventArgs<DocDeliveryLogSearchCriteria>> Search;


        public void DisplaySearchResult(List<DocDeliveryLogDto> docDeliveryLogs)
        {
            litRecordCount.Text = docDeliveryLogs.BuildRecordCount();
            var logs = docDeliveryLogs.OrderByDescending(l => l.LogDateTime).ToList();
            switch (ResultFlag)
            {
                case ExportFlag:
                    DoExport(logs);
                    break;
                default:
                    upcseDataUpdate.DataSource = logs;
                    upcseDataUpdate.DataBind();
                    break;
            }
        }


        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }




        private void DoExport(IEnumerable<DocDeliveryLogDto> docDeliveryLogs)
        {
            var q = docDeliveryLogs.Select(a => a.ToString()).ToList();
            q.Insert(0, DocDeliveryLogDto.Header());
            Response.Export(q.ToArray().NewLineJoin());
        }

        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems { Columns = GetCurrentRunParameters(true) };
        }

        private void DoSearchPreProcessingThenSearch()
        {
            DoSearch(new DocDeliveryLogSearchCriteria
            {
                Parameters = GetCurrentRunParameters(false),
            });
        }

        private void DoSearch(DocDeliveryLogSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<DocDeliveryLogSearchCriteria>(criteria));
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new DocDeliveryLogHandler(this).Initialize();

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);

            var columns = profile != null && profile.Columns != null
                            ? profile.Columns
                            : ConnectSearchFields.DefaultDocDeliveryLogs.Select(f => f.ToParameterColumn()).ToList();

            lstFilterParameters.DataSource = columns;
            lstFilterParameters.DataBind();
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            upcseDataUpdate.ResetLoadCount();
            DoSearchPreProcessingThenSearch();
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            ResultFlag = ExportFlag;
            DoSearchPreProcessingThenSearch();
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = ConnectSearchFields.DocDeliveryLogs.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(ConnectSearchFields.DocDeliveryLogs);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;

        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();

            DoSearchPreProcessingThenSearch();
        }


        protected void OnRedeliverClicked(object sender, ImageClickEventArgs e)
        {
            var parent = ((ImageButton)sender).Parent;
            var entityId = parent.FindControl("hidEntityId").ToCustomHiddenField().Value.ToLong();
            var entityType = parent.FindControl("hidEntityType").ToCustomHiddenField().Value.ToEnum<DocDeliveryEntityType>();
            var processor = new FtpDocDeliveryProcessor(Server);

            Shipment s;
            switch (entityType)
            {
                case DocDeliveryEntityType.BillOfLading:
                    s = new Shipment(entityId);
                    if (!s.KeyLoaded) goto missingEntity;
                    if (s.Customer.Communication == null) goto missingComEntity;
                    processor.RedeliverShipmentBol(s.Customer.Communication, s, this.GenerateBOL(s, true).ToPdf(s.ShipmentNumber + "_BOL"), ActiveUser);
                    break;

                case DocDeliveryEntityType.Invoice:
                    var i = new Invoice(entityId);
                    if (!i.KeyLoaded) goto missingEntity;
                    if (i.CustomerLocation.Customer.Communication == null) goto missingComEntity;
                    processor.RedeliverInvoice(i.CustomerLocation.Customer.Communication, i, this.GenerateInvoice(i, true).ToPdf(i.InvoiceNumber + "_Invoice"), ActiveUser);
                    break;

                case DocDeliveryEntityType.ShipmentDocument:
                    var d = new ShipmentDocument(entityId);
                    if (!d.KeyLoaded) goto missingEntity;
                    if (d.Shipment.Customer.Communication == null) goto missingComEntity;
                    processor.RedeliverShipmentDocument(d.Shipment.Customer.Communication, d, ActiveUser);
                    break;

                case DocDeliveryEntityType.ShipmentStatement:
                    s = new Shipment(entityId);
                    if (!s.KeyLoaded) goto missingEntity;
                    if (s.Customer.Communication == null) goto missingComEntity;
                    var si = new AuditInvoiceDto().FetchAuditInvoiceDtos(s.ShipmentNumber, DetailReferenceType.Shipment, s.TenantId);
                    processor.RedeliverShipmentStatement(s.Customer.Communication, s, this.GenerateShipmentStatement(s, si, true).ToPdf(s.ShipmentNumber + "_Statement"), ActiveUser);
                    break;

                missingComEntity:
                    DisplayMessages(new[] { ValidationMessage.Error("Missing entity communication setup for which to redeliver document") });
                    return;

                missingEntity:
                    DisplayMessages(new[] { ValidationMessage.Error("Missing entity for which to redeliver document") });
                    return;
            }

            // reload for additional log
            DoSearchPreProcessingThenSearch();
        }
    }
}
