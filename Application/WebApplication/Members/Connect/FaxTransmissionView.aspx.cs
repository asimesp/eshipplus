﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Connect;
using LogisticsPlus.Eship.Processor.Handlers.Connect;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Connect;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Connect;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Connect
{
    public partial class FaxTransmissionView : MemberPageBase, IFaxTransmissionView
    {

        private const string ExportFlag = "E";

        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        private SearchField SortByField
        {
            get { return ConnectSearchFields.FaxTransmissionSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue); }
        }

        public override ViewCode PageCode { get { return ViewCode.FaxTransmission; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.ConnectBlue; } }

        public static string PageAddress { get { return "~/Members/Connect/FaxTransmissionView.aspx"; } }


        public event EventHandler<ViewEventArgs<FaxTransmission>> Save;
        public event EventHandler<ViewEventArgs<FaxTransmission>> Delete;
        public event EventHandler<ViewEventArgs<FaxTransmission>> LoadAuditLog;
        public event EventHandler<ViewEventArgs<FaxTransmissionSearchCriteria>> Search;
        public event EventHandler<ViewEventArgs<FaxTransmission>> Lock;
        public event EventHandler<ViewEventArgs<FaxTransmission>> UnLock;


        public void FailedLock(Lock @lock)
        {
            btnSave.Enabled = false;

            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void DisplaySearchResult(List<FaxTransmissionViewSearchDto> transmissions)
        {
            litRecordCount.Text = transmissions.BuildRecordCount();
            upcseDataUpdate.DataSource = transmissions;
            upcseDataUpdate.DataBind();

            if (hidFlag.Value == ExportFlag)
            {
                var lines = transmissions
                    .Select(f => new[]
					             	{
					             		f.ShipmentNumber,
                                        f.SendDateTime == DateUtility.SystemEarliestDateTime ? string.Empty :   f.SendDateTime.ToString(),
                                        f.ResponseDateTime == DateUtility.SystemEarliestDateTime ? string.Empty :   f.ResponseDateTime.ToString(),
										f.LastStatusCheckDateTime == DateUtility.SystemEarliestDateTime ? string.Empty : f.LastStatusCheckDateTime.ToString(),
					             		f.TransmissionKey.ToString(),
					             		f.Message,
					             		f.ResolutionComment,
					             		f.Status.FormattedString(),
										f.ServiceProvider.FormattedString(),
										f.Resolved.ToString()
					             	}.TabJoin())
                    .ToList();
                lines.Insert(0,
                             new[]
				             	{
				             		"Shipment Number", "Send Date", "Initial Response Date", "Last Status Check Date",  "Transmission Key", 
									"Message", "Resolution Comment", "Service Provider", "Status", "Resolved"
				             	}.TabJoin());
                hidFlag.Value = string.Empty;

                Response.Export(lines.ToArray().NewLineJoin(), "FaxTransmissions.txt");
            }
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() || messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }


        private void CloseEditPopup()
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<FaxTransmission>(new FaxTransmission(hidFaxTransmissionId.Value.ToLong(), false)));
        }

        private void ClearFields()
        {
            txtResolutionComment.Text = string.Empty;
            chkResolved.Checked = false;
        }


        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems
            {
                Columns = GetCurrentRunParameters(true),
                SortAscending = rbAsc.Checked,
                SortBy = SortByField,
            };
        }

        private void DoSearchPreProcessingThenSearch(SearchField field)
        {
            DoSearch(new FaxTransmissionSearchCriteria
            {
                Parameters = GetCurrentRunParameters(false),
                SortBy = field,
                SortAscending = rbAsc.Checked
            });
        }

        private void DoSearch(FaxTransmissionSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<FaxTransmissionSearchCriteria>(criteria));
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            new FaxTransmissionHandler(this).Initialize();

            btnSave.Enabled = Access.Modify;

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            // set sort fields
            ddlSortBy.DataSource = ConnectSearchFields.FaxTransmissionSortFields.OrderBy(s => s.DisplayName);
            ddlSortBy.DataBind();
            ddlSortBy.SelectedValue = ConnectSearchFields.FaxTransmissionSortFields[0].Name;


            // sort commands
            lbtnShipmentNumber.CommandName = ConnectSearchFields.ShipmentNumber.Name;
            lbtnSendDate.CommandName = ConnectSearchFields.SendDateTime.Name;
            lbtnResponseDate.CommandName = ConnectSearchFields.ResponseDateTime.Name;
            lbtnLastCheckDate.CommandName = ConnectSearchFields.LastStatusCheckDateTime.Name;
            lbtnStatus.CommandName = ConnectSearchFields.StatusText.Name;
			lbtnServiceProvider.CommandName = ConnectSearchFields.ServiceProviderText.Name;

            //get criteria from Session
            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);

            var criteria = new FaxTransmissionSearchCriteria
            {
                Parameters = profile != null
                                 ? profile.Columns
                                 : ConnectSearchFields.DefaultFaxTransmission.Select(f => f.ToParameterColumn()).ToList(),
                SortAscending = profile == null || profile.SortAscending,
                SortBy = profile == null ? null : profile.SortBy
            };

            lstFilterParameters.DataSource = criteria.Parameters;
            lstFilterParameters.DataBind();

            rbAsc.Checked = criteria.SortAscending;
            rbDesc.Checked = !criteria.SortAscending;

            if (criteria.SortBy != null && ddlSortBy.ContainsValue(criteria.SortBy.Name))
                ddlSortBy.SelectedValue = criteria.SortBy.Name;
        }


        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            ClearFields();

            var imageButton = (ImageButton)sender;
            var faxTransmissionId = imageButton.Parent.FindControl("hidFaxTransmissionId").ToCustomHiddenField();

            var transmission = new FaxTransmission(faxTransmissionId.Value.ToLong(), false);

            hidFaxTransmissionId.Value = transmission.Id.GetString();

            txtShipmentNumber.Text = transmission.ShipmentNumber;
            txtSendDate.Text = transmission.SendDateTime.GetString();
            txtResponseDate.Text = transmission.ResponseDateTime == DateUtility.SystemEarliestDateTime ? string.Empty : transmission.ResponseDateTime.FormattedLongDateAlt();
            txtLastStatusCheckDate.Text = transmission.LastStatusCheckDateTime == DateUtility.SystemEarliestDateTime ? string.Empty : transmission.LastStatusCheckDateTime.FormattedLongDateAlt();
            txtFaxMessage.Text = transmission.Message;
            txtResolutionComment.Text = transmission.ResolutionComment;
			txtServiceProvider.Text = transmission.ServiceProvider.FormattedString();
			chkResolved.Checked = transmission.Resolved;

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = true;

            if (Lock != null)
                Lock(this, new ViewEventArgs<FaxTransmission>(transmission));
        }

        protected void OnCloseClicked(object sender, object e)
        {
            CloseEditPopup();
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            var transmissionId = hidFaxTransmissionId.Value.ToLong();

            var transmission = new FaxTransmission(transmissionId, transmissionId != default(long))
            {
                ResolutionComment = txtResolutionComment.Text,
                Resolved = chkResolved.Checked,
            };

            if (Save != null)
                Save(this, new ViewEventArgs<FaxTransmission>(transmission));

            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnSearchClicked(object sender, EventArgs e)
        {
            upcseDataUpdate.ResetLoadCount();
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            hidFlag.Value = ExportFlag;
            DoSearchPreProcessingThenSearch(SortByField);
        }


        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (CloseEditPopUp) CloseEditPopup();
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = ConnectSearchFields.FaxTransmission.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

        }

        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(ConnectSearchFields.FaxTransmission);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;

        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }



        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();

            rbAsc.Checked = e.Argument.SortAscending;
            rbDesc.Checked = !e.Argument.SortAscending;

            if (e.Argument.SortBy != null && ddlSortBy.ContainsValue(e.Argument.SortBy.Name))
                ddlSortBy.SelectedValue = e.Argument.SortBy.Name;
        }

        protected void OnSearchProfilesAutoRefreshChanged(object sender, ViewEventArgs<int> e)
        {
            tmRefresh.Enabled = e.Argument > default(int);
            if (e.Argument > 0) tmRefresh.Interval = e.Argument;
        }



        protected void OnSortData(object sender, CommandEventArgs e)
        {
            var field = ConnectSearchFields.FaxTransmissionSortFields.FirstOrDefault(c => c.Name == e.CommandName);
            if (field == null) return;
            if (ddlSortBy.ContainsValue(field.Name))
            {
                if (ddlSortBy.SelectedValue == field.Name && rbAsc.Checked)
                {
                    rbAsc.Checked = false;
                    rbDesc.Checked = true;
                }
                else if (ddlSortBy.SelectedValue == field.Name && rbDesc.Checked)
                {
                    rbAsc.Checked = true;
                    rbDesc.Checked = false;
                }

                ddlSortBy.SelectedValue = field.Name;
            }
            DoSearchPreProcessingThenSearch(field);
        }

        protected void OnSortBySelectedIndexChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnSortOrderCheckChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }


        protected void OnTimerTick(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }
    }
}