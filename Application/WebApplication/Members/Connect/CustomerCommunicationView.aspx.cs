﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Connect;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Connect;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Connect
{
    public partial class CustomerCommunicationView : MemberPageBaseWithPageStore, ICustomerCommunicationView
    {
        private const string MilestonesKey = "MilestonesKey";
        private const string NotificationMethodKey = "NotificationMethodKey";

        private const string NotificationsHeader = "Notifications";

        private const string SaveFlag = "S";
        private const string DeleteFlag = "D";

		private const string DownloadEdiTemplatesArgs = "DETA";

		const string EdiTemplatesPath = @"~/Members/Connect/EDITemplates.zip";

        public override ViewCode PageCode { get { return ViewCode.CustomerCommunication; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.ConnectBlue; } }

        public static string PageAddress { get { return "~/Members/Connect/CustomerCommunicationView.aspx"; } }


        public List<DocumentTag> DocumentTags
        {
            set
            {
                var tags = value.Select(dt => new ViewListItem(" " + dt.FormattedString(), dt.Id.ToString()))
                  .OrderBy(i => i.Text)
                  .ToList();

                rptDocumentTags.DataSource = tags;
                rptDocumentTags.DataBind();
            }
        }

        public Dictionary<int, string> Milestones
        {
            set
            {
                if (!PageStore.ContainsKey(MilestonesKey)) PageStore.Add(MilestonesKey, value);
                else PageStore[MilestonesKey] = value;
            }
            private get { return PageStore[MilestonesKey] as Dictionary<int, string>; }
        }

        public Dictionary<int, string> NotificationMethods
        {
            set
            {
                if (!PageStore.ContainsKey(NotificationMethodKey)) PageStore.Add(NotificationMethodKey, value);
                else PageStore[NotificationMethodKey] = value;
            }
            private get { return PageStore[NotificationMethodKey] as Dictionary<int, string>; }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<CustomerCommunication>> Save;
        public event EventHandler<ViewEventArgs<CustomerCommunication>> Delete;
        public event EventHandler<ViewEventArgs<CustomerCommunication>> Lock;
        public event EventHandler<ViewEventArgs<CustomerCommunication>> UnLock;
        public event EventHandler<ViewEventArgs<CustomerCommunication>> LoadAuditLog;
        public event EventHandler<ViewEventArgs<string>> CustomerSearch;

        public void DisplayCustomer(Customer customer)
        {
            txtCustomerNumber.Text = customer == null ? string.Empty : customer.CustomerNumber;
            txtCustomerName.Text = customer == null ? string.Empty : customer.Name;
            hidCustomerId.Value = customer == null ? string.Empty : customer.Id.ToString();
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == SaveFlag)
            {
                // unlock and return to read-only
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<CustomerCommunication>(new CustomerCommunication(hidCustomerCommunicationId.Value.ToLong(), false)));
                SetEditStatus(false);
                memberToolBar.ShowUnlock = false;

                // clean up file cleared out!
                if (!string.IsNullOrEmpty(hidFilesToDelete.Value))
                {
                    Server.RemoveFiles(hidFilesToDelete.Value.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries));
                    hidFilesToDelete.Value = string.Empty;
                }
            }

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;

            litErrorMessages.Text = messages.HasErrors()
                                        ? string.Join(WebApplicationConstants.HtmlBreak,
                                                      messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
                                                        Select(m => m.Message).ToArray())
                                        : string.Empty;
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs)
        {
            auditLogs.Load(logs);
        }

        public void FailedLock(Lock @lock)
        {
            memberToolBar.ShowNew = false;
            memberToolBar.ShowSave = false;
            memberToolBar.ShowDelete = false;
            memberToolBar.ShowUnlock = false;

            SetEditStatus(false);

            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void Set(CustomerCommunication communication)
        {
            if (communication.Id == default(long) && hidFlag.Value == DeleteFlag)
            {
                Server.RemoveDirectory(WebApplicationSettings.CustomerCommunicationFolder(ActiveUser.TenantId, hidCustomerCommunicationId.Value.ToLong()));
                hidFlag.Value = string.Empty;
            }

            LoadCustomerCommunication(communication);
            SetEditStatus(!communication.IsNew);
        }


        private void UpdateNotificationsFromView(CustomerCommunication customerCommunication)
        {
            var notifications = lstNotifications.Items
                .Select(control =>
                {
                    var id = control.FindControl("hidNotificationId").ToCustomHiddenField().Value.ToLong();
                    return new CustomerNotification(id, id != default(long))
                    {
                        Milestone = control.FindControl("ddlMilestone").ToDropDownList().SelectedValue.ToEnum<Milestone>(),
                        NotificationMethod = control.FindControl("ddlNotificationMethod").ToDropDownList().SelectedValue.ToEnum<NotificationMethod>(),
                        Email = control.FindControl("txtEmail").ToTextBox().Text.StripSpacesFromEmails(),
                        Fax = control.FindControl("txtFax").ToTextBox().Text,
                        Enabled = control.FindControl("chkEnabled").ToAltUniformCheckBox().Checked,
                        CustomerCommunication = customerCommunication,
                        TenantId = customerCommunication.TenantId
                    };
                })
                .ToList();

            customerCommunication.Notifications = notifications;
        }

        private void UpdateDocDeliveryDocumentTagsFromView(CustomerCommunication customerCommunication)
        {
            var tags = rptDocumentTags
                .Items
                .Cast<RepeaterItem>()
                .Where(i => i.FindControl("chkSelected").ToCheckBox().Checked)
                .Select(control => new DocDeliveryDocTag
                    {
                        DocumentTagId = control.FindControl("hidTagId").ToCustomHiddenField().Value.ToLong(),
                        CustomerCommunication = customerCommunication,
                        TenantId = customerCommunication.TenantId
                    })
                .ToList();

            customerCommunication.DocDeliveryDocTags = tags;
        }

        
        private void SetEditStatus(bool enabled)
        {
            var notNew = (hidCustomerCommunicationId.Value.ToLong() != default(long));

            pnlDetails.Enabled = enabled;
            pnlNotifications.Enabled = enabled;
            pnlDocDelivery.Enabled = enabled;

            fupEdiVANEnvelopePath.Enabled = notNew && enabled;
            btnClearEdiFileWrapper.Enabled = notNew && enabled;

            memberToolBar.EnableSave = enabled;

            litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;
        }

        private void ProcessTransferredRequest(CustomerCommunication communication)
        {
            if (communication.IsNew) return;

            LoadCustomerCommunication(communication);

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<CustomerCommunication>(communication));
        }

        private void LoadCustomerCommunication(CustomerCommunication c)
        {
            if (!c.IsNew) c.LoadCollections();

            hidCustomerCommunicationId.Value = c.Id.ToString();

            tabNotifications.HeaderText = c.Notifications.BuildTabCount(NotificationsHeader);

            txtConnectGuid.Text = c.ConnectGuid.ToString();
            chkEnabledWebServiceApiAccess.Checked = c.EnableWebServiceAPIAccess;
            chkStopVendorNotifications.Checked = c.StopVendorNotifications;
            chkSendAveryLabel.Checked = c.SendAveryLabel;
            chkSendStandardLabel.Checked = c.SendStandardLabel;
            chkUseSelectiveDropOff.Checked = c.UseSelectiveDropOff;
            chk204.Checked = c.Pickup204;
            chkAck204.Checked = c.AcknowledgePickup204;
            chk997.Checked = c.Pickup997;

            DisplayCustomer(c.Customer);

            //EDI
            chkEdiEnabled.Checked = c.EdiEnabled;
            chkSecureEdiVan.Checked = c.SecureEdiVAN;
            chkRemoveFileAfterPickupEdi.Checked = c.EdiDeleteFileAfterPickup;
            txtEdiCode.Text = c.EdiCode;
            txtEdiVanUrl.Text = c.EdiVANUrl;
            txtEdiVanUsername.Text = c.EdiVANUsername;
            txtEdiVanPassword.Text = c.EdiVANPassword;
            txtEdiVanDefaultFolder.Text = c.EdiVANDefaultFolder;

            lbtnDownloadEdiVANEnvelope.Text = Server.GetFileName(c.EdiVANEnvelopePath);
            hidEdiVANEnvelopePath.Value = c.EdiVANEnvelopePath;

            //FTP
            chkFtpEnabled.Checked = c.FtpEnabled;
            chkSecureFtp.Checked = c.SecureFtp;
            chkSFtp.Checked = c.SSHFtp;
            chkRemoveFileAfterPickupFtp.Checked = c.FtpDeleteFileAfterPickup;
            txtFtpUrl.Text = c.FtpUrl;
            txtFtpUsername.Text = c.FtpUsername;
            txtFtpPassword.Text = c.FtpPassword;
            txtFtpDefaultFolder.Text = c.FtpDefaultFolder;

            lstNotifications.DataSource = c.Notifications.OrderBy(n => n.Milestone.ToString());
            lstNotifications.DataBind();

            // DOCS
	        chkEmailDocDeliveryEnabled.Checked = c.EmailDocDeliveryEnabled;
            chkFtpDocDeliveryEnabled.Checked = c.FtpDocDeliveryEnabled;
            chkDocDeliverySecureFtp.Checked = c.DocDeliverySecureFtp;
            txtDocDeliveryFtpUsername.Text = c.DocDeliveryFtpUsername;
            txtDocDeliveryFtpDefaultFolder.Text = c.DocDeliveryFtpDefaultFolder;
            txtDocDeliveryFtpPassword.Text = c.DocDeliveryFtpPassword;
            txtDocDeliveryFtpUrl.Text = c.DocDeliveryFtpUrl;
            txtDocumentLinkAccessKey.Text = c.DocumentLinkAccessKey.ToString();
            txtDocDeliveryStartDate.Text = c.StartDocDeliveriesFrom.FormattedShortDateSuppressEarliestDate();
            chkDeliverBol.Checked = c.DeliverBolDoc;
            chkDeliverInternalDocs.Checked = c.DeliverInternalShipmentDocs;
            chkDeliverInvoice.Checked = c.DeliverInvoiceDoc;
            chkDeliverStatement.Checked = c.DeliverShipmentStatementDoc;
            chkHoldShipmentTillInvoiced.Checked = c.HoldShipmentDocsTillInvoiced;

            var docTagIds = c.DocDeliveryDocTags.Select(d => d.DocumentTagId);
            foreach (var item in rptDocumentTags.Items.Cast<RepeaterItem>())
            {
                item.FindControl("chkSelected").ToCheckBox().Checked = docTagIds.Contains(item.FindControl("hidTagId").ToCustomHiddenField().Value.ToLong());
            }

            memberToolBar.ShowUnlock = c.HasUserLock(ActiveUser, c.Id);
        }

		private void OnDownloadEdiTemplatesClicked()
		{
			Response.Export(new FileInfo(MapPath(EdiTemplatesPath)), "EDITemplates.zip");
		}

		private List<ToolbarMoreAction> ToolbarMoreActions(bool enabled)
		{
			var downloadEdiTemplates = new ToolbarMoreAction
			{
				CommandArgs = DownloadEdiTemplatesArgs,
				ImageUrl = IconLinks.Export,
				Name = "Download Edi Templates Folder",
				IsLink = false,
				NavigationUrl = string.Empty
			};

			var actions = new List<ToolbarMoreAction> { downloadEdiTemplates };


			return actions;
		}

        protected void Page_Load(object sender, EventArgs e)
        {
            new CustomerCommunicationHandler(this).Initialize();

            memberToolBar.ShowSave = Access.Modify;
            memberToolBar.ShowDelete = Access.Remove;
            memberToolBar.ShowNew = Access.Modify;
            memberToolBar.ShowEdit = Access.Modify;
			memberToolBar.LoadAdditionalActions(ToolbarMoreActions(true));
            customerCommunicationFinder.OpenForEditEnabled = Access.Modify;

            if (IsPostBack) return;

            aceCustomer.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.ActiveUser, ActiveUser.Id.GetString() } });

            if (Loading != null)
                Loading(this, new EventArgs());

            if (Session[WebApplicationConstants.TransferCustomerCommunicationId] != null)
            {
                ProcessTransferredRequest(new CustomerCommunication(Session[WebApplicationConstants.TransferCustomerCommunicationId].ToLong()));
                Session[WebApplicationConstants.TransferCustomerCommunicationId] = null;
            }

            if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.TransferNumber]))
                ProcessTransferredRequest(new CustomerCommunication(Request.QueryString[WebApplicationConstants.TransferNumber].UrlTextDecrypt().ToLong()));

            SetEditStatus(false);
        }

	    protected void OnToolbarCommand(object sender, ViewEventArgs<string> e)
	    {
		    switch (e.Argument)
		    {
				case DownloadEdiTemplatesArgs:
					OnDownloadEdiTemplatesClicked();
					break;
		    }
	    }

	    protected void OnToolbarNewClicked(object sender, EventArgs e)
        {
            litErrorMessages.Text = string.Empty;

            LoadCustomerCommunication(new CustomerCommunication { StartDocDeliveriesFrom = DateTime.Now });
            DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
            SetEditStatus(true);
        }

        protected void OnToolbarEditClicked(object sender, EventArgs e)
        {
            var customerCommunication = new CustomerCommunication(hidCustomerCommunicationId.Value.ToLong(), false);

            if (Lock == null || customerCommunication.IsNew) return;

            SetEditStatus(true);
            memberToolBar.ShowUnlock = true;
            Lock(this, new ViewEventArgs<CustomerCommunication>(customerCommunication));

            LoadCustomerCommunication(customerCommunication);
        }

        protected void OnUnlockClicked(object sender, EventArgs e)
        {
            var communication = new CustomerCommunication(hidCustomerCommunicationId.Value.ToLong(), false);
            if (UnLock != null && !communication.IsNew)
            {
                SetEditStatus(false);
                memberToolBar.ShowUnlock = false;
                UnLock(this, new ViewEventArgs<CustomerCommunication>(communication));
            }
        }

        protected void OnToolbarSaveClicked(object sender, EventArgs e)
        {
            var customerCommunicationId = hidCustomerCommunicationId.Value.ToLong();
            var communication = new CustomerCommunication(customerCommunicationId, customerCommunicationId != default(long));

            if (communication.IsNew)
            {
                communication.ConnectGuid = Guid.NewGuid();
                communication.TenantId = ActiveUser.TenantId;
            }
            else communication.LoadCollections();

            //details
            communication.EnableWebServiceAPIAccess = chkEnabledWebServiceApiAccess.Checked;
            communication.StopVendorNotifications = chkStopVendorNotifications.Checked;
            communication.SendAveryLabel = chkSendAveryLabel.Checked;
            communication.SendStandardLabel = chkSendStandardLabel.Checked;
            communication.UseSelectiveDropOff = chkUseSelectiveDropOff.Checked;
            communication.Pickup204 = chk204.Checked;
            communication.AcknowledgePickup204 = chkAck204.Checked;
            communication.Pickup997 = chk997.Checked;
            communication.Customer = new Customer(hidCustomerId.Value.ToLong(), false);

            //EDI
            communication.EdiEnabled = chkEdiEnabled.Checked;
            communication.SecureEdiVAN = chkSecureEdiVan.Checked;
            communication.EdiDeleteFileAfterPickup = chkRemoveFileAfterPickupEdi.Checked;
            communication.EdiCode = txtEdiCode.Text;
            communication.EdiVANUrl = txtEdiVanUrl.Text;
            communication.EdiVANUsername = txtEdiVanUsername.Text;
            communication.EdiVANPassword = txtEdiVanPassword.Text;
            communication.EdiVANDefaultFolder = txtEdiVanDefaultFolder.Text;

            //EDI File Wrapper
            if (fupEdiVANEnvelopePath.HasFile)
            {
                var virtualPath = WebApplicationSettings.CustomerCommunicationFolder(ActiveUser.TenantId, communication.Id);
                var physicalPath = Server.MapPath(virtualPath);

                if (!Directory.Exists(physicalPath)) Directory.CreateDirectory(physicalPath);

                var fi = new FileInfo(Server.MakeUniqueFilename(Path.Combine(physicalPath, fupEdiVANEnvelopePath.FileName), true));
                fupEdiVANEnvelopePath.WriteToFile(physicalPath, fi.Name, Server.MapPath(communication.EdiVANEnvelopePath));
                communication.EdiVANEnvelopePath = virtualPath + fi.Name;
            }
            else if (string.IsNullOrEmpty(hidEdiVANEnvelopePath.Value)) communication.EdiVANEnvelopePath = string.Empty;



            //FTP
            communication.FtpEnabled = chkFtpEnabled.Checked;
            communication.SecureFtp = chkSecureFtp.Checked;
            communication.SSHFtp = chkSFtp.Checked;
            communication.FtpDeleteFileAfterPickup = chkRemoveFileAfterPickupFtp.Checked;
            communication.FtpUrl = txtFtpUrl.Text;
            communication.FtpUsername = txtFtpUsername.Text;
            communication.FtpPassword = txtFtpPassword.Text;
            communication.FtpDefaultFolder = txtFtpDefaultFolder.Text;



            // DOCS
	        communication.EmailDocDeliveryEnabled = chkEmailDocDeliveryEnabled.Checked;
            communication.FtpDocDeliveryEnabled = chkFtpDocDeliveryEnabled.Checked;
            communication.DocDeliverySecureFtp = chkDocDeliverySecureFtp.Checked;
            communication.DocDeliveryFtpUsername = txtDocDeliveryFtpUsername.Text;
            communication.DocDeliveryFtpDefaultFolder = txtDocDeliveryFtpDefaultFolder.Text;
            communication.DocDeliveryFtpPassword = txtDocDeliveryFtpPassword.Text;
            communication.DocDeliveryFtpUrl = txtDocDeliveryFtpUrl.Text;
            communication.DocumentLinkAccessKey =  txtDocumentLinkAccessKey.Text.ToGuid();
            communication.StartDocDeliveriesFrom = txtDocDeliveryStartDate.Text.ToDateTime();
            communication.DeliverBolDoc = chkDeliverBol.Checked;
            communication.DeliverInternalShipmentDocs = chkDeliverInternalDocs.Checked;
            communication.DeliverInvoiceDoc = chkDeliverInvoice.Checked;
            communication.DeliverShipmentStatementDoc = chkDeliverStatement.Checked;
            communication.HoldShipmentDocsTillInvoiced = chkHoldShipmentTillInvoiced.Checked;

            UpdateDocDeliveryDocumentTagsFromView(communication);

            UpdateNotificationsFromView(communication);

            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<CustomerCommunication>(communication));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<CustomerCommunication>(communication));
        }

        protected void OnToolbarDeleteClicked(object sender, EventArgs e)
        {
            var customerCommunication = new CustomerCommunication(hidCustomerCommunicationId.Value.ToLong(), false);

            hidFlag.Value = DeleteFlag;

            if (Delete != null)
                Delete(this, new ViewEventArgs<CustomerCommunication>(customerCommunication));

            customerCommunicationFinder.Reset();
        }

        protected void OnToolbarFindClicked(object sender, EventArgs e)
        {
            customerCommunicationFinder.Visible = true;
        }
	    

	    protected void OnCustomerCommunicationFinderItemSelected(object sender, ViewEventArgs<CustomerCommunication> e)
        {
            litErrorMessages.Text = string.Empty;

            if (hidCustomerCommunicationId.Value.ToLong() != default(long))
            {
                var oldCustomerCommunication = new CustomerCommunication(hidCustomerCommunicationId.Value.ToLong(), false);
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<CustomerCommunication>(oldCustomerCommunication));
            }

            var customerCommunication = e.Argument;

            LoadCustomerCommunication(customerCommunication);

            customerCommunicationFinder.Visible = false;

            if (customerCommunicationFinder.OpenForEdit && Lock != null)
            {
                SetEditStatus(true);
                memberToolBar.ShowUnlock = true;
                Lock(this, new ViewEventArgs<CustomerCommunication>(customerCommunication));
            }
            else
            {
                SetEditStatus(false);
            }

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<CustomerCommunication>(customerCommunication));
        }

        protected void OnCustomerCommunicationFinderSelectionCancelled(object sender, EventArgs e)
        {
            customerCommunicationFinder.Visible = false;
        }


        protected void OnCustomerFinderSelectionCancelled(object sender, EventArgs e)
        {
            customerFinder.Visible = false;
        }

        protected void OnCustomerFinderItemSelected(object sender, ViewEventArgs<Customer> e)
        {
            DisplayCustomer(e.Argument);
            customerFinder.Visible = false;
        }

        protected void OnCustomerNumberChanged(object sender, EventArgs e)
        {
            if (CustomerSearch != null)
                CustomerSearch(this, new ViewEventArgs<string>(txtCustomerNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
        }

        protected void OnCustomerSearchClicked(object sender, ImageClickEventArgs e)
        {
            customerFinder.Visible = true;
        }


        protected void OnAddNotificationClicked(object sender, EventArgs e)
        {
            var notifications = lstNotifications.Items
                 .Select(control => new
                 {
                     Id = control.FindControl("hidNotificationId").ToCustomHiddenField().Value.ToLong(),
                     Milestone = control.FindControl("ddlMilestone").ToDropDownList().SelectedValue.ToEnum<Milestone>(),
                     NotificationMethod = control.FindControl("ddlNotificationMethod").ToDropDownList().SelectedValue.ToEnum<NotificationMethod>(),
                     Email = control.FindControl("txtEmail").ToTextBox().Text.StripSpacesFromEmails(),
                     Fax = control.FindControl("txtFax").ToTextBox().Text,
                     Enabled = control.FindControl("chkEnabled").ToAltUniformCheckBox().Checked,
                 })
                 .ToList();

            notifications.Add(new
                {
                    Id = default(long),
                    Milestone = Milestone.NewLoadOrder,
                    NotificationMethod = NotificationMethod.Ftp,
                    Email = string.Empty,
                    Fax = string.Empty,
                    Enabled = false
                });

            lstNotifications.DataSource = notifications;
            lstNotifications.DataBind();

            tabNotifications.HeaderText = notifications.BuildTabCount(NotificationsHeader);
            athtuTabUpdater.SetForUpdate(tabNotifications.ClientID, notifications.BuildTabCount(NotificationsHeader));
        }

        protected void OnClearNotificationsClicked(object sender, EventArgs e)
        {
            lstNotifications.DataSource = new List<CustomerNotification>();
            lstNotifications.DataBind();
            tabNotifications.HeaderText = NotificationsHeader;
            athtuTabUpdater.SetForUpdate(tabNotifications.ClientID, NotificationsHeader);
        }

        protected void OnDeleteNotificationClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var notifications = lstNotifications.Items
                .Select(control => new
                {
                    Id = control.FindControl("hidNotificationId").ToCustomHiddenField().Value.ToLong(),
                    Milestone = control.FindControl("ddlMilestone").ToDropDownList().SelectedValue.ToEnum<Milestone>(),
                    NotificationMethod = control.FindControl("ddlNotificationMethod").ToDropDownList().SelectedValue.ToEnum<NotificationMethod>(),
                    Email = control.FindControl("txtEmail").ToTextBox().Text.StripSpacesFromEmails(),
                    Fax = control.FindControl("txtFax").ToTextBox().Text,
                    Enabled = control.FindControl("chkEnabled").ToAltUniformCheckBox().Checked,
                })
                .ToList();

            notifications.RemoveAt(imageButton.Parent.FindControl("hidNotificationIndex").ToCustomHiddenField().Value.ToInt());

            lstNotifications.DataSource = notifications;
            lstNotifications.DataBind();

            tabNotifications.HeaderText = notifications.BuildTabCount(NotificationsHeader);
            athtuTabUpdater.SetForUpdate(tabNotifications.ClientID, notifications.BuildTabCount(NotificationsHeader));
        }

        protected void OnClearEdiFileWrapperClicked(object sender, EventArgs e)
        {
	        hidFilesToDelete.Value += string.Format(";{0}", hidEdiVANEnvelopePath.Value);
	        hidEdiVANEnvelopePath.Value = string.Empty;
	        lbtnDownloadEdiVANEnvelope.Text = string.Empty;
        }

        protected void OnClearDocumentLinkAccessKeyClicked(object sender, EventArgs e)
        {
	        txtDocumentLinkAccessKey.Text = new Guid().ToString();
        }


        protected void OnDocDeliveryEnabledCheckedChanged(object sender, EventArgs e)
        {
            if (chkFtpDocDeliveryEnabled.Checked) txtDocDeliveryStartDate.Text = DateTime.Now.ToShortDateString();
        }


        protected void OnDownloadEdiVanEnvelopeClicked(object sender, EventArgs e)
        {
            Response.Export(Server.ReadFromFile(hidEdiVANEnvelopePath.Value), lbtnDownloadEdiVANEnvelope.Text);
        }

        protected void OnGenerateNewDocumentLinkAccessKeyClicked(object sender, EventArgs e)
        {
	        txtDocumentLinkAccessKey.Text = Guid.NewGuid().ToString();
        }



        protected void OnNotificationsItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var data = e.Item.DataItem;

            var nddl = e.Item.FindControl("ddlNotificationMethod").ToDropDownList();
            nddl.DataSource = NotificationMethods;
            nddl.DataBind();
            if (data.HasGettableProperty("NotificationMethod")) nddl.SelectedValue = ((NotificationMethod)data.GetPropertyValue("NotificationMethod")).ToInt().GetString();

            var mddl = e.Item.FindControl("ddlMilestone").ToDropDownList();
            mddl.DataSource = Milestones;
            mddl.DataBind();
            if (data.HasGettableProperty("Milestone")) mddl.SelectedValue = ((Milestone)data.GetPropertyValue("Milestone")).ToInt().GetString();

        }
    }
}