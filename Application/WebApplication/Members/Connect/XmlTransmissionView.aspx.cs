﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Connect;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Connect;
using LogisticsPlus.Eship.WebApplication.Documents;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Connect
{
    public partial class XmlTransmissionView : MemberPageBase, IXmlTransmissionView
    {

        private const string ExportFlag = "E";
        private const string Wildcard = "%";

        public override ViewCode PageCode { get { return ViewCode.XmlTransmission; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.ConnectBlue; } }

        public static string PageAddress { get { return "~/Members/Connect/XmlTransmissionView.aspx"; } }


        public Dictionary<int, string> SendOkay
        {
            set
            {
                var source = value.ToDictionary(v => v.Key, v => v.Value);
                if (!source.ContainsKey(-1)) source.Add(-1, WebApplicationConstants.NotApplicable);
                ddlSendOkay.DataSource = source;
                ddlSendOkay.DataBind();
            }
        }

        public Dictionary<int, string> Directions
        {
            set
            {
                var source = value.ToDictionary(v => v.Key, v => v.Value);
                if (!source.ContainsKey(-1)) source.Add(-1, "Both");
                ddlDirection.DataSource = source;
                ddlDirection.DataBind();
            }
        }


        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<XmlTransmissionViewSearchCriteria>> Search;

        public void FailedLock(Lock @lock)
        {
        }

        public void DisplaySearchResult(List<XmlTransmission> transmissions)
        {
            if (transmissions.Count == 0) DisplayMessages(new[] { ValidationMessage.Information("No xml transmissions found") });

            var xmlTransmissions = transmissions
                .Select(t => new
                {
                    t.Id,
                    t.ReferenceNumberType,
                    t.ReferenceNumber,
                    t.TransmissionDateTime,
                    t.AcknowledgementDateTime,
                    t.AcknowledgementMessage,
                    t.TransmissionKey,
                    t.DocumentType,
                    t.NotificationMethod,
                    t.Direction,
                    User = t.User == null ? string.Empty : t.User.Username,
                    t.SendOkay
                })
                .ToList();

            litRecordCount.Text = transmissions.BuildRecordCount();
            lvwRegistry.DataSource = xmlTransmissions;
            lvwRegistry.DataBind();

            if (hidFlag.Value == ExportFlag)
            {
                var lines = transmissions
                    .Select(f => new[]
			                        {
                                        f.Id.ToString(),
			                            f.ReferenceNumber.ToString(),
										f.ReferenceNumberType.FormattedString(),
			                            f.TransmissionDateTime == DateUtility.SystemEarliestDateTime ? string.Empty :   f.TransmissionDateTime.ToString(),
			                            f.AcknowledgementDateTime == DateUtility.SystemEarliestDateTime ? string.Empty :   f.AcknowledgementDateTime.ToString(),
			                            f.AcknowledgementMessage.ToString(),
			                            f.TransmissionKey.ToString(),
			                            f.CommunicationReferenceType.FormattedString(),
			                            f.DocumentType.ToString(),
										f.NotificationMethod.FormattedString(),
										f.Direction.ToString(),
										f.User == null ? string.Empty : f.User.Username,
			                            f.SendOkay.ToString()
			                        }.TabJoin())
                    .ToList();
                lines.Insert(0, new[]
				                	{
				                		"Source Key", "Reference Number", "Reference Number Type", "Send Date", "Acknowledgement Date",
				                		"Acknowledgement Message", "Transmission Key",
				                		"Communication Reference Type", "Document Type", "Notification Method", "Direction", "Username",
				                		"Send Okay"
				                	}.TabJoin());

                hidFlag.Value = string.Empty;

                Response.Export(lines.ToArray().NewLineJoin(), "XmlTransmissions.txt");
            }
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        private void DoSearch()
        {
            var criteria = new XmlTransmissionViewSearchCriteria
            {
                Criteria = FormatCriteria(txtSearchCriteria.Text),
                SendOkay = ddlSendOkay.SelectedValue.ToInt(),
                Direction = ddlDirection.SelectedValue.ToInt(),
                StartSendDateTime = txtStartSendDate.Text.ToDateTime().TimeToMinimum(),
                EndSendDateTime = txtEndSendDate.Text.ToDateTime().TimeToMaximum(),
                StartAcknowledgementDateTime = DateUtility.SystemEarliestDateTime,
                EndAcknowledgementDateTime = DateUtility.SystemLatestDateTime
            };

            if (Search != null)
                Search(this, new ViewEventArgs<XmlTransmissionViewSearchCriteria>(criteria));
        }

        public WildcardCriteriaOptionsDefault DefaultOption
        {
            set
            {
                chkBeginsWith.Checked = value == WildcardCriteriaOptionsDefault.BeginsWith;
                chkEndsWith.Checked = value == WildcardCriteriaOptionsDefault.EndsWith;
                chkContains.Checked = value == WildcardCriteriaOptionsDefault.Contains;
                chkExact.Checked = value == WildcardCriteriaOptionsDefault.Exact;
            }
        }

        public string FormatCriteria(string criteria)
        {

            return string.Format("{0}{1}{2}",
                                 chkEndsWith.Checked || chkContains.Checked ? Wildcard : string.Empty,
                                 criteria,
                                 chkBeginsWith.Checked || chkContains.Checked ? Wildcard : string.Empty);
        }

        public string StripWildCardWrappers(string criteria)
        {
            var retVal = criteria.StartsWith(Wildcard) ? criteria.Substring(1) : criteria;
            retVal = retVal.EndsWith(Wildcard) ? retVal.Substring(0, retVal.Length - 1) : retVal;
            if (retVal == Wildcard) retVal = string.Empty;
            return retVal;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new XmlTransmissionHandler(this).Initialize();

            if (IsPostBack) return;

            DefaultOption = WildcardCriteriaOptionsDefault.Contains;

            txtStartSendDate.Text = DateTime.Today.FormattedShortDate();
            txtEndSendDate.Text = DateTime.Today.FormattedShortDate();

            ddlSendOkay.SelectedValue = (-1).ToString();
            ddlDirection.SelectedValue = (-1).ToString();

            if (Loading != null)
                Loading(this, new EventArgs());
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            DoSearch();
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            hidFlag.Value = ExportFlag;
            DoSearch();
        }
        
        
        protected void OnXmlTransmissionDownloadClicked(object sender, EventArgs e)
        {
            var button = (ImageButton)sender;

            var transmissionId = (button.Parent.FindControl("hidXmlTransmissionId").ToCustomHiddenField()).Value.ToLong();

            var transmission = new XmlTransmission(transmissionId);
            if (!transmission.KeyLoaded) return;

            Response.Export(transmission.Xml, "XmlTransmissions-" + Guid.NewGuid() + ".xml");
        }

        protected void OnXmlTransmissionResendClicked(object sender, EventArgs e)
        {
            var button = (ImageButton)sender;

            var transmissionId = (button.Parent.FindControl("hidXmlTransmissionId").ToCustomHiddenField()).Value.ToLong();

            var xmlTransmission = new XmlTransmission(transmissionId);
            if (!xmlTransmission.KeyLoaded)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Error loading transmission for resend") });
                return;
            }

            // DO NOT SEND 214 TO VENDOR!!!
            if (xmlTransmission.DocumentType == EdiDocumentType.EDI214.GetDocTypeString() && xmlTransmission.CommunicationReferenceType == CommunicationReferenceType.Vendor)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Resend operation is invalid: 214 to Vendor.") });
                return;
            }

            var newTrans = xmlTransmission.Duplicate(ActiveUser);

            Communication communication;

            switch (newTrans.CommunicationReferenceType)
            {
                case CommunicationReferenceType.Vendor:
                    communication = new VendorCommunication(newTrans.CommunicationReferenceId);
                    break;
                case CommunicationReferenceType.Customer:
                    communication = new CustomerCommunication(newTrans.CommunicationReferenceId);
                    break;
                default:
                    return;
            }

            Exception ex;
            var xmlTransHandler = new XmlTransmissionProcessHandler();

            xmlTransHandler.SaveTransmission(newTrans, out ex);
            if (ex != null) ErrorLogger.LogError(ex, HttpContext.Current);
            newTrans.TakeSnapShot();
            switch (newTrans.NotificationMethod)
            {
                case NotificationMethod.Edi:
                    newTrans.SendOkay = communication.SendEdi(string.Format("{0}.xml", newTrans.TransmissionKey),
                                                              this.GenerateEdiMessage(communication, newTrans).ToUtf8Bytes(),
                                                              newTrans.DocumentType);
                    break;
                case NotificationMethod.Ftp:
                    newTrans.SendOkay = communication.SendFtp(string.Format("{0}.xml", newTrans.TransmissionKey),
                                                              newTrans.Xml.ToUtf8Bytes(), newTrans.DocumentType);
                    break;
                default:
                    newTrans.SendOkay = false;
                    break;
            }


            xmlTransHandler.SaveTransmission(newTrans, out ex);
            DoSearch();
            if (ex == null) return;
            ErrorLogger.LogError(ex, HttpContext.Current);
            DisplayMessages(new[] { ValidationMessage.Error("Error resending transmission") });
        }

        protected void OnXmlTransmissionProcessClicked(object sender, ImageClickEventArgs e)
        {
            DisplayMessages(new[] { ValidationMessage.Information("Function not implemented at this time!") });
        }
    }
}