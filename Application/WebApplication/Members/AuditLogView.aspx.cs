﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Core;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Core;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members
{
    public partial class AuditLogView : MemberPageBase, IAuditLogView
    {
        private const string ExportFlag = "ExportFlag";

        protected const int DescriptionCharacterLimit = 1100;

        private string ResultFlag
        {
            get { return ViewState["ResultFlag"].GetString(); }
            set { ViewState["ResultFlag"] = value; }
        }

        public static string PageAddress
        {
            get { return "~/Members/AuditLogView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.AuditLog; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.UtilitiesBlue; }
        }

        public event EventHandler<ViewEventArgs<AuditLogViewSearchCriteria>> Search;

        public void DisplaySearchResult(List<AuditLogsViewSearchDto> auditLogs)
        {
            switch (ResultFlag)
            {
                case ExportFlag:
                    DoExport(auditLogs);
                    break;
                default:
                    foreach (var auditLog in auditLogs)
                        auditLog.Description = auditLog.Description.Length > DescriptionCharacterLimit
                                                   ? auditLog.Description.Substring(0, DescriptionCharacterLimit)
                                                   : auditLog.Description;
                    cseDataUpdate.DataSource = auditLogs;
                    cseDataUpdate.DataBind();
                    litRecordCount.Text = auditLogs.BuildRecordCount();
                    break;
            }
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak,
                                             messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems { Columns = GetCurrentRunParameters(true) };
        }

        private void DoExport(IEnumerable<AuditLogsViewSearchDto> auditLogs)
        {
            foreach (var auditLog in auditLogs)
                auditLog.Description = auditLog.Description.Replace(WebApplicationConstants.Tab, string.Empty);

            var q = auditLogs.Select(a => a.ToString().Replace(Environment.NewLine, string.Empty)).ToList();
            q.Insert(0, AuditLogsViewSearchDto.Header());
            Response.Export(q.ToArray().NewLineJoin());
        }



        private void DoSearchPreProcessingThenSearch()
        {
            DoSearch(new AuditLogViewSearchCriteria
            {
                Parameters = GetCurrentRunParameters(false)
            });
        }

        private void DoSearch(AuditLogViewSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<AuditLogViewSearchCriteria>(criteria));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new AuditLogHandler(this);
            handler.Initialize();

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);

            var columns = profile != null && profile.Columns != null
                              ? profile.Columns
                              : CoreSearchFields.DefaultAuditLogs.Select(f => f.ToParameterColumn()).ToList();

            lstFilterParameters.DataSource = columns;
            lstFilterParameters.DataBind();


        }

        protected void OnSearchClicked(object sender, EventArgs e)
        {
            cseDataUpdate.ResetLoadCount();
            ResultFlag = string.Empty;
            DoSearchPreProcessingThenSearch();
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            ResultFlag = ExportFlag;
            DoSearchPreProcessingThenSearch();
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                                                          .Select(
                                                              i =>
                                                              ((ReportRunParameterControl)i.FindControl("reportRunParameterControl"))
                                                                  .RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = CoreSearchFields.AuditLogs.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();
        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(CoreSearchFields.AuditLogs);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }



        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();

            DoSearchPreProcessingThenSearch();
        }


        [ScriptMethod]
        [WebMethod]
        public static object RetrieveEntityDetails(string entityCode, string entityId)
        {
            const string noLoad = "Record not found! Record may have been deleted.";
            string desc;
            var id = entityId.ToLong();
            bool canJumpTo = false;
            var activeUser = HttpContext.Current.RetrieveAuthenticatedUser();

            //Operations
            if (entityCode == new AddressBook().EntityName())
            {
                var addressbook = new AddressBook(id);
                desc = !addressbook.KeyLoaded ? noLoad : string.Format("Addressbook Description: {0}", addressbook.Description);
                canJumpTo = addressbook.KeyLoaded && activeUser.HasAccessTo(ViewCode.AddressBook);
            }
            else if (entityCode == new Asset().EntityName())
            {
                var asset = new Asset(id, false);
                desc = !asset.KeyLoaded ? noLoad : string.Format("Asset Number: {0}", asset.AssetNumber);
            }
            else if (entityCode == new AssetLog().EntityName())
            {
                var assetLog = new AssetLog(id, false);
                desc = !assetLog.KeyLoaded ? noLoad : string.Format("Asset Log Asset: {0} - {1}, Asset Type: {2}", assetLog.Asset.AssetNumber,
                                     assetLog.Asset.Description, assetLog.Asset.AssetType);
            }
            else if (entityCode == new Claim().EntityName())
            {
                var claim = new Claim(id, false);
                desc = !claim.KeyLoaded ? noLoad : string.Format("Claim Number: {0}", claim.ClaimNumber);
                canJumpTo = claim.KeyLoaded && activeUser.HasAccessTo(ViewCode.Claim);
            }
            else if (entityCode == new LoadOrder().EntityName())
            {
                var load = new LoadOrder(id);
                desc = !load.KeyLoaded ? noLoad : string.Format("Load Order Number: {0}", load.LoadOrderNumber);
                canJumpTo = load.KeyLoaded && activeUser.HasAccessTo(ViewCode.LoadOrder);
            }
            else if (entityCode == new VendorPreferredLane().EntityName())
            {
                var lane = new VendorPreferredLane(id);
                desc = !lane.KeyLoaded ? noLoad : string.Format("Vendor: {0} - {1},  Full Lane: {2}", lane.Vendor.VendorNumber, lane.Vendor.Name, lane.FullLane);
            }
            else if (entityCode == new VendorTerminal().EntityName())
            {
                var terminal = new VendorTerminal(id);
                desc = !terminal.KeyLoaded ? noLoad : string.Format("Vendor: {0} - {1},  Terminal: {2}", terminal.Vendor.VendorNumber, terminal.Vendor.Name,
                                     terminal.FullAddress);
            }
            else if (entityCode == new Claim().EntityName())
            {
                var claim = new Claim(id);
                desc = !claim.KeyLoaded ? noLoad : string.Format("Claim Number: {0}", claim.ClaimNumber);
            }
            else if (entityCode == new LibraryItem().EntityName())
            {
                var libraryItem = new LibraryItem(id);
                desc = !libraryItem.KeyLoaded ? noLoad : string.Format("Library Item Description: {0}</br>Customer: {1} - {2}", libraryItem.Description,
                                     libraryItem.Customer.CustomerNumber, libraryItem.Customer.Name);
            }
            else if (entityCode == new MacroPointOrder().EntityName())
            {
                var mpo = new MacroPointOrder(id);
                desc = !mpo.KeyLoaded ? noLoad : string.Format("Macro Point Order Id Number: {0}", mpo.IdNumber);
            }
            else if (entityCode == new ServiceTicket().EntityName())
            {
                var serviceTicket = new ServiceTicket(id);
                desc = !serviceTicket.KeyLoaded ? noLoad : string.Format("Service Ticket Number: {0}", serviceTicket.ServiceTicketNumber);
                canJumpTo = serviceTicket.KeyLoaded && activeUser.HasAccessTo(ViewCode.ServiceTicket);
            }
            else if (entityCode == new Shipment().EntityName())
            {
                var shipment = new Shipment(id);
                desc = !shipment.KeyLoaded ? noLoad : string.Format("Shipment Number: {0}", shipment.ShipmentNumber);
                canJumpTo = shipment.KeyLoaded && activeUser.HasAccessTo(ViewCode.LoadOrder);
            }
            else if (entityCode == new ShoppedRate().EntityName())
            {
                var shoppedRate = new ShoppedRate(id);
                desc = !shoppedRate.KeyLoaded ? noLoad : string.Format("Shopped Rate Reference: {0} Number - {1}", shoppedRate.Type, shoppedRate.ReferenceNumber);
            }

            //Finance
            else if (entityCode == new CommissionPayment().EntityName())
            {
                var commisionPayemnt = new CommissionPayment(id, false);
                desc = !commisionPayemnt.KeyLoaded ? noLoad : string.Format("Commission Payment Reference Shipment Number - {0}", commisionPayemnt.ReferenceNumber);
            }
            else if (entityCode == new Customer().EntityName())
            {
                var customer = new Customer(id);
                desc = !customer.KeyLoaded ? noLoad : string.Format("Customer Number: {0}", customer.CustomerNumber);
                canJumpTo = customer.KeyLoaded && activeUser.HasAccessTo(ViewCode.Customer);
            }
            else if (entityCode == new CustomerControlAccount().EntityName())
            {
                var customerControlAccount = new CustomerControlAccount(id, false);
                desc = !customerControlAccount.KeyLoaded ? noLoad : string.Format("Account Number: {0}</br> Customer Number: {1}", customerControlAccount.AccountNumber,
                                     customerControlAccount.Customer.CustomerNumber);
            }
            else if (entityCode == new CustomerPurchaseOrder().EntityName())
            {
                var customerPurchaseOrder = new CustomerPurchaseOrder(id, false);
                desc = !customerPurchaseOrder.KeyLoaded ? noLoad : string.Format("Customer Purchase Order Number: {0}", customerPurchaseOrder.PurchaseOrderNumber);
            }
            else if (entityCode == new Invoice().EntityName())
            {
                var invoice = new Invoice(id);
                desc = !invoice.KeyLoaded ? noLoad : string.Format("Invoice Number: {0}", invoice.InvoiceNumber);
                canJumpTo = invoice.KeyLoaded && activeUser.HasAccessTo(ViewCode.Invoice);
            }
            else if (entityCode == new MiscReceipt().EntityName())
            {
                var miscReceipt = new MiscReceipt(id, false);
                desc = !miscReceipt.KeyLoaded ? noLoad : string.Format("Misc Receipt for Shipment Number: {0}<br/> Gateway Transaction Id: {1}", (miscReceipt.Shipment ?? new Shipment()).ShipmentNumber, miscReceipt.GatewayTransactionId);
            }
            else if (entityCode == new MiscReceiptApplication().EntityName())
            {
                var application = new MiscReceiptApplication(id, false);
                desc = !application.KeyLoaded ? noLoad : string.Format("Misc Receipt Application Invoice Number: {0}", application.Invoice.InvoiceNumber);
            }
            else if (entityCode == new SalesRepresentative().EntityName())
            {
                var salesRepresentative = new SalesRepresentative(id, false);
                desc = !salesRepresentative.KeyLoaded ? noLoad : string.Format("Sales Representative Number: {0}", salesRepresentative.SalesRepresentativeNumber);
            }
            else if (entityCode == new Tier().EntityName())
            {
                var tier = new Tier(id);
                desc = !tier.KeyLoaded ? noLoad : string.Format("Tier Number: {0}", tier.TierNumber);
                canJumpTo = tier.KeyLoaded && activeUser.HasAccessTo(ViewCode.Tier);
            }
            else if (entityCode == new Vendor().EntityName())
            {
                var vendor = new Vendor(id);
                desc = !vendor.KeyLoaded ? noLoad : string.Format("Vendor Number: {0}", vendor.VendorNumber);
                canJumpTo = vendor.KeyLoaded && activeUser.HasAccessTo(ViewCode.Vendor);
            }
            else if (entityCode == new VendorBill().EntityName())
            {
                var vendorBill = new VendorBill(id);
                desc = !vendorBill.KeyLoaded ? noLoad : string.Format("Vendor Bill Document Number: {0}", vendorBill.DocumentNumber);
                canJumpTo = vendorBill.KeyLoaded && activeUser.HasAccessTo(ViewCode.VendorBill);
            }

            //Registry
            else if (entityCode == new AccountBucket().EntityName())
            {
                var accountBucket = new AccountBucket(id);
                desc = !accountBucket.KeyLoaded ? noLoad : string.Format("Account Bucket Code: {0}", accountBucket.Code);
            }
            else if (entityCode == new AccountBucketUnit().EntityName())
            {
                var accountBucketUnit = new AccountBucketUnit(id);
                desc = !accountBucketUnit.KeyLoaded ? noLoad : string.Format("Account Bucket Unit Name: {0}", accountBucketUnit.Name);
            }
            else if (entityCode == new ChargeCode().EntityName())
            {
                var chargeCode = new ChargeCode(id);
                desc = !chargeCode.KeyLoaded ? noLoad : string.Format("Charge Code: {0}", chargeCode.FormattedString());
            }
            else if (entityCode == new ContactType().EntityName())
            {
                var contactType = new ContactType(id, false);
                desc = !contactType.KeyLoaded ? noLoad : string.Format("Contact Type: {0}", contactType.Code);
            }
            else if (entityCode == new DocumentTag().EntityName())
            {
                var documentTag = new DocumentTag(id);
                desc = !documentTag.KeyLoaded ? noLoad : string.Format("Document Tag: {0}", documentTag.FormattedString());
            }
            else if (entityCode == new DocumentTemplate().EntityName())
            {
                var documentTemplate = new DocumentTemplate(id, false);
                desc = !documentTemplate.KeyLoaded ? noLoad : string.Format("Document Template Code: {0}", documentTemplate.Code);
            }
            else if (entityCode == new EquipmentType().EntityName())
            {
                var equipmentType = new EquipmentType(id);
                desc = !equipmentType.KeyLoaded ? noLoad : string.Format("Equipment Type: {0}", equipmentType.FormattedString());
            }
            else if (entityCode == new FailureCode().EntityName())
            {
                var failureCode = new FailureCode(id);
                desc = !failureCode.KeyLoaded ? noLoad : string.Format("Failure Code: {0}", failureCode.FormattedString());
            }
            else if (entityCode == new InsuranceType().EntityName())
            {
                var insuranceType = new InsuranceType(id, false);
                desc = !insuranceType.KeyLoaded ? noLoad : string.Format("Insurance Type: {0}", insuranceType.FormattedString());
            }
            else if (entityCode == new MileageSource().EntityName())
            {
                var mileageSource = new MileageSource(id);
                desc = !mileageSource.KeyLoaded ? noLoad : string.Format("Mileage Source: {0}", mileageSource.FormattedString());
            }
            else if (entityCode == new NoServiceDay().EntityName())
            {
                var noServiceDay = new NoServiceDay(id);
                desc = !noServiceDay.KeyLoaded ? noLoad : string.Format("No Service Day Date of No Service: {0}",
                                     noServiceDay.DateOfNoService.ToShortDateString());
            }
            else if (entityCode == new PackageType().EntityName())
            {
                var packageType = new PackageType(id);
                desc = !packageType.KeyLoaded ? noLoad : string.Format("Package Type Name: {0}", packageType.TypeName);
            }
            else if (entityCode == new Prefix().EntityName())
            {
                var prefix = new Prefix(id);
                desc = !prefix.KeyLoaded ? noLoad : string.Format("Prefix Code: {0}", prefix.Code);
            }
            else if (entityCode == new QuickPayOption().EntityName())
            {
                var quickPayOption = new QuickPayOption(id, false);
                desc = !quickPayOption.KeyLoaded ? noLoad : string.Format("Quick Pay Option Code: {0}", quickPayOption.Code);
            }
            else if (entityCode == new Service().EntityName())
            {
                var service = new Service(id);
                desc = !service.KeyLoaded ? noLoad : string.Format("Service Code: {0}", service.Code);
            }
            else if (entityCode == new Setting().EntityName())
            {
                var setting = new Setting(id, false);
                desc = !setting.KeyLoaded ? noLoad : string.Format("Setting Code: {0}", setting.Code);
            }
            else if (entityCode == new ShipmentPriority().EntityName())
            {
                var shipmentPriority = new ShipmentPriority(id, false);
                desc = !shipmentPriority.KeyLoaded ? noLoad : string.Format("Shipment Priority: {0}", shipmentPriority.FormattedString());
            }

            //Rating
            else if (entityCode == new BatchRateData().EntityName())
            {
                var batchRateData = new BatchRateData(id);
                desc = !batchRateData.KeyLoaded ? noLoad : string.Format("Batch Rate Data Name: {0}", batchRateData.Name);
            }
            else if (entityCode == new CustomerRating().EntityName())
            {
                var customerRating = new CustomerRating(id);
                desc = !customerRating.KeyLoaded ? noLoad : string.Format("Customer Rating Customer: {0} - {1}", customerRating.Customer.CustomerNumber,
                                     customerRating.Customer.Name);
                canJumpTo = customerRating.KeyLoaded && activeUser.HasAccessTo(ViewCode.CustomerRating);
            }
            else if (entityCode == new FuelTable().EntityName())
            {
                var fuelTable = new FuelTable(id);
                desc = !fuelTable.KeyLoaded ? noLoad : string.Format("Fuel Table Name: {0}", fuelTable.Name);
            }
            else if (entityCode == new Region().EntityName())
            {
                var region = new Region(id);
                desc = !region.KeyLoaded ? noLoad : string.Format("Region Name: {0}", region.Name);
                canJumpTo = region.KeyLoaded && activeUser.HasAccessTo(ViewCode.Region);
            }
            else if (entityCode == new ResellerAddition().EntityName())
            {
                var resellerAddition = new ResellerAddition(id);
                desc = !resellerAddition.KeyLoaded ? noLoad : string.Format("Reseller Addition Name: {0}", resellerAddition.Name);
                canJumpTo = resellerAddition.KeyLoaded && activeUser.HasAccessTo(ViewCode.ResellerAddition);
            }
            else if (entityCode == new VendorRating().EntityName())
            {
                var vendorRating = new VendorRating(id, false);
                desc = !vendorRating.KeyLoaded ? noLoad : string.Format("Vendor Rating Vendor: {0} - {1}", vendorRating.Vendor.VendorNumber,
                                     vendorRating.Vendor.Name);
                canJumpTo = vendorRating.KeyLoaded && activeUser.HasAccessTo(ViewCode.VendorRating);
            }
            else if (entityCode == new Group().EntityName())
            {
                var group = new Group(id);
                desc = !group.KeyLoaded ? noLoad : string.Format("Group Name: {0} ", group.Name);
                canJumpTo = group.KeyLoaded && activeUser.HasAccessTo(ViewCode.Group);
            }
            else if (entityCode == new Tenant().EntityName())
            {
                var tenant = new Tenant(id);
                desc = !tenant.KeyLoaded ? noLoad : string.Format("Tenant Name: {0} ", tenant.Name);
            }
            else if (entityCode == new User().EntityName())
            {
                var user = new User(id);
                desc = !user.KeyLoaded ? noLoad : string.Format("User Username: {0} ", user.Username);
                canJumpTo = user.KeyLoaded && activeUser.HasAccessTo(ViewCode.User);
            }

            //BI
            else if (entityCode == new CustomerGroup().EntityName())
            {
                var customerGroup = new CustomerGroup(id);
                desc = !customerGroup.KeyLoaded ? noLoad : string.Format("Customer Group Name: {0} ", customerGroup.Name);
                canJumpTo = customerGroup.KeyLoaded && activeUser.HasAccessTo(ViewCode.CustomerGroup);
            }
            else if (entityCode == new QlikUser().EntityName())
            {
                var qlikUser = new QlikUser(entityId);
                desc = !qlikUser.KeyLoaded ? noLoad : string.Format("Qlik User Id: {0} ", qlikUser.UserId);
            }
            else if (entityCode == new ReportConfiguration().EntityName())
            {
                var reportConfiguration = new ReportConfiguration(id);
                desc = !reportConfiguration.KeyLoaded ? noLoad : string.Format("Report Configuration Name: {0} ", reportConfiguration.Name);
                canJumpTo = reportConfiguration.KeyLoaded && activeUser.HasAccessTo(ViewCode.ReportConfiguration);
            }
            else if (entityCode == new ReportSchedule().EntityName())
            {
                var reportSchedule = new ReportSchedule(id);
                desc = !reportSchedule.KeyLoaded ? noLoad : string.Format("Report Schedule Configuration Name: {0} ",
                                     reportSchedule.ReportConfiguration.Name);
                canJumpTo = reportSchedule.KeyLoaded && activeUser.HasAccessTo(ViewCode.ReportScheduler);
            }
            else if (entityCode == new VendorGroup().EntityName())
            {
                var vendorGroup = new VendorGroup(id);
                desc = !vendorGroup.KeyLoaded ? noLoad : string.Format("Vendor Group Name: {0} ", vendorGroup.Name);
                canJumpTo = vendorGroup.KeyLoaded && activeUser.HasAccessTo(ViewCode.VendorGroup);
            }

            //Connect
            else if (entityCode == new CustomerCommunication().EntityName())
            {
                var customerCommunication = new CustomerCommunication(id);
                desc = !customerCommunication.KeyLoaded ? noLoad : string.Format("Customer Communication Customer: {0} - {1} ",
                                     customerCommunication.Customer.CustomerNumber,
                                     customerCommunication.Customer.Name);
                canJumpTo = customerCommunication.KeyLoaded && activeUser.HasAccessTo(ViewCode.CustomerCommunication);
            }
            else if (entityCode == new VendorCommunication().EntityName())
            {
                var vendorCommunication = new VendorCommunication(id);
                desc = !vendorCommunication.KeyLoaded ? noLoad : string.Format("Vendor Communication Customer: {0} - {1} ",
                                     vendorCommunication.Vendor.VendorNumber,
                                     vendorCommunication.Vendor.Name);
                canJumpTo = vendorCommunication.KeyLoaded && activeUser.HasAccessTo(ViewCode.VendorCommunication);
            }
            else if (entityCode == new XmlConnect().EntityName())
            {
                var xmlConnect = new XmlConnect(id);
                desc = !xmlConnect.KeyLoaded ? noLoad : string.Format("Xml Connect Record Document Type: {0}<br/>Shipment Number: {1} ",
                                     xmlConnect.DocumentType,
                                     xmlConnect.ShipmentIdNumber);
                canJumpTo = xmlConnect.KeyLoaded && activeUser.HasAccessTo(ViewCode.XmlConnectRecord);
            }
            //Utilities
            else if (entityCode == new Announcement().EntityName())
            {
                var announcement = new Announcement(id, false);
                desc = !announcement.KeyLoaded ? noLoad : string.Format("Announcement Name: {0} ", announcement.Name);
            }
            else if (entityCode == new DeveloperAccessRequest().EntityName())
            {
                var developerAccessRequest = new DeveloperAccessRequest(id);
                desc = !developerAccessRequest.KeyLoaded ? noLoad :
                    string.Format(
                        "Developer Access Request Contact Name: {0}</br>Customer:{1} - {2} ",
                        developerAccessRequest.ContactName, developerAccessRequest.Customer.CustomerNumber,
                        developerAccessRequest.Customer.Name);
                canJumpTo = developerAccessRequest.KeyLoaded && activeUser.HasAccessTo(ViewCode.DeveloperAccessRequestListing);
            }
            // Super User
            else if (entityCode == new AdminUser().EntityName())
            {
                var user = new AdminUser(id);
                desc = !user.KeyLoaded ? noLoad : string.Format("Admin User Username: {0} ", user.Username);
            }
            else if (entityCode == new Country().EntityName())
            {
                var country = new Country(id);
                desc = !country.KeyLoaded ? noLoad : string.Format("Country Name: {0} ", country.Name);
            }
            else if (entityCode == new ReportTemplate().EntityName())
            {
                var reportTemplate = new ReportTemplate(id);
                desc = !reportTemplate.KeyLoaded ? noLoad : string.Format("Report Template Name: {0} ", reportTemplate.Name);
            }
            else if (entityCode == new PostalCode().EntityName())
            {
                var postalCode = new PostalCode(id);
                desc = !postalCode.KeyLoaded ? noLoad : string.Format("Postal Code: {0} ", postalCode.Code);
            }
            else
            {
                desc = string.Format("Entity: {0} with Id: {1} Does not exist.", entityCode, id);
            }

            return new
            {
                Desc = desc,
                CanJumpTo = canJumpTo.GetString(),
                EntityCode = entityCode,
                EntityId = entityId
            };
        }

        protected void OnGoToRecord(object sender, EventArgs e)
        {
            this.GoToRecord2(hidGoToEntityCode.Value, hidGoToEntityId.Value);
        }


        [ScriptMethod]
        [WebMethod]
        public static object RetrieveFullDescription(string id)
        {
            var auditLog = new AuditLog(id.ToLong());
            return auditLog.KeyLoaded ? auditLog.Description.Replace(Environment.NewLine, "<br/>") : "Audit Log record does not exist";
        }
    }
}