﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.WebApplication.Members.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.Administration;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence;
using LogisticsPlus.Eship.WebApplication.Members.Connect;
using LogisticsPlus.Eship.WebApplication.Members.MassUpload.ShipmentDocuments;
using LogisticsPlus.Eship.WebApplication.Members.Operations;
using LogisticsPlus.Eship.WebApplication.Members.Rating;
using LogisticsPlus.Eship.WebApplication.Members.Registry;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members
{
	public class DashboardItemConfiguration
	{
		public string IconName { get; set; }

		public string DisplayName { get; set; }

		public string IconUrl { get; set; }

		public string IconImageLocation { get { return BuildIconImageLocationUrl(IconName); } }

		public ViewCode ViewCode { get; set; }

		public bool OpenInNewWindow { get; set; }

		public DashboardItemConfiguration()
		{
			IconName = string.Empty;
			DisplayName = string.Empty;
			IconUrl = string.Empty;
			OpenInNewWindow = false;
			ViewCode = ViewCode.DashboardPadding;		
		}

		public DashboardItemConfiguration(string name, string url, ViewCode viewCode, bool openInNewWindow = false)
		{
			IconName = name;
			DisplayName = viewCode.FormattedString();
			IconUrl = url;
			ViewCode = viewCode;
			OpenInNewWindow = openInNewWindow;
		}

		public DashboardItemConfiguration(string name, string url, string displayName, bool openInNewWindow = true)
		{
			IconName = name;
			DisplayName = displayName;
			IconUrl = url;
			ViewCode = ViewCode.Null;
			OpenInNewWindow = openInNewWindow;
		}

		public static string BuildIconImageLocationUrl(string iconName)
		{
			return string.IsNullOrEmpty(iconName)
			       	? string.Empty
			       	: string.Format("~/images/icons/dashboardIcons/{0}.png", iconName);
		}

		/* v4.2.x.x items */

		public static List<DashboardItemConfiguration> Operations2(Tenant tenant = null)
		{
			var configurations = new List<DashboardItemConfiguration>
						{
							new DashboardItemConfiguration(ViewCode.AddressBook.ToString(), AddressBookView.PageAddress, ViewCode.AddressBook),
							new DashboardItemConfiguration(ViewCode.Asset.ToString(), AssetView.PageAddress, ViewCode.Asset),
							new DashboardItemConfiguration(ViewCode.AssetLog.ToString(), AssetLogView.PageAddress, ViewCode.AssetLog),
							new DashboardItemConfiguration(ViewCode.CarrierTerminal.ToString(), CarrierTerminalView.PageAddress, ViewCode.CarrierTerminal),
							new DashboardItemConfiguration(ViewCode.CarrierPreferredLane.ToString(), CarrierPreferredLaneView.PageAddress, ViewCode.CarrierPreferredLane),
							new DashboardItemConfiguration(ViewCode.Claim.ToString(), ClaimView.PageAddress, ViewCode.Claim),
							new DashboardItemConfiguration(ViewCode.ClaimDashboard.ToString(), ClaimDashboardView.PageAddress, ViewCode.ClaimDashboard),
                            new DashboardItemConfiguration(ViewCode.CustomerLoadDashboard.ToString(), CustomerLoadDashboardView.PageAddress, ViewCode.CustomerLoadDashboard),
                            new DashboardItemConfiguration(ViewCode.DatLoadboard.ToString(), DatLoadboardAssetView.PageAddress, ViewCode.DatLoadboard),
                            new DashboardItemConfiguration(ViewCode.DispatchDashboard.ToString(), DispatchDashboardView.PageAddress, ViewCode.DispatchDashboard),
							new DashboardItemConfiguration(ViewCode.EstimateRate.ToString(), EstimateRateView.PageAddress, ViewCode.EstimateRate),
                            new DashboardItemConfiguration(ViewCode.Job.ToString(), JobView.PageAddress, ViewCode.Job),
                            new DashboardItemConfiguration(ViewCode.JobDashboard.ToString(), JobDashboardView.PageAddress, ViewCode.JobDashboard),
                            new DashboardItemConfiguration(ViewCode.LibraryItem.ToString(), LibraryItemView.PageAddress, ViewCode.LibraryItem),
                            new DashboardItemConfiguration(ViewCode.RateAndSchedule.ToString(), RateAndScheduleView.PageAddress, ViewCode.RateAndSchedule),
                            new DashboardItemConfiguration(ViewCode.ServiceTicket.ToString(), ServiceTicketView.PageAddress, ViewCode.ServiceTicket),
                            new DashboardItemConfiguration(ViewCode.ServiceTicketDashboard.ToString(), ServiceTicketDashboardView.PageAddress, ViewCode.ServiceTicketDashboard),
                            new DashboardItemConfiguration(ViewCode.Shipment.ToString(), ShipmentView.PageAddress, ViewCode.Shipment),
                            new DashboardItemConfiguration(ViewCode.ShipmentDashboard.ToString(), ShipmentDashboardView.PageAddress, ViewCode.ShipmentDashboard),
                            new DashboardItemConfiguration(ViewCode.PendingPickupsDashboardLTL.ToString(), PendingLtlPickupsDashboardView.PageAddress, ViewCode.PendingPickupsDashboardLTL),
							new DashboardItemConfiguration(ViewCode.ShoppedRate.ToString(), ShoppedRateView.PageAddress, ViewCode.ShoppedRate),
                            new DashboardItemConfiguration(ViewCode.TruckloadQuote.ToString(), TruckloadQuoteView.PageAddress, ViewCode.TruckloadQuote),
							new DashboardItemConfiguration(ViewCode.LoadOrder.ToString(), LoadOrderView.PageAddress, ViewCode.LoadOrder),
                        };

			if (tenant != null) UpdateWithUserDefinedPermissions(configurations, tenant, PermissionCategory.Operations);

            return configurations.OrderBy(c => c.DisplayName.ToString()).ToList();
		}

		public static List<DashboardItemConfiguration> Accounting2(Tenant tenant = null)
		{
			var configurations = new List<DashboardItemConfiguration> 
						{
							
							new DashboardItemConfiguration(ViewCode.AverageWeeklyFuel.ToString(), AverageWeeklyFuelView.PageAddress, ViewCode.AverageWeeklyFuel),
                            new DashboardItemConfiguration(ViewCode.BatchPaymentApplication.ToString(), BatchPaymentApplicationView.PageAddress, ViewCode.BatchPaymentApplication),
                            new DashboardItemConfiguration(ViewCode.CommissionsToBePaid.ToString(), CommissionsToBePaidView.PageAddress, ViewCode.CommissionsToBePaid),
							new DashboardItemConfiguration(ViewCode.Customer.ToString(), CustomerView.PageAddress, ViewCode.Customer), 
							new DashboardItemConfiguration(ViewCode.CustomerControlAccount.ToString(), CustomerControlAccountView.PageAddress, ViewCode.CustomerControlAccount), 
							new DashboardItemConfiguration(ViewCode.CustomerInvoiceDashboard.ToString(), CustomerInvoicesDashboardView.PageAddress, ViewCode.CustomerInvoiceDashboard),
                            new DashboardItemConfiguration(ViewCode.CustomerPaymentProfileManagement.ToString(), CustomerPaymentProfileManagementView.PageAddress, ViewCode.CustomerPaymentProfileManagement),
                            new DashboardItemConfiguration(ViewCode.CustomerPurchaseOrder.ToString(), CustomerPurchaseOrderView.PageAddress, ViewCode.CustomerPurchaseOrder),
                            new DashboardItemConfiguration(ViewCode.DocumentPackDownload.ToString(), DocumentPackDownloadView.PageAddress, ViewCode.DocumentPackDownload),
							new DashboardItemConfiguration(ViewCode.Invoice.ToString(), InvoiceView.PageAddress, ViewCode.Invoice),
							new DashboardItemConfiguration(ViewCode.InvoiceControlAccountUpdate.ToString(), InvoiceControlAccountUpdateView.PageAddress, ViewCode.InvoiceControlAccountUpdate),
							new DashboardItemConfiguration(ViewCode.InvoiceDashboard.ToString(), InvoiceDashboardView.PageAddress, ViewCode.InvoiceDashboard),
							new DashboardItemConfiguration(ViewCode.InvoicePaidAmountUpdate.ToString(), InvoicePaidAmountUpdateView.PageAddress, ViewCode.InvoicePaidAmountUpdate),
							new DashboardItemConfiguration(ViewCode.InvoicePosting.ToString(), InvoicePostingView.PageAddress, ViewCode.InvoicePosting),
							new DashboardItemConfiguration(ViewCode.MiscReceiptDashboard.ToString(), MiscReceiptDashboardView.PageAddress, ViewCode.MiscReceiptDashboard),
							new DashboardItemConfiguration(ViewCode.PendingVendor.ToString(), PendingVendorView.PageAddress, ViewCode.PendingVendor),
							new DashboardItemConfiguration(ViewCode.SalesRepresentative.ToString(), SalesRepresentativeView.PageAddress, ViewCode.SalesRepresentative),
							new DashboardItemConfiguration(ViewCode.ShipmentsToBeInvoiced.ToString(), ShipmentsToBeInvoicedView.PageAddress, ViewCode.ShipmentsToBeInvoiced),
							new DashboardItemConfiguration(ViewCode.Tier.ToString(), TierView.PageAddress, ViewCode.Tier),
							new DashboardItemConfiguration(ViewCode.Vendor.ToString(), VendorView.PageAddress, ViewCode.Vendor),
							new DashboardItemConfiguration(ViewCode.VendorBill.ToString(), VendorBillView.PageAddress, ViewCode.VendorBill),
							new DashboardItemConfiguration(ViewCode.VendorBillPosting.ToString(), VendorBillPostingView.PageAddress, ViewCode.VendorBillPosting),
							new DashboardItemConfiguration(ViewCode.VendorBillDashboard.ToString(), VendorBillsDashboardView.PageAddress, ViewCode.VendorBillDashboard),
							new DashboardItemConfiguration(ViewCode.VendorChargesConfirmation.ToString(), VendorChargeConfirmation.PageAddress, ViewCode.VendorChargesConfirmation),
							new DashboardItemConfiguration(ViewCode.VendorDispute.ToString(), VendorDisputeView.PageAddress, ViewCode.VendorDispute),
						};

			if (tenant != null) UpdateWithUserDefinedPermissions(configurations, tenant, PermissionCategory.Accounting);

			return configurations.OrderBy(c => c.DisplayName.ToString()).ToList();
		}

		public static List<DashboardItemConfiguration> BusinessIntelligence2(Tenant tenant = null)
		{
			var configurations = new List<DashboardItemConfiguration>
						{
                            new DashboardItemConfiguration(ViewCode.CustomerGroup.ToString(), CustomerGroupView.PageAddress, ViewCode.CustomerGroup),
                            new DashboardItemConfiguration(ViewCode.ReportConfiguration.ToString(), ReportConfigurationView.PageAddress, ViewCode.ReportConfiguration),
                            new DashboardItemConfiguration(ViewCode.ReportRun.ToString(), ReportRunView.PageAddress, ViewCode.ReportRun),
						    new DashboardItemConfiguration(ViewCode.ReportRun.ToString(), ReportRunReadyView.PageAddress, "Ready Report Dashboard", false),
                            new DashboardItemConfiguration(ViewCode.ReportScheduleDashboard.ToString(), ReportScheduleDashboardView.PageAddress, ViewCode.ReportScheduleDashboard),
                            new DashboardItemConfiguration(ViewCode.ReportScheduler.ToString(), ReportSchedulerView.PageAddress, ViewCode.ReportScheduler),
                            new DashboardItemConfiguration(ViewCode.VendorGroup.ToString(), VendorGroupView.PageAddress, ViewCode.VendorGroup),
                            new DashboardItemConfiguration(ViewCode.VendorLaneRateHistory.ToString(), VendorLaneRateHistoryView.PageAddress, ViewCode.VendorLaneRateHistory),
                            new DashboardItemConfiguration(ViewCode.VendorPerformanceSummary.ToString(), VendorPerformanceSummaryView.PageAddress, ViewCode.VendorPerformanceSummary),
                            new DashboardItemConfiguration(ViewCode.VendorTerminalLocator.ToString(), VendorTerminalLocatorView.PageAddress, ViewCode.VendorTerminalLocator),
                        };

			if (tenant != null) UpdateWithUserDefinedPermissions(configurations, tenant, PermissionCategory.BusinessIntelligence);

			return configurations.OrderBy(c => c.DisplayName.ToString()).ToList();
		}

		public static List<DashboardItemConfiguration> Registry2(Tenant tenant = null)
		{
			var configurations = new List<DashboardItemConfiguration> 
						{
							new DashboardItemConfiguration(ViewCode.AccountBucket.ToString(), AccountBucketView.PageAddress, ViewCode.AccountBucket),
							new DashboardItemConfiguration(ViewCode.ChargeCode.ToString(), ChargeCodeView.PageAddress, ViewCode.ChargeCode),
							new DashboardItemConfiguration(ViewCode.ContactType.ToString(), ContactTypeView.PageAddress, ViewCode.ContactType),
							new DashboardItemConfiguration(ViewCode.DocumentPrefixMap.ToString(), DocumentPrefixMapView.PageAddress, ViewCode.DocumentPrefixMap),
							new DashboardItemConfiguration(ViewCode.DocumentTag.ToString(), DocumentTagView.PageAddress, ViewCode.DocumentTag),
							new DashboardItemConfiguration(ViewCode.DocumentTemplate.ToString(), DocumentTemplateView.PageAddress, ViewCode.DocumentTemplate),
							new DashboardItemConfiguration(ViewCode.EquipmentType.ToString(), EquipmentTypeView.PageAddress, ViewCode.EquipmentType),
							new DashboardItemConfiguration(ViewCode.FailureCode.ToString(), FailureCodeView.PageAddress, ViewCode.FailureCode),
							new DashboardItemConfiguration(ViewCode.InsuranceType.ToString(), InsuranceTypeView.PageAddress, ViewCode.InsuranceType),
							new DashboardItemConfiguration(ViewCode.MileageSource.ToString(), MileageSourceView.PageAddress, ViewCode.MileageSource),
							new DashboardItemConfiguration(ViewCode.YearMonthlyBusinessDay.ToString(), YrMthBusDayView.PageAddress, ViewCode.YearMonthlyBusinessDay),
							new DashboardItemConfiguration(ViewCode.PackageType.ToString(), PackageTypeView.PageAddress, ViewCode.PackageType),
							new DashboardItemConfiguration(ViewCode.Prefix.ToString(), PrefixView.PageAddress, ViewCode.Prefix),
							new DashboardItemConfiguration(ViewCode.QuickPayOption.ToString(), QuickPayOptionView.PageAddress, ViewCode.QuickPayOption),
							new DashboardItemConfiguration(ViewCode.Service.ToString(), ServiceView.PageAddress, ViewCode.Service),
                            new DashboardItemConfiguration(ViewCode.Project44ChargeCodeMapping.ToString(), P44ChargeCodeView.PageAddress, ViewCode.Project44ChargeCodeMapping),
                            new DashboardItemConfiguration(ViewCode.Project44ServiceMapping.ToString(), P44ServiceMappingView.PageAddress, ViewCode.Project44ServiceMapping),
                            new DashboardItemConfiguration(ViewCode.Setting.ToString(), SettingView.PageAddress, ViewCode.Setting),
							new DashboardItemConfiguration(ViewCode.ShipmentPriority.ToString(), ShipmentPriorityView.PageAddress, ViewCode.ShipmentPriority),
							new DashboardItemConfiguration(ViewCode.UserDefinedPermissionDetail.ToString(), UserDefinedPermissionDetailView.PageAddress, ViewCode.UserDefinedPermissionDetail),
							new DashboardItemConfiguration(ViewCode.UserDepartment.ToString(), UserDepartmentView.PageAddress, ViewCode.UserDepartment)
						};

			if (tenant != null) UpdateWithUserDefinedPermissions(configurations, tenant, PermissionCategory.Registry);

			return configurations.OrderBy(c => c.DisplayName.ToString()).ToList();
		}

		public static List<DashboardItemConfiguration> Utilities2(Tenant tenant = null)
		{
			var configurations = new List<DashboardItemConfiguration> 
						{
							new DashboardItemConfiguration(ViewCode.Announcement.ToString(), AnnouncementView.PageAddress, ViewCode.Announcement),
							new DashboardItemConfiguration(ViewCode.AuditLog.ToString(), AuditLogView.PageAddress, ViewCode.AuditLog),
							new DashboardItemConfiguration(ViewCode.DeveloperAccessRequest.ToString(), DeveloperAccessRequestView.PageAddress, ViewCode.DeveloperAccessRequest),
							new DashboardItemConfiguration(ViewCode.DeveloperAccessRequestListing.ToString(), DeveloperAccessRequestListingView.PageAddress, ViewCode.DeveloperAccessRequestListing),
							new DashboardItemConfiguration(ViewCode.EmailLog.ToString(), EmailLogView.PageAddress, ViewCode.EmailLog),
							new DashboardItemConfiguration(ViewCode.ShipmentMassDocumentUpload.ToString(), ShipmentMassDocumentUploadView.PageAddress, ViewCode.ShipmentMassDocumentUpload),
							new DashboardItemConfiguration(ViewCode.VendorProMassUpdate.ToString(), VendorProMassUpdateView.PageAddress, ViewCode.VendorProMassUpdate)
						};

			if (tenant != null) UpdateWithUserDefinedPermissions(configurations, tenant, PermissionCategory.Utilities);

			return configurations.OrderBy(c => c.DisplayName.ToString()).ToList();
		}

		public static List<DashboardItemConfiguration> Administration2(Tenant tenant = null)
		{
			var configurations = new List<DashboardItemConfiguration> 
						{
							new DashboardItemConfiguration(ViewCode.ExistingRecordLocks.ToString(), ExistingRecordLockView.PageAddress, ViewCode.ExistingRecordLocks),
							new DashboardItemConfiguration(ViewCode.Group.ToString(), GroupView.PageAddress, ViewCode.Group),
							new DashboardItemConfiguration(ViewCode.ManageUploadedImage.ToString(), ManageUploadedImageView.PageAddress, ViewCode.ManageUploadedImage),
							new DashboardItemConfiguration(ViewCode.TenantAdmin.ToString(), TenantAdministrationView.PageAddress, ViewCode.TenantAdmin),
							new DashboardItemConfiguration(ViewCode.User.ToString(), UserView.PageAddress, ViewCode.User),
							new DashboardItemConfiguration(ViewCode.UserImpersonation.ToString(), UserImpersonationView.PageAddress, ViewCode.UserImpersonation),
						};

			if (tenant != null) UpdateWithUserDefinedPermissions(configurations, tenant, PermissionCategory.Administration);

			return configurations.OrderBy(c => c.DisplayName.ToString()).ToList();
		}

		public static List<DashboardItemConfiguration> Sales2(Tenant tenant = null)
		{
			var configurations = new List<DashboardItemConfiguration>();

			return configurations.OrderBy(c => c.DisplayName.ToString()).ToList();
		}

		public static List<DashboardItemConfiguration> Rating2(Tenant tenant = null)
		{
			var configurations = new List<DashboardItemConfiguration>
						{
							new DashboardItemConfiguration(ViewCode.CustomerRating.ToString(), CustomerRatingView.PageAddress, ViewCode.CustomerRating),
							new DashboardItemConfiguration(ViewCode.TruckloadTenderingProfile.ToString(), CustomerTLTenderingProfileView.PageAddress, ViewCode.TruckloadTenderingProfile),
							new DashboardItemConfiguration(ViewCode.FuelTable.ToString(), FuelTableView.PageAddress, ViewCode.FuelTable),
							new DashboardItemConfiguration(ViewCode.RateAnalysisLTL.ToString(), LTLRateAnalysisView.PageAddress, ViewCode.RateAnalysisLTL),
							new DashboardItemConfiguration(ViewCode.RateAnalysisSmallPack.ToString(), SmallPackRateAnalysisView.PageAddress, ViewCode.RateAnalysisSmallPack),
							new DashboardItemConfiguration(ViewCode.Region.ToString(), RegionView.PageAddress, ViewCode.Region),
							new DashboardItemConfiguration(ViewCode.ResellerAddition.ToString(), ResellerAdditionView.PageAddress, ViewCode.ResellerAddition),
							new DashboardItemConfiguration(ViewCode.SmallPackagingMaps.ToString(), SmallPackagingMapView.PageAddress, ViewCode.SmallPackagingMaps),
                            new DashboardItemConfiguration(ViewCode.SmallPackageServiceMaps.ToString(), SmallPackageServiceMapView.PageAddress, ViewCode.SmallPackageServiceMaps),
                            new DashboardItemConfiguration(ViewCode.SmcAvailableCarriers.ToString(), SmcAvailableCarriersView.PageAddress, ViewCode.SmcAvailableCarriers),
                            new DashboardItemConfiguration(ViewCode.SmcPointToPointServiceCheck.ToString(), SmcPointToPointServiceView.PageAddress, ViewCode.SmcPointToPointServiceCheck),
                            new DashboardItemConfiguration(ViewCode.TariffRateAnalysis.ToString(), TariffRateAnalysisView.PageAddress, ViewCode.TariffRateAnalysis),
							new DashboardItemConfiguration(ViewCode.VendorRating.ToString(), VendorRatingView.PageAddress, ViewCode.VendorRating),
							new DashboardItemConfiguration(ViewCode.VendorRatingFuelMassUpdateLTL.ToString(), VendorRatingLTLFuelSurchargeView.PageAddress, ViewCode.VendorRatingFuelMassUpdateLTL),
                            
						};

			if (tenant != null) UpdateWithUserDefinedPermissions(configurations, tenant, PermissionCategory.Rating);

			return configurations.OrderBy(c => c.DisplayName.ToString()).ToList();
		}

		public static List<DashboardItemConfiguration> Connect2(Tenant tenant = null)
		{
			var configurations = new List<DashboardItemConfiguration>
						{
							new DashboardItemConfiguration(ViewCode.CustomerCommunication.ToString(), CustomerCommunicationView.PageAddress, ViewCode.CustomerCommunication),
							new DashboardItemConfiguration(ViewCode.DocDeliveryLog.ToString(), DocDeliveryLogView.PageAddress, ViewCode.DocDeliveryLog),
							new DashboardItemConfiguration(ViewCode.FaxTransmission.ToString(), FaxTransmissionView.PageAddress, ViewCode.FaxTransmission),
                            new DashboardItemConfiguration(ViewCode.MacroPointDashboard.ToString(), MacroPointDashboardView.PageAddress, ViewCode.MacroPointDashboard),
							new DashboardItemConfiguration(ViewCode.VendorCommunication.ToString(), VendorCommunicationView.PageAddress, ViewCode.VendorCommunication),
							new DashboardItemConfiguration(ViewCode.XmlConnectRecord.ToString(), XmlConnectRecordView.PageAddress, ViewCode.XmlConnectRecord),
							new DashboardItemConfiguration(ViewCode.XmlTransmission.ToString(), XmlTransmissionView.PageAddress, ViewCode.XmlTransmission),
						};

			if (tenant != null) UpdateWithUserDefinedPermissions(configurations, tenant, PermissionCategory.Connect);

			return configurations.OrderBy(c => c.DisplayName.ToString()).ToList();
		}

		public static List<DashboardItemConfiguration> SuperAdmin2()
		{
			var configurations = new List<DashboardItemConfiguration> 
						{
                            new DashboardItemConfiguration(ViewCode.AdminAuditLog.ToString(), AdminAuditLogView.PageAddress, ViewCode.AdminAuditLog),
                            new DashboardItemConfiguration(ViewCode.AdminUser.ToString(), AdminUserView.PageAddress, ViewCode.AdminUser),
                            new DashboardItemConfiguration(ViewCode.ApplicationLog.ToString(), ApplicationLogListingView.PageAddress, ViewCode.ApplicationLog),
							new DashboardItemConfiguration(ViewCode.Country.ToString(), CountryView.PageAddress, ViewCode.Country),
							new DashboardItemConfiguration(ViewCode.ErrorLog.ToString(), ApplicationErrorListingView.PageAddress, ViewCode.ErrorLog),
							new DashboardItemConfiguration(ViewCode.PostalCode.ToString(), PostalCodeView.PageAddress, ViewCode.PostalCode),
							new DashboardItemConfiguration(ViewCode.QlikUser.ToString(), QlikUserView.PageAddress, ViewCode.QlikUser),
							new DashboardItemConfiguration(ViewCode.ReportTemplate.ToString(), ReportTemplateView.PageAddress, ViewCode.ReportTemplate),
							new DashboardItemConfiguration(ViewCode.ResetTenantUser.ToString(), ResetTenantUserView.PageAddress, ViewCode.ResetTenantUser),
							new DashboardItemConfiguration(ViewCode.TenantSetup.ToString(), TenantSetupView.PageAddress, ViewCode.TenantSetup),
						};

			return configurations.OrderBy(c => c.DisplayName.ToString()).ToList();
		}


		private static void UpdateWithUserDefinedPermissions(ICollection<DashboardItemConfiguration> configurations, Tenant tenant, PermissionCategory permissionCategory)
		{
			var perms = ProcessorVars
				.RegistryCache[tenant.Id]
				.PermissionDetails
				.Where(d => d.Category == permissionCategory)
				.Select(
					d =>
					new DashboardItemConfiguration(d.Code, string.IsNullOrEmpty(d.NavigationUrl) ? DashboardView.PageAddress : d.NavigationUrl, d.DisplayCode))
				.ToList();

			foreach (var p in perms)
			{
				var cp = configurations.FirstOrDefault(i => i.IconName == p.IconName);
				if (cp == null) configurations.Add(p);
				else cp.DisplayName = p.DisplayName;
			}
		}
	}
}
