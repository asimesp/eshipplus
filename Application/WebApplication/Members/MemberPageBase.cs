﻿using System;
using System.Web;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Smc;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.Utilities.ImmitationViews;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members
{
	public abstract class MemberPageBase : PageBase
	{
		private Permission _permission;
		private User _user;
		private HttpServerUtility _server;

		public override string PageName
		{
			get { return PageCode.ToString().FormattedString(); }
		}

		public User ActiveUser
		{
			get { return (IsImmitationView ? _user : Session[WebApplicationConstants.ActiveUser] as User) ?? new User(); }
			set
			{
				if (this as IHttpContextImmitationView == null)
					Session[WebApplicationConstants.ActiveUser] = value;
				else
					_user = value;
			}
		}

		public new HttpServerUtility Server
		{
			get
			{
				return _server ?? base.Server;
			}
			set
			{
				_server = value;
			}
		}

		protected bool IsImmitationView
		{
			get { return this as IHttpContextImmitationView != null; }
		}

		protected string ActiveUserTenantCode
		{
			get { return ActiveUser.Tenant == null ? string.Empty : ActiveUser.Tenant.Code; }
		}

		protected string ActiveUsername
		{
			get { return ActiveUser.Tenant == null ? string.Empty : ActiveUser.Username; }
		}

		protected long ActiveUserTenantId
		{
			get { return ActiveUser.Tenant == null ? default(long) : ActiveUser.Tenant.Id; }
		}

		public SearchDefaults SearchDefaults
		{
			get
			{
				return IsImmitationView
				       	? null
				       	: Session[WebApplicationConstants.UserSearchDefaults] as SearchDefaults;
			}
			set { if (this as IHttpContextImmitationView == null) Session[WebApplicationConstants.UserSearchDefaults] = value; }
		}

		public Permission Access
		{
			get { return _permission; }
		}

		public abstract ViewCode PageCode { get;} 

		public abstract string SetPageIconImage { set; }

		public SmcServiceSettings RatewareSettings
		{
			get
			{
				if (string.IsNullOrEmpty(ActiveUser.Tenant.SMCRateWareParameterFile)) return new SmcServiceSettings();
				var contents = Server.ReadFromFile(ActiveUser.Tenant.SMCRateWareParameterFile).FromUtf8Bytes();
				return contents.FromXml<SmcServiceSettings>();
			}
		}

		public SmcServiceSettings CarrierConnectSettings
		{
			get
			{
				if (string.IsNullOrEmpty(ActiveUser.Tenant.SMCCarrierConnectParameterFile)) return new SmcServiceSettings();
				var contents = Server.ReadFromFile(ActiveUser.Tenant.SMCCarrierConnectParameterFile).FromUtf8Bytes();
				return contents.FromXml<SmcServiceSettings>();
			}
		}

		public new HttpRequest Request
		{
			get { return IsImmitationView ? HttpContext.Current.Request : base.Request; }
		}

		protected override void OnLoad(EventArgs e)
		{
			// check for persisted login
			var isSuperUserLogin = Session[WebApplicationConstants.IsSuperUserLogin].ToBoolean();
			if ((ActiveUser == null || (ActiveUser != null && !isSuperUserLogin && !ActiveUser.KeyLoaded)) && Request.HasPersistedLogin())
            {
            	ActiveUser = Request.RetrievePersistedLogin();

				Session[WebApplicationConstants.PartnerReturnUrlKey] = Request.RetrievePersistedPartnerReturnUrl();
				Session[WebApplicationConstants.PartnerHomeUrlKey] = Request.RetrievePersistedPartnerHomeUrl();
				Session[WebApplicationConstants.AuthenticationErrorReturnUrl] = Request.RetrievePersistedPartnerAuthenticationErrorReturnUrl();

				if (ActiveUser != null)
				{
					var profile = new SearchProfileDefault(ActiveUser);
					if (!profile.IsNew) SearchDefaults = new SearchDefaults(profile);
				}
            }

			if (ActiveUser == null || (ActiveUser != null && !isSuperUserLogin && !ActiveUser.KeyLoaded))
			{
                var qs = Request.QueryString.Count > 0 ? WebApplicationConstants.RedirectUrlPrefixQuestion + Request.QueryString : string.Empty;
                qs = qs.Replace(WebApplicationConstants.Ampersand, WebApplicationConstants.RedirectUrlPrefixAmperSand);

				var returnUrl = Session[WebApplicationConstants.PartnerReturnUrlKey].GetString();
				var url = string.IsNullOrEmpty(returnUrl) ? _Default.PageAddress : returnUrl;
                Response.Redirect(string.Format("{0}?{1}={2}{3}", url, WebApplicationConstants.RedirectUrlPrefix, Request.AppRelativeCurrentExecutionFilePath, qs));
				return;
			}

			_permission = ActiveUser.RetrievePermission(PageCode);

			if (!PageCode.ExcludePermission() && (PageCode == ViewCode.Null || _permission.Deny || !_permission.Grant))
			{
				Response.Redirect(DashboardView.PageAddress);
				return;
			}

			SetPageIconImage = DashboardItemConfiguration.BuildIconImageLocationUrl(PageCode.ToString());

			if (!IsPostBack)
                EnvironmentUtilities.PageAccessLogger.Info("Tenant:{0}\tUser:{1} {2} ({3})\tPage:{4}\t IP:{5}",
				                                           isSuperUserLogin ? "-" : ActiveUser.Tenant.Name, ActiveUser.FirstName, 
                                                           ActiveUser.LastName, ActiveUser.Username, PageName, Request.UserHostAddress);
            

			base.OnLoad(e);
		}
	}
}
