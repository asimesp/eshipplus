﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Core;
using LogisticsPlus.Eship.WebApplication.Documents;
using LogisticsPlus.Eship.WebApplication.Members.Administration;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members
{
    public partial class TenantSetupView : AdminMemberPageBase, ITenantSetupView
    {
        private const string SaveFlag = "S";
        private const string DeleteFlag = "D";

        public override ViewCode PageCode { get { return ViewCode.TenantSetup; } }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.UtilitiesBlue; }
        }

        public static string PageAddress { get { return "~/Members/TenantSetupView.aspx"; } }

        public Permission GroupPermission
        {
            get
            {
                return new Permission
                {
                    Code = ViewCode.Group.GetString(),
                    Description = string.Empty,
                    Grant = true,
                    Remove = true,
                    Modify = true,
                    ModifyApplicable = true,
                    DeleteApplicable = true,
                    Deny = false
                };
            }
        }

        public Permission UserPermission
        {
            get
            {
                return new Permission
                {
                    Code = ViewCode.User.GetString(),
                    Description = string.Empty,
                    Grant = true,
                    Remove = true,
                    Modify = true,
                    ModifyApplicable = true,
                    DeleteApplicable = true,
                    Deny = false
                };
            }
        }

        public Permission TenantAdminPermission
        {
            get
            {
                return new Permission
                {
                    Code = ViewCode.TenantAdmin.GetString(),
                    Description = string.Empty,
                    Grant = true,
                    Remove = true,
                    Modify = true,
                    ModifyApplicable = true,
                    DeleteApplicable = true,
                    Deny = false
                };
            }
        }

        public event EventHandler<ViewEventArgs<Tenant>> Save;
        public event EventHandler<ViewEventArgs<Tenant>> Delete;

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == SaveFlag)
            {
                SetEditStatus(false);
                if (Directory.Exists(hidDeleteFolder.Value)) Directory.Delete(hidDeleteFolder.Value, true);
                hidDeleteFolder.Value = string.Empty;
            }

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void SetId(long id)
        {
            if (id == default(long) && hidFlag.Value == DeleteFlag)
            {
                Server.RemoveDirectory(WebApplicationSettings.TenantFolder(hidTenantId.Value.ToLong()));
                hidFlag.Value = string.Empty;
                if (Directory.Exists(hidDeleteFolder.Value)) Server.RemoveDirectory(hidDeleteFolder.Value);
                hidDeleteFolder.Value = string.Empty;
            }
            var tenant = new Tenant(id);
            if (tenant.IsNew) tenant.Code = string.Empty;
            LoadTenant(tenant);
        }

        private void SetEditStatus(bool enabled)
        {
            pnlDetails.Enabled = enabled;
            chkRetrieveSeeds.Enabled = enabled;
            pnlSeeds.Enabled = chkRetrieveSeeds.Checked && enabled;

            memberToolBar.EnableSave = enabled;
        }

        private void CloseTenantFinderPopUp()
        {
            tenantFinder.Visible = false;
        }

        private void LoadTenant(Tenant tenant)
        {
            txtCode.Text = tenant.Code;
            txtName.Text = tenant.Name;
            litRecordIdentity.Text = string.Format("- [{0}]", tenant.Name);
            chkActive.Checked = tenant.Active;
            hidTenantId.Value = tenant.Id.ToString();

            lstSeeds.DataSource = tenant.AutoNumbers;
            lstSeeds.DataBind();

            var path = Path.Combine(MapPath(WebApplicationSettings.BaseLoginPath), tenant.Code.GetString());
            if (Directory.Exists(path) && !tenant.IsNew)
            {
                hypCustomLogin.Visible = true;
                hypCustomLogin.NavigateUrl = string.Format("{0}{1}{2}/{3}",
                                                           Request.ResolveSiteRootWithHttp(),
                                                           WebApplicationSettings.BaseLoginPath.Replace("~", string.Empty),
                                                           tenant.Code,
                                                           WebApplicationConstants.CustomLoginPageName);
            }
            else hypCustomLogin.Visible = false;

            btnGenerateCustomLoginFolder.Enabled = !tenant.IsNew;
        }

        private static void AddMissingTenantAutoNumberCodes(Tenant tenant)
        {
            var seeds = ProcessorUtilities.GetAll<AutoNumberCode>(false);

            foreach (var code in tenant.AutoNumbers.Where(code => seeds.ContainsKey(code.Code.ToInt())))
                seeds.Remove(code.Code.ToInt());

            foreach (var seed in seeds)
                tenant.AutoNumbers.Add(new AutoNumber { Code = seed.Key.ToEnum<AutoNumberCode>() });
        }

        private Dictionary<AutoNumberCode, long> RetrieveAutoNumbers()
        {
            var numbers = new Dictionary<AutoNumberCode, long>();
            foreach (var item in lstSeeds.Items)
            {
                var hid = item.FindControl("hidAutoNumberCode").ToCustomHiddenField();
                var tb = item.FindControl("txtSeedValue").ToTextBox();

                if (tb != null && hid != null && !numbers.ContainsKey(hid.Value.ToEnum<AutoNumberCode>()))
                    numbers.Add(hid.Value.ToEnum<AutoNumberCode>(), tb.Text.ToLong());
            }
            return numbers;
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            new TenantSetupHandler(this).Initialize();

            memberToolBar.ShowSave = Access.Modify;
            memberToolBar.ShowDelete = Access.Remove;
            memberToolBar.ShowEdit = Access.Modify;

            if (IsPostBack) return;

            SetEditStatus(false);
        }

        protected void OnTenantFinderItemSelected(object sender, ViewEventArgs<Tenant> e)
        {
            var tenant = e.Argument;

            AddMissingTenantAutoNumberCodes(tenant);
            LoadTenant(tenant);
            SetId(tenant.Id);
            SetEditStatus(tenantFinder.OpenForEdit);
            CloseTenantFinderPopUp();
        }

        protected void OnFindClicked(object sender, EventArgs e)
        {
            tenantFinder.Visible = true;
        }

        protected void OnSaveClicked(object sender, EventArgs e)
        {
            var tenantId = hidTenantId.Value.ToLong();
            var tenant = new Tenant(tenantId, tenantId != default(long)) { Name = txtName.Text, Active = chkActive.Checked };

            // check for need to remove old code
            hidDeleteFolder.Value = !tenant.IsNew && tenant.Code != txtCode.Text ? Path.Combine(WebApplicationSettings.BaseLoginPath, tenant.Code) : string.Empty;


            tenant.Code = txtCode.Text;
            if (chkRetrieveSeeds.Checked)
            {
                var viewNumbers = RetrieveAutoNumbers();

                foreach (var autoNumber in tenant.AutoNumbers.Where(autoNumber => viewNumbers.ContainsKey(autoNumber.Code)))
                    autoNumber.NextNumber = viewNumbers[autoNumber.Code];
                foreach (var key in viewNumbers.Keys)
                    if (tenant.AutoNumbers.All(an => an.Code != key))
                        tenant.AutoNumbers.Add(new AutoNumber { Code = key, NextNumber = viewNumbers[key], Tenant = tenant, });
            }
            else tenant.LoadCollections();

            if (tenant.IsNew)
            {
                tenant.FedExSmallPackParameterFile = string.Empty;
                tenant.SMCCarrierConnectParameterFile = string.Empty;
                tenant.SMCRateWareParameterFile = string.Empty;
                tenant.UpsSmallPackParameterFile = string.Empty;
                tenant.MacroPointParameterFile = string.Empty;
                tenant.AutoRatingNoticeText = string.Empty;
                tenant.RatingNotice = string.Empty;
                tenant.TermsAndConditionsFile = string.Empty;
                tenant.BatchRatingAnalysisEndTime = TimeUtility.Default;
                tenant.BatchRatingAnalysisStartTime = TimeUtility.Default;
                tenant.LogoUrl = string.Empty;
                tenant.TenantScac = string.Empty;
                tenant.PaymentGatewayType = PaymentGatewayType.NotApplicable;
                tenant.PaymentGatewayLoginId = string.Empty;
                tenant.PaymentGatewaySecret = string.Empty;
                tenant.PaymentGatewayTransactionId = string.Empty;
	            tenant.AutoNotificationSubjectPrefix = string.Empty;
                tenant.PaymentGatewayInTestMode = true;
            }

            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<Tenant>(tenant));
        }

        protected void OnNewClicked(object sender, EventArgs e)
        {
            var tenant = new Tenant { Code = string.Empty };

            AddMissingTenantAutoNumberCodes(tenant);
            LoadTenant(tenant);
            chkRetrieveSeeds.Checked = true;
            SetEditStatus(true);
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var tenant = new Tenant(hidTenantId.Value.ToLong(), false);

            hidFlag.Value = DeleteFlag;
            hidDeleteFolder.Value = Path.Combine(WebApplicationSettings.BaseLoginPath, tenant.Code);

            if (Delete != null)
                Delete(this, new ViewEventArgs<Tenant>(tenant));

            SetEditStatus(false);
            tenantFinder.Reset();
        }

        protected void OnTenantFinderSelectionCancelled(object sender, EventArgs e)
        {
            CloseTenantFinderPopUp();
        }

        protected void OnRetrieveSeedsCheckedChanged(object sender, EventArgs e)
        {
            pnlSeeds.Enabled = chkRetrieveSeeds.Checked;
        }

        protected void OnEditClicked(object sender, EventArgs e)
        {
            SetEditStatus(true);
        }


        private void CreateCustomLogin(string customTemplate)
        {

            try
            {
                var tenant = new Tenant(hidTenantId.Value.ToLong());
                var path = MapPath(WebApplicationSettings.BaseLoginPath);
                var siteRoot = Request.ResolveSiteRootWithHttp();
                if (siteRoot.EndsWith("/")) siteRoot = siteRoot.Substring(0, siteRoot.Length - 1);

                // paths
                var oldPath = Path.Combine(path, tenant.Code);
                var newPath = Path.Combine(path, txtCode.Text);

                if (Directory.Exists(oldPath)) Directory.Delete(oldPath, true);
                if (Directory.Exists(newPath))
                {
                    DisplayMessages(new[] { ValidationMessage.Error("Path already exists. Choose new tenant code.") });
                    return;
                }
                Directory.CreateDirectory(newPath);

                // check alerts
                var alerts = Server.RetrieveSystemAlerts();
                alerts = string.IsNullOrEmpty(alerts)
                            ? WebApplicationConstants.CustomLoginAlertsPlaceHolder
                            : string.Format(WebApplicationConstants.CustomLoginAlertsFormat, alerts);
                var template = this.GenerateTenantLoginPage(tenant, customTemplate, alerts);
                template.ToUtf8Bytes().WriteToFile(newPath, WebApplicationConstants.CustomLoginPageName, string.Empty);

                hypCustomLogin.NavigateUrl = string.Format("{0}{1}{2}/{3}", siteRoot, WebApplicationSettings.BaseLoginPath.Replace("~", string.Empty),
                                                           txtCode.Text, WebApplicationConstants.CustomLoginPageName);
                hypCustomLogin.Visible = true;
            }
            catch (Exception e)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Error processing last request. Error: {0}", e.Message) });
            }
        }

        protected void OnGenerateCustomLoginFolderClicked(object sender, EventArgs e)
        {
            if (chkWithCustomTemplate.Checked)
            {
                fileUploader.Title = "Generate Tenant Custom Login";
                fileUploader.Instructions = @"**Please upload custom template. See user guide for format.";
                fileUploader.Visible = true;
            }
            else CreateCustomLogin(string.Empty);
        }

        protected void OnClearCustomLoginFolderClicked(object sender, EventArgs e)
        {
            try
            {
                if (hidTenantId.Value.ToLong() == default(long)) return;
                var tenant = new Tenant(hidTenantId.Value.ToLong());
                Server.RemoveDirectory(Path.Combine(WebApplicationSettings.BaseLoginPath, tenant.Code));
                hypCustomLogin.NavigateUrl = string.Empty;
                hypCustomLogin.Visible = false;
            }
            catch (Exception ex)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Error processing last request. Error: {0}", ex.Message) });
            }
        }

        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            string contents;
            using (e.Reader) contents = e.Reader.ReadToEnd();
            fileUploader.Visible = false;
            CreateCustomLogin(contents);
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }
    }
}
