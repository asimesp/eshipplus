﻿using System;
using System.IO;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.WebApplication.Documents;
using LogisticsPlus.Eship.WebApplication.Members.Operations;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members
{
	public partial class DocumentViewer : DocumentViewerMemberPageBase
	{
		public static string PageAddress { get { return "~/Members/DocumentViewer.aspx"; } }

		public override ViewCode PageCode
		{
			get { return ViewCode.DocumentProcessorPage; }
		}

		public override string SetPageIconImage { set { } }



		public static string GenerateShipmentBolLink(Shipment shipment)
		{
			return GenerateShipmentBolLink(shipment.Id);
		}

		public static string GenerateShipmentBolLink(ShipmentDashboardDto shipmentDashboardDto)
		{
			return GenerateShipmentBolLink(shipmentDashboardDto.Id);
		}

		public static string GenerateShipmentBolLink(long shipmentId)
		{
			return string.Format("{0}?{1}={2}&{3}={4}", PageAddress, WebApplicationConstants.DocumentViewerKey,
								 WebApplicationConstants.DocumentViewerBolKey,
								 WebApplicationConstants.DocumentViewerShipmentId, shipmentId);
		}

		public static string GenerateShipmentDocumentLink(ShipmentDocument document)
		{
			return document.LocationPath.ToLower().EndsWith("png") || document.LocationPath.ToLower().EndsWith("gif")
					? string.Format("{0}?{1}={2}&{3}={4}&{5}={6}", PageAddress,
									WebApplicationConstants.DocumentViewerKey, WebApplicationConstants.DocumentViewerImageKey,
									WebApplicationConstants.DocumentViewerShipmentId, document.Shipment.Id,
									WebApplicationConstants.DocumentViewerDocumentId, document.Id)
					: string.Format("{0}?{1}={2}&{3}={4}&{5}={6}", PageAddress,
									WebApplicationConstants.DocumentViewerKey, WebApplicationConstants.DocumentViewerDocumentKey,
									WebApplicationConstants.DocumentViewerShipmentId, document.Shipment.Id,
									WebApplicationConstants.DocumentViewerDocumentId, document.Id);
		}

		public static string GenerateShipmentStatementLink(Shipment shipment)
		{
			return string.Format("{0}?{1}={2}&{3}={4}",
								 PageAddress,
								 WebApplicationConstants.DocumentViewerKey,
								 WebApplicationConstants.DocumentViewerStatementKey,
								 WebApplicationConstants.DocumentViewerShipmentId,
								 shipment.Id);
		}

	 

        public static string GenerateInvoiceLink(Invoice invoice)
		{
			return string.Format("{0}?{1}={2}&{3}={4}", PageAddress, WebApplicationConstants.DocumentViewerKey,
								 WebApplicationConstants.DocumentViewerInvoiceKey,
								 WebApplicationConstants.DocumentViewerInvoiceId, invoice.Id);
		}

        public static string GenerateEmailLogLink(EmailLog emailLog)
        {
            return string.Format("{0}?{1}={2}&{3}={4}", PageAddress,
                                 WebApplicationConstants.DocumentViewerKey, WebApplicationConstants.DocumentViewerEmailKey,
                                 WebApplicationConstants.DocumentViewerEmailLogId, emailLog.Id.ToString().UrlTextEncrypt());
        }



		protected void Page_Load(object sender, EventArgs e)
		{
			lbContent.Text = string.Empty;

			var key = Request.QueryString[WebApplicationConstants.DocumentViewerKey];

			switch (key)
			{
				case WebApplicationConstants.DocumentViewerImageKey:
					ProcessImageView();
					break;
				case WebApplicationConstants.DocumentViewerDocumentKey:
					ProcessDocumentView();
					break;
				case WebApplicationConstants.DocumentViewerStatementKey:
					ProcessShipmentStatementView();
					break;
				case WebApplicationConstants.DocumentViewerInvoiceKey:
					ProcessInvoiceView();
					return;
                case WebApplicationConstants.DocumentViewerEmailKey:
			        ProcessEmailView();
			        break;
			//case WebApplicationConstants.DocumentViewerBolKey: // default option for BOL.  RateAndSchedule page will send here without WAC.DocViewerBolKey.
                default:
					ProcessShipmentBillOfLading();
					return;
				
			}
		}

	    private void ProcessEmailView()
	    {
	        if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.DocumentViewerEmailLogId]))
	        {
                if (ActiveUser == null) return;
	            var emailLog = new EmailLog(Request.QueryString[WebApplicationConstants.DocumentViewerEmailLogId].UrlTextDecrypt().ToLong());
	            if (ActiveUser.TenantId != emailLog.TenantId) return;
                Response.Write(emailLog.Body);
	            imgContent.Visible = false;
	        }
	    }

	    private void ProcessShipmentStatementView()
		{
			if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.DocumentViewerShipmentId]))
			{
				if (ActiveUser == null) return;
				var shipment = new Shipment(Request.QueryString[WebApplicationConstants.DocumentViewerShipmentId].ToLong());
				if (!shipment.KeyLoaded) return;
				if (!ActiveUser.HasAccessToCustomer(shipment.Customer.Id)) return;

				var shipmentInvoices = new AuditInvoiceDto().FetchAuditInvoiceDtos(shipment.ShipmentNumber, DetailReferenceType.Shipment, shipment.TenantId);

				var html = this.GenerateShipmentStatement(shipment, shipmentInvoices, true);

				var title = string.Format("eShipPlus_TMS_{0}_ShipmentStatement_{1}", WebApplicationSettings.Version, shipment.ShipmentNumber);
				Response.Export(html.ToPdf(title), string.Format("{0}.pdf", title.Replace(" ", "_")));
			}
		}

        private void ProcessDocumentView()
		{
			if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.DocumentViewerShipmentId]))
			{
				if (ActiveUser == null) return;
				var shipment = new Shipment(Request.QueryString[WebApplicationConstants.DocumentViewerShipmentId].ToLong());
				if (!shipment.KeyLoaded) return;
				if (!ActiveUser.HasAccessToCustomer(shipment.Customer.Id)) return;

				var id = Request.QueryString[WebApplicationConstants.DocumentViewerDocumentId].ToLong();
				var doc = shipment.Documents.FirstOrDefault(d => d.Id == id);
				if (doc == null) return;

				Response.Export(Server.ReadFromFile(doc.LocationPath), doc.Name);
			}

			if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.DocumentViewerServiceTicketId]))
			{
				if (ActiveUser == null) return;
				var ticket = new ServiceTicket(Request.QueryString[WebApplicationConstants.DocumentViewerServiceTicketId].ToLong());
				if (!ticket.KeyLoaded) return;
				if (!ActiveUser.HasAccessToCustomer(ticket.Customer.Id)) return;

				var id = Request.QueryString[WebApplicationConstants.DocumentViewerDocumentId].ToLong();
				var doc = ticket.Documents.FirstOrDefault(d => d.Id == id);
				if (doc == null) return;

				Response.Export(Server.ReadFromFile(doc.LocationPath), doc.Name);
			}
		}

		private void ProcessImageView()
		{
			if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.DocumentViewerShipmentId]))
			{
				if (ActiveUser == null) return;
				var shipment = new Shipment(Request.QueryString[WebApplicationConstants.DocumentViewerShipmentId].ToLong());
				if (!shipment.KeyLoaded) return;
				if (!ActiveUser.HasAccessToCustomer(shipment.Customer.Id)) return;

				var id = Request.QueryString[WebApplicationConstants.DocumentViewerDocumentId].ToLong();
				var doc = shipment.Documents.FirstOrDefault(d => d.Id == id);
				if (doc == null) return;

				imgContent.ImageUrl = doc.LocationPath;
				imgContent.AlternateText = doc.Name;
			}
		}

		private void ProcessShipmentBillOfLading()
		{
			if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.DocumentViewerShipmentId]))
			{
				if (ActiveUser == null) return;
				var shipment = new Shipment(Request.QueryString[WebApplicationConstants.DocumentViewerShipmentId].ToLong());
				if (!shipment.KeyLoaded) return;
				if (!ActiveUser.HasAccessToCustomer(shipment.Customer.Id)) return;

				//lbContent.Text = this.GenerateBOL(shipment, true);

				var html = this.GenerateBOL(shipment, true);

				var title = string.Format("eShipPlus_TMS_{0}_{1}", WebApplicationSettings.Version, shipment.ShipmentNumber);
				Response.Export(html.ToPdf(title), string.Format("{0}.pdf", title.Replace(" ", "_")));
			}
		}

		private void ProcessInvoiceView()
		{
			if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.DocumentViewerInvoiceId]))
			{
				if (ActiveUser == null) return;
				var invoice = new Invoice(Request.QueryString[WebApplicationConstants.DocumentViewerInvoiceId].ToLong());
				if (!invoice.KeyLoaded) return;
				if (!ActiveUser.HasAccessToCustomer(invoice.CustomerLocation.Customer.Id)) return;

				var html = this.GenerateInvoice(invoice, true);

				var title = string.Format("eShipPlus_TMS_{0}_{1}", WebApplicationSettings.Version, invoice.InvoiceNumber);
				Response.Export(html.ToPdf(title), string.Format("{0}.pdf", title.Replace(" ", "_")));
			}
		}
	}
}
