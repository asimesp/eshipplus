﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Core;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Core;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members
{
	public partial class DeveloperAccessRequestListingView : MemberPageBase, IDeveloperAccessRequestListingView
	{
		public static string PageAddress { get { return "~/Members/DeveloperAccessRequestListingView.aspx"; } }

		public override ViewCode PageCode { get { return ViewCode.DeveloperAccessRequestListing; } }

		public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.UtilitiesBlue; } }

		private const string NotifyProductionApprovedArgs = "Production";
		private const string NotifyDevelopmentApprovedArgs = "Development";

		public event EventHandler<ViewEventArgs<DeveloperAccessRequest>> Save;
		public event EventHandler<ViewEventArgs<DeveloperAccessRequest>> Delete;
		public event EventHandler<ViewEventArgs<DeveloperAccessRequest>> Lock;
		public event EventHandler<ViewEventArgs<DeveloperAccessRequest>> UnLock;
		public event EventHandler<ViewEventArgs<DeveloperAccessRequest>> LoadAuditLog;

		public void LogException(Exception ex)
		{
			if (ex != null) ErrorLogger.LogError(ex, Context);
		}

		public void DisplayMessages(IEnumerable<ValidationMessage> messages)
		{
			if (messages == null) return;
			if (!messages.Any()) return;



			if (!messages.HasErrors() && !messages.HasWarnings())
			{
				var request = new DeveloperAccessRequest(hidRequestId.Value.ToLong(), false);


				// unlock and return to read-only
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<DeveloperAccessRequest>(request));
				SetEditStatus(false);
				memberToolBar.ShowUnlock = false;
			}

			messageBox.Button = MessageButton.Ok;
			messageBox.Icon = messages.GenerateIcon();
			messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
			messageBox.Visible = true;

			litErrorMessages.Text = messages.HasErrors()
										? string.Join(WebApplicationConstants.HtmlBreak,
													  messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
														Select(m => m.Message).ToArray())
										: string.Empty;
		}

		public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs)
		{
			auditLogs.Load(logs);
		}


		public void FailedLock(Lock @lock)
		{
			memberToolBar.ShowUnlock = false;
			litMessage.Text = @lock.BuildFailedLockMessage();
		}

		public void Set(DeveloperAccessRequest request)
		{
			LoadDeveloperAccessRequest(request);
			SetEditStatus(!request.IsNew);
		}


		private void SetEditStatus(bool enabled)
		{
			pnlRequest.Enabled = enabled;
			memberToolBar.EnableSave = enabled;

			litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;
		}


		private void LoadDeveloperAccessRequest(DeveloperAccessRequest request)
		{
			var oldRequestId = hidRequestId.Value;

			//detail
			hidRequestId.Value = request.Id.ToString();
			txtDateCreated.Text = request.DateCreated.FormattedShortDate();
			txtContactName.Text = request.ContactName;
			txtContactEmail.Text = request.ContactEmail;
			txtCustomerName.Text = request.Customer.Name;
			txtCustomerNumber.Text = request.Customer.CustomerNumber;
			chkAccess.Checked = request.AccessGranted;
			chkProductionAccess.Checked = request.ProductionAccess;
			txtTestInfo.Text = request.TestInformation;

			// if change in request, reset toolbarmoreactions accordingly
			if (oldRequestId != hidRequestId.Value)
			{
				memberToolBar.LoadAdditionalActions(ToolbarMoreActions());
			}

			memberToolBar.ShowUnlock = request.HasUserLock(ActiveUser, request.Id);
		}

		private List<ToolbarMoreAction> ToolbarMoreActions()
		{
			var notifyDevelopmentApproved = new ToolbarMoreAction
			{
				CommandArgs = NotifyDevelopmentApprovedArgs,
				ImageUrl = IconLinks.Utilities,
				Name = "Notify Development Approval",
				IsLink = false,
				NavigationUrl = string.Empty
			};

			var notifyProductionApproved = new ToolbarMoreAction
			{
				CommandArgs = NotifyProductionApprovedArgs,
				ImageUrl = IconLinks.Utilities,
				Name = "Notify Production Approval",
				IsLink = false,
				NavigationUrl = string.Empty
			};

			var notNew = hidRequestId.Value.ToLong() != default(long);
			var actions = new List<ToolbarMoreAction>();

			memberToolBar.ShowMore = notNew;

			if (notNew)
			{
				actions.Add(notifyProductionApproved);
				actions.Add(notifyDevelopmentApproved);
			}

			return actions;
		}


		private void ProcessTransferredRequest(long developerAccessRequestId)
		{
			if (developerAccessRequestId != default(long))
			{
				var developerAccessRequest = new DeveloperAccessRequest(developerAccessRequestId);
				LoadDeveloperAccessRequest(developerAccessRequest);

				if (LoadAuditLog != null)
					LoadAuditLog(this, new ViewEventArgs<DeveloperAccessRequest>(developerAccessRequest));
			}
		}


		private void SendApprovalNotification(string name)
		{
			var production = hidFlag.Value.ToBoolean();
			hidFlag.Value = null;
			var request = new DeveloperAccessRequest(hidRequestId.Value.ToLong());

			if (!request.IsNew)
				this.NotifyDeveloperAccessRequestDevelopmentApproval(request, production, name);
		}


		protected void OnFinderItemSelected(object sender, ViewEventArgs<DeveloperAccessRequest> e)
		{
			if (hidRequestId.Value.ToLong() != default(long))
			{
				var oldRequest = new DeveloperAccessRequest(hidRequestId.Value.ToLong(), false);

				if (UnLock != null)
					UnLock(this, new ViewEventArgs<DeveloperAccessRequest>(oldRequest));
			}

			var request = e.Argument;
			LoadDeveloperAccessRequest(request);

			developerAccessRequestFinder.Visible = false;

			if (developerAccessRequestFinder.OpenForEdit && Lock != null)
			{
				SetEditStatus(true);
				memberToolBar.ShowUnlock = true;
				Lock(this, new ViewEventArgs<DeveloperAccessRequest>(request));
			}
			else
			{
				SetEditStatus(false);
			}

			if (LoadAuditLog != null) LoadAuditLog(this, new ViewEventArgs<DeveloperAccessRequest>(request));
			litErrorMessages.Text = string.Empty;
		}

		protected void OnFinderItemCancelled(object sender, EventArgs e)
		{
			developerAccessRequestFinder.Visible = false;
			pnlDimScreen.Visible = false;
		}


		protected void OnToolbarFindClicked(object sender, EventArgs e)
		{
			developerAccessRequestFinder.Visible = true;
		}

		protected void OnToolbarSaveClicked(object sender, EventArgs e)
		{
			var requestId = hidRequestId.Value.ToLong();
			var request = new DeveloperAccessRequest(requestId, requestId != default(long))
			{
				ContactName = txtContactName.Text,
				ContactEmail = txtContactEmail.Text.StripSpacesFromEmails(),
				AccessGranted = chkAccess.Checked,
				ProductionAccess = chkProductionAccess.Checked,
				TestInformation = txtTestInfo.Text
			};

			if (Save != null)
				Save(this, new ViewEventArgs<DeveloperAccessRequest>(request));

			if (LoadAuditLog != null)
				LoadAuditLog(this, new ViewEventArgs<DeveloperAccessRequest>(request));
		}

		protected void OnToolbarDeleteClicked(object sender, EventArgs e)
		{
			var request = new DeveloperAccessRequest(hidRequestId.Value.ToLong(), false);

			developerAccessRequestFinder.Reset();

			if (Delete != null)
				Delete(this, new ViewEventArgs<DeveloperAccessRequest>(request));
		}

		protected void OnToolbarEditClicked(object sender, EventArgs e)
		{
			var request = new DeveloperAccessRequest(hidRequestId.Value.ToLong(), false);

			if (Lock == null || request.IsNew) return;

			SetEditStatus(true);
			memberToolBar.ShowUnlock = true;
			Lock(this, new ViewEventArgs<DeveloperAccessRequest>(request));

			LoadDeveloperAccessRequest(request);
		}

		protected void OnUnlockClicked(object sender, EventArgs e)
		{
			var request = new DeveloperAccessRequest(hidRequestId.Value.ToLong(), false);
			if (UnLock != null && !request.IsNew)
			{
				SetEditStatus(false);
				memberToolBar.ShowUnlock = false;
				UnLock(this, new ViewEventArgs<DeveloperAccessRequest>(request));
			}
		}

		protected void OnToolbarCommand(object sender, ViewEventArgs<string> e)
		{
			var production = e.Argument == NotifyProductionApprovedArgs;
			hidFlag.Value = production.ToString();

			switch (e.Argument)
			{
				case NotifyProductionApprovedArgs:
					if (!chkProductionAccess.Checked)
					{
						messageBox.Button = MessageButton.YesNo;
						messageBox.Icon = MessageIcon.Question;
						messageBox.Message =
							"You are attempting to send a production approval notification for a developer access request that has not been authorized for producation access, do you want to continue?";
						messageBox.Visible = true;
						break;
					}
					litTitleQualifier.Text = "Production";
					pnlEditOriginalUser.Visible = true;
					pnlDimScreen.Visible = true;
					break;

				case NotifyDevelopmentApprovedArgs:
					if (!chkAccess.Checked)
					{
						messageBox.Button = MessageButton.YesNo;
						messageBox.Icon = MessageIcon.Question;
						messageBox.Message =
							"You are attempting to send a development approval notification for a developer access request that has not been authorized for development access, do you want to continue?";
						messageBox.Visible = true;
						break;
					}
					litTitleQualifier.Text = "Development";
					pnlEditOriginalUser.Visible = true;
					pnlDimScreen.Visible = true;
					break;
			}
		}



		protected void Page_Load(object sender, EventArgs e)
		{
			new DeveloperAccessRequestListingHandler(this).Initialize();

			memberToolBar.ShowSave = Access.Modify;
			memberToolBar.ShowDelete = Access.Remove;

			memberToolBar.LoadAdditionalActions(ToolbarMoreActions());

			if (IsPostBack) return;

			if (Session[WebApplicationConstants.TransferDeveloperAccessRequestId] != null)
			{
				ProcessTransferredRequest(Session[WebApplicationConstants.TransferDeveloperAccessRequestId].ToLong());
				Session[WebApplicationConstants.TransferDeveloperAccessRequestId] = null;
			}

			SetEditStatus(false);
		}


		protected void OnOkayProcess(object sender, EventArgs e)
		{
			messageBox.Visible = false;
		}

		protected void OnNoProcess(object sender, EventArgs e)
		{
			messageBox.Visible = false;
			hidFlag.Value = null;
		}

		protected void OnYesProcess(object sender, EventArgs e)
		{
			messageBox.Visible = false;
			pnlEditOriginalUser.Visible = true;
			pnlDimScreen.Visible = true;
		}


		protected void OnCloseClicked(object sender, EventArgs eventArgs)
		{
			pnlEditOriginalUser.Visible = false;
			pnlDimScreen.Visible = false;
			txtName.Text = string.Empty;
		}

		protected void OnContinueClick(object sender, EventArgs e)
		{
			var name = txtName.Text;
			SendApprovalNotification(name);
			txtName.Text = string.Empty;
			pnlEditOriginalUser.Visible = false;
			pnlDimScreen.Visible = false;
		}
	}
}