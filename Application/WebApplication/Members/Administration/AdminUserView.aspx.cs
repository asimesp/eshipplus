﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Administration;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Administration;
using LogisticsPlus.Eship.WebApplication.Members.Administration.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Administration
{
    public partial class AdminUserView : AdminMemberPageBase, IAdminUserView
    {
        private const string PermissionsHeader = "Permissions";

        private const string SaveFlag = "S";

        public static string PageAddress { get { return "~/Members/Administration/AdminUserView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.AdminUser; } }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.AdminBlue; }
        }

        public event EventHandler<ViewEventArgs<AdminUser>> Save;
        public event EventHandler<ViewEventArgs<AdminUser>> Delete;
        public event EventHandler<ViewEventArgs<AdminUser>> LoadAuditLog;

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == SaveFlag)
            {
                SetEditStatus(false);
            }


            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;

            litErrorMessages.Text = messages.HasErrors()
                                        ? string.Join(WebApplicationConstants.HtmlBreak,
                                                      messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
                                                        Select(m => m.Message).ToArray())
                                        : string.Empty;
        }

        public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs)
        {
            auditLogs.Load(logs);
        }

        public void Set(AdminUser adminUser)
        {
            LoadUser(adminUser);
            SetEditStatus(!adminUser.IsNew);
        }


        private void SetEditStatus(bool enabled)
        {
            pnlDetails.Enabled = enabled;
            pnlPermissions.Enabled = enabled;

            memberToolBar.EnableSave = enabled;

            litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;
        }

        private List<Permission> RetrievePermissions()
        {
            return lstPermissions.Items
                .Select(item => ((PermissionLineItemControl)item.FindControl("pcPermissionLineItem")).RetreivePermission<Permission>())
                .ToList();
        }


        private void LoadUser(AdminUser adminUser)
        {
            txtUsername.Text = adminUser.Username;
            if (adminUser.IsNew) txtPassword.Text = string.Empty;
            txtFirstName.Text = adminUser.FirstName;
            txtLastName.Text = adminUser.LastName;
            txtEmail.Text = adminUser.Email;
            txtNotes.Text = adminUser.Notes;
            chkEnabled.Checked = adminUser.Enabled;

            hidAdminUserId.Value = adminUser.Id.ToString();

            //Permisions
            lstPermissions.DataSource = adminUser.AdminUserPermissions.OrderBy(p => p.Code);
            lstPermissions.DataBind();
            tabPermissions.HeaderText = adminUser.AdminUserPermissions.BuildTabCount(PermissionsHeader);
        }

        private void ProcessTransferredRequest(AdminUser adminUser)
        {
            if (adminUser.IsNew) return;

            LoadUser(adminUser);

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<AdminUser>(adminUser));
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            new AdminUserHandler(this).Initialize();

            memberToolBar.ShowSave = Access.Modify;
            memberToolBar.ShowDelete = Access.Remove;
            memberToolBar.ShowNew = Access.Modify;
            memberToolBar.ShowEdit = Access.Modify;
            adminUserFinder.OpenForEditEnabled = Access.Modify;
            btnAddPermission.Enabled = Access.Modify;

            if (IsPostBack) return;

            SetEditStatus(false);

            if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.TransferNumber]))
                ProcessTransferredRequest(new AdminUser(Request.QueryString[WebApplicationConstants.TransferNumber].UrlTextDecrypt().ToLong()));
        }


        protected void OnNewClicked(object sender, EventArgs e)
        {
            litErrorMessages.Text = string.Empty;

            LoadUser(new AdminUser());
            DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
            SetEditStatus(true);
        }

        protected void OnEditClicked(object sender, EventArgs e)
        {
            var user = new AdminUser(hidAdminUserId.Value.ToLong(), false);

            if (!user.IsNew)
                SetEditStatus(true);

            LoadUser(user);
        }

        protected void OnSaveClicked(object sender, EventArgs e)
        {
            var id = hidAdminUserId.Value.ToLong();
            var adminUser = new AdminUser(id, id != default(long))
            {
                Username = txtUsername.Text,
                FirstName = txtFirstName.Text,
                LastName = txtLastName.Text,
                Email = txtEmail.Text.StripSpacesFromEmails(),
                Notes = txtNotes.Text,
                Enabled = chkEnabled.Checked,
            };
            if (!string.IsNullOrEmpty(txtPassword.Text)) adminUser.Password = txtPassword.Text;
            adminUser.LoadCollections();

            // update permissions
            var vps = RetrievePermissions().ToDictionary(p => p.Code, p => p);
            var remove = adminUser.AdminUserPermissions.Where(p => !vps.ContainsKey(p.Code)).Select(p => p.Code).ToList();
            for (int i = adminUser.AdminUserPermissions.Count - 1; i > -1; i--) if (remove.Contains(adminUser.AdminUserPermissions[i].Code))
                    adminUser.AdminUserPermissions.RemoveAt(i);
            foreach (var p in adminUser.AdminUserPermissions)
            {
                p.TakeSnapShot();
                p.Description = vps[p.Code].Description;
                p.Grant = vps[p.Code].Grant;
                p.Modify = vps[p.Code].Modify;
                p.ModifyApplicable = vps[p.Code].ModifyApplicable;
                p.Remove = vps[p.Code].Remove;
                p.DeleteApplicable = vps[p.Code].DeleteApplicable;
                p.Deny = vps[p.Code].Deny;
                vps.Remove(p.Code); // remove updated permission
            }
            foreach (var key in vps.Keys)// add new
            {
                adminUser.AdminUserPermissions.Add(new AdminUserPermission
                {
                    Description = vps[key].Description,
                    Grant = vps[key].Grant,
                    Modify = vps[key].Modify,
                    ModifyApplicable = vps[key].ModifyApplicable,
                    Remove = vps[key].Remove,
                    DeleteApplicable = vps[key].DeleteApplicable,
                    Deny = vps[key].Deny,
                    Code = vps[key].Code,
                    AdminUser = adminUser,
                });
            }

            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<AdminUser>(adminUser));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<AdminUser>(adminUser));
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var adminUser = new AdminUser(hidAdminUserId.Value.ToLong(), false);
            if (Delete != null)
                Delete(this, new ViewEventArgs<AdminUser>(adminUser));
            adminUserFinder.Reset();
        }

        protected void OnFindClicked(object sender, EventArgs e)
        {
            adminUserFinder.Visible = true;
        }


        protected void OnAdminUserFinderItemSelected(object sender, ViewEventArgs<AdminUser> e)
        {
            litErrorMessages.Text = string.Empty;

            var adminUser = e.Argument;

            LoadUser(adminUser);

            adminUserFinder.Visible = false;

            SetEditStatus(adminUserFinder.OpenForEdit);

            if (LoadAuditLog != null) LoadAuditLog(this, new ViewEventArgs<AdminUser>(adminUser));
        }

        protected void OnAdminUserFinderSelectionCancelled(object sender, EventArgs e)
        {
            adminUserFinder.Visible = false;
        }


        protected void OnPermissionSelectorItemsSelected(object sender, ViewEventArgs<List<Permission>> e)
        {
            var permissions = RetrievePermissions();
            permissions.AddRange(e.Argument);

            lstPermissions.DataSource = permissions.OrderBy(p => p.Code);
            lstPermissions.DataBind();
            tabPermissions.HeaderText = permissions.BuildTabCount(PermissionsHeader);

            permissionSelector.Visible = false;
        }

        protected void OnPermissionSelectorSelectionCancelled(object sender, EventArgs e)
        {
            permissionSelector.Visible = false;
        }


        protected void BindPermissionLineItem(object sender, ListViewItemEventArgs e)
        {
            var lineItem = (PermissionLineItemControl)e.Item.FindControl("pcPermissionLineItem");
            if (lineItem == null) return;
            lineItem.LoadPermission((Permission)e.Item.DataItem);
        }

        protected void OnAddPermissionClicked(object sender, EventArgs e)
        {
            permissionSelector.CodesToExclude = RetrievePermissions().Select(s => s.Code).ToList();
            permissionSelector.Visible = true;
        }

        protected void OnClearPermissionsClicked(object sender, EventArgs e)
        {
            lstPermissions.DataSource = new List<Permission>();
            lstPermissions.DataBind();
            tabPermissions.HeaderText = PermissionsHeader;
            athtuTabUpdater.SetForUpdate(tabPermissions.ClientID, PermissionsHeader);
        }

        protected void OnPermissionItemRemovePermission(object sender, ViewEventArgs<string> e)
        {
            var permissions = RetrievePermissions();
            for (var i = permissions.Count - 1; i > -1; i--)
                if (permissions[i].Code == e.Argument)
                {
                    permissions.RemoveAt(i);
                    break;
                }
            lstPermissions.DataSource = permissions;
            lstPermissions.DataBind();
            tabPermissions.HeaderText = permissions.BuildTabCount(PermissionsHeader);
            athtuTabUpdater.SetForUpdate(tabPermissions.ClientID, permissions.BuildTabCount(PermissionsHeader));
        }
    }
}
