﻿using LogisticsPlus.Eship.Core.Administration;

namespace LogisticsPlus.Eship.WebApplication.Members.Administration
{
	public abstract class AdminMemberPageBase : MemberPageBase
	{
		public AdminUser ActiveSuperUser
		{
			get { return Session[WebApplicationConstants.ActiveSuperUser] as AdminUser; }
			set { Session[WebApplicationConstants.ActiveSuperUser] = value; }
		}

		protected long ActiveSuperUserId
		{
			get { return ActiveSuperUser == null ? default(long) : ActiveSuperUser.Id; }
		}
	}
}
