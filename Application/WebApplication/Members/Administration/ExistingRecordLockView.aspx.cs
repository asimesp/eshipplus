﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Administration;
using LogisticsPlus.Eship.Processor.Handlers.Administration;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Administration;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Administration
{
    public partial class ExistingRecordLockView : MemberPageBase, IExistingRecordLockView
    {
        public static string PageAddress
        {
            get { return "~/Members/Administration/ExistingRecordLockView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.ExistingRecordLocks; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.AdminBlue; }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<long>> ReleaseLock;

        public void DisplayExistingLocks(List<LockDto> locks)
        {
            lvwRegistry.DataSource = locks;
            lvwRegistry.DataBind();
            litRecordCount.Text = locks.BuildRecordCount();
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            new ExistingRecordLockHandler(this).Initialize();

            if (IsPostBack) return;

            if (Loading != null)
                Loading(this, new EventArgs());
        }

        protected void OnDeleteClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("lockId").ToCustomHiddenField();

            if (ReleaseLock != null)
                ReleaseLock(this, new ViewEventArgs<long>(hidden.Value.ToLong()));

        }


        [ScriptMethod]
        [WebMethod]
        public static object RetrieveEntityDetails(string entityCode, string entityId)
        {
            return AuditLogView.RetrieveEntityDetails(entityCode, entityId);
        }

        protected void OnGoToRecord(object sender, EventArgs e)
        {
            this.GoToRecord2(hidGoToEntityCode.Value, hidGoToEntityId.Value);
        }
    }
}
