﻿namespace LogisticsPlus.Eship.WebApplication.Members.Administration
{
	public enum PermissionGroup
	{
		Registry = 0,
		Core,
		Administration, 
		Accounting,
		Operations,
		BusinessIntelligence = 5,
		Connect,
		Rating,
		Sales,
        SuperUser,
	}
}
