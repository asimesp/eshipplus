﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Administration
{
    public partial class ManageUploadedImageView : MemberPageBase
    {
        public override ViewCode PageCode { get { return ViewCode.ManageUploadedImage; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.AdminBlue; } }

        public static string PageAddress { get { return "~/Members/Administration/ManageUploadedImageView.aspx"; } }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Button = MessageButton.Ok;
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }


        private void LoadAndDisplayImages()
        {
            var virtualPath = WebApplicationSettings.UploadedImagesFolder(ActiveUser.TenantId);
            var physicalPath = Server.MapPath(virtualPath);

            if (!Directory.Exists(physicalPath))
            {
                lstUploadedImages.DataSource = null;
                lstUploadedImages.DataBind();
                return;
            }

            var images = new DirectoryInfo(physicalPath)
                .GetFiles()
                .Select(c => new
                {
                    ImageUrl = virtualPath + c,
                    ImageName = c,
                    FullImagePath = Request.ResolveSiteRootWithHttp() + virtualPath.Replace("~", "") + c
                })
                .ToList();

            litRecordCount.Text = images.BuildRecordCount();
            lstUploadedImages.DataSource = images;
            lstUploadedImages.DataBind();
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            btnUploadImage.Enabled = Access.Modify;

            LoadAndDisplayImages();
        }

        protected void OnUploadImageClicked(object sender, EventArgs e)
        {
            var virtualPath = WebApplicationSettings.UploadedImagesFolder(ActiveUser.TenantId);
            var physicalPath = Server.MapPath(virtualPath);

            try
            {
                if (fupImage.HasFile)
                {
                    //ensure unique filename
                    var fi = new FileInfo(Server.MakeUniqueFilename(Path.Combine(physicalPath, fupImage.FileName.Replace(" ", ""), string.Empty), true));
                    fupImage.WriteToFile(physicalPath, fi.Name, string.Empty);

                    LoadAndDisplayImages();
                    return;
                }
                DisplayMessages(new[] { ValidationMessage.Information("File not present") });
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex, Context);
                DisplayMessages(new[] { ValidationMessage.Error("There was an error processing the upload of this image") });
            }
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            if (!Server.RemoveFiles(new[] { ((Image)((LinkButton)sender).Parent.FindControl("imgImage")).ImageUrl }))
                DisplayMessages(new[] { ValidationMessage.Error("Error deleting file.") });

            LoadAndDisplayImages();

        }
    }
}