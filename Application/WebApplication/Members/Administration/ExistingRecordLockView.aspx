﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="ExistingRecordLockView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Administration.ExistingRecordLockView" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Administration" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true"/>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Existing Record Locks
                <small class="ml10"><asp:Literal runat="server" ID="litRecordCount" Text="" /></small>
            </h3>
        </div>
        <hr class="fat" />
        <script type="text/javascript">

            function DisplayEntityDetails(entityCode, entityId) {
                $.ajax({
                    type: "post",
                    url: "ExistingRecordLockView.aspx/RetrieveEntityDetails",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: '{"entityCode":"' + entityCode + '", "entityId":"' + entityId + '"}',
                    success: function (result) {
                        if (result.d != null) {

                            if (!jsHelper.IsTrue(result.d.CanJumpTo)) {
                                $(jsHelper.AddHashTag('<%= btnGoToRecord.ClientID%>')).hide();
                        } else {
                            $(jsHelper.AddHashTag('<%= btnGoToRecord.ClientID%>')).show();
                        }

                        $(jsHelper.AddHashTag('descDiv')).html(result.d.Desc);
                        $(jsHelper.AddHashTag('<%= hidGoToEntityCode.ClientID%>')).val(result.d.EntityCode);
                        $(jsHelper.AddHashTag('<%= hidGoToEntityId.ClientID%>')).val(result.d.EntityId);
                    }

                    $(jsHelper.AddHashTag('<%= pnlEntityDetails.ClientID%>')).show();
                    $(jsHelper.AddHashTag('<%= pnlDimScreenJS.ClientID%>')).show();
                },
                error: function (xhr, status, error) {
                    alert(error);
                }
            });
        }

        function CloseEntityDetails() {
            $(jsHelper.AddHashTag('<%= pnlEntityDetails.ClientID%>')).hide();
            $(jsHelper.AddHashTag('<%= pnlDimScreenJS.ClientID%>')).hide();
        }

        </script>

        <asp:Panel runat="server" ID="pnlEntityDetails"  Style="display: none;">
            <div class="popupControl popupControlOverW750">
                <div class="popheader">
                    <h4>Entity Details
                    </h4>
                    <asp:Image ID="imgClose" ImageUrl="~/images/icons2/close.png" CssClass="close" runat="server" onclick="javascript:CloseEntityDetails();" />
                </div>
                <div class="row">
                    <table class="poptable">
                        <tr>
                            <td style="width: 25%;" class="top pt10 pl10">Description:
                            </td>
                            <td class="text-left top pt10">
                                <div id="descDiv"></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right pr10" colspan="20">
                                <eShip:CustomHiddenField runat="server" ID="hidGoToEntityCode" />
                                <eShip:CustomHiddenField runat="server" ID="hidGoToEntityId" />
                                <asp:Button ID="btnGoToRecord" Text="Go To Record" OnClick="OnGoToRecord" runat="server"
                                    CausesValidation="false" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
        
        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheExistingRecordTable" TableId="existingRecordTable" HeaderZIndex="2"/>
        <asp:ListView ID="lvwRegistry" runat="server" ItemPlaceholderID="itemPlaceHolder">
            <LayoutTemplate>
                <div class="rowgroup">
                    <table id="existingRecordTable" class="line2 pl2">
                        <tr>
                            <th style="width: 18%;">Entity Locked</th>
                            <th style="width: 10%;">Entity Id</th>
                            <th style="width: 18%;">Lock Date Time</th>
                            <th style="width: 20%;">Locked By (Username)</th>
                            <th style="width: 29%;">Locked By (Full Name)</th>
                            <th style="width: 5%;" class="text-center">Action</th>
                        </tr>
                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                    </table>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <eShip:CustomHiddenField ID="lockId" runat="server" Value='<%# Eval("Id") %>' />
                        <%# Eval("LockKey") %>
                    </td>
                    <td>
                        <a class="blue" href='<%# string.Format("javascript:DisplayEntityDetails(\"{0}\", \"{1}\")", Eval("LockKey"), Eval("EntityId")) %>'><%# Eval("EntityId") %></a>
                    </td>
                    <td>
                        <%# Eval("LockDateTime") %>
                    </td>
                    <td>
                        <%# Eval("Username") %>
                        <%# ActiveUser.HasAccessTo(ViewCode.User) 
                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To User Record'><img src='{3}' alt='' width='16' align='absmiddle'/></a>", 
                                    ResolveUrl(UserView.PageAddress),
                                    WebApplicationConstants.TransferNumber, 
                                    Eval("LockUserId").GetString().UrlTextEncrypt(),
                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                : string.Empty  %>
                    </td>
                    <td>
                        <%# Eval("FirstName") %> <%# Eval("LastName")%>
                    </td>
                    <td class="text-center">
                        <asp:ImageButton ID="ibtnDelete" runat="server" ToolTip="Delete" ImageUrl="~/images/icons2/deleteX.png"
                            CausesValidation="false" OnClick="OnDeleteClicked" Enabled='<%# Access.Remove %>' />
                        <ajax:ConfirmButtonExtender runat="server" ID="cbeDelete" TargetControlID="ibtnDelete"
                            ConfirmText="Are you sure you want to delete record?" />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
    </div>
    <asp:Panel ID="pnlDimScreenJS" CssClass="dimBackgroundControl" runat="server" Style="display: none;" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />

</asp:Content>

