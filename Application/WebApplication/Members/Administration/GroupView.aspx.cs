﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Administration;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Administration;
using LogisticsPlus.Eship.WebApplication.Members.Administration.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Administration
{
	public partial class GroupView : MemberPageBaseWithPageStore, IGroupView
	{
		private const string PermissionsHeader = "Permissions";
		private const string UsersHeader = "Users";

		private const string SaveFlag = "S";

		public static string PageAddress { get { return "~/Members/Administration/GroupView.aspx"; } }

		public override ViewCode PageCode { get { return ViewCode.Group; } }

		public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.AdminBlue; } }
		
		private const string GroupUserStoreKey = "GUSK";
		private List<User> StoredGoupUsers
		{
			get { return PageStore[GroupUserStoreKey] as List<User> ?? new List<User>(); }
			set { PageStore[GroupUserStoreKey] = value; }
		}

		public event EventHandler<ViewEventArgs<Group>> Save;
		public event EventHandler<ViewEventArgs<Group>> Delete;
		public event EventHandler<ViewEventArgs<Group>> Lock;
		public event EventHandler<ViewEventArgs<Group>> UnLock;
		public event EventHandler<ViewEventArgs<Group>> LoadAuditLog;

		public void LogException(Exception ex)
		{
			if (ex != null) ErrorLogger.LogError(ex, Context);
		}

		public void DisplayMessages(IEnumerable<ValidationMessage> messages)
		{
			if (messages == null) return;
			if (!messages.Any()) return;

			if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == SaveFlag)
			{
				// unlock and return to read-only
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<Group>(new Group(hidGroupId.Value.ToLong(), false)));
				SetEditStatus(false);
				memberToolBar.ShowUnlock = false;
			}

			messageBox.Icon = messages.GenerateIcon();
			messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
			messageBox.Visible = true;

			litErrorMessages.Text = messages.HasErrors()
										? string.Join(WebApplicationConstants.HtmlBreak,
													  messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
														Select(m => m.Message).ToArray())
										: string.Empty;
		}

		public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs) { auditLogs.Load(logs); }

		public void FailedLock(Lock @lock)
		{
			memberToolBar.ShowNew = false;
			memberToolBar.ShowSave = false;
			memberToolBar.ShowDelete = false;
			memberToolBar.ShowUnlock = false;

			btnAddPermission.Enabled = Access.Modify;
			btnAddUser.Enabled = Access.Modify;

			foreach (var item in lstPermissions.Items)
                ((PermissionLineItemControl)item.FindControl("pcPermissionLineItem")).EnableDelete = false;

			foreach (var item in lstUsers.Items)
				((ImageButton)item.FindControl("ibtnDeleteUser")).Enabled = false;

			litMessage.Text = @lock.BuildFailedLockMessage();
		}

	
		public void Set(Group group)
		{
			LoadGroup(group);
			SetEditStatus(!group.IsNew);
		}

		private static List<User> SortUsers(IEnumerable<User> lstUsers)
		{
			return lstUsers
				.OrderBy(t => t.Username)
				.ThenBy(t=>t.FirstName)
				.ThenBy(t=>t.LastName)
				.ToList();
		}

		private void SetEditStatus(bool enabled)
		{
			pnlDetails.Enabled = enabled;
			pnlPermissions.Enabled = enabled;
			pnlUsers.Enabled = enabled;
			memberToolBar.EnableSave = enabled;
			
			litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;
		}


		private List<Permission> RetrievePermissions()
		{
			return lstPermissions.Items
				.Select(item => ((PermissionLineItemControl)item.FindControl("pcPermissionLineItem")).RetreivePermission<Permission>())
				.ToList();
		}

		private void LoadGroup(Group group)
		{
			hidGroupId.Value = group.Id.ToString();

			txtGroupName.Text = group.Name;
			txtDescription.Text = group.Description;

			//Permissions
			lstPermissions.DataSource = group.Permissions.OrderBy(p => p.Code);
			lstPermissions.DataBind();
			tabPermissions.HeaderText = group.Permissions.BuildTabCount(PermissionsHeader);

			//Users
			var users = group.RetrieveGroupUsers();
			StoredGoupUsers = SortUsers(users);
			peLstUsers.LoadData(SortUsers(users));
		
			tabUsers.HeaderText = users.BuildTabCount(UsersHeader);

			memberToolBar.ShowUnlock = group.HasUserLock(ActiveUser, group.Id);
			
		}

	


		private void ProcessTransferredRequest(long groupId)
		{
			if (groupId != default(long))
			{
				var group = new Group(groupId);
				LoadGroup(group);

				if (LoadAuditLog != null)
					LoadAuditLog(this, new ViewEventArgs<Group>(group));
			}
		}



		protected void Page_Load(object sender, EventArgs e)
		{
			new GroupHandler(this).Initialize();

			memberToolBar.ShowSave = Access.Modify;
			memberToolBar.ShowDelete = Access.Remove;
			memberToolBar.ShowNew = Access.Modify;
			memberToolBar.ShowEdit = Access.Modify;

			groupFinder.OpenForEditEnabled = Access.Modify;
		
			btnAddPermission.Enabled = Access.Modify;
			btnAddUser.Enabled = Access.Modify;
			
			peLstUsers.ParentDataStoreKey = GroupUserStoreKey;

			matchingUser.UsersDataStoreKey = GroupUserStoreKey;
			
			if (IsPostBack) return;

			if (Session[WebApplicationConstants.TransferGroupId] != null)
			{
				ProcessTransferredRequest(Session[WebApplicationConstants.TransferGroupId].ToLong());
				Session[WebApplicationConstants.TransferGroupId] = null;
			}

			SetEditStatus(false);
		}


		protected void OnNewClicked(object sender, EventArgs e)
		{
			litErrorMessages.Text = string.Empty;

			LoadGroup(new Group());
			DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
			SetEditStatus(true);
		}

		protected void OnEditClicked(object sender, EventArgs e)
		{
			var @group = new Group(hidGroupId.Value.ToLong(), false);

			if (Lock != null && !@group.IsNew)
			{
				SetEditStatus(true);
				memberToolBar.ShowUnlock = true;
				Lock(this, new ViewEventArgs<Group>(@group));
			}

			LoadGroup(@group);
		}

		protected void OnUnlockClicked(object sender, EventArgs e)
		{
			var group = new Group(hidGroupId.Value.ToLong(), false);
			if (UnLock != null && !group.IsNew)
			{
				SetEditStatus(false);
				memberToolBar.ShowUnlock = false;
				UnLock(this, new ViewEventArgs<Group>(group));
			}
		}

		
		protected void OnSaveClicked(object sender, EventArgs e)
		{
			var group = new Group(hidGroupId.Value.ToLong(), true) { Name = txtGroupName.Text, Description = txtDescription.Text };
			if (group.IsNew) group.TenantId = ActiveUser.TenantId;
			group.LoadCollections();

			// update permissions
			var vps = RetrievePermissions().ToDictionary(p => p.Code, p => p);
			var remove = group.Permissions.Where(p => !vps.ContainsKey(p.Code)).Select(p => p.Code).ToList();
			for (int i = group.Permissions.Count - 1; i > -1; i--) if (remove.Contains(group.Permissions[i].Code))
					group.Permissions.RemoveAt(i);
			foreach (var p in group.Permissions)
			{
				p.TakeSnapShot();
				p.Description = vps[p.Code].Description;
				p.Grant = vps[p.Code].Grant;
				p.Modify = vps[p.Code].Modify;
				p.ModifyApplicable = vps[p.Code].ModifyApplicable;
				p.Remove = vps[p.Code].Remove;
				p.DeleteApplicable = vps[p.Code].DeleteApplicable;
				p.Deny = vps[p.Code].Deny;
				vps.Remove(p.Code); // remove updated permission
			}
			foreach (var key in vps.Keys)// add new
			{
				group.Permissions.Add(new GroupPermission
				{
					Description = vps[key].Description,
					Grant = vps[key].Grant,
					Modify = vps[key].Modify,
					ModifyApplicable = vps[key].ModifyApplicable,
					Remove = vps[key].Remove,
					DeleteApplicable = vps[key].DeleteApplicable,
					Deny = vps[key].Deny,
					Code = vps[key].Code,
					Group = group,
					TenantId = ActiveUser.TenantId,
				});
			}

			//update users
		
			var vUserIds = StoredGoupUsers.Select(p=>p.Id.ToLong());
			var uremove = group.GroupUsers.Where(gu => !vUserIds.Contains(gu.UserId)).Select(gu => gu.UserId).ToList();
			for (int i = group.GroupUsers.Count - 1; i > -1; i--) if (uremove.Contains(group.GroupUsers[i].UserId))
					group.GroupUsers.RemoveAt(i);
			var presentUser = group.GroupUsers.Select(gu => gu.UserId);
			var uIdadd = vUserIds.Where(vuId => !presentUser.Contains(vuId));
			foreach (var uId in uIdadd)
				group.GroupUsers.Add(new GroupUser { Group = group, TenantId = ActiveUser.TenantId, UserId = uId });

			hidFlag.Value = SaveFlag;

			// save
			if (Save != null)
				Save(this, new ViewEventArgs<Group>(group));


			// update audit log
			if (LoadAuditLog != null)
				LoadAuditLog(this, new ViewEventArgs<Group>(group));
		}

		protected void OnDeleteClicked(object sender, EventArgs e)
		{
			var group = new Group(hidGroupId.Value.ToLong(), false);

			if (Delete != null)
				Delete(this, new ViewEventArgs<Group>(group));

			groupFinder.Reset();
		}

		protected void OnFindClicked(object sender, EventArgs e)
		{
			groupFinder.Visible = true;
		}


		protected void OnGroupFinderSelectionCancelled(object sender, EventArgs e)
		{
			groupFinder.Visible = false;
		}

		protected void OnGroupFinderItemSelected(object sender, ViewEventArgs<Group> e)
		{
			litErrorMessages.Text = string.Empty;

			// check for previous items ...
			if (hidGroupId.Value.ToLong() != default(long))
			{
				var oldGroup = new Group(hidGroupId.Value.ToLong(), false);
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<Group>(oldGroup));
			}

			var group = e.Argument;

			LoadGroup(@group);

			groupFinder.Visible = false;

			if (groupFinder.OpenForEdit && Lock != null)
			{
				SetEditStatus(true);
				memberToolBar.ShowUnlock = true;
				Lock(this, new ViewEventArgs<Group>(group));
			}
			else
			{
				SetEditStatus(false);
			}

			if (LoadAuditLog != null) LoadAuditLog(this, new ViewEventArgs<Group>(group));
		}


		protected void OnPermissionSelectorItemsSelected(object sender, ViewEventArgs<List<Permission>> e)
		{
			var permissions = RetrievePermissions();
			permissions.AddRange(e.Argument);

			lstPermissions.DataSource = permissions.OrderBy(p => p.Code);
			lstPermissions.DataBind();
			tabPermissions.HeaderText = permissions.BuildTabCount(PermissionsHeader);

			permissionSelector.Visible = false;
		}

		protected void OnPermissionSelectorSelectionCancelled(object sender, EventArgs e)
		{
			permissionSelector.Visible = false;
		}


		protected void OnUserFinderMultiItemSelected(object sender, ViewEventArgs<List<User>> e)
		{
			var vUsers = StoredGoupUsers;
			var usersToAdd = e.Argument
				.Select(u => new User(u.Id))
				.Where(u => !vUsers.Select(vu => vu.Username).Contains(u.Username));

			vUsers.AddRange(usersToAdd);

			peLstUsers.LoadData(SortUsers(vUsers), peLstUsers.CurrentPage);
			tabUsers.HeaderText = vUsers.BuildTabCount(UsersHeader);
			userFinder.Visible = false;
		}

		protected void OnUserFinderSelectionCancelled(object sender, EventArgs e)
		{
			userFinder.Visible = false;
		}


		protected void BindPermissionLineItem(object sender, ListViewItemEventArgs e)
		{
			var lineItem = (PermissionLineItemControl)e.Item.FindControl("pcPermissionLineItem");
			if (lineItem == null) return;
			lineItem.LoadPermission((Permission)e.Item.DataItem);
		}

		protected void OnAddPermissionClicked(object sender, EventArgs e)
		{
			permissionSelector.CodesToExclude = RetrievePermissions().Select(s => s.Code).ToList();
			permissionSelector.Visible = true;
		}

		protected void OnClearPermissionsClicked(object sender, EventArgs e)
		{
			lstPermissions.DataSource = new List<Permission>();
			lstPermissions.DataBind();
			tabPermissions.HeaderText = PermissionsHeader;
            athtuTabUpdater.SetForUpdate(tabPermissions.ClientID, PermissionsHeader);
		}

        protected void OnPermissionItemRemovePermission(object sender, ViewEventArgs<string> e)
        {
            var permissions = RetrievePermissions();
            for (var i = permissions.Count - 1; i > -1; i--)
                if (permissions[i].Code == e.Argument)
                {
                    permissions.RemoveAt(i);
                    break;
                }
            lstPermissions.DataSource = permissions;
            lstPermissions.DataBind();
            tabPermissions.HeaderText = permissions.BuildTabCount(PermissionsHeader);
            athtuTabUpdater.SetForUpdate(tabPermissions.ClientID, permissions.BuildTabCount(PermissionsHeader));
        }


		protected void OnAddUserClicked(object sender, EventArgs e)
		{
			userFinder.Visible = true;
		}
        
		protected void OnClearUsersClicked(object sender, EventArgs e)
		{
			peLstUsers.LoadData(new List<User>());
			tabUsers.HeaderText = UsersHeader;
            athtuTabUpdater.SetForUpdate(tabUsers.ClientID, UsersHeader);
		}

		protected void OnFindMatchingUserClicked(object sender, EventArgs e)
		{
			matchingUser.Visible = true;
		}

		protected void OnDeleteUserClicked(object sender, ImageClickEventArgs e)
		{
			var idToRemove = ((ImageButton)sender).FindControl("hidUserId").ToCustomHiddenField().Value.ToInt();
			var vUsers = StoredGoupUsers;
		    
			vUsers = vUsers.Where(v => v.Id != idToRemove).ToList();
			peLstUsers.LoadData(SortUsers(vUsers), peLstUsers.CurrentPage);
            tabUsers.HeaderText = vUsers.BuildTabCount(UsersHeader);
            athtuTabUpdater.SetForUpdate(tabUsers.ClientID, vUsers.BuildTabCount(UsersHeader));
		}


		protected void OnMatchingUserControlClose(object sender, EventArgs e)
		{
			matchingUser.Visible = false;
			pnlDimScreen.Visible = false;
		}

		protected void OnMatchingUserControlSelectedIndex(object sender, ViewEventArgs<int> e)
		{
			if (tabUsers.Visible)
			{
				peLstUsers.LoadPageWithRecordIndex(e.Argument);
				tabContainerMain.ActiveTab = tabUsers;
				matchingUser.Visible = false;
				pnlDimScreen.Visible = false;
			}
			else
			{
				DisplayMessages(new[] {ValidationMessage.Error("Users current inaccesible.  Tab not visible!")});
			}
		}
		}
	}
