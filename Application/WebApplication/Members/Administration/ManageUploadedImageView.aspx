﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="ManageUploadedImageView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Administration.ManageUploadedImageView" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Manage Uploaded Images<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" Text="" /></small>
            </h3>
        </div>
        <hr class="fat" />

        <div class="rowgroup mb0">
            <div class="row mb10">
                <div class="fieldgroup">
                    <label class="upper blue">Image: </label>
                    <asp:FileUpload ID="fupImage" runat="server" CssClass="jQueryUniform" />
                    <asp:Button ID="btnUploadImage" runat="server" Text="Upload Image" OnClick="OnUploadImageClicked" CssClass="ml10" />
                </div>
            </div>
        </div>
        <hr class="dark mb20" />
        <div>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('[id$="imgImage"]').click(function () {
                        // get image url and image object to get true width of image
                        var imgUrl = $(this).attr('src');
                        $(jsHelper.AddHashTag('<%= imgSelected.ClientID %>')).attr('src', imgUrl);
                        var img = new Image();
                        img.src = imgUrl;

                        // set styling for positioning of the image clicked on 
                        var divStyle = $(jsHelper.AddHashTag('divImageDisplay')).attr('style');
                        divStyle += 'width:auto !important; margin-left: -' + Number(img.width / 2) + 'px !important;';
                        $(jsHelper.AddHashTag('divImageDisplay')).attr('style', divStyle);
                        $(jsHelper.AddHashTag('<%= imgClose.ClientID %>')).attr('style', 'position: absolute !important; margin-left: ' + Number(img.width - 10) + 'px !important; top: -10px;');

                        // display image that was clicked on
                        $(jsHelper.AddHashTag('divImageDisplay')).show();
                        $(jsHelper.AddHashTag('<%= pnlDimScreenJS.ClientID %>')).show();
                    });

                    $(jsHelper.AddHashTag('<%= imgClose.ClientID%>')).click(function () {
                        $(jsHelper.AddHashTag('divImageDisplay')).hide();
                        $(jsHelper.AddHashTag('<%= pnlDimScreenJS.ClientID %>')).hide();
                    });
                })
            </script>
            <asp:ListView runat="server" ID="lstUploadedImages" ItemPlaceholderID="itemPlaceholder">
                <LayoutTemplate>
                    <asp:PlaceHolder runat="server" ID="itemPlaceholder" />
                </LayoutTemplate>
                <ItemTemplate>
                    <div class="fieldgroup ml10 mr10 mb10 text-center uploadedImages">
                        <asp:Image ImageUrl='<%# Eval("ImageUrl") %>' ID="imgImage" alt="Images" runat="server" Width="90px" />
                        <br />
                        <label class="text-center">
                            <%# Eval("ImageName") %>
                        </label>
                        <br />
                        <label class="text-center">
                            <asp:LinkButton runat="server" ID="lnkDelete" Text="[Delete]" Visible='<%# Access.Remove %>' OnClick="OnDeleteClicked" CssClass="blue"/>
                        </label>
                    </div>
                </ItemTemplate>
            </asp:ListView>
        </div>
    </div>
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
    <div class="popup trans-background no-border text-center" id="divImageDisplay" style="display: none;">
        <asp:Image runat="server" ID="imgClose" ImageUrl="~/images/icons2/deleteX.png" />
        <asp:Image runat="server" ID="imgSelected" BackColor="White" />
    </div>
    <asp:Panel ID="pnlDimScreenJS" CssClass="dimBackground" runat="server" Style="display: none;" />
</asp:Content>
