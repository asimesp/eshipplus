﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PermissionLineItemControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Administration.Controls.PermissionLineItemControl" %>
<td>
	<asp:Literal ID="litCode" runat="server" />
</td>
<td>
	<asp:Literal ID="litDescription" runat="server" />
	</td>
<td class="text-center">
	<eShip:AltUniformCheckBox ID="chkGrant" runat="server" />
	</td>
<td class="text-center">
	<eShip:AltUniformCheckBox ID="chkDeny" runat="server" />
	</td>
<td class="text-center">
	<eShip:AltUniformCheckBox ID="chkModify" runat="server" />
	</td>
<td class="text-center">
	<eShip:AltUniformCheckBox ID="chkDelete" runat="server" />
</td>
<td class="text-center">
	<asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
		OnClick="OnDeleteClicked" CausesValidation="False"/>
</td>