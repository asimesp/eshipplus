﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Administration;
using LogisticsPlus.Eship.Processor.Handlers.Administration;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Administration;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Administration.Controls
{
    public partial class UserFinderControl : MemberControlBase, IUserFinderView
    {
        public new bool Visible
        {
            get { return pnlUserFinderContent.Visible; }
            set
            {
                base.Visible = true;
                pnlUserFinderContent.Visible = value;
                pnlUserFinderDimScreen.Visible = value;
            }
        }

        public bool OpenForEdit
        {
            get { return hidEditSelected.Value.ToBoolean(); }
        }

        public bool OpenForEditEnabled
        {
            get { return hidOpenForEditEnabled.Value.ToBoolean(); }
            set { hidOpenForEditEnabled.Value = value.ToString(); }
        }

        public bool EnableMultiSelection
        {
            get { return hidUserFinderEnableMultiSelection.Value.ToBoolean(); }
            set
            {
                hidUserFinderEnableMultiSelection.Value = value.ToString();
                btnSelectAll.Visible = value;
                btnSelectAll2.Visible = value;
                upcseDataUpdate.PreserveRecordSelection = value;
            }
        }

        public bool FilterForEmployees
        {
            get { return hidFilterEmployeeOnly.Value.ToBoolean(); }
            set { hidFilterEmployeeOnly.Value = value.ToString(); }
        }

        public bool FilterForNonEmployees
        {
            get { return hidFilterNonEmployeeOnly.Value.ToBoolean(); }
            set { hidFilterNonEmployeeOnly.Value = value.ToString(); }
        }

        public bool FilterForCarrierCoordinator
        {
            get { return hidFilterForCarrierCoordUser.Value.ToBoolean(); }
            set { hidFilterForCarrierCoordUser.Value = value.ToString(); }
        }

        public bool FilterForShipmentCoordinator
        {
            get { return hidFilterForShipmentCoordUser.Value.ToBoolean(); }
            set { hidFilterForShipmentCoordUser.Value = value.ToString(); }
        }

        public bool MatchNonEmployeeDeptToActiveUser
        {
            get { return hidFilterForNonEmployeeUserDepartment.Value.ToBoolean(); }
            set { hidFilterForNonEmployeeUserDepartment.Value = value.GetString(); }
        }

        public event EventHandler<ViewEventArgs<UserSearchCriteria>> Search;

        public event EventHandler<ViewEventArgs<User>> ItemSelected;
        public event EventHandler<ViewEventArgs<List<User>>> MultiItemSelected;
        public event EventHandler SelectionCancel;

        public void DisplaySearchResult(List<UserDto> users)
        {
            var results = users;

            if (!ActiveUser.TenantEmployee && MatchNonEmployeeDeptToActiveUser)
            {
                var deptCode = ActiveUser.Department == null ? string.Empty : ActiveUser.Department.Code;
                results = users
                    .Where(u => u.TenantEmployee || u.DepartmentCode == deptCode)
                    .ToList();
            }
            litRecordCount.Text = results.BuildRecordCount();
            upcseDataUpdate.DataSource = results;
            upcseDataUpdate.DataBind();
        }

        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems { Columns = GetCurrentRunParameters(true) };
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }


        public void Reset()
        {
            hidAreFiltersShowing.Value = true.ToString();
            DisplaySearchResult(new List<UserDto>());
        }


        private void ProcessItemSelection(long userId)
        {
            if (ItemSelected != null)
                ItemSelected(this, new ViewEventArgs<User>(new User(userId, false)));
        }


        private void DoSearchPreProcessingThenSearch()
        {
            var criteria = GetCurrentRunParameters(false);

            if (FilterForEmployees)
            {
                var column = AdminSearchFields.TenantEmployee.ToParameterColumn();
                column.DefaultValue = true.GetString();
                column.Operator = Operator.Equal;
                criteria.Add(column);
            }
            if (FilterForNonEmployees)
            {
                var column = AdminSearchFields.TenantEmployee.ToParameterColumn();
                column.DefaultValue = false.GetString();
                column.Operator = Operator.Equal;
                criteria.Add(column);
            }
            if (FilterForCarrierCoordinator)
            {
                var column = AdminSearchFields.IsCarrierCoordinator.ToParameterColumn();
                column.DefaultValue = true.GetString();
                column.Operator = Operator.Equal;
                criteria.Add(column);
            }
            if (FilterForShipmentCoordinator)
            {
                var column = AdminSearchFields.IsShipmentCoordinator.ToParameterColumn();
                column.DefaultValue = true.GetString();
                column.Operator = Operator.Equal;
                criteria.Add(column);
            }

            DoSearch(criteria);
        }

        private void DoSearch(List<ParameterColumn> columns)
        {
            var criteria = new UserSearchCriteria()
                {
                    Parameters = columns,
                    SortBy = null
                };

            if (Search != null)
                Search(this, new ViewEventArgs<UserSearchCriteria>(criteria));

            var control = lstSearchResults.FindControl("chkSelectAllRecords").ToAltUniformCheckBox();
            if (control != null)
            {
                control.Checked = false;
                control.Visible = EnableMultiSelection;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new UserFinderHandler(this).Initialize();

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            hypGoToUserProfile.NavigateUrl = UserProfileView.PageAddress;
            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);

            var columns = profile != null && profile.Columns != null
                            ? profile.Columns
                            : AdminSearchFields.DefaultUsers.Select(f => f.ToParameterColumn()).ToList();


            if (FilterForEmployees || FilterForNonEmployees) columns.RemoveAll(i => i.ReportColumnName == AdminSearchFields.TenantEmployee.ToParameterColumn().ReportColumnName);
            if (FilterForCarrierCoordinator) columns.RemoveAll(i => i.ReportColumnName == AdminSearchFields.IsCarrierCoordinator.ToParameterColumn().ReportColumnName);
            if (FilterForShipmentCoordinator) columns.RemoveAll(i => i.ReportColumnName == AdminSearchFields.IsShipmentCoordinator.ToParameterColumn().ReportColumnName);

            lstFilterParameters.DataSource = columns;
            lstFilterParameters.DataBind();
        }


        protected void OnCancelClicked(object sender, EventArgs eventArgs)
        {
            if (SelectionCancel != null)
                SelectionCancel(this, new EventArgs());
        }

        protected void OnSearchClicked(object sender, EventArgs e)
        {
            hidAreFiltersShowing.Value = ActiveUser.AlwaysShowFinderParametersOnSearch.ToString();
            upcseDataUpdate.ResetLoadCount();
            DoSearchPreProcessingThenSearch();
        }

        protected void OnSelectClicked(object sender, EventArgs e)
        {
            var button = (Button)sender;
            var hidden = button.FindControl("hidUserId").ToCustomHiddenField();
            hidEditSelected.Value = false.ToString();
            ProcessItemSelection(hidden.Value.ToLong());
        }

        protected void OnEditSelectClicked(object sender, EventArgs e)
        {
            var button = (Button)sender;
            var hidden = button.FindControl("hidUserId").ToCustomHiddenField();
            hidEditSelected.Value = true.ToString();
            ProcessItemSelection(hidden.Value.ToLong());
        }

        protected void OnSelectAllClicked(object sender, EventArgs e)
        {
            var users = (from item in lstSearchResults.Items
                         let hidden = item.FindControl("hidUserId").ToCustomHiddenField()
                         let checkBox = item.FindControl("chkSelected").ToCheckBox()
                         where checkBox != null && checkBox.Checked
                         select new User(hidden.Value.ToLong()))
                .Where(u => u.KeyLoaded)
                .ToList();

            if (MultiItemSelected != null)
                MultiItemSelected(this, new ViewEventArgs<List<User>>(users));
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = AdminSearchFields.Users.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

            Reset();
        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(AdminSearchFields.Users);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;

            Reset();
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();
        }
    }
}