﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PermissionSelectorControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Administration.Controls.PermissionSelectorControl" %>
<asp:Panel runat="server" ID="pnlPermissionSelectorContent" CssClass="popupControlOver popupControlOverW750">
    <div class="popheader">
        <h4>Permission Selector<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" Text="" /></small>
            <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/icons2/close.png" CssClass="close"
                CausesValidation="false" OnClick="OnCancelClicked" runat="server" />

        </h4>
    </div>
    <table class="poptable">
        <tr>
            <td colspan="2" class="p_m0">
                <eShip:TableFreezeHeaderExtender runat="server" ID="tfhePermissionSelectorPermissionsTable" TableId="permissionSelectorPermissionsTable" ScrollerContainerId="permissionSelectorScrollSection"
                    ScrollOffsetControlId="pnlPermissionSelectorContent" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2050" />
                <div class="finderScroll" id="permissionSelectorScrollSection">

                    <table class="stripe sm_chk" id="permissionSelectorPermissionsTable">
                        <tr>
                            <th style="width: 5%;">
                                <eShip:AltUniformCheckBox ID="chkSelectAll" runat="server" OnClientSideClicked="javascript:SetSelectedCheckBoxesStatus(this, 'permissionSelectorPermissionsTable');" />
                            </th>
                            <th style="width: 25%">
                                <asp:LinkButton runat="server" ID="lbtnSortGroup" CssClass="link_nounderline white"
                                    CausesValidation="False" OnCommand="OnSortPermissions">
                                            Group
                                </asp:LinkButton>
                            </th>
                            <th style="width: 35%">
                                <asp:LinkButton runat="server" ID="lbtnSortCode" CssClass="link_nounderline white"
                                    CausesValidation="False" OnCommand="OnSortPermissions">
                                            Code
                                </asp:LinkButton></th>
                            <th style="width: 35%">
                                <asp:LinkButton runat="server" ID="lbtnSortDescription" CssClass="link_nounderline white"
                                    CausesValidation="False" OnCommand="OnSortPermissions">
                                            Description
                                </asp:LinkButton></th>
                        </tr>
                        <asp:ListView runat="server" ID="lstPermissions" ItemPlaceholderID="itemPlaceHolder">
                            <LayoutTemplate>
                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <eShip:AltUniformCheckBox ID="chkSelected" runat="server" Checked='<%# Eval("Selected") %>' OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'permissionSelectorPermissionsTable', 'chkSelectAll');" />
                                    </td>
                                    <td>
                                        <asp:Literal runat="server" ID="litGroup" Text='<%# Eval("Group") %>' />
                                    </td>
                                    <td>
                                        <asp:Literal runat="server" ID="litCode" Text='<%# Eval("Code") %>' />
                                    </td>
                                    <td>
                                        <asp:Literal runat="server" ID="litDescription" Text='<%# Eval("Description") %>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <asp:Button ID="btnAdd" runat="server" Text="ADD" OnClick="OnAddClicked" CausesValidation="false" />
                <asp:Button ID="btnCancel" runat="server" Text="CANCEL" OnClick="OnCancelClicked"
                    CausesValidation="false" />
            </td>
        </tr>
    </table>
    <eShip:CustomHiddenField runat="server" ID="hidSortField" />
    <eShip:CustomHiddenField runat="server" ID="hidSortAscending" />
</asp:Panel>
<asp:Panel ID="pnlPermissionSelectorDimScreen" CssClass="dimBackgroundControlOver" runat="server" Visible="false" />
