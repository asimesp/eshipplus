﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Administration.Controls
{
    public partial class PermissionSelectorControl : MemberControlBase
    {
        private const string GroupSort = "GroupSort";
        private const string DescriptionSort = "DescriptionSort";
        private const string CodeSort = "CodeSort";

        public new bool Visible
        {
            get { return pnlPermissionSelectorContent.Visible; }
            set
            {
                base.Visible = true;
                pnlPermissionSelectorContent.Visible = value;
                pnlPermissionSelectorDimScreen.Visible = value;
            }
        }

        public List<string> CodesToExclude
        {
            set { LoadPermissions(value); }
        }

        public event EventHandler<ViewEventArgs<List<Permission>>> ItemsSelected;
        public event EventHandler SelectionCancel;

        private void LoadPermissions(List<string> codesToExclude)
        {
            var permissions = PermissionGenerator<Permission>.All(codesToExclude, ActiveUser.Tenant);

            var vPermissions = permissions
                .SelectMany(p => p.Value, (p, v) => new
                    {
                        v.Code,
                        v.Description,
                        Group = p.Key.FormattedString(),
                        Selected = false
                    })
                .Where(p => Session[WebApplicationConstants.IsSuperUserLogin].ToBoolean()
                                ? p.Group == PermissionGroup.SuperUser.FormattedString()
                                : p.Group != PermissionGroup.SuperUser.FormattedString())
                .ToList();

            litRecordCount.Text = vPermissions.BuildRecordCount();
            lstPermissions.DataSource = vPermissions.OrderBy(p => p.Group);
            lstPermissions.DataBind();

            chkSelectAll.Checked = false;
        }

        private void ProcessCancelledSelection()
        {
            if (SelectionCancel != null)
                SelectionCancel(this, new EventArgs());
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;

            hidSortAscending.Value = true.GetString();

            lbtnSortCode.CommandName = CodeSort;
            lbtnSortDescription.CommandName = DescriptionSort;
            lbtnSortGroup.CommandName = GroupSort;

            LoadPermissions(new List<string>());
        }

        protected void OnCancelClicked(object sender, EventArgs e)
        {
            ProcessCancelledSelection();
        }

        protected void OnAddClicked(object sender, EventArgs e)
        {
            var selected = (lstPermissions
                .Items
                .Where(item => item.FindControl("chkSelected").ToAltUniformCheckBox().Checked)
                .Select(item => PermissionGenerator<Permission>.Fetch(item.FindControl("litCode").ToLiteral().Text, ActiveUser.Tenant)))
                .ToList();

            if (ItemsSelected != null)
                ItemsSelected(this, new ViewEventArgs<List<Permission>>(selected));
        }

        protected void OnSortPermissions(object sender, CommandEventArgs e)
        {
            var vPermissions = lstPermissions
                .Items
                .Select(i => new
                    {
                        Code = i.FindControl("litCode").ToLiteral().Text,
                        Description = i.FindControl("litDescription").ToLiteral().Text,
                        Group = i.FindControl("litGroup").ToLiteral().Text,
                        Selected = i.FindControl("chkSelected").ToAltUniformCheckBox().Checked
                    });
            
            if (hidSortField.Value == e.CommandName && hidSortAscending.Value.ToBoolean())
			{
				hidSortAscending.Value = false.GetString();
			}
			else if (hidSortField.Value == e.CommandName && !hidSortAscending.Value.ToBoolean())
			{
				hidSortAscending.Value = true.GetString();
			}
            hidSortField.Value = e.CommandName;

            if (hidSortAscending.Value.ToBoolean())
            {
                vPermissions = vPermissions
               .OrderBy(p => e.CommandName == GroupSort
                   ? p.Group
                   : e.CommandName == DescriptionSort
                       ? p.Description
                       : p.Code);
            }
            else
            {
                vPermissions = vPermissions
               .OrderByDescending(p => e.CommandName == GroupSort
                   ? p.Group
                   : e.CommandName == DescriptionSort
                       ? p.Description
                       : p.Code);
            }

            lstPermissions.DataSource = vPermissions;
            lstPermissions.DataBind();
        }
    }
}