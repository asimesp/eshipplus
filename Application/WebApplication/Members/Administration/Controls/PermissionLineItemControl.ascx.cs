﻿using System;
using System.Web.UI;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor.Views;

namespace LogisticsPlus.Eship.WebApplication.Members.Administration.Controls
{
	public partial class PermissionLineItemControl : UserControl
	{
		public bool EnableDelete
		{
			set { ibtnDelete.Enabled = value; }
		}

		public event EventHandler<ViewEventArgs<string>> RemovePermission;

		public void LoadPermission<T>(T permission) where T : Permission
		{
			litCode.Text = permission.Code;
			litDescription.Text = permission.Description;
			chkGrant.Checked = permission.Grant;
			chkDeny.Checked = permission.Deny;
			chkModify.Checked = permission.Modify;
			chkDelete.Checked = permission.Remove;

			chkDelete.Visible = permission.DeleteApplicable;
			chkModify.Visible = permission.ModifyApplicable;
		}

		public T RetreivePermission<T>() where T : Permission, new()
		{
			return new T
			{
				Code = litCode.Text,
				Description = litDescription.Text,
				Grant = chkGrant.Checked,
				Deny = chkDeny.Checked,
				Modify = chkModify.Checked,
				Remove = chkDelete.Checked,

				DeleteApplicable = chkDelete.Visible,
				ModifyApplicable = chkModify.Visible
			};
		}

		protected void OnDeleteClicked(object sender, ImageClickEventArgs e)
		{
			if (RemovePermission != null)
				RemovePermission(this, new ViewEventArgs<string>(litCode.Text));
		}
	}
}