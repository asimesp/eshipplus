﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="AdminUserView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Administration.AdminUserView" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        ShowEdit="true" ShowDelete="true" ShowSave="true" ShowFind="true" ShowMore="false"
        OnFind="OnFindClicked" OnSave="OnSaveClicked" OnDelete="OnDeleteClicked" OnNew="OnNewClicked"
        OnEdit="OnEditClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Admin Users
                <eShip:RecordIdentityDisplayControl runat="server" ID="ridcRecordIdentity" TargetControlId="txtUsername" />
            </h3>

            <div class="lockMessage">
                <asp:Literal ID="litMessage" runat="server" />
            </div>
        </div>

        <hr class="dark mb5" />

        <div class="errorMsgLit">
            <asp:Literal runat="server" ID="litErrorMessages" />
        </div>

        <eShip:AdminUserFinderControl ID="adminUserFinder" runat="server" Visible="False" EnableMultiSelection="False" OpenForEditEnabled="true"
            OnItemSelected="OnAdminUserFinderItemSelected" OnSelectionCancel="OnAdminUserFinderSelectionCancelled" />
        <eShip:PermissionSelectorControl ID="permissionSelector" runat="server" Visible="false" OnItemsSelected="OnPermissionSelectorItemsSelected"
            OnSelectionCancel="OnPermissionSelectorSelectionCancelled" />

        <script type="text/javascript">
            function ClearHidFlag() {
                jsHelper.SetEmpty('<%= hidFlag.ClientID %>');
            }
        </script>

        <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
            Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" />
        <eShip:AjaxTabHeaderTextUpdater runat="server" ID="athtuTabUpdater" />
        <eShip:CustomHiddenField runat="server" ID="hidAdminUserId" />

        <ajax:TabContainer ID="tabAdminUser" runat="server" CssClass="ajaxCustom">
            <ajax:TabPanel runat="server" ID="tabDetails" HeaderText="Details">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlDetails">
                        <div class="rowgroup">
                            <div class="row">
                                <div class="fieldgroup">
                                    <label class="wlabel">Username</label>
                                    <eShip:CustomTextBox ID="txtUsername" runat="server" CssClass="w300" MaxLength="50" />
                                </div>
                                <div class="fieldgroup pt26 ml10">
                                    <asp:CheckBox ID="chkEnabled" runat="server" CssClass="jQueryUniform" />
                                    <label>Enabled</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="fieldgroup">
                                    <label class="wlabel">Password</label>
                                    <eShip:CustomTextBox ID="txtPassword" runat="server" CssClass="w300" MaxLength="50" TextMode="Password" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="fieldgroup">
                                    <label class="wlabel">First Name</label>
                                    <eShip:CustomTextBox ID="txtFirstName" runat="server" CssClass="w300" MaxLength="50" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="fieldgroup">
                                    <label class="wlabel">Last Name</label>
                                    <eShip:CustomTextBox ID="txtLastName" runat="server" CssClass="w300" MaxLength="50" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="fieldgroup">
                                    <label class="wlabel">Email</label>
                                    <eShip:CustomTextBox ID="txtEmail" runat="server" CssClass="w300" MaxLength="100" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="fieldgroup">
                                    <label class="wlabel">
                                        Notes<eShip:TextAreaMaxLengthExtender runat="server" ID="tamleNotes" TargetControlId="txtNotes" MaxLength="1000" />
                                    </label>
                                    <eShip:CustomTextBox ID="txtNotes" runat="server" CssClass="w500 h150" TextMode="MultiLine" />
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabPermissions" HeaderText="Permissions">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlPermissions" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel ID="pnlPermissions" runat="server">
                                <div class="row mb10">
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddPermission" Text="Add Permission" CssClass="mr10" OnClick="OnAddPermissionClicked" />
                                        <asp:Button runat="server" ID="btnClearPermissions" Text="Clear Permissions" OnClick="OnClearPermissionsClicked" />
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheuserPermissionTable" TableId="userPermissionTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table id="userPermissionTable" class="stripe">
                                    <tr>
                                        <th style="width: 22%;">Code</th>
                                        <th style="width: 30%;">Description</th>
                                        <th style="width: 10%;" class="text-center">Grant</th>
                                        <th style="width: 10%;" class="text-center">Deny</th>
                                        <th style="width: 10%;" class="text-center">Action</th>
                                        <th style="width: 10%;" class="text-center">Delete</th>
                                        <th style="width: 8%;" class="text-center">Remove</th>
                                    </tr>
                                    <asp:ListView ID="lstPermissions" OnItemDataBound="BindPermissionLineItem" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <eShip:PermissionLineItemControl ID="pcPermissionLineItem" runat="server" OnRemovePermission="OnPermissionItemRemovePermission" EnableDelete='<%# Access.Modify %>' />
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnAddPermission" />
                        </Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabAuditLog" HeaderText="Audit Log">
                <ContentTemplate>
                    <eShip:AuditLogControl ID="auditLogs" runat="server" />
                </ContentTemplate>
            </ajax:TabPanel>
        </ajax:TabContainer>

    </div>
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
</asp:Content>
