﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="UserImpersonationView.aspx.cs" EnableEventValidation="false"
    Inherits="LogisticsPlus.Eship.WebApplication.Members.Administration.UserImpersonationView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Administration" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
                User Impersonation<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>

        <hr class="dark mb10" />

        <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked" CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="fieldgroup right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="Users" ShowAutoRefresh="False"
                            OnProcessingError="OnSearchProfilesProcessingError" OnSearchProfileSelected="OnSearchProfilesProfileSelected" />
                    </div>
                </div>
            </div>

            <hr class="dark mb10" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>' OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>

            <hr class="fat" />

            <div class="rowgroup">
                <div class="row mb10">
                    <div class="left">
                        <div class="fieldgroup">
                            <asp:Button ID="btnSearch" CssClass="mr10" runat="server" Text="search" OnClick="OnSearchClicked" CausesValidation="False" />
                        </div>
                    </div>
                    <div class="right">
                        <div class="fieldgroup">
                            <label>Sort By:</label>
                            <asp:DropDownList runat="server" ID="ddlSortBy" DataTextField="DisplayName" DataValueField="Name"
                                OnSelectedIndexChanged="OnSortBySelectedIndexChanged" AutoPostBack="True" />
                        </div>
                        <div class="fieldgroup">
                            <asp:RadioButton runat="server" ID="rbAsc" Text="ASC" GroupName="rbSortBy" AutoPostBack="True"
                                ToolTip="Ascending" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                        </div>
                        <div class="fieldgroup">
                            <asp:RadioButton runat="server" ID="rbDesc" Text="DESC" GroupName="rbSortBy" Checked="True"
                                ToolTip="Descending" AutoPostBack="True" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                        </div>
                    </div>
                </div>
            </div>

        </asp:Panel>

        <asp:UpdatePanel runat="server" ID="upDataUpdate">
            <ContentTemplate>
                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheUserListingListingTable" TableId="userListingTable" HeaderZIndex="2" />

                <div class="rowgroup">
                    <table id="userListingTable" class="line2 pl2">
                        <tr>
                            <th style="width: 13%">
                            </th>
                            <th style="width: 29%">
                                <asp:LinkButton runat='server' ID="lbtnSortUsername" CssClass="link_nounderline blue" CausesValidation="False" Text="Username" OnCommand="OnSortData"/>
                            </th>
                            <th style="width: 29%">
                                <asp:LinkButton runat='server' ID="lbtnSortFirstName" CssClass="link_nounderline blue" CausesValidation="False" Text="First Name" OnCommand="OnSortData"/>
                            </th>
                            <th style="width: 29%">
                                <asp:LinkButton runat='server' ID="lbtnSortLastName" CssClass="link_nounderline blue" CausesValidation="False" Text="Last Name" OnCommand="OnSortData"/>
                            </th>
                        </tr>
                        <asp:Repeater runat="server" ID="rptUsers">
                            <ItemTemplate>
                                <tr>
                                    <td class="top">
                                        <eShip:CustomHiddenField ID="hidUserId" runat="server" Value='<%# Eval("Id") %>'/>
                                        <asp:Button ID="Button1" runat="server" CausesValidation="False" Text="Impersonate"
                                            OnClick="OnImpersonateClicked" />
                                    </td>
                                    <td>
                                        <%# Eval("Username") %>
                                        <%# ActiveUser.HasAccessTo(ViewCode.User)
                                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To User Record'><img src='{3}' alt='' width='16' align='absmiddle'/></a>", 
                                                    ResolveUrl(UserView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    Eval("Id").GetString().UrlTextEncrypt(),
                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                : string.Empty  %>
                                    </td>
                                    <td>
                                        <%# Eval("FirstName") %>
                                    </td>
                                    <td>
                                        <%# Eval("LastName") %>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lbtnSortUsername" />
                <asp:PostBackTrigger ControlID="lbtnSortFirstName" />
                <asp:PostBackTrigger ControlID="lbtnSortLastName" />
            </Triggers>
        </asp:UpdatePanel>
    </div>

    <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
        Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" />
    <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
</asp:Content>
