﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="UserProfileView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Administration.UserProfileView" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowSave="true"
        ShowEdit="True" OnSave="OnSaveClicked" OnEdit="OnEditClicked" OnUnlock="OnUnlockClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" ImageUrl="" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>User Profile</h3>

            <div class="lockMessage">
                <asp:Literal ID="litMessage" runat="server" />
            </div>
        </div>

        <hr class="dark mb5" />

        <div class="errorMsgLit">
            <asp:Literal runat="server" ID="litErrorMessages" />
        </div>

        <script type="text/javascript">
            function ClearHidFlag() {
                jsHelper.SetEmpty('<%= hidFlag.ClientID %>');
            }
        </script>

        <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
            Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" />

        <asp:Panel runat="server" ID="pnlDetails">
            <div class="rowgroup">
                <div class="col_1_2 bbox vlinedarkright">
                    <h5>Account Details</h5>
                    <div class="row">
                        <div class="fieldgroup mr20">
                            <label class="wlabel">Username</label>
                            <eShip:CustomTextBox ID="txtUsername" runat="server" MaxLength="50" CssClass="w425" />
                        </div>

                    </div>
                    <div class="row">
                        <div class="fieldgroup mr20">
                            <label class="wlabel">Password</label>
                            <eShip:CustomTextBox ID="txtPassword" runat="server" MaxLength="500" TextMode="Password" CssClass="w200" />
                        </div>
                        <div class="fieldgroup">
                            <label class="wlabel">Confirm Password</label>
                            <eShip:CustomTextBox ID="txtPasswordConfirmation" runat="server" MaxLength="500" TextMode="Password" CssClass="w200" />
                        </div>
                    </div>

                    <h5 class="pt20">Contact Details</h5>
                    <div class="row">
                        <div class="fieldgroup mr20">
                            <label class="wlabel">First Name</label>
                            <eShip:CustomTextBox ID="txtFirstName" runat="server" MaxLength="50" CssClass="w200" />
                        </div>
                        <div class="fieldgroup">
                            <label class="wlabel">Last Name</label>
                            <eShip:CustomTextBox ID="txtLastName" runat="server" MaxLength="50" CssClass="w200" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="fieldgroup mr20">
                            <label class="wlabel">Phone</label>
                            <eShip:CustomTextBox ID="txtPhone" runat="server" MaxLength="50" CssClass="w200" />
                        </div>
                        <div class="fieldgroup">
                            <label class="wlabel">Mobile</label>
                            <eShip:CustomTextBox ID="txtMobile" runat="server" MaxLength="50" CssClass="w200" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="fieldgroup mr20">
                            <label class="wlabel">Fax</label>
                            <eShip:CustomTextBox ID="txtFax" runat="server" MaxLength="50" CssClass="w200" />
                        </div>
                        <div class="fieldgroup">
                            <label class="wlabel">Email</label>
                            <eShip:CustomTextBox ID="txtEmail" runat="server" MaxLength="100" CssClass="w200" />
                        </div>
                    </div>

                    <h5 class="pt20">Account Options</h5>
                    <div class="row">
                        <div class="fieldgroup">
                            <asp:CheckBox ID="chkAlwaysShowRatingNotice" runat="server" CssClass="jQueryUniform" />
                            <label class="w180">Always Show Rating Notice</label>
                        </div>
                        <div class="fieldgroup">
                            <asp:CheckBox ID="chkAlwaysShowFinderParametersOnSearch" runat="server" CssClass="jQueryUniform" />
                            <label class="w180">Always Show Finder Parameters On Search</label>
                        </div>
                    </div>
                </div>

                <div class="col_1_2 bbox pl46">
                    <h5>User Location</h5>
                    <eShip:SimpleAddressInputControl ID="saicUserAddress" runat="server" CssClass="w300" />
                </div>
            </div>
        </asp:Panel>

        <eShip:CustomHiddenField runat="server" ID="hidFlag" />
    </div>
</asp:Content>
