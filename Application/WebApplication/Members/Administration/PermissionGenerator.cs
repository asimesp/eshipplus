﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Administration
{
	public static class PermissionGenerator<T> where T : Permission, new()
	{
		private static Dictionary<PermissionGroup, List<T>> All(Tenant tenant = null)
		{
			return new Dictionary<PermissionGroup, List<T>>
			                                     	{
			                                     		{PermissionGroup.Accounting, Accounting(tenant)},
			                                     		{PermissionGroup.Administration, Administration(tenant)},
			                                     		{PermissionGroup.BusinessIntelligence, BusinessIntelligence(tenant)},
			                                     		{PermissionGroup.Connect, Connect(tenant)},
			                                     		{PermissionGroup.Core, Core(tenant)},
			                                     		{PermissionGroup.Operations, Operations(tenant)},
			                                     		{PermissionGroup.Rating, Rating(tenant)},
			                                     		{PermissionGroup.Registry, Registry(tenant)},
			                                     		{PermissionGroup.Sales, Sales(tenant)},
			                                     		{PermissionGroup.SuperUser, SuperUser()},
			                                     	};
		}

		public static Dictionary<PermissionGroup, List<T>> All(List<string> codesToExclude, Tenant tenant = null)
		{
			var all = All(tenant);
			var subset = new Dictionary<PermissionGroup, List<T>>();
			foreach (var key in all.Keys)
			{
				if (!subset.ContainsKey(key)) subset.Add(key, new List<T>());
				subset[key].AddRange(all[key].Where(i => !codesToExclude.Contains(i.Code)));
			}
			return subset;
		}

		public static List<T> Remove(List<T> permissions, List<string> codesToExclude)
		{
			return codesToExclude.Any() ? permissions.Where(p => !codesToExclude.Contains(p.Code)).ToList() : permissions;
		}

		public static List<T> Accounting(Tenant tenant = null)
		{
			var permissions =
				new List<T>
					{
                        new T {Code = ViewCode.AverageWeeklyFuel.ToString(), ModifyApplicable = true, DeleteApplicable = true},
                        new T {Code = ViewCode.BatchPaymentApplication.ToString(), ModifyApplicable = false, DeleteApplicable = false},
                        new T {Code = ViewCode.CanApprovePendingVendor.ToString(), ModifyApplicable = false, DeleteApplicable = false},
                        new T {Code = ViewCode.CanCollectPaymentFromCustomer.ToString(), ModifyApplicable = false, DeleteApplicable = false},
                        new T {Code = ViewCode.CanRefundPaymentFromCustomer.ToString(), ModifyApplicable = false, DeleteApplicable = false},
                        new T {Code = ViewCode.CanSeePaymentProfilesForCustomer.ToString(), ModifyApplicable = false, DeleteApplicable = false},
                        new T {Code = ViewCode.CommissionsToBePaid.ToString(), ModifyApplicable = true, DeleteApplicable = false},
						new T {Code = ViewCode.Customer.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.CustomerControlAccount.ToString(), ModifyApplicable = true, DeleteApplicable = true},
                        new T {Code = ViewCode.CustomerInvoiceDashboard.ToString(), ModifyApplicable = false, DeleteApplicable = false},
                        new T {Code = ViewCode.CustomerPaymentProfileManagement.ToString(), ModifyApplicable = false, DeleteApplicable = false},
                        new T {Code = ViewCode.CustomerPurchaseOrder.ToString(), ModifyApplicable = true, DeleteApplicable = true},
                        new T {Code = ViewCode.DocumentPackDownload.ToString(), ModifyApplicable = false, DeleteApplicable = false},
                        new T {Code = ViewCode.Invoice.ToString(), ModifyApplicable = true, DeleteApplicable = true},
                        new T {Code = ViewCode.InvoiceControlAccountUpdate.ToString(), ModifyApplicable = false, DeleteApplicable = false},
                        new T {Code = ViewCode.InvoiceDashboard.ToString(), ModifyApplicable = false, DeleteApplicable = false},
                        new T {Code = ViewCode.InvoicePay.ToString(), ModifyApplicable = false, DeleteApplicable = false},
                        new T {Code = ViewCode.InvoicePaidAmountUpdate.ToString(), ModifyApplicable = false, DeleteApplicable = false},
                        new T {Code = ViewCode.InvoicePosting.ToString(), ModifyApplicable = true, DeleteApplicable = false},
                        new T {Code = ViewCode.MiscReceiptDashboard.ToString(), ModifyApplicable = true, DeleteApplicable = true},
                        new T {Code = ViewCode.MiscReceiptApplication.ToString(), ModifyApplicable = true, DeleteApplicable = true},
                        new T {Code = ViewCode.PendingVendor.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.SalesRepresentative.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.ShipmentAudit.ToString(), ModifyApplicable = false, DeleteApplicable = false},
						new T {Code = ViewCode.ShipmentsToBeInvoiced.ToString(), ModifyApplicable = true, DeleteApplicable = false},
						new T {Code = ViewCode.Tier.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.Vendor.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.VendorBill.ToString(), ModifyApplicable = true, DeleteApplicable = true},
                        new T {Code = ViewCode.VendorBillDashboard.ToString(), ModifyApplicable = false, DeleteApplicable = false},
                        new T {Code = ViewCode.VendorBillPosting.ToString(), ModifyApplicable = true, DeleteApplicable = false},
						new T {Code = ViewCode.VendorChargesConfirmation.ToString(), ModifyApplicable = false, DeleteApplicable = false},
						new T {Code = ViewCode.VendorDispute.ToString(), ModifyApplicable = true, DeleteApplicable = true}
					};

			if (tenant != null) UpdateWithUserDefinedPermissionsAndDescriptions(permissions, tenant.Id, PermissionCategory.Accounting);

			foreach (var permission in permissions.Where(permission => string.IsNullOrEmpty(permission.Description)))
				permission.Description = permission.Code.FormattedString();



			return permissions;
		}

		public static List<T> Administration(Tenant tenant = null)
		{
			var permissions =
				new List<T>
					{
						new T {Code = ViewCode.ExistingRecordLocks.ToString(), ModifyApplicable = false, DeleteApplicable = true},
						new T {Code = ViewCode.Group.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.ManageUploadedImage.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.TenantAdmin.ToString(), ModifyApplicable = true, DeleteApplicable = false},
						new T {Code = ViewCode.User.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.UserImpersonation.ToString(), ModifyApplicable = false, DeleteApplicable = false},
					};

			if (tenant != null) UpdateWithUserDefinedPermissionsAndDescriptions(permissions, tenant.Id, PermissionCategory.Administration);

			foreach (var permission in permissions.Where(permission => string.IsNullOrEmpty(permission.Description)))
				permission.Description = permission.Code.FormattedString();

			return permissions;
		}

		public static List<T> BusinessIntelligence(Tenant tenant = null)
		{
			var permissions =
				new List<T>
        			{
        				new T {Code = ViewCode.BypassReportVisibilityFilter.ToString(), ModifyApplicable = false, DeleteApplicable = false},
        				new T {Code = ViewCode.CustomerGroup.ToString(), ModifyApplicable = true, DeleteApplicable = true},
        				new T {Code = ViewCode.ReportConfiguration.ToString(), ModifyApplicable = true, DeleteApplicable = true},
        				new T {Code = ViewCode.ReportConfigurationEditOverride.ToString(), ModifyApplicable = false, DeleteApplicable = false},
        				new T {Code = ViewCode.ReportRun.ToString(), ModifyApplicable = false, DeleteApplicable = false},
                        new T {Code = ViewCode.ReportScheduleDashboard.ToString(), ModifyApplicable = false, DeleteApplicable =false},
        				new T {Code = ViewCode.ReportScheduler.ToString(), ModifyApplicable = true, DeleteApplicable = true},
        				new T {Code = ViewCode.VendorGroup.ToString(), ModifyApplicable = true, DeleteApplicable = true},
        				new T {Code = ViewCode.VendorLaneRateHistory.ToString(), ModifyApplicable = false, DeleteApplicable = false},
        				new T {Code = ViewCode.VendorPerformanceSummary.ToString(), ModifyApplicable = false, DeleteApplicable = false},
        				new T {Code = ViewCode.VendorTerminalLocator.ToString(), ModifyApplicable = false, DeleteApplicable = false},
        			};

			if (tenant != null) UpdateWithUserDefinedPermissionsAndDescriptions(permissions, tenant.Id, PermissionCategory.BusinessIntelligence);

			foreach (var permission in permissions.Where(permission => string.IsNullOrEmpty(permission.Description)))
				permission.Description = permission.Code.FormattedString();

			return permissions;
		}

		public static List<T> Connect(Tenant tenant = null)
		{
			var permissions =
				new List<T>
        			{
        				new T {Code = ViewCode.CustomerCommunication.ToString(), ModifyApplicable = true, DeleteApplicable = true},
        				new T {Code = ViewCode.DocDeliveryLog.ToString(), ModifyApplicable = false, DeleteApplicable = false},
        				new T {Code = ViewCode.FaxTransmission.ToString(), ModifyApplicable = true, DeleteApplicable = false},
                        new T {Code = ViewCode.MacroPointDashboard.ToString(), ModifyApplicable = false, DeleteApplicable = false},
        				new T {Code = ViewCode.VendorCommunication.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.XmlConnectRecord.ToString(), ModifyApplicable = true, DeleteApplicable = false},
						new T {Code = ViewCode.XmlConnectRecordXmlDownload.ToString(), ModifyApplicable = false, DeleteApplicable = false},
						new T {Code = ViewCode.XmlTransmission.ToString(), ModifyApplicable = false, DeleteApplicable = false},
        			};

			if (tenant != null) UpdateWithUserDefinedPermissionsAndDescriptions(permissions, tenant.Id, PermissionCategory.Connect);

			foreach (var permission in permissions.Where(permission => string.IsNullOrEmpty(permission.Description)))
				permission.Description = permission.Code.FormattedString();

			return permissions;
		}

		public static List<T> Core(Tenant tenant = null)
		{
			var permissions =
				new List<T>
					{
						new T {Code = ViewCode.Announcement.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.AuditLog.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.BypassCustomerCreditStop.ToString(), ModifyApplicable = false, DeleteApplicable = false},
						new T {Code = ViewCode.CanSeeArchivedNotes.ToString(), ModifyApplicable = false, DeleteApplicable = false},
						new T {Code = ViewCode.CanSeeClassifiedNotes.ToString(), ModifyApplicable = false, DeleteApplicable = false},
						new T {Code = ViewCode.EmailLog.ToString(), ModifyApplicable = true, DeleteApplicable = false},
						new T {Code = ViewCode.DeveloperAccessRequest.ToString(), ModifyApplicable = false, DeleteApplicable = false},
						new T {Code = ViewCode.DeveloperAccessRequestListing.ToString(), ModifyApplicable = true, DeleteApplicable = true},
					    new T {Code = ViewCode.ShipmentMassDocumentUpload.ToString(), ModifyApplicable = false, DeleteApplicable = false},
						new T {Code = ViewCode.VendorProMassUpdate.ToString(), ModifyApplicable = false, DeleteApplicable = false},
					};

			if (tenant != null) UpdateWithUserDefinedPermissionsAndDescriptions(permissions, tenant.Id, PermissionCategory.Utilities);

			foreach (var permission in permissions.Where(permission => string.IsNullOrEmpty(permission.Description)))
				permission.Description = permission.Code.FormattedString();

			return permissions;
		}

		public static List<T> Operations(Tenant tenant = null)
		{
			var permissions =
				new List<T>
					{
						new T {Code = ViewCode.AddressBook.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.Asset.ToString(), ModifyApplicable = true, DeleteApplicable = true},
                        new T {Code = ViewCode.AssetLog.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.AvailableLoads.ToString(), ModifyApplicable = false, DeleteApplicable = false},
                        new T {Code = ViewCode.CanDisableApiBookingNotifications.ToString(), ModifyApplicable = false, DeleteApplicable = false},
                        new T {Code = ViewCode.CanOverrideApiRatingDates.ToString(), ModifyApplicable = false, DeleteApplicable = false},
                        new T {Code = ViewCode.CanSeeSalesRepFinancialData.ToString(), ModifyApplicable = false, DeleteApplicable = false},
						new T {Code = ViewCode.CarrierTerminal.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.CarrierPreferredLane.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.Claim.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.ClaimDashboard.ToString(), ModifyApplicable = false, DeleteApplicable = false},
                        new T {Code = ViewCode.CustomerLoadDashboard.ToString(), ModifyApplicable = false, DeleteApplicable = false},
                        new T {Code = ViewCode.DatLoadboard.ToString(), ModifyApplicable = true, DeleteApplicable = true},
                        new T {Code = ViewCode.DispatchDashboard.ToString(), ModifyApplicable = true, DeleteApplicable = false},
						new T {Code = ViewCode.EstimateRate.ToString(), ModifyApplicable = false, DeleteApplicable = false},
                        new T {Code = ViewCode.Job.ToString(), ModifyApplicable = true, DeleteApplicable = true},
                        new T {Code = ViewCode.JobDashboard.ToString(), ModifyApplicable = true, DeleteApplicable = true},
                        new T {Code = ViewCode.LibraryItem.ToString(), ModifyApplicable = true, DeleteApplicable = true},
                        new T {Code = ViewCode.LoadOrder.ToString(), ModifyApplicable = true, DeleteApplicable = true},
                        new T {Code = ViewCode.MacroPointShipmentTracking.ToString(), ModifyApplicable = false, DeleteApplicable = false},
                        new T {Code = ViewCode.PendingPickupsDashboardLTL.ToString(), ModifyApplicable = false, DeleteApplicable = false},
						new T {Code = ViewCode.RateAndSchedule.ToString(), ModifyApplicable = false, DeleteApplicable = false},
                        new T {Code = ViewCode.ServiceTicket.ToString(), ModifyApplicable = true, DeleteApplicable = true},
                        new T {Code = ViewCode.ServiceTicketDashboard.ToString(), ModifyApplicable = false, DeleteApplicable = false},
                        new T {Code = ViewCode.Shipment.ToString(), ModifyApplicable = true, DeleteApplicable = true},
                        new T {Code = ViewCode.ShipmentDashboard.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.ShoppedRate.ToString(), ModifyApplicable = true, DeleteApplicable = true},
                        new T {Code = ViewCode.TruckloadQuote.ToString(), ModifyApplicable = false, DeleteApplicable = false},
                        new T {Code = ViewCode.CanSubmitRatedQuote.ToString(), ModifyApplicable = false, DeleteApplicable = false},
                    };

			if (tenant != null) UpdateWithUserDefinedPermissionsAndDescriptions(permissions, tenant.Id, PermissionCategory.Operations);

			foreach (var permission in permissions.Where(permission => string.IsNullOrEmpty(permission.Description)))
				permission.Description = permission.Code.FormattedString();

			return permissions;
		}

		public static List<T> Rating(Tenant tenant = null)
		{
			var permissions =
				new List<T>
					{
                        new T {Code = ViewCode.CustomerRating.ToString(), ModifyApplicable = true, DeleteApplicable = true},
                        new T {Code = ViewCode.TruckloadTenderingProfile.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.FuelTable.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.RateAnalysisLTL.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.RateAnalysisSmallPack.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.Region.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.ResellerAddition.ToString(), ModifyApplicable = true, DeleteApplicable = true},
                        new T {Code = ViewCode.SmallPackagingMaps.ToString(), ModifyApplicable = true, DeleteApplicable = true},
                        new T {Code = ViewCode.SmallPackageServiceMaps.ToString(), ModifyApplicable = true, DeleteApplicable = true},
                        new T {Code = ViewCode.SmcAvailableCarriers.ToString(), ModifyApplicable = false, DeleteApplicable = false},
                        new T {Code = ViewCode.SmcPointToPointServiceCheck.ToString(), ModifyApplicable = false, DeleteApplicable = false},
                        new T {Code = ViewCode.TariffRateAnalysis.ToString(), ModifyApplicable = false, DeleteApplicable = false},
                        new T {Code = ViewCode.VendorRating.ToString(), ModifyApplicable = true, DeleteApplicable = true},
                        new T {Code = ViewCode.VendorRatingFuelMassUpdateLTL.ToString(), ModifyApplicable = true, DeleteApplicable = false},
					};
			if (tenant != null) UpdateWithUserDefinedPermissionsAndDescriptions(permissions, tenant.Id, PermissionCategory.Rating);

			foreach (var rating in permissions.Where(rating => string.IsNullOrEmpty(rating.Description)))
				rating.Description = rating.Code.FormattedString();

			return permissions;
		}

		public static List<T> Registry(Tenant tenant = null)
		{
			var permissions =
				new List<T>
					{
						new T {Code = ViewCode.AccountBucket.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.AccountBucketUnit.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.ChargeCode.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.ContactType.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.DocumentPrefixMap.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.DocumentTag.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.DocumentTemplate.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.EquipmentType.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.FailureCode.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.InsuranceType.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.MileageSource.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.NoServiceDay.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.PackageType.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.Prefix.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.QuickPayOption.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.Service.ToString(), ModifyApplicable = true, DeleteApplicable = true},
                        new T {Code = ViewCode.Project44ChargeCodeMapping.ToString(), ModifyApplicable = true, DeleteApplicable = true},
                        new T {Code = ViewCode.Project44ServiceMapping.ToString(), ModifyApplicable = true, DeleteApplicable = true},
                        new T {Code = ViewCode.Setting.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.ShipmentPriority.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.UserDepartment.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.UserDefinedPermissionDetail.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.YearMonthlyBusinessDay.ToString(), ModifyApplicable = true, DeleteApplicable = true},
					};

			if (tenant != null) UpdateWithUserDefinedPermissionsAndDescriptions(permissions, tenant.Id, PermissionCategory.Registry);

			foreach (var permission in permissions.Where(permission => string.IsNullOrEmpty(permission.Description)))
				permission.Description = permission.Code.FormattedString();

			return permissions;
		}

		public static List<T> Sales(Tenant tenant = null)
		{
			return new List<T>();
		}

		public static List<T> SuperUser()
		{
			var permissions =
				new List<T>
					{
                        new T{ Code = ViewCode.AdminAuditLog.ToString(), ModifyApplicable = false, DeleteApplicable = false},
                        new T {Code = ViewCode.AdminUser.ToString(), ModifyApplicable = true, DeleteApplicable = true},
                        new T {Code = ViewCode.ApplicationLog.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.Country.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.ErrorLog.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.PostalCode.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.QlikUser.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.ReportTemplate.ToString(), ModifyApplicable = true, DeleteApplicable = true},
                        new T{ Code = ViewCode.ResetTenantUser.ToString(), ModifyApplicable = false, DeleteApplicable = false},
						new T {Code = ViewCode.SystemAlerts.ToString(), ModifyApplicable = true, DeleteApplicable = true},
						new T {Code = ViewCode.TenantSetup.ToString(), ModifyApplicable = true, DeleteApplicable = true},
					};

			foreach (var permission in permissions.Where(permission => string.IsNullOrEmpty(permission.Description)))
				permission.Description = permission.Code.FormattedString();

			return permissions;
		}

		public static T Fetch(string code, Tenant tenant = null)
		{
			return (from key in All(tenant).Keys
					from permission in All(tenant)[key]
					where permission.Code == code
					select new T
							   {
								   Code = permission.Code,
								   DeleteApplicable = permission.DeleteApplicable,
								   Deny = permission.Deny,
								   Description = permission.Description,
								   Grant = permission.Grant,
								   Modify = permission.Modify,
								   ModifyApplicable = permission.ModifyApplicable,
								   Remove = permission.Remove,
							   }).FirstOrDefault();
		}


		private static void UpdateWithUserDefinedPermissionsAndDescriptions(List<T> permissions, long tenantId, PermissionCategory permissionCategory)
		{
			var userDefindedPermissions = ProcessorVars
				.RegistryCache[tenantId]
				.PermissionDetails
				.Where(p => p.Category == permissionCategory)
				.ToList();

			// update custom permissions
			var customPermissions = userDefindedPermissions
				.Where(p => p.Type == PermissionType.Custom)
				.Select(p => new T { Code = p.Code, Description = p.DisplayCode, ModifyApplicable = false, DeleteApplicable = false })
				.ToList();
			permissions.AddRange(customPermissions);

			// update existing permissions
			foreach (var p in userDefindedPermissions.Where(p => p.Type == PermissionType.System))
			{
				var perm = permissions.FirstOrDefault(x => x.Code == p.Code);
				if (perm != null) perm.Description = p.DisplayCode;
			}

		}
	}
}