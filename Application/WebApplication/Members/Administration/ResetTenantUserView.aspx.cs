﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Processor.Handlers.Administration;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Administration;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Administration
{
    public partial class ResetTenantUserView : AdminMemberPageBase, IResetTenantUserView
    {
        public static string PageAddress { get { return "~/Members/Administration/ResetTenantUserView.aspx"; } }

        public override ViewCode PageCode
        {
            get { return ViewCode.ResetTenantUser; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.AdminBlue; }
        }

        public event EventHandler<ViewEventArgs<TenantUserResetCriteria>> Reset;

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new ResetTenantUserHandler(this);
            handler.Initialize();
        }

        protected void OnResetTenantUser(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtUsername.Text) || string.IsNullOrEmpty(txtTenantCode.Text))
            {
                DisplayMessages(new[] { ValidationMessage.Error("Username and Tenant Code are required") });
                return;
            }
            var criteria = new TenantUserResetCriteria
            {
                UserName = txtUsername.Text,
                TenantCode = txtTenantCode.Text,
                ResetPassword = chkResetPassword.Checked,
                EnableUser = chkEnableUser.Checked
            };
            if (Reset != null)
                Reset(this, new ViewEventArgs<TenantUserResetCriteria>(criteria));
        }
    }
}
