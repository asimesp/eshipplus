﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Administration;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.Processor.Views.Administration;
using LogisticsPlus.Eship.WebApplication.Members.Administration.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Administration
{
	public partial class UserView : MemberPageBase, IUserView
	{
	    private const string PermissionsHeader = "Permissions";
        private const string ShipAsCustomerHeader = "Ship As Customer";
        private const string PermissionGroupsHeader = "Permission Groups";

		private const string SaveFlag = "S";
		private const string AddGroupsArgs = "AG";
		private const string RemoveGroupsArgs = "RG";
	    private const string ImportUsersArgs = "IU";

	    private const string AddRecord = "Add";
	    private const string RemoveRecord = "Remove";

		public static string PageAddress { get { return "~/Members/Administration/UserView.aspx"; } }

		public override ViewCode PageCode { get { return ViewCode.User; } }

		public override string SetPageIconImage
		{
			set { imgPageImageLogo.ImageUrl = IconLinks.AdminBlue; }
		}

		public List<UserDepartment> Departments
		{
			set
			{
				var depts = value
					.Select(d => new ViewListItem(d.FormattedString(), d.Id.GetString()))
					.OrderBy(v => v.Text)
					.ToList();
				depts.Insert(0, new ViewListItem(WebApplicationConstants.NotApplicable, default(long).ToString()));
				ddlDepartment.DataSource = depts;
				ddlDepartment.DataBind();
			}
		}

		public event EventHandler Loading;
		public event EventHandler<ViewEventArgs<string>> CustomerSearch;
		public event EventHandler<ViewEventArgs<string>> VendorSearch;
		public event EventHandler<ViewEventArgs<User>> Save;
		public event EventHandler<ViewEventArgs<User>> Delete;
		public event EventHandler<ViewEventArgs<User>> Lock;
		public event EventHandler<ViewEventArgs<User>> UnLock;
		public event EventHandler<ViewEventArgs<User>> LoadAuditLog;
		public event EventHandler<ViewEventArgs<List<Group>>> UpdateGroupUsers;
        public event EventHandler<ViewEventArgs<List<GroupUser>>> RemoveGroupUsers;
        public event EventHandler<ViewEventArgs<List<User>>> BatchImport;


        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

		public void DisplayMessages(IEnumerable<ValidationMessage> messages)
		{
			if (messages == null) return;
			if (!messages.Any()) return;

			if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == SaveFlag)
			{
				// unlock and return to read-only
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<User>(new User(hidUserId.Value.ToLong(), false)));
				SetEditStatus(false);
				memberToolBar.ShowUnlock = false;
			}

			if (hidFlag.Value == AddGroupsArgs || hidFlag.Value == RemoveGroupsArgs)
			{
				var usersGroups = new User(hidUserId.Value.ToLong(), false).RetrieveUsersGroups();
				lstGroups.DataSource = usersGroups;
				lstGroups.DataBind();
				tabPermissionGroups.HeaderText = usersGroups.BuildTabCount(PermissionGroupsHeader);
			}

			messageBox.Icon = messages.GenerateIcon();
			messageBox.Message = string.Join("<br />", messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
			messageBox.Visible = true;

			litErrorMessages.Text = messages.HasErrors()
										? string.Join("<br />",
													  messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
														Select(m => m.Message).ToArray())
										: string.Empty;
		}

		public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs)
		{
			auditLogs.Load(logs);
		}

		public void DisplayCustomer(Customer customer)
		{
			txtCustomerNumber.Text = customer.CustomerNumber;
			txtCustomerName.Text = customer.Name;
			hidCustomerId.Value = customer.Id.ToString();

			customerFinder.Visible = false;
		}

        public void DisplayVendor(Vendor vendor)
        {
            txtVendorNumber.Text = vendor.VendorNumber;
            txtVendorName.Text = vendor.Name;
            hidVendorId.Value = vendor.Id.ToString();
        }

		public void FailedLock(Lock @lock)
		{
			memberToolBar.ShowNew = false;
			memberToolBar.ShowSave = false;
			memberToolBar.ShowDelete = false;
			memberToolBar.ShowUnlock = false;

			btnAddPermission.Enabled = Access.Modify;

			foreach (var item in lstPermissions.Items)
				((PermissionLineItemControl)item.FindControl("pcPermissionLineItem")).EnableDelete = false;

			litMessage.Text = @lock.BuildFailedLockMessage();
		}

		public void Set(User user)
		{
			LoadUser(user);
			SetEditStatus(!user.IsNew);
		}


		private List<ToolbarMoreAction> ToolbarMoreActions()
		{
			var addGroups = new ToolbarMoreAction
			{
				CommandArgs = AddGroupsArgs,
				ConfirmCommand = true,
				ImageUrl = IconLinks.Admin,
				Name = "Add Groups",
				IsLink = false,
				NavigationUrl = string.Empty
			};

			var removeGroups = new ToolbarMoreAction
			{
				CommandArgs = RemoveGroupsArgs,
				ConfirmCommand = true,
				ImageUrl = IconLinks.Admin,
				Name = "Remove Groups",
				IsLink = false,
				NavigationUrl = string.Empty
			};

            var importUsers = new ToolbarMoreAction
            {
                CommandArgs = ImportUsersArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Admin,
                Name = "Import Users",
                IsLink = false,
                NavigationUrl = string.Empty
            };
			
            var actions = new List<ToolbarMoreAction>();

            if (ActiveUser.HasAccessToModify(ViewCode.Group) && hidUserId.Value.ToLong() != default(long))
            {
                actions.Add(addGroups);
                actions.Add(removeGroups);
            }
           
            if (ActiveUser.HasAccessToModify(ViewCode.User))
            {
                if (actions.Any()) actions.Add(new ToolbarMoreAction { IsSeperator = true });
                actions.Add(importUsers);
            }

		    memberToolBar.ShowMore = actions.Any();

			return actions;
		}

		private void SetEditStatus(bool enabled)
		{
			pnlDetails.Enabled = enabled;
			pnlPermissions.Enabled = enabled;
			pnlShipAs.Enabled = enabled;

			memberToolBar.EnableSave = enabled;

			litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;

			memberToolBar.LoadAdditionalActions(ToolbarMoreActions());
		}

		private List<Permission> RetrievePermissions()
		{
			return lstPermissions.Items
				.Select(item => ((PermissionLineItemControl)item.FindControl("pcPermissionLineItem")).RetreivePermission<Permission>())
				.ToList();
		}


		private void LoadUser(User user)
		{
			txtUsername.Text = user.Username;
			if (user.IsNew) txtPassword.Text = string.Empty;
			txtFailedAttempts.Text = user.FailedLoginAttempts.ToString();
			hidFailedAttempts.Value = user.FailedLoginAttempts.ToString();
			txtFirstName.Text = user.FirstName;
			txtLastName.Text = user.LastName;
            txtAdUserName.Text = user.AdUserName;
			txtPhone.Text = user.Phone;
			txtMobile.Text = user.Mobile;
			txtFax.Text = user.Fax;
			txtEmail.Text = user.Email;
		    txtQlikUserId.Text = user.QlikUserId;
            txtQlikUserDirectory.Text = user.QlikUserDirectory;
            txtDatLoadboardUsername.Text = user.DatLoadboardUsername;
            txtDatLoadboardPassword.Text = user.DatLoadboardPassword;
			saicUserAddress.LoadAddress(user);
			ddlDepartment.SelectedValue = user.DepartmentId.ToString();
			chkForcePasswordReset.Checked = user.ForcePasswordReset;
			chkEnabled.Checked = user.Enabled;
			chkEmployee.Checked = user.TenantEmployee;
            chkAlwaysShowRatingNotice.Checked = user.AlwaysShowRatingNotice;
            chkAlwaysShowFinderParametersOnSearch.Checked = user.AlwaysShowFinderParametersOnSearch;
			chkIsCarrierCoordinator.Checked = user.IsCarrierCoordinator;
			chkIsShipmentCoordinator.Checked = user.IsShipmentCoordinator;

			if (user.DefaultCustomer != null)
			{
				hidCustomerId.Value = user.DefaultCustomer.Id.ToString();
				txtCustomerName.Text = user.DefaultCustomer.Name;
				txtCustomerNumber.Text = user.DefaultCustomer.CustomerNumber;
			}
			else
			{
				hidCustomerId.Value = string.Empty;
				txtCustomerName.Text = string.Empty;
				txtCustomerNumber.Text = string.Empty;
			}

			hidUserId.Value = user.Id.ToString();

			//Permisions
			lstPermissions.DataSource = user.UserPermissions.OrderBy(p => p.Code);
			lstPermissions.DataBind();
			tabPermissions.HeaderText = user.UserPermissions.BuildTabCount(PermissionsHeader);

			//Permission Groups
			var permissionGroups = user.RetrieveUsersGroups();
			lstGroups.DataSource = permissionGroups;
			lstGroups.DataBind();
			tabPermissionGroups.HeaderText = permissionGroups.BuildTabCount(PermissionGroupsHeader);

			//ShipAS
			var shipAsCustomers = user.RetrieveShipAsCustomers();
			lstShipAs.DataSource = shipAsCustomers;
			lstShipAs.DataBind();
			tabShipAs.HeaderText = shipAsCustomers.BuildTabCount(ShipAsCustomerHeader);

            //Vendor Portal Options
		    chkCanSeeAllVendorsInPortal.Checked = user.CanSeeAllVendorsInVendorPortal;
		    txtVendorNumber.Text = user.VendorPortalVendorId != default(long)
		                               ? user.VendorPortalVendor.VendorNumber
		                               : string.Empty;
            txtVendorName.Text = user.VendorPortalVendorId != default(long)
		                               ? user.VendorPortalVendor.Name
		                               : string.Empty;
		    hidVendorId.Value = user.VendorPortalVendorId.ToString();

			memberToolBar.ShowUnlock = user.HasUserLock(ActiveUser, user.Id);
		}


        private void ProcessTransferredRequest(User user)
        {
            if (user.IsNew) return;
            
            LoadUser(user);

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<User>(user));
        }



		protected void Page_Load(object sender, EventArgs e)
		{
			new UserHandler(this).Initialize();

			memberToolBar.ShowSave = Access.Modify;
			memberToolBar.ShowDelete = Access.Remove;
			memberToolBar.ShowNew = Access.Modify;
			memberToolBar.ShowEdit = Access.Modify;

			userFinder.OpenForEditEnabled = Access.Modify;

			btnAddPermission.Enabled = Access.Modify;
            memberToolBar.LoadAdditionalActions(ToolbarMoreActions());
            
			if (IsPostBack) return;

			aceCustomer.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.ActiveUser, ActiveUser.Id.GetString() } });

            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.UserImportTemplate });

			if (Loading != null)
				Loading(this, new EventArgs());

            if (Session[WebApplicationConstants.TransferUserId] != null)
            {
                ProcessTransferredRequest(new User(Session[WebApplicationConstants.TransferUserId].ToLong()));
                Session[WebApplicationConstants.TransferUserId] = null;
            }

            if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.TransferNumber]))
                ProcessTransferredRequest(new User(Request.QueryString[WebApplicationConstants.TransferNumber].UrlTextDecrypt().ToLong()));

			SetEditStatus(false);
		}


		protected void OnUserFinderItemSelected(object sender, ViewEventArgs<User> e)
		{
			litErrorMessages.Text = string.Empty;

			// check for previous items ...
			if (hidUserId.Value.ToLong() != default(long))
			{
				var oldUser = new User(hidUserId.Value.ToLong(), false);
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<User>(oldUser));
			}

			var user = e.Argument;

			LoadUser(user);

			userFinder.Visible = false;

			if (userFinder.OpenForEdit && Lock != null)
			{
				SetEditStatus(true);
				memberToolBar.ShowUnlock = true;
				Lock(this, new ViewEventArgs<User>(user));
			}
			else
			{
				SetEditStatus(false);
			}

			if (LoadAuditLog != null) LoadAuditLog(this, new ViewEventArgs<User>(user));
		}

		protected void OnUserFinderSelectionCancelled(object sender, EventArgs e)
		{
			userFinder.Visible = false;
		}


		protected void OnPermissionSelectorItemsSelected(object sender, ViewEventArgs<List<Permission>> e)
		{
			var permissions = RetrievePermissions();
			permissions.AddRange(e.Argument);

			lstPermissions.DataSource = permissions.OrderBy(p => p.Code);
			lstPermissions.DataBind();
			tabPermissions.HeaderText = permissions.BuildTabCount(PermissionsHeader);

			permissionSelector.Visible = false;
		}

		protected void OnPermissionSelectorSelectionCancelled(object sender, EventArgs e)
		{
			permissionSelector.Visible = false;
		}


		protected void BindPermissionLineItem(object sender, ListViewItemEventArgs e)
		{
			var lineItem = (PermissionLineItemControl)e.Item.FindControl("pcPermissionLineItem");
			if (lineItem == null) return;
			lineItem.LoadPermission((Permission)e.Item.DataItem);
		}

		protected void OnAddPermissionClicked(object sender, EventArgs e)
		{
			permissionSelector.CodesToExclude = RetrievePermissions().Select(s => s.Code).ToList();
			permissionSelector.Visible = true;
		}

		protected void OnClearPermissionsClicked(object sender, EventArgs e)
		{
			lstPermissions.DataSource = new List<Permission>();
			lstPermissions.DataBind();
			tabPermissions.HeaderText = PermissionsHeader;
            athtuTabUpdater.SetForUpdate(tabPermissions.ClientID, PermissionsHeader);

		}

		protected void OnPermissionItemRemovePermission(object sender, ViewEventArgs<string> e)
		{
			var permissions = RetrievePermissions();
			for (var i = permissions.Count - 1; i > -1; i--)
				if (permissions[i].Code == e.Argument)
				{
					permissions.RemoveAt(i);
					break;
				}
			lstPermissions.DataSource = permissions;
			lstPermissions.DataBind();
			tabPermissions.HeaderText = permissions.BuildTabCount(PermissionsHeader);
            athtuTabUpdater.SetForUpdate(tabPermissions.ClientID, permissions.BuildTabCount(PermissionsHeader));
        }


		protected void OnNewClicked(object sender, EventArgs e)
		{
			litErrorMessages.Text = string.Empty;

			LoadUser(new User { CountryId = ActiveUser.Tenant.DefaultCountryId });
			DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
			SetEditStatus(true);
		}

		protected void OnEditClicked(object sender, EventArgs e)
		{
			var user = new User(hidUserId.Value.ToLong(), false);
			if (Lock != null && !user.IsNew)
			{
				SetEditStatus(true);
				memberToolBar.ShowUnlock = true;
				Lock(this, new ViewEventArgs<User>(user));
			}
			LoadUser(user);
		}

		protected void OnUnlockClicked(object sender, EventArgs e)
		{
			var user = new User(hidUserId.Value.ToLong(), false);
			if (UnLock != null && !user.IsNew)
			{
				SetEditStatus(false);
				memberToolBar.ShowUnlock = false;
				UnLock(this, new ViewEventArgs<User>(user));
			}
		}

		protected void OnSaveClicked(object sender, EventArgs e)
		{
			var id = hidUserId.Value.ToLong();
			var user = new User(id, id != default(long))
						{
							Username = txtUsername.Text,
							FailedLoginAttempts = hidFailedAttempts.Value.ToInt(),
							FirstName = txtFirstName.Text,
							LastName = txtLastName.Text,
                            AdUserName = txtAdUserName.Text,
							Phone = txtPhone.Text,
							Mobile = txtMobile.Text,
							Fax = txtFax.Text,
							Email = txtEmail.Text.StripSpacesFromEmails(),
							DepartmentId = ddlDepartment.SelectedValue.ToLong(),
							ForcePasswordReset = chkForcePasswordReset.Checked,
							Enabled = chkEnabled.Checked,
							AlwaysShowRatingNotice = chkAlwaysShowRatingNotice.Checked,
                            AlwaysShowFinderParametersOnSearch = chkAlwaysShowFinderParametersOnSearch.Checked,
							TenantEmployee = chkEmployee.Checked,
							DefaultCustomer = hidCustomerId.Value.ToLong() == default(long)
												? null
												: new Customer(hidCustomerId.Value.ToLong()),
							IsCarrierCoordinator = chkIsCarrierCoordinator.Checked,
							IsShipmentCoordinator = chkIsShipmentCoordinator.Checked,
                            QlikUserId = txtQlikUserId.Text,
                            QlikUserDirectory = txtQlikUserDirectory.Text,
                            DatLoadboardPassword = txtDatLoadboardPassword.Text,
                            DatLoadboardUsername = txtDatLoadboardUsername.Text,
                            CanSeeAllVendorsInVendorPortal = chkCanSeeAllVendorsInPortal.Checked,
                            VendorPortalVendorId = hidVendorId.Value.ToLong()
						};
			saicUserAddress.RetrieveAddress(user);
			if (!string.IsNullOrEmpty(txtPassword.Text)) user.Password = txtPassword.Text;
			if (user.IsNew)
			{
				user.TenantId = ActiveUser.TenantId;
			}
			user.LoadCollections();

			// update permissions
			var vps = RetrievePermissions().ToDictionary(p => p.Code, p => p);
			var remove = user.UserPermissions.Where(p => !vps.ContainsKey(p.Code)).Select(p => p.Code).ToList();
			for (int i = user.UserPermissions.Count - 1; i > -1; i--) if (remove.Contains(user.UserPermissions[i].Code))
					user.UserPermissions.RemoveAt(i);
			foreach (var p in user.UserPermissions)
			{
				p.TakeSnapShot();
				p.Description = vps[p.Code].Description;
				p.Grant = vps[p.Code].Grant;
				p.Modify = vps[p.Code].Modify;
				p.ModifyApplicable = vps[p.Code].ModifyApplicable;
				p.Remove = vps[p.Code].Remove;
				p.DeleteApplicable = vps[p.Code].DeleteApplicable;
				p.Deny = vps[p.Code].Deny;
				vps.Remove(p.Code); // remove updated permission
			}
			foreach (var key in vps.Keys)// add new
			{
				user.UserPermissions.Add(new UserPermission
											{
												Description = vps[key].Description,
												Grant = vps[key].Grant,
												Modify = vps[key].Modify,
												ModifyApplicable = vps[key].ModifyApplicable,
												Remove = vps[key].Remove,
												DeleteApplicable = vps[key].DeleteApplicable,
												Deny = vps[key].Deny,
												Code = vps[key].Code,
												User = user,
												TenantId = ActiveUser.TenantId,
											});
			}

			// update ship As
			var userShipAs = lstShipAs.Items
				.Select(item => new UserShipAs
									{
										User = user,
										CustomerId = item.FindControl("hidCustomerId").ToCustomHiddenField().Value.ToLong(),
										Tenant = user.Tenant
									}).ToList();

			user.UserShipAs = userShipAs;

			hidFlag.Value = SaveFlag;

			if (Save != null)
				Save(this, new ViewEventArgs<User>(user));

			if (LoadAuditLog != null)
				LoadAuditLog(this, new ViewEventArgs<User>(user));
		}

		protected void OnDeleteClicked(object sender, EventArgs e)
		{
			var user = new User(hidUserId.Value.ToLong(), false);
			if (Delete != null)
				Delete(this, new ViewEventArgs<User>(user));
			userFinder.Reset();
		}

		protected void OnFindClicked(object sender, EventArgs e)
		{
			userFinder.Visible = true;
		}

		protected void OnCommand(object sender, ViewEventArgs<string> e)
		{
			switch (e.Argument)
			{
				case AddGroupsArgs:
					AddGroups();
					break;
				case RemoveGroupsArgs:
					RemoveGroups();
					break;
                case ImportUsersArgs:
			        fileUploader.Visible = true;
			        break;
			}
		}


		private void AddGroups()
		{
			groupFinder.Visible = true;
		}

		private void RemoveGroups()
		{
			var groups = lstGroups.Items
				.Select(i => new
								{
									Id = i.FindControl("hidGroupId").ToCustomHiddenField().Value.ToInt(),
									Name = i.FindControl("litName").ToLiteral().Text
								})
				.ToList();

			lstRemoveGroups.DataSource = groups;
			lstRemoveGroups.DataBind();
			tabPermissionGroups.HeaderText = groups.BuildTabCount(PermissionGroupsHeader);

			pnlSelectGroupsToRemove.Visible = true;
			pnlDimScreen.Visible = true;
		}


		protected void OnCustomerFinderMultiItemSelected(object sender, ViewEventArgs<List<Customer>> e)
		{
			var shipAs = lstShipAs.Items
			                      .Select(c => new
				                      {
					                      Id = c.FindControl("hidCustomerId").ToCustomHiddenField().Value,
					                      Name = c.FindControl("litCustomerName").ToLiteral().Text,
                                          CustomerNumber = c.FindControl("litCustomerNumber").ToLiteral().Text,
					                      Active = c.FindControl("hidActive").ToCustomHiddenField().Value.ToBoolean()
				                      }).ToList();		


			var customers = e.Argument
				.Select(c => new { Id = c.Id.ToString(), c.Name, c.CustomerNumber, c.Active })
				.Where(c => !shipAs.Select(s => s.Id).Contains(c.Id));
			shipAs.AddRange(customers);

			lstShipAs.DataSource = shipAs.OrderBy(c => c.Name).ThenBy(c => c.CustomerNumber);
			lstShipAs.DataBind();
			tabShipAs.HeaderText = shipAs.BuildTabCount(ShipAsCustomerHeader);

			customerFinder.Visible = false;
		}

		protected void OnCustomerFinderSelectionCancelled(object sender, EventArgs e)
		{
			customerFinder.Visible = false;
		}

		protected void OnCustomerFinderItemSelected(object sender, ViewEventArgs<Customer> e)
		{
			hidCustomerId.Value = e.Argument.Id.ToString();
			txtCustomerName.Text = e.Argument.Name;
			txtCustomerNumber.Text = e.Argument.CustomerNumber;

			customerFinder.Visible = false;
		}

		protected void OnFindCustomerClicked(object sender, ImageClickEventArgs e)
		{
			customerFinder.Reset();
			customerFinder.EnableMultiSelection = false;
			customerFinder.Visible = true;
		}

		protected void OnCustomerNumberEntered(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(txtCustomerNumber.Text))
			{
				hidCustomerId.Value = string.Empty;
				txtCustomerName.Text = string.Empty;
				return;
			}

			if (CustomerSearch != null)
				CustomerSearch(this, new ViewEventArgs<string>(txtCustomerNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
		}


		protected void OnAddShipAsClicked(object sender, EventArgs e)
		{
			customerFinder.Reset();
			customerFinder.EnableMultiSelection = true;
			customerFinder.Visible = true;
		}

        protected void OnClearShipAsClicked(object sender, EventArgs e)
        {
			lstShipAs.DataSource = new List<Customer>();
			lstShipAs.DataBind();
			tabShipAs.HeaderText = ShipAsCustomerHeader;
            athtuTabUpdater.SetForUpdate(tabShipAs.ClientID, ShipAsCustomerHeader);
        }

		protected void OnDeleteShipAsClicked(object sender, ImageClickEventArgs e)
		{
			var button = (ImageButton)sender;
			var index = button.Parent.FindControl("hidCustomerItemIndex").ToCustomHiddenField().Value.ToInt();

			var shipAs = lstShipAs.Items
			                      .Select(c => new
				                      {
					                      Id = c.FindControl("hidCustomerId").ToCustomHiddenField().Value,
					                      Name = c.FindControl("litCustomerName").ToLiteral().Text,
                                          CustomerNumber = c.FindControl("litCustomerNumber").ToLiteral().Text,
					                      Active = c.FindControl("hidActive").ToCustomHiddenField().Value.ToBoolean()
				                      })
			                      .ToList();
			shipAs.RemoveAt(index);

			lstShipAs.DataSource = shipAs;
			lstShipAs.DataBind();
			tabShipAs.HeaderText = shipAs.BuildTabCount(ShipAsCustomerHeader);
            athtuTabUpdater.SetForUpdate(tabShipAs.ClientID, shipAs.BuildTabCount(ShipAsCustomerHeader));
		}


		protected void OnGroupFinderMultiItemSelected(object sender, ViewEventArgs<List<Group>> e)
		{
			var user = new User(hidUserId.Value.ToLong(), false);

			if (user.IsNew) return;

			foreach (var group in e.Argument)
				group.GroupUsers.Add(new GroupUser { TenantId = ActiveUser.TenantId, Group = group, User = user });

			hidFlag.Value = AddGroupsArgs;

			groupFinder.Visible = false;

			if (UpdateGroupUsers != null)
				UpdateGroupUsers(this, new ViewEventArgs<List<Group>>(e.Argument));
		}

		protected void OnGroupFinderSelectionCancel(object sender, EventArgs e)
		{
			groupFinder.Visible = false;
		}


		protected void OnCloseEditGroupClicked(object sender, EventArgs eventArgs)
		{
			pnlSelectGroupsToRemove.Visible = false;
			pnlDimScreen.Visible = false;
		}

		protected void OnEditGroupDoneClicked(object sender, EventArgs e)
		{
			var groupsToRemove = lstRemoveGroups.Items
			                                    .Select(i => new
				                                    {
					                                    Id = i.FindControl("hidGroupId").ToCustomHiddenField().Value.ToInt(),
					                                    Selection = i.FindControl("chkSelection").ToCheckBox().Checked
				                                    })
			                                    .Where(i => i.Selection)
			                                    .Select(i => new Group(i.Id, false))
			                                    .ToList();

			var gu = new List<GroupUser>();
			var userId = hidUserId.Value.ToLong();
			foreach (var @group in groupsToRemove)
				gu.AddRange(@group.GroupUsers.Where(rgu => rgu.UserId == userId));

			hidFlag.Value = AddGroupsArgs;

			pnlSelectGroupsToRemove.Visible = false;
			pnlDimScreen.Visible = false;

			if (RemoveGroupUsers != null)
				RemoveGroupUsers(this, new ViewEventArgs<List<GroupUser>>(gu));
		}


	    protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
	    {
            fileUploader.Visible = false;
	    }

	    protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
	    {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            if (lines.Count > 10000)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Batch import is limited to 10,000 records, please adjust your file size.") });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 37;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

	        var customers = new CustomerSearch().FetchCustomers(new CustomerViewSearchCriteria(), ActiveUser.TenantId);
	        var customerNames = customers.Select(c => c.CustomerNumber).ToList();
            msgs.AddRange(chks.Where(i => !string.IsNullOrEmpty(i.Line[14])).CodesAreValid(14, customerNames, new Customer().EntityName()));
            msgs.AddRange(chks.Where(i => !string.IsNullOrEmpty(i.Line[25])).CodesAreValid(25, customerNames, new Customer().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var vendors = new VendorSearch().FetchVendors(new List<ParameterColumn>(), ActiveUser.TenantId);
            var vendorNumbers = vendors.Select(c => c.VendorNumber).ToList();
            msgs.AddRange(chks.Where(i => !string.IsNullOrEmpty(i.Line[37])).CodesAreValid(37, vendorNumbers, new Vendor().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var departments = new UserDepartmentSearch().FetchUserDepartments(string.Empty, ActiveUser.TenantId);
            var departmentCodes = departments.Select(c => c.Code).ToList();
            msgs.AddRange(chks.Where(i => !string.IsNullOrEmpty(i.Line[13])).CodesAreValid(13, departmentCodes, new UserDepartment().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }


	        var permissions = PermissionGenerator<Permission>.All(new List<string>(), ActiveUser.Tenant).SelectMany(p => p.Value);
	        var permissionCodes = permissions.Select(p => p.Code).ToList();
	        // remove spaces from permission codes
            foreach (var chk in chks) chk.Line[19] = chk.Line[19].Replace(" ", string.Empty);
            msgs.AddRange(chks.Where(i => !string.IsNullOrEmpty(i.Line[19])).CodesAreValid(19, permissionCodes, new Permission().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

	        var groups = new GroupSearch().FetchGroups(new List<ParameterColumn>(), ActiveUser.TenantId);
	        var groupNames = groups.Select(g => g.Name).ToList();
            msgs.AddRange(chks.Where(i => !string.IsNullOrEmpty(i.Line[17])).CodesAreValid(17, groupNames, new Group().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var countryCodes = ProcessorVars.RegistryCache.Countries.Values.Select(c => c.Code).ToList();
            msgs.AddRange(chks.Where(i => !string.IsNullOrEmpty(i.Line[32])).CodesAreValid(32, countryCodes, new Country().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

	        var users = new Dictionary<string, User>();
	        var userSearch = new UserSearch();
	        foreach (var line in chks.Select(c => c.Line))
	        {
                User user = null;
                if (!users.ContainsKey(line[0]))
                {
                    user = userSearch.FetchUserByUsernameAndAccessCode(line[0], ActiveUser.Tenant.Code) ?? new User();
                    if (!user.IsNew) user.TakeSnapShot();
                    user.LoadCollections();

                    if (user.IsNew)
                    {
                        user.Username = line[0];
                        user.TenantId = ActiveUser.TenantId;
                    }
                    if (user.IsNew || !string.IsNullOrEmpty(line[1])) user.FirstName = line[1];
                    if (user.IsNew || !string.IsNullOrEmpty(line[2])) user.LastName = line[2];
                    if (user.IsNew || !string.IsNullOrEmpty(line[3])) user.TenantEmployee = line[3].ToBoolean();
                    if (user.IsNew || !string.IsNullOrEmpty(line[4])) user.IsCarrierCoordinator = line[4].ToBoolean();
                    if (user.IsNew || !string.IsNullOrEmpty(line[5])) user.IsShipmentCoordinator = line[5].ToBoolean();
                    if (user.IsNew || !string.IsNullOrEmpty(line[6])) user.Phone = line[6];
                    if (user.IsNew || !string.IsNullOrEmpty(line[7])) user.Mobile = line[7];
                    if (user.IsNew || !string.IsNullOrEmpty(line[8])) user.Fax = line[8];
                    if (user.IsNew || !string.IsNullOrEmpty(line[9])) user.Email = line[9].StripSpacesFromEmails();
                    if (user.IsNew || !string.IsNullOrEmpty(line[10])) user.Enabled = line[10].ToBoolean();
                    if (user.IsNew || !string.IsNullOrEmpty(line[11])) user.AlwaysShowRatingNotice = line[11].ToBoolean();
                    if (user.IsNew || !string.IsNullOrEmpty(line[12])) user.ForcePasswordReset = line[12].ToBoolean();
                    if (user.IsNew || !string.IsNullOrEmpty(line[13])) user.Department = departments.FirstOrDefault(d => d.Code == line[13]);
                    if (user.IsNew || !string.IsNullOrEmpty(line[14])) user.DefaultCustomer = customers.FirstOrDefault(c => c.CustomerNumber == line[14]);
                    if (user.IsNew || !string.IsNullOrEmpty(line[15])) user.AlwaysShowFinderParametersOnSearch = line[15].ToBoolean();
                    if (user.IsNew || !string.IsNullOrEmpty(line[16])) user.Password = line[16];
                    if (user.IsNew || !string.IsNullOrEmpty(line[27])) user.Street1 = line[27];
                    if (user.IsNew || !string.IsNullOrEmpty(line[28])) user.Street2 = line[28];
                    if (user.IsNew || !string.IsNullOrEmpty(line[29])) user.City = line[29];
                    if (user.IsNew || !string.IsNullOrEmpty(line[30])) user.State = line[30];
                    if (user.IsNew || !string.IsNullOrEmpty(line[31])) user.PostalCode = line[31];
                    if (user.IsNew || !string.IsNullOrEmpty(line[32])) user.CountryId = (ProcessorVars.RegistryCache.Countries.Values.FirstOrDefault(c => c.Code == line[32]) ?? new Country()).Id;
                    if (user.IsNew || !string.IsNullOrEmpty(line[33])) user.QlikUserId = line[33];
                    if (user.IsNew || !string.IsNullOrEmpty(line[34])) user.QlikUserDirectory = line[34];
                    if (user.IsNew || !string.IsNullOrEmpty(line[35])) user.DatLoadboardUsername = line[35];
                    if (user.IsNew || !string.IsNullOrEmpty(line[36])) user.DatLoadboardPassword = line[36];
                    if (user.IsNew || !string.IsNullOrEmpty(line[37])) user.VendorPortalVendor = vendors.FirstOrDefault(d => d.VendorNumber == line[37]);
                    if (user.IsNew || !string.IsNullOrEmpty(line[38])) user.CanSeeAllVendorsInVendorPortal = line[38].ToBoolean();
                    if (user.IsNew || !string.IsNullOrEmpty(line[39])) user.AdUserName = line[39];

                    users.Add(user.Username, user);
                }
                else
                {
                    user = users[line[0]];
                }

                if (line[18] == AddRecord && !string.IsNullOrEmpty(line[17]))
                    user.UserGroups.Add(new GroupUser
                    {
                        User = user,
                        TenantId = ActiveUser.TenantId,
                        Group = groups.First(g => g.Name == line[17]),
                    });
                if (line[18] == RemoveRecord && !string.IsNullOrEmpty(line[17]))
                    user.UserGroups.RemoveAll(ug => ug.Group.Name == line[17]);

                if (line[20] == AddRecord && !string.IsNullOrEmpty(line[19]))
                {
                    var permission = permissions.First(p => p.Code == line[19]);
                    user.UserPermissions.Add(new UserPermission
                    {
                        Code = permission.Code,
                        Grant = line[21].ToBoolean(),
                        Deny = line[22].ToBoolean(),
                        Modify = permission.ModifyApplicable && line[23].ToBoolean(),
                        Remove = permission.DeleteApplicable && line[24].ToBoolean(),
                        TenantId = ActiveUser.TenantId,
                        DeleteApplicable = permission.DeleteApplicable,
                        ModifyApplicable = permission.ModifyApplicable,
                        Description = permission.Description,
                        User = user,
                    });
                }
                if (line[20] == RemoveRecord && !string.IsNullOrEmpty(line[19]))
                    user.UserPermissions.RemoveAll(ug => ug.Code == line[19]);

                if (line[26] == AddRecord && !string.IsNullOrEmpty(line[25]))
                    user.UserShipAs.Add(new UserShipAs
                    {
                        TenantId = ActiveUser.TenantId,
                        User = user,
                        Customer = customers.First(c => c.CustomerNumber == line[25]),
                    });
                if (line[26] == RemoveRecord && !string.IsNullOrEmpty(line[25]))
                    user.UserShipAs.RemoveAll(ug => ug.Customer.CustomerNumber == line[25]);
	        }

            if(BatchImport != null)
                BatchImport(this, new ViewEventArgs<List<User>>(users.Select(u => u.Value).ToList()));
	    }

	    protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
	    {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
	    }

        protected void OnVendorNumberEntered(object sender, EventArgs e)
        {
            if (VendorSearch != null)
                VendorSearch(this, new ViewEventArgs<string>(txtVendorNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
        }

        protected void OnVendorSearchClicked(object sender, ImageClickEventArgs e)
        {
            vendorFinder.Visible = true;
        }

        protected void OnClearVendorClicked(object sender, ImageClickEventArgs e)
        {
            DisplayVendor(new Vendor());
        }

        protected void OnVendorFinderSelectionCancelled(object sender, EventArgs e)
        {
            vendorFinder.Visible = false;
        }

        protected void OnVendorFinderItemSelected(object sender, ViewEventArgs<Vendor> e)
        {
            DisplayVendor(e.Argument);
            vendorFinder.Visible = false;
        }
	}
}