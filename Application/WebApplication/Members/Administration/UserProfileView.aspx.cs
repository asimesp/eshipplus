﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Administration;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Administration;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Administration
{
	public partial class UserProfileView : MemberPageBase, IUserView
	{
		private const string SaveFlag = "S";

		public static string PageAddress
		{
			get { return "~/Members/Administration/UserProfileView.aspx"; }
		}

		public override ViewCode PageCode
		{
			get { return ViewCode.UserProfile; }
		}

		public override string SetPageIconImage
		{
			set { imgPageImageLogo.ImageUrl = IconLinks.AdminBlue; }
		}

		public List<UserDepartment> Departments
		{
			set { }
		}

		public event EventHandler Loading;
		public event EventHandler<ViewEventArgs<string>> CustomerSearch;
		public event EventHandler<ViewEventArgs<string>> VendorSearch;
		public event EventHandler<ViewEventArgs<User>> Save;
		public event EventHandler<ViewEventArgs<User>> Delete;
		public event EventHandler<ViewEventArgs<User>> Lock;
		public event EventHandler<ViewEventArgs<User>> UnLock;
		public event EventHandler<ViewEventArgs<User>> LoadAuditLog;
		public event EventHandler<ViewEventArgs<List<Group>>> UpdateGroupUsers;
		public event EventHandler<ViewEventArgs<List<GroupUser>>> RemoveGroupUsers;
	    public event EventHandler<ViewEventArgs<List<User>>> BatchImport;

	    public void LogException(Exception ex)
		{
			if (ex != null) ErrorLogger.LogError(ex, Context);
		}

		public void DisplayMessages(IEnumerable<ValidationMessage> messages)
		{
			if (messages == null) return;
			if (!messages.Any()) return;

			if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == SaveFlag)
			{
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<User>(ActiveUser));
				SetEditStatus(false);
				memberToolBar.ShowUnlock = false;
				ActiveUser = new User(ActiveUser.Id, false); // refresh active user record
			}

			messageBox.Icon = messages.GenerateIcon();
			messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
			messageBox.Visible = true;
		}

		public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> auditLogs)
		{
		}

		public void DisplayCustomer(Customer customer)
		{
		}

        public void DisplayVendor(Vendor vendor)
        {
        }

		public void FailedLock(Lock @lock)
		{
			memberToolBar.ShowSave = false;
			memberToolBar.ShowUnlock = false;
			litMessage.Text = @lock.BuildFailedLockMessage();
		}

		public void Set(User user)
		{
			LoadUser(user);
		}


		private void SetEditStatus(bool enabled)
		{
			pnlDetails.Enabled = enabled;
			memberToolBar.EnableSave = enabled;
			litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;
		}

		private void LoadUser(User user)
		{
			txtUsername.Text = user.Username;
			txtFirstName.Text = user.FirstName;
			txtLastName.Text = user.LastName;
			txtPhone.Text = user.Phone;
			txtMobile.Text = user.Mobile;
			txtFax.Text = user.Fax;
			txtEmail.Text = user.Email;
			saicUserAddress.LoadAddress(user);
			chkAlwaysShowRatingNotice.Checked = user.AlwaysShowRatingNotice;
		    chkAlwaysShowFinderParametersOnSearch.Checked = user.AlwaysShowFinderParametersOnSearch;

			memberToolBar.ShowUnlock = user.HasUserLock(ActiveUser, user.Id);
		}

		private void LockRecord()
		{
			if (Lock != null && !ActiveUser.IsNew)
			{
				SetEditStatus(true);
				memberToolBar.ShowUnlock = true;
				Lock(this, new ViewEventArgs<User>(ActiveUser));
			}
			LoadUser(ActiveUser);
		}


		protected void Page_Load(object sender, EventArgs e)
		{
			new UserHandler(this).Initialize();

			if (IsPostBack) return;

			if (Loading != null)
				Loading(this, new EventArgs());

			LockRecord();
		}

		protected void OnSaveClicked(object sender, EventArgs e)
		{
			var user = new User(ActiveUser.Id, true)
			{
				Username = txtUsername.Text,
				FirstName = txtFirstName.Text,
				LastName = txtLastName.Text,
				Phone = txtPhone.Text,
				Mobile = txtMobile.Text,
				Fax = txtFax.Text,
				Email = txtEmail.Text.StripSpacesFromEmails(),
				AlwaysShowRatingNotice = chkAlwaysShowRatingNotice.Checked,
                AlwaysShowFinderParametersOnSearch = chkAlwaysShowFinderParametersOnSearch.Checked
			};
			saicUserAddress.RetrieveAddress(user);

			//if (!string.IsNullOrEmpty(txtPassword.Text) || !string.IsNullOrEmpty(txtPasswordConfirmation.Text))
			//{
			//	if (txtPassword.Text != txtPasswordConfirmation.Text)
			//	{
			//		DisplayMessages(new[] { ValidationMessage.Error(@"Passwords do not match! Please ensure passwords match before saving.") });
			//		return;
			//	}
			//	user.Password = txtPassword.Text;
			//}
			user.LoadCollections();

			hidFlag.Value = SaveFlag;

			if (Save != null)
				Save(this, new ViewEventArgs<User>(user));
		}

		protected void OnEditClicked(object sender, EventArgs e)
		{
			LockRecord();
		}	

		protected void OnUnlockClicked(object sender, EventArgs e)
		{
			var user = new User(ActiveUser.Id, false);
			if (UnLock != null && !user.IsNew)
			{
				SetEditStatus(false);
				memberToolBar.ShowUnlock = false;
				UnLock(this, new ViewEventArgs<User>(user));
			}
		}
	}
}