﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Administration;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Administration;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Administration
{
	public partial class TenantAdministrationView : MemberPageBase, ITenantAdminView
	{
		private const string SeedsHeader = "Seeds";

		protected const string SaveFlag = "S";
		protected const string MailingLocation = "M";
		protected const string BillingLocation = "B";

		public override ViewCode PageCode { get { return ViewCode.TenantAdmin; } }

		public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.AdminBlue; } }

		public static string PageAddress { get { return "~/Members/Administration/TenantAdministrationView.aspx"; } }

		public List<string> TimeList
		{
			set
			{
				ddlStartTime.DataSource = value;
				ddlStartTime.DataBind();

				ddlEndTime.DataSource = value;
				ddlEndTime.DataBind();
			}
		}
		

		public Dictionary<int, string> MileageEngines
		{
			set
			{
				ddlMileageEngine.DataSource = value
					.Select(t => new ViewListItem(t.Value, t.Key.ToString()))
					.OrderBy(t => t.Text)
					.ToList();
				ddlMileageEngine.DataBind();
			}
		}

        public Dictionary<int, string> PaymentGatewayTypes
        {
            set
            {
                ddlPaymentGatewayType.DataSource = value
                    .Select(t => new ViewListItem(t.Value, t.Key.ToString()))
                    .OrderBy(t => t.Text)
                    .ToList();
                ddlPaymentGatewayType.DataBind();
            }
        }


		public event EventHandler Loading;
		public event EventHandler<ViewEventArgs<Tenant>> Save;
		public event EventHandler<ViewEventArgs<Tenant>> Lock;
		public event EventHandler<ViewEventArgs<Tenant>> UnLock;
		public event EventHandler<ViewEventArgs<Tenant>> LoadAuditLog;
		public event EventHandler<ViewEventArgs<string>> DefaultSystemUserSearch;

		public void DisplayDefaultSystemUser(User user)
		{
			txtDefaultSystemUsername.Text = user == null ? string.Empty : user.Username;
			txtDefaultSystemUser.Text = user == null ? string.Empty : string.Format("{0} {1}", user.FirstName, user.LastName);
			hidDefaultSystemUserId.Value = user == null ? string.Empty : user.Id.ToString();
		}

		public void LogException(Exception ex)
		{
			if (ex != null) ErrorLogger.LogError(ex, Context);
		}

		public void DisplayMessages(IEnumerable<ValidationMessage> messages)
		{
			if (messages == null) return;
			if (!messages.Any()) return;

			if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == SaveFlag)
			{
				ActiveUser.Tenant = new Tenant(ActiveUser.TenantId, false);

				// unlock and return to read-only
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<Tenant>(ActiveUser.Tenant));
				SetEditStatus(false);
				memberToolBar.ShowUnlock = false;

				// clean up file cleared out!
				if (!string.IsNullOrEmpty(hidFilesToDelete.Value))
				{
					Server.RemoveFiles(hidFilesToDelete.Value.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries));
					hidFilesToDelete.Value = string.Empty;
				}

				// update integration parameters if changed.
				if (!string.IsNullOrEmpty(hidRequireIntergrationUpdate.Value))
				{
					if (hidRequireIntergrationUpdate.Value.ToBoolean()) Server.UpdateIntegrationVariables(ActiveUser.Tenant);
					hidRequireIntergrationUpdate.Value = string.Empty;
				}

				LoadTenant(ActiveUser.Tenant);
			}

			messageBox.Icon = messages.GenerateIcon();
			messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
			messageBox.Visible = true;

			litErrorMessages.Text = messages.HasErrors()
										? string.Join(WebApplicationConstants.HtmlBreak,
													  messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
														Select(m => m.Message).ToArray())
										: string.Empty;
		}

		public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs)
		{
			auditLogs.Load(logs);
		}

		public void FailedLock(Lock @lock)
		{
			memberToolBar.ShowNew = false;
			memberToolBar.ShowSave = false;
			memberToolBar.ShowDelete = false;
			memberToolBar.ShowUnlock = false;

			SetEditStatus(false);

			litMessage.Text = @lock.BuildFailedLockMessage();
		}


		private void LoadTenant(Tenant tenant)
		{
			txtCode.Text = tenant.Code;
			txtName.Text = tenant.Name;
			txtScac.Text = tenant.TenantScac;
			txtAutoNotificationSubjectPrefix.Text = tenant.AutoNotificationSubjectPrefix;
			chkActive.Checked = tenant.Active;
			txtAutoRateNoticeText.Content = tenant.AutoRatingNoticeText;
			litAutoRateNoticeText.Text = tenant.AutoRatingNoticeText;
			txtRateNoticeText.Content = tenant.RatingNotice;
			litRateNoticeText.Text = tenant.RatingNotice;
			txtImgRtrvAllowanceDays.Text = tenant.ShipmentDocImgRtrvAllowance.ToString();

		    ddlPaymentGatewayType.SelectedValue = tenant.PaymentGatewayType.ToInt().ToString();
		    txtPaymentGatewayLoginId.Text = tenant.PaymentGatewayLoginId;
		    txtPaymentGatewaySecret.Text = tenant.PaymentGatewaySecret;
		    txtPaymentGatewayTransactionId.Text = tenant.PaymentGatewayTransactionId;
		    chkPaymentGatewayInTestMode.Checked = tenant.PaymentGatewayInTestMode;

			ddlDefaultCountryId.SelectedValue = tenant.DefaultCountryId.GetString();
			ddlBorderCrossingService.SelectedValue = tenant.BorderCrossingServiceId.ToString();
			ddlGuaranteedDeliveryService.SelectedValue = tenant.GuaranteedDeliveryServiceId.ToString();
		    ddlDefaultUnmappedChargeCode.SelectedValue = tenant.DefaultUnmappedChargeCodeId.ToString();
			ddlHarzadousMaterialService.SelectedValue = tenant.HazardousMaterialServiceId.GetString();
			ddlDefaultSchedulingContactType.SelectedValue = tenant.DefaultSchedulingContactTypeId.GetString();
			ddlDefaultPackagingType.SelectedValue = tenant.DefaultPackagingTypeId.GetString();
			ddlAvailableLoadsContactType.SelectedValue = tenant.AvailableLoadsContactTypeId.GetString();
			if (ddlMileageEngine.ContainsValue(tenant.DefaultMileageEngine.ToInt().GetString()))
				ddlMileageEngine.SelectedValue = tenant.DefaultMileageEngine.ToInt().GetString();
			ddlMacroPointChargeCode.SelectedValue = tenant.MacroPointChargeCodeId.GetString();
			ddlPodDocTag.SelectedValue = tenant.PodDocumentTagId.GetString();
			ddlBolDocTag.SelectedValue = tenant.BolDocumentTagId.GetString();
			ddlWniDocTag.SelectedValue = tenant.WniDocumentTagId.GetString();

			DisplayDefaultSystemUser(tenant.DefaultSystemUser);

			if (ddlEndTime.ContainsValue(tenant.BatchRatingAnalysisEndTime))
				ddlEndTime.SelectedValue = tenant.BatchRatingAnalysisEndTime;
			if (ddlStartTime.ContainsValue(tenant.BatchRatingAnalysisStartTime))
				ddlStartTime.SelectedValue = tenant.BatchRatingAnalysisStartTime;

			txtInsuranceCostPerDollar.Text = tenant.InsuranceCostPerDollar.ToString();
			txtInsuranceCostPurchaseFloor.Text = tenant.InsuranceCostPurchaseFloor.ToString();
			txtInsuranceCostTotalPercentage.Text = tenant.InsuranceCostTotalPercentage.ToString();
			txtInsuranceRevenueRatePerDollar.Text = tenant.InsuranceRevenueRatePerDollar.ToString();
			txtInsuranceRevenueTotalPercentage.Text = tenant.InsuranceRevenueTotalPercentage.ToString();

			chkSmcCarrierConnectEnabled.Checked = tenant.SMCCarrierConnectEnabled;
			chkSmcRateWareEnabled.Checked = tenant.SMCRateWareEnabled;
			chkFedExSmallPackEnabled.Checked = tenant.FedExSmallPackEnabled;
			chkUpsSmallPackEnabled.Checked = tenant.UpsSmallPackEnabled;
			chkMacroPointEnabled.Checked = tenant.MacroPointEnabled;

			mailingAddressInput.LoadAddress(tenant.MailingLocation);


			lstMailingContacts.DataSource = tenant.MailingLocation.Contacts;
			lstMailingContacts.DataBind();

			billingAddressInput.LoadAddress(tenant.BillingLocation);

			lstBillingContacts.DataSource = tenant.BillingLocation.Contacts;
			lstBillingContacts.DataBind();

			lbtnDownloadSmcRateWare.Text = Server.GetFileName(tenant.SMCRateWareParameterFile);
			lbtnDownloadSmcRateWare.Visible = !string.IsNullOrEmpty(lbtnDownloadSmcRateWare.Text);
			hidSmcRateWare.Value = tenant.SMCRateWareParameterFile;

			lbtnDownloadSmcCarrierConnect.Text = Server.GetFileName(tenant.SMCCarrierConnectParameterFile);
			lbtnDownloadSmcCarrierConnect.Visible = !string.IsNullOrEmpty(lbtnDownloadSmcCarrierConnect.Text);
			hidSmcCarrierConnect.Value = tenant.SMCCarrierConnectParameterFile;

			lbtnDownloadFedExSmallPack.Text = Server.GetFileName(tenant.FedExSmallPackParameterFile);
			lbtnDownloadFedExSmallPack.Visible = !string.IsNullOrEmpty(lbtnDownloadFedExSmallPack.Text);
			hidFedExSmallPack.Value = tenant.FedExSmallPackParameterFile;

			lbtnDownloadUpsSmallPack.Text = Server.GetFileName(tenant.UpsSmallPackParameterFile);
			lbtnDownloadUpsSmallPack.Visible = !string.IsNullOrEmpty(lbtnDownloadUpsSmallPack.Text);
			hidUpsSmallPack.Value = tenant.UpsSmallPackParameterFile;

			lbtnDownloadMacroPoint.Text = Server.GetFileName(tenant.MacroPointParameterFile);
			lbtnDownloadMacroPoint.Visible = !string.IsNullOrEmpty(lbtnDownloadMacroPoint.Text);
			hidMacroPoint.Value = tenant.MacroPointParameterFile;

			lBtnDownloadTermsAndConditions.Text = Server.GetFileName(tenant.TermsAndConditionsFile);
			lBtnDownloadTermsAndConditions.Visible = !string.IsNullOrEmpty(lBtnDownloadTermsAndConditions.Text);
			hidTermsAndConditions.Value = tenant.TermsAndConditionsFile;

			imgLogo.ImageUrl = tenant.LogoUrl;
			imgLogo.AlternateText = string.IsNullOrEmpty(tenant.LogoUrl) ? string.Empty : string.Format("{0} logo", tenant.Name);
			hidLogoUrl.Value = tenant.LogoUrl;

			hidTenantId.Value = tenant.Id.ToString();
			hidMailingLocationId.Value = tenant.MailingLocation.Id.ToString();
			hidBillingLocationId.Value = tenant.BillingLocation.Id.ToString();


			txtFTLPickupTolerance.Text = tenant.TruckloadPickupTolerance.ToString();
			txtFTLDeliveryTolerance.Text = tenant.TruckloadDeliveryTolerance.ToString();

			//seeds
			var seeds = ProcessorUtilities.GetAll<AutoNumberCode>();
			var autoNumbers = tenant.AutoNumbers.ToList();
			foreach (var seed in seeds.Where(s => autoNumbers.All(a => a.Code != s.Key.ToEnum<AutoNumberCode>())))
				autoNumbers.Add(new AutoNumber {Code = seed.Key.ToEnum<AutoNumberCode>(), NextNumber = 10000, Tenant = tenant});
			lstSeeds.DataSource = autoNumbers;
			lstSeeds.DataBind();
			tabSeeds.HeaderText = autoNumbers.BuildTabCount(SeedsHeader);


			memberToolBar.ShowUnlock = ActiveUser.Tenant.HasUserLock(ActiveUser, ActiveUser.TenantId);
		}

		private void SetEditStatus(bool enabled)
		{
			pnlDetails.Enabled = enabled;
			pnlLocations.Enabled = enabled;
			pnlSeeds.Enabled = enabled;
			pnlIntegration.Enabled = enabled;
			pnlAutoRatingDetails.Enabled = enabled;

			fupFedExSmallPack.Enabled = enabled;
			fupLogoImage.Enabled = enabled;
			fupSmcCarrierConnect.Enabled = enabled;
			fupSmcRateWare.Enabled = enabled;
			fupUpsSmallPack.Enabled = enabled;
			fupTermsAndConditions.Enabled = enabled;
			fupMacroPoint.Enabled = enabled;

			txtAutoRateNoticeText.Enabled = enabled;
			txtAutoRateNoticeText.Visible = enabled;
			litAutoRateNoticeText.Visible = !enabled;
			litAutoRateNoticeText.Text = txtAutoRateNoticeText.Content;

			txtRateNoticeText.Enabled = enabled;
			txtRateNoticeText.Visible = enabled;
			litRateNoticeText.Visible = !enabled;
			litRateNoticeText.Text = txtRateNoticeText.Content;


			memberToolBar.EnableSave = enabled;

			litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;
		}

		private bool LoadFiles(Tenant tenant)
		{
			var virtualPath = WebApplicationSettings.TenantFolder(tenant.Id);
			var physicalPath = Server.MapPath(virtualPath);

			if (!Directory.Exists(physicalPath)) Directory.CreateDirectory(physicalPath);

			if (fupLogoImage.HasFile)
			{
				//ensure unique filename
				var fi = new FileInfo(Server.MakeUniqueFilename(Path.Combine(physicalPath, fupLogoImage.FileName), true));

				fupLogoImage.WriteToFile(physicalPath, fi.Name, Server.MapPath(tenant.LogoUrl));
				tenant.LogoUrl = virtualPath + fi.Name;
			}
			else if (string.IsNullOrEmpty(hidLogoUrl.Value)) tenant.LogoUrl = string.Empty;

			if (fupTermsAndConditions.HasFile)
			{
				//ensure unique filename
				var fi = new FileInfo(Server.MakeUniqueFilename(Path.Combine(physicalPath, fupTermsAndConditions.FileName), true));

				fupTermsAndConditions.WriteToFile(physicalPath, fi.Name, Server.MapPath(tenant.TermsAndConditionsFile));
				tenant.TermsAndConditionsFile = virtualPath + fi.Name;
			}
			else if (string.IsNullOrEmpty(hidTermsAndConditions.Value)) tenant.TermsAndConditionsFile = string.Empty;

			if (fupFedExSmallPack.HasFile)
			{
				//ensure unique filename
				var fi = new FileInfo(Server.MakeUniqueFilename(Path.Combine(physicalPath, fupFedExSmallPack.FileName), true));

				fupFedExSmallPack.WriteToFile(physicalPath, fi.Name, Server.MapPath(tenant.FedExSmallPackParameterFile));
				tenant.FedExSmallPackParameterFile = virtualPath + fi.Name;
			}
			else if (string.IsNullOrEmpty(hidFedExSmallPack.Value)) tenant.FedExSmallPackParameterFile = string.Empty;

			if (fupSmcCarrierConnect.HasFile)
			{
				//ensure unique filename
				var fi = new FileInfo(Server.MakeUniqueFilename(Path.Combine(physicalPath, fupSmcCarrierConnect.FileName), true));

				fupSmcCarrierConnect.WriteToFile(physicalPath, fi.Name, Server.MapPath(tenant.SMCCarrierConnectParameterFile));
				tenant.SMCCarrierConnectParameterFile = virtualPath + fi.Name;
			}
			else if (string.IsNullOrEmpty(hidSmcCarrierConnect.Value)) tenant.SMCCarrierConnectParameterFile = string.Empty;

			if (fupSmcRateWare.HasFile)
			{
				//ensure unique filename
				var fi = new FileInfo(Server.MakeUniqueFilename(Path.Combine(physicalPath, fupSmcRateWare.FileName), true));

				fupSmcRateWare.WriteToFile(physicalPath, fi.Name, Server.MapPath(tenant.SMCRateWareParameterFile));
				tenant.SMCRateWareParameterFile = virtualPath + fi.Name;
			}
			else if (string.IsNullOrEmpty(hidSmcRateWare.Value)) tenant.SMCRateWareParameterFile = string.Empty;

			if (fupUpsSmallPack.HasFile)
			{
				//ensure unique filename
				var fi = new FileInfo(Server.MakeUniqueFilename(Path.Combine(physicalPath, fupUpsSmallPack.FileName), true));

				fupUpsSmallPack.WriteToFile(physicalPath, fi.Name, Server.MapPath(tenant.UpsSmallPackParameterFile));
				tenant.UpsSmallPackParameterFile = virtualPath + fi.Name;
			}
			else if (string.IsNullOrEmpty(hidUpsSmallPack.Value)) tenant.UpsSmallPackParameterFile = string.Empty;

			if (fupMacroPoint.HasFile)
			{
				//ensure unique filename
				var fi = new FileInfo(Server.MakeUniqueFilename(Path.Combine(physicalPath, fupMacroPoint.FileName), true));

				fupMacroPoint.WriteToFile(physicalPath, fi.Name, Server.MapPath(tenant.MacroPointParameterFile));
				tenant.MacroPointParameterFile = virtualPath + fi.Name;
			}
			else if (string.IsNullOrEmpty(hidMacroPoint.Value)) tenant.MacroPointParameterFile = string.Empty;

			return fupUpsSmallPack.HasFile || fupFedExSmallPack.HasFile || fupSmcCarrierConnect.HasFile || fupSmcRateWare.HasFile || fupMacroPoint.HasFile;
		}

		private static void RetrieveContacts(TenantLocation location, ListView listView)
		{
			var contacts = listView.Items
				.Select(item =>
				{
					var id = item.FindControl("hidContactId").ToCustomHiddenField().Value.ToLong();
					return new TenantContact(id, id != default(long))
					{
						Comment = item.FindControl("txtComments").ToTextBox().Text,
						ContactTypeId = item.FindControl("ddlContactContactTypes").ToCachedObjectDropDownList().SelectedValue.ToLong(),
						Email = item.FindControl("txtEmail").ToTextBox().Text.StripSpacesFromEmails(),
						Fax = item.FindControl("txtFax").ToTextBox().Text,
						Mobile = item.FindControl("txtMobile").ToTextBox().Text,
						Name = item.FindControl("txtName").ToTextBox().Text,
						Phone = item.FindControl("txtPhone").ToTextBox().Text,
						Primary = item.FindControl("chkPrimary").ToCheckBox().Checked,
						Location = location,
						Tenant = location.Tenant
					};
				})
				.ToList();

			location.Contacts = contacts;
		}



		protected void Page_Load(object sender, EventArgs e)
		{
			new TenantAdminHandler(this).Initialize();

			memberToolBar.ShowSave = Access.Modify;
			memberToolBar.ShowDelete = Access.Remove;
			memberToolBar.ShowNew = false;

			if (IsPostBack) return;

			aceUser.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.ActiveUser, ActiveUser.Id.GetString() } });

			if (Loading != null)
				Loading(this, new EventArgs());

			LoadTenant(new Tenant(ActiveUser.TenantId));

			if (LoadAuditLog != null)
				LoadAuditLog(this, new ViewEventArgs<Tenant>(ActiveUser.Tenant));

			SetEditStatus(false);
		}



		protected void OnSaveClicked(object sender, EventArgs e)
		{
			var tenant = new Tenant(hidTenantId.Value.ToLong(), true)
			{
				Code = txtCode.Text,
				Name = txtName.Text,
				TenantScac = txtScac.Text,
				AutoNotificationSubjectPrefix = txtAutoNotificationSubjectPrefix.Text,
				DefaultCountryId = ddlDefaultCountryId.SelectedValue.ToLong(),
				HazardousMaterialServiceId = ddlHarzadousMaterialService.SelectedValue.ToLong(),
				BorderCrossingServiceId = ddlBorderCrossingService.SelectedValue.ToLong(),
				GuaranteedDeliveryServiceId = ddlGuaranteedDeliveryService.SelectedValue.ToLong(),
				DefaultUnmappedChargeCodeId = ddlDefaultUnmappedChargeCode.SelectedValue.ToLong(),
				DefaultMileageEngine = ddlMileageEngine.SelectedValue.ToInt().ToEnum<MileageEngine>(),
				DefaultSchedulingContactTypeId = ddlDefaultSchedulingContactType.SelectedValue.ToLong(),
				DefaultPackagingTypeId = ddlDefaultPackagingType.SelectedValue.ToLong(),
				MacroPointChargeCodeId = ddlMacroPointChargeCode.SelectedValue.ToLong(),
				AvailableLoadsContactTypeId = ddlAvailableLoadsContactType.SelectedValue.ToLong(),
				Active = chkActive.Checked,
				InsuranceCostPerDollar = txtInsuranceCostPerDollar.Text.ToDecimal(),
				InsuranceCostTotalPercentage = txtInsuranceCostTotalPercentage.Text.ToDecimal(),
				InsuranceCostPurchaseFloor = txtInsuranceCostPurchaseFloor.Text.ToDecimal(),
				InsuranceRevenueRatePerDollar = txtInsuranceRevenueRatePerDollar.Text.ToDecimal(),
				InsuranceRevenueTotalPercentage = txtInsuranceRevenueTotalPercentage.Text.ToDecimal(),
				SMCCarrierConnectEnabled = chkSmcCarrierConnectEnabled.Checked,
				SMCRateWareEnabled = chkSmcRateWareEnabled.Checked,
				FedExSmallPackEnabled = chkFedExSmallPackEnabled.Checked,
				UpsSmallPackEnabled = chkUpsSmallPackEnabled.Checked,
				MacroPointEnabled = chkMacroPointEnabled.Checked,
				AutoRatingNoticeText = txtAutoRateNoticeText.Content,
				RatingNotice = txtRateNoticeText.Content,
				DefaultSystemUserId = hidDefaultSystemUserId.Value.ToLong(),
				BatchRatingAnalysisEndTime = ddlEndTime.SelectedValue,
				BatchRatingAnalysisStartTime = ddlStartTime.SelectedValue,
				TruckloadPickupTolerance = txtFTLPickupTolerance.Text.ToInt(),
				TruckloadDeliveryTolerance = txtFTLDeliveryTolerance.Text.ToInt(),
				PodDocumentTagId = ddlPodDocTag.SelectedValue.ToLong(),
				BolDocumentTagId = ddlBolDocTag.SelectedValue.ToLong(),
				WniDocumentTagId = ddlWniDocTag.SelectedValue.ToLong(),
				ShipmentDocImgRtrvAllowance = txtImgRtrvAllowanceDays.Text.ToInt(),
                PaymentGatewayType = ddlPaymentGatewayType.SelectedValue.ToInt().ToEnum<PaymentGatewayType>(),
                PaymentGatewayLoginId = txtPaymentGatewayLoginId.Text,
                PaymentGatewaySecret = txtPaymentGatewaySecret.Text,
                PaymentGatewayTransactionId = txtPaymentGatewayTransactionId.Text,
                PaymentGatewayInTestMode = chkPaymentGatewayInTestMode.Checked
			};


			if (tenant.MailingLocation == null) tenant.MailingLocation = new TenantLocation { Tenant = tenant };
			if (tenant.BillingLocation == null) tenant.BillingLocation = new TenantLocation { Tenant = tenant };

			tenant.LoadCollections();

			tenant.BillingLocation.TakeSnapShot();
			mailingAddressInput.RetrieveAddress(tenant.MailingLocation);
			billingAddressInput.RetrieveAddress(tenant.BillingLocation);

			RetrieveContacts(tenant.MailingLocation, lstMailingContacts);
			RetrieveContacts(tenant.BillingLocation, lstBillingContacts);

			var filesLoaded = LoadFiles(tenant);

			// must do this as hidden field can be set in front end when integration enable check boxes are clicked!
			if (!hidRequireIntergrationUpdate.Value.ToBoolean()) hidRequireIntergrationUpdate.Value = filesLoaded.GetString();

			hidFlag.Value = SaveFlag;

			if (Save != null)
				Save(this, new ViewEventArgs<Tenant>(tenant));

			if (LoadAuditLog != null)
				LoadAuditLog(this, new ViewEventArgs<Tenant>(tenant));
		}

		protected void OnEditClicked(object sender, EventArgs e)
		{
			if (Lock != null)
			{
				SetEditStatus(true);
				memberToolBar.ShowUnlock = true;
				Lock(this, new ViewEventArgs<Tenant>(ActiveUser.Tenant));
			}

			LoadTenant(ActiveUser.Tenant);
		}

		protected void OnUnlockClicked(object sender, EventArgs e)
		{
			if (UnLock != null)
			{
				SetEditStatus(false);
				memberToolBar.ShowUnlock = false;
				UnLock(this, new ViewEventArgs<Tenant>(ActiveUser.Tenant));
			}
		}

		protected void OnContactDeleteClicked(object sender, ImageClickEventArgs e)
		{
			var imageButton = (ImageButton)sender;
			var listView = imageButton.Parent.FindControl("hidEditingFlag").ToCustomHiddenField().Value == MailingLocation ? lstMailingContacts : lstBillingContacts;

			var contacts = listView.Items
			                       .Select(item => new
				                       {
					                       Id = item.FindControl("hidContactId").ToCustomHiddenField().Value,
					                       Comment = item.FindControl("txtComments").ToTextBox().Text,
					                       ContactTypeId = item.FindControl("ddlContactContactTypes").ToCachedObjectDropDownList().SelectedValue.ToLong(),
					                       Email = item.FindControl("txtEmail").ToTextBox().Text.StripSpacesFromEmails(),
					                       Fax = item.FindControl("txtFax").ToTextBox().Text,
					                       Mobile = item.FindControl("txtMobile").ToTextBox().Text,
					                       Name = item.FindControl("txtName").ToTextBox().Text,
					                       Phone = item.FindControl("txtPhone").ToTextBox().Text,
					                       Primary = item.FindControl("chkPrimary").ToCheckBox().Checked,
				                       })
			                       .ToList();

			contacts.RemoveAt(imageButton.Parent.FindControl("hidContactIndex").ToCustomHiddenField().Value.ToInt());

			listView.DataSource = contacts;
			listView.DataBind();
		}

		protected void OnAddContactClicked(object sender, EventArgs e)
		{
			var button = (Button)sender;
			ListView lst;
			if (button.ID == btnAddBillingContact.ID) lst = lstBillingContacts;
			else if (button.ID == btnAddMailingContact.ID) lst = lstMailingContacts;
			else lst = new ListView();

			var contacts = lst.Items
			                  .Select(item => new
				                  {
					                  Id = item.FindControl("hidContactId").ToCustomHiddenField().Value,
					                  Comment = item.FindControl("txtComments").ToTextBox().Text,
					                  ContactTypeId = item.FindControl("ddlContactContactTypes").ToCachedObjectDropDownList().SelectedValue.ToLong(),
					                  Email = item.FindControl("txtEmail").ToTextBox().Text.StripSpacesFromEmails(),
					                  Fax = item.FindControl("txtFax").ToTextBox().Text,
					                  Mobile = item.FindControl("txtMobile").ToTextBox().Text,
					                  Name = item.FindControl("txtName").ToTextBox().Text,
					                  Phone = item.FindControl("txtPhone").ToTextBox().Text,
					                  Primary = item.FindControl("chkPrimary").ToCheckBox().Checked,
				                  })
			                  .ToList();
			contacts.Add(new
				{
					Id = default(long).GetString(),
					Comment = string.Empty,
					ContactTypeId = default(long),
					Email = string.Empty,
					Fax = string.Empty,
					Mobile = string.Empty,
					Name = string.Empty,
					Phone = string.Empty,
					Primary = false,
				});

			lst.DataSource = contacts;
			lst.DataBind();
		}


		protected void OnSmcRateWareClearClicked(object sender, EventArgs e)
		{
			hidFilesToDelete.Value += string.Format(";{0}", hidSmcRateWare.Value);
			lbtnDownloadSmcRateWare.Text = string.Empty;
			hidSmcRateWare.Value = string.Empty;
		}

		protected void OnSmcCarrierConnectClearClicked(object sender, EventArgs e)
		{
			hidFilesToDelete.Value += string.Format(";{0}", hidSmcCarrierConnect.Value);
			lbtnDownloadSmcCarrierConnect.Text = string.Empty;
			hidSmcCarrierConnect.Value = string.Empty;
		}

		protected void OnFedExSmallPackClearClicked(object sender, EventArgs e)
		{
			hidFilesToDelete.Value += string.Format(";{0}", hidFedExSmallPack.Value);
			lbtnDownloadFedExSmallPack.Text = string.Empty;
			hidFedExSmallPack.Value = string.Empty;
		}

		protected void OnUpsSmallPackClearClicked(object sender, EventArgs e)
		{
			hidFilesToDelete.Value += string.Format(";{0}", hidUpsSmallPack.Value);
			lbtnDownloadUpsSmallPack.Text = string.Empty;
			hidUpsSmallPack.Value = string.Empty;
		}

		protected void OnMacroPointClearClicked(object sender, EventArgs e)
		{
			hidFilesToDelete.Value += string.Format(";{0}", hidMacroPoint.Value);
			lbtnDownloadMacroPoint.Text = string.Empty;
			hidMacroPoint.Value = string.Empty;
		}

		protected void OnClearLogoClicked(object sender, EventArgs e)
		{
			hidFilesToDelete.Value += string.Format(";{0}", hidLogoUrl.Value);
			hidLogoUrl.Value = string.Empty;
			imgLogo.ImageUrl = string.Empty;
			imgLogo.AlternateText = string.Empty;
		}

		protected void OnClearTermsAndConditionsClicked(object sender, EventArgs e)
		{
			hidFilesToDelete.Value += string.Format(";{0}", hidTermsAndConditions.Value);
			lBtnDownloadTermsAndConditions.Text = string.Empty;
			hidTermsAndConditions.Value = string.Empty;
		}



		protected void OnDownloadSmcRatewareClicked(object sender, EventArgs e)
		{
			Response.Export(Server.ReadFromFile(hidSmcRateWare.Value), lbtnDownloadSmcRateWare.Text);
		}

		protected void OnDownloadSmcCarrierConnectClicked(object sender, EventArgs e)
		{
			Response.Export(Server.ReadFromFile(hidSmcCarrierConnect.Value), lbtnDownloadSmcCarrierConnect.Text);
		}

		protected void OnDownloadFedExSmallPackClicked(object sender, EventArgs e)
		{
			Response.Export(Server.ReadFromFile(hidFedExSmallPack.Value), lbtnDownloadFedExSmallPack.Text);
		}

		protected void OnDownloadUpsSmallPackClicked(object sender, EventArgs e)
		{
			Response.Export(Server.ReadFromFile(hidUpsSmallPack.Value), lbtnDownloadUpsSmallPack.Text);
		}

		protected void OnDownloadMacroPointClicked(object sender, EventArgs e)
		{
			Response.Export(Server.ReadFromFile(hidMacroPoint.Value), lbtnDownloadMacroPoint.Text);
		}

		protected void OnDownloadTermsAndConditionsClicked(object sender, EventArgs e)
		{
			Response.Export(Server.ReadFromFile(hidTermsAndConditions.Value), lBtnDownloadTermsAndConditions.Text);
		}



		protected void OnDefaultSystemUsernameTextChanged(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(txtDefaultSystemUsername.Text))
			{
				DisplayDefaultSystemUser(null);
				return;
			}

			if (DefaultSystemUserSearch != null)
				DefaultSystemUserSearch(this, new ViewEventArgs<string>(txtDefaultSystemUsername.Text.RetrieveSearchCodeFromAutoCompleteString()));
		}

		protected void OnFindDefaultSystemUserClicked(object sender, ImageClickEventArgs e)
		{
			userFinder.Visible = true;
		}



		protected void OnUserFinderItemSelected(object sender, ViewEventArgs<User> e)
		{
			DisplayDefaultSystemUser(e.Argument);
			userFinder.Visible = false;
		}

		protected void OnUserFinderSelectionCancelled(object sender, EventArgs e)
		{
			userFinder.Visible = false;
		}
	}
}