﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="ResetTenantUserView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Administration.ResetTenantUserView" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="false"
        ShowEdit="false" ShowDelete="false" ShowSave="false" ShowFind="false" ShowMore="false" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" ImageUrl="" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Reset Tenant User</h3>
        </div>

        <hr class="fat"  />

        <label>
            This page allows you to reset the failed password attempts for a tenant user.<br />
            &nbsp;&nbsp;
            * Checking the Reset Password checkbox will also reset the tenant users password.<br />
            &nbsp;&nbsp;
            * Checking the Enable Tenant User checkbox will also enable the tenant user if they are disabled.</label>

        <asp:Panel runat="server" ID="pnlLogin">
            <div class="rowgroup">
                <div class="row">
                    <div class="fieldgroup">
                        <label class="wlabel">Username</label>
                        <eShip:CustomTextBox runat="server" ID="txtUsername" CausesValidation="false" MaxLength="50" />
                    </div>
                </div>
                <div class="row">
                    <div class="fieldgroup">
                        <label class="wlabel">Tenant Code</label>
                        <eShip:CustomTextBox runat="server" ID="txtTenantCode" CausesValidation="false" MaxLength="50" />
                    </div>
                </div>
                <div class="row mt10">
                    <div class="fieldgroup">
                        <asp:CheckBox runat="server" ID="chkResetPassword" CausesValidation="False" CssClass="jQueryUniform"/>
                        <label>Reset Password</label>
                    </div>
                </div>
                <div class="row mt10">
                    <div class="fieldgroup">
                        <asp:CheckBox runat="server" ID="chkEnableUser" CausesValidation="False" CssClass="jQueryUniform"/>
                        <label>Enable Tenant User</label>
                    </div>
                </div>
                <div class="row mt10">
                    <div class="fieldgroup">
                        <asp:Button runat="server" ID="btnResetTenantFailedLogins" CausesValidation="False"
                                    Text="Reset" OnClick="OnResetTenantUser" />
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information"/>
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
</asp:Content>
