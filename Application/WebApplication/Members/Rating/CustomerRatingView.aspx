﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="CustomerRatingView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Rating.CustomerRatingView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.Registry" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor.Dto.Registry" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Rating" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>
<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        ShowMore="True" ShowEdit="true" ShowDelete="true" ShowSave="true" ShowFind="true"
        OnFind="OnToolbarFindClicked" OnSave="OnToolbarSaveClicked" OnDelete="OnToolbarDeleteClicked"
        OnNew="OnToolbarNewClicked" OnEdit="OnToolbarEditClicked" OnImport="OpenCustomerImportExport"
        OnExport="OpenCustomerImportExport" OnCommand="OnToolbarCommandClicked" OnUnlock="OnToolbarUnlockClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Customer Ratings
                <eShip:RecordIdentityDisplayControl runat="server" ID="ridcRecordIdentity" TargetControlId="txtCustomerName" />
            </h3>

            <div class="lockMessage">
                <asp:Literal ID="litMessage" runat="server" />
            </div>
        </div>
        <hr class="dark mb5" />
        <div class="errorMsgLit">
            <asp:Literal runat="server" ID="litErrorMessages" />
        </div>

        <script type="text/javascript">
            function ClearHidFlag() {
                jsHelper.SetEmpty('<%= hidFlag.ClientID %>');
            }
        </script>

        <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
            Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" />
        <eShip:CustomerRatingFinderControl ID="customerRatingFinder" runat="server" Visible="false"
            OpenForEditEnabled="true" OnItemSelected="OnCustomerRatingFinderItemSelected"
            OnSelectionCancel="OnCustomerRatingFinderSelectionCancelled" />
        <eShip:CustomerRatingImportExportControl ID="customerRatingImportExport" runat="server" Visible="false"
            OnClose="OnCustomerRatingImportExportClose" OnProcessingError="OnCustomerRatingImportExportProcessingError"
            OnTlSellRatesImported="OnCustomerRatingImportExportTLSellRatesImported" OnLtlSellRatesImported="OnCustomerRatingImportExportLTLSellRatesImported"
            OnSmallPackRatesImported="OnCustomerRatingImportExportSmallPackRatesImported" OnCustomerServiceMarkupsImported="OnCustomerRatingImportExportCustomerServiceMarkupImported" />
        <eShip:CustomerRatingBatchInsertRemoveControl runat="server" ID="customerBatchInsertRemove"
            Visible="False" OnClose="OnCustomerRatingBatchInsertRemoveClose" OnProcessingMessage="OnCustomerRatingBatchInsertRemoveProcessingError" />
        <eShip:CustomerFinderControl ID="customerFinder" runat="server" EnableMultiSelection="false"
            Visible="False" OpenForEditEnabled="false" OnSelectionCancel="OnCustomerFinderSelectionCancelled"
            OnItemSelected="OnCustomerFinderItemSelected" />
        <eShip:VendorFinderControl ID="vendorFinder" runat="server" EnableMultiSelection="false"
            Visible="false" OpenForEditEnabled="false" OnSelectionCancel="OnVendorFinderSelectionCancelled"
            OnItemSelected="OnVendorFinderItemSelected" />
        <eShip:CustomHiddenField runat="server" ID="hidCustomerRatingId" />
        <eShip:AjaxTabHeaderTextUpdater runat="server" ID="athtuTabUpdater" />

        <ajax:TabContainer runat="server" CssClass="ajaxCustom">
            <ajax:TabPanel runat="server" ID="tabDetails" HeaderText="Details">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlDetails">
                        <div class="rowgroup">
                            <div class="col_1_3">
                                <div class="row">
                                <div class="fieldgroup">
                                    <label class="wlabel blue">
                                        Customer
                                        <%= hidCustomerId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.Customer) 
                                                ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Customer Record'><img src={3} width='16' class='middle'/></a>", 
                                                                        ResolveUrl(CustomerView.PageAddress),
                                                                        WebApplicationConstants.TransferNumber, 
                                                                        hidCustomerId.Value.UrlTextEncrypt(),
                                                                        ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                : string.Empty %>
                                    </label>
                                    <eShip:CustomHiddenField runat="server" ID="hidCustomerId" />
                                    <eShip:CustomTextBox runat="server" ID="txtCustomerNumber" CssClass="w80" OnTextChanged="OnCustomerNumberEntered"
                                        AutoPostBack="True" />
                                    <asp:ImageButton ID="imgCustomerSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                                        CausesValidation="False" OnClick="OnCustomerSearchClicked" />
                                    <eShip:CustomTextBox runat="server" ID="txtCustomerName" CssClass="w190 disabled" ReadOnly="True" />
                                    <ajax:AutoCompleteExtender runat="server" ID="aceCustomer" TargetControlID="txtCustomerNumber" ServiceMethod="GetCustomerList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                        EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                                </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel blue">
                                            Project 44 Acccount Group</label>
                                        <asp:DropDownList runat="server" ID="ddlProject44AccountGroup" CssClass="w300" DataTextField="Text" DataValueField="Value"/>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col_2_3 no-right-border pr0">
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel blue">Reseller Addition</label>
                                        <asp:DropDownList ID="ddlResellerAddition" DataTextField="Text" DataValueField="Value" CssClass="w200" runat="server" />
                                    </div>
                                    <div class="fieldgroup_s ">
                                        <asp:CheckBox ID="chkBillReseller" runat="server" CssClass="jQueryUniform" />
                                        <label class="blue">Bill Reseller</label>
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel blue">
                                            Insurance Min ($)</label>
                                        <eShip:CustomTextBox ID="txtInsurancePurchaseFloor" runat="server" MaxLength="9" CssClass="w110" />
                                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtInsurancePurchaseFloor"
                                                                      FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel blue">
                                            Insurance Charge Code</label>
                                        <eShip:CachedObjectDropDownList runat="server" ID="ddlInsuranceChargeCode" CssClass="w170" Type="ChargeCodes" EnableChooseOne="True" DefaultValue="0" />
                                    </div>
                                    <div class="fieldgroup ">
                                        <label class="wlabel">&nbsp;</label>
                                        <asp:CheckBox ID="chkInsuranceEnabled" runat="server" CssClass="jQueryUniform" />
                                        <label class="comlabel blue">Insurance <br />Enabled</label>
                                    </div>
                                </div>
                                <div class="row">
                                    &nbsp;
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div class="rowgroup">
                            <h5>Profits</h5>
                            <table class="line">
                                <tr>
                                    <th>
                                        <h5 class="dark">Freight
                                            <span class="pr5">
                                                <asp:CheckBox ID="chkLineHaulNoProfit" runat="server" CssClass="jQueryUniform" Text=" No Profit" />
                                            </span>
                                        </h5>
                                    </th>
                                    <th>
                                        <h5 class="dark">Fuel
                                            <span class="pr5">
                                                <asp:CheckBox ID="chkFuelNoProfit" runat="server" CssClass="jQueryUniform" Text=" No Profit" />
                                            </span>
                                        </h5>
                                    </th>
                                    <th>
                                        <h5 class="dark">Accessorial
                                            <span class="pr5">
                                                <asp:CheckBox ID="chkAccessorialNoProfit" runat="server" CssClass="jQueryUniform" Text=" No Profit" />
                                            </span>
                                        </h5>
                                    </th>
                                    <th class="pr10">
                                        <h5 class="dark">Misc. Service
                                            <span class="pr5">
                                                <asp:CheckBox ID="chkServiceNoProfit" runat="server" CssClass="jQueryUniform" Text=" No Profit" />
                                            </span>
                                        </h5>
                                    </th>
                                </tr>
                                <tr>
                                    <td class="pb40">
                                        <h5>Ceiling</h5>
                                        <div class="row">
                                            <div class="fieldgroup">
                                                <label class="wlabel">Type</label>
                                                <asp:DropDownList ID="ddlLineHaulCeilingType" DataValueField="Key" DataTextField="Value"
                                                    runat="server" CssClass="w110" />
                                            </div>
                                            <div class="fieldgroup">
                                                <label class="wlabel">Value ($)</label>
                                                <eShip:CustomTextBox ID="txtLineHaulCeilingValue" runat="server" CssClass="w110" />
                                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtLineHaulCeilingValue"
                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="fieldgroup">
                                                <label class="wlabel">Percent (%)</label>
                                                <eShip:CustomTextBox ID="txtLineHaulCeilingPercentage" runat="server" CssClass="w110" />
                                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtLineHaulCeilingPercentage"
                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                            </div>
                                            <div class="fieldgroup_s">
                                                <asp:CheckBox ID="chkLineHaulUseMinimumCeiling" runat="server" CssClass="jQueryUniform" />
                                                <label>Use Lower</label>
                                            </div>
                                        </div>
                                        <h5>Floor</h5>
                                        <div class="row">
                                            <div class="fieldgroup">
                                                <label class="wlabel">Type</label>
                                                <asp:DropDownList ID="ddlLineHaulFloorType" DataValueField="Key" DataTextField="Value"
                                                    runat="server" CssClass="w110" />
                                            </div>
                                            <div class="fieldgroup">
                                                <label class="wlabel">Value ($)</label>
                                                <eShip:CustomTextBox ID="txtLineHaulFloorValue" runat="server" CssClass="w110" />
                                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" TargetControlID="txtLineHaulFloorValue"
                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="fieldgroup">
                                                <label class="wlabel">Percent (%)</label>
                                                <eShip:CustomTextBox ID="txtLineHaulFloorPercentage" runat="server" CssClass="w110" />
                                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" TargetControlID="txtLineHaulFloorPercentage"
                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                            </div>
                                            <div class="fieldgroup_s">
                                                <asp:CheckBox ID="chkLineHaulUseMinimumFloor" runat="server" CssClass="jQueryUniform" />
                                                <label>Use Lower</label>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="pb40">
                                        <h5>Ceiling</h5>
                                        <div class="row">
                                            <div class="fieldgroup">
                                                <label class="wlabel">Type</label>
                                                <asp:DropDownList ID="ddlFuelCeilingType" DataValueField="Key" DataTextField="Value"
                                                    runat="server" CssClass="w110" />
                                            </div>
                                            <div class="fieldgroup">
                                                <label class="wlabel">Value ($)</label>
                                                <eShip:CustomTextBox ID="txtFuelCeilingValue" runat="server" CssClass="w110" />
                                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtFuelCeilingValue"
                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="fieldgroup">
                                                <label class="wlabel">Percent (%)</label>
                                                <eShip:CustomTextBox ID="txtFuelCeilingPercentage" runat="server" CssClass="w110" />
                                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="txtFuelCeilingPercentage"
                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                            </div>
                                            <div class="fieldgroup_s">
                                                <asp:CheckBox ID="chkFuelUseMinimumCeiling" runat="server" CssClass="jQueryUniform" />
                                                <label>Use Lower</label>
                                            </div>
                                        </div>
                                        <h5>Floor</h5>
                                        <div class="row">
                                            <div class="fieldgroup">
                                                <label class="wlabel">Type</label>
                                                <asp:DropDownList ID="ddlFuelFloorType" DataValueField="Key" DataTextField="Value"
                                                    runat="server" CssClass="w110" />
                                            </div>
                                            <div class="fieldgroup">
                                                <label class="wlabel">Value ($)</label>
                                                <eShip:CustomTextBox ID="txtFuelFloorValue" runat="server" CssClass="w110" />
                                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" TargetControlID="txtFuelFloorValue"
                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="fieldgroup">
                                                <label class="wlabel">Percent (%)</label>
                                                <eShip:CustomTextBox ID="txtFuelFloorPercentage" runat="server" CssClass="w110" />
                                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" TargetControlID="txtFuelFloorPercentage"
                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                            </div>
                                            <div class="fieldgroup_s">
                                                <asp:CheckBox ID="chkFuelUseMinimumFloor" runat="server" CssClass="jQueryUniform" />
                                                <label>Use Lower</label>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="pb40">
                                        <h5>Ceiling</h5>
                                        <div class="row">
                                            <div class="fieldgroup">
                                                <label class="wlabel">Type</label>
                                                <asp:DropDownList ID="ddlAccessorialCeilingType" DataValueField="Key" DataTextField="Value"
                                                    runat="server" CssClass="w110" />
                                            </div>
                                            <div class="fieldgroup">
                                                <label class="wlabel">Value ($)</label>
                                                <eShip:CustomTextBox ID="txtAccessorialCeilingValue" runat="server" CssClass="w110" />
                                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtAccessorialCeilingValue"
                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="fieldgroup">
                                                <label class="wlabel">Percent (%)</label>
                                                <eShip:CustomTextBox ID="txtAccessorialCeilingPercentage" runat="server" CssClass="w110" />
                                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="txtAccessorialCeilingPercentage"
                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                            </div>
                                            <div class="fieldgroup_s">
                                                <asp:CheckBox ID="chkAccessorialUseMinimumCeiling" runat="server" CssClass="jQueryUniform" />
                                                <label>Use Lower</label>
                                            </div>
                                        </div>
                                        <h5>Floor</h5>
                                        <div class="row">
                                            <div class="fieldgroup">
                                                <label class="wlabel">Type</label>
                                                <asp:DropDownList ID="ddlAccessorialFloorType" DataValueField="Key" DataTextField="Value"
                                                    runat="server" CssClass="w110" />
                                            </div>
                                            <div class="fieldgroup">
                                                <label class="wlabel">Value ($)</label>
                                                <eShip:CustomTextBox ID="txtAccessorialFloorValue" runat="server" CssClass="w110" />
                                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" TargetControlID="txtAccessorialFloorValue"
                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="fieldgroup">
                                                <label class="wlabel">Percent (%)</label>
                                                <eShip:CustomTextBox ID="txtAccessorialFloorPercentage" runat="server" CssClass="w110" />
                                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" TargetControlID="txtAccessorialFloorPercentage"
                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                            </div>
                                            <div class="fieldgroup_s">
                                                <asp:CheckBox ID="chkAccessorialUseMinimumFloor" runat="server" CssClass="jQueryUniform" />
                                                <label>Use Lower</label>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="pr10 pb40">
                                        <h5>Ceiling</h5>
                                        <div class="row">
                                            <div class="fieldgroup">
                                                <label class="wlabel">Type</label>
                                                <asp:DropDownList ID="ddlServiceCeilingType" DataValueField="Key" DataTextField="Value"
                                                    runat="server" CssClass="w110" />
                                            </div>
                                            <div class="fieldgroup">
                                                <label class="wlabel">Value ($)</label>
                                                <eShip:CustomTextBox ID="txtServiceCeilingValue" runat="server" CssClass="w110" />
                                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtServiceCeilingValue"
                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="fieldgroup">
                                                <label class="wlabel">Percent (%)</label>
                                                <eShip:CustomTextBox ID="txtServiceCeilingPercentage" runat="server" CssClass="w110" />
                                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" TargetControlID="txtServiceCeilingPercentage"
                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                            </div>
                                            <div class="fieldgroup_s">
                                                <asp:CheckBox ID="chkServiceUseMinimumCeiling" runat="server" CssClass="jQueryUniform" />
                                                <label>Use Lower</label>
                                            </div>
                                        </div>
                                        <h5>Floor</h5>
                                        <div class="row">
                                            <div class="fieldgroup">
                                                <label class="wlabel">Type</label>
                                                <asp:DropDownList ID="ddlServiceFloorType" DataValueField="Key" DataTextField="Value"
                                                    runat="server" CssClass="w110" />
                                            </div>
                                            <div class="fieldgroup">
                                                <label class="wlabel">Value ($)</label>
                                                <eShip:CustomTextBox ID="txtServiceFloorValue" runat="server" CssClass="w110" />
                                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" TargetControlID="txtServiceFloorValue"
                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="fieldgroup">
                                                <label class="wlabel">Percent (%)</label>
                                                <eShip:CustomTextBox ID="txtServiceFloorPercentage" runat="server" CssClass="w110" />
                                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" TargetControlID="txtServiceFloorPercentage"
                                                    FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                            </div>
                                            <div class="fieldgroup_s">
                                                <asp:CheckBox ID="chkServiceUseMinimumFloor" runat="server" CssClass="jQueryUniform" />
                                                <label>Use Lower</label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabLTLSellRates" HeaderText="LTL Sell Rates">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlLTLSellRates" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlLTLSellRates" DefaultButton="btnAddLTLSellRate">
                                <div class="row mb10">
                                    <div class="fieldgroup">
                                        <p class="note"><span class="red">*</span> = Inactive vendor rating profile.</p>
                                    </div>
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddLTLSellRate" Text="Add LTL Sell Rate" CssClass="mr10"
                                            CausesValidation="false" OnClick="OnAddLtlSellRateClicked" />
                                        <asp:Button runat="server" ID="btnClearLTLSellRate" Text="Clear LTL Sell Rates" CausesValidation="false" OnClick="OnClearLtlSellRateClicked" />
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheCustomerRatingLTLSellRatesTable" TableId="customerRatingLTLSellRatesTable" HeaderZIndex="2" IgnoreHeaderBackgroundFill="True" />
                                <table class="stripe" id="customerRatingLTLSellRatesTable">
                                    <tr>
                                        <th style="width: 6%;" class="text-center">Indirect
                                        <abbr title="Point">Pt</abbr>
                                            Enabled
                                        </th>
                                        <th style="width: 9%;">Effective Date
                                        </th>
                                        <th style="width: 9%;" class="text-right">MarkUp %
                                        </th>
                                        <th style="width: 9%;" class="text-right">MarkUp
                                        <abbr title="Value">Val.</abbr>($)
                                        </th>
                                        <th style="width: 8%;" class="text-center">Use Lower
                                        </th>
                                        <th style="width: 8%;">
                                            <abbr title="Apply Override Markup from set Actual Weight">
                                                Override Markup Wgt.</abbr>
                                        </th>
                                        <th style="width: 23%;">Vendor Rating
                                        </th>
                                        <th style="width: 5%;" class="text-center">Active
                                        </th>
                                        <th style="width: 8%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstLTLSellRates" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td class="text-center">
                                                    <eShip:CustomHiddenField ID="hidLTLSellRateIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <eShip:CustomHiddenField ID="hidLTLSellRateId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <%# Eval("LTLIndirectPointEnabled").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                                                    <eShip:CustomHiddenField runat="server" ID="hidLTLSellRateIndirectPointEnabled" Value='<%# Eval("LTLIndirectPointEnabled").ToBoolean().GetString() %>' />
                                                </td>
                                                <td>
                                                    <asp:Literal ID="litLTLSellRateEffectiveDate" runat="server" Text='<%# Eval("EffectiveDate").FormattedShortDate() %>' />
                                                </td>
                                                <td class="text-right">
                                                    <asp:Literal ID="litLTLSellRateMarkUpPercent" runat="server" Text='<%# Eval("MarkUpPercent", "{0:n2}") %>' />
                                                </td>
                                                <td class="text-right">
                                                    <asp:Literal ID="litLTLSellRateMarkUpValue" runat="server" Text='<%# Eval("MarkUpValue", "{0:n2}") %>' />
                                                </td>
                                                <td class="text-center">
                                                    <%# Eval("UseMinimum").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                                                    <eShip:CustomHiddenField runat="server" ID="hidLTLSellRateUseMinimum" Value='<%# Eval("UseMinimum") %>' />
                                                </td>
                                                <td>
                                                    <%# Eval("StartOverrideWeightBreak") %>
                                                </td>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidLTLSellRateVendorRatingId" runat="server" Value='<%# Eval("VendorRatingId") %>' />
                                                    <eShip:CustomHiddenField runat="server" ID="hidVendorRatingIsActive" Value='<%# Eval("VendorRatingIsActive") %>' />

                                                    <%# ActiveUser.HasAccessTo(ViewCode.VendorRating) 
                                                            ? string.Format("{0} <a href='{1}?{2}={3}' class='blue' target='_blank' title='Go To Vendor Rating Record'><img src={4} width='16' class='middle'/></a>", 
                                                                ViewVendorRatings.GetValueText(Eval("VendorRatingId").GetString()),    
                                                                ResolveUrl(VendorRatingView.PageAddress),
                                                                WebApplicationConstants.TransferNumber, 
                                                                Eval("VendorRatingId").GetString().UrlTextEncrypt(),
                                                                ResolveUrl("~/images/icons2/arrow_newTab.png"))  
                                                            : ViewVendorRatings.GetValueText(Eval("VendorRatingId").GetString()) %>

                                                    <%# !Eval("VendorRatingIsActive").ToBoolean() ? "<span style=\"color:red;\">*</span>" : string.Empty %>
                                                </td>
                                                <td class="text-center">
                                                    <%# Eval("Active").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                                                    <eShip:CustomHiddenField runat="server" ID="hidLTLSellRateActive" Value='<%# Eval("Active") %>' />
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png"
                                                        CausesValidation="false" OnClick="OnEditLtlSellRateClicked" Enabled='<%# Access.Modify %>' OnClientClick="$(window).scrollTop(0);" />
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        CausesValidation="false" OnClick="OnDeleteLtlSellRateClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                            <tr class="hidden">
                                                <td colspan="9"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="9">
                                                    <div class="ml20">
                                                        <h6>MARKUP OVERRIDES</h6>
                                                        <div class="row pl20 mt10">
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue">Weight Break</label>
                                                                <label class="wlabel blue">Markup Percent</label>
                                                                <label class="wlabel blue">Markup Value</label>
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue w50">L5C</label>
                                                                <label class="wlabel"><%# Eval("OverrideMarkupPercentL5C") %></label>
                                                                <label class="wlabel"><%# Eval("OverrideMarkupValueL5C") %></label>
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue w50">M5C</label>
                                                                <label class="wlabel"><%# Eval("OverrideMarkupPercentM5C") %></label>
                                                                <label class="wlabel"><%# Eval("OverrideMarkupValueM5C") %></label>
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue w50">M1M</label>
                                                                <label class="wlabel"><%# Eval("OverrideMarkupPercentM1M") %></label>
                                                                <label class="wlabel"><%# Eval("OverrideMarkupValueM1M") %></label>
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue w50">M2M</label>
                                                                <label class="wlabel"><%# Eval("OverrideMarkupPercentM2M") %></label>
                                                                <label class="wlabel"><%# Eval("OverrideMarkupValueM2M") %></label>
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue w50">M5M</label>
                                                                <label class="wlabel"><%# Eval("OverrideMarkupPercentM5M") %></label>
                                                                <label class="wlabel"><%# Eval("OverrideMarkupValueM5M") %></label>
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue w50">M10M</label>
                                                                <label class="wlabel"><%# Eval("OverrideMarkupPercentM10M") %></label>
                                                                <label class="wlabel"><%# Eval("OverrideMarkupValueM10M") %></label>
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue w50">M20M</label>
                                                                <label class="wlabel"><%# Eval("OverrideMarkupPercentM20M") %></label>
                                                                <label class="wlabel"><%# Eval("OverrideMarkupValueM20M") %></label>
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue w50">M30M</label>
                                                                <label class="wlabel"><%# Eval("OverrideMarkupPercentM30M") %></label>
                                                                <label class="wlabel"><%# Eval("OverrideMarkupValueM30M") %></label>
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue w50">M40M</label>
                                                                <label class="wlabel"><%# Eval("OverrideMarkupPercentM40M") %></label>
                                                                <label class="wlabel"><%# Eval("OverrideMarkupValueM40M") %></label>
                                                            </div>
                                                            <div class="fieldgroup vlinedarkleft">
                                                                <label class="wlabel blue">Vendor Floor Override Enabled:</label>
                                                                <label class="wlabel blue">Vendor Floor MarkUp Percent:</label> 
                                                                <label class="wlabel blue">Vendor Floor MarkUp Value:</label> 
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel"> <%# Eval("OverrideMarkupVendorFloorEnabled").ToBoolean() ? WebApplicationConstants.HtmlCheck  : WebApplicationConstants.HtmlBreak %></label>
                                                                <label class="wlabel"><%# Eval("OverrideMarkupPercentVendorFloor") %></label>
                                                                <label class="wlabel"><%# Eval("OverrideMarkupValueVendorFloor") %></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                                <asp:Panel runat="server" ID="pnlEditLTLSellRate" Visible="false">
                                    <div class="popup">
                                        <div class="popheader">
                                            <h4>Add/Modify LTL Sell Rate</h4>
                                            <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                                                CausesValidation="false" OnClick="OnCloseEditLtlSellRateClicked" runat="server" />
                                        </div>
                                        <div class="row pt5 pb10 bline">
                                            <div class="col_2_3 bbox pl20">
                                                <div class="row">
                                                    <h5>Sell Rate Attributes</h5>
                                                    <table class="poptable">
                                                        <tr>
                                                            <td class="text-right">
                                                                <label class="upper">Effective Date:</label>
                                                            </td>
                                                            <td>
                                                                <eShip:CustomTextBox runat="server" ID="txtLTLSellRateEffectiveDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                                                               
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td>
                                                                 <eShip:AltUniformCheckBox ID="chkLTLSellRateActive" runat="server" />
                                                                <label>Active</label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right">
                                                                <label class="upper">Markup (%): </label>
                                                            </td>
                                                            <td>
                                                                <eShip:CustomTextBox ID="txtLTLSellRateMarkUpPercent" CssClass="w100" MaxLength="19" runat="server" Type="FloatingPointNumbers" />
                                                                
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right">
                                                                <label class="upper">MarkUp Value ($):</label>
                                                            </td>
                                                            <td>
                                                                <eShip:CustomTextBox ID="txtLTLSellRateMarkUpValue" CssClass="w100" MaxLength="19" runat="server" Type="FloatingPointNumbers" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td>
                                                                <eShip:AltUniformCheckBox ID="chkLTLSellRateUseMinimum" runat="server" />
                                                                <label>Use Lower <span class="fs85em">(* also applies to overrides)</span></label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right">
                                                                <label class="upper">Vendor Rating:</label>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlLTLSellRateVendorRating" runat="server" CssClass="w300 mr10" DataTextField="Text" DataValueField="Value" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td>
                                                                <eShip:AltUniformCheckBox ID="chkLTLSellRateIndirectPointEnabled" runat="server" />
                                                                <label>Indirect Point Enabled</label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right">
                                                                <label class="upper">Start Override Markup Weight:</label>
                                                            </td>
                                                            <td class="pb15">
                                                                <asp:DropDownList ID="ddlStartOverrideMarkupWeightBreak" DataTextField="Value" DataValueField="Key"
                                                                    CssClass="w200" runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col_1_3 bbox pl40 vlinedarkleft no-right-border">
                                                <div class="row">
                                                    <h5>Weight Break Override Markups</h5>
                                                    <table class="poptable">
                                                        <tr>
                                                            <td class="text-right">
                                                                <label class="upper blue">Weight Break</label>
                                                            </td>
                                                            <td>
                                                                <label class="upper blue">Markup Percent</label>
                                                            </td>
                                                            <td>
                                                                <label class="upper blue">Markup Value</label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right">
                                                                <label class="upper blue">L5C</label>
                                                            </td>
                                                            <td>
                                                                <eShip:CustomTextBox runat="server" ID="txtOverrideMarkupPercentL5C" Type="FloatingPointNumbers" CssClass="w90" MaxLength="9" />
                                                            </td>
                                                            <td>
                                                                <eShip:CustomTextBox runat="server" ID="txtOverrideMarkupValueL5C" Type="FloatingPointNumbers" CssClass="w90" MaxLength="9" />
                                                            </td>
                                                        </tr>
                                                        
                                                         <tr>
                                                            <td class="text-right">
                                                                <label class="upper blue">M5C</label>
                                                            </td>
                                                            <td>
                                                                <eShip:CustomTextBox runat="server" ID="txtOverrideMarkupPercentM5C" Type="FloatingPointNumbers" CssClass="w90" MaxLength="9" />
                                                            </td>
                                                            <td>
                                                                <eShip:CustomTextBox runat="server" ID="txtOverrideMarkupValueM5C" Type="FloatingPointNumbers" CssClass="w90" MaxLength="9" />
                                                            </td>
                                                        </tr>
                                                        
                                                         <tr>
                                                            <td class="text-right">
                                                                <label class="upper blue">M1M</label>
                                                            </td>
                                                            <td>
                                                                <eShip:CustomTextBox runat="server" ID="txtOverrideMarkupPercentM1M" Type="FloatingPointNumbers" CssClass="w90" MaxLength="9" />
                                                            </td>
                                                            <td>
                                                                <eShip:CustomTextBox runat="server" ID="txtOverrideMarkupValueM1M" Type="FloatingPointNumbers" CssClass="w90" MaxLength="9" />
                                                            </td>
                                                        </tr>
                                                        
                                                         <tr>
                                                            <td class="text-right">
                                                                <label class="upper blue">M2M</label>
                                                            </td>
                                                            <td>
                                                                <eShip:CustomTextBox runat="server" ID="txtOverrideMarkupPercentM2M" Type="FloatingPointNumbers" CssClass="w90" MaxLength="9" />
                                                            </td>
                                                            <td>
                                                                <eShip:CustomTextBox runat="server" ID="txtOverrideMarkupValueM2M" Type="FloatingPointNumbers" CssClass="w90" MaxLength="9" />
                                                            </td>
                                                        </tr>
                                                        
                                                         <tr>
                                                            <td class="text-right">
                                                                <label class="upper blue">M5M</label>
                                                            </td>
                                                            <td>
                                                                <eShip:CustomTextBox runat="server" ID="txtOverrideMarkupPercentM5M" Type="FloatingPointNumbers" CssClass="w90" MaxLength="9" />
                                                            </td>
                                                            <td>
                                                                <eShip:CustomTextBox runat="server" ID="txtOverrideMarkupValueM5M" Type="FloatingPointNumbers" CssClass="w90" MaxLength="9" />
                                                            </td>
                                                        </tr>
                                                        
                                                         <tr>
                                                            <td class="text-right">
                                                                <label class="upper blue">M10M</label>
                                                            </td>
                                                            <td>
                                                                <eShip:CustomTextBox runat="server" ID="txtOverrideMarkupPercentM10M" Type="FloatingPointNumbers" CssClass="w90" MaxLength="9" />
                                                            </td>
                                                            <td>
                                                                <eShip:CustomTextBox runat="server" ID="txtOverrideMarkupValueM10M" Type="FloatingPointNumbers" CssClass="w90" MaxLength="9" />
                                                            </td>
                                                        </tr>
                                                        
                                                         
                                                         <tr>
                                                            <td class="text-right">
                                                                <label class="upper blue">M20M</label>
                                                            </td>
                                                            <td>
                                                                <eShip:CustomTextBox runat="server" ID="txtOverrideMarkupPercentM20M" Type="FloatingPointNumbers" CssClass="w90" MaxLength="9" />
                                                            </td>
                                                            <td>
                                                                <eShip:CustomTextBox runat="server" ID="txtOverrideMarkupValueM20M" Type="FloatingPointNumbers" CssClass="w90" MaxLength="9" />
                                                            </td>
                                                        </tr>
                                                        
                                                        <tr>
                                                            <td class="text-right">
                                                                <label class="upper blue">M30M</label>
                                                            </td>
                                                            <td>
                                                                <eShip:CustomTextBox runat="server" ID="txtOverrideMarkupPercentM30M" Type="FloatingPointNumbers" CssClass="w90" MaxLength="9" />
                                                            </td>
                                                            <td>
                                                                <eShip:CustomTextBox runat="server" ID="txtOverrideMarkupValueM30M" Type="FloatingPointNumbers" CssClass="w90" MaxLength="9" />
                                                            </td>
                                                        </tr>
                                                        
                                                         <tr>
                                                            <td class="text-right">
                                                                <label class="upper blue">M40M</label>
                                                            </td>
                                                            <td>
                                                                <eShip:CustomTextBox runat="server" ID="txtOverrideMarkupPercentM40M" Type="FloatingPointNumbers" CssClass="w90" MaxLength="9" />
                                                            </td>
                                                            <td>
                                                                <eShip:CustomTextBox runat="server" ID="txtOverrideMarkupValueM40M" Type="FloatingPointNumbers" CssClass="w90" MaxLength="9" />
                                                            </td>
                                                        </tr>

                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row pt10 pb10">
                                            <div class="bbox pl20 bottom_shadow ">
                                                <div class="row pb20">
                                                    <h5>Vendor Floor Override Markup</h5>
                                                    <div class="row pl10">
                                                        <div class="col_1_3 bbox no-right-border">
                                                            <div class="fieldgroup">
                                                                <label class="upper blue">Vendor Floor Markup (%):</label>
                                                                <eShip:CustomTextBox ID="txtOverrideMarkupVendorFloorPercent" CssClass="w100" MaxLength="18" runat="server" Type="FloatingPointNumbers" />
                                                            </div>
                                                        </div>
                                                        <div class="col_1_3 bbox no-right-border">
                                                            <div class="fieldgroup">
                                                                <label class="upper blue">Vendor Floor Markup Value ($):</label>
                                                                <eShip:CustomTextBox ID="txtOverrideMarkupVendorFloorValue" CssClass="w100" MaxLength="18" runat="server" Type="FloatingPointNumbers" />
                                                            </div>
                                                        </div>
                                                        <div class="col_1_3 bbox no-right-border">
                                                            <div class="fieldgroup">
                                                                <eShip:AltUniformCheckBox ID="chkVendorFloorEnabled" runat="server" />
                                                                <label class="blue">Vendor Floor Override Enabled</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row pb10">
                                            <table>
                                                <tr>
                                                    <td></td>
                                                    <td class="pt20">
                                                        <asp:Button ID="btnEditLTLSellRateDone" Text="Done" OnClick="OnEditLtlSellRateDoneClicked" runat="server" CausesValidation="false" />
                                                        <asp:Button ID="btnEditLTLSellRateCancel" Text="Cancel" OnClick="OnCloseEditLtlSellRateClicked" runat="server" CausesValidation="false" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                            
                                    </div>
                                </asp:Panel>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabTLSellRates" HeaderText="TL Sell Rates">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlTLSellRates" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlTLSellRates" DefaultButton="btnAddTLSellRate">
                                <div class="rowgroup mb10">
                                    <div class="row">
                                        <div class="fieldgroup mr20">
                                            <label class="wlabel blue">TL Fuel Rate</label>
                                            <eShip:CustomTextBox runat="server" ID="txtTLFuelRate" CssClass="w200" MaxLength="50" Type="FloatingPointNumbers" />
                                        </div>
                                        <div class="fieldgroup mr20">
                                            <label class="wlabel blue">TL Fuel Charge Code</label>
                                            <eShip:CachedObjectDropDownList runat="server" ID="ddlTLFuelChargeCode" CssClass="w200" Type="ChargeCodes" EnableChooseOne="True" DefaultValue="0" />
                                        </div>
                                        <div class="fieldgroup mr20">
                                            <label class="wlabel blue">TL Fuel Rate Type</label>
                                            <asp:DropDownList ID="ddlTLFuelRateType" runat="server" CssClass="w200" DataTextField="Text" DataValueField="Value" />
                                        </div>
                                        <div class="fieldgroup">
                                            <label class="wlabel blue">TL Freight Charge Code</label>
                                            <eShip:CachedObjectDropDownList runat="server" ID="ddlTLFreightChargeCode" CssClass="w200" Type="ChargeCodes" EnableChooseOne="True" DefaultValue="0" />
                                        </div>
                                    </div>
                                    <h5>TL Sell Rates</h5>
                                    <div class="row">
                                        <div class="fieldgroup">
                                            <eShip:PaginationExtender runat="server" ID="peLstTLSellRates" TargetControlId="lstTLSellRates" PageSize="50" UseParentDataStore="True" />
                                        </div>
                                        <div class="fieldgroup right">
                                            <asp:Button runat="server" ID="btnAddTLSellRate" Text="Add TL Sell Rate" CssClass="mr10" CausesValidation="false" OnClick="OnAddTlSellRateClicked" />
                                            <asp:Button runat="server" ID="btnClearTLSellRates" Text="Clear TL Sell Rates" CausesValidation="false" OnClick="OnClearTlSellRateClicked" />
                                        </div>
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheCustomerRatingTLSellRatesTable" TableId="customerRatingTLSellRatesTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table class="stripe" id="customerRatingTLSellRatesTable">
                                    <tr>
                                        <th style="width: 2%">Index
                                        </th>
                                        <th style="width: 10%;">Effective Date 
                                        </th>
                                        <th style="width: 10%;">Origin City 
                                        </th>
                                        <th style="width: 10%;">Origin State 
                                        </th>
                                        <th style="width: 10%;">Origin Country 
                                        </th>
                                        <th style="width: 10%;">Origin Postal Code 
                                        </th>
                                        <th style="width: 10%;">
                                            <abbr title="Destination">Dest.</abbr>
                                            City 
                                        </th>
                                        <th style="width: 10%;">
                                            <abbr title="Destination">Dest.</abbr>
                                            State 
                                        </th>
                                        <th style="width: 10%;">
                                            <abbr title="Destination">Dest.</abbr>
                                            Country 
                                        </th>
                                        <th style="width: 10%;">
                                            <abbr title="Destination">Dest.</abbr>
                                            Postal Code 
                                        </th>
                                        <th style="width: 8%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstTLSellRates" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td class="top" rowspan="2">
                                                    <%# string.Format("{0}. ", (Container.DataItemIndex+1) + (peLstTLSellRates.CurrentPage-1) * peLstTLSellRates.PageSize) %>
                                                </td>
                                                <td class="top">
                                                    <eShip:CustomHiddenField ID="hidTLSellRateIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <eShip:CustomHiddenField ID="hidTLSellRateId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <%# Eval("EffectiveDate").FormattedShortDate() %>
                                                </td>
                                                <td class="top">
                                                    <%# Eval("OriginCity") %>
                                                </td>
                                                <td class="top">
                                                    <%# Eval("OriginState") %>
                                                </td>
                                                <td class="top">
                                                    <%# new Country(Eval("OriginCountryId").ToLong()).Name %>
                                                </td>
                                                <td class="top">
                                                    <%# Eval("OriginPostalCode") %>
                                                </td>
                                                <td class="top">
                                                    <%# Eval("DestinationCity") %>
                                                </td>
                                                <td class="top">
                                                    <%# Eval("DestinationState") %>
                                                </td>
                                                <td class="top">
                                                    <%# new Country(Eval("OriginCountryId").ToLong()).Name %>
                                                </td>
                                                <td class="top">
                                                    <%# Eval("DestinationPostalCode") %>
                                                </td>
                                                <td class="text-center top" rowspan="2">
                                                    <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png"
                                                        CausesValidation="false" OnClick="OnEditTlSellRateClicked" Enabled='<%# Access.Modify %>' OnClientClick="$(window).scrollTop(0);" />
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        CausesValidation="false" OnClick="OnDeleteTLSellRateClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                            <tr class="hidden">
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr class="bottom_shadow">
                                                <td colspan="10" class="p_m0">
                                                    <div class="rowgroup">
                                                        <div class="col_1_3">
                                                            <div class="row">
                                                                <div class="fieldgroup">
                                                                    <label class="wlabel blue w180">
                                                                        Minimum Weight
                                                                    </label>
                                                                    <%# Eval("MinimumWeight", "{0:n2}") %>
                                                                </div>
                                                                <div class="fieldgroup">
                                                                    <label class="wlabel blue">
                                                                        Maximum Weight
                                                                    </label>
                                                                    <%# Eval("MaximumWeight", "{0:n2}") %>
                                                                </div>
                                                            </div>
                                                            <div class="row mt5">
                                                                <div class="fieldgroup">
                                                                    <label class="wlabel blue">Tariff Type</label>
                                                                    <%# Eval("TariffType").FormattedString() %>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col_1_3">
                                                            <div class="row">
                                                                <div class="fieldgroup">
                                                                    <label class="wlabel blue w180">Rate</label>
                                                                    <%# Eval("Rate", "{0:n2}") %>
                                                                </div>
                                                                <div class="fieldgroup">
                                                                    <label class="wlabel blue">Rate Type</label>
                                                                    <%# Eval("RateType").FormattedString() %>
                                                                </div>
                                                            </div>
                                                            <div class="row mt5">
                                                                <div class="fieldgroup">
                                                                    <label class="wlabel blue w180">Minimum Charge</label>
                                                                    <%# Eval("MinimumCharge", "{0:n2}") %>
                                                                </div>
                                                                <div class="fieldgroup">
                                                                    <label class="wlabel blue">Mileage</label>
                                                                    <%# Eval("Mileage", "{0:n2}") %>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col_1_3">
                                                            <div class="row">
                                                                <div class="fieldgroup">
                                                                    <label class="wlabel blue">
                                                                        Equipment Type
                                                                    </label>
                                                                    <%#new EquipmentType(Eval("EquipmentTypeId").ToLong()).FormattedString() %>
                                                                </div>
                                                            </div>
                                                            <div class="row mt5">
                                                                <div class="fieldgroup">
                                                                    <label class="wlabel blue">
                                                                        Mileage Source
                                                                    </label>
                                                                    <%# new MileageSource(Eval("MileageSourceId").ToLong()).FormattedString() %>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                                <asp:Panel runat="server" ID="pnlEditTLSellRate" Visible="false" CssClass="popup" DefaultButton="btnEditTLSellRateDone">
                                    <div class="popheader">
                                        <h4>Add/Modify TL Sell Rate</h4>
                                        <asp:ImageButton ID="ImageButton2" ImageUrl="~/images/icons2/close.png" CssClass="close"
                                            CausesValidation="false" OnClick="OnCloseEditTLSellRateClicked" runat="server" />
                                    </div>
                                    <div class="row mt20">
                                        <div class="col_1_2 bbox pl20">
                                            <div class="row">
                                                <h5 class="pl40">Location Information</h5>
                                                <table class="poptable">
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">Origin City:</label>
                                                        </td>
                                                        <td>
                                                            <eShip:CustomTextBox ID="txtTLSellRateOriginCity" CssClass="w200" MaxLength="50" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">Origin State:</label>
                                                        </td>
                                                        <td>
                                                            <eShip:CustomTextBox ID="txtTLSellRateOriginState" CssClass="w200" MaxLength="50" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">Origin Country:</label>
                                                        </td>
                                                        <td>
                                                            <eShip:CachedObjectDropDownList runat="server" ID="ddlTLSellRateOriginCountry" CssClass="w200" Type="Countries" EnableChooseOne="True" DefaultValue="0" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">Origin Postal Code:</label>
                                                        </td>
                                                        <td>
                                                            <eShip:CustomTextBox ID="txtTLSellRateOriginPostalCode" CssClass="w200" MaxLength="50" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">Destination City:</label>
                                                        </td>
                                                        <td>
                                                            <eShip:CustomTextBox ID="txtTLSellRateDestinationCity" CssClass="w200" MaxLength="50" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">Destination State:</label>
                                                        </td>
                                                        <td>
                                                            <eShip:CustomTextBox ID="txtTLSellRateDestinationState" CssClass="w200" MaxLength="50" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">Destination Country:</label>
                                                        </td>
                                                        <td>
                                                            <eShip:CachedObjectDropDownList ID="ddlTLSellRateDestinationCountry" runat="server" CssClass="w200" Type="Countries" EnableChooseOne="True" DefaultValue="0" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">Destination Postal Code:</label>
                                                        </td>
                                                        <td>
                                                            <eShip:CustomTextBox ID="txtTLSellRateDestinationPostalCode" CssClass="w200" MaxLength="50" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col_1_2 vlinedarkleft bbox pl40">
                                            <div class="row">
                                                <h5 class="pl20">Sell Rate Settings</h5>
                                                <table class="poptable">
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">Effective Date:</label>
                                                        </td>
                                                        <td>
                                                            <eShip:CustomTextBox runat="server" ID="txtTLSellRateEffectiveDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">Tariff Type:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlTLSellRateTariffType" runat="server" CssClass="w200" DataTextField="Text" DataValueField="Value" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">Minimum Weight:</label>
                                                        </td>
                                                        <td>
                                                            <eShip:CustomTextBox ID="txtTLSellRateMinimumWeight" CssClass="w200" MaxLength="50" runat="server" Type="FloatingPointNumbers" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">Maximum Weight:</label>
                                                        </td>
                                                        <td>
                                                            <eShip:CustomTextBox ID="txtTLSellRateMaximumWeight" CssClass="w200" MaxLength="50" runat="server" Type="FloatingPointNumbers" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">Minimum Charge:</label>
                                                        </td>
                                                        <td>
                                                            <eShip:CustomTextBox ID="txtTLSellRateMinimumCharge" CssClass="w200" MaxLength="50" runat="server" Type="FloatingPointNumbers" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">Rate:</label>
                                                        </td>
                                                        <td>
                                                            <eShip:CustomTextBox ID="txtTLSellRateRate" CssClass="w200" MaxLength="50" runat="server" Type="FloatingPointNumbers" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">Rate Type:</label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlTLSellRateRateType" runat="server" CssClass="w200" DataTextField="Text" DataValueField="Value" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">Equipment Type:</label>
                                                        </td>
                                                        <td>
                                                            <eShip:CachedObjectDropDownList runat="server" ID="ddlTLSellRateEquipmentType" CssClass="w200" Type="EquipmentType" EnableChooseOne="True" DefaultValue="0" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right">
                                                            <label class="upper">Mileage:</label>
                                                        </td>
                                                        <td>
                                                            <eShip:CustomTextBox runat="server" ID="txtTLSellRateMileage" CssClass="w200" MaxLength="50" Type="FloatingPointNumbers" />
                                                        </td>
                                                    </tr>
                                                    <tr class="bottom_shadow">
                                                        <td class="text-right">
                                                            <label class="upper">Mileage Source:</label>
                                                        </td>
                                                        <td>
                                                            <eShip:CachedObjectDropDownList runat="server" ID="ddlTLSellRateMileageSource" CssClass="w200" Type="MileageSources" EnableNotApplicableLong="True" DefaultValue="0" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="pt10">
                                                            <asp:Button ID="btnEditTLSellRateDone" Text="Done" OnClick="OnEditTLSellRateDoneClicked" CausesValidation="False" runat="server" />
                                                            <asp:Button ID="btnCloseEditTLSellRate" Text="Cancel" OnClick="OnCloseEditTLSellRateClicked" runat="server" CausesValidation="false" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabSmallPackRates" HeaderText="Small Pack Rates">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlSmallPackRates" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlSmallPackRates" DefaultButton="btnAddSmallPackRate">
                                <div class="row mb10">
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddSmallPackRate" Text="Add Small Pack Rate" CssClass="mr10" CausesValidation="false" OnClick="OnAddSmallPackRateClicked" />
                                        <asp:Button runat="server" ID="btnClearSmallPackRate" Text="Clear Small Pack Rates" CausesValidation="false" OnClick="OnClearSmallPackRateClicked" />
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheCustomerRatingSmallPackRatesTable" TableId="customerRatingSmallPackRatesTable" HeaderZIndex="2" IgnoreHeaderBackgroundFill="True" />
                                <table class="stripe" id="customerRatingSmallPackRatesTable">
                                    <tr>
                                        <th style="width: 2%">Index
                                        </th>
                                        <th style="width: 27%;">Charge Code
                                        </th>
                                        <th style="width: 8%;" class="text-right">MarkUp %
                                        </th>
                                        <th style="width: 8%;" class="text-right">MarkUp
                                            <abbr title="Value">Val.</abbr>($)
                                        </th>
                                        <th style="width: 5%;" class="text-center">Use Lower
                                        </th>
                                        <th style="width: 20%;">Vendor
                                        </th>
                                        <th style="width: 10%;">Effective
                                        </th>
                                        <th style="width: 5%;" class="text-center">Active
                                        </th>
                                        <th style="width: 8%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstSmallPackRates" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td rowspan="2" class="top">
                                                    <eShip:CustomHiddenField ID="hidSmallPackRateIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <%# string.Format("{0}. ", Container.DataItemIndex+1) %>
                                                </td>
                                                <td>
                                                    <%# new ChargeCode(Eval("ChargeCodeId").ToLong()).FormattedString() %>
                                                </td>
                                                <td class="text-right">
                                                    <%# Eval("MarkUpPercent", "{0:n2}") %>
                                                </td>
                                                <td class="text-right">
                                                    <%# Eval("MarkUpValue", "{0:n2}") %>
                                                </td>
                                                <td class="text-center">
                                                    <%# Eval("UseMinimum").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                                                </td>
                                                <td>
                                                    <%# ActiveUser.HasAccessTo(ViewCode.Vendor) 
                                                            ? string.Format("{0} - {1}<a href='{2}?{3}={4}' class='blue' target='_blank' title='Go To Vendor Record'><img src={5} width='20' class='middle'/></a>", 
                                                                Eval("VendorNumber"),
                                                                Eval("VendorName"),  
                                                                ResolveUrl(VendorView.PageAddress),
                                                                WebApplicationConstants.TransferNumber, 
                                                                Eval("VendorId").GetString().UrlTextEncrypt(),
                                                                ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                            : Eval("VendorId").ToLong() != default(long) 
                                                                ? string.Format("{0} - {1}", Eval("VendorNumber"), Eval("VendorName"))
                                                                : string.Empty %>
                                                </td>
                                                <td>
                                                    <%# Eval("EffectiveDate").FormattedShortDate() %>
                                                </td>
                                                <td class="text-center">
                                                    <%# Eval("Active").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                                                </td>
                                                <td rowspan="2" class="text-center top">
                                                    <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png" OnClientClick="$(window).scrollTop(0);"
                                                        CausesValidation="false" OnClick="OnEditSmallPackRateClicked" Enabled='<%# Access.Modify %>' />
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        CausesValidation="false" OnClick="OnDeleteSmallPackRateClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                            <tr class="hidden">
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr class="bottom_shadow">
                                                <td colspan="7" class="forceLeftBorder pt0">
                                                    <div class="col_1_2 p_m0">
                                                        <div class="row">
                                                            <div class="fieldgroup mr20">
                                                                <label class="wlabel blue w130">Small Pack Engine</label>
                                                                <%# Eval("SmallPackageEngine").FormattedString() %>
                                                            </div>
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue">Small Package Type</label>
                                                                <%# Eval("SmallPackType") %>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                                <asp:Panel runat="server" ID="pnlEditSmallPackRate" Visible="false">
                                    <div class="popup popupControlOverW500">
                                        <div class="popheader">
                                            <h4>Add/Modify Small Pack Rate</h4>
                                            <asp:ImageButton ImageUrl="~/images/icons2/close.png" CssClass="close" CausesValidation="false" OnClick="OnCloseEditLtlSellRateClicked" runat="server" />
                                        </div>
                                        <div class="row pt10">
                                            <table class="poptable">
                                                <tr>
                                                    <td class="text-right">
                                                        <label class="upper">Vendor: </label>
                                                    </td>
                                                    <td>
                                                        <eShip:CustomHiddenField runat="server" ID="hidSmallPackRateVendorId" />
                                                        <eShip:CustomTextBox runat="server" ID="txtSmallPackRateVendorNumber" CssClass="w80" OnTextChanged="OnSmallPackRateVendorNumberEntered"
                                                            AutoPostBack="True" />
                                                        <asp:ImageButton ID="ibtnOpenVendorFinder" runat="server" ImageUrl="~/images/icons2/search_dark.png" CausesValidation="False"
                                                            OnClick="OnSmallPackRateVendorSearchClicked" />
                                                        <eShip:CustomTextBox runat="server" ID="txtSmallPackRateVendorName" CssClass="w230 disabled" ReadOnly="True" />
                                                        <ajax:AutoCompleteExtender runat="server" ID="aceVendor" TargetControlID="txtSmallPackRateVendorNumber" ServiceMethod="GetVendorList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                                            EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right">
                                                        <label class="upper">Small Pack Engine:</label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlSmallPackRateSmallPackEngine" CssClass="w200" DataTextField="Text" DataValueField="Value"
                                                            runat="server" AutoPostBack="true" OnSelectedIndexChanged="OnSmallPackRateSmallPackEngineSelectedIndexChanged" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right">
                                                        <label class="upper">Small Pack Type:</label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlSmallPackRateSmallPackType" CssClass="w200" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right">
                                                        <label class="upper">Effective Date:</label>
                                                    </td>
                                                    <td>
                                                        <eShip:CustomTextBox runat="server" ID="txtSmallPackRateEffectiveDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                                                        <eShip:AltUniformCheckBox ID="chkSmallPackRateActive" runat="server" />
                                                        <label class="upper">Active</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right">
                                                        <label class="upper">MarkUp (%):</label>
                                                    </td>
                                                    <td>
                                                        <eShip:CustomTextBox ID="txtSmallPackRateMarkUpPercent" CssClass="w100" MaxLength="19" runat="server" Type="FloatingPointNumbers" />
                                                        <eShip:AltUniformCheckBox ID="chkSmallPackRateUseMinimum" runat="server" />
                                                        <label class="upper">Use Lower</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right">
                                                        <label class="upper">MarkUp Value ($):</label>
                                                    </td>
                                                    <td>
                                                        <eShip:CustomTextBox ID="txtSmallPackRateMarkUpValue" CssClass="w100" MaxLength="9" runat="server" Type="FloatingPointNumbers" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right">
                                                        <label class="upper">Charge Code:</label>
                                                    </td>
                                                    <td>
                                                        <eShip:CachedObjectDropDownList runat="server" ID="ddlSmallPackRateChargeCode" CssClass="w200" Type="ChargeCodes" EnableChooseOne="True" DefaultValue="0" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <asp:Button ID="btnEditSmallPackRateDone" Text="Done" OnClick="OnEditSmallPackRateDoneClicked" runat="server" CausesValidation="false" />
                                                        <asp:Button ID="btnDoneSmallPackRateClose" Text="Cancel" OnClick="OnCloseEditSmallPackRateClicked" runat="server" CausesValidation="false" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="ibtnOpenVendorFinder" />
                        </Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabCustomerServiceMarkUps" HeaderText="Customer Service MarkUp">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlCustomerServiceMarkups" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlCustomerServiceMarkUps" DefaultButton="btnAddCustomerServiceMarkUp">
                                <div class="row mb10">
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddCustomerServiceMarkUp" Text="Add Customer Service MarkUp" CssClass="mr10" CausesValidation="false" OnClick="OnAddCustomerServiceMarkUpClicked" />
                                        <asp:Button runat="server" ID="btnClearCustomerServiceMarkUps" Text="Clear Customer Service MarkUps" CausesValidation="false" OnClick="OnClearCustomerServiceMarkUpClicked" />
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfhe" TableId="customerRatingCustomerServiceMarkupsTable" HeaderZIndex="2" IgnoreHeaderBackgroundFill="True" />
                                <table class="stripe" id="customerRatingCustomerServiceMarkupsTable">
                                    <tr>
                                        <th style="width: 36%;">Service
                                        </th>
                                        <th style="width: 9%;" class="text-right">MarkUp %
                                        </th>
                                        <th style="width: 9%;" class="text-right">MarkUp
                                        <abbr title="Value">Val.</abbr>($)
                                        </th>
                                        <th style="width: 5%;" class="text-center">Use Lower
                                        </th>
                                        <th style="width: 9%;" class="text-right">Charge Ceiling ($)
                                        </th>
                                        <th style="width: 9%;" class="text-right">Charge Floor ($)
                                        </th>
                                        <th style="width: 9%;">Effective
                                        </th>
                                        <th style="width: 5%;" class="text-center">Active
                                        </th>
                                        <th style="width: 8%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstCustomerServiceMarkUps" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidCustomerServiceMarkUpIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <%# new ServiceViewSearchDto(Eval("ServiceId").ToLong()).FormattedString() %>
                                                </td>
                                                <td class="text-right">
                                                    <%# Eval("MarkUpPercent", "{0:n2}") %>
                                                </td>
                                                <td class="text-right">
                                                    <%# Eval("MarkUpValue", "{0:n2}") %>
                                                </td>
                                                <td class="text-center">
                                                    <%# Eval("UseMinimum").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                                                </td>
                                                <td class="text-right">
                                                    <%# Eval("ServiceChargeCeiling", "{0:n2}") %>
                                                </td>
                                                <td class="text-right">
                                                    <%# Eval("ServiceChargeFloor", "{0:n2}") %>
                                                </td>
                                                <td>
                                                    <%# Eval("EffectiveDate").FormattedShortDate() %>
                                                </td>
                                                <td class="text-center">
                                                    <%# Eval("Active").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png" OnClientClick="$(window).scrollTop(0);"
                                                        CausesValidation="false" OnClick="OnEditCustomerServiceMarkUpClicked" Enabled='<%# Access.Modify %>' />
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        CausesValidation="false" OnClick="OnDeleteCustomerServiceMarkUpClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                                <asp:Panel runat="server" ID="pnlEditCustomerServiceMarkUp" Visible="false">
                                    <div class="popup popupControlOverW500">
                                        <div class="popheader">
                                            <h4>Add/Modify Customer Service MarkUp</h4>
                                            <asp:ImageButton ImageUrl="~/images/icons2/close.png" CssClass="close" CausesValidation="false" OnClick="OnCloseEditLtlSellRateClicked" runat="server" />
                                        </div>
                                        <div class="row pt10">
                                            <table class="poptable">
                                                <tr>
                                                    <td class="text-right">
                                                        <label class="upper">Effective Date:</label>
                                                    </td>
                                                    <td>
                                                        <eShip:CustomTextBox runat="server" ID="txtCustomerServiceMarkUpEffectiveDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                                                        <eShip:AltUniformCheckBox ID="chkCustomerServiceMarkUpActive" runat="server" />
                                                        <label class="upper">Active</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right">
                                                        <label class="upper">MarkUp (%):</label>
                                                    </td>
                                                    <td>
                                                        <eShip:CustomTextBox ID="txtCustomerServiceMarkUpMarkUpPercent" CssClass="w100" MaxLength="19" runat="server" Type="FloatingPointNumbers" />
                                                        <eShip:AltUniformCheckBox ID="chkCustomerServiceMarkUpUseMinimum" runat="server" />
                                                        <label class="upper">Use Lower</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right">
                                                        <label class="upper">MarkUp Value ($):</label>
                                                    </td>
                                                    <td>
                                                        <eShip:CustomTextBox ID="txtCustomerServiceMarkUpMarkUpValue" CssClass="w100" MaxLength="9" runat="server" Type="FloatingPointNumbers" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right">
                                                        <label class="upper">Service:</label>
                                                    </td>
                                                    <td>
                                                        <eShip:CachedObjectDropDownList runat="server" ID="ddlCustomerServiceMarkUpService" CssClass="w200" Type="Services" EnableChooseOne="True" DefaultValue="0" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right">
                                                        <label class="upper">Service Charge Ceiling ($):</label>
                                                    </td>
                                                    <td>
                                                        <eShip:CustomTextBox ID="txtCustomerServiceMarkUpServiceCeiling" CssClass="w200" MaxLength="19" runat="server" Type="FloatingPointNumbers" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right">
                                                        <label class="upper">Service Charge Floor ($):</label>
                                                    </td>
                                                    <td>
                                                        <eShip:CustomTextBox ID="txtCustomerServiceMarkUpServiceFloor" CssClass="w200" MaxLength="9" runat="server" Type="FloatingPointNumbers" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <asp:Button ID="btnEditCustomerServiceMarkUpDone" Text="Done" OnClick="OnEditCustomerServiceMarkUpDoneClicked"
                                                            runat="server" CausesValidation="false" />
                                                        <asp:Button ID="btnDoneCustomerServiceMarkUpClose" Text="Cancel" OnClick="OnCloseEditCustomerServiceMarkUpClicked"
                                                            runat="server" CausesValidation="false" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabAuditLog" HeaderText="Audit Log">
                <ContentTemplate>
                    <eShip:AuditLogControl runat="server" ID="auditLogs" />
                </ContentTemplate>
            </ajax:TabPanel>
        </ajax:TabContainer>
    </div>
    <asp:UpdatePanel runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
            <eShip:CustomHiddenField runat="server" ID="hidFlag" />
            <eShip:CustomHiddenField runat="server" ID="hidEditingIndex" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
