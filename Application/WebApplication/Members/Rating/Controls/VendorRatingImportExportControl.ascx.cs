﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Rating.Controls
{
    /*
	 *	Expected: 
	 *		1. Parent page must inherit from MemberPageBaseWithPageStore to use import export functions!
	 *		2. Application StoreKey's must also be set!
	 * 
	 */
    public partial class VendorRatingImportExportControl : MemberControlBase
    {
        private const string DiscountTierImportCompleteMsg = "LTL Discount Tier Import complete";
        private const string AccessorialsImportCompleteMsg = "LTL Accessorials Import complete";
        private const string AdditionalChargesImportCompleteMsg = "LTL Additional Charges Import complete";
        private const string CFCRulesImportCompleteMsg = "LTL C.F. Capacity Rules Import complete";
        private const string OverlengthRulesImportCompleteMsg = "LTL Overlength Rules Import complete";
        private const string PackageSpecficRatesImportCompleteMsg = "LTL Package Specific Rates Import complete";
        private const string GuaranteedChargesImportCompleteMsg = "LTL Guaranteed Charges Import Complete";

        private MemberPageBaseWithPageStore _parentPage;

        public bool EnableLTLPackageSpecificRatesImport
        {
            get { return chkLTLPackageSpecificRates.Enabled; }
            set
            {
                chkLTLPackageSpecificRates.Enabled = value;
                if (value == false) chkLTLPackageSpecificRates.Checked = false;
            }
        }
	    public bool EnableLTLDiscountTierImport
	    {
			get { return chkDiscountTiers.Enabled; }
		    set
		    {
			    chkDiscountTiers.Enabled = value;
			    if (value == false) chkDiscountTiers.Checked = false;
		    }
	    }
	    public bool EnableLTLCFCRulesImport
	    {
		    get { return chkCFCRules.Enabled; }
		    set
		    {
			    chkCFCRules.Enabled = value;
			    if (value == false) chkCFCRules.Checked = false;
		    }
	    }

	    public bool EnableImport
        {
            set
            {
                btnImport.Enabled = value;
                fupFile.Enabled = value;
                chkFirstLineHeader.Enabled = value;
            }
        }
        public bool EnableExport
        {
            set { btnExport.Enabled = value; }
        }

        private MemberPageBaseWithPageStore ParentPage
        {
            get { return _parentPage ?? (_parentPage = Page as MemberPageBaseWithPageStore); }
        }

        public string LTLDiscountTiersDataStoreKey { get; set; }
        public string LTLAccessorialsDataStoreKey { get; set; }
        public string LTLAdditionalChargesDataStoreKey { get; set; }
        public string LTLCFCRulesDataStoreKey { get; set; }
        public string LTLOverlengthRulesDataStoreKey { get; set; }
        public string LTLPackageSpecificRatesDataStoreKey { get; set; }
        public string LTLGuaranteedChargesDataStoreKey { get; set; }

        private List<DiscountTier> LTLDiscountTiers
        {
            get
            {
                return ParentPage == null || !ParentPage.PageStore.ContainsKey(LTLDiscountTiersDataStoreKey)
                           ? null
                           : ParentPage.PageStore[LTLDiscountTiersDataStoreKey] as List<DiscountTier>;
            }
            set { ParentPage.PageStore[LTLDiscountTiersDataStoreKey] = value; }
        }
        private List<LTLAccessorial> LTLAccessorials
        {
            get
            {
                return ParentPage == null || !ParentPage.PageStore.ContainsKey(LTLAccessorialsDataStoreKey)
                           ? null
                           : ParentPage.PageStore[LTLAccessorialsDataStoreKey] as List<LTLAccessorial>;
            }
            set { ParentPage.PageStore[LTLAccessorialsDataStoreKey] = value; }
        }
        private List<LTLAdditionalCharge> LTLAdditionalCharges
        {
            get
            {
                return ParentPage == null || !ParentPage.PageStore.ContainsKey(LTLAdditionalChargesDataStoreKey)
                           ? null
                           : ParentPage.PageStore[LTLAdditionalChargesDataStoreKey] as List<LTLAdditionalCharge>;
            }
            set { ParentPage.PageStore[LTLAdditionalChargesDataStoreKey] = value; }
        }
        private List<LTLCubicFootCapacityRule> CFCRules
        {
            get
            {
                return ParentPage == null || !ParentPage.PageStore.ContainsKey(LTLCFCRulesDataStoreKey)
                           ? null
                           : ParentPage.PageStore[LTLCFCRulesDataStoreKey] as List<LTLCubicFootCapacityRule>;
            }
            set { ParentPage.PageStore[LTLCFCRulesDataStoreKey] = value; }
        }
        private List<LTLOverLengthRule> LTLOverlengthRules
        {
            get
            {
                return ParentPage == null || !ParentPage.PageStore.ContainsKey(LTLOverlengthRulesDataStoreKey)
                           ? null
                           : ParentPage.PageStore[LTLOverlengthRulesDataStoreKey] as List<LTLOverLengthRule>;
            }
            set { ParentPage.PageStore[LTLOverlengthRulesDataStoreKey] = value; }
        }
        private List<LTLPackageSpecificRate> LTLPackageSpecificRates
        {
            get
            {
                return ParentPage == null || !ParentPage.PageStore.ContainsKey(LTLPackageSpecificRatesDataStoreKey)
                           ? null
                           : ParentPage.PageStore[LTLPackageSpecificRatesDataStoreKey] as List<LTLPackageSpecificRate>;
            }
            set { ParentPage.PageStore[LTLPackageSpecificRatesDataStoreKey] = value; }
        }
        private List<LTLGuaranteedCharge> LTLGuaranteedCharges
        {
            get
            {
                return ParentPage == null || !ParentPage.PageStore.ContainsKey(LTLGuaranteedChargesDataStoreKey)
                           ? null
                           : ParentPage.PageStore[LTLGuaranteedChargesDataStoreKey] as List<LTLGuaranteedCharge>;
            }
            set { ParentPage.PageStore[LTLGuaranteedChargesDataStoreKey] = value; }
        }

        public long VendorRatingId
        {
            get { return hidVendorRatingId.Value.ToLong(); }
            set { hidVendorRatingId.Value = value.ToString(); }
        }

        public event EventHandler<ViewEventArgs<string>> ProcessingError;
        public event EventHandler<ViewEventArgs<string>> ProcessingCompleted;
        public event EventHandler Close;

        public event EventHandler<ViewEventArgs<string>> LTLDiscountTiersImported;
        public event EventHandler<ViewEventArgs<string>> LTLAccessorialsImported;
        public event EventHandler<ViewEventArgs<string>> LTLAdditionalChargesImported;
        public event EventHandler<ViewEventArgs<string>> LTLCFCRulesImported;
        public event EventHandler<ViewEventArgs<string>> LTLOverlengthRulesImported;
        public event EventHandler<ViewEventArgs<string>> LTLPackageSpecificRatesImported;
        public event EventHandler<ViewEventArgs<string>> LTLGuaranteedChargesImported;

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void OnCloseClicked(object sender, EventArgs e)
        {
            if (Close != null)
                Close(this, new EventArgs());
        }

        protected void OnImportClicked(object sender, EventArgs e)
        {
            if (!fupFile.HasFile && ProcessingError != null)
            {
                ProcessingError(this, new ViewEventArgs<string>("Please select a file for import"));
                return;
            }

            try
            {
                var import = false;

                if (chkDiscountTiers.Checked)
                {
                    ImportDiscountTiers();
                    import = true;
                }

                if (chkLTLAccessorials.Checked)
                {
                    ImportLTLAccessorials();
                    import = true;
                }

                if (chkAdditionalCharges.Checked)
                {
                    ImportLTLAdditionalCharges();
                    import = true;
                }

                if (chkCFCRules.Checked)
                {
                    ImportCFCRules();
                    import = true;
                }

                if (chkLTLOverlengthRules.Checked)
                {
                    ImportLTLOverlengthRules();
                    import = true;
                }
                if (chkLTLPackageSpecificRates.Checked)
                {
                    ImportLTLPackageSpecificRates();
                    import = true;
                }
                if (chkLTLGuaranteedCharges.Checked)
                {
                    ImportLTLGuaranteedCharges();
                    import = true;
                }


                if (!import && ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>("Please select an option prior to importing"));
            }
            catch
            {
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>("A processing error has occurred. This may be due to a problem with the input file"));
            }
        }



        protected void OnExportClicked(object sender, EventArgs e)
        {
            var export = false;

            if (chkDiscountTiers.Checked)
            {
                ExportDiscountTiers();
                export = true;
            }

            if (chkLTLAccessorials.Checked)
            {
                ExportLTLAccessorials();
                export = true;
            }

            if (chkAdditionalCharges.Checked)
            {
                ExportLTLAdditionalCharges();
                export = true;
            }

            if (chkCFCRules.Checked)
            {
                ExportCFCRules();
                export = true;
            }

            if (chkLTLOverlengthRules.Checked)
            {
                ExportLTLOverlengthRules();
                export = true;
            }
            if (chkLTLPackageSpecificRates.Checked)
            {
                ExportLTLPackageSpecificRates();
                export = true;
            }
            if (chkLTLGuaranteedCharges.Checked)
            {
                ExportLTLGuaranteedCharges();
                export = true;
            }

            if (!export && ProcessingError != null)
                ProcessingError(this, new ViewEventArgs<string>("Please select an option prior to exporting"));
        }


        private void ImportDiscountTiers()
        {
            var chargeCodeSearch = new ChargeCodeSearch();

            var rating = new VendorRating(VendorRatingId, false);
            var tiers = LTLDiscountTiers ?? new List<DiscountTier>();

            List<string[]> lines;
            try
            {
                lines = fupFile.GetImportData(true, chkFirstLineHeader.Checked);
            }
            catch (Exception ex)
            {
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(ex.Message));
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = chkFirstLineHeader.Checked ? 2 : 1;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 28;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }

            msgs.AddRange(chks.EnumsAreValid<DiscountTierRateBasis>(2));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }

            var weightBreaks = ProcessorUtilities.GetAll<WeightBreak>().Keys.Select(type => type.GetString()).ToList();
            weightBreaks.RemoveAt(WeightBreak.L5C.ToInt());
            msgs.AddRange(chks
                .Where(i => !weightBreaks.Contains(i.Line[7]))
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedEnumErrMsg, typeof(WeightBreak).Name.FormattedString(), i.Index))
                .ToList());
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }

            var chargeCodes = ProcessorVars.RegistryCache[ActiveUser.TenantId].ChargeCodes.Select(c => c.Code).ToList();
            msgs.AddRange(chks.CodesAreValid(3, chargeCodes, new ChargeCode().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }

            var listOfImportedRegionNames = chks.Select(i => i.Line[0]).ToList();
            listOfImportedRegionNames.AddRange(chks.Select(i => i.Line[1]).ToList());

            var rParameters = listOfImportedRegionNames
                .Distinct()
                .Select(n =>
                {
                    var p = RatingSearchFields.RegionName.ToParameterColumn();
                    p.DefaultValue = n;
                    p.Operator = Operator.Equal;
                    return p;
                })
                .ToList();

            var regions = new RegionSearch().FetchRegions(rParameters, ActiveUser.TenantId);
            var regionNames = regions.Select(c => c.Name).ToList();

            msgs.AddRange(chks.CodesAreValid(0, regionNames, new Region().EntityName()));
            msgs.AddRange(chks.CodesAreValid(1, regionNames, new Region().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }

            var classes = WebApplicationSettings.FreightClasses;
            var nDiscountTiers = lines
                .Select(s =>
                {
                    var originRegion = regions.First(r => r.Name == s[0]);
                    var destinationRegion = regions.First(r => r.Name == s[1]);

                    return new DiscountTier
                    {
                        OriginRegionId = originRegion.Id,
                        DestinationRegionId = destinationRegion.Id,
                        RateBasis = s[2].ToEnum<DiscountTierRateBasis>(),
                        FreightChargeCodeId = chargeCodeSearch.FetchChargeCodeIdByCode(s[3], ActiveUser.TenantId),
                        TierPriority = s[4].ToInt(),
                        EffectiveDate = s[5].ToDateTime(),
                        DiscountPercent = s[6].ToDecimal(),
                        WeightBreak = s[7].ToEnum<WeightBreak>(),
                        FloorValue = s[8].ToDecimal(),
                        CeilingValue = s[9].ToDecimal(),
                        FAK50 = classes.EnsureCorrectFreightClass(50, s[10].ToDouble()),
                        FAK55 = classes.EnsureCorrectFreightClass(55, s[11].ToDouble()),
                        FAK60 = classes.EnsureCorrectFreightClass(60, s[12].ToDouble()),
                        FAK65 = classes.EnsureCorrectFreightClass(65, s[13].ToDouble()),
                        FAK70 = classes.EnsureCorrectFreightClass(70, s[14].ToDouble()),
                        FAK775 = classes.EnsureCorrectFreightClass(77.5, s[15].ToDouble()),
                        FAK85 = classes.EnsureCorrectFreightClass(85, s[16].ToDouble()),
                        FAK925 = classes.EnsureCorrectFreightClass(92.5, s[17].ToDouble()),
                        FAK100 = classes.EnsureCorrectFreightClass(100, s[18].ToDouble()),
                        FAK110 = classes.EnsureCorrectFreightClass(110, s[19].ToDouble()),
                        FAK125 = classes.EnsureCorrectFreightClass(125, s[20].ToDouble()),
                        FAK150 = classes.EnsureCorrectFreightClass(150, s[21].ToDouble()),
                        FAK175 = classes.EnsureCorrectFreightClass(175, s[22].ToDouble()),
                        FAK200 = classes.EnsureCorrectFreightClass(200, s[23].ToDouble()),
                        FAK250 = classes.EnsureCorrectFreightClass(250, s[24].ToDouble()),
                        FAK300 = classes.EnsureCorrectFreightClass(300, s[25].ToDouble()),
                        FAK400 = classes.EnsureCorrectFreightClass(400, s[26].ToDouble()),
                        FAK500 = classes.EnsureCorrectFreightClass(500, s[27].ToDouble()),
                        TenantId = rating.TenantId,
                        VendorRating = rating
                    };
                })
                .ToList();

            tiers.AddRange(nDiscountTiers);

            LTLDiscountTiers = tiers;

            if (LTLDiscountTiersImported != null)
                LTLDiscountTiersImported(this, new ViewEventArgs<string>(DiscountTierImportCompleteMsg));
        }

        private void ImportLTLAccessorials()
        {
            var search = new ServiceSearch();

            var rating = new VendorRating(VendorRatingId, false);
            var accessorials = LTLAccessorials ?? new List<LTLAccessorial>();

            List<string[]> lines;
            try
            {
                lines = fupFile.GetImportData(true, chkFirstLineHeader.Checked);
            }
            catch (Exception ex)
            {
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(ex.Message));
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = chkFirstLineHeader.Checked ? 2 : 1;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 6;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }


            var rateTypes = ProcessorUtilities.GetAll<RateType>().Keys.Select(type => type.GetString()).ToList();
            rateTypes.RemoveAt(RateType.LineHaulPercentage.ToInt());
            rateTypes.RemoveAt(RateType.RatePerMile.ToInt());
            msgs.AddRange(chks
                .Where(i => !rateTypes.Contains(i.Line[1]))
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedEnumErrMsg, typeof(RateType).Name.FormattedString(), i.Index))
                .ToList());
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }

            var serviceCodes = ProcessorVars.RegistryCache[ActiveUser.TenantId].Services.Select(c => c.Code).ToList();
            msgs.AddRange(chks.CodesAreValid(0, serviceCodes, new Service().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }


            var nAccessorials = lines
                .Select(s => new LTLAccessorial
                {
                    ServiceId = search.FetchServiceIdByCode(s[0], ActiveUser.TenantId),
                    RateType = s[1].ToEnum<RateType>(),
                    EffectiveDate = s[2].ToDateTime(),
                    Rate = s[3].ToDecimal(),
                    FloorValue = s[4].ToDecimal(),
                    CeilingValue = s[5].ToDecimal(),
                    VendorRating = rating,
                    TenantId = rating.TenantId
                })
                .ToList();
            accessorials.AddRange(nAccessorials);

            LTLAccessorials = accessorials;

            if (LTLAccessorialsImported != null)
                LTLAccessorialsImported(this, new ViewEventArgs<string>(AccessorialsImportCompleteMsg));
        }

        private void ImportLTLAdditionalCharges()
        {
            var cSearch = new ChargeCodeSearch();

            var rating = new VendorRating(VendorRatingId, false);
            var charges = LTLAdditionalCharges ?? new List<LTLAdditionalCharge>();

            List<string[]> lines;
            try
            {
                lines = fupFile.GetImportData(true, chkFirstLineHeader.Checked);
            }
            catch (Exception ex)
            {
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(ex.Message));
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = chkFirstLineHeader.Checked ? 2 : 1;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();
            const int colCnt = 14;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }

            msgs = chks.Where(l => l.Line[0] == string.Empty)
                       .Select(i => ValidationMessage.Error("Import Grouping value missing on line: {0}", i.Index))
                       .ToList();
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }

            var countries = ProcessorVars.RegistryCache.Countries.Values;
            var countryCodes = countries.Select(c => c.Code).ToList();

            msgs.AddRange(chks.Where(l => l.Line[5].ToBoolean()).CodesAreValid(7, countryCodes, new Country().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }

            var rParameters = chks
                .Where(i => !i.Line[5].ToBoolean())
                .Select(i => i.Line[4])
                .Distinct()
                .Select(n =>
                {
                    var p = RatingSearchFields.RegionName.ToParameterColumn();
                    p.DefaultValue = n;
                    p.Operator = Operator.Equal;
                    return p;
                })
                .ToList();
            var regions = new List<Region>();
            if (rParameters.Any()) regions = new RegionSearch().FetchRegions(rParameters, ActiveUser.TenantId);
            var regionNames = regions.Select(c => c.Name).ToList();
            msgs.AddRange(chks.Where(i => !i.Line[5].ToBoolean()).CodesAreValid(4, regionNames, new Region().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }



            var postalCodeSearch = new PostalCodeSearch();
            var postalCodes = new List<PostalCodeViewSearchDto>();
            var invalidPostalCodes = chks
                .Where(i => i.Line[5].ToBoolean())
                .Where(i =>
                {
                    var code = postalCodeSearch.FetchPostalCodeByCode(i.Line[6], countries.First(c => c.Code == i.Line[7]).Id);
                    postalCodes.Add(code);
                    return code.Id == default(long);
                })
                .Select(i => string.Format(WebApplicationConstants.InvalidImportedCodeErrMsg, new PostalCode().EntityName(), i.Index))
                .ToList();
            if (invalidPostalCodes.Any())
            {
                invalidPostalCodes.Add(WebApplicationConstants.NoRecordsImported);
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(string.Join(WebApplicationConstants.HtmlBreak, invalidPostalCodes.ToArray())));
                return;
            }


            var rateTypes = ProcessorUtilities.GetAll<RateType>().Keys.Select(type => type.GetString()).ToList();
            rateTypes.RemoveAt(RateType.RatePerMile.ToInt());
            msgs.AddRange(chks
                .Where(i => !rateTypes.Contains(i.Line[10]))
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedEnumErrMsg, typeof(RateType).Name.FormattedString(), i.Index))
                .ToList());
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }


            //validate the additional charges
            var importGroups = new Dictionary<string, List<ImportLineObj>>();
            foreach (var line in chks)
            {
                if (!importGroups.ContainsKey(line.Line[0])) importGroups.Add(line.Line[0], new List<ImportLineObj>());
                importGroups[line.Line[0]].Add(line);
            }
            var additionalChargesToValidate = importGroups.Select(l => l.Value.First());

            var chargeCodes = ProcessorVars.RegistryCache[ActiveUser.TenantId].ChargeCodes.Select(c => c.Code).ToList();
            msgs.AddRange(additionalChargesToValidate.CodesAreValid(2, chargeCodes, new ChargeCode().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsChanged));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }

            var additionalCharges = new List<LTLAdditionalCharge>();
            foreach (var importGroup in importGroups)
            {
                var line = importGroup.Value[0].Line;

                var additionalCharge = new LTLAdditionalCharge
                {
                    TenantId = ActiveUser.TenantId,
                    VendorRating = rating,
                    Name = line[1],
                    ChargeCodeId = cSearch.FetchChargeCodeIdByCode(line[2], ActiveUser.TenantId),
                    EffectiveDate = line[3].ToDateTime(),
                    AdditionalChargeIndices = new List<AdditionalChargeIndex>()
                };

                for (var i = 0; i < importGroup.Value.Count(); i++)
                {
                    var detailLine = importGroup.Value[i].Line;

                    var usePostalCode = detailLine[5].ToBoolean();
                    var postalCode = usePostalCode ? postalCodes.First(pc => pc.Code.ToLower() == detailLine[6].ToLower() && pc.CountryId == countries.First(c => c.Code == detailLine[7]).Id) : null;
                    var countryId = usePostalCode ? countries.First(r => r.Code == detailLine[7]).Id : default(long);
                    var regionId = !usePostalCode ? regions.First(r => r.Name == detailLine[4]).Id : default(long);

                    var index = new AdditionalChargeIndex
                    {
                        RegionId = regionId,
                        UsePostalCode = usePostalCode,
                        PostalCode = postalCode == null ? string.Empty : postalCode.Code,
                        CountryId = countryId,
                        ApplyOnOrigin = detailLine[8].ToBoolean(),
                        ApplyOnDestination = detailLine[9].ToBoolean(),
                        RateType = detailLine[10].ToEnum<RateType>(),
                        Rate = detailLine[11].ToDecimal(),
                        ChargeFloor = detailLine[12].ToDecimal(),
                        ChargeCeiling = detailLine[13].ToDecimal(),
                        TenantId = rating.TenantId,
                        LTLAdditionalCharge = additionalCharge
                    };
                    additionalCharge.AdditionalChargeIndices.Add(index);
                }
                additionalCharges.Add(additionalCharge);
            }

            charges.AddRange(additionalCharges);

            LTLAdditionalCharges = charges;

            if (LTLAdditionalChargesImported != null)
                LTLAdditionalChargesImported(this, new ViewEventArgs<string>(AdditionalChargesImportCompleteMsg));
        }

        private void ImportCFCRules()
        {

            var rating = new VendorRating(VendorRatingId, false);
            var rules = CFCRules ?? new List<LTLCubicFootCapacityRule>();

            List<string[]> lines;
            try
            {
                lines = fupFile.GetImportData(true, chkFirstLineHeader.Checked);
            }
            catch (Exception ex)
            {
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(ex.Message));
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = chkFirstLineHeader.Checked ? 2 : 1;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 9;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }

            var freightClasses = WebApplicationSettings.FreightClasses.Select(f => f.ToString()).ToList();
            var invalidFreightClass = chks
                .Where(i => !string.IsNullOrEmpty(i.Line[2]))
                .Where(i => !freightClasses.Contains(i.Line[2]))
                .Select(i => string.Format(WebApplicationConstants.InvalidFreightClassErrMsg, i.Index))
                .ToList();
            if (invalidFreightClass.Any())
            {
                invalidFreightClass.Add(WebApplicationConstants.NoRecordsImported);
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(string.Join(WebApplicationConstants.HtmlBreak, invalidFreightClass.ToArray())));
                return;
            }

            var nCFCRules = lines
                .Select(s => new LTLCubicFootCapacityRule
                {
                    LowerBound = s[0].ToDecimal(),
                    UpperBound = s[1].ToDecimal(),
                    AppliedFreightClass = s[2].ToDouble(),
                    ApplyFAK = s[3].ToBoolean(),
                    ApplyDiscount = s[4].ToBoolean(),
                    AveragePoundPerCubicFootLimit = s[5].ToDecimal(),
                    AveragePoundPerCubicFootApplies = s[6].ToBoolean(),
                    PenaltyPoundPerCubicFoot = s[7].ToDecimal(),
                    EffectiveDate = s[8].ToDateTime(),
                    VendorRating = rating,
                    TenantId = rating.TenantId
                })
                .ToList();
            rules.AddRange(nCFCRules);

            CFCRules = rules;

            if (LTLCFCRulesImported != null)
                LTLCFCRulesImported(this, new ViewEventArgs<string>(CFCRulesImportCompleteMsg));
        }

        private void ImportLTLOverlengthRules()
        {
            var search = new ChargeCodeSearch();

            var rating = new VendorRating(VendorRatingId, false);
            var rules = LTLOverlengthRules ?? new List<LTLOverLengthRule>();

            List<string[]> lines;
            try
            {
                lines = fupFile.GetImportData(true, chkFirstLineHeader.Checked);
            }
            catch (Exception ex)
            {
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(ex.Message));
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = chkFirstLineHeader.Checked ? 2 : 1;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 5;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }

            var chargeCodes = ProcessorVars.RegistryCache[ActiveUser.TenantId].ChargeCodes.Select(c => c.Code).ToList();
            msgs.AddRange(chks.CodesAreValid(0, chargeCodes, new ChargeCode().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }

            var nOverlengthRules = lines
                .Select(s => new LTLOverLengthRule
                {
                    ChargeCodeId = search.FetchChargeCodeIdByCode(s[0], ActiveUser.TenantId),
                    LowerLengthBound = s[1].ToDecimal(),
                    UpperLengthBound = s[2].ToDecimal(),
                    Charge = s[3].ToDecimal(),
                    EffectiveDate = s[4].ToDateTime(),
                    VendorRating = rating,
                    TenantId = rating.TenantId
                })
                .ToList();
            rules.AddRange(nOverlengthRules);

            LTLOverlengthRules = rules;

            if (LTLOverlengthRulesImported != null)
                LTLOverlengthRulesImported(this, new ViewEventArgs<string>(OverlengthRulesImportCompleteMsg));
        }

        private void ImportLTLPackageSpecificRates()
        {
            var rating = new VendorRating(VendorRatingId, false);
            var psRates = LTLPackageSpecificRates ?? new List<LTLPackageSpecificRate>();

            List<string[]> lines;
            try
            {
                lines = fupFile.GetImportData(true, chkFirstLineHeader.Checked);
            }
            catch (Exception ex)
            {
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(ex.Message));
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = chkFirstLineHeader.Checked ? 2 : 1;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 6;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }

            var listOfImportedRegionNames = chks.Select(i => i.Line[1]).ToList();
            listOfImportedRegionNames.AddRange(chks.Select(i => i.Line[2]).ToList());

            var rParameters = listOfImportedRegionNames
                .Distinct()
                .Select(n =>
                {
                    var p = RatingSearchFields.RegionName.ToParameterColumn();
                    p.DefaultValue = n;
                    p.Operator = Operator.Equal;
                    return p;
                })
                .ToList();

            var regions = new RegionSearch().FetchRegions(rParameters, ActiveUser.TenantId);
            var regionNames = regions.Select(c => c.Name).ToList();

            msgs.AddRange(chks.CodesAreValid(1, regionNames, new Region().EntityName()));
            msgs.AddRange(chks.CodesAreValid(2, regionNames, new Region().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }

            var nPsRates = lines
                .Select(s =>
                {
                    var originRegion = regions.First(r => r.Name == s[1]);
                    var destinationRegion = regions.First(r => r.Name == s[2]);

                    return new LTLPackageSpecificRate
                    {
                        OriginRegionId = originRegion.Id,
                        DestinationRegionId = destinationRegion.Id,
                        EffectiveDate = s[0].ToDateTime(),
                        PackageQuantity = s[3].ToInt(),
                        Rate = s[4].ToDecimal(),
                        RatePriority = s[5].ToInt(),
                        TenantId = rating.TenantId,
                        VendorRating = rating
                    };
                })
                .ToList();

            psRates.AddRange(nPsRates);

            LTLPackageSpecificRates = psRates;

            if (LTLPackageSpecificRatesImported != null)
                LTLPackageSpecificRatesImported(this, new ViewEventArgs<string>(PackageSpecficRatesImportCompleteMsg));
        }

        private void ImportLTLGuaranteedCharges()
        {
            var rating = new VendorRating(VendorRatingId, false);
            var gCharges = LTLGuaranteedCharges ?? new List<LTLGuaranteedCharge>();

            List<string[]> lines;
            try
            {
                lines = fupFile.GetImportData(true, chkFirstLineHeader.Checked);
            }
            catch (Exception ex)
            {
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(ex.Message));
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = chkFirstLineHeader.Checked ? 2 : 1;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 9;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }

            msgs.AddRange(chks
                .Where(l => !(l.Line[1].Length >= 2 ? l.Line[1].Insert(2, TimeUtility.Seperator.ToString()) : l.Line[1]).IsValidFreeFormTimeString())
                .Select(l=> ValidationMessage.Error("Incorrect time format on line {0}", l.Index)));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }

            var rateTypes = ProcessorUtilities.GetAll<RateType>().Keys.Select(type => type.GetString()).ToList();
            rateTypes.RemoveAt(RateType.PerHundredWeight.ToInt());
            rateTypes.RemoveAt(RateType.RatePerMile.ToInt());
            msgs.AddRange(chks
                .Where(i => !rateTypes.Contains(i.Line[2]))
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedEnumErrMsg, typeof(RateType).Name.FormattedString(), i.Index))
                .ToList());
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }


            var codes = ProcessorVars.RegistryCache[ActiveUser.TenantId].ChargeCodes.Select(c => c.Code).ToList();
            msgs.AddRange(chks.CodesAreValid(7, codes, new ChargeCode().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }

            var chargeCodeSearch = new ChargeCodeSearch();

            var nGuaranteedCharges = lines
                .Select(s => new LTLGuaranteedCharge
                    {
                        Description = s[0],
                        Time = s[1].Insert(2, TimeUtility.Seperator.ToString()),
                        RateType = s[2].ToEnum<RateType>(),
                        Rate = s[3].ToDecimal(),
                        FloorValue = s[4].ToDecimal(),
                        CeilingValue = s[5].ToDecimal(),
                        EffectiveDate = s[6].ToDateTime(),
                        ChargeCodeId = chargeCodeSearch.FetchChargeCodeIdByCode(s[7], ActiveUser.TenantId),
                        CriticalNotes = s[8],
                        TenantId = rating.TenantId,
                        VendorRating = rating
                    })
                .ToList();

            gCharges.AddRange(nGuaranteedCharges);

            LTLGuaranteedCharges = gCharges;

            if (LTLGuaranteedChargesImported != null)
                LTLGuaranteedChargesImported(this, new ViewEventArgs<string>(GuaranteedChargesImportCompleteMsg));
        }

        private void ExportDiscountTiers()
        {
            var rating = new VendorRating(VendorRatingId);

            var lines = rating.LTLDiscountTiers
                .Select(tier => new[]
				             	{
				             		tier.OriginRegion.Name,
				             		tier.DestinationRegion.Name,
				             		tier.RateBasis.ToInt().ToString(),
				             		tier.FreightChargeCode.Code,                                  
				             		tier.TierPriority.ToString(),
				             		tier.EffectiveDate.FormattedShortDate(),
				             		tier.DiscountPercent.ToString(),
									tier.WeightBreak.ToInt().ToString(),
				             		tier.FloorValue.ToString(),
				             		tier.CeilingValue.ToString(),
				             		tier.FAK50.ToString(),
				             		tier.FAK55.ToString(),
				             		tier.FAK60.ToString(),
				             		tier.FAK65.ToString(),
				             		tier.FAK70.ToString(),
				             		tier.FAK775.ToString(),
				             		tier.FAK85.ToString(),
				             		tier.FAK925.ToString(),
				             		tier.FAK100.ToString(),
				             		tier.FAK110.ToString(),
				             		tier.FAK125.ToString(),
				             		tier.FAK150.ToString(),
				             		tier.FAK175.ToString(),
				             		tier.FAK200.ToString(),
				             		tier.FAK250.ToString(),
				             		tier.FAK300.ToString(),
				             		tier.FAK400.ToString(),
				             		tier.FAK500.ToString()
				             	}.TabJoin())
                .ToList();
            lines.Insert(0, new[]
			                	{
			                		"Origin Region", "Destination Region", "Rate Basis", "Freight Charge Code", "Tier Priority",
			                		"Effective Date", "Discount Percent", "Max. Weight Break",
			                		"Floor Value",
			                		"Ceiling Value", "FAK50", "FAK55", "FAK60", "FAK65", "FAK70", "FAK775", "FAK85", "FAK925", "FAK100"
			                		, "FAK110", "FAK125", "FAK150", "FAK175",
			                		"FAK200", "FAK250", "FAK300", "FAK400", "FAK500"
			                	}.TabJoin());

            Response.Export(lines.ToArray().NewLineJoin(), Guid.NewGuid().ToString() + ".txt");
        }

        private void ExportLTLAccessorials()
        {
            var rating = new VendorRating(VendorRatingId);

            var lines = rating.LTLAccessorials
                .Select(a => new[]
				             	{
				             		a.Service.Code,
				             		((int) a.RateType).ToString(),
				             		a.EffectiveDate.FormattedShortDate(),
				             		a.Rate.ToString(),
				             		a.FloorValue.ToString(),
				             		a.CeilingValue.ToString()
				             	}.TabJoin())
                .ToList();
            lines.Insert(0, new[] { "Service Code", "Rate Type", "Effective Date", "Rate", "Floor Value", "Ceiling Value" }.TabJoin());

            Response.Export(lines.ToArray().NewLineJoin(), Guid.NewGuid().ToString() + ".txt");
        }

        private void ExportLTLAdditionalCharges()
        {
            var rating = new VendorRating(VendorRatingId);

            var lines = new List<string>();
            for (var i = 0; i < rating.LTLAdditionalCharges.Count; i++)
            {
                var charge = rating.LTLAdditionalCharges[i];
                var firstChargeIndex = charge.AdditionalChargeIndices.FirstOrDefault() ?? new AdditionalChargeIndex();

                lines.Add(new[]
		            {
		                (i + 1).ToString(), 
                        charge.Name, 
                        charge.ChargeCode.Code, 
                        charge.EffectiveDate.FormattedShortDate(),
		                firstChargeIndex.UsePostalCode || firstChargeIndex.RegionId == default(long) ? string.Empty : firstChargeIndex.Region.Name,
		                firstChargeIndex.UsePostalCode.ToString(),
		                firstChargeIndex.UsePostalCode ? firstChargeIndex.PostalCode.GetString() : string.Empty,
		                firstChargeIndex.UsePostalCode && firstChargeIndex.CountryId != default(long) ? firstChargeIndex.Country.Code : string.Empty,
		                firstChargeIndex.ApplyOnOrigin.ToString(),
		                firstChargeIndex.ApplyOnDestination.ToString(),
		                firstChargeIndex.RateType.ToInt().ToString(),
		                firstChargeIndex.Rate.ToString(),
		                firstChargeIndex.ChargeFloor.ToString(),
		                firstChargeIndex.ChargeCeiling.ToString()
		            }.TabJoin());

                lines.AddRange(charge.AdditionalChargeIndices.Skip(1).Select(index => new[]
		            {
		                (i + 1).ToString(),
                        string.Empty,
                        string.Empty,
                        string.Empty,
                        index.UsePostalCode ? string.Empty : index.Region.Name, 
                        index.UsePostalCode.ToString(),
                        index.UsePostalCode ? index.PostalCode : string.Empty,
		                index.UsePostalCode ? index.Country.Code : string.Empty, index.ApplyOnOrigin.ToString(),
		                index.ApplyOnDestination.ToString(), 
                        index.RateType.ToInt().ToString(), 
                        index.Rate.ToString(),
		                index.ChargeFloor.ToString(), index.ChargeCeiling.ToString()
		            }.TabJoin()));
            }

            lines.Insert(0, new[]
		        {
		            "Group Code", "Name", "Charge Code", "Effective Date", "Region", "Use Postal Code", "Postal Code",
		            "Country Code", "Apply on Origin",
		            "Apply on Destination",
		            "Rate Type", "Rate", "Charge Floor", "Charge Ceiling"
		        }.TabJoin());

            Response.Export(lines.ToArray().NewLineJoin(), Guid.NewGuid().ToString() + ".txt");
        }

        private void ExportCFCRules()
        {
            var rating = new VendorRating(VendorRatingId);

            var lines = rating.LTLCubicFootCapacityRules
                .Select(rule => new[]
				                	{
				                		rule.LowerBound.ToString(),
				                		rule.UpperBound.ToString(),
				                		rule.AppliedFreightClass.ToString() == 0.ToString() ? string.Empty : rule.AppliedFreightClass.ToString(),
				                		rule.ApplyFAK.ToString(),
				                		rule.ApplyDiscount.ToString(),
				                		rule.AveragePoundPerCubicFootLimit.ToString(),
				                		rule.AveragePoundPerCubicFootApplies.ToString(),
				                		rule.PenaltyPoundPerCubicFoot.ToString(),
				                		rule.EffectiveDate.FormattedShortDate()
				                	}.TabJoin())
                .ToList();
            lines.Insert(0, new[]
			                	{
			                		"Lower Bound", "Upper Bound", "Applied Freight Class", "Apply FAK", "Apply Discount",
			                		"Average Pound Per Cubic Foot Limit",
			                		"Average Pound Per Cubic Foot Applies", "Penalty Pound Per Cubic Foot", 
			                		"Effective Date"
			                	}.TabJoin());

            Response.Export(lines.ToArray().NewLineJoin(), Guid.NewGuid().ToString() + ".txt");
        }

        private void ExportLTLOverlengthRules()
        {
            var rating = new VendorRating(VendorRatingId);

            var lines = rating.LTLOverLengthRules
                .Select(r => new[]
				             	{
				             		r.ChargeCode.Code,
				             		r.LowerLengthBound.ToString(),
				             		r.UpperLengthBound.ToString(),
				             		r.Charge.ToString(),
				             		r.EffectiveDate.FormattedShortDate()
				             	}.TabJoin())
                .ToList();
            lines.Insert(0, new[] { "Charge Code", "Lower Length Bound", "Upper Length Bound", "Charge", "Effective Date" }.TabJoin());

            Response.Export(lines.ToArray().NewLineJoin(), Guid.NewGuid().ToString() + ".txt");
        }

        private void ExportLTLPackageSpecificRates()
        {
            var rating = new VendorRating(VendorRatingId);

            var lines = rating.LTLPackageSpecificRates
                .Select(psRate => new[]
				             	{
                                    psRate.EffectiveDate.FormattedShortDate(),
				             		psRate.OriginRegion.Name,
				             		psRate.DestinationRegion.Name,
                                    psRate.PackageQuantity.ToString(),
                                    psRate.Rate.ToString(),
                                    psRate.RatePriority.ToString()
				             	}.TabJoin())
                .ToList();
            lines.Insert(0, new[]
			                	{
			                		"Effective Date", "Origin Region", "Destination Region", "Package Quantity", "Flat Rate", "Rate Priority"
			                	}.TabJoin());

            Response.Export(lines.ToArray().NewLineJoin(), Guid.NewGuid().ToString() + ".txt");
        }

        private void ExportLTLGuaranteedCharges()
        {
            var rating = new VendorRating(VendorRatingId);

            var lines = rating.LTLGuaranteedCharges
                .Select(charge => new[]
				             	{
                                    charge.Description,
                                    charge.Time,
                                    ((int)charge.RateType).ToString(),
                                    charge.Rate.ToString(),
                                    charge.FloorValue.ToString(),
                                    charge.CeilingValue.ToString(),
                                    charge.EffectiveDate.FormattedShortDate(),
                                    charge.ChargeCode.Code
				             	}.TabJoin())
                .ToList();
            lines.Insert(0, new[]
			                	{
			                		"Description", "Time", "Rate Type", "Rate", "Floor Value", "Ceiling Value", "Effective Date", "Charge Code"
			                	}.TabJoin());

            Response.Export(lines.ToArray().NewLineJoin(), Guid.NewGuid().ToString() + ".txt");
        }
    }
}