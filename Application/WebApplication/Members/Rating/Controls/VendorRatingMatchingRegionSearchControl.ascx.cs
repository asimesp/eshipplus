﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Rating;
using LogisticsPlus.Eship.Processor.Views;

namespace LogisticsPlus.Eship.WebApplication.Members.Rating.Controls
{
    /*
    *	Expected: 
    *		1. Parent page must inherit from MemberPageBaseWithPageStore to use discount tier functions!
    *		2. Application StoreKey must also be set!
    * 
    */
    public partial class VendorRatingMatchingRegionSearchControl : MemberControlBase
    {
	    protected const string GotoPage = "Go to page with record index ";

        private MemberPageBaseWithPageStore _parentPage;



        public long VendorRatingId
        {
            get { return hidVendorRatingId.Value.ToLong(); }
            set { hidVendorRatingId.Value = value.ToString(); }
        }


        public string LTLDiscountTiersDataStoreKey { get; set; }


        private MemberPageBaseWithPageStore ParentPage
        {
            get { return _parentPage ?? (_parentPage = Page as MemberPageBaseWithPageStore); }
        }

        private IEnumerable<DiscountTier> LTLDiscountTiers
        {
            get
            {
                return ParentPage == null || !ParentPage.PageStore.ContainsKey(LTLDiscountTiersDataStoreKey)
                           ? null
                           : ParentPage.PageStore[LTLDiscountTiersDataStoreKey] as List<DiscountTier>;
            }
        }


        public event EventHandler Close;
	    public event EventHandler<ViewEventArgs<int>> SelectedIndex;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;

            ddlOriginCountry.SelectedValue = ActiveUser.Tenant.DefaultCountryId.GetString();
            ddlDestinationCountry.SelectedValue = ActiveUser.Tenant.DefaultCountryId.GetString();

        }

        protected void OnCloseClicked(object sender, EventArgs e)
        {
            lstMatchingRegions.DataSource = new List<object>();
            lstMatchingRegions.DataBind();

            if (Close != null)
                Close(this, new EventArgs());
        }

        protected void OnFindMatchingLanesClicked(object sender, EventArgs e)
        {
            ProcessLTLDiscountTiers();
        }

        private void ProcessLTLDiscountTiers()
        {
            if (LTLDiscountTiers == null) return;

            var criteria = new MatchingRatingRegionIdCriteria
            {
                DestinationCountryId = ddlDestinationCountry.SelectedValue.ToLong(),
                DestinationPostalCode = txtDestinationPostalCode.Text,
                OriginCountryId = ddlOriginCountry.SelectedValue.ToLong(),
                OriginPostalCode = txtOriginPostalCode.Text,
                RatingId = VendorRatingId
            };
            var match = new MatchingVendorRatingRegionIdsDto(ActiveUser.TenantId, criteria);

            var cnt = 0;
            var tiers = from item in LTLDiscountTiers
                        let origin = item.OriginRegionId
                        let dest = item.DestinationRegionId
                        let originValid =
                            (!string.IsNullOrEmpty(txtOriginPostalCode.Text) && match.OriginRegionIds.Contains(origin)) ||
                            string.IsNullOrEmpty(txtOriginPostalCode.Text)
                        let destValid =
                            (!string.IsNullOrEmpty(txtDestinationPostalCode.Text) && match.DestinationRegionIds.Contains(dest)) ||
                            string.IsNullOrEmpty(txtDestinationPostalCode.Text)
                        let index = ++cnt
                        where originValid && destValid
                        select new
                        {
                            Index = index,
                            Origin = item.OriginRegion.Name,
                            Destination = item.DestinationRegion.Name,
                        };

            lstMatchingRegions.DataSource = tiers;
            lstMatchingRegions.DataBind();
        }

	    protected void OnIndexClicked(object sender, EventArgs e)
	    {
		    var btn = (ImageButton) sender;

		    if (SelectedIndex != null)
			    SelectedIndex(this, new ViewEventArgs<int>(btn.ToolTip.Replace(GotoPage, string.Empty).ToInt()));
	    }
    }
}