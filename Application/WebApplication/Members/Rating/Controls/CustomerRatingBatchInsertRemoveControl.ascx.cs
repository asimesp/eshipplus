﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Rating;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;
using LogisticsPlus.Eship.SmallPacks;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Rating.Controls
{
    public partial class CustomerRatingBatchInsertRemoveControl : MemberControlBase, ICustomerRatingItemsBatchView
    {
        private const string InvalidServiceTypeErrMsg = "Invalid Service Type on line: {0}";

        public event EventHandler<ViewEventArgs<IEnumerable<ValidationMessage>>> ProcessingMessage;
        public event EventHandler Close;

        public event EventHandler<ViewEventArgs<List<string[]>>> InsertLTLSellRates;
        public event EventHandler<ViewEventArgs<List<string[]>>> RemoveLTLSellRates;
        public event EventHandler<ViewEventArgs<List<string[]>>> InsertSmallPackRates;
        public event EventHandler<ViewEventArgs<List<string[]>>> RemoveSmallPackRates;
        public event EventHandler<ViewEventArgs<List<string[]>>> InsertCustomerServiceMarkups;
        public event EventHandler<ViewEventArgs<List<string[]>>> RemoveCustomerServiceMarkups;
        public event EventHandler<ViewEventArgs<List<string[]>>> InsertTLSellRates;
        public event EventHandler<ViewEventArgs<List<string[]>>> RemoveTLSellRates;
        public event EventHandler<ViewEventArgs<List<string[]>>> InsertProfitMarkups;

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (ProcessingMessage != null)
                ProcessingMessage(this, new ViewEventArgs<IEnumerable<ValidationMessage>>(messages));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            new CustomerRatingItemsBatchHandler(this).Initialize();

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script language='javascript'>");
            sb.Append(@"changeDisableStateRemoveButton();");
            sb.Append(@"</script>");

			System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "JCall1", sb.ToString(), false);
        }

        protected void OnCloseClicked(object sender, EventArgs e)
        {
            if (Close != null)
                Close(this, new EventArgs());
        }

        protected void OnAddClicked(object sender, EventArgs e)
        {
            if (!fupFile.HasFile && ProcessingMessage != null)
            {
                var errors = new[] { ValidationMessage.Error("Missing input file!") };
                ProcessingMessage(this, new ViewEventArgs<IEnumerable<ValidationMessage>>(errors));
                return;
            }

            if (!chkCustomerServiceMarkups.Checked && !chkLTLSellRates.Checked && !chkSmallPackRates.Checked
                && !chkTLSellRates.Checked && !this.chkProfitMarkups.Checked && ProcessingMessage != null)
            {
                var errors = new[] { ValidationMessage.Error("Please select an option!") };
                ProcessingMessage(this, new ViewEventArgs<IEnumerable<ValidationMessage>>(errors));
                return;
            }

            try
            {
                if (chkLTLSellRates.Checked && InsertLTLSellRates != null)
                    InsertLTLSellRates(this, new ViewEventArgs<List<string[]>>(RetrieveFileInputParts(30, true)));
                else if (chkSmallPackRates.Checked && InsertSmallPackRates != null)
                    InsertSmallPackRates(this, new ViewEventArgs<List<string[]>>(RetrieveFileInputParts(10, true)));
                else if (chkCustomerServiceMarkups.Checked && InsertCustomerServiceMarkups != null)
                    InsertCustomerServiceMarkups(this, new ViewEventArgs<List<string[]>>(RetrieveFileInputParts(9, true)));
                else if (chkTLSellRates.Checked && InsertTLSellRates != null)
                    InsertTLSellRates(this, new ViewEventArgs<List<string[]>>(RetrieveFileInputParts(19, true)));
                else if (chkProfitMarkups.Checked && InsertProfitMarkups != null)
	                InsertProfitMarkups(this, new ViewEventArgs<List<string[]>>(RetrieveFileInputParts(37, true)));
            }
            catch (Exception ex)
            {
                if (ProcessingMessage != null)
                {
                    var validationMessages = new[] { ValidationMessage.Error("Processing error: {0}", ex.Message) };
                    ProcessingMessage(this, new ViewEventArgs<IEnumerable<ValidationMessage>>(validationMessages));
                }
            }
        }

        protected void OnRemoveClicked(object sender, EventArgs e)
        {
            if (!fupFile.HasFile && ProcessingMessage != null)
            {
                var errors = new[] { ValidationMessage.Error("Missing input file!") };
                ProcessingMessage(this, new ViewEventArgs<IEnumerable<ValidationMessage>>(errors));
                return;
            }

            if (!chkCustomerServiceMarkups.Checked && !chkLTLSellRates.Checked && !chkSmallPackRates.Checked
                && !chkTLSellRates.Checked && ProcessingMessage != null)
            {
                var errors = new[] { ValidationMessage.Error("Please select an option!") };
                ProcessingMessage(this, new ViewEventArgs<IEnumerable<ValidationMessage>>(errors));
                return;
            }

            try
            {
                if (chkLTLSellRates.Checked && RemoveLTLSellRates != null)
                    RemoveLTLSellRates(this, new ViewEventArgs<List<string[]>>(RetrieveFileInputParts(3, false)));
                else if (chkSmallPackRates.Checked && RemoveSmallPackRates != null)
                    RemoveSmallPackRates(this, new ViewEventArgs<List<string[]>>(RetrieveFileInputParts(4, false)));
                else if (chkCustomerServiceMarkups.Checked && RemoveCustomerServiceMarkups != null)
                    RemoveCustomerServiceMarkups(this, new ViewEventArgs<List<string[]>>(RetrieveFileInputParts(3, false)));
                else if (chkTLSellRates.Checked && RemoveTLSellRates != null)
                    RemoveTLSellRates(this, new ViewEventArgs<List<string[]>>(RetrieveFileInputParts(10, false)));
            }
            catch (Exception ex)
            {
                if (ProcessingMessage != null)
                {
                    var validationMessages = new[] { ValidationMessage.Error("Processing error: {0}", ex.Message) };
                    ProcessingMessage(this, new ViewEventArgs<IEnumerable<ValidationMessage>>(validationMessages));
                }
            }
        }

        private List<string[]> RetrieveFileInputParts(int columnsCount, bool isInsert)
        {
            List<string[]> lines;
            try
            {
                lines = fupFile.GetImportData(true, chkFirstLineHeader.Checked);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return null;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = chkFirstLineHeader.Checked ? 2 : 1;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            msgs.AddRange(chks.LineLengthsAreValid(columnsCount));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return null;
            }

            if (chkCustomerServiceMarkups.Checked)
            {
                var serviceCodes = ProcessorVars.RegistryCache[ActiveUser.TenantId].Services.Select(c => c.Code).ToList();
                msgs.AddRange(chks.CodesAreValid(isInsert ? 1 : 2, serviceCodes, new Service().EntityName()));
                if (msgs.Any())
                {
                    msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsChanged));
                    DisplayMessages(msgs);
                    return null;
                }
            }

            if (chkSmallPackRates.Checked)
            {
                const int smallPackEngineIndex = 2;
                const int serviceTypeIndex = 3;

                var types = ProcessorUtilities.GetAll<SmallPackageEngine>().Keys.Select(type => type.ToString()).ToList();
                types.Remove(SmallPackageEngine.None.ToInt().ToString());
                if (!chks.All(i => types.Contains(i.Line[smallPackEngineIndex])))
                {
                    msgs = chks
                        .Where(i => !types.Contains(i.Line[smallPackEngineIndex]))
                        .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedEnumErrMsg, new SmallPackageEngine().EntityName(), i.Index))
                        .ToList();
                    msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsChanged));
                    DisplayMessages(msgs);
                    return null;
                }

                var serviceTypes = SmallPacksFactory.RetrieveServiceTypes();
                var invalidServiceTypes = chks
                    .Where(i => !(serviceTypes[i.Line[smallPackEngineIndex].ToEnum<SmallPackageEngine>()].Contains(i.Line[serviceTypeIndex])))
                    .Select(i => ValidationMessage.Error(InvalidServiceTypeErrMsg, i.Index))
                    .ToList();
                if (invalidServiceTypes.Any())
                {
                    invalidServiceTypes.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsChanged));
                    DisplayMessages(invalidServiceTypes);
                    return null;
                }

                if (isInsert)
                {
                    var chargeCodes = ProcessorVars.RegistryCache[ActiveUser.TenantId].ChargeCodes.Select(c => c.Code).ToList();
                    msgs.AddRange(chks.CodesAreValid(8, chargeCodes, new ChargeCode().EntityName()));
                    if (msgs.Any())
                    {
                        msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsChanged));
                        DisplayMessages(msgs);
                        return null;
                    }
                }
            }

            if (chkTLSellRates.Checked)
            {
                if (isInsert)
                {
                    var equipmentTypeCodes = ProcessorVars.RegistryCache[ActiveUser.TenantId].EquipmentTypes.Select(c => c.Code).ToList();
                    msgs.AddRange(chks.Where(c => !string.IsNullOrEmpty(c.Line[1])).CodesAreValid(1, equipmentTypeCodes, new EquipmentType().EntityName()));
                    if (msgs.Any())
                    {
                        msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                        DisplayMessages(msgs);
                        return null;
                    }

                    msgs.AddRange(chks.EnumsAreValid<RateType>(3).ToList());
                    if (msgs.Any())
                    {
                        msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                        DisplayMessages(msgs);
                        return null;
                    }

                    msgs.AddRange(chks.EnumsAreValid<TLTariffType>(8).ToList());
                    if (msgs.Any())
                    {
                        msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                        DisplayMessages(msgs);
                        return null;
                    }

                    var mileageSourceCodes = ProcessorVars.RegistryCache[ActiveUser.TenantId].MileageSources.Select(c => c.Code).ToList();
                    msgs.AddRange(chks.Where(c => !string.IsNullOrEmpty(c.Line[18])).CodesAreValid(18, mileageSourceCodes, new MileageSource().EntityName()));
                    if (msgs.Any())
                    {
                        msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                        DisplayMessages(msgs);
                        return null;
                    }
                }

                var originCountryIndex = isInsert ? 11 : 4;
                var destCountryIndex = isInsert ? 15 : 8;
                var countries = ProcessorVars.RegistryCache.Countries.Values;
                var countryCodes = countries.Select(c => c.Code).ToList();

                msgs.AddRange(chks.Where(c => !string.IsNullOrEmpty(c.Line[originCountryIndex])).CodesAreValid(originCountryIndex, countryCodes, new Country().EntityName()));
                msgs.AddRange(chks.Where(c => !string.IsNullOrEmpty(c.Line[destCountryIndex])).CodesAreValid(destCountryIndex, countryCodes, new Country().EntityName()));
                if (msgs.Any())
                {
                    msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                    DisplayMessages(msgs);
                    return null;
                }

                var originPostalCodeIndex = isInsert ? 12 : 5;
                var destPostalCodeIndex = isInsert ? 16 : 9;
                var postalCodeSearch = new PostalCodeSearch();
                msgs.AddRange(chks
                    .Where(c => !string.IsNullOrEmpty(c.Line[originPostalCodeIndex]) && !string.IsNullOrEmpty(c.Line[originCountryIndex]))
                    .Where(i =>
                    {
                        var code = postalCodeSearch.FetchPostalCodeByCode(i.Line[originPostalCodeIndex], countries.First(c => c.Code == i.Line[originCountryIndex]).Id);
                        return code.Id == default(long);
                    })
                    .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedCodeErrMsg, new PostalCode().EntityName(), i.Index))
                    .ToList());
                msgs.AddRange(chks
                    .Where(c => !string.IsNullOrEmpty(c.Line[destPostalCodeIndex]) && !string.IsNullOrEmpty(c.Line[destCountryIndex]))
                    .Where(i =>
                    {
                        var code = postalCodeSearch.FetchPostalCodeByCode(i.Line[destPostalCodeIndex], countries.First(c => c.Code == i.Line[destCountryIndex]).Id);
                        return code.Id == default(long);
                    })
                    .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedCodeErrMsg, new PostalCode().EntityName(), i.Index))
                    .ToList());
                if (msgs.Any())
                {
                    msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                    DisplayMessages(msgs);
                    return null;
                }
            }

            return lines;
        }
    }
}