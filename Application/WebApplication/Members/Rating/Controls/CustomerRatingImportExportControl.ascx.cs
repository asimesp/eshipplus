﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Rating;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.SmallPacks;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Rating.Controls
{
    public partial class CustomerRatingImportExportControl : MemberControlBase
    {
        private const string InvalidServiceTypeErrMsg = "Invalid Service Type on line: {0}";
        private const string ImportCompleteMsg = "Import complete";

        private MemberPageBaseWithPageStore _parentPage;

        public bool EnableImport
        {
            set
            {
                btnImport.Enabled = value;
                fupFile.Enabled = value;
                chkFirstLineHeader.Enabled = value;
            }
        }
        public bool EnableExport
        {
            set { btnExport.Enabled = value; }
        }

        private MemberPageBaseWithPageStore ParentPage
        {
            get { return _parentPage ?? (_parentPage = Page as MemberPageBaseWithPageStore); }
        }

        public string TLSellRateDataStoreKey { get; set; }
        public string LTLSellRateDataStoreKey { get; set; }
        public string SmallPackRateDataStoreKey { get; set;}
        public string CustomerServiceMarkupDataStoreKey { get; set;}


        private List<TLSellRate> TLSellRates
        {
            get
            {
                return ParentPage == null || !ParentPage.PageStore.ContainsKey(TLSellRateDataStoreKey)
                           ? null
                           : ParentPage.PageStore[TLSellRateDataStoreKey] as List<TLSellRate>;
            }
            set { ParentPage.PageStore[TLSellRateDataStoreKey] = value; }
        }

        private List<LTLSellRateViewDto> LTLSellRates
        {
            get
            {
                return ParentPage == null || !ParentPage.PageStore.ContainsKey(LTLSellRateDataStoreKey)
                           ? null
                           : ParentPage.PageStore[LTLSellRateDataStoreKey] as List<LTLSellRateViewDto>;
            }
            set { ParentPage.PageStore[LTLSellRateDataStoreKey] = value; }
        }

        private List<SmallPackRateViewDto> SmallPackRates
        {
            get
            {
                return ParentPage == null || !ParentPage.PageStore.ContainsKey(SmallPackRateDataStoreKey)
                           ? null
                           : ParentPage.PageStore[SmallPackRateDataStoreKey] as List<SmallPackRateViewDto>;
            }
            set { ParentPage.PageStore[SmallPackRateDataStoreKey] = value; }
        }

        private List<CustomerServiceMarkup> CustomerServiceMarkups
        {
            get
            {
                return ParentPage == null || !ParentPage.PageStore.ContainsKey(CustomerServiceMarkupDataStoreKey)
                           ? null
                           : ParentPage.PageStore[CustomerServiceMarkupDataStoreKey] as List<CustomerServiceMarkup>;
            }
            set { ParentPage.PageStore[CustomerServiceMarkupDataStoreKey] = value; }
        }

        public long CustomerRatingId
        {
            get { return hidCustomerRatingId.Value.ToLong(); }
            set { hidCustomerRatingId.Value = value.ToString(); }
        }

        public event EventHandler<ViewEventArgs<string>> ProcessingError;
        public event EventHandler<ViewEventArgs<string>> TlSellRatesImported;
        public event EventHandler<ViewEventArgs<string>> LtlSellRatesImported;
        public event EventHandler<ViewEventArgs<string>> SmallPackRatesImported;
        public event EventHandler<ViewEventArgs<string>> CustomerServiceMarkupsImported;
        public event EventHandler Close;

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void OnCloseClicked(object sender, EventArgs e)
        {
            if (Close != null)
                Close(this, new EventArgs());
        }


        protected void OnImportClicked(object sender, EventArgs e)
        {
            if (!fupFile.HasFile && ProcessingError != null)
            {
                ProcessingError(this, new ViewEventArgs<string>("Please select a file for import"));
                return;
            }

            try
            {
                var import = false;

                if (chkLTLSellRates.Checked)
                {
                    ImportLTLSellRates();
                    import = true;
                }
                if (chkTLSellRates.Checked)
                {
                    ImportTLSellRates();
                    import = true;
                }

                if (chkSmallPackRates.Checked)
                {
                    ImportSmallPackRates();
                    import = true;
                }

                if (chkCustomerServiceMarkups.Checked)
                {
                    ImportCustomerServiceMarkups();
                    import = true;
                }

                if (!import && ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>("Please select an option prior to importing"));

            }
            catch
            {
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>("A processing error has occurred.  This may be due to a problem with the input file"));
            }
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            var export = false;

            if (chkLTLSellRates.Checked)
            {
                ExportLTLSellRates();
                export = true;
            }

            if (chkTLSellRates.Checked)
            {
                ExportTLSellRates();
                export = true;
            }

            if (chkSmallPackRates.Checked)
            {
                ExportSmallPackRates();
                export = true;
            }

            if (chkCustomerServiceMarkups.Checked)
            {
                ExportCustomerServiceMarkups();
                export = true;
            }

            if (!export && ProcessingError != null)
                ProcessingError(this, new ViewEventArgs<string>("Please select an option prior to exporting"));
        }


        private void ImportLTLSellRates()
        {
            var rates = LTLSellRates ?? new List<LTLSellRateViewDto>();

            List<string[]> lines;
            try
            {
                lines = fupFile.GetImportData(true, chkFirstLineHeader.Checked);
            }
            catch (Exception ex)
            {
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(ex.Message));
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = chkFirstLineHeader.Checked ? 2 : 1;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 29;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }

            var vendorRatingSearch = new VendorRatingSearch();
            var vParameters = chks
                .Select(i => i.Line[4])
                .Distinct()
                .Select(n =>
                    {
                        var p = RatingSearchFields.VendorRatingName.ToParameterColumn();
                        p.DefaultValue = n;
                        p.Operator = Operator.Equal;
                        return p;
                    })
                .ToList();
            var vendorRatingList = vendorRatingSearch.FetchVendorRatings(new VendorRatingViewSearchCriteria { Parameters = vParameters }, ActiveUser.TenantId);
            var vendorRatingNames = vendorRatingList.Select(c => c.Name).ToList();
            var invalidVendorRatingNames = chks.CodesAreValid(4, vendorRatingNames, new Vendor().EntityName()).Select(m => m.Message).ToList();
            if (invalidVendorRatingNames.Any())
            {
                invalidVendorRatingNames.Add(WebApplicationConstants.NoRecordsImported);
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(string.Join(WebApplicationConstants.HtmlBreak, invalidVendorRatingNames.ToArray())));
                return;
            }


            var nRates = lines
                .Select(s => new LTLSellRateViewDto
                    {
                        LTLIndirectPointEnabled = s[0].ToBoolean(),
                        EffectiveDate = s[1].ToDateTime(),
                        MarkupPercent = s[2].ToDecimal(),
                        MarkupValue = s[3].ToDecimal(),
                        VendorRatingId = vendorRatingList.First(c => c.Name == s[4]).Id,
                        UseMinimum = s[5].ToBoolean(),
                        Active = s[6].ToBoolean(),
                        VendorRatingIsActive = vendorRatingList.First(c => c.Name == s[4]).Active,
						StartOverrideWeightBreak = (WeightBreak) s[7].ToInt(),
						OverrideMarkupPercentL5C = s[8]!=null? s[8].ToDecimal() : default(decimal),
						OverrideMarkupValueL5C = s[9] != null ? s[9].ToDecimal() : default(decimal),
						OverrideMarkupPercentM5C = s[10] != null ? s[10].ToDecimal() : default(decimal),
						OverrideMarkupValueM5C = s[11] != null ? s[11].ToDecimal() : default(decimal),
						OverrideMarkupPercentM1M = s[12] != null ? s[12].ToDecimal() : default(decimal),
						OverrideMarkupValueM1M = s[13] != null ? s[13].ToDecimal() : default(decimal),
						OverrideMarkupPercentM2M = s[14] != null ? s[14].ToDecimal() : default(decimal),
						OverrideMarkupValueM2M = s[15] != null ? s[15].ToDecimal() : default(decimal),
						OverrideMarkupPercentM5M = s[16] != null ? s[16].ToDecimal() : default(decimal),
						OverrideMarkupValueM5M = s[17] != null ? s[17].ToDecimal() : default(decimal),
						OverrideMarkupPercentM10M = s[18] != null ? s[18].ToDecimal() : default(decimal),
						OverrideMarkupValueM10M = s[19] != null ? s[19].ToDecimal() : default(decimal),
						OverrideMarkupPercentM20M = s[20]!=null? s[20].ToDecimal() : default(decimal),
						OverrideMarkupValueM20M = s[21]!=null? s[21].ToDecimal() : default(decimal),
						OverrideMarkupPercentM30M = s[22] != null ? s[22].ToDecimal() : default(decimal),
						OverrideMarkupValueM30M = s[23] != null ? s[23].ToDecimal() : default(decimal),
						OverrideMarkupPercentM40M = s[24] != null ? s[24].ToDecimal() : default(decimal),
						OverrideMarkupValueM40M = s[25] != null ? s[25].ToDecimal() : default(decimal),
						OverrideMarkupVendorFloorEnabled = s[26].ToBoolean(),
						OverrideMarkupPercentVendorFloor = s[27] != null ? s[27].ToDecimal() : default(decimal),
						OverrideMarkupValueVendorFloor = s[28] != null ? s[28].ToDecimal() : default(decimal)
						
						
                    })
                .ToList();
            rates.AddRange(nRates);

            LTLSellRates = rates;

            if (LtlSellRatesImported != null)
                LtlSellRatesImported(this, new ViewEventArgs<string>(ImportCompleteMsg));
        }

        private void ImportTLSellRates()
        {
            var rating = new CustomerRating(CustomerRatingId, false);
            var tlSellRates = TLSellRates ?? new List<TLSellRate>();

            List<string[]> lines;
            try
            {
                lines = fupFile.GetImportData(true, chkFirstLineHeader.Checked);
            }
            catch (Exception ex)
            {
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(ex.Message));
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = chkFirstLineHeader.Checked ? 2 : 1;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 18;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }

            var equipmentTypes = ProcessorVars.RegistryCache[ActiveUser.TenantId].EquipmentTypes;
            var equipmentTypeCodes = equipmentTypes.Select(c => c.Code).ToList();

            msgs.AddRange(chks.Where(c => !string.IsNullOrEmpty(c.Line[0])).CodesAreValid(0, equipmentTypeCodes, new EquipmentType().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }

            msgs.AddRange(chks.EnumsAreValid<RateType>(2).ToList());
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }

            msgs.AddRange(chks.EnumsAreValid<TLTariffType>(7).ToList());
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }

            var countries = ProcessorVars.RegistryCache.Countries.Values;
            var countryCodes = countries.Select(c => c.Code).ToList();

            msgs.AddRange(chks.Where(c => !string.IsNullOrEmpty(c.Line[10])).CodesAreValid(10, countryCodes, new Country().EntityName()));
            msgs.AddRange(chks.Where(c => !string.IsNullOrEmpty(c.Line[14])).CodesAreValid(14, countryCodes, new Country().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }

            var postalCodeSearch = new PostalCodeSearch();
            var postalCodes = new List<PostalCodeViewSearchDto>();
            var invalidPostalCodes = chks
                .Where(c => !string.IsNullOrEmpty(c.Line[11]) && !string.IsNullOrEmpty(c.Line[10]))
                .Where(i =>
                {
                    var code = postalCodeSearch.FetchPostalCodeByCode(i.Line[11], countries.First(c => c.Code == i.Line[10]).Id);
                    postalCodes.Add(code);
                    return code.Id == default(long);
                })
                .Select(i => string.Format(WebApplicationConstants.InvalidImportedCodeErrMsg, new PostalCode().EntityName(), i.Index))
                .ToList();
            invalidPostalCodes.AddRange(chks
                .Where(c => !string.IsNullOrEmpty(c.Line[15]) && !string.IsNullOrEmpty(c.Line[14]))
                .Where(i =>
                {
                    var code = postalCodeSearch.FetchPostalCodeByCode(i.Line[15], countries.First(c => c.Code == i.Line[14]).Id);
                    postalCodes.Add(code);
                    return code.Id == default(long);
                })
                .Select(i => string.Format(WebApplicationConstants.InvalidImportedCodeErrMsg, new PostalCode().EntityName(), i.Index))
                .ToList());
            if (invalidPostalCodes.Any())
            {
                invalidPostalCodes.Add(WebApplicationConstants.NoRecordsImported);
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(string.Join(WebApplicationConstants.HtmlBreak, invalidPostalCodes.ToArray())));
                return;
            }

            var mileageSources = ProcessorVars.RegistryCache[ActiveUser.TenantId].MileageSources;
            var mileageSourceCodes = mileageSources.Select(c => c.Code).ToList();

            msgs.AddRange(chks.Where(c => !string.IsNullOrEmpty(c.Line[17])).CodesAreValid(17, mileageSourceCodes, new MileageSource().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }


            var nTlSellRates = lines
                .Select(l =>
                {
                    var originPostalCode = !string.IsNullOrEmpty(l[11])
                                               ? !string.IsNullOrEmpty(l[10])
                                                     ? postalCodes.First(pc => pc.Code.ToLower() == l[11].ToLower() && pc.CountryId == countries.First(c => c.Code == l[10]).Id).Code
                                                     : l[11]
                                               : string.Empty;
                    var destPostalCode = !string.IsNullOrEmpty(l[15])
                                               ? !string.IsNullOrEmpty(l[14])
                                                     ? postalCodes.First(pc => pc.Code.ToLower() == l[15].ToLower() && pc.CountryId == countries.First(c => c.Code == l[14]).Id).Code
                                                     : l[15]
                                               : string.Empty;
                    var originCountryId = !string.IsNullOrEmpty(l[10]) ? countries.First(r => r.Code == l[10]).Id : default(long);
                    var destCountryId = !string.IsNullOrEmpty(l[14]) ? countries.First(r => r.Code == l[14]).Id : default(long);
                    var equipmentTypeId = !string.IsNullOrEmpty(l[0]) ? equipmentTypes.First(r => r.Code == l[0]).Id : default(long);
                    var mileageSourceId = !string.IsNullOrEmpty(l[17]) ? mileageSources.First(m => m.Code == l[17]).Id : default(long);
                    return new TLSellRate
                    {
                        EquipmentTypeId = equipmentTypeId,
                        EffectiveDate = l[1].ToDateTime(),
                        RateType = l[2].ToEnum<RateType>(),
                        Rate = l[3].ToDecimal(),
                        MinimumCharge = l[4].ToDecimal(),
                        MinimumWeight = l[5].ToDecimal(),
                        MaximumWeight = l[6].ToDecimal(),
                        TariffType = l[7].ToEnum<TLTariffType>(),
                        OriginCity = l[8],
                        OriginState = l[9],
                        OriginCountryId = originCountryId,
                        OriginPostalCode = originPostalCode,
                        DestinationCity = l[12],
                        DestinationState = l[13],
                        DestinationCountryId = destCountryId,
                        DestinationPostalCode = destPostalCode,
                        Mileage = l[16].ToDecimal(),
                        MileageSourceId = mileageSourceId,
                        TenantId = rating.TenantId,
                        CustomerRating = rating
                    };
                });

            tlSellRates.AddRange(nTlSellRates);
            TLSellRates = tlSellRates;

            if (TlSellRatesImported != null)
                TlSellRatesImported(this, new ViewEventArgs<string>(ImportCompleteMsg));
        }

        private void ImportSmallPackRates()
        {
            var rates = SmallPackRates ?? new List<SmallPackRateViewDto>();

            List<string[]> lines;
            try
            {
                lines = fupFile.GetImportData(true, chkFirstLineHeader.Checked);
            }
            catch (Exception ex)
            {
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(ex.Message));
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = chkFirstLineHeader.Checked ? 2 : 1;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 9;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }

            var types = ProcessorUtilities.GetAll<SmallPackageEngine>().Keys
                 .Select(type => type.ToString())
                 .ToList();
            types.Remove(SmallPackageEngine.None.ToInt().ToString());
            if (!lines.All(i => types.Contains(i[1])))
            {
                var invalidSmallPckEng = chks
                     .Where(i => !types.Contains(i.Line[1]))
                     .Select(i => string.Format(WebApplicationConstants.InvalidImportedEnumErrMsg, new SmallPackageEngine().EntityName(), i.Index))
                     .ToList();
                invalidSmallPckEng.Add(WebApplicationConstants.NoRecordsImported);
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(invalidSmallPckEng.ToArray().HtmlBreakJoin()));
                return;
            }

            var chargeCodes = ProcessorVars.RegistryCache[ActiveUser.TenantId].ChargeCodes;
            var chargeCodeCodes = chargeCodes.Select(c => c.Code).ToList();
            msgs.AddRange(chks.CodesAreValid(7, chargeCodeCodes, new ChargeCode().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }

            var vendorSearch = new VendorSearch();
            var vParameters = chks
                .Select(i => i.Line[0])
                .Distinct()
                .Select(n =>
                {
                    var p = AccountingSearchFields.VendorNumber.ToParameterColumn();
                    p.DefaultValue = n;
                    p.Operator = Operator.Equal;
                    return p;
                })
                .ToList();
            var vendorList = vendorSearch.FetchVendors(vParameters, ActiveUser.TenantId);
            var vendorNumbers = vendorList.Select(c => c.VendorNumber).ToList();
            var invalidVendorNumbers = chks.CodesAreValid(0, vendorNumbers, new Vendor().EntityName()).Select(m => m.Message).ToList();
            if (invalidVendorNumbers.Any())
            {
                invalidVendorNumbers.Add(WebApplicationConstants.NoRecordsImported);
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(invalidVendorNumbers.ToArray().HtmlBreakJoin()));
                return;
            }

            var serviceTypes = SmallPacksFactory.RetrieveServiceTypes();
            var invalidServiceTypes = chks
                .Where(i => !serviceTypes[i.Line[1].ToEnum<SmallPackageEngine>()].Contains(i.Line[2]))
                .Select(i => string.Format(InvalidServiceTypeErrMsg, i.Index))
                .ToList();
            if (invalidServiceTypes.Any())
            {
                invalidServiceTypes.Add(WebApplicationConstants.NoRecordsImported);
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(invalidServiceTypes.ToArray().HtmlBreakJoin()));
                return;
            }

            var nRates = lines
                .Select(s =>
                {
                    var @vendor = vendorList.First(v => v.VendorNumber == s[0]);
                    return new SmallPackRateViewDto
                    {
                        VendorName = vendor.Name,
                        VendorNumber = vendor.VendorNumber,
                        VendorId = vendor.Id,
                        SmallPackageEngine = s[1].ToEnum<SmallPackageEngine>(),
                        SmallPackType = s[2],
                        MarkupPercent = s[3].ToDecimal(),
                        MarkupValue = s[4].ToDecimal(),
                        EffectiveDate = s[5].ToDateTime(),
                        UseMinimum = s[6].ToBoolean(),
                        ChargeCodeId = chargeCodes.First(c => c.Code == s[7]).Id,
                        Active = s[8].ToBoolean(),
                    };
                })
                .ToList();
            rates.AddRange(nRates);

            SmallPackRates = rates;

            if (SmallPackRatesImported != null)
                SmallPackRatesImported(this, new ViewEventArgs<string>(ImportCompleteMsg));
        }

        private void ImportCustomerServiceMarkups()
        {
            var markUps = CustomerServiceMarkups ?? new List<CustomerServiceMarkup>();
            var rating = new CustomerRating(CustomerRatingId, false);

            List<string[]> lines;
            try
            {
                lines = fupFile.GetImportData(true, chkFirstLineHeader.Checked);
            }
            catch (Exception ex)
            {
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(ex.Message));
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = chkFirstLineHeader.Checked ? 2 : 1;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 8;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }

            var serviceCodes = ProcessorVars.RegistryCache[ActiveUser.TenantId].Services;
            var serviceCodeCodes = serviceCodes.Select(c => c.Code).ToList();
            msgs.AddRange(chks.CodesAreValid(0, serviceCodeCodes, new Service().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                if (ProcessingError != null)
                    ProcessingError(this, new ViewEventArgs<string>(msgs.Select(m => m.Message).ToArray().HtmlBreakJoin()));
                return;
            }

            var nMarkUps = lines
                .Select(s =>
                {
                    var @service = serviceCodes.First(c => c.Code == s[0]);
                    return new CustomerServiceMarkup
                    {
                        ServiceId = service.Id,
                        MarkupPercent = s[1].ToDecimal(),
                        MarkupValue = s[2].ToDecimal(),
                        ServiceChargeCeiling = s[3].ToDecimal(),
                        ServiceChargeFloor = s[4].ToDecimal(),
                        EffectiveDate = s[5].ToDateTime(),
                        UseMinimum = s[6].ToBoolean(),
                        Active = s[7].ToBoolean(),
                        CustomerRating = rating,
                        TenantId = rating.TenantId
                    };
                })
                .ToList();
            markUps.AddRange(nMarkUps);

            CustomerServiceMarkups = markUps;

            if (CustomerServiceMarkupsImported != null)
                CustomerServiceMarkupsImported(this, new ViewEventArgs<string>(ImportCompleteMsg));
        }


        private void ExportLTLSellRates()
        {
            var rating = new CustomerRating(CustomerRatingId);

            var lines = rating.LTLSellRates
                .Select(rate => new[]
				                	{
				                		rate.LTLIndirectPointEnabled.ToString(),
				                		rate.EffectiveDate.FormattedShortDate(),
				                		rate.MarkupPercent.ToString(),
				                		rate.MarkupValue.ToString(),
										rate.VendorRating.Name.ToString(),
										rate.UseMinimum.ToString(),
										rate.Active.ToString(),
										rate.StartOverrideWeightBreak.ToString(),
										
										rate.OverrideMarkupPercentL5C.ToString(),
										rate.OverrideMarkupValueL5C.ToString(),
										rate.OverrideMarkupPercentM5C.ToString(), 
										rate.OverrideMarkupValueM5C.ToString(), 
										rate.OverrideMarkupPercentM1M.ToString(), 
										rate.OverrideMarkupValueM1M.ToString(),
										rate.OverrideMarkupPercentM2M.ToString(),
										rate.OverrideMarkupValueM2M.ToString(), 
										rate.OverrideMarkupPercentM5M.ToString(), 
										rate.OverrideMarkupValueM5M.ToString(),
										rate.OverrideMarkupPercentM10M.ToString(),
										rate.OverrideMarkupValueM10M.ToString(),
										rate.OverrideMarkupPercentM20M.ToString(),
										rate.OverrideMarkupValueM20M.ToString(),
										rate.OverrideMarkupPercentM30M.ToString(),
										rate.OverrideMarkupValueM30M.ToString(),
										rate.OverrideMarkupPercentM40M.ToString(),
										rate.OverrideMarkupValueM40M.ToString(),

										rate.OverrideMarkupVendorFloorEnabled.ToString(),
										rate.OverrideMarkupPercentVendorFloor.ToString(),
										rate.OverrideMarkupValueVendorFloor.ToString()
				                	}.TabJoin())
                .ToList();
	        lines.Insert(0, new[]
		        {
			        "Indirect Pt Enabled", "Effective Date", "Markup %", "Markup Val.",
			        "Vendor Rating", "Use Lower", "Active", "Override Markup Weight", "L5CPercentage", "L5CValue",
			        "M5CPercentage", "M5CValue", "M1MPercentage", "M1MValue", "M2MPercentage", "M2MValue", "M5MPercentage",
			        "M5MValue", "M10MPercentage", "M10MValue", "M20MPercentage", "M20MValue", "M30MPercentage", "M30MValue",
			        "M40MPercentage", "M40MValue", "OverrideMarkupVendorFloorEnabled", "OverrideMarkupVendorFloorPercentage", "OverrideMarkupVendorFloorValue"
		        }.TabJoin());

            Response.Export(lines.ToArray().NewLineJoin(), Guid.NewGuid().ToString() + ".txt");
        }

        private void ExportTLSellRates()
        {
            var rating = new CustomerRating(CustomerRatingId);

            var lines = rating.TLSellRates
                .Select(rate => new[]
				                	{
                                        rate.EquipmentTypeId != default(long) ? rate.EquipmentType.Code : string.Empty,
				                		rate.EffectiveDate.FormattedShortDate(),
                                        rate.RateType.ToString(),
                                        rate.Rate.ToString(),
                                        rate.MinimumCharge.ToString(),
                                        rate.MinimumWeight.ToString(),
                                        rate.MaximumWeight.ToString(),
                                        rate.TariffType.ToString(),
                                        rate.OriginCity.GetString(),
                                        rate.OriginState.GetString(),
                                        rate.OriginCountryId != default(long) ? rate.OriginCountry.Code : string.Empty,
                                        rate.OriginPostalCode.GetString(),
                                        rate.DestinationCity.GetString(),
                                        rate.DestinationState.GetString(),
                                        rate.DestinationCountryId != default(long) ? rate.DestinationCountry.Code : string.Empty,
                                        rate.DestinationPostalCode.GetString(),
                                        rate.Mileage.ToString(),
                                        rate.MileageSourceId != default(long) ? rate.MileageSource.Code : string.Empty
				                	}.TabJoin())
                .ToList();
            lines.Insert(0, new[]
			                	{
			                		"Equipment Type","Effective Date","Rate Type","Rate","Minimum Charge","Minimum Weight"
                                    ,"Maximum Weight","Tariff Type","Origin City","Origin State","Origin Country"
                                    ,"Origin Postal Code","Destination City","Destination State","Destination Country"
                                    ,"Destination Postal Code", "Mileage", "Mileage Source"
								}.TabJoin());

            Response.Export(lines.ToArray().NewLineJoin(), Guid.NewGuid().ToString() + ".txt");
        }

        private void ExportSmallPackRates()
        {
            var rating = new CustomerRating(CustomerRatingId);

            var lines = rating.SmallPackRates
                .Select(sellRate => new[]
				                	{
				                		sellRate.Vendor.VendorNumber.ToString(),
				                		sellRate.SmallPackageEngine.ToInt().ToString(),
				                		sellRate.SmallPackType,
				                		sellRate.MarkupPercent.ToString(),
										sellRate.MarkupValue.ToString(),
				                		sellRate.EffectiveDate.FormattedShortDate(),
										sellRate.UseMinimum.ToString(),
										sellRate.ChargeCode.Code.ToString(),
										sellRate.Active.ToString()
				                	}.TabJoin())
                .ToList();
            lines.Insert(0, new[] { "Vendor Number", "Small Pack Engine", "Small Pack Type", "Markup %", "Markup Val.", "Effective", "Use Lower", "Charge Code", "Active" }.TabJoin());

            Response.Export(lines.ToArray().NewLineJoin(), Guid.NewGuid().ToString() + ".txt");
        }

        private void ExportCustomerServiceMarkups()
        {
            var rating = new CustomerRating(CustomerRatingId);

            var lines = rating.CustomerServiceMarkups
                .Select(markUp => new[]
				                	{
				                		markUp.Service.Code.ToString(),
				                		markUp.MarkupPercent.ToString(),
										markUp.MarkupValue.ToString(),
										markUp.ServiceChargeCeiling.ToString(),
										markUp.ServiceChargeFloor.ToString(),
				                		markUp.EffectiveDate.FormattedShortDate(),
										markUp.UseMinimum.ToString(),
										markUp.Active.ToString()
				                	}.TabJoin())
                .ToList();
            lines.Insert(0, new[] { "Service Code", "Markup %", "Markup Val.", "Charge Ceiling", "Charge Floor", "Effective", "Use Lower", "Active" }.TabJoin());

            Response.Export(lines.ToArray().NewLineJoin(), Guid.NewGuid().ToString() + ".txt");
        }
    }
}