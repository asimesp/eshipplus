﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VendorRatingImportExportControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Rating.Controls.VendorRatingImportExportControl" %>
<asp:Panel runat="server" ID="pnlVendorRatingImportExport" CssClass="popupControl popupControlOverW750">
    <div class="popheader">
        <h4>Vendor Rating Import/Export
             <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                 CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
        </h4>
    </div>

    <div class="pl10 pt5">
        <p class="note">&#8226; Please refer to the help menu for the import file format. </p>
    </div>
    <div class="rowgroup pl10 mb0">
        <div class="col_1_2 bbox">
            <h5>Options</h5>
            <div class="row">
                <script type="text/javascript">
                    $(document).ready(function () {
                        // enforce mutual exclusion on checkboxes in ulVendorRatingImportOptions
                        $(jsHelper.AddHashTag('ulVendorRatingImportOptions input:checkbox')).click(function () {
                            if (jsHelper.IsChecked($(this).attr('id'))) {
                                $(jsHelper.AddHashTag('ulVendorRatingImportOptions input:checkbox')).not(jsHelper.AddHashTag($(this).attr('id'))).each(function () {
                                    jsHelper.UnCheckBox($(this).attr('id'));
                                });
                                $.uniform.update();
                            }
                        });
                    });
                </script>
                <ul class="twocol_list" id="ulVendorRatingImportOptions">
                    <li>
                        <asp:CheckBox runat="server" ID="chkLTLAccessorials" CssClass="jQueryUniform" />
                        <label>LTL Accessorials</label>
                    </li>
                    <li>
                        <asp:CheckBox runat="server" ID="chkAdditionalCharges" CssClass="jQueryUniform" />
                        <label>LTL Additional Charges</label>
                    </li>
                    <li>
                        <asp:CheckBox runat="server" ID="chkCFCRules" CssClass="jQueryUniform" />
                        <label>LTL C.F. Capacity Rules</label>
                    </li>
                    <li>
                        <asp:CheckBox runat="server" ID="chkDiscountTiers" CssClass="jQueryUniform" />
                        <label>LTL Discount Tiers</label>
                    </li>
                    <li>
                        <asp:CheckBox runat="server" ID="chkLTLPackageSpecificRates" CssClass="jQueryUniform" />
                        <label>LTL Package Specific Rates</label>
                    </li>
                    <li>
                        <asp:CheckBox runat="server" ID="chkLTLOverlengthRules" CssClass="jQueryUniform" />
                        <label>LTL Overlength Rules</label>
                    </li>
                    <li>
                        <asp:CheckBox runat="server" ID="chkLTLGuaranteedCharges" CssClass="jQueryUniform" />
                        <label>LTL Guaranteed Charges</label>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col_1_2 bbox pl10 vlinedarkleft">
            <h5>File</h5>
            <div class="row">
                <div class="fieldgroup">
                    <asp:FileUpload runat="server" ID="fupFile" CssClass="jQueryUniform" />
                </div>
            </div>
            <div class="row">
                <div class="fieldgroup pt10 pb40 mb10">
                    <asp:CheckBox runat="server" ID="chkFirstLineHeader" Checked="true" CssClass="jQueryUniform" />
                    <label>First line contains header</label>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="fieldgroup right mb10 pt10">
            <asp:Button ID="btnImport" OnClick="OnImportClicked" runat="server" Text="IMPORT" CausesValidation="false" />
            <asp:Button ID="btnExport" OnClick="OnExportClicked" runat="server" Text="EXPORT" CausesValidation="false" />
            <asp:Button ID="btnClose" OnClick="OnCloseClicked" runat="server" Text="CLOSE" CausesValidation="false" />
        </div>
    </div>
</asp:Panel>
<eShip:CustomHiddenField runat="server" ID="hidVendorRatingId" />
