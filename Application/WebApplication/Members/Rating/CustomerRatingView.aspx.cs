﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Dto.Rating;
using LogisticsPlus.Eship.Processor.Handlers.Rating;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;
using LogisticsPlus.Eship.Project44;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;
using P44SDK.V4.Model;

namespace LogisticsPlus.Eship.WebApplication.Members.Rating
{
    public partial class CustomerRatingView : MemberPageBaseWithPageStore, ICustomerRatingView
    {
        private const string LtlSellRatesHeader = "LTL Sell Rates";
        private const string TlSellRatesHeader = "TL Sell Rates";
        private const string SmallPackRatesHeader = "Small Pack Rates";
        private const string CustomerServiceMarkupHeader = "Customer Service Markup";

        private const string SaveFlag = "S";
        private const string BatchInsert = "BatchInsert";
        private const string BatchRemove = "BatchRemove";

        private const string TlSellRateStoreKey = "TLSRSK";
        private const string LtlSellRateStoreKey = "LTLSRSK";
        private const string SmallPackRateStoreKey = "SPRSK";
        private const string CustomerServiceMarkupStoreKey = "CSMSK";


        private List<CustomerServiceMarkup> StoredCustomerServiceMarkups
        {
            get { return PageStore[CustomerServiceMarkupStoreKey] as List<CustomerServiceMarkup> ?? new List<CustomerServiceMarkup>(); }
            set { PageStore[CustomerServiceMarkupStoreKey] = value; }
        }

        private List<LTLSellRateViewDto> StoredLtlSellRates
        {
            get { return PageStore[LtlSellRateStoreKey] as List<LTLSellRateViewDto> ?? new List<LTLSellRateViewDto>(); }
            set { PageStore[LtlSellRateStoreKey] = value; }
        }

        private List<SmallPackRateViewDto> StoredSmallPackRates
        {
            get { return PageStore[SmallPackRateStoreKey] as List<SmallPackRateViewDto> ?? new List<SmallPackRateViewDto>(); }
            set { PageStore[SmallPackRateStoreKey] = value; }
        }

        private List<TLSellRate> StoredTLSellRates
        {
            get { return PageStore[TlSellRateStoreKey] as List<TLSellRate> ?? new List<TLSellRate>(); }
            set { PageStore[TlSellRateStoreKey] = value; }
        }


        public static string PageAddress { get { return "~/Members/Rating/CustomerRatingView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.CustomerRating; } }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RatingBlue; }
        }

        protected List<ViewListItem> ViewVendorRatings
        {
            get { return ViewState["CutomerRatingVendorRatingList"] as List<ViewListItem> ?? new List<ViewListItem>(); }
        }

        public List<ResellerAddition> ResellerAdditions
        {
            set
            {
                var resellerAdd = value
                    .Select(s => new ViewListItem(s.Name, s.Id.ToString()))
                    .OrderBy(i => i.Text)
                    .ToList();
                resellerAdd.Insert(0, new ViewListItem(WebApplicationConstants.ChooseOne, 0.ToString()));

                ddlResellerAddition.DataSource = resellerAdd;
                ddlResellerAddition.DataBind();
            }
        }

        public List<VendorRating> VendorRatings
        {
            set
            {
                var ratings = value
                .Select(s => new ViewListItem(s.Name, s.Id.ToString()))
                .ToList();
                ratings.Insert(0, new ViewListItem(WebApplicationConstants.ChooseOne, 0.ToString()));

                ViewState["CutomerRatingVendorRatingList"] = ratings;
            }
        }


        public Dictionary<int, string> RateType
        {
            set
            {
                var rateTypes = value.Select(t => new ViewListItem(t.Value, t.Key.ToString())).ToList();

                ddlTLSellRateRateType.DataSource = rateTypes;
                ddlTLSellRateRateType.DataBind();

                ddlTLFuelRateType.DataSource = rateTypes;
                ddlTLFuelRateType.DataBind();
            }
        }

        public Dictionary<int, string> SmallPackEngines
        {
            set
            {
                var engines = value.Select(t => new ViewListItem(t.Value, t.Key.ToString())).ToList();
                engines.Insert(0, new ViewListItem(WebApplicationConstants.ChooseOne, 0.ToString()));
                ddlSmallPackRateSmallPackEngine.DataSource = engines;
                ddlSmallPackRateSmallPackEngine.DataBind();
            }
        }

        public Dictionary<SmallPackageEngine, List<string>> SmallPackTypes
        {
            get
            {
                return ViewState["CustomerRatingsSmallPackTypes"] as Dictionary<SmallPackageEngine, List<string>> ??
                       new Dictionary<SmallPackageEngine, List<string>>();
            }
            set
            {
                ViewState["CustomerRatingsSmallPackTypes"] = value;
                SetSmallPackTypes();
            }
        }

        public Dictionary<int, string> TariffType
        {
            set
            {
                var tariffTypes = value.Select(t => new ViewListItem(t.Value, t.Key.ToString())).ToList();

                ddlTLSellRateTariffType.DataSource = tariffTypes;
                ddlTLSellRateTariffType.DataBind();
            }
        }

        public Dictionary<int, string> ValuePercentageTypes
        {
            set
            {
                ddlLineHaulCeilingType.DataSource = value;
                ddlLineHaulCeilingType.DataBind();

                ddlLineHaulFloorType.DataSource = value;
                ddlLineHaulFloorType.DataBind();

                ddlFuelCeilingType.DataSource = value;
                ddlFuelCeilingType.DataBind();

                ddlFuelFloorType.DataSource = value;
                ddlFuelFloorType.DataBind();

                ddlAccessorialCeilingType.DataSource = value;
                ddlAccessorialCeilingType.DataBind();

                ddlAccessorialFloorType.DataSource = value;
                ddlAccessorialFloorType.DataBind();

                ddlServiceCeilingType.DataSource = value;
                ddlServiceCeilingType.DataBind();

                ddlServiceFloorType.DataSource = value;
                ddlServiceFloorType.DataBind();
            }
        }

		public Dictionary<int, string> WeightBreaks
		{
			set
			{
				ddlStartOverrideMarkupWeightBreak.DataSource = value;
				ddlStartOverrideMarkupWeightBreak.DataBind();
			}
		}


        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<CustomerRating>> Save;
        public event EventHandler<ViewEventArgs<CustomerRating>> Delete;
        public event EventHandler<ViewEventArgs<CustomerRating>> Lock;
        public event EventHandler<ViewEventArgs<CustomerRating>> UnLock;
        public event EventHandler<ViewEventArgs<CustomerRating>> LoadAuditLog;
        public event EventHandler<ViewEventArgs<string>> VendorSearch;
        public event EventHandler<ViewEventArgs<string>> CustomerSearch;

        public void DisplayCustomer(Customer customer)
        {
            txtCustomerNumber.Text = customer == null ? string.Empty : customer.CustomerNumber;
            txtCustomerName.Text = customer == null ? string.Empty : customer.Name;
            hidCustomerId.Value = customer == null ? string.Empty : customer.Id.ToString();

            memberToolBar.LoadAdditionalActions(ToolbarMoreActions());
        }

        public void DisplayVendor(Vendor vendor)
        {
            txtSmallPackRateVendorNumber.Text = vendor == null ? string.Empty : vendor.VendorNumber;
            txtSmallPackRateVendorName.Text = vendor == null ? string.Empty : vendor.Name;
            hidSmallPackRateVendorId.Value = vendor == null ? string.Empty : vendor.Id.ToString();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == SaveFlag)
            {
                // unlock and return to read-only
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<CustomerRating>(new CustomerRating(hidCustomerRatingId.Value.ToLong(), false)));
                SetEditStatus(false);
                memberToolBar.ShowUnlock = false;
            }

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.Select(m => m.Message).ToArray());
            messageBox.Visible = true;

            litErrorMessages.Text = messages.HasErrors()
                                        ? string.Join(WebApplicationConstants.HtmlBreak,
                                                      messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
                                                        Select(m => m.Message).ToArray())
                                        : string.Empty;
        }

        public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs)
        {
            auditLogs.Load(logs);
        }

        public void FailedLock(Lock @lock)
        {
            memberToolBar.ShowNew = false;
            memberToolBar.ShowSave = false;
            memberToolBar.ShowDelete = false;
            memberToolBar.ShowUnlock = false;

            SetEditStatus(false);

            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void Set(CustomerRating rating)
        {
            LoadCustomerRating(rating);
            SetEditStatus(!rating.IsNew);
        }


        private static List<TLSellRate> SortTlSellRates(IEnumerable<TLSellRate> tlSellRates)
        {
            return tlSellRates
                .OrderBy(t => t.EffectiveDate)
                .ThenBy(t => t.OriginCity)
                .ThenBy(t => t.OriginState)
                .ThenBy(t => t.OriginPostalCode)
                .ThenBy(t => t.DestinationCity)
                .ThenBy(t => t.DestinationState)
                .ThenBy(t => t.DestinationPostalCode)
                .ToList();
        }

        private static List<SmallPackRateViewDto> SortSmallPackRates(IEnumerable<SmallPackRateViewDto> smallPackRates)
        {
            return smallPackRates
                .OrderBy(s => s.SmallPackageEngine)
                .ThenBy(s => s.SmallPackType)
                .ToList();
        }

        private static List<CustomerServiceMarkup> SortCustomerServiceMarkUps(IEnumerable<CustomerServiceMarkup> customerServiceMarkUps)
        {
			return customerServiceMarkUps
				.OrderBy(s => s.FormattedString())
				.ThenByDescending(s => s.EffectiveDate)
				.ToList();
        }

        private List<LTLSellRateViewDto> SortLtlSellRates(IEnumerable<LTLSellRateViewDto> ltlSellRates)
        {
            return ltlSellRates
                .OrderBy(s => ViewVendorRatings.GetValueText(s.VendorRatingId.GetString()))
                .ToList();
        }


        private void UpdateLtlSellRatesFromView(CustomerRating customerRating)
        {
	        customerRating.LTLSellRates = StoredLtlSellRates
		        .Select(l => new LTLSellRate(l.Id, l.Id != default(long))
			        {
				        LTLIndirectPointEnabled = l.LTLIndirectPointEnabled,
				        EffectiveDate = l.EffectiveDate,
				        MarkupPercent = l.MarkupPercent,
				        MarkupValue = l.MarkupValue,
				        CustomerRating = customerRating,
				        VendorRatingId = l.VendorRatingId,
				        UseMinimum = l.UseMinimum,
				        Active = l.Active,
				        TenantId = customerRating.TenantId,
				        OverrideMarkupValueL5C = l.OverrideMarkupValueL5C,
				        OverrideMarkupPercentL5C = l.OverrideMarkupPercentL5C,
				        OverrideMarkupValueM5C = l.OverrideMarkupValueM5C,
						OverrideMarkupPercentM5C = l.OverrideMarkupPercentM5C,
				        OverrideMarkupValueM1M = l.OverrideMarkupValueM1M,
						OverrideMarkupPercentM1M = l.OverrideMarkupPercentM1M,
				        OverrideMarkupValueM2M = l.OverrideMarkupValueM2M,
						OverrideMarkupPercentM2M = l.OverrideMarkupPercentM2M,
				        OverrideMarkupValueM5M = l.OverrideMarkupValueM5M,
						OverrideMarkupPercentM5M = l.OverrideMarkupPercentM5M,
				        OverrideMarkupValueM10M = l.OverrideMarkupValueM10M,
						OverrideMarkupPercentM10M = l.OverrideMarkupPercentM10M,
				        OverrideMarkupValueM20M = l.OverrideMarkupValueM20M,
						OverrideMarkupPercentM20M = l.OverrideMarkupPercentM20M,
				        OverrideMarkupValueM30M = l.OverrideMarkupValueM30M,
						OverrideMarkupPercentM30M = l.OverrideMarkupPercentM30M,
				        OverrideMarkupValueM40M = l.OverrideMarkupValueM40M,
						OverrideMarkupPercentM40M = l.OverrideMarkupPercentM40M,
						StartOverrideWeightBreak = l.StartOverrideWeightBreak,
                        OverrideMarkupPercentVendorFloor = l.OverrideMarkupPercentVendorFloor,
                        OverrideMarkupValueVendorFloor = l.OverrideMarkupValueVendorFloor,
                        OverrideMarkupVendorFloorEnabled = l.OverrideMarkupVendorFloorEnabled
			        })
		        .ToList();
        }

        private void UpdateSmallPackRatesFromView(CustomerRating customerRating)
        {
            customerRating.SmallPackRates = StoredSmallPackRates
                .Select(s => new SmallPackRate(s.Id, s.Id != default(long))
                    {
                        SmallPackageEngine = s.SmallPackageEngine,
                        SmallPackType = s.SmallPackType,
                        MarkupPercent = s.MarkupPercent,
                        MarkupValue = s.MarkupValue,
                        EffectiveDate = s.EffectiveDate,
                        UseMinimum = s.UseMinimum,
                        Active = s.Active,
                        CustomerRating = customerRating,
                        VendorId = s.VendorId,
                        ChargeCodeId = s.ChargeCodeId,
                        TenantId = customerRating.TenantId
                    }).ToList();
        }

        private void UpdateCustomerServiceMarkUpsFromView(CustomerRating customerRating)
        {
            customerRating.CustomerServiceMarkups = StoredCustomerServiceMarkups;
            if (customerRating.IsNew)
                foreach (var markup in customerRating.CustomerServiceMarkups)
                {
                    markup.TenantId = customerRating.TenantId;
                    markup.CustomerRating = customerRating;
                }
        }

        private void UpdateTLSellRatesFromView(CustomerRating customerRating)
        {
            customerRating.TLSellRates = StoredTLSellRates;

            if (customerRating.IsNew)
                foreach (var tlSellRate in customerRating.TLSellRates)
                {
                    tlSellRate.TenantId = customerRating.TenantId;
                    tlSellRate.CustomerRating = customerRating;
                }
        }


        private void SetSmallPackTypes()
        {
            var engine = ddlSmallPackRateSmallPackEngine.SelectedValue.ToEnum<SmallPackageEngine>();

            ddlSmallPackRateSmallPackType.DataSource = SmallPackTypes.ContainsKey(engine) ? SmallPackTypes[engine] : new List<string>();
            ddlSmallPackRateSmallPackType.DataBind();
        }

        private List<ToolbarMoreAction> ToolbarMoreActions()
        {
            var import = new ToolbarMoreAction
            {
                CommandArgs = BatchInsert,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Rating,
                Name = "Process Customer Rating Item(s) Addition/Removal",
                IsLink = false,
                NavigationUrl = string.Empty
            };

            return new List<ToolbarMoreAction> { import };
        }

        private void SetEditStatus(bool enabled)
        {
            pnlDetails.Enabled = enabled;
            pnlLTLSellRates.Enabled = enabled;
            pnlSmallPackRates.Enabled = enabled;
            pnlCustomerServiceMarkUps.Enabled = enabled;
            pnlTLSellRates.Enabled = enabled;

            memberToolBar.EnableSave = enabled;
            memberToolBar.EnableImport = enabled;
            customerRatingImportExport.EnableImport = enabled;

            litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;

            // must do this here to load correct set of extensible actions
            memberToolBar.LoadAdditionalActions(ToolbarMoreActions());
        }

        private void ProcessTransferredRequest(CustomerRating rating)
        {
            if (rating.IsNew) return;

            LoadCustomerRating(rating);

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<CustomerRating>(rating));
        }

        private void LoadCustomerRating(CustomerRating customerRating)
        {
            if (!customerRating.IsNew) customerRating.LoadCollections();

            hidCustomerRatingId.Value = customerRating.Id.ToString();
            customerRatingImportExport.CustomerRatingId = customerRating.Id;

            DisplayCustomer(customerRating.Customer);

            chkBillReseller.Checked = customerRating.BillReseller;
            ddlResellerAddition.SelectedValue = customerRating.ResellerAdditionId.ToString();

            ddlProject44AccountGroup.SelectedValue = customerRating.Project44AccountGroup;

            //Insurance
            chkInsuranceEnabled.Checked = customerRating.InsuranceEnabled;
            txtInsurancePurchaseFloor.Text = customerRating.InsurancePurchaseFloor.ToString();

			ddlInsuranceChargeCode.SelectedValue = customerRating.InsuranceChargeCodeId.GetString();
            //LINEHAUL
            chkLineHaulNoProfit.Checked = customerRating.NoLineHaulProfit;
            ddlLineHaulCeilingType.SelectedValue = ((int)customerRating.LineHaulProfitCeilingType).ToString();
            txtLineHaulCeilingPercentage.Text = customerRating.LineHaulProfitCeilingPercentage.ToString();
            txtLineHaulCeilingValue.Text = customerRating.LineHaulProfitCeilingValue.ToString();
            chkLineHaulUseMinimumCeiling.Checked = customerRating.UseLineHaulMinimumCeiling;
            ddlLineHaulFloorType.SelectedValue = ((int)customerRating.LineHaulProfitFloorType).ToString();
            txtLineHaulFloorPercentage.Text = customerRating.LineHaulProfitFloorPercentage.ToString();
            txtLineHaulFloorValue.Text = customerRating.LineHaulProfitFloorValue.ToString();
            chkLineHaulUseMinimumFloor.Checked = customerRating.UseLineHaulMinimumFloor;

            //FUEL
            chkFuelNoProfit.Checked = customerRating.NoFuelProfit;
            ddlFuelCeilingType.SelectedValue = ((int)(customerRating.FuelProfitCeilingType)).ToString();
            txtFuelCeilingPercentage.Text = customerRating.FuelProfitCeilingPercentage.ToString();
            txtFuelCeilingValue.Text = customerRating.FuelProfitCeilingValue.ToString();
            chkFuelUseMinimumCeiling.Checked = customerRating.UseFuelMinimumCeiling;
            ddlFuelFloorType.SelectedValue = ((int)customerRating.FuelProfitFloorType).ToString();
            txtFuelFloorPercentage.Text = customerRating.FuelProfitFloorPercentage.ToString();
            txtFuelFloorValue.Text = customerRating.FuelProfitFloorValue.ToString();
            chkFuelUseMinimumFloor.Checked = customerRating.UseFuelMinimumFloor;

            //ACCESSORIAL
            chkAccessorialNoProfit.Checked = customerRating.NoAccessorialProfit;
            ddlAccessorialCeilingType.SelectedValue = ((int)customerRating.AccessorialProfitCeilingType).ToString();
            txtAccessorialCeilingPercentage.Text = customerRating.AccessorialProfitCeilingPercentage.ToString();
            txtAccessorialCeilingValue.Text = customerRating.AccessorialProfitCeilingValue.ToString();
            chkAccessorialUseMinimumCeiling.Checked = customerRating.UseAccessorialMinimumCeiling;
            ddlAccessorialFloorType.SelectedValue = ((int)customerRating.AccessorialProfitFloorType).ToString();
            txtAccessorialFloorPercentage.Text = customerRating.AccessorialProfitFloorPercentage.ToString();
            txtAccessorialFloorValue.Text = customerRating.AccessorialProfitFloorValue.ToString();
            chkAccessorialUseMinimumFloor.Checked = customerRating.UseAccessorialMinimumFloor;

            //SERVICE
            chkServiceNoProfit.Checked = customerRating.NoServiceProfit;
            ddlServiceCeilingType.SelectedValue = ((int)customerRating.ServiceProfitCeilingType).ToString();
            txtServiceCeilingPercentage.Text = customerRating.ServiceProfitCeilingPercentage.ToString();
            txtServiceCeilingValue.Text = customerRating.ServiceProfitCeilingValue.ToString();
            chkServiceUseMinimumCeiling.Checked = customerRating.UseServiceMinimumCeiling;
            ddlServiceFloorType.SelectedValue = ((int)customerRating.ServiceProfitFloorType).ToString();
            txtServiceFloorPercentage.Text = customerRating.ServiceProfitFloorPercentage.ToString();
            txtServiceFloorValue.Text = customerRating.ServiceProfitFloorValue.ToString();
            chkServiceUseMinimumFloor.Checked = customerRating.UseServiceMinimumFloor;

            //LTL
            var ltlSellRates = SortLtlSellRates(new LTLSellRateViewDto().RetrieveCustomerRatingLTLSellRates(customerRating));
            StoredLtlSellRates = ltlSellRates;

            lstLTLSellRates.DataSource = ltlSellRates;
            lstLTLSellRates.DataBind();
            tabLTLSellRates.HeaderText = customerRating.LTLSellRates.BuildTabCount(LtlSellRatesHeader);

            // TL
            var tlSellRates = SortTlSellRates(customerRating.TLSellRates);
            StoredTLSellRates = tlSellRates;
            peLstTLSellRates.LoadData(tlSellRates);

            tabTLSellRates.HeaderText = customerRating.TLSellRates.BuildTabCount(TlSellRatesHeader);

            txtTLFuelRate.Text = customerRating.TLFuelRate == default(decimal) ? string.Empty : customerRating.TLFuelRate.ToString();
            ddlTLFuelRateType.SelectedValue = customerRating.TLFuelRateType.ToInt().ToString();
			ddlTLFuelChargeCode.SelectedValue = customerRating.TLFuelChargeCodeId.GetString();

			ddlTLFreightChargeCode.SelectedValue = customerRating.TLFreightChargeCodeId.GetString();

            //Small Pack
            var smallPackRates = SortSmallPackRates(new SmallPackRateViewDto().RetrieveCustomerRatingSmallPackRates(customerRating));
            StoredSmallPackRates = smallPackRates;

            lstSmallPackRates.DataSource = smallPackRates;
            lstSmallPackRates.DataBind();
            tabSmallPackRates.HeaderText = smallPackRates.BuildTabCount(SmallPackRatesHeader);

            //Customer Service Markups
            var markups = SortCustomerServiceMarkUps(customerRating.CustomerServiceMarkups);
            StoredCustomerServiceMarkups = markups;

            lstCustomerServiceMarkUps.DataSource = markups;
            lstCustomerServiceMarkUps.DataBind();
            tabCustomerServiceMarkUps.HeaderText = markups.BuildTabCount(CustomerServiceMarkupHeader);

            memberToolBar.ShowUnlock = customerRating.HasUserLock(ActiveUser, customerRating.Id);
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            new CustomerRatingHandler(this).Initialize();

            memberToolBar.ShowSave = Access.Modify;
            memberToolBar.ShowDelete = Access.Remove;
            memberToolBar.ShowNew = Access.Modify;
            memberToolBar.ShowEdit = Access.Modify;
            memberToolBar.ShowExport = Access.Grant;
            memberToolBar.ShowImport = Access.Modify;
            memberToolBar.LoadAdditionalActions(ToolbarMoreActions());

            //set page store connections
            customerRatingImportExport.LTLSellRateDataStoreKey = LtlSellRateStoreKey;
            customerRatingImportExport.SmallPackRateDataStoreKey = SmallPackRateStoreKey;
            customerRatingImportExport.CustomerServiceMarkupDataStoreKey = CustomerServiceMarkupStoreKey;
            customerRatingImportExport.TLSellRateDataStoreKey = TlSellRateStoreKey;

            peLstTLSellRates.ParentDataStoreKey = TlSellRateStoreKey;

            customerRatingFinder.OpenForEditEnabled = Access.Modify;

            if (IsPostBack) return;


            var p44Settings = new Project44ServiceSettings
            {
                Hostname = WebApplicationSettings.Project44Host,
                Username = WebApplicationSettings.Project44Username,
                Password = WebApplicationSettings.Project44Password,
                PrimaryLocationId = WebApplicationSettings.Project44PrimaryLocationId
            };
            var p44Wrapper = new Project44Wrapper(p44Settings);
            var capacityProviderGroupCollection = p44Wrapper.GetCapacityProviderGroups();


            var capacityProviderGroups = capacityProviderGroupCollection != null 
                                            ? capacityProviderGroupCollection.Groups
                                            : new List<CapacityProviderAccountGroupInfo>();

            var groupList = capacityProviderGroups.Select(g => new ViewListItem(string.Format("{0} - {1}", g.Code, g.Name), g.Code)).OrderBy(g => g.Text).ToList();
            groupList.Insert(0, new ViewListItem(WebApplicationConstants.NotApplicable, string.Empty));

            ddlProject44AccountGroup.DataSource = groupList;
            ddlProject44AccountGroup.DataBind();

            aceCustomer.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.ActiveUser, ActiveUser.Id.GetString() } });
            aceVendor.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString() } });

            memberToolBar.LoadTemplates(new List<string>
                        {
                            WebApplicationConstants.CustomerRatingBatchInsertCustomerServiceMarkupTemplate,
                            WebApplicationConstants.CustomerRatingBatchInsertLTLSellRatesImportTemplate,
                            WebApplicationConstants.CustomerRatingBatchInsertSmallPackageRatesImportTemplate,
                            WebApplicationConstants.CustomerRatingBatchInsertTLSellRatesImportTemplate,
							WebApplicationConstants.CustomerRatingBatchInsertProfitImportTemplate,

                            WebApplicationConstants.CustomerRatingBatchRemoveCustomerServiceMarkupImportTemplate,
                            WebApplicationConstants.CustomerRatingBatchRemoveLTLSellRatesImportTemplate,
                            WebApplicationConstants.CustomerRatingBatchRemoveSmallPackageRatesImportTemplate,
                            WebApplicationConstants.CustomerRatingBatchRemoveTLSellRatesImportTemplate,

                            WebApplicationConstants.CustomerRatingLTLSellRateImportTemplate,
                            WebApplicationConstants.CustomerRatingCustomerServiceMarkupImportTemplate, 
                            WebApplicationConstants.CustomerRatingSmallPackageRatesImportTemplate,
                            WebApplicationConstants.CustomerRatingTLSellRateImportTemplate,
                        });
            if (Loading != null)
                Loading(this, new EventArgs());

            if (Session[WebApplicationConstants.TransferCustomerRatingId] != null)
            {
                ProcessTransferredRequest(new CustomerRating(Session[WebApplicationConstants.TransferCustomerRatingId].ToLong()));
                Session[WebApplicationConstants.TransferCustomerRatingId] = null;
            }

            if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.TransferNumber]))
                ProcessTransferredRequest(new CustomerRating(Request.QueryString[WebApplicationConstants.TransferNumber].UrlTextDecrypt().ToLong()));

            SetEditStatus(false);
        }


        protected void OnToolbarNewClicked(object sender, EventArgs e)
        {
            litErrorMessages.Text = string.Empty;

            var rating = new CustomerRating
            {
                LineHaulProfitCeilingType = ValuePercentageType.Percentage,
                FuelProfitCeilingType = ValuePercentageType.Percentage,
                AccessorialProfitCeilingType = ValuePercentageType.Percentage,
                ServiceProfitCeilingType = ValuePercentageType.Percentage,

                LineHaulProfitCeilingPercentage = 100.0000m,
                FuelProfitCeilingPercentage = 100.0000m,
                AccessorialProfitCeilingPercentage = 100.0000m,
                ServiceProfitCeilingPercentage = 100.0000m,
            };
            LoadCustomerRating(rating);
            DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
            SetEditStatus(true);
        }

        protected void OnToolbarEditClicked(object sender, EventArgs e)
        {
            var rating = new CustomerRating(hidCustomerRatingId.Value.ToLong(), false);

            if (Lock == null || rating.IsNew) return;

            SetEditStatus(true);
            memberToolBar.ShowUnlock = true;
            Lock(this, new ViewEventArgs<CustomerRating>(rating));

            LoadCustomerRating(rating);
        }

        protected void OnToolbarUnlockClicked(object sender, EventArgs e)
        {
            var customerRating = new CustomerRating(hidCustomerRatingId.Value.ToLong(), false);
            if (UnLock == null || customerRating.IsNew) return;
            
            SetEditStatus(false);
            memberToolBar.ShowUnlock = false;
            UnLock(this, new ViewEventArgs<CustomerRating>(customerRating));
        }

        protected void OnToolbarSaveClicked(object sender, EventArgs e)
        {
            var customerRatingId = hidCustomerRatingId.Value.ToLong();
            var customerRating = new CustomerRating(customerRatingId, customerRatingId != default(long));

            if (customerRating.IsNew) customerRating.TenantId = ActiveUser.TenantId;
            else customerRating.LoadCollections();

            // details
            customerRating.Customer = new Customer(hidCustomerId.Value.ToLong(), false);
            customerRating.BillReseller = chkBillReseller.Checked;
            customerRating.ResellerAdditionId = ddlResellerAddition.SelectedValue.ToLong();
            customerRating.Project44AccountGroup = ddlProject44AccountGroup.SelectedValue;

            //Insurance
            customerRating.InsuranceEnabled = chkInsuranceEnabled.Checked;
            customerRating.InsurancePurchaseFloor = txtInsurancePurchaseFloor.Text.ToDecimal();
            customerRating.InsuranceChargeCodeId = ddlInsuranceChargeCode.SelectedValue.ToLong();

            //LINEHAUL
            customerRating.NoLineHaulProfit = chkLineHaulNoProfit.Checked;

            customerRating.LineHaulProfitCeilingType = ddlLineHaulCeilingType.SelectedValue.ToEnum<ValuePercentageType>();
            customerRating.LineHaulProfitCeilingPercentage = txtLineHaulCeilingPercentage.Text.ToDecimal();
            customerRating.LineHaulProfitCeilingValue = txtLineHaulCeilingValue.Text.ToDecimal();
            customerRating.UseLineHaulMinimumCeiling = chkLineHaulUseMinimumCeiling.Checked;

            customerRating.LineHaulProfitFloorType = ddlLineHaulFloorType.SelectedValue.ToEnum<ValuePercentageType>();
            customerRating.LineHaulProfitFloorPercentage = txtLineHaulFloorPercentage.Text.ToDecimal();
            customerRating.LineHaulProfitFloorValue = txtLineHaulFloorValue.Text.ToDecimal();
            customerRating.UseLineHaulMinimumFloor = chkLineHaulUseMinimumFloor.Checked;

            //FUEL
            customerRating.NoFuelProfit = chkFuelNoProfit.Checked;

            customerRating.FuelProfitCeilingType = ddlFuelCeilingType.SelectedValue.ToEnum<ValuePercentageType>();
            customerRating.FuelProfitCeilingPercentage = txtFuelCeilingPercentage.Text.ToDecimal();
            customerRating.FuelProfitCeilingValue = txtFuelCeilingValue.Text.ToDecimal();
            customerRating.UseFuelMinimumCeiling = chkFuelUseMinimumCeiling.Checked;

            customerRating.FuelProfitFloorType = ddlFuelFloorType.SelectedValue.ToEnum<ValuePercentageType>();
            customerRating.FuelProfitFloorPercentage = txtFuelFloorPercentage.Text.ToDecimal();
            customerRating.FuelProfitFloorValue = txtFuelFloorValue.Text.ToDecimal();
            customerRating.UseFuelMinimumFloor = chkFuelUseMinimumFloor.Checked;

            //ACCESSORIAL
            customerRating.NoAccessorialProfit = chkAccessorialNoProfit.Checked;

            customerRating.AccessorialProfitCeilingType = ddlAccessorialCeilingType.SelectedValue.ToEnum<ValuePercentageType>();
            customerRating.AccessorialProfitCeilingPercentage = txtAccessorialCeilingPercentage.Text.ToDecimal();
            customerRating.AccessorialProfitCeilingValue = txtAccessorialCeilingValue.Text.ToDecimal();
            customerRating.UseAccessorialMinimumCeiling = chkAccessorialUseMinimumCeiling.Checked;

            customerRating.AccessorialProfitFloorType = ddlAccessorialFloorType.SelectedValue.ToEnum<ValuePercentageType>();
            customerRating.AccessorialProfitFloorPercentage = txtAccessorialFloorPercentage.Text.ToDecimal();
            customerRating.AccessorialProfitFloorValue = txtAccessorialFloorValue.Text.ToDecimal();
            customerRating.UseAccessorialMinimumFloor = chkAccessorialUseMinimumFloor.Checked;

            //SERVICE
            customerRating.NoServiceProfit = chkServiceNoProfit.Checked;

            customerRating.ServiceProfitCeilingType = ddlServiceCeilingType.SelectedValue.ToEnum<ValuePercentageType>();
            customerRating.ServiceProfitCeilingPercentage = txtServiceCeilingPercentage.Text.ToDecimal();
            customerRating.ServiceProfitCeilingValue = txtServiceCeilingValue.Text.ToDecimal();
            customerRating.UseServiceMinimumCeiling = chkServiceUseMinimumCeiling.Checked;

            customerRating.ServiceProfitFloorType = ddlServiceFloorType.SelectedValue.ToEnum<ValuePercentageType>();
            customerRating.ServiceProfitFloorPercentage = txtServiceFloorPercentage.Text.ToDecimal();
            customerRating.ServiceProfitFloorValue = txtServiceFloorValue.Text.ToDecimal();
            customerRating.UseServiceMinimumFloor = chkServiceUseMinimumFloor.Checked;

            // TL
            customerRating.TLFuelRate = txtTLFuelRate.Text.ToDecimal();
            customerRating.TLFuelRateType = ddlTLFuelRateType.SelectedValue.ToEnum<RateType>();
            customerRating.TLFuelChargeCodeId = ddlTLFuelChargeCode.SelectedValue.ToLong();
            customerRating.TLFreightChargeCodeId = ddlTLFreightChargeCode.SelectedValue.ToLong();

            UpdateLtlSellRatesFromView(customerRating);
            UpdateSmallPackRatesFromView(customerRating);
            UpdateCustomerServiceMarkUpsFromView(customerRating);
            UpdateTLSellRatesFromView(customerRating);

            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<CustomerRating>(customerRating));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<CustomerRating>(customerRating));
        }

        protected void OnToolbarDeleteClicked(object sender, EventArgs e)
        {
            var rating = new CustomerRating(hidCustomerRatingId.Value.ToLong(), false);

            if (Delete != null)
                Delete(this, new ViewEventArgs<CustomerRating>(rating));

            customerRatingFinder.Reset();
        }

        protected void OnToolbarFindClicked(object sender, EventArgs e)
        {
            customerRatingFinder.Visible = true;
        }

        protected void OnToolbarCommandClicked(object sender, ViewEventArgs<string> e)
        {
            switch (e.Argument)
            {
                case BatchInsert:
                case BatchRemove:
                    customerBatchInsertRemove.Visible = true;
                    pnlDimScreen.Visible = true;
                    break;
            }
        }


        protected void OpenCustomerImportExport(object sender, EventArgs e)
        {
            customerRatingImportExport.Visible = true;
            pnlDimScreen.Visible = true;
        }

        protected void OnCustomerRatingImportExportClose(object sender, EventArgs e)
        {
            customerRatingImportExport.Visible = false;
            pnlDimScreen.Visible = false;
        }

        protected void OnCustomerRatingImportExportProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnCustomerRatingImportExportProcessingCompleted(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Information(e.Argument) });
        }

        protected void OnCustomerRatingBatchInsertRemoveClose(object sender, EventArgs e)
        {
            customerBatchInsertRemove.Visible = false;
            pnlDimScreen.Visible = false;
        }

        protected void OnCustomerRatingBatchInsertRemoveProcessingError(object sender, ViewEventArgs<IEnumerable<ValidationMessage>> e)
        {
            DisplayMessages(e.Argument);
        }


        protected void OnCustomerRatingFinderItemSelected(object sender, ViewEventArgs<CustomerRating> e)
        {
            litErrorMessages.Text = string.Empty;

            if (hidCustomerRatingId.Value.ToLong() != default(long))
            {
                var oldCustomerRating = new CustomerRating(hidCustomerRatingId.Value.ToLong(), false);
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<CustomerRating>(oldCustomerRating));
            }

            var customerRating = e.Argument;

            LoadCustomerRating(customerRating);

            customerRatingFinder.Visible = false;

            if (customerRatingFinder.OpenForEdit && Lock != null)
            {
                SetEditStatus(true);
                memberToolBar.ShowUnlock = true;
                Lock(this, new ViewEventArgs<CustomerRating>(customerRating));
            }
            else
            {
                SetEditStatus(false);
            }

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<CustomerRating>(customerRating));
        }

        protected void OnCustomerRatingFinderSelectionCancelled(object sender, EventArgs e)
        {
            customerRatingFinder.Visible = false;
        }



        protected void OnCustomerNumberEntered(object sender, EventArgs e)
        {
            if (CustomerSearch != null)
                CustomerSearch(this, new ViewEventArgs<string>(txtCustomerNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
        }

        protected void OnCustomerSearchClicked(object sender, ImageClickEventArgs e)
        {
            customerFinder.Visible = true;
        }

        protected void OnCustomerFinderItemSelected(object sender, ViewEventArgs<Customer> e)
        {
            DisplayCustomer(e.Argument);
            customerFinder.Visible = false;
        }

        protected void OnCustomerFinderSelectionCancelled(object sender, EventArgs e)
        {
            customerFinder.Visible = false;
        }


        private void LoadLtlSellRateForEdit(LTLSellRateViewDto ltlSellRate)
        {
            ddlLTLSellRateVendorRating.DataSource = ViewVendorRatings;
            ddlLTLSellRateVendorRating.DataBind();

            chkLTLSellRateIndirectPointEnabled.Checked = ltlSellRate.LTLIndirectPointEnabled;
            txtLTLSellRateEffectiveDate.Text = ltlSellRate.EffectiveDate.FormattedShortDate();
            txtLTLSellRateMarkUpPercent.Text = ltlSellRate.MarkupPercent.ToString();
            txtLTLSellRateMarkUpValue.Text = ltlSellRate.MarkupValue.ToString();
            ddlLTLSellRateVendorRating.SelectedValue = ltlSellRate.VendorRatingId.ToString();
            chkLTLSellRateUseMinimum.Checked = ltlSellRate.UseMinimum;
            chkLTLSellRateActive.Checked = ltlSellRate.Active;
	        ddlStartOverrideMarkupWeightBreak.SelectedValue = ltlSellRate.StartOverrideWeightBreak.ToInt().GetString();

	        txtOverrideMarkupPercentL5C.Text = ltlSellRate.OverrideMarkupPercentL5C.GetString();
	        txtOverrideMarkupValueL5C.Text = ltlSellRate.OverrideMarkupValueL5C.GetString();
			txtOverrideMarkupPercentM5C.Text = ltlSellRate.OverrideMarkupPercentM5C.GetString();
			txtOverrideMarkupValueM5C.Text = ltlSellRate.OverrideMarkupValueM5C.GetString();
			txtOverrideMarkupPercentM1M.Text = ltlSellRate.OverrideMarkupPercentM1M.GetString();
			txtOverrideMarkupValueM1M.Text = ltlSellRate.OverrideMarkupValueM1M.GetString();
			txtOverrideMarkupPercentM2M.Text = ltlSellRate.OverrideMarkupPercentM2M.GetString();
			txtOverrideMarkupValueM2M.Text = ltlSellRate.OverrideMarkupValueM2M.GetString();
			txtOverrideMarkupPercentM5M.Text = ltlSellRate.OverrideMarkupPercentM5M.GetString();
			txtOverrideMarkupValueM5M.Text = ltlSellRate.OverrideMarkupValueM5M.GetString();
			txtOverrideMarkupPercentM10M.Text = ltlSellRate.OverrideMarkupPercentM10M.GetString();
			txtOverrideMarkupValueM10M.Text = ltlSellRate.OverrideMarkupValueM10M.GetString();
			txtOverrideMarkupPercentM20M.Text = ltlSellRate.OverrideMarkupPercentM20M.GetString();
			txtOverrideMarkupValueM20M.Text = ltlSellRate.OverrideMarkupValueM20M.GetString();
			txtOverrideMarkupPercentM30M.Text = ltlSellRate.OverrideMarkupPercentM30M.GetString();
			txtOverrideMarkupValueM30M.Text = ltlSellRate.OverrideMarkupValueM30M.GetString();
			txtOverrideMarkupPercentM40M.Text = ltlSellRate.OverrideMarkupPercentM40M.GetString();
			txtOverrideMarkupValueM40M.Text = ltlSellRate.OverrideMarkupValueM40M.GetString();
            txtOverrideMarkupVendorFloorPercent.Text = ltlSellRate.OverrideMarkupPercentVendorFloor.GetString();
            txtOverrideMarkupVendorFloorValue.Text = ltlSellRate.OverrideMarkupValueVendorFloor.GetString();
            chkVendorFloorEnabled.Checked = ltlSellRate.OverrideMarkupVendorFloorEnabled;
        }

        protected void OnAddLtlSellRateClicked(object sender, EventArgs e)
        {
            hidEditingIndex.Value = (-1).ToString();
            LoadLtlSellRateForEdit(new LTLSellRateViewDto { EffectiveDate = DateTime.Now });
            pnlEditLTLSellRate.Visible = true;
            pnlDimScreen.Visible = true;
            txtLTLSellRateEffectiveDate.Focus();
        }

        protected void OnClearLtlSellRateClicked(object sender, EventArgs e)
        {
            StoredLtlSellRates = new List<LTLSellRateViewDto>();

            lstLTLSellRates.DataSource = new List<LTLSellRateViewDto>();
            lstLTLSellRates.DataBind();
            tabLTLSellRates.HeaderText = LtlSellRatesHeader;
            athtuTabUpdater.SetForUpdate(tabLTLSellRates.ClientID, LtlSellRatesHeader);
        }

        protected void OnEditLtlSellRateClicked(object sender, ImageClickEventArgs e)
        {
            var control = ((ImageButton)sender).Parent;

            var sellRates = SortLtlSellRates(StoredLtlSellRates);

            var itemIdx = control.FindControl("hidLTLSellRateIndex").ToCustomHiddenField().Value.ToInt();

            hidEditingIndex.Value = itemIdx.ToString();

            var sellRate = sellRates[itemIdx];
            LoadLtlSellRateForEdit(sellRate);

            pnlEditLTLSellRate.Visible = true;
            pnlDimScreen.Visible = true;

        }

        protected void OnDeleteLtlSellRateClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var sellRates = SortLtlSellRates(StoredLtlSellRates);
            var itemIdx = imageButton.Parent.FindControl("hidLTLSellRateIndex").ToCustomHiddenField().Value.ToInt();
            sellRates.RemoveAt(itemIdx);

            StoredLtlSellRates = sellRates;

            lstLTLSellRates.DataSource = sellRates;
            lstLTLSellRates.DataBind();
            tabLTLSellRates.HeaderText = sellRates.BuildTabCount(LtlSellRatesHeader);
            athtuTabUpdater.SetForUpdate(tabLTLSellRates.ClientID, sellRates.BuildTabCount(LtlSellRatesHeader));
        }

        protected void OnCloseEditLtlSellRateClicked(object sender, EventArgs eventArgs)
        {
            pnlEditLTLSellRate.Visible = false;
            pnlDimScreen.Visible = false;
        }

        protected void OnEditLtlSellRateDoneClicked(object sender, EventArgs e)
        {
            var sellRates = SortLtlSellRates(StoredLtlSellRates);
            var editingIndex = hidEditingIndex.Value.ToInt();
            var isNew = editingIndex == WebApplicationConstants.InvalidIndex;
            var sellRate = isNew ? new LTLSellRateViewDto { EffectiveDate = DateTime.Now } : sellRates[editingIndex];

            var vendorRating = new VendorRating(ddlLTLSellRateVendorRating.SelectedValue.ToLong());

            sellRate.LTLIndirectPointEnabled = chkLTLSellRateIndirectPointEnabled.Checked;
            sellRate.EffectiveDate = txtLTLSellRateEffectiveDate.Text.ToDateTime();
            sellRate.MarkupPercent = txtLTLSellRateMarkUpPercent.Text.ToDecimal();
            sellRate.MarkupValue = txtLTLSellRateMarkUpValue.Text.ToDecimal();
            sellRate.VendorRatingId = ddlLTLSellRateVendorRating.SelectedValue.ToLong();
            sellRate.UseMinimum = chkLTLSellRateUseMinimum.Checked;
            sellRate.Active = chkLTLSellRateActive.Checked;
            sellRate.VendorRatingIsActive = (!vendorRating.IsNew && vendorRating.Active) || vendorRating.IsNew;

	        sellRate.StartOverrideWeightBreak = ddlStartOverrideMarkupWeightBreak.SelectedValue.ToInt().ToEnum<WeightBreak>();

	        sellRate.OverrideMarkupPercentL5C = txtOverrideMarkupPercentL5C.Text.ToDecimal();
	        sellRate.OverrideMarkupValueL5C = txtOverrideMarkupValueL5C.Text.ToDecimal();
	        sellRate.OverrideMarkupPercentM5C = txtOverrideMarkupPercentM5C.Text.ToDecimal();
	        sellRate.OverrideMarkupValueM5C = txtOverrideMarkupValueM5C.Text.ToDecimal();
	        sellRate.OverrideMarkupPercentM1M = txtOverrideMarkupPercentM1M.Text.ToDecimal();
	        sellRate.OverrideMarkupValueM1M = txtOverrideMarkupValueM1M.Text.ToDecimal();
	        sellRate.OverrideMarkupPercentM2M = txtOverrideMarkupPercentM2M.Text.ToDecimal();
	        sellRate.OverrideMarkupValueM2M = txtOverrideMarkupValueM2M.Text.ToDecimal();
	        sellRate.OverrideMarkupPercentM5M = txtOverrideMarkupPercentM5M.Text.ToDecimal();
	        sellRate.OverrideMarkupValueM5M = txtOverrideMarkupValueM5M.Text.ToDecimal();
	        sellRate.OverrideMarkupPercentM10M = txtOverrideMarkupPercentM10M.Text.ToDecimal();
	        sellRate.OverrideMarkupValueM10M = txtOverrideMarkupValueM10M.Text.ToDecimal();
	        sellRate.OverrideMarkupPercentM20M = txtOverrideMarkupPercentM20M.Text.ToDecimal();
	        sellRate.OverrideMarkupValueM20M = txtOverrideMarkupValueM20M.Text.ToDecimal();
	        sellRate.OverrideMarkupPercentM30M = txtOverrideMarkupPercentM30M.Text.ToDecimal();
	        sellRate.OverrideMarkupValueM30M = txtOverrideMarkupValueM30M.Text.ToDecimal();
	        sellRate.OverrideMarkupPercentM40M = txtOverrideMarkupPercentM40M.Text.ToDecimal();
	        sellRate.OverrideMarkupValueM40M = txtOverrideMarkupValueM40M.Text.ToDecimal();

            sellRate.OverrideMarkupPercentVendorFloor = txtOverrideMarkupVendorFloorPercent.Text.ToDecimal();
            sellRate.OverrideMarkupValueVendorFloor = txtOverrideMarkupVendorFloorValue.Text.ToDecimal();
            sellRate.OverrideMarkupVendorFloorEnabled = chkVendorFloorEnabled.Checked;

            if (isNew)
            {
                sellRates.Add(sellRate);
            }
            else
            {
                sellRates[editingIndex] = sellRate;
            }

            StoredLtlSellRates = sellRates;

            lstLTLSellRates.DataSource = SortLtlSellRates(sellRates);
            lstLTLSellRates.DataBind();
            tabLTLSellRates.HeaderText = sellRates.BuildTabCount(LtlSellRatesHeader);
            athtuTabUpdater.SetForUpdate(tabLTLSellRates.ClientID, sellRates.BuildTabCount(LtlSellRatesHeader));

            pnlEditLTLSellRate.Visible = false;
            pnlDimScreen.Visible = false;
        }



        private void LoadSmallPackRateForEdit(SmallPackRateViewDto smallPackRate)
        {
            txtSmallPackRateVendorNumber.Focus();
            DisplayVendor(new Vendor(smallPackRate.VendorId));
            ddlSmallPackRateSmallPackEngine.SelectedValue = smallPackRate.SmallPackageEngine.ToInt().ToString();
            SetSmallPackTypes();
            ddlSmallPackRateSmallPackType.SelectedValue = smallPackRate.SmallPackType;
			ddlSmallPackRateChargeCode.SelectedValue = smallPackRate.ChargeCodeId.GetString();
            txtSmallPackRateMarkUpPercent.Text = smallPackRate.MarkupPercent.ToString();
            txtSmallPackRateMarkUpValue.Text = smallPackRate.MarkupValue.ToString();
            txtSmallPackRateEffectiveDate.Text = smallPackRate.EffectiveDate.FormattedShortDate();
            chkSmallPackRateUseMinimum.Checked = smallPackRate.UseMinimum;
            chkSmallPackRateActive.Checked = smallPackRate.Active;
        }

        protected void OnAddSmallPackRateClicked(object sender, EventArgs e)
        {
            hidEditingIndex.Value = (-1).ToString();
            LoadSmallPackRateForEdit(new SmallPackRateViewDto { EffectiveDate = DateTime.Now });
            pnlEditSmallPackRate.Visible = true;
            pnlDimScreen.Visible = true;
        }

        protected void OnClearSmallPackRateClicked(object sender, EventArgs e)
        {
            StoredSmallPackRates = new List<SmallPackRateViewDto>();

            lstSmallPackRates.DataSource = new List<SmallPackRateViewDto>();
            lstSmallPackRates.DataBind();
            tabSmallPackRates.HeaderText = SmallPackRatesHeader;
            athtuTabUpdater.SetForUpdate(tabSmallPackRates.ClientID, SmallPackRatesHeader);
        }

        protected void OnEditSmallPackRateClicked(object sender, ImageClickEventArgs e)
        {
            var control = ((ImageButton)sender).Parent;

            var smallPackRates = SortSmallPackRates(StoredSmallPackRates);

            var itemIdx = control.FindControl("hidSmallPackRateIndex").ToCustomHiddenField().Value.ToInt();
            hidEditingIndex.Value = itemIdx.GetString();

            var smallPackRate = smallPackRates[itemIdx];

            LoadSmallPackRateForEdit(smallPackRate);

            pnlEditSmallPackRate.Visible = true;
            pnlDimScreen.Visible = true;
        }

        protected void OnDeleteSmallPackRateClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var smallPackRates = SortSmallPackRates(StoredSmallPackRates);
            smallPackRates.RemoveAt(imageButton.Parent.FindControl("hidSmallPackRateIndex").ToCustomHiddenField().Value.ToInt());

            StoredSmallPackRates = smallPackRates;

            lstSmallPackRates.DataSource = smallPackRates;
            lstSmallPackRates.DataBind();
            tabSmallPackRates.HeaderText = smallPackRates.BuildTabCount(SmallPackRatesHeader);
            athtuTabUpdater.SetForUpdate(tabSmallPackRates.ClientID, smallPackRates.BuildTabCount(SmallPackRatesHeader));
        }

        protected void OnCloseEditSmallPackRateClicked(object sender, EventArgs eventArgs)
        {
            pnlEditSmallPackRate.Visible = false;
            pnlDimScreen.Visible = false;
        }

        protected void OnEditSmallPackRateDoneClicked(object sender, EventArgs e)
        {
            var smallPackRates = SortSmallPackRates(StoredSmallPackRates);
            var vendor = new Vendor(hidSmallPackRateVendorId.Value.ToLong(), false);

            var editingIndex = hidEditingIndex.Value.ToInt();

            var isNew = editingIndex == WebApplicationConstants.InvalidIndex;
            var smallPackRate = isNew ? new SmallPackRateViewDto() : smallPackRates[editingIndex];

            smallPackRate.VendorName = vendor.Name;
            smallPackRate.VendorNumber = vendor.VendorNumber;
            smallPackRate.VendorId = vendor.Id;
            smallPackRate.SmallPackageEngine = ddlSmallPackRateSmallPackEngine.SelectedValue.ToEnum<SmallPackageEngine>();
            smallPackRate.SmallPackType = ddlSmallPackRateSmallPackType.SelectedValue;
            smallPackRate.MarkupPercent = txtSmallPackRateMarkUpPercent.Text.ToDecimal();
            smallPackRate.MarkupValue = txtSmallPackRateMarkUpValue.Text.ToDecimal();
            smallPackRate.EffectiveDate = txtSmallPackRateEffectiveDate.Text.ToDateTime();
            smallPackRate.UseMinimum = chkSmallPackRateUseMinimum.Checked;
            smallPackRate.Active = chkSmallPackRateActive.Checked;
            smallPackRate.ChargeCodeId = ddlSmallPackRateChargeCode.SelectedValue.ToLong();

            if (isNew)
            {
                smallPackRates.Add(smallPackRate);
            }
            else
            {
                smallPackRates[editingIndex] = smallPackRate;
            }

            StoredSmallPackRates = smallPackRates;

            lstSmallPackRates.DataSource = SortSmallPackRates(smallPackRates);
            lstSmallPackRates.DataBind();
            tabSmallPackRates.HeaderText = smallPackRates.BuildTabCount(SmallPackRatesHeader);
            athtuTabUpdater.SetForUpdate(tabSmallPackRates.ClientID, smallPackRates.BuildTabCount(SmallPackRatesHeader));

            pnlEditSmallPackRate.Visible = false;
            pnlDimScreen.Visible = false;
        }

        protected void OnSmallPackRateSmallPackEngineSelectedIndexChanged(object sender, EventArgs e)
        {
            SetSmallPackTypes();
        }

        protected void OnSmallPackRateVendorNumberEntered(object sender, EventArgs e)
        {
            if (VendorSearch != null)
                VendorSearch(this, new ViewEventArgs<string>(txtSmallPackRateVendorNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
        }

        protected void OnSmallPackRateVendorSearchClicked(object sender, ImageClickEventArgs e)
        {
            vendorFinder.Visible = true;
        }

        protected void OnVendorFinderItemSelected(object sender, ViewEventArgs<Vendor> e)
        {
            DisplayVendor(e.Argument);
            vendorFinder.Visible = false;
        }

        protected void OnVendorFinderSelectionCancelled(object sender, EventArgs e)
        {
            vendorFinder.Visible = false;
        }
        


        private void LoadCustomerServiceMarkUpForEdit(CustomerServiceMarkup c)
        {
            txtCustomerServiceMarkUpMarkUpPercent.Text = c.MarkupPercent.ToString();
            txtCustomerServiceMarkUpMarkUpValue.Text = c.MarkupValue.ToString();
            txtCustomerServiceMarkUpEffectiveDate.Text = c.EffectiveDate.FormattedShortDate();
			ddlCustomerServiceMarkUpService.SelectedValue = c.ServiceId.GetString();
            txtCustomerServiceMarkUpServiceCeiling.Text = c.ServiceChargeCeiling.ToString();
            txtCustomerServiceMarkUpServiceFloor.Text = c.ServiceChargeFloor.ToString();
            chkCustomerServiceMarkUpUseMinimum.Checked = c.UseMinimum;
            chkCustomerServiceMarkUpActive.Checked = c.Active;
        }

        protected void OnAddCustomerServiceMarkUpClicked(object sender, EventArgs e)
        {
            hidEditingIndex.Value = (-1).ToString();
            LoadCustomerServiceMarkUpForEdit(new CustomerServiceMarkup { EffectiveDate = DateTime.Now });
            pnlEditCustomerServiceMarkUp.Visible = true;
            pnlDimScreen.Visible = true;
        }

        protected void OnClearCustomerServiceMarkUpClicked(object sender, EventArgs e)
        {
            StoredCustomerServiceMarkups = new List<CustomerServiceMarkup>();

            lstCustomerServiceMarkUps.DataSource = new List<CustomerServiceMarkUpViewDto>();
            lstCustomerServiceMarkUps.DataBind();
            tabCustomerServiceMarkUps.HeaderText = CustomerServiceMarkupHeader;
            athtuTabUpdater.SetForUpdate(tabCustomerServiceMarkUps.ClientID, CustomerServiceMarkupHeader);
        }

        protected void OnEditCustomerServiceMarkUpClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = ((ImageButton)sender).Parent;

            var markups = SortCustomerServiceMarkUps(StoredCustomerServiceMarkups);

            var itemIdx = imageButton.FindControl("hidCustomerServiceMarkUpIndex").ToCustomHiddenField().Value.ToInt();
            hidEditingIndex.Value = itemIdx.GetString();

            LoadCustomerServiceMarkUpForEdit(markups[itemIdx]);

            pnlEditCustomerServiceMarkUp.Visible = true;
            pnlDimScreen.Visible = true;
        }

        protected void OnDeleteCustomerServiceMarkUpClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var markups = SortCustomerServiceMarkUps(StoredCustomerServiceMarkups);

            markups.RemoveAt(imageButton.Parent.FindControl("hidCustomerServiceMarkUpIndex").ToCustomHiddenField().Value.ToInt());

            StoredCustomerServiceMarkups = markups;

            lstCustomerServiceMarkUps.DataSource = markups;
            lstCustomerServiceMarkUps.DataBind();
            tabCustomerServiceMarkUps.HeaderText = markups.BuildTabCount(CustomerServiceMarkupHeader);
            athtuTabUpdater.SetForUpdate(tabCustomerServiceMarkUps.ClientID, markups.BuildTabCount(CustomerServiceMarkupHeader));
        }

        protected void OnCloseEditCustomerServiceMarkUpClicked(object sender, EventArgs eventArgs)
        {
            pnlEditCustomerServiceMarkUp.Visible = false;
            pnlDimScreen.Visible = false;
        }

        protected void OnEditCustomerServiceMarkUpDoneClicked(object sender, EventArgs e)
        {
            var markups = SortCustomerServiceMarkUps(StoredCustomerServiceMarkups);
            var rating = new CustomerRating(hidCustomerRatingId.Value.ToLong());
            var editingIndex = hidEditingIndex.Value.ToInt();
            var isNew = editingIndex == WebApplicationConstants.InvalidIndex;

            var markup = isNew ? new CustomerServiceMarkup { EffectiveDate = DateTime.Now } : markups[editingIndex];
            if (!isNew && !markup.HasChanges())
                markup.TakeSnapShot();

            markup.MarkupPercent = txtCustomerServiceMarkUpMarkUpPercent.Text.ToDecimal();
            markup.MarkupValue = txtCustomerServiceMarkUpMarkUpValue.Text.ToDecimal();
            markup.ServiceId = ddlCustomerServiceMarkUpService.SelectedValue.ToLong();
            markup.ServiceChargeCeiling = txtCustomerServiceMarkUpServiceCeiling.Text.ToDecimal();
            markup.ServiceChargeFloor = txtCustomerServiceMarkUpServiceFloor.Text.ToDecimal();
            markup.EffectiveDate = txtCustomerServiceMarkUpEffectiveDate.Text.ToDateTime();
            markup.UseMinimum = chkCustomerServiceMarkUpUseMinimum.Checked;
            markup.Active = chkCustomerServiceMarkUpActive.Checked;
            markup.CustomerRating = rating;
            markup.TenantId = rating.TenantId;

            if (isNew)
            {
                markups.Add(markup);
            }
            else
            {
                markups[editingIndex] = markup;
            }

            StoredCustomerServiceMarkups = markups;

            lstCustomerServiceMarkUps.DataSource = SortCustomerServiceMarkUps(markups);
            lstCustomerServiceMarkUps.DataBind();
            tabCustomerServiceMarkUps.HeaderText = markups.BuildTabCount(CustomerServiceMarkupHeader);
            athtuTabUpdater.SetForUpdate(tabCustomerServiceMarkUps.ClientID, markups.BuildTabCount(CustomerServiceMarkupHeader));

            pnlEditCustomerServiceMarkUp.Visible = false;
            pnlDimScreen.Visible = false;
        }



        private void LoadTlSellRateForEdit(TLSellRate tlSellRate)
        {
            txtTLSellRateOriginCity.Focus();
            txtTLSellRateEffectiveDate.Text = tlSellRate.IsNew ? DateTime.Now.FormattedShortDate() : tlSellRate.EffectiveDate.FormattedShortDate();
            txtTLSellRateOriginCity.Text = tlSellRate.OriginCity;
            txtTLSellRateOriginState.Text = tlSellRate.OriginState;
            txtTLSellRateOriginPostalCode.Text = tlSellRate.OriginPostalCode;
			ddlTLSellRateOriginCountry.SelectedValue = tlSellRate.OriginCountryId.GetString();
            txtTLSellRateDestinationCity.Text = tlSellRate.DestinationCity;
            txtTLSellRateDestinationState.Text = tlSellRate.DestinationState;
            txtTLSellRateDestinationPostalCode.Text = tlSellRate.DestinationPostalCode;
            ddlTLSellRateDestinationCountry.SelectedValue = tlSellRate.DestinationCountryId.GetString();
            ddlTLSellRateTariffType.SelectedValue = tlSellRate.TariffType.ToInt().GetString();
            txtTLSellRateMinimumWeight.Text = tlSellRate.MinimumWeight.GetString();
            txtTLSellRateMaximumWeight.Text = tlSellRate.MaximumWeight.GetString();
            txtTLSellRateMinimumCharge.Text = tlSellRate.MinimumCharge.GetString();
            txtTLSellRateRate.Text = tlSellRate.Rate.GetString();
            ddlTLSellRateRateType.SelectedValue = tlSellRate.RateType.ToInt().GetString();
			ddlTLSellRateEquipmentType.SelectedValue = tlSellRate.EquipmentTypeId.GetString();

	        txtTLSellRateMileage.Text = tlSellRate.Mileage.GetString();
			ddlTLSellRateMileageSource.SelectedValue = tlSellRate.MileageSourceId.GetString();
        }

        protected void OnClearTlSellRateClicked(object sender, EventArgs e)
        {
            peLstTLSellRates.LoadData(new List<DiscountTier>());
            tabTLSellRates.HeaderText = TlSellRatesHeader;
            athtuTabUpdater.SetForUpdate(tabTLSellRates.ClientID, TlSellRatesHeader);
        }

        protected void OnAddTlSellRateClicked(object sender, EventArgs e)
        {
            hidEditingIndex.Value = (WebApplicationConstants.InvalidIndex).ToString();
            LoadTlSellRateForEdit(new TLSellRate());
            pnlEditTLSellRate.Visible = true;
            pnlDimScreen.Visible = pnlEditTLSellRate.Visible;

        }

        protected void OnEditTlSellRateClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var tlSellRates = SortTlSellRates(StoredTLSellRates);
            var pageIdx = peLstTLSellRates.CurrentPage - 1;
            var itemIdx = imageButton.FindControl("hidTLSellRateIndex").ToCustomHiddenField().Value.ToInt();

            hidEditingIndex.Value = itemIdx.ToString();

            var tlSellRate = tlSellRates[(pageIdx * peLstTLSellRates.PageSize) + itemIdx];

            LoadTlSellRateForEdit(tlSellRate);

            pnlEditTLSellRate.Visible = true;
            pnlDimScreen.Visible = true;
        }

        protected void OnDeleteTLSellRateClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var tlSellRates = SortTlSellRates(StoredTLSellRates);
            var pageIdx = peLstTLSellRates.CurrentPage - 1;
            var itemIdx = imageButton.FindControl("hidTLSellRateIndex").ToCustomHiddenField().Value.ToInt();

            tlSellRates.RemoveAt((pageIdx * peLstTLSellRates.PageSize) + itemIdx);

            peLstTLSellRates.LoadData(SortTlSellRates(tlSellRates), peLstTLSellRates.CurrentPage);
            tabTLSellRates.HeaderText = tlSellRates.BuildTabCount(TlSellRatesHeader);
            athtuTabUpdater.SetForUpdate(tabTLSellRates.ClientID, tlSellRates.BuildTabCount(TlSellRatesHeader));
        }

        protected void OnCloseEditTLSellRateClicked(object sender, EventArgs eventArgs)
        {
            pnlEditTLSellRate.Visible = false;
            pnlDimScreen.Visible = pnlEditTLSellRate.Visible;
        }

        protected void OnEditTLSellRateDoneClicked(object sender, EventArgs e)
        {
            var tlSellRates = SortTlSellRates(StoredTLSellRates);
            var rating = new CustomerRating(hidCustomerRatingId.Value.ToLong());

            var pageIdx = peLstTLSellRates.CurrentPage - 1;

            var isNew = hidEditingIndex.Value.ToInt() == WebApplicationConstants.InvalidIndex;
            var editingIndex = (pageIdx * peLstTLSellRates.PageSize) + hidEditingIndex.Value.ToInt();

            var tlSellRate = isNew ? new TLSellRate() : tlSellRates[editingIndex];

            if (!isNew && !tlSellRate.HasChanges())
                tlSellRate.TakeSnapShot();

            tlSellRate.EffectiveDate = txtTLSellRateEffectiveDate.Text.ToDateTime();
            tlSellRate.OriginCity = txtTLSellRateOriginCity.Text;
            tlSellRate.OriginState = txtTLSellRateOriginState.Text;
            tlSellRate.OriginPostalCode = txtTLSellRateOriginPostalCode.Text;
            tlSellRate.OriginCountryId = ddlTLSellRateOriginCountry.SelectedValue.ToLong();
            tlSellRate.DestinationCity = txtTLSellRateDestinationCity.Text;
            tlSellRate.DestinationState = txtTLSellRateDestinationState.Text;
            tlSellRate.DestinationPostalCode = txtTLSellRateDestinationPostalCode.Text;
            tlSellRate.DestinationCountryId = ddlTLSellRateDestinationCountry.SelectedValue.ToLong();
            tlSellRate.TariffType = ddlTLSellRateTariffType.SelectedValue.ToEnum<TLTariffType>();
            tlSellRate.MinimumWeight = txtTLSellRateMinimumWeight.Text.ToDecimal();
            tlSellRate.MaximumWeight = txtTLSellRateMaximumWeight.Text.ToDecimal();
            tlSellRate.MinimumCharge = txtTLSellRateMinimumCharge.Text.ToDecimal();
            tlSellRate.Rate = txtTLSellRateRate.Text.ToDecimal();
            tlSellRate.RateType = ddlTLSellRateRateType.SelectedValue.ToEnum<RateType>();
            tlSellRate.EquipmentTypeId = ddlTLSellRateEquipmentType.SelectedValue.ToLong();
            tlSellRate.Mileage = txtTLSellRateMileage.Text.ToDecimal();
            tlSellRate.MileageSourceId = ddlTLSellRateMileageSource.SelectedValue.ToLong();
            tlSellRate.CustomerRating = rating;
            tlSellRate.TenantId = rating.TenantId;

            if (isNew)
            {
                tlSellRates.Add(tlSellRate);
            }
            else
            {
                tlSellRates[editingIndex] = tlSellRate;
            }

            peLstTLSellRates.LoadData(SortTlSellRates(tlSellRates), peLstTLSellRates.CurrentPage);

            tabTLSellRates.HeaderText = tlSellRates.BuildTabCount(TlSellRatesHeader);
            athtuTabUpdater.SetForUpdate(tabTLSellRates.ClientID, tlSellRates.BuildTabCount(TlSellRatesHeader));

            pnlEditTLSellRate.Visible = false;
            pnlDimScreen.Visible = false;
        }



        protected void OnCustomerRatingImportExportCustomerServiceMarkupImported(object sender, ViewEventArgs<string> e)
        {
            var markups = StoredCustomerServiceMarkups;

            lstCustomerServiceMarkUps.DataSource = SortCustomerServiceMarkUps(markups);
            lstCustomerServiceMarkUps.DataBind();
            tabCustomerServiceMarkUps.HeaderText = markups.BuildTabCount(CustomerServiceMarkupHeader);

            DisplayMessages(new[] { ValidationMessage.Information(e.Argument) });
        }
        
        protected void OnCustomerRatingImportExportLTLSellRatesImported(object sender, ViewEventArgs<string> e)
        {
            var sellRates = StoredLtlSellRates;

            lstLTLSellRates.DataSource = SortLtlSellRates(sellRates);
            lstLTLSellRates.DataBind();
            tabLTLSellRates.HeaderText = sellRates.BuildTabCount(LtlSellRatesHeader);

            DisplayMessages(new[] { ValidationMessage.Information(e.Argument) });
        }

        protected void OnCustomerRatingImportExportSmallPackRatesImported(object sender, ViewEventArgs<string> e)
        {
            var smallPackRates = StoredSmallPackRates;

            lstSmallPackRates.DataSource = SortSmallPackRates(smallPackRates);
            lstSmallPackRates.DataBind();
            tabSmallPackRates.HeaderText = smallPackRates.BuildTabCount(SmallPackRatesHeader);

            DisplayMessages(new[] { ValidationMessage.Information(e.Argument) });
        }

        protected void OnCustomerRatingImportExportTLSellRatesImported(object sender, ViewEventArgs<string> e)
        {
            var tlSellRates = SortTlSellRates(StoredTLSellRates);
            peLstTLSellRates.LoadData(tlSellRates, peLstTLSellRates.CurrentPage);
            tabTLSellRates.HeaderText = tlSellRates.BuildTabCount(TlSellRatesHeader);
            DisplayMessages(new[] { ValidationMessage.Information(e.Argument) });
        }
    }
}
