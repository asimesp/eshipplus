﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Smc;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Rating
{
	public partial class SmcPointToPointServiceView : MemberPageBase
	{
		public static string PageAddress { get { return "~/Members/Rating/SmcPointToPointServiceView.aspx"; } }

		public override ViewCode PageCode { get { return ViewCode.SmcPointToPointServiceCheck; } }

		public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.RatingBlue; } }


		private void DoSearch(bool export = false)
		{
			var services = new CarrierConnect(ProcessorVars.IntegrationVariables[ActiveUser.TenantId].CarrierConnectParameters)
				.GetPointToPointService(txtOriginPostalCode.Text, new Country(ddlOriginCountry.SelectedValue.ToLong()),
				                        txtDestinationPostalCode.Text, new Country(ddlDestinationCountry.SelectedValue.ToLong()))
				.OrderBy(s=>s.CarrierName)
				.ToList();

			if (!export)
			{
				rptAvailableCarriers.DataSource = services
					.Select(s => new
						{
							Service = s,
							TerminalLink = 
								string.Format("{0}?{1}={2}&{3}={4}&{5}={6}&{7}={8}",
								              SmcCarrierTerminalInformation.PageAddress, WebApplicationConstants.CarrierScac,
								              s.Scac, WebApplicationConstants.OriginTerminalCode, s.OriginTerminalCode,
								              WebApplicationConstants.DestinationTerminalCode, s.DestinationTerminalCode,
								              WebApplicationConstants.CarrierName, s.CarrierName)
						});
				rptAvailableCarriers.DataBind();
				return;
			}

			var data = services
				.Select(t => new[]
					{
						t.CarrierName, t.Scac, t.Method, t.OriginPostalCode,
						t.OriginCountry == null ? string.Empty : t.OriginCountry.Code,
						t.DestinationPostalCode, t.DestinationCountry == null ? string.Empty : t.DestinationCountry.Code,
						t.OriginTerminalCode, t.DestinationTerminalCode, t.MinimumDays.GetString(), t.MinimumCalendarDays.GetString(),
						t.MaximumDays.GetString(), t.MaximumCalendarDays.GetString(), t.Direct.GetString(), t.ServiceCode,
						t.DataReleaseName, t.DataReleaseDate.FormattedLongDateAlt(),
						t.DataExpirationDate.FormattedLongDateAlt()
					})
				.ToList();
			data.Insert(0,
			            new[]
				            {
					            "Carrier Name", "SCAC", "Method", "Origin Postal Code", "Origin Country Code",
					            "Destination Postal Code", "Destination Country Code", "Origin Terminal Code",
					            "Destination Terminal Code", "Minimum Days",
					            "Minimum Calendar Days from " + DateTime.Now.FormattedShortDateAlt(), "Maximum Days",
					            "Maximum Calendar Days from " + DateTime.Now.FormattedShortDateAlt(), "Direct",
					            "Service Code", "Data Release Name", "Data Release Date", "Data Expiration Date"
				            });
			Response.Export(data.Select(d => d.TabJoin()).ToArray().NewLineJoin());
		}


		protected void Page_Load(object sender, EventArgs e)
		{
			if (IsPostBack) return;


            ddlDestinationCountry.SelectedValue = ActiveUser.Tenant.DefaultCountryId.GetString().GetString();
            ddlOriginCountry.SelectedValue = ActiveUser.Tenant.DefaultCountryId.GetString().GetString();
		}

		protected void OnDownloadTerminalsClicked(object sender, ImageClickEventArgs e)
		{
			var button = (ImageButton)sender;
			var hid = button.FindControl("hidScac").ToHiddenField();

			var terminals = new CarrierConnect(ProcessorVars.IntegrationVariables[ActiveUser.TenantId].CarrierConnectParameters)
				.GetCarrierTerminals(hid.Value, DateTime.Now);

			var data = terminals
				.Select(t => new[]
					{
						t.CarrierName, t.Scac, t.Method, t.TerminalName, t.Code, t.Address1, t.Address2, t.City, t.State, t.PostalCode,
						t.PostalCodePlus4, t.CountryCode, t.ContactName, t.ContactTitle, t.ContactEmail, t.ContactPhone, t.ContactFax,
						t.ContactTollFree
					})
				.ToList();
			data.Insert(0,
						new[]
				            {
					            "Carrier Name", "SCAC", "Method", "Terminal Name", "Terminal Code", "Address 1", "Address 2", "City",
					            "State", "Postal Code", "Postal Code + 4", "Country Code", "Contact Name", "ConTact Title",
					            "Contact Email", "Contact Phone", "Contact Fax", "Contact Toll Free"
				            });
			Response.Export(data.Select(d => d.TabJoin()).ToArray().NewLineJoin());
		}

		protected void OnSearchClicked(object sender, EventArgs e)
		{
			DoSearch();
		}

		protected void OnExportClicked(object sender, EventArgs e)
		{
			DoSearch(true);
		}
	}
}