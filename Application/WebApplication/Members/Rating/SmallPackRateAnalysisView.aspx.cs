﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Rating;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Rating
{
    public partial class SmallPackRateAnalysisView : MemberPageBase, IBatchRateDataView
    {

        public static string PageAddress { get { return "~/Members/Rating/SmallPackRateAnalysisView.aspx"; } }

        public override ViewCode PageCode
        {
            get { return ViewCode.RateAnalysisSmallPack; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RatingBlue; }
        }

        public List<VendorRating> VendorRatingProfiles
        {
            set { }
        }

        public Dictionary<int, string> SmallPackEngines
        {
            set
            {
                var engines = value.Select(t => new ViewListItem(t.Value, t.Key.ToString())).ToList();
                engines.Insert(0, new ViewListItem(WebApplicationConstants.ChooseOne, 0.ToString()));
                ddlSmallPackageEngine.DataSource = engines;
                ddlSmallPackageEngine.DataBind();
            }
        }

        public Dictionary<SmallPackageEngine, List<string>> SmallPackTypes
        {
            get
            {
                return ViewState["CustomerRatingsSmallPackTypes"] as Dictionary<SmallPackageEngine, List<string>> ??
                       new Dictionary<SmallPackageEngine, List<string>>();
            }
            set
            {
                ViewState["CustomerRatingsSmallPackTypes"] = value;
                SetSmallPackTypes();
            }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<List<BatchRateData>>> Save;
        public event EventHandler<ViewEventArgs<List<BatchRateData>>> Delete;
        public event EventHandler<ViewEventArgs<ServiceMode>> Search;
        public event EventHandler<ViewEventArgs<VendorRatingViewSearchCriteria>> RefreshVendorRatingProfiles;

        public void DisplayBatchRateDataSet(List<BatchRateData> data)
        {
            litRecordCount.Text = data.BuildRecordCount();
            rptRateAnalysisSets.DataSource = data.OrderBy(d => d.DateCreated)
                .Select(d => new
                {
                    d.Id,
                    d.Name,
                    Engine = d.SmallPackageEngine.FormattedString(),
                    ServiceType = d.SmallPackageEngineType,
                    PackageType = d.PackageType.TypeName,
                    AnalysisEffectiveDate = d.AnalysisEffectiveDate.FormattedShortDate(),
                    DateSubmitted = d.DateCreated.FormattedLongDate(),
                    DateCompleted = d.DateCompleted == DateUtility.SystemEarliestDateTime
                                        ? string.Empty
                                        : d.DateCompleted.FormattedLongDate(),
                    SubmittedBy = d.SubmittedByUser.Username,
                    d.SubmittedByUserId
                });
            rptRateAnalysisSets.DataBind();
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }



        private void SetSmallPackTypes()
        {
            var engine = ddlSmallPackageEngine.SelectedValue.ToEnum<SmallPackageEngine>();

            ddlEngineServiceType.DataSource = SmallPackTypes.ContainsKey(engine) ? SmallPackTypes[engine] : new List<string>();
            ddlEngineServiceType.DataBind();
        }

        private void LoadSmallPackageBatchRateDataSets()
        {
            if (Search != null)
                Search(this, new ViewEventArgs<ServiceMode>(ServiceMode.SmallPackage));
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            new BatchRateDataHandler(this).Initialize();

            if (IsPostBack) return;

            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.SmallPackageRateAnalysisImportTemplate });
            litMaxLineCount.Text = WebApplicationSettings.AnalysisMaximumInputLineCount.GetString();

            if (Loading != null)
                Loading(this, new EventArgs());

            LoadSmallPackageBatchRateDataSets();
        }




        protected void OnBatchRateDataDelete(object sender, ImageClickEventArgs e)
        {
            var button = (ImageButton)sender;
            var id = button.Parent.FindControl("hidBatchRateDataId").ToCustomHiddenField().Value.ToLong();

            if (Delete != null)
                Delete(this, new ViewEventArgs<List<BatchRateData>>(new List<BatchRateData> { new BatchRateData(id) }));
            LoadSmallPackageBatchRateDataSets();
        }



        protected void OnBatchRateDataInputDownload(object sender, ImageClickEventArgs e)
        {
            var button = (ImageButton)sender;
            var id = (button.Parent.FindControl("hidBatchRateDataId")).ToCustomHiddenField().Value.ToLong();
            var data = new BatchRateData(id);
            Response.Export(data.LaneData);
        }

        protected void OnBatchRateDataOutputDownload(object sender, ImageClickEventArgs e)
        {
            var button = (ImageButton)sender;
            var id = (button.Parent.FindControl("hidBatchRateDataId")).ToCustomHiddenField().Value.ToLong();
            var data = new BatchRateData(id);
            Response.Export(data.ResultData);
        }



        protected void OnSubmitBatchRateDataForAnalysis(object sender, EventArgs e)
        {
            // get file content
            if (fupFileUpload.HasFile)
            {
                if (fupFileUpload.FileBytes.Length > WebApplicationSettings.AnalysisMaximumFileUploadSize)
                {
                    DisplayMessages(new[]
                        {
                            ValidationMessage.Error("File is too big. File size cannot exceed {0} bytes.",
                                                    WebApplicationSettings.AnalysisMaximumFileUploadSize)
                        });
                    return;
                }
            }

            List<string[]> lines;
            try
            {
                lines = fupFileUpload.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            if (lines.Count > WebApplicationSettings.AnalysisMaximumInputLineCount)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Batch import is limited to {0} records, please adjust your file size.", WebApplicationSettings.AnalysisMaximumInputLineCount.ToString("n2")) });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 9;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var countrySearch = new CountrySearch();

            var countryCodes = ProcessorVars.RegistryCache.Countries.Values.Select(c => c.Code).ToList();
            msgs.AddRange(chks.CodesAreValid(2, countryCodes, new Country().EntityName()));
            msgs.AddRange(chks.CodesAreValid(4, countryCodes, new Country().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var invalidPostalCodes = chks
                .Where(i => new PostalCodeSearch().FetchPostalCodeByCode(i.Line[1], countrySearch.FetchCountryIdByCode(i.Line[2])).Id == default(long))
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedCodeErrMsg, new PostalCode().EntityName(), i.Index))
                .ToList();
            invalidPostalCodes.AddRange(chks
                .Where(i => new PostalCodeSearch().FetchPostalCodeByCode(i.Line[3], countrySearch.FetchCountryIdByCode(i.Line[4])).Id == default(long))
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedCodeErrMsg, new PostalCode().EntityName(), i.Index))
                .ToList());
            if (invalidPostalCodes.Any())
            {
                invalidPostalCodes.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(invalidPostalCodes);
                return;
            }

            var content = lines.Select(i => i.TabJoin()).ToArray().NewLineJoin();

            var data = new BatchRateData
            {
                Name = txtName.Text,
                VendorRatingId = default(long),
                GroupCode = string.Empty,
                LTLIndirectPointEnabled = false,
                LaneData = content,
                AnalysisEffectiveDate = txtAnalysisEffectiveDate.Text.ToDateTime(),
                ServiceMode = ServiceMode.SmallPackage,
                SmallPackageEngine = ddlSmallPackageEngine.SelectedValue.ToEnum<SmallPackageEngine>(),
                SmallPackageEngineType = ddlEngineServiceType.SelectedValue,
                PackageTypeId = ddlPackageTypes.SelectedValue.ToLong(),
                ResultData = string.Empty,
                ChargeCodeId = default(long),
                DateCreated = DateTime.Now,
                DateCompleted = DateUtility.SystemEarliestDateTime,
                SubmittedByUser = ActiveUser,
                TenantId = ActiveUser.TenantId,
            };

            if (Save != null)
                Save(this, new ViewEventArgs<List<BatchRateData>>(new List<BatchRateData> { data }));

            LoadSmallPackageBatchRateDataSets();
        }

        protected void OnRefreshAnalysisSetList(object sender, EventArgs e)
        {
            LoadSmallPackageBatchRateDataSets();
        }


        protected void OnBatchRateDataEnqueue(object sender, ImageClickEventArgs e)
        {
            var button = (ImageButton)sender;
            var id = (button.Parent.FindControl("hidBatchRateDataId")).ToCustomHiddenField().Value.ToLong();
            RateAnalysisProcessor.Enqueue(id);
            DisplayMessages(new[] { ValidationMessage.Information("Data set scheduled for immediate processing.") });
        }


        protected void OnSmallPackageEngineSelectedIndexChanged(object sender, EventArgs e)
        {
            SetSmallPackTypes();
        }

        protected void OnRateAnalysisSetsItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var item = e.Item;

            var id = item.FindControl("hidBatchRateDataId").ToCustomHiddenField().Value.ToLong();
            var litProcessing = item.FindControl("litProcessing").ToLiteral();

            if (RateAnalysisProcessor.GetQueuedIds().Contains(id))
            {
                litProcessing.Visible = true;
            }
        }
    }
}
