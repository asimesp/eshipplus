﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Rating;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Rating
{
    public partial class LTLRateAnalysisView : MemberPageBase, IBatchRateDataView
    {
        private const string DeleteFlag = "D";
        private const string QueueFlag = "Q";
        protected const string DownloadForGroup = "DG";

        public static string PageAddress { get { return "~/Members/Rating/LTLRateAnalysisView.aspx"; } }

        public override ViewCode PageCode
        {
            get { return ViewCode.RateAnalysisLTL; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RatingBlue; }
        }

        public List<VendorRating> VendorRatingProfiles
        {
            set
            {
                rptVendorRatingProfiles.DataSource = value;
                rptVendorRatingProfiles.DataBind();
            }
        }

        public Dictionary<int, string> SmallPackEngines
        {
            set { }
        }

        public Dictionary<SmallPackageEngine, List<string>> SmallPackTypes
        {
            set { }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<List<BatchRateData>>> Save;
        public event EventHandler<ViewEventArgs<List<BatchRateData>>> Delete;
        public event EventHandler<ViewEventArgs<ServiceMode>> Search;
		public event EventHandler <ViewEventArgs<VendorRatingViewSearchCriteria>> RefreshVendorRatingProfiles;

		public void DisplayBatchRateDataSet(List<BatchRateData> data)
        {
            litRecordCount.Text = data.BuildRecordCount();
            rptRateAnalysisSets.DataSource = data.OrderBy(d => d.DateCreated)
                .Select(d => new
                {
                    d.Id,
                    d.Name,
                    VendorRatingProfile = d.VendorRating.Name,
                    d.VendorRatingId,
                    AllowIndirectPoint = d.LTLIndirectPointEnabled,
                    AnalysisEffectiveDate = d.AnalysisEffectiveDate.FormattedShortDate(),
                    DateSubmitted = d.DateCreated.FormattedLongDate(),
                    DateCompleted = d.DateCompleted == DateUtility.SystemEarliestDateTime
                                        ? string.Empty
                                        : d.DateCompleted.FormattedLongDate(),
                    SubmittedBy = d.SubmittedByUser.Username,
                    d.SubmittedByUserId,
                    d.GroupCode
                });
            rptRateAnalysisSets.DataBind();
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
            messageBox.Button = MessageButton.Ok;
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayGroupProcessMessage()
        {
            messageBox.Icon = MessageIcon.Question;
            messageBox.Message = "Process for all records in group?";
            messageBox.Visible = true;
            messageBox.Button = MessageButton.YesNo;
        }


        private void LoadLTLBatchRateDataSets()
        {
            if (Search != null)
                Search(this, new ViewEventArgs<ServiceMode>(ServiceMode.LessThanTruckload));
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            new BatchRateDataHandler(this).Initialize();

            if (IsPostBack) return;

	        RefreshVendorRatings();

			hidShowInactiveVendors.Value = false.ToString();

            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.LTLRateAnalysisImportTemplate });
            litMaxLineCount.Text = WebApplicationSettings.AnalysisMaximumInputLineCount.GetString();

            if (Loading != null)
                Loading(this, new EventArgs());
        }




        protected void OnBatchRateDataDelete(object sender, ImageClickEventArgs e)
        {
            var button = (ImageButton)sender;
            var id = button.Parent.FindControl("hidBatchRateDataId").ToCustomHiddenField().Value;
            var groupCode = button.Parent.FindControl("litGroupCode").ToLiteral().Text;
            var isPartOfGroup = rptRateAnalysisSets.Items.Cast<RepeaterItem>()
                .Any(i => i.FindControl("litGroupCode").ToLiteral().Text == groupCode &&
                          i.FindControl("hidBatchRateDataId").ToCustomHiddenField().Value != id);

            if (isPartOfGroup && !string.IsNullOrEmpty(groupCode))
            {
                hidProcessRecordId.Value = id;
                hidGroupProcessFlag.Value = DeleteFlag;
                DisplayGroupProcessMessage();
                return;
            }

            DeleteRecords(new[] { id.ToLong() });
        }

        private void DeleteRecords(IEnumerable<long> ids)
        {
            var batch = ids.Select(i => new BatchRateData(i)).ToList();

            if (Delete != null)
                Delete(this, new ViewEventArgs<List<BatchRateData>>(batch));

            LoadLTLBatchRateDataSets();
        }

	    private void RefreshVendorRatings()
	    {
		    if (RefreshVendorRatingProfiles != null)
		    {
			    var criteria = new VendorRatingViewSearchCriteria();
			    if (!chkShowInactive.Checked)
			    {
				    var paramenter = RatingSearchFields.Active.ToParameterColumn();
					paramenter.ReportColumnName = RatingSearchFields.Active.Name;
					paramenter.Operator = Operator.Equal;
				    paramenter.DefaultValue = true.ToString();
					criteria.Parameters.Add(paramenter);

				}
				if (!chkShowExpired.Checked)
				{
					var paramenter = RatingSearchFields.ExpirationDate.ToParameterColumn();
					paramenter.ReportColumnName = RatingSearchFields.ExpirationDate.Name;
					paramenter.Operator = Operator.GreaterThanOrEqual;
					paramenter.DefaultValue = DateTime.Now.ToString(CultureInfo.InvariantCulture);
					criteria.Parameters.Add(paramenter);
				}
				RefreshVendorRatingProfiles(this, new ViewEventArgs<VendorRatingViewSearchCriteria>(criteria));
			}
	    }

        protected void OnBatchRateDataInputDownload(object sender, ImageClickEventArgs e)
        {
            var button = (ImageButton)sender;
            var id = button.Parent.FindControl("hidBatchRateDataId").ToCustomHiddenField().Value.ToLong();
            var data = new BatchRateData(id);
            Response.Export(data.LaneData);
        }

        protected void OnBatchRateDataOutputDownload(object sender, ImageClickEventArgs e)
        {
            var button = (ImageButton)sender;
            var groupCode = button.Parent.FindControl("litGroupCode").ToLiteral().Text;

            if (hidGroupProcessFlag.Value == DownloadForGroup && !string.IsNullOrEmpty(groupCode))
            {
                var items = rptRateAnalysisSets.Items.Cast<RepeaterItem>()
                    .Select(i => new
                    {
                        GroupCode = i.FindControl("litGroupCode").ToLiteral().Text,
                        Id = i.FindControl("hidBatchRateDataId").ToCustomHiddenField().Value
                    })
                    .ToList();
                DownloadResults(items.Where(i => i.GroupCode == groupCode).Select(i => i.Id.ToLong()));
                return;
            }

            var id = button.Parent.FindControl("hidBatchRateDataId").ToCustomHiddenField().Value;
            DownloadResults(new[] { id.ToLong() });
        }

        private void DownloadResults(IEnumerable<long> ids)
        {
            try
            {
                var data = ids.Select(id => new BatchRateData(id)).ToList();
                var batch = data
                    .Select(d => d.ResultData.Split(Environment.NewLine.ToArray(), StringSplitOptions.RemoveEmptyEntries))
                    .Where(d => d.Any())
                    .ToList();

                // Must ensure we don't select the row count from a failed analysis set
                var rows = batch.Select(b => b.Length).Max(); // row count

                var resultData = new List<string>();
                var failedData = new List<string[]>();
                failedData.AddRange(batch.Where(b => b.Any(r => r.Split('\t').Length == 1)));

                for (var row = 0; row < rows; row++)
                {
                    var line = batch.Where(b => b.All(r => r.Split('\t').Length > 1)).Aggregate(string.Empty, (current, dataLine) => string.IsNullOrEmpty(current)
                                                                                        ? dataLine[row]
                                                                                        : new[] { current, string.Empty, dataLine[row] }.TabJoin());
                    resultData.Add(line);
                }

                var title = string.Empty;

                // Ensure we exclude Vendor Ratings that have failed
                foreach (var d in data.Where(d => resultData.Any(r => r.Contains(d.VendorRating.Name))))
                {
                    var colCount = d.ResultData.Split(Environment.NewLine.ToArray(), StringSplitOptions.RemoveEmptyEntries).First().Split('\t').Length;
                    var buffer = new string[colCount];
                    for (var i = 0; i < buffer.Length; i++) buffer[i] = string.Empty;
                    buffer[0] = d.VendorRating.Name;

                    title = string.IsNullOrEmpty(title) ? buffer.TabJoin() : new[] { title, string.Empty, buffer.TabJoin() }.TabJoin();
                }

                resultData.Insert(0, title);
                resultData.Add(string.Empty);
                resultData.Add(string.Empty);
                resultData.Add("Failed Rate Analysis Sets");
                resultData.AddRange(failedData.Select(s => s.TabJoin()));

                Response.Export(resultData.ToArray().NewLineJoin());
            }
            catch (Exception ex)
            {
                LogException(ex);
                DisplayMessages(new[]{ ValidationMessage.Error("An error occurred while trying to download the results set")});
            }
        }



        protected void OnSubmitBatchRateDataForAnalysis(object sender, EventArgs e)
        {
            if (fupFileUpload.HasFile && fupFileUpload.FileBytes.Length > WebApplicationSettings.AnalysisMaximumFileUploadSize)
            {
                DisplayMessages(new[] { ValidationMessage.Error("File is too big. File size cannot exceed {0} bytes.", WebApplicationSettings.AnalysisMaximumFileUploadSize) });
                return;
            }

            List<string[]> lines;
            try
            {
                lines = fupFileUpload.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            if (lines.Count > WebApplicationSettings.AnalysisMaximumInputLineCount)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Batch import is limited to {0} records, please adjust your file size.", WebApplicationSettings.AnalysisMaximumInputLineCount.ToString("n2")) });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 7;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var countrySearch = new CountrySearch();

            var countryCodes = ProcessorVars.RegistryCache.Countries.Values.Select(c => c.Code).ToList();
            msgs.AddRange(chks.CodesAreValid(2, countryCodes, new Country().EntityName()));
            msgs.AddRange(chks.CodesAreValid(4, countryCodes, new Country().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var invalidPostalCodes = chks
                .Where(i => new PostalCodeSearch().FetchPostalCodeByCode(i.Line[1], countrySearch.FetchCountryIdByCode(i.Line[2])).Id == default(long))
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedCodeErrMsg, new PostalCode().EntityName(), i.Index))
                .ToList();
            invalidPostalCodes.AddRange(chks
                .Where(i => new PostalCodeSearch().FetchPostalCodeByCode(i.Line[3], countrySearch.FetchCountryIdByCode(i.Line[4])).Id == default(long))
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedCodeErrMsg, new PostalCode().EntityName(), i.Index))
                .ToList());
            if (invalidPostalCodes.Any())
            {
                invalidPostalCodes.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(invalidPostalCodes);
                return;
            }

            var freightClasses = WebApplicationSettings.FreightClasses.Select(f => f.ToString()).ToList();
            var invalidFreightClass = chks
                .Where(i => !freightClasses.Contains(i.Line[6]))
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidFreightClassErrMsg, i.Index))
                .ToList();
            if (invalidFreightClass.Any())
            {
                invalidFreightClass.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(invalidFreightClass);
                return;
            }

            var content = lines.Select(i => i.TabJoin()).ToArray().NewLineJoin();

            var selectedProfileIds = rptVendorRatingProfiles
                .Items
                .Cast<RepeaterItem>()
                .Where(i => i.FindControl("chkSelected").ToAltUniformCheckBox().Checked)
                .Select(i => i.FindControl("hidVendorRatingId").ToCustomHiddenField().Value)
                .ToList();
            if (!selectedProfileIds.Any())
            {
                DisplayMessages(new[] { ValidationMessage.Error("At least one vendor rating profile must be selected!") });
                return;
            }

            var groupCode = selectedProfileIds.Count > 1 && string.IsNullOrEmpty(txtGroupCode.Text)
                                ? DateTime.Now.ToString("yyMMddhhmmssffff")
                                : txtGroupCode.Text;

            var batch = selectedProfileIds
                .Select(profileId => new BatchRateData
                {
                    Name = txtName.Text,
                    VendorRatingId = profileId.ToLong(),
                    GroupCode = groupCode,
                    LTLIndirectPointEnabled = chkLtlIndirectPointEnabled.Checked,
                    LaneData = content,
                    AnalysisEffectiveDate = txtAnalysisEffectiveDate.Text.ToDateTime(),
                    ServiceMode = ServiceMode.LessThanTruckload,
                    SmallPackageEngine = SmallPackageEngine.None,
                    SmallPackageEngineType = string.Empty,
                    PackageTypeId = default(long),
                    ResultData = string.Empty,
                    ChargeCodeId = default(long),
                    DateCreated = DateTime.Now,
                    DateCompleted = DateUtility.SystemEarliestDateTime,
                    SubmittedByUser = ActiveUser,
                    TenantId = ActiveUser.TenantId,
                })
                .ToList();

            if (Save != null)
                Save(this, new ViewEventArgs<List<BatchRateData>>(batch));

            LoadLTLBatchRateDataSets();
        }

        protected void OnRefreshAnalysisSetList(object sender, EventArgs e)
        {
            LoadLTLBatchRateDataSets();
        }


        protected void OnBatchRateDataEnqueue(object sender, ImageClickEventArgs e)
        {
            var button = (ImageButton)sender;
            var id = button.Parent.FindControl("hidBatchRateDataId").ToCustomHiddenField().Value;
            var groupCode = button.Parent.FindControl("litGroupCode").ToLiteral().Text;
            var isPartOfGroup = rptRateAnalysisSets
                .Items
                .Cast<RepeaterItem>()
                .Any(i => i.FindControl("litGroupCode").ToLiteral().Text == groupCode && i.FindControl("hidBatchRateDataId").ToCustomHiddenField().Value != id);

            if (isPartOfGroup && !string.IsNullOrEmpty(groupCode))
            {
                hidProcessRecordId.Value = id;
                hidGroupProcessFlag.Value = QueueFlag;
                DisplayGroupProcessMessage();
                return;
            }

            QueueRecords(new[] { id.ToLong() });
        }

        private void QueueRecords(IEnumerable<long> ids)
        {
            foreach (var id in ids) RateAnalysisProcessor.Enqueue(id);
            DisplayMessages(new[] { ValidationMessage.Information("Data set scheduled for immediate processing.") });
        }


        protected void OnRateAnalysisSetsItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var item = e.Item;

            var id = item.FindControl("hidBatchRateDataId").ToCustomHiddenField().Value.ToLong();
            var litProcessing = item.FindControl("litProcessing").ToLiteral();

            if (RateAnalysisProcessor.GetQueuedIds().Contains(id))
            {
                litProcessing.Visible = true;
            }
        }

        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;
        }

        protected void OnYesProcess(object sender, EventArgs e)
        {
            var items = rptRateAnalysisSets.Items.Cast<RepeaterItem>()
                .Select(i => new
                {
                    GroupCode = i.FindControl("litGroupCode").ToLiteral().Text,
                    Id = i.FindControl("hidBatchRateDataId").ToCustomHiddenField().Value
                })
                .ToList();

            switch (hidGroupProcessFlag.Value)
            {
                case DeleteFlag:
                    var di = items.First(i => i.Id == hidProcessRecordId.Value);
                    DeleteRecords(items.Where(i => i.GroupCode == di.GroupCode).Select(i => i.Id.ToLong()));
                    break;
                case QueueFlag:
                    var qi = items.First(i => i.Id == hidProcessRecordId.Value);
                    QueueRecords(items.Where(i => i.GroupCode == qi.GroupCode).Select(i => i.Id.ToLong()));
                    break;
                default:
                    DisplayMessages(new[] { ValidationMessage.Error("Unknown operation!") });
                    break;
            }

            hidProcessRecordId.Value = string.Empty;
            hidGroupProcessFlag.Value = string.Empty;
            messageBox.Visible = false;
        }

        protected void OnNoProcess(object sender, EventArgs e)
        {
            switch (hidGroupProcessFlag.Value)
            {
                case DeleteFlag:
                    DeleteRecords(new[] { hidProcessRecordId.Value.ToLong() });
                    break;
                case QueueFlag:
                    QueueRecords(new[] { hidProcessRecordId.Value.ToLong() });
                    break;
                default:
                    DisplayMessages(new[] { ValidationMessage.Error("Unknown operation!") });
                    break;
            }

            hidProcessRecordId.Value = string.Empty;
            hidGroupProcessFlag.Value = string.Empty;
            messageBox.Visible = false;
        }

		protected void OnShowVendorsCheckChanged(object sender, EventArgs e)
		{
			RefreshVendorRatings();
		}
	}
}
