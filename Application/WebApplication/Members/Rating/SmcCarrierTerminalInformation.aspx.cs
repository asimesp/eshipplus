﻿using System;
using System.Linq;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Smc;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Rating
{
	public partial class SmcCarrierTerminalInformation : MemberPageBase
	{
		public static string PageAddress { get { return "~/Members/Rating/SmcCarrierTerminalInformation.aspx"; } }

		public override ViewCode PageCode { get { return ViewCode.SmcPointToPointServiceCheck; } }

		public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.RatingBlue; } }

		protected void Page_Load(object sender, EventArgs e)
		{
			var scac = Request.QueryString[WebApplicationConstants.CarrierScac].GetString();
			var carrierName = Request.QueryString[WebApplicationConstants.CarrierName].GetString();
			var originTerminalCode = Request.QueryString[WebApplicationConstants.OriginTerminalCode].GetString();
			var destinationTerminalCode = Request.QueryString[WebApplicationConstants.DestinationTerminalCode].GetString();

			try
			{
				var variables = ProcessorVars.IntegrationVariables[ActiveUser.TenantId];
				var carrierConnect = new CarrierConnect(variables.CarrierConnectParameters, ProcessorVars.RegistryCache.Countries.Values.ToList());

				var terminals = carrierConnect.GetLTLTerminalInfo(scac, new[] { originTerminalCode, destinationTerminalCode }, DateTime.Now);

				var origin = terminals.FirstOrDefault(t => t.Code == originTerminalCode);
				var dest = terminals.FirstOrDefault(t => t.Code == destinationTerminalCode);

				litCarrierName.Text = carrierName;
				litOrigin.Text = origin == null ? "Origin Terminal Not Found!" : origin.BuildTerminalDisplay();
				litDestination.Text = dest == null ? "Destination Terminal Not Found!" : dest.BuildTerminalDisplay();
			}
			catch (Exception ex)
			{
				ErrorLogger.LogError(ex, Context);
				litOrigin.Text = "Error retrieving origin terminal information";
				litDestination.Text = "Error retrieve destination terminal information";
			}
		}
	}
}