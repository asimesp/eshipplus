﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Smc;
using LogisticsPlus.Eship.Smc.Rates;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Rating
{
    public partial class TariffRateAnalysisView : MemberPageBase
    {
        private struct Dimensions
        {
            public string Key { get; set; }
            public string Length { get; set; }
            public string Width { get; set; }
            public string Height { get; set; }
            public string Quantity { get; set; }
        }

        private const string FreightClassType = "Freight Class Rates";
        private const string DensityType = "Density Rates";

        private readonly string _header = new[]
				{
					"Tariff Name", "Effective Date", "Original Value ($)", "Actual Weight (lb)", "Billed Weight (lb)",
					"Minimum Charge ($)", "Deficit Rate ($)", "Deficit Weight (lb)", "Deficit Charge ($)", "Origin State",
					"Origin Postal Code", "Origin Country", "Destination State", "Destination Postal Code",
					"Destination Country"
				}.TabJoin();

        private readonly string _batchHeader = new[]
				{
					"Shipment ID", "Tariff Name", "Effective Date", "Original Value ($)", "Actual Weight (lb)", "Billed Weight (lb)",
					"Minimum Charge ($)", "Deficit Rate ($)", "Deficit Weight (lb)", "Deficit Charge ($)", "Origin Postal Code",
					"Origin Country", "Destination Postal Code", "Destination Country", "Unit Weight", "Unit Freight Class",
					"Unit Length", "Unit Width", "Unit Height", "Quantity"
				}.TabJoin();

        public static string PageAddress
        {
            get { return "~/Members/Rating/TariffRateAnalysisView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.TariffRateAnalysis; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RatingBlue; }
        }


        private static List<ToolbarMoreAction> ToolbarMoreActions()
        {
            var tariffDetails = new ToolbarMoreAction
            {
                CommandArgs = string.Empty,
                ImageUrl = IconLinks.Rating,
                Name = ViewCode.TariffsDetailLTL.FormattedString(),
                IsLink = true,
                OpenInNewWindow = true,
                NavigationUrl = LTLTariffsDetailView.PageAddress
            };

            var actions = new List<ToolbarMoreAction> { tariffDetails };
            return actions;
        }


        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }


        private void AddItem()
        {
            var items = rptItems.Items.Cast<RepeaterItem>()
                .Select(i => new
                {
                    Weight = i.FindControl("txtWeight").ToTextBox().Text,
                    FreightClass = i.FindControl("ddlFreightClass").ToDropDownList().SelectedValue,
                    Length = i.FindControl("txtLength").ToTextBox().Text,
                    Width = i.FindControl("txtWidth").ToTextBox().Text,
                    Height = i.FindControl("txtHeight").ToTextBox().Text,
                    Quantity = i.FindControl("txtQuantity").ToTextBox().Text,
                })
                .ToList();
            if (items.Count < 101)
                items.Add(
                    new
                    {
                        Weight = string.Empty,
                        FreightClass = WebApplicationSettings.FreightClasses[0].ToString(),
                        Length = string.Empty,
                        Width = string.Empty,
                        Height = string.Empty,
                        Quantity = string.Empty,
                    });
            rptItems.DataSource = items;
            rptItems.DataBind();
        }
        

        private void ProcessFreightClassRating(string selectedTariff, IEnumerable<string[]> inputLines)
        {
            var requests = new List<TariffAnalysisRequest>();
            var dimensions = new List<Dimensions>();

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = inputLines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 11;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            //Checks if country code is 'USA', 'MEX', or 'CAN'
            msgs.AddRange(chks
                .Where(i => !IsValidImportedCountryCode(i.Line[2]) || !IsValidImportedCountryCode(i.Line[4]))
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedCodeErrMsg, new Country().EntityName(), i.Index)));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var invalidPostalCodes = chks
                .Where(i => new PostalCodeSearch().FetchPostalCodeByCode(i.Line[1], ConvertImportedCountryCodeToId(i.Line[2])).Id == default(long))
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedCodeErrMsg, new PostalCode().EntityName(), i.Index))
                .ToList();
            invalidPostalCodes.AddRange(chks
                .Where(i => new PostalCodeSearch().FetchPostalCodeByCode(i.Line[3], ConvertImportedCountryCodeToId(i.Line[4])).Id == default(long))
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedCodeErrMsg, new PostalCode().EntityName(), i.Index))
                .ToList());
            if (invalidPostalCodes.Any())
            {
                invalidPostalCodes.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(invalidPostalCodes);
                return;
            }

            var freightClasses = WebApplicationSettings.FreightClasses.Select(f => f.ToString()).ToList();
            var invalidFreightClass = chks
                .Where(i => !freightClasses.Contains(i.Line[6]))
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidFreightClassErrMsg, i.Index))
                .ToList();
            if (invalidFreightClass.Any())
            {
                invalidFreightClass.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(invalidFreightClass);
                return;
            }

            foreach (var parts in inputLines)
            {
                requests.Add(new TariffAnalysisRequest
                {
                    DestinationCountry = parts[4],
                    DestinationPostalCode = parts[3],
                    OriginCountry = parts[2],
                    OriginPostalCode = parts[1],
                    Key = parts[0],
                    WeightBreak = ddlWeightBreak.SelectedValue.ToEnum<WeightBreak>(),
                    Items = new List<TariffAnalysisDetail>
				             		        	{
				             		        		new TariffAnalysisDetail
				             		        			{
				             		        				Weight = parts[5],
				             		        				FreightClass = parts[6]
				             		        			}
				             		        	},
                });
                dimensions.Add(new Dimensions
                {
                    Key = parts[0],
                    Length = parts[7],
                    Width = parts[8],
                    Height = parts[9],
                    Quantity = parts[10]
                });
            }

            try
            {
                var rateware = new Rateware2(RatewareSettings, CarrierConnectSettings);

                var results = rateware.ProcessTariffRateAnalysis(selectedTariff, requests, DiscountTierRateBasis.FreightClass);

                var lines = results
                    .Select(r => !r.HasError
                                    ? new[]
					             	  	{
					             	  		r.Key, 
					             	  		r.TariffName, 
                                            r.TariffDate, 
                                            r.OriginalValue, 
                                            r.ActualWeight, 
                                            r.BilledWeight,
					             	  		r.MinimumCharge, 
                                            r.DeficitRate, 
                                            r.DeficitWeight, 
                                            r.DeficitCharge, 
                                            r.OriginPostalCode, 
                                            r.OriginCountry,  
                                            r.DestinationPostalCode,
                                            r.DestinationCountry,
                                            r.Items.Select(d => d.Weight).FirstOrDefault(),
                                            r.Items.Select(d => d.FreightClass).FirstOrDefault(),
                                            dimensions.Where(i => i.Key == r.Key).Select(i => i.Length).FirstOrDefault(),
                                            dimensions.Where(i => i.Key == r.Key).Select(i => i.Width).FirstOrDefault(),
                                            dimensions.Where(i => i.Key == r.Key).Select(i => i.Height).FirstOrDefault(),
                                            dimensions.Where(i => i.Key == r.Key).Select(i => i.Quantity).FirstOrDefault()
					             	  	}.TabJoin()
                                    : new[] { r.Key, string.Format("Error: {0}", r.Error) }.TabJoin())
                    .ToList();

                // export
                lines.Insert(0, _batchHeader);
                lines.Insert(0, FreightClassType);
                Response.Export(lines.ToArray().NewLineJoin());
            }
            catch (Exception e)
            {
                ErrorLogger.LogError(e, Context);
                DisplayMessages(new[] { ValidationMessage.Error(e.Message) });
            }
        }

        private void ProcessDensityRating(string selectedTariff, IEnumerable<string[]> inputLines)
        {
            var requests = new List<TariffAnalysisRequest>();
            var dimensions = new List<Dimensions>();

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = inputLines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 11;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            //Checks if country code is 'USA', 'MEX', or 'CAN'
            msgs.AddRange(chks
                .Where(i => !IsValidImportedCountryCode(i.Line[2]) || !IsValidImportedCountryCode(i.Line[4]))
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedCodeErrMsg, new Country().EntityName(), i.Index)));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var invalidPostalCodes = chks
                .Where(i => new PostalCodeSearch().FetchPostalCodeByCode(i.Line[1], ConvertImportedCountryCodeToId(i.Line[2])).Id == default(long))
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedCodeErrMsg, new PostalCode().EntityName(), i.Index))
                .ToList();
            invalidPostalCodes.AddRange(chks
                .Where(i => new PostalCodeSearch().FetchPostalCodeByCode(i.Line[3], ConvertImportedCountryCodeToId(i.Line[4])).Id == default(long))
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedCodeErrMsg, new PostalCode().EntityName(), i.Index))
                .ToList());
            if (invalidPostalCodes.Any())
            {
                invalidPostalCodes.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(invalidPostalCodes);
                return;
            }

            foreach (var parts in inputLines)
            {
                requests.Add(new TariffAnalysisRequest
                {
                    DestinationCountry = parts[4],
                    DestinationPostalCode = parts[3],
                    OriginCountry = parts[2],
                    OriginPostalCode = parts[1],
                    Key = parts[0],
                    WeightBreak = ddlWeightBreak.SelectedValue.ToEnum<WeightBreak>(),
                    Items = new List<TariffAnalysisDetail>
				             		        	{
				             		        		new TariffAnalysisDetail
				             		        			{
				             		        				Weight = parts[5],
				             		        				Length = parts[7],
				             		        				Width = parts[8],
				             		        				Height = parts[9],
				             		        				Quantity = parts[10],
				             		        			}
				             		        	},
                });

                dimensions.Add(new Dimensions
                {
                    Key = parts[0],
                    Length = parts[7],
                    Width = parts[8],
                    Height = parts[9],
                    Quantity = parts[10]
                });
            }


            try
            {
                var rateware = new Rateware2(RatewareSettings, CarrierConnectSettings);

                var results = rateware.ProcessTariffRateAnalysis(selectedTariff, requests, DiscountTierRateBasis.FreightClass);

                var lines = results
                    .Select(r => !r.HasError
                                    ? new[]
					             	  	{
					             	  		r.Key, 
					             	  		r.TariffName, 
                                            r.TariffDate, 
                                            r.OriginalValue, 
                                            r.ActualWeight, 
                                            r.BilledWeight,
					             	  		r.MinimumCharge, 
                                            r.DeficitRate, 
                                            r.DeficitWeight, 
                                            r.DeficitCharge, 
                                            r.OriginPostalCode, 
                                            r.OriginCountry,  
                                            r.DestinationPostalCode,
                                            r.DestinationCountry,
                                            r.Items.Select(d => d.Weight).FirstOrDefault(),
                                            "not applicable",
                                            dimensions.Where(i => i.Key == r.Key).Select(i => i.Length).FirstOrDefault(),
                                            dimensions.Where(i => i.Key == r.Key).Select(i => i.Width).FirstOrDefault(),
                                            dimensions.Where(i => i.Key == r.Key).Select(i => i.Height).FirstOrDefault(),
                                            dimensions.Where(i => i.Key == r.Key).Select(i => i.Quantity).FirstOrDefault()
					             	  	}.TabJoin()
                                    : new[] { r.Key, string.Format("Error: {0}", r.Error) }.TabJoin())
                    .ToList();

                // export
                lines.Insert(0, _batchHeader);
                lines.Insert(0, DensityType);
                Response.Export(lines.ToArray().NewLineJoin());
            }
            catch (Exception e)
            {
                ErrorLogger.LogError(e, Context);
                DisplayMessages(new[] { ValidationMessage.Error(e.Message) });
            }
        }


        private static long ConvertImportedCountryCodeToId(string countryCode)
        {
            switch (countryCode.ToUpper())
            {
                case "USA":
                    return new CountrySearch().FetchCountryIdByCode("US");
                case "MEX":
                    return new CountrySearch().FetchCountryIdByCode("MX");
                case "CAN":
                    return new CountrySearch().FetchCountryIdByCode("CA");
                default:
                    return default(long);

            }
        }

        private static bool IsValidImportedCountryCode(string countryCode)
        {
            return (countryCode == "USA" || countryCode == "MEX" || countryCode == "CAN");
        }



        private void RunSingleLaneAnalysis(bool exportResults)
        {
            var selectedTariffs = rptTariffs
                .Items
                .Cast<RepeaterItem>()
                .Where(i => i.FindControl("chkSelected").ToCheckBox().Checked)
                .Select(i => i.FindControl("hidTariff").ToCustomHiddenField().Value)
                .ToList();

            if (!selectedTariffs.Any())
            {
                DisplayMessages(new[] { ValidationMessage.Error("There must be at least one tariff selected for rating.") });
                return;
            }

            if (chkDensityRates.Checked) ProcessDensityRating(selectedTariffs, exportResults);
            else ProcessFreightClassRating(selectedTariffs, exportResults);
        }

        private void ProcessFreightClassRating(IEnumerable<string> selectedTariffs, bool exportResults)
        {
            // request
            var request = new TariffAnalysisRequest
            {
                DestinationCountry = ddlDestCountry.SelectedValue,
                DestinationPostalCode = txtDestPostalCode.Text,
                OriginCountry = txtOriginCountry.SelectedValue,
                OriginPostalCode = txtOriginPostalCode.Text,
                WeightBreak = ddlWeightBreak.SelectedValue.ToEnum<WeightBreak>(),
                Items = rptItems.Items.Cast<RepeaterItem>()
                    .Select(i => new TariffAnalysisDetail
                    {
                        Weight = (i.FindControl("txtWeight")).ToTextBox().Text,
                        FreightClass = i.FindControl("ddlFreightClass").ToDropDownList().SelectedValue,
                    })
                    .ToList()
            };

            // validate
            var errors = new List<ValidationMessage>();
            for (var i = 0; i < request.Items.Count; i++)
            {
                if (string.IsNullOrEmpty(request.Items[i].Weight)) errors.Add(ValidationMessage.Error("Item #{0} weight is required", i + 1));
                if (string.IsNullOrEmpty(request.Items[i].FreightClass)) errors.Add(ValidationMessage.Error("Item #{0} freight class is required", i + 1));
            }
            if (errors.Any())
            {
                DisplayMessages(errors);
                rptSmcResults.DataSource = new List<object>();
                rptSmcResults.DataBind();
                return;
            }

            try
            {
                var rateware = new Rateware2(RatewareSettings, CarrierConnectSettings);

                var results = rateware.ProcessTariffRateAnalysis(selectedTariffs, request, DiscountTierRateBasis.FreightClass);

                // do errors
                var responseErrors = results
                    .Where(r => r.HasError)
                    .Select(r => ValidationMessage.Error("Tariff: {0} {1} Error: {2}", r.TariffName, r.TariffDate, r.Error))
                    .ToList();
                if (responseErrors.Any()) DisplayMessages(responseErrors);

                // do export or display
                if (exportResults)
                {
                    var lines = results
                        .Select(r => !r.HasError
                                        ? new[]
						             	  	{
						             	  		r.TariffName, r.TariffDate, r.OriginalValue, r.ActualWeight, r.BilledWeight,
						             	  		r.MinimumCharge, r.DeficitRate, r.DeficitWeight, r.DeficitCharge, r.OriginState,
						             	  		r.OriginPostalCode, r.OriginCountry, r.DestinationState, r.DestinationPostalCode,
						             	  		r.DestinationCountry
						             	  	}.TabJoin()
                                        : string.Format("Tariff: {0} {1} Error: {2}", r.TariffName, r.TariffDate, r.Error))
                        .ToList();
                    lines.Insert(0, _header);
                    lines.Insert(0, FreightClassType);
                    Response.Export(lines.ToArray().NewLineJoin());
                }
                else
                {
                    ratingType.Text = FreightClassType;
                    rptSmcResults.DataSource = results;
                    rptSmcResults.DataBind();
                }
            }
            catch (Exception e)
            {
                ErrorLogger.LogError(e, Context);
                DisplayMessages(new[] { ValidationMessage.Error(e.Message) });
            }
        }

        private void ProcessDensityRating(IEnumerable<string> selectedTariffs, bool exportResults)
        {
            // request
            var request = new TariffAnalysisRequest
            {
                DestinationCountry = ddlDestCountry.SelectedValue,
                DestinationPostalCode = txtDestPostalCode.Text,
                OriginCountry = txtOriginCountry.SelectedValue,
                OriginPostalCode = txtOriginPostalCode.Text,
                WeightBreak = ddlWeightBreak.SelectedValue.ToEnum<WeightBreak>(),
                Items = rptItems.Items.Cast<RepeaterItem>()
                    .Select(i => new TariffAnalysisDetail
                    {
                        Weight = i.FindControl("txtWeight").ToTextBox().Text,
                        Length = i.FindControl("txtLength").ToTextBox().Text,
                        Width = i.FindControl("txtWidth").ToTextBox().Text,
                        Height = i.FindControl("txtHeight").ToTextBox().Text,
                        Quantity = i.FindControl("txtQuantity").ToTextBox().Text,
                    })
                    .ToList()
            };

            // validate
            var errors = new List<ValidationMessage>();
            for (var i = 0; i < request.Items.Count; i++)
            {
                if (string.IsNullOrEmpty(request.Items[i].Weight)) errors.Add(ValidationMessage.Error("Item #{0} weight is required", i + 1));
                if (string.IsNullOrEmpty(request.Items[i].Length)) errors.Add(ValidationMessage.Error("Item #{0} length is required", i + 1));
                if (string.IsNullOrEmpty(request.Items[i].Width)) errors.Add(ValidationMessage.Error("Item #{0} width is required", i + 1));
                if (string.IsNullOrEmpty(request.Items[i].Height)) errors.Add(ValidationMessage.Error("Item #{0} height is required", i + 1));
                if (string.IsNullOrEmpty(request.Items[i].Quantity)) errors.Add(ValidationMessage.Error("Item #{0} quantity is required", i + 1));
            }
            if (errors.Any())
            {
                DisplayMessages(errors);

                rptSmcResults.DataSource = new List<object>();
                rptSmcResults.DataBind();
                return;
            }

            try
            {
                var rateware = new Rateware2(RatewareSettings, CarrierConnectSettings);

                var results = rateware.ProcessTariffRateAnalysis(selectedTariffs, request, DiscountTierRateBasis.Density);

                // do errors
                var responseErrors = results
                    .Where(r => r.HasError)
                    .Select(r => ValidationMessage.Error("Tariff: {0} {1} Error: {2}", r.TariffName, r.TariffDate, r.Error))
                    .ToList();
                if (responseErrors.Any()) DisplayMessages(responseErrors);

                // do export or display
                if (exportResults)
                {
                    var lines = results
                        .Select(r => !r.HasError
                                        ? new[]
						             	  	{
						             	  		r.TariffName, r.TariffDate, r.OriginalValue, r.ActualWeight, r.BilledWeight,
						             	  		r.MinimumCharge, r.DeficitRate, r.DeficitWeight, r.DeficitCharge, r.OriginState,
						             	  		r.OriginPostalCode, r.OriginCountry, r.DestinationState, r.DestinationPostalCode,
						             	  		r.DestinationCountry
						             	  	}.TabJoin()
                                        : string.Format("Tariff: {0} {1} Error: {2}", r.TariffName, r.TariffDate, r.Error))
                        .ToList();
                    lines.Insert(0, _header);
                    lines.Insert(0, DensityType);
                    Response.Export(lines.ToArray().NewLineJoin());
                }
                else
                {
                    ratingType.Text = DensityType;
                    rptSmcResults.DataSource = results;
                    rptSmcResults.DataBind();
                }
            }
            catch (Exception e)
            {
                ErrorLogger.LogError(e, Context);
                DisplayMessages(new[] { ValidationMessage.Error(e.Message) });
            }
        }



        protected void Page_Load(object sender, EventArgs e)
        {

            if (IsPostBack) return;

            memberToolBar.LoadAdditionalActions(ToolbarMoreActions());
            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.TarrifRateAnalysisImportTemplate });

            rptTariffs.DataSource = new Rateware2(RatewareSettings, CarrierConnectSettings)
                .ListAvailableTariffs()
                .ToDictionary(t => t, TariffNameFormatter.ReaderFriendlyTariff)
                .Select(t => new ViewListItem(" " + t.Value, t.Key))
                .ToList();
            rptTariffs.DataBind();

            ddlWeightBreak.DataSource = ProcessorUtilities.GetAll<WeightBreak>();
            ddlWeightBreak.DataBind();

            AddItem();
        }



        protected void OnRateClicked(object sender, EventArgs e)
        {
            RunSingleLaneAnalysis(false);
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            RunSingleLaneAnalysis(true);
        }

        protected void OnBatchRateClicked(object sender, EventArgs e)
        {
            var selectedTariffs = rptTariffs
                .Items
                .Cast<RepeaterItem>()
                .Where(i => i.FindControl("chkSelected").ToCheckBox().Checked)
                .Select(i => i.FindControl("hidTariff").ToCustomHiddenField().Value)
                .ToList();

            if (!selectedTariffs.Any())
            {
                DisplayMessages(new[] { ValidationMessage.Error("There must be at least one tariff selected for rating.") });
                return;
            }

            if (selectedTariffs.Count > 1)
            {
                DisplayMessages(new[] { ValidationMessage.Error("You can have only one tariff selected for batch rating.") });
                return;
            }

            fileUploader.Title = "BATCH TARIFF INPUT FILE IMPORT";
            fileUploader.Instructions = @"**Please refer to instructions for import file format.";
            fileUploader.Visible = true;
        }



        protected void OnItemsItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var freightClasses = e.Item.FindControl("ddlFreightClass").ToDropDownList();

            if (freightClasses == null) return;

            freightClasses.DataSource = WebApplicationSettings.FreightClasses.Select(f => new ListItem(f.ToString(), f.ToString()));
            freightClasses.DataBind();

            freightClasses.SelectedValue = e.Item.DataItem.GetPropertyValue("FreightClass").ToString();
        }


        protected void OnAddItemClicked(object sender, EventArgs e)
        {
            AddItem();
        }

        protected void OnDeleteItemClicked(object sender, EventArgs e)
        {
            var items = rptItems.Items.Cast<RepeaterItem>()
               .Select(i => new
               {
                   Weight = i.FindControl("txtWeight").ToTextBox().Text,
                   FreightClass = i.FindControl("ddlFreightClass").ToDropDownList().SelectedValue,
                   Length = i.FindControl("txtLength").ToTextBox().Text,
                   Width = i.FindControl("txtWidth").ToTextBox().Text,
                   Height = i.FindControl("txtHeight").ToTextBox().Text,
                   Quantity = i.FindControl("txtQuantity").ToTextBox().Text,
               })
               .ToList();

            items.RemoveAt(((ImageButton)sender).Parent.FindControl("hidIndex").ToCustomHiddenField().Value.ToInt());

            rptItems.DataSource = items;
            rptItems.DataBind();
        }


        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            fileUploader.Visible = false;

            try
            {
                var selectedTariff = rptTariffs
                                         .Items
                                         .Cast<RepeaterItem>()
                                         .Where(i => i.FindControl("chkSelected").ToCheckBox().Checked)
                                         .Select(i => i.FindControl("hidTariff").ToCustomHiddenField().Value)
                                         .FirstOrDefault() ?? string.Empty;

                var lines = e.GetImportData(true);

                if (lines.Count > WebApplicationSettings.AnalysisMaximumInputLineCount)
                {
                    DisplayMessages(new[] { ValidationMessage.Error("Maximum number of records that can be processed at one time is {0}", WebApplicationSettings.AnalysisMaximumInputLineCount) });
                    return;
                }

                if (chkDensityRates.Checked) ProcessDensityRating(selectedTariff, lines);
                else ProcessFreightClassRating(selectedTariff, lines);
            }
            catch (Exception ex)
            {
                ErrorLogger.LogError(ex, Context);
                DisplayMessages(new[] { ValidationMessage.Error(ex.Message) });
            }
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }



        protected void OnSmcResultsItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var smcResultUnit = e.Item.FindControl("smcResultUnits").ToRepeater();
            var data = e.Item.DataItem as TariffAnalysisResponse;

            if (smcResultUnit == null) return;
            if (data == null) return;

            smcResultUnit.DataSource = data.Items;
            smcResultUnit.DataBind();
        }
    }
}
