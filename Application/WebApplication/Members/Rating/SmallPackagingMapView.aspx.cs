﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Rating;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Rating
{
    public partial class SmallPackagingMapView : MemberPageBase, ISmallPackagingMapView
    {
        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public static string PageAddress { get { return "~/Members/Rating/SmallPackagingMapView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.SmallPackagingMaps; } }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RatingBlue; }
        }

        public Dictionary<int, string> SmallPackageEngines
        {
            set
            {
                ddlSmallPackEngine.DataSource = value.Keys.Select(k => new ViewListItem(value[k], k.GetString())).ToList();
                ddlSmallPackEngine.DataBind();
            }
        }

        public Dictionary<SmallPackageEngine, Dictionary<string, string>> SmallPackageEnginesTypes
        {
            get
            {
                return
                    ViewState["SmallPackagingMapsSmallPackTypes"] as Dictionary<SmallPackageEngine, Dictionary<string, string>> ??
                    new Dictionary<SmallPackageEngine, Dictionary<string, string>>();
            }
            set
            {
                ViewState["SmallPackagingMapsSmallPackTypes"] = value;
                SetSmallPackagingTypes();
            }
        }


        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<SmallPackagingMap>> Save;
        public event EventHandler<ViewEventArgs<SmallPackagingMap>> Delete;
        public event EventHandler<ViewEventArgs<SmallPackagingMap>> Lock;
        public event EventHandler<ViewEventArgs<SmallPackagingMap>> UnLock;
        public event EventHandler<ViewEventArgs<string>> Search;

        public void DisplaySearchResult(List<SmallPackagingMap> smallPackagingMaps)
        {
            lvwRegistry.DataSource = smallPackagingMaps;
            lvwRegistry.DataBind();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() && !messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void FailedLock(Lock @lock)
        {
            btnSave.Enabled = false;

            litMessage.Text = @lock.BuildFailedLockMessage();
        }


        private void SetSmallPackagingTypes()
        {
            var engine = ddlSmallPackEngine.SelectedValue.ToEnum<SmallPackageEngine>();

            ddlEngineTypes.DataSource = SmallPackageEnginesTypes.ContainsKey(engine)
                                            ? SmallPackageEnginesTypes[engine]
                                            : new Dictionary<string, string>();
            ddlEngineTypes.DataBind();
        }

        private void GeneratePopup(string title)
        {
            litTitle.Text = title;

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = pnlEdit.Visible;

            ddlSmallPackEngine.Focus();
        }

        private void CloseEditPopup()
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<SmallPackagingMap>(new SmallPackagingMap(hidSmallPackagingMapId.Value.ToLong(), false)));
        }

        private void ClearFields()
        {
            ddlPackageTypes.SelectedIndex = 0;
            ddlSmallPackEngine.SelectedIndex = 0;
            txtRequiredHeight.Text = string.Empty;
            txtRequiredLength.Text = string.Empty;
            txtRequiredWidth.Text = string.Empty;
            ddlEngineTypes.SelectedIndex = 0;
        }

        private void DoSearch()
        {
            const string criteria = "%";

            if (Search != null)
                Search(this, new ViewEventArgs<string>(criteria));
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new SmallPackagingMapHandler(this);
            handler.Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowNew = Access.Modify;

            if (IsPostBack) return;

            if (Loading != null)
                Loading(this, new EventArgs());

            DoSearch();
        }

        protected void OnNewClick(object sender, EventArgs e)
        {
            ClearFields();
            hidSmallPackagingMapId.Value = string.Empty;
            GeneratePopup("Add Small Packaging Mapping");
        }

        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            ClearFields();
            GeneratePopup("Modify Small Package Map");

            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("smallPackagingMapId").ToCustomHiddenField();

            var smallPackagingMap = new SmallPackagingMap(hidden.Value.ToLong(), false);

            hidSmallPackagingMapId.Value = smallPackagingMap.Id.ToString();
            ddlPackageTypes.SelectedValue = smallPackagingMap.PackageType.Id.GetString();
            ddlSmallPackEngine.SelectedValue = smallPackagingMap.SmallPackageEngine.ToInt().ToString();

            txtRequiredHeight.Text = smallPackagingMap.RequiredHeight.ToString();
            txtRequiredLength.Text = smallPackagingMap.RequiredLength.ToString();
            txtRequiredWidth.Text = smallPackagingMap.RequiredWidth.ToString();

            if (Lock != null)
                Lock(this, new ViewEventArgs<SmallPackagingMap>(smallPackagingMap));
        }

        protected void OnCloseClicked(object sender, object e)
        {
            CloseEditPopup();
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            var smallPackageMapId = hidSmallPackagingMapId.Value.ToLong();

            var smallPackagingMap = new SmallPackagingMap(smallPackageMapId, smallPackageMapId != default(long))
            {
                SmallPackageEngine = ddlSmallPackEngine.SelectedValue.ToEnum<SmallPackageEngine>(),
                PackageTypeId = ddlPackageTypes.SelectedValue.ToInt(),
                SmallPackEngineType = ddlEngineTypes.SelectedValue,
                RequiredHeight = txtRequiredHeight.Text.ToDecimal(),
                RequiredLength = txtRequiredLength.Text.ToDecimal(),
                RequiredWidth = txtRequiredWidth.Text.ToDecimal()
            };


            if (smallPackagingMap.IsNew)
            {
                smallPackagingMap.TenantId = ActiveUser.TenantId;
            }

            if (Save != null)
                Save(this, new ViewEventArgs<SmallPackagingMap>(smallPackagingMap));

            DoSearch();
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("smallPackagingMapId").ToCustomHiddenField();

            if (Delete != null)
                Delete(this, new ViewEventArgs<SmallPackagingMap>(new SmallPackagingMap(hidden.Value.ToLong(), true)));

            DoSearch();
        }



        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (CloseEditPopUp) CloseEditPopup();
        }

        protected void OnSmallPackEngineSelectedIndexChanged(object sender, EventArgs e)
        {
            SetSmallPackagingTypes();
        }
    }
}
