﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Rating;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Rating
{
    public partial class ResellerAdditionView : MemberPageBase, IResellerAdditionView
    {
        private const string SaveFlag = "S";

        public override ViewCode PageCode { get { return ViewCode.ResellerAddition; } }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RatingBlue; }
        }

        public static string PageAddress { get { return "~/Members/Rating/ResellerAdditionView.aspx"; } }

        public Dictionary<int, string> ValuePercentagTypes
        {
            set
            {
                var valuePercentageTypes = value.ToList();

                ddlLineHaulType.DataSource = valuePercentageTypes;
                ddlLineHaulType.DataBind();

                ddlAccessorialType.DataSource = valuePercentageTypes;
                ddlAccessorialType.DataBind();

                ddlFuelType.DataSource = valuePercentageTypes;
                ddlFuelType.DataBind();

                ddlServiceType.DataSource = valuePercentageTypes;
                ddlServiceType.DataBind();
            }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<ResellerAddition>> Save;
        public event EventHandler<ViewEventArgs<ResellerAddition>> Delete;
        public event EventHandler<ViewEventArgs<ResellerAddition>> Lock;
        public event EventHandler<ViewEventArgs<ResellerAddition>> UnLock;
        public event EventHandler<ViewEventArgs<ResellerAddition>> LoadAuditLog;
        public event EventHandler<ViewEventArgs<string>> CustomerSearch;

        public void FailedLock(Lock @lock)
        {
            memberToolBar.ShowNew = false;
            memberToolBar.ShowSave = false;
            memberToolBar.ShowDelete = false;
            memberToolBar.ShowUnlock = false;

            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs)
        {
            auditLogs.Load(logs);
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == SaveFlag)
            {
                // unlock and return to read-only
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<ResellerAddition>(new ResellerAddition(hidResellerAdditionId.Value.ToLong(), false)));
                SetEditStatus(false);
                memberToolBar.ShowUnlock = false;
            }

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Button = MessageButton.Ok;
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;

            litErrorMessages.Text = messages.HasErrors()
                                        ? string.Join(WebApplicationConstants.HtmlBreak,
                                                      messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
                                                        Select(m => m.Message).ToArray())
                                        : string.Empty;

        }

        public void DisplayCustomer(Customer customer)
        {
            txtCustomerNumber.Text = customer.CustomerNumber;
            txtCustomerName.Text = customer.Name;
            hidCustomerId.Value = customer.Id.ToString();
        }

        public void Set(ResellerAddition addition)
        {
            LoadResellerAddition(addition);
            SetEditStatus(!addition.IsNew);
        }

        public void LoadResellerAddition(ResellerAddition resellerAddition)
        {
            txtResellerName.Text = resellerAddition.Name;

            ddlLineHaulType.SelectedValue = resellerAddition.LineHaulType.ToInt().ToString();
            txtLineHaulPercentage.Text = resellerAddition.LineHaulPercentage.ToString();
            txtLineHaulValue.Text = resellerAddition.LineHaulValue.ToString();
            chkUseLineHaulMin.Checked = resellerAddition.UseLineHaulMinimum;
            ddlFuelType.SelectedValue = resellerAddition.FuelType.ToInt().ToString();
            txtFuelPercentage.Text = resellerAddition.FuelPercentage.ToString();
            txtFuelValue.Text = resellerAddition.FuelValue.ToString();
            chkUseFuelMin.Checked = resellerAddition.UseFuelMinimum;
            ddlAccessorialType.SelectedValue = resellerAddition.AccessorialType.ToInt().ToString();
            txtAcessorialPercentage.Text = resellerAddition.AccessorialPercentage.ToString();
            txtAccessorialValue.Text = resellerAddition.AccessorialValue.ToString();
            chkUseAccessorialMin.Checked = resellerAddition.UseAccessorialMinimum;
            ddlServiceType.SelectedValue = resellerAddition.ServiceType.ToInt().ToString();
            txtServicePercentage.Text = resellerAddition.ServicePercentage.ToString();
            txtServiceValue.Text = resellerAddition.ServiceValue.ToString();
            chkUseServiceMin.Checked = resellerAddition.UseServiceMinimum;

            DisplayCustomer(resellerAddition.ResellerCustomerAccount ?? new Customer());

            hidResellerAdditionId.Value = resellerAddition.Id.ToString();

            resellerAdditionFinder.Visible = false;

            memberToolBar.ShowUnlock = resellerAddition.HasUserLock(ActiveUser, resellerAddition.Id);
        }


        private void ProcessTransferredRequest(long resellerAdditionId)
        {
            if (resellerAdditionId != default(long))
            {
                var resellerAddition = new ResellerAddition(resellerAdditionId);
                LoadResellerAddition(resellerAddition);

                if (LoadAuditLog != null)
                    LoadAuditLog(this, new ViewEventArgs<ResellerAddition>(resellerAddition));
            }
        }


        private void SetEditStatus(bool enabled)
        {
            pnlDetails.Enabled = enabled;

            memberToolBar.EnableSave = enabled;

            litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new ResellerAdditionHandler(this);
            handler.Initialize();

            memberToolBar.ShowSave = Access.Modify;
            memberToolBar.ShowDelete = Access.Remove;
            memberToolBar.ShowNew = Access.Modify;
            memberToolBar.ShowEdit = Access.Modify;

            resellerAdditionFinder.OpenForEditEnabled = Access.Modify;

            if (IsPostBack) return;

            aceCustomer.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.ActiveUser, ActiveUser.Id.GetString() } });

            if (Loading != null)
                Loading(this, new EventArgs());

            if (Session[WebApplicationConstants.TransferResellerAdditionId] != null)
            {
                ProcessTransferredRequest(Session[WebApplicationConstants.TransferResellerAdditionId].ToLong());
                Session[WebApplicationConstants.TransferResellerAdditionId] = null;
            }

            SetEditStatus(false);
        }


        protected void OnNewClick(object sender, EventArgs e)
        {
            litErrorMessages.Text = string.Empty;

            SetEditStatus(true);
            DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
            LoadResellerAddition(new ResellerAddition());
        }

        protected void OnEditClicked(object sender, EventArgs e)
        {
            var resellerAddition = new ResellerAddition(hidResellerAdditionId.Value.ToLong(), false);

            if (Lock == null || resellerAddition.IsNew) return;

            SetEditStatus(true);
            memberToolBar.ShowUnlock = true;
            Lock(this, new ViewEventArgs<ResellerAddition>(resellerAddition));

            LoadResellerAddition(resellerAddition);
        }

        protected void OnUnlockClicked(object sender, EventArgs e)
        {
            var resellerAddition = new ResellerAddition(hidResellerAdditionId.Value.ToLong(), false);
            if (UnLock != null && !resellerAddition.IsNew)
            {
                SetEditStatus(false);
                memberToolBar.ShowUnlock = false;
                UnLock(this, new ViewEventArgs<ResellerAddition>(resellerAddition));
            }
        }

        protected void OnSaveClicked(object sender, EventArgs e)
        {
            var resellerAdditionId = hidResellerAdditionId.Value.ToLong();

            var resellerAddition = new ResellerAddition(resellerAdditionId, resellerAdditionId != default(long))
            {
                ResellerCustomerAccount = new Customer(hidCustomerId.Value.ToLong()),
                Name = txtResellerName.Text,
                LineHaulType = ddlLineHaulType.SelectedValue.ToEnum<ValuePercentageType>(),
                LineHaulPercentage = txtLineHaulPercentage.Text.ToDecimal(),
                LineHaulValue = txtLineHaulValue.Text.ToDecimal(),
                UseLineHaulMinimum = chkUseLineHaulMin.Checked,
                FuelType = ddlFuelType.SelectedValue.ToEnum<ValuePercentageType>(),
                FuelPercentage = txtFuelPercentage.Text.ToDecimal(),
                FuelValue = txtFuelValue.Text.ToDecimal(),
                UseFuelMinimum = chkUseFuelMin.Checked,
                AccessorialType = ddlAccessorialType.SelectedValue.ToEnum<ValuePercentageType>(),
                AccessorialPercentage = txtAcessorialPercentage.Text.ToDecimal(),
                AccessorialValue = txtAccessorialValue.Text.ToDecimal(),
                UseAccessorialMinimum = chkUseAccessorialMin.Checked,
                ServiceType = ddlServiceType.SelectedValue.ToEnum<ValuePercentageType>(),
                ServicePercentage = txtServicePercentage.Text.ToDecimal(),
                ServiceValue = txtServiceValue.Text.ToDecimal(),
                UseServiceMinimum = chkUseServiceMin.Checked
            };
            if (resellerAddition.IsNew) resellerAddition.TenantId = ActiveUser.TenantId;

            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<ResellerAddition>(resellerAddition));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<ResellerAddition>(resellerAddition));
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var resellerAddition = new ResellerAddition(hidResellerAdditionId.Value.ToLong(), false);

            if (Delete != null)
                Delete(this, new ViewEventArgs<ResellerAddition>(resellerAddition));

            resellerAdditionFinder.Reset();
        }

        protected void OnFindClicked(object sender, EventArgs e)
        {
            resellerAdditionFinder.Visible = true;
        }


        protected void OnResellerFinderItemSelected(object sender, ViewEventArgs<ResellerAddition> e)
        {
            litErrorMessages.Text = string.Empty;

            if (hidResellerAdditionId.Value.ToLong() != default(long))
            {
                var oldResllerAddtion = new ResellerAddition(hidResellerAdditionId.Value.ToLong(), false);
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<ResellerAddition>(oldResllerAddtion));
            }

            var resellerAddition = e.Argument;

            LoadResellerAddition(resellerAddition);

            resellerAdditionFinder.Visible = false;

            if (resellerAdditionFinder.OpenForEdit && Lock != null)
            {
                SetEditStatus(true);
                memberToolBar.ShowUnlock = true;
                Lock(this, new ViewEventArgs<ResellerAddition>(resellerAddition));
            }
            else
            {
                SetEditStatus(false);
            }

            if (LoadAuditLog != null) LoadAuditLog(this, new ViewEventArgs<ResellerAddition>(resellerAddition));
        }

        protected void OnResellerFinderItemCancelled(object sender, EventArgs e)
        {
            resellerAdditionFinder.Visible = false;
        }


        protected void OnCustomerFinderSelectionCancelled(object sender, EventArgs e)
        {
            customerFinder.Visible = false;
        }

        protected void OnCustomerFinderItemSelected(object sender, ViewEventArgs<Customer> e)
        {
            DisplayCustomer(e.Argument);
            customerFinder.Visible = false;
        }

        protected void OnFindCustomerClicked(object sender, ImageClickEventArgs e)
        {
            customerFinder.Visible = true;
        }


        protected void OnCustomerNumberChanged(object sender, EventArgs e)
        {
            if (CustomerSearch != null)
                CustomerSearch(this, new ViewEventArgs<string>(txtCustomerNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
        }
    }
}