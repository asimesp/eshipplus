﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Operations;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Rating
{
	public partial class CustomerTLTenderingProfileView : MemberPageBase, ICustomerTLTenderingProfile
	{
        // TODO: Chris to review all TODO notes on this page

		private const string SaveFlag = "S";
	
		private const string LanesHeader = "Lanes";
		private const string VendorsHeader = "Preferred Vendors";

		private const string LanesImportArgs = "LIA";
		private const string VendorsImportArgs = "VIA";
		
		public static string PageAddress
		{
			get { return "~/Members/Rating/CustomerTLTenderingProfileView.aspx"; }
		}

		public override ViewCode PageCode
		{
			get { return ViewCode.TruckloadTenderingProfile; }
		}

		public override string SetPageIconImage
		{
			set { imgPageImageLogo.ImageUrl = IconLinks.OperationsBlue; }
		}


		public event EventHandler<ViewEventArgs<CustomerTLTenderingProfile>> TLProfileSave;
		public event EventHandler<ViewEventArgs<CustomerTLTenderingProfile>> TLProfileDelete;
		public event EventHandler<ViewEventArgs<CustomerTLTenderingProfile>> TLProfileLock;
		public event EventHandler<ViewEventArgs<CustomerTLTenderingProfile>> TLProfileUnLock;
		public event EventHandler<ViewEventArgs<CustomerTLTenderingProfile>> TLProfileLoadAuditLog;
		public event EventHandler<ViewEventArgs<long>> TLProfileCanDeleteLane;
		public event EventHandler<ViewEventArgs<string>> CustomerSearch;
		public event EventHandler<ViewEventArgs<string>> VendorSearch;
		public event EventHandler<ViewEventArgs<string>> ProcessingError;

		public void DisplayCustomer(Customer customer)
		{
			txtCustomerNumber.Text = customer == null ? string.Empty : customer.CustomerNumber;
			txtCustomerName.Text = customer == null ? string.Empty : customer.Name;
			hidCustomerId.Value = customer == null ? default(long).ToString() : customer.Id.ToString();
		}

		public void DisplayVendorForLane(Vendor vendor)
		{
			if (vendor.IsNew) return;

			vendorFinderForLanes.Visible = false;

			// TODO: use this instead of getting all lanes and switch below.
			
			foreach (var item in lstLanes.Items)
			{
				if (item.FindControl("hidLaneIndex").ToCustomHiddenField().Value != hidEditingIndex.Value) continue;
				var index = hidPreferredVendorEditingIndex.Value.ToInt() + 1;
				var txtNumber = item.FindControl("txtVendorNumber" + index).ToTextBox();
				var name = item.FindControl("txtVendorName" + index).ToTextBox();
				var vendorId = item.FindControl("hidVendor" + index + "Id").ToCustomHiddenField();

				txtNumber.Text = vendor.VendorNumber;
				name.Text = vendor.Name;
				vendorId.Value = vendor.Id.ToString();
				break;
			}

			// TODO: use anonymous type here instead of instantiating object.
			var vendors = upLstVendors
				.Items
				.Select(c => new
					{
						TLTenderingProfileId = hidProfileId.Value.ToLong(),
						VendorId = c.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong(),
						VendorNumber = c.FindControl("lblVendorNumber").ToLabel().Text,
						Name = c.FindControl("litName").ToLiteral().Text,
						CommunicationType = c.FindControl("ddlCommunicationType").ToDropDownList().SelectedValue.ToEnum<NotificationMethod>()
					})
				.ToList();

			var newVendor = new 
				{
					TLTenderingProfileId = hidProfileId.Value.ToLong(),
					VendorId = vendor.Id,
					VendorNumber = vendor.VendorNumber,
					Name = vendor.Name,
					CommunicationType = vendor.Communication != null
						                    ? vendor.Communication.EdiEnabled && vendor.Communication.DropOff204
												  ? NotificationMethod.Edi
												  : NotificationMethod.Email
											: NotificationMethod.Email
				};

			if (!vendors.Select(v => v.VendorId).Contains(newVendor.VendorId)) vendors.Add(newVendor);

			upLstVendors.DataSource = vendors;
			upLstVendors.DataBind();

			tabVendors.HeaderText = vendors.BuildTabCount(VendorsHeader);
			athtuTabUpdater.SetForUpdate(tabVendors.ClientID, vendors.BuildTabCount(VendorsHeader));
			var lanes = GetCurrentViewLanes();
			peLstLanes.LoadData(lanes);
		}


		public void LogException(Exception ex)
		{
			if (ex != null) ErrorLogger.LogError(ex, Context);
		}

		public void DisplayMessages(IEnumerable<ValidationMessage> messages)
		{
			if (messages == null) return;
			if (!messages.Any()) return;

			if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == SaveFlag)
			{
				SetEditStatus(false);
				memberToolBar.ShowUnlock = false;
			}

			messageBox.Icon = messages.GenerateIcon();
			messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak,
			                                 messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
			messageBox.Button = MessageButton.Ok;
			messageBox.Visible = true;

			litErrorMessages.Text = messages.HasErrors()
				                        ? string.Join(WebApplicationConstants.HtmlBreak,
				                                      messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
				                                               Select(m => m.Message).ToArray())
				                        : string.Empty;
		}

		public void Set(CustomerTLTenderingProfile profile)
		{
			LoadTenderingProfile(profile);
			SetEditStatus(!profile.IsNew);
		}

		public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs)
		{
			auditLogs.Load(logs);
		}

		public void FailedLock(Lock @lock)
		{
			memberToolBar.ShowNew = false;
			memberToolBar.ShowSave = false;
			memberToolBar.ShowDelete = false;
			memberToolBar.ShowUnlock = false;

			SetEditStatus(false);

			litMessage.Text = @lock.BuildFailedLockMessage();
		}

		
		private void SetEditStatus(bool enabled)
		{
			txtMaxWaitTime.Enabled = enabled;
			txtCustomerNumber.Enabled = enabled;
			pnlVendors.Enabled = enabled;
			pnlLanes.Enabled = enabled;
			memberToolBar.ShowExport = enabled || hidProfileId.Value != "";
			memberToolBar.EnableSave = enabled;
			memberToolBar.EnableImport = enabled;
			litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;
			
			hidEditStatus.Value = enabled.ToString();
			memberToolBar.ShowUnlock = enabled;
		}


		private void LoadTenderingProfile(CustomerTLTenderingProfile profile)
		{
			if (!profile.IsNew) profile.LoadCollections();

			hidProfileId.Value = profile.Id.ToString();

			// Customer
			DisplayCustomer(profile.Customer);
			txtMaxWaitTime.Text = profile.MaxWaitTime.ToString();
			// Vendors
			//TODO: use anonymous type for vendors.  See note in DisplayVendorForLane method

			var vendors = new List<object>();
			foreach (var vendor in profile.Vendors)
			{
				vendors.Add(
					new
						{
							TLTenderingProfileId = vendor.TLTenderingProfileId.ToLong(),
							VendorId = vendor.VendorId, 
							VendorNumber = vendor.Vendor.VendorNumber,
							Name = vendor.Vendor.Name,
							CommunicationType = vendor.CommunicationType
						});
			}
		
			upLstVendors.DataSource = vendors;
			upLstVendors.DataBind();
			tabVendors.HeaderText = profile.Vendors.BuildTabCount(VendorsHeader);
			athtuTabUpdaterVendors.SetForUpdate(tabLanes.ClientID, profile.Vendors.BuildTabCount(LanesHeader));


			// Lanes
			peLstLanes.LoadData(profile.Lanes);
			tabLanes.HeaderText = profile.Lanes.BuildTabCount(LanesHeader);
			athtuTabUpdaterLanes.SetForUpdate(tabLanes.ClientID, profile.Lanes.BuildTabCount(LanesHeader));

			memberToolBar.ShowUnlock = profile.HasUserLock(ActiveUser, profile.Id);
			if(hidProfileId.Value != "")
			{
				memberToolBar.ShowExport = true;
			}
		}


		//TODO: remove overloaded method above.  You're duplicating code that is not needed
		private List<CustomerTLTenderingProfileLane> GetCurrentViewLanes(CustomerTLTenderingProfile profile = null)
		{
			var lanes = peLstLanes.GetData<CustomerTLTenderingProfileLane>();
			
			var vLanes = lstLanes
				.Items
				.Select(i => new
				{
					EditIndex = i.FindControl("hidLaneIndex").ToCustomHiddenField().Value.ToInt(),
					Id = i.FindControl("hidTLLane").ToCustomHiddenField().ToLong(),

					Lane = new CustomerTLTenderingProfileLane(i.FindControl("hidTLLane").ToCustomHiddenField().Value.ToLong(), 
						i.FindControl("hidTLLane").ToCustomHiddenField().Value.ToLong()!= default(long))
					{
						TenantId = profile == null ? default(long) : profile.TenantId,
						TLTenderingProfile = profile,
						OriginCity = i.FindControl("txtOriginCity").ToTextBox().Text,
						OriginState = i.FindControl("txtOriginState").ToTextBox().Text,
						OriginPostalCode = i.FindControl("txtOriginPostalCode").ToTextBox().Text,
						OriginCountryId = i.FindControl("ddlOriginCountryCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
						DestinationCity = i.FindControl("txtDestinationCity").ToTextBox().Text,
						DestinationState = i.FindControl("txtDestinationState").ToTextBox().Text,
						DestinationPostalCode = i.FindControl("txtDestinationPostalCode").ToTextBox().Text,
						DestinationCountryId =
							i.FindControl("ddlDestinationCountryCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
						FirstPreferredVendorId = i.FindControl("hidVendor1Id").ToCustomHiddenField().Value.ToLong(),
						SecondPreferredVendorId = i.FindControl("hidVendor2Id").ToCustomHiddenField().Value.ToLong(),
						ThirdPreferredVendorId = i.FindControl("hidVendor3Id").ToCustomHiddenField().Value.ToLong(),
					}
				});

			foreach (var vn in vLanes)
				lanes[vn.EditIndex] = vn.Lane;

			return lanes;
		}

		private CustomerTLTenderingProfile UpdateProfile()
		{
			var profileId = hidProfileId.Value.ToLong();
			var profile = new CustomerTLTenderingProfile(profileId, profileId != default(long));

			if (profile.IsNew)
			{
				profile.TenantId = ActiveUser.TenantId;
			}
			else
			{
				profile.LoadCollections();
			}

			profile.Customer = new Customer(hidCustomerId.Value.ToLong());
			profile.MaxWaitTime = txtMaxWaitTime.Text.ToInt();

			//Update From View
			UpdateVendorsFromView(profile);
			profile.Lanes = GetCurrentViewLanes(profile);
			return profile;
		}

		
		private void UpdateVendorsFromView(CustomerTLTenderingProfile profile)
		{
			var vendors = upLstVendors
				.Items
				.Select(i => new CustomerTLTenderingProfileVendor()
					{
						TenantId = profile.TenantId,
						VendorId = i.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong(),
						TLTenderingProfile = profile,
						CommunicationType = i.FindControl("ddlCommunicationType").ToCachedObjectDropDownList().SelectedValue.ToInt().ToEnum<NotificationMethod>()
					})
				.ToList();

			profile.Vendors = vendors;
		}

		private List<CustomerTLTenderingProfileVendor> GetVendorsFromView()
		{
			var vendors = upLstVendors
				.Items
				.Select(i => new CustomerTLTenderingProfileVendor()
				{
					TenantId = ActiveUser.TenantId,
					VendorId = i.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong(),
					TLTenderingProfile = null,
					CommunicationType = i.FindControl("ddlCommunicationType").ToCachedObjectDropDownList().SelectedValue.ToInt().ToEnum<NotificationMethod>()
				})
				.ToList();
			return vendors;
		}
		

		protected void Page_Load(object sender, EventArgs e)
		{
			new CustomerTLTenderingProfileHandler(this).Initialize();

			memberToolBar.ShowSave = Access.Modify;
			memberToolBar.ShowDelete = Access.Remove;
			memberToolBar.ShowNew = Access.Modify;
			memberToolBar.ShowEdit = Access.Modify;
			memberToolBar.ShowImport = Access.Modify;
			memberToolBar.ShowExport = Access.Modify;
		
			aceCustomer.SetupExtender(new Dictionary<string, string>
				{
					{AutoCompleteUtility.Keys.ActiveUser, ActiveUser.Id.GetString()}
				});

			foreach (var item in lstLanes.Items)
			{
				var aceVendor1 = item.FindControl("aceVendor1").ToAutoCompleteExtender();
				aceVendor1.SetupExtender(new Dictionary<string, string>
				{
					{AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString()}
				});
				var aceVendor2 = item.FindControl("aceVendor2").ToAutoCompleteExtender();
				aceVendor2.SetupExtender(new Dictionary<string, string>
				{
					{AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString()}
				});
				var aceVendor3 = item.FindControl("aceVendor3").ToAutoCompleteExtender();
				aceVendor3.SetupExtender(new Dictionary<string, string>
				{
					{AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString()}
				});
				
			}

			if (IsPostBack) return;

			memberToolBar.LoadTemplates(new List<string>
				{
					WebApplicationConstants.TruckloadTenderingProfileLaneImportTemplate,
					WebApplicationConstants.TruckloadTenderingProfileVendorImportTemplate
				});

			SetEditStatus(false);
		}


		protected void OnToolbarFindClicked(object sender, EventArgs e)
		{
			customerTLTenderProfileFinder.Visible = true;
		}

		protected void OnToolbarSaveClicked(object sender, EventArgs e)
		{
			var profile = UpdateProfile();

			hidFlag.Value = SaveFlag;

			if (TLProfileSave != null)
				TLProfileSave(this, new ViewEventArgs<CustomerTLTenderingProfile>(profile));

			if (TLProfileLoadAuditLog != null)
				TLProfileLoadAuditLog(this, new ViewEventArgs<CustomerTLTenderingProfile>(profile));
		}

		protected void OnToolbarDeleteClicked(object sender, EventArgs e)
		{
			var profile = new CustomerTLTenderingProfile(hidProfileId.Value.ToLong(), false);

			if (TLProfileDelete != null)
				TLProfileDelete(this, new ViewEventArgs<CustomerTLTenderingProfile>(profile));

			customerTLTenderProfileFinder.Reset();
		}

		protected void OnToolbarNewClicked(object sender, EventArgs e)
		{
			var profile = new CustomerTLTenderingProfile()
				{
					Customer = new Customer()
				};

			LoadTenderingProfile(profile);

			DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
			SetEditStatus(true);

			litErrorMessages.Text = string.Empty;
		}

		protected void OnToolbarEditClicked(object sender, EventArgs e)
		{
			var profile = new CustomerTLTenderingProfile(hidProfileId.Value.ToLong(), false);

			if (TLProfileLock == null || profile.IsNew) return;

			SetEditStatus(true);
			memberToolBar.ShowUnlock = true;
			TLProfileLock(this, new ViewEventArgs<CustomerTLTenderingProfile>(profile));

			LoadTenderingProfile(profile);
		}

		protected void OnToolbarUnlockClicked(object sender, EventArgs e)
		{
			var profile = new CustomerTLTenderingProfile(hidProfileId.Value.ToLong(), false);
			if (TLProfileUnLock != null && !profile.IsNew)
			{
				SetEditStatus(false);
				memberToolBar.ShowUnlock = false;
				TLProfileUnLock(this, new ViewEventArgs<CustomerTLTenderingProfile>(profile));
			}
		}

		
		protected void OnToolbarImportExportClicked(object sender, EventArgs e)
		{
			pnlTLtenderingImportExport.Visible = true;
			pnlDimScreen.Visible = true;
		}

		protected void OnImportExportClosedClicked(object sender, EventArgs e)
		{
			pnlTLtenderingImportExport.Visible = false;
			pnlDimScreen.Visible = false;
		}


		protected void OnExportClicked(object sender, EventArgs e)
		{
			pnlTLtenderingImportExport.Visible = false;
			pnlDimScreen.Visible = false;
			try
			{
				var export = false;

				if (chkVendor.Checked)
				{
					OnVendorExportClicked(sender, e);
					export = true;
				}
				if (chkLanes.Checked)
				{
					OnLanesExportClicked(sender, e);
					export = true;
				}


				if (!export) DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error("Please select an option prior to exporting.") });

					
			}
			catch
			{
				DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error("A processing error has occurred.") });
			}
		}

		protected void OnImportClicked(object sender, EventArgs e)
		{
			if (!fupFile.HasFile)
			{
				DisplayMessages(new List<ValidationMessage> {ValidationMessage.Error("Please select a file for import")});
				return;
			}

			pnlTLtenderingImportExport.Visible = false;
			pnlDimScreen.Visible = false;
			List<string[]> lines;
			try
			{
				lines = fupFile.GetImportData(true);
			}
			catch (Exception ex)
			{
				DisplayMessages(new List<ValidationMessage> {ValidationMessage.Error(ex.Message)});
				return;
			}

			try
			{
				var import = false;

				if (chkVendor.Checked)
				{
					ImportVendors(lines);
					import = true;
				}
				if (chkLanes.Checked)
				{
					ImportLanes(lines);
					import = true;
				}


				if (!import)
				{
					DisplayMessages(new List<ValidationMessage>
						{
							ValidationMessage.Error("Please select an option prior to importing.")
						});
				}
			}
			catch
			{
				DisplayMessages(new List<ValidationMessage>
					{
						ValidationMessage.Error("A processing error has occurred. This may be due to a problem with the input file")
					});
			}
		}

		protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
		{
			messageBox.Button = MessageButton.Ok;
			messageBox.Icon = MessageIcon.Error;
			messageBox.Message = e.Argument;

			messageBox.Visible = true;
		}

		protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
		{
			hidFlag.Value = string.Empty;
			fileUploader.Visible = false;
		}

		protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
		{
			List<string[]> lines;
			try
			{
				lines = e.GetImportData(true);
			}
			catch (Exception ex)
			{
				DisplayMessages(new List<ValidationMessage> {ValidationMessage.Error(ex.Message)});
				return;
			}

			fileUploader.Visible = false;

			switch (hidFlag.Value)
			{
				case VendorsImportArgs:
					ImportVendors(lines);
					break;
				case LanesImportArgs:
					ImportLanes(lines);
					break;
			}
		}

		private void ImportLanes(ICollection<string[]> lines)
		{
			if (lines.Count > 5000)
			{
				DisplayMessages(new[]
					{ValidationMessage.Error("Batch import is limited to 5,000 records, please adjust your file size.")});
				return;
			}

			var msgs = new List<ValidationMessage>();
			var cnt = chkFirstLineHeader.Checked ? 2 : 1; 
			var chks = lines
				.Select(l => new ImportLineObj(l, cnt++, l.Length))
				.ToList();

			const int colCnt = 8;
			msgs.AddRange(chks.LineLengthsAreValid(colCnt));
			if (msgs.Any())
			{
				msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
				DisplayMessages(msgs);
				return;
			}

			var countries = ProcessorVars.RegistryCache.Countries.Values.ToList();
			var countryCodes = countries.Select(c => c.Code).ToList();
			// verify Origin Country Code
			msgs.AddRange(chks.CodesAreValid(3, countryCodes, new Country().EntityName()));
			// verify Destination Country Code
			msgs.AddRange(chks.CodesAreValid(7, countryCodes, new Country().EntityName()));

			if (msgs.Any())
			{
				msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
				DisplayMessages(msgs);
				return;
			}

			var postalCodeSearch = new PostalCodeSearch();
			var postalCodes = new List<PostalCodeViewSearchDto>();
			var vendorSearch = new VendorSearch();
		    var getLine = chks.Select(p => p.Line).ToList();
			int lineLen = getLine[0].Length;
			if (lineLen > 7)
			{
				var vParameters = chks
				.Select(i => i.Line[8])
				.Distinct()
				.Select(n =>
				{
					var p = AccountingSearchFields.VendorNumber.ToParameterColumn();
					p.DefaultValue = n;
					p.Operator = Operator.Equal;
					return p;
				})
				.ToList();

				vParameters.AddRange(chks
										 .Select(i => i.Line[9])
										 .Distinct()
										 .Select(n =>
										 {
											 var p = AccountingSearchFields.VendorNumber.ToParameterColumn();
											 p.DefaultValue = n;
											 p.Operator = Operator.Equal;
											 return p;
										 })
										 .ToList());
				vParameters.AddRange(chks
										 .Select(i => i.Line[10])
										 .Distinct()
										 .Select(n =>
										 {
											 var p = AccountingSearchFields.VendorNumber.ToParameterColumn();
											 p.DefaultValue = n;
											 p.Operator = Operator.Equal;
											 return p;
										 })
										 .ToList());
			}
			
		
		    if (msgs.Any())
			{
				msgs.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
				DisplayMessages(msgs);
				return;
			}


			// Verify Origin Postal Code
			var invalidPostalCodes = chks
				.Where(i =>
					{
						var code = postalCodeSearch.FetchPostalCodeByCode(i.Line[2], countries.First(c => c.Code == i.Line[3]).Id);
						if (code.Id != default(long) && !postalCodes.Select(p => p.Code).Contains(code.Code)) postalCodes.Add(code);
						return code.Id == default(long) && i.Line[2] != string.Empty;
					})
				.Select(
					i =>
					ValidationMessage.Error(WebApplicationConstants.InvalidImportedCodeErrMsg, new PostalCode().EntityName(), i.Index))
				.ToList();

			// Verify Destination Postal Code
			invalidPostalCodes.AddRange(chks
				                            .Where(i =>
					                            {
						                            var code = postalCodeSearch.FetchPostalCodeByCode(i.Line[6],
						                                                                              countries.First(
							                                                                              c => c.Code == i.Line[7]).Id);
						                            if (code.Id != default(long) && !postalCodes.Select(p => p.Code).Contains(code.Code))
							                            postalCodes.Add(code);
						                            return code.Id == default(long) && i.Line[6] != string.Empty;
					                            })
				                            .Select(
					                            i =>
					                            ValidationMessage.Error(WebApplicationConstants.InvalidImportedCodeErrMsg,
					                                                    new PostalCode().EntityName(), i.Index))
				                            .ToList());

			if (invalidPostalCodes.Any())
			{
				invalidPostalCodes.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
				DisplayMessages(invalidPostalCodes);
				return;
			}

			//TODO: valid conditions are: City-STate-Country or PostalCode-Country or PostalCode-City-State-Country. Make sure testing below is in line with this.
			// check all lane combinations, as follows:
			// 1.) Origin City is Empty and Origin Postal Code is Empty
			// 2.) Origin State is Empty and Origin Postal Code is Empty
			// 3.) Destination City is Empty and Origin Postal Code is Empty
			// 4.) Destination State is Empty and Origin Postal Code is Empty
			// 5.) Origin Postal Code is Empty and Origin Country is Empty
			// 7.) Destination Postal Code is Empty and Destination Country is Empty
			var missingCityStateAndPostalCode = chks
				.Where(
					i =>
					(i.Line[0] == string.Empty && i.Line[2] == string.Empty) ||
					(i.Line[1] == string.Empty && i.Line[2] == string.Empty) ||
					(i.Line[4] == string.Empty && i.Line[6] == string.Empty) ||
					(i.Line[5] == string.Empty && i.Line[6] == string.Empty) ||
					(i.Line[2] == string.Empty && i.Line[3] == string.Empty) ||
					(i.Line[6] == string.Empty && i.Line[7] == string.Empty) ||
					(i.Line[3] == string.Empty || i.Line[7] == string.Empty))
				.Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedCodeErrMsg, "lane", i.Index))
				.ToList();

			if (missingCityStateAndPostalCode.Any())
			{
				missingCityStateAndPostalCode.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
				DisplayMessages(missingCityStateAndPostalCode);
				return;
			}


			var nLanes = lines
				.Select(s => new CustomerTLTenderingProfileLane(default(long))
					{
						OriginCity = s[0],
						OriginState = s[1],
						OriginPostalCode =
							s[2] == string.Empty ? string.Empty : postalCodes.First(pc => pc.Code.ToLower() == s[2].ToLower()).Code,
						OriginCountryId = countries.First(c => c.Code == s[3]).Id,
						DestinationCity = s[4],
						DestinationState = s[5],
						DestinationPostalCode =
							s[6] == string.Empty ? string.Empty : postalCodes.First(pc => pc.Code.ToLower() == s[6].ToLower()).Code,
						DestinationCountryId = countries.First(c => c.Code == s[7]).Id,
						FirstPreferredVendor =
							s[8] != string.Empty
								? vendorSearch.FetchVendorByNumber(s[8], ActiveUser.TenantId)
								: (s[9] != string.Empty
									   ? vendorSearch.FetchVendorByNumber(s[9], ActiveUser.TenantId)
									   : (s[10] != string.Empty ? vendorSearch.FetchVendorByNumber(s[10], ActiveUser.TenantId) : null)),
						SecondPreferredVendor =
							s[9] != string.Empty
								? vendorSearch.FetchVendorByNumber(s[9], ActiveUser.TenantId)
								: (s[10] != string.Empty ? vendorSearch.FetchVendorByNumber(s[10], ActiveUser.TenantId) : null),
						ThirdPreferredVendor = s[10] != string.Empty ? vendorSearch.FetchVendorByNumber(s[10], ActiveUser.TenantId) : null
					})
				.ToList();

			var lanes = GetCurrentViewLanes();
			lanes.AddRange(nLanes.Where(nl => !lanes.Select(l => l.FullLane).Contains(nl.FullLane)));

			tabLanes.HeaderText = lanes.BuildTabCount(LanesHeader);
			athtuTabUpdater.SetForUpdate(tabLanes.ClientID, lanes.BuildTabCount(LanesHeader));

			peLstLanes.LoadData(lanes);
		}

		private void ImportVendors(IEnumerable<string[]> lines)
		{
			var msgs = new List<ValidationMessage>();
			var cnt = chkFirstLineHeader.Checked ? 2 : 1; 
			var chks = lines
				.Select(l => new ImportLineObj(l, cnt++, l.Length))
				.ToList();
			
			const int colCnt = 3;
			msgs.AddRange(chks.LineLengthsAreValid(colCnt));
			if (msgs.Any())
			{
				msgs.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
				DisplayMessages(msgs);
				return;
			}

			var vendorSearch = new VendorSearch();
			var vParameters = chks
				.Select(i => i.Line[0])
				.Distinct()
				.Select(n =>
					{
						var p = AccountingSearchFields.VendorNumber.ToParameterColumn();
						p.DefaultValue = n;
						p.Operator = Operator.Equal;
						return p;
					})
				.ToList();
			var vendorList = vendorSearch.FetchVendors(vParameters, ActiveUser.TenantId);
			var vendorNumbers = vendorList.Select(c => c.VendorNumber).ToList();
			msgs.AddRange(chks.CodesAreValid(0, vendorNumbers, new Vendor().EntityName()));
			if (msgs.Any())
			{
				msgs.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
				DisplayMessages(msgs);
				return;
			}

			var vendors = upLstVendors.Items
			                        .Select(c => new 
				                        {
											TLTenderingProfileId = hidProfileId.Value.ToLong(),
											VendorId = c.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong(),
											VendorNumber = c.FindControl("lblVendorNumber").ToLabel().Text,
											Name = c.FindControl("litName").ToLiteral().Text,
											CommunicationType = c.FindControl("ddlCommunicationType").ToDropDownList().SelectedValue.ToEnum<NotificationMethod>()
				                        })
			                        .ToList();

			//TODO: check refactoring of using whole object
			var newVendors = lines
				.Select(s => new 
					{
						TLTenderingProfileId = hidProfileId.Value.ToLong(),
						VendorId = s[0] != string.Empty ? vendorSearch.FetchVendorByNumber(s[0], ActiveUser.TenantId).Id : default(long),
					    VendorNumber = s[0] != string.Empty ?  s[0] : string.Empty,
						Name = s[0] != string.Empty ? vendorSearch.FetchVendorByNumber(s[0], ActiveUser.TenantId).Name : string.Empty,
						CommunicationType = s[2].ToInt().ToEnum<NotificationMethod>()
					}).Where(s => !vendors.Select(c => c.VendorId).Contains(s.VendorId))
				.ToList();
			vendors.AddRange(newVendors);

			upLstVendors.DataSource = vendors;
			upLstVendors.DataBind();

			tabVendors.HeaderText = vendors.BuildTabCount(VendorsHeader);
			athtuTabUpdater.SetForUpdate(tabVendors.ClientID, vendors.BuildTabCount(VendorsHeader));
		}


		protected void OnCustomerNumberEntered(object sender, EventArgs e)
		{
			if (CustomerSearch != null)
				CustomerSearch(this, new ViewEventArgs<string>(txtCustomerNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
		}

		protected void OnCustomerSearchClicked(object sender, ImageClickEventArgs e)
		{
			customerFinder.Visible = true;
		}

		protected void OnCustomerFinderSelectionCancel(object sender, EventArgs e)
		{
			customerFinder.Visible = false;
		}

		protected void OnCustomerFinderItemSelected(object sender, ViewEventArgs<Customer> e)
		{
			DisplayCustomer(e.Argument);
			customerFinder.Visible = false;
		}


		protected void OnAddVendorClicked(object sender, EventArgs e)
		{
			vendorFinder.Reset();
			vendorFinder.Visible = true;
		}

		protected void OnDeleteVendorClicked(object sender, ImageClickEventArgs e)
		{
			var imageButton = (ImageButton) sender;

			var vendors = upLstVendors.Items
			                        .Select(c => new 
				                        {
											TLTenderingProfileId = hidProfileId.Value.ToLong(),
											VendorId = c.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong(),
											VendorNumber = c.FindControl("lblVendorNumber").ToLabel().Text,
											Name = c.FindControl("litName").ToLiteral().Text,
											CommunicationType = c.FindControl("ddlCommunicationType").ToDropDownList().SelectedValue.ToEnum<NotificationMethod>()
				                        })
			                        .ToList();

			var removalIndex = imageButton.Parent.FindControl("hidVendorIndex").ToCustomHiddenField().Value.ToInt();
			var vendorId = vendors[removalIndex].VendorId;

			var truckloadBidExists = new TruckloadBidSearch().FetchTruckloadBidByProfileId(hidProfileId.Value.ToLong(), ActiveUser.TenantId);
			//Check if the current vendor is associated with truckload bid.                                                                       
			if (truckloadBidExists != null &&
			    (truckloadBidExists.FirstPreferredVendorId == vendorId.ToLong() ||
			     truckloadBidExists.SecondPreferredVendorId == vendorId.ToLong() ||
			     truckloadBidExists.ThirdPreferredVendorId == vendorId.ToLong()))
			{
				DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error("Cannot remove this vendor. Its is asociated with one or more truckload tenders.") });
				return;
			}


			vendors.RemoveAt(removalIndex);

			var lanes = GetCurrentViewLanes();

			foreach (var lane in lanes.Where(l => l.FirstPreferredVendorId == vendorId ||
			                                      l.SecondPreferredVendorId == vendorId ||
			                                      l.ThirdPreferredVendorId == vendorId))
			{
				// Remove first Preferred Vendor and proprogate others up if applicable
				if (lane.FirstPreferredVendorId == vendorId)
				{
					if (lane.SecondPreferredVendorId != default(long))
					{
						lane.FirstPreferredVendorId = lane.SecondPreferredVendorId;

						if (lane.ThirdPreferredVendorId != default(long))
						{
							lane.SecondPreferredVendorId = lane.ThirdPreferredVendorId;
							lane.ThirdPreferredVendorId = default(long);
						}
						else
						{
							lane.SecondPreferredVendorId = default(long);
						}
					}

					else
					{
						lane.FirstPreferredVendorId = default(long);
					}
				}

				// Remove second Preferred Vendor and propogate third vendor up if applicable
				if (lane.SecondPreferredVendorId == vendorId)
				{
					lane.SecondPreferredVendorId = lane.ThirdPreferredVendorId != default(long)
						                               ? lane.ThirdPreferredVendorId
						                               : default(long);
					lane.ThirdPreferredVendorId = default(long);
				}

				// Remove third Preferred Vendor
				if (lane.ThirdPreferredVendorId == vendorId)
				{
					lane.ThirdPreferredVendorId = default(long);
				}
			}

			peLstLanes.LoadData(lanes);

			upLstVendors.DataSource = vendors;
			upLstVendors.DataBind();

			tabVendors.HeaderText = vendors.BuildTabCount(VendorsHeader);
			athtuTabUpdater.SetForUpdate(tabVendors.ClientID, vendors.BuildTabCount(VendorsHeader));
		}

		protected void OnVendorFinderMultiItemSelected(object sender, ViewEventArgs<List<Vendor>> e)
		{
			var vendors = upLstVendors.Items
			                        .Select(c => new 
				                        {
											TLTenderingProfileId = hidProfileId.Value.ToLong(),
											VendorId = c.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong(),
											VendorNumber = c.FindControl("lblVendorNumber").ToLabel().Text,
											Name = c.FindControl("litName").ToLiteral().Text,
											CommunicationType = c.FindControl("ddlCommunicationType").ToDropDownList().SelectedValue.ToEnum<NotificationMethod>()
				                        })
			                        .ToList();

			var newVendors = e.Argument
			                  .Select(vendor => new 
				                  {
					                  TLTenderingProfileId = hidProfileId.Value.ToLong(),
					                  VendorId = vendor.Id,
									  VendorNumber = vendor.VendorNumber,
									  Name = vendor.Name,
					                  CommunicationType = vendor.Communication != null
						                                      ? vendor.Communication.EdiEnabled && vendor.Communication.DropOff204
																	? NotificationMethod.Edi
																	: NotificationMethod.Email
															  : NotificationMethod.Email
				                  })
			                  .Where(v => !vendors.Select(qv => qv.VendorId).Contains(v.VendorId));

			vendors.AddRange(newVendors);

			upLstVendors.DataSource = vendors;
			upLstVendors.DataBind();
			tabVendors.HeaderText = vendors.BuildTabCount(VendorsHeader);
			vendorFinder.Visible = false;
		}

		protected void OnVendorsItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var listViewItem = (ListViewDataItem) e.Item;
			var excludeTypes = new Dictionary<int, string>(2) {{0, "Ftp"}, {3, "Fax"}};
			var commuinicationType = e.Item.DataItem.GetPropertyValue("CommunicationType");
			var communicationTypesList = ProcessorUtilities.GetAll<NotificationMethod>().Except(excludeTypes);

			var ddlCommunicationType = listViewItem.FindControl("ddlCommunicationType").ToDropDownList();
			ddlCommunicationType.DataSource =
				communicationTypesList.Select(t => new ViewListItem(t.Value, t.Key.ToString())).ToList();
			ddlCommunicationType.DataBind();
			ddlCommunicationType.SelectedValue = commuinicationType.ToInt().ToString();
		}

		protected void OnVendorFinderItemCancelled(object sender, EventArgs e)
		{
			vendorFinder.Reset();
			vendorFinder.Visible = false;
		}

		protected void OnClearVendorsClicked(object sender, EventArgs e)
		{
			upLstVendors.DataSource = new List<CustomerTLTenderingProfileVendor>();

			var lanes = GetCurrentViewLanes();

			foreach (var lane in lanes)
			{
				lane.FirstPreferredVendorId = default(long);
				lane.SecondPreferredVendorId = default(long);
				lane.ThirdPreferredVendorId = default(long);
			}
			peLstLanes.LoadData(lanes);

			upLstVendors.DataBind();
			tabVendors.HeaderText = VendorsHeader;
			athtuTabUpdater.SetForUpdate(tabVendors.ClientID, VendorsHeader);
		}

		protected void OnVendorsReordered(object sender, ViewEventArgs<List<int>> e)
		{
			upLstVendors.DataSource = e
				.Argument
				.Select(idx => new
					{
						Id = upLstVendors.Items[idx - 1].FindControl("hidTlTenderProfileVendorId").ToCustomHiddenField().Value,
						VendorId = upLstVendors.Items[idx - 1].FindControl("hidVendorId").ToCustomHiddenField().Value,
						VendorNumber = upLstVendors.Items[idx - 1].FindControl("lblVendorNumber").ToLabel().Text,
						Name = upLstVendors.Items[idx - 1].FindControl("litName").ToLiteral().Text,
						MaxWaitTime = upLstVendors.Items[idx - 1].FindControl("txtMaxWaitTime").ToTextBox().Text,
					});
			upLstVendors.DataBind();
		}


		protected void OnVendorNumberEntered(object sender, EventArgs e)
		{
			var txtVendorNumber = (TextBox) sender;
			hidEditingIndex.Value = txtVendorNumber.Parent.FindControl("hidLaneIndex").ToCustomHiddenField().Value;

			var lanes = GetCurrentViewLanes();
			var editingLane = lanes[hidEditingIndex.Value.ToInt()];

			var invalidVendorSelection = false;
			switch (txtVendorNumber.ID)
			{
				case "txtVendorNumber1":
					hidPreferredVendorEditingIndex.Value = "0";
					break;
				case "txtVendorNumber2":
					if (editingLane.FirstPreferredVendorId != default(long))
					{
						hidPreferredVendorEditingIndex.Value = "1";
					}
					else
					{
						invalidVendorSelection = true;
					}
					break;
				case "txtVendorNumber3":
					if (editingLane.FirstPreferredVendorId != default(long) && editingLane.SecondPreferredVendorId != default(long))
					{
						hidPreferredVendorEditingIndex.Value = "2";
					}
					else
					{
						invalidVendorSelection = true;
					}
					break;
			}

			if (!invalidVendorSelection)
			{
				if (VendorSearch != null)
					VendorSearch(this, new ViewEventArgs<string>(txtVendorNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
			}
		}

		protected void OnVendorSearchClicked(object sender, CommandEventArgs e)
		{
			var button = (ImageButton) sender;
			hidEditingIndex.Value = button.Parent.FindControl("hidLaneIndex").ToCustomHiddenField().Value;
			var lanes = GetCurrentViewLanes();

			var editingLane = lanes[hidEditingIndex.Value.ToInt()];

			var invalidVendorSelection = false;
			switch (e.CommandName)
			{
				case "PreferredVendor1":
					hidPreferredVendorEditingIndex.Value = "0";
					break;
				case "PreferredVendor2":
					if (editingLane.FirstPreferredVendorId != default(long))
					{
						hidPreferredVendorEditingIndex.Value = "1";
					}
					else
					{
						invalidVendorSelection = true;
					}
					break;
				case "PreferredVendor3":
					if (editingLane.FirstPreferredVendorId != default(long) && editingLane.SecondPreferredVendorId != default(long))
					{
						hidPreferredVendorEditingIndex.Value = "2";
					}
					else
					{
						invalidVendorSelection = true;
					}
					break;
			}

			if (!invalidVendorSelection) vendorFinderForLanes.Visible = true;
		}

		protected void OnVendorFinderForLanesVendorSelected(object sender, ViewEventArgs<Vendor> e)
		{
			vendorFinderForLanes.Visible = false;

			var lanes = GetCurrentViewLanes();

			var vendor = e.Argument;

			switch (hidPreferredVendorEditingIndex.Value.ToInt())
			{
				case 0:
					lanes[hidEditingIndex.Value.ToInt()].FirstPreferredVendor = vendor;
					break;
				case 1:
					lanes[hidEditingIndex.Value.ToInt()].SecondPreferredVendor = vendor;
					break;
				case 2:
					lanes[hidEditingIndex.Value.ToInt()].ThirdPreferredVendor = vendor;
					break;
			}

			var vendors = upLstVendors.Items
			                        .Select(c => new 
				                        {
											TLTenderingProfileId = hidProfileId.Value.ToLong(),
											VendorId = c.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong(),
											VendorNumber = c.FindControl("lblVendorNumber").ToLabel().Text,
											Name = c.FindControl("litName").ToLiteral().Text,
											CommunicationType = c.FindControl("ddlCommunicationType").ToDropDownList().SelectedValue.ToEnum<NotificationMethod>()
				                        })
			                        .ToList();

			var newVendor = new 
				{
					TLTenderingProfileId = hidProfileId.Value.ToLong(),
					VendorId = vendor.Id,
					VendorNumber = vendor.VendorNumber,
					Name = vendor.Name,
					CommunicationType = vendor.Communication != null
						                    ? vendor.Communication.EdiEnabled && vendor.Communication.DropOff204
												  ? NotificationMethod.Edi
												  : NotificationMethod.Email
											: NotificationMethod.Email
				};

			if (!vendors.Select(v => v.VendorId).Contains(newVendor.VendorId)) vendors.Add(newVendor);

			upLstVendors.DataSource = vendors;
			upLstVendors.DataBind();
			tabVendors.HeaderText = vendors.BuildTabCount(VendorsHeader);
			athtuTabUpdater.SetForUpdate(tabVendors.ClientID, vendors.BuildTabCount(VendorsHeader));

			vendorFinder.Visible = false;

			peLstLanes.LoadData(lanes);
		}

		protected void OnVendorFinderForLanesItemCancelled(object sender, EventArgs e)
		{
			vendorFinderForLanes.Visible = false;
			hidEditingIndex.Value = string.Empty;
			hidPreferredVendorEditingIndex.Value = string.Empty;
		}


		protected void OnLanesPageChanging(object sender, EventArgs e)
		{
			var lanes = GetCurrentViewLanes();
			peLstLanes.LoadData(lanes, peLstLanes.CurrentPage);
		}

		protected void OnAddLaneClicked(object sender, EventArgs e)
		{
			var psLanes = GetCurrentViewLanes();

			var customerTlProfile = new CustomerTLTenderingProfile(hidProfileId.Value.ToLong());
			var lane = new CustomerTLTenderingProfileLane
				{
					OriginCountryId = ActiveUser.CountryId,
					DestinationCountryId = ActiveUser.CountryId,
					TLTenderingProfile = customerTlProfile
				};

			psLanes.Add(lane);

			peLstLanes.LoadData(psLanes, peLstLanes.CurrentPage);

			tabLanes.HeaderText = psLanes.BuildTabCount(LanesHeader);
			athtuTabUpdaterLanes.SetForUpdate(tabLanes.ClientID, psLanes.BuildTabCount(LanesHeader));
		}

		protected void OnLanesItemDatabound(object sender, ListViewItemEventArgs e)
		{
			var item = (ListViewDataItem) e.Item;
			var lane = (CustomerTLTenderingProfileLane) e.Item.DataItem;

			var ddlOriginCountryCode = item.FindControl("ddlOriginCountryCode").ToDropDownList();
			var ddlDestinationCountryCode = item.FindControl("ddlOriginCountryCode").ToDropDownList();

			ddlOriginCountryCode.SelectedValue = lane.OriginCountryId.GetString();
			ddlDestinationCountryCode.SelectedValue = lane.DestinationCountryId.GetString();
		}

		protected void OnDeleteLaneClicked(object sender, ImageClickEventArgs e)
		{
			var imageButton = (ImageButton) sender;
			var itemId = imageButton.FindControl("hidTLLane").ToCustomHiddenField().Value.ToLong();
			var lanes = GetCurrentViewLanes();
			var itemIdx = imageButton.FindControl("hidLaneIndex").ToCustomHiddenField().Value.ToInt();
			var fullLane = new CustomerTLTenderingProfileLane(itemId).FullLane;
			if (!ProcessorUtilities.CanDeleteLane(itemId))
			{
				DisplayMessages(new List<ValidationMessage>
					{
						ValidationMessage.Error("Cannot remove Lane:{0} as it is associated with tendering load", fullLane)
					});
				return;
			}
		    lanes.RemoveAt(itemIdx);
			
			peLstLanes.LoadData(lanes);
			tabLanes.HeaderText = lanes.BuildTabCount(LanesHeader);
			athtuTabUpdater.SetForUpdate(tabLanes.ClientID, LanesHeader);
		}

		protected void OnClearLanesClicked(object sender, EventArgs e)
		{
			List<CustomerTLTenderingProfileLane> lanes = GetCurrentViewLanes();
			var msgs = new List<ValidationMessage>();
			foreach (var item in lstLanes.Items)
			{
				int index = item.FindControl("hidLaneIndex").ToCustomHiddenField().Value.ToInt();
				int itemId = item.FindControl("hidTLLane").ToCustomHiddenField().Value.ToInt();
				if (!ProcessorUtilities.CanDeleteLane(itemId))
				{
					var fullLane = new CustomerTLTenderingProfileLane(itemId).FullLane;
					msgs.Add(ValidationMessage.Error("Cannot remove Lane : {0} as it is referenced in truckload tender.", fullLane));
				}
				else
				{
					lanes.RemoveAt(index);
				}
			}
			peLstLanes.LoadData(lanes);
			tabLanes.HeaderText = LanesHeader;
			athtuTabUpdater.SetForUpdate(tabLanes.ClientID, LanesHeader);
			if (!msgs.Any()) return;
			DisplayMessages(msgs);
		}


		//TODO: use Javascript method as in RateAndSchedule View or User View  to do this.

		protected void OnLanesExportClicked(object sender, EventArgs e)
		{
			List<string> lines = lstLanes
				.Items
				.Select(item => new[]
					{
						item.FindControl("txtOriginCity").ToTextBox().Text,
						item.FindControl("txtOriginState").ToTextBox().Text,
						item.FindControl("txtOriginPostalCode").ToTextBox().Text,
						item.FindControl("ddlOriginCountryCode").ToDropDownList().SelectedItem.Text,
						item.FindControl("txtDestinationCity").ToTextBox().Text,
						item.FindControl("txtDestinationState").ToTextBox().Text,
						item.FindControl("txtDestinationPostalCode").ToTextBox().Text,
						item.FindControl("ddlDestinationCountryCode").ToDropDownList().SelectedItem.Text,
						item.FindControl("txtVendorNumber1").ToTextBox().Text,
						item.FindControl("txtVendorName1").ToTextBox().Text,
						item.FindControl("txtVendorNumber2").ToTextBox().Text,
						item.FindControl("txtVendorName2").ToTextBox().Text,
						item.FindControl("txtVendorNumber3").ToTextBox().Text,
						item.FindControl("txtVendorName3").ToTextBox().Text
					}.TabJoin())
				.ToList();
			lines.Insert(0, new[]
				{
					"Origin City", "Origin State", "Origin Postal Code", "Origin Country", "Destination City",
					"Destination State", "Destination Postal Code", "Destination Country", "Preferred Vendor 1",
					"Preferred Vendor Name 1", "Preferred Vendor 2", "Preferred Vendor Name 2", "Preferred Vendor 3",
					"Preferred Vendor Name 3"
				}.TabJoin());
			Response.Export(lines.ToArray().NewLineJoin());
		}

		protected void OnVendorExportClicked(object sender, EventArgs e)
		{
			List<string> lines = upLstVendors
				.Items
				.Select(item => new[]
					{
						item.FindControl("lblVendorNumber").ToLabel().Text,
						item.FindControl("litName").ToLiteral().Text,
						item.FindControl("ddlCommunicationType").ToDropDownList().SelectedItem.Text
					}.TabJoin())
				.ToList();
			lines.Insert(0, new[] {"Vendor Number", "Vendor Name", "Communication Type"}.TabJoin());
			Response.Export(lines.ToArray().NewLineJoin());
		}


		protected void OnCustomerTLTenderProfileFinderItemSelected(object sender, ViewEventArgs<CustomerTLTenderingProfile> e)
		{
			LoadTenderingProfile(e.Argument);

			DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
			SetEditStatus(customerTLTenderProfileFinder.OpenForEdit);

			if (TLProfileLoadAuditLog != null) TLProfileLoadAuditLog(this, new ViewEventArgs<CustomerTLTenderingProfile>(e.Argument));
			litErrorMessages.Text = string.Empty;
			customerTLTenderProfileFinder.Visible = false;
		}

		protected void OnCustomerTLTenderProfileFinderItemCancelled(object sender, EventArgs e)
		{
			customerTLTenderProfileFinder.Visible = false;
		}
	}
}
