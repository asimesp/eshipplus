﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Smc;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Rating
{
	public partial class SmcAvailableCarriersView : MemberPageBase
	{
		public static string PageAddress { get { return "~/Members/Rating/SmcAvailableCarriersView.aspx"; } }

		public override ViewCode PageCode { get { return ViewCode.SmcAvailableCarriers; } }

		public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.RatingBlue; } }

		protected void Page_Load(object sender, EventArgs e)
		{
			if (IsPostBack) return;

			// set auto referesh
			var carriers = new CarrierConnect(ProcessorVars.IntegrationVariables[ActiveUser.TenantId].CarrierConnectParameters)
				.GetAvailableCarriers(DateTime.Now);

			rptAvailableCarriers.DataSource = carriers
				.Select(c => new
					{
						c.Name,
						c.Scac,
						Services = c
					             .Services
					             .Select(s => string.Format(" {0}:{1}", s.ServiceCode.FormattedString(), s.ServiceMethod.FormattedString()))
								 .ToArray()
								 .CommaJoin()
					})
				.ToList();
			rptAvailableCarriers.DataBind();
		}

		protected void OnToolbarExportClicked(object sender, EventArgs e)
		{
			var carriers = new CarrierConnect(ProcessorVars.IntegrationVariables[ActiveUser.TenantId].CarrierConnectParameters)
				.GetAvailableCarriers(DateTime.Now);

			var data = carriers
				.Select(c => new[]
					{
						c.Name,
						c.Scac,
						c
					             .Services
					             .Select(
						             s => string.Format(" {0}:{1}", s.ServiceCode.FormattedString(), s.ServiceMethod.FormattedString()))
					             .ToArray()
					             .CommaJoin()
					})
				.ToList();
			data.Insert(0, new[] {"Carrier Name", "Carrier Scac", "Carrier Services"});

			Response.Export(data.Select(d => d.TabJoin()).ToArray().NewLineJoin());
		}

		protected void OnDownloadTerminalsClicked(object sender, ImageClickEventArgs e)
		{
			var button = (ImageButton) sender;
			var hid = button.FindControl("hidScac").ToHiddenField();

			var terminals = new CarrierConnect(ProcessorVars.IntegrationVariables[ActiveUser.TenantId].CarrierConnectParameters)
				.GetCarrierTerminals(hid.Value, DateTime.Now);

			var data = terminals
				.Select(t => new[]
					{
						t.CarrierName, t.Scac, t.Method, t.TerminalName, t.Code, t.Address1, t.Address2, t.City, t.State, t.PostalCode,
						t.PostalCodePlus4, t.CountryCode, t.ContactName, t.ContactTitle, t.ContactEmail, t.ContactPhone, t.ContactFax,
						t.ContactTollFree
					})
				.ToList();
			data.Insert(0,
			            new[]
				            {
					            "Carrier Name", "SCAC", "Method", "Terminal Name", "Terminal Code", "Address 1", "Address 2", "City",
					            "State", "Postal Code", "Postal Code + 4", "Country Code", "Contact Name", "ConTact Title",
					            "Contact Email", "Contact Phone", "Contact Fax", "Contact Toll Free"
				            });
			Response.Export(data.Select(d => d.TabJoin()).ToArray().NewLineJoin());
		}
	}
}