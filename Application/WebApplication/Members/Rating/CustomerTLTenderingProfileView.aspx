﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="CustomerTLTenderingProfileView.aspx.cs"
	Inherits="LogisticsPlus.Eship.WebApplication.Members.Rating.CustomerTLTenderingProfileView" EnableEventValidation="false" %>

<%@ Register TagPrefix="asp" Namespace="LogisticsPlus.Eship.WebApplication.Members.Controls" Assembly="Pacman" %>

<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
	<eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true" ShowUnlock="False"
		ShowEdit="true" ShowDelete="true" ShowSave="true" ShowFind="true" ShowMore="false" ShowImport="True" ShowExport="True"
		OnFind="OnToolbarFindClicked" OnNew="OnToolbarNewClicked" OnEdit="OnToolbarEditClicked" OnDelete="OnToolbarDeleteClicked" OnUnlock="OnToolbarUnlockClicked"
		OnImport="OnToolbarImportExportClicked" OnExport="OnToolbarImportExportClicked" OnSave="OnToolbarSaveClicked" />

</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
	<div class="clearfix">
		<div class="pageHeader">
			<asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
			<h3>Truckload Tendering Profile
				<eShip:RecordIdentityDisplayControl runat="server" ID="ridcClaimNumberIdentity" TargetControlId="txtClaimNumber" />
				<eShip:RecordIdentityDisplayControl runat="server" ID="ridcClaimCustomerNameIdentity" TargetControlId="txtCustomerName" />
			</h3>
			<div class="lockMessage">
				<asp:Literal ID="litMessage" runat="server" />
			</div>
		</div>
		<hr class="dark mb5" />
		<div class="errorMsgLit">
			<asp:Literal runat="server" ID="litErrorMessages" />
		</div>

		<eShip:CustomerTLTenderProfileFinderControl ID="customerTLTenderProfileFinder" runat="server" OpenForEditEnabled="true"
			OnItemSelected="OnCustomerTLTenderProfileFinderItemSelected" OnSelectionCancel="OnCustomerTLTenderProfileFinderItemCancelled" Visible="False" />
		<eShip:CustomerFinderControl ID="customerFinder" Visible="false" runat="server" OpenForEditEnabled="false"
			EnableMultiSelection="false" OnItemSelected="OnCustomerFinderItemSelected" OnSelectionCancel="OnCustomerFinderSelectionCancel" />
		<eShip:VendorFinderControl EnableMultiSelection="true" ID="vendorFinder" OnlyActiveResults="true"
			OnMultiItemSelected="OnVendorFinderMultiItemSelected" OnSelectionCancel="OnVendorFinderItemCancelled"
			OpenForEditEnabled="false" runat="server" Visible="False" />
		<eShip:VendorFinderControl EnableMultiSelection="false" ID="vendorFinderForLanes" OnlyActiveResults="true"
			OnItemSelected="OnVendorFinderForLanesVendorSelected" OnSelectionCancel="OnVendorFinderForLanesItemCancelled"
			OpenForEditEnabled="false" runat="server" Visible="False" />


		<eShip:AjaxTabHeaderTextUpdater runat="server" ID="athtuTabUpdater" />
		<eShip:AjaxTabHeaderTextUpdater runat="server" ID="athtuTabUpdaterVendors" />
		<eShip:AjaxTabHeaderTextUpdater runat="server" ID="athtuTabUpdaterLanes" />

		<%--//TODO: Does this need to go in its own control?--%>
		<asp:Panel runat="server" ID="pnlTLtenderingImportExport" CssClass="popupControl popupControlOverW750" Visible="false">
			<div class="popheader">
				<h4>Truckload Tendering Import/Export
					<asp:ImageButton ID="btnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
						CausesValidation="false" OnClick="OnImportExportClosedClicked" runat="server" />
				</h4>
			</div>

			<div class="pl10 pt5">
				<p class="note">&#8226; Please refer to the help menu for the import file format. </p>
			</div>
			<div class="rowgroup pl10 mb0">
				<div class="col_1_2 bbox">
					<h5>Options</h5>
					<div class="row">
						<script type="text/javascript">
							$(document).ready(function () {
								// enforce mutual exclusion on checkboxes in ulCustomerRatingImportOptions
								$(jsHelper.AddHashTag('ulCustomerRatingImportOptions input:checkbox')).click(function () {
									if (jsHelper.IsChecked($(this).attr('id'))) {
										$(jsHelper.AddHashTag('ulCustomerRatingImportOptions input:checkbox')).not(jsHelper.AddHashTag($(this).attr('id'))).each(function () {
											jsHelper.UnCheckBox($(this).attr('id'));
										});
										$.uniform.update();
									}
								});
							});
						</script>
						<ul class="twocol_list" id="ulCustomerRatingImportOptions">
							<li>
								<asp:CheckBox runat="server" ID="chkVendor" CssClass="jQueryUniform" />
								<label>Vendors</label>
							</li>
							<li>
								<asp:CheckBox runat="server" ID="chkLanes" CssClass="jQueryUniform" />
								<label>Lanes</label>
							</li>
						</ul>
					</div>
				</div>
				<div class="col_1_2 bbox pl10 vlinedarkleft">
					<h5>File</h5>
					<div class="row">
						<div class="fieldgroup">
							<asp:FileUpload runat="server" ID="fupFile" CssClass="jQueryUniform" />
						</div>
					</div>
					<div class="row">
						<div class="fieldgroup pt10 pb40 mb10">
							<asp:CheckBox runat="server" ID="chkFirstLineHeader" Checked="true" CssClass="jQueryUniform" />
							<label>First line contains header</label>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="fieldgroup right mb10 pt10">
					<asp:Button ID="bttnImport" OnClick="OnImportClicked" runat="server" Text="IMPORT" CausesValidation="false" CssClass="mr10" />
					<asp:Button ID="bttnExport" OnClick="OnExportClicked" runat="server" Text="EXPORT" CausesValidation="false" CssClass="mr10" />
					<asp:Button ID="bttnClose" OnClick="OnImportExportClosedClicked" runat="server" Text="CLOSE" CausesValidation="false" />
				</div>
			</div>
		</asp:Panel>



		<asp:Panel ID="pnlAboveDetails" runat="server" CssClass="rowgroup">

			<div class="row">
				<div class="fieldgroup mr20 vlinedarkright">
					<label class="label upper blue">Customer:</label>

					<%= hidCustomerId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.Customer)
						    ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Customer Record'><img src={3} width='20' class='middle'/></a>",
						                    ResolveUrl(CustomerView.PageAddress),
						                    WebApplicationConstants.TransferNumber,
						                    hidCustomerId.Value.UrlTextEncrypt(),
						                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
						    : string.Empty %>
					<eShip:CustomTextBox runat="server" ID="txtCustomerNumber" CssClass="w150" OnTextChanged="OnCustomerNumberEntered"
						AutoPostBack="True" />
					<asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/icons2/search_dark.png"
						CausesValidation="false" OnClick="OnCustomerSearchClicked" />
					<eShip:CustomTextBox runat="server" ID="txtCustomerName" CssClass="w330 disabled" ReadOnly="True" />
					<ajax:AutoCompleteExtender runat="server" ID="aceCustomer" TargetControlID="txtCustomerNumber" ServiceMethod="GetCustomerList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
						EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
				</div>
				<div>
					<label class="label upper blue">Maximum Wait Time (Minutes):</label>
					<eShip:CustomTextBox runat="server" ID="txtMaxWaitTime" MaxLength="5" Type="FloatingPointNumbers" />
				</div>
			</div>
		</asp:Panel>

		<ajax:TabContainer ID="tabServiceTicket" runat="server" CssClass="ajaxCustom">
			<ajax:TabPanel ID="tabVendors" HeaderText="Preferred Vendors" runat="server">
				<ContentTemplate>
					<asp:UpdatePanel runat="server" ID="upPnlVendors" UpdateMode="Conditional">
						<ContentTemplate>
							<asp:Panel ID="pnlVendors" runat="server">
								<div class="rowgroup mb10">
									<div class="row">
										<div class="fieldgroup right">
											<asp:Button runat="server" ID="btnAddVendorFromFinder" Text="Add Vendor" CssClass="mr10" CausesValidation="false"
												OnClick="OnAddVendorClicked" />
											<asp:Button runat="server" ID="btnClearVendors" Text="Clear Vendors" CausesValidation="False"
												OnClick="OnClearVendorsClicked" />
											<ajax:ConfirmButtonExtender runat="server" ID="btnDelete" TargetControlID="btnClearVendors"
												ConfirmText="Deleting these vendors from here will also remove them from the list of preferred vendors for lanes in this profile. Are you sure you want to delete this vendors?" />
										</div>
									</div>
								</div>
								<eShip:TableFreezeHeaderExtender runat="server" ID="tfheVendors" TableId="preferredVendorsTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />

								<asp:UpdatePanel runat="server" ID="upVendors">
									<ContentTemplate>
										<table class="stripe" id="preferredVendorsTable">
											<tr>
												<th style="width: 5%;">Index</th>
												<th style="width: 40%;">Vendor Number</th>
												<th style="width: 40%;">Vendor Name</th>
												<th style="width: 10%;">Communication Type</th>
												<th style="width: 5%;" class="text-center">Action </th>
											</tr>
											<asp:ListView ID="upLstVendors" runat="server" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnVendorsItemDataBound">
												<LayoutTemplate>
													<asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
												</LayoutTemplate>
												<ItemTemplate>
													<tr>
														<td class="text-center">
															<%# Container.DataItemIndex + 1 %>
															<eShip:CustomHiddenField runat="server" ID="hidVendorId" Value='<%# Eval("VendorId") %>' />
															<eShip:CustomHiddenField ID="hidVendorIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
														</td>
														<td>
															<asp:Label runat="server" ID="lblVendorNumber" Text='<%# Eval("VendorNumber") %>' />
															<%# ActiveUser.HasAccessTo(ViewCode.Vendor) && GetDataItem().HasGettableProperty("VendorId")
																    ? string.Format(" <a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Vendor Record'><img src={3} width='16' class='middle'/></a>",
																                    ResolveUrl(VendorView.PageAddress),
																                    WebApplicationConstants.TransferNumber,
																                    Eval("VendorId").GetString().UrlTextEncrypt(),
																                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
																    : string.Empty %>

														</td>
														<td>
															<asp:Literal ID="litName" runat="server" Text='<%# Eval("Name") %>' />
														</td>
														<td>
															<asp:CachedObjectDropDownList runat="server" ID="ddlCommunicationType" DataTextField="Text" DataValueField="Value" CssClass="w100" />
														</td>
														<td class="text-center">
															<asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
																CausesValidation="false" Enabled='<%# Access.Modify %>' OnClick="OnDeleteVendorClicked" />
														</td>
													</tr>
													<ajax:ConfirmButtonExtender runat="server" ID="cbeDelete" TargetControlID="ibtnDelete"
														ConfirmText="Deleting this vendor from here will also remove it from the list of preferred vendors for lanes in this profile. Are you sure you want to delete this Vendor?" />
												</ItemTemplate>
											</asp:ListView>
										</table>
									</ContentTemplate>
								</asp:UpdatePanel>
							</asp:Panel>
						</ContentTemplate>
						<Triggers>
							<asp:PostBackTrigger ControlID="btnAddVendorFromFinder" />
						</Triggers>
					</asp:UpdatePanel>
				</ContentTemplate>
			</ajax:TabPanel>
			<ajax:TabPanel ID="tabLanes" HeaderText="Lanes" runat="server">
				<ContentTemplate>
					<asp:UpdatePanel runat="server" ID="upPnlLanes" UpdateMode="Always">
						<ContentTemplate>
							<asp:Panel runat="server" ID="pnlLanes">
								<div class="row mb10">
									<div class="fieldgroup left">
										<eShip:PaginationExtender runat="server" ID="peLstLanes" TargetControlId="lstLanes" PageSize="10" UseParentDataStore="False"
											OnPageChanging="OnLanesPageChanging" />
									</div>
									<div class="fieldgroup right">
										<asp:Button runat="server" ID="btnAddLane" Text="Add Lane" CssClass="mr10" CausesValidation="false" OnClick="OnAddLaneClicked" />
										<asp:Button runat="server" Text="Clear Lanes" CausesValidation="False" OnClick="OnClearLanesClicked" />
									</div>
								</div>

								<eShip:TableFreezeHeaderExtender runat="server" ID="tfheLanes" TableId="lanesTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
								<table class="stripe" id="lanesTable">
									<tr>
										<th style="width: 5%;">Index</th>
										<th style="width: 13%;">Origin City</th>
										<th style="width: 19%;">Origin State</th>
										<th style="width: 10%;">Origin Postal Code</th>
										<th style="width: 10%;">Origin Country</th>
										<th style="width: 15%;">Destination City</th>
										<th style="width: 10%;">Destination State</th>
										<th style="width: 10%;">Destination Postal Code</th>
										<th style="width: 10%;">Destination Country</th>
										<th style="width: 8%;" class="text-center">Action </th>
									</tr>
									<asp:ListView ID="lstLanes" runat="server" ItemPlaceholderID="itemPlaceHolder">
										<LayoutTemplate>
											<asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
										</LayoutTemplate>
										<ItemTemplate>
											<tr>
												<td class="top">
													<%# string.Format("{0}. ", (Container.DataItemIndex + 1) + (peLstLanes.CurrentPage - 1)*peLstLanes.PageSize) %>
													<eShip:CustomHiddenField runat="server" ID="hidTLLane" Value='<%# Eval("Id") %>' />
													<eShip:CustomHiddenField ID="hidLaneTierIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
												</td>
												<td>
													<eShip:CustomHiddenField ID="hidLaneIndex" runat="server" Value='<%# Container.DataItemIndex + (peLstLanes.PageSize*(peLstLanes.CurrentPage - 1)) %>' />
													<eShip:CustomTextBox runat="server" ID="txtOriginCity" Text='<%# Eval("OriginCity") %>'
														CssClass="w100" />
												</td>
												<td>
													<eShip:CustomTextBox runat="server" ID="txtOriginState" Text='<%# Eval("OriginState") %>' CssClass="w100" />
												</td>
												<td>

													<eShip:CustomTextBox runat="server" ID="txtOriginPostalCode" Text='<%# Eval("OriginPostalCode") %>' CssClass="w100" onchange="javascript: GetOriginCityAndState(this);" />


												</td>
												<td>
													<eShip:CachedObjectDropDownList runat="server" ID="ddlOriginCountryCode" Type="Countries"
														CssClass="w80" EnableChooseOne="True" DefaultValue="0" SelectedValue='<%# Eval("OriginCountryId") %>'
														AutoPostBack="True" />
												</td>
												<td>
													<eShip:CustomTextBox runat="server" ID="txtDestinationCity" Text='<%# Eval("DestinationCity") %>' CssClass="w100" />
												</td>
												<td>
													<eShip:CustomTextBox runat="server" ID="txtDestinationState" Text='<%# Eval("DestinationState") %>' CssClass="w100" />
												</td>
												<td>
													<eShip:CustomTextBox runat="server" ID="txtDestinationPostalCode" Text='<%# Eval("DestinationPostalCode") %>' CssClass="w100"
														onchange="javascript: GetDestinationCityAndState(this);"/>
												</td>
												<td>
													<eShip:CachedObjectDropDownList runat="server" ID="ddlDestinationCountryCode" Type="Countries"
														CssClass="w80" EnableChooseOne="True" DefaultValue="0" SelectedValue='<%# Eval("DestinationCountryId") %>' />
												</td>
												<td class="text-center">

													<asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
														CausesValidation="true" Enabled='<%# Access.Modify %>' OnClick="OnDeleteLaneClicked" />
												</td>

											</tr>

											<asp:UpdatePanel runat="server" ID="upLaneVendors">
												<ContentTemplate>

													<tr class="bottom_shadow">
														<td colspan="1"></td>
														<td colspan="9">
															<div class="rowgroup">
																<div class="row">
																	<label class="label upper blue">Preferred Vendor 1:</label>
																	<eShip:CustomHiddenField runat="server" ID="hidVendor1Id" Value='<%# Eval("FirstPreferredVendorId") %>' />

																	<eShip:CustomTextBox runat="server" ID="txtVendorNumber1" CssClass="w150" OnTextChanged="OnVendorNumberEntered"
																		AutoPostBack="True" Text='<%# Eval("FirstPreferredVendor.VendorNumber") %>' CausesValidation="true" />
																	<asp:ImageButton ID="searchForPreferrredVendor1" runat="server" ImageUrl="~/images/icons2/search_dark.png"
																		CausesValidation="false" OnCommand="OnVendorSearchClicked" CommandName="PreferredVendor1" />
																	<eShip:CustomTextBox runat="server" ID="txtVendorName1" CssClass="w330 disabled" ReadOnly="True"
																		Text='<%# Eval("FirstPreferredVendor.Name") %>' />
																	<ajax:AutoCompleteExtender runat="server" ID="aceVendor1" TargetControlID="txtVendorNumber1" ServiceMethod="GetVendorList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
																		EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
																</div>
																<div class="row">
																	<label class="label upper blue">Preferred Vendor 2:</label>
																	<eShip:CustomHiddenField runat="server" ID="hidVendor2Id" Value='<%# Eval("SecondPreferredVendorId") %>' />

																	<eShip:CustomTextBox runat="server" ID="txtVendorNumber2" CssClass="w150" OnTextChanged="OnVendorNumberEntered"
																		AutoPostBack="True" Text='<%# Eval("SecondPreferredVendor.VendorNumber") %>' CausesValidation="true" />
																	<asp:ImageButton ID="searchForPreferrredVendor2" runat="server" ImageUrl="~/images/icons2/search_dark.png"
																		CausesValidation="false" OnCommand="OnVendorSearchClicked" CommandName="PreferredVendor2" />
																	<eShip:CustomTextBox runat="server" ID="txtVendorName2" CssClass="w330 disabled" ReadOnly="True"
																		Text='<%# Eval("SecondPreferredVendor.Name") %>' />
																	<ajax:AutoCompleteExtender runat="server" ID="aceVendor2" TargetControlID="txtVendorNumber2" ServiceMethod="GetVendorList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
																		EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
																</div>
																<div class="row">
																	<label class="label upper blue">Preferred Vendor 3:</label>
																	<eShip:CustomHiddenField runat="server" ID="hidVendor3Id" Value='<%# Eval("ThirdPreferredVendorId") %>' />

																	<eShip:CustomTextBox runat="server" ID="txtVendorNumber3" CssClass="w150" OnTextChanged="OnVendorNumberEntered"
																		AutoPostBack="True" Text='<%# Eval("ThirdPreferredVendor.VendorNumber") %>' CausesValidation="true" />
																	<asp:ImageButton ID="searchForPreferrredVendor3" runat="server" ImageUrl="~/images/icons2/search_dark.png"
																		CausesValidation="false" OnCommand="OnVendorSearchClicked" CommandName="PreferredVendor3" />
																	<eShip:CustomTextBox runat="server" ID="txtVendorName3" CssClass="w330 disabled" ReadOnly="True"
																		Text='<%# Eval("ThirdPreferredVendor.Name") %>' />
																	<ajax:AutoCompleteExtender runat="server" ID="aceVendor3" TargetControlID="txtVendorNumber3" ServiceMethod="GetVendorList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
																		EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />

																</div>
															</div>
														</td>
													</tr>


												</ContentTemplate>
												<Triggers>
													<asp:PostBackTrigger ControlID="searchForPreferrredVendor1" />
													<asp:PostBackTrigger ControlID="searchForPreferrredVendor2" />
													<asp:PostBackTrigger ControlID="searchForPreferrredVendor3" />
												</Triggers>
											</asp:UpdatePanel>
										</ItemTemplate>
									</asp:ListView>
								</table>
							</asp:Panel>
						</ContentTemplate>
					</asp:UpdatePanel>
				</ContentTemplate>
			</ajax:TabPanel>
			<ajax:TabPanel runat="server" ID="tabAuditLog" HeaderText="Audit Log">
				<ContentTemplate>
					<eShip:AuditLogControl runat="server" ID="auditLogs" />
				</ContentTemplate>
			</ajax:TabPanel>
		</ajax:TabContainer>
	</div>


	

	<eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
		OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="
		OnFileUploadedError" />

	<asp:UpdatePanel runat="server" UpdateMode="Always">
		<ContentTemplate>
			<asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />

			<eShip:CustomHiddenField runat="server" ID="hidCustomerId" />
			<eShip:CustomHiddenField runat="server" ID="hidProfileId" />
			<eShip:CustomHiddenField runat="server" ID="hidFlag" />
			<eShip:CustomHiddenField runat="server" ID="hidEditingIndex" />
			<eShip:CustomHiddenField runat="server" ID="hidPreferredVendorEditingIndex" />
			<eShip:CustomHiddenField runat="server" ID="hidFilesToDelete" />
			<eShip:CustomHiddenField runat="server" ID="hidEditStatus" />
			<eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
		Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" />
		</ContentTemplate>
	</asp:UpdatePanel>

<%--	Function for updating Stae and City on postal code change--%>
	<script type="text/javascript">
		function GetOriginCityAndState(postalCode) {

			getCityAndState();

			function getCityAndState() {
				var cityId = $(postalCode).parent().parent().find('[id$="txtOriginCity"]').attr("id");
				var stateId = $(postalCode).parent().parent().find('[id$="txtOriginState"]').attr("id");
				var postalCodeVal = $(postalCode).val();
				var countryVal = $(postalCode).parent().parent().find('[id$="ddlOriginCountryCode"]').val();

				var ids = new ControlIds();
				ids.City = cityId;
				ids.State = stateId;

				FindCityAndState(
					postalCodeVal,
					countryVal,
					ids
				);
			}

			return false;
		}
		function GetDestinationCityAndState(postalCode) {

			getCityAndState();

			function getCityAndState() {
				var cityId = $(postalCode).parent().parent().find('[id$="txtDestinationCity"]').attr("id");
				var stateId = $(postalCode).parent().parent().find('[id$="txtDestinationState"]').attr("id");
				var postalCodeVal = $(postalCode).val();
				var countryVal = $(postalCode).parent().parent().find('[id$="ddlDestinationCountryCode"]').val();

				var ids = new ControlIds();
				ids.City = cityId;
				ids.State = stateId;

				FindCityAndState(
					postalCodeVal,
					countryVal,
					ids
				);
			}

			return false;
		}
</script>
</asp:Content>
