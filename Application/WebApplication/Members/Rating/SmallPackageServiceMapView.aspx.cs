﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Rating;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Rating
{
    public partial class SmallPackageServiceMapView : MemberPageBase, ISmallPackageServiceMapView
    {
        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public static string PageAddress { get { return "~/Members/Rating/SmallPackageServiceMapView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.SmallPackageServiceMaps; } }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RatingBlue; }
        }

        public Dictionary<int, string> SmallPackageEngines
        {
            set
            {
                ddlSmallPackEngine.DataSource = value.Keys.Select(k => new ViewListItem(value[k], k.GetString())).ToList();
                ddlSmallPackEngine.DataBind();
            }
        }

        public Dictionary<string, string> SmallPackageEngineServices
        {
            set
            {
                ddlEngineService.DataSource = value.Keys.Select(k => new ViewListItem(value[k], k)).ToList();
                ddlEngineService.DataBind();
            }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<SmallPackageServiceMap>> Save;
        public event EventHandler<ViewEventArgs<SmallPackageServiceMap>> Delete;
        public event EventHandler<ViewEventArgs<SmallPackageServiceMap>> Lock;
        public event EventHandler<ViewEventArgs<SmallPackageServiceMap>> UnLock;
        public event EventHandler Search;

        public void DisplaySearchResult(List<SmallPackageServiceMap> smallPackageServiceMaps)
        {
            litRecordCount.Text = smallPackageServiceMaps.BuildRecordCount();
            lvwRegistry.DataSource = smallPackageServiceMaps;
            lvwRegistry.DataBind();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() && !messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void FailedLock(Lock @lock)
        {
            btnSave.Enabled = false;

            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new SmallPackageServiceMapHandler(this);
            handler.Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowNew = Access.Modify;

            if (IsPostBack) return;

            if (Loading != null)
                Loading(this, new EventArgs());

            DoSearch();
        }

        private void GeneratePopup(string title)
        {
            litTitle.Text = title;

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = pnlEdit.Visible;

            ddlSmallPackEngine.Focus();
        }

        private void CloseEditPopup()
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<SmallPackageServiceMap>(new SmallPackageServiceMap(hidSmallPackageServiceMapId.Value.ToLong(), false)));
        }

        private void ClearFields()
        {
            ddlEngineService.SelectedIndex = 0;
            ddlSmallPackEngine.SelectedIndex = 0;
            ddlService.SelectedIndex = 0;
        }

        protected void OnNewClick(object sender, EventArgs e)
        {
            ClearFields();
            hidSmallPackageServiceMapId.Value = string.Empty;
            GeneratePopup("Add Small Package Service Mapping");
        }

        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            ClearFields();
            GeneratePopup("Modify Small Package Service Map");

            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("smallPackagingServiceMapId").ToCustomHiddenField();

            var smallPackageServiceMap = new SmallPackageServiceMap(hidden.Value.ToLong(), false);

            hidSmallPackageServiceMapId.Value = smallPackageServiceMap.Id.ToString();
            ddlEngineService.Text = smallPackageServiceMap.SmallPackEngineService;
            ddlSmallPackEngine.SelectedValue = ((int)smallPackageServiceMap.SmallPackageEngine).ToString();
            ddlService.SelectedValue = smallPackageServiceMap.ServiceId.GetString();

            if (Lock != null)
                Lock(this, new ViewEventArgs<SmallPackageServiceMap>(smallPackageServiceMap));

        }

        protected void OnCloseClicked(object sender, object e)
        {
            CloseEditPopup();
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            var smallPackageServiceMapId = hidSmallPackageServiceMapId.Value.ToLong();

            var smallPackageServiceMap = new SmallPackageServiceMap(smallPackageServiceMapId, smallPackageServiceMapId != default(long))
            {
                SmallPackageEngine = ddlSmallPackEngine.SelectedValue.ToEnum<SmallPackageEngine>(),
                SmallPackEngineService = ddlEngineService.Text,
                ServiceId = ddlService.SelectedValue.ToInt()
            };


            if (smallPackageServiceMap.IsNew)
            {
                smallPackageServiceMap.TenantId = ActiveUser.TenantId;
            }

            if (Save != null)
                Save(this, new ViewEventArgs<SmallPackageServiceMap>(smallPackageServiceMap));

            DoSearch();
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("smallPackagingServiceMapId").ToCustomHiddenField();

            if (Delete != null)
                Delete(this, new ViewEventArgs<SmallPackageServiceMap>(new SmallPackageServiceMap(hidden.Value.ToLong(), true)));

            DoSearch();
        }

        private void DoSearch()
        {
            if (Search != null)
                Search(this, new ViewEventArgs<string>(SearchUtilities.WildCard));
        }

        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (CloseEditPopUp) CloseEditPopup();
        }
    }

}
