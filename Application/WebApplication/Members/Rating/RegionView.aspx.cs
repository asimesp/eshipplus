﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Dto.Rating;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Rating;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Rating;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Rating
{
    public partial class RegionView : MemberPageBase, IRegionView
    {
        protected const string GotoPage = "Go to page with record index ";

	    private const string MaxAreaPerRegionLimitMsg = "Maximum areas per region is set at {0}.  All additions after {0} are ignored!";

        private const string LocatePostalCodes = "LocatePostalCodes";

	    private const int AreasLimit = 10000;
        private const int PageSize = 200;

        private const string SaveFlag = "S";
        private const string MultiUse = "MU";
        private const string SingleUse = "SU";


        public static string PageAddress { get { return "~/Members/Rating/RegionView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.Region; } }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RatingBlue; }
        }

        public event EventHandler<ViewEventArgs<Region>> Save;
        public event EventHandler<ViewEventArgs<Region>> Delete;
        public event EventHandler<ViewEventArgs<Region>> Lock;
        public event EventHandler<ViewEventArgs<Region>> UnLock;
        public event EventHandler<ViewEventArgs<Region>> LoadAuditLog;

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == SaveFlag)
            {
                // unlock and return to read-only
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<Region>(new Region(hidRegionId.Value.ToLong(), false)));
                SetEditStatus(false);
                memberToolBar.ShowUnlock = false;
            }

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
            messageBox.Button = MessageButton.Ok;

            litErrorMessages.Text = messages.HasErrors()
                                        ? string.Join(WebApplicationConstants.HtmlBreak,
                                                      messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
                                                        Select(m => m.Message).ToArray())
                                        : string.Empty;
        }

        public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs)
        {
            auditLogs.Load(logs);
        }

        public void FailedLock(Lock @lock)
        {
            memberToolBar.ShowNew = false;
            memberToolBar.ShowSave = false;
            memberToolBar.ShowDelete = false;
            memberToolBar.ShowUnlock = false;

            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void Set(Region region)
        {
            LoadRegion(region);
            SetEditStatus(!region.IsNew);
        }

        private void SetEditStatus(bool enabled)
        {
            txtName.Enabled = enabled;
            btnAddSubRegion.Enabled = enabled;
            btnAddPostalCodes.Enabled = enabled;
            btnRemoveAreas.Enabled = enabled;
            btnRemoveSelected.Enabled = enabled;
            chkSelectAllRecords.Enabled = enabled;
            lstAreas.Enabled = enabled;

            memberToolBar.EnableImport = enabled;
            memberToolBar.EnableSave = enabled;

            litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;

            memberToolBar.LoadAdditionalActions(ToolbarMoreActions());
        }


        private List<ToolbarMoreAction> ToolbarMoreActions()
        {
            var matchingDiscountTiers = new ToolbarMoreAction
            {
                CommandArgs = LocatePostalCodes,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Rating,
                Name = "Locate Postal Code",
                IsLink = false,
                NavigationUrl = string.Empty
            };

            var moreActions = new List<ToolbarMoreAction> { matchingDiscountTiers };
            
            return moreActions;
        }



        private void LoadRegion(Region region)
        {
            if (!region.IsNew) region.LoadCollections();

            hidRegionId.Value = region.Id.ToString();

            txtName.Text = region.Name;

	        var ta = new AreaViewSearchDto().FetchRegionAreas(region);
	        peLstAreas.LoadData(SortAreas(ta));

            chkSelectAllRecords.Checked = false;

            memberToolBar.ShowUnlock = region.HasUserLock(ActiveUser, region.Id);
        }

        private static List<AreaViewSearchDto> SortAreas(IEnumerable<AreaViewSearchDto> areas)
        {
            return areas.OrderByDescending(a => a.UseSubRegion)
                        .ThenBy(a => a.SubRegionName)
                        .ThenBy(a => a.PostalCode).ThenBy(a => a.CountryName)
                        .ThenBy(a => a.State).ThenBy(a => a.City).ToList();
        }


        private void UpdateAreas(Region region)
        {
	        var regionAreas = peLstAreas.GetData<AreaViewSearchDto>()
	                                    .Select(i => new Area(i.Id, i.Id != default(long))
		                                    {
			                                    CountryId = i.CountryId,
			                                    PostalCode = i.PostalCode,
			                                    UseSubRegion = i.UseSubRegion,
			                                    SubRegionId = i.SubRegionId,
			                                    Region = region,
			                                    TenantId = region.TenantId
		                                    })
	                                    .ToList();

            region.Areas = regionAreas;
        }


        private void ProcessTransferredRequest(Region region)
        {
            if (region.IsNew) return;
            
            LoadRegion(region);

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<Region>(region));
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new RegionHandler(this).Initialize();

            memberToolBar.ShowSave = Access.Modify;
            memberToolBar.ShowDelete = Access.Remove;
            memberToolBar.ShowNew = Access.Modify;
            memberToolBar.ShowEdit = Access.Modify;
            memberToolBar.ShowImport = Access.Modify;
            memberToolBar.ShowExport = Access.Grant;

            regionFinder.OpenForEditEnabled = Access.Modify;

            memberToolBar.LoadAdditionalActions(ToolbarMoreActions());

            if (IsPostBack) return;

            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.RegionsImportTemplate });

            if (Session[WebApplicationConstants.TransferRegionId] != null)
            {
                ProcessTransferredRequest(new Region(Session[WebApplicationConstants.TransferRegionId].ToLong()));
                Session[WebApplicationConstants.TransferRegionId] = null;
            }

            if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.TransferNumber]))
                ProcessTransferredRequest(new Region(Request.QueryString[WebApplicationConstants.TransferNumber].UrlTextDecrypt().ToLong()));

            SetEditStatus(false);
        }


        protected void OnNewClicked(object sender, EventArgs e)
        {
            litErrorMessages.Text = string.Empty;

            LoadRegion(new Region());
            DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
            SetEditStatus(true);
        }

        protected void OnEditClicked(object sender, EventArgs e)
        {
            var region = new Region(hidRegionId.Value.ToLong(), false);

            if (Lock == null || region.IsNew) return;

            SetEditStatus(true);
            memberToolBar.ShowUnlock = true;
            Lock(this, new ViewEventArgs<Region>(region));

            LoadRegion(region);
        }

        protected void OnUnlockClicked(object sender, EventArgs e)
        {
            var region = new Region(hidRegionId.Value.ToLong(), false);

            if (UnLock == null || region.IsNew) return;

            SetEditStatus(false);
            memberToolBar.ShowUnlock = false;
            UnLock(this, new ViewEventArgs<Region>(region));
        }

        protected void OnSaveClicked(object sender, EventArgs e)
        {
            var regionId = hidRegionId.Value.ToLong();
            var region = new Region(regionId, regionId != default(long));

            if (region.IsNew)
                region.TenantId = ActiveUser.TenantId;
            else
                region.LoadCollections();

            region.Name = txtName.Text;

            UpdateAreas(region);

            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<Region>(region));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<Region>(region));
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var region = new Region(hidRegionId.Value.ToLong(), false);

            if (Delete != null)
                Delete(this, new ViewEventArgs<Region>(region));

            regionFinder.Reset();
        }

        protected void OnFindClicked(object sender, EventArgs e)
        {
            if (hidLastFinderUse.Value != SingleUse)
            {
                regionFinder.Reset();
                hidLastFinderUse.Value = SingleUse;
            }

            regionFinder.EnableMultiSelection = false;
            regionFinder.OpenForEditEnabled = true;
            regionFinder.Visible = true;
        }

        protected void OnCommandClicked(object sender, ViewEventArgs<string> e)
        {
            switch (e.Argument)
            {
                case LocatePostalCodes:
                    pnlLocatePostalCode.Visible = true;
                    pnlDimScreen.Visible = true;
                    break;
            }
        }


        protected void OnExportAreasClicked(object sender, EventArgs e)
        {
            var region = new Region(hidRegionId.Value.ToLong());
            var search = new PostalCodeSearch();

            var lines = region.Areas
                .Select(a =>
                {
                    var code = !a.UseSubRegion
                                ? search.FetchPostalCodeByCode(a.PostalCode, a.CountryId)
                                : new PostalCodeViewSearchDto();
                    return new[]
				             	       	{
				             	       		a.UseSubRegion ? a.SubRegion.Name : string.Empty,
				             	       		a.UseSubRegion.ToString(),
				             	       		code.Code,
				             	       		code.CountryCode,
											code.City,
											code.CityAlias,
											code.State
				             	       	}.TabJoin();
                })
                .ToList();
            lines.Insert(0, new[] { "Region Name", "Use Sub Region", "Postal Code", "Country Code", "City", "City Alias", "State" }.TabJoin());

            Response.Export(lines.ToArray().NewLineJoin());
        }

        protected void OnImportAreasClicked(object sender, EventArgs e)
        {
            fileUploader.Title = "REGION AREAS IMPORT";
            fileUploader.Instructions = @"**Please refer to user guide for import file format.";
            fileUploader.Visible = true;
        }

        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            var msgs = new List<ValidationMessage>();

            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 4;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var postalCodeSearch = new PostalCodeSearch();
            var countries = ProcessorVars.RegistryCache.Countries.Values;
            var countryCodes = countries.Select(c => c.Code).ToList();
            msgs.AddRange(chks.Where(i => !i.Line[1].ToBoolean()).CodesAreValid(3, countryCodes, new Country().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var postalCodes = new List<PostalCodeViewSearchDto>();
            msgs = chks
                .Where(i => !i.Line[1].ToBoolean())
                .Where(i =>
                {
                    var code = postalCodeSearch.FetchPostalCodeByCode(i.Line[2], countries.First(c => c.Code == i.Line[3]).Id);
                    postalCodes.Add(code);
                    return code.Id == default(long);
                })
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedCodeErrMsg, new PostalCode().EntityName(), i.Index))
                .ToList();
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var rParameters = chks
                .Where(i => i.Line[1].ToBoolean())
                .Select(i => i.Line[0])
                .Distinct()
                .Select(n =>
                {
                    var p = RatingSearchFields.RegionName.ToParameterColumn();
                    p.DefaultValue = n;
                    p.Operator = Operator.Equal;
                    return p;
                })
                .ToList();

            var regions = new List<Region>();
            if (rParameters.Any()) regions = new RegionSearch().FetchRegions(rParameters, ActiveUser.TenantId);

            var regionNames = regions.Select(c => c.Name).ToList();
            msgs.AddRange(chks.Where(i => i.Line[1].ToBoolean()).CodesAreValid(0, regionNames, new Region().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var areas = lines
                .Select(s =>
                {
                    var useSubRegion = s[1].ToBoolean();
                    var code = useSubRegion ? null : postalCodes.First(pc => pc.Code.ToLower() == s[2].ToLower() && pc.CountryId == countries.First(c => c.Code == s[3]).Id);
                    var region = useSubRegion ? regions.First(r => r.Name == s[0]) : null;
                    return new AreaViewSearchDto
                    {
                        PostalCode = code == null ? string.Empty : code.Code,
                        City = code == null ? string.Empty : code.City,
                        CityAlias = code == null ? string.Empty : code.CityAlias,
                        State = code == null ? string.Empty : code.State,
                        CountryName = code == null ? string.Empty : code.CountryName,
                        CountryId = code == null ? default(long) : code.CountryId,
                        UseSubRegion = useSubRegion,
                        SubRegionName = region == null ? string.Empty : region.Name,
                        SubRegionId = region == null ? default(long) : region.Id,
                    };
                })
                .ToList();

            var vAreas = peLstAreas.GetData<AreaViewSearchDto>();

            msgs = new List<ValidationMessage>();
            if ((vAreas.Count + areas.Count) >= AreasLimit)
                msgs.Add(ValidationMessage.Warning(MaxAreaPerRegionLimitMsg, AreasLimit));

            cnt = vAreas.Count;
            for (var i = 0; i < areas.Count && (cnt + i) < AreasLimit; i++)
            {
                var area = areas[i];
                if (area.UseSubRegion && vAreas.All(a => a.SubRegionId != area.SubRegionId))
                    vAreas.Add(area);
                else if (!area.UseSubRegion && !vAreas.Any(a => a.PostalCode == area.PostalCode && a.CountryId == area.CountryId))
                    vAreas.Add(area);
            }

			// reload data
	        peLstAreas.LoadData(SortAreas(vAreas));

            fileUploader.Visible = false;

            DisplayMessages(msgs);
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }


        protected void OnRegionFinderItemSelected(object sender, ViewEventArgs<Region> e)
        {
            litErrorMessages.Text = string.Empty;

            if (hidRegionId.Value.ToLong() != default(long))
            {
                var oldRegion = new Region(hidRegionId.Value.ToLong(), false);
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<Region>(oldRegion));
            }

            var region = e.Argument;

            LoadRegion(region);

            regionFinder.Visible = false;

            if (regionFinder.OpenForEdit && Lock != null)
            {
                SetEditStatus(true);
                memberToolBar.ShowUnlock = true;
                Lock(this, new ViewEventArgs<Region>(region));
            }
            else
            {
                SetEditStatus(false);
            }

            if (LoadAuditLog != null) LoadAuditLog(this, new ViewEventArgs<Region>(region));
        }

        protected void OnRegionFinderMultiItemSelected(object sender, ViewEventArgs<List<Region>> e)
        {
			var vAreas = peLstAreas.GetData<AreaViewSearchDto>();

            var msgs = new List<ValidationMessage>();
            var regionId = hidRegionId.Value.ToLong();

            if ((vAreas.Count + e.Argument.Count) >= AreasLimit)
                msgs.Add(ValidationMessage.Warning(MaxAreaPerRegionLimitMsg, AreasLimit));
            if (e.Argument.Any(r => r.Id == regionId))
                msgs.Add(ValidationMessage.Warning("Attempt to add region to itself as a sub region detected.  Record is ignored!")); // ignore below

            var cnt = vAreas.Count;
            for (var i = 0; i < e.Argument.Count && (cnt + i) < AreasLimit; i++)
                if (e.Argument[i].Id != regionId)
                    vAreas.Add(new AreaViewSearchDto
                    {
                        PostalCode = string.Empty,
                        City = string.Empty,
                        CityAlias = string.Empty,
                        State = string.Empty,
                        CountryName = string.Empty,
                        CountryId = default(long),
                        UseSubRegion = true,
                        SubRegionName = e.Argument[i].Name,
                        SubRegionId = e.Argument[i].Id,
                    });
           
			peLstAreas.LoadData(SortAreas(vAreas)); // reload

            regionFinder.Visible = false;
            chkSelectAllRecords.Checked = false;

            DisplayMessages(msgs);
        }

        protected void OnRegionFinderSelectionCancelled(object sender, EventArgs e)
        {
            regionFinder.Visible = false;
        }



        protected void OnAddSubRegionClicked(object sender, EventArgs e)
        {
            if (hidLastFinderUse.Value != MultiUse)
            {
                regionFinder.Reset();
                hidLastFinderUse.Value = MultiUse;
            }

            regionFinder.EnableMultiSelection = true;
            regionFinder.OpenForEditEnabled = false;
            regionFinder.Visible = true;
        }

        protected void OnDeleteAreaClicked(object sender, ImageClickEventArgs e)
        {
            var button = (ImageButton)sender;

			var vAreas = SortAreas(peLstAreas.GetData<AreaViewSearchDto>());
	        var pageIdx = peLstAreas.CurrentPage - 1; // current page is actual page number
            var itemIdx = button.FindControl("hidAreaItemIndex").ToCustomHiddenField().Value.ToInt();

            vAreas.RemoveAt((pageIdx * PageSize) + itemIdx);

	        peLstAreas.LoadData(SortAreas(vAreas), pageIdx + 1);

            chkSelectAllRecords.Checked = false;
        }

        protected void OnAddPostalCodesClicked(object sender, EventArgs e)
        {
            postalCodeFinder.Visible = true;
        }

        protected void OnPostalCodeFinderSelectionCancelled(object sender, EventArgs e)
        {
            postalCodeFinder.Visible = false;
        }

        protected void OnPostalCodeFinderMultiItemSelected(object sender, ViewEventArgs<List<PostalCodeViewSearchDto>> e)
        {
			var vAreas = peLstAreas.GetData<AreaViewSearchDto>();

            var msgs = new List<ValidationMessage>();
            if ((vAreas.Count + e.Argument.Count) >= AreasLimit)
                msgs.Add(ValidationMessage.Warning(MaxAreaPerRegionLimitMsg, AreasLimit));

            var cnt = vAreas.Count;
            for (var i = 0; i < e.Argument.Count && (cnt + i) < AreasLimit; i++)
                if (!vAreas.Any(a => a.PostalCode == e.Argument[i].Code && a.CountryId == e.Argument[i].CountryId))
                    vAreas.Add(new AreaViewSearchDto
                    {
                        PostalCode = e.Argument[i].Code,
                        City = e.Argument[i].City,
                        CityAlias = e.Argument[i].CityAlias,
                        State = e.Argument[i].State,
                        CountryName = e.Argument[i].CountryName,
                        CountryId = e.Argument[i].CountryId,
                        UseSubRegion = false,
                        SubRegionName = string.Empty,
                        SubRegionId = default(long),
                    });
           
			peLstAreas.LoadData(SortAreas(vAreas)); // reload

			postalCodeFinder.Visible = false;
            chkSelectAllRecords.Checked = false;

            DisplayMessages(msgs);
        }


        protected void OnRemoveAreasCloseClicked(object sender, EventArgs e)
        {
            pnlRemoveAreas.Visible = false;
            pnlDimScreen.Visible = false;
        }

        protected void OnRemoveAreasClicked(object sender, EventArgs e)
        {
            pnlRemoveAreas.Visible = true;
            pnlDimScreen.Visible = true;
        }

        protected void OnRemoveAreasRemoveClicked(object sender, EventArgs e)
        {
            var matchCriteria = txtRemovePostalCodeCriteria.Text;

            var filterCity = chkRemoveAreaCityMatch.Checked;
            var filterCode = chkRemoveAreaPostalCodeCodeMatch.Checked;
            var filterCountry = chkRemoveAreaCountryMatch.Checked;
            var filterState = chkRemoveAreaStateMatch.Checked;
            var filterSubRegion = chkRemoveAreaSubRegionMatch.Checked;

			var vAreas = (from i in peLstAreas.GetData<AreaViewSearchDto>()
                          where
                              ((filterCity && !i.City.StartsWith(matchCriteria)) || !filterCity) &&
							  ((filterCity && !i.CityAlias.StartsWith(matchCriteria)) || !filterCity) &&
							  ((filterCode && !i.PostalCode.StartsWith(matchCriteria)) || !filterCode) &&
							  ((filterCountry && !i.CountryName.StartsWith(matchCriteria)) || !filterCountry) &&
							  ((filterState && !i.State.StartsWith(matchCriteria)) || !filterState) &&
							  ((filterSubRegion && !i.SubRegionName.StartsWith(matchCriteria)) || !filterSubRegion)
                          select i)
                .ToList();

			peLstAreas.LoadData(SortAreas(vAreas)); // reload

            pnlRemoveAreas.Visible = false;
            pnlDimScreen.Visible = false;
            chkSelectAllRecords.Checked = false;
        }

        protected void OnRemoveSelectedClicked(object sender, EventArgs e)
        {
	        var indices = lstAreas
		        .Items
		        .Select(i => new
			        {
				        Idx = i.FindControl("hidAreaItemIndex").ToCustomHiddenField().Value.ToInt(),
				        Selected = i.FindControl("chkSelected").ToAltUniformCheckBox().Checked
			        })
		        .Where(i => i.Selected)
		        .OrderByDescending(i => i.Idx)
		        .Select(i => i.Idx)
		        .ToList();

			var vAreas = SortAreas(peLstAreas.GetData<AreaViewSearchDto>());
	        var pageIdx = peLstAreas.CurrentPage - 1;

            foreach (var idx in indices)
                vAreas.RemoveAt((pageIdx * PageSize) + idx);

	        peLstAreas.LoadData(SortAreas(vAreas), pageIdx + 1);

            chkSelectAllRecords.Checked = false;
        }


        protected void OnCloseLocatePostalCodeClicked(object sender, ImageClickEventArgs e)
        {
            lstPageLocations.DataSource = new List<object>();
            lstPageLocations.DataBind();

            pnlLocatePostalCode.Visible = false;
            pnlDimScreen.Visible = false;
        }

        protected void OnLocatePostalCodeIndexClicked(object sender, ImageClickEventArgs e)
        {
            var btn = (ImageButton)sender;
            peLstAreas.LoadPageWithRecordIndex(btn.ToolTip.Replace(GotoPage, string.Empty).ToInt());

            tabRegions.ActiveTab = tabDetails;

            lstPageLocations.DataSource = new List<object>();
            lstPageLocations.DataBind();

            pnlLocatePostalCode.Visible = false;
            pnlDimScreen.Visible = false;
        }

        protected void OnLocatePostalCodeClicked(object sender, EventArgs e)
        {
            if (hidRegionId.Value.ToLong() == default(long)) return;

            var dto = new MatchingRegionPostalCodeWithinRegionDto(ActiveUser.TenantId, ddlCountry.SelectedValue.ToLong(), hidRegionId.Value.ToLong(), txtPostalCode.Text);
            var areas = peLstAreas.GetData<AreaViewSearchDto>();
            var cnt = 0;
            var results = areas
                .Select(a => new {Area = a, Index = ++cnt})
                .Where(a => (!a.Area.UseSubRegion && a.Area.PostalCode == txtPostalCode.Text && a.Area.CountryId == ddlCountry.SelectedValue.ToLong()) || dto.RegionIds.Contains(a.Area.SubRegionId))
                .Select(a => new
                    {
                        a.Index,
                        Text = a.Area.UseSubRegion ? a.Area.SubRegionName : a.Area.PostalCode
                    });

            lstPageLocations.DataSource = results;
            lstPageLocations.DataBind();
        }
    }
}
