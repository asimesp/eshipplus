﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="ResellerAdditionView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Rating.ResellerAdditionView" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        OnNew="OnNewClick" ShowFind="true" OnFind="OnFindClicked" ShowEdit="true" OnEdit="OnEditClicked" OnUnlock="OnUnlockClicked"
        ShowDelete="true" OnDelete="OnDeleteClicked" ShowSave="true" OnSave="OnSaveClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Reseller Addition
                <eShip:RecordIdentityDisplayControl runat="server" ID="ridcRecordIdentity" TargetControlId="txtResellerName" />
            </h3>

            <div class="lockMessage">
                <asp:Literal ID="litMessage" runat="server" />
            </div>
        </div>
        <hr class="dark mb5" />
        <div class="errorMsgLit">
            <asp:Literal runat="server" ID="litErrorMessages" />
        </div>

        <script type="text/javascript">
            function ClearHidFlag() {
                jsHelper.SetEmpty('<%= hidFlag.ClientID %>');
            }
        </script>

        <eShip:CustomerFinderControl ID="customerFinder" Visible="false" runat="server" OpenForEditEnabled="false"
            EnableMultiSelection="false" OnItemSelected="OnCustomerFinderItemSelected" OnSelectionCancel="OnCustomerFinderSelectionCancelled" />
        <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
            Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" />

        <eShip:ResellerAdditionFinderControl runat="server" ID="resellerAdditionFinder" Visible="false"
            OpenForEditEnabled="true" OnItemSelected="OnResellerFinderItemSelected" OnSelectionCancel="OnResellerFinderItemCancelled" />
        <eShip:CustomHiddenField runat="server" ID="hidResellerAdditionId" />

        <ajax:TabContainer ID="TabContainer1" runat="server" CssClass="ajaxCustom">
            <ajax:TabPanel runat="server" ID="tabDetails" HeaderText="Details">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlDetails">
                        <div class="rowgroup mb10">
                            <div class="col_1_2 bbox vlinedarkright">
                                <div class="rowgroup">
                                    <div class="row">
                                        <div class="fieldgroup">
                                            <label class="wlabel blue">Reseller Name</label>
                                            <eShip:CustomTextBox runat="server" ID="txtResellerName" CssClass="w420" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col_1_2 bbox pl46">
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel blue">
                                            Customer
                                            <%= hidCustomerId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.Customer) 
                                                    ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Customer Record'><img src={3} width='20' class='middle'/></a>", 
                                                                            ResolveUrl(CustomerView.PageAddress),
                                                                            WebApplicationConstants.TransferNumber, 
                                                                            hidCustomerId.Value.UrlTextEncrypt(),
                                                                            ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                    : string.Empty %>
                                        </label>
                                        <eShip:CustomHiddenField runat="server" ID="hidCustomerId" />
                                        <eShip:CustomTextBox runat="server" ID="txtCustomerNumber" CssClass="w110" OnTextChanged="OnCustomerNumberChanged"
                                            AutoPostBack="True" />
                                        <asp:RequiredFieldValidator runat="server" ID="rfvCustomer" ControlToValidate="txtCustomerNumber"
                                            ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                        <asp:ImageButton ID="btnFindCustomer" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                                            CausesValidation="false" OnClick="OnFindCustomerClicked" />
                                        <eShip:CustomTextBox runat="server" ID="txtCustomerName" CssClass="w280 disabled" ReadOnly="True" />
                                        <ajax:AutoCompleteExtender runat="server" ID="aceCustomer" TargetControlID="txtCustomerNumber" ServiceMethod="GetCustomerList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                            EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div class="rowgroup">
                            <h5>Reseller Markups</h5>
                            <table class="line">
                                <tr>
                                    <th>
                                        <h5 class="dark">Freight</h5>
                                    </th>
                                    <th>
                                        <h5 class="dark">Fuel</h5>
                                    </th>
                                    <th>
                                        <h5 class="dark">Accessorial</h5>
                                    </th>
                                    <th>
                                        <h5 class="dark">
                                            <abbr title="Miscellaneous">Misc.</abbr>
                                            Service</h5>
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="row">
                                            <div class="fieldgroup">
                                                <label class="wlabel">Type</label>
                                                <asp:DropDownList DataTextField="Value" DataValueField="Key" ID="ddlLineHaulType" runat="server" CssClass="w110" />
                                            </div>
                                            <div class="fieldgroup">
                                                <label class="wlabel">Value ($)</label>
                                                <eShip:CustomTextBox ID="txtLineHaulValue" CssClass="w110" runat="server" MaxLength="9" />
                                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtLineHaulValue"
                                                    FilterType="Custom, Numbers" ValidChars="." />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="fieldgroup">
                                                <label class="wlabel">Percent (%)</label>
                                                <eShip:CustomTextBox ID="txtLineHaulPercentage" CssClass="w110" runat="server" MaxLength="9" />
                                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtLineHaulPercentage"
                                                    FilterType="Custom, Numbers" ValidChars="." />
                                            </div>
                                            <div class="fieldgroup_s">
                                                <asp:CheckBox ID="chkUseLineHaulMin" runat="server" CssClass="jQueryUniform" />
                                                <label>Use Lower</label>
                                            </div>
                                        </div>
                                        <div class="row pb40">&nbsp;</div>
                                    </td>
                                    <td>
                                        <div class="row">
                                            <div class="fieldgroup">
                                                <label class="wlabel">Type</label>
                                                <asp:DropDownList DataTextField="Value" DataValueField="Key" ID="ddlFuelType" runat="server"
                                                    CssClass="w110" />
                                            </div>
                                            <div class="fieldgroup">
                                                <label class="wlabel">Value ($)</label>
                                                <eShip:CustomTextBox ID="txtFuelValue" CssClass="w110" runat="server" MaxLength="9" />
                                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtFuelValue"
                                                    FilterType="Custom, Numbers" ValidChars="." />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="fieldgroup">
                                                <label class="wlabel">Percent (%)</label>
                                                <eShip:CustomTextBox ID="txtFuelPercentage" CssClass="w110" runat="server" MaxLength="9" />
                                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtFuelPercentage"
                                                    FilterType="Custom, Numbers" ValidChars="." />
                                            </div>
                                            <div class="fieldgroup_s">
                                                <asp:CheckBox ID="chkUseFuelMin" CssClass="jQueryUniform" runat="server" />
                                                <label>Use Lower</label>
                                            </div>
                                        </div>
                                        <div class="row pb40">&nbsp;</div>
                                    </td>
                                    <td>
                                        <div class="row">
                                            <div class="fieldgroup">
                                                <label class="wlabel">Type</label>
                                                <asp:DropDownList DataTextField="Value" DataValueField="Key" ID="ddlAccessorialType"
                                                    runat="server" CssClass="w110" />
                                            </div>
                                            <div class="fieldgroup">
                                                <label class="wlabel">Value ($)</label>
                                                <eShip:CustomTextBox ID="txtAccessorialValue" CssClass="w110" runat="server" MaxLength="9" />
                                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtAccessorialValue"
                                                    FilterType="Custom, Numbers" ValidChars="." />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="fieldgroup">
                                                <label class="wlabel">Percent (%)</label>
                                                <eShip:CustomTextBox ID="txtAcessorialPercentage" CssClass="w110" runat="server" MaxLength="9" />
                                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="txtAcessorialPercentage"
                                                    FilterType="Custom, Numbers" ValidChars="." />
                                            </div>
                                            <div class="fieldgroup_s">
                                                <asp:CheckBox ID="chkUseAccessorialMin" CssClass="jQueryUniform" runat="server" />
                                                <label>Use Lower</label>
                                            </div>
                                        </div>
                                        <div class="row pb40">&nbsp;</div>
                                    </td>
                                    <td>
                                        <div class="row">
                                            <div class="fieldgroup">
                                                <label class="wlabel">Type</label>
                                                <asp:DropDownList DataTextField="Value" DataValueField="Key" ID="ddlServiceType"
                                                    runat="server" CssClass="w110" />
                                            </div>
                                            <div class="fieldgroup">
                                                <label class="wlabel">Value ($)</label>
                                                <eShip:CustomTextBox ID="txtServiceValue" CssClass="w110" runat="server" MaxLength="9" />
                                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtServiceValue"
                                                    FilterType="Custom, Numbers" ValidChars="." />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="fieldgroup">
                                                <label class="wlabel">Percent (%)</label>
                                                <eShip:CustomTextBox ID="txtServicePercentage" CssClass="w110" runat="server" MaxLength="9" />
                                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="txtServicePercentage"
                                                    FilterType="Custom, Numbers" ValidChars="." />
                                            </div>
                                            <div class="fieldgroup_s">
                                                <asp:CheckBox ID="chkUseServiceMin" CssClass="jQueryUniform" runat="server" />
                                                <label>Use Lower</label>
                                            </div>
                                        </div>
                                        <div class="row pb40">&nbsp;</div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabAuditLog" HeaderText="Audit Log">
                <ContentTemplate>
                    <eShip:AuditLogControl ID="auditLogs" runat="server" />
                </ContentTemplate>
            </ajax:TabPanel>
        </ajax:TabContainer>

    </div>
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
</asp:Content>
