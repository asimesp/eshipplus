﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="SmcPointToPointServiceView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Rating.SmcPointToPointServiceView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="False" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="pageHeader">
        <asp:Image runat="server" ID="imgPageImageLogo" ImageUrl="" AlternateText="page icon" CssClass="pageHeaderIcon" />
        <h3>SMC<sup>3</sup> Point to Point Service Check </h3>
    </div>
    <div style="width: 100%; color: red; padding: 3px;">
        <asp:Literal runat="server" ID="litErrorMessages" />
    </div>

    <span class="clearfix"></span>
    <hr class="dark mb10" />

    <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
        <table>
            <tr>
                <td class="text-right">
                    <label class="upper blue">Origin Postal Code: </label>
                </td>
                <td class="text-left">
                    <eShip:CustomTextBox runat="server" ID="txtOriginPostalCode" CssClass="w90" MaxLength="10" />
                    <asp:RequiredFieldValidator runat="server" ID="rfvPostalCode" ControlToValidate="txtOriginPostalCode"
                        ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                </td>
                <td class="text-right">
                    <label class="upper blue">Country: </label>
                </td>
                <td class="text-left">
                    <eShip:CachedObjectDropDownList Type="Countries" ID="ddlOriginCountry" runat="server" 
                                        CssClass="w100" EnableChooseOne="True" DefaultValue="0" />
                </td>
                <td class="text-right">
                    <label class="upper blue">Destination Postal Code: </label>
                </td>
                <td class="text-left">
                    <eShip:CustomTextBox runat="server" ID="txtDestinationPostalCode" CssClass="w90" MaxLength="10" />
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtDestinationPostalCode"
                        ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                </td>
                <td class="text-right">
                    <label class="upper blue">Country: </label>
                </td>
                <td class="text-left">
                    <eShip:CachedObjectDropDownList Type="Countries" ID="ddlDestinationCountry" runat="server" 
                                        CssClass="w100" EnableChooseOne="True" DefaultValue="0" />
                </td>
            </tr>
        </table>
        <hr class="fat" />
        <div class="row mb10">
            <div class="fieldgroup right">
                <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="OnSearchClicked" />
                <asp:Button ID="btnExport" runat="server" Text="Export" OnClick="OnExportClicked" />
            </div>
        </div>
    </asp:Panel>

    <table class="line2 pl2" id="pointToPointTable">
        <tr>
            <th style="width: 35%;">Carrier Name</th>
            <th style="width: 7%;">SCAC</th>
            <th style="width: 8%;">Service</th>
            <th style="width: 20%;">Origin Terminal Code</th>
            <th style="width: 20%;">Destination Terminal Code</th>
            <th style="width: 5%;">Direct Lane</th>
            <th style="width: 5%;">Minimum Days</th>
        </tr>
        <asp:Repeater runat="server" ID="rptAvailableCarriers">
            <ItemTemplate>
                <tr>
                    <td><%# Eval("Service.CarrierName") %></td>
                    <td>
                        <%# Eval("Service.Scac") %>
                    </td>
                    <td><%# Eval("Service.ServiceCode") %></td>
                    <td>
                        <%# Eval("Service.OriginTerminalCode") %>
                        <asp:HyperLink runat="server" NavigateUrl='<%# Eval("TerminalLink") %>' Target="_blank" ToolTip="Go to terminal information">
                            <asp:Image runat="server" ImageUrl="~/images/icons2/arrow_newTab.png" CssClass="pl5" AlternateText="Go to terminal information" Width="16px"/>
                        </asp:HyperLink>
                    </td>
                    <td>
                        <%# Eval("Service.DestinationTerminalCode") %>
                        <asp:HyperLink runat="server" NavigateUrl='<%# Eval("TerminalLink") %>' Target="_blank" ToolTip="Go to terminal information">
                            <asp:Image runat="server" ImageUrl="~/images/icons2/arrow_newTab.png" CssClass="pl5" AlternateText="Go to terminal information" Width="16px" />
                        </asp:HyperLink>
                    </td>
                    <td><%# Eval("Service.Direct").ToBoolean() ? WebApplicationConstants.HtmlCheck : string.Empty %></td>
                    <td><%# Eval("Service.MinimumDays") %></td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>

    </table>
</asp:Content>
