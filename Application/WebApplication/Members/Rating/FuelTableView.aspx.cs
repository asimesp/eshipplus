﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Rating;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Rating
{
    public partial class FuelTableView : MemberPageBase, IFuelTableView
    {
        private const string SaveFlag = "S";

        public static string PageAddress { get { return "~/Members/Rating/FuelTableView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.FuelTable; } }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RatingBlue; }
        }

        public event EventHandler<ViewEventArgs<FuelTable>> Save;
        public event EventHandler<ViewEventArgs<FuelTable>> Delete;
        public event EventHandler<ViewEventArgs<FuelTable>> Lock;
        public event EventHandler<ViewEventArgs<FuelTable>> UnLock;
        public event EventHandler<ViewEventArgs<FuelTable>> LoadAuditLog;

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == SaveFlag)
            {
                // unlock and return to read-only
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<FuelTable>(new FuelTable(hidFuelTableId.Value.ToLong(), false)));
                SetEditStatus(false);
                memberToolBar.ShowUnlock = false;
            }

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;

            litErrorMessages.Text = messages.HasErrors()
                                        ? string.Join(WebApplicationConstants.HtmlBreak,
                                                      messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
                                                        Select(m => m.Message).ToArray())
                                        : string.Empty;
        }

        public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs) { auditLogs.Load(logs); }

        public void FailedLock(Lock @lock)
        {
            memberToolBar.ShowNew = false;
            memberToolBar.ShowSave = false;
            memberToolBar.ShowDelete = false;
            memberToolBar.ShowUnlock = false;

            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void Set(FuelTable fuelTable)
        {
            LoadFuelTable(fuelTable);
            SetEditStatus(!fuelTable.IsNew);
        }

        private List<FuelIndex> UpdatePriceIndicesFromView(FuelTable fuelTable)
        {
            var priceIndices = from item in lstPriceIndices.Items
                               let hidden = item.FindControl("hidPriceIndexId").ToCustomHiddenField()
                               where hidden != null
                               let priceIndexId = hidden.Value.ToLong()
                               select new FuelIndex(priceIndexId, priceIndexId != default(long))
                                   {
                                       LowerBound = item.FindControl("txtLowerBound").ToTextBox().Text.ToDecimal(),
                                       UpperBound = item.FindControl("txtUpperBound").ToTextBox().Text.ToDecimal(),
                                       Surcharge = item.FindControl("txtSurcharge").ToTextBox().Text.ToDecimal(),
                                       Tenant = fuelTable.Tenant,
                                       FuelTable = fuelTable
                                   };
            return priceIndices.ToList();
        }

        private void SetEditStatus(bool enabled)
        {
            pnlFuelTable.Enabled = enabled;
            memberToolBar.EnableImport = enabled;
            memberToolBar.EnableSave = enabled;

            litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;
        }


        private void LoadFuelTable(FuelTable fuelTable)
        {
            if (!fuelTable.IsNew) fuelTable.LoadCollections();

            hidFuelTableId.Value = fuelTable.Id.ToString();

            txtName.Text = fuelTable.Name;

            lstPriceIndices.DataSource = fuelTable.FuelIndices;
            lstPriceIndices.DataBind();

            memberToolBar.ShowUnlock = fuelTable.HasUserLock(ActiveUser, fuelTable.Id);
        }

        private void ProcessTransferredRequest(FuelTable fuelTable)
        {
            if (fuelTable.IsNew) return;

            LoadFuelTable(fuelTable);

            if (LoadAuditLog != null) 
                LoadAuditLog(this, new ViewEventArgs<FuelTable>(fuelTable));
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            new FuelTableHandler(this).Initialize();

            memberToolBar.ShowSave = Access.Modify;
            memberToolBar.ShowDelete = Access.Remove;
            memberToolBar.ShowNew = Access.Modify;
            memberToolBar.ShowEdit = Access.Modify;
            memberToolBar.ShowImport = Access.Modify;

            memberToolBar.ShowExport = Access.Grant;

            fuelTableFinder.OpenForEditEnabled = Access.Modify;

            if (IsPostBack) return;
            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.FuelTablesImportTemplate });
            
            if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.TransferNumber]))
                ProcessTransferredRequest(new FuelTable(Request.QueryString[WebApplicationConstants.TransferNumber].UrlTextDecrypt().ToLong()));

            SetEditStatus(false);
        }


        protected void OnNewClicked(object sender, EventArgs e)
        {
            litErrorMessages.Text = string.Empty;

            LoadFuelTable(new FuelTable());
            DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
            SetEditStatus(true);
        }

        protected void OnEditClicked(object sender, EventArgs e)
        {
            var fuelTable = new FuelTable(hidFuelTableId.Value.ToLong(), false);

            if (Lock == null || fuelTable.IsNew) return;

            SetEditStatus(true);
            memberToolBar.ShowUnlock = true;
            Lock(this, new ViewEventArgs<FuelTable>(fuelTable));

            LoadFuelTable(fuelTable);
        }

        protected void OnUnlockClicked(object sender, EventArgs e)
        {
            var fuelTable = new FuelTable(hidFuelTableId.Value.ToLong(), false);
            
            if (UnLock == null || fuelTable.IsNew) return;
            
            SetEditStatus(false);
            memberToolBar.ShowUnlock = false;
            UnLock(this, new ViewEventArgs<FuelTable>(fuelTable));
        }

        protected void OnSaveClicked(object sender, EventArgs e)
        {
            var fuelTableId = hidFuelTableId.Value.ToLong();
            var fuelTable = new FuelTable(fuelTableId, fuelTableId != default(long));

            if (fuelTable.IsNew)
                fuelTable.TenantId = ActiveUser.TenantId;
            else
                fuelTable.LoadCollections();

            fuelTable.Name = txtName.Text;
            fuelTable.FuelIndices = UpdatePriceIndicesFromView(fuelTable);

            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<FuelTable>(fuelTable));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<FuelTable>(fuelTable));
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var fuelTable = new FuelTable(hidFuelTableId.Value.ToLong(), false);

            if (Delete != null)
                Delete(this, new ViewEventArgs<FuelTable>(fuelTable));

            fuelTableFinder.Reset();
        }

        protected void OnFindClicked(object sender, EventArgs e)
        {
            fuelTableFinder.Visible = true;
        }


        protected void OnExportClicked(object sender, EventArgs e)
        {
            var lines = lstPriceIndices
                .Items
                .Select(item => new[]
                    {
                        item.FindControl("txtLowerBound").ToTextBox().Text,
                        item.FindControl("txtUpperBound").ToTextBox().Text,
                        item.FindControl("txtSurcharge").ToTextBox().Text
                    }.TabJoin())
                .ToList();
            lines.Insert(0, new[] { "Lower Bound Value", "Upper Bound Value", "Surcharge Percent" }.TabJoin());
            Response.Export(lines.ToArray().NewLineJoin());
        }

        protected void OnImportClicked(object sender, EventArgs e)
        {
            fileUploader.Title = "FUEL TABLE IMPORT";
            fileUploader.Instructions = @"**Please refer to user guide for import file format.";
            fileUploader.Visible = true;
        }

        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 3;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var indices = lines
                .Select(s => new FuelIndex
                {
                    LowerBound = s[0].ToDecimal(),
                    UpperBound = s[1].ToDecimal(),
                    Surcharge = s[2].ToDecimal(),
                })
                .ToList();

            var vIndices = lstPriceIndices.Items
                .Select(item => new FuelIndex
                {
                    LowerBound = item.FindControl("txtLowerBound").ToTextBox().Text.ToDecimal(),
                    UpperBound = item.FindControl("txtUpperBound").ToTextBox().Text.ToDecimal(),
                    Surcharge = item.FindControl("txtSurcharge").ToTextBox().Text.ToDecimal(),
                })
                .ToList();

            vIndices.AddRange(indices);

            fileUploader.Visible = false;

            lstPriceIndices.DataSource = vIndices;
            lstPriceIndices.DataBind();
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }


        protected void OnFuelTableItemSelected(object sender, ViewEventArgs<FuelTable> e)
        {
            litErrorMessages.Text = string.Empty;

            if (hidFuelTableId.Value.ToLong() != default(long))
            {
                var oldFuelTable = new FuelTable(hidFuelTableId.Value.ToLong(), false);
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<FuelTable>(oldFuelTable));
            }

            var fuelTable = e.Argument;

            LoadFuelTable(fuelTable);

            fuelTableFinder.Visible = false;

            if (fuelTableFinder.OpenForEdit && Lock != null)
            {
                SetEditStatus(true);
                memberToolBar.ShowUnlock = true;
                Lock(this, new ViewEventArgs<FuelTable>(fuelTable));
            }
            else
            {
                SetEditStatus(false);
            }

            if (LoadAuditLog != null) LoadAuditLog(this, new ViewEventArgs<FuelTable>(fuelTable));
        }

        protected void OnFuelTableSelectionCancel(object sender, EventArgs e)
        {
            fuelTableFinder.Visible = false;
        }


        protected void OnAddFuelIndexClicked(object sender, EventArgs e)
        {
            var vPriceIndices = lstPriceIndices.Items
                .Select(item => new
                {
                    Id = item.FindControl("hidPriceIndexId").ToCustomHiddenField().Value.ToLong(),
                    LowerBound = item.FindControl("txtLowerBound").ToTextBox().Text,
                    UpperBound = item.FindControl("txtUpperBound").ToTextBox().Text,
                    Surcharge = item.FindControl("txtSurcharge").ToTextBox().Text,
                })
                .ToList();

            vPriceIndices.Add(new
            {
                Id = default(long),
                LowerBound = string.Empty,
                UpperBound = string.Empty,
                Surcharge = string.Empty,
            });

            lstPriceIndices.DataSource = vPriceIndices;
            lstPriceIndices.DataBind();
        }

        protected void OnPriceIndexDeleteClicked(object sender, ImageClickEventArgs e)
        {
            var button = (ImageButton)sender;
            var rowItemIndex = button.Parent.FindControl("hidPriceItemIndex").ToCustomHiddenField().Value.ToLong();

            var vPriceIndices = lstPriceIndices.Items
                .Select(item => new
                {
                    Id = item.FindControl("hidPriceIndexId").ToCustomHiddenField().Value.ToLong(),
                    LowerBound = item.FindControl("txtLowerBound").ToTextBox().Text,
                    UpperBound = item.FindControl("txtUpperBound").ToTextBox().Text,
                    Surcharge = item.FindControl("txtSurcharge").ToTextBox().Text,
                    rowItemIndex = item.FindControl("hidPriceItemIndex").ToCustomHiddenField().Value.ToLong()
                })
                .Where(c => c.rowItemIndex != rowItemIndex)
                .ToList();

            lstPriceIndices.DataSource = vPriceIndices;
            lstPriceIndices.DataBind();
        }


        protected void OnClearFuelIndicesClicked(object sender, EventArgs e)
        {
            lstPriceIndices.DataSource = new List<FuelIndex>();
            lstPriceIndices.DataBind();
        }
    }
}
