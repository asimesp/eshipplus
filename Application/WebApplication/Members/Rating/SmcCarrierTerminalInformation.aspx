﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="SmcCarrierTerminalInformation.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Rating.SmcCarrierTerminalInformation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="False" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <div class="pageHeader">
        <asp:Image runat="server" ID="imgPageImageLogo" ImageUrl="" AlternateText="page icon" CssClass="pageHeaderIcon" />
        <h3>SMC<sup>3</sup> Terminal Information </h3>
    </div>
    
    <hr class="dark"/>

   <div>
       <div class="row pt20">
           <h3><asp:Literal runat="server" ID="litCarrierName" /> </h3>
       </div>
       <div class="rowgroup pt10">
        <div class="col_1_2 bbox vlinedarkright pl10">
            <h5>Origin</h5>
            <div class="row">
                <div class="fieldgroup">
                    <label>
                        <asp:Literal runat="server" ID="litOrigin" /></label>
                </div>
            </div>
        </div>
        <div class="col_1_2 bbox pl46">
            <h5>Destination</h5>
            <div class="row">
                <div class="fieldgroup">
                    <label>
                        <asp:Literal runat="server" ID="litDestination" /></label>
                </div>
            </div>
        </div>
    </div>
   </div>

</asp:Content>
