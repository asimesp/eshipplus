﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Rating;
using LogisticsPlus.Eship.Processor.Handlers.Rating;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Rating
{
    public partial class VendorRatingLTLFuelSurchargeView : MemberPageBase, IVendorRatingLTLFuelSurchargeView
    {
        public bool HasAccessToModifyVendorRatingFuelMassUpdateLTL { get; set; }

        public static string PageAddress { get { return "~/Members/Rating/VendorRatingLTLFuelSurchargeView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.VendorRatingFuelMassUpdateLTL; } }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RatingBlue; }
        }

        public Dictionary<int, string> FuelIndexRegions
        {
            get
            {
                return ViewState["VendorRatingLTLFuelSurchargesFuelIndexRegions"] as Dictionary<int, string> ??
                       new Dictionary<int, string>();
            }
            set { ViewState["VendorRatingLTLFuelSurchargesFuelIndexRegions"] = value; }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<DateTime>> Search;
        public event EventHandler<ViewEventArgs<List<VendorRating>>> Save;

        public void DisplayVendorRatingSurcharges(List<VendorRatingLTLFuelSurchargeUpdateDto> surcharges)
        {
            lstSurcharges.DataSource = surcharges.OrderBy(s => s.Name);
            lstSurcharges.DataBind();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }


        private void DoSearch()
        {
            if (Search != null)
                Search(this, new ViewEventArgs<DateTime>(txtSurchargeEffectiveDate.Text.ToDateTime()));

            chkSelectAllRecords.Checked = false;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            new VendorRatingLTLFuelSurchargeHandler(this).Initialize();

            HasAccessToModifyVendorRatingFuelMassUpdateLTL = ActiveUser.HasAccessToModify(ViewCode.VendorRatingFuelMassUpdateLTL);

            if (IsPostBack) return;

            memberToolBar.ShowSave = HasAccessToModifyVendorRatingFuelMassUpdateLTL;
            
            if (Loading != null)
                Loading(this, new EventArgs());

            txtSurchargeEffectiveDate.Text = DateTime.Now.FormattedShortDate();

            DoSearch();
        }

        protected void OnSaveClicked(object sender, EventArgs e)
        {
            var ratings = (from i in lstSurcharges.Items
                           let selected = i.FindControl("chkSelected").ToAltUniformCheckBox().Checked
                           let ratingId = i.FindControl("hidVendorRatingId").ToCustomHiddenField().Value.ToLong()
                           let surcharge = i.FindControl("txtNewSurcharge").ToTextBox().Text.ToDecimal().ToString("f4").ToDecimal()
                           where selected
                           select new VendorRating(ratingId, true) { CurrentLTLFuelMarkup = surcharge })
                .ToList();

            if (Save != null)
                Save(this, new ViewEventArgs<List<VendorRating>>(ratings));

            DoSearch();
        }

        protected void OnSearchClicked(object sender, EventArgs e)
        {
            DoSearch();
        }
    }
}
