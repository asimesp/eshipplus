﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Smc;
using LogisticsPlus.Eship.Smc.Rates;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Rating
{
    public partial class LTLTariffsDetailView : MemberPageBase
    {
        public static string PageAddress
        {
            get { return "~/Members/Rating/LTLTariffsDetailView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.TariffsDetailLTL; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RatingBlue; }
        }


        private List<TariffDetails> GetTariffsDetail()
        {
            var rateware = new Rateware2(RatewareSettings, CarrierConnectSettings);
            var tariffs = rateware.ListAvailableTariffsWithDetails()
                .OrderBy(d => d.ReaderFriendlyTariff)
                .ThenBy(d => d.Description)
                .ToList();
            return tariffs;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!ActiveUser.HasAccessTo(ViewCode.VendorRating, ViewCode.TariffRateAnalysis))
            {
                Response.Redirect(DashboardView.PageAddress);
                return;
            }

            if (IsPostBack) return;
            var tariffs = GetTariffsDetail();
            litRecordCount.Text = tariffs.BuildRecordCount();
            rptTariffs.DataSource = tariffs;
            rptTariffs.DataBind();
        }


        protected void OnToolbarExport(object sender, EventArgs e)
        {
            var header = new[]
			             	{
			             		"Line #", "Friendly Name", "Description", "Tariff Name", "Effective Date", "SMC3 Product Number", "SMC3 Release"
			             	};
            var lines = new List<string> { header.TabJoin() };
            var cnt = 1;
            lines.AddRange(GetTariffsDetail()
                            .Select(d => new[]
			               	             	{
												(cnt++).GetString(),
			               	             		d.ReaderFriendlyTariff, d.Description, d.TariffName, d.EffectiveDate, d.ProductNumber,
			               	             		d.Release
			               	             	}.TabJoin()));
            Response.Export(lines.ToArray().NewLineJoin());
        }
    }
}
