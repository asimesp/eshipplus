﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="SmallPackRateAnalysisView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Rating.SmallPackRateAnalysisView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Administration" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                CssClass="pageHeaderIcon" />
            <h3>Small Package Rate Analysis
            <small class="ml10">
                <asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>

        <script type="text/javascript">
            $(document).ready(function () {

                $("div[id$='instructionDiv']").hide();
                $("div[id$='pnlDimScreenJS']").hide();

                $(jsHelper.AddHashTag("importantInformation")).click(function (e) {
                    e.preventDefault();
                    $("div[id$='instructionDiv']").show("slow");
                    $("div[id$='pnlDimScreenJS']").show();
                });

                $(jsHelper.AddHashTag("instructionDiv")).click(function () {
                    $(this).hide("slow", function () { $("div[id$='pnlDimScreenJS']").hide(); });
                });

            });
            
            function CheckFileSize(fileControl) {
                if (fileControl.files.length == 0) return;

                var maxSize = '<%= WebApplicationSettings.AnalysisMaximumFileUploadSize %>';
                var mb = '<%= WebApplicationConstants.MegaByte %>';

                if (fileControl.files[0].size <= maxSize) return;

                // alert and disable upload button
                alert(fileControl.files[0].name + " is too large. Maximum file size is: " + maxSize + " bytes [or " + maxSize / mb + " MB]");
                fileControl.value = jsHelper.EmptyString;
            }
        </script>
        <span class="clearfix"></span>
        <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />

        <div class="imp_note mb10">
            <label class="note">
                &#8226; Enter all required parameters then submit analysis. <span class="red">*</span> = required. 
                    <a href="#" id="importantInformation" class="blue">Click here</a> for lane input file specifications. 
                <span style="color: red;">+</span> = Analysis Set being processed.
            </label>
        </div>
        <div class="rowgroup">
            <div class="col_1_2 bbox vlinedarkright">
                <h5>Submission Settings</h5>
                <div class="row">
                    <div class="fieldgroup">
                        <label class="wlabel">Name</label>
                        <eShip:CustomTextBox runat="server" ID="txtName" CssClass="w200" MaxLength="50" />
                    </div>
                    <div class="fieldgroup">
                        <label class="wlabel">Analysis Effective Date</label>
                        <eShip:CustomTextBox runat="server" ID="txtAnalysisEffectiveDate" CssClass="w100" Type="Date" placeholder="99/99/9999"/>
                    </div>
                </div>
                <div class="row">
                    <div class="fieldgroup">
                        <label class="wlabel">Lanes Input File</label>
                        <asp:FileUpload runat="server" ID="fupFileUpload" CssClass="jQueryUniform" onchange="CheckFileSize(this);"/>
                    </div>
                </div>
            </div>
            <div class="col_1_2 bbox pl46">
                <h5>Small Package Settings</h5>
                <div class="row">
                    <div class="fieldgroup">
                         <label class="wlabel">Small Package Engine</label>
                        <asp:DropDownList runat="server" ID="ddlSmallPackageEngine" DataTextField="Text" CssClass="w200"
                                DataValueField="Value" AutoPostBack="True" OnSelectedIndexChanged="OnSmallPackageEngineSelectedIndexChanged" />
                    </div>
                    <div class="fieldgroup">
                        <label class="wlabel">Service Type</label>
                        <asp:DropDownList runat="server" ID="ddlEngineServiceType" CssClass="w200" />
                    </div>
                </div>
                <div class="row">
                    <div class="fieldgroup">
                        <label class="wlabel">Package Type</label>
                        <eShip:CachedObjectDropDownList Type="PackageType" ID="ddlPackageTypes" runat="server" CssClass="w200" EnableChooseOne="True" DefaultValue="0" />
                    </div>
                </div>
            </div>
        </div>
        <hr class="dark" />
        <div class="row mb20">
            <div class="row mb10">
                <asp:Button runat="server" ID="btnRefreshSetList" Text="Refresh Analysis Sets" CausesValidation="False" CssClass="mr10"
                    OnClick="OnRefreshAnalysisSetList" />
                <asp:Button runat="server" ID="btnSubmit" Text="Submit For Analysis" OnClick="OnSubmitBatchRateDataForAnalysis" />
            </div>
        </div>
        <hr class="fat" />
        <h5>Small Pack Rate Analysis Sets</h5>
        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheSmallPackRateAnalysisSetsTable" TableId="smallPackRateAnalysisSetsTable" HeaderZIndex="2" />
        <table class="line2 pl2" id="smallPackRateAnalysisSetsTable">
            <tr>
                <th style="width: 15%">Name
                </th>
                <th style="width: 10%">Engine
                </th>
                <th style="width: 10%">Service Type
                </th>
                <th style="width: 10%">Package Type
                </th>
                <th style="width: 8%">Analysis Effective Date
                </th>
                <th style="width: 8%">Date Submitted
                </th>
                <th style="width: 8%">Date Completed
                </th>
                <th style="width: 18%">Submitted By
                </th>
                <th style="width: 13%;" class="text-center">Action
                </th>
            </tr>
            <asp:Repeater runat="server" ID="rptRateAnalysisSets" OnItemDataBound="OnRateAnalysisSetsItemDataBound">
                <ItemTemplate>
                    <tr>
                        <td>
                            <eShip:CustomHiddenField runat="server" ID="hidBatchRateDataId" Value='<%# Eval("Id") %>' />
                            <%# Eval("Name") %> <span class="red"><asp:Literal runat="server" ID="litProcessing" Text="+" Visible="False" /></span>
                        </td>
                        <td>
                            <%# Eval("Engine")%>
                        </td>
                        <td>
                            <%# Eval("ServiceType")%>
                        </td>
                        <td>
                            <%# Eval("PackageType")%>
                        </td>
                        <td>
                            <%# Eval("AnalysisEffectiveDate") %>
                        </td>
                        <td>
                            <%# Eval("DateSubmitted") %>
                        </td>
                        <td>
                            <%# Eval("DateCompleted") %>
                        </td>
                        <td>
                            <%# ActiveUser.HasAccessTo(ViewCode.User) 
                                                ? string.Format("{0} <a href='{1}?{2}={3}' class='blue' target='_blank' title='Go To User Record'><img src={4} width='16' class='middle'/></a>", 
                                                    Eval("SubmittedBy"),
                                                    ResolveUrl(UserView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    Eval("SubmittedByUserId").GetString().UrlTextEncrypt(),
                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))  
                                                : Eval("SubmittedBy") %>
                        </td>
                        <td style="text-align: left; padding-left: 1px;">
                            <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                ToolTip="Delete Entry" CausesValidation="false" OnClick="OnBatchRateDataDelete"
                                Enabled='<%# Access.Remove %>' />
                            <asp:ImageButton ID="ibtnEnqueue" runat="server" ImageUrl="~/images/icons2/arrow_general.png"
                                ToolTip="Queue for immediate processing" CausesValidation="false"
                                OnClick="OnBatchRateDataEnqueue" Visible='<%# Access.Modify %>' />
                            <asp:ImageButton ID="ibtnDownloadInput" runat="server" ImageUrl="~/images/icons2/document.png"
                                CausesValidation="false" OnClick="OnBatchRateDataInputDownload"
                                ToolTip="Download Input Set" />
                            <asp:ImageButton ID="ibtnDownloadOutput" runat="server" ImageUrl="~/images/icons2/exportBlue.png"
                                CausesValidation="false" OnClick="OnBatchRateDataOutputDownload"
                                ToolTip="Download Result Set" Visible='<%# !string.IsNullOrEmpty(Eval("DateCompleted").GetString())  %>' />
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
        <div id="instructionDiv" style="display: none;" class="popupControl">
            <div class="popheader">
                <h4>Instructions</h4>
            </div>
            <div class="rowgroup pl20">
                <div class="row">
                    <div class="fieldgroup">
                        <h5>Lane Input File Specification</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="fieldgroup">
                        <label class=" note">Prepare a tab or comma delimited file with the structure defined below.</label>
                    </div>
                </div>
                <div class="row">
                    <div class="fieldgroup">
                        <label class=" note">
                            NOTE: Header row must be present. Detail lines must not exceed
                        <asp:Literal runat="server" ID="litMaxLineCount" />.
                        </label>
                    </div>
                </div>
            </div>
            <table class="norm">
                <tr>
                    <td>
                        <label class="lhInherit">Line ID</label>
                    </td>
                    <td>
                        <label class="lhInherit">Origin Postal Code</label>
                    </td>
                    <td>
                        <label class="lhInherit">Origin Country</label>
                    </td>
                    <td>
                        <label class="lhInherit">Destination Postal Code</label>
                    </td>
                    <td>
                        <label class="lhInherit">Destination Country</label>
                    </td>
                    <td>
                        <label class="lhInherit">Weight (lb)</label>
                    </td>
                    <td>
                        <label class="lhInherit">Length (in)</label>
                    </td>
                    <td>
                        <label class="lhInherit">Width (in)</label>
                    </td>
                    <td>
                        <label class="lhInherit">Height (in)</label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="lhInherit">
                            User defined
                            <br />
                            line identifier</label>
                    </td>
                    <td class="top">
                        <label class="lhInherit">12345</label>
                    </td>
                    <td class="top">
                        <label class="lhInherit">
                            Country IATA code e.g.
                            <br />
                            US = USA, CA = Canada, MX = mexico</label>
                    </td>
                    <td class="top">
                        <label class="lhInherit">16501</label>
                    </td>
                    <td class="top">
                        <label class="lhInherit">
                            Country IATA code e.g.
                            <br />
                            US = USA, CA = Canada, MX = mexico</label>
                    </td>
                    <td class="top">
                        <label class="lhInherit">1000</label>
                    </td>
                    <td class="top"><label class="lhInherit">48</label>
                    </td>
                    <td class="top"><label class="lhInherit">48</label>
                    </td>
                    <td class="top"><label class="lhInherit">48</label>
                    </td>
                </tr>
            </table>
            <div class="row pl20 pt10 mb10">
                <div class="fieldgroup">
                    <label class=" note">*** click anywhere in this box to close pop-up.</label>
                </div>
            </div>
        </div>
    </div>
    <asp:Panel ID="pnlDimScreenJS" CssClass="dimBackground" runat="server" Style="display: none;" />
</asp:Content>
