﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Rating;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Rating;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Rating;
using LogisticsPlus.Eship.WebApplication.Members.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;
using static System.String;

namespace LogisticsPlus.Eship.WebApplication.Members.Rating
{
	public partial class VendorRatingView : MemberPageBaseWithPageStore, IVendorRatingView
	{
		private const string LtlPcfToFcConvTabHeader = "PCF to FC Conv.";
		private const string LtlDiscountTiersHeader = "Disc. Tiers";
		private const string LtlAccessorialsHeader = "Accessorials";
		private const string LtlAdditionalChargesHeader = "Add'l Chg.";
		private const string LtlCfCapacityRulesHeader = "CFC Rules";
		private const string LtlOverLengthRulesHeader = "Over. Rules";
		private const string LtlPackageSpecificRateTabHeader = "Pkg Spec. Rates";
	    private const string LtlGuaranteedChargesTabHeader = "Guar. Chg.";

		private const string SaveFlag = "S";
		private const string MatchingDiscountTiers = "DiscountTiers";

		private const string DiscountTierStoreKey = "DTSK";
		private const string AccessorialsStoreKey = "ASK";
		private const string AdditionalChargesStoreKey = "ACSK";
		private const string AdditionalChargeEditStoreKey = "ACESK";
		private const string CfcRulesStoreKey = "CFCRSK";
		private const string OverLengthRulesStoreKey = "OLRSK";
		private const string PcfToFcConversionsStoreKey = "PTFCSK";
		private const string PackageSpecificRateStoreKey = "PSRSK";
	    private const string GuaranteedChargesStoreKey = "GCSK";
        protected const string AdditionalChargeIndicesStoreKey = "AdditionalChargeIndicesStoreKey";

		private const string VendorratingregionsKey = "VendorRatingRegions";


		private List<DiscountTier> StoredDiscountTiers
		{
			get { return PageStore[DiscountTierStoreKey] as List<DiscountTier> ?? new List<DiscountTier>(); }
			set { PageStore[DiscountTierStoreKey] = value; }
		}

		private List<LTLAccessorial> StoredAccessorials
		{
			get { return PageStore[AccessorialsStoreKey] as List<LTLAccessorial> ?? new List<LTLAccessorial>(); }
			set { PageStore[AccessorialsStoreKey] = value; }
		}

		private List<LTLAdditionalCharge> StoredAddlCharges
		{
			get { return PageStore[AdditionalChargesStoreKey] as List<LTLAdditionalCharge> ?? new List<LTLAdditionalCharge>(); }
			set { PageStore[AdditionalChargesStoreKey] = value; }
		}

		private LTLAdditionalCharge StoredAddlChargeEdit
		{
			get { return PageStore[AdditionalChargeEditStoreKey] as LTLAdditionalCharge ?? new LTLAdditionalCharge(); }
			set { PageStore[AdditionalChargeEditStoreKey] = value; }
		}

		private List<LTLCubicFootCapacityRule> StoredCfcRules
		{
			get { return PageStore[CfcRulesStoreKey] as List<LTLCubicFootCapacityRule> ?? new List<LTLCubicFootCapacityRule>(); }
			set { PageStore[CfcRulesStoreKey] = value; }
		}

		private List<LTLOverLengthRule> StoredOverlengthRules
		{
			get { return PageStore[OverLengthRulesStoreKey] as List<LTLOverLengthRule> ?? new List<LTLOverLengthRule>(); }
			set { PageStore[OverLengthRulesStoreKey] = value; }
		}

		private List<LTLPcfToFcConversion> StoredPcfToFcConversions
		{
			get { return PageStore[PcfToFcConversionsStoreKey] as List<LTLPcfToFcConversion> ?? new List<LTLPcfToFcConversion>(); }
			set { PageStore[PcfToFcConversionsStoreKey] = value; }
		}

		private List<LTLPackageSpecificRate> StoredPackageSpecificRates
		{
			get { return PageStore[PackageSpecificRateStoreKey] as List<LTLPackageSpecificRate> ?? new List<LTLPackageSpecificRate>(); }
			set { PageStore[PackageSpecificRateStoreKey] = value; }
		}

		private List<LTLGuaranteedCharge> StoredGuaranteedCharges
		{
			get { return PageStore[GuaranteedChargesStoreKey] as List<LTLGuaranteedCharge> ?? new List<LTLGuaranteedCharge>(); }
			set { PageStore[GuaranteedChargesStoreKey] = value; }
		}



		public static string PageAddress { get { return "~/Members/Rating/VendorRatingView.aspx"; } }

		public override ViewCode PageCode { get { return ViewCode.VendorRating; } }

		public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.RatingBlue; } }

		protected List<ViewListItem> RegionCollection
		{
			get { return ViewState[VendorratingregionsKey] as List<ViewListItem> ?? new List<ViewListItem>(); }
		}

		public List<FuelTable> FuelTables
		{
			set
			{
				var tables = value.Select(t => new ViewListItem(t.Name, t.Id.ToString())).ToList();
				tables.Insert(0, new ViewListItem(WebApplicationConstants.NotApplicable, 0.ToString()));

				ddlFuelTable.DataSource = tables;
				ddlFuelTable.DataBind();
			}
		}

		public List<Region> Regions
		{
			set
			{
				var regions = value.Select(r => new ViewListItem(r.Name, r.Id.ToString())).ToList();
				regions.Insert(0, new ViewListItem(WebApplicationConstants.NotApplicableShort, 0.ToString()));

				ViewState[VendorratingregionsKey] = regions;

				ddlDiscountTierOriginRegion.DataSource = regions;
				ddlDiscountTierOriginRegion.DataBind();

				ddlDiscountTierDestinationRegion.DataSource = regions;
				ddlDiscountTierDestinationRegion.DataBind();

				ddlLTLPackageSpecificRateDestinationRegion.DataSource = regions;
				ddlLTLPackageSpecificRateDestinationRegion.DataBind();

				ddlLTLPackageSpecificRateOriginRegion.DataSource = regions;
				ddlLTLPackageSpecificRateOriginRegion.DataBind();
			}
		}

		public Dictionary<string, string> Smc3ServiceLevels
		{
			set
			{
				var levels = value.Select(i => new ListItem(i.Value, i.Key)).OrderBy(i=>i.Text).ToList();
				levels.Insert(0, new ListItem(WebApplicationConstants.NotApplicable, string.Empty));
				ddlSmc3ServiceLevel.DataSource = levels;
				ddlSmc3ServiceLevel.DataBind();
			}
		}

		public Dictionary<string, string> LTLTariffs
		{
			set
			{
				var tariffs = value.Select(t => new ViewListItem(t.Value, t.Key)).ToList();
				tariffs.Insert(0, new ViewListItem(WebApplicationConstants.NotApplicable, string.Empty));
				ddlLTLTariff.DataSource = tariffs;
				ddlLTLTariff.DataBind();
			}
		}

		public Dictionary<int, string> FuelIndexRegions
		{
			set
			{
				ddlFuelIndexRegion.DataSource = value;
				ddlFuelIndexRegion.DataBind();
			}
		}

		public Dictionary<int, string> DiscountTierRateBasis
		{
			set
			{
				ddlDiscountTierRateBasis.DataSource = value;
				ddlDiscountTierRateBasis.DataBind();
			}
		}

		public Dictionary<int, string> LTLAccessorialsRateTypes
		{
			set
			{
				ddlLTLAccessorialRateType.DataSource = value;
				ddlLTLAccessorialRateType.DataBind();
			}
		}

		public Dictionary<int, string> LTLAdditionalChargeIndexRateTypes
		{
			get
			{
				return ViewState["VendorRatingsAdditionalChargeIndexRateTypes"] as Dictionary<int, string> ??
					   new Dictionary<int, string>();
			}
			set { ViewState["VendorRatingsAdditionalChargeIndexRateTypes"] = value; }
		}

	    public Dictionary<int, string> LTLGuaranteedChargeRateTypes
	    {
	        set
	        {
                ddlGuarateedChargeRateType.DataSource = value;
	            ddlGuarateedChargeRateType.DataBind();
	        }
	    } 

		public Dictionary<int, string> DaysOfWeek
		{
			set
			{
				ddlFuelUpdatesOn.DataSource = value;
				ddlFuelUpdatesOn.DataBind();
			}
		}

		public Dictionary<int, string> WeightBreaks
		{
			set
			{
				ddlDiscountTierWeightBreak.DataSource = value;
				ddlDiscountTierWeightBreak.DataBind();
			}
		}

		public event EventHandler Loading;
		public event EventHandler<ViewEventArgs<VendorRating>> Save;
		public event EventHandler<ViewEventArgs<VendorRating>> Delete;
		public event EventHandler<ViewEventArgs<VendorRating>> Lock;
		public event EventHandler<ViewEventArgs<VendorRating>> UnLock;
		public event EventHandler<ViewEventArgs<VendorRating>> LoadAuditLog;
		public event EventHandler<ViewEventArgs<string>> VendorSearch;

		public void DisplayVendor(Vendor vendor)
		{
			txtVendorNumber.Text = vendor.VendorNumber;
			txtVendorName.Text = vendor.Name;
			hidVendorId.Value = vendor.Id.ToString();

            memberToolBar.LoadAdditionalActions(ToolbarMoreActions());
        }

		public void LogException(Exception ex)
		{
			if (ex != null) ErrorLogger.LogError(ex, Context);
		}

		public void DisplayMessages(IEnumerable<ValidationMessage> messages)
		{
			if (messages == null) return;
			if (!messages.Any()) return;

			if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == SaveFlag)
			{
				// unlock and return to read-only
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<VendorRating>(new VendorRating(hidVendorRatingId.Value.ToLong(), false)));
				SetEditStatus(false);
				memberToolBar.ShowUnlock = false;
			}

			messageBox.Icon = messages.GenerateIcon();
			messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
			messageBox.Visible = true;

			litErrorMessages.Text = messages.HasErrors()
										? string.Join(WebApplicationConstants.HtmlBreak,
													  messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
														Select(m => m.Message).ToArray())
										: string.Empty;
		}

		public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs)
		{
			auditLogs.Load(logs);
		}

		public void FailedLock(Lock @lock)
		{
			memberToolBar.ShowNew = false;
			memberToolBar.ShowSave = false;
			memberToolBar.ShowDelete = false;
			memberToolBar.ShowUnlock = false;

			SetEditStatus(false);

			litMessage.Text = @lock.BuildFailedLockMessage();
		}

		public void Set(VendorRating vendorRating)
		{
			LoadVendorRating(vendorRating);
			SetEditStatus(!vendorRating.IsNew);
		}


		private List<ToolbarMoreAction> ToolbarMoreActions()
		{
			var matchingDiscountTiers = new ToolbarMoreAction
			{
				CommandArgs = MatchingDiscountTiers,
				ConfirmCommand = false,
				ImageUrl = IconLinks.Rating,
				Name = "Find Matching Discount Tiers",
				IsLink = false,
				NavigationUrl = string.Empty
			};

			var seperator = new ToolbarMoreAction { IsSeperator = true };

			var tariffDetails = new ToolbarMoreAction
			{
				CommandArgs = string.Empty,
				ImageUrl = IconLinks.Rating,
				Name = ViewCode.TariffsDetailLTL.FormattedString(),
				IsLink = true,
				OpenInNewWindow = true,
				NavigationUrl = LTLTariffsDetailView.PageAddress
			};
			var goToVendor = new ToolbarMoreAction
			{
				ConfirmCommand = false,
				ImageUrl = IconLinks.Finance,
				Name = "Go To Vendor",
				IsLink = true,
				NavigationUrl = string.Format("{0}?{1}={2}", VendorView.PageAddress, WebApplicationConstants.TransferNumber, hidVendorId.Value.UrlTextEncrypt()),
                OpenInNewWindow = true
			};
			var moreActions = new List<ToolbarMoreAction> { matchingDiscountTiers, seperator, tariffDetails };

			if (hidVendorId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.Vendor))
				moreActions.AddRange(new List<ToolbarMoreAction> { seperator, goToVendor });

			return moreActions;
		}

		private void SetEditStatus(bool enabled)
		{
			pnlDetails.Enabled = enabled;
			pnlLTLPcfToFcConvTab.Enabled = enabled && !chkProject44Profile.Checked;
			pnlLTLDiscountTiers.Enabled = enabled && !chkProject44Profile.Checked;
			pnlLTLAccessorials.Enabled = enabled && !chkProject44Profile.Checked;
			pnlLTLCubicFootCapacityRules.Enabled = enabled && !chkProject44Profile.Checked;
			pnlLTLOverlengthRules.Enabled = enabled && !chkProject44Profile.Checked;
			pnlLTLAdditionalCharges.Enabled = enabled && !chkProject44Profile.Checked;
			pnlLTLPackageSpecificRatesTab.Enabled = enabled && !chkProject44Profile.Checked;
		    pnlLTLGuaranteedCharges.Enabled = enabled && !chkProject44Profile.Checked;

			memberToolBar.EnableSave = enabled;
			memberToolBar.EnableImport = enabled;
			vendorRatingImportExport.EnableImport = enabled;

			litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;

			// must do this here to load correct set of extensible actions
			memberToolBar.LoadAdditionalActions(ToolbarMoreActions());
		}

		private void BindFreightClassAndFAKSources()
		{
			ddlDiscountTierFak50.DataSource = WebApplicationSettings.FreightClasses;
			ddlDiscountTierFak50.DataBind();
			ddlDiscountTierFak55.DataSource = WebApplicationSettings.FreightClasses;
			ddlDiscountTierFak55.DataBind();
			ddlDiscountTierFak60.DataSource = WebApplicationSettings.FreightClasses;
			ddlDiscountTierFak60.DataBind();
			ddlDiscountTierFak65.DataSource = WebApplicationSettings.FreightClasses;
			ddlDiscountTierFak65.DataBind();
			ddlDiscountTierFak70.DataSource = WebApplicationSettings.FreightClasses;
			ddlDiscountTierFak70.DataBind();
			ddlDiscountTierFak775.DataSource = WebApplicationSettings.FreightClasses;
			ddlDiscountTierFak775.DataBind();
			ddlDiscountTierFak85.DataSource = WebApplicationSettings.FreightClasses;
			ddlDiscountTierFak85.DataBind();
			ddlDiscountTierFak925.DataSource = WebApplicationSettings.FreightClasses;
			ddlDiscountTierFak925.DataBind();
			ddlDiscountTierFak100.DataSource = WebApplicationSettings.FreightClasses;
			ddlDiscountTierFak100.DataBind();
			ddlDiscountTierFak110.DataSource = WebApplicationSettings.FreightClasses;
			ddlDiscountTierFak110.DataBind();
			ddlDiscountTierFak125.DataSource = WebApplicationSettings.FreightClasses;
			ddlDiscountTierFak125.DataBind();
			ddlDiscountTierFak150.DataSource = WebApplicationSettings.FreightClasses;
			ddlDiscountTierFak150.DataBind();
			ddlDiscountTierFak175.DataSource = WebApplicationSettings.FreightClasses;
			ddlDiscountTierFak175.DataBind();
			ddlDiscountTierFak200.DataSource = WebApplicationSettings.FreightClasses;
			ddlDiscountTierFak200.DataBind();
			ddlDiscountTierFak250.DataSource = WebApplicationSettings.FreightClasses;
			ddlDiscountTierFak250.DataBind();
			ddlDiscountTierFak300.DataSource = WebApplicationSettings.FreightClasses;
			ddlDiscountTierFak300.DataBind();
			ddlDiscountTierFak400.DataSource = WebApplicationSettings.FreightClasses;
			ddlDiscountTierFak400.DataBind();
			ddlDiscountTierFak500.DataSource = WebApplicationSettings.FreightClasses;
			ddlDiscountTierFak500.DataBind();

			var faks = WebApplicationSettings.FreightClasses
				.Select(f => new ViewListItem { Text = f.ToString(), Value = f.ToString() })
				.ToList();
			faks.Insert(0, new ViewListItem { Text = WebApplicationConstants.NotApplicableShort, Value = 0.ToString() });

			ddlLTLCFCAppliedFreightClass.DataSource = faks;
			ddlLTLCFCAppliedFreightClass.DataBind();
		}



		private static List<DiscountTier> SortLTLDiscountTiers(IEnumerable<DiscountTier> tiers)
		{
			return tiers
				.OrderBy(t => t.TierPriority)
				.ThenByDescending(t => t.EffectiveDate)
				.ToList();
		}

		private List<LTLAccessorial> SortLTLAccessorials(IEnumerable<LTLAccessorial> accessorials)
		{
			return accessorials
				.OrderBy(a => new Service(a.ServiceId).Description)
				.ToList();
		}

		private static List<LTLCubicFootCapacityRule> SortLtlcfCapacityRules(IEnumerable<LTLCubicFootCapacityRule> cfcRules)
		{
			return cfcRules
				.OrderBy(r => r.LowerBound)
				.ThenBy(r => r.UpperBound)
				.ToList();
		}

		private static List<LTLAdditionalCharge> SortLTLAdditionalCharge(IEnumerable<LTLAdditionalCharge> addlCharges)
		{
			return addlCharges.OrderBy(ac => ac.Name).ToList();
		}

		private static List<AdditionalChargeIndex> SortLTLAdditionalChargeIndices(IEnumerable<AdditionalChargeIndex> addlChargeIndices)
		{
			return addlChargeIndices.OrderBy(aci => aci.UsePostalCode).ThenBy(aci => aci.Region != null ? aci.Region.Name : aci.PostalCode.GetString()).ToList();
		}

		private static List<LTLOverLengthRule> SortLTLOverlengthRules(IEnumerable<LTLOverLengthRule> overlengthRules)
		{
			return overlengthRules
				.OrderBy(r => r.LowerLengthBound)
				.ThenBy(r => r.UpperLengthBound)
				.ToList();
		}

		private static List<LTLPcfToFcConversion> SortLTLPcfToFcConversions(IEnumerable<LTLPcfToFcConversion> conversions)
		{
			return conversions.OrderByDescending(t => t.EffectiveDate).ToList();
		}

		private static List<LTLPackageSpecificRate> SortLTLPackageSpecificRates(IEnumerable<LTLPackageSpecificRate> rates)
		{
			return rates.OrderBy(p => p.RatePriority).ThenByDescending(p => p.EffectiveDate).ThenBy(p => p.PackageQuantity).ToList();
		}

        private static List<LTLGuaranteedCharge> SortLTLGuaranteedCharges(IEnumerable<LTLGuaranteedCharge> charges)
        {
            return charges.OrderByDescending(p => p.EffectiveDate).ThenBy(c => c.Time).ToList();
        } 

		private void LoadVendorRating(VendorRating vendorRating)
		{
			if (!vendorRating.IsNew) vendorRating.LoadCollections();

			hidVendorRatingId.Value = vendorRating.Id.ToString();
			vendorRatingImportExport.VendorRatingId = vendorRating.Id;
			vendorRatingMatchingRegionSearch.VendorRatingId = vendorRating.Id;

			//details
			txtName.Text = vendorRating.Name;
			txtDisplayName.Text = vendorRating.DisplayName;
			chkActive.Checked = vendorRating.Active;
			txtExpirationDate.Text = vendorRating.IsNew ? DateTime.Now.FormattedShortDate() : vendorRating.ExpirationDate.FormattedShortDateSuppressEarliestDate();
			txtVendorName.Text = vendorRating.Vendor == null ? string.Empty : vendorRating.Vendor.Name;
			txtVendorNumber.Text = vendorRating.Vendor == null ? string.Empty : vendorRating.Vendor.VendorNumber;
			hidVendorId.Value = vendorRating.Vendor == null ? string.Empty : vendorRating.Vendor.Id.ToString();
			ddlFuelTable.SelectedValue = vendorRating.FuelTableId.ToString();
		    hypFuelTable.Visible = vendorRating.FuelTableId != default(long) && ActiveUser.HasAccessTo(ViewCode.FuelTable);
            hypFuelTable.HRef = string.Format("{0}?{1}={2}", FuelTableView.PageAddress, WebApplicationConstants.TransferNumber, ddlFuelTable.SelectedValue.UrlTextEncrypt());
			ddlFuelIndexRegion.SelectedValue = vendorRating.FuelIndexRegion.ToInt().ToString();
			txtCurrentLTLFuelMarkUp.Text = vendorRating.CurrentLTLFuelMarkup.ToString();
			txtFuelMarkUpCeiling.Text = vendorRating.FuelMarkupCeiling.ToString();
			txtFuelMarkUpFloor.Text = vendorRating.FuelMarkupFloor.ToString();
            ddlFuelChargeCode.SelectedValue = vendorRating.FuelChargeCodeId.GetString();
			ddlFuelUpdatesOn.SelectedValue = vendorRating.FuelUpdatesOn.ToInt().ToString();
            ddlPackageSpecificRatePackageType.SelectedValue = vendorRating.LTLPackageSpecificRatePackageTypeId.GetString();
            ddlLTLPackageSpecificRateFreightChargeCode.SelectedValue = vendorRating.LTLPackageSpecificFreightChargeCodeId.GetString();
			txtCubicCapacityPenaltyHeight.Text = vendorRating.CubicCapacityPenaltyHeight.ToString();
			txtCubicCapacityPenaltyWidth.Text = vendorRating.CubicCapacityPenaltyWidth.ToString();

			if (!ddlSmc3ServiceLevel.ContainsValue(vendorRating.Smc3ServiceLevel))
			{
				if (!ddlSmc3ServiceLevel.ContainsValue(WebApplicationConstants.PreviousValueNotFound))
				{
					var options = ddlSmc3ServiceLevel.Items
						.Cast<ListItem>()
						.Select(i => new ViewListItem(i.Text, i.Value))
						.ToList();
					options.Insert(0, new ViewListItem(WebApplicationConstants.PreviousValueNotFound, WebApplicationConstants.PreviousValueNotFound));
					ddlSmc3ServiceLevel.DataSource = options;
					ddlSmc3ServiceLevel.DataBind();
				}
				ddlSmc3ServiceLevel.SelectedValue = WebApplicationConstants.PreviousValueNotFound;
			}
			else
			{
				ddlSmc3ServiceLevel.SelectedValue = vendorRating.Smc3ServiceLevel;
			}

			if (!ddlLTLTariff.ContainsValue(vendorRating.LTLTariff))
			{
				if (!ddlLTLTariff.ContainsValue(WebApplicationConstants.PreviousValueNotFound))
				{
					var options = ddlLTLTariff.Items
						.Cast<ListItem>()
						.Select(i => new ViewListItem(i.Text, i.Value))
						.ToList();
					options.Insert(0, new ViewListItem(WebApplicationConstants.PreviousValueNotFound, WebApplicationConstants.PreviousValueNotFound));
					ddlLTLTariff.DataSource = options;
					ddlLTLTariff.DataBind();
				}
				ddlLTLTariff.SelectedValue = WebApplicationConstants.PreviousValueNotFound;
			}
			else
			{
				ddlLTLTariff.SelectedValue = vendorRating.LTLTariff;
			}

			txtLTLTruckLength.Text = vendorRating.LTLTruckLength.ToString();
			txtLTLTruckWidth.Text = vendorRating.LTLTruckWidth.ToString();
			txtLTLTruckHeight.Text = vendorRating.LTLTruckHeight.ToString();
			txtLTLTruckWeight.Text = vendorRating.LTLTruckWeight.ToString();
			txtLTLMinPickupWeight.Text = vendorRating.LTLMinPickupWeight.ToString();
			txtLTLTruckUnitWeight.Text = vendorRating.LTLTruckUnitWeight.ToString();
			txtOverrideAddress.Text = vendorRating.OverrideAddress;
		    txtRatingDetailNotes.Text = vendorRating.RatingDetailNotes;
			txtCriticalBOLNotes.Text = vendorRating.CriticalBOLNotes;
			txtLTLMaxCfForPcfConv.Text = vendorRating.LTLMaxCfForPcfConv.ToString();
		    txtProject44TradingPartnerCode.Text = vendorRating.Project44TradingPartnerCode;

		    txtMaxPackageQuantity.Text = vendorRating.MaxPackageQuantity.ToString();
		    chkMaxPackageQuantityApplies.Checked = vendorRating.MaxPackageQuantityApplies;
            txtMaxItemLength.Text = vendorRating.MaxItemLength.ToString();
            txtMaxItemWidth.Text = vendorRating.MaxItemWidth.ToString();
            txtMaxItemHeight.Text = vendorRating.MaxItemHeight.ToString();
		    chkIndividualItemLimitsApply.Checked = vendorRating.IndividualItemLimitsApply;

			chkEnablePcfToFcConversion.Checked = vendorRating.EnableLTLPcfToFcConversion;
			chkEnablePackageSpecificRatePackageType.Checked = vendorRating.EnableLTLPackageSpecificRates;
		    chkProject44Profile.Checked = vendorRating.Project44Profile;
			tabLTLPcfToFcConvTab.Visible = chkEnablePcfToFcConversion.Checked;
			tabLTLPackageSpecificRates.Visible = chkEnablePackageSpecificRatePackageType.Checked;
			tabLTLDiscountTiers.Visible = !chkEnablePackageSpecificRatePackageType.Checked;
			tabLTLCubicFootCapacityRules.Visible = !chkEnablePackageSpecificRatePackageType.Checked;
			vendorRatingImportExport.EnableLTLPackageSpecificRatesImport = vendorRating.EnableLTLPackageSpecificRates;
			vendorRatingImportExport.EnableLTLDiscountTierImport = !chkEnablePackageSpecificRatePackageType.Checked;
			vendorRatingImportExport.EnableLTLCFCRulesImport = !chkEnablePackageSpecificRatePackageType.Checked;

			// ltl pcf to fc conversion tables
			var conversionTables = SortLTLPcfToFcConversions(vendorRating.LTLPcfToFcConvTab);
			StoredPcfToFcConversions = conversionTables;

			lstLTLPcfToFcConvTab.DataSource = conversionTables;
			lstLTLPcfToFcConvTab.DataBind();
			tabLTLPcfToFcConvTab.HeaderText = conversionTables.BuildTabCount(LtlPcfToFcConvTabHeader);

			// ltl discount tiers
			var discountTiers = SortLTLDiscountTiers(vendorRating.LTLDiscountTiers);
			peLstLTLDiscountTiers.LoadData(discountTiers);
			tabLTLDiscountTiers.HeaderText = vendorRating.LTLDiscountTiers.BuildTabCount(LtlDiscountTiersHeader);

			// ltl additional Charges
			var additionalCharges = SortLTLAdditionalCharge(vendorRating.LTLAdditionalCharges);
			StoredAddlCharges = additionalCharges;
			lstLTLAdditionalCharges.DataSource = additionalCharges;
			lstLTLAdditionalCharges.DataBind();
			tabLTLAdditionalCharges.HeaderText = vendorRating.LTLAdditionalCharges.BuildTabCount(LtlAdditionalChargesHeader);

			// ltl accessorials
			var accessorials = SortLTLAccessorials(vendorRating.LTLAccessorials);
			StoredAccessorials = accessorials;

			lstLTLAccessorials.DataSource = accessorials;
			lstLTLAccessorials.DataBind();
			tabLTLAccessorials.HeaderText = accessorials.BuildTabCount(LtlAccessorialsHeader);

            // ltl guaranteed charges
            var guaranteedCharges = SortLTLGuaranteedCharges(vendorRating.LTLGuaranteedCharges);
            StoredGuaranteedCharges = guaranteedCharges;

            lstLTLGuaranteedCharges.DataSource = guaranteedCharges;
            lstLTLGuaranteedCharges.DataBind();
            tabLTLGuaranteedCharges.HeaderText = guaranteedCharges.BuildTabCount(LtlGuaranteedChargesTabHeader);

			// ltl cubic foot capacity rule
			var cfcRules = SortLtlcfCapacityRules(vendorRating.LTLCubicFootCapacityRules);
			StoredCfcRules = cfcRules;

			lstCFCRules.DataSource = cfcRules;
			lstCFCRules.DataBind();
			tabLTLCubicFootCapacityRules.HeaderText = cfcRules.BuildTabCount(LtlCfCapacityRulesHeader);

			// ltl overlength rules
			var overlengthRules = SortLTLOverlengthRules(vendorRating.LTLOverLengthRules);
			StoredOverlengthRules = overlengthRules;

			lstLTLOverlengthRules.DataSource = overlengthRules;
			lstLTLOverlengthRules.DataBind();
			tabLTLOverlengthChargeRules.HeaderText = overlengthRules.BuildTabCount(LtlOverLengthRulesHeader);

			// ltl package specific rates
			var psRates = SortLTLPackageSpecificRates(vendorRating.LTLPackageSpecificRates);
			peLstPackageSpecificRates.LoadData(psRates);
			tabLTLPackageSpecificRates.HeaderText = psRates.BuildTabCount(LtlPackageSpecificRateTabHeader);


			memberToolBar.ShowUnlock = vendorRating.HasUserLock(ActiveUser, vendorRating.Id);
		}

		private void ProcessTransferredRequest(VendorRating vendorRating)
		{
		    if (vendorRating.IsNew) return;
		    
		    LoadVendorRating(vendorRating);
		    
            if (LoadAuditLog != null)
		        LoadAuditLog(this, new ViewEventArgs<VendorRating>(vendorRating));
		}


		private void UpdatePcfToFcConvTabFromView(VendorRating rating)
		{
			rating.LTLPcfToFcConvTab = StoredPcfToFcConversions;

			if (rating.IsNew)
				foreach (var pcfToFcConvTab in rating.LTLPcfToFcConvTab)
				{
					pcfToFcConvTab.TenantId = rating.TenantId;
					pcfToFcConvTab.VendorRating = rating;
				}
		}

		private void UpdateDiscountTiersFromView(VendorRating rating)
		{
			rating.LTLDiscountTiers = StoredDiscountTiers;

			if (rating.IsNew)
				foreach (var discTier in rating.LTLDiscountTiers)
				{
					discTier.TenantId = rating.TenantId;
					discTier.VendorRating = rating;
				}
		}

		private void UpdateLTlAccessorials(VendorRating rating)
		{
			rating.LTLAccessorials = StoredAccessorials;

			if (rating.IsNew)
				foreach (var accessorial in rating.LTLAccessorials)
				{
					accessorial.TenantId = rating.TenantId;
					accessorial.VendorRating = rating;
				}
		}

		private void UpdateLTLCubicFootCapacityRules(VendorRating rating)
		{
			var cfcRules = StoredCfcRules;
			rating.LTLCubicFootCapacityRules = cfcRules;
			rating.HasLTLCubicFootCapacityRules = cfcRules.Count > 0;

			if (rating.IsNew)
				foreach (var cfcRule in rating.LTLCubicFootCapacityRules)
				{
					cfcRule.TenantId = rating.TenantId;
					cfcRule.VendorRating = rating;
				}
		}

		private void UpdateLTLOverLengthRules(VendorRating rating)
		{
			var overlengthRules = StoredOverlengthRules;
			rating.LTLOverLengthRules = overlengthRules;
			rating.HasLTLOverLengthRules = overlengthRules.Count > 0;

			if (rating.IsNew)
				foreach (var overlengthRule in rating.LTLOverLengthRules)
				{
					overlengthRule.TenantId = rating.TenantId;
					overlengthRule.VendorRating = rating;
				}
		}

		private void UpdateLTLAdditionalCharges(VendorRating rating)
		{
			var additionalCharges = StoredAddlCharges;
			rating.LTLAdditionalCharges = additionalCharges;
			rating.HasAdditionalCharges = additionalCharges.Count > 0;

			if (rating.IsNew)
				foreach (var additionalCharge in rating.LTLAdditionalCharges)
				{
					additionalCharge.TenantId = rating.TenantId;
					additionalCharge.VendorRating = rating;
					foreach (var index in additionalCharge.AdditionalChargeIndices)
						index.TenantId = additionalCharge.TenantId;
				}

		}

		private void UpdateLTLPackageSpecificRates(VendorRating rating)
		{
			rating.LTLPackageSpecificRates = StoredPackageSpecificRates;

			if (rating.IsNew)
				foreach (var psRate in rating.LTLPackageSpecificRates)
				{
					psRate.TenantId = rating.TenantId;
					psRate.VendorRating = rating;
				}
		}

        private void UpdateLTLGuaranteedCharges(VendorRating rating)
        {
            rating.LTLGuaranteedCharges = StoredGuaranteedCharges;

            if (rating.IsNew)
                foreach (var guaranteedCharge in rating.LTLGuaranteedCharges)
                {
                    guaranteedCharge.TenantId = rating.TenantId;
                    guaranteedCharge.VendorRating = rating;
                }
        }


		protected void Page_Load(object sender, EventArgs e)
		{
			new VendorRatingHandler(this).Initialize();

			memberToolBar.ShowSave = Access.Modify;
			memberToolBar.ShowDelete = Access.Remove;
			memberToolBar.ShowNew = Access.Modify;
			memberToolBar.ShowEdit = Access.Modify;
			memberToolBar.ShowExport = Access.Grant;
			memberToolBar.ShowImport = Access.Modify;

			vendorRatingFinder.OpenForEditEnabled = Access.Modify;
			vendorRatingImportExport.EnableExport = Access.Grant;

			// setup import export control
			vendorRatingImportExport.LTLDiscountTiersDataStoreKey = DiscountTierStoreKey;
			vendorRatingImportExport.LTLAccessorialsDataStoreKey = AccessorialsStoreKey;
			vendorRatingImportExport.LTLAdditionalChargesDataStoreKey = AdditionalChargesStoreKey;
			vendorRatingImportExport.LTLCFCRulesDataStoreKey = CfcRulesStoreKey;
			vendorRatingImportExport.LTLOverlengthRulesDataStoreKey = OverLengthRulesStoreKey;
			vendorRatingImportExport.LTLPackageSpecificRatesDataStoreKey = PackageSpecificRateStoreKey;
		    vendorRatingImportExport.LTLGuaranteedChargesDataStoreKey = GuaranteedChargesStoreKey;

			// setup pagers
			peLstPackageSpecificRates.UseParentDataStore = true;
			peLstPackageSpecificRates.ParentDataStoreKey = PackageSpecificRateStoreKey;
			peLstLTLDiscountTiers.UseParentDataStore = true;
			peLstLTLDiscountTiers.ParentDataStoreKey = DiscountTierStoreKey;

			//setup macthing region search control
			vendorRatingMatchingRegionSearch.LTLDiscountTiersDataStoreKey = DiscountTierStoreKey;

			memberToolBar.LoadAdditionalActions(ToolbarMoreActions());

			if (IsPostBack) return;

			aceVendor.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString() } });

			memberToolBar.LoadTemplates(new List<string>
											{
												WebApplicationConstants.VendorRatingAdditionalChargesImportTemplate, 
												WebApplicationConstants.VendorRatingDiscountTiersImportTemplate,
												WebApplicationConstants.VendorRatingLTLAccessorialsImportTemplate,
												WebApplicationConstants.VendorRatingLtlcfCapacityImportTemplate,
												WebApplicationConstants.VendorRatingLTLOverlengthRulesImportExport,
												WebApplicationConstants.VendorRatingLTLPackageSpecificRatesImportTemplate,
                                                WebApplicationConstants.VendorRatingLTLGuaranteedChargesImportTemplate
											});

			if (Loading != null)
				Loading(this, new EventArgs());

			BindFreightClassAndFAKSources();

			if (Session[WebApplicationConstants.TransferVendorRatingId] != null)
			{
				ProcessTransferredRequest(new VendorRating(Session[WebApplicationConstants.TransferVendorRatingId].ToLong()));
				Session[WebApplicationConstants.TransferVendorRatingId] = null;
			}

            if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.TransferNumber]))
                ProcessTransferredRequest(new VendorRating(Request.QueryString[WebApplicationConstants.TransferNumber].UrlTextDecrypt().ToLong()));

			SetEditStatus(false);
		}



		protected void OnVendorRatingFinderItemSelected(object sender, ViewEventArgs<VendorRating> e)
		{
			if (hidVendorRatingId.Value.ToLong() != default(long))
			{
				var oldVendorRating = new VendorRating(hidVendorRatingId.Value.ToLong(), false);
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<VendorRating>(oldVendorRating));
			}

			var vendorRating = e.Argument;

			LoadVendorRating(vendorRating);

			vendorRatingFinder.Visible = false;

			if (vendorRatingFinder.OpenForEdit && Lock != null)
			{
				SetEditStatus(true);
				memberToolBar.ShowUnlock = true;
				Lock(this, new ViewEventArgs<VendorRating>(vendorRating));
			}
			else
			{
				SetEditStatus(false);
			}

			if (LoadAuditLog != null) LoadAuditLog(this, new ViewEventArgs<VendorRating>(vendorRating));
		}

		protected void OnVendorRatingFinderSelectionCancelled(object sender, EventArgs e)
		{
			vendorRatingFinder.Visible = false;
		}

		protected void OnToolbarFindClicked(object sender, EventArgs e)
		{
			vendorRatingFinder.Visible = true;
		}

		protected void OnToolbarSaveClicked(object sender, EventArgs e)
		{
			var ratingId = hidVendorRatingId.Value.ToLong();
			var rating = new VendorRating(ratingId, ratingId != default(long));

			if (rating.IsNew)
			{
				rating.TenantId = ActiveUser.TenantId;
				rating.LTLTariff = string.Empty;
			}
			else rating.LoadCollections();

			// details
			rating.Name = txtName.Text;
			rating.DisplayName = txtDisplayName.Text;
			rating.Active = chkActive.Checked;
			rating.ExpirationDate = txtExpirationDate.Text.ToDateTime().TimeToMaximum();
			rating.Vendor = new Vendor(hidVendorId.Value.ToLong(), false);
			rating.FuelTableId = ddlFuelTable.SelectedValue.ToLong();
			rating.FuelIndexRegion = ddlFuelIndexRegion.SelectedValue.ToEnum<FuelIndexRegion>();
			rating.CurrentLTLFuelMarkup = txtCurrentLTLFuelMarkUp.Text.ToDecimal();
			rating.FuelMarkupCeiling = txtFuelMarkUpCeiling.Text.ToDecimal();
			rating.FuelMarkupFloor = txtFuelMarkUpFloor.Text.ToDecimal();
		    rating.FuelChargeCodeId = ddlFuelChargeCode.SelectedValue.ToLong();
			rating.FuelUpdatesOn = ddlFuelUpdatesOn.SelectedValue.ToEnum<DayOfWeek>();
			rating.LTLPackageSpecificRatePackageTypeId = ddlPackageSpecificRatePackageType.SelectedValue.ToLong();
			rating.LTLPackageSpecificFreightChargeCodeId = ddlLTLPackageSpecificRateFreightChargeCode.SelectedValue.ToLong();

			rating.CubicCapacityPenaltyHeight = txtCubicCapacityPenaltyHeight.Text.ToDecimal();
			rating.CubicCapacityPenaltyWidth = txtCubicCapacityPenaltyWidth.Text.ToDecimal();

			if (ddlLTLTariff.SelectedValue != WebApplicationConstants.PreviousValueNotFound)
				rating.LTLTariff = ddlLTLTariff.SelectedValue;
			if (ddlSmc3ServiceLevel.SelectedValue != WebApplicationConstants.PreviousValueNotFound)
				rating.Smc3ServiceLevel = ddlSmc3ServiceLevel.SelectedValue;

			rating.LTLTruckLength = txtLTLTruckLength.Text.ToDecimal();
			rating.LTLTruckWidth = txtLTLTruckWidth.Text.ToDecimal();
			rating.LTLTruckHeight = txtLTLTruckHeight.Text.ToDecimal();
			rating.LTLTruckWeight = txtLTLTruckWeight.Text.ToDecimal();
			rating.LTLMinPickupWeight = txtLTLMinPickupWeight.Text.ToDecimal();
			rating.LTLTruckUnitWeight = txtLTLTruckUnitWeight.Text.ToDecimal();
            rating.OverrideAddress = txtOverrideAddress.Text;
            rating.RatingDetailNotes = txtRatingDetailNotes.Text;
			rating.CriticalBOLNotes = txtCriticalBOLNotes.Text;
			rating.LTLMaxCfForPcfConv = txtLTLMaxCfForPcfConv.Text.ToDecimal();
		    rating.Project44TradingPartnerCode = string.IsNullOrWhiteSpace(txtProject44TradingPartnerCode.Text)
		        ? rating.Vendor.Scac
		        : txtProject44TradingPartnerCode.Text;

            rating.MaxPackageQuantity = txtMaxPackageQuantity.Text.ToInt();
            rating.MaxPackageQuantityApplies = chkMaxPackageQuantityApplies.Checked;
            rating.MaxItemLength = txtMaxItemLength.Text.ToInt();
            rating.MaxItemWidth = txtMaxItemWidth.Text.ToInt();
            rating.MaxItemHeight = txtMaxItemHeight.Text.ToInt();
            rating.IndividualItemLimitsApply = chkIndividualItemLimitsApply.Checked;

			rating.EnableLTLPcfToFcConversion = chkEnablePcfToFcConversion.Checked;
			rating.EnableLTLPackageSpecificRates = chkEnablePackageSpecificRatePackageType.Checked;
		    rating.Project44Profile = chkProject44Profile.Checked;

			UpdatePcfToFcConvTabFromView(rating);
			UpdateDiscountTiersFromView(rating);
			UpdateLTlAccessorials(rating);
			UpdateLTLCubicFootCapacityRules(rating);
			UpdateLTLOverLengthRules(rating);
			UpdateLTLAdditionalCharges(rating);
            UpdateLTLPackageSpecificRates(rating);
            UpdateLTLGuaranteedCharges(rating);

			hidFlag.Value = SaveFlag;

			if (Save != null)
				Save(this, new ViewEventArgs<VendorRating>(rating));

			if (LoadAuditLog != null)
				LoadAuditLog(this, new ViewEventArgs<VendorRating>(rating));
		}

		protected void OnToolbarDeleteClicked(object sender, EventArgs e)
		{
			var rating = new VendorRating(hidVendorRatingId.Value.ToLong(), false);

			if (Delete != null)
				Delete(this, new ViewEventArgs<VendorRating>(rating));

			vendorRatingFinder.Reset();
		}

		protected void OnToolbarNewClicked(object sender, EventArgs e)
		{
			litErrorMessages.Text = string.Empty;

			LoadVendorRating(new VendorRating());
			DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
			SetEditStatus(true);
		}

		protected void OnToolbarEditClicked(object sender, EventArgs e)
		{
			var rating = new VendorRating(hidVendorRatingId.Value.ToLong(), false);

			if (Lock == null || rating.IsNew) return;

			SetEditStatus(true);
			memberToolBar.ShowUnlock = true;
			Lock(this, new ViewEventArgs<VendorRating>(rating));

			LoadVendorRating(rating);
		}

		protected void OnUnlockClicked(object sender, EventArgs e)
		{
			var vendorRating = new VendorRating(hidVendorRatingId.Value.ToLong(), false);
			if (UnLock != null && !vendorRating.IsNew)
			{
				SetEditStatus(false);
				memberToolBar.ShowUnlock = false;
				UnLock(this, new ViewEventArgs<VendorRating>(vendorRating));
			}
		}

		protected void OnToolbarCommandClicked(object sender, ViewEventArgs<string> e)
		{
			switch (e.Argument)
			{
				case MatchingDiscountTiers:
					vendorRatingMatchingRegionSearch.Visible = true;
					pnlDimScreen.Visible = true;
					break;
			}
		}


		protected void OpenVendorImportExport(object sender, EventArgs e)
		{
			vendorRatingImportExport.Visible = true;
			pnlDimScreen.Visible = true;
		}

		protected void OnVendorRatingImportExportClose(object sender, EventArgs e)
		{
			vendorRatingImportExport.Visible = false;
			pnlDimScreen.Visible = false;
		}

		protected void OnVendorRatingImportExportProcessingError(object sender, ViewEventArgs<string> e)
		{
			DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
		}

		protected void OnVendorRatingImportExportLTLDiscTierImported(object sender, ViewEventArgs<string> e)
		{
			var tiers = SortLTLDiscountTiers(StoredDiscountTiers);
			StoredDiscountTiers = tiers;
			peLstLTLDiscountTiers.LoadData(tiers);
			tabLTLDiscountTiers.HeaderText = tiers.BuildTabCount(LtlDiscountTiersHeader);

			DisplayMessages(new[] { ValidationMessage.Information(e.Argument) });
		}

		protected void OnVendorRatingImportExportLTLAccessorialsImported(object sender, ViewEventArgs<string> e)
		{
			var ltlAccessorials = SortLTLAccessorials(StoredAccessorials);
			StoredAccessorials = ltlAccessorials;
			lstLTLAccessorials.DataSource = ltlAccessorials;
			lstLTLAccessorials.DataBind();
			tabLTLAccessorials.HeaderText = ltlAccessorials.BuildTabCount(LtlAccessorialsHeader);

			DisplayMessages(new[] { ValidationMessage.Information(e.Argument) });
		}

		protected void OnVendorRatingImportExportLtlcfcRulesImported(object sender, ViewEventArgs<string> e)
		{
			var ltlCfcRules = SortLtlcfCapacityRules(StoredCfcRules);
			StoredCfcRules = ltlCfcRules;

			lstCFCRules.DataSource = ltlCfcRules;
			lstCFCRules.DataBind();
			tabLTLCubicFootCapacityRules.HeaderText = ltlCfcRules.BuildTabCount(LtlCfCapacityRulesHeader);

			DisplayMessages(new[] { ValidationMessage.Information(e.Argument) });
		}

		protected void OnVendorRatingImportExportLTLAddtnlChargesImported(object sender, ViewEventArgs<string> e)
		{
			var additionalCharges = SortLTLAdditionalCharge(StoredAddlCharges);
			StoredAddlCharges = additionalCharges;

			lstLTLAdditionalCharges.DataSource = additionalCharges;
			lstLTLAdditionalCharges.DataBind();
			tabLTLAdditionalCharges.HeaderText = additionalCharges.BuildTabCount(LtlAdditionalChargesHeader);

			DisplayMessages(new[] { ValidationMessage.Information(e.Argument) });
		}

		protected void OnVendorRatingImportExportLTLOverlengthRulesImported(object sender, ViewEventArgs<string> e)
		{
			var overlengthRules = SortLTLOverlengthRules(StoredOverlengthRules);

			lstLTLOverlengthRules.DataSource = overlengthRules;
			lstLTLOverlengthRules.DataBind();
			tabLTLOverlengthChargeRules.HeaderText = overlengthRules.BuildTabCount(LtlOverLengthRulesHeader);

			DisplayMessages(new[] { ValidationMessage.Information(e.Argument) });
		}

		protected void OnVendorRatingImportExportLTLPackageSpecificRatesImported(object sender, ViewEventArgs<string> e)
		{
			var psRates = SortLTLPackageSpecificRates(StoredPackageSpecificRates);
			StoredPackageSpecificRates = psRates;

			peLstPackageSpecificRates.LoadData(psRates);
			tabLTLPackageSpecificRates.HeaderText = psRates.BuildTabCount(LtlPackageSpecificRateTabHeader);

			DisplayMessages(new[] { ValidationMessage.Information(e.Argument) });
		}

        protected void OnVendorRatingImportExportLTLGuaranteedChargesImported(object sender, ViewEventArgs<string> e)
        {
            var gCharges = SortLTLGuaranteedCharges(StoredGuaranteedCharges);
            StoredGuaranteedCharges = gCharges;
            lstLTLGuaranteedCharges.DataSource = gCharges;
            lstLTLGuaranteedCharges.DataBind();
            tabLTLGuaranteedCharges.HeaderText = gCharges.BuildTabCount(LtlGuaranteedChargesTabHeader);

            DisplayMessages(new[] { ValidationMessage.Information(e.Argument) });
        }


		protected void OnVendorNumberEntered(object sender, EventArgs e)
		{
			if (VendorSearch != null)
				VendorSearch(this, new ViewEventArgs<string>(txtVendorNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
		}

		protected void OnVendorSearchClicked(object sender, ImageClickEventArgs e)
		{
			vendorFinder.Visible = true;
		}

		protected void OnVendorFinderSelectionCancelled(object sender, EventArgs e)
		{
			vendorFinder.Visible = false;
		}

		protected void OnVendorFinderItemSelected(object sender, ViewEventArgs<Vendor> e)
		{
			litErrorMessages.Text = string.Empty;

			DisplayVendor(e.Argument);
			vendorFinder.Visible = false;
		}


		private void LoadDiscountTierForEdit(DiscountTier tier)
		{
			ddlDiscountTierOriginRegion.SelectedValue = tier.OriginRegionId.ToString();
			ddlDiscountTierDestinationRegion.SelectedValue = tier.DestinationRegionId.ToString();
			ddlDiscountTierRateBasis.SelectedValue = tier.RateBasis.ToInt().ToString();
            ddlDiscountTierFreightChargeCode.SelectedValue = tier.FreightChargeCodeId.GetString();
			ddlDiscountTierWeightBreak.SelectedValue = tier.WeightBreak.ToInt().ToString();
			txtDiscountTierTierPriority.Text = tier.TierPriority.ToString();
			txtDiscountTierEffectiveDate.Text = tier.EffectiveDate.FormattedShortDate();
			txtDiscountTierDiscountPercent.Text = tier.DiscountPercent.ToString();
			txtDiscountTierDiscountCeilingValue.Text = tier.CeilingValue.ToString();
			txtDiscountTierDiscountFloorValue.Text = tier.FloorValue.ToString();

			ddlDiscountTierFak50.SelectedValue = tier.FAK50.ToString();
			ddlDiscountTierFak55.SelectedValue = tier.FAK55.ToString();
			ddlDiscountTierFak60.SelectedValue = tier.FAK60.ToString();
			ddlDiscountTierFak65.SelectedValue = tier.FAK65.ToString();
			ddlDiscountTierFak70.SelectedValue = tier.FAK70.ToString();
			ddlDiscountTierFak775.SelectedValue = tier.FAK775.ToString();
			ddlDiscountTierFak85.SelectedValue = tier.FAK85.ToString();
			ddlDiscountTierFak925.SelectedValue = tier.FAK925.ToString();
			ddlDiscountTierFak100.SelectedValue = tier.FAK100.ToString();
			ddlDiscountTierFak110.SelectedValue = tier.FAK110.ToString();
			ddlDiscountTierFak125.SelectedValue = tier.FAK125.ToString();
			ddlDiscountTierFak150.SelectedValue = tier.FAK150.ToString();
			ddlDiscountTierFak175.SelectedValue = tier.FAK175.ToString();
			ddlDiscountTierFak200.SelectedValue = tier.FAK200.ToString();
			ddlDiscountTierFak250.SelectedValue = tier.FAK250.ToString();
			ddlDiscountTierFak300.SelectedValue = tier.FAK300.ToString();
			ddlDiscountTierFak400.SelectedValue = tier.FAK400.ToString();
			ddlDiscountTierFak500.SelectedValue = tier.FAK500.ToString();
		}

		protected void OnEditDiscountTierClicked(object sender, ImageClickEventArgs e)
		{
			var imageButton = (ImageButton)sender;

			var discountTiers = SortLTLDiscountTiers(StoredDiscountTiers);
			var pageIdx = peLstLTLDiscountTiers.CurrentPage - 1;
			var itemIdx = imageButton.FindControl("hidDiscountTierIndex").ToCustomHiddenField().Value.ToInt();

			hidEditingIndex.Value = itemIdx.ToString();

			var tier = discountTiers[(pageIdx * peLstLTLDiscountTiers.PageSize) + itemIdx];

			LoadDiscountTierForEdit(tier);

			pnlEditDiscountTier.Visible = true;
			pnlDimScreen.Visible = true;
		}

		protected void OnAddLTLDiscountTierClicked(object sender, EventArgs e)
		{
			hidEditingIndex.Value = (WebApplicationConstants.InvalidIndex).ToString();
			LoadDiscountTierForEdit(DiscountTier.NewDiscountTier);
			pnlEditDiscountTier.Visible = true;
			pnlDimScreen.Visible = pnlEditDiscountTier.Visible;
		}

		protected void OnClearLTLDiscountTierClicked(object sender, EventArgs e)
		{
			peLstLTLDiscountTiers.LoadData(new List<DiscountTier>());
			tabLTLDiscountTiers.HeaderText = LtlDiscountTiersHeader;
            athtuTabUpdater.SetForUpdate(tabLTLDiscountTiers.ClientID, LtlDiscountTiersHeader);
        }

		protected void OnDeleteDiscountTierClicked(object sender, ImageClickEventArgs e)
		{
			var imageButton = (ImageButton)sender;

			var discountTiers = SortLTLDiscountTiers(StoredDiscountTiers);
			var pageIdx = peLstLTLDiscountTiers.CurrentPage - 1;
			var itemIdx = imageButton.FindControl("hidDiscountTierIndex").ToCustomHiddenField().Value.ToInt();

			discountTiers.RemoveAt((pageIdx * peLstLTLDiscountTiers.PageSize) + itemIdx);

			peLstLTLDiscountTiers.LoadData(SortLTLDiscountTiers(discountTiers), peLstLTLDiscountTiers.CurrentPage);
			tabLTLDiscountTiers.HeaderText = discountTiers.BuildTabCount(LtlDiscountTiersHeader);
            athtuTabUpdater.SetForUpdate(tabLTLDiscountTiers.ClientID, discountTiers.BuildTabCount(LtlDiscountTiersHeader));

		}

		protected void OnCloseEditDiscountTierClicked(object sender, EventArgs e)
		{
			pnlEditDiscountTier.Visible = false;
			pnlDimScreen.Visible = pnlEditDiscountTier.Visible;
		}

		protected void OnEditDiscountTierDoneClicked(object sender, EventArgs e)
		{
			var discountTiers = SortLTLDiscountTiers(StoredDiscountTiers);
			var rating = new VendorRating(hidVendorRatingId.Value.ToLong());

			var pageIdx = peLstLTLDiscountTiers.CurrentPage - 1;

			var isNew = hidEditingIndex.Value.ToInt() == WebApplicationConstants.InvalidIndex;
			var editingIndex = (pageIdx * peLstLTLDiscountTiers.PageSize) + hidEditingIndex.Value.ToInt();

			var discountTier = isNew ? DiscountTier.NewDiscountTier : discountTiers[editingIndex];

			if (!isNew && !discountTier.HasChanges())
				discountTier.TakeSnapShot();

			discountTier.OriginRegionId = ddlDiscountTierOriginRegion.SelectedValue.ToLong();
			discountTier.DestinationRegionId = ddlDiscountTierDestinationRegion.SelectedValue.ToLong();
			discountTier.RateBasis = ddlDiscountTierRateBasis.SelectedValue.ToEnum<DiscountTierRateBasis>();
			discountTier.FreightChargeCodeId = ddlDiscountTierFreightChargeCode.SelectedValue.ToLong();
			discountTier.TierPriority = txtDiscountTierTierPriority.Text.ToInt();
			discountTier.EffectiveDate = txtDiscountTierEffectiveDate.Text.ToDateTime();
			discountTier.DiscountPercent = txtDiscountTierDiscountPercent.Text.ToDecimal();
			discountTier.WeightBreak = ddlDiscountTierWeightBreak.SelectedValue.ToEnum<WeightBreak>();
			discountTier.FloorValue = txtDiscountTierDiscountFloorValue.Text.ToDecimal();
			discountTier.CeilingValue = txtDiscountTierDiscountCeilingValue.Text.ToDecimal();
			discountTier.FAK50 = ddlDiscountTierFak50.SelectedValue.ToDouble();
			discountTier.FAK55 = ddlDiscountTierFak55.SelectedValue.ToDouble();
			discountTier.FAK60 = ddlDiscountTierFak60.SelectedValue.ToDouble();
			discountTier.FAK65 = ddlDiscountTierFak65.SelectedValue.ToDouble();
			discountTier.FAK70 = ddlDiscountTierFak70.SelectedValue.ToDouble();
			discountTier.FAK775 = ddlDiscountTierFak775.SelectedValue.ToDouble();
			discountTier.FAK85 = ddlDiscountTierFak85.SelectedValue.ToDouble();
			discountTier.FAK925 = ddlDiscountTierFak925.SelectedValue.ToDouble();
			discountTier.FAK100 = ddlDiscountTierFak100.SelectedValue.ToDouble();
			discountTier.FAK110 = ddlDiscountTierFak110.SelectedValue.ToDouble();
			discountTier.FAK125 = ddlDiscountTierFak125.SelectedValue.ToDouble();
			discountTier.FAK150 = ddlDiscountTierFak150.SelectedValue.ToDouble();
			discountTier.FAK150 = ddlDiscountTierFak150.SelectedValue.ToDouble();
			discountTier.FAK150 = ddlDiscountTierFak150.SelectedValue.ToDouble();
			discountTier.FAK175 = ddlDiscountTierFak175.SelectedValue.ToDouble();
			discountTier.FAK200 = ddlDiscountTierFak200.SelectedValue.ToDouble();
			discountTier.FAK250 = ddlDiscountTierFak250.SelectedValue.ToDouble();
			discountTier.FAK300 = ddlDiscountTierFak300.SelectedValue.ToDouble();
			discountTier.FAK400 = ddlDiscountTierFak400.SelectedValue.ToDouble();
			discountTier.FAK500 = ddlDiscountTierFak500.SelectedValue.ToDouble();
			discountTier.VendorRating = rating;
			discountTier.TenantId = rating.TenantId;

			if (isNew)
			{
				discountTiers.Add(discountTier);
			}
			else
			{
				discountTiers[editingIndex] = discountTier;
			}

			peLstLTLDiscountTiers.LoadData(SortLTLDiscountTiers(discountTiers), peLstLTLDiscountTiers.CurrentPage);


			tabLTLDiscountTiers.HeaderText = discountTiers.BuildTabCount(LtlDiscountTiersHeader);
            athtuTabUpdater.SetForUpdate(tabLTLDiscountTiers.ClientID, discountTiers.BuildTabCount(LtlDiscountTiersHeader));

			pnlEditDiscountTier.Visible = false;
			pnlDimScreen.Visible = false;
		}



		private void LoadLTLAccessorialForEdit(LTLAccessorial accessorial)
		{
			ddlLTLAccessorialService.SelectedValue = accessorial.ServiceId.GetString();
			ddlLTLAccessorialRateType.SelectedValue = accessorial.RateType.ToInt().ToString();
			txtLTLAccessorialRate.Text = accessorial.Rate.ToString();
			txtLTLAccessorialRateFloorValue.Text = accessorial.FloorValue.ToString();
			txtLTLAccessorialRateCeilingValue.Text = accessorial.CeilingValue.ToString();
			txtLTLAccessorialEffectiveDate.Text = accessorial.EffectiveDate.FormattedShortDate();
		}

		protected void OnAddLTLAccessorialClicked(object sender, EventArgs e)
		{
			hidEditingIndex.Value = (WebApplicationConstants.InvalidIndex).ToString();
			LoadLTLAccessorialForEdit(new LTLAccessorial { EffectiveDate = DateTime.Now });
			pnlEditLTLAccessorial.Visible = true;
			pnlDimScreen.Visible = true;
		}

		protected void OnClearLTLAccessorialsClicked(object sender, EventArgs e)
		{
			StoredAccessorials = new List<LTLAccessorial>();

			lstLTLAccessorials.DataSource = new List<LTLAccessorial>();
			lstLTLAccessorials.DataBind();
			tabLTLAccessorials.HeaderText = LtlAccessorialsHeader;
            athtuTabUpdater.SetForUpdate(tabLTLAccessorials.ClientID, LtlAccessorialsHeader);
        }

		protected void OnEditLTLAccessorialDoneClicked(object sender, EventArgs e)
		{
			var accessorials = SortLTLAccessorials(StoredAccessorials);
			var rating = new VendorRating(hidVendorRatingId.Value.ToLong());

			var editingIndex = hidEditingIndex.Value.ToInt();
			var isNew = editingIndex == WebApplicationConstants.InvalidIndex;
			var accessorial = isNew ? new LTLAccessorial { EffectiveDate = DateTime.Now } : accessorials[editingIndex];

			if (!isNew && !accessorial.HasChanges())
				accessorial.TakeSnapShot();

			accessorial.ServiceId = ddlLTLAccessorialService.SelectedValue.ToLong();
			accessorial.RateType = ddlLTLAccessorialRateType.SelectedValue.ToEnum<RateType>();
			accessorial.EffectiveDate = txtLTLAccessorialEffectiveDate.Text.ToDateTime();
			accessorial.Rate = txtLTLAccessorialRate.Text.ToDecimal();
			accessorial.FloorValue = txtLTLAccessorialRateFloorValue.Text.ToDecimal();
			accessorial.CeilingValue = txtLTLAccessorialRateCeilingValue.Text.ToDecimal();
			accessorial.VendorRating = rating;
			accessorial.TenantId = rating.TenantId;

			if (isNew)
			{
				accessorials.Add(accessorial);
			}
			else
			{
				accessorials[editingIndex] = accessorial;
			}

			StoredAccessorials = accessorials;

			lstLTLAccessorials.DataSource = SortLTLAccessorials(accessorials);
			lstLTLAccessorials.DataBind();
			tabLTLAccessorials.HeaderText = accessorials.BuildTabCount(LtlAccessorialsHeader);
            athtuTabUpdater.SetForUpdate(tabLTLAccessorials.ClientID, accessorials.BuildTabCount(LtlAccessorialsHeader));

			pnlEditLTLAccessorial.Visible = false;
			pnlDimScreen.Visible = false;
		}

		protected void OnCloseEditLTLAccessorialClicked(object sender, EventArgs e)
		{
			pnlEditLTLAccessorial.Visible = false;
			pnlDimScreen.Visible = pnlEditLTLAccessorial.Visible;
		}

		protected void OnEditLTLAccssorialClicked(object sender, ImageClickEventArgs e)
		{
			var imageButton = ((ImageButton)sender);

			var accessorials = SortLTLAccessorials(StoredAccessorials);

			var itemIdx = imageButton.FindControl("hidLTLAccessorialIndex").ToCustomHiddenField().Value.ToInt();
			hidEditingIndex.Value = itemIdx.ToString();

			var accessorial = accessorials[itemIdx];

			LoadLTLAccessorialForEdit(accessorial);

			pnlEditLTLAccessorial.Visible = true;
			pnlDimScreen.Visible = true;
		}

		protected void OnDeleteLTLAccssorialClicked(object sender, ImageClickEventArgs e)
		{
			var imageButton = (ImageButton)sender;

			var accessorials = SortLTLAccessorials(StoredAccessorials);
			var itemIdx = imageButton.FindControl("hidLTLAccessorialIndex").ToCustomHiddenField().Value.ToInt();

			accessorials.RemoveAt(itemIdx);

			StoredAccessorials = accessorials;

			lstLTLAccessorials.DataSource = accessorials;
			lstLTLAccessorials.DataBind();
			tabLTLAccessorials.HeaderText = accessorials.BuildTabCount(LtlAccessorialsHeader);
            athtuTabUpdater.SetForUpdate(tabLTLAccessorials.ClientID, accessorials.BuildTabCount(LtlAccessorialsHeader));
        }



		private void LoadLTLCubicFootCapacityRuleForEdit(LTLCubicFootCapacityRule rule)
		{
			txtLTLCFCLowerBound.Text = rule.LowerBound.ToString();
			txtLTLCFCUpperBound.Text = rule.UpperBound.ToString();
			ddlLTLCFCAppliedFreightClass.SelectedValue = rule.AppliedFreightClass.ToString();
			chkLTLCFCFAKIsApplicable.Checked = rule.ApplyFAK;
			chkLTLCFCDiscountIsApplicable.Checked = rule.ApplyDiscount;
			txtLTLCFCAvgLbPerCFLimit.Text = rule.AveragePoundPerCubicFootLimit.ToString();
			chkLTLCFCAVgLbPerCFLimitIsApplicable.Checked = rule.AveragePoundPerCubicFootApplies;
			txtLTLCFCPenaltyLbPerCF.Text = rule.PenaltyPoundPerCubicFoot.ToString();
			txtLTLCFCEffectiveDate.Text = rule.EffectiveDate.FormattedShortDate();
		}

		protected void OnAddCFCRuleClicked(object sender, EventArgs e)
		{
			hidEditingIndex.Value = (WebApplicationConstants.InvalidIndex).ToString();
			LoadLTLCubicFootCapacityRuleForEdit(new LTLCubicFootCapacityRule
			{
				AppliedFreightClass = 0,
				ApplyFAK = false,
				EffectiveDate = DateTime.Now,
				ApplyDiscount = true
			});
			pnlEditLTLCFC.Visible = true;
			pnlDimScreen.Visible = true;
		}

		protected void OnClearCFCRulesClicked(object sender, EventArgs e)
		{
			StoredCfcRules = new List<LTLCubicFootCapacityRule>();

			lstCFCRules.DataSource = new List<LTLCubicFootCapacityRule>();
			lstCFCRules.DataBind();
			tabLTLCubicFootCapacityRules.HeaderText = LtlCfCapacityRulesHeader;
            athtuTabUpdater.SetForUpdate(tabLTLCubicFootCapacityRules.ClientID, LtlCfCapacityRulesHeader);
        }

		protected void OnEditLtlcfcClicked(object sender, ImageClickEventArgs e)
		{
			var imageButton = ((ImageButton)sender);

			var cfcRules = SortLtlcfCapacityRules(StoredCfcRules);

			var itemIdx = imageButton.FindControl("hidLTLCFCRuleIndex").ToCustomHiddenField().Value.ToInt();
			hidEditingIndex.Value = itemIdx.ToString();

			var cfcRule = cfcRules[itemIdx];

			LoadLTLCubicFootCapacityRuleForEdit(cfcRule);

			pnlEditLTLCFC.Visible = true;
			pnlDimScreen.Visible = true;
		}

		protected void OnDeleteLtlcfcClicked(object sender, ImageClickEventArgs e)
		{
			var imageButton = (ImageButton)sender;

			var cfcRules = SortLtlcfCapacityRules(StoredCfcRules);
			var itemIdx = imageButton.FindControl("hidLTLCFCRuleIndex").ToCustomHiddenField().Value.ToInt();

			cfcRules.RemoveAt(itemIdx);

			StoredCfcRules = cfcRules;

			lstCFCRules.DataSource = cfcRules;
			lstCFCRules.DataBind();
			tabLTLCubicFootCapacityRules.HeaderText = cfcRules.BuildTabCount(LtlCfCapacityRulesHeader);
            athtuTabUpdater.SetForUpdate(tabLTLCubicFootCapacityRules.ClientID, cfcRules.BuildTabCount(LtlCfCapacityRulesHeader));
        }

		protected void OnCloseEditLtlcfcClicked(object sender, EventArgs e)
		{
			pnlEditLTLCFC.Visible = false;
			pnlDimScreen.Visible = false;
		}

		protected void OnEditLtlcfcDoneClicked(object sender, EventArgs e)
		{
			var cfcRules = SortLtlcfCapacityRules(StoredCfcRules);
			var rating = new VendorRating(hidVendorRatingId.Value.ToLong());

			var editingIndex = hidEditingIndex.Value.ToInt();
			var isNew = editingIndex == WebApplicationConstants.InvalidIndex;

			var cfcRule = isNew ? LTLCubicFootCapacityRule.NewLTLCubicFootCapacityRule : cfcRules[editingIndex];

			if (!isNew && !cfcRule.HasChanges())
				cfcRule.TakeSnapShot();

			cfcRule.LowerBound = txtLTLCFCLowerBound.Text.ToDecimal();
			cfcRule.UpperBound = txtLTLCFCUpperBound.Text.ToDecimal();
			cfcRule.AppliedFreightClass = ddlLTLCFCAppliedFreightClass.SelectedValue.ToDouble();
			cfcRule.ApplyFAK = chkLTLCFCFAKIsApplicable.Checked;
			cfcRule.ApplyDiscount = chkLTLCFCDiscountIsApplicable.Checked;
			cfcRule.AveragePoundPerCubicFootLimit = txtLTLCFCAvgLbPerCFLimit.Text.ToDecimal();
			cfcRule.AveragePoundPerCubicFootApplies = chkLTLCFCAVgLbPerCFLimitIsApplicable.Checked;
			cfcRule.PenaltyPoundPerCubicFoot = txtLTLCFCPenaltyLbPerCF.Text.ToDecimal();
			cfcRule.EffectiveDate = txtLTLCFCEffectiveDate.Text.ToDateTime();
			cfcRule.VendorRating = rating;
			cfcRule.TenantId = rating.TenantId;

			if (isNew)
			{
				cfcRules.Add(cfcRule);
			}
			else
			{
				cfcRules[editingIndex] = cfcRule;
			}

			StoredCfcRules = cfcRules;

			lstCFCRules.DataSource = SortLtlcfCapacityRules(cfcRules);
			lstCFCRules.DataBind();
			tabLTLCubicFootCapacityRules.HeaderText = cfcRules.BuildTabCount(LtlCfCapacityRulesHeader);
            athtuTabUpdater.SetForUpdate(tabLTLCubicFootCapacityRules.ClientID, cfcRules.BuildTabCount(LtlCfCapacityRulesHeader));

			pnlEditLTLCFC.Visible = false;
			pnlDimScreen.Visible = false;
		}



		private void LoadLTLOverLengthRuleForEdit(LTLOverLengthRule rule)
		{
			txtLTLOverlengthRuleLowerBound.Text = rule.LowerLengthBound.ToString();
			txtLTLOverlengthRuleUpperBound.Text = rule.UpperLengthBound.ToString();
			txtLTLOverlengthRuleCharge.Text = rule.Charge.ToString();
            ddlLTLOverlengthRuleChargeCode.SelectedValue = rule.ChargeCodeId.GetString();
			txtLTLOverlengthRuleEffectiveDate.Text = rule.EffectiveDate.FormattedShortDate();
		}

		protected void OnAddLTLOverlengthRuleClicked(object sender, EventArgs e)
		{
			hidEditingIndex.Value = (WebApplicationConstants.InvalidIndex).ToString();
			LoadLTLOverLengthRuleForEdit(new LTLOverLengthRule { EffectiveDate = DateTime.Now });
			pnlEditLTLOverlengthRule.Visible = true;
			pnlDimScreen.Visible = true;
		}

		protected void OnClearLTLOverlengthRulesClicked(object sender, EventArgs e)
		{
			StoredOverlengthRules = new List<LTLOverLengthRule>();

			lstLTLOverlengthRules.DataSource = new List<LTLOverLengthRule>();
			lstLTLOverlengthRules.DataBind();
			tabLTLOverlengthChargeRules.HeaderText = LtlOverLengthRulesHeader;
            athtuTabUpdater.SetForUpdate(tabLTLOverlengthChargeRules.ClientID, LtlOverLengthRulesHeader);
        }

		protected void OnEditLTLOverlengthRuleClicked(object sender, ImageClickEventArgs e)
		{
			var imageButton = (ImageButton)sender;

			var overlenghtRules = SortLTLOverlengthRules(StoredOverlengthRules);

			var itemIdx = imageButton.FindControl("hidLTLOverlengthRuleIndex").ToCustomHiddenField().Value.ToInt();
			hidEditingIndex.Value = itemIdx.ToString();

			var overlengthRule = overlenghtRules[itemIdx];

			LoadLTLOverLengthRuleForEdit(overlengthRule);

			pnlEditLTLOverlengthRule.Visible = true;
			pnlDimScreen.Visible = true;
		}

		protected void OnDeleteLTLOverlengthRuleClicked(object sender, ImageClickEventArgs e)
		{
			var imageButton = (ImageButton)sender;

			var overlengthRules = SortLTLOverlengthRules(StoredOverlengthRules);
			var itemIdx = imageButton.FindControl("hidLTLOverlengthRuleIndex").ToCustomHiddenField().Value.ToInt();

			overlengthRules.RemoveAt(itemIdx);

			StoredOverlengthRules = overlengthRules;

			lstLTLOverlengthRules.DataSource = overlengthRules;
			lstLTLOverlengthRules.DataBind();
			tabLTLOverlengthChargeRules.HeaderText = overlengthRules.BuildTabCount(LtlOverLengthRulesHeader);
            athtuTabUpdater.SetForUpdate(tabLTLOverlengthChargeRules.ClientID, overlengthRules.BuildTabCount(LtlOverLengthRulesHeader));
        }

		protected void OnEditLTLOverlengthRuleDoneClicked(object sender, EventArgs e)
		{
			var overlengthRules = SortLTLOverlengthRules(StoredOverlengthRules);
			var rating = new VendorRating(hidVendorRatingId.Value.ToLong());

			var editingIndex = hidEditingIndex.Value.ToInt();
			var isNew = editingIndex == WebApplicationConstants.InvalidIndex;

			var overlengthRule = isNew ? new LTLOverLengthRule { EffectiveDate = DateTime.Now } : overlengthRules[editingIndex];

			if (!isNew && !overlengthRule.HasChanges())
				overlengthRule.TakeSnapShot();

			overlengthRule.ChargeCodeId = ddlLTLOverlengthRuleChargeCode.SelectedValue.ToLong();
			overlengthRule.LowerLengthBound = txtLTLOverlengthRuleLowerBound.Text.ToDecimal();
			overlengthRule.UpperLengthBound = txtLTLOverlengthRuleUpperBound.Text.ToDecimal();
			overlengthRule.Charge = txtLTLOverlengthRuleCharge.Text.ToDecimal();
			overlengthRule.EffectiveDate = txtLTLOverlengthRuleEffectiveDate.Text.ToDateTime();
			overlengthRule.VendorRating = rating;
			overlengthRule.TenantId = rating.TenantId;

			if (isNew)
			{
				overlengthRules.Add(overlengthRule);
			}
			else
			{
				overlengthRules[editingIndex] = overlengthRule;
			}

			StoredOverlengthRules = overlengthRules;

			lstLTLOverlengthRules.DataSource = SortLTLOverlengthRules(overlengthRules);
			lstLTLOverlengthRules.DataBind();
			tabLTLOverlengthChargeRules.HeaderText = overlengthRules.BuildTabCount(LtlOverLengthRulesHeader);
            athtuTabUpdater.SetForUpdate(tabLTLOverlengthChargeRules.ClientID, overlengthRules.BuildTabCount(LtlOverLengthRulesHeader));

			pnlEditLTLOverlengthRule.Visible = false;
			pnlDimScreen.Visible = false;
		}

		protected void OnCloseEditLTLOverlengthRuleClicked(object sender, EventArgs e)
		{
			pnlEditLTLOverlengthRule.Visible = false;
			pnlDimScreen.Visible = false;
		}



		private void LoadLTLAdditionaChargeForEdit(LTLAdditionalCharge charge)
		{
			txtLTLAdditionalChargeName.Text = charge.Name;
            ddlLTLAdditionalChargeChargeCode.SelectedValue = charge.ChargeCodeId.GetString();
			txtLTLAdditionalChargeEffectiveDate.Text = charge.EffectiveDate.FormattedShortDate();

			StoredAddlChargeEdit = charge;

			pelstEditLTLAdditionalChargeIndices.LoadData(charge.AdditionalChargeIndices);
		}

		private void UpdateEditAdditionalChargeIndices(LTLAdditionalCharge charge)
		{
			var aci = lstEditLTLAdditionalChargeIndices
				.Items
				.Select(cp => new
				{
					EditIdx = cp.FindControl("hidAdditionalChargeIndexItemIndex").ToCustomHiddenField().Value.ToInt(),
					ChgIdx = new AdditionalChargeIndex(cp.FindControl("hidAdditionalChargeIndexId").ToCustomHiddenField().Value.ToLong(), true)
					{
						RegionId = cp.FindControl("ddlAdditionalChargeRegion").ToDropDownList().SelectedValue.ToLong(),
                        UsePostalCode = cp.FindControl("chkAdditionalChargeIndexUsePostalCode").ToAltUniformCheckBox().Checked,
						PostalCode = cp.FindControl("txtAdditionalChargeIndexPostalCode").ToTextBox().Text,
						CountryId = cp.FindControl("ddlAdditionalChargeCountries").ToDropDownList().SelectedValue.ToLong(),
                        ApplyOnOrigin = cp.FindControl("chkAdditionalChargeIndexApplyOnOrigin").ToAltUniformCheckBox().Checked,
                        ApplyOnDestination = cp.FindControl("chkAdditionalChargeIndexApplyOnDestination").ToAltUniformCheckBox().Checked,
						RateType = cp.FindControl("ddlAdditionalChargeIndexRateType").ToDropDownList().SelectedValue.ToInt().ToEnum<RateType>(),
						Rate = cp.FindControl("txtAdditionalChargeIndexRate").ToTextBox().Text.ToDecimal(),
						ChargeFloor = cp.FindControl("txtAdditionalChargeIndexChargeFloor").ToTextBox().Text.ToDecimal(),
						ChargeCeiling = cp.FindControl("txtAdditionalChargeIndexChargeCeiling").ToTextBox().Text.ToDecimal(),
					}
				})
				.ToList();
			var data = pelstEditLTLAdditionalChargeIndices.GetData<AdditionalChargeIndex>();
			foreach (var item in aci)
			{
				data[item.EditIdx] = item.ChgIdx;
				data[item.EditIdx].LTLAdditionalCharge = charge;
				data[item.EditIdx].TenantId = charge.TenantId;
			}
			charge.AdditionalChargeIndices = data;
		}



		protected void OnAddLTLAdditionalChargeClicked(object sender, EventArgs e)
		{
			hidEditingIndex.Value = (WebApplicationConstants.InvalidIndex).ToString();
			LoadLTLAdditionaChargeForEdit(new LTLAdditionalCharge
				{
					EffectiveDate = DateTime.Now,
					AdditionalChargeIndices = new List<AdditionalChargeIndex>()
				});
			pnlEditLTLAdditionalCharge.Visible = true;
			pnlDimScreen.Visible = true;
		}

		protected void OnClearLTLAdditionalChargesClicked(object sender, EventArgs e)
		{
			StoredAddlCharges = new List<LTLAdditionalCharge>();

			lstLTLAdditionalCharges.DataSource = new List<LTLAdditionalCharge>();
			lstLTLAdditionalCharges.DataBind();
			tabLTLAdditionalCharges.HeaderText = LtlAdditionalChargesHeader;
            athtuTabUpdater.SetForUpdate(tabLTLAdditionalCharges.ClientID, LtlAdditionalChargesHeader);
        }

		protected void OnEditLTLAdditionalChargeClicked(object sender, ImageClickEventArgs e)
		{
			var imageButton = (ImageButton)sender;

			var additionalCharges = SortLTLAdditionalCharge(StoredAddlCharges);
			var itemIdx = imageButton.FindControl("hidLTLAdditionalChargeIndex").ToCustomHiddenField().Value.ToInt();

			hidEditingIndex.Value = itemIdx.ToString();

			var additionalCharge = additionalCharges[itemIdx];

			LoadLTLAdditionaChargeForEdit(additionalCharge);

			pnlEditLTLAdditionalCharge.Visible = true;
			pnlDimScreen.Visible = true;
		}

		protected void OnEditLTLAdditionalChargeDoneClicked(object sender, EventArgs e)
		{
			var additionalCharges = SortLTLAdditionalCharge(StoredAddlCharges);
			var rating = new VendorRating(hidVendorRatingId.Value.ToLong());

			var isNew = hidEditingIndex.Value.ToInt() == WebApplicationConstants.InvalidIndex;
			var editingIndex = hidEditingIndex.Value.ToInt();

			var additionalCharge = isNew
									   ? new LTLAdditionalCharge
										   {
											   EffectiveDate = DateTime.Now,
											   AdditionalChargeIndices = new List<AdditionalChargeIndex>()
										   }
									   : additionalCharges[editingIndex];

			if (!isNew && !additionalCharge.HasChanges())
				additionalCharge.TakeSnapShot();

			additionalCharge.Name = txtLTLAdditionalChargeName.Text;
			additionalCharge.EffectiveDate = txtLTLAdditionalChargeEffectiveDate.Text.ToDateTime();
			additionalCharge.ChargeCodeId = ddlLTLAdditionalChargeChargeCode.SelectedValue.ToLong();
			additionalCharge.VendorRating = rating;
			additionalCharge.TenantId = rating.TenantId;
			UpdateEditAdditionalChargeIndices(additionalCharge);
			if (additionalCharge.AdditionalChargeIndices == null) additionalCharge.AdditionalChargeIndices = new List<AdditionalChargeIndex>();

			//Empty out the edit panel controls and control state 
			StoredAddlChargeEdit = new LTLAdditionalCharge();
			pelstEditLTLAdditionalChargeIndices.LoadData(new List<LTLAdditionalCharge>());

			if (isNew) additionalCharges.Add(additionalCharge);
			else additionalCharges[editingIndex] = additionalCharge;

			StoredAddlCharges = SortLTLAdditionalCharge(additionalCharges);

			lstLTLAdditionalCharges.DataSource = StoredAddlCharges;
			lstLTLAdditionalCharges.DataBind();
			tabLTLAdditionalCharges.HeaderText = additionalCharges.BuildTabCount(LtlAdditionalChargesHeader);
            athtuTabUpdater.SetForUpdate(tabLTLAdditionalCharges.ClientID, additionalCharges.BuildTabCount(LtlAdditionalChargesHeader));

			pnlEditLTLAdditionalCharge.Visible = false;
			pnlDimScreen.Visible = false;
		}

		protected void OnEditLTLAdditionalChargeCancelClicked(object sender, EventArgs e)
		{
			//empty out the edit panel controls and control state
			StoredAddlChargeEdit = new LTLAdditionalCharge();
			pelstEditLTLAdditionalChargeIndices.LoadData(new List<AdditionalChargeIndex>());

			pnlEditLTLAdditionalCharge.Visible = false;
			pnlDimScreen.Visible = false;
		}

		protected void OnDeleteLTLAdditionalChargeClicked(object sender, ImageClickEventArgs e)
		{
			var imageButton = (ImageButton)sender;

			var additionalCharges = SortLTLAdditionalCharge(StoredAddlCharges);
			var itemIdx = imageButton.FindControl("hidLTLAdditionalChargeIndex").ToCustomHiddenField().Value.ToInt();

			additionalCharges.RemoveAt(itemIdx);

			StoredAddlCharges = additionalCharges;

			lstLTLAdditionalCharges.DataSource = additionalCharges;
			lstLTLAdditionalCharges.DataBind();
			tabLTLAdditionalCharges.HeaderText = additionalCharges.BuildTabCount(LtlAdditionalChargesHeader);
            athtuTabUpdater.SetForUpdate(tabLTLAdditionalCharges.ClientID, additionalCharges.BuildTabCount(LtlAdditionalChargesHeader));
        }

		protected void OnEditLTLAdditionalChargeIndexItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var item = (ListViewDataItem)e.Item;
			var data = item.DataItem as AdditionalChargeIndex;
			var ddlRateType = item.FindControl("ddlAdditionalChargeIndexRateType").ToDropDownList();
			var ddlRegions = item.FindControl("ddlAdditionalChargeRegion").ToDropDownList();

			if (data == null) return;

			if (ddlRateType != null)
			{
				ddlRateType.DataSource = LTLAdditionalChargeIndexRateTypes;
				ddlRateType.DataBind();
				ddlRateType.SelectedValue = data.RateType.ToInt().ToString();
			}

			if (ddlRegions != null)
			{
				ddlRegions.DataSource = RegionCollection;
				ddlRegions.DataBind();
				ddlRegions.SelectedValue = data.RegionId.ToString();
			}
		}

		protected void OnLTLAdditionalChargesItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var item = (ListViewDataItem)e.Item;
            var data = (LTLAdditionalCharge)item.DataItem;
			var pe = item.FindControl("peLstLTLAdditionalChargeIndices").ToPaginationExtender();
		    pe.ParentDataStoreKey = string.Format("{0}-{1}", AdditionalChargeIndicesStoreKey, pe.ClientID);
			pe.LoadData(SortLTLAdditionalChargeIndices(data.AdditionalChargeIndices));
		}

        protected void OnLTLAdditionChargesDataBound(object sender, EventArgs e)
        {
            // clear out collections no longer present on the page
            var keys = lstLTLAdditionalCharges.Items.Select(i => i.FindControl("peLstLTLAdditionalChargeIndices").ToPaginationExtender().ParentDataStoreKey).ToList();
            var itemsToRemove = PageStore.Where(i => i.Key.StartsWith(AdditionalChargeIndicesStoreKey) && !keys.Contains(i.Key)).Select(i => i.Key).ToArray();
            foreach (var item in itemsToRemove)
                PageStore.Remove(item);
        }

	    protected void OnAddLTLAddtionalChargeIndexClicked(object sender, EventArgs e)
		{
			var charge = StoredAddlChargeEdit;
			UpdateEditAdditionalChargeIndices(charge);

			if (charge.AdditionalChargeIndices.Count <= 500)
				charge.AdditionalChargeIndices.Add(new AdditionalChargeIndex { RateType = RateType.Flat, LTLAdditionalCharge = charge, TenantId = charge.TenantId, });
			else return;

			StoredAddlChargeEdit = charge;

			pelstEditLTLAdditionalChargeIndices.LoadData(charge.AdditionalChargeIndices);
			pelstEditLTLAdditionalChargeIndices.LoadPageWithRecordIndex(charge.AdditionalChargeIndices.Count - 1);
		}

		protected void OnResortEditLTLAddtionalChargeIndexClicked(object sender, EventArgs e)
		{
			var charge = StoredAddlChargeEdit;
			UpdateEditAdditionalChargeIndices(charge);

			charge.AdditionalChargeIndices = SortLTLAdditionalChargeIndices(charge.AdditionalChargeIndices);
			StoredAddlChargeEdit = charge;

			pelstEditLTLAdditionalChargeIndices.LoadData(charge.AdditionalChargeIndices, pelstEditLTLAdditionalChargeIndices.CurrentPage);
		}

		protected void OnDeleteAdditionalChargeIndexClicked(object sender, ImageClickEventArgs e)
		{
			var button = (ImageButton)sender;

			var charge = StoredAddlChargeEdit;
			var itemIdx = button.Parent.FindControl("hidAdditionalChargeIndexItemIndex").ToCustomHiddenField().Value.ToInt();

			UpdateEditAdditionalChargeIndices(charge);

			charge.AdditionalChargeIndices.RemoveAt(itemIdx);

			StoredAddlChargeEdit = charge;

			pelstEditLTLAdditionalChargeIndices.LoadData(charge.AdditionalChargeIndices, pelstEditLTLAdditionalChargeIndices.CurrentPage);
		}

		protected void OnEditLTLAdditionalChargeIndexChange(object sender, EventArgs e)
		{
			var aci = lstEditLTLAdditionalChargeIndices
				.Items
				.Select(cp => new
					{
						EditIdx = cp.FindControl("hidAdditionalChargeIndexItemIndex").ToCustomHiddenField().Value.ToInt(),
						ChgIdx = new AdditionalChargeIndex(cp.FindControl("hidAdditionalChargeIndexId").ToCustomHiddenField().Value.ToLong())
									  {
										  RegionId = cp.FindControl("ddlAdditionalChargeRegion").ToDropDownList().SelectedValue.ToLong(),
                                          UsePostalCode = cp.FindControl("chkAdditionalChargeIndexUsePostalCode").ToAltUniformCheckBox().Checked,
										  PostalCode = cp.FindControl("txtAdditionalChargeIndexPostalCode").ToTextBox().Text,
										  CountryId = cp.FindControl("ddlAdditionalChargeCountries").ToDropDownList().SelectedValue.ToLong(),
										  ApplyOnOrigin = cp.FindControl("chkAdditionalChargeIndexApplyOnOrigin").ToAltUniformCheckBox().Checked,
                                          ApplyOnDestination = cp.FindControl("chkAdditionalChargeIndexApplyOnDestination").ToAltUniformCheckBox().Checked,
										  RateType = cp.FindControl("ddlAdditionalChargeIndexRateType").ToDropDownList().SelectedValue.ToInt().ToEnum<RateType>(),
										  Rate = cp.FindControl("txtAdditionalChargeIndexRate").ToTextBox().Text.ToDecimal(),
										  ChargeFloor = cp.FindControl("txtAdditionalChargeIndexChargeFloor").ToTextBox().Text.ToDecimal(),
										  ChargeCeiling = cp.FindControl("txtAdditionalChargeIndexChargeCeiling").ToTextBox().Text.ToDecimal(),
									  }
					})
				.ToList();
			var data = pelstEditLTLAdditionalChargeIndices.GetData<AdditionalChargeIndex>();
			foreach (var item in aci)
				data[item.EditIdx] = item.ChgIdx;
			pelstEditLTLAdditionalChargeIndices.LoadData(data, pelstEditLTLAdditionalChargeIndices.CurrentPage);
		}



		protected void OnVendorRatingMatchingRegionSearchClose(object sender, EventArgs e)
		{
			vendorRatingMatchingRegionSearch.Visible = false;
			pnlDimScreen.Visible = false;
		}

		protected void OnVendorRatingMathcingRegionSearchSelectedIndex(object sender, ViewEventArgs<int> e)
		{
			if (tabLTLDiscountTiers.Visible)
			{
				peLstLTLDiscountTiers.LoadPageWithRecordIndex(e.Argument);
				tabContainerMain.ActiveTab = tabLTLDiscountTiers;
				vendorRatingMatchingRegionSearch.Visible = false;
				pnlDimScreen.Visible = false;
			}
			else
			{
				DisplayMessages(new[] {ValidationMessage.Error("Discount Tiers current inaccesible.  Tab not visible!")});
			}
		}



		private void LoadLTLPcfToFcConvTabForEdit(LTLPcfToFcConversion convTab)
		{
			txtLTLPcfToFcConvTabConvTable.Text = convTab.ConversionTableData;
			txtLTLPcfToFcConvTabEffectiveDate.Text = convTab.EffectiveDate.FormattedShortDate();
		}

		private void ClearLTLPcfToFcConvTabs()
		{
			StoredPcfToFcConversions = new List<LTLPcfToFcConversion>();

			lstLTLPcfToFcConvTab.DataSource = new List<LTLPcfToFcConversion>();
			lstLTLPcfToFcConvTab.DataBind();
			tabLTLPcfToFcConvTab.HeaderText = LtlPcfToFcConvTabHeader;
            athtuTabUpdater.SetForUpdate(tabLTLPcfToFcConvTab.ClientID, LtlPcfToFcConvTabHeader);
        }

		protected void OnClearLTLPcfToFcConvTabClicked(object sender, EventArgs e)
		{
			ClearLTLPcfToFcConvTabs();
		}

		protected void OnAddLTLPcfToFcConvTabClicked(object sender, EventArgs e)
		{
			hidEditingIndex.Value = (WebApplicationConstants.InvalidIndex).ToString();
			LoadLTLPcfToFcConvTabForEdit(new LTLPcfToFcConversion
			{
				EffectiveDate = DateTime.Now,
				ConversionTableData = string.Empty
			});
			pnlEditLTLPcfToFcConvTab.Visible = true;
			pnlDimScreen.Visible = true;
		}

		protected void OnEditLTLPcfToFcConvTabClicked(object sender, ImageClickEventArgs e)
		{
			var imageButton = (ImageButton)sender;

			var conversionTables = SortLTLPcfToFcConversions(StoredPcfToFcConversions);

			var itemIdx = (imageButton.FindControl("hidLTLPcfToFcConvTabIndex")).ToCustomHiddenField().Value.ToInt();
			hidEditingIndex.Value = itemIdx.ToString();

			var conversionTable = conversionTables[itemIdx];

			LoadLTLPcfToFcConvTabForEdit(conversionTable);

			pnlEditLTLPcfToFcConvTab.Visible = true;
			pnlDimScreen.Visible = true;
		}

		protected void OnDeleteLTLPcfToFcConvTabClicked(object sender, ImageClickEventArgs e)
		{
			var imageButton = (ImageButton)sender;

			var conversionTables = SortLTLPcfToFcConversions(StoredPcfToFcConversions);
			var itemIdx = imageButton.FindControl("hidLTLPcfToFcConvTabIndex").ToCustomHiddenField().Value.ToInt();

			conversionTables.RemoveAt(itemIdx);

			StoredPcfToFcConversions = conversionTables;

			lstLTLPcfToFcConvTab.DataSource = conversionTables;
			lstLTLPcfToFcConvTab.DataBind();
			tabLTLPcfToFcConvTab.HeaderText = conversionTables.BuildTabCount(LtlPcfToFcConvTabHeader);
            athtuTabUpdater.SetForUpdate(tabLTLPcfToFcConvTab.ClientID, conversionTables.BuildTabCount(LtlPcfToFcConvTabHeader));
        }

		protected void OnCloseEditLTLPcfToFcConvTabClicked(object sender, EventArgs e)
		{
			pnlEditLTLPcfToFcConvTab.Visible = false;
			pnlDimScreen.Visible = false;
		}

		protected void OnEditLTLPcfToFcConvTabDoneClicked(object sender, EventArgs e)
		{
			var conversionTables = SortLTLPcfToFcConversions(StoredPcfToFcConversions);
			var rating = new VendorRating(hidVendorRatingId.Value.ToLong());


			var editingIndex = hidEditingIndex.Value.ToInt();
			var isNew = editingIndex == WebApplicationConstants.InvalidIndex;

			var conversionTable = isNew
									  ? new LTLPcfToFcConversion
										  {
											  EffectiveDate = DateTime.Now,
											  ConversionTableData = string.Empty
										  }
									  : conversionTables[editingIndex];

			if (!isNew && !conversionTable.HasChanges())
				conversionTable.TakeSnapShot();

			conversionTable.EffectiveDate = txtLTLPcfToFcConvTabEffectiveDate.Text.ToDateTime();
			conversionTable.ConversionTableData = txtLTLPcfToFcConvTabConvTable.Text;
			conversionTable.VendorRating = rating;
			conversionTable.TenantId = rating.TenantId;

			if (isNew)
			{
				conversionTables.Add(conversionTable);
			}
			else
			{
				conversionTables[editingIndex] = conversionTable;
			}

			StoredPcfToFcConversions = conversionTables;

			lstLTLPcfToFcConvTab.DataSource = SortLTLPcfToFcConversions(conversionTables);
			lstLTLPcfToFcConvTab.DataBind();
			tabLTLPcfToFcConvTab.HeaderText = conversionTables.BuildTabCount(LtlPcfToFcConvTabHeader);
            athtuTabUpdater.SetForUpdate(tabLTLPcfToFcConvTab.ClientID, conversionTables.BuildTabCount(LtlPcfToFcConvTabHeader));

			pnlEditLTLPcfToFcConvTab.Visible = false;
			pnlDimScreen.Visible = false;
		}



		protected void OnchkEnablePcfToFcConversionCheckChanged(object sender, EventArgs e)
		{
			if (!chkEnablePcfToFcConversion.Checked) ClearLTLPcfToFcConvTabs();
			tabLTLPcfToFcConvTab.Visible = chkEnablePcfToFcConversion.Checked;
		}

		protected void OnchkEnablePackageSpecificRatePackageTypeCheckChanged(object sender, EventArgs e)
		{
			if (!chkEnablePackageSpecificRatePackageType.Checked) ClearPackageSpecificRates();
			tabLTLPackageSpecificRates.Visible = chkEnablePackageSpecificRatePackageType.Checked;
			tabLTLDiscountTiers.Visible = !chkEnablePackageSpecificRatePackageType.Checked;
			tabLTLCubicFootCapacityRules.Visible = !chkEnablePackageSpecificRatePackageType.Checked;
			vendorRatingImportExport.EnableLTLPackageSpecificRatesImport = chkEnablePackageSpecificRatePackageType.Checked;
			vendorRatingImportExport.EnableLTLDiscountTierImport = !chkEnablePackageSpecificRatePackageType.Checked;
			vendorRatingImportExport.EnableLTLCFCRulesImport = !chkEnablePackageSpecificRatePackageType.Checked;
		}



		private void LoadPackageSpecificRateForEdit(LTLPackageSpecificRate psRate)
		{
			ddlLTLPackageSpecificRateOriginRegion.SelectedValue = psRate.OriginRegionId.ToString();
			ddlLTLPackageSpecificRateDestinationRegion.SelectedValue = psRate.DestinationRegionId.ToString();
			txtPackageSpecificRateEffectiveDate.Text = psRate.EffectiveDate.FormattedShortDate();
			txtLTLPackageSpecificRateRatePriority.Text = psRate.RatePriority.ToString();
			txtLTLPackageSpecificRateQuantity.Text = psRate.PackageQuantity.ToString();
			txtLTLPackageSpecificRateFlatRate.Text = psRate.Rate.ToString();
		}

		private void ClearPackageSpecificRates()
		{
			peLstPackageSpecificRates.LoadData(new List<LTLPackageSpecificRate>());

		    ddlPackageSpecificRatePackageType.SelectedValue = string.Empty;
            ddlLTLPackageSpecificRateFreightChargeCode.SelectedValue = string.Empty;

			tabLTLPackageSpecificRates.HeaderText = LtlPackageSpecificRateTabHeader;
		}

		protected void OnAddLTLPackageSpecificRateClicked(object sender, EventArgs e)
		{
			hidEditingIndex.Value = (WebApplicationConstants.InvalidIndex).ToString();
			LoadPackageSpecificRateForEdit(new LTLPackageSpecificRate { EffectiveDate = DateTime.Now });
			pnlEditPackageSpecificRate.Visible = true;
			pnlDimScreen.Visible = true;
		}

		protected void OnEditLTLPackageSpecificRateClicked(object sender, ImageClickEventArgs e)
		{
			var imageButton = (ImageButton)sender;

			var psRates = SortLTLPackageSpecificRates(StoredPackageSpecificRates);

			var pageIdx = peLstPackageSpecificRates.CurrentPage - 1;
			var itemIdx = imageButton.FindControl("hidPackageSpecificRateTabIndex").ToCustomHiddenField().Value.ToInt();
			hidEditingIndex.Value = itemIdx.ToString();

			var psRate = psRates[(pageIdx * peLstPackageSpecificRates.PageSize) + itemIdx];

			LoadPackageSpecificRateForEdit(psRate);

			pnlEditPackageSpecificRate.Visible = true;
			pnlDimScreen.Visible = true;
		}

		protected void OnDeleteLTLPackageSpecificRateClicked(object sender, ImageClickEventArgs e)
		{
			var imageButton = (ImageButton)sender;

			var psRates = SortLTLPackageSpecificRates(StoredPackageSpecificRates);

			var pageIdx = peLstPackageSpecificRates.CurrentPage - 1;
			var itemIdx = imageButton.FindControl("hidPackageSpecificRateTabIndex").ToCustomHiddenField().Value.ToInt();

			psRates.RemoveAt((pageIdx * peLstPackageSpecificRates.PageSize) + itemIdx);

			peLstPackageSpecificRates.LoadData(psRates, pageIdx + 1);

			tabLTLPackageSpecificRates.HeaderText = psRates.BuildTabCount(LtlPackageSpecificRateTabHeader);
            athtuTabUpdater.SetForUpdate(tabLTLPackageSpecificRates.ClientID, psRates.BuildTabCount(LtlPackageSpecificRateTabHeader));
		}

		protected void OnCloseEditLTLPackageSpecificRateClicked(object sender, EventArgs eventArgs)
		{
			pnlEditPackageSpecificRate.Visible = false;
			pnlDimScreen.Visible = false;
		}

		protected void OnEditLTLPackageSpecificRateDoneClicked(object sender, EventArgs e)
		{
			var psRates = SortLTLPackageSpecificRates(StoredPackageSpecificRates);
			var rating = new VendorRating(hidVendorRatingId.Value.ToLong());

			var pageIdx = peLstPackageSpecificRates.CurrentPage - 1;

			var editingIndex = (pageIdx * peLstPackageSpecificRates.PageSize) + hidEditingIndex.Value.ToInt();
			var isNew = hidEditingIndex.Value.ToInt() == WebApplicationConstants.InvalidIndex;

			var psRate = isNew
							 ? new LTLPackageSpecificRate { EffectiveDate = DateTime.Now }
							 : psRates[editingIndex];

			if (!isNew && !psRate.HasChanges())
				psRate.TakeSnapShot();

			psRate.OriginRegionId = ddlLTLPackageSpecificRateOriginRegion.SelectedValue.ToLong();
			psRate.DestinationRegionId = ddlLTLPackageSpecificRateDestinationRegion.SelectedValue.ToLong();
			psRate.Rate = txtLTLPackageSpecificRateFlatRate.Text.ToDecimal();
			psRate.RatePriority = txtLTLPackageSpecificRateRatePriority.Text.ToInt();
			psRate.PackageQuantity = txtLTLPackageSpecificRateQuantity.Text.ToInt();
			psRate.EffectiveDate = txtPackageSpecificRateEffectiveDate.Text.ToDateTime();
			psRate.VendorRating = rating;
			psRate.TenantId = rating.TenantId;

			if (isNew)
			{
				psRates.Add(psRate);
			}
			else
			{
				psRates[editingIndex] = psRate;
			}

			peLstPackageSpecificRates.LoadData(SortLTLPackageSpecificRates(psRates), peLstPackageSpecificRates.CurrentPage);
			tabLTLPackageSpecificRates.HeaderText = psRates.BuildTabCount(LtlPackageSpecificRateTabHeader);
            athtuTabUpdater.SetForUpdate(tabLTLPackageSpecificRates.ClientID, psRates.BuildTabCount(LtlPackageSpecificRateTabHeader));

			pnlEditPackageSpecificRate.Visible = false;
			pnlDimScreen.Visible = false;
		}

		protected void OnClearLTLPackageSpecificRatesClicked(object sender, EventArgs e)
		{
			peLstPackageSpecificRates.LoadData(new List<LTLPackageSpecificRate>());
			tabLTLPackageSpecificRates.HeaderText = LtlPackageSpecificRateTabHeader;
            athtuTabUpdater.SetForUpdate(tabLTLPackageSpecificRates.ClientID, LtlPackageSpecificRateTabHeader);
		}


	    protected void OnFuelTableSelectedIndexChanged(object sender, EventArgs e)
	    {
            hypFuelTable.HRef = string.Format("{0}?{1}={2}", FuelTableView.PageAddress, WebApplicationConstants.TransferNumber, ddlFuelTable.SelectedValue.UrlTextEncrypt());
	        hypFuelTable.Visible = ddlFuelTable.SelectedValue.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.FuelTable);
	    }


        private void LoadLTLGuaranteedChargeForEdit(LTLGuaranteedCharge guaranteedCharge)
        {
            txtGuaranteedChargeDescription.Text = guaranteedCharge.Description;
            txtGuaranteedChargeTime.Text = guaranteedCharge.Time;
            ddlGuarateedChargeChargeCode.SelectedValue = guaranteedCharge.ChargeCodeId.GetString();
            ddlGuarateedChargeRateType.SelectedValue = guaranteedCharge.RateType.ToInt().ToString();
            txtGuaranteedChargeRate.Text = guaranteedCharge.Rate.ToString();
            txtGuaranteedChargeFloorValue.Text = guaranteedCharge.FloorValue.ToString();
            txtGuaranteedChargeCeilingValue.Text = guaranteedCharge.CeilingValue.ToString();
            txtGuaranteedChargeEffectiveDate.Text = guaranteedCharge.EffectiveDate.FormattedShortDate();
            txtGuaranteedChargeCriticalNotes.Text = guaranteedCharge.CriticalNotes;
        }

	    protected void OnAddLTLGuaranteedChargeClicked(object sender, EventArgs e)
	    {
            hidEditingIndex.Value = (WebApplicationConstants.InvalidIndex).ToString();
            LoadLTLGuaranteedChargeForEdit(new LTLGuaranteedCharge { EffectiveDate = DateTime.Now });
            pnlEditLTLGuaranteedCharge.Visible = true;
            pnlDimScreen.Visible = true;
	    }

	    protected void OnClearLTLGuaranteedChargesClicked(object sender, EventArgs e)
	    {
            StoredGuaranteedCharges = new List<LTLGuaranteedCharge>();

            lstLTLGuaranteedCharges.DataSource = new List<LTLGuaranteedCharge>();
            lstLTLGuaranteedCharges.DataBind();
            tabLTLGuaranteedCharges.HeaderText = LtlGuaranteedChargesTabHeader;
            athtuTabUpdater.SetForUpdate(tabLTLGuaranteedCharges.ClientID, LtlGuaranteedChargesTabHeader);
	    }

	    protected void OnEditLTLGuaranteedChargeClicked(object sender, ImageClickEventArgs e)
	    {
            var imageButton = ((ImageButton)sender);

            var guaranteedCharges = SortLTLGuaranteedCharges(StoredGuaranteedCharges);

            var itemIdx = imageButton.FindControl("hidLTLGuaranteedChargeIndex").ToCustomHiddenField().Value.ToInt();
            hidEditingIndex.Value = itemIdx.ToString();

            LoadLTLGuaranteedChargeForEdit(guaranteedCharges[itemIdx]);

            pnlEditLTLGuaranteedCharge.Visible = true;
            pnlDimScreen.Visible = true;
	    }

	    protected void OnDeleteLTLGuaranteedChargeClicked(object sender, ImageClickEventArgs e)
	    {
            var imageButton = (ImageButton)sender;

            var guaranteedCharges = SortLTLGuaranteedCharges(StoredGuaranteedCharges);
            var itemIdx = imageButton.FindControl("hidLTLGuaranteedChargeIndex").ToCustomHiddenField().Value.ToInt();

            guaranteedCharges.RemoveAt(itemIdx);

            StoredGuaranteedCharges = guaranteedCharges;

            lstLTLGuaranteedCharges.DataSource = guaranteedCharges;
            lstLTLGuaranteedCharges.DataBind();
            tabLTLGuaranteedCharges.HeaderText = guaranteedCharges.BuildTabCount(LtlGuaranteedChargesTabHeader);
            athtuTabUpdater.SetForUpdate(tabLTLGuaranteedCharges.ClientID, guaranteedCharges.BuildTabCount(LtlGuaranteedChargesTabHeader));
	    }

	    protected void OnEditLTLGuaranteedChargeDoneClicked(object sender, EventArgs e)
	    {
            var guaranteedCharges = SortLTLGuaranteedCharges(StoredGuaranteedCharges);
            var rating = new VendorRating(hidVendorRatingId.Value.ToLong());

            var editingIndex = hidEditingIndex.Value.ToInt();
            var isNew = editingIndex == WebApplicationConstants.InvalidIndex;
            var guaranteedCharge = isNew ? new LTLGuaranteedCharge{ EffectiveDate = DateTime.Now } : guaranteedCharges[editingIndex];

            if (!isNew && !guaranteedCharge.HasChanges())
                guaranteedCharge.TakeSnapShot();

            guaranteedCharge.Description = txtGuaranteedChargeDescription.Text;
            guaranteedCharge.Time = txtGuaranteedChargeTime.Text;
            guaranteedCharge.RateType = ddlGuarateedChargeRateType.SelectedValue.ToEnum<RateType>();
            guaranteedCharge.Rate = txtGuaranteedChargeRate.Text.ToDecimal();
            guaranteedCharge.FloorValue = txtGuaranteedChargeFloorValue.Text.ToDecimal();
            guaranteedCharge.CeilingValue = txtGuaranteedChargeCeilingValue.Text.ToDecimal();
            guaranteedCharge.EffectiveDate = txtGuaranteedChargeEffectiveDate.Text.ToDateTime();
            guaranteedCharge.ChargeCodeId = ddlGuarateedChargeChargeCode.SelectedValue.ToLong();
	        guaranteedCharge.CriticalNotes = txtGuaranteedChargeCriticalNotes.Text;
            
            guaranteedCharge.VendorRating = rating;
            guaranteedCharge.TenantId = rating.TenantId;

            if (isNew)
            {
                guaranteedCharges.Add(guaranteedCharge);
            }
            else
            {
                guaranteedCharges[editingIndex] = guaranteedCharge;
            }

            StoredGuaranteedCharges = guaranteedCharges;

            lstLTLGuaranteedCharges.DataSource = SortLTLGuaranteedCharges(guaranteedCharges);
            lstLTLGuaranteedCharges.DataBind();
            tabLTLGuaranteedCharges.HeaderText = guaranteedCharges.BuildTabCount(LtlGuaranteedChargesTabHeader);
            athtuTabUpdater.SetForUpdate(tabLTLGuaranteedCharges.ClientID, guaranteedCharges.BuildTabCount(LtlGuaranteedChargesTabHeader));

            pnlEditLTLGuaranteedCharge.Visible = false;
            pnlDimScreen.Visible = false;
	    }

	    protected void OnCloseEditLTLGuaranteedChargeClicked(object sender, EventArgs e)
	    {
            pnlEditLTLGuaranteedCharge.Visible = false;
            pnlDimScreen.Visible = pnlEditLTLGuaranteedCharge.Visible;
	    }


	    protected void OnProject44ProfileCheckChanged(object sender, EventArgs e)
	    {
	        pnlLTLPcfToFcConvTab.Enabled = !chkProject44Profile.Checked;
	        pnlLTLDiscountTiers.Enabled = !chkProject44Profile.Checked;
	        pnlLTLAccessorials.Enabled = !chkProject44Profile.Checked;
	        pnlLTLCubicFootCapacityRules.Enabled = !chkProject44Profile.Checked;
	        pnlLTLOverlengthRules.Enabled = !chkProject44Profile.Checked;
	        pnlLTLAdditionalCharges.Enabled = !chkProject44Profile.Checked;
	        pnlLTLPackageSpecificRatesTab.Enabled = !chkProject44Profile.Checked;
	        pnlLTLGuaranteedCharges.Enabled = !chkProject44Profile.Checked;
        }
	}
}