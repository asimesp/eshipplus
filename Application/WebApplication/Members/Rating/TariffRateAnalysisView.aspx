﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="TariffRateAnalysisView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Rating.TariffRateAnalysisView" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowMore="true" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                CssClass="pageHeaderIcon" />
            <h3>Tariff Rate Analysis
            <small class="ml10">
                <asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>

        <script type="text/javascript">
            $(document).ready(function () {

                $("div[id$='instructionDiv']").hide();
                $("div[id$='pnlDimScreenJS']").hide();

                $(jsHelper.AddHashTag("importantInformation")).click(function (e) {
                    e.preventDefault();
                    $("div[id$='instructionDiv']").show("slow");
                    $("div[id$='pnlDimScreenJS']").show();
                });

                $(jsHelper.AddHashTag("instructionDiv")).click(function () {
                    $(this).hide("slow", function () { $("div[id$='pnlDimScreenJS']").hide(); });
                });

                CheckBoxChanged();
                $(jsHelper.AddHashTag("tariffs input[type='checkbox']")).change(CheckBoxChanged);
            });

            function CheckBoxChanged() {
                var chkFilter = jsHelper.AddHashTag("tariffs input[type='checkbox']");
                var selectedCount = $(chkFilter).filter(":checked").length;

                // set count display
                $(jsHelper.AddHashTag('selectedRatingCountSpan')).text('( selected: ' + selectedCount + ' of 5 )');

                // check to disable
                if (selectedCount >= 5) {
                    $(chkFilter).not(":checked").attr("disabled", "disabled");
                } else
                    $(chkFilter).removeAttr("disabled");
                $.uniform.update();
            }
        </script>
        <span class="clearfix"></span>
        <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
        <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
            OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
        <div class="imp_note mb10">
            <label class=" note">
                &#8226; Enter all required parameters then submit analysis. <span class="red">*</span> = required. 
                    <a href="#" id="importantInformation" class="blue">Click here</a> for lane input file specifications.
            </label>
        </div>

        <div class="rowgroup">
            <div class="col_1_3 bbox no-right-border">
                <h5>Rating Information</h5>
                <div class="row">
                    <div class="fieldgroup">
                        <label class="wlabel">Origin Postal Code</label>
                        <eShip:CustomTextBox runat="server" ID="txtOriginPostalCode" CssClass="w200" />
                    </div>
                </div>
                <div class="row">
                    <div class="fieldgroup">
                        <label class="wlabel">Origin Country</label>
                        <asp:DropDownList runat="server" ID="txtOriginCountry" CssClass="w200">
                            <asp:ListItem Value="USA" Text="USA" Selected="True" />
                            <asp:ListItem Value="CAN" Text="Canada" Selected="False" />
                            <asp:ListItem Value="MEX" Text="Mexico" Selected="False" />
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="row">
                    <div class="fieldgroup">
                        <label class="wlabel">Destination Postal Code</label>
                        <eShip:CustomTextBox runat="server" ID="txtDestPostalCode" CssClass="w200" />
                    </div>
                </div>
                <div class="row">
                    <div class="fieldgroup">
                        <label class="wlabel">Destination Country</label>
                        <asp:DropDownList runat="server" ID="ddlDestCountry" CssClass="w200">
                            <asp:ListItem Value="USA" Text="USA" Selected="True" />
                            <asp:ListItem Value="CAN" Text="Canada" Selected="False" />
                            <asp:ListItem Value="MEX" Text="Mexico" Selected="False" />
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="row">
                    <div class="fieldgroup">
                        <label class="wlabel">Weight Break</label>
                        <asp:DropDownList runat="server" ID="ddlWeightBreak" DataTextField="Value" DataValueField="Key" CssClass="w200" />
                    </div>
                </div>
                <div class="row">
                    <div class="fieldgroup pt10">
                        <asp:CheckBox runat="server" ID="chkDensityRates" ToolTip="Check to run density rates for tariffs selected" CssClass="jQueryUniform" />
                        <label>Density Rate</label>
                    </div>
                </div>
            </div>
            <div class="col_2_3 bbox pl20 vlinedarkleft">
                <h5>Available Tariffs <span id="selectedRatingCountSpan" class="fs75em pl20">( selected: 0 of 5 )</span></h5>
                <div class="row sm_chk finderScroll h300" id="tariffs">
                    <asp:Repeater runat="server" ID="rptTariffs">
                        <ItemTemplate>
                            <div class="fieldgroup pt5 pb5 middle">
                                <asp:CheckBox runat="server" ID="chkSelected" CssClass="jQueryUniform" />
                                <eShip:CustomHiddenField runat="server" ID="hidTariff" Value='<%# Eval("Value") %>' />
                                <label class="w130 fs75em lhInherit"><%# Eval("Text") %></label>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
        <hr class="dark mb20" />
        <div class="rowgroup">
            <h5>Items</h5>
            <asp:UpdatePanel runat="server" ID="upPnlItems" UpdateMode="Conditional">
                <ContentTemplate>
                    <table class="stripe">
                        <tr>
                            <th style="width: 5%;">
                                <asp:ImageButton runat="server" ID="addItem" Text="Add Item" CausesValidation="False"
                                    OnClick="OnAddItemClicked" ImageUrl="~/images/icons2/add.png" />
                            </th>
                            <th style="width: 5%;">Item #
                            </th>
                            <th style="width: 15%;">Weight (lb)
                            </th>
                            <th style="width: 15%;">Freight Class
                            </th>
                            <th style="width: 15%;">Length (in)
                            </th>
                            <th style="width: 15%;">Width (in)
                            </th>
                            <th style="width: 15%;">Height (in)
                            </th>
                            <th style="width: 15%;">Quantity
                            </th>
                        </tr>
                        <asp:Repeater runat="server" ID="rptItems" OnItemDataBound="OnItemsItemDataBound">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:ImageButton runat="server" ID="delete" Text="Delete Item" CausesValidation="False"
                                            ImageUrl="~/images/icons2/deleteX.png" OnClick="OnDeleteItemClicked" />
                                        <eShip:CustomHiddenField runat="server" ID="hidIndex" Value='<%# Container.ItemIndex %>' />
                                    </td>
                                    <td>
                                        <%# string.Format("{0}.", Container.ItemIndex+1) %>
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtWeight" CssClass="w100" Text='<%# Eval("Weight") %>' Type="FloatingPointNumbers" />
                                        <asp:RequiredFieldValidator runat="server" ID="rfvInsurancePurchaseFloor" ControlToValidate="txtWeight"
                                            ErrorMessage="weight is required" Text="*" Display="Dynamic" />
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="ddlFreightClass" CssClass="w60" />
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtLength" CssClass="w100" Text='<%# Eval("Length") %>' Type="FloatingPointNumbers" />
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtWidth" CssClass="w100" Text='<%# Eval("Width") %>' Type="FloatingPointNumbers" />
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtHeight" CssClass="w100" Text='<%# Eval("Height") %>' Type="FloatingPointNumbers" />
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox runat="server" ID="txtQuantity" CssClass="w100" Text='<%# Eval("Quantity") %>' Type="NumbersOnly" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="row mt20">
                <div class="fieldgroup">
                    <asp:Button ID="btnRate" runat="server" Text="Rate" OnClick="OnRateClicked" CssClass="mr10" />
                    <asp:Button ID="btnExport" runat="server" Text="Export" OnClick="OnExportClicked" CssClass="mr10" />
                    <asp:Button ID="btnRateBatch" runat="server" Text="Batch Rate" CausesValidation="False"
                        OnClick="OnBatchRateClicked" />
                </div>
            </div>
        </div>

        <hr class="fat" />
        <h5>
            <asp:Literal runat="server" ID="ratingType" />
        </h5>
        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheSmcResultsTable" TableId="smcResultsTable" HeaderZIndex="2"/>
        <table class="line2 pl2" id="smcResultsTable">
            <tr>
                <th style="width: 3%"></th>
                <th style="width: 15%">Tariff</th>
                <th style="width: 12%" class="text-right">Original Value ($)</th>
                <th style="width: 12%" class="text-right">Actual Weight (lb)</th>
                <th style="width: 12%" class="text-right">Billed Weight (lb)</th>
                <th style="width: 10%" class="text-right">Minimum Charge ($)</th>
                <th style="width: 12%" class="text-right">Deficit Rate ($)</th>
                <th style="width: 12%" class="text-right">Deficit Weight (lb)</th>
                <th style="width: 12%" class="text-right">Deficit Charge ($)</th>
            </tr>
            <asp:Repeater runat="server" ID="rptSmcResults" OnItemDataBound="OnSmcResultsItemDataBound">
                <ItemTemplate>
                    <tr>
                        <td class="top" rowspan="2">
                            <%# string.Format("{0}.", Container.ItemIndex+1) %>
                        </td>
                        <td>
                            <%# Eval("TariffName") %> - <%# Eval("TariffDate") %>
                        </td>
                        <td class="text-right">
                            <%# Eval("OriginalValue", "{0:c2}") %>
                        </td>
                        <td class="text-right">
                            <%# Eval("ActualWeight", "{0:f2}") %>
                        </td>
                        <td class="text-right">
                            <%# Eval("BilledWeight", "{0:f2}") %>
                        </td>
                        <td class="text-right">
                            <%# Eval("MinimumCharge", "{0:c2}") %>
                        </td>
                        <td class="text-right">
                            <%# Eval("DeficitRate", "{0:c2}") %>
                        </td>
                        <td class="text-right">
                            <%# Eval("DeficitWeight", "{0:c2}") %>
                        </td>
                        <td class="text-right">
                            <%# Eval("DeficitCharge", "{0:c2}") %>
                        </td>
                    </tr>
                    <asp:Repeater runat="server" ID="smcResultUnits">
                        <HeaderTemplate>
                            <tr class="f9">
                                <td colspan="8" class="forceLeftBorder">
                                    <h6 class="text-center">Item Breakdown</h6>
                                    <table class="contain mb10">
                                        <tr class="bbb1">
                                            <th style="width: 10%;" class="no-top-border">Freight Class
                                            </th>
                                            <th style="width: 15%;" class="text-right no-top-border no-left-border">Weight (lb)
                                            </th>
                                            <th style="width: 10%" class="text-right no-top-border no-left-border">Length
                                            </th>
                                            <th style="width: 10%" class="text-right no-top-border no-left-border">Width
                                            </th>
                                            <th style="width: 10%" class="text-right no-top-border no-left-border">Height
                                            </th>
                                            <th style="width: 15%;" class="text-right no-top-border no-left-border">Quantity
                                            </th>
                                            <th style="width: 15%;" class="text-right no-top-border no-left-border">Rate ($)
                                            </th>
                                            <th style="width: 15%;" class="text-right no-top-border no-left-border">Charge ($)
                                            </th>
                                        </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class="no-bottom-border">
                                <td>
                                    <%# Eval("FreightClass") %>
                                </td>
                                <td class="text-right">
                                    <%# Eval("Weight", "{0:f2}") %>
                                </td>
                                <td class="text-right">
                                    <%# Eval("Length") %>
                                </td>
                                <td class="text-right">
                                    <%# Eval("Width") %>
                                </td>
                                <td class="text-right">
                                    <%# Eval("Height") %>
                                </td>
                                <td class="text-right">
                                    <%# Eval("Quantity") %>
                                </td>
                                <td class="text-right">
                                    <%# Eval("Rate", "{0:c2}") %>
                                </td>
                                <td class="text-right">
                                    <%# Eval("Charge", "{0:c2}") %>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                            </td>
                            </tr>
                        </FooterTemplate>
                    </asp:Repeater>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
    <div id="instructionDiv" style="display: none;" class="popupControl">
        <div class="popheader">
            <h4>Instructions</h4>
        </div>

        <div class="rowgroup pl20 mb10">
            <div class="row">
                <div class="fieldgroup">
                    <h5>Single shipment rate</h5>
                </div>
            </div>
            <div class="row">
                <div class="fieldgroup">
                    <label class="note">Enter shipment parameters and click Rate.</label>
                </div>
            </div>
        </div>

        <div class="rowgroup pl20">
            <div class="row">
                <div class="fieldgroup">
                    <h5>Batch shipment rate</h5>
                </div>
            </div>
            <div class="row">
                <div class="fieldgroup">
                    <label class=" note">
                        Prepare a file with structure (tab or comma delimited) after selecting stop alternation
						weight (if applicable) and density rate (if applicable) then click Batch Rate.
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="fieldgroup">
                    <label class=" note">
                        NOTE: Header row must be present. Only one item per line
                    </label>
                </div>
            </div>
        </div>
        <table class="norm">
            <tr>
                <td style="width: 10%;">
                    <label class="lhInherit">Shipment/ Line ID</label>
                </td>
                <td style="width: 10%;">
                    <label class="lhInherit">Origin Postal Code</label>
                </td>
                <td style="width: 10%;">
                    <label class="lhInherit">Origin Country</label>
                </td>
                <td style="width: 10%;">
                    <label class="lhInherit">Destination Postal Code</label>
                </td>
                <td style="width: 10%;">
                    <label class="lhInherit">Destination Country</label>
                </td>
                <td style="width: 5%;">
                    <label class="lhInherit">Unit Weight (lb)</label>
                </td>
                <td style="width: 10%;">
                    <label class="lhInherit">Unit Freight Class</label>
                </td>
                <td style="width: 10%;">
                    <label class="lhInherit">Length (in)</label>
                </td>
                <td style="width: 10%;">
                    <label class="lhInherit">Width (in)</label>
                </td>
                <td style="width: 10%;">
                    <label class="lhInherit">Height (in)</label>
                </td>
                <td style="width: 5%;">
                    <label class="lhInherit">Quantity</label>
                </td>
            </tr>
            <tr>
                <td class="top">
                    <label class="lhInherit">
                        User defined
                        line identifier</label>
                </td>
                <td class="top">
                    <label class="lhInherit">12345</label>
                </td>
                <td class="top">
                    <label class="lhInherit">
                        Country IATA code e.g.
                        <br />
                        US = USA,
                        <br />
                        CA = Canada,
                        <br />
                        MX = mexico</label>
                </td>
                <td class="top">
                    <label class="lhInherit">16501</label>
                </td>
                <td class="top">
                    <label class="lhInherit">
                        Country IATA code e.g.<br />
                        US = USA,<br />
                        CA = Canada,<br />
                        MX = mexico</label>
                </td>
                <td class="top">
                    <label class="lhInherit">1000</label>
                </td>
                <td class="top">
                    <label class="lhInherit">
                        50 - 500 e.g.
                            <br />
                        50, 55, 60, 250, 500<br />
                        (not applicable when rating density)
                    </label>
                </td>
                <td class="top">
                    <label class="lhInherit">
                        48 (applicable when density)</label>
                </td>
                <td class="top">
                    <label class="comlabel">
                        48 (applicable when density)</label>
                </td>
                <td class="top">
                    <label class="lhInherit">
                        48 (applicable when density)</label>
                </td>
                <td class="text-right top">
                    <label class="lhInherit">1</label>
                </td>
            </tr>
        </table>
        <div class="row pl20 pt10 mb10">
            <div class="fieldgroup">
                <label class=" note">*** click anywhere in this box to close pop-up.</label>
            </div>
        </div>
    </div>
    <asp:Panel ID="pnlDimScreenJS" CssClass="dimBackgroundControl" runat="server" Style="display: none;" />
</asp:Content>
