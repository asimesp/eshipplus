﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="SmallPackagingMapView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Rating.SmallPackagingMapView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.Registry" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        ShowImport="false" OnNew="OnNewClick" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Small Pack Packaging Type Mapping<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>

        <hr class="fat" />

        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheDocumentPrefixMapTable" TableId="documentPrefixMapTable" HeaderZIndex="2"/>
        <asp:ListView ID="lvwRegistry" runat="server" ItemPlaceholderID="itemPlaceHolder">
            <LayoutTemplate>
                <div class="rowgroup">
                    <table id="documentPrefixMapTable" class="line2 pl2">
                        <tr>
                            <th style="width: 10%;">Engine</th>
                            <th style="width: 15%;">Engine Type</th>
                            <th style="width: 21%;">Package Type</th>
                            <th style="width: 14%;">Required Length (<abbr title="Inches">in</abbr>)</th>
                            <th style="width: 14%;">Required Width (<abbr title="Inches">in</abbr>)</th>
                            <th style="width: 14%;">Required Height (<abbr title="Inches">in</abbr>)</th>
                            <th style="width: 8%;">Action</th>
                        </tr>
                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                    </table>
                </div>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <eShip:CustomHiddenField ID="smallPackagingMapId" runat="server" Value='<%# Eval("Id") %>' />
                        <%# Eval("SmallPackageEngine").FormattedString() %>
                    </td>
                    <td>
                        <%# ddlEngineTypes.GetValueText(Eval("SmallPackEngineType").GetString())%>
                    </td>
                    <td>
                        <%# new PackageType(Eval("PackageTypeId").ToLong()).TypeName %>
                    </td>
                    <td class="text-right">
                        <%# Eval("RequiredLength").ToDecimal().ToString("f4") %>
                    </td>
                    <td class="text-right">
                        <%# Eval("RequiredWidth").ToDecimal().ToString("f4") %>
                    </td>
                    <td class="text-right">
                        <%# Eval("RequiredHeight").ToDecimal().ToString("f4") %>
                    </td>
                    <td class="text-center">
                        <asp:ImageButton ID="ibtnEdit" runat="server" ToolTip="Edit" ImageUrl="~/images/icons2/editBlue.png"
                            CausesValidation="false" OnClick="OnEditClicked" Enabled='<%# Access.Modify %>' />
                        <asp:ImageButton ID="ibtnDelete" runat="server" ToolTip="Delete" ImageUrl="~/images/icons2/deleteX.png"
                            CausesValidation="false" OnClick="OnDeleteClicked" Enabled='<%# Access.Remove %>' />
                        <ajax:ConfirmButtonExtender runat="server" ID="cbeDelete" TargetControlID="ibtnDelete"
                            ConfirmText="Are you sure you want to delete record?" />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
        <asp:Panel ID="pnlEdit" runat="server" Visible="false" DefaultButton="btnSave">
            <div class="popupControl popupControlOverW500">
                <div class="popheader">
                    <h4>
                        <asp:Literal ID="litTitle" runat="server" />
                    </h4>
                    <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                        CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
                </div>
                <div class="row">
                    <table class="poptable">
                        <tr>
                            <td colspan="2" class="red">
                                <asp:Literal ID="litMessage" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right top">
                                <label class="upper">Small Pack Engine:</label>
                            </td>
                            <td class="text-left">
                                <eShip:CustomHiddenField ID="hidSmallPackagingMapId" runat="server" />
                                <asp:DropDownList ID="ddlSmallPackEngine" CssClass="w200" DataTextField="Text" DataValueField="Value"
                                    runat="server" AutoPostBack="true" CausesValidation="False" OnSelectedIndexChanged="OnSmallPackEngineSelectedIndexChanged" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right top">
                                <label class="upper">Engine Type:</label>
                            </td>
                            <td class="text-left">
                                <asp:DropDownList ID="ddlEngineTypes" CssClass="w200" DataTextField="Value" DataValueField="Key" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right top">
                                <label class="upper">Package Type:</label>
                            </td>
                            <td class="text-left">
                                <eShip:CachedObjectDropDownList Type="PackageType" ID="ddlPackageTypes" runat="server" CssClass="w200" DefaultValue="0" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right top">
                                <label class="upper">Required Length (in):</label>
                            </td>
                            <td class="text-left">
                                <eShip:CustomTextBox ID="txtRequiredLength" CssClass="w200" runat="server" MaxLength="9" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtRequiredLength"
                                    FilterType="Custom, Numbers" ValidChars="." />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right top">
                                <label class="upper">Required Width (in):</label>
                            </td>
                            <td class="text-left">
                                <eShip:CustomTextBox ID="txtRequiredWidth" CssClass="w200" runat="server" MaxLength="9" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtRequiredWidth"
                                    FilterType="Custom, Numbers" ValidChars="." />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right top">
                                <label class="upper">Required Height (in):</label>
                            </td>
                            <td class="text-left">
                                <eShip:CustomTextBox ID="txtRequiredHeight" CssClass="w200" runat="server" MaxLength="9" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtRequiredHeight"
                                    FilterType="Custom, Numbers" ValidChars="." />
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="pt10">
                                <asp:Button ID="btnSave" Text="Save" OnClick="OnSaveClick" runat="server" />
                                <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnCloseClicked" runat="server"
                                    CausesValidation="false" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information"
		OnOkay="OnOkayProcess" />
	<asp:Panel ID="pnlDimScreen" CssClass="dimBackgroundControl" runat="server" Visible="false" />
</asp:Content>
