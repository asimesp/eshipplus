﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.WebApplication.Members
{
	[Serializable]
	public class SearchDefaults
	{
		public List<SearchDefaultsItem> Profiles { get; protected set; }
        public List<RateAndScheduleDefaultsItem> RateAndScheduleProfiles { get; protected set; }

        public List<SearchDefaultsItem> AccountBuckets
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.AccountBuckets).ToList();
            }
        }
        public List<SearchDefaultsItem> AddressBooks
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.AddressBooks).ToList();
            }
        }
        public List<SearchDefaultsItem> Announcements
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.Announcements).ToList();
            }
        }
        public List<SearchDefaultsItem> Assets
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.Assets).ToList();
            }
        }
        public List<SearchDefaultsItem> AuditLogs
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.AuditLogs).ToList();
            }
        }
        public List<SearchDefaultsItem> CarrierPreferredLanes
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.CarrierPreferredLanes).ToList();
            }
        }
        public List<SearchDefaultsItem> Charges
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.Charges).ToList();
            }
        }
        public List<SearchDefaultsItem> Claims
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.Claims).ToList();
            }
        }
        public List<SearchDefaultsItem> CommissionsToBePaid
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.CommissionsToBePaid).ToList();
            }
        }
        public List<SearchDefaultsItem> CustomerControlAccounts
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.CustomerControlAccounts).ToList();
            }
        }
	    public List<SearchDefaultsItem> CustomerGroups
	    {
	        get
	        {
	            return Profiles.Where(p => p.Type == SearchDefaultsItemType.CustomerGroups).ToList();
	        }
	    }
	    public List<SearchDefaultsItem> Customers
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.Customers).ToList();
            }
        }
	    public List<SearchDefaultsItem> CustomerInvoices
		{
			get
			{
				return Profiles.Where(p => p.Type == SearchDefaultsItemType.CustomerInvoices).ToList();
			}
		}
		public List<SearchDefaultsItem> CustomerLoads
		{
			get
			{
				return Profiles.Where(p => p.Type == SearchDefaultsItemType.CustomerLoads).ToList();
			}
		}
        public List<SearchDefaultsItem> CustomerPurchaseOrders
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.CustomerPurchaseOrders).ToList();
            }
        }
        public List<SearchDefaultsItem> CustomerTLTenderProfiles
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.CustomerTLTenderProfiles).ToList();
            }
        }
        public List<SearchDefaultsItem> DatAssets
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.Dat).ToList();
            }
        }
        public List<SearchDefaultsItem> DeveloperAccessRequests
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.DeveloperAccessRequests).ToList();
            }
        }
	    public List<SearchDefaultsItem> DocumentDeliveryLogs
	    {
	        get
	        {
	            return Profiles.Where(p => p.Type == SearchDefaultsItemType.DocumentDeliveryLogs).ToList();
	        }
	    }
	    public List<SearchDefaultsItem> DocumentTags
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.DocumentTags).ToList();
            }
        }
        public List<SearchDefaultsItem> EmailLogs
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.EmailLogs).ToList();
            }
        }
	    public List<SearchDefaultsItem> EquipmentTypes
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.EquipmentTypes).ToList();
            }
        }
        public List<SearchDefaultsItem> FailureCodes
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.FailureCodes).ToList();
            }
        }
        public List<SearchDefaultsItem> FaxTransmissions
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.FaxTransmissions).ToList();
            }
        }
        public List<SearchDefaultsItem> FuelTables
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.FuelTables).ToList();
            }
        }
        public List<SearchDefaultsItem> Groups
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.Groups).ToList();
            }
        }
		public List<SearchDefaultsItem> Invoices
		{
			get
			{
				return Profiles.Where(p => p.Type == SearchDefaultsItemType.Invoices).ToList();
			}
		}

        public List<SearchDefaultsItem> Jobs
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.Jobs).ToList();
            }
        }
        public List<SearchDefaultsItem> LibraryItems
	    {
	        get
	        {
	            return Profiles.Where(p => p.Type == SearchDefaultsItemType.LibraryItems).ToList();
	        }
	    }
        public List<SearchDefaultsItem> LibraryItemsFinder
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.LibraryItemsFinder).ToList();
            }
        }
        public List<SearchDefaultsItem> LoadOrders
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.LoadOrders).ToList();
            }
        }
        public List<SearchDefaultsItem> MacroPointOrders
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.MacroPointOrders).ToList();
            }
        }
        public List<SearchDefaultsItem> MiscReceipts
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.MiscReceipts).ToList();
            }
        }
        public List<SearchDefaultsItem> NoServiceDays
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.NoServiceDays).ToList();
            }
        }
        public List<SearchDefaultsItem> PendingLtlPickupsDashboard
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.PendingLtlPickupsDashboard).ToList();
            }
        }
	    public List<SearchDefaultsItem> PostalCodes
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.PostalCodes).ToList();
            }
        }
        public List<SearchDefaultsItem> Prefixes
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.Prefixes).ToList();
            }
        }
        public List<SearchDefaultsItem> QuickPayOptions
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.QuickPayOptions).ToList();
            }
        }
        public List<SearchDefaultsItem> Regions
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.Regions).ToList();
            }
        }
        public List<SearchDefaultsItem> ReportConfigurations
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.ReportConfigurations).ToList();
            }
        }
        public List<SearchDefaultsItem> ReportSchedules
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.ReportSchedules).ToList();
            }
        }
        public List<SearchDefaultsItem> ResellerAdditions
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.ResellerAdditions).ToList();
            }
        }
        public List<SearchDefaultsItem> SalesRepresentatives
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.SalesRepresentatives).ToList();
            }
        }
        public List<SearchDefaultsItem> Services
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.Services).ToList();
            }
        }
		public List<SearchDefaultsItem> ServiceTickets
		{
			get
			{
				return Profiles.Where(p => p.Type == SearchDefaultsItemType.ServiceTickets).ToList();
			}
		}
        public List<SearchDefaultsItem> ShipmentDashboard
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.ShipmentDashboard).ToList();
            }
        }
		public List<SearchDefaultsItem> Shipments
		{
			get
			{
				return Profiles.Where(p => p.Type == SearchDefaultsItemType.Shipments).ToList();
			}
		}
	    public List<SearchDefaultsItem> ShoppedRates
	    {
	        get
	        {
	            return Profiles.Where(p => p.Type == SearchDefaultsItemType.ShoppedRates).ToList();
	        }
	    }
        public List<SearchDefaultsItem> Tiers
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.Tiers).ToList();
            }
        }
        public List<SearchDefaultsItem> Users
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.Users).ToList();
            }
        }
        public List<SearchDefaultsItem> Vendors
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.Vendors).ToList();
            }
        }
	    public List<SearchDefaultsItem> VendorBills
		{
			get
			{
				return Profiles.Where(p => p.Type == SearchDefaultsItemType.VendorBills).ToList();
			}
		}
        public List<SearchDefaultsItem> VendorCommunications
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.VendorCommunications).ToList();
            }
        }
        public List<SearchDefaultsItem> VendorDisputes
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.VendorDisputes).ToList();
            }
        }
        public List<SearchDefaultsItem> VendorGroups
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.VendorGroups).ToList();
            }
        }
        public List<SearchDefaultsItem> VendorRatings
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.VendorRatings).ToList();
            }
        }
        public List<SearchDefaultsItem> VendorTerminals
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.VendorTerminals).ToList();
            }
        }
		public List<SearchDefaultsItem> XmlConnectRecords
		{
			get
			{
				return Profiles.Where(p => p.Type == SearchDefaultsItemType.XmlConnectRecords).ToList();
			}
		}
        public List<SearchDefaultsItem> PendingVendors
        {
            get
            {
                return Profiles.Where(p => p.Type == SearchDefaultsItemType.PendingVendors).ToList();
            }
        }
        

	    /// <summary>
		/// Adds new item to list removing existing item if present.  Item
		/// will also be made Current Default.
		/// </summary>
		/// <param name="item">Item to add</param>
		public void AddOrUpdate(SearchDefaultsItem item)
		{
			Profiles.RemoveAll(i => i.Type == item.Type && i.Name == item.Name);
			foreach(var u in Profiles.Where(i=>i.Type == item.Type))
				u.IsCurrentDefault = false;
			item.IsCurrentDefault = true;
			Profiles.Add(item);
		}

		/// <summary>
		/// Removes an item from the list matching on type and name
		/// </summary>
		/// <param name="item">item to remove</param>
		public void Remove(SearchDefaultsItem item)
		{
			Profiles.RemoveAll(i => i.Type == item.Type && i.Name == item.Name);
		}

		/// <summary>
		/// Retrieve the Current Default if present else first Search default profile for the specified type
		/// </summary>
		/// <param name="type">Type for which to retrieve default</param>
		/// <returns>Search Default profile if found else null</returns>
		public SearchDefaultsItem Default(SearchDefaultsItemType type)
		{
			SearchDefaultsItem item = null;
			switch (type)
			{
				case SearchDefaultsItemType.AccountBuckets:
					item = AccountBuckets.FirstOrDefault(i => i.IsCurrentDefault) ?? AccountBuckets.FirstOrDefault();
					break;
				case SearchDefaultsItemType.AddressBooks:
					item = AddressBooks.FirstOrDefault(i => i.IsCurrentDefault) ?? AddressBooks.FirstOrDefault();
					break;
                case SearchDefaultsItemType.Announcements:
					item = Announcements.FirstOrDefault(i => i.IsCurrentDefault) ?? Announcements.FirstOrDefault();
					break;
				case SearchDefaultsItemType.Assets:
					item = Assets.FirstOrDefault(i => i.IsCurrentDefault) ?? Assets.FirstOrDefault();
					break;
				case SearchDefaultsItemType.AuditLogs:
					item = AuditLogs.FirstOrDefault(i => i.IsCurrentDefault) ?? AuditLogs.FirstOrDefault();
					break;
				case SearchDefaultsItemType.CarrierPreferredLanes:
					item = CarrierPreferredLanes.FirstOrDefault(i => i.IsCurrentDefault) ?? CarrierPreferredLanes.FirstOrDefault();
					break;
				case SearchDefaultsItemType.Claims:
					item = Claims.FirstOrDefault(i => i.IsCurrentDefault) ?? Claims.FirstOrDefault();
					break;
				case SearchDefaultsItemType.Jobs:
					item = Jobs.FirstOrDefault(i => i.IsCurrentDefault) ?? Jobs.FirstOrDefault();
					break;
                case SearchDefaultsItemType.CommissionsToBePaid:
                    item = CommissionsToBePaid.FirstOrDefault(i => i.IsCurrentDefault) ?? CommissionsToBePaid.FirstOrDefault();
                    break;
				case SearchDefaultsItemType.CustomerControlAccounts:
					item = CustomerControlAccounts.FirstOrDefault(i => i.IsCurrentDefault) ?? CustomerControlAccounts.FirstOrDefault();
					break;
				case SearchDefaultsItemType.CustomerGroups:
					item = CustomerGroups.FirstOrDefault(i => i.IsCurrentDefault) ?? CustomerGroups.FirstOrDefault();
					break;
				case SearchDefaultsItemType.Customers:
					item = Customers.FirstOrDefault(i => i.IsCurrentDefault) ?? Customers.FirstOrDefault();
					break;
				case SearchDefaultsItemType.CustomerInvoices:
					item = CustomerInvoices.FirstOrDefault(i => i.IsCurrentDefault) ?? CustomerInvoices.FirstOrDefault();
					break;
				case SearchDefaultsItemType.CustomerLoads:
					item = CustomerLoads.FirstOrDefault(i => i.IsCurrentDefault) ?? CustomerLoads.FirstOrDefault();
					break;
				case SearchDefaultsItemType.CustomerPurchaseOrders:
					item = CustomerPurchaseOrders.FirstOrDefault(i => i.IsCurrentDefault) ?? CustomerPurchaseOrders.FirstOrDefault();
					break;
                case SearchDefaultsItemType.CustomerTLTenderProfiles:
                    item = CustomerTLTenderProfiles.FirstOrDefault(i => i.IsCurrentDefault) ?? CustomerTLTenderProfiles.FirstOrDefault();
                    break;
                case SearchDefaultsItemType.Dat:
                    item = Claims.FirstOrDefault(i => i.IsCurrentDefault) ?? DatAssets.FirstOrDefault();
                    break;
				case SearchDefaultsItemType.DeveloperAccessRequests:
					item = DeveloperAccessRequests.FirstOrDefault(i => i.IsCurrentDefault) ?? DeveloperAccessRequests.FirstOrDefault();
					break;
				case SearchDefaultsItemType.DocumentDeliveryLogs:
					item = DocumentDeliveryLogs.FirstOrDefault(i => i.IsCurrentDefault) ?? DocumentDeliveryLogs.FirstOrDefault();
					break;
				case SearchDefaultsItemType.DocumentTags:
					item = DocumentTags.FirstOrDefault(i => i.IsCurrentDefault) ?? DocumentTags.FirstOrDefault();
					break;
                case SearchDefaultsItemType.EmailLogs:
                    item = EmailLogs.FirstOrDefault(i => i.IsCurrentDefault) ?? EmailLogs.FirstOrDefault();
                    break;
				case SearchDefaultsItemType.EquipmentTypes:
					item = EquipmentTypes.FirstOrDefault(i => i.IsCurrentDefault) ?? EquipmentTypes.FirstOrDefault();
					break;
                case SearchDefaultsItemType.FaxTransmissions:
                    item = FaxTransmissions.FirstOrDefault(i => i.IsCurrentDefault) ?? FaxTransmissions.FirstOrDefault();
                    break;
				case SearchDefaultsItemType.FailureCodes:
					item = FailureCodes.FirstOrDefault(i => i.IsCurrentDefault) ?? FailureCodes.FirstOrDefault();
					break;
				case SearchDefaultsItemType.FuelTables:
					item = FuelTables.FirstOrDefault(i => i.IsCurrentDefault) ?? FuelTables.FirstOrDefault();
					break;
				case SearchDefaultsItemType.Groups:
					item = Groups.FirstOrDefault(i => i.IsCurrentDefault) ?? Groups.FirstOrDefault();
					break;
				case SearchDefaultsItemType.Invoices:
					item = Invoices.FirstOrDefault(i => i.IsCurrentDefault) ?? Invoices.FirstOrDefault();
					break;
				case SearchDefaultsItemType.LibraryItems:
					item = LibraryItems.FirstOrDefault(i => i.IsCurrentDefault) ?? LibraryItems.FirstOrDefault();
					break;
				case SearchDefaultsItemType.LibraryItemsFinder:
					item = LibraryItemsFinder.FirstOrDefault(i => i.IsCurrentDefault) ?? LibraryItemsFinder.FirstOrDefault();
					break;
                case SearchDefaultsItemType.LoadOrders:
                    item = LoadOrders.FirstOrDefault(i => i.IsCurrentDefault) ?? LoadOrders.FirstOrDefault();
                    break;
                case SearchDefaultsItemType.MacroPointOrders:
                    item = MacroPointOrders.FirstOrDefault(i => i.IsCurrentDefault) ?? MacroPointOrders.FirstOrDefault();
                    break;
                case SearchDefaultsItemType.MiscReceipts:
                    item = MiscReceipts.FirstOrDefault(i => i.IsCurrentDefault) ?? MiscReceipts.FirstOrDefault();
                    break;
				case SearchDefaultsItemType.NoServiceDays:
					item = NoServiceDays.FirstOrDefault(i => i.IsCurrentDefault) ?? NoServiceDays.FirstOrDefault();
					break;
				case SearchDefaultsItemType.PostalCodes:
					item = PostalCodes.FirstOrDefault(i => i.IsCurrentDefault) ?? PostalCodes.FirstOrDefault();
					break;
				case SearchDefaultsItemType.Prefixes:
					item = Prefixes.FirstOrDefault(i => i.IsCurrentDefault) ?? Prefixes.FirstOrDefault();
					break;
				case SearchDefaultsItemType.QuickPayOptions:
					item = QuickPayOptions.FirstOrDefault(i => i.IsCurrentDefault) ?? QuickPayOptions.FirstOrDefault();
					break;
				case SearchDefaultsItemType.Regions:
					item = Regions.FirstOrDefault(i => i.IsCurrentDefault) ?? Regions.FirstOrDefault();
					break;
				case SearchDefaultsItemType.ReportConfigurations:
					item = ReportConfigurations.FirstOrDefault(i => i.IsCurrentDefault) ?? ReportConfigurations.FirstOrDefault();
					break;
				case SearchDefaultsItemType.ReportSchedules:
					item = ReportSchedules.FirstOrDefault(i => i.IsCurrentDefault) ?? ReportSchedules.FirstOrDefault();
					break;
				case SearchDefaultsItemType.ResellerAdditions:
					item = ResellerAdditions.FirstOrDefault(i => i.IsCurrentDefault) ?? ResellerAdditions.FirstOrDefault();
					break;
				case SearchDefaultsItemType.SalesRepresentatives:
					item = SalesRepresentatives.FirstOrDefault(i => i.IsCurrentDefault) ?? SalesRepresentatives.FirstOrDefault();
					break;
				case SearchDefaultsItemType.Services:
					item = Services.FirstOrDefault(i => i.IsCurrentDefault) ?? Services.FirstOrDefault();
					break;
				case SearchDefaultsItemType.ServiceTickets:
					item = ServiceTickets.FirstOrDefault(i => i.IsCurrentDefault) ?? ServiceTickets.FirstOrDefault();
					break;
                case SearchDefaultsItemType.PendingLtlPickupsDashboard:
                    item = PendingLtlPickupsDashboard.FirstOrDefault(i => i.IsCurrentDefault) ?? PendingLtlPickupsDashboard.FirstOrDefault();
                    break;
                case SearchDefaultsItemType.ShipmentDashboard:
                    item = ShipmentDashboard.FirstOrDefault(i => i.IsCurrentDefault) ?? ShipmentDashboard.FirstOrDefault();
                    break;
				case SearchDefaultsItemType.Shipments:
					item = Shipments.FirstOrDefault(i => i.IsCurrentDefault) ?? Shipments.FirstOrDefault();
					break;
				case SearchDefaultsItemType.ShoppedRates:
					item = ShoppedRates.FirstOrDefault(i => i.IsCurrentDefault) ?? ShoppedRates.FirstOrDefault();
					break;
				case SearchDefaultsItemType.Tiers:
					item = Tiers.FirstOrDefault(i => i.IsCurrentDefault) ?? Tiers.FirstOrDefault();
					break;
				case SearchDefaultsItemType.Users:
					item = Users.FirstOrDefault(i => i.IsCurrentDefault) ?? Users.FirstOrDefault();
					break;
				case SearchDefaultsItemType.Vendors:
					item = Vendors.FirstOrDefault(i => i.IsCurrentDefault) ?? Vendors.FirstOrDefault();
					break;
				case SearchDefaultsItemType.VendorBills:
					item = VendorBills.FirstOrDefault(i => i.IsCurrentDefault) ?? VendorBills.FirstOrDefault();
					break;
				case SearchDefaultsItemType.VendorCommunications:
					item = VendorCommunications.FirstOrDefault(i => i.IsCurrentDefault) ?? VendorCommunications.FirstOrDefault();
					break;
				case SearchDefaultsItemType.VendorGroups:
					item = VendorGroups.FirstOrDefault(i => i.IsCurrentDefault) ?? VendorGroups.FirstOrDefault();
					break;
				case SearchDefaultsItemType.VendorRatings:
					item = VendorRatings.FirstOrDefault(i => i.IsCurrentDefault) ?? VendorRatings.FirstOrDefault();
					break;
				case SearchDefaultsItemType.VendorTerminals:
					item = VendorTerminals.FirstOrDefault(i => i.IsCurrentDefault) ?? VendorTerminals.FirstOrDefault();
					break;
				case SearchDefaultsItemType.XmlConnectRecords:
					item = XmlConnectRecords.FirstOrDefault(i => i.IsCurrentDefault) ?? VendorTerminals.FirstOrDefault();
					break;
				case SearchDefaultsItemType.PendingVendors:
					item = Vendors.FirstOrDefault(i => i.IsCurrentDefault) ?? PendingVendors.FirstOrDefault();
					break;
			}
			return item;
		}

		/// <summary>
		/// Returns list of profiles for specified type
		/// </summary>
		/// <param name="type">Type for which to retrieve list</param>
		/// <returns>List of profiles else empty list</returns>
		public List<SearchDefaultsItem> List(SearchDefaultsItemType type)
		{
			switch (type)
			{
                case SearchDefaultsItemType.AccountBuckets:
                    return AccountBuckets;
                case SearchDefaultsItemType.AddressBooks:
                    return AddressBooks;
                case SearchDefaultsItemType.Announcements:
                    return Announcements;
                case SearchDefaultsItemType.Assets:
                    return Assets;
                case SearchDefaultsItemType.AuditLogs:
                    return AuditLogs;
                case SearchDefaultsItemType.CarrierPreferredLanes:
                    return CarrierPreferredLanes;
                case SearchDefaultsItemType.Charges:
                    return Charges;
                case SearchDefaultsItemType.Claims:
			        return Claims;
                case SearchDefaultsItemType.CommissionsToBePaid:
                    return CommissionsToBePaid;
                case SearchDefaultsItemType.CustomerControlAccounts:
                    return CustomerControlAccounts;
                case SearchDefaultsItemType.CustomerGroups:
                    return CustomerGroups;
                case SearchDefaultsItemType.Customers:
                    return Customers;
				case SearchDefaultsItemType.CustomerInvoices:
					return CustomerInvoices;
				case SearchDefaultsItemType.CustomerLoads:
			        return CustomerLoads;
                case SearchDefaultsItemType.CustomerPurchaseOrders:
					return CustomerPurchaseOrders;
                case SearchDefaultsItemType.Dat:
					return DatAssets;
                case SearchDefaultsItemType.DeveloperAccessRequests:
                    return DeveloperAccessRequests;
                case SearchDefaultsItemType.DocumentDeliveryLogs:
                    return DocumentDeliveryLogs;
                case SearchDefaultsItemType.DocumentTags:
                    return DocumentTags;
                case SearchDefaultsItemType.EmailLogs:
                    return EmailLogs;
                case SearchDefaultsItemType.EquipmentTypes:
                    return EquipmentTypes;
                case SearchDefaultsItemType.FailureCodes:
                    return FailureCodes;
                case SearchDefaultsItemType.FaxTransmissions:
                    return FaxTransmissions;
                case SearchDefaultsItemType.FuelTables:
                    return FuelTables;
                case SearchDefaultsItemType.Groups:
                    return Groups;
                case SearchDefaultsItemType.Invoices:
                    return Invoices;
                case SearchDefaultsItemType.Jobs:
                    return Jobs;
                case SearchDefaultsItemType.LibraryItems:
                    return LibraryItems;
                case SearchDefaultsItemType.LibraryItemsFinder:
                    return LibraryItemsFinder;
                case SearchDefaultsItemType.LoadOrders:
                    return LoadOrders;
                case SearchDefaultsItemType.MacroPointOrders:
                    return MacroPointOrders;
                case SearchDefaultsItemType.MiscReceipts:
                    return MiscReceipts;
                case SearchDefaultsItemType.NoServiceDays:
                    return NoServiceDays;
                case SearchDefaultsItemType.PostalCodes:
                    return PostalCodes;
                case SearchDefaultsItemType.Prefixes:
                    return Prefixes;
                case SearchDefaultsItemType.QuickPayOptions:
			        return QuickPayOptions;
                 case SearchDefaultsItemType.Regions:
                    return Regions;
                case SearchDefaultsItemType.ReportConfigurations:
                    return ReportConfigurations;
                case SearchDefaultsItemType.ReportSchedules:
                    return ReportSchedules;
                case SearchDefaultsItemType.ResellerAdditions:
                    return ResellerAdditions;
                case SearchDefaultsItemType.SalesRepresentatives:
                    return SalesRepresentatives;
                case SearchDefaultsItemType.Services:
                    return Services;
				case SearchDefaultsItemType.ServiceTickets:
					return ServiceTickets;
                case SearchDefaultsItemType.PendingLtlPickupsDashboard:
                    return PendingLtlPickupsDashboard;
                case SearchDefaultsItemType.ShipmentDashboard:
                    return ShipmentDashboard;
				case SearchDefaultsItemType.Shipments:
					return Shipments;
                case SearchDefaultsItemType.ShoppedRates:
                    return ShoppedRates;
                case SearchDefaultsItemType.Tiers:
                    return Tiers;
                case SearchDefaultsItemType.Users:
                    return Users;
                case SearchDefaultsItemType.Vendors:
                    return Vendors;
                case SearchDefaultsItemType.VendorDisputes:
                    return VendorDisputes;
                case SearchDefaultsItemType.VendorBills:
                    return VendorBills;
                case SearchDefaultsItemType.VendorCommunications:
                    return VendorCommunications;
                case SearchDefaultsItemType.VendorGroups:
                    return VendorGroups;
                case SearchDefaultsItemType.VendorRatings:
			        return VendorRatings;
                case SearchDefaultsItemType.VendorTerminals:
                    return VendorTerminals;
				case SearchDefaultsItemType.XmlConnectRecords:
					return XmlConnectRecords;
				case SearchDefaultsItemType.PendingVendors:
					return PendingVendors;

				default:
					return new List<SearchDefaultsItem>();
			}
		}


        /// <summary>
        /// Adds new item to list removing existing item if present.  Item
        /// will also be made Current Default.
        /// </summary>
        /// <param name="item">Item to add</param>
        public void AddOrUpdate(RateAndScheduleDefaultsItem item)
        {
            RateAndScheduleProfiles.RemoveAll(i => i.Name == item.Name);
            foreach (var u in RateAndScheduleProfiles)
                u.IsCurrentDefault = false;
            item.IsCurrentDefault = true;
            RateAndScheduleProfiles.Add(item);
        }

        /// <summary>
        /// Removes an item from the list matching on type and name
        /// </summary>
        /// <param name="item">item to remove</param>
        public void Remove(RateAndScheduleDefaultsItem item)
        {
            RateAndScheduleProfiles.RemoveAll(i => i.Name == item.Name);
        }

        /// <summary>
        /// Retrieve the Current Default if present else first rate and schedule default profile for the specified type
        /// </summary>
        /// <returns>Rate and Schedule Default profile if found else null</returns>
        public RateAndScheduleDefaultsItem Default()
        {
            return RateAndScheduleProfiles.FirstOrDefault(i => i.IsCurrentDefault) ??
                   RateAndScheduleProfiles.FirstOrDefault();
        }

        public SearchDefaults()
		{
			Profiles = new List<SearchDefaultsItem>();
            RateAndScheduleProfiles = new List<RateAndScheduleDefaultsItem>();
		}

		public SearchDefaults(SearchProfileDefault @default)
		{
			if (@default == null) return;

			Profiles = @default.Profiles.FromXml<List<SearchDefaultsItem>>() ?? new List<SearchDefaultsItem>();
            RateAndScheduleProfiles = @default.RateAndScheduleProfiles.FromXml<List<RateAndScheduleDefaultsItem>>() ??
                                      new List<RateAndScheduleDefaultsItem>();
        }
	}
}
