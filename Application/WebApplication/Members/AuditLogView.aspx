﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="AuditLogView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.AuditLogView" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" ImageUrl="" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                AUDIT LOG
                <small class="ml10"><asp:Literal runat="server" ID="litRecordCount" Text="" /></small>
            </h3>
        </div>
        <span class="clearfix"></span>
        <hr class="dark mb10" />

        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb0">
                <div class="row mb0">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="AuditLogs" OnProcessingError="OnSearchProfilesProcessingError"
                            OnSearchProfileSelected="OnSearchProfilesProfileSelected" />
                    </div>
                </div>
            </div>
            <hr class="dark mb20" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                        OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>

            <hr class="fat" />
            <div class="rowgroup">
                <div class="row mb10">
                    <div class="left">
                        <asp:Button ID="btnSearch" CssClass="left" runat="server" Text="SEARCH" OnClick="OnSearchClicked"
                            CausesValidation="False" />
                        <asp:Button ID="btnExport" runat="server" Text="EXPORT" OnClick="OnExportClicked"
                            CssClass="ml10" />
                    </div>
                </div>
            </div>
        </asp:Panel>

        <asp:UpdatePanel runat="server" ID="upDataUpdate">
            <ContentTemplate>

                <script type="text/javascript">

                    function DisplayEntityDetails(entityCode, entityId) {
                        $.ajax({
                            type: "post",
                            url: "AuditLogView.aspx/RetrieveEntityDetails",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            data: '{"entityCode":"' + entityCode + '", "entityId":"' + entityId + '"}',
                            success: function(result) {
                                if (result.d != null) {

                                    if (!jsHelper.IsTrue(result.d.CanJumpTo)) {
                                        $(jsHelper.AddHashTag('<%= btnGoToRecord.ClientID%>')).hide();
                                    } else {
                                        $(jsHelper.AddHashTag('<%= btnGoToRecord.ClientID%>')).show();
                                    }

                                    $(jsHelper.AddHashTag('descDiv')).html(result.d.Desc);
                                    $(jsHelper.AddHashTag('<%= hidGoToEntityCode.ClientID%>')).val(result.d.EntityCode);
                                    $(jsHelper.AddHashTag('<%= hidGoToEntityId.ClientID%>')).val(result.d.EntityId);
                                }

                                $(jsHelper.AddHashTag('<%= pnlEntityDetails.ClientID%>')).show();
                                $(jsHelper.AddHashTag('<%= pnlDimScreenJS.ClientID%>')).show();
                            },
                            error: function(xhr, status, error) {
                                alert(error);
                            }
                        });
                    }

            function CloseEntityDetails() {
                $(jsHelper.AddHashTag('<%= pnlEntityDetails.ClientID%>')).hide();
                $(jsHelper.AddHashTag('<%= pnlDimScreenJS.ClientID%>')).hide();
            }

            function DisplayFullDescription(id) {
                $.ajax({
                    type: "post",
                    url: "AuditLogView.aspx/RetrieveFullDescription",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: '{"id":"' + id + '"}',
                    success: function (result) {
                        if (result.d != null) {
                            $(jsHelper.AddHashTag('divFullDescription')).html(result.d);
                        }
                        $(jsHelper.AddHashTag('<%= pnlAuditLogFullDescription.ClientID%>')).show();
                        $(jsHelper.AddHashTag('<%= pnlDimScreenJS.ClientID%>')).show();
                    },
                    error: function (xhr, status, error) {
                        alert(error);
                    }
                });
            }

            function CloseAuditLogFullDescription() {
                $(jsHelper.AddHashTag('<%= pnlAuditLogFullDescription.ClientID%>')).hide();
                $(jsHelper.AddHashTag('<%= pnlDimScreenJS.ClientID%>')).hide();
            }
                </script>

                <asp:Panel runat="server" ID="pnlEntityDetails" Style="display: none;">
                    <div class="popupAnnouncement popupControlOverW750 popup-position-fixed">
                        <div class="popheader">
                            <h4>Entity Details
                            </h4>
                            <asp:Image ID="imgClose" ImageUrl="~/images/icons2/close.png" CssClass="close" runat="server" onclick="javascript:CloseEntityDetails();" />
                        </div>
                        <div class="row">
                            <table class="poptable">
                                <tr>
                                    <td style="width: 25%;" class="top pt10 pl10">Description:
                                    </td>
                                    <td style="text-align: left;" class="top pt10">
                                        <div id="descDiv"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-right pr10" colspan="20">
                                        <asp:HiddenField runat="server" ID="hidGoToEntityCode" />
                                        <asp:HiddenField runat="server" ID="hidGoToEntityId" />
                                        <asp:Button ID="btnGoToRecord" Text="Go To Record" OnClick="OnGoToRecord" runat="server"
                                            CausesValidation="false" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlAuditLogFullDescription" Style="display: none;">
                    <div class="popupAnnouncement popupControlOverW750 popup-position-fixed">
                        <div class="popheader">
                            <h4>Full Description
                            </h4>
                            <asp:Image ID="Image1" ImageUrl="~/images/icons2/close.png" CssClass="close" runat="server" onclick="javascript:CloseAuditLogFullDescription();" />
                        </div>
                        <div class="row pl10 pr10">
                            <table class="poptable">
                                <tr>
                                    <td class="text-left pr10" colspan="20">
                                        <div id="divFullDescription" class="finderScroll"></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </asp:Panel>


                <asp:Panel ID="pnlDimScreenJS" CssClass="dimBackgroundControl" runat="server" Style="display: none;" />

                <eShip:ContinuousScrollExtender ID="cseDataUpdate" runat="server" BindableControlId="lvwAuditLog" UpdatePanelToExtendId="upDataUpdate" />
                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheAuditLogTable" TableId="auditLogTable" HeaderZIndex="2" />

                <asp:ListView ID="lvwAuditLog" runat="server" ItemPlaceholderID="itemPlaceHolder"
                    EnableViewState="False">
                    <LayoutTemplate>
                        <table id="auditLogTable" class="line2 pl2">
                            <tr>
                                <th style="width: 15%;">Entity Code
                                </th>
                                <th style="width: 10%;">Entity Id
                                </th>
                                <th style="width: 40%;">Description
                                </th>
                                <th style="width: 15%;">User
                                </th>
                                <th style="width: 20%;">Log Date/Time
                                </th>
                            </tr>
                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <%# Eval("EntityCode") %>
                            </td>
                            <td>
                                <a class="blue" href='<%# string.Format("javascript:DisplayEntityDetails(\"{0}\", \"{1}\")", Eval("EntityCode"), Eval("EntityId")) %>'><%# Eval("EntityId") %></a>
                            </td>
                            <td>
                                <%# Eval("Description") %>
                                <a href='<%# string.Format("javascript:DisplayFullDescription(\"{0}\")", Eval("Id")) %>' class="blue"
                                    <%# Eval("Description").ToString().Length < DescriptionCharacterLimit ? "style='display:none;'" : string.Empty %>>...</a>
                            </td>
                            <td>
                                <%# Eval("UserName")%>
                            </td>
                            <td>
                                <%# Eval("LogDateTime") %>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>

            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lvwAuditLog" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackgroundControl" runat="server" Visible="false" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
</asp:Content>
