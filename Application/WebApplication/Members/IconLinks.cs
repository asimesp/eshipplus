﻿namespace LogisticsPlus.Eship.WebApplication.Members
{
	public class IconLinks
	{
		public const string Add = "~/images/icons2/add.png";
		public const string Copy = "~/images/icons2/copy.png";
		public const string Cog = "~/images/icons2/cog.png";
		public const string Close = "~/images/icons2/close.png";
		public const string CollapseImage = "~/images/icons2/bullet-minus.png";
		public const string Delete = "~/images/icons2/deleteX.png";
		public const string Document = "~/images/icons2/document.png";
		public const string ExpandImage = "~/images/icons2/bullet-add.png";
		public const string ExternalLink = "~/images/icons2/ExternalLink.png";
		public const string EyeOpen = "~/images/icons2/eye-open.png";
		public const string Import = "~/images/icons2/import.png";
		public const string MacroPointTracking = "~/images/icons2/MacroPointTracking.png";
		public const string MacroPointTrackingStopped = "~/images/icons2/MacroPointTrackingStopped.png";
        public const string New = "~/images/icons2/new.png";
        public const string Stats = "~/images/icons2/stats.png";
		public const string Export = "~/images/icons2/export.png";


		public const string Admin = "~/images/Admin.png";
		public const string AdminBlue = "~/images/AdminBlue.png";
		public const string Finance = "~/images/finance.png";
		public const string FinanceBlue = "~/images/financeBlue.png";
		public const string Operations = "~/images/operations.png";
		public const string OperationsBlue = "~/images/operationsBlue.png";
        public const string Rating = "~/images/rating.png";
        public const string RatingBlue = "~/images/ratingBlue.png";
		public const string Registry = "~/images/Registry.png";
		public const string RegistryBlue = "~/images/RegistryBlue.png";
		public const string Utilities = "~/images/utilities.png";
		public const string UtilitiesBlue = "~/images/utilitiesBlue.png";
		public const string Reports = "~/images/reports.png";
		public const string ReportsBlue = "~/images/reportsBlue.png";
		public const string Connect = "~/images/connect.png";
		public const string ConnectBlue = "~/images/connectBlue.png";
	}
}