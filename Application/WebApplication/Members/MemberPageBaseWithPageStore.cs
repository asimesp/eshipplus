﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Processor;

namespace LogisticsPlus.Eship.WebApplication.Members
{
	public abstract class MemberPageBaseWithPageStore : MemberPageBase
	{
		private Dictionary<string, object> _pageStore = new Dictionary<string, object>();

		public Dictionary<string, object> PageStore { get { return _pageStore ?? (_pageStore = new Dictionary<string, object>()); } }

		public string DefaultStoreKey { get { return PageCode.GetString(); } }

		protected override void OnInit(EventArgs e)
		{
			Page.RegisterRequiresControlState(this);
			base.OnInit(e);
		}

		protected override void LoadControlState(object savedState)
		{
			var fromSave = savedState as List<Object>;

			if (fromSave == null) return;
			if (fromSave.Count < 2) return;

			_pageStore = fromSave[1] as Dictionary<string, object>;

			base.LoadControlState(fromSave[0]);
		}

		protected override object SaveControlState()
		{
			var toSave = new List<object>
				{
					base.SaveControlState(),
					PageStore
				};
			return toSave;
		}

		protected override void OnLoad(EventArgs e)
		{
			if (!PageStore.ContainsKey(DefaultStoreKey)) PageStore.Add(DefaultStoreKey, null);

			base.OnLoad(e);
		}
	}
}