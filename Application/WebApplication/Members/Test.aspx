﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="Test.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Test" %>
<%@ Register TagPrefix="eship" Namespace="LogisticsPlus.Eship.WebApplication.Members.Controls" Assembly="Pacman" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar runat="server" ID="mainToolbar" ShowDelete="True" ShowEdit="True" ShowExport="True" ShowFind="True" ShowImport="True" ShowMore="True" ShowNew="True" ShowSave="True" ShowUnlock="True" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>Equipment Types</h3>

            <div class="lockMessage">
                Read-Only
            </div>
        </div>
        
        <div class="row">
            <eship:CachedObjectDropDownList runat="server" ID="ddl" DataTextField="Text" DataValueField="Value" Type="PackageType" DefaultValue="0" EnableChooseOne="True" />
            <asp:DropDownList runat="server" ID="ddl2" DataTextField="Text" DataValueField="Value" AutoPostBack="True" OnSelectedIndexChanged="OnDdl2SelectedIndexChanged"/>
        </div>

        <hr class="fat" />

        <div class="rowgroup">
            <table class="line2 pl2">
                <tr>
                    <th width="100">Code</th>
                    <th>Type Name</th>
                    <th>Group</th>
                    <th width="40">Active</th>
                    <th width="60">Modify</th>
                </tr>
                <tr>
                    <td>28VA</td>
                    <td>28 Van (Pup)</td>
                    <td>Container Drayage</td>
                    <td>
                        <input class="uni" type="checkbox" checked="checked" /></td>
                    <td>
                        <input type="image" src="../images/icons2/editBlue.png" />
                        <input type="image" src="../images/icons2/deleteX.png" />
                    </td>
                </tr>
                <tr>
                    <td>48VA</td>
                    <td>48 VAN</td>
                    <td>Dry Van</td>
                    <td>
                        <input class="uni" type="checkbox" checked="checked" /></td>
                    <td>
                        <input type="image" src="../images/icons2/edit.png" />
                        <input type="image" src="../images/icons2/delete.png" />
                    </td>
                </tr>
                <tr>
                    <td>53VA</td>
                    <td>53 VAN</td>
                    <td>Dry Van</td>
                    <td>
                        <input class="uni" type="checkbox" checked="checked" /></td>
                    <td>
                        <input type="image" src="../images/icons2/edit.png" />
                        <input type="image" src="../images/icons2/delete.png" />
                    </td>
                </tr>
                <tr>
                    <td>AIR</td>
                    <td>AIR</td>
                    <td>Misc Accessorial</td>
                    <td>
                        <input class="uni" type="checkbox" checked="checked" /></td>
                    <td>
                        <input type="image" src="../images/icons2/edit.png" />
                        <input type="image" src="../images/icons2/delete.png" />
                    </td>
                </tr>
            </table>
        </div>

    </div>
</asp:Content>
