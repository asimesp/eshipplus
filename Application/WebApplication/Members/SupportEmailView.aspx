﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="SupportEmailView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.SupportEmailView" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar runat="server" ID="memberToolBar" ShowDashboard="True" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">

        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" ImageUrl="~/images/utilitiesBlue.png" AlternateText="page icon"
                CssClass="pageHeaderIcon" />
            <h3>Support Email</h3>
        </div>

        <hr class="dark mb5" />

        <eShip:MessageBoxControl ID="messageBox" runat="server" Button="Ok" Icon="Information" OnOkay="OnOkayProcess" Visible="false" />


        <div class="rowgroup">
            <div class="col_1_3 bbox no-right-border mr10">
                <div class="row">
                    <div class="fieldgroup mr25">
                        <label class="wlabel">Send To</label>
                        <eShip:CustomTextBox runat="server" CssClass="w140 disabled" ReadOnly="True" Text="Support Team" />
                    </div>
                    <div class="fieldgroup">
                        <label class="wlabel">Toll Free Contact</label>
                        <eShip:CustomTextBox runat="server" ID="txtTollFree" CssClass="w140 disabled" ReadOnly="True" />
                    </div>
                </div>
                <div class="row">
                    <div class="fieldgroup">
                        <label class="wlabel">Reply Email<span class="red pl5">*</span></label>
                        <eShip:CustomTextBox ID="txtReplyEmail" runat="server" MaxLength="100" CssClass="w310" />
                        <asp:RegularExpressionValidator ID="fromExpressionValidator" runat="server" ControlToValidate="txtReplyEmail"
                            Display="Dynamic" ValidationExpression=".+[@].+[.].+" Text="*" ErrorMessage="Required Email Format: example@domain.com" />
                    </div>
                </div>
                <div class="row">
                    <div class="fieldgroup">
                        <label class="wlabel">Support Category</label>
                        <asp:DropDownList ID="ddlCategory" runat="server" CssClass="w310">
                            <asp:ListItem Value="0" Text="Shipment Question" Selected="True" />
                            <asp:ListItem Value="1" Text="General Question" />
                            <asp:ListItem Value="2" Text="POD Request" />
                            <asp:ListItem Value="3" Text="Reschedule Pickup" />
                            <asp:ListItem Value="4" Text="Freight Class Help" />
                            <asp:ListItem Value="5" Text="Other" />
                        </asp:DropDownList>
                    </div>

                </div>
                <div class="row">
                    <div class="fieldgroup">
                        <label class="wlabel">Subject<span class="red pl5">*</span></label>
                        <eShip:CustomTextBox ID="txtsubject" runat="server" MaxLength="100" CssClass="w310" />

                    </div>
                </div>
                <div class="row">
                    <div class="fieldgroup right pr30">
                        <label class="wlabel">&nbsp;</label>
                        <asp:Button runat="server" ID="btnSend" Text="SEND" CausesValidation="false" OnClick="OnSendSupportEmailClick" />
                        <asp:Button runat="server" ID="btnCancel" Text="CANCEL" OnClick="OnCancelSupportEmail"
                            CausesValidation="False" />
                    </div>
                </div>
            </div>
            <div class="col_2_3 bbox pl46 vlinedarkleft">
                <div class="row">
                    <div class="fieldgroup">
                        <label class="wlabel">Support Message<span class="red pl5">*</span></label>
                        <ajax:Editor runat="server" ID="aeBody" Height="300px" Width="650px" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <eShip:CustomHiddenField runat="server" ID="hidValidationError" />
</asp:Content>
