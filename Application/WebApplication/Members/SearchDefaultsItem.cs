﻿using System;
using System.Collections.Generic;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;

namespace LogisticsPlus.Eship.WebApplication.Members
{
	[Serializable]
	public class SearchDefaultsItem
	{
		public string Name { get; set; }
		public bool IsCurrentDefault { get; set; }
		public SearchDefaultsItemType Type { get; set; }
		public SearchField SortBy { get; set; }
		public bool SortAscending { get; set; }
		public List<ParameterColumn> Columns { get; set; }

		public SearchDefaultsItem()
		{
			Columns = new List<ParameterColumn>();
		}
	}
}
