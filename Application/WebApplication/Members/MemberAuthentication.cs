﻿using System;
using System.Web;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members
{
	public static class MemberAuthentication
	{
		public static void SetAuthenticatedUser(this User user, string redirect)
		{
			if (user.TenantId != default(long))
			{
                redirect = redirect
                    .Replace(WebApplicationConstants.RedirectUrlPrefixQuestion, WebApplicationConstants.QuestionMark)
                    .Replace(WebApplicationConstants.RedirectUrlPrefixAmperSand, WebApplicationConstants.Ampersand);

				HttpContext.Current.Response.PersistLogin(user);
				var d = new SearchProfileDefault(user);
				HttpContext.Current.Session[WebApplicationConstants.UserSearchDefaults] = d.IsNew ? null : new SearchDefaults(d);
			}
			else
			{
				HttpContext.Current.Session[WebApplicationConstants.UserSearchDefaults] = null;
			}

			HttpContext.Current.Session[WebApplicationConstants.ActiveUser] = user;
			if (!string.IsNullOrEmpty(redirect))
			{
				HttpContext.Current.Response.Redirect(redirect, false);
				HttpContext.Current.ApplicationInstance.CompleteRequest();
			}
		}

		public static void RemoveAuthenticatedUser(this User user, string redirect)
		{
			if (user.TenantId != default(long))
			{
				user.PurgeUserRecordLocks();

                var originalUserId = HttpContext.Current.Session[WebApplicationConstants.OriginalUserId];
                if (originalUserId != null)
                {
                    user = new User(originalUserId.ToLong());

                    if (user.TenantId != default(long)) user.PurgeUserRecordLocks();
                }
				HttpContext.Current.Response.RemovePersistedLogin();
			}

			HttpContext.Current.Session[WebApplicationConstants.ActiveUser] = null;
			HttpContext.Current.Session[WebApplicationConstants.UserSearchDefaults] = null;
            HttpContext.Current.Session[WebApplicationConstants.OriginalUserId] = null;
            HttpContext.Current.Session[WebApplicationConstants.IsImpersonating] = null;
			if (!string.IsNullOrEmpty(redirect))
			{
				HttpContext.Current.Response.Redirect(redirect, false);
				HttpContext.Current.ApplicationInstance.CompleteRequest();
			}
		}

        public static void Impersonate(this User userToImpersonate, long currentActiveUserId)
        {
            var originalUserId = HttpContext.Current.Session[WebApplicationConstants.OriginalUserId].ToLong();
            // don't overwrite the original User Id if we impersonate someone else while we were impersonating
            if (originalUserId == default(long))
            {
                HttpContext.Current.Session[WebApplicationConstants.OriginalUserId] = currentActiveUserId;
            }

            // but ensure that the locks from the last person impersonated are purged
            else
            {
                var formerlyImpersonatedUser = new User(currentActiveUserId);
                if (formerlyImpersonatedUser.TenantId != default(long))
                {
                    formerlyImpersonatedUser.PurgeUserRecordLocks();
                }
            }

            HttpContext.Current.Session[WebApplicationConstants.IsImpersonating] = true.ToString();

            var d = new SearchProfileDefault(userToImpersonate);
            HttpContext.Current.Session[WebApplicationConstants.UserSearchDefaults] = d.IsNew ? null : new SearchDefaults(d);
            HttpContext.Current.Session[WebApplicationConstants.ActiveUser] = userToImpersonate;

            var originalUser = new User(originalUserId == default(long) ? currentActiveUserId : originalUserId);
            
            new AuditLog
            {
                Description = string.Format("User Impersonated by: '{0}'", originalUser.Username),
                TenantId = originalUser.TenantId,
                User = originalUser,
                EntityCode = userToImpersonate.EntityName(),
                EntityId = userToImpersonate.Id.ToString(),
                LogDateTime = DateTime.Now
            }.Log();

            HttpContext.Current.Response.Redirect(DashboardView.PageAddress, false);
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }

        public static void StopImpersonating(this User impersonatedUser)
        {
            if (impersonatedUser.TenantId != default(long))
            {
                impersonatedUser.PurgeUserRecordLocks();
            }

            var originalUserId = HttpContext.Current.Session[WebApplicationConstants.OriginalUserId].ToLong();
            var originalUser = new User(originalUserId);

            var d = new SearchProfileDefault(originalUser);
            HttpContext.Current.Session[WebApplicationConstants.UserSearchDefaults] = d.IsNew ? null : new SearchDefaults(d);

            if (!originalUser.IsNew)
                HttpContext.Current.Session[WebApplicationConstants.ActiveUser] = originalUser;


            HttpContext.Current.Session[WebApplicationConstants.OriginalUserId] = null;
            HttpContext.Current.Session[WebApplicationConstants.IsImpersonating] = null;

            HttpContext.Current.Response.Redirect(DashboardView.PageAddress, false);
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }

		public static User RetrieveAuthenticatedUser(this HttpContext context)
		{
			return context.Session == null ? null : context.Session[WebApplicationConstants.ActiveUser] as User;
		}
	}
}
