﻿using System;
using System.Linq;
using System.Web.UI;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers;
using LogisticsPlus.Eship.Processor.Searches.Core;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.WebApplication.Members.Administration;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members
{
	public partial class Member : MasterPage, IViewSwitchView
	{
        public User ActiveUser { get { return Context.RetrieveAuthenticatedUser(); } }

        public event EventHandler<ViewEventArgs<string>> RecordViewSwitch;

		protected void Page_Load(object sender, EventArgs e)
		{
            new ViewSwitchHandler(this).Initialize();

			if (IsPostBack) return;

            var originalUser = new User(Session[WebApplicationConstants.OriginalUserId].ToLong());
            var isImpersonating = Session[WebApplicationConstants.IsImpersonating].ToBoolean();
            var user = Context.RetrieveAuthenticatedUser();

            litUser.Text = isImpersonating
                               ? string.Format("{0} {1}", originalUser.FirstName, originalUser.LastName)
                               : user == null
                                ? string.Empty
                                : string.Format("{0} {1}", user.FirstName, user.LastName);

            litAuthenticationText.Text = isImpersonating
                                             ? "impersonating"
                                             : "logged in as";

            litUsername.Text = user == null ? string.Empty : user.Username;

            imgStopImpersonating.Visible = isImpersonating;

            if (user != null)
            {
                imgVendorLogo.ImageUrl = user.RetrievePageLogoUrl();
                imgVendorLogo.Visible = !string.IsNullOrEmpty(imgVendorLogo.ImageUrl);
                LoadCriticalMessages(user.TenantId);

                hypUserProfile.NavigateUrl = UserProfileView.PageAddress;
            }
		}

        public void OnStopImpersonatingClicked(object sender, ImageClickEventArgs e)
        {
            ActiveUser.StopImpersonating();
        }

		private void LoadCriticalMessages(long tenantId)
		{
			var announcements = ProcessorVars.Announcements.ContainsKey(tenantId)
									? ProcessorVars.Announcements[tenantId]
									: new AnnouncementSearch().FetchEnabledAnnouncements(tenantId);

			var vAnnouncements = announcements.Where(a => a.Type == AnnouncementType.Critical).ToList();
			vAnnouncements.AddRange(announcements.Where(a => a.Type != AnnouncementType.Critical));

			lvwAnnouncements.DataSource = vAnnouncements;
			lvwAnnouncements.DataBind();
		}

		protected string FormatAnnouncement(object message)
		{
			if (message == null) return string.Empty;
			var value = message.ToString();
			const int cutOff = 100;
			if (value.Length <= cutOff) return value;
			var display = value.Substring(0, cutOff);

			for (int i = display.Length - 1; i > -1; i--)
				if (display[i] != ' ')
				{
					display = display.Substring(0, i);
				}
				else break;
			 
			return string.Format("{0}... <span id='clickForMore'>click for more</span>", display);
		}


	    protected void OnSwitchToClassicViewClicked(object sender, EventArgs e)
	    {
            if(RecordViewSwitch != null)
                RecordViewSwitch(this, new ViewEventArgs<string>(string.Format("{0} has switched to classic view", ActiveUser.Username)));

	        Session[WebApplicationConstants.ApplicationMode] = WebApplicationConstants.ClassicView;

	    }
	}
}