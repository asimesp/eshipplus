﻿using System;
using System.IO;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members
{
    public partial class ApplicationErrorListingView : MemberPageBase
    {
        public static string PageAddress
        {
            get { return "~/Members/ApplicationErrorListingView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.ErrorLog; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.AdminBlue; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var folder = WebApplicationSettings.ErrorsFolder;
                var path = Server.MapPath(folder);

                lvwErrorLog.DataSource = Directory.GetFiles(path)
                    .Select(f => new FileInfo(f))
                    .Select(i => new { FileName = i.Name, Path = folder + i.Name, DateCreated = i.CreationTime })
                    .OrderByDescending(l => l.DateCreated)
                    .ToList();
                lvwErrorLog.DataBind();
            }
        }

        protected void OnPurgeClicked(object sender, EventArgs e)
        {
            var folder = WebApplicationSettings.ErrorsFolder;
            var path = Server.MapPath(folder);
            var cutOffDate = txtCutOffDate.Text.ToDateTime().TimeToMaximum();

            var fileInfos = Directory.GetFiles(path)
                .Select(f => new FileInfo(f))
                .ToList();

            for (int i = fileInfos.Count - 1; i > -1; i--)
                if (fileInfos[i].CreationTime < cutOffDate)
                {
                    fileInfos[i].Delete();
                    fileInfos.RemoveAt(i);
                }

            lvwErrorLog.DataSource = fileInfos
                .Select(i => new { FileName = i.Name, Path = folder + i.Name, DateCreated = i.CreationTime })
                .OrderByDescending(l => l.DateCreated)
                .ToList();
            lvwErrorLog.DataBind();
        }
    }
}
