﻿using System.Data;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence
{
    public class ReportChart
    {
        public DataTable Data;
        public ChartConfiguration Chart { get; set; }
        public Pivot Pivot;
    }
}