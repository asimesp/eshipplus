﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;
using SortDirection = LogisticsPlus.Eship.Core.BusinessIntelligence.SortDirection;

namespace LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence
{
	public partial class ReportConfigurationView : MemberPageBaseWithPageStore, IReportConfigurationView
	{
		private const string ReportChartTypesKey = "ReportChartTypesKey";
		private const string SortDirectionsKey = "SortDirectionsKey";
        private const string ChartDataSourceTypesKey = "ChartDataSourceTypesKey";
		private const string SqlDbTypesMethodKey = "SqlDbTypesMethodKey";

		private const string DataColumnsHeader = "Columns";
		private const string QlikSheetsHeader = "Qlik Sheets";
		private const string SortColumnsHeader = "Sorting";
		private const string SummaryColumnsHeader = "Summaries";
		private const string PivotColumnsHeader = "Pivots";
		private const string ParametersHeader = "Parameters";
		private const string RequiredParametersHeader = "Req'd Parameters";
		private const string ChartsHeader = "Charts";

		private const string SaveFlag = "S";
		private const string DuplicateArgs = "Duplicate";
		private const string ClearConfigurationFlag = "C";
		private const string ClearOrChangeConfigurationOnTemplateFlag = "CCONT";

		public static string PageAddress { get { return "~/Members/BusinessIntelligence/ReportConfigurationView.aspx"; } }

		public override ViewCode PageCode { get { return ViewCode.ReportConfiguration; } }

		public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.ReportsBlue; } }

		public Dictionary<int, string> SqlDbTypes
		{
			set
			{
				if (!PageStore.ContainsKey(SqlDbTypesMethodKey)) PageStore.Add(SqlDbTypesMethodKey, value);
				else PageStore[SqlDbTypesMethodKey] = value;
			}
			private get { return PageStore[SqlDbTypesMethodKey] as Dictionary<int, string>; }
		}

		public Dictionary<int, string> ReportConfigurationVisibilities
		{
			set
			{
				ddlReportConfigurationVisibility.DataSource = value.Select(t => new ViewListItem(t.Value, t.Key.ToString())).ToList();
				ddlReportConfigurationVisibility.DataBind();
			}
		}

		public Dictionary<int, string> Operators
		{
			set { }
		}

		public Dictionary<int, string> ReportChartTypes
		{
			set
			{
				if (!PageStore.ContainsKey(ReportChartTypesKey)) PageStore.Add(ReportChartTypesKey, value);
				else PageStore[ReportChartTypesKey] = value;
			}
			private get { return PageStore[ReportChartTypesKey] as Dictionary<int, string>; }
		}

		public Dictionary<int, string> SortDirections
		{
			set
			{
				if (!PageStore.ContainsKey(SortDirectionsKey)) PageStore.Add(SortDirectionsKey, value);
				else PageStore[SortDirectionsKey] = value;
			}
			private get { return PageStore[SortDirectionsKey] as Dictionary<int, string>; }
		}

        public Dictionary<int, string> ChartDataSourceTypes
        {
            set
            {
                if (!PageStore.ContainsKey(ChartDataSourceTypesKey)) PageStore.Add(ChartDataSourceTypesKey, value);
                else PageStore[ChartDataSourceTypesKey] = value;
            }
            private get { return PageStore[ChartDataSourceTypesKey] as Dictionary<int, string>; }
        }

		protected List<ViewListItem> Colors
		{
			get
			{
				return ProcessorUtilities
					.GetAll<KnownColor>()
					.Select(c => new ViewListItem(c.Value, c.Key.GetString()))
					.OrderBy(i => i.Text)
					.ToList();
			}
		}

		public event EventHandler Loading;
		public event EventHandler<ViewEventArgs<ReportConfiguration>> Lock;
		public event EventHandler<ViewEventArgs<ReportConfiguration>> UnLock;
		public event EventHandler<ViewEventArgs<ReportConfiguration>> Search;
		public event EventHandler<ViewEventArgs<ReportConfiguration>> Save;
		public event EventHandler<ViewEventArgs<ReportConfiguration>> Delete;
		public event EventHandler<ViewEventArgs<ReportConfiguration>> LoadAuditLog;

		public void LogException(Exception ex)
		{
			if (ex != null) ErrorLogger.LogError(ex, Context);
		}

		public void DisplayMessages(IEnumerable<ValidationMessage> messages)
		{
			if (messages == null) return;
			if (!messages.Any()) return;

			if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == SaveFlag)
			{
				// unlock and return to read-only
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<ReportConfiguration>(new ReportConfiguration(hidReportConfigurationId.Value.ToLong(), false)));

				switch (hidFlag.Value)
				{
					case SaveFlag:
						SetEditStatus(false);
						memberToolBar.ShowUnlock = false;
						break;
				}
			}

			messageBox.Icon = messages.GenerateIcon();
			messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
			messageBox.Button = MessageButton.Ok;
			messageBox.Visible = true;
		}

		public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs) { auditLogs.Load(logs); }

		public void FailedLock(Lock @lock)
		{
			memberToolBar.ShowNew = false;
			memberToolBar.ShowSave = false;
			memberToolBar.ShowDelete = false;
			memberToolBar.ShowUnlock = false;

			SetEditStatus(false);

			litMessage.Text = @lock.BuildFailedLockMessage();
		}

		public void Set(ReportConfiguration reportConfiguration)
		{
			LoadReportConfiguration(reportConfiguration);
			SetEditStatus(!reportConfiguration.IsNew);
		}


		private void SetEditStatus(bool enabled)
		{
			seDataColumns.Enabled = enabled;
			seSortColumns.Enabled = enabled;
			seSummaryColumns.Enabled = enabled;

			hidEditStatus.Value = enabled.ToString();

			pnlDetails.Enabled = enabled;
			pnlUsers.Enabled = enabled;
			pnlDataColumns.Enabled = enabled;
			pnlSortColumns.Enabled = enabled;
			pnlQlikSheets.Enabled = enabled;
			pnlParameterColumns.Enabled = enabled;
			pnlSummaryColumns.Enabled = enabled;
			pnlChartConfiguration.Enabled = enabled;
			pnlRequiredParameterColumns.Enabled = enabled;
		    pnlPivots.Enabled = enabled;

		    foreach (var sePivotColumns in lstPivots.Items.Select(pivot => pivot.FindControl("sePivotColumns").ToSortableExtender()))
		    {
		        sePivotColumns.Enabled = enabled;
		    }

			memberToolBar.EnableSave = enabled;

			litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;

			// must do this here to load correct set of extensible actions
			memberToolBar.LoadAdditionalActions(ToolbarMoreActions());
		}

		private List<ToolbarMoreAction> ToolbarMoreActions()
		{
			var duplicate = new ToolbarMoreAction
			{
				CommandArgs = DuplicateArgs,
				ConfirmCommand = true,
				ImageUrl = IconLinks.Utilities,
				IsLink = false,
				Name = "Duplicate Configuration",
				NavigationUrl = string.Empty
			};

			var actions = new List<ToolbarMoreAction>();
			if (Access.Modify && hidReportConfigurationId.Value.ToLong() != default(long))
				actions.Add(duplicate);

			// The 'More' menu toolbar option is hidden if there are no options to display inside the 'More' toolbar option
			memberToolBar.ShowMore = actions.Any();

			return actions;
		}


		private void UpdateUsersFromView(ReportConfiguration configuration)
		{
			var userIds = lstUsers.Items.Select(item => (item.FindControl("hidUserId").ToCustomHiddenField()).Value).Select(v => v.ToLong());

			var selectUsers = userIds
				.Select(u => new ReportCustomizationUser
					{
						ReportConfiguration = configuration,
						TenantId = ActiveUser.TenantId,
						UserId = u
					}
				)
				.ToList();

			configuration.SelectUsers = selectUsers;
		}

	    private void UpdateGroupsFromView(ReportConfiguration configuration)
	    {
	        var groupIds = lstGroups.Items.Select(item => (item.FindControl("hidGroupId").ToCustomHiddenField()).Value).Select(v => v.ToLong());

	        var selectGroups = groupIds.Select(g => new ReportCustomizationGroup
	            {
	                ReportConfiguration = configuration,
	                TenantId = ActiveUser.TenantId,
                    GroupId = g
	            }
	        ).ToList();

	        configuration.SelectGroups = selectGroups;
	    }

        private void UpdateDataColumnsFromView(ReportCustomization customization)
		{
			var dataColumns = lstDataColumns.Items
				.Select(item => new ReportColumn
				{
					Name = (item.FindControl("txtDataColumnName").ToTextBox()).Text,
					Column = (item.FindControl("txtDataColumnColumn").ToTextBox()).Text,
					DataType = (item.FindControl("ddlDataColumnDataType").ToDropDownList()).SelectedValue.ToInt().ToEnum<SqlDbType>(),
					Custom = (item.FindControl("hidDataColumnCustom")).ToCustomHiddenField().Value.ToBoolean(),
				})
				.ToList();

			customization.DataColumns = dataColumns;
		}

		private void UpdateQlikSheetsFromView(ReportConfiguration configuration)
		{
			var sheets = lstQlikSheets
				.Items
				.Select(i =>
					{
						var id = i.FindControl("hidQlikSheetId").ToCustomHiddenField().Value.ToLong();
						return new QlikSheet(id, id != default(long))
							{
								Name = i.FindControl("txtQlikSheetName").ToTextBox().Text,
								Link = i.FindControl("txtQlikSheetLink").ToTextBox().Text,
								DisplayOrder = i.FindControl("hidQlikSheetIndex").ToCustomHiddenField().Value.ToInt(),
								ReportConfiguration = configuration,
								TenantId = configuration.TenantId,
							};
					})
				.ToList();

			configuration.QlikSheets = sheets;
		}

		private void UpdateSortColumnsFromView(ReportCustomization customization)
		{
			var sortColumns = lstSortColumns
				.Items
				.Select(item => new SortColumn
				{
					ReportColumnName = item.FindControl("litSortColumnName").ToLiteral().Text,
					Priority = item.FindControl("hidSortColumnIndex").ToCustomHiddenField().Value.ToInt(),
					Direction = item.FindControl("ddlSortColumnSortDirection").ToDropDownList().SelectedValue.ToInt().ToEnum<SortDirection>(),
				})
				.ToList();

			customization.SortColumns = sortColumns;
		}

		private void UpdateSummaryColumnsFromView(ReportCustomization customization)
		{
            var numericTypes = new List<SqlDbType> { SqlDbType.Decimal, SqlDbType.BigInt, SqlDbType.Float, SqlDbType.Int };

		    var summaryColumns = lstSummaryColumns
		        .Items
		        .Select(item =>
		            {
		                var summaryColumn = lstDataColumns.Items.FirstOrDefault(i => i.FindControl("txtDataColumnName").ToTextBox().Text == item.FindControl("litSummaryColumnName").ToLiteral().Text);
                        var isNumeric = summaryColumn != null && numericTypes.Contains(summaryColumn.FindControl("ddlDataColumnDataType")
                                                                             .ToDropDownList()
		                                                                     .SelectedValue
                                                                             .ToInt()
                                                                             .ToEnum<SqlDbType>());

                        return summaryColumn != null && isNumeric ? 
                                        new Summary
		                                {
		                                    Name = item.FindControl("litSummaryColumnName").ToLiteral().Text,
		                                    ReportColumnName = summaryColumn
		                                                                  .FindControl("txtDataColumnColumn").ToTextBox().Text,
		                                    SortOrder = item.FindControl("hidSummaryColumnIndex").ToCustomHiddenField().Value.ToInt(),
		                                } : null;
		            })
		        .ToList();

			customization.Summaries = summaryColumns.Where(s => s != null).ToList();
		}

		private void UpdateParameterColumnsFromView(ReportCustomization customization)
		{
			var parameters = RetrieveParameters(true);

			customization.Parameters = parameters;
		}

		private void UpdateRequiredParametersFromView(ReportCustomization customization)
		{
			var parameters = lstRequiredParameters.Items
				.Select(i => ((ReportConfigurationParameterControl)i.FindControl("reportConfigurationRequiredParameterControl")).RetrieveRequiredParameter(true))
				.ToList();

			customization.RequiredValueParameters = parameters;
		}

	    private List<Pivot> GetCurrentViewPivots()
        {
            var pivots = peLstPivots.GetData<Pivot>();

            var vPivots = lstPivots
                .Items
                .Select(i => new
                {
                    EditIndex = i.FindControl("hidPivotIndex").ToCustomHiddenField().Value.ToInt(),
                    Pivot = new Pivot
                    {
                        TableName = i.FindControl("txtPivotTableName").ToTextBox().Text,
                        GuidId = i.FindControl("hidGuidId").ToCustomHiddenField().Value,
                        PivotTableColumns = i.FindControl("lstPivotColumns").ToListView().Items.Select(n => new PivotTableColumn
                        {
                            ReportColumnName = n.FindControl("litColumnName").ToLiteral().Text,
                            DataType =
                                n.FindControl("hidDataType")
                                 .ToCustomHiddenField()
                                 .Value.ToInt()
                                 .ToEnum<SqlDbType>(),
                            MinAggregate = n.FindControl("chkTypeAggregateMin").ToAltUniformCheckBox().Checked,
                            MaxAggregate = n.FindControl("chkTypeAggregateMax").ToAltUniformCheckBox().Checked,
                            AvgAggregate = n.FindControl("chkTypeAggregateAverage").ToAltUniformCheckBox().Checked,
                            CntAggregate = n.FindControl("chkTypeAggregateCount").ToAltUniformCheckBox().Checked,
                            SumAggregate = n.FindControl("chkTypeAggregateSum").ToAltUniformCheckBox().Checked,
                            ColumnType = n.FindControl("hidColumnType").ToCustomHiddenField().Value.ToInt().ToEnum<ColumnType>()
                        }).ToList()
                    }
                });

            foreach (var vn in vPivots)
                pivots[vn.EditIndex] = vn.Pivot;

            return pivots;
        }

        private List<ChartConfiguration> GetCurrentViewCharts()
        {
            var charts = peLstCharts.GetData<ChartConfiguration>();

            var vCharts = lstCharts
                .Items
                .Select(i => new
                    {
                        EditIndex = i.FindControl("hidChartIndex").ToCustomHiddenField().Value.ToInt(),
                        Chart = new ChartConfiguration()
                            {
                                Title = i.FindControl("txtChartTitle").ToTextBox().Text,
                                XAxisDataColumn = i.FindControl("ddlXAxisDataColumn").ToDropDownList().SelectedValue,
                                XAxisTitle = i.FindControl("txtXAxisTitle").ToTextBox().Text,
                                YAxisTitle = i.FindControl("txtYAxisTitle").ToTextBox().Text,
                                DataSourceType =
                                    i.FindControl("ddlDataSource")
                                     .ToDropDownList()
                                     .SelectedValue.ToEnum<ChartDataSourceType>(),
                                PivotGuidId = i.FindControl("hidPivotGuidId").ToCustomHiddenField().Value,
                                PivotTableName = i.FindControl("hidPivotTableName").ToCustomHiddenField().Value,
                                YAxisDataConfigurations = i.FindControl("lstYAxisColumns").ToListView().Items
                                                           .Select(item => new YAxisConfiguration
                                                               {
                                                                   ReportColumnName =
                                                                       (item.FindControl("litYAxisColumnName")
                                                                            .ToLiteral()).Text,
                                                                   Type =
                                                                       (item.FindControl("ddlYAxisColumnChartType")
                                                                            .ToDropDownList()).SelectedValue.ToInt()
                                                                                              .ToEnum<ReportChartType>(),
                                                               })
                                                           .ToList()
                            }
                    });

            foreach (var vn in vCharts)
                charts[vn.EditIndex] = vn.Chart;

            return charts;
        }

		private ReportConfiguration UpdateReportConfiguration()
		{
			var configurationId = hidReportConfigurationId.Value.ToLong();
			var configuration = new ReportConfiguration(configurationId, configurationId != default(long));

			if (!hidEditStatus.Value.ToBoolean()) return configuration;

            configuration.ReportTemplateId = hidReportTemplateId.Value.ToLong();

			if (configuration.IsNew)
			{
			    configuration.TenantId = ActiveUser.TenantId;
			    configuration.LastRun = DateUtility.SystemEarliestDateTime;
			}
			else
			{
			    configuration.LoadCollections();
			}

			var createdBy = string.IsNullOrEmpty(hidReportUserId.Value) ? ActiveUser : new User(hidReportUserId.Value.ToLong());
			configuration.User = createdBy;
			configuration.Name = txtConfigurationName.Text;
			configuration.Description = txtConfigurationDescription.Text;
			configuration.QlikConfiguration = chkQlikConfiguration.Checked;
			configuration.Visibility = ddlReportConfigurationVisibility.SelectedValue.ToInt().ToEnum<ReportConfigurationVisibility>();

            if (!configuration.QlikConfiguration && configuration.ReportTemplateId != default(long))
			{
				// update customer group settings
				if (configuration.ReportTemplate.CanFilterByCustomerGroup)
				{
					configuration.CustomerGroupId = ddlCustomerGroup.SelectedValue.ToLong();
					configuration.ReadonlyCustomerGroupFilter = chkReadonlyCustomerGroupFilter.ToCheckBox().Checked;
				}
				else
				{
					configuration.CustomerGroupId = default(long);
					configuration.ReadonlyCustomerGroupFilter = false;
				}

				// update Vendor group settings
				if (configuration.ReportTemplate.CanFilterByVendorGroup)
				{
					configuration.VendorGroupId = ddlVendorGroup.SelectedValue.ToLong();
					configuration.ReadonlyVendorGroupFilter = chkReadonlyVendorGroupFilter.ToCheckBox().Checked;
				}
				else
				{
					configuration.VendorGroupId = default(long);
					configuration.ReadonlyVendorGroupFilter = false;
				}
			}
			UpdateUsersFromView(configuration);
		    UpdateGroupsFromView(configuration);

            var customization = new ReportCustomization();
			UpdateDataColumnsFromView(customization);
			UpdateQlikSheetsFromView(configuration);
			UpdateSortColumnsFromView(customization);
			UpdateSummaryColumnsFromView(customization);
			UpdateParameterColumnsFromView(customization);
		    customization.Charts = GetCurrentViewCharts();
			UpdateRequiredParametersFromView(customization);
		    customization.Pivots = GetCurrentViewPivots();
		    customization.HideDataTable = chkHideRawDataTable.Checked;

            // Remove YAxisDataConfigurations from charts that point to pivot columns with unchecked aggregates
		    foreach (var chart in customization.Charts)
		    {
		        if (chart.DataSourceType != ChartDataSourceType.Pivot) continue;

		        var pivot = customization.Pivots.FirstOrDefault(a => chart.PivotGuidId == a.GuidId);

		        if (pivot == null) continue; 
		            
		        var pivotColumns = new List<string>();
		        foreach (var col in pivot.PivotTableColumns)
		        {
		            pivotColumns.AddRange(
		                col.AggregateTypes()
		                   .Select(agg => string.Format("{0}: {1}", agg.FormattedString(), col.ReportColumnName)));
		        }

		        chart.YAxisDataConfigurations.RemoveAll(i => !pivotColumns.Contains(i.ReportColumnName));
		    }

			configuration.SerializedCustomization = customization.ToXml();

			return configuration;
		}


		private void LoadReportConfiguration(ReportConfiguration configuration)
		{
			hidReportUserId.Value = null;

			imgFindUser.Enabled = configuration.User == ActiveUser ||
								  ActiveUser.HasAccessTo(ViewCode.ReportConfigurationEditOverride);

			hidReportTemplateId.Value = configuration.ReportTemplateId.ToString();

            ddlReportConfigurationVisibility.SelectedValue = configuration.Visibility.ToInt().ToString();

			if (configuration.ReportTemplate == null && !configuration.QlikConfiguration)
			{
				hidReportTemplateId.Value = default(long).GetString();
				hidReportConfigurationId.Value = default(long).GetString();

				txtReportTemplateName.Text = string.Empty;
				txtReportTemplateDescription.Text = string.Empty;
				txtConfigurationName.Text = string.Empty;
				txtConfigurationDescription.Text = string.Empty;
				txtCreatedBy.Text = string.Empty;
				chkQlikConfiguration.Checked = false;
			    chkHideRawDataTable.Checked = false;

				lstUsers.DataSource = new List<User>();
				lstUsers.DataBind();

			    lstGroups.DataSource = new List<Group>();
			    lstGroups.DataBind();

                lstDataColumns.DataSource = new DataTable();
				lstDataColumns.DataBind();
				tabDataColumns.HeaderText = DataColumnsHeader;

				lstQlikSheets.DataSource = new List<QlikSheet>();
				lstQlikSheets.DataBind();
				tabQlikSheets.HeaderText = QlikSheetsHeader;

				lstSortColumns.DataSource = new List<SortColumn>();
				lstSortColumns.DataBind();
				tabSortColumns.HeaderText = SortColumnsHeader;

				lstSummaryColumns.DataSource = new List<Summary>();
				lstSummaryColumns.DataBind();
				tabSummaryColumns.HeaderText = SummaryColumnsHeader;

				lstParameterColumns.DataSource = new List<ParameterColumn>();
				lstParameterColumns.DataBind();
				tabParameters.HeaderText = ParametersHeader;

				lstRequiredParameters.DataSource = new List<RequiredValueParameter>();
				lstRequiredParameters.DataBind();
				tabRequiredParameters.HeaderText = RequiredParametersHeader;
                
			    peLstPivots.LoadData(new List<Pivot>());
				tabPivots.HeaderText = PivotColumnsHeader;

				lstSystemColumns.DataSource = new DataTable();
				lstSystemColumns.DataBind();

				peLstCharts.LoadData(new List<ChartConfiguration>());
			    tabChartChartConfiguration.HeaderText = ChartsHeader;

				pnlCustomerGroups.Visible = false;
				pnlVendorGroups.Visible = false;

				auditLogs.Load(new List<AuditLogsViewSearchDto>());
				memberToolBar.ShowUnlock = false;
			    txtLastRunDate.Text = string.Empty;

				ConfigureDisplay(configuration.QlikConfiguration);
				return;
			}

			if (!configuration.QlikConfiguration)
			{

				if (configuration.ReportTemplate != null && configuration.ReportTemplate.CanFilterByCustomerGroup)
				{
					pnlCustomerGroups.Visible = true;
					var viewListItems =
					new CustomerGroupSearch().FetchCustomerGroups(new List<ParameterColumn>(), ActiveUser.TenantId)
											 .Select(c => new ViewListItem(c.Name, c.Id.ToString()))
											 .ToList();
					viewListItems.Add(new ViewListItem(WebApplicationConstants.NotApplicable, default(long).ToString()));

					ddlCustomerGroup.DataSource = viewListItems;
					ddlCustomerGroup.DataBind();

					ddlCustomerGroup.SelectedValue = configuration.CustomerGroupId.ToString();
					chkReadonlyCustomerGroupFilter.ToCheckBox().Checked = configuration.ReadonlyCustomerGroupFilter;
				}
				else
				{
					pnlCustomerGroups.Visible = false;
				}


				if (configuration.ReportTemplate != null && configuration.ReportTemplate.CanFilterByVendorGroup)
				{
					pnlVendorGroups.Visible = true;
					var viewListItems =
						new VendorGroupSearch().FetchVendorGroups(new List<ParameterColumn>(), ActiveUser.TenantId)
											   .Select(c => new ViewListItem(c.Name, c.Id.ToString()))
											   .ToList();
					viewListItems.Add(new ViewListItem(WebApplicationConstants.NotApplicable, default(long).ToString()));

					ddlVendorGroup.DataSource = viewListItems;
					ddlVendorGroup.DataBind();

					ddlVendorGroup.SelectedValue = configuration.VendorGroupId.ToString();
					chkReadonlyVendorGroupFilter.ToCheckBox().Checked = configuration.ReadonlyVendorGroupFilter;
				}
				else
				{
					pnlVendorGroups.Visible = false;
				}
				//Fill System Column ListView with Data from ReportTemplate
				var defaultCustomization = configuration.ReportTemplate != null ? configuration.ReportTemplate.DefaultCustomization.FromXml<ReportCustomization>() : new ReportCustomization();

				lstSystemColumns.DataSource = defaultCustomization.DataColumns;
				lstSystemColumns.DataBind();
			}




			if (!configuration.IsNew)
			{
                var isQlikReport = configuration.ReportTemplate == null;

                hidReportTemplateId.Value = configuration.ReportTemplateId.ToString();
                txtReportTemplateName.Text = isQlikReport ? string.Empty : configuration.ReportTemplate.Name;
                txtReportTemplateDescription.Text = isQlikReport ? string.Empty : configuration.ReportTemplate.Description;

				txtConfigurationName.Text = configuration.Name;
				txtConfigurationDescription.Text = configuration.Description;
				txtCreatedBy.Text = configuration.User.Username;
				hidReportUserId.Value = configuration.User.Id.ToString();
				configuration.LoadCollections();

				var users = configuration.RetrieveSelectUsers().OrderBy(u => u.Username);
				lstUsers.DataSource = users;
				lstUsers.DataBind();

			    var groups = configuration.RetrieveSelectGroups().OrderBy(u => u.Name);
			    lstGroups.DataSource = groups;
			    lstGroups.DataBind();
            }
			else
			{
                if (configuration.ReportTemplate != null)
                {
                    hidReportTemplateId.Value = configuration.ReportTemplateId.ToString();
                    txtReportTemplateName.Text = configuration.ReportTemplate.Name;
                    txtReportTemplateDescription.Text = configuration.ReportTemplate.Description;
                }

				txtConfigurationName.Text = string.Empty;
				txtConfigurationDescription.Text = string.Empty;
				txtCreatedBy.Text = ActiveUser.Username;
				DisplayAuditLogs(new List<AuditLogsViewSearchDto>());

				var users = configuration.SelectUsers.Select(rsu => rsu.User).OrderBy(u => u.Username);
				lstUsers.DataSource = users;
				lstUsers.DataBind();

			    var groups = configuration.SelectGroups.Select(rsu => rsu.Group).OrderBy(u => u.Name);
			    lstGroups.DataSource = groups;
			    lstGroups.DataBind();
            }

			hidReportConfigurationId.Value = configuration.Id.ToString();
			chkQlikConfiguration.Checked = configuration.QlikConfiguration;


            txtLastRunDate.Text = configuration.LastRun <= DateUtility.SystemEarliestDateTime
                                    ? string.Empty
                                    : configuration.LastRun.FormattedShortDate();

			var reportCustomization = configuration.SerializedCustomization.FromXml<ReportCustomization>() ??
				new ReportCustomization
				{
					DataColumns = new List<ReportColumn>(),
					SortColumns = new List<SortColumn>(),
					Summaries = new List<Summary>(),
					Parameters = new List<ParameterColumn>(),
					RequiredValueParameters = new List<RequiredValueParameter>(),
                    Pivots = new List<Pivot>(),
					Charts = new List<ChartConfiguration>()
				};

			lstDataColumns.DataSource = reportCustomization.DataColumns;
			lstDataColumns.DataBind();
			tabDataColumns.HeaderText = reportCustomization.DataColumns.BuildTabCount(DataColumnsHeader);

			lstQlikSheets.DataSource = configuration.QlikSheets.OrderBy(q => q.DisplayOrder);
			lstQlikSheets.DataBind();
			tabQlikSheets.HeaderText = configuration.QlikSheets.BuildTabCount(QlikSheetsHeader);

			lstSortColumns.DataSource = reportCustomization.SortColumns.OrderBy(c => c.Priority);
			lstSortColumns.DataBind();
			tabSortColumns.HeaderText = reportCustomization.SortColumns.BuildTabCount(SortColumnsHeader);

			lstSummaryColumns.DataSource = reportCustomization.Summaries.OrderBy(c => c.SortOrder);
			lstSummaryColumns.DataBind();
			tabSummaryColumns.HeaderText = reportCustomization.Summaries.BuildTabCount(SummaryColumnsHeader);

			lstParameterColumns.DataSource = reportCustomization.Parameters.OrderBy(c => c.ReportColumnName).ThenBy(c => c.Operator);
			lstParameterColumns.DataBind();
			tabParameters.HeaderText = reportCustomization.Parameters.BuildTabCount(ParametersHeader);

		    chkHideRawDataTable.Checked = reportCustomization.HideDataTable;

			// check for required parameters upates
			var rp = reportCustomization.RequiredValueParameters;
			if (configuration.ReportTemplate != null)
			{
				var dConfig = configuration.ReportTemplate.DefaultCustomization.FromXml<ReportCustomization>();

				// adding new
				var arp = dConfig != null
							? dConfig.RequiredValueParameters.Where(dp => !rp.Select(p => p.Name).Contains(dp.Name)).ToList()
							: new List<RequiredValueParameter>();
				if (arp.Any()) rp.AddRange(arp);

				//removing obsolete
				var current = dConfig != null ? dConfig.RequiredValueParameters.Select(p => p.Name).ToList() : new List<string>();
				for (var i = rp.Count - 1; i >= 0; i--)
					if (!current.Contains(rp[i].Name))
						rp.RemoveAt(i);
			}
			lstRequiredParameters.DataSource = rp;
			lstRequiredParameters.DataBind();
			tabRequiredParameters.HeaderText = rp.BuildTabCount(RequiredParametersHeader);

            // pivots
            peLstPivots.LoadData(reportCustomization.Pivots);
		    tabPivots.HeaderText = reportCustomization.Pivots.BuildTabCount(PivotColumnsHeader);

            peLstCharts.LoadData(reportCustomization.Charts);
            tabChartChartConfiguration.HeaderText = reportCustomization.Charts.BuildTabCount(ChartsHeader);

		    memberToolBar.ShowUnlock = configuration.HasUserLock(ActiveUser, configuration.Id);
			ConfigureDisplay(configuration.QlikConfiguration);
		}

		private void DuplicateConfiguration()
		{
			var configuration = UpdateReportConfiguration().Duplicate();

			LoadReportConfiguration(configuration);

			SetEditStatus(true);
		}


		private void ProcessConfigurationTemplateSelection(ReportTemplate reportTemplate)
		{
			var configuration = UpdateReportConfiguration();


			if (reportTemplate == null || configuration.ReportTemplateId != reportTemplate.Id)
			{
				configuration.SerializedCustomization = new ReportCustomization().ToXml();
			}

			configuration.ReportTemplate = reportTemplate;

			LoadReportConfiguration(configuration);
		}



		private void ProcessTransferredRequest(long configurationId)
		{
			if (configurationId != default(long))
			{
				var configuration = new ReportConfiguration(configurationId);
				
				LoadReportConfiguration(configuration);

				if (LoadAuditLog != null)
					LoadAuditLog(this, new ViewEventArgs<ReportConfiguration>(configuration));
			}
		}

		private void ConfigureReportTemplatePopup()
		{
			var templates = new ReportTemplateSearch().FetchReportTemplates(new List<ParameterColumn>());

			rptReportTemplates.DataSource = templates.OrderBy(r => r.Name);
			rptReportTemplates.DataBind();

			pnlNoReports.Visible = !templates.Any();
		}


		private void ConfigureDisplay(bool qlikConfiguration)
		{
			var template = new ReportTemplate(hidReportTemplateId.Value.ToLong());

			pnlCustomerGroups.Visible = !qlikConfiguration && template.CanFilterByCustomerGroup;
			pnlVendorGroups.Visible = !qlikConfiguration && template.CanFilterByVendorGroup;
			pnlTemplateInfo.Visible = !qlikConfiguration;

			tabQlikSheets.Visible = qlikConfiguration;

			tabDataColumns.Visible = !qlikConfiguration;
			tabSortColumns.Visible = !qlikConfiguration;
			tabSummaryColumns.Visible = !qlikConfiguration;
			tabParameters.Visible = !qlikConfiguration;
			tabRequiredParameters.Visible = !qlikConfiguration;
		    tabPivots.Visible = !qlikConfiguration;
			tabChartChartConfiguration.Visible = !qlikConfiguration;


		}


		protected void Page_Load(object sender, EventArgs e)
		{
			new ReportConfigurationHandler(this).Initialize();

			memberToolBar.ShowSave = Access.Modify;
			memberToolBar.ShowDelete = Access.Remove;
			memberToolBar.ShowNew = Access.Modify;
			memberToolBar.ShowEdit = Access.Modify;
			memberToolBar.LoadAdditionalActions(ToolbarMoreActions());

			if (IsPostBack) return;

			if (Loading != null)
				Loading(this, new EventArgs());

			if (Session[WebApplicationConstants.TransferReportConfigurationId] != null)
			{
				ProcessTransferredRequest(Session[WebApplicationConstants.TransferReportConfigurationId].ToLong());
				Session[WebApplicationConstants.TransferReportConfigurationId] = null;
			}

			SetEditStatus(false);

			ConfigureReportTemplatePopup();
		}


		protected void OnToolbarNewClicked(object sender, EventArgs e)
		{
			litErrorMessages.Text = string.Empty;

			LoadReportConfiguration(new ReportConfiguration { ReportTemplate = new ReportTemplate(), Visibility = ReportConfigurationVisibility.EmployeesOnly, QlikConfiguration = false });
			SetEditStatus(true);
			txtCreatedBy.Text = ActiveUser.Username;
		}

		protected void OnToolbarEditClicked(object sender, EventArgs e)
		{
			var reportConfiguration = new ReportConfiguration(hidReportConfigurationId.Value.ToLong(), false);

			if (Lock == null || reportConfiguration.IsNew) return;

			if (ActiveUser.Id != reportConfiguration.UserId && !ActiveUser.HasAccessTo(ViewCode.ReportConfigurationEditOverride)) return;

			if ((hidReportTemplateId.Value.ToLong() != default(long) && hidReportConfigurationId.Value.ToLong() != default(long)) ||
			    hidReportConfigurationId.Value.ToLong() != default(long) && chkQlikConfiguration.Checked)
				SetEditStatus(true);

			memberToolBar.ShowUnlock = true;
			Lock(this, new ViewEventArgs<ReportConfiguration>(reportConfiguration));
			LoadReportConfiguration(reportConfiguration);
		}

		protected void OnToolbarUnlockClicked(object sender, EventArgs e)
		{
			var reportConfiguration = new ReportConfiguration(hidReportConfigurationId.Value.ToLong(), false);

			if (UnLock == null || reportConfiguration.IsNew) return;

			SetEditStatus(false);
			memberToolBar.ShowUnlock = false;
			UnLock(this, new ViewEventArgs<ReportConfiguration>(reportConfiguration));

		}

		protected void OnToolbarSaveClicked(object sender, EventArgs e)
		{
			var reportConfiguration = UpdateReportConfiguration();

			// pre-validation for YAxisDataConfigurations because Y Axis Configurations are input ready
			var customization = new ReportCustomization {Charts = GetCurrentViewCharts(), Pivots = GetCurrentViewPivots()};
            UpdateDataColumnsFromView(customization);

		    var reportValidator = new ReportConfigurationValidator();
            var pivotTableNames = customization.Pivots.Select(p => p.TableName).ToList();
		    var chartNames = customization.Charts.Select(c => c.Title).ToList();

		    if (
                !reportValidator.NamesAreValid(customization.DataColumns.Select(c => c.Name).ToList(), pivotTableNames,
		                                         chartNames, reportConfiguration.Name))
		    {
		        DisplayMessages(reportValidator.Messages);
		        return;
		    }

		    foreach (var chart in customization.Charts)
		    {
		        var validator = new ChartValidationUtilities();

                var yColumns = chart.YAxisDataConfigurations
                                    .Select(c => new KeyValuePair<string, ReportChartType>
                                                     (
                                                     c.ReportColumnName,
                                                     c.Type
                                                     ))
                                    .ToList();

                if (validator.IsValid(new List<KeyValuePair<string, ReportChartType>>(), yColumns) &&
                    (validator.XAxisIsValid(chart.XAxisDataColumn, chart.DataSourceType, customization.DataColumns.Select(r => r.Name).ToList()))) continue;

                DisplayMessages(validator.Messages);
                return;
            }


            if (reportConfiguration.QlikConfiguration)
			{
				reportConfiguration.ReportTemplateId = default(long);
				reportConfiguration.ReportTemplate = new ReportTemplate();
				reportConfiguration.SerializedCustomization = string.Empty;
				reportConfiguration.CustomerGroupId = default(long);
				reportConfiguration.CustomerGroup = new CustomerGroup();
				reportConfiguration.VendorGroupId = default(long);
				reportConfiguration.VendorGroup = new VendorGroup();
			}
			else
			{
				reportConfiguration.QlikSheets = new List<QlikSheet>();
			}

			hidFlag.Value = SaveFlag;

			if (Save != null)
				Save(this, new ViewEventArgs<ReportConfiguration>(reportConfiguration));

			if (LoadAuditLog != null)
				LoadAuditLog(this, new ViewEventArgs<ReportConfiguration>(reportConfiguration));
		}

		protected void OnToolbarDeleteClicked(object sender, EventArgs e)
		{
			var reportConfiguration = new ReportConfiguration(hidReportConfigurationId.Value.ToLong(), false);

			if (ActiveUser.Id != reportConfiguration.UserId && !ActiveUser.HasAccessTo(ViewCode.ReportConfigurationEditOverride))
			{
				DisplayMessages(new[] { ValidationMessage.Error("You do not have access to delete configurations you did not create.") });
				return;
			}

			if (Delete != null)
				Delete(this, new ViewEventArgs<ReportConfiguration>(reportConfiguration));

			reportConfigurationFinder.Reset();
		}

		protected void OnToolbarFindClicked(object sender, EventArgs e)
		{
			reportConfigurationFinder.Visible = true;
		}

		protected void OnToolbarCommand(object sender, ViewEventArgs<string> e)
		{
			switch (e.Argument)
			{
				case DuplicateArgs:
					DuplicateConfiguration();
					break;
			}
		}


		protected void OnQlikConfigurationCheckChanged(object sender, EventArgs e)
		{
			ConfigureDisplay(chkQlikConfiguration.Checked);
		}

		protected void OnReportConfigurationFinderItemSelected(object sender, ViewEventArgs<ReportConfiguration> e)
		{
			litErrorMessages.Text = string.Empty;

			if (hidReportConfigurationId.Value.ToLong() != default(long))
			{
				var oldReportConfiguration = new ReportConfiguration(hidReportConfigurationId.Value.ToLong(), false);

				if (UnLock != null)
					UnLock(this, new ViewEventArgs<ReportConfiguration>(oldReportConfiguration));
			}

			var reportConfiguration = e.Argument;

			LoadReportConfiguration(reportConfiguration);

			reportConfigurationFinder.Visible = false;

			var permissionToEdit = ActiveUser.Id == reportConfiguration.UserId || ActiveUser.HasAccessTo(ViewCode.ReportConfigurationEditOverride);

			if (reportConfigurationFinder.OpenForEdit && Lock != null && permissionToEdit)
			{
				SetEditStatus(true);
				memberToolBar.ShowUnlock = true;
				Lock(this, new ViewEventArgs<ReportConfiguration>(reportConfiguration));
			}
			else
			{
				SetEditStatus(false);
			}

			if (LoadAuditLog != null) LoadAuditLog(this, new ViewEventArgs<ReportConfiguration>(reportConfiguration));
		}

		protected void OnReportConfigurationFinderItemCancelled(object sender, EventArgs e)
		{
			reportConfigurationFinder.Visible = false;
		}


		protected void OnAddUserClicked(object sender, EventArgs e)
		{
			userFinder.Reset();
			userFinder.EnableMultiSelection = true;
			userFinder.Visible = true;
		}

		protected void OnClearUsersClicked(object sender, EventArgs e)
		{
			lstUsers.DataSource = new List<User>();
			lstUsers.DataBind();
		}

		protected void OnDeleteUserClicked(object sender, ImageClickEventArgs e)
		{
			var idToRemove = (((ImageButton)sender).FindControl("hidUserId").ToCustomHiddenField()).Value;
			var vUsers = lstUsers.Items
				.Select(item => new
				{
					Id = (item.FindControl("hidUserId").ToCustomHiddenField()).Value,
					Username = (item.FindControl("lblUsername").ToLabel()).Text,
					FirstName = (item.FindControl("litFirstName").ToLiteral()).Text,
					LastName = (item.FindControl("litLastName").ToLiteral()).Text,
					Email = (item.FindControl("litEmail").ToLiteral()).Text,
				})
				.Where(u => u.Id != idToRemove)
				.ToList();
			lstUsers.DataSource = vUsers.OrderBy(item => item.Username);
			lstUsers.DataBind();
		}

		protected void OnUserFinderMultiItemSelected(object sender, ViewEventArgs<List<User>> e)
		{
			var vUsers = lstUsers.Items
				.Select(item => new
				{
					Id = (item.FindControl("hidUserId").ToCustomHiddenField()).Value,
					Username = (item.FindControl("lblUsername").ToLabel()).Text,
					FirstName = (item.FindControl("litFirstName").ToLiteral()).Text,
					LastName = (item.FindControl("litLastName").ToLiteral()).Text,
					Email = (item.FindControl("litEmail").ToLiteral()).Text,
				})
				.ToList();

			var usersToAdd = e.Argument
				.Select(u => new { Id = u.Id.ToString(), u.Username, u.FirstName, u.LastName, u.Email })
				.Where(u => !vUsers.Select(vu => vu.Username).Contains(u.Username));

			vUsers.AddRange(usersToAdd);

			lstUsers.DataSource = vUsers.OrderBy(item => item.Username);
			lstUsers.DataBind();

			userFinder.Visible = false;
		}

		protected void OnUserFinderSelectionCancelled(object sender, EventArgs e)
		{
			userFinder.Visible = false;
		}

		protected void OnUserFinderSingleItemSelected(object sender, ViewEventArgs<User> e)
		{
			var reportUser = e.Argument;

			txtCreatedBy.Text = reportUser.Username;
			hidReportUserId.Value = reportUser.Id.ToString();
			userFinder.Visible = false;
		}

		protected void OnFindNewReportUserClicked(object sender, ImageClickEventArgs e)
		{
			userFinder.Reset();
			userFinder.EnableMultiSelection = false;

			userFinder.Visible = true;
		}

        protected void OnAddGroupClicked(object sender, EventArgs e)
        {
            groupFinder.Reset();
            groupFinder.EnableMultiSelection = true;
            groupFinder.Visible = true;
        }

        protected void OnClearGroupsClicked(object sender, EventArgs e)
        {
            lstGroups.DataSource = new List<Group>();
            lstGroups.DataBind();
        }

        protected void OnDeleteGroupClicked(object sender, ImageClickEventArgs e)
        {
            var idToRemove = (((ImageButton)sender).FindControl("hidGroupId").ToCustomHiddenField()).Value;
            var vGroups = lstGroups.Items
                .Select(item => new
                {
                    Id = (item.FindControl("hidGroupId").ToCustomHiddenField()).Value,
                    Name = (item.FindControl("litGroupName").ToLiteral()).Text
                })
                .Where(u => u.Id != idToRemove)
                .ToList();
            lstGroups.DataSource = vGroups.OrderBy(item => item.Name);
            lstGroups.DataBind();
        }

        protected void OnGroupFinderMultiItemSelected(object sender, ViewEventArgs<List<Group>> e)
        {
            var vGroups = lstGroups.Items
                .Select(item => new
                {
                    Id = (item.FindControl("hidGroupId").ToCustomHiddenField()).Value,
                    Name = (item.FindControl("litGroupName").ToLiteral()).Text
                })
                .ToList();

            var groupsToAdd = e.Argument
                .Select(g => new { Id = g.Id.ToString(), Name = g.Name })
                .Where(g => !vGroups.Select(vg => vg.Name).Contains(g.Name));

            vGroups.AddRange(groupsToAdd);

            lstGroups.DataSource = vGroups.OrderBy(item => item.Name);
            lstGroups.DataBind();

            groupFinder.Visible = false;
        }

        protected void OnGroupFinderSelectionCancelled(object sender, EventArgs e)
        {
            groupFinder.Visible = false;
        }


        protected void OnAddSystemColumnsClicked(object sender, EventArgs e)
		{
			var systemColumns = lstSystemColumns.Items
				.Select(i => new
				{
					Name = (i.FindControl("hidSystemColumnName").ToCustomHiddenField()).Value,
					Column = (i.FindControl("hidSystemColumnColumn").ToCustomHiddenField()).Value,
					DataType = (i.FindControl("hidSystemColumnDataType").ToCustomHiddenField()).Value,
					Custom = (i.FindControl("hidSystemColumnCustom").ToCustomHiddenField()).Value,
				})
				.ToList();

			chkSystemColumnSelectAll.Checked = false;
			lstAddSystemColumns.DataSource = systemColumns.OrderBy(c => c.Name);
			lstAddSystemColumns.DataBind();

			pnlAddSystemColumns.Visible = true;
			pnlDimScreen.Visible = true;
		}

		protected void OnAddSelectedSystemColumnsDoneClicked(object sender, EventArgs e)
		{
			var columns = lstDataColumns.Items
				.Select(i => new
				{
					Name = i.FindControl("txtDataColumnName").ToTextBox().Text,
					Column = i.FindControl("txtDataColumnColumn").ToTextBox().Text,
					DataType = (i.FindControl("ddlDataColumnDataType").ToDropDownList()).SelectedValue.ToInt().ToEnum<SqlDbType>(),
					Custom = (i.FindControl("hidDataColumnCustom")).ToCustomHiddenField().Value.ToBoolean(),
					Selection = false
				})
				.ToList();

			var newColumns = lstAddSystemColumns.Items
				.Select(i => new
				{
					Name = (i.FindControl("litSystemColumnName").ToLiteral()).Text,
					Column = (i.FindControl("hidSystemColumnColumnName").ToCustomHiddenField()).Value,
					DataType = (i.FindControl("hidSystemColumnDataType").ToCustomHiddenField()).Value.ToEnum<SqlDbType>(),
					Custom = (i.FindControl("hidSystemColumnCustom").ToCustomHiddenField()).Value.ToBoolean(),
					Selection = (i.FindControl("chkSelection")).ToCheckBox().Checked
				})
				.Where(c => c.Selection)
				.ToList();

			columns.AddRange(newColumns);

			lstDataColumns.DataSource = columns;
			lstDataColumns.DataBind();
			tabDataColumns.HeaderText = columns.BuildTabCount(DataColumnsHeader);

            // update available system columns for pivots
            var pivots = GetCurrentViewPivots();
            peLstPivots.LoadData(pivots);
			
            pnlAddSystemColumns.Visible = false;
			pnlDimScreen.Visible = false;
		}

		protected void OnCloseAddSystemColumnsClicked(object sender, EventArgs e)
		{
			pnlAddSystemColumns.Visible = false;
			pnlDimScreen.Visible = false;
		}

		protected void OnAddCustomColumnClicked(object sender, EventArgs e)
		{
			var columns = lstDataColumns.Items
				.Select(i => new
				{
					Name = i.FindControl("txtDataColumnName").ToTextBox().Text,
					Column = i.FindControl("txtDataColumnColumn").ToTextBox().Text,
					DataType = (i.FindControl("ddlDataColumnDataType").ToDropDownList()).SelectedValue.ToInt().ToEnum<SqlDbType>(),
					Custom = (i.FindControl("hidDataColumnCustom")).ToCustomHiddenField().Value.ToBoolean(),
				})
				.ToList();

			columns.Add(new
			{
				Name = string.Empty,
				Column = string.Empty,
				DataType = SqlDbType.NVarChar,
				Custom = true,
			});

			lstDataColumns.DataSource = columns;
			lstDataColumns.DataBind();
			tabDataColumns.HeaderText = columns.BuildTabCount(DataColumnsHeader);
			athtuTabUpdaterMain.SetForUpdate(tabDataColumns.ClientID, columns.BuildTabCount(DataColumnsHeader));
			pnlDimScreen.Visible = false;
		}

		protected void OnDeleteDataColumnClicked(object sender, ImageClickEventArgs e)
		{
			var imageButton = (ImageButton)sender;

			var columns = lstDataColumns.Items
				.Select(i => new
				{
					Name = i.FindControl("txtDataColumnName").ToTextBox().Text,
					Column = i.FindControl("txtDataColumnColumn").ToTextBox().Text,
					DataType = (i.FindControl("ddlDataColumnDataType").ToDropDownList()).SelectedValue.ToInt().ToEnum<SqlDbType>(),
					Custom = (i.FindControl("hidDataColumnCustom")).ToCustomHiddenField().Value.ToBoolean(),
				})
				.ToList();

			var index = imageButton.Parent.FindControl("hidDataColumnIndex").ToCustomHiddenField().Value.ToInt();
			var column = columns[index];

			columns.RemoveAt(index);

			lstDataColumns.DataSource = columns;
			lstDataColumns.DataBind();
			tabDataColumns.HeaderText = columns.BuildTabCount(DataColumnsHeader);
			athtuTabUpdaterMain.SetForUpdate(tabDataColumns.ClientID, columns.BuildTabCount(DataColumnsHeader));


            // remove corresponding pivot/aggregate columns
            if (lstPivots.Items.Any())
            {
                if (!columns.Any())
                {
                    peLstPivots.LoadData(new List<Pivot>());
                    tabPivots.HeaderText = PivotColumnsHeader;
                    athtuTabUpdaterPivots.SetForUpdate(tabPivots.ClientID, PivotColumnsHeader);
                }
                else
                {
                    var pivots = GetCurrentViewPivots();
                    foreach (var pivot in pivots)
                    {
                        pivot.PivotTableColumns.RemoveAll(p => p.ReportColumnName == column.Name);
                    }
                    peLstPivots.LoadData(pivots);
                }
            }

            if (lstCharts.Items.Any())
            {
                if (!columns.Any())
                {
                    peLstCharts.LoadData(new List<Pivot>());
                    tabChartChartConfiguration.HeaderText = ChartsHeader;
                    athtuTabUpdaterCharts.SetForUpdate(tabChartChartConfiguration.ClientID, ChartsHeader);
                }
                else
                {
                    var charts = GetCurrentViewCharts();
                    foreach (var chart in charts)
                    {
                        chart.YAxisDataConfigurations.RemoveAll(p => p.ReportColumnName == column.Name);
                    }
                    peLstCharts.LoadData(charts);
                }
            }

			// if column used, remove
			if (column.Custom)
			{
                var customization = new ReportCustomization();
                
				if (lstSortColumns.Items.Any())
				{
					UpdateSortColumnsFromView(customization);
					customization.SortColumns.RemoveAll(c => c.ReportColumnName == column.Name);
					lstSortColumns.DataSource = customization.SortColumns.OrderBy(c => c.Priority);
					lstSortColumns.DataBind();
					tabSortColumns.HeaderText = customization.SortColumns.BuildTabCount(SortColumnsHeader);
					athtuTabUpdaterSorting.SetForUpdate(tabSortColumns.ClientID, customization.SortColumns.BuildTabCount(SortColumnsHeader));
				}

				if (lstSummaryColumns.Items.Any())
				{
					UpdateSummaryColumnsFromView(customization);
					customization.Summaries.RemoveAll(s => s.Name == column.Name);
					lstSummaryColumns.DataSource = customization.Summaries.OrderBy(c => c.SortOrder);
					lstSummaryColumns.DataBind();
					tabSummaryColumns.HeaderText = customization.Summaries.BuildTabCount(SummaryColumnsHeader);
					athtuTabUpdaterSummaries.SetForUpdate(tabSummaryColumns.ClientID, customization.Summaries.BuildTabCount(SummaryColumnsHeader));
				}

				if (lstParameterColumns.Items.Any())
				{
					UpdateParameterColumnsFromView(customization);
					customization.Parameters.RemoveAll(p => p.ReportColumnName == column.Name);
					lstParameterColumns.DataSource = customization.Parameters.OrderBy(c => c.ReportColumnName).ThenBy(c => c.Operator);
					lstParameterColumns.DataBind();
					tabParameters.HeaderText = customization.Parameters.BuildTabCount(ParametersHeader);
					athtuTabUpdaterParameters.SetForUpdate(tabParameters.ClientID, customization.Parameters.BuildTabCount(ParametersHeader));
				}
			}
		}

		protected void OnClearDataColumnsClicked(object sender, EventArgs e)
		{
			var columns = lstDataColumns.Items
				.Select(i => new
				{
					Name = i.FindControl("txtDataColumnName").ToTextBox().Text,
					Column = i.FindControl("txtDataColumnColumn").ToTextBox().Text,
					DataType = (i.FindControl("ddlDataColumnDataType").ToDropDownList()).SelectedValue.ToInt().ToEnum<SqlDbType>(),
					Custom = (i.FindControl("hidDataColumnCustom")).ToCustomHiddenField().Value.ToBoolean(),
				})
				.Where(c => c.Custom)
				.ToList();

			lstDataColumns.DataSource = new List<ReportColumn>();
			lstDataColumns.DataBind();
			tabDataColumns.HeaderText = DataColumnsHeader;
			athtuTabUpdaterMain.SetForUpdate(tabDataColumns.ClientID, DataColumnsHeader);

            // remove corresponding pivot/aggregate columns
            if (lstPivots.Items.Any())
            {
                peLstPivots.LoadData(new List<Pivot>());
                tabPivots.HeaderText = PivotColumnsHeader;
                athtuTabUpdaterPivots.SetForUpdate(tabPivots.ClientID, PivotColumnsHeader);
            }

            if (lstCharts.Items.Any())
            {
                peLstCharts.LoadData(new List<ChartConfiguration>());
                tabChartChartConfiguration.HeaderText = ChartsHeader;
                athtuTabUpdaterCharts.SetForUpdate(tabChartChartConfiguration.ClientID, ChartsHeader);
            }

            if (!columns.Any()) return;

            // handle removal of all custom columns from other listviews
            var customization = new ReportCustomization();
            var colNames = columns.Select(c => c.Name);

			if (lstSortColumns.Items.Any())
			{
				UpdateSortColumnsFromView(customization);
				customization.SortColumns.RemoveAll(c => colNames.Contains(c.ReportColumnName));
				lstSortColumns.DataSource = customization.SortColumns.OrderBy(c => c.Priority);
				lstSortColumns.DataBind();
				tabSortColumns.HeaderText = customization.SortColumns.BuildTabCount(SortColumnsHeader);
				athtuTabUpdaterSorting.SetForUpdate(tabSortColumns.ClientID, customization.SortColumns.BuildTabCount(SortColumnsHeader));
			}

			if (lstSummaryColumns.Items.Any())
			{
				UpdateSummaryColumnsFromView(customization);
				customization.Summaries.RemoveAll(s => colNames.Contains(s.Name));
				lstSummaryColumns.DataSource = customization.Summaries.OrderBy(c => c.SortOrder);
				lstSummaryColumns.DataBind();
				tabSummaryColumns.HeaderText = customization.Summaries.BuildTabCount(SummaryColumnsHeader);
				athtuTabUpdaterSummaries.SetForUpdate(tabSummaryColumns.ClientID, customization.Summaries.BuildTabCount(SummaryColumnsHeader));
			}

			if (lstParameterColumns.Items.Any())
			{
				UpdateParameterColumnsFromView(customization);
				customization.Parameters.RemoveAll(p => colNames.Contains(p.ReportColumnName));
				lstParameterColumns.DataSource = customization.Parameters.OrderBy(c => c.ReportColumnName).ThenBy(c => c.Operator);
				lstParameterColumns.DataBind();
				tabParameters.HeaderText = customization.Parameters.BuildTabCount(ParametersHeader);
				athtuTabUpdaterParameters.SetForUpdate(tabParameters.ClientID, customization.Parameters.BuildTabCount(ParametersHeader));
			}
		}

		protected void OnDataColumnsItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var data = e.Item.DataItem;

			var ddlDataColumnDataType = e.Item.FindControl("ddlDataColumnDataType").ToDropDownList();
			ddlDataColumnDataType.DataSource = SqlDbTypes;
			ddlDataColumnDataType.DataBind();
			if (data.HasGettableProperty("DataType")) ddlDataColumnDataType.SelectedValue = data.GetPropertyValue("DataType").ToInt().GetString();
			if (data.HasGettableProperty("Custom")) ddlDataColumnDataType.Enabled = data.GetPropertyValue("Custom").ToBoolean();
		}

		protected void OnDataColumnsItemsReordered(object sender, ViewEventArgs<List<int>> e)
		{
			lstDataColumns.DataSource = e
				.Argument
				.Select(idx => new
				{
					Name = lstDataColumns.Items[idx - 1].FindControl("txtDataColumnName").ToTextBox().Text,
					Column = lstDataColumns.Items[idx - 1].FindControl("txtDataColumnColumn").ToTextBox().Text,
					DataType = lstDataColumns.Items[idx - 1].FindControl("ddlDataColumnDataType").ToDropDownList().SelectedValue.ToInt().ToEnum<SqlDbType>(),
					Custom = lstDataColumns.Items[idx - 1].FindControl("hidDataColumnCustom").ToCustomHiddenField().Value.ToBoolean(),
				});
			lstDataColumns.DataBind();
		}

		protected void OnAddQlikSheetClicked(object sender, EventArgs e)
		{
			var sheets = lstQlikSheets
				.Items
				.Select(i => new
					{
						Id = i.FindControl("hidQlikSheetId").ToCustomHiddenField().Value,
						Name = i.FindControl("txtQlikSheetName").ToTextBox().Text,
						Link = i.FindControl("txtQlikSheetLink").ToTextBox().Text,
					})
				.ToList();

			sheets.Add(new
				{
					Id = "0",
					Name = string.Empty,
					Link = string.Empty,
				});

			lstQlikSheets.DataSource = sheets;
			lstQlikSheets.DataBind();
			tabQlikSheets.HeaderText = sheets.BuildTabCount(QlikSheetsHeader);
			athtuTabUpdaterMain.SetForUpdate(tabQlikSheets.ClientID, sheets.BuildTabCount(QlikSheetsHeader));
			pnlDimScreen.Visible = false;
		}

		protected void OnClearQlikSheetsClicked(object sender, EventArgs e)
		{
			lstQlikSheets.DataSource = new List<QlikSheet>();
			lstQlikSheets.DataBind();
			tabQlikSheets.HeaderText = QlikSheetsHeader;
			athtuTabUpdaterMain.SetForUpdate(tabQlikSheets.ClientID, QlikSheetsHeader);
		}

		protected void OnQlikSheetsReordered(object sender, ViewEventArgs<List<int>> e)
		{
			lstQlikSheets.DataSource = e
				.Argument
				.Select(idx => new
					{
						Id = lstQlikSheets.Items[idx - 1].FindControl("hidQlikSheetId").ToCustomHiddenField().Value,
						Name = lstQlikSheets.Items[idx - 1].FindControl("txtQlikSheetName").ToTextBox().Text,
						Link = lstQlikSheets.Items[idx - 1].FindControl("txtQlikSheetLink").ToTextBox().Text,
					});
			lstQlikSheets.DataBind();
		}

		protected void OnDeleteQlikSheetClicked(object sender, ImageClickEventArgs e)
		{
			var indexToRemove = (((ImageButton)sender).FindControl("hidQlikSheetIndex").ToCustomHiddenField()).Value;
			var vSheets = lstQlikSheets
				.Items
				.Select(i => new
					{
						Id = i.FindControl("hidQlikSheetId").ToCustomHiddenField().Value,
						Name = i.FindControl("txtQlikSheetName").ToTextBox().Text,
						Link = i.FindControl("txtQlikSheetLink").ToTextBox().Text,
					})
				.ToList();
			vSheets.RemoveAt(indexToRemove.ToInt());
			lstQlikSheets.DataSource = vSheets;
			lstQlikSheets.DataBind();
			tabQlikSheets.HeaderText = vSheets.BuildTabCount(QlikSheetsHeader);
			athtuTabUpdaterMain.SetForUpdate(tabQlikSheets.ClientID, vSheets.BuildTabCount(QlikSheetsHeader));
		}


		protected void OnAddSortColumnsClicked(object sender, EventArgs e)
		{
			var columns = lstSystemColumns.Items
							.Select(i => new
							{
								Name = (i.FindControl("hidSystemColumnName").ToCustomHiddenField()).Value,
								Custom = false,
							})
							.ToList();
			columns.AddRange(lstDataColumns.Items
							.Select(i => new
							{
								Name = (i.FindControl("txtDataColumnName").ToTextBox()).Text,
								Custom = (i.FindControl("hidDataColumnCustom")).ToCustomHiddenField().Value.ToBoolean(),
							})
							.Where(i => i.Custom)
							.ToList());

			chkSortColumnSelectAll.Checked = false;
			lstEditSortColumns.DataSource = columns.OrderBy(c => c.Name);
			lstEditSortColumns.DataBind();

			pnlEditSortColumns.Visible = true;
			pnlDimScreen.Visible = true;
		}

		protected void OnAddSelectedSortColumnsDoneClicked(object sender, EventArgs e)
		{
			var columns = lstSortColumns.Items
				.Select(i => new
				{
					ReportColumnName = (i.FindControl("litSortColumnName").ToLiteral()).Text,
					Direction = (i.FindControl("ddlSortColumnSortDirection").ToDropDownList()).SelectedValue.ToInt().ToEnum<SortDirection>(),
					Selection = false
				})
				.ToList();

			var newColumns = lstEditSortColumns.Items
				.Select(i => new
					{
						ReportColumnName = (i.FindControl("litSortColumnName").ToLiteral()).Text,
						Direction = SortDirection.Ascending,
						Selection = (i.FindControl("chkSelection")).ToCheckBox().Checked
					})
				.ToList();

			columns.AddRange(newColumns.Where(c => c.Selection));

			lstSortColumns.DataSource = columns;
			lstSortColumns.DataBind();
			tabSortColumns.HeaderText = columns.BuildTabCount(SortColumnsHeader);

			pnlEditSortColumns.Visible = false;
			pnlDimScreen.Visible = false;
		}

		protected void OnCloseEditSortColumnsClicked(object sender, EventArgs e)
		{
			pnlEditSortColumns.Visible = false;
			pnlDimScreen.Visible = false;
		}

		protected void OnDeleteSortColumnClicked(object sender, ImageClickEventArgs e)
		{
			var imageButton = (ImageButton)sender;

			var columns = lstSortColumns.Items
				.Select(i => new
				{
					ReportColumnName = (i.FindControl("litSortColumnName").ToLiteral()).Text,
					Direction = (i.FindControl("ddlSortColumnSortDirection").ToDropDownList()).SelectedValue.ToInt().ToEnum<SortDirection>(),
				})
				.ToList();

			columns.RemoveAt((imageButton.Parent.FindControl("hidSortColumnIndex").ToCustomHiddenField()).Value.ToInt());

			lstSortColumns.DataSource = columns;
			lstSortColumns.DataBind();
			tabSortColumns.HeaderText = columns.BuildTabCount(SortColumnsHeader);
			athtuTabUpdaterMain.SetForUpdate(tabSortColumns.ClientID, columns.BuildTabCount(SortColumnsHeader));
		}

		protected void OnClearSortColumnsClicked(object sender, EventArgs e)
		{
			lstSortColumns.DataSource = new List<SortColumn>();
			lstSortColumns.DataBind();
			tabSortColumns.HeaderText = SortColumnsHeader;
			athtuTabUpdaterMain.SetForUpdate(tabSortColumns.ClientID, SortColumnsHeader);
		}

		protected void OnSortColumnsItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var data = e.Item.DataItem;

			var ddlSortColumnSortDirection = e.Item.FindControl("ddlSortColumnSortDirection").ToDropDownList();
			ddlSortColumnSortDirection.DataSource = SortDirections;
			ddlSortColumnSortDirection.DataBind();

			if (data.HasGettableProperty("Direction")) ddlSortColumnSortDirection.SelectedValue = ((SortDirection)data.GetPropertyValue("Direction")).ToInt().GetString();
		}

		protected void OnSortColumnsItemsReordered(object sender, ViewEventArgs<List<int>> e)
		{
			lstSortColumns.DataSource = e
				.Argument
				.Select(idx => new
				{
					ReportColumnName = lstSortColumns.Items[idx - 1].FindControl("litSortColumnName").ToLiteral().Text,
					Direction = lstSortColumns.Items[idx - 1].FindControl("ddlSortColumnSortDirection").ToDropDownList().SelectedValue.ToInt().ToEnum<SortDirection>(),
				});
			lstSortColumns.DataBind();

		}


		protected void OnAddSummaryColumnsClicked(object sender, EventArgs e)
		{
			var numericTypes = new List<SqlDbType> { SqlDbType.Decimal, SqlDbType.BigInt, SqlDbType.Float, SqlDbType.Int };

			var columns = lstSystemColumns.Items
							.Select(i => new
							{
								Name = (i.FindControl("hidSystemColumnName").ToCustomHiddenField()).Value,
								ReportColumnName = (i.FindControl("hidSystemColumnColumn").ToCustomHiddenField()).Value,
								DataType = (i.FindControl("hidSystemColumnDataType").ToCustomHiddenField()).Value.ToEnum<SqlDbType>(),
								Custom = false
							})
							.ToList();

			columns.AddRange(lstDataColumns.Items
							.Select(i => new
							{
								Name = (i.FindControl("txtDataColumnName").ToTextBox()).Text,
								ReportColumnName = (i.FindControl("txtDataColumnColumn").ToTextBox()).Text,
								DataType = (i.FindControl("ddlDataColumnDataType").ToDropDownList()).SelectedValue.ToInt().ToEnum<SqlDbType>(),
								Custom = (i.FindControl("hidDataColumnCustom")).ToCustomHiddenField().Value.ToBoolean()
							})
							.Where(i => i.Custom)
							.ToList());

			chkSummaryColumnSelectAll.Checked = false;
			lstEditSummaryColumns.DataSource = columns.OrderBy(c => c.Name).Where(c => numericTypes.Contains(c.DataType));
			lstEditSummaryColumns.DataBind();

			pnlEditSummaryColumns.Visible = true;
			pnlDimScreen.Visible = true;
		}

		protected void OnAddSelectedSummaryColumnsDoneClicked(object sender, EventArgs e)
		{
			var columns = lstSummaryColumns.Items
				.Select(i => new
				{
					Name = (i.FindControl("litSummaryColumnName").ToLiteral()).Text,
					ReportColumnName = (i.FindControl("hidSummaryColumnColumnName").ToCustomHiddenField()).Value,
					Selection = false
				})
				.ToList();

			var newColumns = lstEditSummaryColumns.Items
				.Select(i => new
					{
						Name = (i.FindControl("litSummaryColumnName").ToLiteral()).Text,
						ReportColumnName = (i.FindControl("hidSummaryColumnColumn").ToCustomHiddenField()).Value,
						Selection = (i.FindControl("chkSelection")).ToCheckBox().Checked
					})
				.ToList();

			columns.AddRange(newColumns.Where(c => c.Selection));

			lstSummaryColumns.DataSource = columns;
			lstSummaryColumns.DataBind();
			tabSummaryColumns.HeaderText = columns.BuildTabCount(SummaryColumnsHeader);

			pnlEditSummaryColumns.Visible = false;
			pnlDimScreen.Visible = false;
		}

		protected void OnCloseEditSummaryColumnsClicked(object sender, EventArgs e)
		{
			pnlEditSummaryColumns.Visible = false;
			pnlDimScreen.Visible = false;
		}

		protected void OnDeleteSummaryColumnClicked(object sender, ImageClickEventArgs e)
		{
			var imageButton = (ImageButton)sender;

			var columns = lstSummaryColumns.Items
				.Select(i => new
				{
					Name = (i.FindControl("litSummaryColumnName").ToLiteral()).Text,
					ReportColumnName = (i.FindControl("hidSummaryColumnColumnName").ToCustomHiddenField()).Value,
					Selection = false
				})
				.ToList();

			columns.RemoveAt((imageButton.Parent.FindControl("hidSummaryColumnIndex").ToCustomHiddenField()).Value.ToInt());

			lstSummaryColumns.DataSource = columns;
			lstSummaryColumns.DataBind();
			tabSummaryColumns.HeaderText = columns.BuildTabCount(SummaryColumnsHeader);
			athtuTabUpdaterMain.SetForUpdate(tabSummaryColumns.ClientID, columns.BuildTabCount(SummaryColumnsHeader));
		}

		protected void OnClearSummaryColumnsClicked(object sender, EventArgs e)
		{
			lstSummaryColumns.DataSource = new List<Summary>();
			lstSummaryColumns.DataBind();
			tabSummaryColumns.HeaderText = SummaryColumnsHeader;
			athtuTabUpdaterMain.SetForUpdate(tabSummaryColumns.ClientID, SummaryColumnsHeader);
		}

		protected void OnSummaryColumnsItemsReordered(object sender, ViewEventArgs<List<int>> e)
		{
			lstSummaryColumns.DataSource = e
				.Argument
				.Select(idx => new
				{
					Name = lstSummaryColumns.Items[idx - 1].FindControl("litSummaryColumnName").ToLiteral().Text,
					ReportColumnName = lstSummaryColumns.Items[idx - 1].FindControl("hidSummaryColumnColumnName").ToCustomHiddenField().Value,
				});
			lstSummaryColumns.DataBind();
		}


		private List<ParameterColumn> RetrieveParameters(bool preserveKeywordDate)
		{
			var parameters = lstParameterColumns.Items
				.Select(i => ((ReportConfigurationParameterControl)i.FindControl("reportConfigurationParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
			return parameters;
		}

		protected void OnAddParameterColumnsClicked(object sender, EventArgs e)
		{
			var columns = lstSystemColumns.Items
				.Select(i =>
					{
						var sqlDbType = (i.FindControl("hidSystemColumnDataType").ToCustomHiddenField()).Value.ToEnum<SqlDbType>();
						return new
							{
								Name = (i.FindControl("hidSystemColumnName").ToCustomHiddenField()).Value,
								DataType = sqlDbType,
								Custom = (i.FindControl("hidSystemColumnCustom").ToCustomHiddenField()).Value.ToBoolean(),
								Operator = sqlDbType.SearchDefaultParameter()
							};
					})
				.Distinct()
				.ToList();

			columns.AddRange(lstDataColumns.Items
				.Select(i =>
					{
						var sqlDbType = (i.FindControl("ddlDataColumnDataType").ToDropDownList()).SelectedValue.ToInt().ToEnum<SqlDbType>();
						return new
							{
								Name = (i.FindControl("txtDataColumnName").ToTextBox()).Text,
								DataType = sqlDbType,
								Custom = (i.FindControl("hidDataColumnCustom")).ToCustomHiddenField().Value.ToBoolean(),
								Operator = sqlDbType.SearchDefaultParameter()
							};
					})
				.Where(i => i.Custom)
				.ToList());

			chkParameterSelectAll.Checked = false;
			lstEditParameters.DataSource = columns.OrderBy(c => c.Name);
			lstEditParameters.DataBind();

			pnlEditParameterColumns.Visible = true;
			pnlDimScreen.Visible = true;
		}

		protected void OnAddSelectedParametersDoneClicked(object sender, EventArgs e)
		{
			var parameters = RetrieveParameters(true);

			var newParameters = lstEditParameters.Items
				.Select(i => new
				{
					Name = i.FindControl("litParameterName").ToLiteral().Text,
					DataType = i.FindControl("hidParameterDataType").ToCustomHiddenField().Value,
					Operator = i.FindControl("hidParameterOperator").ToCustomHiddenField().Value,
					Selection = (i.FindControl("chkSelection")).ToCheckBox().Checked
				})
				.Where(p => p.Selection)
				.Select(p => new ParameterColumn
				{
					ReportColumnName = p.Name,
					DefaultValue = string.Empty,
					ReadOnly = false,
					Operator = p.Operator.ToEnum<Operator>()
				})
				.ToList();

			parameters.AddRange(newParameters);

			lstParameterColumns.DataSource = parameters.OrderBy(c => c.ReportColumnName).ThenBy(c => c.Operator);
			lstParameterColumns.DataBind();
			tabParameters.HeaderText = parameters.BuildTabCount(ParametersHeader);

			pnlEditParameterColumns.Visible = false;
			pnlDimScreen.Visible = false;

		}

		protected void OnCloseEditParametersClicked(object sender, EventArgs e)
		{
			pnlEditParameterColumns.Visible = false;
			pnlDimScreen.Visible = false;
		}

		protected void OnDeleteParameterColumnClicked(object sender, ViewEventArgs<int> e)
		{
			var parameters = RetrieveParameters(true);

			parameters.RemoveAt(e.Argument);

			lstParameterColumns.DataSource = parameters.OrderBy(c => c.ReportColumnName).ThenBy(c => c.Operator);
			lstParameterColumns.DataBind();
			tabParameters.HeaderText = parameters.BuildTabCount(ParametersHeader);
			athtuTabUpdaterMain.SetForUpdate(tabParameters.ClientID, parameters.BuildTabCount(ParametersHeader));
		}

		protected void OnClearParametersClicked(object sender, EventArgs e)
		{
			lstParameterColumns.DataSource = new List<ParameterColumn>();
			lstParameterColumns.DataBind();
			tabParameters.HeaderText = ParametersHeader;
			athtuTabUpdaterMain.SetForUpdate(tabParameters.ClientID, ParametersHeader);
		}

		protected void OnParameterColumnsItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var parameter = (ParameterColumn)e.Item.DataItem;
			if (parameter == null) return;
			var columns = lstSystemColumns
				.Items
				.Select(i => new
					{
						Name = (i.FindControl("hidSystemColumnName").ToCustomHiddenField()).Value,
						DataType = (i.FindControl("hidSystemColumnDataType").ToCustomHiddenField()).Value.ToEnum<SqlDbType>(),
						Custom = (i.FindControl("hidSystemColumnCustom").ToCustomHiddenField()).Value.ToBoolean()
					})
				.Distinct()
				.ToList();
			columns.AddRange(lstDataColumns
								 .Items
								 .Select(i => new
									 {
										 Name = (i.FindControl("txtDataColumnName").ToTextBox()).Text,
										 DataType = (i.FindControl("ddlDataColumnDataType").ToDropDownList()).SelectedValue.ToInt().ToEnum<SqlDbType>(),
										 Custom = (i.FindControl("hidDataColumnCustom")).ToCustomHiddenField().Value.ToBoolean(),
									 })
								 .Where(i => i.Custom)
								 .ToList());
			var field = columns.FirstOrDefault(f => f.Name == parameter.ReportColumnName);
			if (field == null) return;

			var detailControl = (ReportConfigurationParameterControl)e.Item.FindControl("reportConfigurationParameterControl");
			detailControl.LoadParameter(parameter, field.DataType, true);
		}
        


        protected void OnAddPivotClicked(object sender, EventArgs e)
        {
            var pivots = GetCurrentViewPivots();

            pivots.Add(new Pivot
                {
                    TableName = string.Empty,
                    GuidId = Guid.NewGuid().GetString(),
                    PivotTableColumns = new List<PivotTableColumn>()
                });

            peLstPivots.LoadData(pivots, pivots.Count);
            tabPivots.HeaderText = pivots.BuildTabCount(PivotColumnsHeader);
            athtuTabUpdaterMain.SetForUpdate(tabPivots.ClientID, pivots.BuildTabCount(PivotColumnsHeader));

            var charts = GetCurrentViewCharts();
            peLstCharts.LoadData(charts, peLstCharts.CurrentPage);
        }

        protected void OnClearPivotsClicked(object sender, EventArgs e)
        {
            peLstPivots.LoadData(new List<Pivot>());
            tabPivots.HeaderText = PivotColumnsHeader;
            athtuTabUpdaterMain.SetForUpdate(tabPivots.ClientID, PivotColumnsHeader);
        }

        protected void OnAddSelectedPivotColumnsClicked(object sender, EventArgs e)
        {
            var button = (Button) sender;

            var lstPivotColumns = button.Parent.FindControl("lstPivotColumns").ToListView();
            var lstPivotColumnsToAdd = button.Parent.FindControl("lstPivotColumnsToAdd").ToListView();

            var columns = lstPivotColumns.Items
                .Select(i => new
                {
                    ReportColumnName = (i.FindControl("litColumnName").ToLiteral()).Text,
                    Selected = false,
                    ColumnType = i.FindControl("hidColumnType").ToCustomHiddenField().Value.ToInt().ToEnum<ColumnType>(),
                    MinAggregate = i.FindControl("chkTypeAggregateMin").ToAltUniformCheckBox().Checked,
                    MaxAggregate = i.FindControl("chkTypeAggregateMax").ToAltUniformCheckBox().Checked,
                    AvgAggregate = i.FindControl("chkTypeAggregateAverage").ToAltUniformCheckBox().Checked,
                    CntAggregate = i.FindControl("chkTypeAggregateCount").ToAltUniformCheckBox().Checked,
                    SumAggregate = i.FindControl("chkTypeAggregateSum").ToAltUniformCheckBox().Checked,
                    DataType = (i.FindControl("hidDataType").ToCustomHiddenField()).Value.ToInt(),
                })
                .ToList();

            var columnsAvailableToAdd = lstPivotColumnsToAdd
                .Items
                .Select(i => new
                    {
                        ReportColumnName = (i.FindControl("litColumnName").ToLiteral()).Text,
                        Selected = (i.FindControl("chkSelection")).ToAltUniformCheckBox().Checked,
                        ColumnType = ColumnType.Pivot,
                        MinAggregate = false,
                        MaxAggregate = false,
                        AvgAggregate = false,
                        CntAggregate = false,
                        SumAggregate = false,
                        DataType = (i.FindControl("hidDataType").ToCustomHiddenField()).Value.ToInt(),
                    })
                .ToList();

            columns.AddRange(columnsAvailableToAdd.Where(c => c.Selected));

            var chkSelectAllPivotColumns = button.FindControl("chkSelectAllPivotColumns").ToAltUniformCheckBox();
            chkSelectAllPivotColumns.Checked = false;

            lstPivotColumns.DataSource = columns;
            lstPivotColumns.DataBind();

            lstPivotColumnsToAdd.DataSource = columnsAvailableToAdd.Where(c => !c.Selected);
            lstPivotColumnsToAdd.DataBind();
        }

	    protected void OnClearPivotColumnsClicked(object sender, EventArgs e)
	    {
	        var button = (Button) sender;

	        var lstPivotColumns = button.Parent.FindControl("lstPivotColumns").ToListView();
	        var lstPivotColumnsToAdd = button.Parent.FindControl("lstPivotColumnsToAdd").ToListView();

	        var columns = lstDataColumns
	            .Items
	            .Select(i => new
	                {
	                    ReportColumnName = (i.FindControl("txtDataColumnName").ToTextBox()).Text,
	                    DataType = (i.FindControl("ddlDataColumnDataType").ToDropDownList())
	                             .SelectedValue
	                             .ToInt()
	                             .ToEnum<SqlDbType>(),
	                })
	            .ToList();

	        lstPivotColumns.DataSource = new List<PivotTableColumn>();
            lstPivotColumns.DataBind();

	        lstPivotColumnsToAdd.DataSource = columns;
	        lstPivotColumnsToAdd.DataBind();
	    }

	    protected void OnDeletePivotColumnClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var lstPivotColumns = imageButton.Parent.Parent.Parent.Parent.FindControl("lstPivotColumns").ToListView();
            var lstPivotColumnsToAdd = imageButton.Parent.Parent.Parent.Parent.FindControl("lstPivotColumnsToAdd").ToListView();

            var columns = lstPivotColumns.Items
                .Select(i => new PivotTableColumn
                {
                    ReportColumnName = (i.FindControl("litColumnName").ToLiteral()).Text,
                    DataType = (i.FindControl("hidDataType").ToCustomHiddenField()).Value.ToEnum<SqlDbType>(),
                    MinAggregate = i.FindControl("chkTypeAggregateMin").ToAltUniformCheckBox().Checked,
                    MaxAggregate = i.FindControl("chkTypeAggregateMax").ToAltUniformCheckBox().Checked,
                    AvgAggregate = i.FindControl("chkTypeAggregateAverage").ToAltUniformCheckBox().Checked,
                    CntAggregate = i.FindControl("chkTypeAggregateCount").ToAltUniformCheckBox().Checked,
                    SumAggregate = i.FindControl("chkTypeAggregateSum").ToAltUniformCheckBox().Checked,
                    ColumnType = i.FindControl("hidColumnType").ToCustomHiddenField().Value.ToInt().ToEnum<ColumnType>()
                })
                .ToList();
                
            columns.RemoveAt((imageButton.Parent.FindControl("hidColumnIndex").ToCustomHiddenField()).Value.ToInt());

            var dataColumns = lstDataColumns.Items
                                            .Select(i => new
                                                {
                                                    ReportColumnName = (i.FindControl("txtDataColumnName").ToTextBox()).Text,
                                                    DataType = (i.FindControl("ddlDataColumnDataType").ToDropDownList())
                                                             .SelectedValue
                                                             .ToInt()
                                                             .ToEnum<SqlDbType>(),
                                                })
                                            .Where(i => !columns.Select(n => n.ReportColumnName).Contains(i.ReportColumnName));

            lstPivotColumns.DataSource = columns;
            lstPivotColumns.DataBind();

            lstPivotColumnsToAdd.DataSource = dataColumns;
            lstPivotColumnsToAdd.DataBind();
        }

        protected void OnPivotColumnsItemsReordered(object sender, ViewEventArgs<List<int>> e)
        {
            var se = (SortableExtender) sender;
            var lstPivotColumns = se.Parent.FindControl("lstPivotColumns").ToListView();

            lstPivotColumns.DataSource = e
                .Argument
                .Select(idx => new PivotTableColumn
                    {
                        ReportColumnName = lstPivotColumns.Items[idx - 1].FindControl("litColumnName").ToLiteral().Text,
                        DataType = lstPivotColumns.Items[idx - 1].FindControl("hidDataType")
                                                                 .ToCustomHiddenField()
                                                                 .Value.ToEnum<SqlDbType>(),
                        MinAggregate = lstPivotColumns.Items[idx - 1].FindControl("chkTypeAggregateMin").ToAltUniformCheckBox().Checked,
                        MaxAggregate = lstPivotColumns.Items[idx - 1].FindControl("chkTypeAggregateMax").ToAltUniformCheckBox().Checked,
                        AvgAggregate = lstPivotColumns.Items[idx - 1].FindControl("chkTypeAggregateAverage").ToAltUniformCheckBox().Checked,
                        CntAggregate = lstPivotColumns.Items[idx - 1].FindControl("chkTypeAggregateCount").ToAltUniformCheckBox().Checked,
                        SumAggregate = lstPivotColumns.Items[idx - 1].FindControl("chkTypeAggregateSum").ToAltUniformCheckBox().Checked,
                        ColumnType = (lstPivotColumns.Items[idx - 1]).FindControl("hidColumnType").ToCustomHiddenField().Value.ToInt().ToEnum<ColumnType>()
                    });
            lstPivotColumns.DataBind();
        }

        protected void OnDeletePivotClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var pivots = GetCurrentViewPivots();
            var itemIdx = imageButton.FindControl("hidPivotIndex").ToCustomHiddenField().Value.ToInt();

            var charts = GetCurrentViewCharts();
            charts.RemoveAll(i => i.PivotGuidId == pivots[itemIdx].GuidId);

            pivots.RemoveAt(itemIdx);
            peLstPivots.LoadData(pivots);

            tabPivots.HeaderText = pivots.BuildTabCount(PivotColumnsHeader);
            athtuTabUpdaterMain.SetForUpdate(tabPivots.ClientID, pivots.BuildTabCount(PivotColumnsHeader));

            peLstCharts.LoadData(charts);

            tabChartChartConfiguration.HeaderText = charts.BuildTabCount(ChartsHeader);
            athtuTabUpdaterCharts.SetForUpdate(tabChartChartConfiguration.ClientID, charts.BuildTabCount(ChartsHeader));
        }

        protected void OnPivotsItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var item = (ListViewDataItem) e.Item;
            var pivot = (Pivot) item.DataItem;

            var lstPivotColumns = item.FindControl("lstPivotColumns").ToListView();
            var sePivotColumns = item.FindControl("sePivotColumns").ToSortableExtender();

            lstPivotColumns.DataSource = pivot.PivotTableColumns;
            lstPivotColumns.DataBind();

            var columns = lstDataColumns.Items
                                        .Select(i => new
                                        {
                                            ReportColumnName = (i.FindControl("txtDataColumnName").ToTextBox()).Text,
                                            DataType = (i.FindControl("ddlDataColumnDataType").ToDropDownList())
                                                                                             .SelectedValue
                                                                                             .ToInt()
                                                                                             .ToEnum<SqlDbType>(),
                                        })
                                        .Where(i => !pivot.PivotTableColumns.Select(p => p.ReportColumnName)
                                                                            .ToList()
                                                                            .Contains(i.ReportColumnName))
                                        .ToList();

            var lstPivotColumnsToAdd = item.FindControl("lstPivotColumnsToAdd").ToListView();

            lstPivotColumnsToAdd.DataSource = columns;
            lstPivotColumnsToAdd.DataBind();

            sePivotColumns.Enabled = hidEditStatus.Value.ToBoolean();
        }

	    protected void LoadCurrentViewPivots(object sender, EventArgs e)
	    {
            var pivots = GetCurrentViewPivots();
            peLstPivots.LoadData(pivots, peLstPivots.CurrentPage);
	    }

        protected void OnAggregateCheckboxChanged(object sender, EventArgs e)
        {
            var chkBox = (AltUniformCheckBox) sender;
            var hidColumnType = chkBox.Parent.FindControl("hidColumnType").ToCustomHiddenField();

            if (chkBox.Checked)
            {
                hidColumnType.Value = ColumnType.Aggregate.ToInt().ToString();
            }
        }

        protected void OnPivotCheckboxChanged(object sender, EventArgs e)
        {
            var chkBox = (AltUniformCheckBox)sender;
            var hidColumnType = chkBox.Parent.FindControl("hidColumnType").ToCustomHiddenField();

            if (chkBox.Checked)
            {
                hidColumnType.Value = ColumnType.Pivot.ToInt().ToString();
            }
        }


        protected void OnAddChartClicked(object sender, EventArgs e)
        {
            var charts = GetCurrentViewCharts();

            charts.Add(new ChartConfiguration
            {
                Title = string.Empty,
                XAxisTitle = string.Empty,
                YAxisTitle = string.Empty
            });

            peLstCharts.LoadData(charts, charts.Count);
            tabChartChartConfiguration.HeaderText = charts.BuildTabCount(ChartsHeader);
            athtuTabUpdaterMain.SetForUpdate(tabChartChartConfiguration.ClientID, charts.BuildTabCount(ChartsHeader));
        }

        protected void OnClearChartsClicked(object sender, EventArgs e)
        {
            peLstCharts.LoadData(new List<ChartConfiguration>());
            tabChartChartConfiguration.HeaderText = ChartsHeader;
            athtuTabUpdaterMain.SetForUpdate(tabChartChartConfiguration.ClientID, ChartsHeader);
        }

        protected void OnChartsPageChanging(object sender, EventArgs e)
        {
            var charts = GetCurrentViewCharts();
            peLstCharts.LoadData(charts, peLstCharts.CurrentPage);
        }

        protected void OnChartsItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var data = (ChartConfiguration) e.Item.DataItem;

            var ddlDataSource = e.Item.FindControl("ddlDataSource").ToDropDownList();
            ddlDataSource.DataSource = ChartDataSourceTypes;

            ddlDataSource.DataBind();
            ddlDataSource.SelectedValue = data.DataSourceType.ToInt().ToString();

            var ddlXAxisDataColumn = e.Item.FindControl("ddlXAxisDataColumn").ToDropDownList();
            
            var xAxisDataList = lstDataColumns.Items.Select(s => new
                {
                    Key = s.FindControl("txtDataColumnName").ToTextBox().Text, 
                    Value = s.FindControl("txtDataColumnName").ToTextBox().Text
                }).ToList();

            xAxisDataList.Insert(0, new {Key = WebApplicationConstants.ChooseOne, Value = WebApplicationConstants.ChooseOne});
            ddlXAxisDataColumn.DataSource = xAxisDataList;
            ddlXAxisDataColumn.DataBind();
            ddlXAxisDataColumn.SelectedValue = data.XAxisDataColumn;

            var pnlPivotOptions = e.Item.FindControl("pnlPivotOptions").ToPanel();
            var pnlXSeriesOptions = ddlDataSource.Parent.FindControl("pnlXSeriesOptions").ToPanel();

            pnlPivotOptions.Visible = ddlDataSource.SelectedValue.ToEnum<ChartDataSourceType>() == ChartDataSourceType.Pivot;
            pnlXSeriesOptions.Visible = ddlDataSource.SelectedValue.ToEnum<ChartDataSourceType>() != ChartDataSourceType.Pivot;

            var pivots = GetCurrentViewPivots();
            var ddlPivotTable = e.Item.FindControl("ddlPivotTable").ToDropDownList();
            ddlPivotTable.DataSource = pivots.Select(i => new
                {
                    Value = i.GuidId,
                    Text = i.TableName
                });

            ddlPivotTable.DataBind();
            ddlPivotTable.SelectedValue = data.PivotGuidId;

            var lstYAxisColumns = e.Item.FindControl("lstYAxisColumns").ToListView();
            lstYAxisColumns.DataSource = data.YAxisDataConfigurations;
            lstYAxisColumns.DataBind();
        }

        protected void OnDataSourceSelectedIndexChanged(object sender, EventArgs e)
        {
            var ddlDataSource = (DropDownList) sender;

            var pnlPivotOptions = ddlDataSource.Parent.FindControl("pnlPivotOptions").ToPanel();
            var pnlXSeriesOptions = ddlDataSource.Parent.FindControl("pnlXSeriesOptions").ToPanel();

            var hidPivotGuidId = ddlDataSource.Parent.FindControl("hidPivotGuidId").ToCustomHiddenField();
            var hidPivotTableName = ddlDataSource.Parent.FindControl("hidPivotTableName").ToCustomHiddenField();

            var lstYAxisColumns = ddlDataSource.Parent.FindControl("lstYAxisColumns").ToListView();

            lstYAxisColumns.DataSource = new List<YAxisConfiguration>();
            lstYAxisColumns.DataBind();


            if (ddlDataSource.SelectedValue.ToEnum<ChartDataSourceType>() == ChartDataSourceType.Original)
            {
                hidPivotGuidId.Value = string.Empty;
                hidPivotTableName.Value = string.Empty;
            }
            else
            {
                var ddlXAxisDataColumn = ddlDataSource.FindControl("ddlXAxisDataColumn").ToDropDownList();
                ddlXAxisDataColumn.SelectedValue = WebApplicationConstants.ChooseOne;

                var pivots = GetCurrentViewPivots();
                if (pivots.Any())
                {
                    hidPivotGuidId.Value = pivots.First().GuidId;
                    hidPivotTableName.Value = pivots.First().TableName;
                }
                else
                {
                    hidPivotGuidId.Value = string.Empty;
                    hidPivotTableName.Value = string.Empty;
                }
            }

            pnlPivotOptions.Visible = ddlDataSource.SelectedValue.ToEnum<ChartDataSourceType>() == ChartDataSourceType.Pivot;
            pnlXSeriesOptions.Visible = ddlDataSource.SelectedValue.ToEnum<ChartDataSourceType>() != ChartDataSourceType.Pivot;
        }

	    protected void OnPivotTableSelectedIndexChanged(object sender, EventArgs e)
        {
            var ddlPivotTable = (DropDownList)sender;
            var lstYAxisColumns = ddlPivotTable.Parent.FindControl("lstYAxisColumns").ToListView();
            var hidPivotGuidId = ddlPivotTable.Parent.FindControl("hidPivotGuidId").ToCustomHiddenField();
            var hidPivotTableName = ddlPivotTable.Parent.FindControl("hidPivotTableName").ToCustomHiddenField();

            hidPivotGuidId.Value = ddlPivotTable.SelectedValue;
            hidPivotTableName.Value = ddlPivotTable.SelectedItem.Text;

            lstYAxisColumns.DataSource = new List<YAxisConfiguration>();
            lstYAxisColumns.DataBind();
        }

        protected void OnDeleteChartClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var charts = GetCurrentViewCharts();
            var itemIdx = imageButton.FindControl("hidChartIndex").ToCustomHiddenField().Value.ToInt();
            charts.RemoveAt(itemIdx);
            peLstCharts.LoadData(charts);

            tabChartChartConfiguration.HeaderText = charts.BuildTabCount(ChartsHeader);
            athtuTabUpdaterMain.SetForUpdate(tabChartChartConfiguration.ClientID, charts.BuildTabCount(ChartsHeader));
        }

		protected void OnAddYAxisColumnsClicked(object sender, EventArgs e)
		{
		    var btnAddYAxisColumn = (Button) sender;

		    hidEditingIndex.Value = btnAddYAxisColumn.Parent.FindControl("hidChartIndex").ToCustomHiddenField().Value;

		    var ddlDataSource = btnAddYAxisColumn.Parent.FindControl("ddlDataSource").ToDropDownList();
            var ddlPivotTable = btnAddYAxisColumn.Parent.FindControl("ddlPivotTable").ToDropDownList();
		    var pivot = GetCurrentViewPivots().FirstOrDefault(i => i.GuidId == ddlPivotTable.SelectedValue);

		    var dataSourceIsPivot = ddlDataSource.SelectedValue.ToEnum<ChartDataSourceType>() == ChartDataSourceType.Pivot;

		    var columns = dataSourceIsPivot && pivot == null
		                      ? null
		                      : dataSourceIsPivot
		                            ? pivot.PivotTableColumns.SelectMany(col => col.AggregateTypes().Select(a => new
		                                {
		                                    Name = string.Format("{0}: {1}", a.FormattedString(), col.ReportColumnName),
		                                    Custom = false
		                                })).ToList()

		                            : lstDataColumns.Items
		                                            .Select(i => new
		                                                {
		                                                    Name = (i.FindControl("txtDataColumnName").ToTextBox()).Text,
		                                                    Custom =
		                                                             (i.FindControl("hidDataColumnCustom"))
		                                                             .ToCustomHiddenField()
		                                                             .Value.ToBoolean(),
		                                                })
		                                            .ToList();


		    chkYAxisColumnSelectAll.Checked = false;

		    if (columns != null)
		    {
		        lstEditYAxisColumns.DataSource = columns.OrderBy(c => c.Name);
		        lstEditYAxisColumns.DataBind();
		    }
		    else
		    {
		        lstEditYAxisColumns.DataSource = new List<object>();
                lstEditYAxisColumns.DataBind();
		    }

		    pnlEditYAxisColumns.Visible = true;
			pnlDimScreen.Visible = true;
		}

		protected void OnAddSelectedYAxisColumnsDoneClicked(object sender, EventArgs e)
		{
		    var charts = GetCurrentViewCharts();
		    var editingIndex = hidEditingIndex.Value.ToInt();

            var columns = lstEditYAxisColumns.Items
				.Select(i => new
				{
					ReportColumnName = (i.FindControl("litYAxisColumnName").ToLiteral()).Text,
					Type = ReportChartType.Bar,
					Selection = (i.FindControl("chkSelection")).ToCheckBox().Checked
				})
				.Where(c => c.Selection)
				.ToList();

		    charts[editingIndex].YAxisDataConfigurations.AddRange(columns.Select(i => new YAxisConfiguration()
		        {
		            ReportColumnName = i.ReportColumnName,
		            Type = i.Type,
		        }));

		    peLstCharts.LoadData(charts, peLstCharts.CurrentPage);

		    hidEditingIndex.Value = string.Empty;
			pnlEditYAxisColumns.Visible = false;
			pnlDimScreen.Visible = false;
		}

		protected void OnCloseEditYAxisColumnsClicked(object sender, EventArgs e)
		{
            hidEditingIndex.Value = string.Empty;
			pnlEditYAxisColumns.Visible = false;
			pnlDimScreen.Visible = false;
		}

		protected void OnDeleteYAxisColumnClicked(object sender, ImageClickEventArgs e)
		{
            var imageButton = (ImageButton)sender;
            var lstYAxisColumns = imageButton.Parent.Parent.Parent.Parent.FindControl("lstYAxisColumns").ToListView();

            var columns = lstYAxisColumns.Items
                .Select(i => new 
                {
                    ReportColumnName = (i.FindControl("litYAxisColumnName").ToLiteral()).Text,
                    Type = (i.FindControl("ddlYAxisColumnChartType").ToDropDownList()).SelectedValue.ToInt().ToEnum<ReportChartType>(),
                })
                .ToList();

            columns.RemoveAt((imageButton.Parent.FindControl("hidYAxisColumnIndex").ToCustomHiddenField()).Value.ToInt());

            lstYAxisColumns.DataSource = columns;
            lstYAxisColumns.DataBind();
		}

		protected void OnClearAllYAxisColumnsClicked(object sender, EventArgs e)
		{
            var button = (Button)sender;
            var lstYAxisColumns = button.Parent.FindControl("lstYAxisColumns").ToListView();

            lstYAxisColumns.DataSource = new List<YAxisConfiguration>();
            lstYAxisColumns.DataBind();
		}

		protected void OnYAxisColumnsItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var data = e.Item.DataItem;

			var ddlYAxisColumnChartType = e.Item.FindControl("ddlYAxisColumnChartType").ToDropDownList();
			ddlYAxisColumnChartType.DataSource = ReportChartTypes;
			ddlYAxisColumnChartType.DataBind();

			if (data.HasGettableProperty("Type")) ddlYAxisColumnChartType.SelectedValue = ((ReportChartType)data.GetPropertyValue("Type")).ToInt().GetString();
		}


		protected void OnLstRequiredParametersItemDataBound(object sender, ListViewItemEventArgs e)
		{
			var item = e.Item as ListViewDataItem;
			if (item == null) return;

			var control = (ReportConfigurationParameterControl)item.FindControl("reportConfigurationRequiredParameterControl");

			control.LoadParameter(item.DataItem as RequiredValueParameter, true);
		}

		protected void OnFindReportTemplateClicked(object sender, ImageClickEventArgs e)
		{

			if (hidReportTemplateId.Value.ToLong() != default(long))
			{
				messageBox.Icon = MessageIcon.Warning;
				messageBox.Message = "If you select a new report template all previous selections (except users) will be cleared. Do you wish to proceed?";
				messageBox.Visible = true;
				messageBox.Button = MessageButton.YesNo;
				hidFlag.Value = ClearOrChangeConfigurationOnTemplateFlag;
			}
			else
			{
				pnlReportTemplates.Visible = true;
				pnlDimScreen.Visible = true;
			}
		}

		protected void OnClearTemplateClicked(object sender, ImageClickEventArgs e)
		{
			messageBox.Icon = MessageIcon.Question;
			messageBox.Message = "If you clear this configuration the template will be removed, and any columns, sortings, summaries, parameters, required parameters, pivots, or charts currently added will be lost. Do you wish to proceed?";
			messageBox.Visible = true;
			messageBox.Button = MessageButton.YesNo;
			hidFlag.Value = ClearConfigurationFlag;
		}

		protected void OnFilterResultsClick(object sender, EventArgs e)
		{
			var filter = txtFilter.Text.Trim().ToLower();
			var templates = new ReportTemplateSearch().FetchReportTemplates(new List<ParameterColumn>());
			var configurations = string.IsNullOrEmpty(filter) ? templates : templates.Where(rc => rc.Name.ToLower().Contains(filter) || rc.Description.ToLower().Contains(filter)).ToList();

			rptReportTemplates.DataSource = configurations.OrderBy(r => r.Name);
			rptReportTemplates.DataBind();

			pnlNoReports.Visible = !configurations.Any();
		}

		protected void OnNoReportsClicked(object sender, EventArgs e)
		{
			pnlReportTemplates.Visible = false;
			pnlDimScreen.Visible = false;
		}

		protected void OnSelectReportTemplateClicked(object sender, EventArgs e)
		{
			var button = (LinkButton)sender;
			var reportTemplate = new ReportTemplate(button.Parent.FindControl("hidReportConfigurationId").ToCustomHiddenField().Value.ToInt());

			if (reportTemplate.Id != hidReportTemplateId.Value.ToLong())
				ProcessConfigurationTemplateSelection(reportTemplate);

			pnlReportTemplates.Visible = false;
			pnlDimScreen.Visible = false;
		}

		protected void OnMessageBoxYesClicked(object sender, EventArgs e)
		{
			switch (hidFlag.Value)
			{
				case ClearOrChangeConfigurationOnTemplateFlag:
					hidFlag.Value = string.Empty;
					messageBox.Visible = false;
					pnlReportTemplates.Visible = true;
					pnlDimScreen.Visible = true;
					break;
				case ClearConfigurationFlag:
					hidFlag.Value = string.Empty;
					messageBox.Visible = false;
					ProcessConfigurationTemplateSelection(null);
					break;
			}

		}

		protected void OnMessageBoxNoClicked(object sender, EventArgs e)
		{
			messageBox.Visible = false;
			pnlReportTemplates.Visible = false;
			pnlDimScreen.Visible = false;
		}


	}
}