﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence
{
    public partial class VendorLaneRateHistoryView : MemberPageBase, IVendorLaneRateHistoryView
    {
        public static string PageAddress { get { return "~/Members/BusinessIntelligence/VendorLaneRateHistoryView.aspx"; } }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.ReportsBlue; }
        }

        public override ViewCode PageCode { get { return ViewCode.VendorLaneRateHistory; } }

        public event EventHandler<ViewEventArgs<LaneHistorySearchCriteria>> Search;

        public void DisplaySearchResult(List<LaneHistoryRateDto> laneHistoryRates)
        {
            var ltlRates = laneHistoryRates.Where(r => r.ServiceMode == ServiceMode.LessThanTruckload).ToList();
            var ftlRates = laneHistoryRates.Where(r => r.ServiceMode == ServiceMode.Truckload).ToList();

            tabVendorLaneRateHistory.Visible = ltlRates.Count > 0 || ftlRates.Count > 0;

            lstLtlRates.DataSource = ltlRates;
            lstLtlRates.DataBind();

            lstFTlRates.DataSource = ftlRates;
            lstFTlRates.DataBind();
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new VendorLaneRateHistoryHandler(this).Initialize();

            if (IsPostBack) return;

            var countryId = ActiveUser.Tenant.DefaultCountryId.ToString();
			ddlDestinationCountry.SelectedValue = countryId.GetString();
			ddlOriginCountry.SelectedValue = countryId.GetString();
        }

        protected void OnSearchClicked(object sender, EventArgs e)
        {
            var criteria = new LaneHistorySearchCriteria
            {
                DestinationCountryId = ddlDestinationCountry.SelectedValue.ToLong(),
                DestinationPostalCode = txtDestinationPostalCode.Text,
                OriginCountryId = ddlOriginCountry.SelectedValue.ToLong(),
                OriginPostalCode = txtOriginPostalCode.Text,
                DesiredPickupDate = txtStartDate.Text.ToDateTime() <= DateUtility.SystemEarliestDateTime.AddYears(1) 
                    ? DateUtility.SystemEarliestDateTime.AddYears(1) 
                    : txtStartDate.Text.ToDateTime(),
                TotalWeight = txtTotalWeight.Text.ToDecimal(),
                Radius = txtMileageRadius.Text == string.Empty 
                    ? default(int) 
                    : txtMileageRadius.Text.ToDouble()
            };

            if (Search != null)
                Search(this, new ViewEventArgs<LaneHistorySearchCriteria>(criteria));
        }

        protected void OnLtlRatesDataBound(object sender, ListViewItemEventArgs e)
        {
            var item = (ListViewDataItem)e.Item;
            var rate = (LaneHistoryRateDto)item.DataItem;

            var lstCharges = item.FindControl("lstLtlCharges").ToListView();

            lstCharges.DataSource = rate.Charges.Values;
            lstCharges.DataBind();
        }

        protected void OnFtlRatesDataBound(object sender, ListViewItemEventArgs e)
        {
            var item = (ListViewDataItem)e.Item;
            var rate = (LaneHistoryRateDto)item.DataItem;

            var lstCharges = item.FindControl("lstFtlCharges").ToListView();

            lstCharges.DataSource = rate.Charges.Values;
            lstCharges.DataBind();
        }

        protected void OnVendorInfoCancel(object sender, EventArgs e)
        {
            vendorInfoControl.Visible = false;
        }

        protected void OnVendorNameClick(object sender, EventArgs e)
        {
            var button = (LinkButton)sender;
            var vendorId = button.Parent.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong();
            vendorInfoControl.DisplayVendorInformation(vendorId, txtOriginPostalCode.Text,
                                                       ddlOriginCountry.SelectedValue.ToLong(),
                                                       txtDestinationPostalCode.Text,
                                                       ddlDestinationCountry.SelectedValue.ToLong(),
                                                       txtStartDate.Text.ToDateTime(), txtMileageRadius.Text.ToDouble());
            vendorInfoControl.Visible = true;
        }
    }
}
