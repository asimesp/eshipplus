﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using LogisticsPlus.Eship.WebApplication.Members.Administration;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence
{
    public partial class ReportTemplateView : AdminMemberPageBase, IReportTemplateView
    {
        private const string QueryUpdateFlag = "Q";
        private const string ConfigurationUpdateFlag = "C";

        public static string PageAddress
        {
            get { return "~/Members/BusinessIntelligence/ReportTemplateView.aspx"; }
        }

        public override ViewCode PageCode { get { return ViewCode.ReportTemplate; } }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.ReportsBlue; }
        }

        public event EventHandler<ViewEventArgs<ReportTemplate>> Save;
        public event EventHandler<ViewEventArgs<ReportTemplate>> Delete;

        public void Set(ReportTemplate reportTemplate)
        {
            LoadTemplate(reportTemplate);
            SetEditStatus(!reportTemplate.IsNew);
        }

        private void SetEditStatus(bool enabled)
        {
            pnlDetails.Enabled = enabled;
            pnlQuery.Enabled = enabled;
            pnlDefaultConfiguration.Enabled = enabled;
            memberToolBar.EnableSave = enabled;
            litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            if (!messages.HasErrors() && !messages.HasWarnings()) SetEditStatus(false); //return to read only


            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void LoadTemplate(ReportTemplate reportTemplate)
        {
            hidReportTemplateId.Value = reportTemplate.Id.ToString();
            txtName.Text = reportTemplate.Name;
            txtDescription.Text = reportTemplate.Description;
            litQuery.Text = reportTemplate.Query.StripTags().ReplaceNewLineWithHtmlBreak().ReplaceTab();
            litDefaultConfiguration.Text = reportTemplate.DefaultCustomization.StripTags().ReplaceNewLineWithHtmlBreak().ReplaceTab();

            chkCanFilterByCustomerGroup.Checked = reportTemplate.CanFilterByCustomerGroup;
            chkCanFilterByVendorGroup.Checked = reportTemplate.CanFilterByVendorGroup;
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new ReportTemplateHandler(this);
            handler.Initialize();

            memberToolBar.ShowSave = Access.Modify;
            memberToolBar.ShowDelete = Access.Remove;
            memberToolBar.ShowNew = Access.Modify;
            memberToolBar.ShowEdit = Access.Modify;

            reportTemplateFinder.OpenForEditEnabled = Access.Modify;

            if (IsPostBack) return;

            SetEditStatus(false);
        }


        protected void OnToolbarNewClicked(object sender, EventArgs e)
        {
            Set(new ReportTemplate { Query = string.Empty, DefaultCustomization = string.Empty });
            reportTemplateFinder.Reset();
            SetEditStatus(true);
        }

        protected void OnToolBarSaveClicked(object sender, EventArgs e)
        {
            var reportTemplateId = hidReportTemplateId.Value.ToLong();

            var template = new ReportTemplate(reportTemplateId, reportTemplateId != default(long))
            {
                Name = txtName.Text,
                Description = txtDescription.Text,
                CanFilterByCustomerGroup = chkCanFilterByCustomerGroup.Checked,
                CanFilterByVendorGroup = chkCanFilterByVendorGroup.Checked,
                Query = litQuery.Text.ReplaceTags().ReplaceHtmlBreakWithNewLine().ReplaceTab(),
                DefaultCustomization = litDefaultConfiguration.Text.ReplaceTags().ReplaceHtmlBreakWithNewLine().ReplaceTab(),
            };

            if (Save != null)
                Save(this, new ViewEventArgs<ReportTemplate>(template));
        }

        protected void OnToolBarDeleteClicked(object sender, EventArgs e)
        {
            if (Delete != null)
                Delete(this, new ViewEventArgs<ReportTemplate>(new ReportTemplate(hidReportTemplateId.Value.ToLong())));

            SetEditStatus(false);

            reportTemplateFinder.Reset();
        }

        protected void OnToolbarFindClicked(object sender, EventArgs e)
        {
            reportTemplateFinder.Visible = true;
        }

        protected void OnToolBarEditClicked(object sender, EventArgs e)
        {
            SetEditStatus(true);
        }


        protected void OnReportTemplateFinderItemSelected(object sender, ViewEventArgs<ReportTemplate> e)
        {
            LoadTemplate(e.Argument);

            reportTemplateFinder.Visible = false;

            SetEditStatus(reportTemplateFinder.OpenForEdit);
        }

        protected void OnReportTemplateFinderSelectionCancelled(object sender, EventArgs e)
        {
            reportTemplateFinder.Visible = false;
        }



        protected void OnUpdateQueryClicked(object sender, EventArgs e)
        {
            hidFlag.Value = QueryUpdateFlag;
            fileUploader.Title = "Upload Query";
            fileUploader.Instructions = "Query file is SQL parameterized query file.";
            fileUploader.Visible = true;
        }

        protected void OnUpdateDefaultConfigurationClicked(object sender, EventArgs e)
        {
            hidFlag.Value = ConfigurationUpdateFlag;
            fileUploader.Title = "Upload Default Configuration";
            fileUploader.Instructions = "Default configuration should be an xml file.";
            fileUploader.Visible = true;
        }

        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            using (e.Reader)
                switch (hidFlag.Value)
                {
                    case QueryUpdateFlag:
                        litQuery.Text = e.Reader.ReadToEnd().StripTags().ReplaceNewLineWithHtmlBreak().StripTab();
                        break;
                    case ConfigurationUpdateFlag:
                        litDefaultConfiguration.Text = e.Reader.ReadToEnd().StripTags().ReplaceNewLineWithHtmlBreak().StripTab();
                        break;
                }
            fileUploader.Visible = false;
        }
    }
}
