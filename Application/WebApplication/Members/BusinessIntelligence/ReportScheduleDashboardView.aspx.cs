﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.Utilities.Que;
using LogisticsPlus.Eship.WebApplication.ViewCodes;


namespace LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence
{
    public partial class ReportScheduleDashboardView : MemberPageBase, IReportScheduleDashboardView
    {
        private SearchField SortByField
        {
            get { return BusinessIntelligenceSearchFields.ReportScheduleSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue); }
        }

        public static string PageAddress { get { return "~/Members/BusinessIntelligence/ReportScheduleDashboardView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.ReportScheduleDashboard; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.ReportsBlue; } }
        

        public event EventHandler<ViewEventArgs<ReportScheduleViewSearchCriteria>> Search;

        public void DisplaySearchResult(List<ReportSchedule> schedules)
        {
            rptReportSchedules.DataSource = schedules;
            rptReportSchedules.DataBind();
            litRecordCount.Text = schedules.BuildRecordCount();
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }


        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems
            {
                Columns = GetCurrentRunParameters(true),
                SortAscending = rbAsc.Checked,
                SortBy = SortByField
            };
        }

        private void DoSearchPreProcessingThenSearch(SearchField field)
        {
            DoSearch(new ReportScheduleViewSearchCriteria
            {
                Parameters = GetCurrentRunParameters(false),
                ActiveUserId = ActiveUser.Id,
                SortAscending = rbAsc.Checked,
                SortBy = field
            });
        }

        private void DoSearch(ReportScheduleViewSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<ReportScheduleViewSearchCriteria>(criteria));
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new ReportScheduleDashboardHandler(this).Initialize();

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            // set sort fields
            ddlSortBy.DataSource = BusinessIntelligenceSearchFields.ReportScheduleSortFields.OrderBy(s => s.DisplayName);
            ddlSortBy.DataBind();
            ddlSortBy.SelectedValue = BusinessIntelligenceSearchFields.ReportScheduleConfigurationName.Name;

            lbtnSortConfigurationName.CommandName = BusinessIntelligenceSearchFields.ReportScheduleConfigurationName.Name;
            lbtnSortStartDate.CommandName = BusinessIntelligenceSearchFields.ReportScheduleStartDate.Name;
            lbtnSortEndDate.CommandName = BusinessIntelligenceSearchFields.ReportScheduleEndDate.Name;
            lbtnSortLastRun.CommandName = BusinessIntelligenceSearchFields.ReportScheduleLastRun.Name;
            lbtnSortTime.CommandName = BusinessIntelligenceSearchFields.ReportScheduleTime.Name;
            
            // get search criteria
            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);
            var criteria = new ReportScheduleViewSearchCriteria
                {
                    ActiveUserId = ActiveUser.Id,
                    Parameters = profile != null
                                     ? profile.Columns
                                     : BusinessIntelligenceSearchFields.DefaultReportConfigurations.Select(
                                         f => f.ToParameterColumn()).ToList(),
                    SortAscending = profile == null || profile.SortAscending,
                    SortBy = profile == null ? null : profile.SortBy
                };

            lstFilterParameters.DataSource = criteria.Parameters;
            lstFilterParameters.DataBind();

            rbAsc.Checked = criteria.SortAscending;
            rbDesc.Checked = !criteria.SortAscending;

            if (criteria.SortBy != null && ddlSortBy.ContainsValue(criteria.SortBy.Name))
                ddlSortBy.SelectedValue = criteria.SortBy.Name;
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }


        protected void OnAddToScheduleQueueClicked(object sender, EventArgs e)
        {
            chkSelectAllRecords.Checked = false;
            var selectedSchedules = rptReportSchedules
                .Items
                .Cast<RepeaterItem>()
                .Where(i => i.FindControl("chkSelected").ToAltUniformCheckBox().Checked)
                .Select(i =>
                    {
                        i.FindControl("chkSelected").ToAltUniformCheckBox().Checked = false;
                        return i.FindControl("hidScheduleId").ToCustomHiddenField().Value.ToLong();
                    })
                .Select(rs => new ReportSchedule(rs))
                .ToList();

            if (selectedSchedules.Any())
            {
                ReportQueue.Add(selectedSchedules);
                DisplayMessages(new[] { ValidationMessage.Information("Reports successfully queued.") });
            }
            else
            {
                DisplayMessages(new[] { ValidationMessage.Information("No Reports Selected.") });
            }
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = BusinessIntelligenceSearchFields.ReportSchedules.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();
        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(BusinessIntelligenceSearchFields.ReportSchedules);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSortData(object sender, CommandEventArgs e)
        {
            var field = BusinessIntelligenceSearchFields.ReportScheduleSortFields.FirstOrDefault(c => c.Name == e.CommandName);
            if (field == null) return;
            if (ddlSortBy.ContainsValue(field.Name))
            {
                if (ddlSortBy.SelectedValue == field.Name && rbAsc.Checked)
                {
                    rbAsc.Checked = false;
                    rbDesc.Checked = true;
                }
                else if (ddlSortBy.SelectedValue == field.Name && rbDesc.Checked)
                {
                    rbAsc.Checked = true;
                    rbDesc.Checked = false;
                }

                ddlSortBy.SelectedValue = field.Name;
            }

            DoSearchPreProcessingThenSearch(field);
        }

        protected void OnSortBySelectedIndexChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnSortOrderCheckChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();

            rbAsc.Checked = e.Argument.SortAscending;
            rbDesc.Checked = !e.Argument.SortAscending;

            if (e.Argument.SortBy != null && ddlSortBy.ContainsValue(e.Argument.SortBy.Name))
                ddlSortBy.SelectedValue = e.Argument.SortBy.Name;
        }
    }
}