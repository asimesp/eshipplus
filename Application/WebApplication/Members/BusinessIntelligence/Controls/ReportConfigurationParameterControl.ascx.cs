﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls
{
    public partial class ReportConfigurationParameterControl : MemberControlBase
    {
        public int ItemIndex
        {
            get { return hidItemIndex.Value.ToInt(); }
            set { hidItemIndex.Value = value.ToString(); }
        }

        public bool IsRequiredParameter
        {
            get { return hidIsRequiredParameter.Value.ToBoolean(); }
        }

        public event EventHandler<ViewEventArgs<int>> RemoveParameter;


        private void ConfigureDisplay(SqlDbType dataType, string value1, string value2, bool preserveKeywordDate)
        {
            ResetParameterColumnFields();
            switch (dataType)
            {
                case SqlDbType.NVarChar:
                    pnlText.Visible = true;
                    pnlNumeric.Visible = false;
                    pnlDateTime.Visible = false;
                    pnlBoolean.Visible = false;
                    txtParameterText.Text = value1;
                    pnlText2.Visible = true;
                    pnlNumeric2.Visible = false;
                    pnlDateTime2.Visible = false;
                    pnlBoolean2.Visible = false;
                    txtParameterText2.Text = value2;
                    break;
                case SqlDbType.BigInt:
                case SqlDbType.Decimal:
                case SqlDbType.Float:
                case SqlDbType.Int:
                    pnlText.Visible = false;
                    pnlNumeric.Visible = true;
                    pnlDateTime.Visible = false;
                    pnlBoolean.Visible = false;
                    txtParameterNumeric.Text = value1;

                    pnlText2.Visible = false;
                    pnlNumeric2.Visible = true;
                    pnlDateTime2.Visible = false;
                    pnlBoolean2.Visible = false;
                    txtParameterNumeric2.Text = value2;
                    break;
                case SqlDbType.DateTime:
                    ddlParameterTime.DataSource = TimeUtility.BuildTimeList();
                    ddlParameterTime.DataBind();
                    pnlText.Visible = false;
                    pnlNumeric.Visible = false;
                    pnlDateTime.Visible = true;
                    pnlBoolean.Visible = false;
                    SetDateTimeValues(string.IsNullOrEmpty(value1)
                                        ? DateTime.Now.TimeToMinimum().ToString()
                                        : value1, txtParameterDateTime, ddlParameterTime, preserveKeywordDate);

                    ddlParameterTime2.DataSource = TimeUtility.BuildTimeList();
                    ddlParameterTime2.DataBind();
                    pnlText2.Visible = false;
                    pnlNumeric2.Visible = false;
                    pnlDateTime2.Visible = true;
                    pnlBoolean2.Visible = false;
                    SetDateTimeValues(string.IsNullOrEmpty(value2)
                                        ? DateTime.Now.AddDays(1).TimeToMinimum().ToString()
                                        : value2, txtParameterDateTime2, ddlParameterTime2, preserveKeywordDate);
                    break;
                case SqlDbType.Bit:
                    ddlParameterOperator.Enabled = false;
                    var boolValues = new List<ViewListItem> { new ViewListItem("Yes", true.ToString()), new ViewListItem("No", false.ToString()) };

                    pnlText.Visible = false;
                    pnlNumeric.Visible = false;
                    pnlDateTime.Visible = false;
                    pnlBoolean.Visible = true;
                    ddlParameterBoolean.DataSource = boolValues;
                    ddlParameterBoolean.DataBind();
                    if (ddlParameterBoolean.ContainsValue(value1)) ddlParameterBoolean.SelectedValue = value1;

                    pnlText2.Visible = false;
                    pnlNumeric2.Visible = false;
                    pnlDateTime2.Visible = false;
                    pnlBoolean2.Visible = true;
                    ddlParameterBoolean2.DataSource = boolValues;
                    ddlParameterBoolean2.DataBind();
                    if (ddlParameterBoolean2.ContainsValue(value2)) ddlParameterBoolean2.SelectedValue = value2;
                    break;
            }
        }

        
        public void LoadParameter(ParameterColumn parameter, SqlDbType dataType, bool preserveKeywordDate = false)
        {
            hidIsRequiredParameter.Value = false.ToString();
            litParameterName.Text = parameter.ReportColumnName;
            chkIsReadOnly.Checked = parameter.ReadOnly;
            hidParameterDataType.Value = dataType.ToString();

            pnlDefaultValue2.Visible = true;
            if (parameter.Operator == Operator.Between || parameter.Operator == Operator.NotBetween)
                pnlDefaultValue2.Attributes.Add("style", "display: inline-block;");
            else
                pnlDefaultValue2.Attributes.Add("style", "display: none;");

            ConfigureDisplay(dataType, parameter.DefaultValue, parameter.DefaultValue2, preserveKeywordDate);

            var operators = ProcessorUtilities.GetAll<Operator>();
            ddlParameterOperator.DataSource = operators
                .Select(t => new ViewListItem(t.Value, t.Key.ToString()))
                .OrderBy(o => o.Text);
            ddlParameterOperator.DataBind();
            ddlParameterOperator.SelectedValue = parameter.Operator.ToInt().ToString();
        }
        
        public void LoadParameter(RequiredValueParameter parameter, bool preserveKeywordDate = false)
        {
            hidIsRequiredParameter.Value = true.ToString();
            litParameterName.Text = parameter.Name.FormattedString();
            hidRequiredParameterName.Value = parameter.Name;
            chkIsReadOnly.Checked = parameter.ReadOnly;
            hidParameterDataType.Value = parameter.DataType.ToString();
            litParameterColumnDataType.Text = parameter.DataType.ToString();

            pnlDefaultValue2.Visible = false;

            ConfigureDisplay(parameter.DataType, parameter.DefaultValue, string.Empty, preserveKeywordDate);

            ddlParameterOperator.Enabled = false;
            var operators = ProcessorUtilities.GetAll<Operator>();
            ddlParameterOperator.DataSource = operators
                .Select(t => new ViewListItem(t.Value, t.Key.ToString()))
                .OrderBy(o => o.Text);
            ddlParameterOperator.DataBind();
            ddlParameterOperator.SelectedValue = Operator.Equal.ToInt().ToString();
        }

        
        public ParameterColumn RetrieveParameter(bool preserveKeywordDate = false)
        {
            var dataType = hidParameterDataType.Value.ToEnum<SqlDbType>();

            var parameter = new ParameterColumn
            {
                ReportColumnName = litParameterName.Text,
                Operator = dataType == SqlDbType.Bit ? Operator.Equal : ddlParameterOperator.SelectedValue.ToEnum<Operator>(),
                ReadOnly = chkIsReadOnly.Checked
            };

            switch (dataType)
            {
                case SqlDbType.NVarChar:
                    parameter.DefaultValue = txtParameterText.Text;
                    parameter.DefaultValue2 = txtParameterText2.Text;
                    break;
                case SqlDbType.BigInt:
                case SqlDbType.Decimal:
                case SqlDbType.Float:
                case SqlDbType.Int:
                    parameter.DefaultValue = txtParameterNumeric.Text == string.Empty ? 0.ToString() : txtParameterNumeric.Text;
                    parameter.DefaultValue2 = (parameter.Operator != Operator.Between &&
                                               parameter.Operator != Operator.NotBetween)
                                                  ? string.Empty
                                                  : txtParameterNumeric2.Text == string.Empty
                                                        ? 0.ToString()
                                                        : txtParameterNumeric2.Text;
                    break;
                case SqlDbType.DateTime:
                    parameter.DefaultValue = preserveKeywordDate
                                                ? txtParameterDateTime.Text.IsKeywordDate()
                                                    ? txtParameterDateTime.Text.BuildKeywordDateString(ddlParameterTime.SelectedValue)
                                                    : txtParameterDateTime.Text
                                                        .ToDateTime()
                                                        .SetTime(ddlParameterTime.SelectedValue)
                                                        .ToString()
                                                : txtParameterDateTime.Text.IsKeywordDate()
                                                    ? txtParameterDateTime.Text
                                                        .GetKeywordDatePortion().ParseKeywordDate()
                                                        .SetTime(ddlParameterTime.SelectedValue)
                                                        .ToString()
                                                    : txtParameterDateTime.Text
                                                        .ToDateTime()
                                                        .SetTime(ddlParameterTime.SelectedValue)
                                                        .ToString();
                    parameter.DefaultValue2 = (parameter.Operator != Operator.Between &&
                                               parameter.Operator != Operator.NotBetween)
                                                  ? string.Empty
                                                  : preserveKeywordDate
                                                        ? txtParameterDateTime2.Text.IsKeywordDate()
                                                              ? txtParameterDateTime2.Text
                                                                .BuildKeywordDateString(ddlParameterTime2.SelectedValue)
                                                              : txtParameterDateTime2.Text
                                                                .ToDateTime()
                                                                .SetTime(ddlParameterTime2.SelectedValue)
                                                                .ToString()
                                                        : txtParameterDateTime2.Text.IsKeywordDate()
                                                              ? txtParameterDateTime2.Text
                                                                .GetKeywordDatePortion()
                                                                .ParseKeywordDate()
                                                                .SetTime(ddlParameterTime2.SelectedValue)
                                                                .ToString()
                                                              : txtParameterDateTime2.Text
                                                                .ToDateTime()
                                                                .SetTime(ddlParameterTime2.SelectedValue)
                                                                .ToString();
                    break;
                case SqlDbType.Bit:
                    parameter.DefaultValue = ddlParameterBoolean.SelectedValue;
                    parameter.DefaultValue2 = ddlParameterBoolean2.SelectedValue;
                    break;
            }

            return parameter;
        }

		public RequiredValueParameter RetrieveRequiredParameter(bool preserveKeywordDate = false)
        {
            var dataType = hidParameterDataType.Value.ToEnum<SqlDbType>();

            var parameter = new RequiredValueParameter
            {
                Name = hidRequiredParameterName.Value,
                DataType = dataType,
                ReadOnly = chkIsReadOnly.Checked
            };

            switch (dataType)
            {
                case SqlDbType.NVarChar:
                    parameter.DefaultValue = txtParameterText.Text;
                    break;
                case SqlDbType.BigInt:
                case SqlDbType.Decimal:
                case SqlDbType.Float:
                case SqlDbType.Int:
                    parameter.DefaultValue = txtParameterNumeric.Text == string.Empty ? 0.ToString() : txtParameterNumeric.Text;
                    break;
                case SqlDbType.DateTime:
                    parameter.DefaultValue = preserveKeywordDate
                                                ? txtParameterDateTime.Text.IsKeywordDate()
                                                    ? txtParameterDateTime.Text.BuildKeywordDateString(ddlParameterTime.SelectedValue)
                                                    : txtParameterDateTime.Text
                                                        .ToDateTime()
                                                        .SetTime(ddlParameterTime.SelectedValue)
                                                        .ToString()
                                                : txtParameterDateTime.Text.IsKeywordDate()
                                                    ? txtParameterDateTime.Text
                                                        .GetKeywordDatePortion().ParseKeywordDate()
                                                        .SetTime(ddlParameterTime.SelectedValue)
                                                        .ToString()
                                                    : txtParameterDateTime.Text
                                                        .ToDateTime()
                                                        .SetTime(ddlParameterTime.SelectedValue)
                                                        .ToString();;
                    break;
                case SqlDbType.Bit:
                    parameter.DefaultValue = ddlParameterBoolean.SelectedValue;
                    break;
            }

            return parameter;
        }


        private void ResetParameterColumnFields()
        {
            txtParameterText.Text = string.Empty;
            txtParameterNumeric.Text = string.Empty;
            txtParameterDateTime.Text = string.Empty;

            txtParameterText2.Text = string.Empty;
            txtParameterNumeric2.Text = string.Empty;
            txtParameterDateTime2.Text = string.Empty;
        }

        private static void SetDateTimeValues(string value, TextBox txtDate, DropDownList ddlTime, bool preserveKeywordDate)
        {
            if (value.IsKeywordDate())
            {
                txtDate.Text = preserveKeywordDate
                    ? value.GetKeywordDatePortion()
                    : value.GetKeywordDatePortion().ParseKeywordDate().FormattedShortDate();
                var time = value.GetKeywordTimePortion();
                if (ddlTime.ContainsValue(time)) ddlTime.SelectedValue = time;
            }
            else
            {
                var dateTime = value.ToDateTime();
                txtDate.Text = dateTime.FormattedShortDate();
                var time = dateTime.ToString("HH:mm");
                if (ddlTime.ContainsValue(time)) ddlTime.SelectedValue = time;
            }
        }


        protected void OnRemoveParameterClicked(object sender, ImageClickEventArgs e)
        {
            var itemIndex = ItemIndex;

            if (RemoveParameter != null)
                RemoveParameter(this, new ViewEventArgs<int>(itemIndex));
        }
    }
}