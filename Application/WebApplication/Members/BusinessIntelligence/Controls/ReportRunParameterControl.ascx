﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportRunParameterControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls.ReportRunParameterControl" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.BusinessIntelligence" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<eShip:CustomHiddenField ID="hidItemIndex" runat="server" />

<script type="text/javascript">
    $(function () {
        $('#<%= pnlDefaultValue2.ClientID %>').css('display', $('#<%= ddlParameterOperator.ClientID %>').val() == '<%= Operator.Between.ToInt() %>' || $('#<%= ddlParameterOperator.ClientID %>').val() == '<%= Operator.NotBetween.ToInt() %>' ? 'inline-block' : 'none');
        
        $('#<%= ddlParameterOperator.ClientID %>').change(function () {
            var val = $('#<%= ddlParameterOperator.ClientID %>').val();
            $('#<%= pnlDefaultValue2.ClientID %>').css('display', val == '<%= Operator.Between.ToInt() %>' || val == '<%= Operator.NotBetween.ToInt() %>' ? 'inline-block' : 'none');
        });

        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
            $('#<%= pnlDefaultValue2.ClientID %>').css('display', $('#<%= ddlParameterOperator.ClientID %>').val() == '<%= Operator.Between.ToInt() %>' || $('#<%= ddlParameterOperator.ClientID %>').val() == '<%= Operator.NotBetween.ToInt() %>' ? 'inline-block' : 'none');
            
            $('#<%= ddlParameterOperator.ClientID %>').change(function () {
                var val = $('#<%= ddlParameterOperator.ClientID %>').val();
                $('#<%= pnlDefaultValue2.ClientID %>').css('display', val == '<%= Operator.Between.ToInt() %>' || val == '<%= Operator.NotBetween.ToInt() %>' ? 'inline-block' : 'none');
            });
        });
    });
</script>

<tr>
    <td>
        <eShip:CustomHiddenField runat="server" ID="hidIsRequiredParameter" />
        <asp:ImageButton runat="server" ID="imgRemoveParameter" ToolTip="Remove Parameter"
            CausesValidation="False" ImageUrl="~/images/icons2/deleteX.png" OnClick="OnRemoveParameterClicked" />
    </td>
    <td>
        <eShip:CustomHiddenField runat="server" ID="hidParameterReadOnly" />
        <eShip:CustomHiddenField runat="server" ID="hidParameterDataType" />
        <eShip:CustomHiddenField runat="server" ID="hidRequiredParameterName" />
        <label class="upper">
            <asp:Literal runat="server" ID="litParameterName" />
        </label>
    </td>
    <td>
        <asp:DropDownList runat="server" ID="ddlParameterOperator" DataValueField="Value"
            DataTextField="Text" CssClass="w150" CausesValidation="False" />
    </td>
    <td>
        <asp:Panel runat="server" ID="pnlText" Visible="false" Style="display: inline-block;">
            <eShip:CustomTextBox ID="txtParameterText" runat="server" CssClass="w130" />
        </asp:Panel>

        <asp:Panel runat="server" ID="pnlNumeric" Visible="false" Style="display: inline-block;">
            <eShip:CustomTextBox ID="txtParameterNumeric" runat="server"  CssClass="w130" />
        </asp:Panel>
        
        <asp:Panel runat="server" ID="pnlTime" Visible="false" Style="display: inline-block;">
			<eShip:CustomTextBox runat="server" ID="txtTime"  CssClass="w100" Type="Time" />
        </asp:Panel>

        <asp:Panel runat="server" ID="pnlDateTime" Visible="false" Style="display: inline-block;">
            <eShip:CustomTextBox ID="txtParameterDateTime" runat="server"  CssClass="w130 mr10" Type="Date" />
			<eShip:CustomTextBox runat="server" ID="txtParameterTime"  CssClass="w100" Type="Time" />
        </asp:Panel>

        <asp:Panel runat="server" ID="pnlBoolean" Visible="false" Style="display: inline-block;">
            <asp:DropDownList runat="server" ID="ddlParameterBoolean" DataValueField="Value"
                DataTextField="Text"  CssClass="w80"/>
        </asp:Panel>

        <asp:Panel runat="server" ID="pnlDefaultValue2" Style="display: inline-block;">
            <asp:Panel runat="server" ID="pnlText2" Visible="false">
                <asp:Image runat="server" ImageUrl="~/images/icons2/amper.png" CssClass="middle"/>
				<eShip:CustomTextBox ID="txtParameterText2" runat="server"  CssClass="w130" />
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlNumeric2" Visible="false">
                <asp:Image runat="server" ImageUrl="~/images/icons2/amper.png" CssClass="middle" />
				<eShip:CustomTextBox ID="txtParameterNumeric2" runat="server"  CssClass="w130" />
            </asp:Panel>
            
            <asp:Panel runat="server" ID="pnlTime2" Visible="false" Style="display: inline-block;">
                <asp:Image runat="server" ImageUrl="~/images/icons2/amper.png" CssClass="middle" />
			    <eShip:CustomTextBox runat="server" ID="txtTime2"  CssClass="w100" Type="Time" />
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlDateTime2" Visible="false" >
               <asp:Image runat="server" ImageUrl="~/images/icons2/amper.png" CssClass="middle" />
				<eShip:CustomTextBox ID="txtParameterDateTime2" runat="server"  CssClass="w130 mr10" Type="Date"  />
				<eShip:CustomTextBox runat="server" ID="txtParameterTime2"  CssClass="w100" Type="Time" />
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlBoolean2" Visible="false">
                <asp:Image runat="server" ImageUrl="~/images/icons2/amper.png" CssClass="middle"/>
				<asp:DropDownList runat="server" ID="ddlParameterBoolean2" DataValueField="Value"
                    DataTextField="Text" CssClass="w80"/>
            </asp:Panel>
        </asp:Panel>
    </td>
</tr>
