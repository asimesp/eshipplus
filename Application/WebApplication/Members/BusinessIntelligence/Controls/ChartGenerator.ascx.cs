﻿using System.Linq;
using System.Web.UI;

namespace LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls
{
    public partial class ChartGenerator : UserControl
    {
        private string _xAxisTitle;
        private string _yAxisTitle;
        private ChartData _data;


        public ChartData Data
        {
            get { return _data ?? new ChartData(); }
            set { _data = value; }
        }

        public string Title { get; set; }

        public string XAxisTitle
        {
            get
            {
                if (Data != null)
                {
                    return Data.ChartIsBarOrStackedBar() ? _yAxisTitle : _xAxisTitle;
                }

                return _xAxisTitle;
            }
            set { _xAxisTitle = value; }
        }

        public string YAxisTitle
        {
            get
            {
                if (Data != null)
                {
                    return Data.ChartIsBarOrStackedBar() ? _xAxisTitle : _yAxisTitle;
                }

                return _yAxisTitle;
            }
            set { _yAxisTitle = value; }
        }

	    public XYSeries DataSource { get; set; }

        public override void DataBind()
        {
            if(DataSource != null) Data = new ChartData(DataSource.X.ToList(),DataSource.Y.ToList());
            base.DataBind();
        }
	}
}