﻿using System.Collections.Generic;

namespace LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls
{
	public class XYSeries
	{
		public IEnumerable<object> X { get; set; }
		public IEnumerable<SeriesYAxis> Y { get; set; }

		public XYSeries()
		{
			Y = null;
			X = null;
		}
	}
}
