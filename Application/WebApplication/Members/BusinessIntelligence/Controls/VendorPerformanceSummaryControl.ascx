﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VendorPerformanceSummaryControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls.VendorPerformanceSummaryControl" %>
<%@ Register TagPrefix="pc" TagName="ChartGenerator" Src="~/Members/BusinessIntelligence/Controls/ChartGenerator.ascx" %>
<asp:Panel runat="server" ID="pnlVendorPerformanceSummaryControl" CssClass="popupControl">
    <div class="popheader">
        <h4>Vendor Performance Summary</h4>
        <asp:ImageButton ImageUrl="~/images/icons2/close.png" CssClass="close" CausesValidation="false" OnClick="OnCancelClicked" runat="server" />
    </div>
    <div class="rowgroup p_m0">
        <div class="row">
            <div class="fieldgroup pl10 pt10 pb10">
                <table class="poptable">
                    <tr>
                        <td class="text-right">
                            <label class="upper">Date Unit:</label>
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlDateUnit" DataTextField="Text" DataValueField="Value" AutoPostBack="True" OnSelectedIndexChanged="OnDateUnitSelectIndexChanged" />
                        </td>
                        <td class="text-right">
                            <label class="upper">Start Date:</label>
                        </td>
                        <td>
                            <eShip:CustomTextBox runat="server" ID="txtStartDate" CssClass="w100" Type="Date" placeholder="99/99/9999" AutoPostBack="True" OnTextChanged="OnStartDateTextChanged"/>
                        </td>
                        <td class="text-right">
                            <label class="upper">End Date:</label>
                        </td>
                        <td>
                            <eShip:CustomTextBox runat="server" ID="txtEndDate" CssClass="w100" Type="Date" placeholder="99/99/9999" AutoPostBack="True" OnTextChanged="OnEndDateTextChanged"/>
                        </td>
                    </tr>
                </table>
            </div>
            <asp:Panel runat="server" ID="pnlInDepthAnalysis" CssClass="fieldgroup pt10 mt5">
                <label>
                    <asp:HyperLink runat="server" ID="hypInDepthAnalaysis" Target="_blank" CssClass="blue">View In-Depth Analysis</asp:HyperLink>
                </label>
            </asp:Panel>
        </div>

       <pc:ChartGenerator ID="chartGenerator" runat="server" />
    </div>
</asp:Panel>
<asp:Panel runat="server" ID="pnlVendorPerformanceSummaryControlDimScreen" CssClass="dimBackgroundControl" />
<eShip:CustomHiddenField runat="server" ID="hidServiceMode" />
<eShip:CustomHiddenField runat="server" ID="hidVendorId" />
