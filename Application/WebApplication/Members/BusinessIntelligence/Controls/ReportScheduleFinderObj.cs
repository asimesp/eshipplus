﻿using System;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls
{
    [Serializable]
    public class ReportScheduleFinderObj
    {
        public long Id { get; set; }

        public string ReportConfiguration { get; set; }

        public string Username { get; set; }

        public ScheduleInterval ScheduleInterval { get; set; }

        public string Time { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }
    }
}