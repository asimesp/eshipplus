﻿using System.Collections.Generic;
using System.Drawing;
using System.Web.UI.DataVisualization.Charting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls
{
	public class SeriesYAxis
	{
		public string Name { get; set; }
		public IEnumerable<object> Data { get; set; }
		public SeriesChartType Type { get; set; }

		public SeriesYAxis()
			: this(string.Empty, null, SeriesChartType.Bar, 0, Color.Empty)
		{
		}

		public SeriesYAxis(string name, IEnumerable<object> data)
			: this(name, data, SeriesChartType.Bar, 0, Color.Empty)
		{
		}

		public SeriesYAxis(string name, IEnumerable<object> data, SeriesChartType type, int priority, Color color)
		{
			Name = name;
			Data = data;
			Type = type;
		}

		public static SeriesChartType Convert(ReportChartType type)
		{
			switch (type)
			{
				case ReportChartType.Line:
					return SeriesChartType.Line;
				case ReportChartType.Column:
					return SeriesChartType.Column;
				case ReportChartType.Bar:
					return SeriesChartType.Bar;
				case ReportChartType.Pie:
					return SeriesChartType.Pie;
				case ReportChartType.StackedBar:
					return SeriesChartType.StackedBar;
				case ReportChartType.StackedColumn:
					return SeriesChartType.StackedColumn;
				default:
					return SeriesChartType.Point;
			}
		}
	}
}
