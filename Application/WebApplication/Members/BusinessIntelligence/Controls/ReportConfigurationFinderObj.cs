﻿using System;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls
{
    [Serializable]
    public class ReportConfigurationFinderObj
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public ReportConfigurationVisibility Visibility { get; set; }

        public string CreatedBy { get; set; }
    }
}