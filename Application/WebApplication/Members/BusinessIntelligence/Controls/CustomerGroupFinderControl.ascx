﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerGroupFinderControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls.CustomerGroupFinderControl" %>
<%@ Register TagPrefix="pc" TagName="ReportRunParameterControl" Src="~/Members/BusinessIntelligence/Controls/ReportRunParameterControl.ascx" %>
<asp:Panel runat="server" ID="pnlCustomerGroupFinderContent" CssClass="popupControl" DefaultButton="btnSearch">
    <div class="popheader">
        <h4>Customer Group Finder
                <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                    CausesValidation="false" OnClick="OnCancelClicked" runat="server" />
        </h4>
    </div>
    <script type="text/javascript">
        $(function () {
            if (jsHelper.IsTrue($('#<%= hidAreFiltersShowing.ClientID%>').val())) {
                $('#<%= pnlSearch.ClientID%>').show();
                $('#<%= pnlSearchShowFilters.ClientID%>').hide();
            } else {
                $('#<%= pnlSearch.ClientID%>').hide();
                $('#<%= pnlSearchShowFilters.ClientID%>').show();
            }
        });

        function ToggleFilters() {
            $('#<%= hidAreFiltersShowing.ClientID%>').val(jsHelper.IsTrue($('#<%= hidAreFiltersShowing.ClientID%>').val()) ? false : true);
            $('#<%= pnlSearch.ClientID%>').toggle();
            $('#<%= pnlSearchShowFilters.ClientID%>').toggle();
            var top = $('#<%= pnlCustomerGroupFinderContent.ClientID%> div[class="finderScroll"]').offset().top - $('#<%= pnlCustomerGroupFinderContent.ClientID%>').offset().top - 2;
            $('#<%= pnlCustomerGroupFinderContent.ClientID%> div[class="finderScroll"] > div[id^="hd"]').css('top', top + 'px');
        }
    </script>
    <table id="finderTable" class="poptable">
        <tr>
            <td>
                <asp:Panel runat="server" ID="pnlSearchShowFilters" class="rowgroup mb0" style="display:none;">
                    <div class="row">
                        <div class="fieldgroup">
                            <asp:Button ID="btnSelectAll2" runat="server" Text="ADD SELECTED" OnClick="OnSelectAllClicked" CausesValidation="false" />
                        </div>
                        <div class="fieldgroup right">
                            <button onclick="ToggleFilters(); return false;">Show Filters</button>
                        </div>
                        <div class="fieldgroup right mb5 mt5 mr10">
                            <span class="fs75em  ">*** If you would like to show parameters when searching change the 'Always Show Finder Parameters On Search' option on your <asp:HyperLink ID="hypGoToUserProfile" Target="_blank" CssClass="blue" runat="server" Text="User Profile"/></span>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
                    <div class="mt5">
                        <div class="rowgroup mb0">
                            <div class="row">
                                <div class="fieldgroup">
                                    <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                                        CausesValidation="False" CssClass="add"/>
                                </div>
                                <div class="fieldgroup right">
                                    <button onclick="ToggleFilters(); return false;">Hide Filters</button>
                                </div>
                                <div class="right">
                                    <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="CustomerGroups" OnProcessingError="OnSearchProfilesProcessingError"
                                        OnSearchProfileSelected="OnSearchProfilesProfileSelected" />
                                </div>
                            </div>
                        </div>

                        <hr class="mb5" />
                        <script type="text/javascript">$(window).load(function () {SetControlFocus();});</script><table class="mb5" id="parametersTable">
                            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                                OnItemDataBound="OnParameterItemDataBound">
                                <LayoutTemplate>
                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <pc:ReportRunParameterControl ID="reportRunParameter" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                                        OnRemoveParameter="OnParameterRemove" />
                                </ItemTemplate>
                            </asp:ListView>
                            <tr>
                                <td colspan="20" class="mt10">
                                    <asp:Button ID="btnSearch" CssClass="left" runat="server" Text="SEARCH" OnClick="OnSearchClicked"
                                        CausesValidation="False"/>
                                    <asp:Button ID="btnCancel" OnClick="OnCancelClicked" runat="server" Text="CANCEL"
                                        CausesValidation="false" CssClass="ml10"/>
                                    <asp:Button ID="btnSelectAll" runat="server" Text="ADD SELECTED" OnClick="OnSelectAllClicked"
                                        CausesValidation="false" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </td>
        </tr>
    </table>

    <asp:UpdatePanel runat="server" ID="upDataUpdate">
        <ContentTemplate>
            <eShip:ContinuousScrollExtender ID="upcseDataUpdate" runat="server" BindableControlId="lstSearchResults" UpdatePanelToExtendId="upDataUpdate" ScrollableDivId="customerGroupFinderScrollSection"
                SelectionCheckBoxControlId="chkSelected" SelectionUniqueRefHiddenFieldId="hidCustomerGroupId" />
            <eShip:TableFreezeHeaderExtender runat="server" ID="tfheGroupFinderTable" TableId="customerGroupFinderTable" ScrollerContainerId="customerGroupFinderScrollSection" ScrollOffsetControlId="pnlCustomerGroupFinderContent"
                IgnoreHeaderBackgroundFill="True" HeaderZIndex="2050" />
            <div class="finderScroll" id="customerGroupFinderScrollSection">
                <asp:ListView ID="lstSearchResults" runat="server" ItemPlaceholderID="itemPlaceHolder">
                    <LayoutTemplate>
                        <table id="customerGroupFinderTable" class="stripe sm_chk">
                            <tr>
                                <th style="width: 18%">
                                    <eShip:AltUniformCheckBox ID="chkSelectAllRecords" runat="server" OnClientSideClicked="javascript:SetSelectedCheckBoxesStatus(this, 'customerGroupFinderTable');" />
                                </th>
                                <th style="width: 20%">Group Name
                                </th>
                                <th style="width: 62%">Description
                                </th>
                            </tr>
                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:Button ID="btnSelect" runat="server" Text="Select" OnClick="OnSelectClicked"
                                    Visible='<%# !EnableMultiSelection %>' CausesValidation="false"  />
                                <asp:Button ID="btnEditSelect" runat="server" Text="Edit" OnClick="OnEditSelectClicked"
                                    Visible='<%# !EnableMultiSelection && OpenForEditEnabled %>' CausesValidation="false" />
                                <eShip:AltUniformCheckBox runat="server" ID="chkSelected" Visible='<%# EnableMultiSelection %>' 
                                    OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'customerGroupFinderTable', 'chkSelectAllRecords');"/>
                                <eShip:CustomHiddenField ID="hidCustomerGroupId" Value='<%# Eval("Id") %>' runat="server" />
                            </td>
                            <td>
                                <%# Eval("Name") %>
                            </td>
                            <td>
                                <%# Eval("Description") %>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lstSearchResults" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Panel>
<eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
<asp:Panel ID="pnlCustomerGroupFinderDimScreen" CssClass="dimBackgroundControl" runat="server"
    Visible="false" />
<eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
<eShip:CustomHiddenField runat="server" ID="hidUserFinderEnableMultiSelection" />
<eShip:CustomHiddenField runat="server" ID="hidEditSelected" />
<eShip:CustomHiddenField runat="server" ID="hidOpenForEditEnabled" />
<eShip:CustomHiddenField runat="server" ID="hidAreFiltersShowing" Value="true" />