﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportScheduleFinderControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls.ReportScheduleFinderControl" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Register TagPrefix="pc" TagName="ReportRunParameterControl" Src="~/Members/BusinessIntelligence/Controls/ReportRunParameterControl.ascx" %>
<asp:Panel runat="server" ID="pnlReportScheduleFinderContent" CssClass="popupControl" DefaultButton="btnSearch">
    <div class="popheader">
        <h4>Report Schedule Finder<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" Text="" /></small>
            <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/icons2/close.png" CssClass="close"
                CausesValidation="false" OnClick="OnCancelClicked" runat="server" />
        </h4>
    </div>
    <script type="text/javascript">
        $(function () {
            if (jsHelper.IsTrue($('#<%= hidAreFiltersShowing.ClientID%>').val())) {
                $('#<%= pnlSearch.ClientID%>').show();
                $('#<%= pnlSearchShowFilters.ClientID%>').hide();
            } else {
                $('#<%= pnlSearch.ClientID%>').hide();
                $('#<%= pnlSearchShowFilters.ClientID%>').show();
            }
        });

        function ToggleFilters() {
            $('#<%= hidAreFiltersShowing.ClientID%>').val(jsHelper.IsTrue($('#<%= hidAreFiltersShowing.ClientID%>').val()) ? false : true);
            $('#<%= pnlSearch.ClientID%>').toggle();
            $('#<%= pnlSearchShowFilters.ClientID%>').toggle();
            var top = $('#<%= pnlReportScheduleFinderContent.ClientID%> div[class="finderScroll"]').offset().top - $('#<%= pnlReportScheduleFinderContent.ClientID%>').offset().top - 2;
            $('#<%= pnlReportScheduleFinderContent.ClientID%> div[class="finderScroll"] > div[id^="hd"]').css('top', top + 'px');
        }
    </script>
    <table id="finderTable" class="poptable">
        <tr>
            <td>
                <asp:Panel runat="server" ID="pnlSearchShowFilters" class="rowgroup mb0" style="display:none;">
                    <div class="row">
                        <div class="fieldgroup">
                            <asp:Button ID="btnSelectAll2" runat="server" Text="ADD SELECTED" OnClick="OnSelectAllClicked" CssClass="ml10" CausesValidation="false" />
                        </div>
                        <div class="fieldgroup right">
                            <button onclick="ToggleFilters(); return false;">Show Filters</button>
                        </div>
                        <div class="fieldgroup right mb5 mt5 mr10">
                            <span class="fs75em  ">*** If you would like to show parameters when searching change the 'Always Show Finder Parameters On Search' option on your <asp:HyperLink ID="hypGoToUserProfile" Target="_blank" CssClass="blue" runat="server" Text="User Profile"/></span>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
                    <div class="mt5">
                        <div class="rowgroup mb0">
                            <div class="row">
                                <div class="fieldgroup">
                                    <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                                        CausesValidation="False" CssClass="add" />
                                </div>
                                <div class="fieldgroup right">
                                    <button onclick="ToggleFilters(); return false;">Hide Filters</button>
                                </div>
                                <div class="fieldgroup right">
                                    <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="ReportSchedules" ShowAutoRefresh="False"
                                        OnProcessingError="OnSearchProfilesProcessingError" OnSearchProfileSelected="OnSearchProfilesProfileSelected" />
                                </div>
                            </div>
                        </div>

                        <hr class="mb5" />

                        <script type="text/javascript">$(window).load(function () {SetControlFocus();});</script><table class="mb5" id="parametersTable">
                            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                                OnItemDataBound="OnParameterItemDataBound">
                                <LayoutTemplate>
                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <pc:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex%>'
                                        OnRemoveParameter="OnParameterRemove" />
                                </ItemTemplate>
                            </asp:ListView>
                            <tr>
                                <td colspan="4">
                                    <asp:Button ID="btnSearch" CssClass="left" runat="server" Text="SEARCH" OnClick="OnSearchClicked"
                                        CausesValidation="False" />
                                    <asp:Button ID="btnCancel" OnClick="OnCancelClicked" runat="server" Text="CANCEL"
                                        CssClass="ml10" CausesValidation="false" />
                                    <asp:Button ID="btnSelectAll" runat="server" Text="ADD SELECTED" OnClick="OnSelectAllClicked"
                                        CssClass="ml10" CausesValidation="false" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel runat="server" ID="upDataUpdate">
        <ContentTemplate>
            <eShip:ContinuousScrollExtender ID="upcseDataUpdate" runat="server" BindableControlId="lstSearchResults" UpdatePanelToExtendId="upDataUpdate" ScrollableDivId="reportScheduleFinderScrollSection"
                SelectionCheckBoxControlId="chkSelected" SelectionUniqueRefHiddenFieldId="hidReportScheduleId" />
            <eShip:TableFreezeHeaderExtender runat="server" ID="tfheReportScheduleFinderTable" TableId="reportScheduleFinderTable" ScrollerContainerId="reportScheduleFinderScrollSection" ScrollOffsetControlId="pnlReportScheduleFinderContent"
                IgnoreHeaderBackgroundFill="True" HeaderZIndex="2050" />
            <div class="finderScroll" id="reportScheduleFinderScrollSection">
                <asp:ListView ID="lstSearchResults" runat="server" ItemPlaceholderID="itemPlaceHolder">
                    <LayoutTemplate>
                        <table id="reportScheduleFinderTable" class="stripe sm_chk">
                            <tr>
                                <th style="width: 15%">
                                    <eShip:AltUniformCheckBox ID="chkSelectAllRecords" runat="server" OnClientSideClicked="javascript:SetSelectedCheckBoxesStatus(this, 'reportScheduleFinderTable');" />
                                </th>
                                <th style="width: 35%">Report Configuration
                                </th>
                                <th style="width: 15%">User
                                </th>
                                <th style="width: 10%">Interval
                                </th>
                                <th style="width: 5%">Time
                                </th>
                                <th style="width: 10%">Start Date
                                </th>
                                <th style="width: 10%">End Date
                                </th>
                            </tr>
                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:Button ID="btnSelect" runat="server" Text="Select" OnClick="OnSelectClicked"
                                    Visible='<%# !EnableMultiSelection%>' CausesValidation="false" />
                                <asp:Button ID="btnEditSelect" runat="server" Text="Edit" OnClick="OnEditSelectClicked"
                                    Visible='<%# !EnableMultiSelection && OpenForEditEnabled%>' CausesValidation="false" />
                                <eShip:AltUniformCheckBox runat="server" ID="chkSelected" Visible='<%# EnableMultiSelection%>'
                                    OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'reportScheduleFinderTable', 'chkSelectAllRecords');" />
                                <eShip:CustomHiddenField ID="hidReportScheduleId" Value='<%# Eval("Id")%>' runat="server" />
                            </td>
                            <td>
                                <%# Eval("ReportConfiguration")%>
                            </td>
                            <td>
                                <%# Eval("Username")%>
                            </td>
                            <td>
                                <%# Eval("ScheduleInterval").FormattedString()%>
                            </td>
                            <td>
                                <%# Eval("Time") %>
                            </td>
                            <td>
                                <%# Eval("Start").FormattedShortDate()%>
                            </td>
                            <td>
                                <%# Eval("End").FormattedShortDate()%>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lstSearchResults" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Panel>
<eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
<asp:Panel ID="pnlReportScheduleFinderDimScreen" CssClass="dimBackgroundControl" runat="server"
    Visible="false" />
<eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information"/>
<eShip:CustomHiddenField runat="server" ID="hidEditSelected" />
<eShip:CustomHiddenField runat="server" ID="hidOpenForEditEnabled" />
<eShip:CustomHiddenField runat="server" ID="hidOnlyActiveResults" />
<eShip:CustomHiddenField runat="server" ID="hidReportScheduleFinderEnableMultiSelection" />
<eShip:CustomHiddenField runat="server" ID="hidAreFiltersShowing" Value="true" />