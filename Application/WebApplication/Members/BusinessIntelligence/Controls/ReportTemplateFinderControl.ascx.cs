﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using LogisticsPlus.Eship.WebApplication.Members.Administration;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls
{
    public partial class ReportTemplateFinderControl : MemberControlBase, IReportTemplateFinderView
    {
        public new bool Visible
        {
            get { return pnlReportTemplateFinderContent.Visible; }
            set
            {
                base.Visible = true;
                pnlReportTemplateFinderContent.Visible = value;
                pnlReportTemplateFinderDimScreen.Visible = value;
            }
        }

        public bool OpenForEdit
        {
            get { return hidEditSelected.Value.ToBoolean(); }
        }

        public bool OpenForEditEnabled
        {
            get { return hidOpenForEditEnabled.Value.ToBoolean(); }
            set { hidOpenForEditEnabled.Value = value.ToString(); }
        }

        public bool EnableMultiSelection
        {
            get { return hidReportTemplateFinderEnableMultiSelection.Value.ToBoolean(); }
            set
            {
                hidReportTemplateFinderEnableMultiSelection.Value = value.ToString();
                btnSelectAll.Visible = value;
                btnSelectAll2.Visible = value;
                cseDataUpdate.PreserveRecordSelection = value;
            }
        }

        public event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
        public event EventHandler<ViewEventArgs<ReportTemplate>> ItemSelected;
        public event EventHandler<ViewEventArgs<List<ReportTemplate>>> MultiItemSelected;
        public event EventHandler SelectionCancel;

        public void DisplaySearchResult(List<ReportTemplate> reportTemplates)
        {
            litRecordCount.Text = reportTemplates.BuildRecordCount();
            cseDataUpdate.DataSource = reportTemplates.OrderBy(t => t.Name).ToList();
            cseDataUpdate.DataBind();
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void Reset() { DisplaySearchResult(new List<ReportTemplate>()); }

        private void DoSearch(List<ParameterColumn> columns)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<List<ParameterColumn>>(columns));

            var control = lstSearchResults.FindControl("chkSelectAllRecords").ToAltUniformCheckBox();
            if (control != null)
            {
                control.Checked = false;
                control.Visible = EnableMultiSelection;
            }
        }


        private void ProcessItemSelection(long reportTemplateId)
        {
            if (ItemSelected != null)
                ItemSelected(this, new ViewEventArgs<ReportTemplate>(new ReportTemplate(reportTemplateId)));
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new ReportTemplateFinderHandler(this).Initialize();

            if (IsPostBack) return;

            lstFilterParameters.DataSource = BusinessIntelligenceSearchFields.Default.Select(f => f.ToParameterColumn()).ToList();
            lstFilterParameters.DataBind();
        }

        protected void OnSearchClicked(object sender, EventArgs e)
        {
            hidAreFiltersShowing.Value = false.ToString();
            cseDataUpdate.ResetLoadCount();
            DoSearch(GetCurrentRunParameters());
        }


        protected void OnSelectClicked(object sender, EventArgs e)
        {
            var button = (Button)sender;
            hidEditSelected.Value = false.ToString();
            ProcessItemSelection(button.Parent.FindControl("hidReportTemplateId").ToCustomHiddenField().Value.ToLong());
        }

        protected void OnEditSelectClicked(object sender, EventArgs e)
        {
            var button = (Button)sender;
            hidEditSelected.Value = true.ToString();
            ProcessItemSelection(button.Parent.FindControl("hidReportTemplateId").ToCustomHiddenField().Value.ToLong());
        }

        protected void OnCancelClicked(object sender, EventArgs eventArgs)
        {
            if (SelectionCancel != null)
                SelectionCancel(this, new EventArgs());
        }

        protected void OnSelectAllClicked(object sender, EventArgs e)
        {
            var reportTemplates = (from item in lstSearchResults.Items
                                   let hidden = item.FindControl("hidReportTemplateId").ToCustomHiddenField()
                                   let checkBox = item.FindControl("chkSelected").ToAltUniformCheckBox()
                                   where checkBox != null && checkBox.Checked
                                   select new ReportTemplate(hidden.Value.ToLong()))
                .Where(u => u.KeyLoaded)
                .ToList();

            if (MultiItemSelected != null)
                MultiItemSelected(this, new ViewEventArgs<List<ReportTemplate>>(reportTemplates));
        }


        private List<ParameterColumn> GetCurrentRunParameters()
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter()).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = BusinessIntelligenceSearchFields.ReportTemplates.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters();

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

            Reset();
        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(BusinessIntelligenceSearchFields.ReportTemplates);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters();
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;

            Reset();
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }
    }
}