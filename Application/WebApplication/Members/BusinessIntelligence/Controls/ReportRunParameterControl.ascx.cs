﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls
{
    public partial class ReportRunParameterControl : MemberControlBase
    {
        public int ItemIndex
        {
            get { return hidItemIndex.Value.ToInt(); }
            set { hidItemIndex.Value = value.ToString(); }
        }

        public bool IsRequiredParameter
        {
            get { return hidIsRequiredParameter.Value.ToBoolean(); }
        }

        public event EventHandler<ViewEventArgs<int>> RemoveParameter;


        private void ConfigureDisplay(SqlDbType dataType, string value1, string value2, bool preserveKeywordDate)
        {
            ResetParameterColumnFields();
            switch (dataType)
            {
                case SqlDbType.NVarChar:
                    pnlText.Visible = true;
                    pnlTime.Visible = false;
                    pnlTime2.Visible = false;
                    pnlNumeric.Visible = false;
                    pnlDateTime.Visible = false;
                    pnlBoolean.Visible = false;
                    txtParameterText.Text = value1;
                    pnlText2.Visible = true;
                    pnlNumeric2.Visible = false;
                    pnlDateTime2.Visible = false;
                    pnlBoolean2.Visible = false;
                    txtParameterText2.Text = value2;
                    break;
                case SqlDbType.Time:
                    pnlTime.Visible = true;
                    pnlTime2.Visible = true;
                    txtTime.Text = value1;
                    txtTime2.Text = value2;
                    pnlText.Visible = false;
                    pnlNumeric.Visible = false;
                    pnlDateTime.Visible = false;
                    pnlBoolean.Visible = false;
                    pnlText2.Visible = false;
                    pnlNumeric2.Visible = false;
                    pnlDateTime2.Visible = false;
                    pnlBoolean2.Visible = false;
                    break;
                case SqlDbType.BigInt:
                case SqlDbType.Decimal:
                case SqlDbType.Float:
                case SqlDbType.Int:
                    pnlText.Visible = false;
                    pnlTime.Visible = false;
                    pnlTime2.Visible = false;
                    pnlNumeric.Visible = true;
                    pnlDateTime.Visible = false;
                    pnlBoolean.Visible = false;
                    txtParameterNumeric.Text = value1;

                    pnlText2.Visible = false;
                    pnlNumeric2.Visible = true;
                    pnlDateTime2.Visible = false;
                    pnlBoolean2.Visible = false;
                    txtParameterNumeric2.Text = value2;
                    break;
                case SqlDbType.DateTime:
                    pnlText.Visible = false;
                    pnlTime.Visible = false;
                    pnlTime2.Visible = false;
                    pnlNumeric.Visible = false;
                    pnlDateTime.Visible = true;
                    pnlBoolean.Visible = false;
                    SetDateTimeValues(string.IsNullOrEmpty(value1)
                                        ? DateTime.Now.TimeToMinimum().ToString()
                                        : value1, txtParameterDateTime, txtParameterTime, preserveKeywordDate);

                    pnlText2.Visible = false;
                    pnlNumeric2.Visible = false;
                    pnlDateTime2.Visible = true;
                    pnlBoolean2.Visible = false;
                    SetDateTimeValues(string.IsNullOrEmpty(value2)
                                        ? DateTime.Now.AddDays(1).TimeToMinimum().ToString()
                                        : value2, txtParameterDateTime2, txtParameterTime2, preserveKeywordDate);
                    break;
                case SqlDbType.Bit:
                    ddlParameterOperator.Enabled = false;
                    var boolValues = new List<ViewListItem> { new ViewListItem("Yes", true.ToString()), new ViewListItem("No", false.ToString()) };

                    pnlText.Visible = false;
                    pnlTime.Visible = false;
                    pnlTime2.Visible = false;
                    pnlNumeric.Visible = false;
                    pnlDateTime.Visible = false;
                    pnlBoolean.Visible = true;
                    ddlParameterBoolean.DataSource = boolValues;
                    ddlParameterBoolean.DataBind();
                    if (ddlParameterBoolean.ContainsValue(value1)) ddlParameterBoolean.SelectedValue = value1;

                    pnlText2.Visible = false;
                    pnlNumeric2.Visible = false;
                    pnlDateTime2.Visible = false;
                    pnlBoolean2.Visible = true;
                    ddlParameterBoolean2.DataSource = boolValues;
                    ddlParameterBoolean2.DataBind();
                    if (ddlParameterBoolean2.ContainsValue(value2)) ddlParameterBoolean2.SelectedValue = value2;
                    break;
            }
        }


        public void LoadParameter(List<ReportColumn> reportColumns, ParameterColumn parameter)
        {
            LoadParameter(reportColumns, parameter, false);
        }

        public void LoadParameter(List<ReportColumn> reportColumns, ParameterColumn parameter, bool preserveKeywordDate)
        {
            var dataType = reportColumns.Where(i => i.Name == parameter.ReportColumnName).Select(c => c.DataType).First();
            LoadParameter(parameter, dataType, preserveKeywordDate);
        }



        public void LoadParameter(ParameterColumn parameter, SqlDbType dataType)
        {
            LoadParameter(parameter, dataType, false);
        }

        public void LoadParameter(ParameterColumn parameter, SqlDbType dataType, bool preserveKeywordDate)
        {
            hidIsRequiredParameter.Value = false.ToString();
            imgRemoveParameter.Visible = !parameter.ReadOnly;

            pnlText.Enabled = !parameter.ReadOnly;
            pnlText2.Enabled = !parameter.ReadOnly;
            pnlNumeric.Enabled = !parameter.ReadOnly;
            pnlNumeric2.Enabled = !parameter.ReadOnly;
            pnlDefaultValue2.Enabled = !parameter.ReadOnly;
            pnlBoolean.Enabled = !parameter.ReadOnly;
            pnlBoolean2.Enabled = !parameter.ReadOnly;
            pnlDateTime.Enabled = !parameter.ReadOnly;
            pnlDateTime2.Enabled = !parameter.ReadOnly;
            ddlParameterOperator.Enabled = !parameter.ReadOnly;
            pnlTime.Enabled = !parameter.ReadOnly;
            pnlTime2.Enabled = !parameter.ReadOnly;


            litParameterName.Text = parameter.ReportColumnName;
            hidParameterReadOnly.Value = parameter.ReadOnly.ToString();
            hidParameterDataType.Value = dataType.ToString();

            pnlDefaultValue2.Visible = true;
            if (parameter.Operator == Operator.Between || parameter.Operator == Operator.NotBetween)
                pnlDefaultValue2.Attributes.Add("style", "display: inline-block;");
            else
                pnlDefaultValue2.Attributes.Add("style", "display: none;");

            ConfigureDisplay(dataType, parameter.DefaultValue, parameter.DefaultValue2, preserveKeywordDate);

            var operators = ProcessorUtilities.GetAll<Operator>();
            ddlParameterOperator.DataSource = operators
                .Select(t => new ViewListItem(t.Value, t.Key.ToString()))
                .OrderBy(o => o.Text);
            ddlParameterOperator.DataBind();
            ddlParameterOperator.SelectedValue = parameter.Operator.ToInt().ToString();
        }



        public void LoadParameter(RequiredValueParameter parameter)
        {
            LoadParameter(parameter, false);
        }

        public void LoadParameter(RequiredValueParameter parameter, bool preserveKeywordDate)
        {
            hidIsRequiredParameter.Value = true.ToString();
            imgRemoveParameter.Visible = false;

            pnlText.Enabled = !parameter.ReadOnly;
            pnlText2.Enabled = !parameter.ReadOnly;
            pnlNumeric.Enabled = !parameter.ReadOnly;
            pnlNumeric2.Enabled = !parameter.ReadOnly;
            pnlDefaultValue2.Enabled = !parameter.ReadOnly;
            pnlBoolean.Enabled = !parameter.ReadOnly;
            pnlBoolean2.Enabled = !parameter.ReadOnly;
            pnlDateTime.Enabled = !parameter.ReadOnly;
            pnlDateTime2.Enabled = !parameter.ReadOnly;
            pnlTime.Enabled = !parameter.ReadOnly;
            pnlTime2.Enabled = !parameter.ReadOnly;
            ddlParameterOperator.Enabled = !parameter.ReadOnly;

            litParameterName.Text = parameter.Name.FormattedString();
            hidRequiredParameterName.Value = parameter.Name;
            hidParameterReadOnly.Value = parameter.ReadOnly.ToString();
            hidParameterDataType.Value = parameter.DataType.ToString();

            pnlDefaultValue2.Visible = true;
            pnlDefaultValue2.Attributes.Add("style", "display: none;");

            ConfigureDisplay(parameter.DataType, parameter.DefaultValue, string.Empty, preserveKeywordDate);

            ddlParameterOperator.Enabled = false;
            var operators = ProcessorUtilities.GetAll<Operator>();
            ddlParameterOperator.DataSource = operators
                .Select(t => new ViewListItem(t.Value, t.Key.ToString()))
                .OrderBy(o => o.Text);
            ddlParameterOperator.DataBind();
            ddlParameterOperator.SelectedValue = Operator.Equal.ToInt().ToString();
        }




        public ParameterColumn RetrieveParameter()
        {
            return RetrieveParameter(false);
        }

        public ParameterColumn RetrieveParameter(bool preserveKeywordDate)
        {
            var dataType = hidParameterDataType.Value.ToEnum<SqlDbType>();

            var parameter = new ParameterColumn
            {
                ReportColumnName = litParameterName.Text,
                Operator = dataType == SqlDbType.Bit ? Operator.Equal : ddlParameterOperator.SelectedValue.ToEnum<Operator>(),
                ReadOnly = hidParameterReadOnly.Value.ToBoolean()
            };

            switch (dataType)
            {
                case SqlDbType.NVarChar:
                    parameter.DefaultValue = txtParameterText.Text;
                    parameter.DefaultValue2 = txtParameterText2.Text;
                    break;
                case SqlDbType.BigInt:
                case SqlDbType.Decimal:
                case SqlDbType.Float:
                case SqlDbType.Int:
                    parameter.DefaultValue = txtParameterNumeric.Text == string.Empty ? 0.ToString() : txtParameterNumeric.Text;
                    parameter.DefaultValue2 = txtParameterNumeric2.Text == string.Empty ? 0.ToString() : txtParameterNumeric2.Text;
                    break;
                case SqlDbType.Time:
                    parameter.DefaultValue = txtTime.Text;
                    parameter.DefaultValue2 = txtTime2.Text;
                    break;
                case SqlDbType.DateTime:
                    parameter.DefaultValue = preserveKeywordDate
                                                ? txtParameterDateTime.Text.IsKeywordDate()
                                                    ? txtParameterDateTime.Text.BuildKeywordDateString(txtParameterTime.Text)
                                                    : txtParameterDateTime.Text
                                                        .ToDateTime()
                                                        .SetTime(txtParameterTime.Text)
                                                        .ToString()
                                                : txtParameterDateTime.Text.IsKeywordDate()
                                                    ? txtParameterDateTime.Text
                                                        .GetKeywordDatePortion().ParseKeywordDate()
                                                        .SetTime(txtParameterTime.Text)
                                                        .ToString()
                                                    : txtParameterDateTime.Text
                                                        .ToDateTime()
                                                        .SetTime(txtParameterTime.Text)
                                                        .ToString();
                    parameter.DefaultValue2 = preserveKeywordDate
                                                ? txtParameterDateTime2.Text.IsKeywordDate()
                                                    ? txtParameterDateTime2.Text.BuildKeywordDateString(txtParameterTime2.Text)
                                                    : txtParameterDateTime2.Text
                                                        .ToDateTime()
                                                        .SetTime(txtParameterTime2.Text)
                                                        .ToString()
                                                : txtParameterDateTime2.Text.IsKeywordDate()
                                                    ? txtParameterDateTime2.Text
                                                        .GetKeywordDatePortion().ParseKeywordDate()
                                                        .SetTime(txtParameterTime2.Text)
                                                        .ToString()
                                                    : txtParameterDateTime2.Text
                                                        .ToDateTime()
                                                        .SetTime(txtParameterTime2.Text)
                                                        .ToString();
                    break;
                case SqlDbType.Bit:
                    parameter.DefaultValue = ddlParameterBoolean.SelectedValue;
                    parameter.DefaultValue2 = ddlParameterBoolean2.SelectedValue;
                    break;
            }

            return parameter;
        }

        public RequiredValueParameter RetrieveRequiredParameter()
        {
            var dataType = hidParameterDataType.Value.ToEnum<SqlDbType>();

            var parameter = new RequiredValueParameter
            {
                Name = hidRequiredParameterName.Value,
                DataType = dataType,
                ReadOnly = hidParameterReadOnly.Value.ToBoolean(),
            };

            switch (dataType)
            {
                case SqlDbType.NVarChar:
                    parameter.DefaultValue = txtParameterText.Text;
                    break;
                case SqlDbType.Time:
                    parameter.DefaultValue = txtTime.Text;
                    break;
                case SqlDbType.BigInt:
                case SqlDbType.Decimal:
                case SqlDbType.Float:
                case SqlDbType.Int:
                    parameter.DefaultValue = txtParameterNumeric.Text == string.Empty ? 0.ToString() : txtParameterNumeric.Text;
                    break;
                case SqlDbType.DateTime:
                    parameter.DefaultValue = txtParameterDateTime.Text.ToDateTime().SetTime(txtParameterTime.Text).ToString();
                    break;
                case SqlDbType.Bit:
                    parameter.DefaultValue = ddlParameterBoolean.SelectedValue;
                    break;
            }

            return parameter;
        }


        private void ResetParameterColumnFields()
        {
            txtParameterText.Text = string.Empty;
            txtParameterNumeric.Text = string.Empty;
            txtParameterDateTime.Text = string.Empty;
            txtTime.Text = string.Empty;

            txtParameterText2.Text = string.Empty;
            txtParameterNumeric2.Text = string.Empty;
            txtParameterDateTime2.Text = string.Empty;
            txtTime2.Text = string.Empty;
        }

        private static void SetDateTimeValues(string value, TextBox txtDate, TextBox txtTime, bool preserveKeywordDate)
        {
            if (value.IsKeywordDate())
            {
                txtDate.Text = preserveKeywordDate
                    ? value.GetKeywordDatePortion()
                    : value.GetKeywordDatePortion().ParseKeywordDate().FormattedShortDate();
	            txtTime.Text = value.GetKeywordTimePortion();
            }
            else
            {
                var dateTime = value.ToDateTime();
                txtDate.Text = dateTime.FormattedShortDate();
	            txtTime.Text = dateTime.ToString("HH:mm");
            }
        }


        protected void OnRemoveParameterClicked(object sender, ImageClickEventArgs e)
        {
            var itemIndex = ItemIndex;

            if (RemoveParameter != null)
                RemoveParameter(this, new ViewEventArgs<int>(itemIndex));
        }
    }
}