﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls
{
    public partial class VendorPerformanceSummaryControl : MemberControlBase
    {
        private const string AllSeriveModesLabel = "All Servcice Modes";
        private const string OnTimeDeliveryLabel = "On-Time Delivery (%)";
        private const string OnTimePickupLabel = "On-Time Pickup (%)";
        private const string OnTimeOverallLabel = "On-Time Overall (%)";
        private const string YAxisLabel = "Percent";


        public new bool Visible
        {
            get
            {
                return pnlVendorPerformanceSummaryControl.Visible;
            }
            set
            {
                base.Visible = value;
                pnlVendorPerformanceSummaryControl.Visible = true;
                pnlVendorPerformanceSummaryControlDimScreen.Visible = true;
            }
        }

        public Dictionary<int, string> DateUnits
        {
            set
            {
                ddlDateUnit.DataSource = value.Select(t => new ViewListItem(t.Value, t.Key.ToString())).ToList();
                ddlDateUnit.DataBind();
            }
        }


        public bool CanGetInDepthAnalysis
        {
            set { pnlInDepthAnalysis.Visible = value; }
        }

        public DateTime StartDate
        {
            get { return txtStartDate.Text.ToDateTime(); }
            set { txtStartDate.Text = value.FormattedShortDate(); }
        }

        public DateTime EndDate
        {
            get { return txtEndDate.Text.ToDateTime(); }
            set { txtEndDate.Text = value.FormattedShortDate(); }
        }

        public ServiceMode ServiceMode
        {
            get { return string.IsNullOrEmpty(hidServiceMode.Value) ? ServiceMode.NotApplicable : hidServiceMode.Value.ToEnum<ServiceMode>(); }
            set { hidServiceMode.Value = value.GetString(); }
        }

        public DateUnit DateUnit
        {
            get { return ddlDateUnit.SelectedValue.ToInt().ToEnum<DateUnit>(); }
            set
            {
                if (ddlDateUnit.ContainsValue(value.ToInt().ToString()))
                    ddlDateUnit.SelectedValue = value.ToInt().ToString();
            }
        }


        public event EventHandler Cancel;


        public void LoadVendorPerformanceSummary(long vendorId)
        {
            var vendor = new Vendor(vendorId);
            hypInDepthAnalaysis.Visible = !vendor.IsNew;

            if (vendor.IsNew) return;

            hidVendorId.Value = vendorId.GetString();
            hypInDepthAnalaysis.NavigateUrl = string.Format("{0}?{1}={2}", VendorPerformanceSummaryView.PageAddress,
                                                            WebApplicationConstants.TransferNumber,
                                                            hidVendorId.Value.UrlTextEncrypt());
            var series = new List<KeyValuePair<DateTime, List<VendorStatisticsPerformanceShipmentDto>>>();

            var startDate = StartDate;
            var endDate = EndDate;
            var serviceMode = ServiceMode;
            var shipments = new VendorStatisticsPerformanceShipmentDto().RetrieveVendorStatisticsPerformanceShipmentDtos(vendor.TenantId, vendor.VendorNumber, startDate, endDate, ActiveUser.Id, serviceMode);

            switch (DateUnit)
            {
                case DateUnit.Week:
                    {
                        var dateRangeStart = startDate.FirstDateOfWeek();
                        var dateRangeEnd = dateRangeStart.AddDays(7).AddMilliseconds(-1);
                        do
                        {
                            series.Add(new KeyValuePair<DateTime, List<VendorStatisticsPerformanceShipmentDto>>(dateRangeStart, shipments.Where(s => s.DateCreated >= dateRangeStart && s.DateCreated <= dateRangeEnd).ToList()));
                            dateRangeStart = dateRangeStart.AddDays(7);
                            dateRangeEnd = dateRangeStart.AddDays(7).AddMilliseconds(-1);
                        } while (dateRangeStart <= endDate);
                    }
                    break;
                case DateUnit.Month:
                    {
                        var dateRangeStart = new DateTime(startDate.Year, startDate.Month, 1);
                        var dateRangeEnd = dateRangeStart.AddMonths(1).AddMilliseconds(-1);
                        do
                        {
                            series.Add(new KeyValuePair<DateTime, List<VendorStatisticsPerformanceShipmentDto>>(dateRangeStart, shipments.Where(s => s.DateCreated >= dateRangeStart && s.DateCreated <= dateRangeEnd).ToList()));
                            dateRangeStart = dateRangeStart.AddMonths(1);
                            dateRangeEnd = dateRangeStart.AddMonths(1).AddMilliseconds(-1);
                        } while (dateRangeStart <= endDate);
                    }
                    break;
            }

            var ySeriesDelivery = new SeriesYAxis(OnTimeDeliveryLabel, series.Select(s => (object)((s.Value.Any() ? s.Value.Sum(sh => sh.OnTimeDelivery ? 1m : 0m) / s.Value.Count.ToDecimal() : 0m) * 100m)).ToList())
                {
                    Type = SeriesChartType.Line,
                };

            var ySeriesPickup = new SeriesYAxis(OnTimePickupLabel, series.Select(s => (object)((s.Value.Any() ? s.Value.Sum(sh => sh.OnTimePickup ? 1m : 0m) / s.Value.Count.ToDecimal() : 0m) * 100m)).ToList())
                {
                    Type = SeriesChartType.Line,
                };

            var ySeriesOverall = new SeriesYAxis(OnTimeOverallLabel, series.Select(s => (object)((s.Value.Any() ? s.Value.Sum(sh => (sh.OnTimeDelivery && sh.OnTimePickup ? 1m : 0m) / (s.Value.Count.ToDecimal())) : 0m) * 100m)).ToList())
                {
                    Type = SeriesChartType.Line,
                };

            var xySeries = new XYSeries
                {
                    X = series.Select(s => (object)(DateUnit == DateUnit.Week ? s.Key.FormattedShortDate() : string.Format("{0:MMMM yyyy}", s.Key))).ToList(),
                    Y = new[] { ySeriesDelivery, ySeriesPickup, ySeriesOverall },
                };

            chartGenerator.Title = string.Format("{0} - {1} : {2}", vendor.VendorNumber, vendor.Name, serviceMode == ServiceMode.NotApplicable ? AllSeriveModesLabel : serviceMode.FormattedString());
            chartGenerator.XAxisTitle = DateUnit.FormattedString();
            chartGenerator.YAxisTitle = YAxisLabel;
            chartGenerator.DataSource = xySeries;
            chartGenerator.DataBind();
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;
            DateUnit = DateUnit.Month;
            if (StartDate <= DateUtility.SystemEarliestDateTime) StartDate = DateTime.Now.AddYears(-1);
            if (EndDate <= DateUtility.SystemEarliestDateTime) EndDate = DateTime.Now;
        }


        protected void OnCancelClicked(object sender, ImageClickEventArgs e)
        {
            if (Cancel != null)
                Cancel(this, new EventArgs());
        }


        protected void OnDateUnitSelectIndexChanged(object sender, EventArgs e)
        {
            LoadVendorPerformanceSummary(hidVendorId.Value.ToLong());
        }

        protected void OnStartDateTextChanged(object sender, EventArgs e)
        {
            LoadVendorPerformanceSummary(hidVendorId.Value.ToLong());
        }

        protected void OnEndDateTextChanged(object sender, EventArgs e)
        {
            LoadVendorPerformanceSummary(hidVendorId.Value.ToLong());
        }
    }
}