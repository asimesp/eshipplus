﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using LogisticsPlus.Eship.WebApplication.Members.Administration;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls
{
    public partial class ReportConfigurationFinderControl : MemberControlBase, IReportConfigurationFinderView
    {
        public new bool Visible
        {
            get { return pnlReportConfigurationFinderContent.Visible; }
            set
            {
                base.Visible = true;
                pnlReportConfigurationFinderContent.Visible = value;
                pnlReportConfigurationFinderDimScreen.Visible = value;
            }
        }

        public bool OpenForEdit
        {
            get { return hidEditSelected.Value.ToBoolean(); }
        }

        public bool OpenForEditEnabled
        {
            get { return hidOpenForEditEnabled.Value.ToBoolean(); }
            set { hidOpenForEditEnabled.Value = value.ToString(); }
        }

        public bool EnableMultiSelection
        {
            get { return hidReportConfigurationFinderEnableMultiSelection.Value.ToBoolean(); }
            set
            {
                hidReportConfigurationFinderEnableMultiSelection.Value = value.ToString();
                btnSelectAll.Visible = value;
                btnSelectAll2.Visible = value;
                upcseDataUpdate.PreserveRecordSelection = value;
            }
        }

		public bool BypassVisibilityFilter { get { return ActiveUser.HasAccessTo(ViewCode.BypassReportVisibilityFilter); } }

	    public event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
        public event EventHandler<ViewEventArgs<ReportConfiguration>> ItemSelected;
        public event EventHandler<ViewEventArgs<List<ReportConfiguration>>> MultiItemSelected;
        public event EventHandler SelectionCancel;

        public void DisplaySearchResult(List<ReportConfiguration> reportConfigurations)
        {
            var configurations = reportConfigurations
                .Select(c => new ReportConfigurationFinderObj
                {
                    Id = c.Id,
                    Name = c.Name,
                    Description = c.Description,
                    Visibility = c.Visibility,
                    CreatedBy = c.User == null ? string.Empty : c.User.Username
                })
                .ToList();

            litRecordCount.Text = configurations.BuildRecordCount();
            upcseDataUpdate.DataSource = configurations.OrderBy(c => c.Name).ToList();
            upcseDataUpdate.DataBind();
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void Reset()
        {
            hidAreFiltersShowing.Value = true.ToString(); 
            DisplaySearchResult(new List<ReportConfiguration>());
        }


        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems { Columns = GetCurrentRunParameters(true) };
        }

        private void DoSearch(List<ParameterColumn> columns)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<List<ParameterColumn>>(columns));

            var control = lstSearchResults.FindControl("chkSelectAllRecords").ToAltUniformCheckBox();
            if (control != null)
            {
                control.Checked = false;
                control.Visible = EnableMultiSelection;
            }
        }


        private void ProcessItemSelection(long reportConfigurationId)
        {
            if (ItemSelected != null)
                ItemSelected(this, new ViewEventArgs<ReportConfiguration>(new ReportConfiguration(reportConfigurationId)));
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new ReportConfigurationFinderHandler(this).Initialize();

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            hypGoToUserProfile.NavigateUrl = UserProfileView.PageAddress;
            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);

            var columns = profile != null && profile.Columns != null
                            ? profile.Columns
                            : BusinessIntelligenceSearchFields.DefaultReportConfigurations.Select(f => f.ToParameterColumn()).ToList();

            lstFilterParameters.DataSource = columns;
            lstFilterParameters.DataBind();
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            hidAreFiltersShowing.Value = ActiveUser.AlwaysShowFinderParametersOnSearch.ToString();
            upcseDataUpdate.ResetLoadCount();
            DoSearch(GetCurrentRunParameters(false));
        }

        protected void OnSelectClicked(object sender, EventArgs e)
        {
            var button = (Button)sender;
            hidEditSelected.Value = false.ToString();
            ProcessItemSelection(button.Parent.FindControl("hidReportConfigurationId").ToCustomHiddenField().Value.ToLong());
        }

        protected void OnEditSelectClicked(object sender, EventArgs e)
        {
            var button = (Button)sender;
            hidEditSelected.Value = true.ToString();
            ProcessItemSelection(button.Parent.FindControl("hidReportConfigurationId").ToCustomHiddenField().Value.ToLong());
        }

        protected void OnCancelClicked(object sender, EventArgs eventArgs)
        {
            if (SelectionCancel != null)
                SelectionCancel(this, new EventArgs());
        }

        protected void OnSelectAllClicked(object sender, EventArgs e)
        {
            var reportConfigurations = (from item in lstSearchResults.Items
                                        let hidden = item.FindControl("hidReportConfigurationId").ToCustomHiddenField()
                                        let checkBox = item.FindControl("chkSelected").ToAltUniformCheckBox()
                                        where checkBox != null && checkBox.Checked
                                        select new ReportConfiguration(hidden.Value.ToLong()))
                .Where(u => u.KeyLoaded)
                .ToList();

            if (MultiItemSelected != null)
                MultiItemSelected(this, new ViewEventArgs<List<ReportConfiguration>>(reportConfigurations));
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = BusinessIntelligenceSearchFields.ReportConfigurations.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

            Reset();
        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(BusinessIntelligenceSearchFields.ReportConfigurations);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;

            Reset();
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }



        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();
        }

    }
}