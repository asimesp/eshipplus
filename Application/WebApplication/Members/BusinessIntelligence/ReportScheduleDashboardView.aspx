﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="ReportScheduleDashboardView.aspx.cs" EnableEventValidation="false"
    Inherits="LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.ReportScheduleDashboardView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Administration" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
                Report Schedule Dashboard<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>

        <hr class="dark mb10" />

        <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked" CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="fieldgroup right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="ReportSchedules" ShowAutoRefresh="False"
                            OnProcessingError="OnSearchProfilesProcessingError" OnSearchProfileSelected="OnSearchProfilesProfileSelected" />
                    </div>
                </div>
            </div>

            <hr class="dark mb10" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>' OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>

            <hr class="fat" />

            <div class="rowgroup">
                <div class="row mb10">
                    <div class="left">
                        <div class="fieldgroup">
                            <asp:Button ID="btnSearch" CssClass="mr10" runat="server" Text="search" OnClick="OnSearchClicked" CausesValidation="False" />
                        </div>

                        <div class="fieldgroup right">
                            <asp:Button ID="btnAddToScheduleQueue" CssClass="mr10" runat="server" Text="Add To Schedule Queue" OnClick="OnAddToScheduleQueueClicked"
                                CausesValidation="False" />
                        </div>
                    </div>
                    <div class="right">
                        <div class="fieldgroup">
                            <label>Sort By:</label>
                            <asp:DropDownList runat="server" ID="ddlSortBy" DataTextField="DisplayName" DataValueField="Name"
                                OnSelectedIndexChanged="OnSortBySelectedIndexChanged" AutoPostBack="True" />
                        </div>
                        <div class="fieldgroup">
                            <asp:RadioButton runat="server" ID="rbAsc" Text="ASC" GroupName="rbSortBy" AutoPostBack="True"
                                ToolTip="Ascending" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                        </div>
                        <div class="fieldgroup">
                            <asp:RadioButton runat="server" ID="rbDesc" Text="DESC" GroupName="rbSortBy" Checked="True"
                                ToolTip="Descending" AutoPostBack="True" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                        </div>
                    </div>
                </div>
            </div>

        </asp:Panel>

        <script type="text/javascript">
            function SetCheckAllReportSchedules(control) {
                $("#reportScheduleListingTable input:checkbox[id*='chkSelected']").each(function () {
                    this.checked = control.checked && this.disabled == false;
                    SetAltUniformCheckBoxClickedStatus(this);
                });

                // necessary to keep frozen header and header checkbox states the same
                $('[id*=' + control.id + ']').each(function () {
                    this.checked = control.checked;
                    SetAltUniformCheckBoxClickedStatus(this);
                });
            }
        </script>

        <asp:UpdatePanel runat="server" ID="upDataUpdate">
            <ContentTemplate>
                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheReportScheduleListingTable" TableId="reportScheduleListingTable" HeaderZIndex="2" />

                <div class="rowgroup">
                    <table id="reportScheduleListingTable" class="line2 pl2">
                        <tr>
                            <th style="width: 3%;">
                                <eShip:AltUniformCheckBox ID="chkSelectAllRecords" runat="server" OnClientSideClicked="javascript:SetCheckAllReportSchedules(this);" />
                            </th>
                            <th style="width: 28%">
                                <asp:LinkButton runat='server' ID="lbtnSortConfigurationName" CssClass="link_nounderline blue" CausesValidation="False" Text="Report Schedule Configuration" OnCommand="OnSortData"/>
                            </th>
                            <th style="width: 13%">User
                            </th>
                            <th style="width: 5%">Interval
                            </th>
                            <th style="width: 2%">
                                 <asp:LinkButton runat='server' ID="lbtnSortTime" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData" Text="Time" />
                            </th>
                            <th style="width: 16%">
                                <asp:LinkButton runat='server' ID="lbtnSortStartDate" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData" Text="Start Date"/>
                            </th>
                            <th style="width: 16%">
                                <asp:LinkButton runat='server' ID="lbtnSortEndDate" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData" Text="End Date"/>
                            </th>
                            <th style="width: 17%">
                                <asp:LinkButton runat='server' ID="lbtnSortLastRun" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData" Text="Last Run"/>
                            </th>
                        </tr>
                        <asp:Repeater runat="server" ID="rptReportSchedules">
                            <ItemTemplate>
                                <tr>
                                    <td class="top">
                                        <eShip:CustomHiddenField ID="hidScheduleId" runat="server" Value='<%# Eval("Id") %>'/>
                                        <eShip:AltUniformCheckBox runat="server" ID="chkSelected" OnClientSideClicked=" SetCheckAllCheckBoxUnChecked(this, 'reportScheduleListingTable', 'chkSelectAllRecords'); " />
                                    </td>
                                    <td>
                                        <%# Eval("ReportConfiguration.Name") %>
                                        <%# ActiveUser.HasAccessTo(ViewCode.ReportScheduler)
                                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Report Schedule Record'><img src='{3}' alt='' width='16' align='absmiddle'/></a>", 
                                                    ResolveUrl(ReportSchedulerView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    Eval("Id").GetString().UrlTextEncrypt(),
                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                : string.Empty  %>
                                    </td>
                                    <td>
                                        <%# Eval("User.Username") %>
                                        <%# ActiveUser.HasAccessTo(ViewCode.User)
                                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To User Record'><img src='{3}' alt='' width='16' align='absmiddle'/></a>", 
                                                    ResolveUrl(UserView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    Eval("UserId").GetString().UrlTextEncrypt(),
                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                : string.Empty  %>
                                    </td>
                                    <td>
                                        <%# Eval("ScheduleInterval") %>
                                    </td>
                                    <td>
                                        <%# Eval("Time") %>
                                    </td>
                                    <td>
                                        <%# Eval("Start") %>
                                    </td>
                                    <td>
                                        <%# Eval("End") %>
                                    </td>
                                    <td>
                                        <%# Eval("LastRun") %>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lbtnSortConfigurationName" />
                <asp:PostBackTrigger ControlID="lbtnSortStartDate" />
                <asp:PostBackTrigger ControlID="lbtnSortEndDate" />
                <asp:PostBackTrigger ControlID="lbtnSortLastRun" />
                <asp:PostBackTrigger ControlID="lbtnSortTime" />
            </Triggers>
        </asp:UpdatePanel>
    </div>

    <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
        Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" />
    <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
</asp:Content>
