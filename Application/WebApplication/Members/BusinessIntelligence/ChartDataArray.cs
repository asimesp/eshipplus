﻿using System.Collections.Generic;
using System.Linq;
using System.Web.UI.DataVisualization.Charting;

namespace LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence
{
    public class ChartDataArray
    {
        private const string DataWithLabelTemplate = @"{{   data: [{0}],
                                                            label: ""{1}"", 
                                                            points: {{ show: {2} }},
                                                            lines: {{ show: {3} }},
                                                            bars: {{
                                                                show: {4},
                                                                stacked: {5},
                                                                horizontal: {6},
                                                                grouped: true
                                                                }}
                                                       }},";
        private const string DataJsonTemplate = "[{0},{1}],";
        
        private bool ChartIsBarOrColumn()
        {
            return ChartIsStacked() || Type == SeriesChartType.Bar || Type == SeriesChartType.Column;
        }

        private bool ChartIsStacked()
        {
            return Type == SeriesChartType.StackedBar || Type == SeriesChartType.StackedColumn;
        }

        private bool ChartIsBar()
        {
            return Type == SeriesChartType.Bar || Type == SeriesChartType.StackedBar;
        }


        public List<ChartDataArrayCoordinates> Data;
        public string Label;
        public SeriesChartType Type;

        public string ReturnJsonDataString()
        {
            var data = Data.Aggregate(string.Empty, (current, point) => current + string.Format(DataJsonTemplate, point.X, point.Y));

            return string.Format(DataWithLabelTemplate, data, Label, (Type == SeriesChartType.Point).ToString().ToLower(), 
                                                                     (Type == SeriesChartType.Line).ToString().ToLower(),
                                                                     ChartIsBarOrColumn().ToString().ToLower(),
                                                                     ChartIsStacked().ToString().ToLower(),
                                                                     ChartIsBar().ToString().ToLower());
        }
    }
}