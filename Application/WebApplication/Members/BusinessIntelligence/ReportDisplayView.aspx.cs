﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.UI;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;
using QlikAuthNet;

namespace LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence
{
	public partial class ReportDisplayView : MemberPageBase
	{
		public override ViewCode PageCode
		{
			get { return ViewCode.ReportRun; }
		}

		public override string SetPageIconImage
		{
			set {  }
		}

		public static string PageAddress { get { return "~/Members/BusinessIntelligence/ReportDisplayView.aspx"; } }

		public static string PageAddressWithId(long id)
		{
			return string.Format("{0}?Id={1}", PageAddress, id);
		}


		private void LoadData()
		{
			if (string.IsNullOrEmpty(ActiveUser.QlikUserId) || string.IsNullOrEmpty(ActiveUser.QlikUserDirectory))
			{
				var data2 = new List<object>
					{
						new
							{
								Hightlight = true,
								Index = 1,
								Name = "No Assigned Qlik User Id or User Directory!",
								Link = string.Empty
							}
					};

				rptSheetTags.DataSource = data2;
				rptSheetTags.DataBind();

				return;
			}

			var req = new Ticket
				{
					UserDirectory = ActiveUser.QlikUserDirectory,
					UserId = ActiveUser.QlikUserId,
					ProxyRestUri = WebApplicationSettings.QlikProxyRestUri,
					TargetId = string.Empty,
					CertificateName = WebApplicationSettings.QlikClientCertificateName,
					CertificateLocation = StoreLocation.LocalMachine,
				};

			var id = Request.QueryString["Id"].ToLong();
			var config = new ReportConfiguration(id);
			var cnt = config.QlikSheets.Count;
			var data = config
				.QlikSheets
				.OrderBy(i => i.DisplayOrder)
				.Select(i => new
					{
						Highlight = i.DisplayOrder == 0,
						Index = cnt--,
						i.Name,
						Link = string.Format("{0}&{1}&identity={2}", i.Link, req.TicketRequest(), ActiveUser.Username)
					})
				.ToList();

            var rcUpdateHandler = new ReportConfigurationUpdateHandler();

            Exception ex;
            rcUpdateHandler.GenerateRunAuditLog(id, ActiveUser, out ex);

            if(ex != null) ErrorLogger.LogError(ex, Context);

			rptSheets.DataSource = data;
			rptSheets.DataBind();

			rptSheetTags.DataSource = data;
			rptSheetTags.DataBind();
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (IsPostBack) return;
			LoadData();
		}
	}
}