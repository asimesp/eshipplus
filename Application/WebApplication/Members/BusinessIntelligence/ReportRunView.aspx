﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="ReportRunView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.ReportRunView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence" %>
<%@ Register TagPrefix="pc" TagName="ChartGenerator" Src="~/Members/BusinessIntelligence/Controls/ChartGenerator.ascx" %>
<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="false"
        ShowEdit="false" ShowDelete="false" ShowSave="false" ShowFind="false" ShowMore="false"
        ShowExport="false" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Report Run</h3>
        </div>
        <hr class="dark mb10" />
        <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
        
        <asp:Panel runat="server" ID="pnlReportParameters" DefaultButton="btnReportRun">
            <div class="rowgroup">
                <div class="row">
                    <div class="fieldgroup">
                        <label class="w200 text-right upper blue pb5">Configuration Name:</label>
                    </div>
                    <div class="fieldgroup">
                        <label>
                            <asp:Literal runat="server" ID="litConfigurationName" />
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="fieldgroup">
                        <label class="w200 text-right upper blue pb5">Configuration Description:</label>
                    </div>
                    <div class="fieldgroup">
                        <label>
                            <asp:Literal runat="server" ID="litConfigurationDescription" />
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="fieldgroup">
                        <label class="w200 text-right upper blue pb5">Export Format: </label>
                    </div>
                    <div class="fieldgroup">
                        <asp:DropDownList runat="server" ID="ddlExtension" CssClass="w100" DataTextField="Text"
                            DataValueField="Value" />
                    </div>
                </div>
                <div class="row">
                    <div class="fieldgroup">
                        <asp:Button runat="server" ID="btnReportNew" Text="Change Report" ToolTip="Change Report"
                            OnClick="OnReportNewClicked" />
                    </div>
                </div>
                <div class="row mb10 pt20">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                </div>
                <hr class="dark" />
                <div class="row">
                    <div class="fieldgroup">
                        <table>
                            <asp:ListView runat="server" ID="lstReportConfigurationRequiredParameters" ItemPlaceholderID="itemPlaceHolder"
                                OnItemDataBound="OnRequiredParameterItemDataBound">
                                <LayoutTemplate>
                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>' />
                                </ItemTemplate>
                            </asp:ListView>
                            <asp:ListView runat="server" ID="lstReportConfigurationParameters" ItemPlaceholderID="itemPlaceHolder"
                                OnItemDataBound="OnParameterItemDataBound">
                                <LayoutTemplate>
                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                                        OnRemoveParameter="OnParameterRemove" />
                                </ItemTemplate>
                            </asp:ListView>
                            <tr style='<%= pnlCustomerGroups.Visible || pnlVendorGroups.Visible ? string.Empty: "display: none;" %>'>
                                <td>&nbsp;</td>
                            </tr>
                            <asp:Panel runat="server" ID="pnlCustomerGroups" Visible="False">
                                <tr>
                                    <td></td>
                                    <td>
                                        <label class="upper">Customer Group</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="ddlCustomGroupOperator" Enabled="False" DataTextField="Text" DataValueField="Value" CssClass="w150" />
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlCustomerGroup" runat="server" CssClass="w130" DataValueField="Value"
                                            DataTextField="Text" />
                                    </td>
                                </tr>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="pnlVendorGroups" Visible="False">
                                <tr>
                                    <td></td>
                                    <td>
                                        <label class="upper">Vendor Group</label>
                                    </td>
                                    <td>
                                        <asp:DropDownList runat="server" ID="ddlVendorGroupOperator" Enabled="False" DataTextField="Text" DataValueField="Value" CssClass="w150" />
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlVendorGroup" runat="server" CssClass="w130" DataValueField="Value"
                                            DataTextField="Text" />
                                    </td>
                                </tr>
                            </asp:Panel>
                        </table>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <hr class="fat mb10" />
        <div class="row mb10">
            <div class="fieldgroup">
                <asp:Button runat="server" ID="btnReportRun" Text="Run Report" ToolTip="Change Report" OnClick="OnReportRunClicked" CssClass="mr10" />
                <asp:Button runat="server" ID="btnReportExport" Text="Export Report" ToolTip="Change Report"
                    OnClick="OnExportClicked" />
            </div>
        </div>
        <asp:Panel runat="server" ID="pnlReportResults">
            <asp:Panel runat="server" ID="pnlReportSummaries" EnableViewState="False">
                <div class="rowgroup">
                    <h5>Summary</h5>
                    <table>
                        <asp:Repeater runat="server" ID="rptSummaries" EnableViewState="False">
                            <ItemTemplate>
                                <tr>
                                    <td class="text-right p_m0">
                                        <label class="upper"><%# Eval("Key") %>: </label>
                                    </td>
                                    <td class="text-right p_m0">
                                        <label><%# Eval("Value") %></label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </div>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlReportData" Visible="False" EnableViewState="False">
                <div class="rowgroup">
                    
                    <asp:Repeater ID="rptData" runat="server" EnableViewState="False" OnItemDataBound="OnDataItemDataBound">
                        <ItemTemplate>
                            
                            <asp:Repeater runat="server" ID="rptCharts" EnableViewState="False" 
                                OnItemDataBound="OnChartsItemDataBound" >
                                <ItemTemplate>
                                    <h5><%# Eval("Chart.Title") %></h5>
                                    <table class="contain">
                                        <tr>
                                            <td class="text-center">
                                                <pc:ChartGenerator ID="reportChart" runat="server" EnableViewState="False" />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:Repeater>

                            <div class="reportTableScroll mb30" id="pivotDataScrollSection">
                                <h5><%# Eval("HideOriginalTable").ToBoolean() ? string.Empty : Eval("Data.TableName") %></h5>
                                <table class="stripe" id="pivotDataTable">
                                    <tr>
                                        <asp:Repeater runat="server" ID="rptTableColumns" EnableViewState="False">
                                            <ItemTemplate>
                                                <th class='<%# Eval("Align").GetString() == "center" ? "text-center" : Eval("Align").GetString() == "right" ?  "text-right" : "text-left" %>'>
                                                    <%# Eval("Name") %>
                                                </th>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tr>
                                    <asp:Repeater runat="server" ID="rptTableRows" OnItemDataBound="OnRowsItemDataBound" EnableViewState="False">
                                        <ItemTemplate>
                                            <tr>
                                                <asp:Repeater runat="server" ID="rptRowColumns">
                                                    <ItemTemplate>
                                                        <td class='<%# Eval("Align").GetString() == "center" ? "text-center" : Eval("Align").GetString() == "right" ?  "text-right" : "text-left" %>'>
                                                            <%# Eval("Value") %>
                                                        </td>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlReportConfigurations" Visible="false" CssClass="popup popupControlOver" DefaultButton="btnFilter">
            <div class="popheader">
                <h4>Report Configurations</h4>
            </div>
            <div class="row pt20 pb20 pl10">
                <div class="fieldgroup">
                    <label class="upper blue">Filter:</label>
                    <eShip:CustomTextBox runat="server" ID="txtFilter" CssClass="w200" />
                </div>
                <div class="fieldgroup ml10">
                    <asp:Button runat="server" ID="btnFilter" Text="Filter Results" OnClick="OnFilterResultsClick" />
                </div>
                <div class="fieldgroup right pr10">
                    <asp:Button runat="server" ID="btnNoReports" Text="Dashboard" OnClick="OnNoReportsClicked" CausesValidation="False" />
                </div>

            </div>
            <div class="row">
                <hr class="w100p ml-inherit dark p_m0" />
                <div class="finderScroll">
                        <asp:Repeater runat="server" ID="rptReportConfigurations">
                            <HeaderTemplate>
                                <ul class="twocol_list pl10 pt10">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <eShip:CustomHiddenField runat="server" ID="hidReportConfigurationId" Value='<%# Eval("Id") %>' />
                                <li class="pb10">
                                    <div class="fieldgroup shadow w90p pb10 pt10 pl10 pr10 h150" style="overflow: auto">

                                        <label class="wlabel">
                                            <asp:LinkButton ID="LinkButton1" runat="server" Text='<%# Eval("Name") %>' OnClick="OnSelectReportConfigurationClicked" CssClass="blue" Visible='<%# !Eval("QlikConfiguration").ToBoolean() %>' />

                                            <%# Eval("QlikConfiguration").ToBoolean()
                                                ? string.Format("<a href='{1}' class='blue' target='_blank' title='Go To Qlik Report Display'>{0}</a> <a href='{1}' class='blue' target='_blank' title='Go To Qlik Report Display'><img src={2} width='16' class='middle'/></a>", 
                                                                        Eval("Name"),
                                                                        ResolveUrl(ReportDisplayView.PageAddressWithId(Eval("Id").ToLong())),
                                                                        ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                : string.Empty %>
                                            
                                        </label>

                                        <div class="clearfix"></div>
                                        <div class="pl10 mt10">
                                            <label class="pl10"><%# Eval("Description") %></label>
                                        </div>
                                    </div>
                                </li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ul>
                            </FooterTemplate>
                        </asp:Repeater>
                </div>
            </div>

            <asp:Panel runat="server" ID="pnlNoReports" Visible="False">
                <div class="row">
                    <div class="fieldgroup">
                        <label class="pt20 pl20 pb20">There are no reports available to run.</label>
                    </div>
                </div>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlParameterAdd" Visible="false" CssClass="popup popupControlOverW500">
            <div class="popheader">
                <h4>Add Report Parameter</h4>
            </div>
            <table class="poptable">
                <tr>
                    <td colspan="2" class="p_m0">
                        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheParameterSelectorPermissionsTable" TableId="reportParameterSelectorTable" ScrollerContainerId="reportParameterSelectorScrollSection"
                            ScrollOffsetControlId="pnlParameterAdd" IgnoreHeaderBackgroundFill="True" HeaderZIndex="1050" />
                        <div class="finderScroll" id="reportParameterSelectorScrollSection">
                            <table id="reportParameterSelectorTable" class="stripe sm_chk">
                                <tr>
                                    <th style="width: 5%;">
                                        <eShip:AltUniformCheckBox ID="chkReportParameterSelectorSelectAll" runat="server" OnClientSideClicked="javascript:SetSelectedCheckBoxesStatus(this, 'reportParameterSelectorTable');" />
                                    </th>
                                    <th style="width: 95%;" class="text-left">Name
                                    </th>
                                </tr>
                                <asp:ListView runat="server" ID="lstParameters" ItemPlaceholderID="itemPlaceHolder">
                                    <LayoutTemplate>
                                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <eShip:AltUniformCheckBox ID="chkSelection" runat="server" OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'reportParameterSelectorTable', 'chkReportParameterSelectorSelectAll');" />
                                            </td>
                                            <td class="text-left">
                                                <eShip:CustomHiddenField runat="server" ID="hidParameterDataType" Value='<%# Eval("DataType") %>' />
                                                <eShip:CustomHiddenField runat="server" ID="hidParameterReadOnly" Value='<%# Eval("ReadOnly") %>' />
                                                <eShip:CustomHiddenField runat="server" ID="hidParameterOperator" Value='<%# Eval("Operator") %>' />
                                                <asp:Literal runat="server" ID="litParameterName" Text='<%# Eval("Name") %>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td class="text-left">
                        <asp:Button ID="btnParameterAddDone" Text="Done" OnClick="OnParameterAddDoneClicked" runat="server" CausesValidation="false" />
                        <asp:Button ID="btnParameterAddClose" Text="Cancel" OnClick="OnParameterAddCloseClicked" runat="server" CausesValidation="false" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
    <eShip:CustomHiddenField runat="server" ID="hidReportConfigurationId" />
</asp:Content>
