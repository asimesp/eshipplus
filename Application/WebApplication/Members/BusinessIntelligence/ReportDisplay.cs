﻿using System.Collections.Generic;
using System.Data;
using LogisticsPlus.Eship.Core.BusinessIntelligence;

namespace LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence
{
    public class ReportDisplay
    {
        public List<ChartConfiguration> Charts;
        public DataTable Data { get; set; }
        public Pivot Pivot { get; set; }
        public bool HideOriginalTable { get; set; }
    }
}