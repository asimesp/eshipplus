﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence
{
    public partial class CustomerGroupView : MemberPageBase, ICustomerGroupView
    {
        private const string CustomersHeader = "Customers";

        private const string SaveFlag = "S";

        public static string PageAddress
        {
            get { return "~/Members/BusinessIntelligence/CustomerGroupView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.CustomerGroup; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.ReportsBlue; }
        }

        public event EventHandler<ViewEventArgs<CustomerGroup>> Save;
        public event EventHandler<ViewEventArgs<CustomerGroup>> Delete;
        public event EventHandler<ViewEventArgs<CustomerGroup>> Lock;
        public event EventHandler<ViewEventArgs<CustomerGroup>> UnLock;
        public event EventHandler<ViewEventArgs<CustomerGroup>> LoadAuditLog;

        public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs)
        {
            auditLogs.Load(logs);
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == SaveFlag)
            {
                // unlock and return to read-only
                if (UnLock != null)
                    UnLock(this,
                           new ViewEventArgs<CustomerGroup>(new CustomerGroup(hidCustomerGroupId.Value.ToLong(), false)));
                SetEditStatus(false);
                memberToolBar.ShowUnlock = false;
            }

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Button = MessageButton.Ok;
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;

            litErrorMessages.Text = messages.HasErrors()
                                        ? string.Join(WebApplicationConstants.HtmlBreak,
                                                      messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(
                                                          m => m.Type).
                                                          Select(m => m.Message).ToArray())
                                        : string.Empty;
        }

        public void FailedLock(Lock @lock)
        {
            memberToolBar.ShowNew = false;
            memberToolBar.ShowSave = false;
            memberToolBar.ShowDelete = false;
            memberToolBar.ShowUnlock = false;

            SetEditStatus(false);

            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        private void SetEditStatus(bool enabled)
        {
            pnlDetails.Enabled = enabled;
            pnlCustomers.Enabled = enabled;

            memberToolBar.EnableSave = enabled;
            memberToolBar.EnableImport = enabled;

            litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;
        }

        public void Set(CustomerGroup @group)
        {
            LoadCustomerGroup(@group);
            SetEditStatus(!@group.IsNew);
        }


        private void UpdateCustomerGroupMaps(CustomerGroup group)
        {
            var maps = lstCustomers.Items
                .Select(item => new CustomerGroupMap
                {
                    Customer =
                        new Customer(
                        item.FindControl("hidCustomerId").ToCustomHiddenField().Value.ToLong()),
                    CustomerGroup = group,
                    TenantId = ActiveUser.TenantId
                })
                .ToList();

            group.CustomerGroupMaps = maps;
        }

        private void LoadCustomerGroup(CustomerGroup @group)
        {
            hidCustomerGroupId.Value = @group.Id.ToString();

            txtGroupName.Text = @group.Name;
            txtDescription.Text = @group.Description;

            var customers = @group.IsNew ? new List<Customer>() : @group.RetrieveGroupCustomers();
            lstCustomers.DataSource = customers.OrderBy(c => c.Name).ThenBy(c => c.CustomerNumber);
            lstCustomers.DataBind();

            tabCustomers.HeaderText = @group.IsNew
                                          ? new List<Customer>().BuildTabCount(CustomersHeader)
                                          : @group.RetrieveGroupCustomers().BuildTabCount(CustomersHeader);

            memberToolBar.ShowUnlock = group.HasUserLock(ActiveUser, group.Id);
        }


        private void ProcessTransferredRequest(long groupId)
        {
            if (groupId != default(long))
            {
                var customerGroup = new CustomerGroup(groupId);
                LoadCustomerGroup(customerGroup);

                if (LoadAuditLog != null)
                    LoadAuditLog(this, new ViewEventArgs<CustomerGroup>(customerGroup));
            }
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            new CustomerGroupHandler(this).Initialize();

            memberToolBar.ShowSave = Access.Modify;
            memberToolBar.ShowDelete = Access.Remove;
            memberToolBar.ShowNew = Access.Modify;
            memberToolBar.ShowEdit = Access.Modify;
            memberToolBar.ShowImport = Access.Modify;

            customerGroupFinder.OpenForEditEnabled = Access.Modify;

            if (IsPostBack) return;
            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.CustomerGroupsImportTemplate });

            if (Session[WebApplicationConstants.TransferCustomerGroupId] != null)
            {
                ProcessTransferredRequest(Session[WebApplicationConstants.TransferCustomerGroupId].ToLong());
                Session[WebApplicationConstants.TransferCustomerGroupId] = null;
            }

            SetEditStatus(false);
        }

        protected void OnNewClicked(object sender, EventArgs e)
        {
            litErrorMessages.Text = string.Empty;

            DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
            LoadCustomerGroup(new CustomerGroup());
            SetEditStatus(true);
        }

        protected void OnEditClicked(object sender, EventArgs e)
        {
            var group = new CustomerGroup(hidCustomerGroupId.Value.ToLong());

            if (Lock == null || @group.IsNew) return;

            SetEditStatus(true);
            memberToolBar.ShowUnlock = true;
            Lock(this, new ViewEventArgs<CustomerGroup>(@group));
            LoadCustomerGroup(@group);
        }

        protected void OnUnlockClicked(object sender, EventArgs e)
        {
            var group = new CustomerGroup(hidCustomerGroupId.Value.ToLong(), false);
            if (UnLock != null && !group.IsNew)
            {
                SetEditStatus(false);
                memberToolBar.ShowUnlock = false;
                UnLock(this, new ViewEventArgs<CustomerGroup>(group));
            }
        }

        protected void OnSaveClicked(object sender, EventArgs e)
        {
            var groupId = hidCustomerGroupId.Value.ToLong();

            var customerGroup = new CustomerGroup(groupId, groupId != default(long))
            {
                Name = txtGroupName.Text,
                Description = txtDescription.Text
            };
            UpdateCustomerGroupMaps(customerGroup);

            if (customerGroup.IsNew)
                customerGroup.TenantId = ActiveUser.TenantId;

            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<CustomerGroup>(customerGroup));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<CustomerGroup>(customerGroup));
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var customerGroup = new CustomerGroup(hidCustomerGroupId.Value.ToLong(), false);

            if (Delete != null)
                Delete(this, new ViewEventArgs<CustomerGroup>(customerGroup));

            customerGroupFinder.Reset();
            SetEditStatus(false);
        }

        protected void OnFindClicked(object sender, EventArgs e)
        {
            customerGroupFinder.Visible = true;
        }

        protected void OnImportClicked(object sender, EventArgs e)
        {
            fileUploader.Title = "CUSTOMER GROUPS IMPORT";
            fileUploader.Instructions = @"**Please refer to user guide for import file format";
            fileUploader.Visible = true;
        }


        protected void OnAddCustomerClicked(object sender, EventArgs e)
        {
            customerFinder.Visible = true;
        }

        protected void OnClearCustomersClicked(object sender, EventArgs e)
        {
            lstCustomers.DataSource = new List<Permission>();
            lstCustomers.DataBind();
            tabCustomers.HeaderText = CustomersHeader;
            athtuTabUpdater.SetForUpdate(tabCustomers.ClientID, CustomersHeader);
        }

        protected void OnRemoveCustomerClicked(object sender, ImageClickEventArgs e)
        {
            var button = (ImageButton)sender;

            var customers = lstCustomers.Items
                .Select(item => new
                {
                    Id = item.FindControl("hidCustomerId").ToCustomHiddenField().Value.ToLong(),
                    Name = item.FindControl("litCustomerName").ToLiteral().Text,
                    CustomerNumber = item.FindControl("lblCustomerNumber").ToLabel().Text,
                    Active = item.FindControl("hidActive").ToCustomHiddenField().Value.ToBoolean(),
                })
                .ToList();

            customers.RemoveAt(button.FindControl("hidCustomerIndex").ToCustomHiddenField().Value.ToInt());

            lstCustomers.DataSource = customers;
            lstCustomers.DataBind();

            tabCustomers.HeaderText = customers.BuildTabCount(CustomersHeader);
            athtuTabUpdater.SetForUpdate(tabCustomers.ClientID, customers.BuildTabCount(CustomersHeader));
        }

        protected void OnCustomerFinderMultiItemSelected(object sender, ViewEventArgs<List<Customer>> e)
        {
            var customers = lstCustomers.Items
                .Select(c => new
                {
                    Id = c.FindControl("hidCustomerId").ToCustomHiddenField().Value,
                    Name = c.FindControl("litCustomerName").ToLiteral().Text,
                    CustomerNumber = c.FindControl("lblCustomerNumber").ToLabel().Text,
                    Active = c.FindControl("hidActive").ToCustomHiddenField().Value.ToBoolean()
                }).ToList();


            var newCustomers = e.Argument
                .Select(c => new { Id = c.Id.ToString(), c.Name, c.CustomerNumber, c.Active })
                .Where(c => !customers.Select(s => s.Id).Contains(c.Id));
            customers.AddRange(newCustomers);

            lstCustomers.DataSource = customers.OrderBy(c => c.Name).ThenBy(c => c.CustomerNumber);
            lstCustomers.DataBind();

            tabCustomers.HeaderText = customers.BuildTabCount(CustomersHeader);

            customerFinder.Visible = false;
        }

        protected void OnCustomerFinderSelectionCancelled(object sender, EventArgs eventArgs)
        {
            customerFinder.Visible = false;
        }

        protected void OnCustomerGroupFinderItemSelected(object sender, ViewEventArgs<CustomerGroup> e)
        {
            litErrorMessages.Text = string.Empty;

            if (hidCustomerGroupId.Value.ToLong() != default(long))
            {
                var oldGroup = new CustomerGroup(hidCustomerGroupId.Value.ToLong(), false);
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<CustomerGroup>(oldGroup));
            }

            var @group = e.Argument;

            LoadCustomerGroup(@group);

            customerGroupFinder.Visible = false;

            if (customerGroupFinder.OpenForEdit && Lock != null)
            {
                SetEditStatus(true);
                memberToolBar.ShowUnlock = true;
                Lock(this, new ViewEventArgs<CustomerGroup>(@group));
            }
            else
            {
                SetEditStatus(false);
            }

            customerFinder.Reset();

            if (LoadAuditLog != null) LoadAuditLog(this, new ViewEventArgs<CustomerGroup>(@group));
        }

        protected void OnCustomerGroupFinderSelectionCancelled(object sender, EventArgs eventArgs)
        {
            customerGroupFinder.Visible = false;
        }


        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;
            messageBox.Visible = true;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 2;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var customerSearch = new CustomerSearch();

            msgs.AddRange(chks
                .Where(i => customerSearch.FetchCustomerByNumber(i.Line[0], ActiveUser.TenantId) == null)
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedCodeErrMsg, new Customer().EntityName(), i.Index))
                .ToList());
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var uploadedCustomers = lines
                .Select(line => customerSearch.FetchCustomerByNumber(line[0], ActiveUser.TenantId))
                .ToList();

            var customers = lstCustomers.Items
                .Select(c => new
                {
                    Id = (c.FindControl("hidCustomerId")).ToCustomHiddenField().Value,
                    Name = (c.FindControl("litCustomerName")).ToLiteral().Text,
                    CustomerNumber = (c.FindControl("lblCustomerNumber")).ToLabel().Text,
                    Active = (c.FindControl("hidActive")).ToCustomHiddenField().Value.ToBoolean()
                }).ToList();

            // add new customers
            customers.AddRange(uploadedCustomers
                                .Select(c => new { Id = c.Id.ToString(), c.Name, c.CustomerNumber, c.Active })
                                .Where(c => !customers.Select(s => s.Id).Contains(c.Id))
                                .ToList());

            lstCustomers.DataSource = customers.OrderBy(c => c.Name).ThenBy(c => c.CustomerNumber);
            lstCustomers.DataBind();
            tabCustomers.HeaderText = customers.BuildTabCount(CustomersHeader);

            fileUploader.Visible = false;
        }
    }
}

