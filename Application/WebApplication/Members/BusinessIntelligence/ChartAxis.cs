﻿namespace LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence
{
    public enum ChartAxis
    {
        //Axis types. DO NOT RE-ORDER
        XAxis = 0,
        YAxis
    }
}