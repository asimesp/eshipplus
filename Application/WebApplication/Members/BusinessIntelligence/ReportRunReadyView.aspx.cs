﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Validation.BusinessIntelligence;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence
{
    public partial class ReportRunReadyView : MemberPageBase
    {
        public static string PageAddress { get { return "~/Members/BusinessIntelligence/ReportRunReadyView.aspx"; } }

        public override string PageName
        {
            get { return "Ready Report Dashboard"; }
        }

        public override ViewCode PageCode { get { return ViewCode.ReportRun; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.ReportsBlue; } }


        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        private void LoadReadyReports()
        {
            var records = new BackgroundReportRunSearch().FetchBackgroundReportRunRecordsForUser(ActiveUser.Id);

            rptReportSchedules.DataSource = records
                .OrderByDescending(r => r.ExpirationDate)
                .ThenBy(r => r.DateCreated)
                .Select(r => new
                {
                    r.Id,
                    r.ReportName,
                    r.DateCreated,
                    r.CompletedReportFileName,
                    IsComplete = r.ExpirationDate > DateUtility.SystemEarliestDateTime
                });
            rptReportSchedules.DataBind();
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;

            LoadReadyReports();
        }



        protected void OnRefreshClicked(object sender, EventArgs e)
        {
            LoadReadyReports();
        }

        protected void OnDownloadReportClicked(object sender, EventArgs e)
        {
            var ibtnDownload = (ImageButton)sender;
            var fileName = ibtnDownload.Parent.FindControl("hidFileName").ToCustomHiddenField().Value;
            var physicalPath = Server.MapPath(string.Format("{0}{1}", WebApplicationSettings.BackgroundReportRunFolder, fileName));

            var extension = fileName.Split('.').Last();
            var reportName = string.Format("{0}-{1:yyyyMMdd_hhmmss}.{2}", ibtnDownload.Parent.FindControl("litReportName").ToLiteral().Text, DateTime.Now, extension);

            var file = new FileInfo(physicalPath);
            Response.Export(file, string.Format("{0}", reportName));
        }

        protected void OnDeleteReportClicked(object sender, EventArgs e)
        {
            var manager = new BackgroundReportRunRecordManager();
            var ibtnDelete = (ImageButton)sender;

            var id = ibtnDelete.Parent.FindControl("hidId").ToCustomHiddenField().Value.ToLong();

            var record = new BackgroundReportRunRecord(id);
            var fileName = record.CompletedReportFileName;

            Exception ex;
            manager.DeleteBackgroundReport(record, ActiveUser, out ex);

            if (ex != null)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Failed to delete background report record.") });
                ErrorLogger.LogError(ex, Context);
            }
            else
            {
                var physicalPath = Server.MapPath(string.Format("{0}{1}", WebApplicationSettings.BackgroundReportRunFolder, fileName));

                var file = new FileInfo(physicalPath);
                try
                {
                    file.Delete();
                }
                catch (Exception exc)
                {
                    ErrorLogger.LogError(exc, Context);
                }
            }

            LoadReadyReports();
        }
    }
}