﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence
{
    public partial class VendorPerformanceSummaryView : MemberPageBase, IVendorPerformanceSummaryDisplayView
    {
        private readonly List<string> _exportHeader = new List<string>
		{
		                                		"Vendor Name", "Vendor Number", "Vendor SCAC", "Mode", "Total Loads", "Considered",
		                                		"Overall On-Time %", "Pickup On-Time %", "Delivery On-Time %", "Accepted Tender %",
		                                		"Mileage"
		                                	};

        private const string AmountPaidUsd = "Total Cost (USD)";
        protected const string AllModes = "All Modes";

        public static string PageAddress { get { return "~/Members/BusinessIntelligence/VendorPerformanceSummaryView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.VendorPerformanceSummary; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.ReportsBlue; } }


        public Dictionary<int, string> DateUnits
        {
            set { vpscVendorPerformance.DateUnits = value; }
        }


        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<string>> CustomerSearch;
        public event EventHandler<ViewEventArgs<string>> VendorSearch;


        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;

            litErrorMessages.Text = messages.HasErrors()
                                        ? string.Join(WebApplicationConstants.HtmlBreak,
                                                      messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
                                                        Select(m => m.Message).ToArray())
                                        : string.Empty;
        }

        public void DisplayCustomer(Customer customer)
        {
            txtCustomerNumber.Text = customer == null ? string.Empty : customer.CustomerNumber;
            txtCustomerName.Text = customer == null ? string.Empty : customer.Name;
            hidCustomerId.Value = customer == null ? default(long).ToString() : customer.Id.ToString();
        }

        public void DisplayVendor(Vendor vendor)
        {
            txtVendorName.Text = vendor == null ? string.Empty : vendor.Name;
            txtVendorNumber.Text = vendor == null ? string.Empty : vendor.VendorNumber;
            hidVendorId.Value = vendor == null ? default(long).ToString() : vendor.Id.ToString();

        }
        
        
        public void LoadData(Dictionary<ServiceMode, VendorPerformanceSummaryCriteria> criteriaList)
        {
            var dto = new VendorPerformanceSummaryDto();
            var results = criteriaList
                .Select(i => new
                {
                    i.Key,
                    Data = dto.Fetch(i.Value)
                })
                .ToDictionary(i => i.Key, i => i.Data);

            rptData.DataSource = results;
            rptData.DataBind();
        }

        public byte[] GetDataForExport(Dictionary<ServiceMode, VendorPerformanceSummaryCriteria> criteriaList)
        {
            var dto = new VendorPerformanceSummaryDto();
            var results = criteriaList
                .Select(i => dto.Fetch(i.Value)
                                .Select(d => new[]
				             	             	{
				             	             		d.Name, d.VendorNumber, d.Scac, 
													i.Key == ServiceMode.NotApplicable ? AllModes : i.Key.GetString(),
				             	             		d.TotalOverallLoadCount.ToString("n0"),
				             	             		d.TotalNonExcludedShipments.ToString("n0"),
				             	             		d.PercentOnTimeOverall.ToString("n2"),
				             	             		d.PercentOnTimePickup.ToString("n2"),
				             	             		d.PercentOnTimeDelivery.ToString("n2"),
				             	             		d.PercentAcceptedTenders.ToString("n2"),
				             	             		d.TotalMileage.ToString("n2"),
				             	             		ActiveUser.TenantEmployee
				             	             			? d.TotalAmountPaid.ToString("n2")
				             	             			: string.Empty
				             	             	}.TabJoin())
                                .ToArray()
                                .NewLineJoin())
                .ToList();

            if (ActiveUser.TenantEmployee) _exportHeader.Add(AmountPaidUsd);
            results.Insert(0, _exportHeader.ToArray().TabJoin());
            return results.Any() ? results.ToArray().NewLineJoin().ToUtf8Bytes() : new byte[0];
        }

        public byte[] GetDataForExport(VendorPerformanceSummaryCriteria criteria)
        {
            var dto = new VendorPerformanceSummaryDto();
            var results = dto.Fetch(criteria)
                .Select(d => new[]
				             	{
				             		d.Name, d.VendorNumber, d.Scac,
				             		criteria.Mode == ServiceMode.NotApplicable ? AllModes : criteria.Mode.GetString(),
				             		d.TotalOverallLoadCount.ToString("n0"),
				             		d.TotalNonExcludedShipments.ToString("n0"),
				             		d.PercentOnTimeOverall.ToString("n2"),
				             		d.PercentOnTimePickup.ToString("n2"),
				             		d.PercentOnTimeDelivery.ToString("n2"),
				             		d.PercentAcceptedTenders.ToString("n2"),
				             		d.TotalMileage.ToString("n2"),
				             		ActiveUser.TenantEmployee
				             			? d.TotalAmountPaid.ToString("n2")
				             			: string.Empty
				             	}.TabJoin())
                .ToList();

            if (ActiveUser.TenantEmployee) _exportHeader.Add(AmountPaidUsd);
            results.Insert(0, _exportHeader.ToArray().TabJoin());
            return results.Any() ? results.ToArray().NewLineJoin().ToUtf8Bytes() : new byte[0];
        }


        private void GetVendorStatistics()
        {
            var criteriaList = new Dictionary<ServiceMode, VendorPerformanceSummaryCriteria>();

            if (chkAllModes.Checked)
            {
                criteriaList.Add(ServiceMode.NotApplicable, GetCriteria(ServiceMode.NotApplicable));
            }
            else
            {
                if (chkAir.Checked) criteriaList.Add(ServiceMode.Air, GetCriteria(ServiceMode.Air));
                if (chkFTL.Checked) criteriaList.Add(ServiceMode.Truckload, GetCriteria(ServiceMode.Truckload));
                if (chkLTL.Checked) criteriaList.Add(ServiceMode.LessThanTruckload, GetCriteria(ServiceMode.LessThanTruckload));
                if (chkRail.Checked) criteriaList.Add(ServiceMode.Rail, GetCriteria(ServiceMode.Rail));
                if (chkSP.Checked) criteriaList.Add(ServiceMode.SmallPackage, GetCriteria(ServiceMode.SmallPackage));
            }

            vpscVendorPerformance.StartDate = txtStartShipmentDate.Text.ToDateTime();
            vpscVendorPerformance.EndDate = txtEndShipmentDate.Text.ToDateTime();
            vpscVendorPerformance.DateUnit = DateUnit.Month;

            LoadData(criteriaList);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new VendorPerformanceSummaryDisplayHandler(this).Initialize();

            if (IsPostBack) return;

            if(Loading != null)
                Loading(this, new EventArgs());

            aceCustomer.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.ActiveUser, ActiveUser.Id.GetString() } });
            aceVendor.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString() } });

            chkAllModes.Checked = true;
            pnlVendor.Visible = ActiveUser.TenantEmployee;
            txtVendorNumber.Text = string.Empty;
            pnlVendorBlank.Visible = !ActiveUser.TenantEmployee;
            txtStartShipmentDate.Text = DateTime.Now.AddYears(-1).FormattedShortDate();
            txtEndShipmentDate.Text = DateTime.Now.FormattedShortDate();

            if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.TransferNumber]))
            {
                var vendor = new Vendor(Request.QueryString[WebApplicationConstants.TransferNumber].UrlTextDecrypt().ToLong());
                DisplayVendor(vendor);
                GetVendorStatistics();
            }
        }



        protected void OnCustomerNumberEntered(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtCustomerNumber.Text))
            {
                DisplayCustomer(null);
                return;
            }

            if (CustomerSearch != null)
                CustomerSearch(this, new ViewEventArgs<string>(txtCustomerNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
        }

        protected void OnCustomerSearchClicked(object sender, ImageClickEventArgs e)
        {
            customerFinder.Visible = true;
        }

        protected void OnCustomerFinderSelectionCancelled(object sender, EventArgs e)
        {
            customerFinder.Visible = false;
        }

        protected void OnCustomerFinderItemSelected(object sender, ViewEventArgs<Customer> e)
        {
            DisplayCustomer(e.Argument);
            customerFinder.Visible = false;
        }



        protected void OnVendorNumberEntered(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtVendorNumber.Text))
            {
                DisplayVendor(null);
                return;
            }

            if (VendorSearch != null)
                VendorSearch(this, new ViewEventArgs<string>(txtVendorNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
        }

        protected void OnVendorSearchClicked(object sender, ImageClickEventArgs e)
        {
            vendorFinder.Reset();
            vendorFinder.EnableMultiSelection = false;
            vendorFinder.Visible = true;
        }

        protected void OnVendorFinderItemCancelled(object sender, EventArgs e)
        {
            vendorFinder.Visible = false;
        }

        protected void OnVendorFinderItemSelected(object sender, ViewEventArgs<Vendor> e)
        {
            DisplayVendor(e.Argument);
            vendorFinder.Visible = false;
        }



        private VendorPerformanceSummaryCriteria GetCriteria(ServiceMode mode)
        {
            return new VendorPerformanceSummaryCriteria
            {
                CustomerNumber = ActiveUser.HasAccessToCustomer(hidCustomerId.Value.ToLong()) ? txtCustomerNumber.Text : string.Empty,
                VendorNumber = txtVendorNumber.Text,
                UserId = ActiveUser.Id,
                TenantId = ActiveUser.TenantId,
                StartShipmentDateCreated = txtStartShipmentDate.Text.ToDateTime().TimeToMinimum(),
                EndShipmentDateCreated = txtEndShipmentDate.Text.ToDateTime().TimeToMaximum(),
                Mode = mode
            };
        }

        protected void OnGetVendorStatisticsClicked(object sender, EventArgs e)
        {
            GetVendorStatistics();
        }

        protected void OnExportVendorStatisticsClicked(object sender, EventArgs e)
        {
            var criteriaList = new Dictionary<ServiceMode, VendorPerformanceSummaryCriteria>();

            if (chkAllModes.Checked)
            {
                criteriaList.Add(ServiceMode.NotApplicable, GetCriteria(ServiceMode.NotApplicable));
            }
            else
            {
                if (chkAir.Checked) criteriaList.Add(ServiceMode.Air, GetCriteria(ServiceMode.Air));
                if (chkFTL.Checked) criteriaList.Add(ServiceMode.Truckload, GetCriteria(ServiceMode.Truckload));
                if (chkLTL.Checked) criteriaList.Add(ServiceMode.LessThanTruckload, GetCriteria(ServiceMode.LessThanTruckload));
                if (chkRail.Checked) criteriaList.Add(ServiceMode.Rail, GetCriteria(ServiceMode.Rail));
                if (chkSP.Checked) criteriaList.Add(ServiceMode.SmallPackage, GetCriteria(ServiceMode.SmallPackage));
            }

            Response.Export(GetDataForExport(criteriaList));
        }


        protected void OnDataItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var mode = (KeyValuePair<ServiceMode, List<VendorPerformanceSummaryDto>>)e.Item.DataItem;
            var rpt = e.Item.FindControl("rptModeDetails").ToRepeater();

            if (rpt == null) return;

            rpt.DataSource = mode.Value;
            rpt.DataBind();
        }

        protected void OnDisplayVendorPerformanceClicked(object sender, ImageClickEventArgs e)
        {
            var control = (Control) sender;

            vpscVendorPerformance.Visible = true;
            vpscVendorPerformance.ServiceMode = control.Parent.Parent.Parent.FindControl("hidServiceMode").ToCustomHiddenField().Value.ToEnum<ServiceMode>();
            vpscVendorPerformance.LoadVendorPerformanceSummary(((Control)sender).FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong());
        }

        protected void OnVendorPerformanceClose(object sender, EventArgs e)
        {
            vpscVendorPerformance.Visible = false;
        }
    }
}
