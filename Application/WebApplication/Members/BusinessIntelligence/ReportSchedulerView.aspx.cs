﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence
{
    public partial class ReportSchedulerView : MemberPageBase, IReportSchedulerView
    {
        private const string DuplicateArgs = "Duplicate";

        private const string SaveFlag = "S";

        public static string PageAddress { get { return "~/Members/BusinessIntelligence/ReportSchedulerView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.ReportScheduler; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.ReportsBlue; } }

        public Dictionary<int, string> ScheduleIntervals
        {
            set
            {
                ddlScheduleInterval.DataSource = value.Select(t => new ViewListItem(t.Value, t.Key.ToString())).ToList();
                ddlScheduleInterval.DataBind();
            }
        }

        public Dictionary<int, string> ExportFileExtension
        {
            set
            {
                ddlExtension.DataSource = value.Select(e => new ViewListItem(e.Value, e.Key.ToString())).ToList();
                ddlExtension.DataBind();
            }
        }

        public List<string> Time
        {
            set
            {
                ddlTime.DataSource = value;
                ddlTime.DataBind();
            }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<ReportSchedule>> Lock;
        public event EventHandler<ViewEventArgs<ReportSchedule>> UnLock;
        public event EventHandler<ViewEventArgs<ReportSchedule>> Search;
        public event EventHandler<ViewEventArgs<ReportSchedule>> Save;
        public event EventHandler<ViewEventArgs<ReportSchedule>> Delete;
        public event EventHandler<ViewEventArgs<ReportSchedule>> LoadAuditLog;

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == SaveFlag)
            {
                // unlock and return to read-only
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<ReportSchedule>(new ReportSchedule(hidReportScheduleId.Value.ToLong(), false)));

                switch (hidFlag.Value)
                {
                    case SaveFlag:
                        SetEditStatus(false);
                        memberToolBar.ShowUnlock = false;
                        hidResetLastRunDate.Value = false.ToString(); // reset back to default
                        break;
                }
            }

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;

            litErrorMessages.Text = messages.HasErrors()
                                        ? string.Join(WebApplicationConstants.HtmlBreak,
                                                      messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
                                                        Select(m => m.Message).ToArray())
                                        : string.Empty;
        }

        public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs) { auditLogs.Load(logs); }

        public void FailedLock(Lock @lock)
        {
            memberToolBar.ShowNew = false;
            memberToolBar.ShowSave = false;
            memberToolBar.ShowDelete = false;
            memberToolBar.ShowUnlock = false;

            SetEditStatus(false);

            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void Set(ReportSchedule reportSchedule)
        {
            LoadReportSchedule(reportSchedule);
            SetEditStatus(!reportSchedule.IsNew);
        }


        private void SetEditStatus(bool enabled)
        {
            pnlDetails.Enabled = enabled;

            litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;
        }


        private ReportSchedule UpdateReportSchedule()
        {
            var reportScheduleId = hidReportScheduleId.Value.ToLong();
            var reportSchedule = new ReportSchedule(reportScheduleId, reportScheduleId != default(long));

            if (reportSchedule.IsNew)
            {
                reportSchedule.TenantId = ActiveUser.TenantId;
                reportSchedule.LastRun = DateUtility.SystemEarliestDateTime;
            }

            // reset requested
            if (hidResetLastRunDate.Value.ToBoolean()) reportSchedule.LastRun = DateUtility.SystemEarliestDateTime;

            reportSchedule.ReportConfigurationId = hidReportConfigurationId.Value.ToLong();
            reportSchedule.UserId = hidUserId.Value.ToLong();

            reportSchedule.ScheduleInterval = ddlScheduleInterval.SelectedValue.ToEnum<ScheduleInterval>();
            reportSchedule.Start = txtStartDate.Text.ToDateTime().TimeToMinimum();
            reportSchedule.End = txtEndDate.Text.ToDateTime().TimeToMaximum();
            reportSchedule.Time = ddlTime.SelectedValue;
            reportSchedule.Extension = ddlExtension.SelectedValue.ToEnum<ReportExportExtension>();
            reportSchedule.Notify = chkNotify.Checked;
            reportSchedule.SuppressNotificationForEmptyReport = chkSuppressNotificationForEmptyReport.Checked;
            reportSchedule.NotifyEmails = txtNotifyEmails.Text.StripSpacesFromEmails();
            reportSchedule.ExcludeSunday = chkExcludeSunday.Checked;
            reportSchedule.ExcludeMonday = chkExcludeMonday.Checked;
            reportSchedule.ExcludeTuesday = chkExcludeTuesday.Checked;
            reportSchedule.ExcludeWednesday = chkExcludeWednesday.Checked;
            reportSchedule.ExcludeThursday = chkExcludeThursday.Checked;
            reportSchedule.ExcludeFriday = chkExcludeFriday.Checked;
            reportSchedule.ExcludeSaturday = chkExcludeSaturday.Checked;
            reportSchedule.Enabled = chkEnabled.Checked;

            reportSchedule.CustomerGroupId = ddlCustomerGroup.SelectedValue.ToLong();
            reportSchedule.VendorGroupId = ddlVendorGroup.SelectedValue.ToLong();

            //ftp
            reportSchedule.FtpEnabled = chkFtpEnabled.Checked;
            reportSchedule.SecureFtp = chkSecureFtp.Checked;
            reportSchedule.FtpUrl = txtFtpUrl.Text;
            reportSchedule.FtpUsername = txtFtpUsername.Text;
            reportSchedule.FtpPassword = txtFtpPassword.Text;
            reportSchedule.FtpDefaultFolder = txtFtpDefaultFolder.Text;
            reportSchedule.DeliveryFilename = this.txtDeliveryFilename.Text;

            return reportSchedule;
        }

        private void LoadReportSchedule(ReportSchedule reportSchedule)
        {
            hidReportScheduleId.Value = reportSchedule.Id.ToString();

            DisplayReportConfiguration(reportSchedule.ReportConfiguration);

            if (reportSchedule.ReportConfiguration.ReportTemplate != null)
            {
                if (reportSchedule.ReportConfiguration.ReportTemplate.CanFilterByCustomerGroup)
                    ddlCustomerGroup.SelectedValue = reportSchedule.CustomerGroupId.ToString();
                if (reportSchedule.ReportConfiguration.ReportTemplate.CanFilterByVendorGroup)
                    ddlVendorGroup.SelectedValue = reportSchedule.VendorGroupId.ToString();
            }

            DisplayUser(reportSchedule.User);

            ddlScheduleInterval.SelectedValue = reportSchedule.ScheduleInterval.ToInt().ToString();
            txtStartDate.Text = reportSchedule.Start.FormattedShortDate();
            txtEndDate.Text = reportSchedule.End.FormattedShortDate();
            txtLastRunDate.Text = reportSchedule.LastRun == DateUtility.SystemEarliestDateTime
                                    ? string.Empty
                                    : reportSchedule.LastRun.FormattedShortDate();
            if (ddlTime.ContainsValue(reportSchedule.Time)) ddlTime.SelectedValue = reportSchedule.Time;

            var extension = reportSchedule.Extension.ToInt().ToString();
            if (ddlExtension.ContainsValue(extension)) ddlExtension.SelectedValue = extension;

            chkNotify.Checked = reportSchedule.Notify;
            chkSuppressNotificationForEmptyReport.Checked = reportSchedule.SuppressNotificationForEmptyReport;
            txtNotifyEmails.Text = reportSchedule.NotifyEmails;

            chkExcludeSunday.Checked = reportSchedule.ExcludeSunday;
            chkExcludeMonday.Checked = reportSchedule.ExcludeMonday;
            chkExcludeTuesday.Checked = reportSchedule.ExcludeTuesday;
            chkExcludeWednesday.Checked = reportSchedule.ExcludeWednesday;
            chkExcludeThursday.Checked = reportSchedule.ExcludeThursday;
            chkExcludeFriday.Checked = reportSchedule.ExcludeFriday;
            chkExcludeSaturday.Checked = reportSchedule.ExcludeSaturday;
            chkEnabled.Checked = reportSchedule.Enabled;
            txtDeliveryFilename.Text = reportSchedule.DeliveryFilename;

            //ftp
            chkFtpEnabled.Checked = reportSchedule.FtpEnabled;
            chkSecureFtp.Checked = reportSchedule.SecureFtp;
            txtFtpUrl.Text = reportSchedule.FtpUrl;
            txtFtpUsername.Text = reportSchedule.FtpUsername;
            txtFtpPassword.Text = reportSchedule.FtpPassword;
            txtFtpDefaultFolder.Text = reportSchedule.FtpDefaultFolder;

            memberToolBar.ShowUnlock = reportSchedule.HasUserLock(ActiveUser, reportSchedule.Id);
        }

        private void DuplicateReportSchedule()
        {
            var reportSchedule = UpdateReportSchedule().Duplicate();

            LoadReportSchedule(reportSchedule);

            SetEditStatus(true);

            DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
        }

        private void ProcessTransferredRequest(long reportScheduleId)
        {
            if (reportScheduleId != default(long))
            {
                var reportSchedule = new ReportSchedule(reportScheduleId);
                LoadReportSchedule(reportSchedule);

                if (LoadAuditLog != null)
                    LoadAuditLog(this, new ViewEventArgs<ReportSchedule>(reportSchedule));
            }
        }

        private List<ToolbarMoreAction> ToolbarMoreActions()
        {
            var duplicate = new ToolbarMoreAction
                {
                    CommandArgs = DuplicateArgs,
                    ConfirmCommand = true,
                    ImageUrl = IconLinks.Operations,
                    IsLink = false,
                    Name = "Duplicate Report Schedule",
                    NavigationUrl = string.Empty
                };

            return new List<ToolbarMoreAction> { duplicate };
        }


        private void DisplayReportConfiguration(ReportConfiguration configuration)
        {
            hidReportConfigurationId.Value = configuration.Id.ToString();
            txtReportConfiguration.Text = configuration.Name;
            txtReportConfigurationDescription.Text = configuration.Description;

            if (configuration.ReportTemplate == null)
            {
                pnlCustomerGroups.Visible = false;
                pnlVendorGroups.Visible = false;
                return;
            }

            if (configuration.ReportTemplate.CanFilterByCustomerGroup)
            {
                pnlCustomerGroups.Visible = true;
                var viewListItems = new CustomerGroupSearch().FetchCustomerGroups(new List<ParameterColumn>(), ActiveUser.TenantId).Select(c => new ViewListItem(c.Name, c.Id.ToString())).ToList();
                viewListItems.Add(new ViewListItem(WebApplicationConstants.NotApplicable, default(long).ToString()));

                ddlCustomerGroup.DataSource = viewListItems;
                ddlCustomerGroup.DataBind();

                ddlCustomerGroup.SelectedValue = ddlCustomerGroup.ContainsValue(configuration.CustomerGroupId.ToString())
                                                    ? configuration.CustomerGroupId.ToString()
                                                    : default(long).ToString();
                ddlCustomerGroup.Enabled = !configuration.ReadonlyCustomerGroupFilter;
            }
            else
            {
                pnlCustomerGroups.Visible = false;
                ddlCustomerGroup.SelectedValue = default(long).ToString();
            }

            if (configuration.ReportTemplate.CanFilterByVendorGroup)
            {
                pnlVendorGroups.Visible = true;
                var viewListItems = new VendorGroupSearch().FetchVendorGroups(new List<ParameterColumn>(), ActiveUser.TenantId).Select(v => new ViewListItem(v.Name, v.Id.ToString())).ToList();
                viewListItems.Add(new ViewListItem(WebApplicationConstants.NotApplicable, default(long).ToString()));

                ddlVendorGroup.DataSource = viewListItems;
                ddlVendorGroup.DataBind();

                ddlVendorGroup.SelectedValue = ddlVendorGroup.ContainsValue(configuration.VendorGroupId.ToString())
                                                ? configuration.VendorGroupId.ToString()
                                                : default(long).ToString();
                ddlVendorGroup.Enabled = !configuration.ReadonlyVendorGroupFilter;
            }
            else
            {
                pnlVendorGroups.Visible = false;
                ddlVendorGroup.SelectedValue = default(long).ToString();
            }

        }

        private void DisplayUser(User user)
        {
            hidUserId.Value = user.Id.ToString();
            txtUser.Text = user.Username;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new ReportSchedulerHandler(this).Initialize();

            memberToolBar.ShowSave = Access.Modify;
            memberToolBar.ShowDelete = Access.Remove;
            memberToolBar.ShowNew = Access.Modify;
            memberToolBar.ShowEdit = Access.Modify;

            memberToolBar.LoadAdditionalActions(ToolbarMoreActions());


            if (IsPostBack) return;

            hidResetLastRunDate.Value = false.ToString();	// set default to false

            if (Loading != null)
                Loading(this, new EventArgs());


            if (Session[WebApplicationConstants.TransferReportScheduleId] != null)
            {
                ProcessTransferredRequest(Session[WebApplicationConstants.TransferReportScheduleId].ToLong());
                Session[WebApplicationConstants.TransferReportScheduleId] = null;
            }

            if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.TransferNumber]))
                ProcessTransferredRequest(Request.QueryString[WebApplicationConstants.TransferNumber].UrlTextDecrypt().ToLong());

            SetEditStatus(false);
        }


        protected void OnToolbarNewClicked(object sender, EventArgs e)
        {
            litErrorMessages.Text = string.Empty;

            DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
            LoadReportSchedule(new ReportSchedule
            {
                Start = DateTime.Today,
                End = DateTime.Today.AddYears(1),
                LastRun = DateUtility.SystemEarliestDateTime,
                Time = TimeUtility.Default,
            });
            SetEditStatus(true);
        }

        protected void OnToolbarEditClicked(object sender, EventArgs e)
        {
            var reportSchedule = new ReportSchedule(hidReportScheduleId.Value.ToLong(), false);

            if (Lock == null || reportSchedule.IsNew) return;

            SetEditStatus(true);
            memberToolBar.ShowUnlock = true;
            Lock(this, new ViewEventArgs<ReportSchedule>(reportSchedule));

            LoadReportSchedule(reportSchedule);
        }

        protected void OnUnlockClicked(object sender, EventArgs e)
        {
            var reportSchedule = new ReportSchedule(hidReportScheduleId.Value.ToLong(), false);
            if (UnLock != null && !reportSchedule.IsNew)
            {
                SetEditStatus(false);
                memberToolBar.ShowUnlock = false;
                UnLock(this, new ViewEventArgs<ReportSchedule>(reportSchedule));
            }
        }

        protected void OnToolbarSaveClicked(object sender, EventArgs e)
        {
            var reportSchedule = UpdateReportSchedule();

            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<ReportSchedule>(reportSchedule));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<ReportSchedule>(reportSchedule));
        }

        protected void OnToolbarDeleteClicked(object sender, EventArgs e)
        {
            var reportSchedule = new ReportSchedule(hidReportScheduleId.Value.ToLong(), false);

            if (Delete != null)
                Delete(this, new ViewEventArgs<ReportSchedule>(reportSchedule));

            LoadReportSchedule(new ReportSchedule
            {
                Start = DateTime.Today,
                End = DateTime.Today,
                LastRun = DateTime.Today.AddDays(14),
                Time = "12:00"
            });

            reportScheduleFinder.Reset();
            reportConfigurationFinder.Reset();
        }

        protected void OnToolbarFindClicked(object sender, EventArgs e)
        {
            reportScheduleFinder.Visible = true;
        }

        protected void OnToolbarCommand(object sender, ViewEventArgs<string> e)
        {
            switch (e.Argument)
            {
                case DuplicateArgs:
                    DuplicateReportSchedule();
                    break;
            }
        }

        protected void OnReportScheduleFinderItemSelected(object sender, ViewEventArgs<ReportSchedule> e)
        {
            litErrorMessages.Text = string.Empty;

            if (hidReportScheduleId.Value.ToLong() != default(long))
            {
                var oldReportSchedule = new ReportSchedule(hidReportScheduleId.Value.ToLong(), false);

                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<ReportSchedule>(oldReportSchedule));
            }

            var reportSchedule = e.Argument;

            hidReportScheduleId.Value = reportSchedule.Id.ToString();

            LoadReportSchedule(reportSchedule);

            reportScheduleFinder.Visible = false;

            if (reportScheduleFinder.OpenForEdit && Lock != null)
            {
                SetEditStatus(true);
                memberToolBar.ShowUnlock = true;
                Lock(this, new ViewEventArgs<ReportSchedule>(reportSchedule));
            }
            else
            {
                SetEditStatus(false);
            }

            if (LoadAuditLog != null) LoadAuditLog(this, new ViewEventArgs<ReportSchedule>(reportSchedule));
        }

        protected void OnReportScheduleFinderItemCancelled(object sender, EventArgs e)
        {
            reportScheduleFinder.Visible = false;
        }


        protected void OnReportConfigurationSearchClicked(object sender, ImageClickEventArgs e)
        {
            reportConfigurationFinder.EnableMultiSelection = false;
            reportConfigurationFinder.OpenForEditEnabled = false;
            reportConfigurationFinder.Visible = true;
        }

        protected void OnReportConfigurationFinderItemSelected(object sender, ViewEventArgs<ReportConfiguration> e)
        {
            DisplayReportConfiguration(e.Argument);

            reportConfigurationFinder.Visible = false;
        }

        protected void OnReportConfigurationFinderItemCancelled(object sender, EventArgs e)
        {
            reportConfigurationFinder.Visible = false;
        }


        protected void OnUserSearchClicked(object sender, ImageClickEventArgs e)
        {
            userFinder.EnableMultiSelection = false;
            userFinder.OpenForEditEnabled = false;
            userFinder.Visible = true;
        }

        protected void OnUserFinderItemSelected(object sender, ViewEventArgs<User> e)
        {
            DisplayUser(e.Argument);

            userFinder.Visible = false;
        }

        protected void OnUserFinderItemCancelled(object sender, EventArgs e)
        {
            userFinder.Visible = false;
        }



        protected void OnResetLastRunDateClicked(object sender, EventArgs e)
        {
            txtLastRunDate.Text = string.Empty;
            hidResetLastRunDate.Value = true.ToString();
        }
    }
}
