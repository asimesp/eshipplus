﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="ReportRunReadyView.aspx.cs" EnableEventValidation="false"
    Inherits="LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.ReportRunReadyView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
                Ready Report Dashboard<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>

        <hr class="dark mb10" />
        <div class="row mb20">
            <div class="right">
                <asp:Button runat="server" ID="btnRefresh" Text="Refresh" CssClass="w100" OnClick="OnRefreshClicked" CausesValidation="False" />
            </div>
        </div>
        <div class="rowgroup">
            <table id="reportScheduleListingTable" class="stripe">
                <tr>
                    <th style="width: 70%;">Report Name
                    </th>
                    <th style="width: 20%;">Date Created
                    </th>
                    <th style="width: 10%" class="text-center">Actions
                    </th>
                </tr>
                <asp:Repeater runat="server" ID="rptReportSchedules">
                    <ItemTemplate>
                        <tr>
                            <td class="top">
                                <eShip:CustomHiddenField ID="hidFileName" runat="server" Value='<%# Eval("CompletedReportFileName") %>' />
                                <eShip:CustomHiddenField ID="hidId" runat="server" Value='<%# Eval("Id") %>' />
                                <asp:Literal runat="server" ID="litReportName" Text='<%# Eval("ReportName") %>' />
                            </td>
                            <td>
                                <%# Eval("DateCreated") %>    
                            </td>
                            <td class="text-center">
                                <asp:ImageButton ID="ibtnDownload" runat="server" ImageUrl="~/images/icons2/exportBlue.png"
                                    CausesValidation="false" Visible='<%# Eval("IsComplete").ToBoolean() %>' OnClick="OnDownloadReportClicked" />
                                <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                    CausesValidation="false" Visible='<%# Eval("IsComplete").ToBoolean() %>' OnClick="OnDeleteReportClicked" />
                                <ajax:ConfirmButtonExtender runat="server" ID="cbeDelete" TargetControlID="ibtnDelete"
                                    ConfirmText="Are you sure you want to delete this report run record?" />
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </div>
    </div>

    <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
        Button="Ok" Icon="Information" />
</asp:Content>
