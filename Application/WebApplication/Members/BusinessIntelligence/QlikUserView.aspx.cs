﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using LogisticsPlus.Eship.WebApplication.Members.Administration;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence
{
    public partial class QlikUserView : AdminMemberPageBase, IQlikUserView
    {
        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public static string PageAddress { get { return "~/Members/BusinessIntelligence/QlikUserView.aspx"; } }

        public override ViewCode PageCode
        {
            get { return ViewCode.QlikUser; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RegistryBlue; }
        }

        public event EventHandler Search;
        public event EventHandler<ViewEventArgs<QlikUser>> Save;
        public event EventHandler<ViewEventArgs<QlikUser>> Delete;
        public event EventHandler<ViewEventArgs<List<QlikUser>>> BatchImport;


        public void DisplaySearchResult(List<QlikUser> qlikUsers)
        {
            var data = qlikUsers.Select(a => new
                {
                    a.UserId,
                    a.Name,
                    HasAttributes = a.QlikUserAttributes.Any(),
                    a.QlikUserAttributes
                })
                .ToList();

            litRecordCount.Text = data.BuildRecordCount();
            rptQlikUsers.DataSource = data;
            rptQlikUsers.DataBind();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() && !messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        private void DoSearch()
        {
            if (Search != null)
                Search(this, new ViewEventArgs<List<ParameterColumn>>(new List<ParameterColumn>()));
        }

        private void GeneratePopup(string title)
        {
            litTitle.Text = title;

            pnlEditQlikUser.Visible = true;
            pnlDimScreen.Visible = pnlEditQlikUser.Visible;

            txtUserId.Focus();
        }

        private void CloseEditPopup()
        {
            pnlEditQlikUser.Visible = false;
            pnlDimScreen.Visible = false;
        }

        private void ClearFields()
        {
            txtUserId.Text = string.Empty;
            txtName.Text = string.Empty;

            rptEditQlikUserAttributes.DataSource = new List<QlikUserAttribute>();
            rptEditQlikUserAttributes.DataBind();
            rptEditQlikUserAttributes.Visible = false;
        }

        private QlikUser RetrieveViewQlikUser()
        {
            var qlikUser = new QlikUser(txtUserId.Text);
            if(!qlikUser.IsNew) qlikUser.TakeSnapShot();

            qlikUser.UserId = txtUserId.Text;
            qlikUser.Name = txtName.Text;
            qlikUser.QlikUserAttributes = rptEditQlikUserAttributes
                .Items
                .Cast<RepeaterItem>()
                .Select(i => new QlikUserAttribute
                    {
                        Type = i.FindControl("txtType").ToTextBox().Text,
                        Value = i.FindControl("txtValue").ToTextBox().Text,
                        QlikUser = qlikUser,
                    })
                .ToList();

            return qlikUser;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new QlikUserHandler(this).Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowNew = Access.Modify;
            memberToolBar.ShowImport = Access.Modify;
            memberToolBar.ShowExport = Access.Grant;

            if (IsPostBack) return;

            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.QlikUserImportTemplate });

            DoSearch();
        }


        protected void OnNewClick(object sender, EventArgs eventArgs)
        {
            ClearFields();
            GeneratePopup("Add Qlik User");
            txtUserId.SetTextBoxEnabled(true);
        }

        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            ClearFields();
            GeneratePopup("Modify Qlik User");

            var qlikUser = new QlikUser(((ImageButton)sender).Parent.FindControl("hidQlikUserId").ToCustomHiddenField().Value);

            txtUserId.SetTextBoxEnabled(false);
            txtUserId.Text = qlikUser.UserId;
            txtName.Text = qlikUser.Name;

            rptEditQlikUserAttributes.DataSource = qlikUser.QlikUserAttributes;
            rptEditQlikUserAttributes.DataBind();
            rptEditQlikUserAttributes.Visible = qlikUser.QlikUserAttributes.Any();
        }

        protected void OnCloseClicked(object sender, object e)
        {
            CloseEditPopup();
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            var qlikUser = RetrieveViewQlikUser();

            if (Save != null)
                Save(this, new ViewEventArgs<QlikUser>(qlikUser));

            DoSearch();
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("hidQlikUserId").ToCustomHiddenField();

            var qlikUser = new QlikUser(hidden.Value, true);
            qlikUser.LoadQlikUserAttributes();

            if (Delete != null)
                Delete(this, new ViewEventArgs<QlikUser>(qlikUser));

            DoSearch();
        }


        protected void OnImportClicked(object sender, EventArgs e)
        {
            fileUploader.Title = "QLIK USER IMPORT";
            fileUploader.Instructions = @"**Please refer to user guide for import file format.";
            fileUploader.Visible = true;
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            var lines = rptQlikUsers
                .Items
                .Cast<RepeaterItem>()
                .Select(item => new[]
                    {
                        item.FindControl("hidQlikUserId").ToCustomHiddenField().Value,
                        item.FindControl("litName").ToLiteral().Text.ToString()
                    }.TabJoin())
                .ToList();
            lines.Insert(0, new[] {"User Id", "Name" }.TabJoin());

            Response.Export(lines.ToArray().NewLineJoin(), "QlikUsers.txt");
        }

        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 2;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var qlikUsers = lines
                .Select(s => new QlikUser
                {
                    UserId = s[0],
                    Name = s[1],
                    QlikUserAttributes = new List<QlikUserAttribute>()
                })
                .ToList();

            if (BatchImport != null)
                BatchImport(this, new ViewEventArgs<List<QlikUser>>(qlikUsers));

            fileUploader.Visible = false;
            DoSearch();
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }


        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;
            if (CloseEditPopUp) CloseEditPopup();
        }


        protected void OnQlikUserAttributeDeleteClicked(object sender, ImageClickEventArgs e)
        {
            var button = (ImageButton)sender;
            var rowItemIndex = button.FindControl("hidQlikUserAttributeItemIndex").ToCustomHiddenField().Value.ToLong();

            var vQlikUserAttributes = rptEditQlikUserAttributes
                .Items
                .Cast<RepeaterItem>()
                .Select(i => new
                {
                    Type = i.FindControl("txtType").ToTextBox().Text,
                    Value = i.FindControl("txtValue").ToTextBox().Text,
                    RowItemIndex = i.FindControl("hidQlikUserAttributeItemIndex").ToCustomHiddenField().Value.ToLong()
                })
                .Where(a => a.RowItemIndex != rowItemIndex)
                .ToList();

            rptEditQlikUserAttributes.DataSource = vQlikUserAttributes;
            rptEditQlikUserAttributes.DataBind();
            rptEditQlikUserAttributes.Visible = vQlikUserAttributes.Any();
        }

        protected void OnAddQlikUserAttributeClicked(object sender, EventArgs e)
        {

            var vQlikUserAttributes = rptEditQlikUserAttributes
                .Items
                .Cast<RepeaterItem>()
                .Select(i => new
                {
                    Type = i.FindControl("txtType").ToTextBox().Text,
                    Value = i.FindControl("txtValue").ToTextBox().Text,
                })
                .ToList();

            vQlikUserAttributes.Add(new
            {
                Type = string.Empty,
                Value = string.Empty,
            });

            rptEditQlikUserAttributes.DataSource = vQlikUserAttributes;
            rptEditQlikUserAttributes.DataBind();
            rptEditQlikUserAttributes.Visible = vQlikUserAttributes.Any();
        }

        protected void OnQlikUserItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var item = e.Item;
            if (!item.DataItem.HasGettableProperty("QlikUserAttributes")) return;

            var rptQlikUserAttributes = item.FindControl("rptQlikUserAttributes").ToRepeater();
            rptQlikUserAttributes.DataSource = item.DataItem.GetPropertyValue("QlikUserAttributes");
            rptQlikUserAttributes.DataBind();
            rptQlikUserAttributes.Visible = ((List<QlikUserAttribute>)item.DataItem.GetPropertyValue("QlikUserAttributes")).Any();
        }


        protected void OnEditQlikUserAttributesItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var item = e.Item;
            var aceQlikUserAttributeType = item.FindControl("aceQlikUserAttributeType").ToAutoCompleteExtender();
            if (aceQlikUserAttributeType != null) aceQlikUserAttributeType.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.ActiveSuperUser, ActiveSuperUserId.GetString() } });
            var aceQlikUserAttributeValue = item.FindControl("aceQlikUserAttributeValue").ToAutoCompleteExtender();
            if (aceQlikUserAttributeValue != null) aceQlikUserAttributeValue.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.ActiveSuperUser, ActiveSuperUserId.GetString() } });

        }

        protected void OnQlikUserAttributeTypeChanged(object sender, EventArgs e)
        {
            var textbox = (TextBox) sender;
            var aceQlikUserAttributeValue = textbox.FindControl("aceQlikUserAttributeValue").ToAutoCompleteExtender();
            aceQlikUserAttributeValue.SetupExtender(new Dictionary<string, string>
                {
                    {AutoCompleteUtility.Keys.ActiveSuperUser, ActiveSuperUserId.GetString()},
                    {AutoCompleteUtility.Keys.QlikUserAttributeType, textbox.Text}
                });
        }
    }
}