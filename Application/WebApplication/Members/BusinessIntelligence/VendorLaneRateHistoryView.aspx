﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="VendorLaneRateHistoryView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.VendorLaneRateHistoryView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">

    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                CssClass="pageHeaderIcon" />
            <h3>Vendor Lane Rate - History
            <small class="ml10">
                <asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>
        <span class="clearfix"></span>
        <hr class="dark mb10" />

        <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
        <eShip:VendorInformationControl runat="server" ID="vendorInfoControl" Visible="False" OnCancel="OnVendorInfoCancel" />

        <asp:Panel runat="server" DefaultButton="btnSearch">
            <table>
                <tr>
                    <td class="text-right">
                        <label class="upper blue">Origin Postal Code: </label>
                    </td>
                    <td>
                        <eShip:CustomTextBox ID="txtOriginPostalCode" CssClass="w150" runat="server" MaxLength="10" />
                        <asp:RequiredFieldValidator runat="server" ID="rfvSearchCriteria" ControlToValidate="txtOriginPostalCode"
                            ErrorMessage="All fields marked with an ' * ' are required" Text="*" />
                    </td>
                    <td class="text-right">
                        <label class="upper blue">Origin Country: </label>
                    </td>
                    <td>
                        <eShip:CachedObjectDropDownList runat="server" ID ="ddlDestinationCountry" CssClass="w150" Type="Countries" EnableChooseOne="True" DefaultValue="0"/>
                    </td>
                </tr>
                <tr>
                    <td class="text-right">
                        <label class="upper blue">Destination Postal Code: </label>
                    </td>
                    <td>
                        <eShip:CustomTextBox ID="txtDestinationPostalCode" CssClass="w150" runat="server" MaxLength="10" />
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtDestinationPostalCode"
                            ErrorMessage="All fields marked with an ' * ' are required" Text="*"></asp:RequiredFieldValidator>
                    </td>
                    <td class="text-right">
                        <label class="upper blue">Destination Country: </label>
                    </td>
                    <td>
                        <eShip:CachedObjectDropDownList runat="server" ID ="ddlOriginCountry" CssClass="w150" Type="Countries" EnableChooseOne="True" DefaultValue="0"/>
                    </td>
                </tr>
                <tr>
                    <td class="text-right">
                        <label class="upper blue">Weight Used For Calc: </label>
                    </td>
                    <td>
                        <eShip:CustomTextBox runat="server" ID="txtTotalWeight" CssClass="w150" MaxLength="50" Type="FloatingPointNumbers" />
                    </td>
                    <td class="text-right">
                        <label class="upper blue">Within One Year Prior To:</label>
                    </td>
                    <td class="AjaxCalendarTable text-left">
                        <eShip:CustomTextBox runat="server" ID="txtStartDate" CssClass="w100" Type="Date" placeholder="99/99/9999"/>
                    </td>
                    <td class="text-right">
                        <label class="upper blue">Within Mileage Radius: </label>
                    </td>
                    <td>
                        <eShip:CustomTextBox runat="server" ID="txtMileageRadius" CssClass="w150" MaxLength="50" />
                        <ajax:FilteredTextBoxExtender ID="filMileageRadius" runat="server" TargetControlID="txtMileageRadius"
                            FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                    </td>
                </tr>
            </table>
            <hr class="fat" />
            <div class="row mb10">
                <div class="right">
                    <asp:Button ID="btnSearch" runat="server" Text="SEARCH" OnClick="OnSearchClicked" CssClass="right" />
                </div>
            </div>
        </asp:Panel>
        <ajax:TabContainer ID="tabVendorLaneRateHistory" runat="server" CssClass="ajaxCustom" Visible="false">
            <ajax:TabPanel runat="server" ID="tabLTL" HeaderText="Less Than Truckload">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlRates">
                        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheLtlRatesTable" TableId="ltlRatesTable" HeaderZIndex="2" />
                        <div class="rowgroup">
                            <table class="line2 pl2" id="ltlRatesTable">
                                <tr>
                                    <th style="width: 8%;">Vendor Number </th>
                                    <th style="width: 26%;">Name </th>
                                    <th style="width: 8%;" class="text-right">
                                        <abbr title="Contract Rate/Quote/Shipment record count">
                                            Record Count</abbr>
                                    </th>
                                    <th style="width: 8%;" class="text-right">Lowest Buy ($) </th>
                                    <th style="width: 8%;" class="text-right">Highest Buy ($) </th>
                                    <th style="width: 8%;" class="text-right">Avg. Buy ($) </th>
                                    <th style="width: 8%;" class="text-right">Lowest Sell ($) </th>
                                    <th style="width: 8%;" class="text-right">Highest Sell ($) </th>
                                    <th style="width: 10%;" class="text-right">Avg. Sell ($) </th>
                                    <th style="width: 8%;" class="text-center">Contract Rate </th>
                                </tr>
                                <asp:ListView ID="lstLtlRates" runat="server" ItemPlaceholderID="itemPlaceHolder"
                                    OnItemDataBound="OnLtlRatesDataBound">
                                    <LayoutTemplate>
                                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td rowspan="2" class="top">
                                                <%# ActiveUser.HasAccessTo(ViewCode.Customer) 
                                                    ? string.Format("{0} <a href='{1}?{2}={3}' class='blue' target='_blank' title='Go To Vendor Record'><img src={4} width='16' class='middle'/></a>", 
                                                        Eval("VendorNumber"),
                                                        ResolveUrl(VendorView.PageAddress),WebApplicationConstants.TransferNumber, 
                                                        Eval("VendorId").GetString().UrlTextEncrypt(),  
                                                        ResolveUrl("~/images/icons2/arrow_newTab.png"))  
                                                    : Eval("VendorNumber") %>
                                            </td>
                                            <td>
                                                <eShip:CustomHiddenField runat="server" ID="hidVendorId" Value='<%# Eval("VendorId")%>' />
                                                <asp:LinkButton runat="server" ID="lbtnVendorName" Text='<%# Eval("VendorName")%>' CssClass="blue"
                                                    OnClick="OnVendorNameClick" />
                                            </td>
                                            <td class="text-right">
                                                <%# Eval("RecordCount")%>
                                            </td>
                                            <td class="text-right">
                                                <%# Eval("TotalLowestBuy").ToDecimal().ToString("f4")%>
                                            </td>
                                            <td class="text-right">
                                                <%# Eval("TotalHighestBuy").ToDecimal().ToString("f4")%>
                                            </td>
                                            <td class="text-right">
                                                <%# Eval("TotalAvgBuy").ToDecimal().ToString("f4")%>
                                            </td>
                                            <td class="text-right">
                                                <%# Eval("TotalLowestSell").ToDecimal().ToString("f4")%>
                                            </td>
                                            <td class="text-right">
                                                <%# Eval("TotalHighestSell").ToDecimal().ToString("f4")%>
                                            </td>
                                            <td class="text-right">
                                                <%# Eval("TotalAvgSell").ToDecimal().ToString("f4")%>
                                            </td>
                                            <td class="text-center top" rowspan="2">
                                                <%# Eval("ContractRate").ToBoolean() ? WebApplicationConstants.HtmlCheck : string.Empty%>
                                            </td>
                                        </tr>
                                        <tr class="f9">
                                            <td colspan="8" class="forceLeftBorder">
                                                <table class="contain pl2">
                                                    <tr class="bbb1">
                                                        <th style="width: 8%;" class="no-top-border">Code </th>
                                                        <th style="width: 32%;" class="no-top-border no-left-border">Description </th>
                                                        <th style="width: 10%;" class="text-right no-top-border no-left-border">Lowest Buy ($) </th>
                                                        <th style="width: 10%;" class="text-right no-top-border no-left-border">Highest Buy ($) </th>
                                                        <th style="width: 10%;" class="text-right no-top-border no-left-border">Avg Buy ($) </th>
                                                        <th style="width: 10%;" class="text-right no-top-border no-left-border">Lowest Sell ($) </th>
                                                        <th style="width: 10%;" class="text-right no-top-border no-left-border">Highest Sell ($) </th>
                                                        <th style="width: 10%;" class="text-right no-top-border no-left-border">Avg Sell ($) </th>
                                                    </tr>
                                                    <asp:ListView ID="lstLtlCharges" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                                        <LayoutTemplate>
                                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                                        </LayoutTemplate>
                                                        <ItemTemplate>
                                                            <tr class="no-bottom-border">
                                                                <td>
                                                                    <%# Eval("ChargeCode")%>
                                                                </td>
                                                                <td>
                                                                    <%# Eval("ChargeCodeDescription")%>
                                                                </td>
                                                                <td class="text-right">
                                                                    <%# Eval("LowestBuy").ToDecimal().ToString("f4")%>
                                                                </td>
                                                                <td class="text-right">
                                                                    <%# Eval("HighestBuy").ToDecimal().ToString("f4")%>
                                                                </td>
                                                                <td class="text-right">
                                                                    <%# Eval("AverageBuy").ToDecimal().ToString("f4")%>
                                                                </td>
                                                                <td class="text-right">
                                                                    <%# Eval("LowestSell").ToDecimal().ToString("f4")%>
                                                                </td>
                                                                <td class="text-right">
                                                                    <%# Eval("HighestSell").ToDecimal().ToString("f4")%>
                                                                </td>
                                                                <td class="text-right">
                                                                    <%# Eval("AverageSell").ToDecimal().ToString("f4")%>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                </table>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </table>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabFTL" HeaderText="Full Truck Load">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlFTL">
                        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheFtlRates" TableId="ftlRates" HeaderZIndex="2" />
                        <div class="rowgroup">
                            <table class="line2 pl2" id="ftlRates">
                                <tr>
                                    <th style="width: 8%;">Vendor Number </th>
                                    <th style="width: 34%;">Name </th>
                                    <th style="width: 8%;" class="text-right">
                                        <abbr title="Contract Rate/Quote/Shipment record count">
                                            Record Count</abbr>
                                    </th>
                                    <th style="width: 10%;" class="text-right">Lowest Buy ($)</th>
                                    <th style="width: 10%;" class="text-right">Highest Buy ($)</th>
                                    <th style="width: 10%;" class="text-right">Lowest Sell ($)</th>
                                    <th style="width: 12%;" class="text-right">Highest Sell ($)</th>
                                    <th style="width: 8%;" class="text-center">Contract Rate </th>
                                </tr>
                                <asp:ListView ID="lstFTlRates" runat="server" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnFtlRatesDataBound">
                                    <LayoutTemplate>
                                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td rowspan="2" class="top">
                                                 <%# ActiveUser.HasAccessTo(ViewCode.Vendor) 
                                                    ? string.Format("{0} <a href='{1}?{2}={3}' class='blue' target='_blank' title='Go To Vendor Record'><img src={4} width='20' class='middle'/></a>", 
                                                        Eval("VendorNumber"),
                                                        ResolveUrl(VendorView.PageAddress),WebApplicationConstants.TransferNumber, 
                                                        Eval("VendorId").GetString().UrlTextEncrypt(),  
                                                        ResolveUrl("~/images/icons2/arrow_newTab.png"))  
                                                    : Eval("VendorNumber") %>
                                            </td>
                                            <td>
                                                <eShip:CustomHiddenField runat="server" ID="hidVendorId" Value='<%# Eval("VendorId")%>' />
                                                <asp:LinkButton runat="server" ID="lbtnVendorName" Text='<%# Eval("VendorName")%>' CssClass="blue" OnClick="OnVendorNameClick" />
                                            </td>
                                            <td class="text-right">
                                                <%# Eval("RecordCount")%>
                                            </td>
                                            <td class="text-right">
                                                <%# Eval("TotalLowestBuy").ToDecimal().ToString("f4")%>
                                            </td>
                                            <td class="text-right">
                                                <%# Eval("TotalHighestBuy").ToDecimal().ToString("f4")%>
                                            </td>
                                            <td class="text-right">
                                                <%# Eval("TotalLowestSell").ToDecimal().ToString("f4")%>
                                            </td>
                                            <td class="text-right">
                                                <%# Eval("TotalHighestSell").ToDecimal().ToString("f4")%>
                                            </td>
                                            <td rowspan="2" class="top text-center">
                                                <%# Eval("ContractRate").ToBoolean() ? WebApplicationConstants.HtmlCheck : string.Empty%>
                                            </td>
                                        </tr>

                                        <tr class="f9">
                                            <td colspan="6" class="forceLeftBorder">
                                                <table class="contain pl2">
                                                    <tr class="bbb1">
                                                        <th style="width: 8%;" class="no-top-border">Code </th>
                                                        <th style="width: 31.5%;" class="no-top-border no-left-border">Description </th>
                                                        <th style="width: 15%;" class="text-right no-top-border no-left-border">Lowest Buy ($) </th>
                                                        <th style="width: 15%;" class="text-right no-top-border no-left-border">Highest Buy ($) </th>
                                                        <th style="width: 15.5%;" class="text-right no-top-border no-left-border">Lowest Sell ($) </th>
                                                        <th style="width: 16%;" class="text-right no-top-border no-left-border">Highest Sell ($) </th>
                                                    </tr>
                                                    <asp:ListView ID="lstFtlCharges" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                                        <LayoutTemplate>
                                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                                        </LayoutTemplate>
                                                        <ItemTemplate>
                                                            <tr class="no-bottom-border">
                                                                <td>
                                                                    <%# Eval("ChargeCode")%>
                                                                </td>
                                                                <td>
                                                                    <%# Eval("ChargeCodeDescription")%>
                                                                </td>
                                                                <td class="text-right">
                                                                    <%# Eval("LowestBuy").ToDecimal().ToString("f4")%>
                                                                </td>
                                                                <td class="text-right">
                                                                    <%# Eval("HighestBuy").ToDecimal().ToString("f4")%>
                                                                </td>
                                                                <td class="text-right">
                                                                    <%# Eval("LowestSell").ToDecimal().ToString("f4")%>
                                                                </td>
                                                                <td class="text-right">
                                                                    <%# Eval("HighestSell").ToDecimal().ToString("f4")%>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                </table>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </table>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
        </ajax:TabContainer>
    </div>
</asp:Content>
