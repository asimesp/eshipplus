﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ReportBase.Master" AutoEventWireup="true" CodeBehind="ReportDisplayView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.ReportDisplayView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            SetSectionSizes();
        });

        $(window).resize(function () {
            SetSectionSizes();
        });

        function SetSectionSizes() {
            $("div[iframe]").each(function() {
                $(this).height($(window).height() - $("#tabs").height());
                $(this).parent().css("top", $("#tabs").css("height"));
            });
        }


        function ShowIndex(control, index) {
            $("div[iframe]").each(function () {
                if ($(this).attr("iframe") != index)
                    $(this).parent().css('z-index', 0);
                else
                    $(this).parent().css('z-index', 1);
            });
            $(control).parent().find('span').each(function () { $(this).removeClass("text-primary"); });
            $(control).addClass("text-primary");
        }

    </script>


    <div id="tabs" class="text-center pt5" style="width: 100%;">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <asp:Repeater runat="server" ID="rptSheetTags">
                <ItemTemplate>
                    <span onclick='javascript:ShowIndex(this, <%# Container.ItemIndex %>);'
                        class='ml10 mr10 hidden <%# Container.ItemIndex == 0 ? "text-primary" : string.Empty %>' style="cursor: pointer;">
                        <%# Eval("Name") %>
                    </span>

                    <li role="presentation" class='<%# Container.ItemIndex == 0 ? "active" : string.Empty %>'>
                        <a href="#" aria-controls="home" role="tab" data-toggle="tab" onclick='javascript:ShowIndex(this, <%# Container.ItemIndex %>);'><%# Eval("Name") %></a>
                    </li>

                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </div>

    <asp:Repeater runat="server" ID="rptSheets">
        <ItemTemplate>
            <div style='z-index: <%# Eval("Index") %>; position: fixed; top: 50px; width: 100%;' id='tab<%# Container.ItemIndex %>'>
                <div iframe='<%# Container.ItemIndex %>'>
                    <iframe src='<%# Eval("Link") %>' frameborder='0' width='100%' height='100%'></iframe>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>



</asp:Content>
