﻿using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;

namespace LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence
{
	public class ChartValidationUtilities : ValidatorBase
	{
		private const string IncompatableChartTypesMessage = "Y values with a chart type of {0} can only be displayed with other Y values of the same chart type.<br/>";

        private const string NewChartTypesNotCompatableWithExisting = "One or more of the Y value chart types you are attempting to add is not compatable with the existing Y value chart types for this configuration.<br/>";

        public bool XAxisIsValid(string xAxis, ChartDataSourceType type, List<string> dataColumns)
        {
            var valid = true;

            if (type == ChartDataSourceType.Pivot) return valid;

            if (!dataColumns.Contains(xAxis))
            {
                Messages.Add(ValidationMessage.Warning("One or more charts are using an invalid X-Axis Data Column"));
                valid = false;
            }

            return valid;
        }

		public bool IsValid(List<KeyValuePair<string, ReportChartType>> originalYcolumns, List<KeyValuePair<string, ReportChartType>> newYcolumns)
		{
			var valid = true;

            if(!originalYcolumns.Any() && !newYcolumns.Any())
		    {
		        Messages.Add(ValidationMessage.Warning("Charts require at least one Y Series"));
		        valid = false;
		    }

			//check columns for duplicates
			if (originalYcolumns.Any(oi => newYcolumns.Any(ni => ni.Key == oi.Key)) || newYcolumns.Any(ni => originalYcolumns.Any(oi => oi.Key == ni.Key)))
				Messages.Add(ValidationMessage.Warning("Chart configuration has duplicate Y value columns"));

			//check columns for compatability
			if (!YAxisColumnsAreValid(originalYcolumns, newYcolumns))
				valid = false;

			return valid;
		}

		public bool YAxisColumnsAreValid(List<KeyValuePair<string, ReportChartType>> originalYcolumns, List<KeyValuePair<string, ReportChartType>> newYcolumns)
		{
			if (!newYcolumns.Any()) return true;

			var valid = true;

			if (newYcolumns.Any(c => c.Value == ReportChartType.Pie) && newYcolumns.Any(ct => ct.Value != ReportChartType.Pie))
			{
				Messages.Add(ValidationMessage.Error(IncompatableChartTypesMessage, ReportChartType.Pie));
				valid = false;
			}
			if (newYcolumns.Any(c => c.Value == ReportChartType.Bar) && newYcolumns.Any(ct => ct.Value != ReportChartType.Bar))
			{
				Messages.Add(ValidationMessage.Error(IncompatableChartTypesMessage, ReportChartType.Bar));
				valid = false;
			}
			if (newYcolumns.Any(c => c.Value == ReportChartType.StackedBar) && newYcolumns.Any(ct => ct.Value != ReportChartType.StackedBar))
			{
				Messages.Add(ValidationMessage.Error(IncompatableChartTypesMessage, ReportChartType.StackedBar));
				valid = false;
			}
			if (newYcolumns.Any(c => c.Value == ReportChartType.StackedColumn) && newYcolumns.Any(ct => ct.Value != ReportChartType.StackedColumn))
			{
				Messages.Add(ValidationMessage.Error(IncompatableChartTypesMessage, ReportChartType.StackedColumn));
				valid = false;
			}


			if (!originalYcolumns.Any()) return valid;

			switch (originalYcolumns.First().Value)
			{
				case ReportChartType.Pie:
					if (newYcolumns.Any(c => c.Value != ReportChartType.Pie))
					{
						Messages.Add(ValidationMessage.Error(NewChartTypesNotCompatableWithExisting));
						valid = false;
					}
					break;
				case ReportChartType.Bar:
					if (newYcolumns.Any(c => c.Value != ReportChartType.Bar))
					{
						Messages.Add(ValidationMessage.Error(NewChartTypesNotCompatableWithExisting));
						valid = false;
					}
					break;
				case ReportChartType.StackedBar:
					if (newYcolumns.Any(c => c.Value != ReportChartType.StackedBar))
					{
						Messages.Add(ValidationMessage.Error(NewChartTypesNotCompatableWithExisting));
						valid = false;
					}
					break;
				case ReportChartType.StackedColumn:
					if (newYcolumns.Any(c => c.Value != ReportChartType.StackedColumn))
					{
						Messages.Add(ValidationMessage.Error(NewChartTypesNotCompatableWithExisting));
						valid = false;
					}
					break;
				default:
					if (newYcolumns.Any(c => c.Value == ReportChartType.Pie 
						|| c.Value == ReportChartType.Bar
						|| c.Value == ReportChartType.StackedBar
						|| c.Value == ReportChartType.StackedColumn))
					{
						Messages.Add(ValidationMessage.Error(NewChartTypesNotCompatableWithExisting));
						valid = false;
					}
					break;
			}

			return valid;
		}
	}
}
