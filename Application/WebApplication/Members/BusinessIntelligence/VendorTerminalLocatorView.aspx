﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="VendorTerminalLocatorView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.VendorTerminalLocatorView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core.Registry" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="false"
        ShowEdit="false" ShowDelete="false" ShowSave="false" ShowFind="false" ShowImport="false"
        ShowExport="false" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                CssClass="pageHeaderIcon" />
            <h3>Vendor Terminal Locator
            <small class="ml10">
                <asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>
        <span class="clearfix"></span>
        <hr class="dark mb10" />

        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
            <table>
                <tr>
                    <td class="text-right">
                        <label class="upper blue">Postal Code: </label>
                    </td>
                    <td class="text-left">
                        <eShip:CustomTextBox runat="server" ID="txtPostalCode" CssClass="w150" MaxLength="10" />
                        <asp:RequiredFieldValidator runat="server" ID="rfvPostalCode" ControlToValidate="txtPostalCode"
                            ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                    </td>
                    <td class="text-right">
                        <label class="upper blue">Country: </label>
                    </td>
                    <td class="text-left">
                        <eShip:CachedObjectDropDownList runat="server" ID ="ddlTerminalCountries" CssClass="w150" Type="Countries" EnableChooseOne="True" DefaultValue="0"/>
                    </td>
                    <td>
                        <label class="upper blue">Within Miles: </label>
                        <eShip:CustomTextBox ID="txtRadius" CssClass="w70" runat="server" />
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtRadius"
                            ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" TargetControlID="txtRadius"
                            FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                    </td>
                </tr>
            </table>
            <hr class="fat" />
            <div class="row mb10">
                <div class="fieldgroup right">
                    <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="OnSearchClicked" />
                    <asp:Button ID="btnExport" runat="server" Text="Export" OnClick="OnExportClicked" />
                </div>
            </div>
        </asp:Panel>
        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheViewRegistryTable" TableId="terminalTable" HeaderZIndex="2" />
        <table class="line2 pl2" id="terminalTable">
            <tr>
                <th style="width: 18%">Name</th>
                <th style="width: 27%">Address 1</th>
                <th style="width: 27%">Address 2</th>
                <th style="width: 16%">Country</th>
                <th style="width: 7%">Distance</th>
                <th style="width: 5%">Active</th>
            </tr>
            <asp:ListView ID="lstTerminals" runat="server" ItemPlaceholderID="itemPlaceHolder">
                <LayoutTemplate>
                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                </LayoutTemplate>
                <ItemTemplate>
                    <tr>
                        <td rowspan="2" class="top">
                            <%# Eval("Name")%>
                        </td>
                        <td>
                            <%# Eval("Address1") %>
                        </td>
                        <td>
                            <%# Eval("Address2") %>
                        </td>
                        <td>
                            <%# new Country(Eval("CountryId").ToLong()).Name %>
                        </td>
                        <td>
                            <%# Eval("Distance") %>
                        </td>
                        <td rowspan="2" class="top text-center">
                            <%# Eval("Active").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                        </td>
                    </tr>
                    <tr class="f9">
                        <td colspan="4" class="forceLeftBorder">
                            <div class="rowgroup mb20">
                                <div class="col_1_2">
                                    <div class="col_1_2">
                                        <div class="row">
                                            <label class="wlabel blue">
                                                Vendor
                                                
                                            </label>
                                            <label>
                                                <%# Eval("Vendor") %>
                                                <asp:HyperLink runat="server" Target="_blank" Visible='<%# ActiveUser.HasAccessTo(ViewCode.Vendor) %>' ToolTip="Go To Vendor Record"
                                                    NavigateUrl='<%# string.Format("{0}?{1}={2}", VendorView.PageAddress, WebApplicationConstants.TransferNumber, Eval("VendorId").GetString().UrlTextEncrypt())%>'>
                                                    <asp:Image ImageUrl="~/images/icons2/arrow_newTab.png" runat="server" Width="16px" CssClass="middle"/>
                                                </asp:HyperLink>
                                                &nbsp;
                                            </label>
                                        </div>
                                        <div class="row">
                                            <label class="wlabel blue">MC</label>
                                            <label><%# Eval("MC") %>&nbsp;</label>
                                        </div>
                                    </div>
                                    <div class="col_1_2">
                                        <div class="row">
                                            <label class="wlabel blue">SCAC</label>
                                            <label><%# Eval("Scac") %>&nbsp;</label>
                                        </div>
                                        <div class="row">
                                            <label class="wlabel blue">DOT</label>
                                            <label><%# Eval("DOT") %>&nbsp;</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col_1_2">
                                    <div class="col_1_2">
                                        <div class="row">
                                            <label class="wlabel blue">Contact</label>
                                            <label><%# Eval("ContactName")%>&nbsp;</label>
                                        </div>
                                        <div class="row">
                                            <label class="wlabel blue">Phone</label>
                                            <label><%# Eval("Phone") %>&nbsp;</label>
                                        </div>
                                        <div class="row">
                                            <label class="wlabel blue">Email</label>
                                            <label><%# Eval("Email") %>&nbsp;</label>
                                        </div>
                                    </div>
                                    <div class="col_1_2">
                                        <div class="row">
                                            <label class="wlabel blue">Mobile</label>
                                            <label><%# Eval("Mobile") %>&nbsp;</label>
                                        </div>
                                        <div class="row">
                                            <label class="wlabel blue">Fax</label>
                                            <label><%# Eval("Fax") %>&nbsp;</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </table>
    </div>
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
</asp:Content>
