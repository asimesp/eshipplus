﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.DataVisualization.Charting;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;


namespace LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence
{
    public class ChartData
    {
        private const string TickArrayJsonString = "[{0},\"{1}\"],";
        private const string JsNull = "null";
        private const string ModeNormal = "normal";
        private const string ModeTime = "time";
        private const string XyZoom = "xy";

        public List<object> XData { get; private set; }
        public List<SeriesYAxis> YData { get; private set; }

        public ChartData(List<object> xSource, List<SeriesYAxis> ySource)
        {
            XData = xSource;
            YData = ySource;
        }

        public ChartData()
        {
        }

        public SeriesChartType ChartType { get { return YData.First().Type; } }

        public string GetData()
        {
            return string.Format("[{0}]", GenerateJsonDataString());
        }

        private string GenerateJsonDataString()
        {
            var dataString = string.Empty;

            switch (ChartType)
            {
                case SeriesChartType.Pie:
                    // Flotr2 cannot handle more than one pie graph/series per chart
                    var yPieAxis = YData.First();

                    for (var i = 0; i < XData.Count; i++)
                    {
                        var chartData = new ChartDataArray
                            {
                                Label = XData[i].ToString(),
                                Data = new List<ChartDataArrayCoordinates>(),
                                Type = SeriesChartType.Pie
                            };

                        chartData.Data.Add(new ChartDataArrayCoordinates
                            {
                                X = 0,
                                Y = yPieAxis.Data.ToList()[i]
                            });

                        dataString += chartData.ReturnJsonDataString();
                    }

                    break;
                default:
                    foreach (var yAxis in YData)
                    {
                        var chartData = new ChartDataArray
                            {
                                Label = yAxis.Name,
                                Data = new List<ChartDataArrayCoordinates>(),
                                Type = yAxis.Type
                            };

                        for (var i = 0; i < XData.Count; i++)
                        {
                            var xSource = ChartIsBarOrStackedBar() ? yAxis.Data.ToList()[i] : XData.ToList()[i];
                            var ySource = ChartIsBarOrStackedBar() ? XData.ToList()[i] : yAxis.Data.ToList()[i];

                            var xIsDateTime = xSource is DateTime;

                            // Flotr2 requires dates to be created in this manner to work
                            var x = xIsDateTime
                                        ? string.Format("(new Date(Date.UTC({0},{1},{2},{3},{4},{5}))).getTime()",
                                                        xSource.ToDateTime().Year,
                                                        xSource.ToDateTime().Month,
                                                        xSource.ToDateTime().Day,
                                                        xSource.ToDateTime().Hour,
                                                        xSource.ToDateTime().Minute,
                                                        xSource.ToDateTime().Second)
                                        : xSource;

                            chartData.Data.Add(new ChartDataArrayCoordinates
                                {
                                    X = !xIsDateTime && x is string ? i : x,
                                    Y = ySource is string || ySource is DateTime ? i : ySource // dates on y-axis are not supported
                                });
                        }

                        dataString += chartData.ReturnJsonDataString();
                    }
                    break;
            }


            return dataString;
        }



        public List<object> GetAxisDataSourceList(ChartAxis axis)
        {
            // If the chart is a bar or stacked bar, the axes will be inverted
            var list = ChartIsBarOrStackedBar()
                           ? axis == ChartAxis.XAxis
                                 ? YData.SelectMany(a => a.Data).ToList()
                                 : XData.ToList()
                           : axis == ChartAxis.XAxis
                                 ? XData.ToList()
                                 : YData.SelectMany(a => a.Data).ToList();

            return list;
        }

        public string GetAxisTickDecimalPlaces(ChartAxis axis)
        {
            var list = GetAxisDataSourceList(axis);

            if (!list.Any()) return JsNull;

            // prevents flotr2 from displaying decimal places on an axis with string values
            var decPlaces = list.First() is string
                                ? "0"
                                : JsNull;

            return decPlaces;
        }

        // work-around to Flotr2 only allowing numeric or date values on axes
        public string GenerateAxisTicks(ChartAxis axis)
        {
            var list = GetAxisDataSourceList(axis);

            if (axis == ChartAxis.XAxis && GetXAxisMode() == ModeTime)
                return JsNull;

            if (!list.Any() || (list.First().GetType() != typeof(string) && list.First().GetType() != typeof(DateTime)))
                return JsNull;

            var ticks = "[";

            for (var i = 0; i < list.Count; i++)
            {
                ticks += string.Format(TickArrayJsonString, i, list[i]);
            }

            ticks += "]";
            return ticks;
        }



        public string GetXAxisMode()
        {
            var list = GetAxisDataSourceList(ChartAxis.XAxis);

            if (list.First() is DateTime)
                return ModeTime;

            return ModeNormal;
        }

        public string GetSelectionMode()
        {
            // Flotr2 does not support zooming for pie charts
            return YData.First().Type == SeriesChartType.Pie ? string.Empty : XyZoom;
        }

        public bool ChartIsBarOrStackedBar()
        {
            return ChartType == SeriesChartType.Bar || ChartType == SeriesChartType.StackedBar;
        }

        public bool CanMouseTrack()
        {
            // Flotr2 does not support mouse tracking for grouped & stacked bar/column charts
            return YData.All(y => y.Type != SeriesChartType.Bar) && YData.All(y => y.Type != SeriesChartType.StackedBar)
                                                                 && YData.All(y => y.Type != SeriesChartType.Column)
                                                                 && YData.All(y => y.Type != SeriesChartType.StackedColumn);
        }
    }
}