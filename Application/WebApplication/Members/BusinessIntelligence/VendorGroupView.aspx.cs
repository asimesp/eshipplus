﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence
{
    public partial class VendorGroupView : MemberPageBase, IVendorGroupView
    {
        private const string VendorsHeader = "Vendors";

        private const string SaveFlag = "S";

        public static string PageAddress { get { return "~/Members/BusinessIntelligence/VendorGroupView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.VendorGroup; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.ReportsBlue; } }

        public event EventHandler<ViewEventArgs<VendorGroup>> Save;
        public event EventHandler<ViewEventArgs<VendorGroup>> Delete;
        public event EventHandler<ViewEventArgs<VendorGroup>> Lock;
        public event EventHandler<ViewEventArgs<VendorGroup>> UnLock;
        public event EventHandler<ViewEventArgs<VendorGroup>> LoadAuditLog;

        public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs) { auditLogs.Load(logs); }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == SaveFlag)
            {
                // unlock and return to read-only
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<VendorGroup>(new VendorGroup(hidVendorGroupId.Value.ToLong(), false)));
                SetEditStatus(false);
                memberToolBar.ShowUnlock = false;
            }

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Button = MessageButton.Ok;
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;

            litErrorMessages.Text = messages.HasErrors()
                                        ? string.Join(WebApplicationConstants.HtmlBreak,
                                                      messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
                                                        Select(m => m.Message).ToArray())
                                        : string.Empty;
        }

        public void FailedLock(Lock @lock)
        {
            memberToolBar.ShowNew = false;
            memberToolBar.ShowSave = false;
            memberToolBar.ShowDelete = false;
            memberToolBar.ShowUnlock = false;

            SetEditStatus(false);

            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        private void SetEditStatus(bool enabled)
        {
            pnlDetails.Enabled = enabled;
            pnlVendors.Enabled = enabled;

            memberToolBar.EnableSave = enabled;
            memberToolBar.EnableImport = enabled;
            litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;
        }

        public void Set(VendorGroup @group)
        {
            LoadVendorGroup(@group);
            SetEditStatus(!@group.IsNew);
        }


        private void UpdateVendorGroupMaps(VendorGroup group)
        {
            var maps = lstVendors.Items
                .Select(item => new VendorGroupMap
                {
                    Vendor = new Vendor(item.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong()),
                    VendorGroup = group,
                    TenantId = ActiveUser.TenantId,
                })
                .ToList();
            group.VendorGroupMaps = maps;
        }

        private void LoadVendorGroup(VendorGroup group)
        {
            hidVendorGroupId.Value = group.Id.ToString();
            txtGroupName.Text = group.Name;
            txtDescription.Text = group.Description;

            var vendors = group.IsNew ? new List<Vendor>() : group.RetrieveGroupVendors();
            lstVendors.DataSource = vendors.OrderBy(c => c.Name).ThenBy(c => c.VendorNumber);
            lstVendors.DataBind();

            tabVendors.HeaderText = group.IsNew ? new List<Vendor>().BuildTabCount(VendorsHeader) : group.RetrieveGroupVendors().BuildTabCount(VendorsHeader);

            memberToolBar.ShowUnlock = group.HasUserLock(ActiveUser, group.Id);
        }


        private void ProcessTransferredRequest(long groupId)
        {
            if (groupId != default(long))
            {
                var group = new VendorGroup(groupId);
                LoadVendorGroup(group);

                if (LoadAuditLog != null)
                    LoadAuditLog(this, new ViewEventArgs<VendorGroup>(group));
            }
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            new VendorGroupHandler(this).Initialize();

            memberToolBar.ShowSave = Access.Modify;
            memberToolBar.ShowDelete = Access.Remove;
            memberToolBar.ShowNew = Access.Modify;
            memberToolBar.ShowEdit = Access.Modify;
            memberToolBar.ShowImport = Access.Modify;

            vendorGroupFinder.OpenForEditEnabled = Access.Modify;

            if (IsPostBack) return;
            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.VendorGroupsImportTemplate });

            if (Session[WebApplicationConstants.TransferVendorGroupId] != null)
            {
                ProcessTransferredRequest(Session[WebApplicationConstants.TransferVendorGroupId].ToLong());
                Session[WebApplicationConstants.TransferVendorGroupId] = null;
            }

            SetEditStatus(false);
        }

        protected void OnNewClicked(object sender, EventArgs e)
        {
            litErrorMessages.Text = string.Empty;

            DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
            LoadVendorGroup(new VendorGroup());
            SetEditStatus(true);
        }

        protected void OnEditClicked(object sender, EventArgs e)
        {
            var group = new VendorGroup(hidVendorGroupId.Value.ToLong());

            if (Lock == null || @group.IsNew) return;
            SetEditStatus(true);
            memberToolBar.ShowUnlock = true;
            Lock(this, new ViewEventArgs<VendorGroup>(@group));
        }

        protected void OnUnlockClicked(object sender, EventArgs e)
        {
            var group = new VendorGroup(hidVendorGroupId.Value.ToLong(), false);
            if (UnLock != null && !group.IsNew)
            {
                SetEditStatus(false);
                memberToolBar.ShowUnlock = false;
                UnLock(this, new ViewEventArgs<VendorGroup>(group));
            }
        }

        protected void OnSaveClicked(object sender, EventArgs e)
        {
            var groupId = hidVendorGroupId.Value.ToLong();

            var @group = new VendorGroup(groupId, groupId != default(long))
            {
                Name = txtGroupName.Text,
                Description = txtDescription.Text
            };
            UpdateVendorGroupMaps(@group);

            if (@group.IsNew)
                @group.TenantId = ActiveUser.TenantId;

            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<VendorGroup>(@group));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<VendorGroup>(@group));
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var vendorGroup = new VendorGroup(hidVendorGroupId.Value.ToLong(), false);

            if (Delete != null)
                Delete(this, new ViewEventArgs<VendorGroup>(vendorGroup));

            vendorGroupFinder.Reset();
            SetEditStatus(false);
        }

        protected void OnFindClicked(object sender, EventArgs e)
        {
            vendorGroupFinder.Visible = true;
        }

        protected void OnImportClicked(object sender, EventArgs e)
        {
            fileUploader.Title = "VENDOR GROUPS IMPORT";
            fileUploader.Instructions = @"**Please refer to user guide for import file format";
            fileUploader.Visible = true;
        }


        protected void OnAddVendorClicked(object sender, EventArgs e)
        {
            vendorFinder.Visible = true;
        }

        protected void OnClearVendorsClicked(object sender, EventArgs e)
        {
            lstVendors.DataSource = new List<Permission>();
            lstVendors.DataBind();
            tabVendors.HeaderText = VendorsHeader;
            athtuTabUpdater.SetForUpdate(tabVendors.ClientID, VendorsHeader);
        }

        protected void OnRemoveVendorClicked(object sender, ImageClickEventArgs e)
        {
            var button = (ImageButton)sender;

            var vendors = lstVendors.Items
                .Select(item => new
                {
                    Id = item.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong(),
                    Name = item.FindControl("litVendorName").ToLiteral().Text,
                    VendorNumber = item.FindControl("lblVendorNumber").ToLabel().Text,
                    Active = item.FindControl("hidActive").ToCustomHiddenField().Value.ToBoolean(),
                })
                .ToList();

            vendors.RemoveAt(button.FindControl("hidVendorIndex").ToCustomHiddenField().Value.ToInt());

            lstVendors.DataSource = vendors;
            lstVendors.DataBind();

            tabVendors.HeaderText = vendors.BuildTabCount(VendorsHeader);
            athtuTabUpdater.SetForUpdate(tabVendors.ClientID, vendors.BuildTabCount(VendorsHeader));
        }

        protected void OnVendorFinderMultiItemSelected(object sender, ViewEventArgs<List<Vendor>> e)
        {
            var vendors = lstVendors.Items
                .Select(c => new
                {
                    Id = c.FindControl("hidVendorId").ToCustomHiddenField().Value,
                    Name = c.FindControl("litVendorName").ToLiteral().Text,
                    VendorNumber = c.FindControl("lblVendorNumber").ToLabel().Text,
                    Active = c.FindControl("hidActive").ToCustomHiddenField().Value.ToBoolean()
                }).ToList();


            var newVendors = e.Argument
                .Select(c => new { Id = c.Id.ToString(), c.Name, c.VendorNumber, c.Active })
                .Where(c => !vendors.Select(s => s.Id).Contains(c.Id));
            vendors.AddRange(newVendors);

            lstVendors.DataSource = vendors.OrderBy(c => c.Name).ThenBy(c => c.VendorNumber);
            lstVendors.DataBind();

            tabVendors.HeaderText = vendors.BuildTabCount(VendorsHeader);

            vendorFinder.Visible = false;
        }

        protected void OnVendorFinderSelectionCancelled(object sender, EventArgs eventArgs)
        {
            vendorFinder.Visible = false;
        }


        protected void OnVendorGroupFinderItemSelected(object sender, ViewEventArgs<VendorGroup> e)
        {
            litErrorMessages.Text = string.Empty;

            if (hidVendorGroupId.Value.ToLong() != default(long))
            {
                var oldGroup = new VendorGroup(hidVendorGroupId.Value.ToLong(), false);
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<VendorGroup>(oldGroup));
            }

            var @group = e.Argument;

            LoadVendorGroup(@group);

            vendorGroupFinder.Visible = false;

            if (vendorGroupFinder.OpenForEdit && Lock != null)
            {
                SetEditStatus(true);
                memberToolBar.ShowUnlock = true;
                Lock(this, new ViewEventArgs<VendorGroup>(@group));
            }
            else
            {
                SetEditStatus(false);
            }

            vendorFinder.Reset();

            if (LoadAuditLog != null) LoadAuditLog(this, new ViewEventArgs<VendorGroup>(@group));
        }

        protected void OnVendorGroupFinderSelectionCancelled(object sender, EventArgs eventArgs)
        {
            vendorGroupFinder.Visible = false;
        }


        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;
            messageBox.Visible = true;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 2;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var vendorSearch = new VendorSearch();
            var vParameters = chks
                .Select(i => i.Line[0])
                .Distinct()
                .Select(n =>
                {
                    var p = AccountingSearchFields.VendorNumber.ToParameterColumn();
                    p.DefaultValue = n;
                    p.Operator = Operator.Equal;
                    return p;
                })
                .ToList();
            var vendorList = vendorSearch.FetchVendors(vParameters, ActiveUser.TenantId);
            var vendorNumbers = vendorList.Select(c => c.VendorNumber).ToList();
            msgs.AddRange(chks.CodesAreValid(0, vendorNumbers, new Vendor().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var uploadedVendors = chks
                .Select(i => vendorList.First(v => v.VendorNumber == i.Line[0]))
                .ToList();

            var vendors = lstVendors.Items
                .Select(c => new
                {
                    Id = (c.FindControl("hidVendorId")).ToCustomHiddenField().Value,
                    Name = (c.FindControl("litVendorName")).ToLiteral().Text,
                    VendorNumber = (c.FindControl("lblVendorNumber")).ToLabel().Text,
                    Active = (c.FindControl("hidActive")).ToCustomHiddenField().Value.ToBoolean()
                }).ToList();

            // add new vendors
            vendors.AddRange(uploadedVendors
                                .Select(c => new { Id = c.Id.ToString(), c.Name, c.VendorNumber, c.Active })
                                .Where(c => !vendors.Select(s => s.Id).Contains(c.Id))
                                .ToList());

            lstVendors.DataSource = vendors.OrderBy(c => c.Name).ThenBy(c => c.VendorNumber);
            lstVendors.DataBind();
            tabVendors.HeaderText = vendors.BuildTabCount(VendorsHeader);

            fileUploader.Visible = false;
        }
    }
}