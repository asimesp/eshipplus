﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.BusinessIntelligence;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence
{
    public partial class VendorTerminalLocatorView : MemberPageBase, IVendorTerminalLocatorView
    {
        private const string ExportFlag = "E";

        public override ViewCode PageCode { get { return ViewCode.VendorTerminalLocator; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.ReportsBlue; } }

        public static string PageAddress { get { return "~/Members/BusinessIntelligence/VendorTerminalLocatorView.aspx"; } }

        public event EventHandler<ViewEventArgs<VendorTerminalLocatorSearchCriteria>> Search;

        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new VendorTerminalLocatorHandler(this);
            handler.Initialize();

            if (IsPostBack) return;

            var countryId = ActiveUser.Tenant.DefaultCountryId.ToString();
            ddlTerminalCountries.SelectedValue = countryId.GetString();
        }

        public void DisplaySearchResult(List<VendorTerminalLocatorDto> terminals)
        {
            var nTerminals = terminals
                 .Select(t => new
                 {
                     t.Name,
                     Vendor = string.Format("{0} - {1}", t.VendorNumber, t.VendorName),
                     t.VendorId,
                     Address1 = string.Format("{0} {1}", t.Street1, t.Street2),
                     Address2 = string.Format("{0}, {1} {2}", t.City, t.State, t.PostalCode),
                     t.CountryId,
                     t.MC,
                     t.DOT,
                     t.Scac,
                     t.Active,
                     t.ContactName,
                     t.Phone,
                     t.Mobile,
                     t.Fax,
                     t.Email,
                     Distance = t.Distance.ToString("n2")
                 }).ToList();

            litRecordCount.Text = nTerminals.BuildRecordCount();
            lstTerminals.DataSource = nTerminals;
            lstTerminals.DataBind();

            if (hidFlag.Value == ExportFlag)
            {
                var eTerminals = terminals
                    .Select(t => new[]
                                     {
                                         t.Name,
                                         t.Street1,
                                         t.Street2,
                                         t.City,
                                         t.State,
                                         t.PostalCode,
										 new Country(t.CountryId).Name,
                                         t.ContactName,
                                         t.Phone,
                                         t.Mobile,
                                         t.Fax,
                                         t.Email,
                                         t.Distance.ToString(),
                                         t.Scac,
                                         t.VendorName,
                                         t.VendorNumber,
                                         t.MC,
                                         t.DOT,
                                         t.Active.ToString()
                                     }.TabJoin())
                    .ToList();
                eTerminals.Insert(0, new[] {
                                            "Name",
                                            "Street1", 
                                            "Street2", 
                                            "City", 
                                            "State",
                                            "PostalCode",
                                            "Country",
                                            "ContactName",
                                            "Phone",
                                            "Mobile",
                                            "Fax",
                                            "Email",
                                            "Distance",
                                            "SCAC",                                            
                                            "VendorName",
                                            "VendorNumber",
                                            "MC",
                                            "DOT",
                                            "Active"
                                            }.TabJoin());

                Response.Export(eTerminals.ToArray().NewLineJoin(), "VendorTerminals.txt");

            }
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Button = MessageButton.Ok;
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        private void DoSearch()
        {
            var criteria = new VendorTerminalLocatorSearchCriteria
            {
                PostalCode = txtPostalCode.Text,
                CountryId = ddlTerminalCountries.SelectedValue.ToLong(),
                Radius = txtRadius.Text.ToDecimal(),
                UseKilometers = false
            };

            if (Search != null) Search(this, new ViewEventArgs<VendorTerminalLocatorSearchCriteria>(criteria));
        }


        protected void OnExportClicked(object sender, EventArgs e)
        {
            hidFlag.Value = ExportFlag;
            DoSearch();
        }

        protected void OnSearchClicked(object sender, EventArgs e)
        {
            DoSearch();
        }
    }
}