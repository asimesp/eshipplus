﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Reports;
using LogisticsPlus.Eship.Processor.Searches.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence
{
    using System.IO;

    public partial class ReportRunView : MemberPageBase
    {
        public static string PageAddress { get { return "~/Members/BusinessIntelligence/ReportRunView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.ReportRun; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.ReportsBlue; } }

        public List<string> ReportColumnNames
        {
            get { return ViewState["ReportRunReportColumnNames"] as List<string> ?? new List<string>(); }
            set { ViewState["ReportRunReportColumnNames"] = value; }
        }

        public Dictionary<string, Type> ReportColumnDataTypes
        {
            get
            {
                return ViewState["ReportRunReportColumnDataTypes"] as Dictionary<string, Type> ??
                       new Dictionary<string, Type>();
            }
            set { ViewState["ReportRunReportColumnDataTypes"] = value; }
        }


        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }


        private List<ParameterColumn> GetCurrentRunParameters()
        {
            var currentRunParameters = lstReportConfigurationParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter()).ToList();
            return currentRunParameters;
        }

        private List<RequiredValueParameter> GetCurrentRunRequiredParameters()
        {
            var currentRunParameters = lstReportConfigurationRequiredParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveRequiredParameter()).ToList();
            return currentRunParameters;
        }



        private void ConfigureReportConfigurationPopup()
        {
            pnlReportResults.Visible = false;

            var filter = txtFilter.Text.Trim().ToLower();
            var configList = new ReportConfigurationSearch().FetchReportConfigurationsVisibleToUser(ActiveUser.Id, ActiveUser.TenantId);
            var configurations = string.IsNullOrEmpty(filter) ? configList : configList.Where(rc => rc.Name.ToLower().Contains(filter) || rc.Description.ToLower().Contains(filter)).ToList();

            rptReportConfigurations.DataSource = configurations;
            rptReportConfigurations.DataBind();

            pnlNoReports.Visible = !configurations.Any();
            pnlReportConfigurations.Visible = true;
            pnlDimScreen.Visible = true;

			txtFilter.Focus();
        }

        private void LoadReportConfigurationParameters(ReportConfiguration reportConfiguration)
        {
            var customization = reportConfiguration.SerializedCustomization.FromXml<ReportCustomization>();

            litConfigurationName.Text = reportConfiguration.Name;
            litConfigurationDescription.Text = reportConfiguration.Description;

            lstReportConfigurationParameters.DataSource = customization == null || !customization.Parameters.Any()
                                                            ? new List<ParameterColumn>()
                                                            : customization.Parameters.OrderBy(p => p.ReportColumnName).ToList();
            lstReportConfigurationParameters.DataBind();

            lstReportConfigurationRequiredParameters.DataSource = customization == null || !customization.RequiredValueParameters.Any()
                                                                    ? new List<RequiredValueParameter>()
                                                                    : customization.RequiredValueParameters;
            lstReportConfigurationRequiredParameters.DataBind();
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;

            // setup
            ReportColumnDataTypes = new Dictionary<string, Type>();
            ddlExtension.DataSource = ProcessorUtilities.GetExportFileExtension()
                .Select(ex => new ViewListItem(ex.Value, ex.Key.ToString()))
                .ToList();
            ddlExtension.DataBind();

            ddlCustomGroupOperator.DataSource = new List<ViewListItem>{new ViewListItem {Text = "Equal", Value = string.Empty}};
            ddlCustomGroupOperator.DataBind();

            ddlVendorGroupOperator.DataSource = new List<ViewListItem> { new ViewListItem { Text = "Equal", Value = string.Empty } };
            ddlVendorGroupOperator.DataBind();

            ConfigureReportConfigurationPopup();
        }

        protected void OnSelectReportConfigurationClicked(object sender, EventArgs e)
        {
            lstReportConfigurationParameters.DataSource = null;
            lstReportConfigurationParameters.DataBind();

            lstParameters.DataSource = null;
            lstParameters.DataBind();

            var button = (LinkButton)sender;

            hidReportConfigurationId.Value = string.Empty;
            hidReportConfigurationId.Value = button.Parent.FindControl("hidReportConfigurationId").ToCustomHiddenField().Value.ToLong().ToString();
            var reportConfigurationId = hidReportConfigurationId.Value.ToLong();

            var configuration = new ReportConfiguration(reportConfigurationId);

            if (configuration.ReportTemplate.CanFilterByCustomerGroup)
            {
                pnlCustomerGroups.Visible = true;
                var viewListItems = new CustomerGroupSearch().FetchCustomerGroups(new List<ParameterColumn>(), ActiveUser.TenantId).Select(c => new ViewListItem(c.Name, c.Id.ToString())).ToList();
                viewListItems.Add(new ViewListItem(WebApplicationConstants.NotApplicable, default(long).ToString()));

                ddlCustomerGroup.DataSource = viewListItems;
                ddlCustomerGroup.DataBind();

                ddlCustomerGroup.SelectedValue = ddlCustomerGroup.ContainsValue(configuration.CustomerGroupId.ToString())
                                                    ? configuration.CustomerGroupId.ToString()
                                                    : default(long).ToString();
                ddlCustomerGroup.Enabled = !configuration.ReadonlyCustomerGroupFilter;
            }
            else
            {
                pnlCustomerGroups.Visible = false;
            }

            if (configuration.ReportTemplate.CanFilterByVendorGroup)
            {
                pnlVendorGroups.Visible = true;
                var viewListItems = new VendorGroupSearch().FetchVendorGroups(new List<ParameterColumn>(), ActiveUser.TenantId).Select(v => new ViewListItem(v.Name, v.Id.ToString())).ToList();
                viewListItems.Add(new ViewListItem(WebApplicationConstants.NotApplicable, default(long).ToString()));

                ddlVendorGroup.DataSource = viewListItems;
                ddlVendorGroup.DataBind();


                ddlVendorGroup.SelectedValue = ddlVendorGroup.ContainsValue(configuration.VendorGroupId.ToString())
                                                ? configuration.VendorGroupId.ToString()
                                                : default(long).ToString();
                ddlVendorGroup.Enabled = !configuration.ReadonlyVendorGroupFilter;
            }
            else
            {
                pnlVendorGroups.Visible = false;
            }

            LoadReportConfigurationParameters(configuration);

            pnlReportConfigurations.Visible = false;
            pnlDimScreen.Visible = false;
        }


        protected void OnReportNewClicked(object sender, EventArgs e)
        {
            ConfigureReportConfigurationPopup();
        }

	    protected void OnExportClicked(object sender, EventArgs e)
	    {
		    var reportConfiguration = new ReportConfiguration(hidReportConfigurationId.Value.ToLong());
		    var customization = reportConfiguration.SerializedCustomization.FromXml<ReportCustomization>();

		    try
		    {
			    customization.Parameters = GetCurrentRunParameters();
			    customization.RequiredValueParameters = GetCurrentRunRequiredParameters();

			    reportConfiguration.SerializedCustomization = customization.ToXml();
		        
                var rcUpdateHandler = new ReportConfigurationUpdateHandler();

		        Exception ex;
                rcUpdateHandler.GenerateExportAuditLog(reportConfiguration.Id, ActiveUser,ddlExtension.SelectedItem.Text,out ex);

                if (ex != null) throw ex;

			    var engine = new ReportEngine();
		        var canFilterByCustomerGroup = reportConfiguration.ReportTemplate.CanFilterByCustomerGroup;
		        var canFilterByVendorGroup = reportConfiguration.ReportTemplate.CanFilterByVendorGroup;
		        ReportData data;

		        if (!canFilterByCustomerGroup && !canFilterByVendorGroup)
		            data = engine.GenerateReport(reportConfiguration, ActiveUser,
		                WebApplicationSettings.MaxCellThresholdForBackgroundReports,
		                WebApplicationSettings.EnableBackgroundReportRunProcessor);
		        else
		        {
		            var customerGroupId = ddlCustomerGroup.SelectedValue.ToLong();
		            var vendorGroupId = ddlVendorGroup.SelectedValue.ToLong();

		            data = engine.GenerateReport(reportConfiguration, ActiveUser, customerGroupId, vendorGroupId,
		                WebApplicationSettings.MaxCellThresholdForBackgroundReports,
		                WebApplicationSettings.EnableBackgroundReportRunProcessor);
		        }


                if (data == null)
		        {
		            DisplayMessages(new[] { ValidationMessage.Information("Report returned too many results to be exported and has been queued. An email confirmation will be sent to you once the report completes.") });
		            return;
		        }

		        var reportData = data;

		        if (reportData.Table.Rows.Count == 0)
		        {
		            DisplayMessages(new[] { ValidationMessage.Information("Report returned no results.") });
		            return;
		        }

			    var extension = ddlExtension.SelectedValue.ToEnum<ReportExportExtension>();
			    var filename = string.Format("{0}-{1:yyyyMMdd_hhmmss}.{2}", data.HasTable ? data.Table.TableName : "Report",
			                                 DateTime.Now,
			                                 extension);

                FileInfo info;

                switch (extension)
                {
                    case ReportExportExtension.txt:
                        info = data.TransformTabDelimited(Server);
                        break;
                    case ReportExportExtension.xlsx:
                        info = data.TransformExcel(Server, customization.DataColumns, customization.Charts);
                        break;
                    case ReportExportExtension.csv:
                        info = data.TransformCsv(Server);
                        break;
                    default:
                        info = data.TransformCsv(Server);
                        break;
                }
                
                Response.Export(info, filename);
				info.Delete();
		    }
		    catch (Exception ex)
		    {
		        pnlReportResults.Visible = false;
			    DisplayMessages(new[] {ValidationMessage.Error("There was an error processing the report configuration.")});
			    ErrorLogger.LogError(ex, Context);
		    }
	    }


	    protected void OnRequiredParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (RequiredValueParameter)item.DataItem;

            if (parameter == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter);
        }


        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var reportConfigurationId = hidReportConfigurationId.Value.ToLong();
            var reportConfiguration = new ReportConfiguration(reportConfigurationId);
            var templateCustomization = reportConfiguration.ReportTemplate.DefaultCustomization.FromXml<ReportCustomization>();
            var configCustomization = reportConfiguration.SerializedCustomization.FromXml<ReportCustomization>();

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            var allColumns = templateCustomization.DataColumns.Where(t => !t.Custom).ToList();
            allColumns.AddRange(configCustomization.DataColumns.Where(t => t.Custom).ToList());

            detailControl.LoadParameter(allColumns, parameter);
        }

        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            pnlReportResults.Visible = false;

            var reportConfiguration = new ReportConfiguration(hidReportConfigurationId.Value.ToLong());
            var customization = reportConfiguration.SerializedCustomization.FromXml<ReportCustomization>();

            var columns = customization.DataColumns
                .Select(p => new
                {
                    p.Name,
                    p.DataType,
                    ReadOnly = false,
                    Operator = Operator.Equal
                })
                .ToList();

            lstParameters.DataSource = columns.OrderBy(p => p.Name);
            lstParameters.DataBind();

            pnlParameterAdd.Visible = true;
            pnlDimScreen.Visible = true;
        }

        protected void OnParameterAddDoneClicked(object sender, EventArgs e)
        {
            var parameters = lstParameters.Items
                .Select(i => new
                {
                    Name = i.FindControl("litParameterName").ToLiteral().Text,
                    DataType = i.FindControl("hidParameterDataType").ToCustomHiddenField().Value,
                    ReadOnly = i.FindControl("hidParameterReadOnly").ToCustomHiddenField().Value,
                    Operator = i.FindControl("hidParameterOperator").ToCustomHiddenField().Value,
                    Selection = i.FindControl("chkSelection").ToAltUniformCheckBox().Checked
                })
                .Where(p => p.Selection)
                .ToList();


            var currentRunParameters = GetCurrentRunParameters();

            var parameterColumns = parameters.Select(p => new ParameterColumn
            {
                ReportColumnName = p.Name,
                DefaultValue = string.Empty,
                ReadOnly = false,
                Operator = p.Operator.ToEnum<Operator>()
            });

            currentRunParameters.AddRange(parameterColumns);

            lstReportConfigurationParameters.DataSource = currentRunParameters;
            lstReportConfigurationParameters.DataBind();

            pnlParameterAdd.Visible = false;
            pnlDimScreen.Visible = false;
        }


        protected void OnParameterAddCloseClicked(object sender, EventArgs e)
        {
            pnlParameterAdd.Visible = false;
            pnlDimScreen.Visible = false;
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            pnlReportResults.Visible = false;
            var parameters = GetCurrentRunParameters();

            parameters.RemoveAt(e.Argument);

            lstReportConfigurationParameters.DataSource = parameters;
            lstReportConfigurationParameters.DataBind();
        }


        protected void OnReportRunClicked(object sender, EventArgs e)
        {
            pnlReportResults.Visible = false;
            pnlReportSummaries.Visible = false;
            pnlReportData.Visible = false;

            var reportConfiguration = new ReportConfiguration(hidReportConfigurationId.Value.ToLong());
            var customization = reportConfiguration.SerializedCustomization.FromXml<ReportCustomization>();
            ReportData data;

            try
            {

                customization.Parameters = GetCurrentRunParameters();
                customization.RequiredValueParameters = GetCurrentRunRequiredParameters();

                reportConfiguration.SerializedCustomization = customization.ToXml();

                var rcUpdateHandler = new ReportConfigurationUpdateHandler();

                Exception ex;
                rcUpdateHandler.GenerateRunAuditLog(reportConfiguration.Id, ActiveUser, out ex);

                if (ex != null) throw ex;

                var engine = new ReportEngine();

                var canFilterByCustomerGroup = reportConfiguration.ReportTemplate.CanFilterByCustomerGroup;
                var canFilterByVendorGroup = reportConfiguration.ReportTemplate.CanFilterByVendorGroup;

                if (!canFilterByCustomerGroup && !canFilterByVendorGroup)
                    data = engine.GenerateReport(reportConfiguration, ActiveUser,
                        WebApplicationSettings.MaxCellThresholdForBackgroundReports,
                        WebApplicationSettings.EnableBackgroundReportRunProcessor);
                else
                {
                    var customerGroupId = ddlCustomerGroup.SelectedValue.ToLong();
                    var vendorGroupId = ddlVendorGroup.SelectedValue.ToLong();

                    data = engine.GenerateReport(reportConfiguration, ActiveUser, customerGroupId, vendorGroupId,
                        WebApplicationSettings.MaxCellThresholdForBackgroundReports,
                        WebApplicationSettings.EnableBackgroundReportRunProcessor);
                }

                if (data == null)
                {
                    DisplayMessages(new[] { ValidationMessage.Information("Report returned too many results to be displayed within the browser and has been queued. An email confirmation will be sent to you once the report completes.") });
                    return;
                }

                if (data.Table.Rows.Count == 0)
                {
                    DisplayMessages(new[] { ValidationMessage.Information("Report returned no results.") });
                    return;
                }
            }
            catch (Exception ex)
            {
                DisplayMessages(new[] { ValidationMessage.Error("There was an error processing the report configuration.") });
                ErrorLogger.LogError(ex, Context);
                return;
            }
            

            //Summary

            if (data.HasSummaries)
            {
                var summaries = data
                    .Summaries
                    .Select(s => new
                    {
                        s.Key,
                        Value = s.Value.GetString().Contains(".") ? s.Value.ToDecimal().ToString("n2") : s.Value
                    })
                    .ToList();

                rptSummaries.DataSource = summaries;
                rptSummaries.DataBind();

                pnlReportSummaries.Visible = true;
            }

            //Data

            var reportDisplay = new List<ReportDisplay>
                {
                    new ReportDisplay
                        {
                            Charts = customization.Charts.Where(c => c.DataSourceType == ChartDataSourceType.Original).ToList(),
                            Data = data.Table,
                            HideOriginalTable = customization.HideDataTable
                        }
                };

            reportDisplay.AddRange(customization.Pivots.Select(pivot => new ReportDisplay
                {
                    Charts = customization.Charts.Where(c => c.PivotGuidId == pivot.GuidId).ToList(),
                    Data = data.PivotTables.FirstOrDefault(d => d.TableName == pivot.TableName),
                    Pivot = pivot
                }));

            rptData.DataSource = reportDisplay.Where(rd => rd.Data != null).ToList();
            rptData.DataBind();

            pnlReportData.Visible = true;
            pnlReportResults.Visible = true;
        }

        protected void OnDataItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var reportDisplay = (ReportDisplay) e.Item.DataItem;

            if (!reportDisplay.HideOriginalTable)
            {
                var rptTableColumns = e.Item.FindControl("rptTableColumns").ToRepeater();
                var rptTableRows = e.Item.FindControl("rptTableRows").ToRepeater();

                ReportColumnDataTypes.Clear();
                ReportColumnNames.Clear();

                foreach (
                    var col in
                        reportDisplay.Data.Columns.Cast<DataColumn>()
                                     .Where(col => !ReportColumnDataTypes.ContainsKey(col.ColumnName.ToLower())))
                {
                    ReportColumnDataTypes.Add(col.ColumnName.ToLower(), col.DataType);
                }

                ReportColumnNames =
                    reportDisplay.Data.Columns.Cast<DataColumn>().Select(c => c.ColumnName.ToLower()).ToList();

                // check column types (in case of select * query where customization will have no data columns)
                if (!ReportColumnDataTypes.Any() && reportDisplay.Data.Rows.Count > 0)
                {
                    // attempt to get datatypes
                    var row = reportDisplay.Data.Rows[0];
                    foreach (var c in ReportColumnNames.Distinct())
                    {
                        var value = row[c].GetString();
                        if (value.Length == 0) ReportColumnDataTypes.Add(c.ToLower(), typeof (string));
                        else if (value.Contains(".") && value.ToDecimal() != default(decimal))
                            ReportColumnDataTypes.Add(c.ToLower(), typeof (decimal));
                        else if (value.ToInt() != default(int))
                            ReportColumnDataTypes.Add(c.ToLower(), typeof (int));
                        else if (value.ToDateTime() != DateUtility.SystemEarliestDateTime)
                            ReportColumnDataTypes.Add(c.ToLower(), typeof (DateTime));
                        else
                            ReportColumnDataTypes.Add(c.ToLower(), typeof (string));
                    }
                }


                rptTableColumns.DataSource = ReportColumnNames
                    .Select(c =>
                        {
                            var dt = ReportColumnDataTypes.ContainsKey(c)
                                         ? ReportColumnDataTypes[c]
                                         : typeof (string); // default
                            return new
                                {
                                    Name = c,
                                    Align = dt == typeof (bool)
                                                ? "center"
                                                : dt == typeof (decimal) || dt == typeof (int) || dt == typeof (float)
                                                      ? "right"
                                                      : "left"
                                };
                        });

                rptTableColumns.DataBind();

                rptTableRows.DataSource = reportDisplay.Data.Rows;
                rptTableRows.DataBind();
            }

            var charts = reportDisplay.Charts.Select(chart => new ReportChart
                    {
                        Data = reportDisplay.Data,
                        Chart = chart,
                        Pivot = reportDisplay.Pivot
                    }).ToList();

            var rptCharts = e.Item.FindControl("rptCharts").ToRepeater();
            rptCharts.DataSource = charts;
            rptCharts.DataBind();
        }

        protected void OnRowsItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var rpt = e.Item.FindControl("rptRowColumns").ToRepeater();
            if (rpt == null) return;
            var data = (DataRow)e.Item.DataItem;
            if (data == null) return;

            var columns = ReportColumnNames
                .Select(n =>
                {
                    var dt = ReportColumnDataTypes.ContainsKey(n) ? ReportColumnDataTypes[n] : typeof(string);
                    return
                        new
                        {
                            Value = dt == typeof(int) || dt == typeof(long)
                                        ? data[n].ToDecimal().ToString("n0")
                                        : dt == typeof(float) || dt == typeof(decimal)
                                              ? data[n].ToDecimal().ToString("n2")
                                              : data[n],
                            Align = dt == typeof(bool) 
                                ? "center"
                                : dt == typeof(int) || dt == typeof(long) || dt == typeof(float) || dt == typeof(decimal) 
                                    ? "right" 
                                    : "left"
                        };
                })
                .ToList();

            rpt.DataSource = columns;
            rpt.DataBind();
        }

        protected void OnChartsItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var reportChartData = (ReportChart) e.Item.DataItem;

            if (reportChartData.Chart == null) return;

            try
            {
                if (!reportChartData.Chart.YAxisDataConfigurations.Any()) return;

                // fill chart data column dictionary
                var xAxisData = new List<object>();
                var chartData = new Dictionary<string, List<object>>();

                if (reportChartData.Chart.DataSourceType == ChartDataSourceType.Pivot)
                {
                    foreach (DataRow row in reportChartData.Data.Rows)
                    {
                        var xAxisLabel = reportChartData.Pivot.PivotTableColumns
                                                        .Where(p => p.ColumnType == ColumnType.Pivot)
                                                        .Aggregate(string.Empty,
                                                                   (current, col) =>
                                                                   current + (row[col.ReportColumnName] + " | "));

                        xAxisData.Add(xAxisLabel);
                    }
                }

                else
                {
                    chartData.Add(reportChartData.Chart.XAxisDataColumn, new List<object>());
                }

                foreach (var yAxisCustomization in reportChartData.Chart
                                                                  .YAxisDataConfigurations
                                                                  .Where(
                                                                      yAxisCustomization =>
                                                                      !chartData.ContainsKey(
                                                                          yAxisCustomization.ReportColumnName)))
                    chartData.Add(yAxisCustomization.ReportColumnName, new List<object>());


                foreach (DataRow row in reportChartData.Data.Rows)
                    foreach (var key in chartData.Keys)
                        chartData[key].Add(row[key]);

                // build your XYSeries
                var seriesYAxises = (from yAxisConfiguration in reportChartData.Chart.YAxisDataConfigurations
                                     select new SeriesYAxis
                                         {
                                             Name = yAxisConfiguration.ReportColumnName,
                                             Type = SeriesYAxis.Convert(yAxisConfiguration.Type),
                                             Data = chartData[yAxisConfiguration.ReportColumnName],
                                         }).ToList();

                var xySeries = new XYSeries
                    {
                        X = reportChartData.Chart.DataSourceType == ChartDataSourceType.Pivot
                                ? xAxisData
                                : chartData[reportChartData.Chart.XAxisDataColumn],
                        Y = seriesYAxises
                    };

                var reportChart = (ChartGenerator) e.Item.FindControl("reportChart");

                reportChart.DataSource = xySeries;
                reportChart.DataBind();
                reportChart.Title = reportChartData.Chart.Title;
                reportChart.XAxisTitle = reportChartData.Chart.XAxisTitle;
                reportChart.YAxisTitle = reportChartData.Chart.YAxisTitle;
            }
            catch (Exception ex)
            {
                pnlReportData.Visible = false;
                pnlReportResults.Visible = false;
                DisplayMessages(new[] {ValidationMessage.Error("Invalid Chart Configuration")});
                ErrorLogger.LogError(ex, Context);
            }
        }

        protected void OnNoReportsClicked(object sender, EventArgs e)
        {
            Response.Redirect(DashboardView.PageAddress);
        }


        protected void OnFilterResultsClick(object sender, EventArgs e)
        {
            var filter = txtFilter.Text.Trim().ToLower();
            var configList = new ReportConfigurationSearch().FetchReportConfigurationsVisibleToUser(ActiveUser.Id, ActiveUser.TenantId);
            var configurations = string.IsNullOrEmpty(filter) ? configList : configList.Where(rc => rc.Name.ToLower().Contains(filter) || rc.Description.ToLower().Contains(filter)).ToList();

            rptReportConfigurations.DataSource = configurations;
            rptReportConfigurations.DataBind();

            pnlNoReports.Visible = !configurations.Any();
        }
    }
}