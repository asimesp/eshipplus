﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="ReportSchedulerView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.ReportSchedulerView" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        ShowEdit="true" ShowDelete="true" ShowSave="true" ShowFind="true" ShowMore="true"
        OnFind="OnToolbarFindClicked" OnSave="OnToolbarSaveClicked" OnDelete="OnToolbarDeleteClicked"
        OnNew="OnToolbarNewClicked" OnEdit="OnToolbarEditClicked" OnUnlock="OnUnlockClicked"  OnCommand="OnToolbarCommand" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" ImageUrl="" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Report Scheduler
                <eShip:RecordIdentityDisplayControl runat="server" ID="ridcRecordIdentity" TargetControlId="txtReportConfiguration" />
            <div class="lockMessage">
                <asp:Literal ID="litMessage" runat="server" />
            </div>
        </div>
        <hr class="dark mb5" />
        <div class="errorMsgLit">
            <asp:Literal runat="server" ID="litErrorMessages" />
        </div>
        <eShip:ReportScheduleFinderControl runat="server" ID="reportScheduleFinder" Visible="false"
            OpenForEditEnabled="true" EnableMultiSelection="false" OnItemSelected="OnReportScheduleFinderItemSelected"
            OnSelectionCancel="OnReportScheduleFinderItemCancelled" />
        <eShip:ReportConfigurationFinderControl runat="server" ID="reportConfigurationFinder" Visible="false"
            OpenForEditEnabled="true" EnableMultiSelection="false" OnItemSelected="OnReportConfigurationFinderItemSelected"
            OnSelectionCancel="OnReportConfigurationFinderItemCancelled" />
        <eShip:UserFinderControl runat="server" ID="userFinder" Visible="false" OpenForEditEnabled="true"
            EnableMultiSelection="false" OnItemSelected="OnUserFinderItemSelected" OnSelectionCancel="OnUserFinderItemCancelled" />
        <eShip:CustomHiddenField ID="hidReportScheduleId" runat="server" />

        <script language="javascript" type="text/javascript">
            function ClearHidFlag() {
                jsHelper.SetEmpty('<%= hidFlag.ClientID %>');
            }
        </script>

        <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
            Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" />

        <ajax:TabContainer ID="tabReportSchedule" runat="server" CssClass="ajaxCustom">
            <ajax:TabPanel runat="server" ID="tabDetails" HeaderText="Details">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlDetails">
                        <div class="rowgroup">
                            <div class="col_1_2 bbox vlinedarkright">
                                <h5>Report Information</h5>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Report Configuration</label>
                                        <eShip:CustomHiddenField ID="hidReportConfigurationId" runat="server" Value="" />
                                        <eShip:CustomTextBox runat="server" ID="txtReportConfiguration" CssClass="w420 disabled" ReadOnly="True" Type="NotSet" />
                                        <asp:ImageButton ID="imgReportConfigurationSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                                            CausesValidation="False" OnClick="OnReportConfigurationSearchClicked" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Report Configuration Description</label>
                                        <eShip:CustomTextBox runat="server" ID="txtReportConfigurationDescription" CssClass="w420 disabled" TextMode="MultiLine" ReadOnly="True" Type="NotSet" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">User</label>
                                        <eShip:CustomHiddenField ID="hidUserId" runat="server" Value="" />
                                        <eShip:CustomTextBox runat="server" ID="txtUser" CssClass="w420 disabled" ReadOnly="True" Type="NotSet" />
                                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                                            CausesValidation="False" OnClick="OnUserSearchClicked" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Delivery Filename</label>
                                        <eShip:CustomTextBox runat="server" ID="txtDeliveryFilename" CssClass="w420" MaxLength="100" Type="NotSet" />
                                    </div>
                                </div>
                                <div class="row">
                                    <asp:Panel runat="server" ID="pnlCustomerGroups" Visible="False">
                                        <div class="fieldgroup mr20">
                                            <label class="wlabel">Customer Group</label>
                                            <asp:DropDownList ID="ddlCustomerGroup" runat="server" CssClass="w200" DataValueField="Value"
                                                DataTextField="Text" />
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel runat="server" ID="pnlVendorGroups" Visible="False">
                                        <div class="fieldgroup">
                                            <label class="wlabel">Vendor Group</label>
                                            <asp:DropDownList ID="ddlVendorGroup" runat="server" CssClass="w200" DataValueField="Value"
                                                DataTextField="Text" />
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Export File Format</label>
                                        <asp:DropDownList runat="server" ID="ddlExtension" CssClass="w200" DataTextField="Text"
                                            DataValueField="Value" />
                                    </div>
                                </div>
                                <h5 class="pt10">Schedule</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel w180 mr20">Start Date</label>
                                        <eShip:CustomTextBox runat="server" ID="txtStartDate" CssClass="w100" Type="Date" placeholder="99/99/9999"/>
                                    </div>
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">End Date</label>
                                        <eShip:CustomTextBox runat="server" ID="txtEndDate" CssClass="w100" Type="Date" placeholder="99/99/9999"/>
                                    </div>
                                    <div class="fieldgroup pt26 pl40">
                                        <asp:CheckBox runat="server" ID="chkEnabled" CssClass="jQueryUniform" />
                                        <label>Enabled</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Schedule Interval</label>
                                        <asp:DropDownList runat="server" ID="ddlScheduleInterval" DataValueField="Value"
                                            DataTextField="Text" CssClass="w200" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Time of Day to Run Report</label>
                                        <asp:DropDownList runat="server" ID="ddlTime" CssClass="w100" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Last Run Date</label>
                                        <eShip:CustomTextBox runat="server" ID="txtLastRunDate" CssClass="w200 disabled" ReadOnly="True" Type="NotSet" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">&nbsp;</label>
                                        <asp:Button runat="server" ID="btnResetLastRunDate" Text="RESET" OnClick="OnResetLastRunDateClicked" />
                                        <eShip:CustomHiddenField runat="server" ID="hidResetLastRunDate" Value="" />
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="wlabel pt5">Exclude Days of Week</label>
                                    <ul class="twocol_list pt5">
                                        <li>
                                            <asp:CheckBox runat="server" ID="chkExcludeSunday" CssClass="jQueryUniform" />
                                            <label>Sunday</label>
                                        </li>
                                        <li>
                                            <asp:CheckBox runat="server" ID="chkExcludeThursday" CssClass="jQueryUniform" />
                                            <label>Thursday</label>
                                        </li>
                                        <li>
                                            <asp:CheckBox runat="server" ID="chkExcludeMonday" CssClass="jQueryUniform" />
                                            <label>Monday</label>
                                        </li>
                                        <li>
                                            <asp:CheckBox runat="server" ID="chkExcludeFriday" CssClass="jQueryUniform" />
                                            <label>Friday</label>
                                        </li>
                                        <li>
                                            <asp:CheckBox runat="server" ID="chkExcludeTuesday" CssClass="jQueryUniform" />
                                            <label>Tuesday</label>
                                        </li>
                                        <li>
                                            <asp:CheckBox runat="server" ID="chkExcludeSaturday" CssClass="jQueryUniform" />
                                            <label>Saturday</label>
                                        </li>
                                        <li>
                                            <asp:CheckBox runat="server" ID="chkExcludeWednesday" CssClass="jQueryUniform" />
                                            <label>Wednesday</label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col_1_2 bbox pl46">
                                <h5>Email Notification Settings</h5>
                                <div class="row">
                                    <ul class="twocol_list">
                                        <li>
                                            <asp:CheckBox runat="server" ID="chkNotify" CssClass="jQueryUniform" />
                                            <label>Enabled</label>
                                        </li>
                                        
                                        <li>
                                            <asp:CheckBox runat="server" ID="chkSuppressNotificationForEmptyReport" CssClass="jQueryUniform" />
                                            <label>Suppress Notification For Empty Report</label>
                                        </li>
                                    </ul>
                                </div>
                                <div class="row">
                                    <ul class="display-inline note mt5 pt5">
                                        <li>Emails are delimited by a new line, comma, semi-colon, colon, tab, or vertical bar</li>
                                    </ul>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Notify Emails
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDescription" TargetControlId="txtNotifyEmails" MaxLength="1000" />
                                        </label>
                                        <eShip:CustomTextBox runat="server" ID="txtNotifyEmails" TextMode="MultiLine" CssClass="w420 h150" Type="NotSet" />
                                    </div>
                                </div>
                                <h5 class="pt26">FTP  Notification Settings</h5>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <asp:CheckBox ID="chkFtpEnabled" runat="server" CssClass="jQueryUniform" />
                                        <label class="w180">Enabled</label>
                                    </div>
                                    <div class="fieldgroup pl5">
                                        <asp:CheckBox ID="chkSecureFtp" runat="server" CssClass="jQueryUniform" />
                                        <label>Secure FTP (SSL)</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Url</label>
                                        <eShip:CustomTextBox ID="txtFtpUrl" MaxLength="100" runat="server" CssClass="w200" Type="NotSet" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Default Folder</label>
                                        <eShip:CustomTextBox ID="txtFtpDefaultFolder" MaxLength="50" runat="server" CssClass="w200" Type="NotSet" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Username</label>
                                        <eShip:CustomTextBox ID="txtFtpUsername" MaxLength="50" runat="server" CssClass="w200" Type="NotSet" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Password</label>
                                        <eShip:CustomTextBox ID="txtFtpPassword" MaxLength="50" runat="server" CssClass="w200" Type="NotSet" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabAuditLog" HeaderText="Audit Log">
                <ContentTemplate>
                    <eShip:AuditLogControl ID="auditLogs" runat="server" />
                </ContentTemplate>
            </ajax:TabPanel>
        </ajax:TabContainer>
        <eShip:CustomHiddenField runat="server" ID="hidFlag" />
    </div>
</asp:Content>
