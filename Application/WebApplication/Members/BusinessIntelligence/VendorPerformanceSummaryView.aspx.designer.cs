﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence {
    
    
    public partial class VendorPerformanceSummaryView {
        
        /// <summary>
        /// memberToolBar control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.MainToolbarControl memberToolBar;
        
        /// <summary>
        /// imgPageImageLogo control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image imgPageImageLogo;
        
        /// <summary>
        /// litRecordCount control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal litRecordCount;
        
        /// <summary>
        /// litErrorMessages control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal litErrorMessages;
        
        /// <summary>
        /// messageBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.MessageBoxControl messageBox;
        
        /// <summary>
        /// vendorFinder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls.VendorFinderControl vendorFinder;
        
        /// <summary>
        /// customerFinder control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls.CustomerFinderControl customerFinder;
        
        /// <summary>
        /// pnlReportParameters control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnlReportParameters;
        
        /// <summary>
        /// hidCustomerId control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomHiddenField hidCustomerId;
        
        /// <summary>
        /// txtCustomerNumber control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtCustomerNumber;
        
        /// <summary>
        /// ibtnFindCustomer control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton ibtnFindCustomer;
        
        /// <summary>
        /// txtCustomerName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtCustomerName;
        
        /// <summary>
        /// aceCustomer control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.AutoCompleteExtender aceCustomer;
        
        /// <summary>
        /// pnlVendor control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnlVendor;
        
        /// <summary>
        /// hidVendorId control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomHiddenField hidVendorId;
        
        /// <summary>
        /// txtVendorNumber control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtVendorNumber;
        
        /// <summary>
        /// imgVendorSearch control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton imgVendorSearch;
        
        /// <summary>
        /// txtVendorName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtVendorName;
        
        /// <summary>
        /// aceVendor control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::AjaxControlToolkit.AutoCompleteExtender aceVendor;
        
        /// <summary>
        /// pnlVendorBlank control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnlVendorBlank;
        
        /// <summary>
        /// txtStartShipmentDate control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtStartShipmentDate;
        
        /// <summary>
        /// txtEndShipmentDate control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.CustomTextBox txtEndShipmentDate;
        
        /// <summary>
        /// chkAllModes control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox chkAllModes;
        
        /// <summary>
        /// chkLTL control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox chkLTL;
        
        /// <summary>
        /// chkFTL control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox chkFTL;
        
        /// <summary>
        /// chkSP control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox chkSP;
        
        /// <summary>
        /// chkRail control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox chkRail;
        
        /// <summary>
        /// chkAir control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBox chkAir;
        
        /// <summary>
        /// btnGetVendorStatistics control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnGetVendorStatistics;
        
        /// <summary>
        /// btnExportVendorStatistics control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnExportVendorStatistics;
        
        /// <summary>
        /// tfheVendorPerformanceSummaryTable control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.Controls.FreeHeaderExtender.TableFreezeHeaderExtender tfheVendorPerformanceSummaryTable;
        
        /// <summary>
        /// rptData control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Repeater rptData;
        
        /// <summary>
        /// vpscVendorPerformance control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls.VendorPerformanceSummaryControl vpscVendorPerformance;
    }
}
