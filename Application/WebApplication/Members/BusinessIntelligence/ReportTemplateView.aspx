﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="ReportTemplateView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.ReportTemplateView" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowFind="True" OnFind="OnToolbarFindClicked"
        OnNew="OnToolbarNewClicked" OnSave="OnToolBarSaveClicked" OnEdit="OnToolBarEditClicked"
        OnDelete="OnToolBarDeleteClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">

        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" ImageUrl="" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Report Template
            </h3>

            <div class="lockMessage">
                <asp:Literal ID="litMessage" runat="server" />
            </div>
        </div>

        <hr class="dark mb5" />

        <div class="errorMsgLit">
            <asp:Literal runat="server" ID="litErrorMessages" />
        </div>

        <eShip:ReportTemplateFinderControl ID="reportTemplateFinder" runat="server" EnableMultiSelection="false"
            Visible="false" OnItemSelected="OnReportTemplateFinderItemSelected" OnSelectionCancel="OnReportTemplateFinderSelectionCancelled" />
        <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
        <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
            OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
        <ajax:TabContainer ID="tabReportTemplate" runat="server" CssClass="ajaxCustom">
            <ajax:TabPanel runat="server" ID="tabDetails" HeaderText="Details">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlDetails">
                        <div class="rowgroup">
                            <div class="row">
                                <div class="fieldgroup">
                                    <label class="wlabel">Name</label>
                                    <eShip:CustomTextBox ID="txtName" runat="server" CssClass="w300" MaxLength="50" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="fieldgroup">
                                    <label class="wlabel">
                                        Description
                                    <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDescription" TargetControlId="txtDescription" MaxLength="200" />
                                    </label>
                                    <eShip:CustomTextBox ID="txtDescription" runat="server" TextMode="MultiLine" CssClass="w500 h150" />
                                </div>
                            </div>
                            <div class="row mt10">
                                <div class="fieldgroup">
                                    &nbsp;<asp:CheckBox runat="server" ID="chkCanFilterByVendorGroup" CssClass="jQueryUniform" />
                                    <label>Can Filter By Vendor Group</label>
                                    <asp:CheckBox runat="server" ID="chkCanFilterByCustomerGroup" CssClass="jQueryUniform ml10" />
                                    <label>Can Filter By Customer Group</label>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabQuery" HeaderText="Query">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlQuery">
                        <asp:Button runat="server" ID="btnUpdateQuery" Text="Update Query" CausesValidation="False" CssClass="right" OnClick="OnUpdateQueryClicked" />
                        <br />
                        <br class="mb10" />
                        <asp:Literal runat="server" ID="litQuery" />
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabDefaultConfiguration" HeaderText="Default Configuration">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlDefaultConfiguration">
                        <asp:Button runat="server" ID="btnUpdateConfiguration" Text="Update Default Configuration" CausesValidation="False"
                            OnClick="OnUpdateDefaultConfigurationClicked" CssClass="right" />
                        <br />
                        <br class="mb10" />
                        <asp:Literal runat="server" ID="litDefaultConfiguration" />
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
        </ajax:TabContainer>
        <eShip:CustomHiddenField runat="server" ID="hidFlag" />
        <eShip:CustomHiddenField runat="server" ID="hidReportTemplateId" />
    </div>
</asp:Content>
