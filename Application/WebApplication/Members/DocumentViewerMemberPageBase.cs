﻿using System;
using System.Web;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members
{
	public abstract class DocumentViewerMemberPageBase : MemberPageBase
	{
		protected override void OnLoad(EventArgs e)
		{
			if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.DocumentDeliveryUKey]))
			{
				var decode = HttpUtility.HtmlDecode(Request.QueryString[WebApplicationConstants.DocumentDeliveryUKey]);
				var id = FtpDocDeliveryProcessor.DecryptKey(FtpDocDeliveryProcessor.DecodeFromQueryString(decode)).ToLong();
				var user = new User(id);
				ActiveUser = user.KeyLoaded ? user : null;
			}


			base.OnLoad(e);
		}
	}
}
