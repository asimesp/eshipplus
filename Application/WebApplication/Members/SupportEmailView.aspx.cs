﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members
{
	public partial class SupportEmailView : MemberPageBase
	{
		public override string SetPageIconImage
		{
			set { }
		}

		public override ViewCode PageCode { get { return ViewCode.SupportEmail; } }

		public static string PageAddress { get { return "~/Members/SupportEmail.aspx"; } }

		public void DisplayMessages(IEnumerable<ValidationMessage> messages)
		{
			if (messages == null) return;
			if (!messages.Any()) return;

			messageBox.Icon = messages.GenerateIcon();
			messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
			messageBox.Visible = true;
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (IsPostBack) return;

			txtTollFree.Text = ActiveUser != null && ActiveUser.DefaultCustomer != null ? ActiveUser.DefaultCustomer.Tier.SupportTollFree : string.Empty;
			txtReplyEmail.Text = ActiveUser != null ? ActiveUser.Email : string.Empty;
		}

		protected void OnOkayProcess(object sender, EventArgs e)
		{
			messageBox.Visible = false;
			if (!hidValidationError.Value.ToBoolean())
				Response.Redirect(DashboardView.PageAddress);
		}

		protected void OnCancelSupportEmail(object sender, EventArgs eventArgs)
		{
			Response.Redirect(DashboardView.PageAddress);
		}

		protected void OnSendSupportEmailClick(object sender, EventArgs e)
		{

			var err = new List<ValidationMessage>();
			if (string.IsNullOrEmpty(txtReplyEmail.Text)) err.Add(ValidationMessage.Error("A reply email is required."));
			if (string.IsNullOrEmpty(txtsubject.Text)) err.Add(ValidationMessage.Error("A subject is required."));
			if (string.IsNullOrEmpty(aeBody.Content)) err.Add(ValidationMessage.Error("Message content is required"));

			if (err.Any())
			{
				DisplayMessages(err);
				hidValidationError.Value = true.GetString();
				return;
			}
			hidValidationError.Value = false.GetString();

			try
			{
				var category = ddlCategory.SelectedItem.Text;
				var email = new EmailMessage
				{
					From = WebApplicationSettings.DoNotReplyEmail,
					Subject = string.Format("{0} - {1}", category, txtsubject.Text),
					Body = aeBody.Content,
					ReplyEmail = txtReplyEmail.Text.StripSpacesFromEmails(),
				};
				this.SendSupportEmail(email, category);

				DisplayMessages(new[] { ValidationMessage.Information(@"Your customer support email has been sent.") });
			}

			catch
			{
				DisplayMessages(new[] { ValidationMessage.Error(@"There was an error processing your request") });
			}
		}
	}
}