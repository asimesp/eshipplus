﻿using System.Web.UI;
using LogisticsPlus.Eship.Core.Administration;

namespace LogisticsPlus.Eship.WebApplication.Members
{
	public class MemberControlBase : UserControl
	{
		public User ActiveUser
		{
			get { return Session[WebApplicationConstants.ActiveUser] as User ?? new User(); }
			set { Session[WebApplicationConstants.ActiveUser] = value; }
		}

		public SearchDefaults SearchDefaults
		{
			get { return Session[WebApplicationConstants.UserSearchDefaults] as SearchDefaults; }
			set { Session[WebApplicationConstants.UserSearchDefaults] = value; }
		}
	}
}
