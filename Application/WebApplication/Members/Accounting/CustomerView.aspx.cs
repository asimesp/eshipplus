﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.Administration;
using LogisticsPlus.Eship.WebApplication.Members.Connect;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Rating;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting
{
	public partial class CustomerView : MemberPageBase, ICustomerView, ILocationCollectionResource
	{
		private const string LocationsHeader = "Locations";
		private const string RequiredDocumentTagsHeader = "Req'd Doc. Tags";
		private const string CustomFieldsHeader = "Cust. Settings";
		private const string DocumentsHeader = "Documents";
		
        private const string GoToCustomerControlAccount = "GoToCustomerControlAccount";
		private const string GoToCustomerPurchaseOrder = "GoToCustomerPurchaseOrder";
		private const string AddUsersToCustomer = "AddUsersToCustomer";

		private const string SaveFlag = "S";
        private const string DeleteFlag = "D";

		private const string CustomerServiceRepFlag = "CSRF";
		private const string NonEmployeeUserFlag = "NEUF";

		public static string PageAddress { get { return "~/Members/Accounting/CustomerView.aspx"; } }

		public override ViewCode PageCode { get { return ViewCode.Customer; } }

		public override string SetPageIconImage
		{
			set { imgPageImageLogo.ImageUrl = IconLinks.FinanceBlue; }
		}

		public List<ViewListItem> ContactTypeCollection
		{
			get
			{
				var viewListItems = ProcessorVars.RegistryCache[ActiveUser.TenantId]
					.ContactTypes
					.OrderBy(c => c.Description)
					.Select(t => new ViewListItem(t.Code, t.Id.ToString()))
					.ToList();
				viewListItems.Insert(0, new ViewListItem(WebApplicationConstants.ChooseOne, default(long).ToString()));
				return viewListItems;
			}
		}

		public List<ViewListItem> CountryCollection
		{
			get
			{
				var viewListItems = ProcessorVars.RegistryCache.Countries
					.Values
					.OrderByDescending(t => t.SortWeight)
					.ThenBy(t => t.Name)
					.Select(c => new ViewListItem(c.Name, c.Id.ToString()))
					.ToList();
				viewListItems.Insert(0, new ViewListItem(WebApplicationConstants.ChooseOne, default(long).ToString()));
				return viewListItems;
			}
		}

	    public Dictionary<int, string> CustomerTypes
		{
			set
			{
				ddlCustomerType.DataSource = value;
				ddlCustomerType.DataBind();
			}
		}

		public event EventHandler Loading;
		public event EventHandler<ViewEventArgs<Customer>> Save;
		public event EventHandler<ViewEventArgs<Customer>> Delete;
		public event EventHandler<ViewEventArgs<Customer>> Lock;
		public event EventHandler<ViewEventArgs<Customer>> UnLock;
		public event EventHandler<ViewEventArgs<Customer>> LoadAuditLog;
		public event EventHandler<ViewEventArgs<string>> SalesRepSearch;
		public event EventHandler<ViewEventArgs<string>> TierSearch;
		public event EventHandler<ViewEventArgs<string>> PrefixSearch;
		public event EventHandler<ViewEventArgs<string>> AccountBucketSearch;
		public event EventHandler<ViewEventArgs<List<UserShipAs>>> AddCustomerUsers;
        public event EventHandler<ViewEventArgs<Customer>> CreateCustomerInPaymentGateway;
        public event EventHandler<ViewEventArgs<Customer>> DeleteCustomerInPaymentGateway;

		public void DisplayAccountBucket(AccountBucket accountBucket)
		{
			txtAccountBucketCode.Text = accountBucket.Code;
			txtAccountBucketDescription.Text = accountBucket.Description;
			hidAccountBucketId.Value = accountBucket.Id.ToString();
		}

		public void DisplayTier(Tier tier)
		{
			txtTierNumber.Text = tier.TierNumber;
			txtTierName.Text = tier.Name;
			hidTierId.Value = tier.Id.ToString();
		}

		public void DisplaySalesRep(SalesRepresentative salesRepresentative)
		{
			txtSalesRepNumber.Text = salesRepresentative.SalesRepresentativeNumber;
			txtSalesRepName.Text = salesRepresentative.Name;
			hidSalesRepId.Value = salesRepresentative.Id.ToString();
		}

		public void DisplayPrefix(Prefix prefix)
		{
			txtPrefixCode.Text = prefix.Code;
			txtPrefixDescription.Text = prefix.Description;
			hidPrefixId.Value = prefix.Id.ToString();
		}

		public void LogException(Exception ex)
		{
			if (ex != null) ErrorLogger.LogError(ex, Context);
		}

		public void DisplayMessages(IEnumerable<ValidationMessage> messages)
		{
			if (messages == null) return;
			if (!messages.Any()) return;

			if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == SaveFlag)
			{
				// clean up file cleared out!
				if (!string.IsNullOrEmpty(hidFilesToDelete.Value))
				{
					Server.RemoveFiles(hidFilesToDelete.Value.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries));
					hidFilesToDelete.Value = string.Empty;
				}

				// unlock and return to read-only
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<Customer>(new Customer(hidCustomerId.Value.ToLong(), false)));
				SetEditStatus(false);
				memberToolBar.ShowUnlock = false;
			}

			if (hidFlag.Value == AddUsersToCustomer)
			{
				LoadUsers(new Customer(hidCustomerId.Value.ToLong(), false).RetrieveNonEmployeeUsers());
				hidFlag.Value = string.Empty;
			}


			messageBox.Icon = messages.GenerateIcon();
			messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
			messageBox.Visible = true;

			litErrorMessages.Text = messages.HasErrors()
										? string.Join(WebApplicationConstants.HtmlBreak,
													  messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
														Select(m => m.Message).ToArray())
										: string.Empty;
		}

		public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs)
		{
			auditLogs.Load(logs);
		}

		public void FailedLock(Lock @lock)
		{
			memberToolBar.ShowNew = false;
			memberToolBar.ShowSave = false;
			memberToolBar.ShowDelete = false;
			memberToolBar.ShowUnlock = false;

			SetEditStatus(false);

			litMessage.Text = @lock.BuildFailedLockMessage();
		}

		public void SetId(long id)
		{
			if (id == default(long) && hidFlag.Value == DeleteFlag)
			{
				Server.RemoveDirectory(WebApplicationSettings.CustomerFolder(ActiveUser.TenantId, hidCustomerId.Value.ToLong()));
				hidFlag.Value = string.Empty;
			}

			LoadCustomer(new Customer(id, false));
			hidCustomerId.Value = id.ToString();
			SetEditStatus(id != default(long));
		}



		private List<ToolbarMoreAction> ToolbarMoreActions()
		{
			var goToCustomerRatings = new ToolbarMoreAction
			{
				ConfirmCommand = false,
				ImageUrl = IconLinks.Rating,
				Name = "Go To Customer Ratings",
				IsLink = true,
				NavigationUrl = string.Format("{0}?{1}={2}", CustomerRatingView.PageAddress, WebApplicationConstants.TransferNumber, hidRatingId.Value.UrlTextEncrypt()),
                OpenInNewWindow = true
			};
			var goToCustomerCommunications = new ToolbarMoreAction
			{
				ConfirmCommand = false,
                ImageUrl = IconLinks.Connect,
				Name = "Go To Customer Communications",
				IsLink = true,
                NavigationUrl = string.Format("{0}?{1}={2}", CustomerCommunicationView.PageAddress, WebApplicationConstants.TransferNumber, hidCommunicationId.Value.UrlTextEncrypt()),
                OpenInNewWindow = true
			};

			var goToCustomerControlAccount = new ToolbarMoreAction
			{
				CommandArgs = GoToCustomerControlAccount,
				ConfirmCommand = false,
                ImageUrl = IconLinks.Finance,
				Name = "Go To Customer Control Account",
				IsLink = false,
				NavigationUrl = string.Empty
			};

			var goToCustomerPurchaseOrder = new ToolbarMoreAction
			{
				CommandArgs = GoToCustomerPurchaseOrder,
				ConfirmCommand = false,
                ImageUrl = IconLinks.Finance,
				Name = "Go To Customer Purchase Order",
				IsLink = false,
				NavigationUrl = string.Empty
			};
			var addUsers = new ToolbarMoreAction
			{
				CommandArgs = AddUsersToCustomer,
				ConfirmCommand = true,
                ImageUrl = IconLinks.Finance,
				Name = "Add Users",
				IsLink = false,
				NavigationUrl = string.Empty
			};
			var seperator = new ToolbarMoreAction { IsSeperator = true };

			var moreActions = new List<ToolbarMoreAction>();

			if (hidRatingId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.CustomerRating))
				moreActions.Add(goToCustomerRatings);

            if (hidCommunicationId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.CustomerCommunication))
				moreActions.Add(goToCustomerCommunications);

			if (hidCustomerId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.CustomerControlAccount))
				moreActions.Add(goToCustomerControlAccount);

			if (hidCustomerId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.CustomerPurchaseOrder))
				moreActions.Add(goToCustomerPurchaseOrder);

			if (hidCustomerId.Value.ToLong() != default(long) && ActiveUser.HasAccessToModify(ViewCode.User))
				moreActions.AddRange(new[] { seperator, addUsers });

			// The 'More' menu toolbar option is hidden if there are no options to display inside the 'More' toolbar option
			memberToolBar.ShowMore = moreActions.Any();

			return moreActions;
		}

		private void SetEditStatus(bool enabled)
		{
			pnlDetails.Enabled = enabled;
			pnlAssociatedUsers.Enabled = enabled;
			pnlCustomFields.Enabled = enabled;
			pnlCustChargeCodeMap.Enabled = enabled;
			pnlLocations.Enabled = enabled;
			pnlRequiredDocumentTags.Enabled = enabled;
			pnlNonEmployeeUsers.Enabled = enabled;
		    pnlPaymentProfiles.Enabled = enabled;
			memberToolBar.EnableSave = enabled;

			pnlDocuments.Enabled = ((hidCustomerId.Value.ToLong() != default(long)) && enabled); //Customer must exist before documents can be added.
			tabDocuments.Visible = hidCustomerId.Value.ToLong() != default(long);

			fupLogoImage.Enabled = enabled && hidCustomerId.Value.ToLong() != default(long);

			litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;
		}



		private void UpdateDetails(Customer customer)
		{
			customer.Active = chkActive.Checked;
			customer.CustomerNumber = txtCustomerNumber.Text;
			customer.Name = txtCustomerName.Text;

			customer.CustomerType = ddlCustomerType.SelectedValue.ToEnum<CustomerType>();
			customer.Tier = hidTierId.Value.ToLong() == default(long) ? null : new Tier(hidTierId.Value.ToLong(), false);
			customer.SalesRepresentative = hidSalesRepId.Value.ToLong() == default(long)
											? null
											: new SalesRepresentative(hidSalesRepId.Value.ToLong(), false);

			customer.RequiredMileageSourceId = ddlMileageSources.SelectedValue.ToLong();
			customer.DefaultAccountBucketId = hidAccountBucketId.Value.ToLong();
			customer.PrefixId = hidPrefixId.Value.ToLong();

			customer.HidePrefix = chkHidePrefix.Checked;
			customer.InvoiceTerms = txtInvoiceTerms.Text.ToInt();
			customer.AdditionalBillOfLadingText = txtAdditionBolText.Text;
			customer.Notes = txtNotes.Text;
			customer.CustomField1 = txtCustomField1.Text;
			customer.CustomField2 = txtCustomField2.Text;
			customer.CustomField3 = txtCustomField3.Text;
			customer.CustomField4 = txtCustomField4.Text;
			customer.CreditLimit = txtCreditLimit.Text.ToDecimal();
			customer.CareOfAddressFormatEnabled = chkCareOfAdddressFormatEnabled.Checked;

			customer.ShipperBillOfLadingSeed = txtShipperBol.Text.ToLong();
			customer.ShipperBillOfLadingPrefix = txtShipperBolPrefix.Text;
			customer.ShipperBillOfLadingSuffix = txtShipperBolSuffix.Text;
			customer.EnableShipperBillOfLading = chkEnableBolSuffix.Checked;

			customer.InvoiceRequiresCarrierProNumber = chkCarrierProNumber.Checked;
			customer.InvoiceRequiresShipperReference = chkShipperReference.Checked;
			customer.InvoiceRequiresControlAccount = chkCustomerConrolAccountRequired.Checked;
            customer.CanInvoiceNonDeliveredShipment = chkCanInvoiceNonDeliveredShipment.Checked;
			customer.ShipmentRequiresNMFC = chkNMFCRequired.Checked;
		    customer.RateAndScheduleInDemo = chkRateAndScheduleInDemo.Checked;
		    customer.PurgeExpiredQuotes = chkPurgeExpiredQuotes.Checked;
		    customer.AllowLocationContactNotification = chkAllowLocationContactNotification.Checked;
            

			customer.ShipmentRequiresPurchaseOrderNumber = chkPurchaseOrderRequired.Checked;
			customer.ValidatePurchaseOrderNumber = chkValidatePurchaseOrder.Checked;
			customer.InvalidPurchaseOrderNumberMessage = txtInvalidPurchaseOrderMessage.Text;
			customer.CustomVendorSelectionMessage = txtCustomVendorSelectionMessage.Text;

			customer.AuditInstructions = txtAuditInstructions.Text;

			customer.CustomerToleranceEnabled = chkCustomerToleranceEnabled.Checked;
			customer.TruckloadPickupTolerance = txtFTLPickupTolerance.Text.ToInt();
			customer.TruckloadDeliveryTolerance = txtFTLDeliveryTolerance.Text.ToInt();

		    customer.IsCashOnly = chkIsCashOnly.Checked;
		    customer.CanPayByCreditCard = chkCanPayByCreditCard.Checked;
		}

		private void UpdateCustomFields(Customer customer)
		{
			var customFields = lstCustomFields
				.Items
				.Select(item =>
					{
						var id = item.FindControl("hidCustomFieldId").ToCustomHiddenField().Value.ToLong();
						return new CustomField(id, id != default(long))
							{
								Name = item.FindControl("txtName").ToTextBox().Text,
								DisplayOnOrigin = item.FindControl("chkDisplayOnOrigin").ToAltUniformCheckBox().Checked,
								DisplayOnDestination = item.FindControl("chkDisplayOnDestination").ToAltUniformCheckBox().Checked,
								ShowOnDocuments = item.FindControl("chkShowOnDocuments").ToAltUniformCheckBox().Checked,
								Required = item.FindControl("chkRequired").ToAltUniformCheckBox().Checked,
								Tenant = customer.Tenant,
								Customer = customer,
							};

					})
				.ToList();

			customer.CustomFields = customFields;
		}

		private void UpdateCustomerChargeCodeMap(Customer customer)
		{
			var customerChargeCodeMap = lstCustChargeCodeMap
				.Items
				.Select(item =>
				{
					var id = item.FindControl("hidCustChargeCodeMapId").ToCustomHiddenField().Value.ToLong();
					return new CustomerChargeCodeMap(id, id != default(long))
					{
						ChargeCodeId = item.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
						ExternalCode = item.FindControl("txtExternalChargeCode").ToTextBox().Text,
						ExternalDescription = item.FindControl("txtExternalChargeCodeDescription").ToTextBox().Text,
						CustomerId = customer.Id,
						Tenant = customer.Tenant,
						};

				})
				.ToList();

			customer.CustomChargeCodeMap = customerChargeCodeMap;
		}

		private void UpdateServiceReps(Customer customer)
		{
			var serviceRepresentatives = lstCSR
				.Items
				.Select(item => new CustomerServiceRepresentative
					{
						UserId = item.FindControl("hidUserId").ToCustomHiddenField().Value.ToLong(),
						Customer = customer,
						Tenant = customer.Tenant
					})
				.ToList();

			customer.ServiceRepresentatives = serviceRepresentatives;
		}

		private void UpdateRequiredInvoiceDocumentTags(Customer customer)
		{
			var documentTags = lstRequiredDocumentTags
				.Items
				.Select(item => new RequiredInvoiceDocumentTag
					{
						DocumentTagId = item.FindControl("hidDocumentTagId").ToCustomHiddenField().Value.ToLong(),
						Customer = customer,
						Tenant = customer.Tenant
					})
				.ToList();

			customer.RequiredInvoiceDocumentTags = documentTags;
		}

		private void UpdateDocumentsFromView(Customer customer)
		{
			var documents = lstDocuments.Items
				.Select(i =>
				{
					var id = i.FindControl("hidDocumentId").ToCustomHiddenField().Value.ToLong();
					return new CustomerDocument(id, id != default(long))
					{
						DocumentTagId = i.FindControl("hidDocumentTagId").ToCustomHiddenField().Value.ToLong(),
						Name = i.FindControl("litDocumentName").ToLiteral().Text,
						Description = i.FindControl("litDocumentDescription").ToLiteral().Text,
						LocationPath = i.FindControl("hidLocationPath").ToCustomHiddenField().Value,
						IsInternal = i.FindControl("hidIsInternal").ToCustomHiddenField().Value.ToBoolean(),
						Customer = customer,
						TenantId = customer.TenantId
					};
				})
				.ToList();

			customer.Documents = documents;
		}

		private void UpdateCustomerLogo(Customer customer)
		{
			var virtualPath = WebApplicationSettings.CustomerFolder(customer.TenantId, customer.Id);
			var physicalPath = Server.MapPath(virtualPath);

			if (customer.IsNew) customer.LogoUrl = string.Empty;

			if (fupLogoImage.HasFile)
			{
				if (!Directory.Exists(physicalPath)) Directory.CreateDirectory(physicalPath);

				//ensure unique filename
				var fi = new FileInfo(Server.MakeUniqueFilename(Path.Combine(physicalPath, fupLogoImage.FileName), true));
				fupLogoImage.WriteToFile(physicalPath, fi.Name, Server.MapPath(customer.LogoUrl));
				customer.LogoUrl = virtualPath + fi.Name;
			}
			else if (string.IsNullOrEmpty(hidLogoUrl.Value))
			{
				hidFilesToDelete.Value += string.Format("{0};", customer.LogoUrl);
				customer.LogoUrl = string.Empty;
			}
		}


		private List<CustomerLocation> RetrieveViewLocations(Customer customer)
		{
			var locations = new List<CustomerLocation>();
			foreach (var item in lstLocations.Items)
			{
				var control = item.FindControl("llicLocation").ToLocationListingInputControl();
				if (control == null) continue;
				var locationId = control.RetrieveLocationId();
				var location = new CustomerLocation(locationId, locationId != default(long));
				if (location.IsNew)
				{
					location.DateCreated = DateTime.Now;
					location.Tenant = customer.Tenant;
					location.Customer = customer;
				}
				control.UpdateLocation(location);

				locations.Add(location);
			}
			return locations;
		}


		private void ProcessTransferredRequest(Customer customer)
		{
		    if (customer.IsNew) return;
		    
            LoadCustomer(customer);
		    if (LoadAuditLog != null) LoadAuditLog(this, new ViewEventArgs<Customer>(customer));
		}

		private void LoadCustomer(Customer customer)
		{
			if (!customer.IsNew) customer.LoadCollections();

			hidCustomerId.Value = customer.Id.ToString();

			//DETAILS TAB
			chkActive.Checked = customer.Active;
			txtDateCreated.Text = customer.DateCreated.FormattedShortDate();
			txtCustomerNumber.Text = customer.CustomerNumber;
			txtCustomerName.Text = customer.Name;

			ddlCustomerType.SelectedValue = customer.CustomerType.ToInt().ToString();

			DisplayTier(customer.Tier ?? new Tier());
			DisplaySalesRep(customer.SalesRepresentative ?? new SalesRepresentative());
			DisplayAccountBucket(customer.DefaultAccountBucket ?? new AccountBucket());
			DisplayPrefix(customer.Prefix ?? new Prefix());

			chkHidePrefix.Checked = customer.HidePrefix;
            ddlMileageSources.SelectedValue = customer.RequiredMileageSourceId.GetString();
			txtInvoiceTerms.Text = customer.InvoiceTerms.ToString();
			txtAdditionBolText.Text = customer.AdditionalBillOfLadingText;
			txtNotes.Text = customer.Notes;
			txtCustomField1.Text = customer.CustomField1;
			txtCustomField2.Text = customer.CustomField2;
			txtCustomField3.Text = customer.CustomField3;
			txtCustomField4.Text = customer.CustomField4;
			txtCreditLimit.Text = customer.CreditLimit.ToString();
			chkCareOfAdddressFormatEnabled.Checked = customer.CareOfAddressFormatEnabled;

			imgLogo.ImageUrl = customer.LogoUrl;
			imgLogo.Visible = !string.IsNullOrEmpty(customer.LogoUrl);
			hidLogoUrl.Value = customer.LogoUrl;
			imgLogo.AlternateText = string.IsNullOrEmpty(customer.LogoUrl)
										? string.Empty
										: string.Format("{0} logo", customer.Name);

			txtShipperBol.Text = customer.ShipperBillOfLadingSeed.ToString();
			txtShipperBolPrefix.Text = customer.ShipperBillOfLadingPrefix;
			txtShipperBolSuffix.Text = customer.ShipperBillOfLadingSuffix;
			chkEnableBolSuffix.Checked = customer.EnableShipperBillOfLading;

			chkCustomerToleranceEnabled.Checked = customer.CustomerToleranceEnabled;
			txtFTLPickupTolerance.Text = customer.TruckloadPickupTolerance.ToString();
			txtFTLDeliveryTolerance.Text = customer.TruckloadDeliveryTolerance.ToString();

		    txtPaymentGatewayKey.Text = customer.PaymentGatewayKey;
		    chkIsCashOnly.Checked = customer.IsCashOnly;
		    chkCanPayByCreditCard.Checked = customer.CanPayByCreditCard;
            btnCreateNewPaymentGatewayKey.Visible = string.IsNullOrEmpty(customer.PaymentGatewayKey);
            btnDeletePaymentGatewayKey.Visible = !string.IsNullOrEmpty(customer.PaymentGatewayKey);
            tabPaymentProfiles.Visible = !string.IsNullOrEmpty(customer.PaymentGatewayKey) && ActiveUser.HasAccessTo(ViewCode.CanSeePaymentProfilesForCustomer);

			//PROCESS REQUIREMENTS
			chkCarrierProNumber.Checked = customer.InvoiceRequiresCarrierProNumber;
			chkShipperReference.Checked = customer.InvoiceRequiresShipperReference;
			chkNMFCRequired.Checked = customer.ShipmentRequiresNMFC;
		    chkRateAndScheduleInDemo.Checked = customer.RateAndScheduleInDemo;
		    chkPurgeExpiredQuotes.Checked = customer.PurgeExpiredQuotes;
		    chkAllowLocationContactNotification.Checked = customer.AllowLocationContactNotification;

			chkPurchaseOrderRequired.Checked = customer.ShipmentRequiresPurchaseOrderNumber;
			chkValidatePurchaseOrder.Checked = customer.ValidatePurchaseOrderNumber;
			txtInvalidPurchaseOrderMessage.Text = customer.InvalidPurchaseOrderNumberMessage;
			txtCustomVendorSelectionMessage.Text = customer.CustomVendorSelectionMessage;
			chkCustomerConrolAccountRequired.Checked = customer.InvoiceRequiresControlAccount;
		    chkCanInvoiceNonDeliveredShipment.Checked = customer.CanInvoiceNonDeliveredShipment;

			txtAuditInstructions.Text = customer.AuditInstructions;

			//LOCATIONS
			lstLocations.DataSource = customer.Locations;
			lstLocations.DataBind();
			tabLocations.HeaderText = customer.Locations.BuildTabCount(LocationsHeader);

			//REQUIRED INVOICE DOCUMENT TAGS
			var requiredDocumentTags = customer.RetrieveRequiredInvoiceDocumentTags();
			lstRequiredDocumentTags.DataSource = requiredDocumentTags;
			lstRequiredDocumentTags.DataBind();
			tabRequiredDocumentTags.HeaderText = requiredDocumentTags.BuildTabCount(RequiredDocumentTagsHeader);

			//CSR
			var customerServiceReps = customer.RetrieveServiceRepresentatives();
			lstCSR.DataSource = customerServiceReps;
			lstCSR.DataBind();
			litCustomerServiceRepCount.Text = customerServiceReps.BuildTabCount();

			//CUSTOM FIELDS
			lstCustomFields.DataSource = customer.CustomFields;
			lstCustomFields.DataBind();
			litCustomFieldsCount.Text = customer.CustomFields.BuildRecordCount();

			//Charge Code Map
			lstCustChargeCodeMap.DataSource = customer.CustomChargeCodeMap;
			lstCustChargeCodeMap.DataBind();
			litChargeCodeMapCount.Text = customer.CustomChargeCodeMap.BuildRecordCount();
			

			//Documents
			lstDocuments.DataSource = customer.Documents;
			lstDocuments.DataBind();
			tabDocuments.HeaderText = customer.Documents.BuildTabCount(DocumentsHeader);


		    hidCommunicationId.Value = customer.Communication != null
		                                   ? customer.Communication.Id.GetString()
		                                   : default(long).GetString();
		    hidRatingId.Value = customer.Rating != null
		                            ? customer.Rating.Id.GetString()
		                            : default(long).GetString();
            memberToolBar.LoadAdditionalActions(ToolbarMoreActions());

			LoadUsers(customer.RetrieveNonEmployeeUsers());

            if (!string.IsNullOrEmpty(customer.PaymentGatewayKey) && ActiveUser.HasAccessTo(ViewCode.CanSeePaymentProfilesForCustomer))
                ppmPaymentProfileManager.LoadCustomerToManagePaymentProfiles(customer);

			memberToolBar.ShowUnlock = customer.HasUserLock(ActiveUser, customer.Id);
		}

		private void LoadUsers(List<User> user)
		{
            const string linkRow =
				@"<tr>
					<td>{0}&nbsp;<a title='Go To User Record' target='_blank' class='blue' href='{1}?{2}={3}'><img src='{4}' alt='' width='16' align='absmiddle'/></td>
					<td>{5}</td>
                    <td>{6}</td>
                    <td><a title='email' class='blue' href='mailto:{7}'>{7}</a></td>
				</tr>";
            const string row =
                @"<tr>
					<td>{0}</td>
					<td>{1}</td>
                    <td>{2}</td>
                    <td>{3}</td>
				</tr>";

		    var hasAccessToUser = ActiveUser.HasAccessTo(ViewCode.User);
			var rows = user
                .Select(u => hasAccessToUser
                    ? string.Format(linkRow, u.Username, ResolveUrl(UserView.PageAddress), WebApplicationConstants.TransferNumber, u.Id.GetString().UrlTextEncrypt(), ResolveUrl("~/images/icons2/arrow_newTab.png"),u.FirstName, u.LastName, u.Email) 
					: string.Format(row, u.Username, u.FirstName, u.LastName, u.Email))
				.ToArray()
				.SpaceJoin();
			litUsers.Text = rows;
			litNonEmployeeUsersCount.Text = user.BuildTabCount();
		}


		private void GotoToCustomerControlAccount()
		{
			var customer = new Customer(hidCustomerId.Value.ToLong());

			if (UnLock != null)
				UnLock(this, new ViewEventArgs<Customer>(customer));

			Session[WebApplicationConstants.TransferCustomerId] = customer.Id;
			Response.Redirect(CustomerControlAccountView.PageAddress);
		}

		private void GotoToCustomerPurchaseOrder()
		{
			var customer = new Customer(hidCustomerId.Value.ToLong());

			if (UnLock != null)
				UnLock(this, new ViewEventArgs<Customer>(customer));

			Session[WebApplicationConstants.TransferCustomerId] = customer.Id;
			Response.Redirect(CustomerPurchaseOrderView.PageAddress);
		}

		private void AddUsers()
		{
			hidFlag.Value = NonEmployeeUserFlag;
			userFinder.Reset();
			userFinder.FilterForNonEmployees = true;
			userFinder.Visible = true;
		}



		protected void Page_Load(object sender, EventArgs e)
		{
			new CustomerHandler(this).Initialize();

			memberToolBar.ShowSave = Access.Modify;
			memberToolBar.ShowDelete = Access.Remove;
			memberToolBar.ShowNew = Access.Modify;
			memberToolBar.ShowEdit = Access.Modify;

			customerFinder.OpenForEditEnabled = Access.Modify;

			memberToolBar.LoadAdditionalActions(ToolbarMoreActions());

			if (IsPostBack) return;

			aceAccountBucketCode.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString() } });
			acePrefixCode.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString() } });
			aceTier.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString() } });
			aceSalesRep.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString() } });

			if (Loading != null)
				Loading(this, new EventArgs());

			if (Session[WebApplicationConstants.TransferCustomerId] != null)
			{
				ProcessTransferredRequest(new Customer(Session[WebApplicationConstants.TransferCustomerId].ToLong()));
				Session[WebApplicationConstants.TransferCustomerId] = null;
			}

            if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.TransferNumber]))
                ProcessTransferredRequest(new Customer(Request.QueryString[WebApplicationConstants.TransferNumber].UrlTextDecrypt().ToLong()));

			SetEditStatus(false);
		}




		protected void OnFindClicked(object sender, EventArgs e)
		{
			customerFinder.Visible = true;
		}

		protected void OnNewClicked(object sender, EventArgs e)
		{
			litErrorMessages.Text = string.Empty;
			LoadCustomer(new Customer { DateCreated = DateTime.Now });
			DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
			SetEditStatus(true);
			chkActive.Checked = true;
		}

		protected void OnEditClicked(object sender, EventArgs e)
		{
			var customer = new Customer(hidCustomerId.Value.ToLong(), false);

			if (Lock == null || customer.IsNew) return;

			SetEditStatus(true);
			memberToolBar.ShowUnlock = true;
			Lock(this, new ViewEventArgs<Customer>(customer));

			LoadCustomer(customer);
		}

		protected void OnUnlockClicked(object sender, EventArgs e)
		{
			var group = new Customer(hidCustomerId.Value.ToLong(), false);
			if (UnLock != null && !group.IsNew)
			{
				SetEditStatus(false);
				memberToolBar.ShowUnlock = false;
				UnLock(this, new ViewEventArgs<Customer>(group));
			}
		}

		protected void OnSaveClicked(object sender, EventArgs e)
		{
			var customerId = hidCustomerId.Value.ToLong();
			var customer = new Customer(customerId, customerId != default(long));

			if (customer.IsNew)
			{
				customer.TenantId = ActiveUser.TenantId;
				customer.DateCreated = DateTime.Now;
			    customer.PaymentGatewayKey = string.Empty;
			}
			else
			{
				customer.LoadCollections();
			}

			UpdateDetails(customer);
			UpdateCustomFields(customer);
			UpdateCustomerChargeCodeMap(customer);
			UpdateServiceReps(customer);
			UpdateRequiredInvoiceDocumentTags(customer);
			UpdateDocumentsFromView(customer);

			customer.Locations = RetrieveViewLocations(customer);

			UpdateCustomerLogo(customer);

			hidFlag.Value = SaveFlag;

			if (Save != null)
				Save(this, new ViewEventArgs<Customer>(customer));

			if (LoadAuditLog != null)
				LoadAuditLog(this, new ViewEventArgs<Customer>(customer));
		}

		protected void OnDeleteClicked(object sender, EventArgs e)
		{
			var customer = new Customer(hidCustomerId.Value.ToLong(), false);

			hidFlag.Value = DeleteFlag;

			if (Delete != null)
				Delete(this, new ViewEventArgs<Customer>(customer));

			customerFinder.Reset();
		}

		protected void OnToolbarCommandClicked(object sender, ViewEventArgs<string> e)
		{
			switch (e.Argument)
			{
				case GoToCustomerControlAccount:
					GotoToCustomerControlAccount();
					break;
				case GoToCustomerPurchaseOrder:
					GotoToCustomerPurchaseOrder();
					break;
				case AddUsersToCustomer:
					AddUsers();
					break;
			}
		}


		protected void OnCustomerFinderItemSelected(object sender, ViewEventArgs<Customer> e)
		{
			litErrorMessages.Text = string.Empty;

			if (hidCustomerId.Value.ToLong() != default(long))
			{
				var oldCustomer = new Customer(hidCustomerId.Value.ToLong(), false);
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<Customer>(oldCustomer));
			}

			var customer = e.Argument;

			LoadCustomer(customer);

			customerFinder.Visible = false;

			if (customerFinder.OpenForEdit && Lock != null)
			{
				SetEditStatus(true);
				memberToolBar.ShowUnlock = true;
				Lock(this, new ViewEventArgs<Customer>(customer));
			}
			else
			{
				SetEditStatus(false);
			}

			if (LoadAuditLog != null) LoadAuditLog(this, new ViewEventArgs<Customer>(customer));
		}

		protected void OnCustomerFinderSelectionCancel(object sender, EventArgs e)
		{
			customerFinder.Visible = false;
		}


		protected void OnCustomFieldDeleteClicked(object sender, ImageClickEventArgs e)
		{
			var imageButton = (ImageButton)sender;

			var customFields = lstCustomFields
				.Items
				.Select(item => new
					{
						Id = item.FindControl("hidCustomFieldId").ToCustomHiddenField().Value,
						Name = item.FindControl("txtName").ToTextBox().Text,
						DisplayOnOrigin = item.FindControl("chkDisplayOnOrigin").ToAltUniformCheckBox().Checked,
						DisplayOnDestination = item.FindControl("chkDisplayOnDestination").ToAltUniformCheckBox().Checked,
						ShowOnDocuments = item.FindControl("chkShowOnDocuments").ToAltUniformCheckBox().Checked,
						Required = item.FindControl("chkRequired").ToAltUniformCheckBox().Checked
					})
				.ToList();

			customFields.RemoveAt(imageButton.Parent.FindControl("hidCustomFieldIndex").ToCustomHiddenField().Value.ToInt());

			lstCustomFields.DataSource = customFields;
			lstCustomFields.DataBind();
			litCustomFieldsCount.Text = customFields.BuildTabCount();
		}

		protected void OnAddCustomFieldClicked(object sender, EventArgs e)
		{
			hidCustomFieldEditItemIndex.Value = (-1).ToString();

			var customFields = lstCustomFields
				.Items
				.Select(item => new
					{
						Id = item.FindControl("hidCustomFieldId").ToCustomHiddenField().Value,
						Name = item.FindControl("txtName").ToTextBox().Text,
						DisplayOnOrigin = item.FindControl("chkDisplayOnOrigin").ToAltUniformCheckBox().Checked,
						DisplayOnDestination = item.FindControl("chkDisplayOnDestination").ToAltUniformCheckBox().Checked,
						ShowOnDocuments = item.FindControl("chkShowOnDocuments").ToAltUniformCheckBox().Checked,
						Required = item.FindControl("chkRequired").ToAltUniformCheckBox().Checked
					})
				.ToList();

			// add new
			customFields.Add(new
				{
					Id = default(long).GetString(),
					Name = string.Empty,
					DisplayOnOrigin = false,
					DisplayOnDestination = false,
					ShowOnDocuments = false,
					Required = false
				});

			lstCustomFields.DataSource = customFields;
			lstCustomFields.DataBind();
			litCustomFieldsCount.Text = new List<Object>().BuildTabCount();
			litCustomFieldsCount.Text = customFields.BuildTabCount();
		}
		
		protected void OnClearCustomFieldsClicked(object sender, EventArgs e)
		{
			lstCustomFields.DataSource = new List<Object>();
			lstCustomFields.DataBind();
			tabCustomFields.HeaderText = CustomFieldsHeader;
			litCustomFieldsCount.Text = new List<Object>().BuildTabCount();
		}


		protected void OnAddChargeCodeMap(object sender, EventArgs e)
		{
			hidChargeCodeMapItemIndex.Value = (-1).ToString();

			var custChargeCodeMap = lstCustChargeCodeMap
				.Items
				.Select(item => new
				{
					Id = item.FindControl("hidCustChargeCodeMapId").ToCustomHiddenField().Value.ToLong(),
					ChargeCodeId = item.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
					ExternalCode = item.FindControl("txtExternalChargeCode").ToTextBox().Text,
					ExternalDescription = item.FindControl("txtExternalChargeCodeDescription").ToTextBox().Text,

				})
				.ToList();

			// add new
			custChargeCodeMap.Add(new
			{
				Id = default(long),
				ChargeCodeId = default(long),
				ExternalCode = string.Empty,
				ExternalDescription = string.Empty
			});


			lstCustChargeCodeMap.DataSource = custChargeCodeMap;
			lstCustChargeCodeMap.DataBind();

			litChargeCodeMapCount.Text = custChargeCodeMap.BuildTabCount();
		}

		protected void OnClearCustChargeCodeMap(object sender, EventArgs e)
		{
			lstCustChargeCodeMap.DataSource = new List<Object>();
			lstCustChargeCodeMap.DataBind();
			litChargeCodeMapCount.Text = new List<Object>().BuildTabCount();
		}

		protected void OnCustomerChargeCodeMapDeleteClicked(object sender, ImageClickEventArgs e)
		{
			var imageButton = (ImageButton)sender;

			var customFields = lstCustChargeCodeMap
				.Items
				.Select(item => new
				{
					Id = item.FindControl("hidCustChargeCodeMapId").ToCustomHiddenField().Value.ToLong(),
					ChargeCodeId = item.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
					ExternalCode = item.FindControl("txtExternalChargeCode").ToTextBox().Text,
					ExternalDescription = item.FindControl("txtExternalChargeCodeDescription").ToTextBox().Text
				})
				.ToList();

			customFields.RemoveAt(imageButton.Parent.FindControl("hidChargeCodeIndex").ToCustomHiddenField().Value.ToInt());

			lstCustChargeCodeMap.DataSource = customFields;
			lstCustChargeCodeMap.DataBind();
			litChargeCodeMapCount.Text = customFields.BuildTabCount();
		}




		private void AddCustomerServiceRepresentativeUsers(IEnumerable<User> users)
		{
			var vUsers = lstCSR.Items
				   .Select(item => new
				   {
                       Id = item.FindControl("hidUserId").ToCustomHiddenField().Value,
                       FirstName = item.FindControl("litSalesRepFirstName").ToLiteral().Text,
                       LastName = item.FindControl("litSalesRepLastName").ToLiteral().Text,
                       Phone = item.FindControl("litSalesRepPhone").ToLiteral().Text,
                       Fax = item.FindControl("litSalesRepFax").ToLiteral().Text,
                       Email = item.FindControl("litSalesRepEmail").ToLiteral().Text,
				   })
				   .ToList();

			var usersToAdd = users
				.Select(u => new { Id = u.Id.ToString(), u.FirstName, u.LastName, u.Phone, u.Fax, u.Email })
				.Where(u => !vUsers.Select(vu => vu.Id).Contains(u.Id));

			vUsers.AddRange(usersToAdd);

			lstCSR.DataSource = vUsers
				.OrderBy(item => item.FirstName)
				.ThenBy(item => item.FirstName)
				.ThenBy(item => item.LastName);
			lstCSR.DataBind();
			litCustomerServiceRepCount.Text = vUsers.BuildTabCount();
		}

		private void AddNonEmployeeUsers(IEnumerable<User> users)
		{
			var userShipAsList = users
				.Select(u => new UserShipAs
				{
					CustomerId = hidCustomerId.Value.ToLong(),
					User = u,
					TenantId = u.TenantId
				})
				.ToList();

			hidFlag.Value = AddUsersToCustomer;

			if (AddCustomerUsers != null)
				AddCustomerUsers(this, new ViewEventArgs<List<UserShipAs>>(userShipAsList));
		}

		protected void OnUserFinderMultiItemSelected(object sender, ViewEventArgs<List<User>> e)
		{
			switch (hidFlag.Value)
			{
				case CustomerServiceRepFlag:
					AddCustomerServiceRepresentativeUsers(e.Argument);
					break;
				case NonEmployeeUserFlag:
					AddNonEmployeeUsers(e.Argument);
					break;
			}

			hidFlag.Value = string.Empty;
			userFinder.Visible = false;
		}

		protected void OnUserFinderSelectionCancelled(object sender, EventArgs e)
		{
			userFinder.Visible = false;
		}


		protected void OnAddServiceRepClicked(object sender, EventArgs e)
		{
			hidFlag.Value = CustomerServiceRepFlag;
			userFinder.Reset();
			userFinder.FilterForNonEmployees = false;
			userFinder.Visible = true;
		}

		protected void OnClearServiceRepsClicked(object sender, EventArgs e)
		{
			lstCSR.DataSource = new List<Object>();
			lstCSR.DataBind();
			litCustomerServiceRepCount.Text = string.Empty;
		}

		protected void OnDeleteCsrClicked(object sender, ImageClickEventArgs e)
		{
			var button = (ImageButton)sender;
			var idToRemove = button.Parent.FindControl("hidUserId").ToCustomHiddenField().Value;

			var custServiceReps = lstCSR
				.Items
				.Select(item => new
					{
						Id = item.FindControl("hidUserId").ToCustomHiddenField().Value,
						FirstName = item.FindControl("litSalesRepFirstName").ToLiteral().Text,
						LastName = item.FindControl("litSalesRepLastName").ToLiteral().Text,
						Phone = item.FindControl("litSalesRepPhone").ToLiteral().Text,
						Fax = item.FindControl("litSalesRepFax").ToLiteral().Text,
						Email = item.FindControl("litSalesRepEmail").ToLiteral().Text,
					})
				.Where(u => u.Id != idToRemove)
				.ToList();

			lstCSR.DataSource = custServiceReps;
			lstCSR.DataBind();
			litCustomerServiceRepCount.Text = custServiceReps.BuildTabCount();
		}

		


		protected void BindLocationLineItem(object sender, ListViewItemEventArgs e)
		{
			var item = e.Item as ListViewDataItem;
			if (item == null) return;
			var ic = item.FindControl("llicLocation").ToLocationListingInputControl();
			var location = item.DataItem as CustomerLocation;
			if (location == null) return;
			ic.LoadLocation(location);

		}

		protected void OnAddLocationClicked(object sender, EventArgs e)
		{
			var locations = RetrieveViewLocations(new Customer(hidCustomerId.Value.ToLong(), false));
			locations.Add(new CustomerLocation());
			lstLocations.DataSource = locations;
			lstLocations.DataBind();
		}

		protected void OnDeleteLocation(object sender, ViewEventArgs<int> e)
		{
			var locations = RetrieveViewLocations(new Customer(hidCustomerId.Value.ToLong(), false));
			locations.RemoveAt(e.Argument);
			lstLocations.DataSource = locations;
			lstLocations.DataBind();
			tabLocations.HeaderText = locations.BuildTabCount(LocationsHeader);
		}



		protected void OnAddDocumentTagClicked(object sender, EventArgs e)
		{
			documentTagFinder.Visible = true;
		}

		protected void OnClearDocumentTagsClicked(object sender, EventArgs e)
		{
			lstRequiredDocumentTags.DataSource = new List<Object>();
			lstRequiredDocumentTags.DataBind();
			tabRequiredDocumentTags.HeaderText = RequiredDocumentTagsHeader;
            athtuTabUpdater.SetForUpdate(tabRequiredDocumentTags.ClientID, new List<Object>().BuildTabCount(RequiredDocumentTagsHeader));
		}

		protected void OnDocumentTagFinderSelectionCancelled(object sender, EventArgs e)
		{
			documentTagFinder.Visible = false;
		}

		protected void OnDocumentTagFinderMultiItemSelected(object sender, ViewEventArgs<List<DocumentTag>> e)
		{
			var vTags = lstRequiredDocumentTags
				.Items
				.Select(i => new
					{
						Id = i.FindControl("hidDocumentTagId").ToCustomHiddenField().Value,
						Code = i.FindControl("litInvReqDocTagCode").ToLiteral().Text,
						Description = i.FindControl("litInvReqDocTagDescription").ToLiteral().Text
					})
				.ToList();
			vTags.AddRange(e.Argument.Select(t => new { Id = t.Id.ToString(), t.Code, t.Description })
							.Where(t => !vTags.Select(vt => vt.Id).Contains(t.Id)));
			lstRequiredDocumentTags.DataSource = vTags;
			lstRequiredDocumentTags.DataBind();
			tabRequiredDocumentTags.HeaderText = vTags.BuildTabCount(RequiredDocumentTagsHeader);

			documentTagFinder.Visible = false;
		}

		protected void OnDeleteInvReqDocTagClicked(object sender, ImageClickEventArgs e)
		{
			var button = (ImageButton)sender;
			var idToRemove = button.Parent.FindControl("hidDocumentTagId").ToCustomHiddenField().Value;

			var requiredDocumentTags = lstRequiredDocumentTags.Items
				.Select(item => new
				{
					Id = item.FindControl("hidDocumentTagId").ToCustomHiddenField().Value,
					Code = item.FindControl("litInvReqDocTagCode").ToLiteral().Text,
					Description = item.FindControl("litInvReqDocTagDescription").ToLiteral().Text,
				})
				.Where(u => u.Id != idToRemove)
				.ToList();

			lstRequiredDocumentTags.DataSource = requiredDocumentTags;
			lstRequiredDocumentTags.DataBind();
			tabRequiredDocumentTags.HeaderText = requiredDocumentTags.BuildTabCount(RequiredDocumentTagsHeader);
            athtuTabUpdater.SetForUpdate(tabRequiredDocumentTags.ClientID, requiredDocumentTags.BuildTabCount(RequiredDocumentTagsHeader));
		}



		protected void OnSalesRepFinderItemSelected(object sender, ViewEventArgs<SalesRepresentative> e)
		{
			DisplaySalesRep(e.Argument);
			salesRepFinder.Visible = false;
		}

		protected void OnSalesRepFinderItemCancelled(object sender, EventArgs e)
		{
			salesRepFinder.Visible = false;
		}

		protected void OnFindServiceRepresentative(object sender, ImageClickEventArgs e)
		{
			salesRepFinder.Visible = true;
		}

		protected void OnSalesRepNumberChanged(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(txtSalesRepNumber.Text))
			{
				DisplaySalesRep(new SalesRepresentative());
				return;
			}

			if (SalesRepSearch != null)
				SalesRepSearch(this, new ViewEventArgs<string>(txtSalesRepNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
		}


		protected void OnTierFinderItemSelected(object sender, ViewEventArgs<Tier> e)
		{
			DisplayTier(e.Argument);
			tierFinder.Visible = false;
		}

		protected void OnTierFinderSelectionCancelled(object sender, EventArgs e)
		{
			tierFinder.Visible = false;
		}

		protected void OnTierNumberChanged(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(txtTierNumber.Text))
			{
				DisplayTier(new Tier());
				return;
			}

			if (TierSearch != null)
				TierSearch(this, new ViewEventArgs<string>(txtTierNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
		}

		protected void OnFindTier(object sender, ImageClickEventArgs e)
		{
			tierFinder.Visible = true;
		}


		protected void OnClearLogoClicked(object sender, EventArgs e)
		{
			hidFilesToDelete.Value += string.Format("{0};", hidLogoUrl.Value);
			hidLogoUrl.Value = string.Empty;
			imgLogo.ImageUrl = string.Empty;
			imgLogo.AlternateText = string.Empty;
			imgLogo.Visible = false;
		}


		protected void OnFindPrefix(object sender, ImageClickEventArgs e)
		{
			prefixFinder.Visible = true;
		}

		protected void OnPrefixFinderItemSelected(object sender, ViewEventArgs<Prefix> e)
		{
			DisplayPrefix(e.Argument);
			prefixFinder.Visible = false;
		}

		protected void OnPrefixFinderSelectionCancelled(object sender, EventArgs e)
		{
			prefixFinder.Visible = false;
		}

		protected void OnPrefixCodeChanged(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(txtPrefixCode.Text))
			{
				DisplayPrefix(new Prefix());
				return;
			}

			if (PrefixSearch != null)
				PrefixSearch(this, new ViewEventArgs<string>(txtPrefixCode.Text.RetrieveSearchCodeFromAutoCompleteString()));
		}


		protected void OnFindAccountBucket(object sender, ImageClickEventArgs e)
		{
			accountBucketFinder.Visible = true;
		}

		protected void OnAccountBucketFinderItemSelected(object sender, ViewEventArgs<AccountBucket> e)
		{
			DisplayAccountBucket(e.Argument);
			accountBucketFinder.Visible = false;
		}

		protected void OnAccountBucketFinderSelectionCancelled(object sender, EventArgs e)
		{
			accountBucketFinder.Visible = false;
		}

		protected void OnAccountBucketCodeChanged(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(txtAccountBucketCode.Text))
			{
				DisplayAccountBucket(new AccountBucket());
				return;
			}

			if (AccountBucketSearch != null)
				AccountBucketSearch(this, new ViewEventArgs<string>(txtAccountBucketCode.Text.RetrieveSearchCodeFromAutoCompleteString()));
		}


		private void LoadDocumentForEdit(CustomerDocument document)
		{
			chkDocumentIsInternal.Checked = document.IsInternal;
			txtDocumentName.Text = document.Name;
			txtDocumentDescription.Text = document.Description;
			ddlDocumentTag.SelectedValue = document.DocumentTagId.GetString();
			txtLocationPath.Text = GetLocationFileName(document.LocationPath);
			hidLocationPath.Value = document.LocationPath;
		}

		protected string GetLocationFileName(object relativePath)
		{
			return relativePath == null ? string.Empty : Server.GetFileName(relativePath.ToString());
		}

		protected void OnAddDocumentClicked(object sender, EventArgs e)
		{
			hidEditingIndex.Value = (-1).ToString();
			LoadDocumentForEdit(new CustomerDocument());
			pnlEditDocument.Visible = true;
			pnlDimScreen.Visible = true;
		}

		protected void OnClearDocumentsClicked(object sender, EventArgs e)
		{
			var paths = lstDocuments.Items.Select(i => new {LocationPath = i.FindControl("hidLocationPath").ToCustomHiddenField().Value,}).ToList();
			foreach (var path in paths)
				hidFilesToDelete.Value += string.Format("{0};", path);

			lstDocuments.DataSource = new List<Object>();
			lstDocuments.DataBind();
			tabDocuments.HeaderText = DocumentsHeader;
		}

		protected void OnEditDocumentClicked(object sender, ImageClickEventArgs e)
		{
			var control = ((ImageButton)sender).Parent;

			hidEditingIndex.Value = control.FindControl("hidDocumentIndex").ToCustomHiddenField().Value;

			var document = new CustomerDocument
				{
					Name = control.FindControl("litDocumentName").ToLiteral().Text,
					Description = control.FindControl("litDocumentDescription").ToLiteral().Text,
					DocumentTagId = control.FindControl("hidDocumentTagId").ToCustomHiddenField().Value.ToLong(),
					LocationPath = control.FindControl("hidLocationPath").ToCustomHiddenField().Value,
					IsInternal = control.FindControl("hidIsInternal").ToCustomHiddenField().Value.ToBoolean(),
				};

			LoadDocumentForEdit(document);

			pnlEditDocument.Visible = true;
			pnlDimScreen.Visible = true;
		}

		protected void OnDeleteDocumentClicked(object sender, ImageClickEventArgs e)
		{
			var imageButton = (ImageButton)sender;

			var documents = lstDocuments.Items
				.Select(i => new
				{
					Id = i.FindControl("hidDocumentId").ToCustomHiddenField().Value.ToLong(),
					Name = i.FindControl("litDocumentName").ToLiteral().Text,
					Description = i.FindControl("litDocumentDescription").ToLiteral().Text,
					DocumentTagId = i.FindControl("hidDocumentTagId").ToCustomHiddenField().Value.ToLong(),
					LocationPath = i.FindControl("hidLocationPath").ToCustomHiddenField().Value,
					IsInternal = i.FindControl("hidIsInternal").ToCustomHiddenField().Value.ToBoolean(),
				})
				.ToList();

			var index = imageButton.Parent.FindControl("hidDocumentIndex").ToCustomHiddenField().Value.ToInt();
			hidFilesToDelete.Value += string.Format("{0};", documents[index].LocationPath);
			documents.RemoveAt(index);

			lstDocuments.DataSource = documents;
			lstDocuments.DataBind();
			tabDocuments.HeaderText = documents.BuildTabCount(DocumentsHeader);
		}

		protected void OnCloseEditDocumentClicked(object sender, EventArgs eventArgs)
		{
			pnlEditDocument.Visible = false;
			pnlDimScreen.Visible = false;
		}

		protected void OnEditDocumentDoneClicked(object sender, EventArgs e)
		{
			var virtualPath = WebApplicationSettings.CustomerFolder(ActiveUser.TenantId, hidCustomerId.Value.ToLong());
			var physicalPath = Server.MapPath(virtualPath);

			var documents = lstDocuments.Items
			                            .Select(i => new
				                            {
					                            Id = i.FindControl("hidDocumentId").ToCustomHiddenField().Value.ToLong(),
					                            Name = i.FindControl("litDocumentName").ToLiteral().Text,
					                            Description = i.FindControl("litDocumentDescription").ToLiteral().Text,
					                            DocumentTagId = i.FindControl("hidDocumentTagId").ToCustomHiddenField().Value,
					                            LocationPath = i.FindControl("hidLocationPath").ToCustomHiddenField().Value,
					                            IsInternal = i.FindControl("hidIsInternal").ToCustomHiddenField().Value.ToBoolean(),
				                            })
			                            .ToList();

			var documentIndex = hidEditingIndex.Value.ToInt();

			var documentId = default(long);
			if (documentIndex != -1)
			{
				documentId = documents[documentIndex].Id;
				documents.RemoveAt(documentIndex);
			}

			var locationPath = hidLocationPath.Value; // this should actually be a hidden field holding the existing path during edit

			documents.Insert(0, new
			{
				Id = documentId,
				Name = txtDocumentName.Text,
				Description = txtDocumentDescription.Text,
				DocumentTagId = ddlDocumentTag.SelectedValue,
				LocationPath = locationPath,
				IsInternal = chkDocumentIsInternal.Checked,
			});

			if (fupLocationPath.HasFile)
			{
				// ensure directory is present
				if (!Directory.Exists(physicalPath)) Directory.CreateDirectory(physicalPath);

				//ensure unique filename
				var fi = new FileInfo(Server.MakeUniqueFilename(Path.Combine(physicalPath, fupLocationPath.FileName), true));

				// save file
				fupLocationPath.WriteToFile(physicalPath, fi.Name, Server.MapPath(documents[0].LocationPath));

				// remove updated record
				documents.RemoveAt(0);

				// re-add record with new file
				documents.Insert(0, new
				{
					Id = documentId,
					Name = txtDocumentName.Text,
					Description = txtDocumentDescription.Text,
					DocumentTagId = ddlDocumentTag.SelectedValue,
					LocationPath = virtualPath + fi.Name,
					IsInternal = chkDocumentIsInternal.Checked
				});
			}

			lstDocuments.DataSource = documents.OrderBy(d => d.Name);
			lstDocuments.DataBind();
			tabDocuments.HeaderText = documents.BuildTabCount(DocumentsHeader);

			pnlEditDocument.Visible = false;
			pnlDimScreen.Visible = false;
		}

		protected void OnLocationPathClicked(object sender, EventArgs e)
		{
			var button = (LinkButton)sender;
			var path = button.Parent.FindControl("hidLocationPath").ToCustomHiddenField().Value;
			Response.Export(Server.ReadFromFile(path), button.Text);
		}


	    protected void OnCreateNewPaymentGatewayClicked(object sender, EventArgs e)
	    {
            var customer = new Customer(hidCustomerId.Value.ToLong());
	        
            if(CreateCustomerInPaymentGateway != null)
                CreateCustomerInPaymentGateway(this, new ViewEventArgs<Customer>(customer));

	        txtPaymentGatewayKey.Text = customer.PaymentGatewayKey;
            btnCreateNewPaymentGatewayKey.Visible = string.IsNullOrEmpty(customer.PaymentGatewayKey);
            btnDeletePaymentGatewayKey.Visible = !string.IsNullOrEmpty(customer.PaymentGatewayKey);
            tabPaymentProfiles.Visible = !string.IsNullOrEmpty(customer.PaymentGatewayKey) && ActiveUser.HasAccessTo(ViewCode.CanSeePaymentProfilesForCustomer);
            ppmPaymentProfileManager.LoadCustomerToManagePaymentProfiles(!string.IsNullOrEmpty(customer.PaymentGatewayKey) ? customer : null);
	    }

	    protected void OnDeletePaymentGatewayKeyClicked(object sender, EventArgs e)
	    {
            var customer = new Customer(hidCustomerId.Value.ToLong());

            if (DeleteCustomerInPaymentGateway != null)
                DeleteCustomerInPaymentGateway(this, new ViewEventArgs<Customer>(customer));

            txtPaymentGatewayKey.Text = customer.PaymentGatewayKey;
	        chkCanPayByCreditCard.Checked = customer.CanPayByCreditCard;
            btnCreateNewPaymentGatewayKey.Visible = string.IsNullOrEmpty(customer.PaymentGatewayKey);
            btnDeletePaymentGatewayKey.Visible = !string.IsNullOrEmpty(customer.PaymentGatewayKey);
            tabPaymentProfiles.Visible = !string.IsNullOrEmpty(customer.PaymentGatewayKey) && ActiveUser.HasAccessTo(ViewCode.CanSeePaymentProfilesForCustomer);
            ppmPaymentProfileManager.LoadCustomerToManagePaymentProfiles(!string.IsNullOrEmpty(customer.PaymentGatewayKey) ? customer : null);
        }


	    protected void OnPaymentProfileManagerProcessingMessages(object sender, ViewEventArgs<List<ValidationMessage>> e)
	    {
            DisplayMessages(e.Argument);
	    }
	}
}