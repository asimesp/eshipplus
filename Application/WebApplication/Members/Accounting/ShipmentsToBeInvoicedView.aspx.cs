﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.Processor.Views.Operations;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Operations.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting
{
    public partial class ShipmentsToBeInvoicedView : MemberPageBase, IShipmentsToBeInvoicedView
    {
        private SearchField SortByField
        {
            get { return OperationsSearchFields.ShipmentSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue); }
        }

        protected bool HasAccessToModifyShipmentsToBeInvoiced { get; set; }

        private const string ImportSetAuditedFlag = "ImportSetAuditedFlag";

        private const string SingleArgs = "SINGLE";
        private const string MultiArgs = "MULTI";
        private const string ImportAuditedArgs = "AUDIT";

        public static string PageAddress { get { return "~/Members/Accounting/ShipmentsToBeInvoicedView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.ShipmentsToBeInvoiced; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.FinanceBlue; } }

        public event EventHandler<ViewEventArgs<List<Shipment>>> SetShipmentsReadyToInvoice;
        public event EventHandler<ViewEventArgs<List<Shipment>>> SingleSave;
        public event EventHandler<ViewEventArgs<List<Shipment>>> MultiSave;
        public event EventHandler<ViewEventArgs<ShipmentViewSearchCriteria>> Search;

        public void DisplaySearchResult(List<ShipmentDashboardDto> shipments)
        {
            litRecordCount.Text = shipments.BuildRecordCount();
            lstShipmentDetails.DataSource = shipments;
            lstShipmentDetails.DataBind();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }



        private List<ToolbarMoreAction> ToolbarMoreActions()
        {
            var single = new ToolbarMoreAction
            {
                CommandArgs = SingleArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Finance,
                Name = "Create Combined Invoice",
                IsLink = false,
                NavigationUrl = string.Empty,
                EnableProcessingDiv = true
            };

            var multi = new ToolbarMoreAction
            {
                CommandArgs = MultiArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Finance,
                Name = "Create Individual Invoices",
                IsLink = false,
                NavigationUrl = string.Empty,
                EnableProcessingDiv = true
            };

            var importForAuditedMarking = new ToolbarMoreAction
            {
                CommandArgs = ImportAuditedArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Finance,
                Name = "Import & Set Shipments Audited",
                IsLink = false,
                NavigationUrl = string.Empty,
                EnableProcessingDiv = true
            };

            var seperator = new ToolbarMoreAction { IsSeperator = true };

            var moreActions = new List<ToolbarMoreAction>();

            if (ActiveUser.HasAccessToModify(ViewCode.ShipmentsToBeInvoiced))
                moreActions.AddRange(new List<ToolbarMoreAction> { single, multi, seperator, importForAuditedMarking });
            else
                memberToolBar.ShowMore = false;

            return moreActions;
        }


        private List<Shipment> RetrieveSelectedShipments()
        {
            var selected = lstShipmentDetails.Items
                .Select(s => new
                {
                    Id = s.FindControl("hidShipmentId").ToCustomHiddenField().Value.ToLong(),
                    Selected = ((ShipmentDashboardDetailControl)s.FindControl("shipmentDashboardDetailControl")),
                })
                .Where(s => s.Selected.FindControl("chkSelection").ToCheckBox().Checked).Select(s => new Shipment(s.Id))
                .ToList();

            return selected;
        }



        private void ProcessInvoices(string args)
        {
            var shipments = RetrieveSelectedShipments();

            if (shipments.Count == 0)
            {
                DisplayMessages(new[] { ValidationMessage.Error("At least one shipment record must be selected.") });
                return;
            }

            switch (args)
            {
                case SingleArgs:
                    if (SingleSave != null) SingleSave(this, new ViewEventArgs<List<Shipment>>(shipments));
                    break;
                case MultiArgs:
                    if (MultiSave != null) MultiSave(this, new ViewEventArgs<List<Shipment>>(shipments));
                    break;
            }

            if (hidFlag.Value == ImportSetAuditedFlag)
            {
                hidFlag.Value = string.Empty;
                pnlSearch.Enabled = true;
                DisplaySearchResult(new List<ShipmentDashboardDto>());
            }
            else
            {
                DoSearchPreProcessingThenSearch(OperationsSearchFields.ShipmentSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue));
            }
        }


        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems
            {
                Columns = GetCurrentRunParameters(true),
                SortAscending = rbAsc.Checked,
                SortBy = SortByField,
            };
        }

        private void DoSearchPreProcessingThenSearch(SearchField field)
        {
            DoSearch(new ShipmentViewSearchCriteria
            {
                Parameters = GetCurrentRunParameters(false),
                ActiveUserId = ActiveUser.Id,
                SortBy = field,
                SortAscending = rbAsc.Checked
            });
        }

        private void DoSearch(ShipmentViewSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<ShipmentViewSearchCriteria>(criteria));

            chkSelectAllRecords.Checked = false;
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            new ShipmentsToBeInvoicedHandler(this).Initialize();

            HasAccessToModifyShipmentsToBeInvoiced = ActiveUser.HasAccessToModify(ViewCode.ShipmentsToBeInvoiced);
            memberToolBar.LoadAdditionalActions(ToolbarMoreActions());

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            // set sort fields
            ddlSortBy.DataSource = OperationsSearchFields.ShipmentSortFields.OrderBy(s => s.DisplayName);
            ddlSortBy.DataBind();
            ddlSortBy.SelectedValue = OperationsSearchFields.ShipmentNumber.Name;

            lbtnSortShipmentNumber.CommandName = OperationsSearchFields.ShipmentNumber.Name;
            lbtnSortDateCreated.CommandName = OperationsSearchFields.DateCreated.Name;
            lbtnSortDesiredPickup.CommandName = OperationsSearchFields.DesiredPickupDate.Name;
            lbtnSortActualPickup.CommandName = OperationsSearchFields.ActualPickupDate.Name;
            lbtnSortEstimatedDelivery.CommandName = OperationsSearchFields.EstimatedDeliveryDate.Name;
            lbtnSortActualDelivery.CommandName = OperationsSearchFields.ActualDeliveryDate.Name;
            lbtnSortServiceMode.CommandName = OperationsSearchFields.ServiceMode.Name;
            lbtnSortStatus.CommandName = OperationsSearchFields.Status.Name;

            hidFlag.Value = string.Empty;	// default set;

            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);
            var criteria = new ShipmentViewSearchCriteria
            {
                ActiveUserId = ActiveUser.Id,
                Parameters = profile != null
                                ? profile.Columns
                                : OperationsSearchFields.Default.Select(f => f.ToParameterColumn()).ToList(),
                SortAscending = profile == null || profile.SortAscending,
                SortBy = profile == null ? null : profile.SortBy
            };

            var p = criteria.Parameters.FirstOrDefault(cp => cp.ReportColumnName == OperationsSearchFields.AuditedForInvoicing.ToParameterColumn().ReportColumnName);
            if (p != null) criteria.Parameters.Remove(p);

            lstFilterParameters.DataSource = criteria.Parameters;
            lstFilterParameters.DataBind();

            rbAsc.Checked = criteria.SortAscending;
            rbDesc.Checked = !criteria.SortAscending;

            if (criteria.SortBy != null && ddlSortBy.ContainsValue(criteria.SortBy.Name))
                ddlSortBy.SelectedValue = criteria.SortBy.Name;
        }



        protected void OnToolbarCommand(object sender, ViewEventArgs<string> e)
        {
            switch (e.Argument)
            {
                case SingleArgs:
                case MultiArgs:
                    ProcessInvoices(e.Argument);
                    break;
                case ImportAuditedArgs:
                    OpenImportDialog();
                    break;
            }
        }

        protected void OnDocumentDisplayClosed(object sender, EventArgs e)
        {
            documentDisplay.Visible = false;
        }

        protected void OnSearchClicked(object sender, EventArgs e)
        {
            var field = OperationsSearchFields.ShipmentSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue);
            DoSearchPreProcessingThenSearch(field);
        }


        protected void OnShipmentDetailsItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var shipment = (ShipmentDashboardDto)item.DataItem;

            if (shipment == null) return;

            var detailControl = (ShipmentDashboardDetailControl)item.FindControl("shipmentDashboardDetailControl");

            detailControl.LoadShipment(shipment);
        }

        protected void OnShippingLabelGeneratorCloseClicked(object sender, EventArgs e)
        {
            shippingLabelGenerator.Visible = false;
        }

        protected void OnGenerateShippingLabel(object sender, ViewEventArgs<DetailRecordInfo> e)
        {
            shippingLabelGenerator.GenerateLabels(e.Argument.RecordId);
            shippingLabelGenerator.Visible = true;
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }


        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = OperationsSearchFields.Shipments.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(OperationsSearchFields.Shipments
                                .Where(s => s.Name != OperationsSearchFields.AuditedForInvoicing.Name)
                                .ToList());
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;

        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        private void OpenImportDialog()
        {
            fileUploader.Visible = true;
            pnlSearch.Enabled = false;
            hidFlag.Value = ImportSetAuditedFlag;
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            if (lines.Count > 1000)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Import is limited to 1,000 records, please adjust your file size.") });
                return;
            }

            var search = new ShipmentSearch();
            var shipments = lines
                .Select(n => n[0].Trim())
                .Distinct()
                .Select(n => search.FetchShipmentByShipmentNumber(n, ActiveUser.TenantId))
                .Where(s => s != null)
                .ToList();

            if (SetShipmentsReadyToInvoice != null)
                SetShipmentsReadyToInvoice(this, new ViewEventArgs<List<Shipment>>(shipments));

            fileUploader.Visible = false;
        }

        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
            pnlSearch.Enabled = true;
            hidFlag.Value = string.Empty;
        }


        protected void OnSortData(object sender, CommandEventArgs e)
        {
            var field = OperationsSearchFields.ShipmentSortFields.FirstOrDefault(c => c.Name == e.CommandName);
            if (field == null) return;
            if (ddlSortBy.ContainsValue(field.Name))
            {
                if (ddlSortBy.SelectedValue == field.Name && rbAsc.Checked)
                {
                    rbAsc.Checked = false;
                    rbDesc.Checked = true;
                }
                else if (ddlSortBy.SelectedValue == field.Name && rbDesc.Checked)
                {
                    rbAsc.Checked = true;
                    rbDesc.Checked = false;
                }

                ddlSortBy.SelectedValue = field.Name;
            }
            DoSearchPreProcessingThenSearch(field);
        }

        protected void OnSortBySelectedIndexChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(OperationsSearchFields.ShipmentSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue));
        }

        protected void OnSortOrderCheckChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(OperationsSearchFields.ShipmentSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue));
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns.Where(s => s.ReportColumnName != OperationsSearchFields.AuditedForInvoicing.ToParameterColumn().ReportColumnName);
            lstFilterParameters.DataBind();

            rbAsc.Checked = e.Argument.SortAscending;
            rbDesc.Checked = !e.Argument.SortAscending;

            if (e.Argument.SortBy != null && ddlSortBy.ContainsValue(e.Argument.SortBy.Name))
                ddlSortBy.SelectedValue = e.Argument.SortBy.Name;
        }

        protected void OnSearchProfilesAutoRefreshChanged(object sender, ViewEventArgs<int> e)
        {
            tmRefresh.Enabled = e.Argument > default(int);
            if (e.Argument > 0) tmRefresh.Interval = e.Argument;
        }


        protected void OnTimerTick(object sender, EventArgs e)
        {
            var field = OperationsSearchFields.ShipmentSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue);
            DoSearchPreProcessingThenSearch(field);
        }
    }
}
