﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.WebApplication.Documents;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Operations;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting
{
	public partial class ShipmentAuditView : MemberPageBase
	{
		private const string GenerateShipmentStatementArgs = "GenerateShipmentStatementA";

		public override ViewCode PageCode { get { return ViewCode.ShipmentAudit; } }

		public static string PageAddress { get { return "~/Members/Accounting/ShipmentAuditView.aspx"; } }

		public override string SetPageIconImage
		{
			set { imgPageImageLogo.ImageUrl = IconLinks.FinanceBlue; }
		}


		private void GenerateShipmentStatement()
		{
			var shipment = new Shipment(hidShipmentId.Value.ToLong());
			var shipmentInvoices = new AuditInvoiceDto().FetchAuditInvoiceDtos(shipment.ShipmentNumber, DetailReferenceType.Shipment, shipment.TenantId);

			documentDisplay.Title = "Shipment Statement";
			documentDisplay.DisplayHtml(this.GenerateShipmentStatement(shipment, shipmentInvoices));
			documentDisplay.Visible = true;
		}

		private List<ToolbarMoreAction> ToolbarMoreActions()
		{
			var shipmentStatement = new ToolbarMoreAction
			{
				CommandArgs = GenerateShipmentStatementArgs,
				ImageUrl = IconLinks.Finance,
				Name = "Generate Shipment Statement",
				IsLink = false,
				NavigationUrl = string.Empty
			};
			var actions = new List<ToolbarMoreAction> { shipmentStatement };

			return actions;
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			var key = Request.QueryString[WebApplicationConstants.ShipmentAuditId].UrlTextDecrypt().ToLong();

			// permission check
			if (!ActiveUser.HasAccessToAll(ViewCode.ShipmentAudit) && ActiveUser.TenantEmployee)
			{
				tabShipment.Visible = false;
				pnlNoAccess.Visible = true;
				pnlRecordNotFound.Visible = false;

				return;
			}

			// missing key
			if (key == default(long))
			{
				tabShipment.Visible = false;
				pnlRecordNotFound.Visible = true;
				pnlNoAccess.Visible = false;
				return;
			}

			var shipment = new Shipment(key);

			LoadShipment(shipment);
			memberToolBar.LoadAdditionalActions(ToolbarMoreActions());

            hypShipmentView.NavigateUrl = string.Format("{0}?{1}={2}", ShipmentView.PageAddress, WebApplicationConstants.TransferNumber, shipment.ShipmentNumber);
            hypShipmentView.Visible = ActiveUser.HasAccessTo(ViewCode.Shipment);
		}


		protected void OnToolbarCommand(object sender, ViewEventArgs<string> e)
		{
			switch (e.Argument)
			{
				case GenerateShipmentStatementArgs:
					GenerateShipmentStatement();
					break;
			}
		}


		protected object GetPackageTypeDescription(object eval)
		{
			return (ProcessorVars.RegistryCache[ActiveUser.TenantId].PackageTypes.FirstOrDefault(t => t.Id == eval.ToLong()) ??
			        new PackageType()).TypeName;
		}

		protected object GetChargeCodeDescription(object eval)
		{
			return (ProcessorVars.RegistryCache[ActiveUser.TenantId].ChargeCodes.FirstOrDefault(t => t.Id == eval.ToLong()) ??
			        new ChargeCode()).FormattedString();
		}


		private void LoadShipment(Shipment shipment)
		{
			if (!shipment.KeyLoaded || (shipment.Customer != null && !ActiveUser.HasAccessToCustomer(shipment.Customer.Id)))
			{
				tabShipment.Visible = false;
				return;
			}

			shipment.LoadCollections();

			var shipmentInvoices = new AuditInvoiceDto().FetchAuditInvoiceDtos(shipment.ShipmentNumber, DetailReferenceType.Shipment, shipment.TenantId);
			var shipmentVendorBills = new AuditVendorBillDto().FetchAuditVendorBillDtos(shipment.ShipmentNumber, DetailReferenceType.Shipment, shipment.TenantId);

			hidShipmentId.Value = shipment.Id.ToString();
			litShipmentHeader.Text = shipment.ShipmentNumber;
			txtStatus.Text = shipment.Status.FormattedString();

			//tab counts
			tabItems.HeaderText = shipment.Items.BuildTabCount("Items");
			tabCharges.HeaderText = shipment.Charges.BuildTabCount("Charges");
			tabVendors.HeaderText = shipment.Vendors.BuildTabCount("Vendors");
			tabInvoices.HeaderText = shipmentInvoices.BuildTabCount("Invoices");
			tabVendorBills.HeaderText = shipmentVendorBills.BuildTabCount("Vendor Bills");

			//Details Tab
			txtShipmentNumber.Text = shipment.ShipmentNumber;
			txtCustomerName.Text = shipment.Customer.Name;
			txtCustomerNumber.Text = shipment.Customer.CustomerNumber;

			var origin = shipment.Origin;
			txtOrigin.Text = string.Format("{0}{1}{2} {3}{4}{5}, {6} {7} {8}",
				origin.Description, Environment.NewLine, origin.Street1, origin.Street2, Environment.NewLine, origin.City, origin.State, origin.PostalCode, origin.Country.Name);

			var destination = shipment.Destination;
			txtDestination.Text = string.Format("{0}{1}{2} {3}{4}{5}, {6} {7} {8}",
				destination.Description, Environment.NewLine, destination.Street1, destination.Street2, Environment.NewLine, destination.City, destination.State, destination.PostalCode, destination.Country.Name);

			if (shipment.Prefix == null)
			{
				txtPrefixCode.Text = WebApplicationConstants.NotApplicableShort;
				txtPrefixDescription.Text = WebApplicationConstants.NotApplicable;
			}
			else
			{
				txtPrefixCode.Text = shipment.Prefix.Code;
				txtPrefixDescription.Text = shipment.Prefix.Description;
			}

			var primaryAccountBucket = shipment.AccountBuckets.First(a => a.Primary).AccountBucket;
			txtPrimaryAccountBucket.Text = primaryAccountBucket.Code;
			txtPrimaryAccountBucketDescription.Text = primaryAccountBucket.Description;

			txtDateCreated.Text = shipment.DateCreated.FormattedShortDate();
			txtDesiredPickupDate.Text = shipment.DesiredPickupDate.FormattedShortDate();
			txtActualPickupDate.Text = shipment.ActualPickupDate.FormattedShortDateSuppressEarliestDate();
			txtEstimatedDeliveryDate.Text = shipment.EstimatedDeliveryDate.FormattedShortDate();
			txtActualDeliveryDate.Text = shipment.ActualDeliveryDate.FormattedShortDateSuppressEarliestDate();

			//Items Tab
			lstItems.DataSource = shipment.Items;
			lstItems.DataBind();

			//Charges Tab
			lstCharges.DataSource = shipment.Charges;
			lstCharges.DataBind();

			chargeStatistics.LoadCharges(shipment.Charges, shipment.ResellerAdditionId);

			//Job Summary
			var estimateCost = !shipment.Charges.Any() ? 0m : shipment.Charges.Sum(c => c.FinalBuy);
			var estimateRevenue = !shipment.Charges.Any() ? 0m : shipment.Charges.Sum(c => c.AmountDue);
			var estimateProfit = estimateRevenue - estimateCost;

			litEstimateCost.Text = estimateCost.ToString("c2");
			litEstimateRevenue.Text = estimateRevenue.ToString("c2");
			litEstimateProfit.Text = estimateProfit.ToString("c2");

			var actualCost = shipmentVendorBills
				.Select(i => i.BillType == InvoiceType.Credit ? -1 * i.AmountDue : i.AmountDue)
				.Sum();

			var actualRevenue = shipmentInvoices
				.Select(i => i.InvoiceType == InvoiceType.Credit ? -1 * i.AmountDue : i.AmountDue)
				.Sum();

			litActualCost.Text = actualCost.ToString("c2");
			litActualRevenue.Text = actualRevenue.ToString("c2");

			var actualProfit = (actualRevenue - actualCost);
			litActualProfit.Text = actualProfit.ToString("c2");

			litDiffCost.Text = (actualCost - estimateCost).ToString("c2");
			litDiffRevenue.Text = (actualRevenue - estimateRevenue).ToString("c2");
			litDiffProfit.Text = (actualProfit - estimateProfit).ToString("c2");

			litGPMEstimate.Text = estimateCost == 0 ? default(decimal).ToString("f2") : (estimateProfit / estimateCost * 100).ToString("f2");
			litGPMActual.Text = actualCost == 0 ? default(decimal).ToString("f2") : (actualProfit / actualCost * 100).ToString("f2");

			var invoiceCount = shipmentInvoices.Where(c => c.InvoiceType == InvoiceType.Invoice).Select(i => i.InvoiceNumber).Distinct().Count();
			var creditCount = shipmentInvoices.Where(c => c.InvoiceType == InvoiceType.Credit).Select(i => i.InvoiceNumber).Distinct().Count();
			var supplementalCount = shipmentInvoices.Where(c => c.InvoiceType == InvoiceType.Supplemental).Select(i => i.InvoiceNumber).Distinct().Count();
			var vendorBillCount = shipmentVendorBills.Where(c => c.BillType == InvoiceType.Invoice).Select(i => string.Format("{0}.{1}", i.DocumentNumber, i.VendorBillId)).Distinct().Count();
			var creditVendorBillCount = shipmentVendorBills.Where(c => c.BillType == InvoiceType.Credit).Select(i => i.DocumentNumber).Distinct().Count();

			litInvoiceCount.Text = invoiceCount.ToString();
			litCreditInvoicesCount.Text = creditCount.ToString();
			litSupplementalInvoicesCount.Text = supplementalCount.ToString();
			litVendorBillsCount.Text = vendorBillCount.ToString();
			litCreditVendorBillsCount.Text = creditVendorBillCount.ToString();

			var paid = shipmentInvoices
				.Where(i => i.InvoiceType != InvoiceType.Credit)
				.GroupBy(si => si.InvoiceNumber)
				.Select(i => new { i.Key, Values = i })
				.Sum(x => x.Values.First().PaidAmount);

			litTotalOutstanding.Text = actualRevenue == 0 || paid > actualRevenue ? 0.ToString("c2") : (actualRevenue - paid).ToString("c2");

			//Vendors Tab
			var vendors = shipment.Vendors
				.Select(v => new
				{
					v.Vendor.Name,
					v.Vendor.VendorNumber,
					SCAC = v.Vendor.Scac,
					v.ProNumber,
					FailureCode = v.FailureCodeId != default(long)
									? string.Format("{0} ({1})", v.FailureCode.Description, v.FailureCode.Code)
									: string.Empty,
					v.FailureComments,
					v.Primary
				})
				.ToList();

			lstVendors.DataSource = vendors.OrderBy(v => v.Primary);
			lstVendors.DataBind();

			//Invoices Tab

			lstInvoices.DataSource = shipmentInvoices;
			lstInvoices.DataBind();

			//Vendor Bills Tab
			lstVendorBills.DataSource = shipmentVendorBills;
			lstVendorBills.DataBind();
		}

		protected void OnDocumentDisplayClosed(object sender, EventArgs e)
		{
			documentDisplay.Visible = false;
		}
	}
}
