﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.Operations;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting
{
    public partial class InvoiceControlAccountUpdateView : MemberPageBase, IInvoiceControlAccountUpdatesView
    {
        public static string PageAddress { get { return "~/Members/Accounting/InvoiceControlAccountUpdateView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.InvoiceControlAccountUpdate; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.FinanceBlue; } }


        public event EventHandler RetrieveRecordsToUpdate;
        public event EventHandler<ViewEventArgs<List<Invoice>>> Save;

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void DisplayRecordsToUpdate(List<InvoiceControlAccountUpdateDto> recordsToUpdate)
        {
            lstInvoices.DataSource = recordsToUpdate;
            lstInvoices.DataBind();
        }

        public void DisplayUpdateMessages(List<ValidationMessage> messages)
        {
            lstCompleteInvoices.DataSource = messages;
            lstCompleteInvoices.DataBind();

            lstInvoices.DataSource = new List<Invoice>();
            lstInvoices.DataBind();

            memberToolBar.ShowSave = false;
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new InvoiceControlAccountUpdatesHandler(this);
            handler.Initialize();

            if (IsPostBack) return;

            hidEditingIndex.Value = string.Empty;

            if (RetrieveRecordsToUpdate != null)
                RetrieveRecordsToUpdate(this, new EventArgs());
        }



        protected void OnSaveInvoices(object sender, EventArgs e)
        {
            var invoices = new List<Invoice>();
            foreach (var i in lstInvoices.Items)
            {
                var controlAccountNumber = i.FindControl("txtControlAccount").ToTextBox().Text;
                if (string.IsNullOrEmpty(controlAccountNumber)) continue; // if not present skip!

                var id = i.FindControl("hidInvoiceId").ToCustomHiddenField().Value.ToLong();
                var invoice = new Invoice(id, id != default(long));
                invoice.LoadCollections();
                invoice.CustomerControlAccountNumber = controlAccountNumber;
                invoices.Add(invoice);
            }

            if (Save != null)
                Save(this, new ViewEventArgs<List<Invoice>>(invoices));
        }


        protected void OnCustomerControlAccountSearchClicked(object sender, ImageClickEventArgs e)
        {
            var button = (ImageButton)sender;
            hidEditingIndex.Value = button.Parent.FindControl("hidItemIndex").ToCustomHiddenField().Value;
            customerControlAccountFinder.CustomerIdFilter = button.Parent.FindControl("hidCustomerId").ToCustomHiddenField().Value.ToLong();
            customerControlAccountFinder.Visible = true;
        }

        protected void OnCustomerControlAccountFinderItemSelected(object sender, ViewEventArgs<CustomerControlAccount> e)
        {
            if (string.IsNullOrEmpty(hidEditingIndex.Value)) return;

            var index = hidEditingIndex.Value.ToInt();
            if (index >= lstInvoices.Items.Count) return;

            var textBox = lstInvoices.Items[index].FindControl("txtControlAccount").ToTextBox();

            textBox.Text = e.Argument.AccountNumber;

            customerControlAccountFinder.Visible = false;
        }

        protected void OnCustomerControlAccountFinderSelectionCancel(object sender, EventArgs e)
        {
            customerControlAccountFinder.Visible = false;
        }
    }
}
