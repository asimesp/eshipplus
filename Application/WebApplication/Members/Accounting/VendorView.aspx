﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="VendorView.aspx.cs" EnableEventValidation="false"
    Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.VendorView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.Registry" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Rating" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        ShowEdit="true" ShowDelete="true" ShowSave="true" ShowFind="true" OnFind="OnFindClicked" OnUnlock="OnUnlockClicked"
        OnSave="OnSaveClicked" OnDelete="OnDeleteClicked" OnNew="OnNewClicked" OnEdit="OnEditClicked" OnCommand="OnToolbarCommandClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="pageHeader">
        <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
            CssClass="pageHeaderIcon" />
        <h3>Vendors
            <eShip:RecordIdentityDisplayControl runat="server" ID="ridcRecordIdentity" TargetControlId="txtVendorName" />
        </h3>

        <div class="lockMessage">
            <asp:Literal ID="litMessage" runat="server" />
        </div>
    </div>

    <hr class="dark mb5" />

    <div class="errorMsgLit">
        <asp:Literal runat="server" ID="litErrorMessages" />
    </div>

    <eShip:VendorFinderControl ID="vendorFinder" Visible="false" runat="server" OpenForEditEnabled="true"
        EnableMultiSelection="false" OnItemSelected="OnVendorFinderItemSelected" OnSelectionCancel="OnVendorFinderSelectionCancel" />
    <eShip:UserFinderControl ID="userFinder" runat="server" Visible="false" EnableMultiSelection="false" FilterForEmployees="True"
        OpenForEditEnabled="False" OnItemSelected="OnUserFinderItemSelected" OnSelectionCancel="OnUserFinderSelectionCancelled" />
    <eShip:ServiceFinderControl ID="serviceFinder" runat="server" EnableMultiSelection="true" Visible="false"
        OnMultiItemSelected="OnServiceFinderMultiItemSelected" OnSelectionCancel="OnServiceFinderSelectionCancelled" />
    <eShip:EquipmentTypeFinderControl ID="equipmentTypeFinder" runat="server" EnableMultiSelection="true"
        Visible="false" OnMultiItemSelected="OnEquipmentTypeFinderMultiItemSelected"
        OnSelectionCancel="OnEquipmentTypeFinderSelectionCancelled" />
    <eShip:NoServiceDayFinderControl ID="noServiceDayFinder" runat="server" EnableMultiSelection="true"
        Visible="false" OnMultiItemSelected="OnNoServiceDayFinderMultiItemSelected" OnSelectionCancel="OnNoServiceDayFinderSelectionCancelled" />
    <script type="text/javascript">
        function ClearHidFlag() {
            jsHelper.SetEmpty('<%= hidFlag.ClientID %>');
        }
    </script>

    <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
        Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" />
    <eShip:AjaxTabHeaderTextUpdater runat="server" ID="athtuTabUpdater" />
    <eShip:VendorPerformanceSummaryControl runat="server" ID="vpscVendorPerformance" Visible="False" OnCancel="OnVendorPerformanceClose" />

    <eShip:CustomHiddenField runat="server" ID="hidVendorId" />
    <ajax:TabContainer ID="tabVendors" runat="server" CssClass="ajaxCustom">
        <ajax:TabPanel runat="server" ID="tabDetails" HeaderText="Details">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlDetails">

                    <div class="rowgroup">
                        <div class="row">
                            <div class="fieldgroup mr10">
                                <label class="wlabel blue">Date Created</label>
                                <eShip:CustomTextBox ID="txtDateCreated" runat="server" CssClass="w110 disabled" ReadOnly="True" />
                            </div>
                            <div class="fieldgroup mr10 vlinedarkright">
                                <label class="wlabel blue">Vendor Number</label>
                                <eShip:CustomTextBox ID="txtVendorNumber" MaxLength="50" ReadOnly="True" runat="server" CssClass="w200 disabled" />
                            </div>
                            <div class="fieldgroup mr10">
                                <label class="wlabel blue">Logo Image <span class="fs85em">(max: 75px by 400px)</span></label>
                                <asp:FileUpload ID="fupLogoImage" runat="server" CssClass="jQueryUniform" />
                                <asp:Button ID="btnClearLogo" runat="server" Text="CLEAR LOGO" OnClick="OnClearLogoClicked" CausesValidation="false" />
                            </div>
                            <div class="fieldgroup">
                                <asp:Image runat="server" ID="imgLogo" CssClass="right, Img60MaxHeight" Visible="False" />
                                <eShip:CustomHiddenField runat="server" ID="hidLogoUrl" />
                            </div>
                        </div>
                    </div>

                    <hr class="dark" />

                    <div class="rowgroup">
                        <div class="col_1_2 bbox">
                            <h5>General Details</h5>
                            <div class="row">
                                <div class="fieldgroup mr20">
                                    <label class="wlabel">Name</label>
                                    <eShip:CustomTextBox ID="txtVendorName" MaxLength="50" runat="server" CssClass="w200" />
                                </div>
                                <div class="fieldgroup">
                                    <label class="wlabel">&nbsp;</label>
                                    <asp:CheckBox ID="chkActive" runat="server" CssClass="jQueryUniform" />
                                    <label>Active</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="fieldgroup mr20">
                                    <label class="wlabel">Broker Reference Number</label>
                                    <eShip:CustomTextBox ID="txtBrokerReferenceNumber" MaxLength="50" runat="server" CssClass="w200" />
                                </div>
                                <div class="fieldgroup">
                                    <label class="wlabel">Broker Reference Number Type</label>
                                    <asp:DropDownList ID="ddlBrokerReferenceNumberType" DataTextField="Value" DataValueField="Key" runat="server" CssClass="w200" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="fieldgroup mr20">
                                    <label class="wlabel">DOT</label>
                                    <eShip:CustomTextBox ID="txtDOT" MaxLength="50" runat="server" CssClass="w200" />
                                </div>
                                <div class="fieldgroup">
                                    <label class="wlabel">MC</label>
                                    <eShip:CustomTextBox ID="txtMC" MaxLength="50" runat="server" CssClass="w200" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="fieldgroup mr20">
                                    <label class="wlabel">SCAC</label>
                                    <eShip:CustomTextBox ID="txtSCAC" MaxLength="4" runat="server" CssClass="w200" />
                                </div>
                                <div class="fieldgroup">
                                    <label class="wlabel">Federal ID Number</label>
                                    <eShip:CustomTextBox ID="txtFederalIdNumber" MaxLength="50" runat="server" CssClass="w200" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="fieldgroup">
                                    <label class="wlabel">Tracking Url</label>
                                    <eShip:CustomTextBox ID="txtTrackingUrl" MaxLength="200" runat="server" CssClass="w425" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="fieldgroup">
                                    <label class="wlabel">
                                        Notation <span class="fs85em">[visible to all by permission]</span>
                                        <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleNotation" TargetControlId="txtNotation" MaxLength="100" />
                                    </label>
                                    <eShip:CustomTextBox ID="txtNotation" TextMode="MultiLine" runat="server" CssClass="w425 h150" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="fieldgroup">
                                    <label class="wlabel">
                                        Notes <span class="fs85em">[internal only]</span>
                                        <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleNotes" MaxLength="1000" TargetControlId="txtNotes" />
                                    </label>
                                    <eShip:CustomTextBox ID="txtNotes" TextMode="MultiLine" runat="server" CssClass="w425 h150" />
                                </div>
                            </div>

                            <div class="row mt20">
                                <div class="fieldgroup">
                                    <asp:UpdatePanel runat="server" ID="upInternalServiceRep">
                                        <ContentTemplate>
                                            <label class="wlabel">Internal Service Representative</label>
                                            <eShip:CustomTextBox runat="server" ID="txtVendorServiceRepUsername" MaxLength="50" CssClass="w130"
                                                AutoPostBack="True" CausesValidation="False" OnTextChanged="OnVendorServiceRepUsernameTextChanged" />
                                            <asp:ImageButton runat="server" ID="imgVendorServiceRepUsernameSearch" ToolTip="Find Service Representative"
                                                ImageUrl="~/images/icons2/search_dark.png" CausesValidation="False" OnClick="OnFindVendorServiceRepUserClicked" />
                                            <eShip:CustomTextBox runat="server" ID="txtVendorServiceRepName" ReadOnly="True" CssClass="w260 disabled" />
                                            <eShip:CustomHiddenField runat="server" ID="hidVendorServiceRepUserId" />
                                            <ajax:AutoCompleteExtender runat="server" ID="aceUser" TargetControlID="txtVendorServiceRepUsername" ServiceMethod="GetUserList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                                EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="imgVendorServiceRepUsernameSearch" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                        <div class="col_1_2 bbox vlinedarkleft pl40">
                            <h5>Attributes</h5>
                            <div class="row">
                                <ul class="twocol_list">
                                    <li>
                                        <asp:CheckBox ID="chkCarrier" runat="server" CssClass="jQueryUniform" />
                                        <label>Is Carrier</label>
                                    </li>
                                    <li>
                                        <asp:CheckBox ID="chkAgent" runat="server" CssClass="jQueryUniform" />
                                        <label>Is Agent</label>
                                    </li>
                                    <li>
                                        <asp:CheckBox ID="chkBroker" runat="server" CssClass="jQueryUniform" />
                                        <label>Is Broker</label>
                                    </li>
                                </ul>
                            </div>
                            <h5>Service Modes</h5>
                            <div class="row">
                                <ul class="twocol_list">
                                    <li>
                                        <asp:CheckBox ID="chkLTL" runat="server" CssClass="jQueryUniform" />
                                        <label>Less Than Truckload</label>
                                    </li>
                                    <li>
                                        <asp:CheckBox ID="chkFTL" runat="server" CssClass="jQueryUniform" />
                                        <label>Full Truckload</label>
                                    </li>
                                    <li>
                                        <asp:CheckBox ID="chkAir" runat="server" CssClass="jQueryUniform" />
                                        <label>Air</label>
                                    </li>
                                    <li>
                                        <asp:CheckBox ID="chkPTL" runat="server" CssClass="jQueryUniform" />
                                        <label>Partial Truckload</label>
                                    </li>
                                    <li>
                                        <asp:CheckBox ID="chkRail" runat="server" CssClass="jQueryUniform" />
                                        <label>Rail</label>
                                    </li>
                                    <li>
                                        <asp:CheckBox ID="chkSmallPack" runat="server" CssClass="jQueryUniform" />
                                        <label>Small Package</label>
                                    </li>
                                </ul>
                            </div>
                            <h5>Certifications</h5>
                            <div class="row">
                                <ul class="twocol_list">
                                    <li>
                                        <asp:CheckBox ID="chkTSACertified" runat="server" CssClass="jQueryUniform" />
                                        <label>TSA</label>
                                    </li>
                                    <li>
                                        <asp:CheckBox ID="chkSmartWayCertified" runat="server" CssClass="jQueryUniform" />
                                        <label>SmartWay</label>
                                    </li>
                                    <li>
                                        <asp:CheckBox ID="chkCTPATCertified" runat="server" CssClass="jQueryUniform" />
                                        <label>CTPAT</label>
                                    </li>
                                    <li>
                                        <asp:CheckBox runat="server" ID="chkPIPCertified" CssClass="jQueryUniform" />
                                        <label>PIP</label>
                                    </li>
                                </ul>
                            </div>
                            <div class="row">
                                <div class="fieldgroup mr20">
                                    <label class="wlabel">CTPAT Number</label>
                                    <eShip:CustomTextBox runat="server" ID="txtCTPATNumber" MaxLength="50" CssClass="w230" />
                                </div>
                                <div class="fieldgroup">
                                    <label class="wlabel">PIP Number</label>
                                    <eShip:CustomTextBox runat="server" ID="txtPIPNumber" MaxLength="50" CssClass="w230" />
                                </div>
                            </div>
                            <h5 class="pt20">Custom Fields</h5>
                            <div class="row">
                                <div class="fieldgroup mr20">
                                    <label class="wlabel">Custom Field 1</label>
                                    <eShip:CustomTextBox runat="server" ID="txtCustomField1" CssClass="w200" MaxLength="50" />
                                </div>
                                <div class="fieldgroup mr20">
                                    <label class="wlabel">Custom Field 2</label>
                                    <eShip:CustomTextBox runat="server" ID="txtCustomField2" CssClass="w200" MaxLength="50" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="fieldgroup mr20">
                                    <label class="wlabel">Custom Field 3</label>
                                    <eShip:CustomTextBox runat="server" ID="txtCustomField3" CssClass="w200" MaxLength="50" />
                                </div>
                                <div class="fieldgroup mr20">
                                    <label class="wlabel">Custom Field 4</label>
                                    <eShip:CustomTextBox runat="server" ID="txtCustomField4" CssClass="w200" MaxLength="50" />
                                </div>
                            </div>
                            <h5 class="pt10">Record Audit Details</h5>
                            <asp:UpdatePanel runat="server" ID="upAuditDetails">
                                <ContentTemplate>
                                    <div class="row">
                                        <div class="fieldgroup">
                                            <label class="wlabel">Last Audit Date</label>
                                            <eShip:CustomTextBox runat="server" ID="txtLastAuditDate" ReadOnly="True" CssClass="w300 disabled mr10" />
                                            <asp:Button runat="server" ID="btnUpdateAudit" Text="Update Audit" OnClick="OnUpdateAuditClicked" OnClientClick="ShowProgressNotice(true)" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="fieldgroup">
                                            <label class="wlabel">Last Audited By</label>
                                            <eShip:CustomTextBox runat="server" ID="txtLastAuditedBy" CssClass="w425 disabled" ReadOnly="True" />
                                            <eShip:CustomHiddenField runat="server" ID="hidLastAuditedByUserId" />
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </ajax:TabPanel>
        <ajax:TabPanel runat="server" ID="tabLocations" HeaderText="Locations">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlLocations">

                    <div class="row mb10 bottom_shadow pb5">
                        <div class="fieldgroup left">
                            <asp:CheckBox runat="server" ID="chkExludeFromAvailableLoadsBlast" Text="Exclude from available loads blast" CssClass="jQueryUniform" />
                        </div>
                        <div class="fieldgroup right">
                            <asp:Button runat="server" ID="btnAddLocation" Text="Add Location"
                                CausesValidation="False" OnClick="OnAddLocationClicked" />
                        </div>
                    </div>

                    <asp:ListView ID="lstLocations" runat="server" OnItemDataBound="BindLocationLineItem"
                        ItemPlaceholderID="itemPlaceHolder">
                        <LayoutTemplate>
                            <table class="stripe">
                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td class="bottom_shadow">
                                    <eShip:LocationListingInputControl ID="llicLocation" runat="server" LocationItemIndex='<%# Container.DataItemIndex %>' OnDeleteLocation="OnDeleteLocation" />
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </asp:Panel>
            </ContentTemplate>
        </ajax:TabPanel>
        <ajax:TabPanel runat="server" ID="tabServices" HeaderText="Services">
            <ContentTemplate>

                <asp:UpdatePanel runat="server" ID="upPnlServices" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel runat="server" ID="pnlServices">

                            <div class="row mb10">
                                <div class="fieldgroup right">
                                    <asp:Button runat="server" ID="btnAddService" Text="Add Services"
                                        CausesValidation="False" OnClick="OnAddServiceClicked" />
                                    <asp:Button runat="server" ID="btnClearServices" Text="Clear Services"
                                        CausesValidation="False" OnClick="OnClearServicesClicked" />
                                </div>
                            </div>

                            <eShip:TableFreezeHeaderExtender runat="server" ID="tfheVendorServicesTable" TableId="vendorServicesTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                            <table class="stripe" id="vendorServicesTable">
                                <tr>
                                    <th>Service Code
                                    </th>
                                    <th>Service Description
                                    </th>
                                    <th>Category
                                    </th>
                                    <th>Charge Code
                                    </th>
                                    <th>Charge Code Description
                                    </th>
                                    <th class="text-center">Action
                                    </th>
                                </tr>
                                <asp:ListView ID="lstServices" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                    <LayoutTemplate>
                                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <eShip:CustomHiddenField ID="hidServiceId" runat="server" Value='<%# Eval("Id") %>' />
                                                <asp:Literal ID="litServiceCode" Text='<%# Eval("Code") %>' runat="server" />
                                            </td>
                                            <td>
                                                <asp:Literal ID="litServiceDescription" Text='<%# Eval("Description") %>' runat="server" />
                                            </td>
                                            <td>
                                                <asp:Literal ID="litServiceCategory" Text='<%# Eval("Category") %>' runat="server" />
                                            </td>
                                            <td>
                                                <eShip:CustomHiddenField ID="hidServiceChargeCodeId" Value='<%# Eval("ChargeCodeId") %>' runat="server" />
                                                <asp:Literal ID="litServiceChargeCode" Text='<%# Eval("ChargeCode") %>' runat="server" />
                                            </td>
                                            <td>
                                                <asp:Literal ID="litServiceChargeCodeDescription" Text='<%# Eval("ChargeCodeDescription") %>'
                                                    runat="server" />
                                            </td>
                                            <td class="text-center">
                                                <asp:ImageButton ID="ibtnServiceDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                    OnClick="OnDeleteServiceClicked" CausesValidation="false" Enabled='<%# Access.Modify %>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnAddService" />
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajax:TabPanel>
        <ajax:TabPanel runat="server" ID="tabEquipmentTypes" HeaderText="Equip. Types">
            <ContentTemplate>

                <asp:UpdatePanel runat="server" ID="upPnlEquipmentTypes" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel runat="server" ID="pnlEquipmentTypes">
                            <div class="row mb10">
                                <div class="fieldgroup right">
                                    <asp:Button runat="server" ID="btnAddEquipmentType" Text="Add Equipment Types"
                                        CausesValidation="False" OnClick="OnAddEquipmentTypeClicked" />
                                    <asp:Button runat="server" ID="btnClearEquipmentTypes" Text="Clear Equipment Types"
                                        CausesValidation="False" OnClick="OnClearEquipmentTypesClicked" />
                                </div>
                            </div>

                            <eShip:TableFreezeHeaderExtender runat="server" ID="tfheEquipmentTypeTable" TableId="equipmentTypeTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                            <table class="stripe" id="equipmentTypeTable">
                                <tr>
                                    <th>Code
                                    </th>
                                    <th>Type Name
                                    </th>
                                    <th>Group
                                    </th>
                                    <th>DAT Equipment Type
                                    </th>
                                    <th style="width: 5%;" class="text-center">Active
                                    </th>
                                    <th style="width: 5%;" class="text-center">Action
                                    </th>
                                </tr>
                                <asp:ListView ID="lstEquipmentTypes" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                    <LayoutTemplate>
                                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <eShip:CustomHiddenField ID="hidEquipmentTypeId" runat="server" Value='<%# Eval("Id") %>' />
                                                <asp:Literal ID="litEquipmentTypeCode" Text='<%# Eval("Code") %>' runat="server" />
                                            </td>
                                            <td>
                                                <asp:Literal ID="litEquipmentTypeTypeName" Text='<%# Eval("TypeName") %>' runat="server" />
                                            </td>
                                            <td>
                                                <%# Eval("Group").FormattedString() %>
                                                <eShip:CustomHiddenField ID="hidEquipmentTypeGroup" Value='<%# Eval("Group") %>' runat="server" />
                                            </td>
                                            <td>
                                                <%# Eval("DatEquipmentType").FormattedString() %>
                                                <eShip:CustomHiddenField ID="hidDatEquipmentType" runat="server" Value='<%# Eval("DatEquipmentType") %>' />
                                            </td>
                                            <td class="text-center">
                                                <eShip:CustomHiddenField ID="hidEquipmentTypeActive" Value='<%# Eval("Active") %>' runat="server" />
                                                <%# Eval("Active").ToBoolean() ? WebApplicationConstants.HtmlCheck : string.Empty %>
                                            </td>
                                            <td class="text-center">
                                                <asp:ImageButton ID="ibtnEquipmentTypeDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                    OnClick="OnDeleteEquipmentTypeClicked" CausesValidation="false" Enabled='<%# Access.Modify %>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnAddEquipmentType" />
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajax:TabPanel>
        <ajax:TabPanel runat="server" ID="tabVendorInsurance" HeaderText="Vendor Insurance">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlVendorInsurance">

                    <div class="row mb10">
                        <div class="fieldgroup right">
                            <asp:Button runat="server" ID="btnAddVendorInsurance" Text="Add Insurance Coverage"
                                CausesValidation="False" OnClick="OnAddVendorInsuranceClicked" />
                            <asp:Button runat="server" ID="btnClearVendorInsurance" Text="Clear Insurance Coverage"
                                CausesValidation="False" OnClick="OnClearVendorInsuranceClicked" />
                        </div>
                    </div>

                    <eShip:TableFreezeHeaderExtender runat="server" ID="tfheVendorInsuranceTable" TableId="vendorInsuranceTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                    <table class="stripe" id="vendorInsuranceTable">
                        <tr>
                            <th style="width: 3%;">&nbsp;</th>
                            <th style="width: 20%;">Carrier Name
                            </th>
                            <th style="width: 20%;">Policy Number
                            </th>
                            <th style="width: 20%;">Certificate Holder
                            </th>
                            <th style="width: 12%;" class="text-right">Coverage Amount ($)
                            </th>

                            <th style="width: 7%;">Effective
                            </th>
                            <th style="width: 8%;">Expiration
                            </th>
                            <th style="width: 5%;" class="text-center">Required
                            </th>
                            <th style="width: 5%;" class="text-center">Action
                            </th>
                        </tr>
                        <asp:ListView ID="lstVendorInsurance" runat="server" ItemPlaceholderID="itemPlaceHolder">
                            <LayoutTemplate>
                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td rowspan="2" class="top">
                                        <%# Container.DataItemIndex+1 %>.
                                    </td>
                                    <td>
                                        <eShip:CustomHiddenField ID="hidVendorInsuranceId" runat="server" Value='<%# Eval("Id") %>' />
                                        <eShip:CustomHiddenField runat="server" ID="hidVendorInsuranceEditItemIndex" Value='<%# Container.DataItemIndex %>' />
                                        <eShip:CustomTextBox ID="txtVendorInsuranceCarrierName" runat="server" Text='<%# Eval("CarrierName") %>' CssClass="w170" MaxLength="50" />
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox ID="txtVendorInsurancePolicyNumber" runat="server" Text='<%# Eval("PolicyNumber") %>' CssClass="w170" MaxLength="50" />
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox ID="txtVendorInsuranceCertificateHolder" runat="server" Text='<%# Eval("CertificateHolder") %>' CssClass="w170" MaxLength="50" />
                                    </td>
                                    <td class="text-right">
                                        <eShip:CustomTextBox ID="txtVendorInsuranceCoverageAmount" runat="server" Text='<%# Eval("CoverageAmount").ToDecimal().ToString("f4") %>' CssClass="w100" MaxLength="19" />
                                        <ajax:FilteredTextBoxExtender ID="fteVendorInsuranceCoverageAmount" runat="server" TargetControlID="txtVendorInsuranceCoverageAmount" FilterType="Custom, Numbers" ValidChars="." />
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox ID="txtVendorInsuranceEffectiveDate" runat="server" Text='<%# Eval("EffectiveDate").FormattedShortDate() %>' CssClass="w100" Type="Date" placeholder="99/99/9999" />
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox ID="txtVendorInsuranceExpirationDate" runat="server" Text='<%# Eval("ExpirationDate").FormattedShortDate() %>' CssClass="w100" Type="Date" placeholder="99/99/9999" />
                                    </td>
                                    <td class="text-center">
                                        <eShip:AltUniformCheckBox ID="chkVendorInsuranceRequired" Checked='<%# Eval("Required") %>' runat="server" />
                                    </td>
                                    <td class="text-center top" rowspan="2">
                                        <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                            CausesValidation="false" OnClick="OnDeleteVendorInsuranceClicked" Enabled='<%# Access.Modify %>' />
                                    </td>
                                </tr>
                                <tr class="hidden">
                                    <td>&nbsp;</td>
                                </tr>
                                <tr class="bottom_shadow">
                                    <td colspan="8">
                                        <div class="row">
                                            <div class="fieldgroup mr20">
                                                <label class="wlabel blue">
                                                    Special Instructions
                                                    <eShip:TextAreaMaxLengthExtender runat="server" MaxLength="500" ID="tamleVendorInsuranceSpecialInstruction" TargetControlId="txtVendorInsuranceSpecialInstruction" />
                                                </label>
                                                <eShip:CustomTextBox ID="txtVendorInsuranceSpecialInstruction" runat="server" Text='<%# Eval("SpecialInstruction") %>' TextMode="MultiLine" CssClass="w740" />
                                            </div>
                                            <div class="fieldgroup">
                                                <label class="wlabel blue">Insurance Type</label>
                                                <eShip:CachedObjectDropDownList Type="InsuranceTypes" ID="ddlVendorInsuranceType" runat="server" CssClass="w200"
                                                    EnableChooseOne="True" DefaultValue="0" SelectedValue='<%# Eval("InsuranceTypeId") %>' />
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </ajax:TabPanel>
        <ajax:TabPanel runat="server" ID="tabVendorPackageCustomMapping" HeaderText="Pkg. Mappings">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlVendorPackageCustomMapping">

                    <div class="row mb10">
                        <div class="fieldgroup right">
                            <asp:Button runat="server" ID="Button1" Text="Add Package Mapping"
                                CausesValidation="False" OnClick="OnAddVendorPackageCustomMappingClicked" />
                            <asp:Button runat="server" ID="Button2" Text="Clear Package Mapping"
                                CausesValidation="False" OnClick="OnClearVendorPackageCustomMappingClicked" />
                        </div>
                    </div>

                    <eShip:TableFreezeHeaderExtender runat="server" ID="tfheVendorPackageCustomMapping" TableId="vendorPackageCustomMapping" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                    <table class="stripe" id="vendorPackageCustomMapping">
                        <tr>
                            <th style="width: 3%;">&nbsp;</th>
                            <th style="width: 25%;">Package Type
                            </th>
                            <th style="width: 25%;">Vendor Code
                            </th>
                            <th style="width: 5%;" class="text-center">Action
                            </th>
                        </tr>
                        <asp:ListView ID="lstVendorPackageCustomMapping" runat="server" ItemPlaceholderID="itemPlaceHolder">
                            <LayoutTemplate>
                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="top">
                                        <%# Container.DataItemIndex+1 %>.
                                    </td>
                                    <td>
                                        <eShip:CustomHiddenField ID="hidVendorPackageCustomMappingId" runat="server" Value='<%# Eval("Id") %>' />
                                        <eShip:CustomHiddenField runat="server" ID="hidVendorPackageCustomMappingEditItemIndex" Value='<%# Container.DataItemIndex %>' />
                                         <eShip:CachedObjectDropDownList Type="PackageType" ID="ddlPackageType" runat="server" CssClass="w200"
                                                    EnableChooseOne="True" DefaultValue="0" SelectedValue='<%# Eval("PackageTypeId") %>' />
                                    </td>
                                    <td>
                                        <eShip:CustomTextBox ID="txtVendorPackageCustomMappingVendorCode" runat="server" Text='<%# Eval("VendorCode") %>' CssClass="w170" MaxLength="50" />
                                    </td>
                                    <td class="text-center top">
                                        <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                            CausesValidation="false" OnClick="OnDeleteVendorPackageCustomMappingClicked" Enabled='<%# Access.Modify %>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </asp:Panel>
            </ContentTemplate>
        </ajax:TabPanel>
        <ajax:TabPanel ID="tabDocuments" runat="server" HeaderText="Documents">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlDocuments">

                    <div class="row mb10">
                        <div class="fieldgroup right">
                            <asp:Button runat="server" ID="btnAddDocument" Text="Add Document"
                                CausesValidation="false" OnClick="OnAddDocumentClicked" />
                            <asp:Button runat="server" ID="btnClearDocuments" Text="Clear Documents"
                                CausesValidation="false" OnClick="OnClearDocumentsClicked" />
                        </div>
                    </div>

                    <script type="text/javascript">
                        $(document).ready(function () {
                            $("a[id$='lnkVendorDocumentLocationPath']").each(function () {
                                jsHelper.SetDoPostBackJsLink($(this).prop('id'));
                            });
                        });
                    </script>

                    <eShip:TableFreezeHeaderExtender runat="server" ID="tfheDocumentsTable" TableId="documentsTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                    <table class="stripe" id="documentsTable">
                        <tr>
                            <th style="width: 5%;" class="text-center">Internal
                            </th>
                            <th style="width: 20%;">Name
                            </th>
                            <th style="width: 25%;">Description
                            </th>
                            <th style="width: 22%;">Document Tag
                            </th>
                            <th style="width: 20%;">File
                            </th>
                            <th style="width: 8%;" class="text-center">Action
                            </th>
                        </tr>
                        <asp:ListView ID="lstDocuments" runat="server" ItemPlaceholderID="itemPlaceHolder">
                            <LayoutTemplate>
                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="text-center top">
                                        <eShip:CustomHiddenField runat="server" ID="hidIsInternal" Value='<%# Eval("IsInternal") %>' />
                                        <%# Eval("IsInternal").ToBoolean() ? WebApplicationConstants.HtmlCheck : string.Empty %>
                                    </td>
                                    <td class="top">
                                        <eShip:CustomHiddenField ID="hidDocumentIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                        <eShip:CustomHiddenField ID="hidDocumentId" runat="server" Value='<%# Eval("Id") %>' />
                                        <asp:Literal ID="litDocumentName" runat="server" Text='<%# Eval("Name") %>' />
                                    </td>
                                    <td class="top">
                                        <asp:Literal ID="litDocumentDescription" runat="server" Text='<%# Eval("Description") %>' />
                                    </td>
                                    <td class="top">
                                        <eShip:CustomHiddenField ID="hidDocumentTagId" runat="server" Value='<%# Eval("DocumentTagId") %>' />
                                        <%# new DocumentTag(Eval("DocumentTagId").ToLong()).Description %>
                                    </td>
                                    <td class="top">
                                        <eShip:CustomHiddenField ID="hidLocationPath" runat="server" Value='<%# Eval("LocationPath") %>' />
                                        <asp:LinkButton runat="server" ID="lnkVendorDocumentLocationPath" Text='<%# GetLocationFileName(Eval("LocationPath")) %>'
                                            OnClick="OnLocationPathClicked" CausesValidation="false" CssClass="blue" />
                                    </td>
                                    <td class="text-center top">
                                        <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png"
                                            CausesValidation="false" OnClick="OnEditDocumentClicked" Enabled='<%# Access.Modify %>' />
                                        <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                            CausesValidation="false" OnClick="OnDeleteDocumentClicked" Enabled='<%# Access.Modify %>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>

                    <asp:Panel runat="server" ID="pnlEditDocument" Visible="False" CssClass="popup popupControlOverW500">
                        <div class="popheader mb10">
                            <h4>Add/Modify Document
                            </h4>
                            <asp:ImageButton ID="ImageButton5" ImageUrl="~/images/icons2/close.png" CssClass="close"
                                CausesValidation="false" OnClick="OnCloseEditDocumentClicked" runat="server" />
                        </div>

                        <table class="poptable">
                            <tr>
                                <td class="text-right">
                                    <label class="upper">Name:</label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox runat="server" ID="txtDocumentName" CssClass="w200" MaxLength="50" />
                                    <asp:CheckBox runat="server" ID="chkDocumentIsInternal" CssClass="jQueryUniform ml10" />
                                    <label>Is Internal</label>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <label class="upper">Document Tag:</label>
                                </td>
                                <td>
                                    <eShip:CachedObjectDropDownList Type="DocumentTag" ID="ddlDocumentTag" runat="server"
                                        CssClass="w300" EnableChooseOne="True" DefaultValue="0" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right top">
                                    <label class="upper">Description:</label>
                                    <br />
                                    <label class="lhInherit">
                                        <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDocumentDescription" MaxLength="500" TargetControlId="txtDocumentDescription" />
                                    </label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox runat="server" ID="txtDocumentDescription" TextMode="MultiLine" CssClass="w300 h150" />
                                </td>
                            </tr>

                            <tr>
                                <td class="text-right">
                                    <label class="upper">Location Path:</label>
                                </td>
                                <td>
                                    <eShip:CustomHiddenField ID="hidLocationPath" runat="server" />
                                    <asp:FileUpload runat="server" ID="fupLocationPath" CssClass="jQueryUniform" />
                                </td>
                            </tr>
                            <tr>
                                <td class="text-right">
                                    <label class="upper">Current File:</label>
                                </td>
                                <td>
                                    <eShip:CustomTextBox ID="txtLocationPath" runat="server" ReadOnly="True" CssClass="w300 disabled" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <asp:Button ID="btnEditDocumentDone" Text="Done" OnClick="OnEditDocumentDoneClicked" runat="server" CausesValidation="false" />
                                    <asp:Button ID="btnCloseEditDocument" Text="Cancel" OnClick="OnCloseEditDocumentClicked" runat="server" CausesValidation="false" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </asp:Panel>
            </ContentTemplate>
        </ajax:TabPanel>
        <ajax:TabPanel runat="server" ID="tabNoServiceDays" HeaderText="No Service Days">
            <ContentTemplate>

                <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Panel runat="server" ID="pnlNoServiceDays">
                            <div class="row mb10">
                                <div class="fieldgroup right">
                                    <asp:Button runat="server" ID="btnAddNoServiceDays" Text="Add No Service Days"
                                        CausesValidation="False" OnClick="OnAddNoServiceDayClicked" />
                                    <asp:Button runat="server" ID="btnClearNoServiceDays" Text="Clear No Service Days"
                                        CausesValidation="False" OnClick="OnClearNoServiceDayClicked" />
                                </div>
                            </div>

                            <eShip:TableFreezeHeaderExtender runat="server" ID="tfheNoServiceDaysTable" TableId="noServiceDaysTable" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                            <table class="stripe" id="noServiceDaysTable">
                                <tr>
                                    <th style="width: 30%;">Date Of No Service
                                    </th>
                                    <th style="width: 65%;">Description
                                    </th>
                                    <th style="width: 5%;" class="text-center">Action
                                    </th>
                                </tr>
                                <asp:ListView ID="lstNoServiceDays" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                    <LayoutTemplate>
                                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <eShip:CustomHiddenField ID="hidNoServiceDayId" runat="server" Value='<%# Eval("Id") %>' />
                                                <asp:Literal ID="litNoServiceDayDate" Text='<%# Eval("DateOfNoService").FormattedShortDate() %>'
                                                    runat="server" />
                                            </td>
                                            <td>
                                                <asp:Literal ID="litNoServiceDayDescription" Text='<%# Eval("Description") %>' runat="server" />
                                            </td>
                                            <td class="text-center">
                                                <asp:ImageButton ID="ibtnNoServiceDayDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                    OnClick="OnDeleteNoServiceDayClicked" CausesValidation="false" Enabled='<%# Access.Modify %>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:ListView>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnAddNoServiceDays" />
                    </Triggers>
                </asp:UpdatePanel>
            </ContentTemplate>
        </ajax:TabPanel>
        <ajax:TabPanel runat="server" ID="tabAuditLog" HeaderText="Audit Log">
            <ContentTemplate>
                <eShip:AuditLogControl ID="auditLogs" runat="server" />
            </ContentTemplate>
        </ajax:TabPanel>
    </ajax:TabContainer>
    <asp:Panel ID="vendorRatingsDisplay" runat="server" Visible="False" CssClass="popup popupControlOverW750">
        <div class="popheader">
            <h4>Vendor Ratings
            </h4>
            <asp:ImageButton ID="ibtnClose2" ImageUrl="~/images/icons2/close.png" CssClass="close"
                CausesValidation="false" OnClick="OnVendorRatingsDisplaySelectionCancelled" runat="server" />
        </div>
        <div class="row">
            <table id="ratingTable" class="poptable stripe">
                <tr>
                    <th style="width: 42%">Name </th>
                    <th style="width: 42%">Display Name</th>
                    <th style="width: 6%" class="text-center">Active</th>
                    <th style="width: 10%">&nbsp;</th>
                </tr>
                <asp:ListView ID="lstVendorRatings" runat="server" ItemPlaceholderID="itemPlaceHolder">
                    <LayoutTemplate>
                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <%# Eval("Name") %>
                            </td>
                            <td><%# Eval("DisplayName") %></td>
                            <td class="text-center">
                                <%# Eval("Active").ToBoolean() ? WebApplicationConstants.HtmlCheck : string.Empty %>
                            </td>
                            <td class="text-center">
                                <a target="_blank" href='<%# ResolveUrl(string.Format("{0}?{1}={2}", VendorRatingView.PageAddress, WebApplicationConstants.TransferNumber, Eval("Id").GetString().UrlTextEncrypt())) %>'
                                    title="Go to Vendor Rating Record">
                                    <img src='<%# ResolveUrl("~/images/icons2/arrow_newTab.png") %>' alt="" />
                                </a>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </table>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
    <eShip:CustomHiddenField runat="server" ID="hidEditingIndex" />
    <eShip:CustomHiddenField runat="server" ID="hidFilesToDelete" />
    <eShip:CustomHiddenField runat="server" ID="hidCommunicationId" />
    <eShip:CustomHiddenField runat="server" ID="hidHasRating" />
</asp:Content>
