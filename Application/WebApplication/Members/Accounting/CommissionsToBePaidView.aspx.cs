﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting
{
    public partial class CommissionsToBePaidView : MemberPageBaseWithPageStore, ICommissionsToBePaidView
    {
        private const string ExportFlag = "ExportFlag";
        private const string ImportFlag = "ImportFlag";
        private const string CommissionPaymentStoreKey = "CPSK";

        private const int MaxCommissionImportLines = 2000;

        private string ResultFlag
        {
            get { return ViewState["ResultFlag"].GetString(); }
            set { ViewState["ResultFlag"] = value; }
        }

        protected List<CommissionPayment> StoredCommissionPayments
        {
            get { return PageStore[CommissionPaymentStoreKey] as List<CommissionPayment> ?? new List<CommissionPayment>(); }
            set { PageStore[CommissionPaymentStoreKey] = value; }
        }



        public static string PageAddress { get { return "~/Members/Accounting/CommissionsToBePaidView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.CommissionsToBePaid; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.FinanceBlue; } }

        public event EventHandler<ViewEventArgs<List<CommissionPayment>>> Save;
        public event EventHandler<ViewEventArgs<CommissionsToBePaidViewSearchCriteria>> Search;
        public event EventHandler<ViewEventArgs<List<CommissionPayment>>> BatchImport;


        public void DisplaySearchResult(List<CommissionsToBePaidViewSearchDto> commissions)
        {
            if (commissions == null) return;
            switch (ResultFlag)
            {
                case ExportFlag:
                    DoExport(commissions);
                    break;
                default:
                    lstCommissions.DataSource = commissions.OrderBy(c => c.SalesRepName).ThenBy(c => c.ReferenceNumber);
                    lstCommissions.DataBind();
                    break;
            }

            litRecordCount.Text = commissions.BuildRecordCount();

            if (!commissions.Any())
                DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSearchResultsMsg) });
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (hidFlag.Value == ImportFlag)
            {
                pnlImportComplete.Visible = true;
                pnlDimScreen.Visible = true;
                hidFlag.Value = string.Empty;
                return;
            }

            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join("<br />", messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }



        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems { Columns = GetCurrentRunParameters(true) };
        }

        private void DoExport(IEnumerable<CommissionsToBePaidViewSearchDto> commissions)
        {
            var q = commissions.Select(a => a.ToString()).ToList();
            q.Insert(0, CommissionsToBePaidViewSearchDto.Header());
            Response.Export(q.ToArray().NewLineJoin());
        }

        private void DoSearch()
        {
            var criteria = new CommissionsToBePaidViewSearchCriteria
            {
                Parameters = GetCurrentRunParameters(false),
                ActiveUserId = ActiveUser.Id,
                OnlyCommsWithNoPayment = chkFilterPaid.Checked
            };

            if (Search != null)
                Search(this, new ViewEventArgs<CommissionsToBePaidViewSearchCriteria>(criteria));
        }




        protected void Page_Load(object sender, EventArgs e)
        {
            new CommissionsToBePaidHandler(this).Initialize();

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;
            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.CommissionsToBePaidImportTemplate });

            chkFilterPaid.Checked = false;

            //get criteria
            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);
            var defaultCriteria = new CommissionsToBePaidViewSearchCriteria
            {
                ActiveUserId = ActiveUser.Id,
                Parameters = profile != null
                                 ? profile.Columns
                                 : AccountingSearchFields.DefaultCommissionsToBePaid.Select(f => f.ToParameterColumn()).ToList(),
            };

            lstFilterParameters.DataSource = defaultCriteria.Parameters;
            lstFilterParameters.DataBind();
        }


        protected void OnToolbarSave(object sender, EventArgs e)
        {
            var commissions = new List<CommissionPayment>();

            foreach (var i in lstCommissions.Items.Where(i => i.FindControl("chkSelected").ToAltUniformCheckBox().Checked))
            {
                var salesRepId = i.FindControl("hidSalesRepresentativeId").ToCustomHiddenField().Value.ToLong();
                var paymentDate = i.FindControl("txtPaymentDate").ToTextBox().Text.ToDateTime();
                var refNumber = i.FindControl("litRefNumber").ToLiteral().Text;
                var amountPaid = i.FindControl("txtAmountPaid").ToTextBox().Text.ToDecimal();
                var addlEntityAmountPaid = i.FindControl("txtAdditionalEntityAmountPaid").ToTextBox().Text.ToDecimal();
                var type = i.FindControl("chkReversal").ToAltUniformCheckBox().Checked
                               ? CommissionPaymentType.Reversal
                               : CommissionPaymentType.Payment;

                if (amountPaid > 0)
                    commissions.Add(new CommissionPayment
                    {
                        SalesRepresentativeId = salesRepId,
                        DateCreated = DateTime.Now,
                        PaymentDate = paymentDate,
                        ReferenceNumber = refNumber,
                        AmountPaid = amountPaid,
                        Type = type,
                        AdditionalEntityCommission = false,
                        TenantId = ActiveUser.TenantId
                    });

                if (addlEntityAmountPaid > 0)
                    commissions.Add(new CommissionPayment
                    {
                        SalesRepresentativeId = salesRepId,
                        DateCreated = DateTime.Now,
                        PaymentDate = paymentDate,
                        ReferenceNumber = refNumber,
                        AmountPaid = addlEntityAmountPaid,
                        Type = type,
                        AdditionalEntityCommission = true,
                        TenantId = ActiveUser.TenantId
                    });
            }

            if (Save != null)
                Save(this, new ViewEventArgs<List<CommissionPayment>>(commissions));

            chkSelectAllRecords.Checked = false;

            DoSearch();
        }

        protected void OnToolbarImport(object sender, EventArgs e)
        {
            fileUploader.Title = "COMMISSIONS TO BE PAID IMPORT";
            fileUploader.Instructions = @"**Please refer to help menu for import file format.";
            fileUploader.Visible = true;
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            ResultFlag = string.Empty;
            DoSearch();
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            ResultFlag = ExportFlag;
            DoSearch();
        }



        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = AccountingSearchFields.CommissionsToBePaid.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

        }



        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(AccountingSearchFields.CommissionsToBePaid);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;

        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }



        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();
        }


        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            if (lines.Count > MaxCommissionImportLines)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Maximum number of records that can be processed at one time is {0}", MaxCommissionImportLines) });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 5;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var sParameters = chks
                .Select(i => i.Line[0])
                .Distinct()
                .Select(n =>
                {
                    var p = AccountingSearchFields.ReferenceNumber.ToParameterColumn();
                    p.DefaultValue = n;
                    p.Operator = Operator.Equal;
                    return p;
                })
                .ToList();

            var paymentDtos = new List<CommissionsToBePaidViewSearchDto>();
            if (sParameters.Any())paymentDtos = new CommissionsToBePaidSearch().FetchCommissionsToBePaid(new CommissionsToBePaidViewSearchCriteria{ActiveUserId = ActiveUser.Id,Parameters = sParameters}, ActiveUser.TenantId);
            msgs.AddRange(chks.CodesAreValid(0, paymentDtos.Select(s => s.ReferenceNumber).ToList(), "Reference Number"));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            // check that all amounts paid are non-negative
            msgs.AddRange(chks
                .Where(l => l.Line[2].ToDecimal() < 0)
                .Select(l => ValidationMessage.Error("Amount Paid cannot be less than zero on line {0}", l.Index)));
            msgs.AddRange(chks
                .Where(l => l.Line[3].ToDecimal() < 0)
                .Select(l => ValidationMessage.Error("Additional Entity Amount Paid cannot be less than zero on line {0}", l.Index)));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var payments = new List<CommissionPayment>();

            foreach (var line in lines)
            {
                var salesRepId = paymentDtos.First(s => s.ReferenceNumber == line[0]).SalesRepId;

                if (line[2].ToDecimal() > 0)
                    payments.Add(new CommissionPayment
                        {
                            SalesRepresentativeId = salesRepId,
                            DateCreated = DateTime.Now,
                            PaymentDate = line[1].ToDateTime(),
                            ReferenceNumber = line[0],
                            AmountPaid = line[2].ToDecimal(),
                            Type = line[4].ToBoolean() ? CommissionPaymentType.Reversal : CommissionPaymentType.Payment,
                            AdditionalEntityCommission = false,
                            TenantId = ActiveUser.TenantId
                        });

                if (line[3].ToDecimal() > 0)
                    payments.Add(new CommissionPayment
                    {
                        SalesRepresentativeId = salesRepId,
                        DateCreated = DateTime.Now,
                        PaymentDate = line[1].ToDateTime(),
                        ReferenceNumber = line[0],
                        AmountPaid = line[3].ToDecimal(),
                        Type = line[4].ToBoolean() ? CommissionPaymentType.Reversal : CommissionPaymentType.Payment,
                        AdditionalEntityCommission = true,
                        TenantId = ActiveUser.TenantId
                    });
            }

            hidFlag.Value = ImportFlag;

            if (BatchImport != null)
                BatchImport(this, new ViewEventArgs<List<CommissionPayment>>(payments));

            StoredCommissionPayments = payments;

            fileUploader.Visible = false;
            DoSearch();
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }



        protected void OnDownloadImportResultsFile(object sender, EventArgs e)
        {
            var payments = StoredCommissionPayments;

            var lines = new List<string>{new[]{"Reference Number", "Payment Date", "Amount Paid", "For Additional Entity", "Is Reversal", "Result"}.TabJoin()};

            lines.AddRange(payments.Select(p =>
                                           new[]
                                               {
                                                   p.ReferenceNumber,
                                                   p.PaymentDate.FormattedShortDate(),
                                                   p.AmountPaid.ToString("n2"),
                                                   p.AdditionalEntityCommission.ToString(),
                                                   p.Type.ToString(),
                                                   p.IsNew ? "FAILURE" : "SUCCESS"
                                               }.TabJoin()));

            Response.Export(lines.ToArray().NewLineJoin(), "CommissionPaymentImportResults.txt");
        }

        protected void OnCloseClicked(object sender, ImageClickEventArgs e)
        {
            pnlDimScreen.Visible = false;
            pnlImportComplete.Visible = false;
            StoredCommissionPayments = new List<CommissionPayment>();
        }
    }
}