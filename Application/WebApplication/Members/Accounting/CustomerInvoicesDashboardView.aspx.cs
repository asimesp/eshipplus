﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ionic.Zip;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Documents;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting
{
    public partial class CustomerInvoicesDashboardView : MemberPageBase, ICustomerInvoiceDashboardView
    {
        private const long BolTagId = -1;

        private SearchField SortByField
        {
            get { return AccountingSearchFields.CustomerInvoices.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue); }
        }


        protected Permission InvoiceAccess { get; set; }

        public static string PageAddress { get { return "~/Members/Accounting/CustomerInvoicesDashboardView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.CustomerInvoiceDashboard; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.FinanceBlue; } }


        public List<DocumentTag> DocumentTags
        {
            set
            {
                var tags = value
                    .Select(tag => new ViewListItem(string.Format(" {0}", tag.Description), tag.Id.ToString()))
                    .ToList();

                tags.Add(new ViewListItem(" " + WebApplicationConstants.BOLDescription, BolTagId.ToString()));

                rptDocTagsToDownload.DataSource = tags.OrderBy(v => v.Text);
                rptDocTagsToDownload.DataBind();
            }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<InvoiceViewSearchCriteria>> Search;

        public void DisplaySearchResult(List<InvoiceDashboardDto> invoices)
        {
            litRecordCount.Text = invoices.BuildRecordCount();
            upcseDataUpdate.DataSource = invoices;
            upcseDataUpdate.DataBind();
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }


        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems
            {
                Columns = GetCurrentRunParameters(true),
                SortAscending = rbAsc.Checked,
                SortBy = SortByField,
            };
        }

        private void DoSearchPreProcessingThenSearch(SearchField field)
        {
            DoSearch(new InvoiceViewSearchCriteria
            {
                Parameters = GetCurrentRunParameters(false),
                ActiveUserId = ActiveUser.Id,
                SortBy = field,
                SortAscending = rbAsc.Checked
            });
        }

        private void DoSearch(InvoiceViewSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<InvoiceViewSearchCriteria>(criteria));
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new CustomerInvoiceDashboardHandler(this).Initialize();

            InvoiceAccess = ActiveUser.RetrievePermission(ViewCode.Invoice);

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            if (Loading != null)
                Loading(this, new EventArgs());

            // set sort fields
            ddlSortBy.DataSource = AccountingSearchFields.CustomerInvoiceSortFields.OrderBy(s => s.DisplayName);
            ddlSortBy.DataBind();
            ddlSortBy.SelectedValue = AccountingSearchFields.InvoiceNumber.Name;

            //link up sort buttons
            lbtnSortInvoiceNumber.CommandName = AccountingSearchFields.InvoiceNumber.Name;
            lbtnSortPostDate.CommandName = AccountingSearchFields.PostDate.Name;
            lbtnSortDueDate.CommandName = AccountingSearchFields.DueDate.Name;
            lbtnSortPaidAmount.CommandName = AccountingSearchFields.PaidAmount.Name;
            lbtnSortAmountDue.CommandName = AccountingSearchFields.AmountDue.Name;
            lbtnSortCustomerName.CommandName = AccountingSearchFields.CustomerName.Name;
            lbtnSortCustomerNumber.CommandName = AccountingSearchFields.CustomerNumber.Name;
            lbtnSortTypeText.CommandName = AccountingSearchFields.InvoiceTypeText.Name;

            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);
            var criteria = new InvoiceViewSearchCriteria
            {
                ActiveUserId = ActiveUser.Id,
                Parameters = profile != null
                                 ? profile.Columns
                                 : AccountingSearchFields.DefaultInvoices.Select(
                                     f => f.ToParameterColumn()).ToList(),
                SortAscending = profile == null || profile.SortAscending,
                SortBy = profile == null ? null : profile.SortBy
            };

            lstFilterParameters.DataSource = criteria.Parameters;
            lstFilterParameters.DataBind();

            rbAsc.Checked = criteria.SortAscending;
            rbDesc.Checked = !criteria.SortAscending;

            if (criteria.SortBy != null && ddlSortBy.ContainsValue(criteria.SortBy.Name))
                ddlSortBy.SelectedValue = criteria.SortBy.Name;
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            chkSelectAllRecords.Checked = false;
            upcseDataUpdate.ResetLoadCount();
            var field = AccountingSearchFields.CustomerInvoiceSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue);
            DoSearchPreProcessingThenSearch(field);
        }



        protected void GenerateDocumentPackageRequest(long invoiceId, List<long> tagIds)
        {
            var invoice = new Invoice(invoiceId);
            invoice.LoadCollections();

            var details = invoice.Details
                .Where(d => d.ReferenceType == DetailReferenceType.Shipment)
                .Select(d => new { d.ReferenceNumber })
                .Distinct()
                .ToList();


            ZipFile zip;
			var zipFileName = Server.MakeUniqueFilename(WebApplicationSettings.TempFolder + string.Format("{0:yyyyMMdd_hhmmss}_Document_Package.zip", DateTime.Now));
	        using (zip = new ZipFile(zipFileName))
	        {
		        foreach (var validReferenceNumber in details)
		        {
			        var shipment = new ShipmentSearch().FetchShipmentByShipmentNumber(validReferenceNumber.ReferenceNumber, ActiveUser.TenantId);
			        shipment.LoadCollections();

			        if (tagIds.Contains(BolTagId))
			        {
				        var entryName = string.Format("{0}_Bol.pdf", validReferenceNumber.ReferenceNumber);
				        zip.AddEntry(entryName, this.GenerateBOL(shipment, true).ToPdf(shipment.ShipmentNumber));
			        }


			        foreach (var document in shipment.Documents.Where(d => !d.IsNew && !d.IsInternal && tagIds.Contains(d.DocumentTagId)))
			        {
				        var fileName = Server.MapPath(document.LocationPath);
				        if (!File.Exists(fileName)) continue;
				        var info = new FileInfo(fileName);
				        var name = string.Format("{0}_{1}_{2}{3}", shipment.ShipmentNumber,document.DocumentTagId == default(long) ? string.Empty : document.DocumentTag.Code,  document.Name, info.Extension);
				        if (!zip.ContainsEntry(name))
					        zip.AddEntry(name, fileName.ReadFromFile());
			        }

                    var serviceTickets = invoice.RetrieveAssociatedServiceTickets();

                    foreach (var serviceTicket in serviceTickets)
                    {
                        serviceTicket.LoadCollections();

                        foreach (var document in serviceTicket.Documents.Where(d => !d.IsNew && !d.IsInternal && tagIds.Contains(d.DocumentTagId)))
                        {
                            var fileName = Server.MapPath(document.LocationPath);
                            if (!File.Exists(fileName)) continue;
                            var info = new FileInfo(fileName);
                            var name = string.Format("{0}_{1}_{2}{3}", serviceTicket.ServiceTicketNumber, document.DocumentTagId == default(long) ? string.Empty : document.DocumentTag.Code, document.Name, info.Extension);
                            if (!zip.ContainsEntry(name))
                                zip.AddEntry(name, fileName.ReadFromFile());
                        }
                    }
		        }

		        var invoiceName = string.Format("{0}_Invoice.pdf", invoice.InvoiceNumber);
		        zip.AddEntry(invoiceName, this.GenerateInvoice(invoice, true).ToPdf(invoice.InvoiceNumber));

				zip.Save();
	        }

			var zipFileInfo = new FileInfo(zipFileName);
			Response.Export(zipFileInfo, zipFileInfo.Name);
			zipFileInfo.Delete();
        }

        protected void OnGenerateInvoiceRequest(object sender, ImageClickEventArgs e)
        {
            var button = (Control) sender;
            var invoiceId = button.FindControl("hidInvoiceId").ToCustomHiddenField().Value.ToLong();
            var tagIds = rptDocTagsToDownload.Items
                .Cast<RepeaterItem>()
                .Where(i => i.FindControl("chkSelected").ToCheckBox().Checked)
                .Select(i => i.FindControl("hidDocumentTagId").ToCustomHiddenField().Value.ToLong())
                .ToList();
            if (tagIds.Any())
            {
                GenerateDocumentPackageRequest(invoiceId, tagIds);
            }
            else
            {
                documentDisplay.Visible = true;
                hidLastInvoiceId.Value = invoiceId.ToString();
                var invoice = new Invoice(invoiceId);
                documentDisplay.Title = string.Format("{0} Invoice", invoice.InvoiceNumber);
                documentDisplay.DisplayHtml(this.GenerateInvoice(invoice));
                documentDisplay.Visible = true;
            }
        }

        protected void OnDocumentDisplayClosed(object sender, EventArgs e)
        {
            documentDisplay.Visible = false;
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = AccountingSearchFields.CustomerInvoices.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(AccountingSearchFields.CustomerInvoices);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;

        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSortData(object sender, CommandEventArgs e)
        {
            var field = AccountingSearchFields.CustomerInvoiceSortFields.FirstOrDefault(c => c.Name == e.CommandName);
            if (field == null) return;
            if (ddlSortBy.ContainsValue(field.Name))
            {
                if (ddlSortBy.SelectedValue == field.Name && rbAsc.Checked)
                {
                    rbAsc.Checked = false;
                    rbDesc.Checked = true;
                }
                else if (ddlSortBy.SelectedValue == field.Name && rbDesc.Checked)
                {
                    rbAsc.Checked = true;
                    rbDesc.Checked = false;
                }

                ddlSortBy.SelectedValue = field.Name;
            }

            DoSearchPreProcessingThenSearch(field);
        }

        protected void OnSortBySelectedIndexChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(AccountingSearchFields.CustomerInvoiceSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue));
        }

        protected void OnSortOrderCheckChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(AccountingSearchFields.CustomerInvoiceSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue));
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();

            rbAsc.Checked = e.Argument.SortAscending;
            rbDesc.Checked = !e.Argument.SortAscending;

            if (e.Argument.SortBy != null && ddlSortBy.ContainsValue(e.Argument.SortBy.Name))
                ddlSortBy.SelectedValue = e.Argument.SortBy.Name;
        }


        protected void OnPayForInvoicesClicked(object sender, EventArgs e)
        {
            var invoices = lstInvoiceDetails
                .Items
                .Where(i => i.FindControl("chkSelected").ToAltUniformCheckBox().Checked)
                .Select(i => new Invoice(i.FindControl("hidInvoiceId").ToCustomHiddenField().Value.ToLong()))
                .ToList();

            if (!invoices.Any())
            {
                DisplayMessages(new[]{ValidationMessage.Error("No invoices have been selected to be paid for.")});
                return;
            }

            if (invoices.Select(i => i.CustomerLocation.Customer.CustomerNumber).Distinct().Count() > 1)
            {
                DisplayMessages(new []{ValidationMessage.Error("Only one customer's invoices may be paid for in one transaction. Please select one or more invoices from the same customer account.")});
                return;
            }

            Session[WebApplicationConstants.TransferListOfInvoicesObject] = invoices;
            Response.Redirect(InvoicePayView.PageAddress);
        }
    }
}
