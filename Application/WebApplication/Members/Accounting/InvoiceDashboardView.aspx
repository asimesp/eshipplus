﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="InvoiceDashboardView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.InvoiceDashboardView" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowNew="false" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo"  AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Invoices Dashboard
                <small class="ml10">
                    <asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>
        <span class="clearfix"></span>
        <hr class="dark mb10" />
        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="Invoices" OnProcessingError="OnSearchProfilesProcessingError"
                            OnSearchProfileSelected="OnSearchProfilesProfileSelected" />
                    </div>
                </div>
            </div>
            <hr class="dark mb10" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                        OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>
            <hr class="fat" />
            <div class="rowgroup">
                <div class="row mb10">
                    <div class="left">
                        <asp:Button ID="btnSearch" CssClass="left" runat="server" Text="SEARCH" OnClick="OnSearchClicked"
                            CausesValidation="False" />
                    </div>
                    <div class="right">
                        <div class="fieldgroup">
                            <label>Sort By:</label>
                            <asp:DropDownList runat="server" ID="ddlSortBy" DataTextField="DisplayName" DataValueField="Name"
                                OnSelectedIndexChanged="OnSortBySelectedIndexChanged" AutoPostBack="True" />
                        </div>
                        <div class="fieldgroup">
                            <asp:RadioButton runat="server" ID="rbAsc" Text="ASC" GroupName="rbSortBy" AutoPostBack="True"
                                ToolTip="Ascending" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                        </div>
                        <div class="fieldgroup">
                            <asp:RadioButton runat="server" ID="rbDesc" Text="DESC" GroupName="rbSortBy" Checked="True"
                                ToolTip="Descending" AutoPostBack="True" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:UpdatePanel runat="server" ID="upDataUpdate">
            <ContentTemplate>
                <eShip:ContinuousScrollExtender ID="upcseDataUpdate" runat="server" BindableControlId="lstInvoiceDetails" UpdatePanelToExtendId="upDataUpdate" />
                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheInvoiceDashboardTable" TableId="invoiceDashboardTable" HeaderZIndex="2" />
                <div class="rowgroup">
                    <table id="invoiceDashboardTable" class="line2 pl2">
                        <tr>
                            <th style="width: 10%;">
                                <asp:LinkButton runat="server" ID="lbtnSortInvoiceNumber" CssClass="link_nounderline blue"
                                    CausesValidation="False" OnCommand="OnSortData">
                            Invoice Number
                                </asp:LinkButton>
                            </th>
                            <th style="width: 10%;">
                                <asp:LinkButton runat="server" ID="lbtnSortInvoiceDate" CssClass="link_nounderline blue"
                                    CausesValidation="False" OnCommand="OnSortData">
                            Invoice Date
                                </asp:LinkButton>
                            </th>
                            <th style="width: 10%;">
                                <asp:LinkButton runat="server" ID="lbtnSortPostDate" CssClass="link_nounderline blue"
                                    CausesValidation="False" OnCommand="OnSortData">
                            Posted Date
                                </asp:LinkButton>
                            </th>
                            <th style="width: 10%;">
                                <asp:LinkButton runat="server" ID="lbtnSortDueDate" CssClass="link_nounderline blue" CausesValidation="False"
                                    OnCommand="OnSortData">
                            Due Date
                                </asp:LinkButton>
                            </th>
                            <th style="width: 14%;">
                                <asp:LinkButton runat="server" ID="lbtnSortAmountDue" CssClass="link_nounderline blue"
                                    CausesValidation="False" OnCommand="OnSortData">
                            Amount Due /Credit
                                </asp:LinkButton>
                            </th>
                            <th style="width: 13%;">
                                <asp:LinkButton runat="server" ID="lbtnSortPaidAmount" CssClass="link_nounderline blue"
                                    CausesValidation="False" OnCommand="OnSortData">
                            Paid Amount 
                                </asp:LinkButton>
                            </th>
                            <th style="width: 17%;">
                                <asp:LinkButton runat="server" ID="lbtnSortUsername" CssClass="link_nounderline blue"
                                    CausesValidation="False" OnCommand="OnSortData">
                            User
                                </asp:LinkButton>
                            </th>
                            <th style="width: 9%;">
                                <asp:LinkButton runat="server" ID="lbtnSortTypeText" CssClass="link_nounderline blue"
                                    CausesValidation="False" OnCommand="OnSortData">
                            Type
                                </asp:LinkButton>
                            </th>
                            <th style="width: 7%;" class="text-center">Action</th>
                        </tr>
                        <asp:ListView runat="server" ID="lstInvoiceDetails" ItemPlaceholderID="itemPlaceHolder"
                            OnItemDataBound="OnInvoiceDetailsItemDataBound">
                            <LayoutTemplate>
                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <eShip:InvoiceDashboardDetailControl ID="invoiceDashboardDetail2" ItemIndex='<%# Container.DataItemIndex %>'
                                    runat="server" OnGenerateInvoice="OnGenerateInvoiceRequest" />
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lbtnSortInvoiceNumber" />
                <asp:PostBackTrigger ControlID="lbtnSortInvoiceDate" />
                <asp:PostBackTrigger ControlID="lbtnSortPostDate" />
                <asp:PostBackTrigger ControlID="lbtnSortDueDate" />
                <asp:PostBackTrigger ControlID="lbtnSortAmountDue" />
                <asp:PostBackTrigger ControlID="lbtnSortPaidAmount" />
                <asp:PostBackTrigger ControlID="lbtnSortUsername" />
                <asp:PostBackTrigger ControlID="lbtnSortTypeText" />
                <asp:PostBackTrigger ControlID="lstInvoiceDetails" />
            </Triggers>
        </asp:UpdatePanel>
        <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
        <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
        <eShip:DocumentDisplayControl runat="server" ID="documentDisplay" Visible="false" OnClose="OnDocumentDisplayClosed" />
        <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
        <eShip:CustomHiddenField runat="server" ID="hidLastInvoiceId" />
    </div>
</asp:Content>
