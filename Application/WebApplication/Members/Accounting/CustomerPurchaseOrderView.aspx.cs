﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting
{
    public partial class CustomerPurchaseOrderView : MemberPageBase, ICustomerPurchaseOrderView
    {
        private const string ExportFlag = "E";

        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public override ViewCode PageCode { get { return ViewCode.CustomerPurchaseOrder; } }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.FinanceBlue; }
        }

        public static string PageAddress { get { return "~/Members/Accounting/CustomerPurchaseOrderView.aspx"; } }

        public event EventHandler<ViewEventArgs<string>> CustomerSearch;
        public event EventHandler<ViewEventArgs<CustomerPurchaseOrder>> Save;
        public event EventHandler<ViewEventArgs<CustomerPurchaseOrder>> Delete;
        public event EventHandler<ViewEventArgs<CustomerPurchaseOrder>> Lock;
        public event EventHandler<ViewEventArgs<CustomerPurchaseOrder>> UnLock;
        public event EventHandler<ViewEventArgs<PurchaseOrderViewSearchCriteria>> Search;
        public event EventHandler<ViewEventArgs<List<CustomerPurchaseOrder>>> BatchImport;

        public void DisplaySearchResult(List<CustomerPurchaseOrder> customerPurchaseOrders)
        {
            litRecordCount.Text = customerPurchaseOrders.BuildRecordCount();
            lvwRegistry.DataSource = customerPurchaseOrders;
            lvwRegistry.DataBind();

            if (hidFlag.Value == ExportFlag)
            {
                var lines = customerPurchaseOrders
                    .Select(a => new[]
					             	{
					             		a.PurchaseOrderNumber.ToString(),
					             		a.ValidPostalCode,
					             		a.ApplyOnOrigin.ToString(),
					             		a.ApplyOnDestination.ToString(),
					             		a.MaximumUses.ToString(),
					             		a.CurrentUses.ToString(),
					             		a.ExpirationDate.FormattedShortDate(),
					             		a.AdditionalPurchaseOrderNotes,
                                        a.Country.Code
					             	}.TabJoin())
                    .ToList();
                lines.Insert(0,
                             new[]
				             	{
				             		"PO Number", "Postal Code", "Apply On Origin", "Apply On Destination", "Max. Uses",
				             		"Current Uses", "Expiration Date", "Additional Notes","CountryCode"
				             	}.TabJoin());

                hidFlag.Value = string.Empty;

                Response.Export(lines.ToArray().NewLineJoin(), "CustomerPurchaseOrders.txt");
            }
        }

        public void DisplayCustomer(Customer customer)
        {
            txtCustomerNumber.Text = customer == null ? string.Empty : customer.CustomerNumber;
            txtCustomerName.Text = customer == null ? string.Empty : customer.Name;
            hidCustomerId.Value = customer == null ? default(long).ToString() : customer.Id.ToString();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() || messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Button = MessageButton.Ok;
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void FailedLock(Lock @lock)
        {
            btnSave.Enabled = false;
            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems { Columns = GetCurrentRunParameters(true) };
        }

        private void DoSearchPreProcessingThenSearch()
        {
            DoSearch(new PurchaseOrderViewSearchCriteria
            {
                Parameters = GetCurrentRunParameters(false),
                CustomerId = hidCustomerId.Value.ToLong()
            });
        }

        private void DoSearch(PurchaseOrderViewSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<PurchaseOrderViewSearchCriteria>(criteria));
        }


        private void GeneratePopup(string title)
        {
            litTitle.Text = title;

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = pnlEdit.Visible;
        }

        private void CloseEditPopup()
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<CustomerPurchaseOrder>(new CustomerPurchaseOrder(hidCustomerPurchaseOrderId.Value.ToLong(), false)));
        }

        private void LoadCustomerPurchaseOrderForEdit(CustomerPurchaseOrder customerPurchaseOrder)
        {
            hidCustomerPurchaseOrderId.Value = customerPurchaseOrder.Id.ToString();

            txtPoNumber.Text = customerPurchaseOrder.PurchaseOrderNumber;
            txtPostalCode.Text = customerPurchaseOrder.ValidPostalCode;
            chkApplyOnOrigin.Checked = customerPurchaseOrder.ApplyOnOrigin;
            chkApplyOnDestination.Checked = customerPurchaseOrder.ApplyOnDestination;
            txtMaxUses.Text = customerPurchaseOrder.MaximumUses.ToString();
            txtCurrentUses.Text = customerPurchaseOrder.CurrentUses.ToString();
            txtExpirationDate.Text = customerPurchaseOrder.ExpirationDate.FormattedShortDate();
            txtNotes.Text = customerPurchaseOrder.AdditionalPurchaseOrderNotes;
			ddlCountries.SelectedValue = customerPurchaseOrder.CountryId.GetString();
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            new CustomerPurchaseOrderHandler(this).Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowImport = Access.Modify;
            memberToolBar.ShowNew = Access.Modify;

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            aceCustomer.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.ActiveUser, ActiveUser.Id.GetString() } });

            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.CustomerPurchaseOrderImportTemplate });

            if (!ActiveUser.TenantEmployee && !ActiveUser.UserShipAs.Any()) DisplayCustomer(ActiveUser.DefaultCustomer);

            var profile = SearchDefaults == null ? null : SearchDefaults.Default(SearchDefaultsItemType.CustomerPurchaseOrders);

            var columns = profile != null && profile.Columns != null
                            ? profile.Columns
                            : AccountingSearchFields.DefaultCustomerPurchaseOrders.Select(f => f.ToParameterColumn()).ToList();

            // Set expiration date per original page logic, if there is a value for the expiration date column per search defaults, leave it
            var eDates = columns
                .Where(i => i.ReportColumnName == AccountingSearchFields.ExpirationDate.ToParameterColumn().ReportColumnName)
                .Where(i => string.IsNullOrEmpty(i.DefaultValue))
                .ToList();
            foreach (var name in eDates)
                name.DefaultValue = DateTime.Now.AddYears(-1).ToString();

            lstFilterParameters.DataSource = columns;
            lstFilterParameters.DataBind();

            if (Session[WebApplicationConstants.TransferCustomerId] != null)
            {
                DisplayCustomer(new Customer(Session[WebApplicationConstants.TransferCustomerId].ToLong()));
                Session[WebApplicationConstants.TransferCustomerId] = null;
            }
        }


        protected void OnSaveClick(object sender, EventArgs e)
        {
            var customerPurchaseOrderId = hidCustomerPurchaseOrderId.Value.ToLong();

            var customerPurchaseOrder = new CustomerPurchaseOrder(customerPurchaseOrderId, customerPurchaseOrderId != default(long))
            {
                Customer = new Customer(hidCustomerId.Value.ToLong()),
                PurchaseOrderNumber = txtPoNumber.Text,
                ValidPostalCode = txtPostalCode.Text,
                ApplyOnOrigin = chkApplyOnOrigin.Checked,
                ApplyOnDestination = chkApplyOnDestination.Checked,
                MaximumUses = txtMaxUses.Text.ToInt(),
                CurrentUses = txtCurrentUses.Text.ToInt(),
                ExpirationDate = txtExpirationDate.Text.ToDateTime(),
                AdditionalPurchaseOrderNotes = txtNotes.Text,
                CountryId = ddlCountries.SelectedValue.ToLong()
            };

            if (customerPurchaseOrder.IsNew) customerPurchaseOrder.TenantId = ActiveUser.TenantId;

            if (Save != null)
                Save(this, new ViewEventArgs<CustomerPurchaseOrder>(customerPurchaseOrder));

            DoSearchPreProcessingThenSearch();
        }

        protected void OnNewClick(object sender, EventArgs e)
        {
            if (hidCustomerId.Value.ToLong() == default(long))
            {
                DisplayMessages(new[] { ValidationMessage.Error("Please select a customer before adding a new customer purchase order") });
                return;
            }

            GeneratePopup("Add Customer Purchase Order");
            LoadCustomerPurchaseOrderForEdit(new CustomerPurchaseOrder { ExpirationDate = DateTime.Now, CountryId = ActiveUser.Tenant.DefaultCountryId });
        }

        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            GeneratePopup("Modify Customer Purchase Order");

            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("customerPurchaseOrderId").ToCustomHiddenField();

            var customerPurchaseOrder = new CustomerPurchaseOrder(hidden.Value.ToLong(), false);

            LoadCustomerPurchaseOrderForEdit(customerPurchaseOrder);

            if (Lock != null)
                Lock(this, new ViewEventArgs<CustomerPurchaseOrder>(customerPurchaseOrder));
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("customerPurchaseOrderId").ToCustomHiddenField();

            if (Delete != null)
                Delete(this, new ViewEventArgs<CustomerPurchaseOrder>(new CustomerPurchaseOrder(hidden.Value.ToLong(), true)));

            DoSearchPreProcessingThenSearch();
        }

        protected void OnCloseClicked(object sender, object e)
        {
            CloseEditPopup();
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch();
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            hidFlag.Value = ExportFlag;
            DoSearchPreProcessingThenSearch();
        }


        protected void OnCustomerNumberEntered(object sender, EventArgs eventArgs)
        {
            if (string.IsNullOrEmpty(txtCustomerNumber.Text))
            {
                hidCustomerId.Value = string.Empty;
                txtCustomerName.Text = string.Empty;
                return;
            }

            DisplaySearchResult(new List<CustomerPurchaseOrder>());

            if (CustomerSearch != null)
                CustomerSearch(this, new ViewEventArgs<string>(txtCustomerNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));

        }

        protected void OnCustomerSearchClicked(object sender, ImageClickEventArgs imageClickEventArgs)
        {
            customerFinder.Visible = true;
        }

        protected void OnCustomerFinderSelectionCancelled(object sender, EventArgs e)
        {
            customerFinder.Visible = false;
        }

        protected void OnCustomerFinderItemSelected(object sender, ViewEventArgs<Customer> e)
        {
            DisplayCustomer(e.Argument);

            DisplaySearchResult(new List<CustomerPurchaseOrder>());

            customerFinder.Visible = false;
        }


        protected void OnImportClicked(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(hidCustomerId.Value))
            {
                fileUploader.Title = "CUSTOMER PURCHASE ORDER IMPORT";
                fileUploader.Instructions = @"**Please refer to user guide for import file format.";
                fileUploader.Visible = true;
            }
            else
            {
                messageBox.Button = MessageButton.Ok;
                messageBox.Icon = MessageIcon.Error;
                messageBox.Message = "Please Select a Customer to Batch Import for";

                messageBox.Visible = true;
            }
        }

        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 9;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var countries = ProcessorVars.RegistryCache.Countries.Values;
            var countryCodes = countries.Select(c => c.Code).ToList();
            msgs.AddRange(chks.CodesAreValid(8, countryCodes, new Country().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var postalCodeSearch = new PostalCodeSearch();
            var postalCodes = new List<PostalCodeViewSearchDto>();
            var invalidPostalCodes = chks
                .Where(i =>
                {
                    var code = postalCodeSearch.FetchPostalCodeByCode(i.Line[1], countries.First(c => c.Code == i.Line[8]).Id);
                    postalCodes.Add(code);
                    return code.Id == default(long);
                })
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedCodeErrMsg, new PostalCode().EntityName(), i.Index))
                .ToList();
            if (invalidPostalCodes.Any())
            {
                invalidPostalCodes.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(invalidPostalCodes);
                return;
            }

            var customer = new Customer(hidCustomerId.Value.ToLong());
            var customerPurchaseOrders = lines
                .Select(s => new CustomerPurchaseOrder
                {
                    TenantId = ActiveUser.TenantId,
                    Customer = customer,
                    PurchaseOrderNumber = s[0],
                    ValidPostalCode = postalCodes.First(pc => pc.Code.ToLower() == s[1].ToLower()).Code,
                    ApplyOnOrigin = s[2].ToBoolean(),
                    ApplyOnDestination = !s[2].ToBoolean() && s[3].ToBoolean(),
                    MaximumUses = s[4].ToInt(),
                    CurrentUses = s[5].ToInt(),
                    ExpirationDate = s[6].ToDateTime(),
                    AdditionalPurchaseOrderNotes = s[7],
                    CountryId = countries.First(c => c.Code == s[8]).Id
                })
                .ToList();

            if (BatchImport != null)
                BatchImport(this, new ViewEventArgs<List<CustomerPurchaseOrder>>(customerPurchaseOrders));
            fileUploader.Visible = false;
            DoSearchPreProcessingThenSearch();
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }


        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (CloseEditPopUp) CloseEditPopup();
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = AccountingSearchFields.CustomerPurchaseOrders.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(AccountingSearchFields.CustomerPurchaseOrders);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;

        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();
        }
        
    }
}
