﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="InvoicePaidAmountUpdateView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.InvoicePaidAmountUpdateView" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="false"
        ShowEdit="false" ShowDelete="false" ShowSave="false" ShowFind="false" ShowImport="true"
        ShowExport="false" OnImport="OnImportInvoices" OnSave="OnSaveInvoices" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Invoice Paid Amount Updates
            </h3>
        </div>
        <hr class="dark mb10" />
        <div>
            <ul>
                <li class="note">Amount will be added to existing amount paid.</li>
            </ul>
        </div>
        <hr class="fat" />
        <div class="rowgroup">
            <eShip:TableFreezeHeaderExtender runat="server" ID="tfheInvoicePaidAmountUpdatesTable" TableId="invoicePaidAmountUpdatesTable" HeaderZIndex="2"/>
            <asp:ListView ID="lstInvoices" runat="server" ItemPlaceholderID="itemPlaceHolder">
                <LayoutTemplate>
                    <h5>Invoices to Update</h5>
                    <table class="line2 pl2" id="invoicePaidAmountUpdatesTable">
                        <tr>
                            <th style="width: 15%;" class="text-right">Invoice Number
                            </th>
                            <th style="width: 15%;" class="text-right">Amount Paid ($)
                            </th>
                            <th style="width: 70%;">&nbsp;
                            </th>
                        </tr>
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr>
                        <td class="text-right">
                            <eShip:CustomHiddenField runat="server" ID="hidInvoiceId" Value='<%# Eval("Id") %>' />
                            <asp:Literal runat="server" ID="litInvoiceNumber" Text='<%# Eval("InvoiceNumber") %>' />
                        </td>
                        <td>
                            <eShip:CustomTextBox runat="server" ID="txtAmountPaid" Text='<%# Eval("PaidAmount","{0:f2}") %>' />
                        </td>
                        <td>&nbsp;
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </div>
        <div class="rowgroup">
            <eShip:TableFreezeHeaderExtender runat="server" ID="tfheInvoicePaidAmountUpdatesCompletedTable" TableId="invoicePaidAmountUpdatesCompletedTable" HeaderZIndex="2"/>
            <asp:ListView ID="lstCompleteInvoices" runat="server" ItemPlaceholderID="itemPlaceHolder">
					<LayoutTemplate>
					    <h5>Invoice Update Results</h5>
						<table class="line2 pl2" id="invoicePaidAmountUpdatesCompletedTable">
							<tr>
								<th style="width: 15%;" class="text-right">
									Invoice Number
								</th>
								<th style="width: 30%;" class="text-right">
									Customer
								</th>
								<th style="width: 15%;" class="text-right">
									Amount Due ($)
								</th>
								<th style="width: 15%;" class="text-right">
									Amount Paid ($)
								</th>
								<th style="width: 25%;">
									Message
								</th>
							</tr>
							<asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
						</table>
					</LayoutTemplate>
					<ItemTemplate>
						<tr>
							<td class="text-right">
								<%# Eval("InvoiceNumber") %>
							</td>
							<td>
								<%# Eval("Customer") %>
							</td>
							<td class="text-right">
								<%# Eval("AmountDue", "{0:f4}") %>
							</td>
							<td class="text-right">
								<%# Eval("AmountPaid", "{0:f4}")%>
							</td>
							<td class='<%# Eval("IsError").ToBoolean() ? "red": string.Empty %>'>
								<%# Eval("MessageText") %>
							</td>
						</tr>
					</ItemTemplate>
				</asp:ListView>
        </div>
    </div>
    <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
		OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
	<eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information"/>
</asp:Content>
