﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.PaymentGateway;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting
{
    public partial class MiscReceiptDashboardView : MemberPageBase, IMiscReceiptDashboardView
    {
        private const string ExportFlag = "ExportFlag";
        private const string RefundFlag = "RF";

        private const string PayForShipmentWithCreditCardArgs = "PFSWCC";
        private const string CreateCheckMiscReceiptForShipmentArgs = "CCMRFS";


        private SearchField SortByField
        {
            get { return AccountingSearchFields.MiscReceiptSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue); }
        }


        public override ViewCode PageCode { get { return ViewCode.MiscReceiptDashboard; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.FinanceBlue; } }

        public static string PageAddress { get { return "~/Members/Accounting/MiscReceiptDashboardView.aspx"; } }

        public event EventHandler<ViewEventArgs<MiscReciptViewSearchCriteria>> Search;
        public event EventHandler<ViewEventArgs<MiscReceipt, decimal>> RefundMiscReceipt;
        public event EventHandler<ViewEventArgs<MiscReceipt>> VoidMiscReceipt;


        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplaySearchResult(List<MiscReceiptViewSearchDto> miscReceipts)
        {
            switch (hidFlag.Value)
            {
                case ExportFlag:
                    DoExport(miscReceipts);
                    break;
                default:
                    litRecordCount.Text = miscReceipts.BuildRecordCount();
                    upcseDataUpdate.DataSource = miscReceipts;
                    upcseDataUpdate.DataBind();
                    break;
            }
            hidFlag.Value = string.Empty;
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == RefundFlag)
            {
                pnlDimScreen.Visible = false;
                pnlMiscReceiptRefund.Visible = false;
                DoSearchPreProcessingThenSearch(SortByField);
            }

            hidFlag.Value = string.Empty;
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        private void DoExport(IEnumerable<MiscReceiptViewSearchDto> miscReceipts)
        {
            var q = miscReceipts.Select(a => a.ToString()).ToList();
            q.Insert(0, MiscReceiptViewSearchDto.Header());
            Response.Export(q.ToArray().NewLineJoin());
        }


        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems
            {
                Columns = GetCurrentRunParameters(true),
                SortAscending = rbAsc.Checked,
                SortBy = SortByField,
            };
        }

        private void DoSearchPreProcessingThenSearch(SearchField field)
        {
            DoSearch(new MiscReciptViewSearchCriteria
            {
                Parameters = GetCurrentRunParameters(false),
                SortBy = field,
                SortAscending = rbAsc.Checked
            });
        }

        private void DoSearch(MiscReciptViewSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<MiscReciptViewSearchCriteria>(criteria));
        }


        private List<ToolbarMoreAction> ToolbarMoreActions()
        {
            var payForShipmentWithCreditCard = new ToolbarMoreAction
            {
                CommandArgs = PayForShipmentWithCreditCardArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Finance,
                Name = "Pay For Shipment Using Credit Card",
                IsLink = false,
                NavigationUrl = string.Empty
            };

            var createCheckMiscReceipt = new ToolbarMoreAction
                {
                    CommandArgs = CreateCheckMiscReceiptForShipmentArgs,
                    ConfirmCommand = false,
                    ImageUrl = IconLinks.Finance,
                    Name = "Create Check Misc Receipt For Shipment",
                    IsLink = false,
                    NavigationUrl = string.Empty
                };

            var actions = new List<ToolbarMoreAction>();
            if (Access.Modify)
            {
                actions.Add(payForShipmentWithCreditCard);
                actions.Add(createCheckMiscReceipt);
            }
            memberToolBar.ShowMore = actions.Any();
            return actions;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new MiscReceiptDashboardHandler(this).Initialize();

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            memberToolBar.LoadAdditionalActions(ToolbarMoreActions());

            if (IsPostBack) return;

            // set auto referesh
            tmRefresh.Interval = WebApplicationConstants.DefaultRefreshInterval.ToInt();

            // set sort fields
            ddlSortBy.DataSource = AccountingSearchFields.MiscReceiptSortFields.OrderBy(s => s.DisplayName);
            ddlSortBy.DataBind();
            ddlSortBy.SelectedValue = AccountingSearchFields.ShipmentNumber.Name;

            lbtnSortShipmentNumber.CommandName = AccountingSearchFields.ShipmentNumber.Name;
            lbtnSortLoadOrderNumber.CommandName = AccountingSearchFields.LoadOrderNumber.Name;
            lbtnSortAmountPaid.CommandName = AccountingSearchFields.AmountPaid.Name;
            lbtnSortPaymentDate.CommandName = AccountingSearchFields.PaymentDate.Name;
            lbtnSortGatewayTransactionId.CommandName = AccountingSearchFields.GatewayTransactionId.Name;
            lbtnSortPaymentGatewayType.CommandName = AccountingSearchFields.PaymentGatewayType.Name;
            lbtnSortReversal.CommandName = AccountingSearchFields.Reversal.Name;

            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);

            var criteria = new MiscReciptViewSearchCriteria
            {
                Parameters = profile != null
                                 ? profile.Columns
                                 : AccountingSearchFields.DefaultMiscReceipts.Select(f => f.ToParameterColumn()).ToList(),
                SortAscending = profile == null || profile.SortAscending,
                SortBy = profile == null ? null : profile.SortBy
            };

            lstFilterParameters.DataSource = criteria.Parameters;
            lstFilterParameters.DataBind();

            rbAsc.Checked = criteria.SortAscending;
            rbDesc.Checked = !criteria.SortAscending;

            if (criteria.SortBy != null && ddlSortBy.ContainsValue(criteria.SortBy.Name))
                ddlSortBy.SelectedValue = criteria.SortBy.Name;
        }


        protected void OnCommand(object sender, ViewEventArgs<string> e)
        {
            switch (e.Argument)
            {
                case PayForShipmentWithCreditCardArgs:
                    pecPayment.PaymentGatewayType = new Tenant(ActiveUser.TenantId).PaymentGatewayType;
                    shipmentFinder.Visible = true;
                    break;
                case CreateCheckMiscReceiptForShipmentArgs:
                    pecPayment.PaymentGatewayType = PaymentGatewayType.Check;
                    shipmentFinder.Visible = true;
                    break;
            }
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            upcseDataUpdate.ResetLoadCount();
            hidFlag.Value = string.Empty;
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            hidFlag.Value = ExportFlag;
            DoSearchPreProcessingThenSearch(SortByField);
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }


        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = AccountingSearchFields.MiscReceipts.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

        }

        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(AccountingSearchFields.MiscReceipts);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;

        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSortBySelectedIndexChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnSortOrderCheckChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnSortData(object sender, CommandEventArgs e)
        {
            var field = AccountingSearchFields.MiscReceiptSortFields.FirstOrDefault(c => c.Name == e.CommandName);
            if (field == null) return;
            if (ddlSortBy.ContainsValue(field.Name))
            {
                if (ddlSortBy.SelectedValue == field.Name && rbAsc.Checked)
                {
                    rbAsc.Checked = false;
                    rbDesc.Checked = true;
                }
                else if (ddlSortBy.SelectedValue == field.Name && rbDesc.Checked)
                {
                    rbAsc.Checked = true;
                    rbDesc.Checked = false;
                }

                ddlSortBy.SelectedValue = field.Name;
            }
            DoSearchPreProcessingThenSearch(field);
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();

            rbAsc.Checked = e.Argument.SortAscending;
            rbDesc.Checked = !e.Argument.SortAscending;

            if (e.Argument.SortBy != null && ddlSortBy.ContainsValue(e.Argument.SortBy.Name))
                ddlSortBy.SelectedValue = e.Argument.SortBy.Name;
        }

        protected void OnSearchProfilesAutoRefreshChanged(object sender, ViewEventArgs<int> e)
        {
            tmRefresh.Enabled = e.Argument > default(int);
            if (e.Argument > 0) tmRefresh.Interval = e.Argument;
        }


        protected void OnTimerTick(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }


        protected void OnMiscReceiptsItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var item = e.Item;

            if (item == null) return;

            var miscReceipt = (MiscReceiptViewSearchDto)item.DataItem;

            var rptMiscReceiptApplications = item.FindControl("rptMiscReceiptApplications").ToRepeater();
            rptMiscReceiptApplications.DataSource = miscReceipt.MiscReceiptApplications;
            rptMiscReceiptApplications.DataBind();
        }


        protected void OnPaymentEntryCloseClicked(object sender, EventArgs e)
        {
            pecPayment.Visible = false;
        }

        protected void OnPaymentEntryProcessingMessages(object sender, ViewEventArgs<List<ValidationMessage>> e)
        {
            DisplayMessages(e.Argument);
        }

        protected void OnPaymentEntryPaymentSuccessfullyProcessed(object sender, EventArgs e)
        {
            pecPayment.Clear();
            pecPayment.Visible = false;
            DoSearchPreProcessingThenSearch(SortByField);
        }


        protected void OnShipmentFinderItemSelected(object sender, ViewEventArgs<Shipment> e)
        {
            pecPayment.Visible = true;
            shipmentFinder.Visible = false;
            pecPayment.LoadShipmentToPayFor(e.Argument);
        }

        protected void OnShipmentFinderItemCancelled(object sender, EventArgs e)
        {
            shipmentFinder.Visible = false;
        }


        protected void OnVoidOrCreateRefundForReceipt(object sender, ImageClickEventArgs e)
        {
            var control = (Control)sender;
            hidReceiptToBeRefundedOrVoidedId.Value = control.FindControl("hidMiscReceiptId").ToCustomHiddenField().Value;

            var miscReceipt = new MiscReceipt(hidReceiptToBeRefundedOrVoidedId.Value.ToLong());
            if (miscReceipt.PaymentGatewayType == PaymentGatewayType.NotApplicable || miscReceipt.PaymentGatewayType == PaymentGatewayType.Check || miscReceipt.PaymentGatewayType != miscReceipt.Tenant.PaymentGatewayType)
            {
                var maxRefundableAmount = miscReceipt.AmountLeftToBeAppliedOrRefunded(miscReceipt.TenantId);
                if (maxRefundableAmount == 0m)
                {
                    DisplayMessages(new[] { ValidationMessage.Error("Receipt has already been fully refunded") });
                    return;
                }

                txtRefundAmount.Text = maxRefundableAmount.ToString("n4");
                rvRefundAmount.MaximumValue = maxRefundableAmount.ToString();
                pnlMiscReceiptRefund.Visible = true;
                pnlDimScreen.Visible = true;
            }
            else
            {
                try
                {
                    switch (miscReceipt.GetTransactionStatusViaPaymentGateway())
                    {
                        case TransactionStatus.CommunicationError:
                        case TransactionStatus.ApprovedReview:
                        case TransactionStatus.GeneralError:
                        case TransactionStatus.FailedReview:
                        case TransactionStatus.SettlementError:
                        case TransactionStatus.UnderReview:
                        case TransactionStatus.FdsPendingReview:
                        case TransactionStatus.FdsAuthorizedPendingReview:
                        case TransactionStatus.ReturnedItem:
                        case TransactionStatus.NotApplicable:
                            DisplayMessages(new[] { ValidationMessage.Error("Misc. Receipt is neither refundable or able to be voided.") });
                            return;
                        case TransactionStatus.Declined:
                            DisplayMessages(new[] { ValidationMessage.Error("Misc. Receipt has been declined and cannot be refunded or voided.") });
                            return;
                        case TransactionStatus.Expired:
                            DisplayMessages(new[] { ValidationMessage.Error("Misc. Receipt is expired and cannot be refunded or voided.") });
                            return;
                        case TransactionStatus.Voided:
                            DisplayMessages(new[] { ValidationMessage.Error("Misc. Receipt has already been voided.") });
                            return;
                        case TransactionStatus.CouldNotVoid:
                            DisplayMessages(new[] { ValidationMessage.Error("Misc. Receipt could not be voided.") });
                            return;
                        case TransactionStatus.RefundSettledSuccessfully:
                            DisplayMessages(new[] { ValidationMessage.Error("Misc. Receipt has already been settled successfully.") });
                            return;
                        case TransactionStatus.RefundPendingSettlement:
                            DisplayMessages(new[] { ValidationMessage.Error("Misc. Receipt is pending settlement for refund.") });
                            return;
                        case TransactionStatus.AuthorizedPendingCapture:
                        case TransactionStatus.CapturedPendingSettlement:
                            if (miscReceipt.AmountLeftToBeAppliedOrRefunded(miscReceipt.TenantId) < miscReceipt.AmountPaid)
                            {
                                DisplayMessages(new[] { ValidationMessage.Error("Misc. Receipt is not yet settled and has been applied to an invoice therefore a refund or void may not be issued at this time.") });
                                return;
                            }

                            hidReceiptToBeRefundedOrVoidedId.Value = miscReceipt.Id.ToString();
                            messageBox.Button = MessageButton.YesNo;
                            messageBox.Icon = MessageIcon.Information;
                            messageBox.Message = "This Misc. Receipt may be voided. Would you like to void this receipt?";
                            messageBox.Visible = true;
                            return;
                        case TransactionStatus.SettledSuccessfully:
                            var maxRefundableAmount = miscReceipt.AmountLeftToBeAppliedOrRefunded(miscReceipt.TenantId);
                            if (maxRefundableAmount == 0m)
                            {
                                DisplayMessages(new[] { ValidationMessage.Error("Receipt has already been fully refunded.") });
                                return;
                            }

                            txtRefundAmount.Text = maxRefundableAmount.ToString("n4");
                            rvRefundAmount.MaximumValue = maxRefundableAmount.ToString();
                            pnlMiscReceiptRefund.Visible = true;
                            pnlDimScreen.Visible = true;
                            return;
                    }
                }
                catch (Exception ex)
                {
                    DisplayMessages(new[] { ValidationMessage.Error("Cannot refund or void receipt due to payment gateway error: {0}", ex.Message) });
                }
            }
        }

        protected void OnCloseRefundClicked(object sender, EventArgs e)
        {
            pnlMiscReceiptRefund.Visible = false;
            pnlDimScreen.Visible = false;
            hidReceiptToBeRefundedOrVoidedId.Value = string.Empty;
        }

        protected void OnRefundMiscReceiptClicked(object sender, EventArgs e)
        {
            var miscReceipt = new MiscReceipt(hidReceiptToBeRefundedOrVoidedId.Value.ToLong());

            hidFlag.Value = RefundFlag;

            if (RefundMiscReceipt != null)
                RefundMiscReceipt(this, new ViewEventArgs<MiscReceipt, decimal>(miscReceipt, txtRefundAmount.Text.ToDecimal()));

            DoSearchPreProcessingThenSearch(SortByField);
            hidReceiptToBeRefundedOrVoidedId.Value = string.Empty;
        }


        protected void OnMessageBoxYes(object sender, EventArgs e)
        {
            if (VoidMiscReceipt != null)
                VoidMiscReceipt(this, new ViewEventArgs<MiscReceipt>(new MiscReceipt(hidReceiptToBeRefundedOrVoidedId.Value.ToLong())));
            hidReceiptToBeRefundedOrVoidedId.Value = string.Empty;
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnMessageBoxNo(object sender, EventArgs e)
        {
            messageBox.Visible = false;
            hidReceiptToBeRefundedOrVoidedId.Value = string.Empty;
        }
    }
}