﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="InvoiceView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.InvoiceView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>
<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        ShowEdit="true" ShowDelete="true" ShowSave="true" ShowFind="true" ShowMore="true"
        ShowImport="True" OnFind="OnToolbarFindClicked" OnSave="OnToolbarSaveClicked"
        OnDelete="OnToolbarDeleteClicked" OnImport="OnImportDetailsClicked" OnNew="OnToolbarNewClicked"
        OnEdit="OnToolbarEditClicked" OnCommand="OnToolbarCommand" OnUnlock="OnToolbarUnlockClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Invoices<eShip:RecordIdentityDisplayControl runat="server" ID="ridcInvoiceIdentity" TargetControlId="txtInvoiceNumber" />
            </h3>

            <div class="lockMessage">
                <asp:Literal ID="litMessage" runat="server" />
            </div>
        </div>

        <hr class="dark mb5" />

        <div class="errorMsgLit">
            <asp:Literal runat="server" ID="litErrorMessages" />
        </div>

        <eShip:InvoiceFinderControl runat="server" ID="invoiceFinder" Visible="false" EnableMultiSelection="false"
            OnItemSelected="OnInvoiceFinderItemSelected" OnSelectionCancel="OnInvoiceFinderItemCancelled"
            OpenForEditEnabled="true" />
        <eShip:ServiceTicketFinderControl runat="server" ID="serviceTicketFinder" Visible="false"
            EnableMultiSelection="true" FilterForServiceTicketsToBeInvoicedOnly="True" OnMultiItemSelected="OnServiceTicketFinderMultiItemSelected"
            OnSelectionCancel="OnServiceTicketFinderItemCancelled" OpenForEditEnabled="true" />
        <eShip:ShipmentFinderControl runat="server" ID="shipmentFinder" Visible="false" EnableMultiSelection="true"
            FilterForShipmentsToBeInvoicedOnly="True" OnMultiItemSelected="OnShipmentFinderMultiItemSelected"
            OnSelectionCancel="OnShipmentFinderItemCancelled" OpenForEditEnabled="true" />
        <eShip:PrefixFinderControl ID="prefixFinder" runat="server" EnableMultiSelection="false"
            Visible="False" OnSelectionCancel="OnPrefixFinderSelectionCancelled" OnItemSelected="OnPrefixFinderItemSelected" />
        <eShip:CustomerFinderControl ID="customerFinder" runat="server" EnableMultiSelection="false"
            Visible="False" OpenForEditEnabled="false" OnSelectionCancel="OnCustomerFinderSelectionCancelled"
            OnItemSelected="OnCustomerFinderItemSelected" />
        <eShip:CustomerControlAccountFinderControl ID="customerControlAccountFinder" runat="server"
            Visible="False" OnItemSelected="OnCustomerControlAccountFinderItemSelected" OnSelectionCancel="OnCustomerControlAccountFinderSelectionCancel" />
        <eShip:DocumentDisplayControl runat="server" ID="documentDisplay" Visible="false" OnClose="OnDocumentDisplayClosed" />
        <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
            OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
        <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information"
            OnOkay="OnOkayProcess" OnYes="OnYesClicked" OnNo="OnNoClicked" />
        <eShip:ApplyMiscReceiptsToInvoiceControl runat="server" Id="amrtiApplyMiscReceiptsToInvoice" Visible="False" OnClose="OnCloseApplyMiscReceiptToInvoiceClicked" 
            OnDisplayProcessingMessages="OnApplyMiscReceiptsToInvoiceDisplayProcessingMessages" />
        <eShip:AjaxTabHeaderTextUpdater runat="server" ID="athtuTabUpdater" />

        <eShip:CustomHiddenField ID="hidInvoiceId" runat="server" />

        <ajax:TabContainer ID="tabInvoices" runat="server" CssClass="ajaxCustom">
            <ajax:TabPanel runat="server" ID="tabDetails" HeaderText="Details">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlDetails">
                        <div class="rowgroup">
                            <div class="fieldgroup mr10">
                                <label class="wlabel blue">Invoice Number</label>
                                <eShip:CustomTextBox ID="txtInvoiceNumber" runat="server" ReadOnly="True" CssClass="w200 disabled" />
                            </div>
                            <div class="fieldgroup vlinedarkright mr30">
                                <label class="wlabel blue">Type</label>
                                <eShip:CustomTextBox ID="txtInvoiceType" runat="server" ReadOnly="True" CssClass="w150 disabled" />
                            </div>
                            <div class="fieldgroup vlinedarkright pl10 mr30">
                                <label class="wlabel blue">User</label>
                                <eShip:CustomTextBox runat="server" ID="txtUser" CssClass="w240 disabled" ReadOnly="True" />
                            </div>
                            <div class="fieldgroup pl10">
                                <label class="wlabel blue">Original Invoice Number</label>
                                <eShip:CustomHiddenField runat="server" ID="hidOriginalInvoiceId" />
                                <eShip:CustomTextBox ID="txtOriginalInvoiceNumber" runat="server" ReadOnly="True" CssClass="w240 disabled" />
                                <asp:ImageButton ID="ibtnOriginInvoiceFinder" runat="server" ImageUrl="~/images/icons2/search_dark.png" CausesValidation="False" OnClick="OnOriginalInvoiceSearchClicked" />
                            </div>
                        </div>
                        <hr class="dark" />
                        <div class="rowgroup">
                            <div class="col_1_2 bbox">
                                <h5>Billing Information</h5>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Customer
                                            <%= hidCustomerId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.Customer) 
                                                    ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Customer Record'><img src={3} width='20' class='middle'/></a>", 
                                                                            ResolveUrl(CustomerView.PageAddress),
                                                                            WebApplicationConstants.TransferNumber, 
                                                                            hidCustomerId.Value.UrlTextEncrypt(),
                                                                            ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                    : string.Empty %>
                                        </label>
                                        <eShip:CustomHiddenField runat="server" ID="hidCustomerId" />
                                        <eShip:CustomTextBox runat="server" ID="txtCustomerNumber" CssClass="w110" OnTextChanged="OnCustomerNumberEntered"
                                            AutoPostBack="True" />
                                        <asp:ImageButton ID="imgCustomerSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                                            CausesValidation="False" OnClick="OnCustomerSearchClicked" />
                                        <eShip:CustomTextBox runat="server" ID="txtCustomerName" CssClass="w280 disabled" ReadOnly="True" />
                                        <ajax:AutoCompleteExtender runat="server" ID="aceCustomer" TargetControlID="txtCustomerNumber" ServiceMethod="GetCustomerList" ServicePath="~/Services/eShipPlusWSv4P.asmx" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" DelimiterCharacters="" Enabled="True" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Customer Location</label>
                                        <script type="text/javascript">
                                            $(document).ready(function () {
                                                $(jsHelper.AddHashTag('<%= ddlCustomerLocation.ClientID %>')).change(function () {
                                                    $(jsHelper.AddHashTag(this.value)).show().siblings().hide();
                                                });
                                                $(jsHelper.AddHashTag('<%= ddlCustomerLocation.ClientID %>')).change();
                                            })
                                        </script>
                                        <asp:DropDownList runat="server" ID="ddlCustomerLocation" DataTextField="Text" DataValueField="Value" CssClass="w420" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Address</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <div>
                                            <eShip:CustomTextBox runat="server" TextMode="MultiLine" CssClass="w420 disabled" ReadOnly="True" />
                                        </div>
                                        <asp:Repeater runat="server" ID="rptCustomerLocations">
                                            <ItemTemplate>
                                                <div id='<%# Eval("Id") %>' style="display: none;">
                                                    <eShip:CustomTextBox runat="server" TextMode="MultiLine" CssClass="w420 disabled" ReadOnly="True"
                                                        Text='<%# string.Format("{0} {1} {2}{3}{4} {5} {6}{3}{7}", Eval("BillTo"), Eval("Street1"), Eval("Street2"), Environment.NewLine, 
                                                                Eval("City"), Eval("State"), Eval("PostalCode"), Eval("CountryName")) %>' />
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                                <div class="row pb20">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Customer Control Account</label>
                                        <eShip:CustomTextBox runat="server" ID="txtCustomerControlAccount" CssClass="w200 disabled" ReadOnly="True" />
                                        <asp:ImageButton ID="ibtnCustomerControlAccountSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                                            CausesValidation="False" OnClick="OnCustomerControlAccountSearchClicked" Visible="False" />
                                        <asp:ImageButton ID="ibtnClearCustomerControlAccount" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                            CausesValidation="False" OnClick="OnClearCustomerControlAccount" />
                                    </div>
                                </div>
                                <h5>Financial Information</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Invoice Total</label>
                                        <asp:UpdatePanel runat="server">
                                            <ContentTemplate>
                                                <eShip:CustomTextBox runat="server" ID="txtInvoiceTotal" CssClass="w200 disabled" ReadOnly="True" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Total Supplemental Adjustments</label>
                                        <eShip:CustomTextBox runat="server" ID="txtSupplementalAdjustments" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Total Credit Adjustments</label>
                                        <eShip:CustomTextBox runat="server" ID="txtCreditAdjustments" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Paid Amount ($)</label>
                                        <eShip:CustomTextBox runat="server" ID="txtPaidAmount" CssClass="w200" />
                                        <ajax:FilteredTextBoxExtender ID="ftePaidAmount" runat="server" TargetControlID="txtPaidAmount"
                                            FilterType="Custom, Numbers" ValidChars="." Enabled="True" />
                                    </div>
                                </div>
                            </div>
                            <div class="col_1_2 bbox vlinedarkleft pl46">
                                <h5>Milestones</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Date Created</label>
                                        <eShip:CustomTextBox runat="server" ID="txtInvoiceDate" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                    <div class="fieldgroup">
                                        <label class="wlabel">Due Date</label>
                                        <eShip:CustomTextBox runat="server" ID="txtDueDate" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Posted</label>
                                        <eShip:CustomTextBox runat="server" ID="txtPostDate" CssClass="w200 disabled" ReadOnly="True" />

                                    </div>
                                    <div class="fieldgroup_s">
                                        <asp:CheckBox runat="server" ID="chkPosted" Enabled="False" CssClass="jQueryUniform" />
                                    </div>
                                </div>
                                <div class="row pb20">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Exported</label>
                                        <eShip:CustomTextBox runat="server" ID="txtExportDate" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                    <div class="fieldgroup_s">
                                        <asp:CheckBox runat="server" ID="chkExported" Enabled="False" CssClass="jQueryUniform" />
                                    </div>
                                </div>
                                <h5>General Information</h5>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Prefix</label>
                                        <eShip:CustomHiddenField runat="server" ID="hidPrefixId" />
                                        <asp:Panel ID="Panel1" runat="server" DefaultButton="imgPrefixSearch">
                                            <eShip:CustomTextBox runat="server" ID="txtPrefixCode" CssClass="w80" OnTextChanged="OnPrefixCodeEntered"
                                                AutoPostBack="True" />
                                            <asp:ImageButton ID="imgPrefixSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                                                CausesValidation="False" OnClick="OnPrefixSearchClicked" />
                                            <eShip:CustomTextBox runat="server" ID="txtPrefixDescription" CssClass="w230 disabled mr10" ReadOnly="True" />
                                            <ajax:AutoCompleteExtender runat="server" ID="acePrefixCode" TargetControlID="txtPrefixCode" ServiceMethod="GetPrefixList" ServicePath="~/Services/eShipPlusWSv4P.asmx" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" DelimiterCharacters="" Enabled="True" />
                                            <asp:CheckBox runat="server" ID="chkHidePrefix" CssClass="jQueryUniform" />
                                            <label>Hide</label>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">
                                            Special Instructions
                                            <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleSpecialInstructions" TargetControlId="txtSpecialInstructions" MaxLength="500" />
                                        </label>
                                        <eShip:CustomTextBox runat="server" ID="txtSpecialInstructions" TextMode="MultiLine" Rows="3" CssClass="w420 h150" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabInvoiceDetails" HeaderText="Invoice Details">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlDetails" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel runat="server" ID="pnlInvoiceDetails">
                                <div class="row mb10">
                                    <div class="fieldgroup">
                                        <label class="wlabel blue">Total Amount Due</label>
                                        <eShip:CustomTextBox ID="txtTotalDue" runat="server" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                    <div class="fieldgroup right pt26">
                                        <asp:Button runat="server" ID="btnAddShipment" Text="Add Shipment" CssClass="mr10" CausesValidation="false" OnClick="OnAddShipmentClicked" />
                                        <asp:Button runat="server" ID="btnAddServiceTicket" Text="Add Service Ticket" CssClass="mr10" CausesValidation="false" OnClick="OnAddServiceTicketClicked" />
                                        <asp:Button runat="server" ID="btnAddOriginalInvoiceDetail" Text="Add Original Invoice Detail" CssClass="mr10" CausesValidation="false" OnClick="OnAddOriginalInvoiceDetailClicked" />
                                        <asp:Button runat="server" ID="btnAddDetail" Text="Add Detail" CssClass="mr10" CausesValidation="false" OnClick="OnAddDetailClicked" />
                                        <asp:Button runat="server" ID="btnClearDetails" Text="Clear Details" CausesValidation="False" OnClick="OnClearDetailsClicked" />
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    $(window).load(function () {
                                        $('#invoiceDetailsTable [id$="txtAmountDue"]').each(function () {
                                            UpdateDetailAmountDue(this);
                                        });
                                        UpdateTotalAmountDue();

                                        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                                            $('#invoiceDetailsTable [id$="txtAmountDue"]').each(function () {
                                                UpdateDetailAmountDue(this);
                                            });
                                            UpdateTotalAmountDue();
                                        });
                                    });

                                    function UpdateDetailAmountDue(control) {
                                        var unitSell = $(control).parent().parent().find('[id$="txtUnitSell"]').val();
                                        var unitDiscount = $(control).parent().parent().find('[id$="txtUnitDiscount"]').val();
                                        var quantity = $(control).parent().parent().find('[id$="txtQuantity"]').val();

                                        if (quantity < 1) quantity = 1;
                                        $(control).parent().parent().find('[id$="txtAmountDue"]').val(((unitSell - unitDiscount) * quantity).toFixed(4));
                                    }

                                    function UpdateTotalAmountDue() {
                                        var totalAmountDue = 0;
                                        $('#invoiceDetailsTable [id$="txtAmountDue"]').each(function () {
                                            totalAmountDue += Number($(this).val());
                                        });
                                        totalAmountDue = totalAmountDue.toFixed(2);

                                        $('#<%= txtTotalDue.ClientID %>').val('$' + totalAmountDue);
                                        $('#<%= txtInvoiceTotal.ClientID %>').val('$' + totalAmountDue);

                                    }
                                </script>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheInvoiceDetailsTable" TableId="invoiceDetailsTable" HeaderZIndex="2" IgnoreHeaderBackgroundFill="True" />
                                <table class="stripe" id="invoiceDetailsTable">
                                    <tr>
                                        <th style="width: 15%;">Reference Number
                                        </th>
                                        <th style="width: 9%;">Reference Type
                                        </th>
                                        <th style="width: 8%;" class="text-center">Quantity
                                        </th>
                                        <th style="width: 10%;" class="text-right">Unit Sell ($)
                                        </th>
                                        <th style="width: 10%;" class="text-right">Unit Discount ($)
                                        </th>
                                        <th style="width: 10%;" class="text-right">Amount Due ($)
                                        </th>
                                        <th style="width: 30%;">Account Bucket
                                        </th>
                                        <th style="width: 8%;">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstInvoiceDetails" runat="server" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnInvoiceDetailsItemDataBound">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td class="top">
                                                    <eShip:CustomHiddenField ID="hidInvoiceDetailIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <eShip:CustomHiddenField ID="hidInvoiceDetailId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <eShip:CustomTextBox ID="txtRefNumber" runat="server" Text='<%# Eval("ReferenceNumber") %>' CssClass="w200" />
                                                </td>
                                                <td class="top">
                                                    <asp:DropDownList ID="ddlReferenceType" DataTextField="Text" DataValueField="Value" runat="server" CssClass="w130" />
                                                </td>
                                                <td class="text-right top">
                                                    <eShip:CustomTextBox ID="txtQuantity" runat="server" Text='<%# Eval("Quantity") %>' CssClass="w60" onblur="javascript:UpdateDetailAmountDue(this); UpdateTotalAmountDue();" Type="NumbersOnly" />
                                                </td>
                                                <td class="text-right top">
                                                    <eShip:CustomTextBox ID="txtUnitSell" runat="server" Text='<%# Eval("UnitSell", "{0:f4}") %>' CssClass="w80" onblur="javascript:UpdateDetailAmountDue(this); UpdateTotalAmountDue();" Type="FloatingPointNumbers" />
                                                </td>
                                                <td class="text-right top">
                                                    <eShip:CustomTextBox ID="txtUnitDiscount" runat="server" Text='<%# Eval("UnitDiscount", "{0:f4}") %>' CssClass="w80" onblur="javascript:UpdateDetailAmountDue(this); UpdateTotalAmountDue();" Type="FloatingPointNumbers" />
                                                </td>
                                                <td class="text-right top">
                                                    <eShip:CustomTextBox ID="txtAmountDue" runat="server" Text='<%# Eval("AmountDue", "{0:f4}") %>' CssClass="w80 disabled" ReadOnly="True" Type="FloatingPointNumbers" />
                                                </td>
                                                <td class="top">
                                                    <eShip:CachedObjectDropDownList runat="server" ID ="ddlAccountBucket" CssClass="w230" Type="AccountBuckets" 
                                                        EnableChooseOne="True" DefaultValue="0" SelectedValue='<%# Eval("AccountBucketId") %>'/>
                                                </td>
                                                <td class="text-center top">
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        CausesValidation="false" OnClick="OnDeleteInvoiceDetailClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                            <tr class="hidden">
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td colspan="10" class="pt0 bottom_shadow">
                                                    <div class="col_1_3">
                                                        <div class="row">
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue">Charge Code</label>
                                                                <eShip:CachedObjectDropDownList runat="server" ID ="ddlChargeCode" CssClass="w300" Type="ChargeCodes"
                                                                     EnableChooseOne="True" DefaultValue="0" SelectedValue='<%# Eval("ChargeCodeId") %>'/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col_1_3 no-right-border">
                                                        <div class="row">
                                                            <div class="fieldgroup">
                                                                <label class="wlabel blue">Comment</label>
                                                                <eShip:CustomTextBox ID="txtComment" runat="server" Text='<%# Eval("Comment") %>' CssClass="w300" MaxLength="50" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnAddShipment" />
                            <asp:PostBackTrigger ControlID="btnAddServiceTicket" />
                            <asp:PostBackTrigger ControlID="btnAddOriginalInvoiceDetail" />
                        </Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabPrintLogs" HeaderText="Print Logs">
                <ContentTemplate>
                    <eShip:TableFreezeHeaderExtender runat="server" ID="tfhePrintLogsTable" TableId="printLogsTable" HeaderZIndex="2" IgnoreHeaderBackgroundFill="True" />
                    <table class="stripe" id="printLogsTable">
                        <tr>
                            <th style="width: 15%;">Username
                            </th>
                            <th style="width: 15%;">User First Name
                            </th>
                            <th style="width: 15%;">User Last Name
                            </th>
                            <th style="width: 25%;">User Default Customer
                            </th>
                            <th style="width: 10%;" class="text-center">Employee
                            </th>
                            <th style="width: 20%">Log Date
                            </th>
                        </tr>
                        <asp:ListView runat="server" ID="lstPrintLogs" ItemPlaceholderID="itemPlaceHolder">
                            <LayoutTemplate>
                                <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <%# Eval("UserLogon") %>
                                    </td>
                                    <td>
                                        <%# Eval("UserFirstName")%>
                                    </td>
                                    <td>
                                        <%# Eval("UserLastName")%>
                                    </td>
                                    <td>
                                        <%# Eval("UserDefaultCustomerNumber") %> - <%# Eval("UserDefaultCustomerName")%>
                                    </td>
                                    <td class="text-center">
                                        <%# Eval("TenantEmployee").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                                    </td>
                                    <td>
                                        <%# Eval("LogDate").FormattedLongDate() %>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </table>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabAuditLog" HeaderText="Audit Log">
                <ContentTemplate>
                    <eShip:AuditLogControl runat="server" ID="auditLogs" />
                </ContentTemplate>
            </ajax:TabPanel>
        </ajax:TabContainer>
    </div>
    <asp:Panel runat="server" ID="pnlNewInvoiceSelection" Visible="false">
        <div class="popupControl popupControlOverW500">
            <div class="popheader">
                <h4>New Invoice Type Selection</h4>
                <asp:ImageButton ID="ImageButton2" ImageUrl="~/images/icons2/close.png" CssClass="close" CausesValidation="false" OnClick="OnCloseNewInvoiceSelectionTypeClicked" runat="server" />
            </div>
            <div class="row text-center pt10 pb10">
                <asp:Button runat="server" ID="btnInvoiceSelect" Text="INVOICE" OnClick="OnInvoiceTypeSelectClicked" CssClass="text-center mr10" Width="110px" CausesValidation="false" />
                <asp:Button runat="server" ID="btnSupplementalSelect" Text="SUPPLEMENTAL" OnClick="OnInvoiceTypeSelectClicked" CssClass="text-center mr10" Width="110px" CausesValidation="false" />
                <asp:Button runat="server" ID="btnCreditSelect" Text="CREDIT" OnClick="OnInvoiceTypeSelectClicked" CssClass="text-center mr10" Width="110px" CausesValidation="false" />
                <asp:Button runat="server" ID="btnCancelSelect" Text="CANCEL" OnClick="OnCloseNewInvoiceSelectionTypeClicked" CssClass="text-center mr10" Width="110px" CausesValidation="False" />
            </div>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlEditPostedInvoice" Visible="false">
        <div class="popupControl popupControlOverW500">
            <div class="popheader">
                <h4>Update Amount Paid
                </h4>
                <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                    CausesValidation="false" OnClick="OnCloseUpdateAmountPaidClicked" runat="server" />
            </div>
            <div class="row">
                <table class="poptable">
                    <tr>
                        <td class="text-right">
                            <label class="upper">Invoice Number:</label>
                        </td>
                        <td>
                            <eShip:CustomTextBox runat="server" ID="txtPostedInvoiceNumber" CssClass="w200 disabled" ReadOnly="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">
                            <label class="upper">Invoice Total:</label>
                        </td>
                        <td>
                            <eShip:CustomTextBox runat="server" ID="txtPostedInvoiceTotal" CssClass="w200 disabled" ReadOnly="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">
                            <label class="upper">Payment Amount:</label>
                        </td>
                        <td>
                            <eShip:CustomTextBox runat="server" ID="txtUpdatePaidAmount" CssClass="w200" />
                            <ajax:FilteredTextBoxExtender runat="server" TargetControlID="txtUpdatePaidAmount"
                                FilterType="Custom, Numbers" ValidChars="." />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="pt10">
                            <asp:Button ID="btnDoneEditInvoice" Text="Update" OnClick="OnDoneUpdateAmountPaidClick" runat="server" CausesValidation="False" />
                            <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnCloseUpdateAmountPaidClicked" runat="server" CausesValidation="false" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlAddOriginalInvoiceDetail" Visible="False">
        <div class="popupControl popupControlOverW500">
            <div class="popheader">
                <h4>Add Original Invoice Detail
                </h4>
                <asp:ImageButton ID="ibtnCloseOriginalInvoiceDetail" ImageUrl="~/images/icons2/close.png" CssClass="close"
                    CausesValidation="false" OnClick="OnCloseAddOriginalInvoiceDetailClicked" runat="server" />
            </div>
            <div class="row pt10">
                <table class="poptable">
                    <tr>
                        <td class="text-right">
                            <label class="upper">Original Invoice Detail: </label>
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlDetailOriginalReference" DataTextField="Text" DataValueField="Value" CssClass="w300" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:Button runat="server" ID="btnAddDoneOriginalInvoiceDetail" Text="Add Detail" CausesValidation="False" OnClick="OnAddDoneOriginalInvoiceDetailClicked" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlModifyApplicationOfMiscReceiptsToInvoice" CssClass="popup popupControlOverW500" Visible="False">
        <div class="popheader">
            <h4>Update Amount Paid And Misc Receipt Applications</h4>
            <asp:ImageButton runat="server" ID="ibtnCloseModifyApplicationOfMiscReceiptsToInvoice" CssClass="close" ImageUrl="~/images/icons2/close.png" OnClick="OnCloseModifyApplicationOfMiscReceiptsToInvoice" />
        </div>
        <div class="row pt10">
            <table class="poptable">
                 <tr>
                        <td class="text-right">
                            <label class="upper">Invoice Number:</label>
                        </td>
                        <td>
                            <eShip:CustomTextBox runat="server" ID="txtModifyMiscReceiptAppPostedInvoiceNumber" CssClass="w200 disabled" ReadOnly="True" />
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">
                            <label class="upper">Invoice Total:</label>
                        </td>
                        <td>
                            <eShip:CustomTextBox runat="server" ID="txtModifyMiscReceiptAppPostedInvoiceTotal" CssClass="w200 disabled" ReadOnly="True" />
                        </td>
                    </tr>
            </table>
        </div>
        <script type="text/javascript">
            $(function() {
                $('input:text[id$="txtMiscReceiptApplicationAmount"]').each(function() {
                    $(this).change(function() {
                        UpdateModifyMiscReceiptAppsTotalPaidAmount();
                    });
                });
                
                $('#<%= txtPaidAmountAmountUnrelatedToMiscReceiptApplications.ClientID %>').change(function() {
                    UpdateModifyMiscReceiptAppsTotalPaidAmount();
                });

                UpdateModifyMiscReceiptAppsTotalPaidAmount();
            });
            
            function UpdateModifyMiscReceiptAppsTotalPaidAmount() {
                var amountTextboxControls = $('input:text[id$="txtMiscReceiptApplicationAmount"]');
                var total = 0;
                for (var i = 0; i < amountTextboxControls.length; i++) {
                    var amount = parseFloat($(amountTextboxControls[i]).val());
                    if (!isNaN(amount))
                        total += parseFloat($(amountTextboxControls[i]).val());
                }
                var amountUnrelated = parseFloat($('#<%= txtPaidAmountAmountUnrelatedToMiscReceiptApplications.ClientID %>').val());
                if (!isNaN(amountUnrelated)) total += amountUnrelated;

                $('#<%= txtModifyMiscReceiptAppsTotalPaidAmount.ClientID %>').val(total.toFixed(4));
            }
        </script>
        <asp:Repeater runat="server" ID="rptShipmentsMiscReceiptApplications" OnItemDataBound="OnShipmentsMiscReceiptApplicationsItemDataBound">
            <ItemTemplate>
                <h5 class="pl20"> <%# !string.IsNullOrEmpty(Eval("ShipmentNumber").ToString()) ? string.Format("Shipment {0}", Eval("ShipmentNumber")) : "Miscellaneous" %></h5>
                <eShip:CustomHiddenField runat="server" ID="hidMiscReceiptId" Value='<%# Eval("Id") %>'/>
                <table class="stripe">
                    <tr>
                        <th style="width: 70%">Application Date
                        </th>
                        <th style="width: 30%">Amount
                        </th>
                    </tr>
                    <asp:Repeater runat="server" ID="rptMiscReceiptApplications">
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <eShip:CustomHiddenField runat="server" Id="hidMiscReceiptApplicationId" Value='<%# Eval("Id") %>'/>
                                    <%# Eval("ApplicationDate") %>
                                </td>
                                <td>
                                    <eShip:CustomTextBox runat="server" ID="txtMiscReceiptApplicationAmount" Text='<%# Eval("Amount") %>' Type="FloatingPointNumbers" CssClass="w100"/>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </table>
            </ItemTemplate>
        </asp:Repeater>
        <div class="row pt10 pb5">
            <table class="poptable">
                <tr>
                    <td style="width:71%" class="text-right">
                        <label class="upper">Paid Amount Unrelated To Misc Receipts:</label>
                    </td>
                    <td>
                        <eShip:CustomTextBox runat="server" ID="txtPaidAmountAmountUnrelatedToMiscReceiptApplications" CssClass="w100" Type="FloatingPointNumbers" />
                    </td>
                </tr>
                <tr>
                    <td class="text-right">
                        <label class="upper">Total Paid Amount:</label>
                    </td>
                    <td>
                        <eShip:CustomTextBox runat="server" ID="txtModifyMiscReceiptAppsTotalPaidAmount" CssClass="w100 disabled" ReadOnly="True"/>
                    </td>
                </tr>
                <tr>
                    <td class="pt10" colspan="2">
                        <asp:Button runat="server" ID="btnCloseModifyApplicationOfMiscReceiptsToInvoice" CssClass="right" CausesValidation="False" OnClick="OnCloseModifyApplicationOfMiscReceiptsToInvoice" Text="Cancel" />
                        <asp:Button runat="server" ID="btnModifyApplicationOfMiscReceiptsToInvoice" CssClass="right mr10" CausesValidation="False" OnClick="OnModifyApplicationOfMiscReceiptsToInvoice" Text="Update" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:UpdatePanel runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
            <eShip:CustomHiddenField runat="server" ID="hidFlag" />
            <eShip:CustomHiddenField runat="server" ID="hidMilestone" />
            <eShip:CustomHiddenField runat="server" ID="hidEditStatus" />
            <eShip:CustomHiddenField runat="server" ID="hidPostTypeFlag" />
            <eShip:CustomHiddenField runat="server" ID="hidMessageBoxFlag" />
            <eShip:CustomHiddenField runat="server" ID="hidMiscReceiptsRelatedToPostedInvoiceExists" />
            <eShip:CustomHiddenField runat="server" ID="hidMiscReceiptApplicationsRelatedToPostedInvoiceExists" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
