﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Documents;
using LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting
{
    public partial class InvoiceDashboardView : MemberPageBase, IInvoiceDashboardView
    {
        private SearchField SortByField
        {
            get { return AccountingSearchFields.InvoiceSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue); }
        }

        protected Permission InvoiceAccess { get; set; }

        public static string PageAddress { get { return "~/Members/Accounting/InvoiceDashboardView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.InvoiceDashboard; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.FinanceBlue; } }

        public event EventHandler<ViewEventArgs<InvoiceViewSearchCriteria>> Search;

        public void DisplaySearchResult(List<InvoiceDashboardDto> invoices)
        {
            litRecordCount.Text = invoices.BuildRecordCount();
            upcseDataUpdate.DataSource = invoices;
            upcseDataUpdate.DataBind();
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems
            {
                Columns = GetCurrentRunParameters(true),
                SortAscending = rbAsc.Checked,
                SortBy = SortByField,
            };
        }

        private void DoSearchPreProcessingThenSearch(SearchField field)
        {
            DoSearch(new InvoiceViewSearchCriteria
            {
                Parameters = GetCurrentRunParameters(false),
                ActiveUserId = ActiveUser.Id,
                SortBy = field,
                SortAscending = rbAsc.Checked
            });
        }

        private void DoSearch(InvoiceViewSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<InvoiceViewSearchCriteria>(criteria));
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new InvoiceDashboardHandler(this).Initialize();

            InvoiceAccess = ActiveUser.RetrievePermission(ViewCode.Invoice);

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            // set sort fields
            ddlSortBy.DataSource = AccountingSearchFields.InvoiceSortFields.OrderBy(s => s.DisplayName);
            ddlSortBy.DataBind();
            ddlSortBy.SelectedValue = AccountingSearchFields.InvoiceNumber.Name;

            //link up sort buttons
            lbtnSortInvoiceNumber.CommandName = AccountingSearchFields.InvoiceNumber.Name;
            lbtnSortInvoiceDate.CommandName = AccountingSearchFields.InvoiceDate.Name;
            lbtnSortPostDate.CommandName = AccountingSearchFields.PostDate.Name;
            lbtnSortDueDate.CommandName = AccountingSearchFields.DueDate.Name;
            lbtnSortAmountDue.CommandName = AccountingSearchFields.AmountDue.Name;
            lbtnSortPaidAmount.CommandName = AccountingSearchFields.PaidAmount.Name;
            lbtnSortUsername.CommandName = AccountingSearchFields.Username.Name;
            lbtnSortTypeText.CommandName = AccountingSearchFields.InvoiceTypeText.Name;

            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);
            var criteria = new InvoiceViewSearchCriteria
                {
                    ActiveUserId = ActiveUser.Id,
                    Parameters = profile != null
                                     ? profile.Columns
                                     : AccountingSearchFields.DefaultInvoices.Select(f => f.ToParameterColumn()).ToList(),
                    SortAscending = profile == null || profile.SortAscending,
                    SortBy = profile == null ? null : profile.SortBy,
                    DisableNoRecordNotification = true
                };

            lstFilterParameters.DataSource = criteria.Parameters;
            lstFilterParameters.DataBind();

            rbAsc.Checked = criteria.SortAscending;
            rbDesc.Checked = !criteria.SortAscending;

            if (criteria.SortBy != null && ddlSortBy.ContainsValue(criteria.SortBy.Name))
                ddlSortBy.SelectedValue = criteria.SortBy.Name;
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            var field = AccountingSearchFields.InvoiceSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue);
            upcseDataUpdate.ResetLoadCount();
            DoSearchPreProcessingThenSearch(field);
        }

        protected void OnInvoiceDetailsItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var invoice = (InvoiceDashboardDto)item.DataItem;

            if (invoice == null) return;

            var detailControl = (InvoiceDashboardDetail2Control)item.FindControl("invoiceDashboardDetail2");

            detailControl.LoadInvoice(invoice);
        }

       
        protected void OnGenerateInvoiceRequest(object sender, ViewEventArgs<long> e)
        {
            documentDisplay.Visible = true;
            hidLastInvoiceId.Value = e.Argument.ToString();
            var invoice = new Invoice(e.Argument);
            documentDisplay.Title = string.Format("{0} Invoice", invoice.InvoiceNumber);
            documentDisplay.DisplayHtml(this.GenerateInvoice(invoice));
            documentDisplay.Visible = true;
        }

        protected void OnDocumentDisplayClosed(object sender, EventArgs e)
        {
            documentDisplay.Visible = false;
        }



        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = AccountingSearchFields.Invoices.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(AccountingSearchFields.Invoices);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;

        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSortData(object sender, CommandEventArgs e)
        {
            var field = AccountingSearchFields.InvoiceSortFields.FirstOrDefault(c => c.Name == e.CommandName);
            if (field == null) return;
            if (ddlSortBy.ContainsValue(field.Name))
            {
                if (ddlSortBy.SelectedValue == field.Name && rbAsc.Checked)
                {
                    rbAsc.Checked = false;
                    rbDesc.Checked = true;
                }
                else if (ddlSortBy.SelectedValue == field.Name && rbDesc.Checked)
                {
                    rbAsc.Checked = true;
                    rbDesc.Checked = false;
                }

                ddlSortBy.SelectedValue = field.Name;
            }

            DoSearchPreProcessingThenSearch(field);
        }

        protected void OnSortBySelectedIndexChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(AccountingSearchFields.InvoiceSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue));
        }

        protected void OnSortOrderCheckChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(AccountingSearchFields.InvoiceSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue));
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();

            rbAsc.Checked = e.Argument.SortAscending;
            rbDesc.Checked = !e.Argument.SortAscending;

            if (e.Argument.SortBy != null && ddlSortBy.ContainsValue(e.Argument.SortBy.Name))
                ddlSortBy.SelectedValue = e.Argument.SortBy.Name;
        }
    }
}