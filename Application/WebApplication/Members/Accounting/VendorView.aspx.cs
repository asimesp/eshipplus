﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.Connect;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting
{
	public partial class VendorView : MemberPageBase, IVendorView, ILocationCollectionResource
	{
		private const string LocationsHeader = "Locations";
		private const string ServicesHeader = "Services";
		private const string EquipmentHeader = "Equip. Types";
		private const string VendorInsuranceHeader = "Insurance";
		private const string VendorPackageCustomMappingHeader = "Pkg. Mappings";
		private const string DocumentsHeader = "Documents";
		private const string NoServiceDaysHeader = "No Service Days";

		private const string SaveFlag = "S";
		private const string DeleteFlag = "D";

        private const string GoToVendorRatingsDisplay = "GoToVendorRatingsDisplay";
        private const string ShowVendorPerformanceStats = "ShowVendorPerformanceStats";

		public static string PageAddress { get { return "~/Members/Accounting/VendorView.aspx"; } }

		public override ViewCode PageCode { get { return ViewCode.Vendor; } }

		public override string SetPageIconImage
		{
			set { imgPageImageLogo.ImageUrl = IconLinks.FinanceBlue; }
		}

		public List<ViewListItem> ContactTypeCollection
		{
			get
			{
				var viewListItems = ProcessorVars.RegistryCache[ActiveUser.TenantId]
					.ContactTypes
					.OrderBy(c => c.Description)
					.Select(t => new ViewListItem(t.Code, t.Id.ToString()))
					.ToList();
				viewListItems.Insert(0, new ViewListItem(WebApplicationConstants.ChooseOne, default(long).ToString()));
				return viewListItems;
			}
		}

		public List<ViewListItem> CountryCollection
		{
			get
			{
				var viewListItems = ProcessorVars.RegistryCache.Countries
					.Values
					.OrderByDescending(t => t.SortWeight)
					.ThenBy(t => t.Name)
					.Select(c => new ViewListItem(c.Name, c.Id.ToString()))
					.ToList();
				viewListItems.Insert(0, new ViewListItem(WebApplicationConstants.ChooseOne, default(long).ToString()));
				return viewListItems;
			}
		}

		public Dictionary<int, string> BrokerReferenceTypes
		{
			set
			{
				ddlBrokerReferenceNumberType.DataSource = value ?? new Dictionary<int, string>();
				ddlBrokerReferenceNumberType.DataBind();
			}
		}

        public Dictionary<int, string> DateUnits
        {
            set { vpscVendorPerformance.DateUnits = value; }
        }


		public event EventHandler Loading;
		public event EventHandler<ViewEventArgs<Vendor>> Save;
		public event EventHandler<ViewEventArgs<Vendor>> Delete;
		public event EventHandler<ViewEventArgs<Vendor>> Lock;
		public event EventHandler<ViewEventArgs<Vendor>> UnLock;
		public event EventHandler<ViewEventArgs<string>> VendorServiceRepSearch;
		public event EventHandler<ViewEventArgs<Vendor>> LoadAuditLog;

		public void LogException(Exception ex)
		{
			if (ex != null) ErrorLogger.LogError(ex, Context);
		}

		public void DisplayVendorServiceRep(User user)
		{
			txtVendorServiceRepUsername.Text = user == null ? string.Empty : user.Username;
			txtVendorServiceRepName.Text = user == null ? string.Empty : string.Format("{0} {1}", user.FirstName, user.LastName);
			hidVendorServiceRepUserId.Value = user == null ? string.Empty : user.Id.ToString();
		}

		public void DisplayMessages(IEnumerable<ValidationMessage> messages)
		{
			if (messages == null) return;
			if (!messages.Any()) return;

			if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == SaveFlag)
			{
				// clean up file cleared out!
				if (!string.IsNullOrEmpty(hidFilesToDelete.Value))
				{
					Server.RemoveFiles(hidFilesToDelete.Value.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries));
					hidFilesToDelete.Value = string.Empty;
				}

				// unlock and return to read-only
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<Vendor>(new Vendor(hidVendorId.Value.ToLong(), false)));
				SetEditStatus(false);
				memberToolBar.ShowUnlock = false;
			}

			messageBox.Icon = messages.GenerateIcon();
			messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
			messageBox.Visible = true;

			litErrorMessages.Text = messages.HasErrors()
										? string.Join(WebApplicationConstants.HtmlBreak,
													  messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
														Select(m => m.Message).ToArray())
										: string.Empty;
		}

		public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs)
		{
			auditLogs.Load(logs);
		}

		public void FailedLock(Lock @lock)
		{
			memberToolBar.ShowNew = false;
			memberToolBar.ShowSave = false;
			memberToolBar.ShowDelete = false;
			memberToolBar.ShowUnlock = false;

			SetEditStatus(false);

			litMessage.Text = @lock.BuildFailedLockMessage();
		}

		public void Set(Vendor vendor)
		{
			if (vendor.IsNew && hidFlag.Value == DeleteFlag)
			{
				Server.RemoveDirectory(WebApplicationSettings.VendorFolder(ActiveUser.TenantId, hidVendorId.Value.ToLong()));
				hidFlag.Value = string.Empty;
			}

			LoadVendor(vendor); // if previously new, reload
			SetEditStatus(!vendor.IsNew);
		}


		private List<VendorLocation> RetrieveViewLocations(Vendor vendor)
		{
			var locations = new List<VendorLocation>();
			foreach (var item in lstLocations.Items)
			{
				var control = item.FindControl("llicLocation").ToLocationListingInputControl();
				if (control == null) continue;
				var locationId = control.RetrieveLocationId();
				var location = new VendorLocation(locationId, locationId != default(long));
				if (location.IsNew)
				{
					location.DateCreated = DateTime.Now;
					location.Tenant = vendor.Tenant;
					location.Vendor = vendor;
				}
				control.UpdateLocation(location);

				locations.Add(location);
			}
			return locations;
		}

		private List<VendorInsurance> RetrieveViewInsurances(Vendor vendor)
		{
			var insurances = new List<VendorInsurance>();

			var vendorInsurances = from item in lstVendorInsurance.Items
								   let hidden = item.FindControl("hidVendorInsuranceId").ToCustomHiddenField()
								   where hidden != null
								   let insuranceId = hidden.Value.ToLong()
								   select new VendorInsurance(insuranceId, insuranceId != default(long))
								   {
									   CarrierName = item.FindControl("txtVendorInsuranceCarrierName").ToTextBox().Text,
									   PolicyNumber = item.FindControl("txtVendorInsurancePolicyNumber").ToTextBox().Text,
									   CertificateHolder = item.FindControl("txtVendorInsuranceCertificateHolder").ToTextBox().Text,
									   CoverageAmount = item.FindControl("txtVendorInsuranceCoverageAmount").ToTextBox().Text.ToDecimal(),
									   Required = item.FindControl("chkVendorInsuranceRequired").ToAltUniformCheckBox().Checked,
									   EffectiveDate = item.FindControl("txtVendorInsuranceEffectiveDate").ToTextBox().Text.ToDateTime(),
									   ExpirationDate = item.FindControl("txtVendorInsuranceExpirationDate").ToTextBox().Text.ToDateTime(),
									   InsuranceTypeId = item.FindControl("ddlVendorInsuranceType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
									   SpecialInstruction = item.FindControl("txtVendorInsuranceSpecialInstruction").ToTextBox().Text
								   };
			foreach (var insurance in vendorInsurances)
			{
				if (insurance.IsNew)
				{
					insurance.Tenant = vendor.Tenant;
					insurance.Vendor = vendor;
				}

				insurances.Add(insurance);
			}
			return insurances;
		}

		private List<VendorPackageCustomMapping> RetrieveViewPackageMappings(Vendor vendor)
		{
			var packageMappings = new List<VendorPackageCustomMapping>();

			var viewPackages = from item in lstVendorPackageCustomMapping.Items
			                       let hidden = item.FindControl("hidVendorPackageCustomMappingId").ToCustomHiddenField()
			                       where hidden != null
			                       let packageMappingId = hidden.Value.ToLong()
			                       select new VendorPackageCustomMapping(packageMappingId, packageMappingId != default(long))
				                       {
					                       VendorCode = item.FindControl("txtVendorPackageCustomMappingVendorCode").ToTextBox().Text,
					                       PackageTypeId = item.FindControl("ddlPackageType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
				                       };
            foreach (var package in viewPackages)
            {
                if (package.IsNew)
                {
                    package.Tenant = vendor.Tenant;
                    package.Vendor = vendor;
                }

                packageMappings.Add(package);
            }
			return packageMappings;
		}


		private void DisplayLastAuditInformation(User user, DateTime lastAuditDate)
		{
			txtLastAuditDate.Text = lastAuditDate.FormattedLongDate();
			txtLastAuditedBy.Text = user == null
										? string.Empty
										: string.Format("{0} [{1} {2}]", user.Username, user.FirstName, user.LastName);
			hidLastAuditedByUserId.Value = user == null ? default(long).ToString() : user.Id.ToString();
		}

		private void LoadVendor(Vendor vendor)
		{
			if (!vendor.IsNew) vendor.LoadCollections();

			hidVendorId.Value = vendor.Id.ToString();

			//DETAILS TAB
			chkActive.Checked = vendor.Active;
			txtVendorNumber.Text = vendor.VendorNumber;
			txtVendorName.Text = vendor.Name;
			txtNotation.Text = vendor.Notation;
			txtNotes.Text = vendor.Notes;
			chkTSACertified.Checked = vendor.TSACertified;
			chkCTPATCertified.Checked = vendor.CTPATCertified;
			chkPIPCertified.Checked = vendor.PIPCertified;
			txtCTPATNumber.Text = vendor.CTPATNumber;
			txtPIPNumber.Text = vendor.PIPNumber;
			chkSmartWayCertified.Checked = vendor.SmartWayCertified;
			txtSCAC.Text = vendor.Scac;
			txtFederalIdNumber.Text = vendor.FederalIDNumber;
			txtDateCreated.Text = vendor.DateCreated.FormattedShortDate();
			txtBrokerReferenceNumber.Text = vendor.BrokerReferenceNumber;
			ddlBrokerReferenceNumberType.SelectedValue = vendor.BrokerReferenceNumberType.ToInt().ToString();
			txtMC.Text = vendor.MC;
			txtDOT.Text = vendor.DOT;
			txtTrackingUrl.Text = vendor.TrackingUrl;
			chkLTL.Checked = vendor.HandlesLessThanTruckload;
			chkFTL.Checked = vendor.HandlesTruckload;
			chkAir.Checked = vendor.HandlesAir;
			chkPTL.Checked = vendor.HandlesPartialTruckload;
			chkRail.Checked = vendor.HandlesRail;
			chkSmallPack.Checked = vendor.HandlesSmallPack;
			chkCarrier.Checked = vendor.IsCarrier;
			chkAgent.Checked = vendor.IsAgent;
			chkBroker.Checked = vendor.IsBroker;

			txtCustomField1.Text = vendor.CustomField1;
			txtCustomField2.Text = vendor.CustomField2;
			txtCustomField3.Text = vendor.CustomField3;
			txtCustomField4.Text = vendor.CustomField4;

			DisplayVendorServiceRep(vendor.VendorServiceRep);
			DisplayLastAuditInformation(vendor.LastAuditedBy, vendor.LastAuditDate);

			//logo
			imgLogo.ImageUrl = vendor.LogoUrl;
			imgLogo.Visible = !string.IsNullOrEmpty(vendor.LogoUrl);
			hidLogoUrl.Value = vendor.LogoUrl;
			imgLogo.AlternateText = string.IsNullOrEmpty(vendor.LogoUrl)
										? string.Empty
										: string.Format("{0} logo", vendor.Name);

			//Locations
			chkExludeFromAvailableLoadsBlast.Checked = vendor.ExcludeFromAvailableLoadsBlast;
			lstLocations.DataSource = vendor.Locations;
			lstLocations.DataBind();
			tabLocations.HeaderText = vendor.Locations.BuildTabCount(LocationsHeader);

			//Services
			var services = new ServiceViewSearchDto().RetrieveVendorServices(vendor).OrderBy(s => s.Code);
			lstServices.DataSource = services;
			lstServices.DataBind();
			tabServices.HeaderText = services.BuildTabCount(ServicesHeader);

			//Equipment Types
			var equipment = vendor.RetrieveEquipmentTypes().OrderBy(e => e.TypeName);
			lstEquipmentTypes.DataSource = equipment;
			lstEquipmentTypes.DataBind();
			tabEquipmentTypes.HeaderText = equipment.BuildTabCount(EquipmentHeader);

			//Vendor Insurance
			lstVendorInsurance.DataSource = vendor.Insurances;
			lstVendorInsurance.DataBind();
			tabVendorInsurance.HeaderText = vendor.Insurances.BuildTabCount(VendorInsuranceHeader);

			//package mappings
			lstVendorPackageCustomMapping.DataSource = vendor.VendorPackageCustomMappings;
			lstVendorPackageCustomMapping.DataBind();
			tabVendorPackageCustomMapping.HeaderText = vendor.VendorPackageCustomMappings.BuildTabCount(VendorPackageCustomMappingHeader);

			//Documents
			lstDocuments.DataSource = vendor.Documents;
			lstDocuments.DataBind();
			tabDocuments.HeaderText = vendor.Documents.BuildTabCount(DocumentsHeader);

			//No Service Days
			var noServiceDays = vendor.RetrieveNoServiceDays().OrderBy(c => c.DateOfNoService);
			lstNoServiceDays.DataSource = noServiceDays;
			lstNoServiceDays.DataBind();
			tabNoServiceDays.HeaderText = noServiceDays.BuildTabCount(NoServiceDaysHeader);

			hidCommunicationId.Value = vendor.Communication != null ? vendor.Communication.Id.GetString() : default(long).GetString();
			hidHasRating.Value = vendor.Ratings.Any().GetString();
			memberToolBar.LoadAdditionalActions(ToolbarMoreActions());

			memberToolBar.ShowUnlock = vendor.HasUserLock(ActiveUser, vendor.Id);
		}

		private void ProcessTransferredRequest(Vendor vendor)
		{
		    if (vendor.IsNew) return;
		    
            LoadVendor(vendor);
		    if (LoadAuditLog != null) LoadAuditLog(this, new ViewEventArgs<Vendor>(vendor));
		}


		private void UpdateDetails(Vendor vendor)
		{
			vendor.Active = chkActive.Checked;
			vendor.VendorNumber = txtVendorNumber.Text;
			vendor.Name = txtVendorName.Text;
			vendor.Notation = txtNotation.Text;
			vendor.Notes = txtNotes.Text;
			vendor.TSACertified = chkTSACertified.Checked;
			vendor.CTPATCertified = chkCTPATCertified.Checked;
			vendor.PIPCertified = chkPIPCertified.Checked;
			vendor.CTPATNumber = txtCTPATNumber.Text;
			vendor.PIPNumber = txtPIPNumber.Text;
			vendor.SmartWayCertified = chkSmartWayCertified.Checked;
			vendor.Scac = txtSCAC.Text;
			vendor.FederalIDNumber = txtFederalIdNumber.Text;
			vendor.BrokerReferenceNumber = txtBrokerReferenceNumber.Text;
			vendor.BrokerReferenceNumberType = ddlBrokerReferenceNumberType.SelectedValue.ToEnum<BrokerReferenceNumberType>();
			vendor.MC = txtMC.Text;
			vendor.DOT = txtDOT.Text;
			vendor.TrackingUrl = txtTrackingUrl.Text;

			vendor.HandlesLessThanTruckload = chkLTL.Checked;
			vendor.HandlesTruckload = chkFTL.Checked;
			vendor.HandlesAir = chkAir.Checked;
			vendor.HandlesPartialTruckload = chkPTL.Checked;
			vendor.HandlesRail = chkRail.Checked;
			vendor.HandlesSmallPack = chkSmallPack.Checked;
			vendor.IsCarrier = chkCarrier.Checked;
			vendor.IsAgent = chkAgent.Checked;
			vendor.IsBroker = chkBroker.Checked;

			vendor.CustomField1 = txtCustomField1.Text;
			vendor.CustomField2 = txtCustomField2.Text;
			vendor.CustomField3 = txtCustomField3.Text;
			vendor.CustomField4 = txtCustomField4.Text;

			vendor.VendorServiceRepId = hidVendorServiceRepUserId.Value.ToLong();

			vendor.LastAuditedByUserId = hidLastAuditedByUserId.Value.ToLong();
			vendor.LastAuditDate = txtLastAuditDate.Text.ToDateTime();

			vendor.ExcludeFromAvailableLoadsBlast = chkExludeFromAvailableLoadsBlast.Checked;
		}

		private void UpdateServices(Vendor vendor)
		{
			var services = lstServices.Items
				.Select(item => new VendorService
				{
					ServiceId = item.FindControl("hidServiceId").ToCustomHiddenField().Value.ToLong(),
					Vendor = vendor,
					Tenant = vendor.Tenant
				})
				.ToList();

			vendor.Services = services;
		}

		private void UpdateEquipmentTypes(Vendor vendor)
		{
			var equipmentTypes = lstEquipmentTypes.Items
				.Select(item => new VendorEquipment
				{
					EquipmentTypeId = item.FindControl("hidEquipmentTypeId").ToCustomHiddenField().Value.ToLong(),
					Vendor = vendor,
					Tenant = vendor.Tenant
				})
				.ToList();

			vendor.Equipments = equipmentTypes;
		}

		private void UpdateNoServiceDays(Vendor vendor)
		{
			var noServiceDays = lstNoServiceDays
				.Items
				.Select(item => new VendorNoServiceDay
					{
						NoServiceDayId = item.FindControl("hidNoServiceDayId").ToCustomHiddenField().Value.ToLong(),
						Vendor = vendor,
						Tenant = vendor.Tenant
					})
				.ToList();

			vendor.NoServiceDays = noServiceDays;
		}

		private void UpdateDocumentsFromView(Vendor vendor)
		{
			var documents = lstDocuments.Items
				.Select(i =>
				{
					var id = i.FindControl("hidDocumentId").ToCustomHiddenField().Value.ToLong();
					return new VendorDocument(id, id != default(long))
					{
						DocumentTagId = i.FindControl("hidDocumentTagId").ToCustomHiddenField().Value.ToLong(),
						Name = i.FindControl("litDocumentName").ToLiteral().Text,
						Description = i.FindControl("litDocumentDescription").ToLiteral().Text,
						LocationPath = i.FindControl("hidLocationPath").ToCustomHiddenField().Value,
						IsInternal = i.FindControl("hidIsInternal").ToCustomHiddenField().Value.ToBoolean(),
						Vendor = vendor,
						TenantId = vendor.TenantId
					};
				})
				.ToList();

			vendor.Documents = documents;
		}

		private void UpdateVendorLogo(Vendor vendor)
		{
			var virtualPath = WebApplicationSettings.VendorFolder(vendor.TenantId, vendor.Id);
			var physicalPath = Server.MapPath(virtualPath);

			if (vendor.IsNew) vendor.LogoUrl = string.Empty;

			if (fupLogoImage.HasFile)
			{
				if (!Directory.Exists(physicalPath)) Directory.CreateDirectory(physicalPath);

				//ensure unique filename
				var fi = new FileInfo(Server.MakeUniqueFilename(Path.Combine(physicalPath, fupLogoImage.FileName), true));
				fupLogoImage.WriteToFile(physicalPath, fi.Name, Server.MapPath(vendor.LogoUrl));
				vendor.LogoUrl = virtualPath + fi.Name;
			}
			else if (string.IsNullOrEmpty(hidLogoUrl.Value))
			{
				hidFilesToDelete.Value += string.Format("{0};", vendor.LogoUrl);
				vendor.LogoUrl = string.Empty;
			}
		}


		private List<ToolbarMoreAction> ToolbarMoreActions()
		{
			var goToVendorRatingsMenu = new ToolbarMoreAction
			{
				CommandArgs = GoToVendorRatingsDisplay,
				ConfirmCommand = false,
				ImageUrl = IconLinks.Rating,
				Name = "Go To Vendor Ratings",
				IsLink = false,
				NavigationUrl = string.Empty
			};
			var goToVendorCommunications = new ToolbarMoreAction
			{
				ConfirmCommand = false,
				ImageUrl = IconLinks.Connect,
				Name = "Go To Vendor Communications",
				IsLink = true,
				NavigationUrl = string.Format("{0}?{1}={2}", VendorCommunicationView.PageAddress, WebApplicationConstants.TransferNumber, hidCommunicationId.Value.UrlTextEncrypt()),
                OpenInNewWindow = true
			};
            var showVendorPerformanceStats = new ToolbarMoreAction
            {
                CommandArgs = ShowVendorPerformanceStats,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Stats,
                Name = "Show Vendor Performance Stats",
                IsLink = false,
                NavigationUrl = string.Empty
            };

			var moreActions = new List<ToolbarMoreAction>();

			if (hidHasRating.Value.ToBoolean() && ActiveUser.HasAccessTo(ViewCode.VendorRating))
				moreActions.Add(goToVendorRatingsMenu);

			if (hidCommunicationId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.VendorCommunication))
				moreActions.Add(goToVendorCommunications);

            if(hidVendorId.Value.ToLong() != default(long))
                moreActions.Add(showVendorPerformanceStats);

			// The 'More' menu toolbar option is hidden if there are no options to display inside the 'More' toolbar option
			memberToolBar.ShowMore = moreActions.Any();

			return moreActions;
		}

		private void SetEditStatus(bool enabled)
		{
			pnlDetails.Enabled = enabled;
			pnlLocations.Enabled = enabled;
			pnlServices.Enabled = enabled;
			pnlEquipmentTypes.Enabled = enabled;
			pnlVendorInsurance.Enabled = enabled;
			pnlNoServiceDays.Enabled = enabled;
		    pnlVendorPackageCustomMapping.Enabled = enabled;
			memberToolBar.EnableSave = enabled;

			pnlDocuments.Enabled = ((hidVendorId.Value.ToLong() != default(long)) && enabled); //Vendor must exist before documents can be added.
			tabDocuments.Visible = hidVendorId.Value.ToLong() != default(long);


			fupLogoImage.Enabled = enabled && hidVendorId.Value.ToLong() != default(long);

			litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;
		}


	    private void ShowVendorRatingsDisplay()
		{
			lstVendorRatings.DataSource = (new Vendor(hidVendorId.Value.ToLong()).Ratings);
			lstVendorRatings.DataBind();

			vendorRatingsDisplay.Visible = true;
			pnlDimScreen.Visible = vendorRatingsDisplay.Visible;
		}


		protected bool ValueIsNotEmpty(object value)
		{
			return value != null && !string.IsNullOrEmpty(value.ToString());
		}



		protected void Page_Load(object sender, EventArgs e)
		{
			new VendorHandler(this).Initialize();

			memberToolBar.ShowSave = Access.Modify;
			memberToolBar.ShowDelete = Access.Remove;
			memberToolBar.ShowNew = Access.Modify;
			memberToolBar.ShowEdit = Access.Modify;

			vendorFinder.OpenForEditEnabled = Access.Modify;

			memberToolBar.LoadAdditionalActions(ToolbarMoreActions());

			if (IsPostBack) return;

			aceUser.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.ActiveUser, ActiveUser.Id.GetString() } });

			if (Loading != null)
				Loading(this, new EventArgs());

			chkExludeFromAvailableLoadsBlast.Text += string.Format(" (i.e. contacts of type - {0})",
																   ActiveUser.Tenant.AvailableLoadsContactType.Code);

			if (Session[WebApplicationConstants.TransferVendorId] != null)
			{
				ProcessTransferredRequest(new Vendor(Session[WebApplicationConstants.TransferVendorId].ToLong()));
				Session[WebApplicationConstants.TransferVendorId] = null;
			}

            if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.TransferNumber]))
                ProcessTransferredRequest(new Vendor(Request.QueryString[WebApplicationConstants.TransferNumber].UrlTextDecrypt().ToLong()));

			SetEditStatus(false);
		}



		protected void OnNewClicked(object sender, EventArgs e)
		{
			litErrorMessages.Text = string.Empty;

			LoadVendor(new Vendor { DateCreated = DateTime.Now, LastAuditDate = DateTime.Now, LastAuditedBy = ActiveUser });
			DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
			SetEditStatus(true);
		}

		protected void OnEditClicked(object sender, EventArgs e)
		{
			var vendor = new Vendor(hidVendorId.Value.ToLong(), false);

			if (Lock == null || vendor.IsNew) return;

			SetEditStatus(true);
			memberToolBar.ShowUnlock = true;
			Lock(this, new ViewEventArgs<Vendor>(vendor));

			LoadVendor(vendor);
		}

		protected void OnUnlockClicked(object sender, EventArgs e)
		{
			var vendor = new Vendor(hidVendorId.Value.ToLong(), false);
			if (UnLock != null && !vendor.IsNew)
			{
				SetEditStatus(false);
				memberToolBar.ShowUnlock = false;
				UnLock(this, new ViewEventArgs<Vendor>(vendor));
			}
		}

		protected void OnSaveClicked(object sender, EventArgs e)
		{
			var vendorId = hidVendorId.Value.ToLong();
			var vendor = new Vendor(vendorId, vendorId != default(long));

			if (vendor.IsNew)
			{
				vendor.TenantId = ActiveUser.TenantId;
				vendor.DateCreated = DateTime.Now;
			}
			else
			{
				vendor.LoadCollections();
			}

			UpdateDetails(vendor);
			vendor.Locations = RetrieveViewLocations(vendor);
			UpdateServices(vendor);
			UpdateEquipmentTypes(vendor);
			vendor.Insurances = RetrieveViewInsurances(vendor);
			vendor.VendorPackageCustomMappings = RetrieveViewPackageMappings(vendor);
			UpdateNoServiceDays(vendor);
			UpdateDocumentsFromView(vendor);

			UpdateVendorLogo(vendor);

			hidFlag.Value = SaveFlag;

			if (Save != null)
				Save(this, new ViewEventArgs<Vendor>(vendor));

			if (LoadAuditLog != null)
				LoadAuditLog(this, new ViewEventArgs<Vendor>(vendor));
		}

		protected void OnDeleteClicked(object sender, EventArgs e)
		{
			var vendor = new Vendor(hidVendorId.Value.ToLong(), false);

			hidFlag.Value = DeleteFlag;

			if (Delete != null)
				Delete(this, new ViewEventArgs<Vendor>(vendor));

			vendorFinder.Reset();
		}

		protected void OnFindClicked(object sender, EventArgs e)
		{
			vendorFinder.Visible = true;
		}

		protected void OnToolbarCommandClicked(object sender, ViewEventArgs<string> e)
		{
			switch (e.Argument)
			{
				case GoToVendorRatingsDisplay:
					ShowVendorRatingsDisplay();
					break;
                case ShowVendorPerformanceStats:
                    vpscVendorPerformance.Visible = true;
                    vpscVendorPerformance.LoadVendorPerformanceSummary(hidVendorId.Value.ToLong());
			        break;
			}
		}



		protected void OnClearLogoClicked(object sender, EventArgs e)
		{
			hidFilesToDelete.Value += string.Format("{0};", hidLogoUrl.Value);
			hidLogoUrl.Value = string.Empty;
			imgLogo.ImageUrl = string.Empty;
			imgLogo.AlternateText = string.Empty;
			imgLogo.Visible = false;
		}


		protected void OnVendorFinderItemSelected(object sender, ViewEventArgs<Vendor> e)
		{
			litErrorMessages.Text = string.Empty;

			if (hidVendorId.Value.ToLong() != default(long))
			{
				var oldVendor = new Vendor(hidVendorId.Value.ToLong(), false);
				if (UnLock != null)
					UnLock(this, new ViewEventArgs<Vendor>(oldVendor));
			}

			var vendor = e.Argument;

			LoadVendor(vendor);

			vendorFinder.Visible = false;

			if (vendorFinder.OpenForEdit && Lock != null)
			{
				SetEditStatus(true);
				memberToolBar.ShowUnlock = true;
				Lock(this, new ViewEventArgs<Vendor>(vendor));
			}
			else
			{
				SetEditStatus(false);
			}

			if (LoadAuditLog != null) LoadAuditLog(this, new ViewEventArgs<Vendor>(vendor));
		}

		protected void OnVendorFinderSelectionCancel(object sender, EventArgs e)
		{
			vendorFinder.Visible = false;
		}


		protected void OnVendorRatingsDisplaySelectionCancelled(object sender, EventArgs e)
		{
			vendorRatingsDisplay.Visible = false;
			pnlDimScreen.Visible = vendorRatingsDisplay.Visible;

			lstVendorRatings.DataSource = new List<object>();
			lstVendorRatings.DataBind();
		}


		protected void OnAddLocationClicked(object sender, EventArgs e)
		{
			var locations = RetrieveViewLocations(new Vendor(hidVendorId.Value.ToLong(), false));
			locations.Add(new VendorLocation());
			lstLocations.DataSource = locations;
			lstLocations.DataBind();
		}

		protected void OnDeleteLocation(object sender, ViewEventArgs<int> e)
		{
			var locations = RetrieveViewLocations(new Vendor(hidVendorId.Value.ToLong(), false));
			locations.RemoveAt(e.Argument);
			lstLocations.DataSource = locations;
			lstLocations.DataBind();
			tabLocations.HeaderText = locations.BuildTabCount(LocationsHeader);
		}

		protected void BindLocationLineItem(object sender, ListViewItemEventArgs e)
		{
			var item = e.Item as ListViewDataItem;
			if (item == null) return;
			var ic = item.FindControl("llicLocation").ToLocationListingInputControl();
			var location = item.DataItem as VendorLocation;
			if (location == null) return;
			ic.LoadLocation(location);
		}



		protected void OnServiceFinderMultiItemSelected(object sender, ViewEventArgs<List<ServiceViewSearchDto>> e)
		{
			var vServices = lstServices.Items
				.Select(i => new
				{
					Id = i.FindControl("hidServiceId").ToCustomHiddenField().Value,
					Code = i.FindControl("litServiceCode").ToLiteral().Text,
					Description = i.FindControl("litServiceDescription").ToLiteral().Text,
					Category = i.FindControl("litServiceCategory").ToLiteral().Text.ToInt().ToEnum<ServiceCategory>(),
					ChargeCodeId = i.FindControl("hidServiceChargeCodeId").ToCustomHiddenField().Value.ToLong(),
					ChargeCode = i.FindControl("litServiceChargeCode").ToLiteral().Text,
					ChargeCodeDescription = i.FindControl("litServiceChargeCodeDescription").ToLiteral().Text
				})
				.ToList();
			vServices.AddRange(e.Argument.Select(t => new { Id = t.Id.ToString(), t.Code, t.Description, t.Category, t.ChargeCodeId, t.ChargeCode, t.ChargeCodeDescription })
							.Where(t => !vServices.Select(vt => vt.Id).Contains(t.Id)));
			lstServices.DataSource = vServices.OrderBy(s => s.Code);
			lstServices.DataBind();
			tabServices.HeaderText = vServices.BuildTabCount(ServicesHeader);
			serviceFinder.Visible = false;
		}

		protected void OnServiceFinderSelectionCancelled(object sender, EventArgs e)
		{
			serviceFinder.Visible = false;
		}

		protected void OnAddServiceClicked(object sender, EventArgs e)
		{
			serviceFinder.Visible = true;
		}

		protected void OnClearServicesClicked(object sender, EventArgs e)
		{
			lstServices.DataSource = new List<object>();
			lstServices.DataBind();
			tabServices.HeaderText = ServicesHeader;
            athtuTabUpdater.SetForUpdate(tabServices.ClientID, ServicesHeader);
		}

		protected void OnDeleteServiceClicked(object sender, ImageClickEventArgs e)
		{
			var button = (ImageButton)sender;
			var idToRemove = button.Parent.FindControl("hidServiceId").ToCustomHiddenField().Value;
			var service = lstServices
				.Items
				.Select(item => new
					{
						Id = item.FindControl("hidServiceId").ToCustomHiddenField().Value,
						Code = item.FindControl("litServiceCode").ToLiteral().Text,
						Description = item.FindControl("litServiceDescription").ToLiteral().Text,
						Category = item.FindControl("litServiceCategory").ToLiteral().Text.ToInt().ToEnum<ServiceCategory>(),
						ChargeCodeId = item.FindControl("hidServiceChargeCodeId").ToCustomHiddenField().Value.ToLong(),
						ChargeCode = item.FindControl("litServiceChargeCode").ToLiteral().Text,
						ChargeCodeDescription = item.FindControl("litServiceChargeCodeDescription").ToLiteral().Text
					})
				.Where(u => u.Id != idToRemove)
				.ToList();

			lstServices.DataSource = service.OrderBy(s => s.Code);
			lstServices.DataBind();
			tabServices.HeaderText = service.BuildTabCount(ServicesHeader);
            athtuTabUpdater.SetForUpdate(tabServices.ClientID, service.BuildTabCount(ServicesHeader));
		}


		protected void OnEquipmentTypeFinderMultiItemSelected(object sender, ViewEventArgs<List<EquipmentType>> e)
		{
			var vEquipmentTypes = lstEquipmentTypes
				.Items
				.Select(i => new
					{
						Id = i.FindControl("hidEquipmentTypeId").ToCustomHiddenField().Value,
						Code = i.FindControl("litEquipmentTypeCode").ToLiteral().Text,
						TypeName = i.FindControl("litEquipmentTypeTypeName").ToLiteral().Text,
						Group = i.FindControl("hidEquipmentTypeGroup").ToCustomHiddenField().Value.ToEnum<EquipmentTypeGroup>(),
                        DatEquipmentType = i.FindControl("hidDatEquipmentType").ToCustomHiddenField().Value.ToEnum<DatEquipmentType>(),
                        Active = i.FindControl("hidEquipmentTypeActive").ToCustomHiddenField().Value.ToBoolean(),
					})
				.ToList();
			vEquipmentTypes.AddRange(e.Argument
										.Select(t => new { Id = t.Id.ToString(), t.Code, t.TypeName, t.Group, t.DatEquipmentType, t.Active })
										.Where(t => !vEquipmentTypes.Select(vt => vt.Id).Contains(t.Id)));
			lstEquipmentTypes.DataSource = vEquipmentTypes.OrderBy(t => t.TypeName);
			lstEquipmentTypes.DataBind();
			tabEquipmentTypes.HeaderText = vEquipmentTypes.BuildTabCount(EquipmentHeader);
			equipmentTypeFinder.Visible = false;
		}

		protected void OnEquipmentTypeFinderSelectionCancelled(object sender, EventArgs e)
		{
			equipmentTypeFinder.Visible = false;
		}

		protected void OnAddEquipmentTypeClicked(object sender, EventArgs e)
		{
			equipmentTypeFinder.Visible = true;
		}

		protected void OnClearEquipmentTypesClicked(object sender, EventArgs e)
		{
			lstEquipmentTypes.DataSource = new List<object>();
			lstEquipmentTypes.DataBind();
			tabEquipmentTypes.HeaderText = EquipmentHeader;
            athtuTabUpdater.SetForUpdate(tabEquipmentTypes.ClientID, EquipmentHeader);
		}

		protected void OnDeleteEquipmentTypeClicked(object sender, ImageClickEventArgs e)
		{
			var button = (ImageButton)sender;
			var idToRemove = button.Parent.FindControl("hidEquipmentTypeId").ToCustomHiddenField().Value;
			var vEquipmentType = lstEquipmentTypes
				.Items
				.Select(item => new
					{
						Id = item.FindControl("hidEquipmentTypeId").ToCustomHiddenField().Value,
						Code = item.FindControl("litEquipmentTypeCode").ToLiteral().Text,
						TypeName = item.FindControl("litEquipmentTypeTypeName").ToLiteral().Text,
						Group = item.FindControl("hidEquipmentTypeGroup").ToCustomHiddenField().Value.ToEnum<EquipmentTypeGroup>(),
                        DatEquipmentType = item.FindControl("hidDatEquipmentType").ToCustomHiddenField().Value.ToEnum<DatEquipmentType>(),
                        Active = item.FindControl("hidEquipmentTypeActive").ToCustomHiddenField().Value.ToBoolean(),
					})
				.Where(u => u.Id != idToRemove)
				.ToList();
			lstEquipmentTypes.DataSource = vEquipmentType.OrderBy(t => t.TypeName);
			lstEquipmentTypes.DataBind();
			tabEquipmentTypes.HeaderText = vEquipmentType.BuildTabCount(EquipmentHeader);
            athtuTabUpdater.SetForUpdate(tabEquipmentTypes.ClientID, vEquipmentType.BuildTabCount(EquipmentHeader));
		}



		protected void OnNoServiceDayFinderMultiItemSelected(object sender, ViewEventArgs<List<NoServiceDay>> e)
		{
			var vNoServiceDays = lstNoServiceDays
				.Items
				.Select(i => new
					{
						Id = i.FindControl("hidNoServiceDayId").ToCustomHiddenField().Value,
						DateOfNoService = i.FindControl("litNoServiceDayDate").ToLiteral().Text.ToDateTime(),
						Description = i.FindControl("litNoServiceDayDescription").ToLiteral().Text,
					})
				.ToList();
			vNoServiceDays.AddRange(e.Argument.Select(t => new { Id = t.Id.ToString(), t.DateOfNoService, t.Description })
							.Where(t => !vNoServiceDays.Select(vt => vt.Id).Contains(t.Id)));
			lstNoServiceDays.DataSource = vNoServiceDays.OrderBy(t => t.DateOfNoService);
			lstNoServiceDays.DataBind();
			tabNoServiceDays.HeaderText = vNoServiceDays.BuildTabCount(NoServiceDaysHeader);
			noServiceDayFinder.Visible = false;
		}

		protected void OnNoServiceDayFinderSelectionCancelled(object sender, EventArgs e)
		{
			noServiceDayFinder.Visible = false;
		}

		protected void OnAddNoServiceDayClicked(object sender, EventArgs e)
		{
			noServiceDayFinder.Visible = true;
		}

		protected void OnClearNoServiceDayClicked(object sender, EventArgs e)
		{
			lstNoServiceDays.DataSource = new List<object>();
			lstNoServiceDays.DataBind();
			tabNoServiceDays.HeaderText = NoServiceDaysHeader;
            athtuTabUpdater.SetForUpdate(tabNoServiceDays.ClientID, NoServiceDaysHeader);
		}

		protected void OnDeleteNoServiceDayClicked(object sender, ImageClickEventArgs e)
		{
			var button = (ImageButton)sender;
			var idToRemove = button.Parent.FindControl("hidNoServiceDayId").ToCustomHiddenField().Value;
			var vNoServiceDay = lstNoServiceDays
				.Items
				.Select(i => new
					{
						Id = i.FindControl("hidNoServiceDayId").ToCustomHiddenField().Value,
						DateOfNoService = i.FindControl("litNoServiceDayDate").ToLiteral().Text.ToDateTime(),
						Description = i.FindControl("litNoServiceDayDescription").ToLiteral().Text,
					})
				.Where(u => u.Id != idToRemove)
				.ToList();
			lstNoServiceDays.DataSource = vNoServiceDay.OrderBy(t => t.DateOfNoService);
			lstNoServiceDays.DataBind();
			tabNoServiceDays.HeaderText = vNoServiceDay.BuildTabCount(NoServiceDaysHeader);
            athtuTabUpdater.SetForUpdate(tabNoServiceDays.ClientID, vNoServiceDay.BuildTabCount(NoServiceDaysHeader));
		}


		protected void OnAddVendorInsuranceClicked(object sender, EventArgs e)
		{
			var vInsurance = lstVendorInsurance
				.Items
				.Select(item => new
				{
					Id = item.FindControl("hidVendorInsuranceId").ToCustomHiddenField().Value,
					CarrierName = item.FindControl("txtVendorInsuranceCarrierName").ToTextBox().Text,
					PolicyNumber = item.FindControl("txtVendorInsurancePolicyNumber").ToTextBox().Text,
					CertificateHolder = item.FindControl("txtVendorInsuranceCertificateHolder").ToTextBox().Text,
					CoverageAmount = item.FindControl("txtVendorInsuranceCoverageAmount").ToTextBox().Text.ToDecimal(),
					Required = item.FindControl("chkVendorInsuranceRequired").ToAltUniformCheckBox().Checked,
					EffectiveDate = item.FindControl("txtVendorInsuranceEffectiveDate").ToTextBox().Text.ToDateTime(),
					ExpirationDate = item.FindControl("txtVendorInsuranceExpirationDate").ToTextBox().Text.ToDateTime(),
					InsuranceTypeId = item.FindControl("ddlVendorInsuranceType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
					SpecialInstruction = item.FindControl("txtVendorInsuranceSpecialInstruction").ToTextBox().Text,
				})
				.ToList();
			
			// add new
			vInsurance.Add(new
				{
					Id = default(long).GetString(),
					CarrierName = string.Empty,
					PolicyNumber = string.Empty,
					CertificateHolder = string.Empty,
					CoverageAmount = 0m,
					Required = false,
					EffectiveDate = DateTime.Now,
					ExpirationDate = DateTime.Now.AddYears(1),
					InsuranceTypeId = default(long),
					SpecialInstruction = string.Empty,
				});

			lstVendorInsurance.DataSource = vInsurance;
			lstVendorInsurance.DataBind();
			tabVendorInsurance.HeaderText = vInsurance.BuildTabCount(VendorInsuranceHeader);
		}

		protected void OnClearVendorInsuranceClicked(object sender, EventArgs e)
		{
			lstVendorInsurance.DataSource = new List<object>();
			lstVendorInsurance.DataBind();
			tabVendorInsurance.HeaderText = VendorInsuranceHeader;
		}

		protected void OnDeleteVendorInsuranceClicked(object sender, ImageClickEventArgs e)
		{
			var button = (ImageButton)sender;

			var vInsurance = lstVendorInsurance
				.Items
				.Select(item => new
					{
						Id = item.FindControl("hidVendorInsuranceId").ToCustomHiddenField().Value,
						CarrierName = item.FindControl("txtVendorInsuranceCarrierName").ToTextBox().Text,
						PolicyNumber = item.FindControl("txtVendorInsurancePolicyNumber").ToTextBox().Text,
						CertificateHolder = item.FindControl("txtVendorInsuranceCertificateHolder").ToTextBox().Text,
						CoverageAmount = item.FindControl("txtVendorInsuranceCoverageAmount").ToTextBox().Text.ToDecimal(),
						Required = item.FindControl("chkVendorInsuranceRequired").ToAltUniformCheckBox().Checked,
						EffectiveDate = item.FindControl("txtVendorInsuranceEffectiveDate").ToTextBox().Text.ToDateTime(),
						ExpirationDate = item.FindControl("txtVendorInsuranceExpirationDate").ToTextBox().Text.ToDateTime(),
						InsuranceTypeId = item.FindControl("ddlVendorInsuranceType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
						SpecialInstruction = item.FindControl("txtVendorInsuranceSpecialInstruction").ToTextBox().Text,
					})
				.ToList();		

			vInsurance.RemoveAt(button.Parent.FindControl("hidVendorInsuranceEditItemIndex").ToCustomHiddenField().Value.ToInt());

			lstVendorInsurance.DataSource = vInsurance;
			lstVendorInsurance.DataBind();
			tabVendorInsurance.HeaderText = vInsurance.BuildTabCount(VendorInsuranceHeader);
		}



		protected void OnAddVendorPackageCustomMappingClicked(object sender, EventArgs e)
		{
			var vPackages = lstVendorPackageCustomMapping
				.Items
				.Select(item => new
					{
						Id = item.FindControl("hidVendorPackageCustomMappingId").ToCustomHiddenField().Value,
						VendorCode = item.FindControl("txtVendorPackageCustomMappingVendorCode").ToTextBox().Text,
						PackageTypeId = item.FindControl("ddlPackageType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
					})
				.ToList();

			// add new
			vPackages.Add(new
				{
					Id = default(long).GetString(),
					VendorCode = string.Empty,
					PackageTypeId = default(long),
				});

			lstVendorPackageCustomMapping.DataSource = vPackages;
			lstVendorPackageCustomMapping.DataBind();
			tabVendorPackageCustomMapping.HeaderText = vPackages.BuildTabCount(VendorPackageCustomMappingHeader);
		}

		protected void OnClearVendorPackageCustomMappingClicked(object sender, EventArgs e)
		{
			lstVendorPackageCustomMapping.DataSource = new List<object>();
			lstVendorPackageCustomMapping.DataBind();
			tabVendorPackageCustomMapping.HeaderText = VendorPackageCustomMappingHeader;
		}

		protected void OnDeleteVendorPackageCustomMappingClicked(object sender, ImageClickEventArgs e)
		{
			var button = (ImageButton)sender;

			var vPackages = lstVendorPackageCustomMapping
				.Items
				.Select(item => new
				{
					Id = item.FindControl("hidVendorPackageCustomMappingId").ToCustomHiddenField().Value,
					VendorCode = item.FindControl("txtVendorPackageCustomMappingVendorCode").ToTextBox().Text,
					PackageTypeId = item.FindControl("ddlPackageType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
				})
				.ToList();

			vPackages.RemoveAt(button.Parent.FindControl("hidVendorPackageCustomMappingEditItemIndex").ToCustomHiddenField().Value.ToInt());

			lstVendorPackageCustomMapping.DataSource = vPackages;
			lstVendorPackageCustomMapping.DataBind();
			tabVendorPackageCustomMapping.HeaderText = vPackages.BuildTabCount(VendorPackageCustomMappingHeader);
		}





		private void LoadDocumentForEdit(VendorDocument document)
		{
			chkDocumentIsInternal.Checked = document.IsInternal;
			txtDocumentName.Text = document.Name;
			txtDocumentDescription.Text = document.Description;
			ddlDocumentTag.SelectedValue = document.DocumentTagId.GetString();
			txtLocationPath.Text = GetLocationFileName(document.LocationPath);
			hidLocationPath.Value = document.LocationPath;
		}

		protected string GetLocationFileName(object relativePath)
		{
			return relativePath == null ? string.Empty : Server.GetFileName(relativePath.ToString());
		}

		protected void OnAddDocumentClicked(object sender, EventArgs e)
		{
			hidEditingIndex.Value = (-1).ToString();
			LoadDocumentForEdit(new VendorDocument());
			pnlEditDocument.Visible = true;
			pnlDimScreen.Visible = true;
		}

		protected void OnClearDocumentsClicked(object sender, EventArgs e)
		{
			var paths = lstDocuments.Items.Select(i => new { LocationPath = i.FindControl("hidLocationPath").ToCustomHiddenField().Value, }).ToList();
			foreach (var path in paths)
				hidFilesToDelete.Value += string.Format("{0};", path);

			lstDocuments.DataSource = new List<Object>();
			lstDocuments.DataBind();
			tabDocuments.HeaderText = DocumentsHeader;
		}

		protected void OnEditDocumentClicked(object sender, ImageClickEventArgs e)
		{
			var control = ((ImageButton)sender).Parent;

			hidEditingIndex.Value = control.FindControl("hidDocumentIndex").ToCustomHiddenField().Value;

			var document = new VendorDocument
			{
				Name = control.FindControl("litDocumentName").ToLiteral().Text,
				Description = control.FindControl("litDocumentDescription").ToLiteral().Text,
				DocumentTagId = control.FindControl("hidDocumentTagId").ToCustomHiddenField().Value.ToLong(),
				LocationPath = control.FindControl("hidLocationPath").ToCustomHiddenField().Value,
				IsInternal = control.FindControl("hidIsInternal").ToCustomHiddenField().Value.ToBoolean(),
			};

			LoadDocumentForEdit(document);

			pnlEditDocument.Visible = true;
			pnlDimScreen.Visible = true;
		}

		protected void OnDeleteDocumentClicked(object sender, ImageClickEventArgs e)
		{
			var imageButton = (ImageButton)sender;

			var documents = lstDocuments
				.Items
				.Select(i => new
					{
						Id = i.FindControl("hidDocumentId").ToCustomHiddenField().Value.ToLong(),
						Name = i.FindControl("litDocumentName").ToLiteral().Text,
						Description = i.FindControl("litDocumentDescription").ToLiteral().Text,
						DocumentTagId = i.FindControl("hidDocumentTagId").ToCustomHiddenField().Value.ToLong(),
						LocationPath = i.FindControl("hidLocationPath").ToCustomHiddenField().Value,
						IsInternal = i.FindControl("hidIsInternal").ToCustomHiddenField().Value.ToBoolean(),
					})
				.ToList();

			var index = imageButton.Parent.FindControl("hidDocumentIndex").ToCustomHiddenField().Value.ToInt();
			hidFilesToDelete.Value += string.Format("{0};", documents[index].LocationPath);
			documents.RemoveAt(index);

			lstDocuments.DataSource = documents;
			lstDocuments.DataBind();
			tabDocuments.HeaderText = documents.BuildTabCount(DocumentsHeader);
		}

		protected void OnCloseEditDocumentClicked(object sender, EventArgs eventArgs)
		{
			pnlEditDocument.Visible = false;
			pnlDimScreen.Visible = false;
		}

		protected void OnEditDocumentDoneClicked(object sender, EventArgs e)
		{
			var virtualPath = WebApplicationSettings.VendorFolder(ActiveUser.TenantId, hidVendorId.Value.ToLong());
			var physicalPath = Server.MapPath(virtualPath);

			var documents = lstDocuments
				.Items
				.Select(i => new
					{
						Id = i.FindControl("hidDocumentId").ToCustomHiddenField().Value.ToLong(),
						Name = i.FindControl("litDocumentName").ToLiteral().Text,
						Description = i.FindControl("litDocumentDescription").ToLiteral().Text,
						DocumentTagId = i.FindControl("hidDocumentTagId").ToCustomHiddenField().Value,
						LocationPath = i.FindControl("hidLocationPath").ToCustomHiddenField().Value,
						IsInternal = i.FindControl("hidIsInternal").ToCustomHiddenField().Value.ToBoolean(),
					})
				.ToList();

			var documentIndex = hidEditingIndex.Value.ToInt();

			var documentId = default(long);
			if (documentIndex != -1)
			{
				documentId = documents[documentIndex].Id;
				documents.RemoveAt(documentIndex);
			}

			var locationPath = hidLocationPath.Value; // this should actually be a hidden field holding the existing path during edit

			documents.Insert(0, new
			{
				Id = documentId,
				Name = txtDocumentName.Text,
				Description = txtDocumentDescription.Text,
				DocumentTagId = ddlDocumentTag.SelectedValue,
				LocationPath = locationPath,
				IsInternal = chkDocumentIsInternal.Checked,
			});

			if (fupLocationPath.HasFile)
			{
				// ensure directory is present
				if (!Directory.Exists(physicalPath)) Directory.CreateDirectory(physicalPath);

				//ensure unique filename
				var fi = new FileInfo(Server.MakeUniqueFilename(Path.Combine(physicalPath, fupLocationPath.FileName), true));

				// save file
				fupLocationPath.WriteToFile(physicalPath, fi.Name, Server.MapPath(documents[0].LocationPath));

				// remove updated record
				documents.RemoveAt(0);

				// re-add record with new file
				documents.Insert(0, new
				{
					Id = documentId,
					Name = txtDocumentName.Text,
					Description = txtDocumentDescription.Text,
					DocumentTagId = ddlDocumentTag.SelectedValue,
					LocationPath = virtualPath + fi.Name,
					IsInternal = chkDocumentIsInternal.Checked
				});
			}

			lstDocuments.DataSource = documents.OrderBy(d => d.Name);
			lstDocuments.DataBind();
			tabDocuments.HeaderText = documents.BuildTabCount(DocumentsHeader);

			pnlEditDocument.Visible = false;
			pnlDimScreen.Visible = false;
		}

		protected void OnLocationPathClicked(object sender, EventArgs e)
		{
			var button = (LinkButton)sender;
			var path = button.Parent.FindControl("hidLocationPath").ToCustomHiddenField().Value;
			Response.Export(Server.ReadFromFile(path), button.Text);
		}



		protected void OnVendorServiceRepUsernameTextChanged(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(txtVendorServiceRepUsername.Text))
			{
				DisplayVendorServiceRep(null);
				return;
			}

			if (VendorServiceRepSearch != null)
				VendorServiceRepSearch(this, new ViewEventArgs<string>(txtVendorServiceRepUsername.Text.RetrieveSearchCodeFromAutoCompleteString()));
		}

		protected void OnFindVendorServiceRepUserClicked(object sender, ImageClickEventArgs e)
		{
			userFinder.Visible = true;
		}

		protected void OnUserFinderItemSelected(object sender, ViewEventArgs<User> e)
		{

			DisplayVendorServiceRep(e.Argument);
			hidFlag.Value = string.Empty;
			userFinder.Visible = false;
		}

		protected void OnUserFinderSelectionCancelled(object sender, EventArgs e)
		{
			userFinder.Visible = false;
			hidFlag.Value = string.Empty;
		}


		protected void OnUpdateAuditClicked(object sender, EventArgs e)
		{
			DisplayLastAuditInformation(ActiveUser, DateTime.Now);
		}


	    protected void OnVendorPerformanceClose(object sender, EventArgs e)
	    {
	        vpscVendorPerformance.Visible = false;
	    }
	}
}