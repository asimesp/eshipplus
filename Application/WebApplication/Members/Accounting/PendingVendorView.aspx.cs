﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting
{
    public partial class PendingVendorView : MemberPageBase, IPendingVendorView, ILocationCollectionResource
    {
        private const string NewPendingVendor = "NewPendingVendor";

        private const string LocationsHeader = "Locations";
        private const string ServicesHeader = "Services";
        private const string EquipmentHeader = "Equipment Types";
        private const string PendingVendorInsuranceHeader = "Pending Vendor Insurance";
        private const string DocumentsHeader = "Documents";

        private const string SaveFlag = "S";
        private const string DeleteFlag = "D";

        private bool _moveFilesFromTempLocation;
        private bool _transferToVendorOnSuccessfulApproval;
        private Vendor _savedVendor;

        public static string PageAddress { get { return "~/Members/Accounting/PendingVendorView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.PendingVendor; } }

        public bool CreateAndSaveVendor { get { return _transferToVendorOnSuccessfulApproval; } }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.FinanceBlue; }
        }


        public Dictionary<int, string> BrokerReferenceTypes
        {
            set
            {
                ddlBrokerReferenceNumberType.DataSource = value ?? new Dictionary<int, string>();
                ddlBrokerReferenceNumberType.DataBind();
            }
        }

        public List<ViewListItem> ContactTypeCollection
        {
            get
            {
                var viewListItems = ProcessorVars.RegistryCache[ActiveUser.TenantId]
                    .ContactTypes
                    .OrderBy(c => c.Description)
                    .Select(t => new ViewListItem(t.Code, t.Id.ToString()))
                    .ToList();
                viewListItems.Insert(0, new ViewListItem(WebApplicationConstants.ChooseOne, default(long).ToString()));
                return viewListItems;
            }
        }

        public List<ViewListItem> CountryCollection
        {
            get
            {
                var viewListItems = ProcessorVars.RegistryCache.Countries
                    .Values
                    .OrderByDescending(t => t.SortWeight)
                    .ThenBy(t => t.Name)
                    .Select(c => new ViewListItem(c.Name, c.Id.ToString()))
                    .ToList();
                viewListItems.Insert(0, new ViewListItem(WebApplicationConstants.ChooseOne, default(long).ToString()));
                return viewListItems;
            }
        }
        

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<PendingVendor>> Save;
        public event EventHandler<ViewEventArgs<Vendor>> SaveVendor;
        public event EventHandler<ViewEventArgs<PendingVendor>> Delete;
        public event EventHandler<ViewEventArgs<PendingVendor>> Lock;
        public event EventHandler<ViewEventArgs<PendingVendor>> UnLock;
        public event EventHandler<ViewEventArgs<string>> PendingVendorServiceRepSearch;
        public event EventHandler<ViewEventArgs<PendingVendor>> LoadAuditLog;


        public string CopyFileForVendor(Vendor vendor, string sourceFileVirtualPath)
        {
            var absPath = Server.MapPath(sourceFileVirtualPath);
            var info = new FileInfo(absPath);
            var vendorFolder = WebApplicationSettings.VendorFolder(vendor.TenantId, vendor.Id);
            if (!Directory.Exists(Server.MapPath(vendorFolder))) Directory.CreateDirectory(Server.MapPath(vendorFolder));
            var newVirtualPath = vendorFolder + info.Name;
            File.Copy(absPath, Server.MapPath(newVirtualPath));
            return newVirtualPath;
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }


        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == SaveFlag)
            {
                // clean up file cleared out!
                if (!string.IsNullOrEmpty(hidFilesToDelete.Value))
                {
                    Server.RemoveFiles(hidFilesToDelete.Value.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries));
                    hidFilesToDelete.Value = string.Empty;
                }

                var pendingVendor = new PendingVendor(hidPendingVendorId.Value.ToLong(), false);

                // check for file moves
                // variable set in save method for new files load when record is new record.
                if (_moveFilesFromTempLocation)
                {
                    _moveFilesFromTempLocation = false; // must set to false first to avoid processing load with next save from ProcessFile... method.
                    pendingVendor.LoadCollections(); // require all connections reloaded
                    ProcessFileMoveFromTempLocation(pendingVendor);
                    return;
                }

                // unlock and return to read-only
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<PendingVendor>(pendingVendor));

                // send notifications
                if (!string.IsNullOrEmpty(hidNotificationsRequired.Value))
                {
                    ProcessNotifications(pendingVendor);
                    hidNotificationsRequired.Value = string.Empty;
                }

                SetEditStatus(false);
                memberToolBar.ShowUnlock = false;
            }

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;

            litErrorMessages.Text = messages.HasErrors()
                                        ? string.Join(WebApplicationConstants.HtmlBreak,
                                                      messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
                                                        Select(m => m.Message).ToArray())
                                        : string.Empty;
        }

        public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs)
        {
            auditLogs.Load(logs);
        }

        public void FailedLock(Lock @lock)
        {
            memberToolBar.ShowNew = false;
            memberToolBar.ShowSave = false;
            memberToolBar.ShowDelete = false;
            memberToolBar.ShowUnlock = false;

            SetEditStatus(false);

            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void Set(PendingVendor pendingVendor)
        {
            if (pendingVendor.IsNew && hidFlag.Value == DeleteFlag)
            {
                Server.RemoveDirectory(WebApplicationSettings.PendingVendorFolder(ActiveUser.TenantId, hidPendingVendorId.Value.ToLong()));
                hidFlag.Value = string.Empty;
            }
            LoadPendingVendor(pendingVendor); // if previously new, reload
            SetEditStatus(!pendingVendor.IsNew);
        }

        public void Set(Vendor vendor)
        {
            _savedVendor = vendor;
        }


        private List<PendingVendorLocation> RetrieveViewLocations(PendingVendor pendingVendor)
        {
            var locations = new List<PendingVendorLocation>();
            foreach (var item in lstLocations.Items)
            {
                var control = item.FindControl("llicLocation").ToLocationListingInputControl();
                if (control == null) continue;
                var locationId = control.RetrieveLocationId();
                var location = new PendingVendorLocation(locationId, locationId != default(long));
                if (location.IsNew)
                {
                    location.DateCreated = DateTime.Now;
                    location.Tenant = pendingVendor.Tenant;
                    location.PendingVendor = pendingVendor;
                }
                control.UpdateLocation(location);

                locations.Add(location);
            }
            return locations;
        }

        private List<PendingVendorInsurance> RetrieveViewInsurances(PendingVendor pendingVendor)
        {
            var insurances = new List<PendingVendorInsurance>();

            var pendingVendorInsurances = from item in lstPendingVendorInsurance.Items
                                          let hidden = item.FindControl("hidPendingVendorInsuranceId").ToCustomHiddenField()
                                          where hidden != null
                                          let insuranceId = hidden.Value.ToLong()
                                          select new PendingVendorInsurance(insuranceId, insuranceId != default(long))
                                          {
                                              CarrierName = item.FindControl("txtPendingVendorInsuranceCarrierName").ToTextBox().Text,
                                              PolicyNumber = item.FindControl("txtPendingVendorInsurancePolicyNumber").ToTextBox().Text,
                                              CertificateHolder = item.FindControl("txtPendingVendorInsuranceCertificateHolder").ToTextBox().Text,
                                              CoverageAmount = item.FindControl("txtPendingVendorInsuranceCoverageAmount").ToTextBox().Text.ToDecimal(),
                                              Required = item.FindControl("chkPendingVendorInsuranceRequired").ToAltUniformCheckBox().Checked,
                                              EffectiveDate = item.FindControl("txtPendingVendorInsuranceEffectiveDate").ToTextBox().Text.ToDateTime(),
                                              ExpirationDate = item.FindControl("txtPendingVendorInsuranceExpirationDate").ToTextBox().Text.ToDateTime(),
                                              InsuranceTypeId = item.FindControl("ddlPendingVendorInsuranceType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                                              SpecialInstruction = item.FindControl("txtPendingVendorInsuranceSpecialInstruction").ToTextBox().Text
                                          };
            foreach (var insurance in pendingVendorInsurances)
            {
                if (insurance.IsNew)
                {
                    insurance.Tenant = pendingVendor.Tenant;
                    insurance.PendingVendor = pendingVendor;
                }

                insurances.Add(insurance);
            }
            return insurances;
        }


        private void LoadPendingVendor(PendingVendor pendingVendor)
        {
            if (!pendingVendor.IsNew) pendingVendor.LoadCollections();

            hidPendingVendorId.Value = pendingVendor.Id.ToString();

            pnlPendingVendorApprovals.Visible = ActiveUser.HasAccessTo(ViewCode.CanApprovePendingVendor) && !pendingVendor.IsNew;

            var vendorExistsForPendingVendor = !pendingVendor.IsNew && new VendorSearch().FetchVendorByNumber(pendingVendor.VendorNumber, pendingVendor.TenantId) != null;

            //DETAILS TAB
            chkApproved.Checked = pendingVendor.Approved;
            chkRejected.Checked = pendingVendor.Rejected;
            chkApproved.Enabled = !vendorExistsForPendingVendor;
            chkRejected.Enabled = !vendorExistsForPendingVendor;
            txtPendingVendorNumber.Text = pendingVendor.VendorNumber;
            txtPendingVendorName.Text = pendingVendor.Name;
            txtNotation.Text = pendingVendor.Notation;
            txtNotes.Text = pendingVendor.Notes;
            chkTSACertified.Checked = pendingVendor.TSACertified;
            chkCTPATCertified.Checked = pendingVendor.CTPATCertified;
            chkPIPCertified.Checked = pendingVendor.PIPCertified;
            txtCTPATNumber.Text = pendingVendor.CTPATNumber;
            txtPIPNumber.Text = pendingVendor.PIPNumber;
            chkSmartWayCertified.Checked = pendingVendor.SmartWayCertified;
            txtSCAC.Text = pendingVendor.Scac;
            txtFederalIdNumber.Text = pendingVendor.FederalIDNumber;
            txtDateCreated.Text = pendingVendor.DateCreated.FormattedShortDate();
            txtBrokerReferenceNumber.Text = pendingVendor.BrokerReferenceNumber;
            ddlBrokerReferenceNumberType.SelectedValue = pendingVendor.BrokerReferenceNumberType.ToInt().ToString();
            txtMC.Text = pendingVendor.MC;
            txtDOT.Text = pendingVendor.DOT;
            txtTrackingUrl.Text = pendingVendor.TrackingUrl;
            chkLTL.Checked = pendingVendor.HandlesLessThanTruckload;
            chkFTL.Checked = pendingVendor.HandlesTruckload;
            chkAir.Checked = pendingVendor.HandlesAir;
            chkPTL.Checked = pendingVendor.HandlesPartialTruckload;
            chkRail.Checked = pendingVendor.HandlesRail;
            chkSmallPack.Checked = pendingVendor.HandlesSmallPack;
            chkCarrier.Checked = pendingVendor.IsCarrier;
            chkAgent.Checked = pendingVendor.IsAgent;
            chkBroker.Checked = pendingVendor.IsBroker;

            hidCreatedByUserId.Value = pendingVendor.CreatedByUserId.ToString();
            txtCreatedByUser.Text = pendingVendor.CreatedByUserId != default(long) ? pendingVendor.CreatedByUser.Username : string.Empty;

            //Locations
            lstLocations.DataSource = pendingVendor.Locations;
            lstLocations.DataBind();
            tabLocations.HeaderText = pendingVendor.Locations.BuildTabCount(LocationsHeader);

            //Services
            var services = new ServiceViewSearchDto().RetrieveVendorServices(pendingVendor).OrderBy(s => s.Code);
            lstServices.DataSource = services;
            lstServices.DataBind();
            tabServices.HeaderText = services.BuildTabCount(ServicesHeader);

            //Equipment Types
            var equipment = pendingVendor.RetrieveEquipmentTypes().OrderBy(e => e.TypeName);
            lstEquipmentTypes.DataSource = equipment;
            lstEquipmentTypes.DataBind();
            tabEquipmentTypes.HeaderText = equipment.BuildTabCount(EquipmentHeader);

            //PendingVendor Insurance
            lstPendingVendorInsurance.DataSource = pendingVendor.Insurances;
            lstPendingVendorInsurance.DataBind();
            tabPendingVendorInsurance.HeaderText = pendingVendor.Insurances.BuildTabCount(PendingVendorInsuranceHeader);

            //Documents
            lstDocuments.DataSource = pendingVendor.Documents;
            lstDocuments.DataBind();
            tabDocuments.HeaderText = pendingVendor.Documents.BuildTabCount(DocumentsHeader);

            memberToolBar.ShowUnlock = pendingVendor.HasUserLock(ActiveUser, pendingVendor.Id);
        }
        

        private void UpdateDetails(PendingVendor pendingVendor)
        {
            pendingVendor.Approved = chkApproved.Checked;
            pendingVendor.Rejected = chkRejected.Checked;
            pendingVendor.VendorNumber = txtPendingVendorNumber.Text;
            pendingVendor.Name = txtPendingVendorName.Text;
            pendingVendor.Notation = txtNotation.Text;
            pendingVendor.Notes = txtNotes.Text;
            pendingVendor.TSACertified = chkTSACertified.Checked;
            pendingVendor.CTPATCertified = chkCTPATCertified.Checked;
            pendingVendor.PIPCertified = chkPIPCertified.Checked;
            pendingVendor.CTPATNumber = txtCTPATNumber.Text;
            pendingVendor.PIPNumber = txtPIPNumber.Text;
            pendingVendor.SmartWayCertified = chkSmartWayCertified.Checked;
            pendingVendor.Scac = txtSCAC.Text;
            pendingVendor.FederalIDNumber = txtFederalIdNumber.Text;
            pendingVendor.BrokerReferenceNumber = txtBrokerReferenceNumber.Text;
            pendingVendor.BrokerReferenceNumberType = ddlBrokerReferenceNumberType.SelectedValue.ToEnum<BrokerReferenceNumberType>();
            pendingVendor.MC = txtMC.Text;
            pendingVendor.DOT = txtDOT.Text;
            pendingVendor.TrackingUrl = txtTrackingUrl.Text;

            pendingVendor.HandlesLessThanTruckload = chkLTL.Checked;
            pendingVendor.HandlesTruckload = chkFTL.Checked;
            pendingVendor.HandlesAir = chkAir.Checked;
            pendingVendor.HandlesPartialTruckload = chkPTL.Checked;
            pendingVendor.HandlesRail = chkRail.Checked;
            pendingVendor.HandlesSmallPack = chkSmallPack.Checked;
            pendingVendor.IsCarrier = chkCarrier.Checked;
            pendingVendor.IsAgent = chkAgent.Checked;
            pendingVendor.IsBroker = chkBroker.Checked;

            pendingVendor.CreatedByUserId = hidCreatedByUserId.Value.ToLong();
        }

        private void UpdateServices(PendingVendor pendingVendor)
        {
            var services = lstServices.Items
                .Select(item => new PendingVendorService
                {
                    ServiceId = item.FindControl("hidServiceId").ToCustomHiddenField().Value.ToLong(),
                    PendingVendor = pendingVendor,
                    Tenant = pendingVendor.Tenant
                })
                .ToList();

            pendingVendor.Services = services;
        }

        private void UpdateEquipmentTypes(PendingVendor pendingVendor)
        {
            var equipmentTypes = lstEquipmentTypes.Items
                .Select(item => new PendingVendorEquipment
                {
                    EquipmentTypeId = item.FindControl("hidEquipmentTypeId").ToCustomHiddenField().Value.ToLong(),
                    PendingVendor = pendingVendor,
                    Tenant = pendingVendor.Tenant
                })
                .ToList();

            pendingVendor.Equipments = equipmentTypes;
        }

        private void UpdateDocumentsFromView(PendingVendor pendingVendor)
        {
            var documents = lstDocuments.Items
                .Select(i =>
                {
                    var id = i.FindControl("hidDocumentId").ToCustomHiddenField().Value.ToLong();
                    return new PendingVendorDocument(id, id != default(long))
                    {
                        DocumentTagId = i.FindControl("hidDocumentTagId").ToCustomHiddenField().Value.ToLong(),
                        Name = i.FindControl("litDocumentName").ToLiteral().Text,
                        Description = i.FindControl("litDocumentDescription").ToLiteral().Text,
                        LocationPath = i.FindControl("hidLocationPath").ToCustomHiddenField().Value,
                        IsInternal = i.FindControl("hidIsInternal").ToCustomHiddenField().Value.ToBoolean(),
                        PendingVendor = pendingVendor,
                        TenantId = pendingVendor.TenantId
                    };
                })
                .ToList();

            pendingVendor.Documents = documents;
        }


        private void SetEditStatus(bool enabled)
        {
            pnlDetails.Enabled = enabled;
            pnlLocations.Enabled = enabled;
            pnlServices.Enabled = enabled;
            pnlEquipmentTypes.Enabled = enabled;
            pnlPendingVendorInsurance.Enabled = enabled;
            pnlDocuments.Enabled = enabled; 
            memberToolBar.EnableSave = enabled;

            litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;
        }


        private void ProcessNotifications(PendingVendor pendingVendor)
        {
            var notifications = hidNotificationsRequired.Value
                .Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                .Distinct()
                .ToList();
            foreach (var notification in notifications)
                switch (notification)
                {
                    case NewPendingVendor:
                        this.NotifyNewPendingVendor(pendingVendor);
                        break;
                }
        }

        private void ProcessFileMoveFromTempLocation(PendingVendor pendingVendor)
        {
            var pendingVendorFolder = WebApplicationSettings.PendingVendorFolder(pendingVendor.TenantId, pendingVendor.Id);
            if (pendingVendor.Documents.Any() && !Directory.Exists(Server.MapPath(pendingVendorFolder)))
                Directory.CreateDirectory(Server.MapPath(pendingVendorFolder));
            foreach (var doc in pendingVendor.Documents)
                if (!string.IsNullOrEmpty(doc.LocationPath))
                {
                    var virtualOldPath = doc.LocationPath;
                    var absOldPath = Server.MapPath(virtualOldPath);
                    var oldFileInfo = new FileInfo(absOldPath);
                    var virtualNewPath = pendingVendorFolder + oldFileInfo.Name;
                    var absNewPath = Server.MapPath(virtualNewPath);

                    if (absOldPath == absNewPath) continue;

                    File.Copy(absOldPath, absNewPath);

                    doc.TakeSnapShot();
                    doc.LocationPath = virtualNewPath;
                    hidFilesToDelete.Value += string.Format("{0};", virtualOldPath);
                }

            if (Lock != null)
                Lock(this, new ViewEventArgs<PendingVendor>(pendingVendor));

            if (Save != null)
                Save(this, new ViewEventArgs<PendingVendor>(pendingVendor));
        }

        private void ProcessTransferredRequest(long pendingVendorId)
        {
            if (pendingVendorId != default(long))
            {
                var pendingVendor = new PendingVendor(pendingVendorId);
                LoadPendingVendor(pendingVendor);

                if (LoadAuditLog != null) LoadAuditLog(this, new ViewEventArgs<PendingVendor>(pendingVendor));
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new PendingVendorHandler(this).Initialize();

            memberToolBar.ShowSave = Access.Modify;
            memberToolBar.ShowDelete = Access.Remove;
            memberToolBar.ShowNew = Access.Modify;
            memberToolBar.ShowEdit = Access.Modify;

            pendingVendorFinder.OpenForEditEnabled = Access.Modify;

            if (IsPostBack) return;

            if (Loading != null)
                Loading(this, new EventArgs());

            if (Session[WebApplicationConstants.TransferPendingVendorId] != null)
            {
                ProcessTransferredRequest(Session[WebApplicationConstants.TransferPendingVendorId].ToLong());
                Session[WebApplicationConstants.TransferPendingVendorId] = null;
            }

            SetEditStatus(false);
        }



        protected void OnNewClicked(object sender, EventArgs e)
        {
            litErrorMessages.Text = string.Empty;

            LoadPendingVendor(new PendingVendor { DateCreated = DateTime.Now, CreatedByUserId = ActiveUser.Id});
            DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
            SetEditStatus(true);
        }

        protected void OnEditClicked(object sender, EventArgs e)
        {
            var pendingVendor = new PendingVendor(hidPendingVendorId.Value.ToLong(), false);

            if (Lock == null || pendingVendor.IsNew) return;

            SetEditStatus(true);
            memberToolBar.ShowUnlock = true;
            Lock(this, new ViewEventArgs<PendingVendor>(pendingVendor));

            LoadPendingVendor(pendingVendor);
        }

        protected void OnUnlockClicked(object sender, EventArgs e)
        {
            var pendingVendor = new PendingVendor(hidPendingVendorId.Value.ToLong(), false);
            if (UnLock != null && !pendingVendor.IsNew)
            {
                SetEditStatus(false);
                memberToolBar.ShowUnlock = false;
                UnLock(this, new ViewEventArgs<PendingVendor>(pendingVendor));
            }
        }

        protected void OnSaveClicked(object sender, EventArgs e)
        {
            var pendingVendorId = hidPendingVendorId.Value.ToLong();
            var pendingVendor = new PendingVendor(pendingVendorId, pendingVendorId != default(long));

            if (pendingVendor.IsNew)
            {
                pendingVendor.TenantId = ActiveUser.TenantId;
                pendingVendor.DateCreated = DateTime.Now;
                hidNotificationsRequired.Value += string.Format("{0};", NewPendingVendor);
            }
            else
            {
                pendingVendor.LoadCollections();
            }

            // previously unapproved/approved
            var approved = pendingVendor.Approved;

            UpdateDetails(pendingVendor);
			pendingVendor.Locations = RetrieveViewLocations(pendingVendor);
			UpdateServices(pendingVendor);
			UpdateEquipmentTypes(pendingVendor);
			pendingVendor.Insurances = RetrieveViewInsurances(pendingVendor);
			UpdateDocumentsFromView(pendingVendor);

            hidFlag.Value = SaveFlag;

            _moveFilesFromTempLocation = pendingVendor.IsNew && pendingVendor.Documents.Any();
            _transferToVendorOnSuccessfulApproval = pendingVendor.Approved && !approved; // vendor was previously not approved, now approved after page update

            if (Save != null)
                Save(this, new ViewEventArgs<PendingVendor>(pendingVendor));

            if (_transferToVendorOnSuccessfulApproval)
            {
                _transferToVendorOnSuccessfulApproval = false;
                if (_savedVendor == null) return;
                Session[WebApplicationConstants.TransferVendorId] = _savedVendor.Id;
                Response.Redirect(VendorView.PageAddress);
                return;
            }

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<PendingVendor>(pendingVendor));
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var pendingVendor = new PendingVendor(hidPendingVendorId.Value.ToLong(), false);

            hidFlag.Value = DeleteFlag;

            if (Delete != null)
                Delete(this, new ViewEventArgs<PendingVendor>(pendingVendor));

            pendingVendorFinder.Reset();
        }

        protected void OnFindClicked(object sender, EventArgs e)
        {
            pendingVendorFinder.Visible = true;
        }

        
        protected void OnPendingVendorFinderItemSelected(object sender, ViewEventArgs<PendingVendor> e)
        {
            litErrorMessages.Text = string.Empty;

            if (hidPendingVendorId.Value.ToLong() != default(long))
            {
                var oldPendingVendor = new PendingVendor(hidPendingVendorId.Value.ToLong(), false);
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<PendingVendor>(oldPendingVendor));
            }

            var pendingVendor = e.Argument;

            LoadPendingVendor(pendingVendor);

            pendingVendorFinder.Visible = false;

            if (pendingVendorFinder.OpenForEdit && Lock != null)
            {
                SetEditStatus(true);
                memberToolBar.ShowUnlock = true;
                Lock(this, new ViewEventArgs<PendingVendor>(pendingVendor));
            }
            else
            {
                SetEditStatus(false);
            }

            if (LoadAuditLog != null) LoadAuditLog(this, new ViewEventArgs<PendingVendor>(pendingVendor));
        }

        protected void OnPendingVendorFinderSelectionCancel(object sender, EventArgs e)
        {
            pendingVendorFinder.Visible = false;
        }


        protected void OnAddLocationClicked(object sender, EventArgs e)
        {
            var locations = RetrieveViewLocations(new PendingVendor(hidPendingVendorId.Value.ToLong(), false));
            locations.Add(new PendingVendorLocation());
            lstLocations.DataSource = locations;
            lstLocations.DataBind();
        }

        protected void OnDeleteLocation(object sender, ViewEventArgs<int> e)
        {
            var locations = RetrieveViewLocations(new PendingVendor(hidPendingVendorId.Value.ToLong(), false));
            locations.RemoveAt(e.Argument);
            lstLocations.DataSource = locations;
            lstLocations.DataBind();
            tabLocations.HeaderText = locations.BuildTabCount(LocationsHeader);
        }

        protected void BindLocationLineItem(object sender, ListViewItemEventArgs e)
        {
            var item = e.Item as ListViewDataItem;
            if (item == null) return;
            var ic = item.FindControl("llicLocation").ToLocationListingInputControl();
            var location = item.DataItem as PendingVendorLocation;
            if (location == null) return;
            ic.LoadLocation(location);
        }



        protected void OnServiceFinderMultiItemSelected(object sender, ViewEventArgs<List<ServiceViewSearchDto>> e)
        {
            var vServices = lstServices.Items
                .Select(i => new
                {
                    Id = i.FindControl("hidServiceId").ToCustomHiddenField().Value,
                    Code = i.FindControl("litServiceCode").ToLiteral().Text,
                    Description = i.FindControl("litServiceDescription").ToLiteral().Text,
                    Category = i.FindControl("litServiceCategory").ToLiteral().Text.ToInt().ToEnum<ServiceCategory>(),
                    ChargeCodeId = i.FindControl("hidServiceChargeCodeId").ToCustomHiddenField().Value.ToLong(),
                    ChargeCode = i.FindControl("litServiceChargeCode").ToLiteral().Text,
                    ChargeCodeDescription = i.FindControl("litServiceChargeCodeDescription").ToLiteral().Text
                })
                .ToList();
            vServices.AddRange(e.Argument.Select(t => new { Id = t.Id.ToString(), t.Code, t.Description, t.Category, t.ChargeCodeId, t.ChargeCode, t.ChargeCodeDescription })
                            .Where(t => !vServices.Select(vt => vt.Id).Contains(t.Id)));
            lstServices.DataSource = vServices.OrderBy(s => s.Code);
            lstServices.DataBind();
            tabServices.HeaderText = vServices.BuildTabCount(ServicesHeader);
            serviceFinder.Visible = false;
        }

        protected void OnServiceFinderSelectionCancelled(object sender, EventArgs e)
        {
            serviceFinder.Visible = false;
        }

        protected void OnAddServiceClicked(object sender, EventArgs e)
        {
            serviceFinder.Visible = true;
        }

        protected void OnClearServicesClicked(object sender, EventArgs e)
        {
            lstServices.DataSource = new List<object>();
            lstServices.DataBind();
            tabServices.HeaderText = ServicesHeader;
            athtuTabUpdater.SetForUpdate(tabServices.ClientID, new List<object>().BuildTabCount(ServicesHeader));
        }

        protected void OnDeleteServiceClicked(object sender, ImageClickEventArgs e)
        {
            var button = (ImageButton)sender;
            var idToRemove = button.Parent.FindControl("hidServiceId").ToCustomHiddenField().Value;
            var service = lstServices
                .Items
                .Select(item => new
                {
                    Id = item.FindControl("hidServiceId").ToCustomHiddenField().Value,
                    Code = item.FindControl("litServiceCode").ToLiteral().Text,
                    Description = item.FindControl("litServiceDescription").ToLiteral().Text,
                    Category = item.FindControl("litServiceCategory").ToLiteral().Text.ToInt().ToEnum<ServiceCategory>(),
                    ChargeCodeId = item.FindControl("hidServiceChargeCodeId").ToCustomHiddenField().Value.ToLong(),
                    ChargeCode = item.FindControl("litServiceChargeCode").ToLiteral().Text,
                    ChargeCodeDescription = item.FindControl("litServiceChargeCodeDescription").ToLiteral().Text
                })
                .Where(u => u.Id != idToRemove)
                .ToList();

            lstServices.DataSource = service.OrderBy(s => s.Code);
            lstServices.DataBind();
            tabServices.HeaderText = service.BuildTabCount(ServicesHeader);
            athtuTabUpdater.SetForUpdate(tabServices.ClientID, service.BuildTabCount(ServicesHeader));
        }


        protected void OnEquipmentTypeFinderMultiItemSelected(object sender, ViewEventArgs<List<EquipmentType>> e)
        {
            var vEquipmentTypes = lstEquipmentTypes
                .Items
                .Select(i => new
                {
                    Id = i.FindControl("hidEquipmentTypeId").ToCustomHiddenField().Value,
                    Code = i.FindControl("litEquipmentTypeCode").ToLiteral().Text,
                    TypeName = i.FindControl("litEquipmentTypeTypeName").ToLiteral().Text,
                    Group = i.FindControl("hidEquipmentTypeGroup").ToCustomHiddenField().Value.ToEnum<EquipmentTypeGroup>(),
                    DatEquipmentType = i.FindControl("hidDatEquipmentType").ToCustomHiddenField().Value.ToEnum<DatEquipmentType>(),
                    Active = i.FindControl("hidEquipmentTypeActive").ToCustomHiddenField().Value.ToBoolean(),
                })
                .ToList();
            vEquipmentTypes.AddRange(e.Argument
                                        .Select(t => new { Id = t.Id.ToString(), t.Code, t.TypeName, t.Group, t.DatEquipmentType, t.Active })
                                        .Where(t => !vEquipmentTypes.Select(vt => vt.Id).Contains(t.Id)));
            lstEquipmentTypes.DataSource = vEquipmentTypes.OrderBy(t => t.TypeName);
            lstEquipmentTypes.DataBind();
            tabEquipmentTypes.HeaderText = vEquipmentTypes.BuildTabCount(EquipmentHeader);
            equipmentTypeFinder.Visible = false;
        }

        protected void OnEquipmentTypeFinderSelectionCancelled(object sender, EventArgs e)
        {
            equipmentTypeFinder.Visible = false;
        }

        protected void OnAddEquipmentTypeClicked(object sender, EventArgs e)
        {
            equipmentTypeFinder.Visible = true;
        }

        protected void OnClearEquipmentTypesClicked(object sender, EventArgs e)
        {
            lstEquipmentTypes.DataSource = new List<object>();
            lstEquipmentTypes.DataBind();
            tabEquipmentTypes.HeaderText = EquipmentHeader;
            athtuTabUpdater.SetForUpdate(tabEquipmentTypes.ClientID, EquipmentHeader);
        }

        protected void OnDeleteEquipmentTypeClicked(object sender, ImageClickEventArgs e)
        {
            var button = (ImageButton)sender;
            var idToRemove = button.Parent.FindControl("hidEquipmentTypeId").ToCustomHiddenField().Value;
            var vEquipmentType = lstEquipmentTypes
                .Items
                .Select(item => new
                {
                    Id = item.FindControl("hidEquipmentTypeId").ToCustomHiddenField().Value,
                    Code = item.FindControl("litEquipmentTypeCode").ToLiteral().Text,
                    TypeName = item.FindControl("litEquipmentTypeTypeName").ToLiteral().Text,
                    Group = item.FindControl("hidEquipmentTypeGroup").ToCustomHiddenField().Value.ToEnum<EquipmentTypeGroup>(),
                    DatEquipmentType = item.FindControl("hidDatEquipmentType").ToCustomHiddenField().Value.ToEnum<DatEquipmentType>(),
                    Active = item.FindControl("hidEquipmentTypeActive").ToCustomHiddenField().Value.ToBoolean(),
                })
                .Where(u => u.Id != idToRemove)
                .ToList();
            lstEquipmentTypes.DataSource = vEquipmentType.OrderBy(t => t.TypeName);
            lstEquipmentTypes.DataBind();
            tabEquipmentTypes.HeaderText = vEquipmentType.BuildTabCount(EquipmentHeader);
            athtuTabUpdater.SetForUpdate(tabEquipmentTypes.ClientID, vEquipmentType.BuildTabCount(EquipmentHeader));
        }



        protected void OnAddPendingVendorInsuranceClicked(object sender, EventArgs e)
        {
            var vInsurance = lstPendingVendorInsurance
                .Items
                .Select(item => new
                {
                    Id = item.FindControl("hidPendingVendorInsuranceId").ToCustomHiddenField().Value,
                    CarrierName = item.FindControl("txtPendingVendorInsuranceCarrierName").ToTextBox().Text,
                    PolicyNumber = item.FindControl("txtPendingVendorInsurancePolicyNumber").ToTextBox().Text,
                    CertificateHolder = item.FindControl("txtPendingVendorInsuranceCertificateHolder").ToTextBox().Text,
                    CoverageAmount = item.FindControl("txtPendingVendorInsuranceCoverageAmount").ToTextBox().Text.ToDecimal(),
                    Required = item.FindControl("chkPendingVendorInsuranceRequired").ToAltUniformCheckBox().Checked,
                    EffectiveDate = item.FindControl("txtPendingVendorInsuranceEffectiveDate").ToTextBox().Text.ToDateTime(),
                    ExpirationDate = item.FindControl("txtPendingVendorInsuranceExpirationDate").ToTextBox().Text.ToDateTime(),
                    InsuranceTypeId = item.FindControl("ddlPendingVendorInsuranceType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    SpecialInstruction = item.FindControl("txtPendingVendorInsuranceSpecialInstruction").ToTextBox().Text,
                })
                .ToList();

            // add new
            vInsurance.Add(new
            {
                Id = default(long).GetString(),
                CarrierName = string.Empty,
                PolicyNumber = string.Empty,
                CertificateHolder = string.Empty,
                CoverageAmount = 0m,
                Required = false,
                EffectiveDate = DateTime.Now,
                ExpirationDate = DateTime.Now.AddYears(1),
                InsuranceTypeId = default(long),
                SpecialInstruction = string.Empty,
            });

            lstPendingVendorInsurance.DataSource = vInsurance;
            lstPendingVendorInsurance.DataBind();
            tabPendingVendorInsurance.HeaderText = vInsurance.BuildTabCount(PendingVendorInsuranceHeader);
        }

        protected void OnClearPendingVendorInsuranceClicked(object sender, EventArgs e)
        {
            lstPendingVendorInsurance.DataSource = new List<object>();
            lstPendingVendorInsurance.DataBind();
            tabPendingVendorInsurance.HeaderText = PendingVendorInsuranceHeader;
        }

        protected void OnDeletePendingVendorInsuranceClicked(object sender, ImageClickEventArgs e)
        {
            var button = (ImageButton)sender;

            var vInsurance = lstPendingVendorInsurance
                .Items
                .Select(item => new
                {
                    Id = item.FindControl("hidPendingVendorInsuranceId").ToCustomHiddenField().Value,
                    CarrierName = item.FindControl("txtPendingVendorInsuranceCarrierName").ToTextBox().Text,
                    PolicyNumber = item.FindControl("txtPendingVendorInsurancePolicyNumber").ToTextBox().Text,
                    CertificateHolder = item.FindControl("txtPendingVendorInsuranceCertificateHolder").ToTextBox().Text,
                    CoverageAmount = item.FindControl("txtPendingVendorInsuranceCoverageAmount").ToTextBox().Text.ToDecimal(),
                    Required = item.FindControl("chkPendingVendorInsuranceRequired").ToAltUniformCheckBox().Checked,
                    EffectiveDate = item.FindControl("txtPendingVendorInsuranceEffectiveDate").ToTextBox().Text.ToDateTime(),
                    ExpirationDate = item.FindControl("txtPendingVendorInsuranceExpirationDate").ToTextBox().Text.ToDateTime(),
                    InsuranceTypeId = item.FindControl("ddlPendingVendorInsuranceType").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    SpecialInstruction = item.FindControl("txtPendingVendorInsuranceSpecialInstruction").ToTextBox().Text,
                })
                .ToList();

            vInsurance.RemoveAt(button.Parent.FindControl("hidPendingVendorInsuranceEditItemIndex").ToCustomHiddenField().Value.ToInt());

            lstPendingVendorInsurance.DataSource = vInsurance;
            lstPendingVendorInsurance.DataBind();
            tabPendingVendorInsurance.HeaderText = vInsurance.BuildTabCount(PendingVendorInsuranceHeader);
        }



        private void LoadDocumentForEdit(PendingVendorDocument document)
        {
            chkDocumentIsInternal.Checked = document.IsInternal;
            txtDocumentName.Text = document.Name;
            txtDocumentDescription.Text = document.Description;
            ddlDocumentTag.SelectedValue = document.DocumentTagId.GetString();
            txtLocationPath.Text = GetLocationFileName(document.LocationPath);
            hidLocationPath.Value = document.LocationPath;
        }

        protected string GetLocationFileName(object relativePath)
        {
            return relativePath == null ? string.Empty : Server.GetFileName(relativePath.ToString());
        }

        protected void OnAddDocumentClicked(object sender, EventArgs e)
        {
            hidEditingIndex.Value = (-1).ToString();
            LoadDocumentForEdit(new PendingVendorDocument());
            pnlEditDocument.Visible = true;
            pnlDimScreen.Visible = true;
        }

        protected void OnClearDocumentsClicked(object sender, EventArgs e)
        {
            var paths = lstDocuments.Items.Select(i => new { LocationPath = i.FindControl("hidLocationPath").ToCustomHiddenField().Value, }).ToList();
            foreach (var path in paths)
                hidFilesToDelete.Value += string.Format("{0};", path);

            lstDocuments.DataSource = new List<Object>();
            lstDocuments.DataBind();
            tabDocuments.HeaderText = DocumentsHeader;
        }

        protected void OnEditDocumentClicked(object sender, ImageClickEventArgs e)
        {
            var control = ((ImageButton)sender).Parent;

            hidEditingIndex.Value = control.FindControl("hidDocumentIndex").ToCustomHiddenField().Value;

            var document = new PendingVendorDocument
            {
                Name = control.FindControl("litDocumentName").ToLiteral().Text,
                Description = control.FindControl("litDocumentDescription").ToLiteral().Text,
                DocumentTagId = control.FindControl("hidDocumentTagId").ToCustomHiddenField().Value.ToLong(),
                LocationPath = control.FindControl("hidLocationPath").ToCustomHiddenField().Value,
                IsInternal = control.FindControl("hidIsInternal").ToCustomHiddenField().Value.ToBoolean(),
            };

            LoadDocumentForEdit(document);

            pnlEditDocument.Visible = true;
            pnlDimScreen.Visible = true;
        }

        protected void OnDeleteDocumentClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var documents = lstDocuments
                .Items
                .Select(i => new
                {
                    Id = i.FindControl("hidDocumentId").ToCustomHiddenField().Value.ToLong(),
                    Name = i.FindControl("litDocumentName").ToLiteral().Text,
                    Description = i.FindControl("litDocumentDescription").ToLiteral().Text,
                    DocumentTagId = i.FindControl("hidDocumentTagId").ToCustomHiddenField().Value.ToLong(),
                    LocationPath = i.FindControl("hidLocationPath").ToCustomHiddenField().Value,
                    IsInternal = i.FindControl("hidIsInternal").ToCustomHiddenField().Value.ToBoolean(),
                })
                .ToList();

            var index = imageButton.Parent.FindControl("hidDocumentIndex").ToCustomHiddenField().Value.ToInt();
            hidFilesToDelete.Value += string.Format("{0};", documents[index].LocationPath);
            documents.RemoveAt(index);

            lstDocuments.DataSource = documents;
            lstDocuments.DataBind();
            tabDocuments.HeaderText = documents.BuildTabCount(DocumentsHeader);
        }

        protected void OnCloseEditDocumentClicked(object sender, EventArgs eventArgs)
        {
            pnlEditDocument.Visible = false;
            pnlDimScreen.Visible = false;
        }

        protected void OnEditDocumentDoneClicked(object sender, EventArgs e)
        {
            var virtualPath = WebApplicationSettings.PendingVendorFolder(ActiveUser.TenantId, hidPendingVendorId.Value.ToLong());
            var physicalPath = Server.MapPath(virtualPath);

            var documents = lstDocuments
                .Items
                .Select(i => new
                {
                    Id = i.FindControl("hidDocumentId").ToCustomHiddenField().Value.ToLong(),
                    Name = i.FindControl("litDocumentName").ToLiteral().Text,
                    Description = i.FindControl("litDocumentDescription").ToLiteral().Text,
                    DocumentTagId = i.FindControl("hidDocumentTagId").ToCustomHiddenField().Value,
                    LocationPath = i.FindControl("hidLocationPath").ToCustomHiddenField().Value,
                    IsInternal = i.FindControl("hidIsInternal").ToCustomHiddenField().Value.ToBoolean(),
                })
                .ToList();

            var documentIndex = hidEditingIndex.Value.ToInt();

            var documentId = default(long);
            if (documentIndex != -1)
            {
                documentId = documents[documentIndex].Id;
                documents.RemoveAt(documentIndex);
            }

            var locationPath = hidLocationPath.Value; // this should actually be a hidden field holding the existing path during edit

            documents.Insert(0, new
            {
                Id = documentId,
                Name = txtDocumentName.Text,
                Description = txtDocumentDescription.Text,
                DocumentTagId = ddlDocumentTag.SelectedValue,
                LocationPath = locationPath,
                IsInternal = chkDocumentIsInternal.Checked,
            });

            if (fupLocationPath.HasFile)
            {
                // ensure directory is present
                if (!Directory.Exists(physicalPath)) Directory.CreateDirectory(physicalPath);

                //ensure unique filename
                var fi = new FileInfo(Server.MakeUniqueFilename(Path.Combine(physicalPath, fupLocationPath.FileName), true));

                // save file
                fupLocationPath.WriteToFile(physicalPath, fi.Name, Server.MapPath(documents[0].LocationPath));

                // remove updated record
                documents.RemoveAt(0);

                // re-add record with new file
                documents.Insert(0, new
                {
                    Id = documentId,
                    Name = txtDocumentName.Text,
                    Description = txtDocumentDescription.Text,
                    DocumentTagId = ddlDocumentTag.SelectedValue,
                    LocationPath = virtualPath + fi.Name,
                    IsInternal = chkDocumentIsInternal.Checked
                });
            }

            lstDocuments.DataSource = documents.OrderBy(d => d.Name);
            lstDocuments.DataBind();
            tabDocuments.HeaderText = documents.BuildTabCount(DocumentsHeader);

            pnlEditDocument.Visible = false;
            pnlDimScreen.Visible = false;
        }

        protected void OnLocationPathClicked(object sender, EventArgs e)
        {
            var button = (LinkButton)sender;
            var path = button.Parent.FindControl("hidLocationPath").ToCustomHiddenField().Value;
            Response.Export(Server.ReadFromFile(path), button.Text);
        }
    }
}