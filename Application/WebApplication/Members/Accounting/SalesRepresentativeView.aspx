﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="SalesRepresentativeView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.SalesRepresentativeView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        ShowEdit="true" ShowDelete="true" ShowSave="true" ShowFind="true" ShowMore="false" ShowImport="True"
        OnFind="OnToolbarFindClicked" OnSave="OnToolbarSaveClicked" OnDelete="OnToolbarDeleteClicked" OnUnlock="OnToolbarUnlockClicked"
        OnNew="OnToolbarNewClicked" OnEdit="OnToolbarEditClicked" OnImport="OnImportClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Sales Representatives
               <eShip:RecordIdentityDisplayControl runat="server" ID="ridcRecordIdentity" TargetControlId="txtName" />
            </h3>

            <div class="lockMessage">
                <asp:Literal ID="litMessage" runat="server" />
            </div>
        </div>

        <hr class="dark mb5" />

        <div class="errorMsgLit">
            <asp:Literal runat="server" ID="litErrorMessages" />
        </div>

        <eShip:SalesRepresentativeFinderControl runat="server" Visible="false" ID="salesRepFinder" OpenForEditEnabled="True"
            OnItemSelected="OnSalesRepFinderItemSelected" OnSelectionCancel="OnSalesRepFinderItemCancelled" />
        <eShip:CustomerFinderControl ID="customerFinder" runat="server" EnableMultiSelection="false"
            OnlyActiveResults="True" Visible="False" OpenForEditEnabled="false" OnSelectionCancel="OnCustomerFinderSelectionCancelled"
            OnItemSelected="OnCustomerFinderItemSelected" />
        <eShip:CustomHiddenField runat="server" ID="hidSalesRepresentativeId" />
        <eShip:AjaxTabHeaderTextUpdater runat="server" ID="athtuTabUpdater" />

        <ajax:TabContainer ID="tabSalesRepresentatives" runat="server" CssClass="ajaxCustom">
            <ajax:TabPanel runat="server" ID="tabDetails" HeaderText="Details">
                <ContentTemplate>
                    <asp:Panel ID="pnlDetails" runat="server">
                        <div class="rowgroup">
                            <div class="col_1_3 bbox no-right-border">
                                <h5>Record Information</h5>
                                <div class="row">
                                    <div class="fieldgroup mr20">
                                        <label class="wlabel">Sales Representative Number</label>
                                        <eShip:CustomTextBox ID="txtSalesRepNumber" runat="server" CssClass="w200 disabled" MaxLength="50" ReadOnly="true" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Date Created</label>
                                        <eShip:CustomTextBox ID="txtDateCreated" runat="server" CssClass="w200 disabled" ReadOnly="True" />
                                    </div>
                                </div>

                                <h5 class="pt20">Additional Entity Information</h5>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Entity Name</label>
                                        <eShip:CustomTextBox ID="txtAddlEntityName" MaxLength="50" runat="server" CssClass="w200" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Entity Commission Percent</label>
                                        <eShip:CustomTextBox ID="txtAddlEntityCommPercent" MaxLength="50" runat="server" CssClass="w200" />
                                        <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtAddlEntityCommPercent"
                                            FilterType="Custom, Numbers" ValidChars="." />
                                    </div>
                                </div>
                            </div>
                            <div class="col_1_3 bbox vlinedarkleft">
                                <h5>Location Information</h5>
                                <div class="rowgroup">
                                    <eShip:SimpleAddressInputControl ID="mailingAddressInput" runat="server" />
                                </div>
                            </div>
                            <div class="col_1_3 bbox pl25">
                                <h5>Contact Information</h5>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Name</label>
                                        <eShip:CustomTextBox runat="server" ID="txtName" MaxLength="50" CssClass="w200" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Company Name</label>
                                        <eShip:CustomTextBox ID="txtCompanyName" MaxLength="50" runat="server" CssClass="w200" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Phone</label>
                                        <eShip:CustomTextBox ID="txtPhone" MaxLength="50" runat="server" CssClass="w200" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Fax</label>
                                        <eShip:CustomTextBox ID="txtFax" MaxLength="50" runat="server" CssClass="w200" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Mobile</label>
                                        <eShip:CustomTextBox ID="txtMobile" MaxLength="50" runat="server" CssClass="w200" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="fieldgroup">
                                        <label class="wlabel">Email</label>
                                        <eShip:CustomTextBox ID="txtEmail" MaxLength="100" runat="server" CssClass="w200" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabSalesRepCommTiers" HeaderText="Commission Tiers">
                <ContentTemplate>
                    <asp:UpdatePanel runat="server" ID="upPnlDetails" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel ID="pnlSalesRepCommTiers" runat="server">
                                <div class="row mb10">
                                    <div class="fieldgroup right">
                                        <asp:Button runat="server" ID="btnAddSalesRepCommTier" Text="Add Commission Tier" CssClass="mr10" CausesValidation="false" OnClick="OnAddSalesRepCommTierClicked" />
                                        <asp:Button runat="server" ID="btnClearDetails" Text="Clear Commission Tiers" CausesValidation="False" OnClick="OnClearSalesRepCommTierClicked" />
                                    </div>
                                </div>
                                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheSalesRepCommTier" TableId="salesRepCommTier" IgnoreHeaderBackgroundFill="True" HeaderZIndex="2" />
                                <table class="stripe" id="salesRepCommTier">
                                    <tr>
                                        <th style="width: 33%;">Customer
                                        </th>
                                        <th style="width: 9%;">Commission<br/> Percent
                                        </th>
                                        <th style="width: 10%;">Ceiling Value
                                        </th>
                                        <th style="width: 10%;">Floor Value
                                        </th>
                                        <th style="width: 9%;">Effective Date
                                        </th>
                                        <th style="width: 9%;">Expiration Date
                                        </th>
                                        <th style="width: 16%;">Service Mode
                                        </th>
                                        <th style="width: 5%;" class="text-center">Action
                                        </th>
                                    </tr>
                                    <asp:ListView ID="lstSalesRepCommTiers" runat="server" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnCommissionTiersItemDataBound">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidSalesRepCommTierIndex" runat="server" Value='<%# Container.DataItemIndex %>' />
                                                    <eShip:CustomHiddenField ID="hidSalesRepCommTierId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <eShip:CustomHiddenField ID="hidSalesRepCommTierCustomerId" runat="server" Value='<%# Eval("CustomerId") %>' />
                                                    <eShip:CustomTextBox runat="server" ID="txtCustomerNumber" CssClass="w80 disabled" Text='<%# Eval("CustomerNumber") %>' ReadOnly="True"/>
                                                    <eShip:CustomTextBox runat="server" ID="txtCustomerName" CssClass="w200 disabled" ReadOnly="True" Text='<%# Eval("CustomerName") %>' />
                                                    <asp:ImageButton ID="imgModifyCommissionTier" runat="server" ImageUrl="~/images/icons2/editBlue.png"
                                                        CausesValidation="False" OnClick="OnEditCommissionTierClicked" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtCommissionPercent" runat="server" MaxLength="9" Text='<%# Eval("CommissionPercent", "{0:f4}") %>' CssClass="w70" Type="FloatingPointNumbers" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtCeilingValue" runat="server" MaxLength="9" Text='<%# Eval("CeilingValue", "{0:f4}") %>' CssClass="w80" Type="FloatingPointNumbers" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtFloorValue" runat="server" MaxLength="9" Text='<%# Eval("FloorValue", "{0:f4}") %>' CssClass="w80" Type="FloatingPointNumbers" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtEfectiveDate" runat="server" Text='<%# Eval("EffectiveDate").ToDateTime().FormattedShortDate() %>' CssClass="w80" Type="Date" placeholder="99/99/9999" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox ID="txtExpirationDate" runat="server" Text='<%# Eval("ExpirationDate").ToDateTime().FormattedShortDate() %>' CssClass="w80" Type="Date" placeholder="99/99/9999" />
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlServiceMode" runat="server" DataTextField="Text" DataValueField="Value" CssClass="w150" />
                                                </td>
                                                <td class="text-center">
                                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                                        CausesValidation="false" OnClick="OnDeleteSalesRepCommTierClicked" Enabled='<%# Access.Modify %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                                <asp:Panel runat="server" ID="pnlEditSalesRepCommTier" Visible="false">
                                    <div class="popup popupControlOverW500">
                                        <div class="popheader">
                                            <h4>Add/Modify Commission Tier</h4>
                                            <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                                                CausesValidation="false" OnClick="OnCloseSalesRepCommTierClicked" runat="server" />
                                        </div>
                                        <div class="row pt10">
                                            <table class="poptable">
                                                <tr>
                                                    <td class="text-right">
                                                        <label class="upper">Customer:</label>
                                                    </td>
                                                    <td>
                                                        <eShip:CustomHiddenField runat="server" ID="hidCustomerId" />
                                                        <eShip:CustomTextBox runat="server" ID="txtEditCustomerNumber" CssClass="w80" OnTextChanged="OnCustomerNumberEntered" AutoPostBack="True" />
                                                        <asp:ImageButton ID="imgCustomerSearch" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                                                            CausesValidation="False" OnClick="OnCustomerSearchClicked" />
                                                        <eShip:CustomTextBox runat="server" ID="txtEditCustomerName" CssClass="w230 disabled" ReadOnly="True" />
                                                        <ajax:AutoCompleteExtender runat="server" ID="aceEditCustomer" TargetControlID="txtEditCustomerNumber" ServiceMethod="GetCustomerList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                                                            EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right">
                                                        <label class="upper">Commission %:</label>
                                                    </td>
                                                    <td>
                                                        <eShip:CustomTextBox ID="txtEditCommissionPercent" runat="server" CssClass="w200" MaxLength="9" Type="FloatingPointNumbers" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right">
                                                        <label class="upper">Ceiling Value:</label>
                                                    </td>
                                                    <td>
                                                        <eShip:CustomTextBox ID="txtEditCeilingValue" runat="server" CssClass="w200" MaxLength="9" Type="FloatingPointNumbers" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right">
                                                        <label class="upper">Floor Value:</label>
                                                    </td>
                                                    <td>
                                                        <eShip:CustomTextBox ID="txtEditFloorValue" runat="server" CssClass="w200" MaxLength="9" Type="FloatingPointNumbers" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right">
                                                        <label class="upper">Effective Date:</label>
                                                    </td>
                                                    <td>
                                                        <eShip:CustomTextBox runat="server" ID="txtEditEffectiveDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right">
                                                        <label class="upper">Expiration Date:</label>
                                                    </td>
                                                    <td>
                                                        <eShip:CustomTextBox runat="server" ID="txtEditExpirationDate" CssClass="w100" Type="Date" placeholder="99/99/9999" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right">
                                                        <label class="upper">Service Mode:</label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlEditServiceMode" DataTextField="Text" DataValueField="Value" runat="server" CssClass="w200" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <asp:Button ID="btnDone" Text="Done" OnClick="OnEditSalesRepCommTierDoneClicked" runat="server" CausesValidation="false" />
                                                        <asp:Button ID="btnClose" Text="Cancel" OnClick="OnCloseSalesRepCommTierClicked" runat="server" CausesValidation="false" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="imgCustomerSearch" />
                        </Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabAuditLog" HeaderText="Audit Log">
                <ContentTemplate>
                    <eShip:AuditLogControl runat="server" ID="auditLogs" />
                </ContentTemplate>
            </ajax:TabPanel>
        </ajax:TabContainer>
    </div>
    <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
        OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information" OnOkay="OnOkayProcess" />

    <asp:UpdatePanel runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
            <eShip:CustomHiddenField runat="server" ID="hidEditingIndex" />
            <eShip:CustomHiddenField runat="server" ID="hidEditStatus" />
            <eShip:CustomHiddenField runat="server" ID="hidFlag" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
