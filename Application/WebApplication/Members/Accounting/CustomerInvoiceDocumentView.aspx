﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="CustomerInvoiceDocumentView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.CustomerInvoiceDocumentView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Operations" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="false" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
                Customer Invoice Document
            </h3>
        </div>
        <hr class="dark mb5" />
        
        <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
            Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" />

        <asp:Panel runat="server" ID="pnlDocuments">
            <div class="rowgroup">
                <h5>Invoice
                    <asp:Literal runat="server" ID="litInvoiceNumber" />
                    <%= !string.IsNullOrEmpty(litInvoiceNumber.Text) && ActiveUser.HasAccessTo(ViewCode.Invoice) 
                            ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Invoice Record'><img src={3} width='20' class='middle'/></a>", 
                                                    ResolveUrl(InvoiceView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    litInvoiceNumber.Text,
                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
                            : string.Empty %>
                </h5>
                <div class="row">
                    <div class="fieldgroup">
                        <label>
                            <asp:ImageButton runat="server" ID="ibtnInvoiceDocument" OnClick="OnInvoiceDocumentClicked" ImageUrl="~/images/icons2/downloadDocument.png"/>
                            Invoice Document
                        </label>
                    </div>
                </div>
            </div>
             <div class="rowgroup">
                <asp:Repeater runat="server" ID="rptServiceTickets" OnItemDataBound="OnServiceTicketsDocumentsItemDataBound">
                    <ItemTemplate>
                        <h5>Service Ticket
                            <asp:Literal runat="server" ID="litShipmentNumber" Text='<%# Eval("ServiceTicketNumber") %>' /> 
                            Documents
                            <%# !string.IsNullOrEmpty(Eval("ServiceTicketNumber").ToString()) && ActiveUser.HasAccessTo(ViewCode.ServiceTicket) 
                            ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Shipment Record'><img src={3} width='20' class='middle'/></a>", 
                                                    ResolveUrl(ServiceTicketView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    Eval("ServiceTicketNumber"),
                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
                            : string.Empty %>
                        </h5>
                        <table class="stripe">
                            <tr>
                                <th style="width: 22%;">Name
                                </th>
                                <th style="width: 32%;">Description
                                </th>
                                <th style="width: 20%;">Document Tag
                                </th>
                                <th style="width: 25%;">File
                                </th>
                            </tr>
                            <asp:Repeater runat="server" ID="rptServiceTicketDocuments">
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <%# Eval("Name") %>
                                        </td>
                                        <td>
                                            <%# Eval("Description") %>
                                        </td>
                                        <td>
                                            <%# Eval("DocumentTagId").ToLong() == default(long) ? string.Empty : Eval("DocumentTag.Description") %>
                                        </td>
                                        <td>
                                            <eShip:CustomHiddenField ID="hidLocationPath" runat="server" Value='<%# Eval("LocationPath") %>' />
                                            <asp:ImageButton runat="server" ID="ibtnShipmentDocumentLocationPath" OnClick="OnLocationPathClicked" ImageUrl="~/images/icons2/downloadDocument.png"/>
                                            <asp:Literal runat="server" ID="litDocumentPath" Text='<%# GetLocationFileName(Eval("LocationPath")) %>'/>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <div class="rowgroup">
                <asp:Repeater runat="server" ID="rptShipments" OnItemDataBound="OnShipmentsItemDataBound">
                    <ItemTemplate>
                        <h5>Shipment
                            <asp:Literal runat="server" ID="litShipmentNumber" Text='<%# Eval("ShipmentNumber") %>' /> 
                            Documents
                            <%# !string.IsNullOrEmpty(Eval("ShipmentNumber").ToString()) && ActiveUser.HasAccessTo(ViewCode.Shipment) 
                            ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Shipment Record'><img src={3} width='20' class='middle'/></a>", 
                                                    ResolveUrl(ShipmentView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    Eval("ShipmentNumber"),
                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
                            : string.Empty %>
                        </h5>
                        <div class="row">
                            <div class="fieldgroup">
                                <label>
                                    <asp:ImageButton runat="server" ID="ibtnShipmentBillOfLading" OnClick="OnShipmentBillOfLadingClicked" ImageUrl="~/images/icons2/downloadDocument.png"/>
                                    Bill Of Lading
                                </label>
                            </div>
                        </div>
                        <div class="row pb10">
                            <div class="fieldgroup">
                                <label>
                                    <asp:ImageButton runat="server" ID="ibtnShipmentStatement" OnClick="OnShipmentStatementClicked" ImageUrl="~/images/icons2/downloadDocument.png"/>
                                    Shipment Statement
                                </label>
                            </div>
                        </div>
                        <table class="stripe">
                            <tr>
                                <th style="width: 22%;">Name
                                </th>
                                <th style="width: 32%;">Description
                                </th>
                                <th style="width: 20%;">Document Tag
                                </th>
                                <th style="width: 25%;">File
                                </th>
                            </tr>
                            <asp:Repeater runat="server" ID="rptShipmentDocuments">
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <%# Eval("Name") %>
                                        </td>
                                        <td>
                                            <%# Eval("Description") %>
                                        </td>
                                        <td>
                                            <%# Eval("DocumentTagId").ToLong() == default(long) ? string.Empty : Eval("DocumentTag.Description") %>
                                        </td>
                                        <td>
                                            <eShip:CustomHiddenField ID="hidLocationPath" runat="server" Value='<%# Eval("LocationPath") %>' />
                                            <asp:ImageButton runat="server" ID="ibtnShipmentDocumentLocationPath" OnClick="OnLocationPathClicked" ImageUrl="~/images/icons2/downloadDocument.png"/>
                                            <asp:Literal runat="server" ID="litDocumentPath" Text='<%# GetLocationFileName(Eval("LocationPath")) %>'/>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </ItemTemplate>
                </asp:Repeater>
                <div class="row pt20">
                    <div class="fieldgroup">
                        <asp:Button runat="server" ID="btnDownloadAllDocuments" Text="Download All Documents" OnClick="OnDownloadAlldocumentsClicked" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlRecordNotFound" Visible="false">
            <div class="rowgroup">
                <h5 class="red">Record Not Found</h5>
                <div class="row">
                    <div class="fieldgroup">
                        <label>The record you are attempting to access was not found!</label>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
    <eShip:CustomHiddenField runat="server" ID="hidInvoiceId" />
</asp:Content>
