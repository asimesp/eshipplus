﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ionic.Zip;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.WebApplication.Documents;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting
{
    public partial class CustomerInvoiceDocumentView : MemberPageBase
    {
        public const string InvoicePdfFileName = "Invoice_{0}.pdf";
        public const string BillOfLadingPdfFileName = "BOL_{0}.pdf";
        public const string ShipmentStatementPdfFileName = "ShipmentStatement_{0}.pdf";

        public const string CouldNotGenerateBolMsg = "Could not generate a Bill of Lading for this shipment.";
        public const string CouldNotGenerateInvoiceDocumentMsg = "Could not generate an invoice document.";
        public const string CouldNotGenerateShipmentStatementMsg = "Could not generate a Shipment Statement for this shipment.";

        public static string PageAddress { get { return "~/Members/Accounting/CustomerInvoiceDocumentView.aspx"; } }

        public override ViewCode PageCode
        {
            get { return ViewCode.CustomerInvoiceDocument; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.FinanceBlue; }
        }


        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }


        protected string GetLocationFileName(object relativePath)
        {
            return relativePath == null ? string.Empty : Server.GetFileName(relativePath.ToString());
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack) return;

            var invoice = new InvoiceSearch().FetchInvoiceByInvoiceNumber(Request.QueryString[WebApplicationConstants.TransferNumber].GetString().UrlTextDecrypt(), ActiveUser.TenantId);

            if (invoice == null || invoice.IsNew)
            {
                pnlDocuments.Visible = false;
                pnlRecordNotFound.Visible = true;
            }
            else
            {
                pnlDocuments.Visible = true;
                pnlRecordNotFound.Visible = false;

                litInvoiceNumber.Text = invoice.InvoiceNumber;
                hidInvoiceId.Value = invoice.Id.GetString();
                var shipments = invoice.RetrieveAssociatedShipments();
                var serviceTickets = invoice.RetrieveAssociatedServiceTickets();

                rptShipments.DataSource = shipments;
                rptShipments.DataBind();

                rptServiceTickets.DataSource = serviceTickets;
                rptServiceTickets.DataBind();
            }
        }

        protected void OnLocationPathClicked(object sender, EventArgs e)
        {
            var button = (ImageButton)sender;
            var path = button.Parent.FindControl("hidLocationPath").ToCustomHiddenField().Value;
            Response.Export(Server.ReadFromFile(path), button.FindControl("litDocumentPath").ToLiteral().Text);
        }

        protected void OnShipmentsItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem == null) return;

            var shipment = (Shipment)e.Item.DataItem;

            var rptShipmentDocuments = e.Item.FindControl("rptShipmentDocuments").ToRepeater();
            rptShipmentDocuments.DataSource = shipment.Documents.Where(d => !d.IsInternal);
            rptShipmentDocuments.DataBind();
        }

        protected void OnServiceTicketsDocumentsItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem == null) return;

            var serviceTicket = (ServiceTicket)e.Item.DataItem;

            var rptServiceTicketDocuments = e.Item.FindControl("rptServiceTicketDocuments").ToRepeater();
            rptServiceTicketDocuments.DataSource = serviceTicket.Documents.Where(d => !d.IsInternal);
            rptServiceTicketDocuments.DataBind();
        }


        protected void OnInvoiceDocumentClicked(object sender, EventArgs e)
        {
            var invoice = new Invoice(hidInvoiceId.Value.ToLong());
            var content = this.GenerateInvoice(invoice, true);
            if (string.IsNullOrEmpty(content))
            {
                DisplayMessages(new[] { ValidationMessage.Error(CouldNotGenerateInvoiceDocumentMsg) });
                return;
            }
            var name = string.Format(InvoicePdfFileName, invoice.InvoiceNumber);
            var pdf = content.ToPdf(name);
            Response.Export(pdf, name);
        }

        protected void OnShipmentBillOfLadingClicked(object sender, EventArgs e)
        {
            var shipment = new ShipmentSearch().FetchShipmentByShipmentNumber(((Control)sender).FindControl("litShipmentNumber").ToLiteral().Text, ActiveUser.TenantId);
            var content = this.GenerateBOL(shipment, true);
            if (string.IsNullOrEmpty(content))
            {
                DisplayMessages(new []{ValidationMessage.Error(CouldNotGenerateBolMsg)});
                return;
            }
            var name = string.Format(BillOfLadingPdfFileName, shipment.ShipmentNumber);
            var pdf = content.ToPdf(name);
            Response.Export(pdf, name);
        }

        protected void OnShipmentStatementClicked(object sender, EventArgs e)
        {
            var shipment = new ShipmentSearch().FetchShipmentByShipmentNumber(((Control)sender).FindControl("litShipmentNumber").ToLiteral().Text, ActiveUser.TenantId);
            var shipmentInvoices = new AuditInvoiceDto().FetchAuditInvoiceDtos(shipment.ShipmentNumber, DetailReferenceType.Shipment, shipment.TenantId);
            var content = this.GenerateShipmentStatement(shipment, shipmentInvoices, true);
            if (string.IsNullOrEmpty(content))
            {
                DisplayMessages(new[] { ValidationMessage.Error(CouldNotGenerateBolMsg) });
                return;
            }
            var name = string.Format(ShipmentStatementPdfFileName, shipment.ShipmentNumber);
            var pdf = content.ToPdf(name);
            Response.Export(pdf, name);
        }

        protected void OnDownloadAlldocumentsClicked(object sender, EventArgs e)
        {
            var invoice = new Invoice(hidInvoiceId.Value.ToLong());
            invoice.LoadCollections();
            var shipments = invoice.RetrieveAssociatedShipments();
            var serviceTickets = invoice.RetrieveAssociatedServiceTickets();
            var content = string.Empty;
            ZipFile zip;
            using (zip = new ZipFile())
            {
                foreach (var shipment in shipments)
                {
                    shipment.LoadCollections();

                    // bol
                    var entryName = string.Format(BillOfLadingPdfFileName, shipment.ShipmentNumber);
                    content = this.GenerateBOL(shipment, true);
                    if(!string.IsNullOrEmpty(content))
                        zip.AddEntry(entryName, content.ToPdf(shipment.ShipmentNumber));

                    // shipment statement
                    entryName = string.Format(ShipmentStatementPdfFileName, shipment.ShipmentNumber);
                    var shipmentInvoices = new AuditInvoiceDto().FetchAuditInvoiceDtos(shipment.ShipmentNumber, DetailReferenceType.Shipment, shipment.TenantId);
                    content = this.GenerateShipmentStatement(shipment, shipmentInvoices, true);
                    if (!string.IsNullOrEmpty(content))
                        zip.AddEntry(entryName, content.ToPdf(entryName));

                    foreach (var document in shipment.Documents.Where(d => !d.IsNew && !d.IsInternal))
                    {
                        var fileName = Server.MapPath(document.LocationPath);
                        if (!File.Exists(fileName)) continue;
                        var info = new FileInfo(fileName);
                        var name = string.Format("{0}_{1}{2}", shipment.ShipmentNumber, document.Name, info.Extension);
                        if (!zip.ContainsEntry(name))
                            zip.AddEntry(name, fileName.ReadFromFile());
                    }
                }

                foreach (var serviceTicket in serviceTickets)
                {
                    serviceTicket.LoadCollections();

                    foreach (var document in serviceTicket.Documents.Where(d => !d.IsNew && !d.IsInternal))
                    {
                        var fileName = Server.MapPath(document.LocationPath);
                        if (!File.Exists(fileName)) continue;
                        var info = new FileInfo(fileName);
                        var name = string.Format("{0}_{1}{2}", serviceTicket.ServiceTicketNumber, document.Name, info.Extension);
                        if (!zip.ContainsEntry(name))
                            zip.AddEntry(name, fileName.ReadFromFile());
                    }
                }

                var invoiceName = string.Format(InvoicePdfFileName, invoice.InvoiceNumber);
                content = this.GenerateInvoice(invoice, true);
                if (!string.IsNullOrEmpty(content))
                    zip.AddEntry(invoiceName, content.ToPdf(invoice.InvoiceNumber));
                Response.Export(zip.GetZippedBytes(), string.Format("{0:yyyyMMdd_hhmmss}_Document_Package.zip", DateTime.Now));
            }
        }
    }
}