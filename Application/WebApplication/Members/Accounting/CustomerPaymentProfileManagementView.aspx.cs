﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web.UI;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting
{
    public partial class CustomerPaymentProfileManagementView : MemberPageBase, ICustomerPaymentProfileManagerView
    {
        public static string PageAddress
        {
            get { return "~/Members/Accounting/CustomerPaymentProfileManagementView.aspx"; }
        }

        public override ViewCode PageCode
        {
            get { return ViewCode.CustomerPaymentProfileManagement; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.RegistryBlue; }
        }

        public event EventHandler<ViewEventArgs<string>> CustomerSearch;
        
        
        public void DisplayCustomer(Customer customer)
        {
            txtCustomerNumber.Text = customer == null ? string.Empty : customer.CustomerNumber;
            txtCustomerName.Text = customer == null ? string.Empty : customer.Name;
            hidCustomerId.Value = customer == null ? string.Empty : customer.Id.ToString();

            ppmPaymentProfileManager.LoadCustomerToManagePaymentProfiles(customer);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Button = MessageButton.Ok;
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;

            litErrorMessages.Text = messages.HasErrors()
                                        ? string.Join(WebApplicationConstants.HtmlBreak,
                                                      messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
                                                        Select(m => m.Message).ToArray())
                                        : string.Empty;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new CustomerPaymentProfileManagementHandler(this).Initialize();

            if (IsPostBack) return;

            aceCustomer.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.ActiveUser, ActiveUser.Id.GetString() } });

            txtCustomerNumber.Focus();
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            if (ActiveUser.TenantEmployee || ActiveUser.UserShipAs.Any()) return;

            if (!ActiveUser.DefaultCustomer.Active)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Customer is not active") });
                return;
            }

            DisplayCustomer(new Customer(ActiveUser.DefaultCustomer.Id));

            txtCustomerNumber.ReadOnly = true;
            txtCustomerNumber.BackColor = Color.LightGray;
            imgCustomerSearch.Enabled = false;
        }


        protected void OnCustomerNumberEntered(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtCustomerNumber.Text))
            {
                DisplayCustomer(null);
                return;
            }

            if (CustomerSearch != null)
                CustomerSearch(this, new ViewEventArgs<string>(txtCustomerNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
        }

        protected void OnCustomerSearchClicked(object sender, ImageClickEventArgs e)
        {
            customerFinder.Visible = true;
        }

        protected void OnCustomerFinderSelectionCancelled(object sender, EventArgs e)
        {
            customerFinder.Visible = false;
        }

        protected void OnCustomerFinderItemSelected(object sender, ViewEventArgs<Customer> e)
        {
            DisplayCustomer(e.Argument);
            customerFinder.Visible = false;
        }


        protected void OnPaymentProfileManagerProcessingMessages(object sender, ViewEventArgs<List<ValidationMessage>> e)
        {
            DisplayMessages(e.Argument);
        }
    }
}