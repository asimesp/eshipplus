﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;
using Ionic.Zip;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Documents;
using LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting
{
    public partial class InvoicePostingView : MemberPageBase, IInvoicePostingView
    {
        private SearchField SortByField
        {
            get { return AccountingSearchFields.InvoicePostingSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue); }
        }

        protected bool HasAccessToModifyInvoicePosting { get; set; }

        private const string PostArgs = "POST";
        private const string PostDownloadArgs = "DOWNLOAD";

        public static string PageAddress { get { return "~/Members/Accounting/InvoicePostingView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.InvoicePosting; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl =IconLinks.FinanceBlue; } }

        public event EventHandler<ViewEventArgs<List<Invoice>>> Post;
        public event EventHandler<ViewEventArgs<List<Invoice>>> PostDownload;
        public event EventHandler<ViewEventArgs<InvoiceViewSearchCriteria>> Search;

        public void HandlePostedInvoicesAndMessages(IEnumerable<Invoice> invoices, IEnumerable<ValidationMessage> messages)
        {
            if (invoices == null) return;

            DisplayMessages(messages); // send messages to screen

            ZipFile zip;
            using (zip = new ZipFile())
                foreach (var invoice in invoices)
                {
                    var entryName = string.Format("{0}.pdf", invoice.InvoiceNumber);
                    zip.AddEntry(entryName, this.GenerateInvoice(invoice, true).ToPdf(invoice.InvoiceNumber));
                }

            var name = string.Format("{0:yyyyMMdd_hhmmss_ffff}_Invoice_Batch.zip", DateTime.Now);
            var file = Path.Combine(Server.MapPath(WebApplicationSettings.TempFolder), name);

            hidFileName.Value = name;
            hidFilePath.Value = file;

            using (var stream = new FileStream(file, FileMode.Create))
                zip.Save(stream);

            pnlDimScreen.Visible = true;
            pnlDownloadDocument.Visible = true;

            // referesh page
            DoSearchPreProcessingThenSearch(SortByField);
        }

        public void ProcessNotifications(IEnumerable<Invoice> invoices)
        {
            if (chkDoNotSendNotifications.Checked) return;

            foreach (var invoice in invoices)
                this.NotifyPostedInvoice(invoice);
        }

        public void DisplaySearchResult(List<InvoiceDashboardDto> invoices)
        {
            litRecordCount.Text = invoices.BuildRecordCount();
            lstInvoiceDetails.DataSource = invoices;
            lstInvoiceDetails.DataBind();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Button = MessageButton.Ok;
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }


        private List<ToolbarMoreAction> ToolbarMoreActions()
        {
            var post = new ToolbarMoreAction
            {
                CommandArgs = PostArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.New,
                Name = "Post Invoice(s)",
                IsLink = false,
                NavigationUrl = string.Empty,
                EnableProcessingDiv = true
            };

            var postdownload = new ToolbarMoreAction
            {
                CommandArgs = PostDownloadArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.New,
                Name = "Post Invoice(s) & Download Invoice Document(s)",
                IsLink = false,
                NavigationUrl = string.Empty,
                EnableProcessingDiv = true
            };

            var moreActions = new List<ToolbarMoreAction>();

            if (ActiveUser.HasAccessToModify(ViewCode.InvoicePosting))
                moreActions.AddRange(new List<ToolbarMoreAction> { post, postdownload });
            else
                memberToolBar.ShowMore = false;

            return moreActions;
        }


        private List<Invoice> RetrieveSelectedInvoices()
        {
            var selected = lstInvoiceDetails.Items
                .Select(s => new
                {
                    Id = s.FindControl("hidInvoiceId").ToCustomHiddenField().Value.ToLong(),
                    Detail = ((InvoicePostingDetail2Control)s.FindControl("invoicePostingDetail2")),
                })
                .Where(s => s.Detail != null && s.Detail.Selected)
                .Select(s => new Invoice(s.Id))
                .Where(i => !i.Posted) // ensure only non posted invoices
                .ToList();

            return selected;
        }

        private void PostInvoices()
        {
            var invoices = RetrieveSelectedInvoices();

            if (!invoices.Any()) return;

            // check for auto notification present
            var noAutoNotification = invoices
                .Any(i =>
                {
                    var communication = i.CustomerLocation.Customer.Communication;
                    return communication == null || !communication.Notifications.Any(n => n.Milestone == Milestone.InvoicePosted && n.Enabled);
                });

            if (noAutoNotification && !chkDoNotSendNotifications.Checked)
            {
                const string msg =
                    @"You are posting invoices without the automatic download. One or more of these
					invoices have customer accounts with no automatic notification. Do you want to continue? y/n:";
                messageBox.Icon = MessageIcon.Question;
                messageBox.Button = MessageButton.YesNo;
                messageBox.Message = msg;
                messageBox.Visible = true;
                return;
            }

            if (Post != null)
                Post(this, new ViewEventArgs<List<Invoice>>(invoices));

            DoSearchPreProcessingThenSearch(SortByField);
        }

        private void PostDownloadInvoices()
        {
            var invoices = RetrieveSelectedInvoices();

            if (!invoices.Any()) return;

            if (PostDownload != null)
                PostDownload(this, new ViewEventArgs<List<Invoice>>(invoices));
        }

        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems
            {
                Columns = GetCurrentRunParameters(true),
                SortAscending = rbAsc.Checked,
                SortBy = SortByField,
            };
        }

        private void DoSearchPreProcessingThenSearch(SearchField field)
        {
            var criteria = (new InvoiceViewSearchCriteria
            {
                Parameters = GetCurrentRunParameters(false),
                ActiveUserId = ActiveUser.Id,
                SortBy = field,
                SortAscending = rbAsc.Checked
            });

            // filter for non posted only
            var postedParameter = AccountingSearchFields.Posted.ToParameterColumn();
            postedParameter.DefaultValue = false.ToString();
            postedParameter.Operator = Operator.Equal;
            criteria.Parameters.Add(postedParameter);

            DoSearch(criteria);
        }

        private void DoSearch(InvoiceViewSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<InvoiceViewSearchCriteria>(criteria));
            chkSelectAllRecords.Checked = false;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new InvoicePostingHandler(this).Initialize();

            HasAccessToModifyInvoicePosting = ActiveUser.HasAccessToModify(ViewCode.InvoicePosting);
            memberToolBar.LoadAdditionalActions(ToolbarMoreActions());

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            // set auto referesh
            tmRefresh.Interval = WebApplicationConstants.DefaultRefreshInterval.ToInt();

            // set sort fields
            ddlSortBy.DataSource = AccountingSearchFields.InvoicePostingSortFields.OrderBy(s => s.DisplayName);
            ddlSortBy.DataBind();
            ddlSortBy.SelectedValue = AccountingSearchFields.DocumentNumber.Name;

            //link up sort buttons
            lbtnSortInvoiceNumber.CommandName = AccountingSearchFields.InvoiceNumber.Name;
            lbtnSortInvoiceDate.CommandName = AccountingSearchFields.InvoiceDate.Name;
            lbtnSortUsername.CommandName = AccountingSearchFields.Username.Name;
            lbtnSortTypeText.CommandName = AccountingSearchFields.InvoiceTypeText.Name;
            lbtnSortAmountDue.CommandName = AccountingSearchFields.AmountDue.Name;
            lbtnSortPrefix.CommandName = AccountingSearchFields.PrefixCode.Name;

            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);
            var criteria = new InvoiceViewSearchCriteria
            {
                ActiveUserId = ActiveUser.Id,
                Parameters = profile != null
                                 ? profile.Columns
                                 : AccountingSearchFields.DefaultInvoices.Select(
                                     f => f.ToParameterColumn()).ToList(),
                SortAscending = profile == null || profile.SortAscending,
                SortBy = profile == null ? null : profile.SortBy
            };

            // ensure non posted invoices only
            var postedColumns = new List<ParameterColumn>();

            postedColumns.AddRange(criteria.Parameters.Where(c => c.ReportColumnName == AccountingSearchFields.Posted.ToParameterColumn().ReportColumnName));

            foreach (var column in postedColumns)
                criteria.Parameters.Remove(column);

            lstFilterParameters.DataSource = criteria.Parameters;
            lstFilterParameters.DataBind();

            rbAsc.Checked = criteria.SortAscending;
            rbDesc.Checked = !criteria.SortAscending;

            if (criteria.SortBy != null && ddlSortBy.ContainsValue(criteria.SortBy.Name))
                ddlSortBy.SelectedValue = criteria.SortBy.Name;
        }


        protected void OnToolbarCommand(object sender, ViewEventArgs<string> e)
        {
            switch (e.Argument)
            {
                case PostArgs:
                    PostInvoices();
                    break;
                case PostDownloadArgs:
                    PostDownloadInvoices();
                    break;
            }
        }

        protected void OnSearchClicked(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }


        protected void OnInvoiceDetailsItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var invoice = (InvoiceDashboardDto)item.DataItem;

            if (invoice == null) return;

            var detailControl = (InvoicePostingDetail2Control)item.FindControl("invoicePostingDetail2");
            detailControl.LoadInvoice(invoice);
        }

        
        protected void OnDownloadDocumentsClicked(object sender, EventArgs e)
        {
	        var info = new FileInfo(hidFilePath.Value);
            Response.Export(info, hidFileName.Value);
        }

        protected void OnDownloadDocumentsCloseClicked(object sender, EventArgs e)
        {
            hidFileName.Value = string.Empty;
            hidFilePath.Value = string.Empty;
            pnlDownloadDocument.Visible = false;
            pnlDimScreen.Visible = false;
        }


        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;
        }

        protected void OnYesProcess(object sender, EventArgs e)
        {
            if (Post != null)
                Post(this, new ViewEventArgs<List<Invoice>>(RetrieveSelectedInvoices()));

            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnNoProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = AccountingSearchFields.Invoices.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            //Remove posted as available parameter because
            //we set it in DoSearchPreProcessingThenSearch as a filter per original code
            parameterSelector.LoadParameterSelector(AccountingSearchFields.Invoices
                                .Where(f => f.Name != AccountingSearchFields.Posted.Name)
                                .ToList());
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;

        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSortData(object sender, CommandEventArgs e)
        {
            var field = AccountingSearchFields.InvoicePostingSortFields.FirstOrDefault(c => c.Name == e.CommandName);
            if (field == null) return;
            if (ddlSortBy.ContainsValue(field.Name))
            {
                if (ddlSortBy.SelectedValue == field.Name && rbAsc.Checked)
                {
                    rbAsc.Checked = false;
                    rbDesc.Checked = true;
                }
                else if (ddlSortBy.SelectedValue == field.Name && rbDesc.Checked)
                {
                    rbAsc.Checked = true;
                    rbDesc.Checked = false;
                }

                ddlSortBy.SelectedValue = field.Name;
            }

            DoSearchPreProcessingThenSearch(field);
        }

        protected void OnSortBySelectedIndexChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnSortOrderCheckChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns.Where(c => c.ReportColumnName != AccountingSearchFields.Posted.ToParameterColumn().ReportColumnName);
            lstFilterParameters.DataBind();

            rbAsc.Checked = e.Argument.SortAscending;
            rbDesc.Checked = !e.Argument.SortAscending;

            if (e.Argument.SortBy != null && ddlSortBy.ContainsValue(e.Argument.SortBy.Name))
                ddlSortBy.SelectedValue = e.Argument.SortBy.Name;
        }

        protected void OnSearchProfilesAutoRefreshChanged(object sender, ViewEventArgs<int> e)
        {
            tmRefresh.Enabled = e.Argument > default(int);
            if (e.Argument > 0) tmRefresh.Interval = e.Argument;
        }


        protected void OnTimerTick(object sender, EventArgs e)
        {
            var field = AccountingSearchFields.CustomerInvoiceSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue);
            DoSearchPreProcessingThenSearch(field);
        }
    }
}