﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="InvoicePayView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.InvoicePayView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowNew="false" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
                Pay Invoices
            </h3>
        </div>
        <span class="clearfix"></span>
        <hr class="dark mb10" />
        <div class="errorMsgLit">
            <asp:Literal runat="server" ID="litErrorMessages" />
        </div>
        <eShip:PaymentEntryControl runat="server" ID="pecPayment" Visible="False" CanModifyPaymentAmount="False" OnClose="OnPaymentEntryCloseClicked" OnProcessingMessages="OnPaymentEntryProcessingMessages"
            OnCustomAuthorizeAndCapturePayment="OnPaymentEntryCustomAuthorizeAndCapturePayment"/>

        <asp:Panel runat="server" ID="pnlNullTransfer" Visible="False">
            <table class="line2 pl2">
                <tr class="header">
                    <td>
                        <h5 class="red">TRANSFER ERROR!</h5>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p class="pb40 mt20 fs120em">
                            You are getting this error because you have accessed this page directly from your
						    web browser or you attempted to refresh the page after a prior process was complete.
                        </p>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlSuccessfulPayment" Visible="False">
            <table class="line2 pl2">
                <tr class="header">
                    <td>
                        <h5 class="green">PAYMENT PROCESSED SUCCESSFULLY</h5>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p class="pb40 mt20 fs120em">
                            Thank you for your payment.
                        </p>
                        <asp:Button runat="server" ID="btnReturnToCustomerInvoiceDashboard" Text="Return to Customer Invoice Dashboard" OnClick="OnReturnToCustomerInvoiceDashboardClicked" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlInvoicePayment">
            <h4>Customer Acount:
                <asp:Literal runat="server" ID="litCustomer" /></h4>
            <asp:Repeater runat="server" ID="rptInvoices" OnItemDataBound="OnInvoicesItemDataBound">
                <ItemTemplate>
                    <h5>Invoice
                        <eShip:CustomHiddenField runat="server" ID="hidInvoiceId" Value='<%# Eval("Id") %>' />
                        <asp:Literal runat="server" ID="litInvoiceNumber" Text='<%# Eval("InvoiceNumber") %>' />
                        <%# !string.IsNullOrEmpty(Eval("InvoiceNumber").ToString()) && ActiveUser.HasAccessTo(ViewCode.Invoice) 
                            ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Invoice Record'><img src={3} width='20' class='middle'/></a>", 
                                                    ResolveUrl(InvoiceView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    Eval("InvoiceNumber"),
                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
                            : string.Empty %>
                    </h5>
                    <table class="stripe">
                        <tr>
                            <th style="width: 20%;">Reference Number
                            </th>
                            <th style="width: 30%;">Reference Type
                            </th>
                            <th style="width: 50%;" class="text-right">Amount Due
                            </th>
                        </tr>
                        <asp:Repeater runat="server" ID="rptInvoiceDetails">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <%# Eval("ReferenceNumber") %>
                                    </td>
                                    <td>
                                        <%# Eval("ReferenceType") %>
                                    </td>
                                    <td class="text-right">
                                        <%# Eval("AmountDue", "{0:c4}") %>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <div class="row">
                        <div class="fieldgroup right">
                            <label class="lhInherit">Amount Due: </label>
                            <label class="w90 text-right lhInherit"><%# Eval("TotalAmountDue").ToDecimal().ToString("c4") %></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="fieldgroup right">
                            <label class="lhInherit">Paid Amount: </label>
                            <label class="w90 text-right lhInherit"><%# Eval("PaidAmount").ToDecimal().ToString("c4") %></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="fieldgroup right">
                            <label class="lhInherit">Balance Due: </label>
                            <label class="w90 text-right lhInherit"><%# (Eval("TotalAmountDue").ToDecimal() - Eval("PaidAmount").ToDecimal()).ToString("c4") %></label>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>

            <h4 class="text-right pr5 pt10 pb10">Total:
                <asp:Literal runat="server" ID="litTotal" />
            </h4>
            <div class="rowgroup">
                <div class="row">
                    <div class="fieldgroup right">
                        <asp:Button runat="server" ID="btnPay" Text="Pay" OnClick="OnPayClicked" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlCouldNotGetAllLocks" Visible="False">
            <div class="popup popupControlOverW500">
                <div class="popheader">
                    <h4>MESSAGES</h4>
                </div>
                <div class="row pl10 pt10 pb10">
                    <div class="fieldgroup">
                        Currently the following invoice records are being reviewed and updated by Logistics Plus operators and it may take a few minutes complete:
                        <asp:Literal runat="server" ID="litInvoiceNumbersForFailedLocks" />
                    </div>
                </div>
                <div class="row">
                    <div class="fieldgroup right pr10 pb10">
                        <asp:Button runat="server" ID="btnRefresh" Text="Refresh" OnClick="OnRefreshInvoiceLocksClicked" />
                        <asp:Button runat="server" ID="btnReturnToCustomerInvoiceDashboard2" Text="Return to Customer Invoice Dashboard" OnClick="OnReturnToCustomerInvoiceDashboardClicked" />
                    </div>
                </div>
            </div>
        </asp:Panel>

    </div>
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
</asp:Content>
