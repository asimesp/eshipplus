﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting
{
    public partial class MiscReceiptApplicationView : MemberPageBase, IMiscReceiptApplicationView
    {
        private const string UpdatePageFlag = "UP";

        public static string PageAddress { get { return "~/Members/Accounting/MiscReceiptApplicationView.aspx"; } }
        
        public override ViewCode PageCode
        {
            get { return ViewCode.MiscReceiptApplication; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.FinanceBlue; }
        }


        public event EventHandler<ViewEventArgs<List<MiscReceiptApplication>>> ApplyMiscReceiptToInvoices;
        public event EventHandler<ViewEventArgs<MiscReceiptApplication>> Save; 
        public event EventHandler<ViewEventArgs<MiscReceiptApplication>> Delete;


        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == UpdatePageFlag)
            {
                LoadMiscReceipt(new MiscReceipt(hidMiscReceiptId.Value.ToLong()));
                pnlEditMiscReceiptApplication.Visible = false;
                pnlDimScreen.Visible = false;
            }

            hidFlag.Value = string.Empty;
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }


        private void LoadMiscReceipt(MiscReceipt miscReceipt)
        {
            if (miscReceipt == null || miscReceipt.IsNew)
            {
                pnlError.Visible = true;
                pnlMiscReceipt.Visible = false;
                return;
            }

            hidMiscReceiptId.Value = miscReceipt.Id.GetString();
            
            txtShipmentNumber.Text = miscReceipt.ShipmentId != default(long) ? miscReceipt.Shipment.ShipmentNumber : string.Empty;
            txtAmountPaid.Text = miscReceipt.AmountPaid.ToString("c4");
            txtAmountLeftToBeApplied.Text = miscReceipt.AmountLeftToBeAppliedOrRefunded(miscReceipt.TenantId).ToString("c4");
            hidUserId.Value = miscReceipt.UserId.ToString();
            txtUser.Text = miscReceipt.User.FullName;
            hidCustomerId.Value = miscReceipt.CustomerId.ToString();
            txtCustomer.Text = miscReceipt.Customer.CustomerNumber;
            txtPaymentDate.Text = miscReceipt.PaymentDate.FormattedLongDate();
            txtGatewayTransactionId.Text = miscReceipt.GatewayTransactionId;
            txtPaymentGatewayType.Text = miscReceipt.PaymentGatewayType.FormattedString();

            pnlMiscReceipt.Visible = true;
            pnlError.Visible = false;

            rptMiscReceiptApplications.DataSource = miscReceipt.MiscReceiptApplications;
            rptMiscReceiptApplications.DataBind();

            chkSelectAllRecords.Checked = false;
            rptInvoices.DataSource = new InvoiceSearch().FetchUnpaidPostedInvoiceDtosForShipment(miscReceipt.ShipmentId != default(long) ? miscReceipt.Shipment.ShipmentNumber : string.Empty, ActiveUser.Id, ActiveUser.TenantId);
            rptInvoices.DataBind();
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new MiscReceiptApplicationHandler(this).Initialize();

            if (IsPostBack) return;

            if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.TransferNumber]))
            {
                LoadMiscReceipt(new MiscReceipt(Request.QueryString[WebApplicationConstants.TransferNumber].UrlTextDecrypt().ToLong()));
            }
            else
            {
                pnlError.Visible = true;
                pnlMiscReceipt.Visible = false;
            }
        }


        protected void OnDeleteMiscReceiptApplicationClicked(object sender, ImageClickEventArgs e)
        {
            var application = new MiscReceiptApplication(((Control)sender).FindControl("hidMiscReceiptApplicationId").ToCustomHiddenField().Value.ToLong());
            hidFlag.Value = UpdatePageFlag;
            if(Delete != null)
                Delete(this, new ViewEventArgs<MiscReceiptApplication>(application));
        }

        protected void OnApplyMiscReceiptClicked(object sender, EventArgs e)
        {
            var applications = rptInvoices
                .Items
                .Cast<RepeaterItem>()
                .Where(i => i.FindControl("chkSelected").ToAltUniformCheckBox().Checked)
                .Select(i => new MiscReceiptApplication
                    {
                        ApplicationDate = DateTime.Now,
                        Amount = i.FindControl("txtAmountToApply").ToTextBox().Text.ToDecimal(),
                        InvoiceId = i.FindControl("hidInvoiceId").ToCustomHiddenField().Value.ToLong(),
                        MiscReceiptId = hidMiscReceiptId.Value.ToLong(),
                        TenantId = ActiveUser.TenantId,
                    })
                .ToList();

            if (!applications.Any())
            {
                DisplayMessages(new []{ ValidationMessage.Error("No Invoices have been selected to be applied to.")});
                return;
            }

            hidFlag.Value = UpdatePageFlag;
            if(ApplyMiscReceiptToInvoices != null)
                ApplyMiscReceiptToInvoices(this, new ViewEventArgs<List<MiscReceiptApplication>>(applications));
        }

        protected void OnModifyMiscReceiptApplicationClicked(object sender, ImageClickEventArgs e)
        {
            pnlEditMiscReceiptApplication.Visible = true;
            pnlDimScreen.Visible = true;

            var application = new MiscReceiptApplication(((Control)sender).FindControl("hidMiscReceiptApplicationId").ToCustomHiddenField().Value.ToLong());

            hidEditMiscReceiptApplicationId.Value = application.Id.GetString();
            txtEditMiscReceiptApplicationAmount.Text = application.Amount.GetString();
        }


        protected void OnCancelClicked(object sender, EventArgs e)
        {
            pnlEditMiscReceiptApplication.Visible = false;
            pnlDimScreen.Visible = false;

            hidEditMiscReceiptApplicationId.Value = string.Empty;
            txtEditMiscReceiptApplicationAmount.Text = string.Empty;
        }

        protected void OnSaveMiscReceiptApplicationClicked(object sender, EventArgs e)
        {
            var application = new MiscReceiptApplication(hidEditMiscReceiptApplicationId.Value.ToLong(), true)
                {
                    Amount = txtEditMiscReceiptApplicationAmount.Text.ToDecimal()
                };

            hidFlag.Value = UpdatePageFlag;
            if(Save != null)
                Save(this, new ViewEventArgs<MiscReceiptApplication>(application));
        }


        protected void OnAddInvoicesToBeAppliedToClicked(object sender, EventArgs e)
        {
            invoiceFinder.Visible = true;
        }

        protected void OnInvoiceFinderMultiItemSelected(object sender, ViewEventArgs<List<Invoice>> e)
        {
            var invoices = rptInvoices
                .Items
                .Cast<RepeaterItem>()
                .Select(i => new InvoiceDashboardDto(new Invoice(i.FindControl("hidInvoiceId").ToCustomHiddenField().Value.ToLong())))
                .ToList();

            invoices.AddRange(e.Argument.Where(i => invoices.All(inv => inv.Id != i.Id)).Select(i => new InvoiceDashboardDto(i)));

            rptInvoices.DataSource = invoices;
            rptInvoices.DataBind();

            invoiceFinder.Visible = false;
        }

        protected void OnInvoiceFinderItemCancelled(object sender, EventArgs e)
        {
            invoiceFinder.Visible = false;
        }
    }
}