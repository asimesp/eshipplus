﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting
{
    public partial class AverageWeeklyFuelView : MemberPageBase, IAverageWeeklyFuelView
    {
        private const string ExportFlag = "E";

        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public static string PageAddress
        {
            get { return "~/Members/Accounting/AverageWeeklyFuelView.aspx"; }
        }

        public override ViewCode PageCode { get { return ViewCode.AverageWeeklyFuel; } }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.FinanceBlue; }
        }

        public event EventHandler<ViewEventArgs<AverageWeeklyFuel>> Save;
        public event EventHandler<ViewEventArgs<AverageWeeklyFuel>> Delete;
        public event EventHandler<ViewEventArgs<AverageWeeklyFuel>> Lock;
        public event EventHandler<ViewEventArgs<AverageWeeklyFuel>> UnLock;
        public event EventHandler<ViewEventArgs<AverageWeeklyFuelViewSearchCriteria>> Search;
        public event EventHandler<ViewEventArgs<List<AverageWeeklyFuel>>> BatchImport;

        public long GetChargeCodeId(string chargeCodeCode, Dictionary<string, long> chargeCodeIds, ChargeCodeSearch search)
        {
            if (chargeCodeIds.ContainsKey(chargeCodeCode)) return chargeCodeIds[chargeCodeCode];
            var id = search.FetchChargeCodeIdByCode(chargeCodeCode, ActiveUser.TenantId);
            if (id != default(long)) chargeCodeIds.Add(chargeCodeCode, id);
            return id;
        }

        public void FailedLock(Lock @lock)
        {
            btnSave.Enabled = false;

            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void DisplaySearchResult(List<AverageWeeklyFuelViewSearchDto> averageWeeklyFuels)
        {
            if (averageWeeklyFuels.Count == 0)
            {
                DisplayMessages(new[] { ValidationMessage.Information("The search yielded no results.") });
                return;
            }

            litRecordCount.Text = averageWeeklyFuels.BuildRecordCount();
            lvwRegistry.DataSource = averageWeeklyFuels;
            lvwRegistry.DataBind();

            if (hidFlag.Value == ExportFlag)
            {
                var lines = averageWeeklyFuels
                    .Select(a => new[]
					             	{
										a.EffectiveDate.FormattedShortDate(),
					             		a.ChargeCode,
					             		a.EastCoastCost.ToString(),
					             		a.NewEnglandCost.ToString(),
					             		a.CentralAtlanticCost.ToString(),
					             		a.LowerAtlanticCost.ToString(),
					             		a.MidwestCost.ToString(),
					             		a.GulfCoastCost.ToString(),
					             		a.RockyMountainCost.ToString(),
					             		a.WestCoastCost.ToString(),
					             		a.WestCoastLessCaliforniaCost.ToString(),
					             		a.CaliforniaCost.ToString(),
					             		a.NationalCost.ToString()
					             	}.TabJoin())
                    .ToList();
                lines.Insert(0,
                             new[]
				             	{
				             		"Effective Date", "Charge Code", "East Coast", "New England", "Cen. Atlantic", "Low. Atlantic",
				             		"Midwest", "Gulf Coast", "Rocky Mtn.", "West Coast", "West Coast Less California", "California", "National"
				             	}.TabJoin());

                hidFlag.Value = string.Empty;

                Response.Export(lines.ToArray().NewLineJoin(), "AverageWeeklyFuels.txt");
            }
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() && !messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        private void DoSearch()
        {
            var criteria = new AverageWeeklyFuelViewSearchCriteria
            {
                TenantId = ActiveUser.TenantId,
                Criteria = string.Empty,
                StartDate = txtStartDate.Text.ToDateTime().TimeToMinimum(),
                EndDate = txtEndDate.Text.ToDateTime().TimeToMaximum()
            };

            if (Search != null)
                Search(this, new ViewEventArgs<AverageWeeklyFuelViewSearchCriteria>(criteria));
        }

        private AverageWeeklyFuel RetrieveAverageWeeklyFuel()
        {
            var averageWeeklyFuelId = hidAverageWeeklyFuelId.Value.ToLong();

            var averageWeeklyFuel = new AverageWeeklyFuel(averageWeeklyFuelId, averageWeeklyFuelId != default(long))
            {
                EffectiveDate = txtEffectiveDate.Text.ToDateTime(),
                ChargeCodeId = ddlChargeCodes.SelectedValue.ToLong(),
                EastCoastCost = txtEastCoastCost.Text.ToDecimal(),
                NewEnglandCost = txtNewEnglandCost.Text.ToDecimal(),
                CentralAtlanticCost = txtCentralAtlanticCost.Text.ToDecimal(),
                LowerAtlanticCost = txtLowerAtlanticCost.Text.ToDecimal(),
                MidwestCost = txtMidwestCost.Text.ToDecimal(),
                GulfCoastCost = txtGulfCoastCost.Text.ToDecimal(),
                RockyMountainCost = txtRockyMountainCost.Text.ToDecimal(),
                WestCoastCost = txtWestCoastCost.Text.ToDecimal(),
                WestCoastLessCaliforniaCost = txtWestCoastLessCaliforniaCost.Text.ToDecimal(),
                CaliforniaCost = txtCaliforniaCost.Text.ToDecimal(),
                NationalCost = txtNationalCost.Text.ToDecimal(),
            };


            if (averageWeeklyFuel.IsNew)
            {
                averageWeeklyFuel.TenantId = ActiveUser.TenantId;
                averageWeeklyFuel.DateCreated = DateTime.Now;
            }

            return averageWeeklyFuel;
        }

        private void CloseEditPopup()
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<AverageWeeklyFuel>(new AverageWeeklyFuel(hidAverageWeeklyFuelId.Value.ToLong(), false)));
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new AverageWeeklyFuelHandler(this);
            handler.Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowImport = Access.Modify;
            memberToolBar.ShowNew = Access.Modify;

            if (IsPostBack) return;
            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.AverageWeeklyFuelImportTemplate });

            txtStartDate.Text = DateTime.Now.AddMonths(-1).FormattedShortDate();
            txtEndDate.Text = DateTime.Now.FormattedShortDate();

            DoSearch();
        }


        private void LoadAverageWeeklyFuelForEdit(AverageWeeklyFuel averageWeeklyFuel)
        {
            hidAverageWeeklyFuelId.Value = averageWeeklyFuel.Id.ToString();

            txtEffectiveDate.Text = averageWeeklyFuel.EffectiveDate.FormattedShortDate();

			ddlChargeCodes.SelectedValue = averageWeeklyFuel.ChargeCodeId.GetString();
			
            txtEastCoastCost.Text = averageWeeklyFuel.EastCoastCost.ToString();
            txtNewEnglandCost.Text = averageWeeklyFuel.NewEnglandCost.ToString();
            txtCentralAtlanticCost.Text = averageWeeklyFuel.CentralAtlanticCost.ToString();
            txtLowerAtlanticCost.Text = averageWeeklyFuel.LowerAtlanticCost.ToString();
            txtMidwestCost.Text = averageWeeklyFuel.MidwestCost.ToString();
            txtGulfCoastCost.Text = averageWeeklyFuel.GulfCoastCost.ToString();
            txtRockyMountainCost.Text = averageWeeklyFuel.RockyMountainCost.ToString();
            txtWestCoastCost.Text = averageWeeklyFuel.WestCoastCost.ToString();
            txtWestCoastLessCaliforniaCost.Text = averageWeeklyFuel.WestCoastLessCaliforniaCost.ToString();
            txtCaliforniaCost.Text = averageWeeklyFuel.CaliforniaCost.ToString();
            txtNationalCost.Text = averageWeeklyFuel.NationalCost.ToString();
        }

        protected void OnNewClick(object sender, EventArgs e)
        {
            hidAverageWeeklyFuelId.Value = string.Empty;

            LoadAverageWeeklyFuelForEdit(new AverageWeeklyFuel { EffectiveDate = DateTime.Now });

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = true;

            txtEffectiveDate.Focus();
        }

        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("averageWeeklyFuelId").ToCustomHiddenField();

            var averageWeeklyFuel = new AverageWeeklyFuel(hidden.Value.ToLong(), false);

            LoadAverageWeeklyFuelForEdit(averageWeeklyFuel);

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = true;

            if (Lock != null)
                Lock(this, new ViewEventArgs<AverageWeeklyFuel>(averageWeeklyFuel));
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            var averageWeeklyFuel = RetrieveAverageWeeklyFuel();

            if (Save != null)
                Save(this, new ViewEventArgs<AverageWeeklyFuel>(averageWeeklyFuel));

            DoSearch();
        }

        protected void OnCloseClicked(object sender, EventArgs eventArgs)
        {
            CloseEditPopup();
        }

        protected void OnDeleteClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("averageWeeklyFuelId").ToCustomHiddenField();

            if (Delete != null)
                Delete(this, new ViewEventArgs<AverageWeeklyFuel>(new AverageWeeklyFuel(hidden.Value.ToLong(), true)));

            DoSearch();
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            DoSearch();
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            hidFlag.Value = ExportFlag;
            DoSearch();
        }


        protected void OnImportClicked(object sender, EventArgs e)
        {
            fileUploader.Title = "AVERAGE WEEKLY FUEL IMPORT";
            fileUploader.Instructions = @"**Please refer to user guide for import file format.";
            fileUploader.Visible = true;
        }

        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }


            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 13;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var chargeCodes = ProcessorVars.RegistryCache[ActiveUser.TenantId].ChargeCodes.Select(c => c.Code).ToList();
            msgs.AddRange(chks.CodesAreValid(1, chargeCodes, new ChargeCode().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var chargeCodeSearch = new ChargeCodeSearch();
            var chargeCodeIds = new Dictionary<string, long>();

            var averageWeeklyFuel = lines
                .Select(s => new AverageWeeklyFuel
                {
                    EffectiveDate = s[0].ToDateTime(),
                    ChargeCodeId = GetChargeCodeId(s[1], chargeCodeIds, chargeCodeSearch), // this is actually filled by charge code id.
                    EastCoastCost = s[2].ToDecimal(),
                    NewEnglandCost = s[3].ToDecimal(),
                    CentralAtlanticCost = s[4].ToDecimal(),
                    LowerAtlanticCost = s[5].ToDecimal(),
                    MidwestCost = s[6].ToDecimal(),
                    GulfCoastCost = s[7].ToDecimal(),
                    RockyMountainCost = s[8].ToDecimal(),
                    WestCoastCost = s[9].ToDecimal(),
                    WestCoastLessCaliforniaCost = s[10].ToDecimal(),
                    CaliforniaCost = s[11].ToDecimal(),
                    NationalCost = s[12].ToDecimal(),
                    DateCreated = DateTime.Now,
                    TenantId = ActiveUser.TenantId
                })
                .ToList();

            if (BatchImport != null)
                BatchImport(this, new ViewEventArgs<List<AverageWeeklyFuel>>(averageWeeklyFuel));
            fileUploader.Visible = false;
            DoSearch();
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }


        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (!CloseEditPopUp) return;

            CloseEditPopup();

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<AverageWeeklyFuel>(RetrieveAverageWeeklyFuel()));
        }
    }
}