﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting
{
    public partial class BatchPaymentApplicationView : MemberPageBase, IBatchPaymentApplicationView
    {
        private SearchField SortByField
        {
            get { return AccountingSearchFields.BatchPaymentApplicationSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue); }
        }

        public override ViewCode PageCode { get { return ViewCode.BatchPaymentApplication; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.FinanceBlue; } }

        public static string PageAddress { get { return "~/Members/Accounting/BatchPaymentApplicationView.aspx"; } }


        public event EventHandler<ViewEventArgs<MiscReciptViewSearchCriteria>> Search;
        public event EventHandler<ViewEventArgs<List<MiscReceiptApplication>>> ApplyMiscReceiptToInvoices;


        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplaySearchResult(List<MiscReceiptViewSearchDto> miscReceipts)
        {
            litRecordCount.Text = miscReceipts.BuildRecordCount();
            upcseDataUpdate.DataSource = miscReceipts;
            upcseDataUpdate.DataBind();
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }


        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems
            {
                Columns = GetCurrentRunParameters(true),
                SortAscending = rbAsc.Checked,
                SortBy = SortByField,
            };
        }

        private void DoSearchPreProcessingThenSearch(SearchField field)
        {
            DoSearch(new MiscReciptViewSearchCriteria
            {
                Parameters = GetCurrentRunParameters(false),
                SortBy = field,
                SortAscending = rbAsc.Checked
            });
        }

        private void DoSearch(MiscReciptViewSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<MiscReciptViewSearchCriteria>(criteria));
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new BatchPaymentApplicationHandler(this).Initialize();

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            // set sort fields
            ddlSortBy.DataSource = AccountingSearchFields.BatchPaymentApplicationSortFields.OrderBy(s => s.DisplayName);
            ddlSortBy.DataBind();
            ddlSortBy.SelectedValue = AccountingSearchFields.ShipmentNumber.Name;

            lbtnSortShipmentNumber.CommandName = AccountingSearchFields.ShipmentNumber.Name;
            lbtnSortLoadOrderNumber.CommandName = AccountingSearchFields.LoadOrderNumber.Name;
            lbtnSortAmountPaid.CommandName = AccountingSearchFields.AmountPaid.Name;
            lbtnSortGatewayTransactionId.CommandName = AccountingSearchFields.GatewayTransactionId.Name;
            lbtnAmountLeftToBeApplied.CommandName = AccountingSearchFields.AppliableAmount.Name;

            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);

            var criteria = new MiscReciptViewSearchCriteria
            {
                Parameters = profile != null
                                 ? profile.Columns
                                 : AccountingSearchFields.DefaultMiscReceipts.Select(f => f.ToParameterColumn()).ToList(),
                SortAscending = profile == null || profile.SortAscending,
                SortBy = profile == null ? null : profile.SortBy
            };

            lstFilterParameters.DataSource = criteria.Parameters;
            lstFilterParameters.DataBind();

            rbAsc.Checked = criteria.SortAscending;
            rbDesc.Checked = !criteria.SortAscending;

            if (criteria.SortBy != null && ddlSortBy.ContainsValue(criteria.SortBy.Name))
                ddlSortBy.SelectedValue = criteria.SortBy.Name;
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            upcseDataUpdate.ResetLoadCount();
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnToolbarSave(object sender, EventArgs e)
        {
            var miscReceiptApplications = new List<MiscReceiptApplication>();

            foreach (var i in rptMiscReceipts.Items.Cast<RepeaterItem>().Where(i => i.FindControl("chkSelected").ToAltUniformCheckBox().Checked))
            {
                var miscReceiptId = i.FindControl("hidMiscReceiptId").ToCustomHiddenField();
                var rptApplicableInvoices = i.FindControl("rptApplicableInvoices").ToRepeater();

                var invoiceApps = rptApplicableInvoices
                    .Items
                    .Cast<RepeaterItem>()
                    .Where(n => n.FindControl("chkInvoiceSelected").ToAltUniformCheckBox().Checked)
                    .Select(n => new MiscReceiptApplication
                        {
                            ApplicationDate = DateTime.Now,
                            Amount = n.FindControl("txtAmountToApply").ToTextBox().Text.ToDecimal(),
                            InvoiceId = n.FindControl("hidInvoiceId").ToCustomHiddenField().Value.ToLong(),
                            MiscReceiptId = miscReceiptId.Value.ToLong(),
                            TenantId = ActiveUser.TenantId,
                        })
                    .ToList();

                if (!invoiceApps.Any()) continue;
                miscReceiptApplications.AddRange(invoiceApps);
            }

            if (!miscReceiptApplications.Any())
            {
                DisplayMessages(new[] { ValidationMessage.Error("No invoices were selected for payment.") });
                return;
            }

            if (ApplyMiscReceiptToInvoices != null)
                ApplyMiscReceiptToInvoices(this, new ViewEventArgs<List<MiscReceiptApplication>>(miscReceiptApplications));

            chkSelectAllRecords.Checked = false;
            DoSearchPreProcessingThenSearch(SortByField);
        }


        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = AccountingSearchFields.MiscReceipts.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

        }

        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(AccountingSearchFields.MiscReceipts);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;

        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();

            rbAsc.Checked = e.Argument.SortAscending;
            rbDesc.Checked = !e.Argument.SortAscending;

            if (e.Argument.SortBy != null && ddlSortBy.ContainsValue(e.Argument.SortBy.Name))
                ddlSortBy.SelectedValue = e.Argument.SortBy.Name;
        }


        protected void OnSortBySelectedIndexChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnSortOrderCheckChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnSortData(object sender, CommandEventArgs e)
        {
            var field = AccountingSearchFields.BatchPaymentApplicationSortFields.FirstOrDefault(c => c.Name == e.CommandName);
            if (field == null) return;
            if (ddlSortBy.ContainsValue(field.Name))
            {
                if (ddlSortBy.SelectedValue == field.Name && rbAsc.Checked)
                {
                    rbAsc.Checked = false;
                    rbDesc.Checked = true;
                }
                else if (ddlSortBy.SelectedValue == field.Name && rbDesc.Checked)
                {
                    rbAsc.Checked = true;
                    rbDesc.Checked = false;
                }

                ddlSortBy.SelectedValue = field.Name;
            }
            DoSearchPreProcessingThenSearch(field);
        }


        protected void OnMiscReceiptsItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var item = e.Item;

            if (item == null) return;

            var miscReceiptDto = (MiscReceiptViewSearchDto)item.DataItem;

            var rptApplicableInvoices = item.FindControl("rptApplicableInvoices").ToRepeater();
            rptApplicableInvoices.DataSource = new InvoiceSearch().FetchUnpaidPostedInvoiceDtosForShipment(miscReceiptDto.ShipmentNumber, ActiveUser.Id, ActiveUser.TenantId);
            rptApplicableInvoices.DataBind();
        }
    }
}