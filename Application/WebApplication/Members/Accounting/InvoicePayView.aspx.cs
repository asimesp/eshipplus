﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.PaymentGateway;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting
{
    public partial class InvoicePayView : MemberPageBase, IInvoicePayView
    {
        private const string ProcessingPaymentFlag = "ProcessingPaymentFlag";

        public override ViewCode PageCode
        {
            get { return ViewCode.InvoicePay; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.FinanceBlue; }
        }

        public static string PageAddress
        {
            get { return "~/Members/Accounting/InvoicePayView.aspx"; }
        }

        public event EventHandler<ViewEventArgs<List<Invoice>>> LockInvoices;
        public event EventHandler<ViewEventArgs<MiscReceipt, PaymentInformation>> ProcessPayment;

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            var messagesList = messages.ToList();

            if (hidFlag.Value == ProcessingPaymentFlag)
            {
                if (messagesList.HasErrors() || messagesList.HasWarnings())
                {
                    messagesList.Add(ValidationMessage.Error("Due to the errors no payment has gone through."));
                }
                else
                {
                    pnlSuccessfulPayment.Visible = true;
                    pnlInvoicePayment.Visible = false;
                    pecPayment.Clear();
                    pecPayment.Visible = false;
                }
            }

            hidFlag.Value = string.Empty;

            messageBox.Icon = messagesList.GenerateIcon();
            messageBox.Button = MessageButton.Ok;
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messagesList.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;

            litErrorMessages.Text = messagesList.HasErrors()
                                        ? string.Join(WebApplicationConstants.HtmlBreak,
                                                      messagesList.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
                                                        Select(m => m.Message).ToArray())
                                        : string.Empty;
        }

        public void FailedLock(List<string> invoices)
        {
            litInvoiceNumbersForFailedLocks.Text = string.Join(", ", invoices);
            pnlCouldNotGetAllLocks.Visible = true;
            pnlDimScreen.Visible = true;
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new InvoicePayHandler(this).Initialize();

            if (IsPostBack) return;

            pecPayment.PaymentGatewayType = ActiveUser.Tenant.PaymentGatewayType;

            var invoices = Session[WebApplicationConstants.TransferListOfInvoicesObject] as List<Invoice>;
            if (invoices == null || !invoices.Any())
            {
                pnlNullTransfer.Visible = true;
                pnlInvoicePayment.Visible = false;
                return;
            }

            var invoicesToBind = invoices.Select(i => new
            {
                i.Id,
                i.InvoiceNumber,
                i.PaidAmount,
                TotalAmountDue = i.Details.Sum(id => id.AmountDue),
                i.Details
            });

            rptInvoices.DataSource = invoicesToBind;
            rptInvoices.DataBind();
            var customer = invoices.First().CustomerLocation.Customer;
            litCustomer.Text = string.Format("{0} - {1}", customer.CustomerNumber, customer.Name);
            litTotal.Text = invoices.Sum(i => i.Details.Sum(d => d.AmountDue) - i.PaidAmount).ToString("c4");

            Session[WebApplicationConstants.TransferListOfInvoicesObject] = null;

            if (LockInvoices != null)
                LockInvoices(this, new ViewEventArgs<List<Invoice>>(invoices));
        }


        protected void OnPayClicked(object sender, EventArgs e)
        {
            var invoices = rptInvoices
                .Items
                .Cast<RepeaterItem>()
                .Select(i => new Invoice(i.FindControl("hidInvoiceId").ToCustomHiddenField().Value.ToLong()))
                .ToList();

            pecPayment.Clear();
            pecPayment.LoadInvoicesToBePaidFor(invoices);
            pecPayment.Visible = true;
        }


        protected void OnPaymentEntryCloseClicked(object sender, EventArgs e)
        {
            pecPayment.Clear();
            pecPayment.Visible = false;
        }

        protected void OnPaymentEntryProcessingMessages(object sender, ViewEventArgs<List<ValidationMessage>> e)
        {
            DisplayMessages(e.Argument);
        }

        protected void OnPaymentEntryCustomAuthorizeAndCapturePayment(object sender, ViewEventArgs<PaymentInformation> e)
        {
            var invoices = rptInvoices
                .Items
                .Cast<RepeaterItem>()
                .Select(i => new Invoice(i.FindControl("hidInvoiceId").ToCustomHiddenField().Value.ToLong())).ToList();
            var paymentInfo = e.Argument;

            // assume always > 0 invoices due to Page_Load validation
            var miscReceipt = new MiscReceipt
            {
                AmountPaid = paymentInfo.ChargeAmount,
                PaymentDate = DateTime.Now,
                CustomerId = invoices.First().CustomerLocation.Customer.Id,
                PaymentGatewayType = ActiveUser.Tenant.PaymentGatewayType,
                ShipmentId = default(long),
                LoadOrderId = default(long),
                TenantId = ActiveUser.TenantId,
                Reversal = false,
                UserId = ActiveUser.Id,
                GatewayTransactionId = string.Empty,
                MiscReceiptApplications = new List<MiscReceiptApplication>(),
                NameOnCard = paymentInfo.NameOnCard,
                PaymentProfileId = paymentInfo.CustomerPaymentProfileId,
                OriginalMiscReceiptId = default(long),
            };

            var applications = invoices
               .Select(i => new MiscReceiptApplication
               {
                   Amount = i.Details.Sum(d => d.AmountDue) - i.PaidAmount,
                   Invoice = i,
                   TenantId = ActiveUser.TenantId,
                   ApplicationDate = DateTime.Now,
                   MiscReceipt = miscReceipt
               })
               .ToList();

            miscReceipt.MiscReceiptApplications = applications;

            if (applications.Sum(mra => mra.Amount) != e.Argument.ChargeAmount)
            {
                DisplayMessages(new []{ValidationMessage.Error("Amount to be paid does not match payment amount entered. Please refresh your web browser.")});
                return;
            }

            hidFlag.Value = ProcessingPaymentFlag;

            if(ProcessPayment != null)
                ProcessPayment(this, new ViewEventArgs<MiscReceipt, PaymentInformation>(miscReceipt, e.Argument));
        }


        protected void OnInvoicesItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item == null) return;

            var rptInvoiceDetails = e.Item.FindControl("rptInvoiceDetails").ToRepeater();
            if (rptInvoiceDetails == null) return;

            var details = e.Item.DataItem.GetPropertyValue("Details") as List<InvoiceDetail>;
            if (details == null) return;
            var groupedDetails = details
                .GroupBy(l => l.ReferenceNumber)
                .Select(cl => new
                    {
                        cl.First().ReferenceNumber,
                        cl.First().ReferenceType,
                        AmountDue = cl.Sum(c => c.AmountDue).ToString("c4"),
                    })
                .ToList();
            rptInvoiceDetails.DataSource = groupedDetails;
            rptInvoiceDetails.DataBind();
        }

        
        protected void OnRefreshInvoiceLocksClicked(object sender, EventArgs e)
        {
            var invoices = rptInvoices
                .Items
                .Cast<RepeaterItem>()
                .Select(i => new Invoice(i.FindControl("hidInvoiceId").ToCustomHiddenField().Value.ToLong()))
                .ToList();

            pnlCouldNotGetAllLocks.Visible = false;
            pnlDimScreen.Visible = false;

            if (LockInvoices != null)
                LockInvoices(this, new ViewEventArgs<List<Invoice>>(invoices));
        }

        protected void OnReturnToCustomerInvoiceDashboardClicked(object sender, EventArgs e)
        {
            Response.Redirect(CustomerInvoicesDashboardView.PageAddress);
        }
    }
}