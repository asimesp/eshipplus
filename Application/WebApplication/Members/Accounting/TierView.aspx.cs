﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting
{
    public partial class TierView : MemberPageBase, ITierView
    {
        private const string CustomersHeader = "Customers";

        private const string SaveFlag = "S";
        private const string DeleteFlag = "D";

        public static string PageAddress { get { return "~/Members/Accounting/TierView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.Tier; } }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.FinanceBlue; }
        }

        public event EventHandler<ViewEventArgs<Tier>> Save;
        public event EventHandler<ViewEventArgs<Tier>> Delete;
        public event EventHandler<ViewEventArgs<Tier>> Lock;
        public event EventHandler<ViewEventArgs<Tier>> UnLock;
        public event EventHandler<ViewEventArgs<Tier>> LoadAuditLog;

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == SaveFlag)
            {
                // clean up file cleared out!
                if (!string.IsNullOrEmpty(hidFilesToDelete.Value))
                {
                    Server.RemoveFiles(hidFilesToDelete.Value.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries));
                    hidFilesToDelete.Value = string.Empty;
                }

                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<Tier>(new Tier(hidTierId.Value.ToLong(), false)));
                SetEditStatus(false);
                memberToolBar.ShowUnlock = false;
            }

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;

            litErrorMessages.Text = messages.HasErrors()
                                        ? string.Join(WebApplicationConstants.HtmlBreak,
                                                      messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
                                                        Select(m => m.Message).ToArray())
                                        : string.Empty;
        }

        public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs)
        {
            auditLogs.Load(logs);
        }

        public void FailedLock(Lock @lock)
        {
            memberToolBar.ShowNew = false;
            memberToolBar.ShowSave = false;
            memberToolBar.ShowDelete = false;
            memberToolBar.ShowUnlock = false;

            SetEditStatus(false);

            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void Set(Tier tier)
        {
            if (tier.IsNew && hidFlag.Value == DeleteFlag)
            {
                Server.RemoveDirectory(WebApplicationSettings.TierFolder(ActiveUser.TenantId, hidTierId.Value.ToLong()));
                hidFlag.Value = string.Empty;
            }

            LoadTier(tier);
            SetEditStatus(!tier.IsNew);
        }

        private void SetEditStatus(bool enabled)
        {
            pnlDetails.Enabled = enabled;
            pnlNotificationEmails.Enabled = enabled;
            memberToolBar.EnableSave = enabled;

            fupLogoUrl.Enabled = enabled && hidTierId.Value.ToLong() != default(long);

            litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;
        }


        private void LoadTier(Tier tier)
        {
            hidTierId.Value = tier.Id.ToString();

            txtName.Text = tier.Name;
            txtTierNumber.Text = tier.TierNumber;
            txtAdditionalBolText.Text = tier.AdditionalBillOfLadingText;
            txtLTLNotificationEmails.Text = tier.LTLNotificationEmails;
            txtFTLNotificationEmails.Text = tier.FTLNotificationEmails;
            txtSPNotificationEmails.Text = tier.SPNotificationEmails;
            txtRailNotificationEmails.Text = tier.RailNotificationEmails;
            txtAirNotificationEmails.Text = tier.AirNotificationEmails;
            txtGeneralNotificationEmails.Text = tier.GeneralNotificationEmails;
            txtPendingVendorNotificationEmails.Text = tier.PendingVendorNotificationEmails;
            txtAccountPayableDisputeNotificationEmails.Text = tier.AccountPayableDisputeNotificationEmails;

            txtTollFreeContactNumber.Text = tier.TollFreeContactNumber;
            txtSupportTollFree.Text = tier.SupportTollFree;
            txtSupportEmails.Text = tier.TierSupportEmails;
            chkActive.Checked = tier.Active;
            txtDateCreated.Text = tier.DateCreated == new DateTime() ? string.Empty :  tier.DateCreated.FormattedShortDate();

            imgLogo.ImageUrl = tier.LogoUrl;
            imgLogo.AlternateText = string.IsNullOrEmpty(tier.LogoUrl) ? string.Empty : string.Format("{0} logo", tier.Name);
	        imgLogo.Visible = !string.IsNullOrEmpty(tier.LogoUrl);
            hidLogoUrl.Value = tier.LogoUrl;

            LoadCustomers(tier.RetrieveTierCustomers());

            memberToolBar.ShowUnlock = tier.HasUserLock(ActiveUser, tier.Id);
        }

        private void LoadCustomers(List<Customer> customers)
        {
            const string row =
                @"<tr>
					<td>{0}</td>
					<td>{1}</td>
				</tr>";
            const string linkRow =
                @"<tr>
                    <td>{0}</td>
                    <td>{1} <a href='{2}?{3}={4}' target='_blank' title='Go To Customer Record' class='blue'><img src={5} width='16' class='middle'/></a></td>
                </tr>";
            var hasAccessToCustomer = ActiveUser.HasAccessTo(ViewCode.Customer);
            var rows = customers
                .Select(c => hasAccessToCustomer
                                 ? string.Format(linkRow, c.Name, c.CustomerNumber, ResolveUrl(CustomerView.PageAddress), WebApplicationConstants.TransferNumber, c.Id.GetString().UrlTextEncrypt(), ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                 : string.Format(row, c.Name, c.CustomerNumber))
                .ToArray()
                .SpaceJoin();
            litCustomers.Text = rows;
            tabCustomers.HeaderText = customers.BuildTabCount(CustomersHeader);
        }


        private void ProcessTransferredRequest(Tier tier)
        {
            if (tier.IsNew) return;
            
            LoadTier(tier);

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<Tier>(tier));
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new TierHandler(this).Initialize();

            memberToolBar.ShowSave = Access.Modify;
            memberToolBar.ShowDelete = Access.Remove;
            memberToolBar.ShowNew = Access.Modify;
            memberToolBar.ShowEdit = Access.Modify;

            tierFinder.OpenForEditEnabled = Access.Modify;

            if (IsPostBack) return;

            if (Session[WebApplicationConstants.TransferTierId] != null)
            {
                ProcessTransferredRequest(new Tier(Session[WebApplicationConstants.TransferTierId].ToLong()));
                Session[WebApplicationConstants.TransferTierId] = null;
            }

            if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.TransferNumber]))
                ProcessTransferredRequest(new Tier(Request.QueryString[WebApplicationConstants.TransferNumber].UrlTextDecrypt().ToLong()));

            SetEditStatus(false);
        }


        protected void OnTierFinderItemSelected(object sender, ViewEventArgs<Tier> e)
        {
            litErrorMessages.Text = string.Empty;

            // check for previous items ...
            if (hidTierId.Value.ToLong() != default(long))
            {
                var oldTier = new Tier(hidTierId.Value.ToLong(), false);
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<Tier>(oldTier));
            }

            var tier = e.Argument;

            LoadTier(tier);

            tierFinder.Visible = false;

            if (tierFinder.OpenForEdit && Lock != null)
            {
                SetEditStatus(true);
                memberToolBar.ShowUnlock = true;
                Lock(this, new ViewEventArgs<Tier>(tier));
            }
            else
            {
                SetEditStatus(false);
            }

            if (LoadAuditLog != null) LoadAuditLog(this, new ViewEventArgs<Tier>(tier));
        }

        protected void OnTierFinderSelectionCancelled(object sender, EventArgs e)
        {
            tierFinder.Visible = false;
        }


        protected void OnNewClicked(object sender, EventArgs e)
        {
            litErrorMessages.Text = string.Empty;
            LoadTier(new Tier { DateCreated = DateTime.Now });
            DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
            SetEditStatus(true);
        }

        protected void OnEditClicked(object sender, EventArgs e)
        {
            var tier = new Tier(hidTierId.Value.ToLong(), false);

            if (Lock == null || tier.IsNew) return;

            SetEditStatus(true);
            memberToolBar.ShowUnlock = true;
            Lock(this, new ViewEventArgs<Tier>(tier));
        }

        protected void OnUnlockClicked(object sender, EventArgs e)
        {
            var tier = new Tier(hidTierId.Value.ToLong(), false);
            
            if (UnLock == null || tier.IsNew) return;
            
            SetEditStatus(false);
            memberToolBar.ShowUnlock = false;
            UnLock(this, new ViewEventArgs<Tier>(tier));
        }

        protected void OnSaveClicked(object sender, EventArgs e)
        {
            var tier = new Tier(hidTierId.Value.ToLong(), true)
            {
                Name = txtName.Text,
                AdditionalBillOfLadingText = txtAdditionalBolText.Text,
                LTLNotificationEmails = txtLTLNotificationEmails.Text.StripSpacesFromEmails(),
                FTLNotificationEmails = txtFTLNotificationEmails.Text.StripSpacesFromEmails(),
                SPNotificationEmails = txtSPNotificationEmails.Text.StripSpacesFromEmails(),
                RailNotificationEmails = txtRailNotificationEmails.Text.StripSpacesFromEmails(),
                AirNotificationEmails = txtAirNotificationEmails.Text.StripSpacesFromEmails(),
                GeneralNotificationEmails = txtGeneralNotificationEmails.Text.StripSpacesFromEmails(),
                AccountPayableDisputeNotificationEmails = txtAccountPayableDisputeNotificationEmails.Text.StripSpacesFromEmails(),
                PendingVendorNotificationEmails = txtPendingVendorNotificationEmails.Text.StripSpacesFromEmails(),
                TollFreeContactNumber = txtTollFreeContactNumber.Text,
                SupportTollFree = txtSupportTollFree.Text,
                TierSupportEmails = txtSupportEmails.Text.StripSpacesFromEmails(),
                Active = chkActive.Checked
            };

            if (tier.IsNew)
            {
                tier.TenantId = ActiveUser.TenantId;
                tier.DateCreated = DateTime.Now;
                tier.LogoUrl = string.Empty;
            }

            // update logo
            if (fupLogoUrl.HasFile)
            {
                var virtualPath = WebApplicationSettings.TierFolder(tier.TenantId, tier.Id);
                var physicalPath = Server.MapPath(virtualPath);

                //ensure unique filename
                var fi = new FileInfo(Server.MakeUniqueFilename(Path.Combine(physicalPath, fupLogoUrl.FileName), true));
                fupLogoUrl.WriteToFile(physicalPath, fi.Name, Server.MapPath(tier.LogoUrl));
                tier.LogoUrl = virtualPath + fi.Name;
            }
            else if (string.IsNullOrEmpty(hidLogoUrl.Value))
            {
                hidFilesToDelete.Value += string.Format("{0};", tier.LogoUrl);
                tier.LogoUrl = string.Empty;
            }

            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<Tier>(tier));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<Tier>(tier));
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            var tier = new Tier(hidTierId.Value.ToLong(), false);

            hidFlag.Value = DeleteFlag;

            if (Delete != null)
                Delete(this, new ViewEventArgs<Tier>(tier));

            tierFinder.Reset();
        }

        protected void OnFindClicked(object sender, EventArgs e)
        {
            tierFinder.Visible = true;
        }


        protected void OnClearLogoUrlClicked(object sender, EventArgs e)
        {
            hidFilesToDelete.Value += string.Format("{0};", hidLogoUrl.Value);
            hidLogoUrl.Value = string.Empty;
            imgLogo.ImageUrl = string.Empty;
	        imgLogo.Visible = false;
            imgLogo.AlternateText = string.Empty;
        }

    }
}
