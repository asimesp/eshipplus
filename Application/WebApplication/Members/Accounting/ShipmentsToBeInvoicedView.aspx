﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="ShipmentsToBeInvoicedView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.ShipmentsToBeInvoicedView"
    EnableEventValidation="false" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowMore="True" OnCommand="OnToolbarCommand" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:UpdatePanel runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
                        Shipments To Be Invoiced<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </h3>
        </div>

        <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
            OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" Title="Shipment Number Import"
            Instructions="Upload shipment numbers for shipments to be marked audited for invoicing. One shipment number per line." />

        <hr class="dark mb10" />
        <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="Shipments" OnProcessingError="OnSearchProfilesProcessingError"
                            OnSearchProfileSelected="OnSearchProfilesProfileSelected" ShowAutoRefresh="True" OnAutoRefreshChanged="OnSearchProfilesAutoRefreshChanged" />
                    </div>
                </div>
            </div>
            <hr class="dark mb10" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>' OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>
            <hr class="fat" />
            <div class="rowgroup">
                <div class="row mb10">
                    <div class="left">
                        <asp:Button ID="btnSearch" CssClass="left" runat="server" Text="SEARCH" OnClick="OnSearchClicked"
                            CausesValidation="False" />
                    </div>
                    <div class="right">
                        <div class="fieldgroup">
                            <label>Sort By:</label>
                            <asp:DropDownList runat="server" ID="ddlSortBy" DataTextField="DisplayName" DataValueField="Name"
                                OnSelectedIndexChanged="OnSortBySelectedIndexChanged" AutoPostBack="True" />
                        </div>
                        <div class="fieldgroup">
                            <asp:RadioButton runat="server" ID="rbAsc" Text="ASC" GroupName="rbSortBy" AutoPostBack="True"
                                ToolTip="Ascending" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                        </div>
                        <div class="fieldgroup">
                            <asp:RadioButton runat="server" ID="rbDesc" Text="DESC" GroupName="rbSortBy" Checked="True"
                                ToolTip="Descending" AutoPostBack="True" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:UpdatePanel runat="server" ID="upDataUpdate">
            <ContentTemplate>
                <eShip:DocumentDisplayControl runat="server" ID="documentDisplay" Visible="false" OnClose="OnDocumentDisplayClosed" />
                <eShip:ShippingLabelGeneratorControl runat="server" ID="shippingLabelGenerator" Visible="false" OnClose="OnShippingLabelGeneratorCloseClicked" />
                <asp:Timer runat="server" ID="tmRefresh" Interval="30000" OnTick="OnTimerTick" />
                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheShipmentsTable" TableId="shipmentsTable" HeaderZIndex="2" />
                <table class="line2 pl2" id="shipmentsTable">
                    <tr>
                        <th style="width: 2%; <%= HasAccessToModifyShipmentsToBeInvoiced ? string.Empty : "display:none;"%>">
                            <eShip:AltUniformCheckBox ID="chkSelectAllRecords" runat="server"
                                OnClientSideClicked="javascript:SetSelectedCheckBoxesStatus(this, 'shipmentsTable');" />
                        </th>
                        <th style="width: 9%;">
                            <asp:LinkButton runat="server" ID="lbtnSortShipmentNumber" CssClass="link_nounderline blue"
                                OnCommand="OnSortData">
                            <abbr title="Shipment Number">Shipment #</abbr>
                            </asp:LinkButton>
                        </th>
                        <th style="width: 8%;">
                            <asp:LinkButton runat="server" ID="lbtnSortDateCreated" OnCommand="OnSortData" CssClass="link_nounderline blue">
                            Date Created
                            </asp:LinkButton>
                        </th>
                        <th style="width: 8%;">
                            <asp:LinkButton runat="server" ID="lbtnSortDesiredPickup" CssClass="link_nounderline blue"
                                OnCommand="OnSortData">
                           Desired Pickup Date
                            </asp:LinkButton>
                        </th>
                        <th style="width: 8%;">
                            <asp:LinkButton runat="server" ID="lbtnSortActualPickup" CssClass="link_nounderline blue"
                                OnCommand="OnSortData">
                            Actual Pickup Date
                            </asp:LinkButton>
                        </th>
                        <th style="width: 8%;">
                            <asp:LinkButton runat="server" ID="lbtnSortEstimatedDelivery" CssClass="link_nounderline blue"
                                OnCommand="OnSortData">
                            Estimated Delivery Date
                            </asp:LinkButton>
                        </th>
                        <th style="width: 8%;">
                            <asp:LinkButton runat="server" ID="lbtnSortActualDelivery" CssClass="link_nounderline blue"
                                OnCommand="OnSortData">
                            Actual Delivery Date
                            </asp:LinkButton>
                        </th>
                        <th style="width: 8%;">Cost
                        </th>
                        <th style="width: 8%;">Amount Due
                        </th>
                        <th style="width: 11%;">
                            <asp:LinkButton runat="server" ID="lbtnSortServiceMode" Text="Service Mode" CssClass="link_nounderline blue"
                                OnCommand="OnSortData" />
                        </th>
                        <th style="width: 10%;">
                            <asp:LinkButton runat="server" ID="lbtnSortStatus" Text="Status" CssClass="link_nounderline blue"
                                OnCommand="OnSortData" />
                        </th>
                        <th style="width: 7%;" class="text-center">Action</th>
                    </tr>
                    <asp:ListView runat="server" ID="lstShipmentDetails" ItemPlaceholderID="itemPlaceHolder"
                        OnItemDataBound="OnShipmentDetailsItemDataBound">
                        <LayoutTemplate>
                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                        </LayoutTemplate>
                        <ItemTemplate>
                            <eShip:CustomHiddenField runat="server" ID="hidShipmentId" Value='<%# Eval("Id") %>' />
                            <eShip:ShipmentDashboardDetailControl ID="shipmentDashboardDetailControl" ItemIndex='<%# Container.DataItemIndex %>'
                                runat="server" EnableOpen='<%# ActiveUser.HasAccessTo(ViewCode.Shipment) %>'
                                OpenForAudit="True" EnabledMultiSelect="<%# HasAccessToModifyShipmentsToBeInvoiced %>"
                                OnGenerateShippingLabel="OnGenerateShippingLabel" />
                        </ItemTemplate>
                    </asp:ListView>
                </table>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lbtnSortShipmentNumber" />
                <asp:PostBackTrigger ControlID="lbtnSortDateCreated" />
                <asp:PostBackTrigger ControlID="lbtnSortDesiredPickup" />
                <asp:PostBackTrigger ControlID="lbtnSortActualPickup" />
                <asp:PostBackTrigger ControlID="lbtnSortEstimatedDelivery" />
                <asp:PostBackTrigger ControlID="lbtnSortActualDelivery" />
                <asp:PostBackTrigger ControlID="lbtnSortServiceMode" />
                <asp:PostBackTrigger ControlID="lbtnSortStatus" />
               
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
</asp:Content>
