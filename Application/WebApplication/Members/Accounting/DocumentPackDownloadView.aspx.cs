﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;
using Ionic.Zip;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Documents;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting
{
    public partial class DocumentPackDownloadView : MemberPageBase, IDocumentPackDownloadView
    {
        private const long BolTagId = -1;

        public static string PageAddress { get { return "~/Members/Accounting/DocumentPackDownloadView.aspx"; } }

        public override ViewCode PageCode
        {
            get { return ViewCode.DocumentPackDownload; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.FinanceBlue; }
        }

        public Dictionary<int, string> EntityTypes
        {
            set
            {
            }
        }

        public List<DocumentTag> DocumentTags
        {
            set
            {
                var tags = value
                    .Select(tag => new
                        {
                            Text = string.Format(" {0}", tag.Description),
                            Value = tag.Id.ToString(),
                            Selected = false
                        })
                    .OrderBy(v => v.Text)
                    .ToList();

                rptDocTagsToDownload.DataSource = tags;
                rptDocTagsToDownload.DataBind();
            }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<DocumentPackEntityDtoSearchCriteria>> Search;


        public void DisplaySearchResult(List<DocumentPackEntityDto> entities)
        {
            litRecordCount.Text = entities.BuildRecordCount();
            lstDocumentPackEntities.DataSource = entities.OrderBy(e => e.Number);
            lstDocumentPackEntities.DataBind();

            chkSelectAllRecords.Checked = false;
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Button = MessageButton.Ok;
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }



        private void LoadFilterParameters()
        {
            if (chkInvoiceRecordType.Checked)
            {
                searchProfiles.Type = SearchDefaultsItemType.Invoices;

                var iprofile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);
                lstFilterParameters.DataSource = iprofile != null
                                                     ? iprofile.Columns
                                                     : AccountingSearchFields.DefaultInvoices.Select(f => f.ToParameterColumn()).ToList();
                lstFilterParameters.DataBind();
                pnlInvoiceOnly.Visible = true;

                // incclude BOL tag
                var tags = rptDocTagsToDownload.Items
                                               .Cast<RepeaterItem>()
                                               .Select(i => new
                                                   {
                                                       i.FindControl("litDocumentTagName").ToLiteral().Text,
                                                       i.FindControl("hidDocumentTagId").ToCustomHiddenField().Value,
                                                       Selected = i.FindControl("chkSelected").ToCheckBox().Checked
                                                   })
                                               .ToList();
                var bolTag = new { Text = " " + WebApplicationConstants.BOLDescription, Value = BolTagId.ToString(), Selected = false };
                if (!tags.Contains(bolTag)) tags.Add(bolTag);
                rptDocTagsToDownload.DataSource = tags.OrderBy(t => t.Text);
                rptDocTagsToDownload.DataBind();
            }
            else if (chkShipmentRecordType.Checked)
            {
                searchProfiles.Type = SearchDefaultsItemType.Shipments;

                var sprofile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);
                lstFilterParameters.DataSource = sprofile != null
                                                     ? sprofile.Columns
                                                     : OperationsSearchFields.Default.Select(f => f.ToParameterColumn()).ToList();
                lstFilterParameters.DataBind();
                pnlInvoiceOnly.Visible = false;

                // remove BOL tag
                var tags = rptDocTagsToDownload.Items
                                               .Cast<RepeaterItem>()
                                               .Select(i => new
                                                   {
                                                       i.FindControl("litDocumentTagName").ToLiteral().Text,
                                                       i.FindControl("hidDocumentTagId").ToCustomHiddenField().Value,
                                                       Selected = i.FindControl("chkSelected").ToCheckBox().Checked
                                                   })
                                               .ToList();

                for (var i = tags.Count - 1; i >= 0; i--)
                    if (tags[i].Value.ToLong() == BolTagId)
                    {
                        tags.RemoveAt(i);
                        break;
                    }

                rptDocTagsToDownload.DataSource = tags;
                rptDocTagsToDownload.DataBind();
            }
        }


        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems { Columns = GetCurrentRunParameters(true) };
        }

        private void DoSearchPreProcessingThenSearch()
        {
            var criteria = new DocumentPackEntityDtoSearchCriteria
            {
                ActiveUserId = ActiveUser.Id,
                Type = chkInvoiceRecordType.Checked ? DocumentPackEntityType.Invoice : DocumentPackEntityType.Shipment,
                Parameters = GetCurrentRunParameters(false)
            };

            if (Search != null)
                Search(this, new ViewEventArgs<DocumentPackEntityDtoSearchCriteria>(criteria));
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            new DocumentPackDownloadHandler(this).Initialize();

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            pnlInvoiceOnly.Visible = chkInvoiceRecordType.Checked;

            if (IsPostBack) return;

            if (Loading != null)
                Loading(this, new EventArgs());

            LoadFilterParameters();
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch();
        }


        protected void OnDownloadClicked(object sender, EventArgs e)
        {
            var selectedItems = lstDocumentPackEntities.Items
                .Select(i => new
                {
                    Selected = i.FindControl("chkSelected").ToAltUniformCheckBox().Checked,
                    Id = i.FindControl("hidEntityId").ToCustomHiddenField().Value.ToLong(),
                    Type = i.FindControl("hidEntityType").ToCustomHiddenField().Value.ToEnum<DocumentPackEntityType>()
                })
                .Where(i => i.Selected);

            if (!selectedItems.Any())
            {
                DisplayMessages(new[] { ValidationMessage.Information("No records selected for download.") });
                return;
            }


            ZipFile zip;
            var fileName = Server.MakeUniqueFilename(WebApplicationSettings.TempFolder + string.Format("{0:yyyyMMdd_hhmmss}_DocumentPack.zip", DateTime.Now));
            using (zip = new ZipFile(fileName))
            {
                foreach (var item in selectedItems)
                {
                    switch (item.Type)
                    {
                        case DocumentPackEntityType.Shipment:
                            var shipment = new Shipment(item.Id);
                            if (shipment.KeyLoaded) ProcessShipmentDocuments(shipment, zip, string.Empty);
                            break;
                        case DocumentPackEntityType.Invoice:
                            var invoice = new Invoice(item.Id);
                            if (invoice.KeyLoaded) ProcessInvoiceDocuments(invoice, zip);
                            break;
                    }
                }
                zip.Save();
            }

            var info = new FileInfo(fileName);
            Response.Export(info, info.Name);
            info.Delete();
        }

        private void ProcessShipmentDocuments(Shipment shipment, ZipFile zip, string key)
        {
            var tagIds = rptDocTagsToDownload.Items
                .Cast<RepeaterItem>()
                .Where(i => i.FindControl("chkSelected").ToCheckBox().Checked)
                .Select(i => i.FindControl("hidDocumentTagId").ToCustomHiddenField().Value.ToLong())
                .ToList();

            var groupKey = string.IsNullOrEmpty(key) ? string.Empty : string.Format("{0}_", key);

            // add bol if applicable
            if (tagIds.Contains(BolTagId) || chkShipmentRecordType.Checked)
            {
                var entryName = string.Format("{0}{1}_Bol.pdf", groupKey, shipment.ShipmentNumber);
                zip.AddEntry(entryName, this.GenerateBOL(shipment, true).ToPdf(shipment.ShipmentNumber));
            }

            // add other documents
            foreach (var document in shipment.Documents)
                if ((chkDownloadAll.Checked || (chkDownloadNonInternal.Checked && !document.IsInternal)) && tagIds.Contains(document.DocumentTagId))
                {
                    var fileName = Server.MapPath(document.LocationPath);
                    if (!File.Exists(fileName)) continue;
                    var info = new FileInfo(fileName);
                    var name = string.Format("{0}{1}_{2}_{3}{4}", groupKey, shipment.ShipmentNumber, document.DocumentTagId != default(long) ? document.DocumentTag.Code : string.Empty, document.Name, info.Extension);
                    if (!zip.ContainsEntry(name))
                        zip.AddEntry(name, fileName.ReadFromFile());
                }
        }

        private void ProcessInvoiceDocuments(Invoice invoice, ZipFile zip)
        {
            var entryName = string.Format("{0}_Invoice.pdf", invoice.InvoiceNumber);
            zip.AddEntry(entryName, this.GenerateInvoice(invoice, true).ToPdf(invoice.InvoiceNumber));

            if (!chkDownloadInvoiceOnly.Checked)
                foreach (var shipment in invoice.RetrieveAssociatedShipments())
                    ProcessShipmentDocuments(shipment, zip, invoice.InvoiceNumber);
        }


        protected void OnRecordTypeCheckedChanged(object sender, EventArgs e)
        {
            LoadFilterParameters();
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = chkInvoiceRecordType.Checked
                            ? AccountingSearchFields.Invoices.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName)
                            : OperationsSearchFields.Shipments.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            var fields = chkInvoiceRecordType.Checked
                            ? AccountingSearchFields.Invoices.Where(f => f.Name != AccountingSearchFields.Posted.Name)
                            : OperationsSearchFields.Shipments;

            parameterSelector.LoadParameterSelector(fields.ToList());
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;

        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();
        }

    }
}
