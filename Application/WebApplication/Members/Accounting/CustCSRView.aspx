﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="CustCSRView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.CustCSRView" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" ImageUrl="" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Customer Service Representatives
                <asp:Literal runat="server" ID="litHeader" />
            </h3>
        </div>
        <asp:Panel runat="server" ID="pnlNoCsr" Visible="False">
            <table class="line2 pl2">
                <tr class="header">
                    <td>
                        <h5 class="red">No Customer Service Representatives</h5>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p class="pb40 mt20 fs120em">
                            There are no customer service representatives assigned to this account.
                        </p>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlNoAccess" Visible="False">
            <table class="line2 pl2">
                <tr class="header">
                    <td>
                        <h5 class="red">Access Restriction</h5>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p class="pb40 mt20 fs120em">
                            Employees must have access to Customers in order to view page content.
                        </p>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlListCsr" Visible="False">
            <div class="rowgroup">
                <table class="stripe">
                    <tr>
                        <th style="width: 15%;">First Name
                        </th>
                        <th style="width: 15%;">Last Name
                        </th>
                        <th style="width: 20%;">Phone
                        </th>
                        <th style="width: 20%;">Fax
                        </th>
                        <th style="width: 30%;">Email
                        </th>
                    </tr>
                    <asp:ListView ID="lstCSR" runat="server" ItemPlaceholderID="itemPlaceHolder">
                        <LayoutTemplate>
                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <%# Eval("FirstName") %>
                                </td>
                                <td>
                                    <%# Eval("LastName") %>
                                </td>
                                <td>
                                    <%# Eval("Phone") %>
                                </td>
                                <td>
                                    <%# Eval("Fax") %> 
                                </td>
                                <td>
                                    <%# Eval("Email") %>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </table>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
