﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;
using Lock = LogisticsPlus.Eship.Core.Lock;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting
{
    public partial class SalesRepresentativeView : MemberPageBaseWithPageStore, ISalesRepresentativeView
    {
        private const string ServiceModeStoreKey = "SMSK";

        private const string SalesRepCommTierHeader = "Commission Tiers";

        private const string DeleteFlag = "D";
        private const string SaveFlag = "S";

        public override ViewCode PageCode { get { return ViewCode.SalesRepresentative; } }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.FinanceBlue; }
        }

        public static string PageAddress { get { return "~/Members/Accounting/SalesRepresentativeView.aspx"; } }


        public List<ViewListItem> ServiceModesCollection
        {
            get { return PageStore[ServiceModeStoreKey] as List<ViewListItem> ?? new List<ViewListItem>(); }

        }

        public Dictionary<int, string> ServiceModes
        {
            set
            {
                var data = value.Select(t => new ViewListItem(t.Value, t.Key.ToString())).ToList();
                PageStore[ServiceModeStoreKey] = data;

                ddlEditServiceMode.DataSource = data;
                ddlEditServiceMode.DataBind();
            }
        }


        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<SalesRepresentative>> Save;
        public event EventHandler<ViewEventArgs<SalesRepresentative>> Delete;
        public event EventHandler<ViewEventArgs<SalesRepresentative>> Lock;
        public event EventHandler<ViewEventArgs<SalesRepresentative>> UnLock;
        public event EventHandler<ViewEventArgs<SalesRepresentative>> LoadAuditLog;
        public event EventHandler<ViewEventArgs<List<SalesRepresentative>>> BatchImport;
        public event EventHandler<ViewEventArgs<string>> CustomerSearch;


        public void DisplayCustomer(Customer customer)
        {
            txtEditCustomerNumber.Text = customer == null ? string.Empty : customer.CustomerNumber;
            txtEditCustomerName.Text = customer == null ? string.Empty : customer.Name;
            hidCustomerId.Value = customer == null ? default(long).ToString() : customer.Id.ToString();
        }

        public void FailedLock(Lock @lock)
        {
            memberToolBar.ShowNew = false;
            memberToolBar.ShowSave = false;
            memberToolBar.ShowDelete = false;
            memberToolBar.ShowUnlock = false;

            SetEditStatus(false);

            litErrorMessages.Text = @lock.BuildFailedLockMessage();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == SaveFlag)
            {
                // unlock and return to read-only
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<SalesRepresentative>(new SalesRepresentative(hidSalesRepresentativeId.Value.ToLong(), false)));
                SetEditStatus(false);
                memberToolBar.ShowUnlock = false;
            }

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;

            litErrorMessages.Text = messages.HasErrors()
                                        ? string.Join(WebApplicationConstants.HtmlBreak,
                                                      messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
                                                        Select(m => m.Message).ToArray())
                                        : string.Empty;
        }

        public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs) { auditLogs.Load(logs); }

        public void Set(SalesRepresentative salesRep)
        {
            LoadSalesRepresentative(salesRep);
            SetEditStatus(!salesRep.IsNew);
        }


        private void SetEditStatus(bool enabled)
        {
            hidEditStatus.Value = enabled.ToString();

            pnlSalesRepCommTiers.Enabled = enabled;
            pnlDetails.Enabled = enabled;

            memberToolBar.EnableSave = enabled;

            litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;
        }

        private void LoadSalesRepresentative(SalesRepresentative salesRep)
        {
            if (!salesRep.IsNew) salesRep.LoadCollections();

            hidSalesRepresentativeId.Value = salesRep.Id.ToString();
            txtSalesRepNumber.Text = salesRep.SalesRepresentativeNumber.GetString();
            txtName.Text = salesRep.Name.GetString();
            txtPhone.Text = salesRep.Phone.GetString();
            txtMobile.Text = salesRep.Mobile.GetString();
            txtFax.Text = salesRep.Fax.GetString();
            txtEmail.Text = salesRep.Email.GetString();
            txtCompanyName.Text = salesRep.CompanyName.GetString();
            txtDateCreated.Text = salesRep.IsNew ? DateTime.Now.FormattedShortDate() : salesRep.DateCreated.FormattedShortDate();
            txtAddlEntityName.Text = salesRep.AdditionalEntityName.GetString();
            txtAddlEntityCommPercent.Text = salesRep.AdditionalEntityCommPercent.ToString();
            var location = new Location
            {
                Street1 = salesRep.Street1,
                Street2 = salesRep.Street2,
                City = salesRep.City,
                State = salesRep.State,
                PostalCode = salesRep.PostalCode,
                CountryId = salesRep.CountryId
            };
            mailingAddressInput.LoadAddress(location);

            var salesRepCommTiers = salesRep.SalesRepCommTiers
                .OrderByDescending(srct => srct.EffectiveDate)
                .Select(srct => new
                {
                    srct.Id,
                    srct.CustomerId,
                    CustomerName = srct.Customer == null ? string.Empty : srct.Customer.Name,
                    CustomerNumber = srct.Customer == null ? string.Empty : srct.Customer.CustomerNumber,
                    srct.CommissionPercent,
                    srct.CeilingValue,
                    srct.FloorValue,
                    EffectiveDate = srct.EffectiveDate.FormattedShortDate(),
                    ExpirationDate = srct.ExpirationDate.FormattedShortDate(),
                    srct.ServiceMode
                })
                .ToList();

            lstSalesRepCommTiers.DataSource = salesRepCommTiers;
            lstSalesRepCommTiers.DataBind();
            tabSalesRepCommTiers.HeaderText = salesRepCommTiers.BuildTabCount(SalesRepCommTierHeader);

            memberToolBar.ShowUnlock = salesRep.HasUserLock(ActiveUser, salesRep.Id);
        }

        private void ProcessTransferredRequest(SalesRepresentative salesRep)
        {
            if (salesRep.IsNew) return;

            LoadSalesRepresentative(salesRep);

            if (LoadAuditLog != null) 
                LoadAuditLog(this, new ViewEventArgs<SalesRepresentative>(salesRep));
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            new SalesRepresentativeHandler(this).Initialize();


            memberToolBar.ShowImport = Access.Modify;
            memberToolBar.ShowNew = Access.Modify;
            memberToolBar.ShowDelete = Access.Remove;
            memberToolBar.ShowEdit = Access.Modify;

            if (IsPostBack) return;

            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.SalesRepresentativeImportTemplate });
            aceEditCustomer.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.ActiveUser, ActiveUser.Id.GetString() } });

            if (Loading != null)
                Loading(this, new EventArgs());

            if (IsPostBack) return;

            if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.TransferNumber]))
                ProcessTransferredRequest(new SalesRepresentative(Request.QueryString[WebApplicationConstants.TransferNumber].UrlTextDecrypt().ToLong(), false));

            SetEditStatus(false);
        }


        protected void OnToolbarEditClicked(object sender, EventArgs eventArgs)
        {
            var salesRep = new SalesRepresentative(hidSalesRepresentativeId.Value.ToLong(), false);

            if (Lock == null || salesRep.IsNew) return;

            SetEditStatus(true);
            memberToolBar.ShowUnlock = true;
            Lock(this, new ViewEventArgs<SalesRepresentative>(salesRep));

            LoadSalesRepresentative(salesRep);
        }

        protected void OnToolbarUnlockClicked(object sender, EventArgs e)
        {
            var salesRep = new SalesRepresentative(hidSalesRepresentativeId.Value.ToLong(), false);
            if (UnLock == null || salesRep.IsNew) return;

            SetEditStatus(false);
            memberToolBar.ShowUnlock = false;
            UnLock(this, new ViewEventArgs<SalesRepresentative>(salesRep));
        }

        protected void OnToolbarDeleteClicked(object sender, EventArgs eventArgs)
        {
            var salesRep = new SalesRepresentative(hidSalesRepresentativeId.Value.ToLong(), false);
            hidFlag.Value = DeleteFlag;

            if (Delete != null)
                Delete(this, new ViewEventArgs<SalesRepresentative>(salesRep));
            DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
            salesRepFinder.Reset();
        }

        protected void OnToolbarFindClicked(object sender, EventArgs e)
        {
            salesRepFinder.Visible = true;
        }

        protected void OnToolbarSaveClicked(object sender, EventArgs e)
        {
            var salesRepId = hidSalesRepresentativeId.Value.ToLong();
            var salesRep = new SalesRepresentative(salesRepId, salesRepId != default(long));

            if (salesRep.IsNew)
            {
                salesRep.TenantId = ActiveUser.TenantId;
                salesRep.DateCreated = DateTime.Now;
            }
            else salesRep.LoadCollections();

            //Get information from the view
            salesRep.Name = txtName.Text;
            salesRep.Phone = txtPhone.Text;
            salesRep.Mobile = txtMobile.Text;
            salesRep.Fax = txtFax.Text;
            salesRep.Email = txtEmail.Text.StripSpacesFromEmails();
            salesRep.CompanyName = txtCompanyName.Text;

            salesRep.AdditionalEntityName = txtAddlEntityName.Text;
            salesRep.AdditionalEntityCommPercent = txtAddlEntityCommPercent.Text.ToDecimal();
            salesRep.CompanyName = txtCompanyName.Text;
            if (salesRep.IsNew) salesRep.DateCreated = DateTime.Now;
            var location = new Location();
            mailingAddressInput.RetrieveAddress(location);
            salesRep.Street1 = location.Street1;
            salesRep.Street2 = location.Street2;
            salesRep.City = location.City;
            salesRep.State = location.State;
            salesRep.PostalCode = location.PostalCode;
            salesRep.CountryId = location.CountryId;

            var salesRepCommTiers = lstSalesRepCommTiers.Items
               .Select(control =>
               {
                   var id = control.FindControl("hidSalesRepCommTierId").ToCustomHiddenField().Value.ToLong();
                   return new SalesRepCommTier(id, id != default(long))
                   {
                       CustomerId = control.FindControl("hidSalesRepCommTierCustomerId").ToCustomHiddenField().Value.ToLong(),
                       CommissionPercent = control.FindControl("txtCommissionPercent").ToTextBox().Text.ToDecimal(),
                       CeilingValue = control.FindControl("txtCeilingValue").ToTextBox().Text.ToDecimal(),
                       FloorValue = control.FindControl("txtFloorValue").ToTextBox().Text.ToDecimal(),
                       EffectiveDate = control.FindControl("txtEfectiveDate").ToTextBox().Text.ToDateTime().TimeToMinimum(),
                       ExpirationDate = control.FindControl("txtExpirationDate").ToTextBox().Text.ToDateTime().TimeToMaximum(),
                       ServiceMode = control.FindControl("ddlServiceMode").ToDropDownList().SelectedValue.Replace(" ", string.Empty).ToEnum<ServiceMode>(),
                       TenantId = salesRep.TenantId,
                       SalesRepresentative = salesRep,
                   };
               })
               .ToList();
            salesRep.SalesRepCommTiers = salesRepCommTiers;

            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<SalesRepresentative>(salesRep));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<SalesRepresentative>(salesRep));
        }

        protected void OnToolbarNewClicked(object sender, EventArgs e)
        {
            litErrorMessages.Text = string.Empty;

            LoadSalesRepresentative(new SalesRepresentative());
            DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
            SetEditStatus(true);
        }



        protected void OnImportClicked(object sender, EventArgs e)
        {
            fileUploader.Title = "SALES REPRESENTATIVE IMPORT";
            fileUploader.Instructions = @"**Please refer to user guide for import file format.";
            fileUploader.Visible = true;
        }

        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 23;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            //ensure every line has a import grouping code
            msgs = chks.Where(l => l.Line[0] == string.Empty)
                       .Select(i => ValidationMessage.Error("Import Grouping value missing on line: {0}", i.Index))
                       .ToList();
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var customerSearch = new CustomerSearch();
            msgs.AddRange(chks
                .Where(i => !string.IsNullOrEmpty(i.Line[16]))
                .Where(i => customerSearch.FetchCustomerByNumber(i.Line[16], ActiveUser.TenantId) == null)
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedCodeErrMsg, new Customer().EntityName(), i.Index))
                .ToList());
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            msgs.AddRange(chks.EnumsAreValid<ServiceMode>(22));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            //separate the lines grouped by import grouping code and validate the sales representative line
            var importGroups = new Dictionary<string, List<ImportLineObj>>();
            foreach (var line in chks)
            {
                if (!importGroups.ContainsKey(line.Line[0])) importGroups.Add(line.Line[0], new List<ImportLineObj>());
                importGroups[line.Line[0]].Add(line);
            }

            var countries = ProcessorVars.RegistryCache.Countries.Values;
            var countryCodes = countries.Select(c => c.Code).ToList();

            var salesRepsToValidate = importGroups.Select(l => l.Value.First());
            msgs.AddRange(salesRepsToValidate.CodesAreValid(8, countryCodes, new Country().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var postalCodeSearch = new PostalCodeSearch();
            var postalCodes = new List<PostalCodeViewSearchDto>();
            var invalidPostalCodes = salesRepsToValidate
                .Where(i =>
                {
                    var code = postalCodeSearch.FetchPostalCodeByCode(i.Line[9], countries.First(c => c.Code == i.Line[8]).Id);
                    postalCodes.Add(code);
                    return code.Id == default(long);
                })
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedCodeErrMsg, new PostalCode().EntityName(), i.Index))
                .ToList();
            if (invalidPostalCodes.Any())
            {
                invalidPostalCodes.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(invalidPostalCodes);
                return;
            }

            var salesRepresentatives = new List<SalesRepresentative>();
            foreach (var importGroup in importGroups)
            {
                var line = importGroup.Value[0].Line;

                var salesRepresentative = new SalesRepresentative
                {
                    Name = line[1],
                    SalesRepresentativeNumber = line[2],
                    CompanyName = line[3],
                    Street1 = line[4],
                    Street2 = line[5],
                    City = line[6],
                    State = line[7],
                    CountryId = countries.First(c => c.Code == line[8]).Id,
                    PostalCode = postalCodes.First(pc => pc.Code.ToLower() == line[9].ToLower()).Code,
                    Phone = line[10],
                    Mobile = line[11],
                    Fax = line[12],
                    Email = line[13].StripSpacesFromEmails(),
                    AdditionalEntityName = line[14],
                    AdditionalEntityCommPercent = line[15].ToDecimal(),
                    DateCreated = DateTime.Now,
                    TenantId = ActiveUser.TenantId,
                    SalesRepCommTiers = new List<SalesRepCommTier>()
                };

                for (var i = 0; i < importGroup.Value.Count(); i++)
                {
                    salesRepresentative.SalesRepCommTiers.Add(new SalesRepCommTier
                    {
                        Customer = customerSearch.FetchCustomerByNumber(importGroup.Value[i].Line[16], ActiveUser.TenantId),
                        CommissionPercent = importGroup.Value[i].Line[17].ToDecimal(),
                        CeilingValue = importGroup.Value[i].Line[18].ToInt(),
                        FloorValue = importGroup.Value[i].Line[19].ToInt(),
                        EffectiveDate = importGroup.Value[i].Line[20].ToDateTime(),
                        ExpirationDate = importGroup.Value[i].Line[21].ToDateTime(),
                        ServiceMode = importGroup.Value[i].Line[22].ToEnum<ServiceMode>(),
                        SalesRepresentative = salesRepresentative,
                        TenantId = salesRepresentative.TenantId
                    });
                }
                salesRepresentatives.Add(salesRepresentative);
            }

            fileUploader.Visible = false;

            if (BatchImport != null)
                BatchImport(this, new ViewEventArgs<List<SalesRepresentative>>(salesRepresentatives));
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }


        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;
        }



        protected void OnSalesRepFinderItemSelected(object sender, ViewEventArgs<SalesRepresentative> e)
        {
            litErrorMessages.Text = string.Empty;

            if (hidSalesRepresentativeId.Value.ToLong() != default(long))
            {
                var oldSalesRep = new SalesRepresentative(hidSalesRepresentativeId.Value.ToLong(), false);
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<SalesRepresentative>(oldSalesRep));
            }

            var salesRep = e.Argument;

            LoadSalesRepresentative(salesRep);
            salesRepFinder.Visible = false;

            if (salesRepFinder.OpenForEdit && Lock != null)
            {
                SetEditStatus(true);
                memberToolBar.ShowUnlock = true;
                Lock(this, new ViewEventArgs<SalesRepresentative>(salesRep));
            }
            else
            {
                SetEditStatus(false);
            }

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<SalesRepresentative>(salesRep));
        }

        protected void OnSalesRepFinderItemCancelled(object sender, EventArgs e)
        {
            salesRepFinder.Visible = false;
        }


        private void LoadSalesRepCommTierForEdit(SalesRepCommTier detail)
        {
            DisplayCustomer(detail.Customer);
            hidCustomerId.Value = detail.CustomerId.ToString();
            txtEditCommissionPercent.Text = detail.CommissionPercent.ToString();
            txtEditCeilingValue.Text = detail.CeilingValue.ToString();
            txtEditFloorValue.Text = detail.FloorValue.ToString();
            txtEditEffectiveDate.Text = detail.EffectiveDate <= DateUtility.SystemEarliestDateTime ? DateTime.Now.FormattedShortDate() : detail.EffectiveDate.FormattedShortDate();
            txtEditExpirationDate.Text = detail.ExpirationDate <= DateUtility.SystemEarliestDateTime ? DateTime.Now.AddYears(1).FormattedShortDate() : detail.ExpirationDate.FormattedShortDate();
            ddlEditServiceMode.SelectedIndex = detail.ServiceMode.ToInt();
        }

        protected void OnCommissionTiersItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var item = (ListViewDataItem)e.Item;
            var dataItem = item.DataItem;

            var ddlServiceMode = item.FindControl("ddlServiceMode").ToDropDownList();
            ddlServiceMode.DataSource = ServiceModesCollection;
            ddlServiceMode.DataBind();
            if (dataItem.HasGettableProperty("ServiceMode")) ddlServiceMode.SelectedValue = dataItem.GetPropertyValue("ServiceMode").ToInt().GetString();

        }

        protected void OnAddSalesRepCommTierClicked(object sender, EventArgs e)
        {
            var salesRepCommTiers = lstSalesRepCommTiers.Items
                .Select(i => new
                {
                    Id = i.FindControl("hidSalesRepCommTierId").ToCustomHiddenField().Value.ToLong(),
                    CustomerId = i.FindControl("hidSalesRepCommTierCustomerId").ToCustomHiddenField().Value.ToLong(),
                    CustomerNumber = i.FindControl("txtCustomerNumber").ToTextBox().Text,
                    CustomerName = i.FindControl("txtCustomerName").ToTextBox().Text,
                    CommissionPercent = i.FindControl("txtCommissionPercent").ToTextBox().Text.ToDecimal(),
                    CeilingValue = i.FindControl("txtCeilingValue").ToTextBox().Text.ToDecimal(),
                    FloorValue = i.FindControl("txtFloorValue").ToTextBox().Text.ToDecimal(),
                    EffectiveDate = i.FindControl("txtEfectiveDate").ToTextBox().Text.ToDateTime(),
                    ExpirationDate = i.FindControl("txtExpirationDate").ToTextBox().Text.ToDateTime(),
                    ServiceMode = i.FindControl("ddlServiceMode").ToDropDownList().SelectedValue.Replace(" ", string.Empty).ToEnum<ServiceMode>()
                })
                .ToList();

            salesRepCommTiers.Add(new
                {
                    Id = default(long),
                    CustomerId = default(long),
                    CustomerNumber = string.Empty,
                    CustomerName = string.Empty,
                    CommissionPercent = default(decimal),
                    CeilingValue = default(decimal),
                    FloorValue = default(decimal),
                    EffectiveDate = DateTime.Now,
                    ExpirationDate = DateTime.Now.AddYears(1),
                    ServiceMode = ServiceMode.NotApplicable
                });

            lstSalesRepCommTiers.DataSource = salesRepCommTiers;
            lstSalesRepCommTiers.DataBind();
            tabSalesRepCommTiers.HeaderText = salesRepCommTiers.BuildTabCount(SalesRepCommTierHeader);
            athtuTabUpdater.SetForUpdate(tabSalesRepCommTiers.ClientID, salesRepCommTiers.BuildTabCount(SalesRepCommTierHeader));
        }

        protected void OnClearSalesRepCommTierClicked(object sender, EventArgs e)
        {
            lstSalesRepCommTiers.DataSource = new List<SalesRepCommTier>();
            lstSalesRepCommTiers.DataBind();
            tabSalesRepCommTiers.HeaderText = SalesRepCommTierHeader;
            athtuTabUpdater.SetForUpdate(tabSalesRepCommTiers.ClientID, SalesRepCommTierHeader);
        }

        protected void OnDeleteSalesRepCommTierClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var salesRepCommTiers = lstSalesRepCommTiers.Items
               .Select(i => new
               {
                   Id = i.FindControl("hidSalesRepCommTierId").ToCustomHiddenField().Value.ToLong(),
                   CustomerId = i.FindControl("hidSalesRepCommTierCustomerId").ToCustomHiddenField().Value.ToLong(),
                   CustomerNumber = i.FindControl("txtCustomerNumber").ToTextBox().Text,
                   CustomerName = i.FindControl("txtCustomerName").ToTextBox().Text,
                   CommissionPercent = i.FindControl("txtCommissionPercent").ToTextBox().Text.ToDecimal(),
                   CeilingValue = i.FindControl("txtCeilingValue").ToTextBox().Text.ToDecimal(),
                   FloorValue = i.FindControl("txtFloorValue").ToTextBox().Text.ToDecimal(),
                   EffectiveDate = i.FindControl("txtEfectiveDate").ToTextBox().Text.ToDateTime(),
                   ExpirationDate = i.FindControl("txtExpirationDate").ToTextBox().Text.ToDateTime(),
                   ServiceMode = i.FindControl("ddlServiceMode").ToDropDownList().SelectedValue.Replace(" ", string.Empty).ToEnum<ServiceMode>()
               })
               .ToList();

            salesRepCommTiers.RemoveAt(imageButton.Parent.FindControl("hidSalesRepCommTierIndex").ToCustomHiddenField().Value.ToInt());

            lstSalesRepCommTiers.DataSource = salesRepCommTiers;
            lstSalesRepCommTiers.DataBind();
            tabSalesRepCommTiers.HeaderText = salesRepCommTiers.BuildTabCount(SalesRepCommTierHeader);
            athtuTabUpdater.SetForUpdate(tabSalesRepCommTiers.ClientID, salesRepCommTiers.BuildTabCount(SalesRepCommTierHeader));
        }

        protected void OnCloseSalesRepCommTierClicked(object sender, EventArgs eventArgs)
        {
            hidEditingIndex.Value = string.Empty;
            pnlEditSalesRepCommTier.Visible = false;
            pnlDimScreen.Visible = false;
        }

        protected void OnEditCommissionTierClicked(object sender, ImageClickEventArgs e)
        {
            var control = ((ImageButton)sender).Parent;

            hidEditingIndex.Value = control.FindControl("hidSalesRepCommTierIndex").ToCustomHiddenField().Value;

            var detail = new SalesRepCommTier
            {
                CustomerId = control.FindControl("hidSalesRepCommTierCustomerId").ToCustomHiddenField().Value.ToLong(),
                CommissionPercent = control.FindControl("txtCommissionPercent").ToTextBox().Text.ToDecimal(),
                CeilingValue = control.FindControl("txtCeilingValue").ToTextBox().Text.ToDecimal(),
                FloorValue = control.FindControl("txtFloorValue").ToTextBox().Text.ToDecimal(),
                EffectiveDate = control.FindControl("txtEfectiveDate").ToTextBox().Text.ToDateTime(),
                ExpirationDate = control.FindControl("txtExpirationDate").ToTextBox().Text.ToDateTime(),
                ServiceMode = control.FindControl("ddlServiceMode").ToDropDownList().SelectedValue.Replace(" ", string.Empty).ToEnum<ServiceMode>()
            };

            LoadSalesRepCommTierForEdit(detail);

            pnlEditSalesRepCommTier.Visible = true;
            pnlDimScreen.Visible = true;
            txtEditCustomerNumber.Focus();
        }

        protected void OnEditSalesRepCommTierDoneClicked(object sender, EventArgs e)
        {
            var salesRepCommTiers = lstSalesRepCommTiers.Items
                .Select(i => new
                {
                    Id = i.FindControl("hidSalesRepCommTierId").ToCustomHiddenField().Value.ToLong(),
                    CustomerId = i.FindControl("hidSalesRepCommTierCustomerId").ToCustomHiddenField().Value.ToLong(),
                    CustomerNumber = i.FindControl("txtCustomerNumber").ToTextBox().Text,
                    CustomerName = i.FindControl("txtCustomerName").ToTextBox().Text,
                    CommissionPercent = i.FindControl("txtCommissionPercent").ToTextBox().Text.ToDecimal(),
                    CeilingValue = i.FindControl("txtCeilingValue").ToTextBox().Text.ToDecimal(),
                    FloorValue = i.FindControl("txtFloorValue").ToTextBox().Text.ToDecimal(),
                    EffectiveDate = i.FindControl("txtEfectiveDate").ToTextBox().Text.ToDateTime(),
                    ExpirationDate = i.FindControl("txtExpirationDate").ToTextBox().Text.ToDateTime(),
                    ServiceMode = i.FindControl("ddlServiceMode").ToDropDownList().SelectedValue.Replace(" ", string.Empty).ToEnum<ServiceMode>()
                })
                .ToList();

            var detailIndex = hidEditingIndex.Value.ToInt();
            salesRepCommTiers[detailIndex] = new
            {
                salesRepCommTiers[detailIndex].Id,
                CustomerId = hidCustomerId.Value.ToLong(),
                CustomerNumber = txtEditCustomerNumber.ToTextBox().Text,
                CustomerName = txtEditCustomerName.Text,
                CommissionPercent = txtEditCommissionPercent.Text.ToDecimal(),
                CeilingValue = txtEditCeilingValue.Text.ToDecimal(),
                FloorValue = txtEditFloorValue.Text.ToDecimal(),
                EffectiveDate = txtEditEffectiveDate.Text.ToDateTime(),
                ExpirationDate = txtEditExpirationDate.Text.ToDateTime(),
                ServiceMode = ddlEditServiceMode.SelectedValue.ToEnum<ServiceMode>(),
            };

            lstSalesRepCommTiers.DataSource = salesRepCommTiers;
            lstSalesRepCommTiers.DataBind();
            tabSalesRepCommTiers.HeaderText = salesRepCommTiers.BuildTabCount(SalesRepCommTierHeader);
            athtuTabUpdater.SetForUpdate(tabSalesRepCommTiers.ClientID, salesRepCommTiers.BuildTabCount(SalesRepCommTierHeader));

            pnlEditSalesRepCommTier.Visible = false;
            pnlDimScreen.Visible = false;
        }


        protected void OnCustomerNumberEntered(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtEditCustomerNumber.Text))
            {
                hidCustomerId.Value = string.Empty;
                txtEditCustomerName.Text = string.Empty;
                return;
            }

            if (CustomerSearch != null)
                CustomerSearch(this, new ViewEventArgs<string>(txtEditCustomerNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
        }

        protected void OnCustomerSearchClicked(object sender, ImageClickEventArgs e)
        {
            customerFinder.Visible = true;
        }

        protected void OnCustomerFinderItemSelected(object sender, ViewEventArgs<Customer> e)
        {
            DisplayCustomer(e.Argument);
            customerFinder.Visible = false;
        }

        protected void OnCustomerFinderSelectionCancelled(object sender, EventArgs e)
        {
            hidEditingIndex.Value = string.Empty;
            customerFinder.Visible = false;
        }
    }
}
