﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Connect;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Documents;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Operations;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting
{
    public partial class InvoiceView : MemberPageBaseWithPageStore, IInvoiceView
    {
        private const string InvoiceDetailsHeader = "Invoice Details";
        private const string InvoicePrintLogsHeader = "Print Logs";

        private const string ApplyMiscReceiptsToInvoicesArgs = "ApplyMiscReceiptsToInvoices";
        private const string ReturnToPostingArgs = "ReturnToPosting";
        private const string ReturnToDashboardArgs = "ReturnToDashboard";
        private const string InvoiceDocumentArgs = "Document";
        private const string UpdateAmountPaidArgs = "EditPostedInvoice";
        private const string UpdateAmountPaidWithMiscReceiptApplicationsArgs = "UpdateAmountPaidWithMiscReceiptApplications";
        private const string PostGenerateArgs = "PostGenerate";
        private const string PostArgs = "Post";
        private const string ManualExportArgs = "ManualExport";
        private const string ReverseExportArgs = "ReverseExport";
        private const string ResendInvoicePostedNotificationsArgs = "ResendInvoicePostedNotifications";

        private const string SaveFlag = "S";
        private const string PostFlag = "S";
        private const string NotificationFlag = "NF";
        private const string NoAutoNotifiationFlag = "NNF";
        private const string ZeroAmountDueWarningFlag = "ZADWF";
        private const string PostAndGenerateFlag = "P";
        private const string CustomerChangedFlag = "CustomerChanged";
        private const string UpdateMiscReceiptApplicationsFlag = "UMRA";


        private const string OriginalInvoiceFind = "OIF";

        private const string None = "- None -";

        private const string InvoiceDetailReferenceTypeStoreKey = "IDRTSK";

        public static string PageAddress { get { return "~/Members/Accounting/InvoiceView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.Invoice; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.FinanceBlue; } }



        private List<ViewListItem> StoredInvoiceDetailReferenceTypes
        {
            get { return PageStore[InvoiceDetailReferenceTypeStoreKey] as List<ViewListItem> ?? new List<ViewListItem>(); }
            set { PageStore[InvoiceDetailReferenceTypeStoreKey] = value; }
        }

        public Dictionary<int, string> InvoiceDetailReferenceType
        {
            set
            {
                StoredInvoiceDetailReferenceTypes = value.Select(t => new ViewListItem(t.Value, t.Key.ToString())).ToList();
            }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<Invoice>> Save;
        public event EventHandler<ViewEventArgs<Invoice>> Delete;
        public event EventHandler<ViewEventArgs<Invoice>> Lock;
        public event EventHandler<ViewEventArgs<Invoice>> UnLock;
        public event EventHandler<ViewEventArgs<Invoice>> Search;
        public event EventHandler<ViewEventArgs<string>> PrefixSearch;
        public event EventHandler<ViewEventArgs<Invoice>> LoadAuditLog;
        public event EventHandler<ViewEventArgs<string>> CustomerSearch;
        public event EventHandler<ViewEventArgs<long>> LoadAndLockMiscReceiptsApplicableToPostedInvoice;
        public event EventHandler<ViewEventArgs<Invoice, List<MiscReceiptApplication>>> SaveMiscReceiptApplicationsAndInvoice;
        public event EventHandler<ViewEventArgs<Invoice, List<MiscReceipt>>> UnlockInvoiceAndMiscReceipts;

        public void DisplayPrefix(Prefix prefix)
        {
            txtPrefixCode.Text = prefix == null ? string.Empty : prefix.Code;
            txtPrefixDescription.Text = prefix == null ? string.Empty : prefix.Description;
            hidPrefixId.Value = prefix == null ? default(long).ToString() : prefix.Id.ToString();
        }

        public void DisplayCustomer(Customer customer)
        {
            var customerIsNull = customer == null;
            var customerIsNotNull = customer != null;

            txtCustomerNumber.Text = customerIsNull ? string.Empty : customer.CustomerNumber;
            txtCustomerName.Text = customerIsNull ? string.Empty : customer.Name;
            hidCustomerId.Value = customerIsNull ? default(long).ToString() : customer.Id.ToString();
            ddlCustomerLocation.Enabled = customerIsNotNull;

            btnAddServiceTicket.Enabled = customerIsNotNull;
            btnAddShipment.Enabled = customerIsNotNull;
            btnAddDetail.Enabled = customerIsNotNull;
            btnAddOriginalInvoiceDetail.Enabled = customerIsNotNull;

            var customerLocations =
                customerIsNotNull && customer.Locations.Any()
                    ? customer.Locations
                        .Select(l => new ViewListItem(string.Format("{0} - {1} {2}", l.LocationNumber, l.GetBillToString(), l.FullAddress), l.Id.ToString().Trim()))
                        .ToList()
                    : new List<ViewListItem> { new ViewListItem(WebApplicationConstants.NotAvailable, default(long).GetString()) };

            ddlCustomerLocation.DataSource = customerLocations;
            ddlCustomerLocation.DataBind();
            var id = customerIsNotNull && customer.Locations.Any(l => l.BillToLocation && l.MainBillToLocation)
                        ? customer.Locations.First(l => l.BillToLocation && l.MainBillToLocation).Id
                        : default(long);
            if (ddlCustomerLocation.ContainsValue(id.ToString())) ddlCustomerLocation.SelectedValue = id.ToString();

            ibtnCustomerControlAccountSearch.Visible = customerIsNotNull;
            customerControlAccountFinder.CustomerIdFilter = customerIsNull ? default(long) : customer.Id;

            if (hidPrefixId.Value.ToLong() == default(long) && customerIsNotNull && customer.PrefixId != default(long))
                DisplayPrefix(customer.Prefix);

            if (customerIsNull) return;


            txtCustomerControlAccount.Text = string.Empty;
            customerControlAccountFinder.Reset();
            shipmentFinder.Reset();
            serviceTicketFinder.Reset();

            var customerLocationAddresses =
                    customer.Locations
                    .Select(l => new
                    {
                        l.Id,
                        BillTo = l.GetBillToString(),
                        l.Street1,
                        l.Street2,
                        l.City,
                        l.State,
                        l.PostalCode,
                        CountryName = l.Country.Name
                    }).ToList();


            rptCustomerLocations.DataSource = customerLocationAddresses;
            rptCustomerLocations.DataBind();

            if (lstInvoiceDetails.Items.Any() && hidFlag.Value == CustomerChangedFlag)
            {
                hidMessageBoxFlag.Value = CustomerChangedFlag;
                messageBox.Title = "Customer Changed";
                messageBox.Message = "The customer has changed. Do you want to clear the invoice details?";
                messageBox.Button = MessageButton.YesNo;
                messageBox.Icon = MessageIcon.Question;
                messageBox.Visible = true;
            }
        }

        public void DisplayMiscReceipts(List<MiscReceiptViewSearchDto> miscReceipts)
        {
            rptShipmentsMiscReceiptApplications.DataSource = miscReceipts;
            rptShipmentsMiscReceiptApplications.DataBind();

            var paidAmount = txtPaidAmount.Text.ToDecimal();
            paidAmount -= miscReceipts.SelectMany(mr => mr.MiscReceiptApplications).Sum(mra => mra.Amount);
            txtPaidAmountAmountUnrelatedToMiscReceiptApplications.Text = paidAmount.GetString();
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            if (!messages.HasErrors() && !messages.HasWarnings() && (hidFlag.Value == SaveFlag || hidFlag.Value == PostAndGenerateFlag))
            {
                // unlock and return to read-only
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<Invoice>(new Invoice(hidInvoiceId.Value.ToLong(), false)));

                switch (hidFlag.Value)
                {
                    case SaveFlag:
                        SetEditStatus(false);
                        break;
                    case PostAndGenerateFlag:
                        SetEditStatus(false);
                        GenerateInvoiceDocument();
                        var invoice = new Invoice(hidInvoiceId.Value.ToLong());
                        var miscReceipts = new MiscReceiptSearch().FetchMiscReceiptsApplicableToPostedInvoice(invoice.Id, invoice.TenantId);
                        hidMiscReceiptsRelatedToPostedInvoiceExists.Value = (invoice.Posted && miscReceipts.Any()).ToString();
                        hidMiscReceiptApplicationsRelatedToPostedInvoiceExists.Value = (invoice.Posted && miscReceipts.SelectMany(mr => mr.MiscReceiptApplications).Any(mra => mra.InvoiceId == invoice.Id)).ToString();
                        memberToolBar.LoadAdditionalActions(ToolbarMoreActions());
                        break;
                }
                memberToolBar.ShowUnlock = false;
            }

            if (!messages.HasErrors() && !messages.HasWarnings() && hidFlag.Value == UpdateMiscReceiptApplicationsFlag)
            {
                pnlModifyApplicationOfMiscReceiptsToInvoice.Visible = false;
                pnlDimScreen.Visible = false;
                var invoice = new Invoice(hidInvoiceId.Value.ToLong());
                LoadInvoice(invoice);
                if (LoadAuditLog != null)
                    LoadAuditLog(this, new ViewEventArgs<Invoice>(invoice));
            }

            hidMessageBoxFlag.Value = string.Empty;
            messageBox.Icon = messages.GenerateIcon();
            messageBox.Button = MessageButton.Ok;
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;

            litErrorMessages.Text = messages.HasErrors()
                                        ? string.Join(WebApplicationConstants.HtmlBreak,
                                                      messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
                                                        Select(m => m.Message).ToArray())
                                        : string.Empty;
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs)
        {
            auditLogs.Load(logs);
        }

        public void FailedLock(Lock @lock)
        {
            memberToolBar.ShowNew = false;
            memberToolBar.ShowSave = false;
            memberToolBar.ShowDelete = false;
            memberToolBar.ShowUnlock = false;

            pnlDimScreen.Visible = false;
            pnlEditPostedInvoice.Visible = false;

            SetEditStatus(false);

            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void FailedMiscReceiptLocks()
        {
            pnlDimScreen.Visible = false;
            pnlModifyApplicationOfMiscReceiptsToInvoice.Visible = false;
        }

        public void Set(Invoice invoice)
        {
            LoadInvoice(invoice);
            SetEditStatus(!invoice.IsNew && invoice.ShouldEnable());
        }


        private List<ToolbarMoreAction> ToolbarMoreActions()
        {
            var invoiceDashboard = new ToolbarMoreAction
            {
                CommandArgs = ReturnToDashboardArgs,
                ImageUrl = IconLinks.Finance,
                Name = ViewCode.InvoiceDashboard.FormattedString(),
                IsLink = false,
                NavigationUrl = string.Empty
            };

            var post = new ToolbarMoreAction
            {
                CommandArgs = PostArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Finance,
                Name = "Post Invoice",
                IsLink = false,
                NavigationUrl = string.Empty
            };

            var postGenerate = new ToolbarMoreAction
            {
                CommandArgs = PostGenerateArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Finance,
                IsLink = false,
                Name = "Post Invoice & Generate Invoice Document",
                NavigationUrl = string.Empty
            };

            var invoiceDocument = new ToolbarMoreAction
            {
                CommandArgs = InvoiceDocumentArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Finance,
                IsLink = false,
                Name = "Generate Invoice Document",
                NavigationUrl = string.Empty
            };

            var updateAmountPaid = new ToolbarMoreAction
            {
                CommandArgs = UpdateAmountPaidArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Finance,
                IsLink = false,
                Name = "Update Amount Paid",
                NavigationUrl = string.Empty
            };

            var updateAmountPaidIncludingMiscReceiptApplication = new ToolbarMoreAction
            {
                CommandArgs = UpdateAmountPaidWithMiscReceiptApplicationsArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Finance,
                IsLink = false,
                Name = "Update Amount Paid",
                NavigationUrl = string.Empty
            };

            var manualExport = new ToolbarMoreAction
            {
                CommandArgs = ManualExportArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Finance,
                IsLink = false,
                Name = "Set Exported",
                NavigationUrl = string.Empty
            };

            var reverseExport = new ToolbarMoreAction
            {
                CommandArgs = ReverseExportArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Finance,
                IsLink = false,
                Name = "Reverse Export",
                NavigationUrl = string.Empty
            };

            var resendInvoicePostedNotification = new ToolbarMoreAction
            {
                CommandArgs = ResendInvoicePostedNotificationsArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Finance,
                IsLink = false,
                Name = "Resend Invoice Posted Notifications",
                NavigationUrl = string.Empty
            };

            var applyMiscReceiptsToInvoice = new ToolbarMoreAction
            {
                CommandArgs = ApplyMiscReceiptsToInvoicesArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Finance,
                IsLink = false,
                Name = "Apply Misc Receipts To Invoice",
                NavigationUrl = string.Empty
            };


            var separator = new ToolbarMoreAction { IsSeperator = true };

            var notNew = hidInvoiceId.Value.ToLong() != default(long);
            var actions = new List<ToolbarMoreAction>();

            if (Access.Modify && notNew && !chkPosted.Checked)
            {
                actions.Add(post);
                actions.Add(postGenerate);
            }

            if (Access.Grant && notNew && chkPosted.Checked)
            {
                if (actions.Any()) actions.Add(separator);
                actions.Add(invoiceDocument);
            }

            if (Access.Modify && chkPosted.Checked)
            {
                if (actions.Any()) actions.Add(separator);

                actions.Add(hidMiscReceiptApplicationsRelatedToPostedInvoiceExists.Value.ToBoolean()
                                ? updateAmountPaidIncludingMiscReceiptApplication
                                : updateAmountPaid);
                
                if (hidMiscReceiptsRelatedToPostedInvoiceExists.Value.ToBoolean())
                    actions.Add(applyMiscReceiptsToInvoice);
                
                actions.Add(chkExported.Checked ? reverseExport : manualExport);
                if (CanSendInvoicePostedNotification()) 
                    actions.Add(resendInvoicePostedNotification);
            }

            if (actions.Any()) actions.Add(separator);

            actions.Add(invoiceDashboard);

            return actions;
        }

        private void SetEditStatus(bool enabled)
        {
            hidEditStatus.Value = enabled.ToString();

            pnlDetails.Enabled = enabled;
            ibtnOriginInvoiceFinder.Enabled = enabled && txtInvoiceType.Text != InvoiceType.Invoice.ToString();
            pnlInvoiceDetails.Enabled = enabled;

            memberToolBar.ShowImport = enabled;
            memberToolBar.EnableSave = enabled;

            litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;

            // must do this here to load correct set of extensible actions
            memberToolBar.LoadAdditionalActions(ToolbarMoreActions());
        }

        private void SetInvoiceDetailsButtonConfiguration(InvoiceType value)
        {
            switch (value)
            {
                case InvoiceType.Invoice:
                    txtInvoiceType.Text = InvoiceType.Invoice.ToString();
                    btnAddShipment.Visible = true;
                    btnAddServiceTicket.Visible = true;
                    btnAddDetail.Visible = true;
                    btnAddOriginalInvoiceDetail.Visible = false;
                    txtOriginalInvoiceNumber.Text = WebApplicationConstants.NotApplicable;
                    ibtnOriginInvoiceFinder.Enabled = false;
                    break;
                case InvoiceType.Supplemental:
                    txtInvoiceType.Text = InvoiceType.Supplemental.ToString();
                    btnAddShipment.Visible = false;
                    btnAddServiceTicket.Visible = false;
                    btnAddOriginalInvoiceDetail.Visible = true;
                    btnAddDetail.Visible = true;
                    txtOriginalInvoiceNumber.Text = string.Empty;
                    ibtnOriginInvoiceFinder.Enabled = true;
                    break;
                case InvoiceType.Credit:
                    txtInvoiceType.Text = InvoiceType.Credit.ToString();
                    btnAddShipment.Visible = false;
                    btnAddServiceTicket.Visible = false;
                    btnAddDetail.Visible = true;
                    btnAddOriginalInvoiceDetail.Visible = true;
                    txtOriginalInvoiceNumber.Text = string.Empty;
                    ibtnOriginInvoiceFinder.Enabled = true;
                    break;
            }
        }


        private void UpdateInvoiceDetailsFromView(Invoice invoice)
        {
            var details = lstInvoiceDetails.Items
                .Select(i =>
                {
                    var id = i.FindControl("hidInvoiceDetailId").ToCustomHiddenField().Value.ToLong();
                    return new InvoiceDetail(id, id != default(long))
                        {
                            ReferenceNumber = i.FindControl("txtRefNumber").ToTextBox().Text,
                            ReferenceType = i.FindControl("ddlReferenceType").ToDropDownList().SelectedValue.ToInt().ToEnum<DetailReferenceType>(),
                            Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
                            UnitSell = i.FindControl("txtUnitSell").ToTextBox().Text.ToDecimal(),
                            UnitDiscount = i.FindControl("txtUnitDiscount").ToTextBox().Text.ToDecimal(),
							ChargeCodeId = i.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                            AccountBucketId = i.FindControl("ddlAccountBucket").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                            Comment = i.FindControl("txtComment").ToTextBox().Text,
                            Invoice = invoice,
                            Tenant = invoice.Tenant
                        };
                })
                .ToList();

            invoice.Details = details;
        }

        private Invoice UpdateInvoice()
        {
            var invoiceId = hidInvoiceId.Value.ToLong();
            var invoice = new Invoice(invoiceId, invoiceId != default(long));

            if (!hidEditStatus.Value.ToBoolean()) return invoice;

            if (invoice.IsNew)
            {
                invoice.TenantId = ActiveUser.TenantId;
                invoice.InvoiceNumber = string.Empty;
                invoice.User = ActiveUser;
                invoice.InvoiceDate = DateTime.Now;
                invoice.DueDate = DateUtility.SystemEarliestDateTime;
                invoice.PostDate = DateUtility.SystemEarliestDateTime;
                invoice.ExportDate = DateUtility.SystemEarliestDateTime;
            }
            else
            {
                invoice.LoadCollections();
            }

            invoice.PrefixId = hidPrefixId.Value.ToLong();
            invoice.HidePrefix = chkHidePrefix.Checked;
            invoice.OriginalInvoiceId = hidOriginalInvoiceId.Value.ToLong();
            invoice.InvoiceType = txtInvoiceType.Text.ToEnum<InvoiceType>();
            invoice.InvoiceNumber = txtInvoiceNumber.Text;
            invoice.PaidAmount = txtPaidAmount.Text.ToDecimal();
            invoice.CustomerControlAccountNumber = string.IsNullOrEmpty(txtCustomerControlAccount.Text)
                                                       ? string.Empty
                                                       : txtCustomerControlAccount.Text;

            invoice.CustomerLocation = new CustomerLocation(ddlCustomerLocation.SelectedValue.ToLong());
            invoice.SpecialInstruction = txtSpecialInstructions.Text;

            //Update From View
            UpdateInvoiceDetailsFromView(invoice);

            return invoice;
        }


        private void LoadInvoice(Invoice invoice)
        {
            SetInvoiceDetailsButtonConfiguration(invoice.InvoiceType);


            if (!invoice.IsNew)
            {
                invoice.LoadCollections();
                var miscReceipts = new MiscReceiptSearch().FetchMiscReceiptsApplicableToPostedInvoice(invoice.Id, invoice.TenantId);
                hidMiscReceiptsRelatedToPostedInvoiceExists.Value = (invoice.Posted && miscReceipts.Any()).ToString();
                hidMiscReceiptApplicationsRelatedToPostedInvoiceExists.Value = (invoice.Posted && miscReceipts.SelectMany(mr => mr.MiscReceiptApplications).Any(mra => mra.InvoiceId == invoice.Id)).ToString();
            }

            hidInvoiceId.Value = invoice.Id.ToString();

            //Above Details
            txtInvoiceNumber.Text = invoice.InvoiceNumber;
			SetPageTitle(txtInvoiceNumber.Text);
            txtInvoiceType.Text = invoice.InvoiceType.ToString();
            hidOriginalInvoiceId.Value = invoice.OriginalInvoiceId.ToString();
            txtOriginalInvoiceNumber.Text = invoice.OriginalInvoice != null
                                                ? invoice.OriginalInvoice.InvoiceNumber
                                                : invoice.InvoiceType == InvoiceType.Invoice
                                                      ? WebApplicationConstants.NotApplicable
                                                      : string.Empty;
            //Details
            txtInvoiceDate.Text = invoice.InvoiceDate.FormattedShortDate();
            txtDueDate.Text = invoice.DueDate == DateUtility.SystemEarliestDateTime
                                ? string.Empty
                                : invoice.DueDate.FormattedShortDate();
            chkPosted.Checked = invoice.Posted;
            txtPostDate.Text = invoice.PostDate == DateUtility.SystemEarliestDateTime
                                ? string.Empty
                                : invoice.PostDate.FormattedShortDate();
            
            chkExported.Checked = invoice.Exported;
            txtExportDate.Text = invoice.ExportDate == DateUtility.SystemEarliestDateTime
                                    ? string.Empty
                                    : invoice.ExportDate.FormattedShortDate();
            txtPaidAmount.Text = invoice.PaidAmount.ToString("f2");
            txtUser.Text = invoice.User != null ? invoice.User.Username : ActiveUser.Username;

            if (invoice.CustomerLocation == null)
            {
                ddlCustomerLocation.Enabled = false;
                DisplayCustomer(null);
            }
            else
            {
                DisplayCustomer(invoice.CustomerLocation.Customer);
                ddlCustomerLocation.SelectedValue = invoice.CustomerLocation.Id.ToString();
            }

            txtSpecialInstructions.Text = invoice.SpecialInstruction;

            DisplayPrefix(invoice.Prefix);
            chkHidePrefix.Checked = invoice.HidePrefix;

            if (!string.IsNullOrEmpty(invoice.CustomerControlAccountNumber))
                txtCustomerControlAccount.Text = invoice.CustomerControlAccountNumber;

            //Invoice Details
            lstInvoiceDetails.DataSource = invoice.Details;
            lstInvoiceDetails.DataBind();
            SetAmountDue(invoice.Details.Sum(d => d.AmountDue));
            tabInvoiceDetails.HeaderText = invoice.Details.BuildTabCount(InvoiceDetailsHeader);

            // print logs
            var printLogs = !invoice.IsNew ? invoice.RetrieveInvoicePrintLogs() : new List<InvoicePrintLog>();
            lstPrintLogs.DataSource = printLogs;
            lstPrintLogs.DataBind();
            tabPrintLogs.HeaderText = printLogs.BuildTabCount(InvoicePrintLogsHeader);

            // adjustments
            if (invoice.InvoiceType == InvoiceType.Invoice)
            {
                var search = new InvoiceSearch();
                txtSupplementalAdjustments.Text = search.FetchInvoiceTotalSupplementalAdjustments(invoice.Id, invoice.TenantId).ToString("c2");
                txtCreditAdjustments.Text = search.FetchInvoiceTotalCreditAdjustments(invoice.Id, invoice.TenantId).ToString("c2");
            }
            else
            {
                txtSupplementalAdjustments.Text = None;
                txtCreditAdjustments.Text = None;
            }

            memberToolBar.ShowUnlock = invoice.HasUserLock(ActiveUser, invoice.Id);
        }

        private void ProcessTransferredRequest(Invoice invoice, bool disableEdit = false)
        {
            if (invoice == null) return;

            if (!ActiveUser.HasAccessToCustomer(invoice.CustomerLocation.Customer.Id)) return;

            LoadInvoice(invoice);

            if (Lock != null && !invoice.IsNew && Access.Modify && !disableEdit)
            {
                SetEditStatus(invoice.ShouldEnable());
                Lock(this, new ViewEventArgs<Invoice>(invoice));
            }
            else if (invoice.IsNew) SetEditStatus(invoice.ShouldEnable());
            else SetEditStatus(false);

            if (LoadAuditLog != null) 
                LoadAuditLog(this, new ViewEventArgs<Invoice>(invoice));
        }


        private void ReturnToDashboard()
        {
            if (UnLock != null)
                UnLock(this, new ViewEventArgs<Invoice>(new Invoice(hidInvoiceId.Value.ToLong(), false)));

            Response.Redirect(InvoiceDashboardView.PageAddress);
        }

        private void ReturnToPosting()
        {
            if (UnLock != null)
                UnLock(this, new ViewEventArgs<Invoice>(new Invoice(hidInvoiceId.Value.ToLong(), false)));

            Response.Redirect(InvoicePostingView.PageAddress);
        }


        private void PostInvoice(bool generateDocuments, bool sendNotification)
        {
            var invoice = UpdateInvoice();

            invoice.TakeSnapShot();

            invoice.PostDate = DateTime.Now;
            invoice.Posted = true;
            invoice.DueDate = invoice.PostDate.AddDays(invoice.CustomerLocation.Customer.InvoiceTerms);

            Lock(this, new ViewEventArgs<Invoice>(invoice));

            hidFlag.Value = generateDocuments ? PostAndGenerateFlag : SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<Invoice>(invoice));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<Invoice>(invoice));

            if (sendNotification) this.NotifyPostedInvoice(invoice);
        }

        private void GenerateInvoiceDocument()
        {
            var invoice = UpdateInvoice();
            documentDisplay.Title = string.Format("{0} Invoice", invoice.InvoiceNumber);
            documentDisplay.DisplayHtml(this.GenerateInvoice(invoice));
            documentDisplay.Visible = true;

            // reload invoice print logs
            var printLogs = invoice.RetrieveInvoicePrintLogs();
            lstPrintLogs.DataSource = printLogs;
            lstPrintLogs.DataBind();
            tabPrintLogs.HeaderText = printLogs.BuildTabCount(InvoicePrintLogsHeader);
        }


        private void PreProcessPosting()
        {
            var invoice = UpdateInvoice();

            if (invoice.Details.Sum(d => d.AmountDue) == 0)
            {
                const string msg = @"You are about to post an invoice with a zero balance.  Do you want to continue? y/n:";
                hidMessageBoxFlag.Value = ZeroAmountDueWarningFlag;
                messageBox.Icon = MessageIcon.Question;
                messageBox.Button = MessageButton.YesNo;
                messageBox.Message = msg;
                messageBox.Visible = true;
                return;
            }
            ProcessPosting();
        }

        private void ProcessPosting()
        {
            if (CanSendInvoicePostedNotification())
                DisplaySendNotificationsQuestion();
            else if (hidPostTypeFlag.Value == PostAndGenerateFlag)
                PostInvoice(hidPostTypeFlag.Value == PostAndGenerateFlag, false);
            else
                DisplayNoAutoNotificationOnCustomerMessage();
        }

        private void DisplayNoAutoNotificationOnCustomerMessage()
        {
            const string msg =
                @"You are posting an invoice without generating the invoice document. The customer account 
					on this invoice has no automatic notification. Do you want to continue? y/n:";
            hidMessageBoxFlag.Value = NoAutoNotifiationFlag;
            messageBox.Icon = MessageIcon.Question;
            messageBox.Button = MessageButton.YesNo;
            messageBox.Message = msg;
            messageBox.Visible = true;
        }

        private bool CanSendInvoicePostedNotification()
        {
            var customerId = hidCustomerId.Value.ToLong();
            if (customerId == default(long)) return false;

            var customer = new Customer(customerId);

            var communication = customer.Communication;
            if (communication == null) return false;

            return communication.Notifications.Any(n => n.Milestone == Milestone.InvoicePosted && n.Enabled);
        }

        private void DisplaySendNotificationsQuestion()
        {
            hidMessageBoxFlag.Value = NotificationFlag;
            messageBox.Button = MessageButton.YesNo;
            messageBox.Icon = MessageIcon.Question;
            messageBox.Message = @"Send Notifications?";
            messageBox.Visible = true;
        }

        private void ResendInvoicePostedNotifications()
        {
            this.NotifyPostedInvoice(new Invoice(hidInvoiceId.Value.ToLong()));
            DisplayMessages(new []{ ValidationMessage.Information("Invoice posted notifications have been resent.") });
        }

        private void SetExported()
        {
            var invoice = UpdateInvoice();

            invoice.TakeSnapShot();

            invoice.ExportDate = DateTime.Now;
            invoice.Exported = true;

            Lock(this, new ViewEventArgs<Invoice>(invoice));

            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<Invoice>(invoice));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<Invoice>(invoice));
        }

        private void ReverseExported()
        {
            var invoice = UpdateInvoice();

            invoice.TakeSnapShot();

            invoice.ExportDate = DateUtility.SystemEarliestDateTime;
            invoice.Exported = false;

            Lock(this, new ViewEventArgs<Invoice>(invoice));

            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<Invoice>(invoice));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<Invoice>(invoice));
        }


        private void DisplayApplyMiscReceiptsToInvoicePopup()
        {
            amrtiApplyMiscReceiptsToInvoice.Visible = true;
            amrtiApplyMiscReceiptsToInvoice.LoadInvoiceAndRelatedMiscReceipts(hidInvoiceId.Value.ToLong());
        }


        private void ResetFlags()
        {
            hidPostTypeFlag.Value = string.Empty;
            hidMilestone.Value = string.Empty;
            hidFlag.Value = string.Empty;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new InvoiceHandler(this).Initialize();

            memberToolBar.ShowSave = Access.Modify;
            memberToolBar.ShowDelete = Access.Remove;
            memberToolBar.ShowNew = Access.Modify;
            memberToolBar.ShowEdit = Access.Modify;
            memberToolBar.LoadAdditionalActions(ToolbarMoreActions());

            btnInvoiceSelect.CommandArgument = InvoiceType.Invoice.ToString();
            btnCreditSelect.CommandArgument = InvoiceType.Credit.ToString();
            btnSupplementalSelect.CommandArgument = InvoiceType.Supplemental.ToString();

			SetPageTitle(txtInvoiceNumber.Text);

            if (IsPostBack) return;

            aceCustomer.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.ActiveUser, ActiveUser.Id.GetString() } });
            acePrefixCode.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString() } });

            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.InvoiceDetailImportTemplate });


            if (Loading != null)
                Loading(this, new EventArgs());

            SetEditStatus(false);

            if (Session[WebApplicationConstants.TransferInvoiceId] != null)
            {
                ProcessTransferredRequest(new Invoice(Session[WebApplicationConstants.TransferInvoiceId].ToLong()));
                Session[WebApplicationConstants.TransferInvoiceId] = null;
            }

            if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.TransferNumber]))
                ProcessTransferredRequest(new InvoiceSearch().FetchInvoiceByInvoiceNumber(Request.QueryString[WebApplicationConstants.TransferNumber].GetString(), ActiveUser.TenantId), true);
        }


        protected void OnToolbarFindClicked(object sender, EventArgs e)
        {
            if (!invoiceFinder.OpenForEditEnabled)
            {
                invoiceFinder.OpenForEditEnabled = Access.Modify;
                invoiceFinder.Reset();
            }

            hidFlag.Value = string.Empty;
            invoiceFinder.EnableMultiSelection = false;
            invoiceFinder.ShowInvoiceTypeOnly = false;
            invoiceFinder.Visible = true;
        }

        protected void OnToolbarSaveClicked(object sender, EventArgs e)
        {
            var invoice = UpdateInvoice();

            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<Invoice>(invoice));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<Invoice>(invoice));
        }

        protected void OnToolbarDeleteClicked(object sender, EventArgs e)
        {
            var invoice = new Invoice(hidInvoiceId.Value.ToLong(), false);

            if (invoice.Posted)
            {
                DisplayMessages(new[] { ValidationMessage.Error(string.Format("Invoice {0} is posted, can not delete", invoice.InvoiceNumber)) });
                return;
            }

            if (Delete != null)
                Delete(this, new ViewEventArgs<Invoice>(invoice));

            invoiceFinder.Reset();
            serviceTicketFinder.Reset();
            shipmentFinder.Reset();
        }

        protected void OnToolbarNewClicked(object sender, EventArgs e)
        {
            litErrorMessages.Text = string.Empty;

            invoiceFinder.Reset();
            serviceTicketFinder.Reset();
            shipmentFinder.Reset();

            ResetFlags();

            pnlDimScreen.Visible = true;
            pnlNewInvoiceSelection.Visible = true;
        }

        protected void OnToolbarEditClicked(object sender, EventArgs e)
        {
            LockRecord(new Invoice(hidInvoiceId.Value.ToLong(), false));
        }

        protected void OnToolbarUnlockClicked(object sender, EventArgs e)
        {
            var invoice = new Invoice(hidInvoiceId.Value.ToLong(), false);
            if (UnLock == null || invoice.IsNew) return;

            SetEditStatus(false);
            memberToolBar.ShowUnlock = false;
            UnLock(this, new ViewEventArgs<Invoice>(invoice));
        }

        protected void OnToolbarCommand(object sender, ViewEventArgs<string> e)
        {
            switch (e.Argument)
            {
                case ReturnToDashboardArgs:
                    ReturnToDashboard();
                    break;
                case ReturnToPostingArgs:
                    ReturnToPosting();
                    break;
                case PostArgs:
                    hidPostTypeFlag.Value = PostFlag;
                    PreProcessPosting();
                    break;
                case PostGenerateArgs:
                    hidPostTypeFlag.Value = PostAndGenerateFlag;
                    PreProcessPosting();
                    break;
                case InvoiceDocumentArgs:
                    GenerateInvoiceDocument();
                    break;
                case UpdateAmountPaidArgs:
                    UpdateAmountPaid();
                    break;
                case UpdateAmountPaidWithMiscReceiptApplicationsArgs:
                    UpdateAmountPaidWithMiscReceiptApplications();
                    break;
                case ManualExportArgs:
                    SetExported();
                    break;
                case ReverseExportArgs:
                    ReverseExported();
                    break;
                case ResendInvoicePostedNotificationsArgs:
                    ResendInvoicePostedNotifications();
                    break;
                case ApplyMiscReceiptsToInvoicesArgs:
                    DisplayApplyMiscReceiptsToInvoicePopup();
                    break;
            }
        }

        private void LockRecord(Invoice invoice)
        {
            if (Lock == null || invoice.IsNew) return;

            SetEditStatus(invoice.ShouldEnable());
            if (invoice.ShouldEnable()) memberToolBar.ShowUnlock = true;
            Lock(this, new ViewEventArgs<Invoice>(invoice));

            LoadInvoice(invoice);
        }


        protected void OnInvoiceTypeSelectClicked(object sender, EventArgs e)
        {
            var value = ((Button)sender).Text;

            var invoice = new Invoice
            {
                InvoiceDate = DateTime.Now,
                DueDate = DateUtility.SystemEarliestDateTime,
                PostDate = DateUtility.SystemEarliestDateTime,
                ExportDate = DateUtility.SystemEarliestDateTime,
                InvoiceType = value.ToEnum<InvoiceType>(),
                Details = new List<InvoiceDetail>(),
            };

            pnlNewInvoiceSelection.Visible = false;
            pnlDimScreen.Visible = false;

            LoadInvoice(invoice);

            DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
            SetEditStatus(true);
        }

        protected void OnCloseNewInvoiceSelectionTypeClicked(object sender, EventArgs e)
        {
            pnlNewInvoiceSelection.Visible = false;
            pnlDimScreen.Visible = false;
        }


        protected void OnInvoiceFinderItemSelected(object sender, ViewEventArgs<Invoice> e)
        {
            litErrorMessages.Text = string.Empty;

            invoiceFinder.Visible = false;

            switch (hidFlag.Value)
            {
                case OriginalInvoiceFind:
                    txtOriginalInvoiceNumber.Text = e.Argument.InvoiceNumber;
                    hidOriginalInvoiceId.Value = e.Argument.Id.ToString();
                    hidFlag.Value = string.Empty;
                    break;
                default:
                    ResetFlags();
                    if (hidInvoiceId.Value.ToLong() != default(long))
                    {
                        var oldInvoice = new Invoice(hidInvoiceId.Value.ToLong(), false);
                        if (UnLock != null)
                            UnLock(this, new ViewEventArgs<Invoice>(oldInvoice));
                    }
                    LoadInvoice(e.Argument);
                    if (invoiceFinder.OpenForEdit && Lock != null)
                    {
                        SetEditStatus(e.Argument.ShouldEnable());
                        if (e.Argument.ShouldEnable()) memberToolBar.ShowUnlock = true;
                        Lock(this, new ViewEventArgs<Invoice>(e.Argument));
                    }
                    else
                    {
                        SetEditStatus(false);
                    }

                    if (LoadAuditLog != null)
                        LoadAuditLog(this, new ViewEventArgs<Invoice>(e.Argument));

                    break;
            }
        }

        protected void OnInvoiceFinderItemCancelled(object sender, EventArgs e)
        {
            invoiceFinder.Visible = false;
        }


        protected void OnCustomerNumberEntered(object sender, EventArgs e)
        {
            hidFlag.Value = CustomerChangedFlag;
            if (CustomerSearch != null)
                CustomerSearch(this, new ViewEventArgs<string>(txtCustomerNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
        }

        protected void OnCustomerSearchClicked(object sender, ImageClickEventArgs e)
        {
            hidFlag.Value = CustomerChangedFlag;
            customerFinder.Visible = true;
        }

        protected void OnCustomerFinderSelectionCancelled(object sender, EventArgs e)
        {
            hidFlag.Value = string.Empty;
            customerFinder.Visible = false;
        }

        protected void OnCustomerFinderItemSelected(object sender, ViewEventArgs<Customer> e)
        {
            DisplayCustomer(e.Argument);
            customerFinder.Visible = false;
        }


        protected void OnAddShipmentClicked(object sender, EventArgs e)
        {
            shipmentFinder.CustomerId = hidCustomerId.Value.ToLong();
            shipmentFinder.Visible = true;
        }

        protected void OnShipmentFinderMultiItemSelected(object sender, ViewEventArgs<List<Shipment>> e)
        {
            var details = lstInvoiceDetails.Items
                .Select(i => new
                {
                    Id = i.FindControl("hidInvoiceDetailId").ToCustomHiddenField().Value.ToLong(),
                    ReferenceNumber = i.FindControl("txtRefNumber").ToTextBox().Text,
                    ReferenceType = i.FindControl("ddlReferenceType").ToDropDownList().SelectedValue.ToInt().ToEnum<DetailReferenceType>(),
                    Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
                    UnitSell = i.FindControl("txtUnitSell").ToTextBox().Text.ToDecimal(),
                    UnitDiscount = i.FindControl("txtUnitDiscount").ToTextBox().Text.ToDecimal(),
                    AmountDue = i.FindControl("txtAmountDue").ToTextBox().Text.ToDecimal(),
					ChargeCodeId = i.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    AccountBucketId = i.FindControl("ddlAccountBucket").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    Comment = i.FindControl("txtComment").ToTextBox().Text
                })
                .ToList();

            foreach (var shipment in e.Argument)
            {
                if (shipment.Status == ShipmentStatus.Invoiced)
                {
                    DisplayMessages(new[] { ValidationMessage.Error(string.Format("Shipment {0} is already invoiced.", shipment.ShipmentNumber)) });
                    return;
                }

                var sh = shipment;
                var tdetails = shipment.Charges
                    .Select(c => new
                    {
                        Id = default(long),
                        ReferenceNumber = sh.ShipmentNumber,
                        ReferenceType = DetailReferenceType.Shipment,
                        c.Quantity,
                        UnitSell = c.UnitSell -
                                   (sh.ResellerAdditionId != default(long) && sh.BillReseller
                                        ? c.GetResellerAdditions(sh.ResellerAddition)
                                        : 0),
                        c.UnitDiscount,
                        c.AmountDue,
                        c.ChargeCodeId,
                        sh.AccountBuckets.First(a => a.Primary).AccountBucketId,
                        c.Comment
                    });

                details.AddRange(tdetails);
            }

            lstInvoiceDetails.DataSource = details;
            lstInvoiceDetails.DataBind();
            SetAmountDue(details.Sum(d => d.AmountDue));
            tabInvoiceDetails.HeaderText = details.BuildTabCount(InvoiceDetailsHeader);
            shipmentFinder.Visible = false;
        }

        protected void OnShipmentFinderItemCancelled(object sender, EventArgs e)
        {
            shipmentFinder.Visible = false;
        }


        protected void OnAddServiceTicketClicked(object sender, EventArgs e)
        {
            serviceTicketFinder.CustomerId = hidCustomerId.Value.ToLong();
	        serviceTicketFinder.CustomerLocationId = ddlCustomerLocation.SelectedValue.ToLong();
            serviceTicketFinder.Visible = true;
        }

        protected void OnServiceTicketFinderMultiItemSelected(object sender, ViewEventArgs<List<ServiceTicket>> e)
        {
            var details = lstInvoiceDetails.Items
               .Select(i => new
               {
                   Id = i.FindControl("hidInvoiceDetailId").ToCustomHiddenField().Value.ToLong(),
                   ReferenceNumber = i.FindControl("txtRefNumber").ToTextBox().Text,
                   ReferenceType = i.FindControl("ddlReferenceType").ToDropDownList().SelectedValue.ToInt().ToEnum<DetailReferenceType>(),
                   Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
                   UnitSell = i.FindControl("txtUnitSell").ToTextBox().Text.ToDecimal(),
                   UnitDiscount = i.FindControl("txtUnitDiscount").ToTextBox().Text.ToDecimal(),
                   AmountDue = i.FindControl("txtAmountDue").ToTextBox().Text.ToDecimal(),
				   ChargeCodeId = i.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                   AccountBucketId = i.FindControl("ddlAccountBucket").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                   Comment = i.FindControl("txtComment").ToTextBox().Text
               })
               .ToList();

            foreach (var serviceTicket in e.Argument)
            {
                if (serviceTicket.Status == ServiceTicketStatus.Invoiced)
                {
                    DisplayMessages(new[] { ValidationMessage.Error(string.Format("Service Ticket {0} is already invoiced.", serviceTicket.ServiceTicketNumber)) });
                    return;
                }

                var st = serviceTicket;
                var tdetails = serviceTicket.Charges
                    .Select(c => new
                    {
                        Id = default(long),
                        ReferenceNumber = st.ServiceTicketNumber,
                        ReferenceType = DetailReferenceType.ServiceTicket,
                        c.Quantity,
                        UnitSell = c.UnitSell -
                                   (st.ResellerAdditionId != default(long) && st.BillReseller
                                        ? c.GetResellerAdditions(st.ResellerAddition)
                                        : 0),
                        c.UnitDiscount,
                        c.AmountDue,
                        c.ChargeCodeId,
                        AccountBucketId = st.AccountBucket.Id,
                        c.Comment
                    });

                details.AddRange(tdetails);
            }

            lstInvoiceDetails.DataSource = details;
            lstInvoiceDetails.DataBind();
            SetAmountDue(details.Sum(d => d.AmountDue));
            tabInvoiceDetails.HeaderText = details.BuildTabCount(InvoiceDetailsHeader);
            serviceTicketFinder.Visible = false;
        }

        protected void OnServiceTicketFinderItemCancelled(object sender, EventArgs e)
        {
            serviceTicketFinder.Visible = false;
        }


        protected void OnAddDetailClicked(object sender, EventArgs e)
        {
            var details = lstInvoiceDetails.Items
                .Select(i => new
                {
                    Id = i.FindControl("hidInvoiceDetailId").ToCustomHiddenField().Value.ToLong(),
                    ReferenceNumber = i.FindControl("txtRefNumber").ToTextBox().Text,
                    ReferenceType = i.FindControl("ddlReferenceType").ToDropDownList().SelectedValue.ToInt().ToEnum<DetailReferenceType>(),
                    Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
                    UnitSell = i.FindControl("txtUnitSell").ToTextBox().Text.ToDecimal(),
                    UnitDiscount = i.FindControl("txtUnitDiscount").ToTextBox().Text.ToDecimal(),
                    AmountDue = i.FindControl("txtAmountDue").ToTextBox().Text.ToDecimal(),
					ChargeCodeId = i.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    AccountBucketId = i.FindControl("ddlAccountBucket").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    Comment = i.FindControl("txtComment").ToTextBox().Text
                })
                .ToList();

            details.Add(new
                {
                    Id = default(long),
                    ReferenceNumber = string.Empty,
                    ReferenceType = DetailReferenceType.Miscellaneous,
                    Quantity = 1,
                    UnitSell = 0.00m,
                    UnitDiscount = 0.00m,
                    AmountDue = 0.00m,
                    ChargeCodeId = default(long),
                    AccountBucketId = default(long),
                    Comment = string.Empty
                });

            lstInvoiceDetails.DataSource = details;
            lstInvoiceDetails.DataBind();
            tabInvoiceDetails.HeaderText = details.BuildTabCount(InvoiceDetailsHeader);
            athtuTabUpdater.SetForUpdate(tabInvoiceDetails.ClientID, details.BuildTabCount(InvoiceDetailsHeader));
        }

        protected void OnDeleteInvoiceDetailClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var details = lstInvoiceDetails.Items
               .Select(i => new
               {
                   Id = i.FindControl("hidInvoiceDetailId").ToCustomHiddenField().Value.ToLong(),
                   ReferenceNumber = i.FindControl("txtRefNumber").ToTextBox().Text,
                   ReferenceType = i.FindControl("ddlReferenceType").ToDropDownList().SelectedValue.ToInt().ToEnum<DetailReferenceType>(),
                   Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
                   UnitSell = i.FindControl("txtUnitSell").ToTextBox().Text.ToDecimal(),
                   UnitDiscount = i.FindControl("txtUnitDiscount").ToTextBox().Text.ToDecimal(),
                   AmountDue = i.FindControl("txtAmountDue").ToTextBox().Text.ToDecimal(),
				   ChargeCodeId = i.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                   AccountBucketId = i.FindControl("ddlAccountBucket").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                   Comment = i.FindControl("txtComment").ToTextBox().Text
               })
               .ToList();

            details.RemoveAt(imageButton.Parent.FindControl("hidInvoiceDetailIndex").ToCustomHiddenField().Value.ToInt());

            lstInvoiceDetails.DataSource = details;
            lstInvoiceDetails.DataBind();
            SetAmountDue(details.Sum(d => d.AmountDue));
            tabInvoiceDetails.HeaderText = details.BuildTabCount(InvoiceDetailsHeader);
            athtuTabUpdater.SetForUpdate(tabInvoiceDetails.ClientID, details.BuildTabCount(InvoiceDetailsHeader));
        }

        protected void OnInvoiceDetailsItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var item = (ListViewDataItem)e.Item;
            var dataItem = item.DataItem;

            var invoiceType = txtInvoiceType.Text.ToEnum<InvoiceType>();
            var isCreditorSupplemental = invoiceType == InvoiceType.Credit || invoiceType == InvoiceType.Supplemental;
            var referenceType = dataItem.GetPropertyValue("ReferenceType").ToInt().ToEnum<DetailReferenceType>();

            var txtRefNumber = item.FindControl("txtRefNumber").ToTextBox();
            txtRefNumber.ReadOnly = !isCreditorSupplemental;
            txtRefNumber.CssClass = isCreditorSupplemental ? "w200" : "w200 disabled";

            var ddlReferenceType = item.FindControl("ddlReferenceType").ToDropDownList();
            ddlReferenceType.DataSource = StoredInvoiceDetailReferenceTypes;
            ddlReferenceType.DataBind();
            if (dataItem.HasGettableProperty("ReferenceType")) ddlReferenceType.SelectedValue = dataItem.GetPropertyValue("ReferenceType").ToInt().GetString();
            ddlReferenceType.Enabled = isCreditorSupplemental;

		    var ddlAccountBucket = item.FindControl("ddlAccountBucket").ToCachedObjectDropDownList();
		    ddlAccountBucket.Enabled = referenceType == DetailReferenceType.Miscellaneous || isCreditorSupplemental;


		    var ddlChargeCode = item.FindControl("ddlChargeCode").ToCachedObjectDropDownList();
		    if (dataItem.HasGettableProperty("ChargeCodeId"))
		    {
				ddlChargeCode.SelectedValue = item.DataItem.GetPropertyValue("ChargeCodeId").GetString();
		    }
		    ddlChargeCode.Enabled = referenceType == DetailReferenceType.Miscellaneous || isCreditorSupplemental;
	 
		 }

        private void SetAmountDue(decimal amountDue)
        {
            txtTotalDue.Text = amountDue.ToString("c2");
            txtInvoiceTotal.Text = amountDue.ToString("c2");
        }

        protected void OnClearDetailsClicked(object sender, EventArgs e)
        {
            var newList = new List<InvoiceDetail>();
            lstInvoiceDetails.DataSource = newList;
            lstInvoiceDetails.DataBind();
            SetAmountDue(0.00m);
            tabInvoiceDetails.HeaderText = newList.BuildTabCount(InvoiceDetailsHeader);
            athtuTabUpdater.SetForUpdate(tabInvoiceDetails.ClientID, new List<string>().BuildTabCount(InvoiceDetailsHeader));
        }


        protected void OnCloseAddOriginalInvoiceDetailClicked(object sender, ImageClickEventArgs e)
        {
            pnlAddOriginalInvoiceDetail.Visible = false;
            pnlDimScreen.Visible = false;

        }

        protected void OnAddOriginalInvoiceDetailClicked(object sender, EventArgs e)
        {
            if (hidOriginalInvoiceId.Value.ToLong() != default(long))
            {
                var orgDetails = new Invoice(hidOriginalInvoiceId.Value.ToLong()).Details
                    .GroupBy(d => string.Format("{0} {1} [Acct. Bucket: {2}]", d.ReferenceType.FormattedString(), d.ReferenceNumber, d.AccountBucket.Code))
                    .Select(i => new ViewListItem(i.Key, i.First().Id.GetString()))
                    .ToList();
                ddlDetailOriginalReference.DataSource = orgDetails;
                ddlDetailOriginalReference.DataBind();
            }
            else
            {
                ddlDetailOriginalReference.DataSource = new List<object>();
                ddlDetailOriginalReference.DataBind();
            }

            pnlAddOriginalInvoiceDetail.Visible = true;
            pnlDimScreen.Visible = true;
        }

        protected void OnAddDoneOriginalInvoiceDetailClicked(object sender, EventArgs e)
        {
            var details = lstInvoiceDetails.Items
                .Select(i => new
                {
                    Id = i.FindControl("hidInvoiceDetailId").ToCustomHiddenField().Value.ToLong(),
                    ReferenceNumber = i.FindControl("txtRefNumber").ToTextBox().Text,
                    ReferenceType = i.FindControl("ddlReferenceType").ToDropDownList().SelectedValue.ToInt().ToEnum<DetailReferenceType>(),
                    Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
                    UnitSell = i.FindControl("txtUnitSell").ToTextBox().Text.ToDecimal(),
                    UnitDiscount = i.FindControl("txtUnitDiscount").ToTextBox().Text.ToDecimal(),
                    AmountDue = i.FindControl("txtAmountDue").ToTextBox().Text.ToDecimal(),
                    ChargeCodeId = i.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    AccountBucketId = i.FindControl("ddlAccountBucket").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                    Comment = i.FindControl("txtComment").ToTextBox().Text
                })
                .ToList();


            var orgDetail = new InvoiceDetail(ddlDetailOriginalReference.SelectedValue.ToLong());
            details.Add(new
            {
                Id = default(long),
                orgDetail.ReferenceNumber,
                orgDetail.ReferenceType,
                Quantity = 1,
                UnitSell = default(decimal),
                UnitDiscount = default(decimal),
                AmountDue = default(decimal),
                ChargeCodeId = default(long),
                orgDetail.AccountBucketId,
                Comment = string.Empty
            });

            lstInvoiceDetails.DataSource = details;
            lstInvoiceDetails.DataBind();
            SetAmountDue(details.Sum(d => d.AmountDue));
            tabInvoiceDetails.HeaderText = details.BuildTabCount(InvoiceDetailsHeader);
            athtuTabUpdater.SetForUpdate(tabInvoiceDetails.ClientID, details.BuildTabCount(InvoiceDetailsHeader));

            pnlAddOriginalInvoiceDetail.Visible = false;
            pnlDimScreen.Visible = false;
        }


        protected void OnDocumentDisplayClosed(object sender, EventArgs e)
        {
            documentDisplay.Visible = false;
        }


        protected void OnYesClicked(object sender, EventArgs e)
        {
            switch (hidMessageBoxFlag.Value)
            {
                case ZeroAmountDueWarningFlag:
                    hidMessageBoxFlag.Value = string.Empty;	// must clear here as hidMessageBoxFlag re-used in process posting method!
                    messageBox.Visible = false;
                    ProcessPosting();
                    return;
                case NotificationFlag:
                    PostInvoice(hidPostTypeFlag.Value == PostAndGenerateFlag, true);
                    break;
                case NoAutoNotifiationFlag:
                    PostInvoice(hidPostTypeFlag.Value == PostAndGenerateFlag, false);
                    break;
                case CustomerChangedFlag:
                    lstInvoiceDetails.DataSource = new List<InvoiceDetail>();
                    lstInvoiceDetails.DataBind();
                    tabInvoiceDetails.HeaderText = InvoiceDetailsHeader;
                    SetAmountDue(0.00m);
                    break;
            }

            hidMessageBoxFlag.Value = string.Empty;
            messageBox.Visible = false;
        }

        protected void OnNoClicked(object sender, EventArgs e)
        {
            if (hidMessageBoxFlag.Value == NotificationFlag)
                PostInvoice(hidPostTypeFlag.Value == PostAndGenerateFlag, false);

            hidMessageBoxFlag.Value = string.Empty;
            messageBox.Visible = false;
        }

        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;
            hidFlag.Value = string.Empty;
        }


        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            if (lines.Count > 10000)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Batch import is limited to 10,000 records, please adjust your file size.") });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 7;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            msgs.AddRange(chks.EnumsAreValid<DetailReferenceType>(1));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var accountBucketSearch = new AccountBucketSearch();
            var chargeCodeSearch = new ChargeCodeSearch();

            var accountBuckets = ProcessorVars.RegistryCache[ActiveUser.TenantId].AccountBuckets.Select(c => c.Code).ToList();
            msgs.AddRange(chks.CodesAreValid(5, accountBuckets, new AccountBucket().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var chargeCodes = ProcessorVars.RegistryCache[ActiveUser.TenantId].ChargeCodes.Select(c => c.Code).ToList();
            msgs.AddRange(chks.CodesAreValid(6, chargeCodes, new ChargeCode().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var nInvoiceDetails = lines
                .Select(s => new InvoiceDetail
                {
                    ReferenceNumber = s[0].Trim(),
                    ReferenceType = s[1].ToEnum<DetailReferenceType>(),
                    Quantity = s[2].ToInt(),
                    UnitSell = s[3].ToDecimal(),
                    UnitDiscount = s[4].ToDecimal(),
                    AccountBucket = accountBucketSearch.FetchAccountBucketByCode(s[5], ActiveUser.TenantId),
                    ChargeCodeId = chargeCodeSearch.FetchChargeCodeIdByCode(s[6], ActiveUser.TenantId),
                })
                .ToList();

            var invoiceDetails = lstInvoiceDetails.Items
                    .Select(i =>
                    {
                        var id = i.FindControl("hidInvoiceDetailId").ToCustomHiddenField().Value.ToLong();
                        return new InvoiceDetail(id)
                        {
                            ReferenceNumber = i.FindControl("txtRefNumber").ToTextBox().Text,
                            ReferenceType = i.FindControl("ddlReferenceType").ToDropDownList().SelectedValue.ToInt().ToEnum<DetailReferenceType>(),
                            Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
                            UnitSell = i.FindControl("txtUnitSell").ToTextBox().Text.ToDecimal(),
                            UnitDiscount = i.FindControl("txtUnitDiscount").ToTextBox().Text.ToDecimal(),
							ChargeCodeId = i.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                            AccountBucketId = i.FindControl("ddlAccountBucket").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                            Comment = i.FindControl("txtComment").ToTextBox().Text
                        };
                    })
                    .ToList();

            invoiceDetails.AddRange(nInvoiceDetails);
            lstInvoiceDetails.DataSource = invoiceDetails;
            lstInvoiceDetails.DataBind();
            tabInvoiceDetails.HeaderText = invoiceDetails.BuildTabCount(InvoiceDetailsHeader);
            fileUploader.Visible = false;
        }

        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnImportDetailsClicked(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(hidInvoiceId.Value))
            {
                fileUploader.Title = "INVOICE DETAIL ITEM IMPORT";
                fileUploader.Instructions = @"**Please refer to user guide for import file format";
                fileUploader.Visible = true;
            }
            else
            {
                messageBox.Button = MessageButton.Ok;
                messageBox.Icon = MessageIcon.Error;
                messageBox.Message = "Please select an invoice to batch import for";
                messageBox.Visible = true;
            }
        }


        protected void OnOriginalInvoiceSearchClicked(object sender, ImageClickEventArgs e)
        {
            if (invoiceFinder.OpenForEditEnabled)
            {
                invoiceFinder.OpenForEditEnabled = false;
                invoiceFinder.Reset();
            }

            hidFlag.Value = OriginalInvoiceFind;
            invoiceFinder.EnableMultiSelection = false;

            invoiceFinder.ShowInvoiceTypeOnly = true;
            invoiceFinder.Visible = true;
        }


        private void UpdateAmountPaid()
        {
            var invoice = new Invoice(hidInvoiceId.Value.ToLong(), false);

            txtPostedInvoiceNumber.Text = txtInvoiceNumber.Text;
            txtPostedInvoiceTotal.Text = txtInvoiceTotal.Text;
            txtUpdatePaidAmount.Text = invoice.PaidAmount.ToString(); // get current paid amounnt

            pnlDimScreen.Visible = true;
            pnlEditPostedInvoice.Visible = true;

            LockRecord(invoice);
        }

        protected void OnCloseUpdateAmountPaidClicked(object sender, EventArgs e)
        {
            pnlDimScreen.Visible = false;
            pnlEditPostedInvoice.Visible = false;

            // unlock and return to read-only
            if (UnLock != null)
                UnLock(this, new ViewEventArgs<Invoice>(new Invoice(hidInvoiceId.Value.ToLong(), false)));
            SetEditStatus(false);
        }

        protected void OnDoneUpdateAmountPaidClick(object sender, EventArgs e)
        {
            pnlDimScreen.Visible = false;
            pnlEditPostedInvoice.Visible = false;

            var invoice = new Invoice(hidInvoiceId.Value.ToLong(), true) { PaidAmount = txtUpdatePaidAmount.Text.ToDecimal() };

            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<Invoice>(invoice));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<Invoice>(invoice));
        }


        protected void OnClearCustomerControlAccount(object sender, ImageClickEventArgs e)
        {
            txtCustomerControlAccount.Text = string.Empty;
        }

        protected void OnCustomerControlAccountSearchClicked(object sender, ImageClickEventArgs e)
        {
            customerControlAccountFinder.Visible = true;
        }

        protected void OnCustomerControlAccountFinderItemSelected(object sender, ViewEventArgs<CustomerControlAccount> e)
        {
            txtCustomerControlAccount.Text = e.Argument.AccountNumber;
            customerControlAccountFinder.Visible = false;
        }

        protected void OnCustomerControlAccountFinderSelectionCancel(object sender, EventArgs e)
        {
            customerControlAccountFinder.Visible = false;
        }


        protected void OnPrefixCodeEntered(object sender, EventArgs e)
        {
            if (PrefixSearch != null)
                PrefixSearch(this, new ViewEventArgs<string>(txtPrefixCode.Text.RetrieveSearchCodeFromAutoCompleteString()));
        }

        protected void OnPrefixSearchClicked(object sender, ImageClickEventArgs e)
        {
            prefixFinder.Visible = true;
        }

        protected void OnPrefixFinderSelectionCancelled(object sender, EventArgs e)
        {
            prefixFinder.Visible = false;
        }

        protected void OnPrefixFinderItemSelected(object sender, ViewEventArgs<Prefix> e)
        {
            DisplayPrefix(e.Argument);
            prefixFinder.Visible = false;
        }


        protected void OnCloseApplyMiscReceiptToInvoiceClicked(object sender, EventArgs eventArgs)
        {
            amrtiApplyMiscReceiptsToInvoice.Visible = false;
        }

        protected void OnApplyMiscReceiptsToInvoiceDisplayProcessingMessages(object sender, ViewEventArgs<List<ValidationMessage>> e)
        {
            DisplayMessages(e.Argument);

            if (e.Argument.HasErrors() || e.Argument.HasWarnings()) return;
            
            amrtiApplyMiscReceiptsToInvoice.Visible = false;
            amrtiApplyMiscReceiptsToInvoice.DisplayMiscReceipts(new List<MiscReceiptViewSearchDto>());
            var invoice = new Invoice(hidInvoiceId.Value.ToLong());
            LoadInvoice(invoice);
            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<Invoice>(invoice));
        }


        private void UpdateAmountPaidWithMiscReceiptApplications()
        {
            txtModifyMiscReceiptAppPostedInvoiceNumber.Text = txtInvoiceNumber.Text;
            txtModifyMiscReceiptAppPostedInvoiceTotal.Text = txtInvoiceTotal.Text;

            pnlModifyApplicationOfMiscReceiptsToInvoice.Visible = true;
            pnlDimScreen.Visible = true;

            if(LoadAndLockMiscReceiptsApplicableToPostedInvoice != null)
                LoadAndLockMiscReceiptsApplicableToPostedInvoice(this, new ViewEventArgs<long>(hidInvoiceId.Value.ToLong()));
        }

        protected void OnCloseModifyApplicationOfMiscReceiptsToInvoice(object sender, EventArgs e)
        {
            pnlModifyApplicationOfMiscReceiptsToInvoice.Visible = false;
            pnlDimScreen.Visible = false;

            var miscReceipts = rptShipmentsMiscReceiptApplications
                .Items
                .Cast<RepeaterItem>()
                .Select(i => new MiscReceipt(i.FindControl("hidMiscReceiptId").ToCustomHiddenField().Value.ToLong()))
                .ToList();

            if (UnlockInvoiceAndMiscReceipts != null)
                UnlockInvoiceAndMiscReceipts(this, new ViewEventArgs<Invoice, List<MiscReceipt>>(new Invoice(hidInvoiceId.Value.ToLong()), miscReceipts));
        }

        protected void OnModifyApplicationOfMiscReceiptsToInvoice(object sender, EventArgs e)
        {
            var miscReceiptApplications = rptShipmentsMiscReceiptApplications
                .Items
                .Cast<RepeaterItem>()
                .SelectMany(i => i.FindControl("rptMiscReceiptApplications").ToRepeater()
                                  .Items
                                  .Cast<RepeaterItem>()
                                  .Select(item => new MiscReceiptApplication(item.FindControl("hidMiscReceiptApplicationId").ToCustomHiddenField().Value.ToLong(), true)
                                      {
                                          Amount = item.FindControl("txtMiscReceiptApplicationAmount").ToTextBox().Text.ToDecimal()
                                      })
                ).ToList();

            var invoice = new Invoice(hidInvoiceId.Value.ToLong(), true);
            invoice.LoadCollections();
            invoice.PaidAmount = miscReceiptApplications.Sum(m => m.Amount) + txtPaidAmountAmountUnrelatedToMiscReceiptApplications.Text.ToDecimal();

            hidFlag.Value = UpdateMiscReceiptApplicationsFlag;

            if (SaveMiscReceiptApplicationsAndInvoice != null)
                SaveMiscReceiptApplicationsAndInvoice(this, new ViewEventArgs<Invoice, List<MiscReceiptApplication>>(invoice, miscReceiptApplications));
        }


        protected void OnShipmentsMiscReceiptApplicationsItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item == null) return;

            var miscReceipt = (MiscReceiptViewSearchDto)e.Item.DataItem;
            if (miscReceipt == null) return;

            var rptMiscReceiptApplications = e.Item.FindControl("rptMiscReceiptApplications").ToRepeater();

            rptMiscReceiptApplications.DataSource = miscReceipt.MiscReceiptApplications;
            rptMiscReceiptApplications.DataBind();
        }
    }
}