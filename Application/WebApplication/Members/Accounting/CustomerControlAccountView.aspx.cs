﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Registry;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting
{
    public partial class CustomerControlAccountView : MemberPageBase, ICustomerControlAccountView
    {
        private const string ExportFlag = "E";

        private bool CloseEditPopUp
        {
            get { return ViewState["CloseEditPopUp"].ToBoolean(); }
            set { ViewState["CloseEditPopUp"] = value; }
        }

        public override ViewCode PageCode { get { return ViewCode.CustomerControlAccount; } }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.FinanceBlue; }
        }

        public static string PageAddress { get { return "~/Members/Accounting/CustomerControlAccountView.aspx"; } }

        public event EventHandler<ViewEventArgs<CustomerControlAccount>> Save;
        public event EventHandler<ViewEventArgs<CustomerControlAccount>> Delete;
        public event EventHandler<ViewEventArgs<CustomerControlAccount>> Lock;
        public event EventHandler<ViewEventArgs<CustomerControlAccount>> UnLock;
        public event EventHandler<ViewEventArgs<CustomerControlAccountSearchCriteria>> Search;
        public event EventHandler<ViewEventArgs<List<CustomerControlAccount>>> BatchImport;
        public event EventHandler<ViewEventArgs<string>> CustomerSearch;


        public void DisplaySearchResult(List<CustomerControlAccount> customerControlAccounts)
        {
            litRecordCount.Text = customerControlAccounts.BuildRecordCount();
            lvwRegistry.DataSource = customerControlAccounts;
            lvwRegistry.DataBind();

            if (hidFlag.Value == ExportFlag)
            {
                var lines = customerControlAccounts
                    .Select(a => new[]
					             	{
					             		a.AccountNumber,
										a.GenericCategory,
					             		a.Description,
					             		a.Street1,
					             		a.Street2,
					             		a.City,
					             		a.State,
					             		a.PostalCode,
                                        a.Country.Code
					             	}.TabJoin())
                    .ToList();
                lines.Insert(0,
                             new[]
				             	{
				             		"Account Number", "Customer Category", "Description", "Street 1", "Street 2",
				             		"City", "State", "Postal Code", "Country Code"
				             	}.TabJoin());

                hidFlag.Value = string.Empty;

                Response.Export(lines.ToArray().NewLineJoin(), "CustomerControlAccounts.txt");
            }
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            CloseEditPopUp = !messages.HasErrors() || messages.HasWarnings();

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Button = MessageButton.Ok;
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void DisplayCustomer(Customer customer)
        {
            txtCustomerNumber.Text = customer == null ? string.Empty : customer.CustomerNumber;
            txtCustomerName.Text = customer == null ? string.Empty : customer.Name;
            hidCustomerId.Value = customer == null ? default(long).ToString() : customer.Id.ToString();
        }


        public void FailedLock(Lock @lock)
        {
            btnSave.Enabled = false;
            litMessage.Text = @lock.BuildFailedLockMessage();
        }


        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems { Columns = GetCurrentRunParameters(true) };
        }

        private void DoSearchPreProcessingThenSearch()
        {
            DoSearch(new CustomerControlAccountSearchCriteria
            {
                Parameters = GetCurrentRunParameters(false),
                CustomerId = hidCustomerId.Value.ToLong()
            });
        }

        private void DoSearch(CustomerControlAccountSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<CustomerControlAccountSearchCriteria>(criteria));
        }


        private void GeneratePopup(string title)
        {
            litTitle.Text = title;

            pnlEdit.Visible = true;
            pnlDimScreen.Visible = pnlEdit.Visible;
        }

        private void CloseEditPopup()
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;

            if (UnLock != null)
                UnLock(this, new ViewEventArgs<CustomerControlAccount>(new CustomerControlAccount(hidCustomerControlAccountId.Value.ToLong(), false)));
        }

        private void LoadCustomerControlAccountForEdit(CustomerControlAccount account)
        {
            hidCustomerControlAccountId.Value = account.Id.ToString();

            txtAccountNumber.Text = account.AccountNumber;
            txtCustomerCategory.Text = account.GenericCategory;
            txtDescription.Text = account.Description;
            txtStreet1.Text = account.Street1;
            txtStreet2.Text = account.Street2;
            txtCity.Text = account.City;
            txtState.Text = account.State;
            txtPostalCode.Text = account.PostalCode;
			ddlEditCountry.SelectedValue = account.CountryId.GetString();
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            new CustomerControlAccountHandler(this).Initialize();

            btnSave.Enabled = Access.Modify;
            memberToolBar.ShowImport = Access.Modify;
            memberToolBar.ShowNew = Access.Modify;

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            aceCustomer.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.ActiveUser, ActiveUser.Id.GetString() } });

            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.CustomerControlAccountImportTemplate });
            if (!ActiveUser.TenantEmployee && !ActiveUser.UserShipAs.Any()) DisplayCustomer(ActiveUser.DefaultCustomer);


			var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);

            var columns = profile != null && profile.Columns != null
                            ? profile.Columns
                            : AccountingSearchFields.DefaultCustomerControlAccounts.Select(f => f.ToParameterColumn()).ToList();

            // Set country per original page logic, if there is a value for the country column per search defaults, leave it
            var cnames = columns
                .Where(i => i.ReportColumnName == AccountingSearchFields.NonAliasedCountryName.ToParameterColumn().ReportColumnName)
                .Where(i => !string.IsNullOrEmpty(i.DefaultValue))
                .ToList();
            foreach (var name in cnames)
                name.DefaultValue = ActiveUser.Country.Name;

            var ccodes = columns
                .Where(i => i.ReportColumnName == AccountingSearchFields.NonAliasedCountryCode.ToParameterColumn().ReportColumnName)
                .Where(i => string.IsNullOrEmpty(i.DefaultValue))
                .ToList();
            foreach (var code in ccodes)
                code.DefaultValue = ActiveUser.Country.Code;

            lstFilterParameters.DataSource = columns;
            lstFilterParameters.DataBind();

            if (Session[WebApplicationConstants.TransferCustomerId] != null)
            {
                DisplayCustomer(new Customer(Session[WebApplicationConstants.TransferCustomerId].ToLong()));
                Session[WebApplicationConstants.TransferCustomerId] = null;
            }
        }


        protected void OnNewClick(object sender, EventArgs e)
        {
            if (hidCustomerId.Value.ToLong() == default(long))
            {
                DisplayMessages(new[] { ValidationMessage.Error("Please select a customer before adding a new customer control account") });
                return;
            }

            GeneratePopup("Add Customer Control Account");
            LoadCustomerControlAccountForEdit(new CustomerControlAccount { CountryId = ActiveUser.Tenant.DefaultCountryId });
        }

        protected void OnImportClicked(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(hidCustomerId.Value))
            {
                fileUploader.Title = "CUSTOMER CONTROL ACCOUNT IMPORT";
                fileUploader.Instructions = @"**Please refer to user guide for import file format.";
                fileUploader.Visible = true;
            }
            else
            {
                messageBox.Button = MessageButton.Ok;
                messageBox.Icon = MessageIcon.Error;
                messageBox.Message = "Please Select a Customer to Batch Import for";

                messageBox.Visible = true;
            }
        }



        protected void OnSearchClicked(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch();
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            hidFlag.Value = ExportFlag;
            DoSearchPreProcessingThenSearch();
        }



        protected void OnOkayProcess(object sender, EventArgs e)
        {
            messageBox.Visible = false;

            if (CloseEditPopUp) CloseEditPopup();
        }




        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 9;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var countries = ProcessorVars.RegistryCache.Countries.Values.ToList();
            var countryCodes = countries.Select(c => c.Code).ToList();
            msgs.AddRange(chks.CodesAreValid(8, countryCodes, new Country().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var postalCodeSearch = new PostalCodeSearch();
            var postalCodes = new List<PostalCodeViewSearchDto>();
            var invalidPostalCodes = chks
                .Where(i =>
                {
                    var code = postalCodeSearch.FetchPostalCodeByCode(i.Line[7], countries.First(c => c.Code == i.Line[8]).Id);
                    postalCodes.Add(code);
                    return code.Id == default(long);
                })
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedCodeErrMsg, new PostalCode().EntityName(), i.Index))
                .ToList();
            if (invalidPostalCodes.Any())
            {
                invalidPostalCodes.Add(ValidationMessage.Error(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(invalidPostalCodes);
                return;
            }

            var customer = new Customer(hidCustomerId.Value.ToLong());
            var accounts = lines
                .Select(s => new CustomerControlAccount
                {
                    TenantId = ActiveUser.TenantId,
                    Customer = customer,
                    AccountNumber = s[0],
                    GenericCategory = s[1],
                    Description = s[2],
                    Street1 = s[3],
                    Street2 = s[4],
                    City = s[5],
                    State = s[6],
                    PostalCode = postalCodes.First(pc => pc.Code.ToLower() == s[7].ToLower()).Code,
                    CountryId = countries.First(c => c.Code == s[8]).Id
                })
                .ToList();

            if (BatchImport != null)
                BatchImport(this, new ViewEventArgs<List<CustomerControlAccount>>(accounts));
            fileUploader.Visible = false;
            DoSearchPreProcessingThenSearch();
        }




        protected void OnCustomerSearchClicked(object sender, ImageClickEventArgs e)
        {
            customerFinder.Visible = true;
        }

        protected void OnCustomerFinderItemSelected(object sender, ViewEventArgs<Customer> e)
        {

            DisplayCustomer(e.Argument);

            DisplaySearchResult(new List<CustomerControlAccount>());

            customerFinder.Visible = false;
        }

        protected void OnCustomerFinderSelectionCancelled(object sender, EventArgs e)
        {
            customerFinder.Visible = false;
        }

        protected void OnCustomerNumberEntered(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtCustomerNumber.Text))
            {
                hidCustomerId.Value = string.Empty;
                txtCustomerName.Text = string.Empty;
                return;
            }

            DisplaySearchResult(new List<CustomerControlAccount>());

            if (CustomerSearch != null)
                CustomerSearch(this, new ViewEventArgs<string>(txtCustomerNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));

        }




        protected void OnEditClicked(object sender, ImageClickEventArgs e)
        {
            GeneratePopup("Modify Customer Control Account");

            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("customerControlAccountId").ToCustomHiddenField();

            var account = new CustomerControlAccount(hidden.Value.ToLong(), false);

            LoadCustomerControlAccountForEdit(account);

            if (Lock != null)
                Lock(this, new ViewEventArgs<CustomerControlAccount>(account));
        }

        protected void OnDeleteClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;
            var hidden = imageButton.Parent.FindControl("customerControlAccountId").ToCustomHiddenField();

            if (Delete != null)
                Delete(this, new ViewEventArgs<CustomerControlAccount>(new CustomerControlAccount(hidden.Value.ToLong(), true)));

            DoSearchPreProcessingThenSearch();
        }



        protected void OnCloseClicked(object sender, EventArgs e)
        {
            CloseEditPopup();
        }

        protected void OnSaveClick(object sender, EventArgs e)
        {
            var accountId = hidCustomerControlAccountId.Value.ToLong();

            var account = new CustomerControlAccount(accountId, accountId != default(long))
            {
                Customer = new Customer(hidCustomerId.Value.ToLong()),
                AccountNumber = txtAccountNumber.Text,
                GenericCategory = txtCustomerCategory.Text,
                Description = txtDescription.Text,
                Street1 = txtStreet1.Text,
                Street2 = txtStreet2.Text,
                City = txtCity.Text,
                State = txtState.Text,
                PostalCode = txtPostalCode.Text,
                CountryId = ddlEditCountry.SelectedValue.ToLong(),
            };

            if (account.IsNew) account.TenantId = ActiveUser.TenantId;

            if (Save != null)
                Save(this, new ViewEventArgs<CustomerControlAccount>(account));

            DoSearchPreProcessingThenSearch();
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = AccountingSearchFields.CustomerControlAccounts.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(AccountingSearchFields.CustomerControlAccounts);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;

        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();
        }
    }
}
