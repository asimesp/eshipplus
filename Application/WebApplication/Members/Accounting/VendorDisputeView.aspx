﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="VendorDisputeView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.VendorDisputeView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Operations" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="false" ShowUnlock="False"
        ShowEdit="true" ShowDelete="true" ShowSave="true" ShowFind="true" ShowMore="false"
        OnFind="OnToolbarFindClicked" OnSave="OnToolbarSaveClicked" OnDelete="OnToolbarDeleteClicked" OnUnlock="OnToolbarUnlockClicked"
        OnEdit="OnToolbarEditClicked" OnCommand="OnToolbarCommand" />
</asp:Content>

<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
            <h3>Vendor Dispute</h3>
            <div class="shipmentAlertSection">
                <asp:Panel ID="pnlResolvedAlert" runat="server" CssClass="flag-shipment" Visible="false">
                    Resolved
                </asp:Panel>
            </div>
            <div class="lockMessage">
                <asp:Literal ID="litMessage" runat="server" />
            </div>
        </div>

        <hr class="dark mb5" />

        <div class="errorMsgLit">
            <asp:UpdatePanel runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <asp:Literal runat="server" ID="litErrorMessages" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <eShip:CustomHiddenField ID="hidVendorDisputeId" runat="server" />
        <eShip:VendorDisputeFinderControl runat="server" ID="vendorDisputeFinder" Visible="false" OnItemSelected="OnVendorDisputeFinderItemSelected"
            OnSelectionCancel="OnVendorDisputeFinderItemCancelled" OpenForEditEnabled="True" />

        <ajax:TabContainer ID="tabJob" runat="server" CssClass="ajaxCustom">
            <ajax:TabPanel runat="server" ID="tabDetails" HeaderText="Details">
                <ContentTemplate>
                    <asp:Panel ID="pnlDetails" runat="server">
                        <div class="rowgroup">
                            <div class="row">
                                <div class="row mb10">
                                    <div class="rowgroup">
                                        <div class="col_1_3 bbox vlinedarkright">
                                            <h5>General Details</h5>
                                            <div class="row">
                                                <div class="fieldgroup">
                                                    <label class="wlabel">Dispute Reference Number</label>
                                                    <eShip:CustomTextBox runat="server" ID="txtDisputeReferenceNumber" CssClass="w160 disabled" MaxLength="50" Type="NotSet" ReadOnly="true" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="fieldgroup">
                                                    <label class="wlabel w130">Created Date</label>
                                                    <eShip:CustomTextBox runat="server" ID="txtDateCreated" CssClass="w100" Type="Date" placeholder="99/99/9999" ReadOnly="true" Enabled="false" />
                                                </div>
                                                <div class="fieldgroup">
                                                    <label class="wlabel">Resolution Date</label>
                                                    <eShip:CustomTextBox runat="server" ID="txtResolutionDate" CssClass="w100" Type="Date" placeholder="99/99/9999" ReadOnly="true" Enabled="false" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="fieldgroup">
                                                    <label class="wlabel">Vendor</label>
                                                    <eShip:CustomHiddenField runat="server" ID="hidVendorId" Value="" />
                                                    <asp:Panel runat="server">
                                                        <eShip:CustomTextBox runat="server" ID="txtVendorNumber" CssClass="w90" AutoPostBack="True" Type="NotSet" ReadOnly="true" />
                                                        <asp:RequiredFieldValidator runat="server" ControlToValidate="txtVendorNumber"
                                                            ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                                                        <eShip:CustomTextBox runat="server" ID="txtVendorName" CssClass="w230 disabled" ReadOnly="True" Type="NotSet" />
                                                    </asp:Panel>
                                                </div>
                                            </div>
											   <div class="row">
                                                <div class="fieldgroup">
                                                    <label class="wlabel">
                                                        Dispute comment
                                                        <eShip:TextAreaMaxLengthExtender runat="server" ID="TextAreaMaxLengthExtender1" TargetControlId="txtResolutionComments" MaxLength="1000" />
                                                    </label>
                                                    <eShip:CustomTextBox ID="txtDispuetComments" runat="server" Text='<%# Eval("DisputeComments") %>' TextMode="MultiLine" CssClass="w324p h20" />
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="fieldgroup">
                                                    <label class="wlabel w130">
                                                        Created by&nbsp;
                                                        <asp:Image runat="server" ID="imgContactInformation" ToolTip="Contact Information" ImageAlign="AbsMiddle" Width="20px" ImageUrl="~/images/icons2/eye-open.png" />
                                                    </label>
                                                    <eShip:CustomTextBox runat="server" ID="txtCreatedBy" CssClass="w240 disabled" MaxLength="50" Type="NotSet" ReadOnly="true" />
                                                </div>
                                            </div>
                                            <script type="text/javascript">
                                                $(document).ready(function () {
                                                    $(jsHelper.AddHashTag('<%= imgContactInformation.ClientID %>')).on('mouseover mouseenter', function () {
                                                        $(jsHelper.AddHashTag('contactInformationDiv')).slideDown();
                                                    });
                                                    $(jsHelper.AddHashTag('<%= imgCloseContactInformation.ClientID %>')).click(function () {
                                                        $(jsHelper.AddHashTag('contactInformationDiv')).slideUp();
                                                    });
                                                    $(jsHelper.AddHashTag('<%= imgContactResolvedInformation.ClientID %>')).on('mouseover mouseenter', function () {
                                                        $(jsHelper.AddHashTag('contactResolvedInformationDiv')).slideDown();
                                                    });
                                                    $(jsHelper.AddHashTag('<%= imgCloseContactResolvedInformation.ClientID %>')).click(function () {
                                                        $(jsHelper.AddHashTag('contactResolvedInformationDiv')).slideUp();
                                                    });
                                                });
                                            </script>
                                            <div class="row mt20" id="contactInformationDiv" style="display: none;">
                                                <div class="wAuto mr20 shadow">
                                                    <div class="popheader">
                                                        <h4>Contact Information&nbsp;
                                                            <asp:Image ID="imgCloseContactInformation" ImageUrl="~/images/icons2/close.png" CssClass="close" runat="server" />
                                                        </h4>
                                                    </div>
                                                    <table class="contain">
                                                        <tr>
                                                            <td class="text-right p_m0" style="width: 20%">
                                                                <label class="upper blue">Phone: </label>
                                                            </td>
                                                            <td class="p_m0">
                                                                <label>
                                                                    <a id="lnkUserPhone" runat="server" />
                                                                </label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right p_m0">
                                                                <label class="upper blue">Mobile: </label>
                                                            </td>
                                                            <td class="p_m0">
                                                                <label>
                                                                    <a id="lnkUserMobile" runat="server" />
                                                                </label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right p_m0">
                                                                <label class="upper blue">Fax: </label>
                                                            </td>
                                                            <td class="p_m0">
                                                                <label>
                                                                    <a id="lnkUserFax" runat="server" />
                                                                </label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right p_m0">
                                                                <label class="upper blue">Email: </label>
                                                            </td>
                                                            <td class="p_m0">
                                                                <label>
                                                                    <a id="lnkUserEmail" runat="server" />
                                                                </label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="fieldgroup">
                                                    <label class="wlabel w130">
                                                        Resolved by&nbsp;
                                                        <asp:Image runat="server" ID="imgContactResolvedInformation" ToolTip="Contact Information" ImageAlign="AbsMiddle" Width="20px" ImageUrl="~/images/icons2/eye-open.png" />
                                                    </label>
                                                    <eShip:CustomTextBox runat="server" ID="txtResolvedBy" CssClass="w240 disabled" MaxLength="50" Type="NotSet" ReadOnly="true" />
                                                </div>
                                            </div>
                                            <div class="row mt20" id="contactResolvedInformationDiv" style="display: none;">
                                                <div class="wAuto mr20 shadow">
                                                    <div class="popheader">
                                                        <h4>Contact Information&nbsp;
                                                            <asp:Image ID="imgCloseContactResolvedInformation" ImageUrl="~/images/icons2/close.png" CssClass="close" runat="server" />
                                                        </h4>
                                                    </div>
                                                    <table class="contain">
                                                        <tr>
                                                            <td class="text-right p_m0" style="width: 20%">
                                                                <label class="upper blue">Phone: </label>
                                                            </td>
                                                            <td class="p_m0">
                                                                <label>
                                                                    <a id="lnkResolvedUserPhone" runat="server" />
                                                                </label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right p_m0">
                                                                <label class="upper blue">Mobile: </label>
                                                            </td>
                                                            <td class="p_m0">
                                                                <label>
                                                                    <a id="lnkResolvedUserMobile" runat="server" />
                                                                </label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right p_m0">
                                                                <label class="upper blue">Fax: </label>
                                                            </td>
                                                            <td class="p_m0">
                                                                <label>
                                                                    <a id="lnkResolvedUserFax" runat="server" />
                                                                </label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-right p_m0">
                                                                <label class="upper blue">Email: </label>
                                                            </td>
                                                            <td class="p_m0">
                                                                <label>
                                                                    <a id="lnkResolvedUserEmail" runat="server" />
                                                                </label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col_2_3 bbox pl20 pt16">
                                            <div class="row">
                                                <div class="fieldgroup row mr20 fieldgroup">
                                                    <label class="wlabel blue">
                                                        Resolution comments
                                                        <eShip:TextAreaMaxLengthExtender runat="server" ID="tfheResolutionComments" TargetControlId="txtResolutionComments" MaxLength="1000" />
                                                    </label>
                                                    <eShip:CustomTextBox ID="txtResolutionComments" runat="server" Text='<%# Eval("ResolutionComments") %>' TextMode="MultiLine" CssClass="w100p h275" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
            <ajax:TabPanel runat="server" ID="tabDisputeComponents" HeaderText="Dispute Components">
                <ContentTemplate>
                    <asp:Panel ID="pnlDisputeComponents" runat="server">
                        <div class="rowgroup">
                            <div class="row">

                                <table class="stripe">
                                    <tr>
                                        <th style="width: 20%;">Component Type</th>
                                        <th style="width: 20%;">Component Number</th>
                                        <th style="width: 20%;">Charge Code</th>
                                        <th style="width: 20%;">Original Charge Amount</th>
                                        <th style="width: 20%;">Revised Charge Amount</th>
                                    </tr>
                                    <asp:ListView ID="lstDisputeComponents" runat="server" ItemPlaceholderID="itemPlaceHolder">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <eShip:CustomHiddenField ID="hidDisputeComponentId" runat="server" Value='<%# Eval("Id") %>' />
                                                    <asp:Literal ID="litComponentType" runat="server" Text='<%# Eval("ChargeLineIdType") %>' />
                                                </td>
                                                <td>
                                                    <asp:Literal ID="litComponentNumber" runat="server" Text='<%# Eval("ComponentNumber") %>' />
                                                    <%# Eval("ChargeLineIdType").ToString() == "Shipment" && ActiveUser.HasAccessTo(ViewCode.Shipment)
                                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Shipment Record'><img src='{3}' alt='' width='16' align='absmiddle'/></a>",
                                                                ResolveUrl(ShipmentView.PageAddress),
                                                                WebApplicationConstants.TransferNumber,
                                                                Eval("ComponentNumber").GetString(),
                                                                ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                : string.Empty %>
                                                    <%# Eval("ChargeLineIdType").ToString() == "ServiceTicket" && ActiveUser.HasAccessTo(ViewCode.ServiceTicket)
                                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Service Ticket Record'><img src='{3}' alt='' width='16' align='absmiddle'/></a>",
                                                                ResolveUrl(ServiceTicketView.PageAddress),
                                                                WebApplicationConstants.TransferNumber,
                                                                Eval("ComponentNumber").GetString(),
                                                                ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                : string.Empty %>
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox runat="server" ID="txtChargeCode" Text='<%# Eval("ChargeCode.Code") %>' CssClass="w160 disabled" MaxLength="50" Type="NotSet" ReadOnly="true" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox runat="server" ID="txtOriginalChargeAmount" Text='<%# Eval("OriginalChargeAmount") %>' CssClass="w90 disabled" MaxLength="50" Type="NotSet" ReadOnly="false" />
                                                </td>
                                                <td>
                                                    <eShip:CustomTextBox runat="server" ID="txtRevisedChargeAmount" Text='<%# Eval("RevisedChargeAmount") %>' CssClass="w90" MaxLength="50" Type="NotSet" ReadOnly="false" />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </table>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajax:TabPanel>
			<ajax:TabPanel runat="server" ID="tabAuditLog" HeaderText="Audit Log">
                <ContentTemplate>
                    <eShip:AuditLogControl ID="auditLogs" runat="server" />
                </ContentTemplate>
			 </ajax:TabPanel>
        </ajax:TabContainer>

        <eShip:MessageBoxControl runat="server" ID="messageBox" HandleButtonsClientSide="Ok" Visible="false"
            Button="Ok" Icon="Information" OkayClientScript="ClearHidFlag();" />

        <asp:UpdatePanel runat="server" ID="upHiddens" UpdateMode="Always">
            <ContentTemplate>
                <eShip:CustomHiddenField runat="server" ID="hidFlag" />
                <eShip:CustomHiddenField runat="server" ID="hidEditStatus" />
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
</asp:Content>
