﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="BatchPaymentApplicationView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.BatchPaymentApplicationView" EnableEventValidation="false" %>

<%@ Register TagPrefix="eShip" TagName="MainToolBar" Src="~/Members/Controls/MainToolbarControl.ascx" %>

<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Operations" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>
<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowSave="True" OnSave="OnToolbarSave" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
                Batch Payment Application<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>
        <hr class="dark mb10" />
        <asp:Panel runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="MiscReceipts"
                            OnProcessingError="OnSearchProfilesProcessingError" OnSearchProfileSelected="OnSearchProfilesProfileSelected" />
                    </div>
                </div>
            </div>
            <hr class="dark mb10" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                        OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>
            <hr class="fat" />
            <div class="row mb10">
                <div class="fieldgroup">
                    <asp:Button ID="btnSearch" CssClass="left" runat="server" Text="SEARCH" OnClick="OnSearchClicked" CausesValidation="False" />
                </div>
                <div class="fieldgroup right">
                    <label>Sort By:</label>
                    <asp:DropDownList runat="server" ID="ddlSortBy" DataTextField="DisplayName" DataValueField="Name"
                        OnSelectedIndexChanged="OnSortBySelectedIndexChanged" AutoPostBack="True" CssClass="mr10" />
                    <asp:RadioButton runat="server" ID="rbAsc" Text="ASC" GroupName="rbSortBy" AutoPostBack="True" ToolTip="Ascending"
                        OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                    <asp:RadioButton runat="server" ID="rbDesc" Text="DESC" GroupName="rbSortBy" Checked="True" ToolTip="Descending" AutoPostBack="True"
                        OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                </div>
            </div>
        </asp:Panel>
        <script type="text/javascript">

            function SetCheckAllMiscReceipts(control) {
                $("#miscReceiptTable input:checkbox[id*='chkSelected']").each(function () {
                    this.checked = control.checked;
                    SetAltUniformCheckBoxClickedStatus(this);
                });

                // necessary to keep frozen header and header checkbox states the same
                $('[id*=' + control.id + ']').each(function () {
                    this.checked = control.checked;
                    SetAltUniformCheckBoxClickedStatus(this);
                });
            }

        </script>
        <asp:UpdatePanel runat="server" ID="upDataUpdate">
            <ContentTemplate>
                <eShip:UpdatePanelContinuousScrollExtender ID="upcseDataUpdate" runat="server" BindableControlId="rptMiscReceipts"
                    UpdatePanelToExtendId="upDataUpdate" />
                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheMiscReceiptDashboardTable" TableId="miscReceiptTable" HeaderZIndex="2" />
                <div class="rowgroup">
                    <table id="miscReceiptTable" class="line2 pl2">
                        <tr>
                            <th style="width: 5%;">
                                <eShip:AltUniformCheckBox ID="chkSelectAllRecords" runat="server"
                                    OnClientSideClicked=" javascript:SetCheckAllMiscReceipts(this); " />
                            </th>
                            <th style="width: 15%;">
                                <asp:LinkButton runat="server" ID="lbtnSortShipmentNumber" CssClass="link_nounderline blue" CausesValidation="False"
                                    OnCommand="OnSortData" Text='<abbr title="Shipment Number">Shipment #</abbr>' />
                            </th>
                            <th style="width: 15%;">
                                <asp:LinkButton runat="server" ID="lbtnSortLoadOrderNumber" CssClass="link_nounderline blue" CausesValidation="False"
                                    OnCommand="OnSortData" Text='<abbr title="Load Order Number">Load Order #</abbr>' />
                            </th>
                            <th style="width: 25%;">
                                <asp:LinkButton runat="server" ID="lbtnSortGatewayTransactionId" CssClass="link_nounderline blue" CausesValidation="False"
                                    OnCommand="OnSortData" Text="Gateway Transaction Id" />
                            </th>
                            <th style="width: 20%;">
                                <asp:LinkButton runat="server" ID="lbtnSortAmountPaid" CssClass="link_nounderline blue" CausesValidation="False"
                                    OnCommand="OnSortData" Text="Amount Paid" />
                            </th>
                            <th style="width: 20%;">
                                <asp:LinkButton runat="server" ID="lbtnAmountLeftToBeApplied" CssClass="link_nounderline blue" CausesValidation="False"
                                    OnCommand="OnSortData" Text="Amount Left To Be Applied" />
                            </th>
                        </tr>
                        <asp:Repeater runat="server" ID="rptMiscReceipts" OnItemDataBound="OnMiscReceiptsItemDataBound">
                            <ItemTemplate>
                                <tr>
                                    <td rowspan="2" class="top">
                                        <eShip:AltUniformCheckBox ID="chkSelected" runat="server"
                                            OnClientSideClicked=" SetCheckAllCheckBoxUnChecked(this, 'miscReceiptTable', 'chkSelectAllRecords'); " />
                                    </td>
                                    <td>
                                        <eShip:CustomHiddenField runat="server" ID="hidMiscReceiptId" Value='<%# Eval("Id") %>' />

                                        <%# Eval("ShipmentNumber") %>
                                        <%# ActiveUser.HasAccessTo(ViewCode.Shipment) && Eval("ShipmentId").ToLong() != default(long)
                                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Shipment Record'><img src='{3}' alt='' width='16' align='absmiddle'/></a>",
                                                                ResolveUrl(ShipmentView.PageAddress),
                                                                WebApplicationConstants.TransferNumber,
                                                                Eval("ShipmentNumber").GetString(),
                                                                ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                : string.Empty %>
                                    </td>
                                    <td class="top">
                                        <%# Eval("LoadOrderNumber") %>
                                        <%# ActiveUser.HasAccessTo(ViewCode.LoadOrder) && Eval("LoadOrderId").ToLong() != default(long)
                                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Load Order Record'><img src='{3}' alt='' width='16' align='absmiddle'/></a>",
                                                                ResolveUrl(LoadOrderView.PageAddress),
                                                                WebApplicationConstants.TransferNumber,
                                                                Eval("LoadOrderNumber").GetString(),
                                                                ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                : string.Empty %>
                                    </td>
                                    <td class="top">
                                        <%# Eval("GatewayTransactionId") %>
                                    </td>
                                    <td class="top">
                                        <%# Eval("AmountPaid").ToDecimal().ToString("c2") %>
                                    </td>
                                    <td class="top">
                                        <%# Eval("AppliableAmount").ToDecimal().ToString("c2") %>
                                    </td>
                                </tr>
                                <tr class="hidden">
                                    <td>&nbsp;</td>
                                </tr>
                                <tr class="f9">
                                    <td colspan="6" class="top forceLeftBorder">
                                        <asp:Repeater runat="server" ID="rptApplicableInvoices">
                                            <HeaderTemplate>
                                                <div class="rowgroup mb5">
                                                    <div class="row">
                                                        <h6 class="mt0 mb0">Applicable Invoices</h6>
                                                        <table class="contain pl2">
                                                            <tr class="bbb1">
                                                                <th style="width: 5%;" class="no-top-border">Apply
                                                                </th>
                                                                <th style="width: 25%;" class="no-top-border no-left-border">Invoice Number
                                                                </th>
                                                                <th style="width: 25%;" class="no-top-border no-left-border">Amount Paid
                                                                </th>
                                                                <th style="width: 25%;" class="no-top-border no-left-border">Total Amount Due
                                                                </th>
                                                                <th style="width: 20%;" class="no-top-border no-left-border">Amount To Apply
                                                                </th>
                                                            </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr class="no-bottom-border">
                                                    <td>
                                                        <eShip:AltUniformCheckBox runat="server" ID="chkInvoiceSelected" />
                                                    </td>
                                                    <td>
                                                        <eShip:CustomHiddenField runat="server" ID="hidInvoiceId" Value='<%# Eval("Id") %>' />

                                                        <%# ActiveUser.HasAccessTo(ViewCode.Invoice)
                                                                ? string.Format("{0} <a href='{1}?{2}={3}' class='blue' target='_blank' title='Go To Invoice Record'><img src='{4}' alt='' width='16' align='absmiddle'/></a>",
                                                                                Eval("InvoiceNumber"),
                                                                                ResolveUrl(InvoiceView.PageAddress),
                                                                                WebApplicationConstants.TransferNumber,
                                                                                Eval("InvoiceNumber").GetString(),
                                                                                ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                                : Eval("InvoiceNumber") %>
                                                    </td>
                                                    <td>
                                                        <%# Eval("PaidAmount").ToDecimal().ToString("c2") %>
                                                    </td>
                                                    <td>
                                                        <%# Eval("AmountDue").ToDecimal().ToString("c2") %>
                                                    </td>
                                                    <td>
                                                        <eShip:CustomTextBox runat="server" ID="txtAmountToApply" Width="128" Type="FloatingPointNumbers" />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </div>
                                            </div>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lbtnSortShipmentNumber" />
                <asp:PostBackTrigger ControlID="lbtnSortLoadOrderNumber" />
                <asp:PostBackTrigger ControlID="lbtnSortAmountPaid" />
                <asp:PostBackTrigger ControlID="lbtnSortGatewayTransactionId" />
                <asp:PostBackTrigger ControlID="lbtnAmountLeftToBeApplied" />
                <asp:PostBackTrigger ControlID="rptMiscReceipts" />
            </Triggers>
        </asp:UpdatePanel>
        <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
        <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
        <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
    </div>
</asp:Content>
