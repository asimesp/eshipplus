﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting
{
    public partial class CustCSRView : MemberPageBase
    {
        public static string PageAddress { get { return "~/Members/Accounting/CustCSRView.aspx"; } }

        public override ViewCode PageCode
        {
            get { return ViewCode.Customer; }
        }

        public override string SetPageIconImage
        {
            set { imgPageImageLogo.ImageUrl = IconLinks.FinanceBlue; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var key = Request.QueryString[WebApplicationConstants.CsrCustId].ToLong();

            // permission check
            if (!ActiveUser.HasAccessTo(ViewCode.Customer) && ActiveUser.TenantEmployee)
            {
                pnlListCsr.Visible = false;
                pnlNoAccess.Visible = true;
                pnlNoCsr.Visible = false;
                return;
            }

            var customer = key == default(long) ? new Customer() : new Customer(key);
            litHeader.Text = string.Format("- {0} - {1}", customer.CustomerNumber, customer.Name);
            var reps = customer.KeyLoaded ? customer.RetrieveServiceRepresentatives() : new List<User>();
            if (!reps.Any())
            {
                pnlListCsr.Visible = false;
                pnlNoCsr.Visible = true;
                pnlNoAccess.Visible = false;
            }
            else
            {
                pnlListCsr.Visible = true;
                lstCSR.DataSource = reps;
                lstCSR.DataBind();

                pnlNoCsr.Visible = false;
                pnlNoAccess.Visible = false;
            }
        }
    }
}
