﻿using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Notifications;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting
{
    public partial class VendorChargeConfirmation : MemberPageBase, IChargeView
    {
        private SearchField SortByField
        {
            get { return AccountingSearchFields.VendorChargeConfirmationSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue); }
        }

        public override ViewCode PageCode { get { return ViewCode.VendorChargesConfirmation; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.FinanceBlue; } }

        public static string PageAddress { get { return "~/Members/Accounting/VendorChargeConfirmationView.aspx"; } }
        
        public event EventHandler<ViewEventArgs<ChargeViewSearchCriteria>> Search;
        public event EventHandler<ViewEventArgs<List<ChargeViewSearchDto>>> ConfirmCharges;
        public event EventHandler<ViewEventArgs<List<ChargeViewSearchDto>>> DisputeCharges;


        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplaySearchResult(List<ChargeItemDto> charges)
        {
            litRecordCount.Text = charges.BuildRecordCount();
            upcseDataUpdate.DataSource = charges;
            upcseDataUpdate.DataBind();
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }


        public void OnConfirmChargesComplete()
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;

            DoSearchPreProcessingThenSearch(SortByField);
        }


		public void OnDisputeChargesComplete()
		{
			pnlDisputeCharges.Visible = false;
			pnlDimScreen.Visible = false;
		
			DoSearchPreProcessingThenSearch(SortByField);
		}

        public void SendNotification(List<ChargeViewSearchDto> charges, ChargeNotificationType type)
        {
            switch (type)
            {
                case ChargeNotificationType.Confirm:
                    this.NotifyVendorChargeConfirmation(charges);
                    break;

                case ChargeNotificationType.Dispute:
                    this.NotifyVendorChargeDispute(charges);
                    break;
            }
        }


        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems
            {
                Columns = GetCurrentRunParameters(true),
                SortAscending = rbAsc.Checked,
                SortBy = SortByField,
            };
        }

        private void DoSearchPreProcessingThenSearch(SearchField field)
        {
            DoSearch(new ChargeViewSearchCriteria
            {
                Parameters = GetCurrentRunParameters(false),
                SortBy = field,
                SortAscending = rbAsc.Checked
            });
        }

        private void DoSearch(ChargeViewSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<ChargeViewSearchCriteria>(criteria));
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        private List<ChargeViewSearchDto> GetSelectedCharges()
        {
            var cs = new ChargeSearch();
            var charges = new List<ChargeViewSearchDto>();

            foreach (var i in rptEntities.Items.Cast<RepeaterItem>().Where(i => i.FindControl("chkSelected").ToAltUniformCheckBox().Checked))
            {
                var chargeType = i.FindControl("hidChargeType").ToCustomHiddenField().Value.ToEnum<ChargeType>();

                var rptChargesInner = i.FindControl("rptCharges").ToRepeater();

                var invoiceCharges = rptChargesInner
                    .Items
                    .Cast<RepeaterItem>()
                    .Where(n => n.FindControl("chkInvoiceSelected").ToAltUniformCheckBox().Checked)
                    .Select(n => cs.FetchChargesByTypeAndId(
                        n.FindControl("hidChargeId").ToCustomHiddenField().Value,
                        n.FindControl("hidChargeNumber").ToCustomHiddenField().Value,
                        chargeType))
                    .ToList();

                if (!invoiceCharges.Any()) continue;
                charges.AddRange(invoiceCharges);
            }

            return charges;
        }

        private void UploadDocument(FileUpload fileUpload, string physicalPath)
        {
            // ensure directory is present
            if (!Directory.Exists(physicalPath)) Directory.CreateDirectory(physicalPath);

            //ensure unique filename
            var fi = new FileInfo(Server.MakeUniqueFilename(Path.Combine(physicalPath, fileUpload.FileName), true));

            // save file
            fileUpload.WriteToFile(physicalPath, fi.Name, physicalPath);
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new VendorChargeConfirmationHandler(this).Initialize();

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            // set sort fields
            ddlSortBy.DataSource = AccountingSearchFields.VendorChargeConfirmationSortFields.OrderBy(s => s.DisplayName);
            ddlSortBy.DataBind();

            lbtnSortType.CommandName = AccountingSearchFields.ChargeType.Name;
            lbtnSortNumber.CommandName = AccountingSearchFields.ChargeNumber.Name;
            lbtnUnitBuyTotal.CommandName = AccountingSearchFields.ChargeUnitBuy.Name;

            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);
			//get criteria from Session
			var searchDefault = Session[WebApplicationConstants.ChargeViewSearchCriteria] as SearchDefaultsItem;
			var storedCriteria = searchDefault == null
									? null
									: new ChargeViewSearchCriteria
									{
										Parameters = searchDefault.Columns,
										SortAscending = searchDefault.SortAscending,
										SortBy = searchDefault.SortBy
									};

			var defaultCriteria = new ChargeViewSearchCriteria
            {
                Parameters = profile != null
                                 ? profile.Columns
                                 : AccountingSearchFields.DefaultVendorChargeConfirmation.Select(f => f.ToParameterColumn()).ToList(),
                SortAscending = profile == null || profile.SortAscending,
                SortBy = profile == null ? null : profile.SortBy
            };
			var criteria = storedCriteria ?? defaultCriteria;
            lstFilterParameters.DataSource = criteria.Parameters;
            lstFilterParameters.DataBind();

            rbAsc.Checked = criteria.SortAscending;
            rbDesc.Checked = !criteria.SortAscending;

            if (criteria.SortBy != null && ddlSortBy.ContainsValue(criteria.SortBy.Name))
                ddlSortBy.SelectedValue = criteria.SortBy.Name;

			if (storedCriteria != null) DoSearch(criteria);

			Session[WebApplicationConstants.ChargeViewSearchCriteria] = null;
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            upcseDataUpdate.ResetLoadCount();
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = AccountingSearchFields.Charges.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

        }

        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(AccountingSearchFields.Charges);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;

        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();

            rbAsc.Checked = e.Argument.SortAscending;
            rbDesc.Checked = !e.Argument.SortAscending;

            if (e.Argument.SortBy != null && ddlSortBy.ContainsValue(e.Argument.SortBy.Name))
                ddlSortBy.SelectedValue = e.Argument.SortBy.Name;
        }


        protected void OnSortBySelectedIndexChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnSortOrderCheckChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnSortData(object sender, CommandEventArgs e)
        {
            var field = AccountingSearchFields.VendorChargeConfirmationSortFields.FirstOrDefault(c => c.Name == e.CommandName);
            if (field == null) return;
            if (ddlSortBy.ContainsValue(field.Name))
            {
                if (ddlSortBy.SelectedValue == field.Name && rbAsc.Checked)
                {
                    rbAsc.Checked = false;
                    rbDesc.Checked = true;
                }
                else if (ddlSortBy.SelectedValue == field.Name && rbDesc.Checked)
                {
                    rbAsc.Checked = true;
                    rbDesc.Checked = false;
                }

                ddlSortBy.SelectedValue = field.Name;
            }
            DoSearchPreProcessingThenSearch(field);
        }

        
        protected void OnChargesItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item == null) return;

            var chargeDto = (ChargeItemDto)e.Item.DataItem;

            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;

            var rptCharges = (Repeater)e.Item.FindControl("rptCharges");
            rptCharges.DataSource = chargeDto.Charges;
            rptCharges.DataBind();
        }


        protected void OnConfirmClicked(object sender, EventArgs e)
        {
            var charges = GetSelectedCharges();

            if (charges.Count > 0)
            {
                var groupedChargeList = charges
                    .GroupBy(g => new { g.Number, g.ChargeType, g.VendorId })
                    .Select(grp => new { GroupID = grp.Key, Charges = grp.ToList() })
                    .ToList();

                rptGroupsChargesToApprove.DataSource = groupedChargeList;
                rptGroupsChargesToApprove.DataBind();

                pnlEdit.Visible = true;
                pnlDimScreen.Visible = true;
            }
            else
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Information(ProcessorVars.NoSelectedItemsMsg) });
            }
        }

        protected void OnDisputeClicked(object sender, EventArgs e)
        {
            var charges = GetSelectedCharges();

			if (charges.Count > 0)
			{
				var groupedChargeList = charges
					.GroupBy(g => new { g.Number, g.ChargeType, g.VendorId })
					.Select(grp => new { GroupID = grp.Key, Charges = grp.ToList() })
					.ToList();

				rptGroupsChargesToDispute.DataSource = groupedChargeList;
				rptGroupsChargesToDispute.DataBind();

				pnlDisputeCharges.Visible = true;
				pnlDimScreen.Visible = true;
			}
			else
			{
				DisplayMessages(new List<ValidationMessage> { ValidationMessage.Information(ProcessorVars.NoSelectedItemsMsg) });
			}

        }
	
        protected void OnApproveChargesItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item == null) return;

            var charges = ((e.Item.DataItem as dynamic).Charges as List<ChargeViewSearchDto>);

            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;

            var rptApproveCharges = (Repeater)e.Item.FindControl("rptApproveCharges");
            rptApproveCharges.DataSource = charges;
            rptApproveCharges.DataBind();
        }

		protected void OnDisputeChargesItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item == null) return;

			var charges = ((e.Item.DataItem as dynamic).Charges as List<ChargeViewSearchDto>);

			if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;

			var rptApproveCharges = (Repeater)e.Item.FindControl("rptDisputeCharges");
			rptApproveCharges.DataSource = charges;
			rptApproveCharges.DataBind();
		}

        protected void OnSaveClick(object sender, EventArgs e)
        {
            var cs = new ChargeSearch();
            var charges = new List<ChargeViewSearchDto>();
            var documentTag = new DocumentTagSearch().FetchDocumentTags(new List<ParameterColumn>(), ActiveUser.TenantId);
            var tagPod = documentTag.Single(s => s.Code == "POD");
            var tagInvoice = documentTag.Single(s => s.Code == "OTHER");

            var groupChargesRepeaterItems = rptGroupsChargesToApprove.Items.Cast<RepeaterItem>();
            if (groupChargesRepeaterItems.Any(i =>
                {
                    var fileUpload = i.FindControl("fupLocationPathPODDocument") as FileUpload;
                    return fileUpload != null && string.IsNullOrWhiteSpace(fileUpload.FileName);
                }))
            {
                DisplayMessages(new[] { ValidationMessage.Error("POD document is required.") });
                return;
            }

            foreach (var i in groupChargesRepeaterItems)
            {
                var documents = new List<Document>();
                var chargeType = i.FindControl("hidGroupChargeType").ToCustomHiddenField().Value.ToInt().ToEnum<ChargeType>();
                var parentId = i.FindControl("hidParentId").ToCustomHiddenField().Value;
                var rptChargesInner = i.FindControl("rptApproveCharges").ToRepeater();
                var documentNumber = i.FindControl("txtDocumentNumber").ToCustomTextBox().Text;
                var documentDate = i.FindControl("txtDocumentDate").ToCustomTextBox().Text.ToDateTime();

                var virtualPath = WebApplicationSettings.ShipmentFolder(ActiveUser.TenantId, parentId.ToLong());
                var physicalPath = Server.MapPath(virtualPath);

                var fupLocationPathInvoiceDocument = i.FindControl("fupLocationPathInvoiceDocument") as FileUpload;
                var fupLocationPathPodDocument = i.FindControl("fupLocationPathPODDocument") as FileUpload;


                if (fupLocationPathInvoiceDocument != null && !string.IsNullOrWhiteSpace(fupLocationPathInvoiceDocument.FileName))
                {
                    UploadDocument(fupLocationPathInvoiceDocument, physicalPath);

                    //SaveDocumentInfo
                    var document = new Document
                    {
                        DocumentTagId = tagInvoice.Id,
                        Name = fupLocationPathInvoiceDocument.FileName,
                        Description = string.Format("{0} {1}", documentNumber, tagInvoice.Code),
                        LocationPath = Path.Combine(virtualPath, fupLocationPathInvoiceDocument.FileName),
                        TenantId = ActiveUser.TenantId
                    };
                    documents.Add(document);
                }

                if (fupLocationPathPodDocument != null && !string.IsNullOrWhiteSpace(fupLocationPathPodDocument.FileName))
                {
                    UploadDocument(fupLocationPathPodDocument, physicalPath);

                    var document = new Document
                    {
                        DocumentTagId = tagPod.Id,
                        Name = fupLocationPathPodDocument.FileName,
                        Description = string.Format("{0} {1}", documentNumber, tagPod.Code),
                        LocationPath = Path.Combine(virtualPath, fupLocationPathPodDocument.FileName),
                        TenantId = ActiveUser.TenantId
                    };
                    documents.Add(document);
                }

                var invoiceCharges = rptChargesInner
                    .Items
                    .Cast<RepeaterItem>()
                    .Select(n => cs.FetchChargesByTypeAndId(
                        n.FindControl("hidChargeId").ToCustomHiddenField().Value,
                        n.FindControl("hidChargeNumber").ToCustomHiddenField().Value,
                        chargeType))
                    .ToList();

                invoiceCharges.ForEach(c =>
                {
                    c.DocumentNumber = documentNumber;
                    c.DocumentDate = documentDate;
                    c.Documents = documents;
                });


                if (!invoiceCharges.Any()) continue;
                charges.AddRange(invoiceCharges);
            }

            if (charges.Any())
            {
                if (ConfirmCharges != null)
                    ConfirmCharges(this, new ViewEventArgs<List<ChargeViewSearchDto>>(charges));
            }
            else
            {
                DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSelectedItemsMsg) });
            }

        }
        
        protected void OnCloseClicked(object sender, EventArgs e)
        {
            pnlEdit.Visible = false;
            pnlDimScreen.Visible = false;
        }

	    protected void OnDisputeSaveClick(object sender, EventArgs e)
	    {
			var cs = new ChargeSearch();
			var charges = new List<ChargeViewSearchDto>();
			
			var groupChargesRepeaterItems = rptGroupsChargesToDispute.Items.Cast<RepeaterItem>();

			foreach (var i in groupChargesRepeaterItems)
			{
				var chargeType = i.FindControl("hidGroupChargeType").ToCustomHiddenField().Value.ToInt().ToEnum<ChargeType>();
			    var rptChargesInner = i.FindControl("rptDisputeCharges").ToRepeater();
				var disputeComments = i.FindControl("txtDisputeComment").ToCustomTextBox().Text;
     		
				var invoiceCharges = rptChargesInner
					.Items
					.Cast<RepeaterItem>()
					.Select(n => cs.FetchChargesByTypeAndId(
						n.FindControl("hidChargeId").ToCustomHiddenField().Value,
						n.FindControl("hidChargeNumber").ToCustomHiddenField().Value,
						chargeType))
					.ToList();

				invoiceCharges.ForEach(c =>
				{
					c.DisputeComments = disputeComments;
					
				});


				if (!invoiceCharges.Any()) continue;
				charges.AddRange(invoiceCharges);
			}


			if (charges.Count > 0)
			{
				 if (DisputeCharges != null)
					DisputeCharges(this, new ViewEventArgs<List<ChargeViewSearchDto>>(charges));
			}
			else
			{
				DisplayMessages(new[] { ValidationMessage.Information(ProcessorVars.NoSelectedItemsMsg) });
			}
	    }

		protected void OnDisputeCloseClicked(object sender, EventArgs e)
		{
			pnlDisputeCharges.Visible = false;
			pnlDimScreen.Visible = false;
	    }

	    protected string GetLocationFileName(object relativePath)
        {
            return relativePath == null ? string.Empty : Server.GetFileName(relativePath.ToString());
        }

        protected void OnLocationPathClicked(object sender, EventArgs e)
        {
            var button = (LinkButton)sender;
            var path = button.Parent.FindControl("hidLocationPath").ToCustomHiddenField().Value;
            Response.Export(Server.ReadFromFile(path), button.Text);
        }
    }
}