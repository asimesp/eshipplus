﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting
{
    public partial class InvoicePaidAmountUpdateView : MemberPageBase, IInvoicePaidAmountUpdatesView
    {
        public static string PageAddress { get { return "~/Members/Accounting/InvoicePaidAmountUpdateView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.InvoicePaidAmountUpdate; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.FinanceBlue; } }

        public event EventHandler<ViewEventArgs<List<Invoice>>> Save;

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void DisplayUpdatedRecords(List<InvoicePaidAmountUpdateDto> updatedInvoices)
        {
            lstCompleteInvoices.DataSource = updatedInvoices;
            lstCompleteInvoices.DataBind();

            lstInvoices.DataSource = new List<Invoice>();
            lstInvoices.DataBind();

            memberToolBar.ShowSave = false;
        }



        protected void OnImportInvoices(object sender, EventArgs e)
        {
            fileUploader.Title = "INVOICE PAID AMOUNT UPDATE IMPORT";
            fileUploader.Instructions = @"**Please refer to user guide for import file format";
            fileUploader.Visible = true;
        }

        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {
            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 2;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            if (lines.Count > 1000)
            {
                var message = new[] { ValidationMessage.Error("Mass update is limited to 1000 invoices, please adjust your input file") };
                DisplayMessages(message);
            }

            var search = new InvoiceSearch();
            var inputToUpdate = lines
                .Select(s => new
                {
                    Id = search.FetchInvoiceIdByInvoiceNumber(s[0], ActiveUser.TenantId),
                    InvoiceNumber = s[0],
                    PaidAmount = s[1].ToDecimal(),
                })
                .ToList();

            lstInvoices.DataSource = inputToUpdate;
            lstInvoices.DataBind();

            lstCompleteInvoices.DataSource = new List<Invoice>();
            lstCompleteInvoices.DataBind();

            fileUploader.Visible = false;
            memberToolBar.ShowSave = true;
        }

        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            fileUploader.Visible = false;
        }



        protected void OnSaveInvoices(object sender, EventArgs e)
        {
            var rInvoices = new List<Invoice>();
            foreach (var i in lstInvoices.Items)
            {
                var id = i.FindControl("hidInvoiceId").ToCustomHiddenField().Value.ToLong();
                var wInvoice = new Invoice(id, id != default(long));
                wInvoice.LoadCollections();
                wInvoice.PaidAmount += i.FindControl("txtAmountPaid").ToTextBox().Text.ToDecimal();

                // NOTE: will come back up with error.  Number is for reference purposes.
                if (wInvoice.Id == default(long)) wInvoice.InvoiceNumber = i.FindControl("litInvoiceNumber").ToLiteral().Text;

                rInvoices.Add(wInvoice);
            }

            if (Save != null)
                Save(this, new ViewEventArgs<List<Invoice>>(rInvoices));


        }


        protected void Page_Load(object sender, EventArgs e)
        {
            var handler = new InvoicePaidAmountUpdateHandler(this);
            handler.Initialize();

            if (IsPostBack) return;
            memberToolBar.LoadTemplates(new List<string> { WebApplicationConstants.InvoicePaidAmountUpdatesImportTemplate });

        }
    }
}
