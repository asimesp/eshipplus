﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Core.Registry;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Core;
using LogisticsPlus.Eship.Processor.Dto.Operations;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Operations;
using LogisticsPlus.Eship.Processor.Searches.Registry;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Operations;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting
{
    public partial class VendorBillView : MemberPageBaseWithPageStore, IVendorBillView
    {
        private const string VendorBillDetailsHeader = "Vendor Bill Details";
        private const string VendorBillDocumentsHeader = "Documents";

        private const string ReturnArgs = "RETURN";
        private const string ReturnPostingArgs = "ReturnPosting";
        private const string PostArgs = "POST";
        private const string SaveFlag = "S";
        private const string PostFlag = "P";
        private const string SearchFlag = "SEARCH";
        private const string ApplyToDocumentFlag = "APPLY";
        private const string ManualExportArgs = "ManualExport";
        private const string ReverseExportArgs = "ReverseExport";
        private const string VendorBillImportArgs = "VBUA";
        private const string VendorBillDetailsImportArgs = "VBDUA";

        private const string InvoiceDetailReferenceTypeStoreKey = "IDRTSK";
        private const string VendorBillDetailStoreKey = "VBDSK";

        public static string PageAddress { get { return "~/Members/Accounting/VendorBillView.aspx"; } }

        public override ViewCode PageCode { get { return ViewCode.VendorBill; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.FinanceBlue; } }

        private List<ViewListItem> StoredInvoiceDetailReferenceTypes
        {
            get { return PageStore[InvoiceDetailReferenceTypeStoreKey] as List<ViewListItem> ?? new List<ViewListItem>(); }
            set { PageStore[InvoiceDetailReferenceTypeStoreKey] = value; }
        }

        private List<VendorBillDetail> StoredVendorBillDetails
        {
            get
            {
                if (!PageStore.ContainsKey(VendorBillDetailStoreKey)) PageStore.Add(VendorBillDetailStoreKey, new List<VendorBillDetail>());
                return PageStore[VendorBillDetailStoreKey] as List<VendorBillDetail> ?? new List<VendorBillDetail>();
            }
        }

        public Dictionary<int, string> VendorBillDetailReferenceType
        {
            set
            {
                StoredInvoiceDetailReferenceTypes = value.Select(t => new ViewListItem(t.Value, t.Key.ToString())).ToList();
            }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<VendorBill>> Save;
        public event EventHandler<ViewEventArgs<VendorBill>> Delete;
        public event EventHandler<ViewEventArgs<VendorBill>> Lock;
        public event EventHandler<ViewEventArgs<VendorBill>> UnLock;
        public event EventHandler<ViewEventArgs<VendorBill>> Search;
        public event EventHandler<ViewEventArgs<VendorBill>> LoadAuditLog;
        public event EventHandler<ViewEventArgs<string>> VendorSearch;
        public event EventHandler<ViewEventArgs<List<VendorBill>>> BatchSave;

        public void DisplayVendor(Vendor vendor)
        {
            txtVendorNumber.Text = vendor == null ? string.Empty : vendor.VendorNumber;
            txtVendorName.Text = vendor == null ? string.Empty : vendor.Name;
            hidVendorId.Value = vendor == null ? default(long).ToString() : vendor.Id.ToString();
            ddlVendorLocation.Enabled = vendor != null;

            btnAddServiceTicket.Visible = vendor != null;
            btnAddShipment.Visible = vendor != null;
            pnlVendorFiltering.Visible = vendor != null;

            var locations =
                vendor != null && vendor.Locations.Any()
                    ? vendor.Locations
                        .Select(l => new ViewListItem(string.Format("{0} - {1} {2}", l.LocationNumber, l.GetRemitToString(), l.FullAddress), l.Id.ToString()))
                        .ToList()
                    : new List<ViewListItem> { new ViewListItem(WebApplicationConstants.NotAvailable, default(long).GetString()) };

            ddlVendorLocation.DataSource = locations;
            ddlVendorLocation.DataBind();
            var id = vendor != null && vendor.Locations.Any(l => l.RemitToLocation && l.MainRemitToLocation)
                        ? vendor.Locations.First(l => l.RemitToLocation && l.MainRemitToLocation).Id
                        : default(long);
            if (ddlVendorLocation.ContainsValue(id.ToString())) ddlVendorLocation.SelectedValue = id.ToString();

            if (vendor == null) return;

            var customerLocationAddresses =
                    vendor.Locations
                    .Select(l => new
                    {
                        l.Id,
                        RemitTo = l.GetRemitToString(),
                        l.Street1,
                        l.Street2,
                        l.City,
                        l.State,
                        l.PostalCode,
                        CountryName = l.Country.Name
                    }).ToList();

            rptVendorLocations.DataSource = customerLocationAddresses;
            rptVendorLocations.DataBind();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            if (!messages.HasErrors() && !messages.HasWarnings() && (hidFlag.Value == SaveFlag || hidFlag.Value == PostFlag))
            {
                // unlock and return to read-only
                if (UnLock != null)
                    UnLock(this, new ViewEventArgs<VendorBill>(new VendorBill(hidVendorBillId.Value.ToLong(), false)));

                switch (hidFlag.Value)
                {
                    case SaveFlag:
                        SetEditStatus(false);
                        break;
                }
                memberToolBar.ShowUnlock = false;
            }

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;

            litErrorMessages.Text = messages.HasErrors()
                                        ? string.Join(WebApplicationConstants.HtmlBreak,
                                                      messages.Where(m => m.Type == ValidateMessageType.Error).OrderBy(m => m.Type).
                                                        Select(m => m.Message).ToArray())
                                        : string.Empty;
        }

        public void DisplayAuditLogs(IEnumerable<AuditLogsViewSearchDto> logs) { auditLogs.Load(logs); }

        public void FailedLock(Lock @lock)
        {
            memberToolBar.ShowNew = false;
            memberToolBar.ShowSave = false;
            memberToolBar.ShowDelete = false;
            memberToolBar.ShowUnlock = false;

            SetEditStatus(false);

            litMessage.Text = @lock.BuildFailedLockMessage();
        }

        public void Set(VendorBill bill)
        {
            LoadVendorBill(bill);
            SetEditStatus(!bill.IsNew && bill.ShouldEnable());
        }



        private List<ToolbarMoreAction> ToolbarMoreActions(bool enabled)
        {
            var vendorBillDashboard = new ToolbarMoreAction
            {
                CommandArgs = ReturnArgs,
                ImageUrl = IconLinks.Finance,
                Name = ViewCode.VendorBillDashboard.FormattedString(),
                IsLink = false,
                NavigationUrl = string.Empty
            };

            var post = new ToolbarMoreAction
            {
                CommandArgs = PostArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Finance,
                Name = "Post Vendor Bill",
                IsLink = false,
                NavigationUrl = string.Empty
            };

            var manualExport = new ToolbarMoreAction
            {
                CommandArgs = ManualExportArgs,
                ConfirmCommand = true,
                ImageUrl = IconLinks.Finance,
                IsLink = false,
                Name = "Set Exported",
                NavigationUrl = string.Empty
            };

            var reverseExport = new ToolbarMoreAction
            {
                CommandArgs = ReverseExportArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Finance,
                IsLink = false,
                Name = "Reverse Export",
                NavigationUrl = string.Empty
            };
            var billImport = new ToolbarMoreAction
            {
                CommandArgs = VendorBillImportArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Finance,
                IsLink = false,
                Name = "Import Vendor Bill(s)",
                NavigationUrl = string.Empty
            };
            var billDetailImport = new ToolbarMoreAction
            {
                CommandArgs = VendorBillDetailsImportArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.Finance,
                IsLink = false,
                Name = "Import Vendor Bill Detail(s)",
                NavigationUrl = string.Empty
            };

            var separator = new ToolbarMoreAction { IsSeperator = true };
            var notNew = hidVendorBillId.Value.ToLong() != default(long);
            var actions = new List<ToolbarMoreAction>();

            if (Access.Modify && notNew && !chkPosted.Checked) actions.Add(post);

            if (Access.Modify && chkPosted.Checked)
            {
                if (actions.Any()) actions.Add(separator);
                actions.Add(chkExported.Checked ? reverseExport : manualExport);
            }

            if (Access.Modify && !Access.Deny)
            {
                if (actions.Any()) actions.Add(separator);
                actions.Add(billImport);
                if (enabled) actions.Add(billDetailImport);
            }

            if (actions.Any()) actions.Add(separator);

            actions.Add(vendorBillDashboard);


            return actions;
        }

        private void SetEditStatus(bool enabled)
        {
            hidEditStatus.Value = enabled.ToString();

            imgApplyToDocumentSearch.Enabled = enabled && txtBillType.Text != InvoiceType.Invoice.ToString();
            pnlDetails.Enabled = enabled;
            pnlVendorBillDetails.Enabled = enabled;

            memberToolBar.EnableSave = enabled;

            litMessage.Text = enabled ? string.Empty : WebApplicationConstants.ReadOnly;

            // must do this here to load correct set of extensible actions
            memberToolBar.LoadAdditionalActions(ToolbarMoreActions(enabled));
        }



        private void UpdateVendorBillDetailsFromView(VendorBill vendorBill)
        {
            var details = lstVendorBillDetails
                .Items
                .Select(i =>
                    {
                        var id = i.FindControl("hidVendorDetailBillId").ToCustomHiddenField().Value.ToLong();
                        return new
                            {
                                EditIdx = i.FindControl("hidVendorBillDetailIndex").ToCustomHiddenField().Value.ToInt(),
                                Detail = new VendorBillDetail(id, id != default(long))
                                        {
                                            ReferenceNumber = i.FindControl("txtRefNumber").ToTextBox().Text,
                                            ReferenceType = i.FindControl("ddlReferenceType").ToDropDownList().SelectedValue.ToInt().ToEnum<DetailReferenceType>(),
                                            Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
                                            UnitBuy = i.FindControl("txtUnitBuy").ToTextBox().Text.ToDecimal(),
											ChargeCodeId = i.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                                            AccountBucketId = i.FindControl("ddlAccountBucket").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                                            Note = i.FindControl("txtNote").ToTextBox().Text
                                        }
                            };
                    })
                .ToList();
            vendorBill.Details = SortVendorBillDetails(StoredVendorBillDetails);

            foreach (var item in details)
                vendorBill.Details[item.EditIdx] = item.Detail;

            foreach (var detail in vendorBill.Details.Where(d => d.IsNew))
            {
                detail.TenantId = vendorBill.TenantId;
                detail.VendorBill = vendorBill;
            }
        }

        private VendorBill UpdateVendorBill()
        {
            var vendorBillId = hidVendorBillId.Value.ToLong();
            var vendorBill = new VendorBill(vendorBillId, vendorBillId != default(long));

            if (vendorBill.IsNew)
            {
                vendorBill.TenantId = ActiveUser.TenantId;
                vendorBill.User = ActiveUser;
                vendorBill.DateCreated = DateTime.Now;
                vendorBill.PostDate = DateUtility.SystemEarliestDateTime;
                vendorBill.ExportDate = DateUtility.SystemEarliestDateTime;
            }
            else
            {
                vendorBill.LoadCollections();
            }

            var editStatus = hidEditStatus.Value.ToBoolean();
            if (!editStatus) return vendorBill;

            vendorBill.BillType = txtBillType.Text.ToEnum<InvoiceType>();
            vendorBill.DocumentNumber = txtDocumentNumber.Text;
            vendorBill.DocumentDate = string.IsNullOrEmpty(txtDocumentDate.Text) ? DateUtility.SystemEarliestDateTime : txtDocumentDate.Text.ToDateTime();
            vendorBill.VendorLocation = new VendorLocation(ddlVendorLocation.SelectedValue.ToLong());
            vendorBill.ApplyToDocumentId = hidApplyToDocumentId.Value.ToLong();

            //Update From View
            UpdateVendorBillDetailsFromView(vendorBill);

            return vendorBill;
        }



        private void LoadVendorBill(VendorBill vendorBill)
        {
            imgApplyToDocumentSearch.Enabled = vendorBill.BillType != InvoiceType.Invoice;

            if (!vendorBill.IsNew) vendorBill.LoadCollections();

            hidVendorBillId.Value = vendorBill.Id.ToString();

            //Above Details
            txtDocumentNumber.Text = vendorBill.DocumentNumber;
            SetPageTitle(txtDocumentNumber.Text);
            txtBillType.Text = vendorBill.BillType.ToString();
            txtApplyToDocument.Text = vendorBill.ApplyToDocument != null
                                          ? vendorBill.ApplyToDocument.DocumentNumber
                                          : vendorBill.BillType == InvoiceType.Invoice
                                                ? WebApplicationConstants.NotApplicable
                                                : string.Empty;
            hidApplyToDocumentId.Value = vendorBill.ApplyToDocumentId.ToString();

            //Details
            txtDateCreated.Text = vendorBill.DateCreated.FormattedShortDate();
            txtDocumentDate.Text = vendorBill.DocumentDate == DateUtility.SystemEarliestDateTime
                                       ? string.Empty
                                       : vendorBill.DocumentDate.FormattedShortDate();
            chkPosted.Checked = vendorBill.Posted;
            txtPostDate.Text = vendorBill.PostDate == DateUtility.SystemEarliestDateTime
                                   ? string.Empty
                                   : vendorBill.PostDate.FormattedShortDate();
            chkExported.Checked = vendorBill.Exported;
            txtExportDate.Text = vendorBill.ExportDate == DateUtility.SystemEarliestDateTime
                                     ? string.Empty
                                     : vendorBill.ExportDate.FormattedShortDate();
            txtUser.Text = vendorBill.User != null ? vendorBill.User.Username : ActiveUser.Username;

            if (vendorBill.VendorLocation == null)
            {
                ddlVendorLocation.Enabled = false;
                DisplayVendor(null);
            }
            else
            {
                DisplayVendor(vendorBill.VendorLocation.Vendor);
                ddlVendorLocation.SelectedValue = vendorBill.VendorLocation.Id.ToString();
            }

            //VendorBill Details
            var details = SortVendorBillDetails(vendorBill.Details);
            peLstVendorBillDetails.LoadData(details);
            tabVendorBillDetails.HeaderText = vendorBill.Details.BuildTabCount(VendorBillDetailsHeader);

            SetTotalAmount(vendorBill.Details.Sum(d => d.TotalAmount));

            var billSearch = new VendorBillSearch();
            // adjustments
            txtAdjustments.Text = vendorBill.BillType == InvoiceType.Invoice
                                      ? billSearch.FetchInvoiceTotalCreditAdjustments(vendorBill.Id, vendorBill.TenantId).ToString("c2")
                                      : "- None -";

            memberToolBar.ShowUnlock = vendorBill.HasUserLock(ActiveUser, vendorBill.Id);

            var chargesLinkedToBill = billSearch.ChargesAreLinkedToVendorBill(vendorBill.Id, vendorBill.TenantId);

            imgVendorSearch.Enabled = !chargesLinkedToBill;
            txtVendorNumber.ReadOnly = chargesLinkedToBill;
            txtVendorNumber.CssClass = chargesLinkedToBill ? "w110 disabled" : "w110";
            ddlVendorLocation.Enabled = !chargesLinkedToBill;

			//Vendor Bill Documents
			var documents = new VendorBillSearch().FetchAssociatedDocuments(vendorBill.Id);
	        lstDocuments.DataSource = documents;
			lstDocuments.DataBind();
	        tabDocuments.HeaderText = documents.BuildTabCount(VendorBillDocumentsHeader);

        }

        private static List<VendorBillDetail> SortVendorBillDetails(IEnumerable<VendorBillDetail> details)
        {
            return details
                .OrderBy(d => d.ReferenceType)
                .ThenBy(d => d.ReferenceNumber)
                .ToList();
        }


        private void ProcessTransferredRequest(VendorBill vendorBill, bool disableEdit = false)
        {
            if (vendorBill == null) return;
			if (vendorBill.IsNew || vendorBill.VendorLocation == null)
			{
				DisplayMessages(new[] { ValidationMessage.Error("Vendor bill does not exist.") });
				return;
			}

            LoadVendorBill(vendorBill);

            if (Lock != null && !vendorBill.IsNew && Access.Modify && !disableEdit)
            {
                SetEditStatus(vendorBill.ShouldEnable());
                Lock(this, new ViewEventArgs<VendorBill>(vendorBill));
            }
            else if (vendorBill.IsNew) SetEditStatus(vendorBill.ShouldEnable());
            else SetEditStatus(false);

            if (LoadAuditLog != null) LoadAuditLog(this, new ViewEventArgs<VendorBill>(vendorBill));
        }

        private void ReturnToDashboard()
        {
            if (UnLock != null)
                UnLock(this, new ViewEventArgs<VendorBill>(new VendorBill(hidVendorBillId.Value.ToLong(), false)));

            Response.Redirect(VendorBillsDashboardView.PageAddress);
        }

        private void ReturnToPosting()
        {
            if (UnLock != null)
                UnLock(this, new ViewEventArgs<VendorBill>(new VendorBill(hidVendorBillId.Value.ToLong(), false)));

            Response.Redirect(VendorBillPostingView.PageAddress);
        }


        private void PostVendorBill()
        {
            var vendorBill = UpdateVendorBill();

            vendorBill.PostDate = DateTime.Now;
            vendorBill.Posted = true;

            Lock(this, new ViewEventArgs<VendorBill>(vendorBill));

            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<VendorBill>(vendorBill));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<VendorBill>(vendorBill));
        }


        private void SetExported()
        {
            var vendorBill = UpdateVendorBill();

            vendorBill.ExportDate = DateTime.Now;
            vendorBill.Exported = true;

            Lock(this, new ViewEventArgs<VendorBill>(vendorBill));

            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<VendorBill>(vendorBill));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<VendorBill>(vendorBill));
        }

        private void ReverseExported()
        {
            var vendorBill = UpdateVendorBill();

            vendorBill.ExportDate = DateUtility.SystemEarliestDateTime;
            vendorBill.Exported = false;

            Lock(this, new ViewEventArgs<VendorBill>(vendorBill));

            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<VendorBill>(vendorBill));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<VendorBill>(vendorBill));
        }



        private void SetTotalAmount(decimal totalAmount)
        {
            txtTotalAmount.Text = totalAmount.ToString("c2");
            txtVendorBillTotal.Text = totalAmount.ToString("c2");
        }



        protected void DisplayDialogForImport(string arg)
        {
            hidFlag.Value = arg;
            fileUploader.Title = string.Format("VENDOR BILL {0}IMPORT", arg == VendorBillDetailsImportArgs ? "DETAIL ITEM " : string.Empty);
            fileUploader.Instructions = @"**Please refer to user guide for import file format";
            fileUploader.Visible = true;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new VendorBillHandler(this).Initialize();

            memberToolBar.ShowSave = Access.Modify;
            memberToolBar.ShowDelete = Access.Remove;
            memberToolBar.ShowNew = Access.Modify;
            memberToolBar.ShowEdit = Access.Modify;
            memberToolBar.LoadAdditionalActions(ToolbarMoreActions(hidEditStatus.Value.ToBoolean()));

            btnInvoiceSelect.CommandArgument = InvoiceType.Invoice.ToString();
            btnCreditSelect.CommandArgument = InvoiceType.Credit.ToString();

            SetPageTitle(txtDocumentNumber.Text);

            peLstVendorBillDetails.UseParentDataStore = true;
            peLstVendorBillDetails.ParentDataStoreKey = VendorBillDetailStoreKey;

            if (IsPostBack) return;

            aceVendor.SetupExtender(new Dictionary<string, string> { { AutoCompleteUtility.Keys.Tenant, ActiveUser.TenantId.GetString() } });

            memberToolBar.LoadTemplates(new List<string>
                                            {
                                                WebApplicationConstants.VendorBillsDetailsImportTemplate, 
                                                WebApplicationConstants.VendorBillsMassUploadImportTemplate
                                            });

            if (Loading != null)
                Loading(this, new EventArgs());

            SetEditStatus(false);

            if (Session[WebApplicationConstants.TransferVendorBillId] != null)
            {
                ProcessTransferredRequest(new VendorBill(Session[WebApplicationConstants.TransferVendorBillId].ToLong()));
                Session[WebApplicationConstants.TransferVendorBillId] = null;
            }

            if (!string.IsNullOrEmpty(Request.QueryString[WebApplicationConstants.TransferNumber]))
                ProcessTransferredRequest(new VendorBill(Request.QueryString[WebApplicationConstants.TransferNumber].UrlTextDecrypt().ToLong()), true);
        }


        protected void OnToolbarNewClicked(object sender, EventArgs e)
        {
            litErrorMessages.Text = string.Empty;

            vendorBillFinder.Reset();
            serviceTicketFinder.Reset();
            shipmentFinder.Reset();

            pnlDimScreen.Visible = true;
            pnlNewVendorBillSelection.Visible = true;
        }

        protected void OnVendorBillTypeSelectClicked(object sender, EventArgs e)
        {
            var value = (Button)sender;

            switch (value.CommandArgument.ToEnum<InvoiceType>())
            {
                case InvoiceType.Invoice:
                    txtBillType.Text = InvoiceType.Invoice.ToString();
                    txtApplyToDocument.Text = WebApplicationConstants.NotApplicable;
                    imgApplyToDocumentSearch.Enabled = false;
                    break;
                case InvoiceType.Credit:
                    txtBillType.Text = InvoiceType.Credit.ToString();
                    txtApplyToDocument.Text = string.Empty;
                    imgApplyToDocumentSearch.Enabled = false;
                    break;
            }

            var vendorBill = new VendorBill
            {
                DateCreated = DateTime.Now,
                DocumentDate = DateUtility.SystemEarliestDateTime,
                PostDate = DateUtility.SystemEarliestDateTime,
                ExportDate = DateUtility.SystemEarliestDateTime,
                BillType = txtBillType.Text.ToEnum<InvoiceType>(),
                ApplyToDocumentId = 0,
                Details = new List<VendorBillDetail>(),
            };

            pnlNewVendorBillSelection.Visible = false;
            pnlDimScreen.Visible = false;

            LoadVendorBill(vendorBill);

            DisplayAuditLogs(new List<AuditLogsViewSearchDto>());
            SetEditStatus(true);
        }

        protected void OnCloseNewVendorBillSelectionTypeClicked(object sender, EventArgs eventArgs)
        {
            pnlNewVendorBillSelection.Visible = false;
            pnlDimScreen.Visible = false;
        }


        protected void OnToolbarEditClicked(object sender, EventArgs e)
        {
            var vendorBill = new VendorBill(hidVendorBillId.Value.ToLong(), false);
			if (vendorBill.IsNew || vendorBill.VendorLocation == null) return;

            if (Lock == null || vendorBill.IsNew) return;

            SetEditStatus(vendorBill.ShouldEnable());
            memberToolBar.ShowUnlock = true;
            Lock(this, new ViewEventArgs<VendorBill>(vendorBill));

            LoadVendorBill(vendorBill);
        }

        protected void OnToolbarUnlockClicked(object sender, EventArgs e)
        {
            var vendorBill = new VendorBill(hidVendorBillId.Value.ToLong(), false);
            if (UnLock != null && !vendorBill.IsNew)
            {
                SetEditStatus(false);
                memberToolBar.ShowUnlock = false;
                UnLock(this, new ViewEventArgs<VendorBill>(vendorBill));
            }
        }

        protected void OnToolbarSaveClicked(object sender, EventArgs e)
        {
            var vendorBill = UpdateVendorBill();

            hidFlag.Value = SaveFlag;

            if (Save != null)
                Save(this, new ViewEventArgs<VendorBill>(vendorBill));

            if (LoadAuditLog != null)
                LoadAuditLog(this, new ViewEventArgs<VendorBill>(vendorBill));
        }

        protected void OnToolbarDeleteClicked(object sender, EventArgs e)
        {
            var vendorBill = new VendorBill(hidVendorBillId.Value.ToLong(), false);
			if(vendorBill.IsNew || vendorBill.VendorLocation == null) return;

            if (vendorBill.Posted)
            {
                DisplayMessages(new[] { ValidationMessage.Error(string.Format("Vendor Bill {0} is posted, can not delete", vendorBill.DocumentNumber)) });
                return;
            }

            if (Delete != null)
                Delete(this, new ViewEventArgs<VendorBill>(vendorBill));

            vendorBillFinder.Reset();
            serviceTicketFinder.Reset();
            shipmentFinder.Reset();
        }

        protected void OnToolbarFindClicked(object sender, EventArgs e)
        {
            hidFlag.Value = SearchFlag;
            if (!vendorBillFinder.OpenForEditEnabled)
            {
                vendorBillFinder.OpenForEditEnabled = Access.Modify;
                vendorBillFinder.Reset();
            }
            vendorBillFinder.ShowInvoiceTypeOnly = false;
            vendorBillFinder.ExcludeInvoiceId = default(long);
            vendorBillFinder.Visible = true;
        }

        protected void OnToolbarCommand(object sender, ViewEventArgs<string> e)
        {
            switch (e.Argument)
            {
                case ReturnArgs:
                    ReturnToDashboard();
                    break;
                case ReturnPostingArgs:
                    ReturnToPosting();
                    break;
                case PostArgs:
                    PostVendorBill();
                    break;
                case ManualExportArgs:
                    SetExported();
                    break;
                case ReverseExportArgs:
                    ReverseExported();
                    break;
                case VendorBillImportArgs:
                case VendorBillDetailsImportArgs:
                    DisplayDialogForImport(e.Argument);
                    break;
            }
        }


        protected void OnVendorBillFinderItemSelected(object sender, ViewEventArgs<VendorBill> e)
        {
            litErrorMessages.Text = string.Empty;

            var vendorBill = e.Argument;

            switch (hidFlag.Value)
            {
                case ApplyToDocumentFlag:
                    hidApplyToDocumentId.Value = vendorBill.Id.ToString();
                    txtApplyToDocument.Text = vendorBill.DocumentNumber;
                    vendorBillFinder.Visible = false;

                    var details = SortVendorBillDetails(vendorBill
                                                            .Details
                                                            .Select(d => new VendorBillDetail
                                                                {
                                                                    AccountBucketId = d.AccountBucketId,
                                                                    ChargeCodeId = d.ChargeCodeId,
                                                                    Note = d.Note,
                                                                    Quantity = d.Quantity,
                                                                    UnitBuy = d.UnitBuy,
                                                                    TenantId = d.TenantId,
                                                                    ReferenceType = d.ReferenceType,
                                                                    ReferenceNumber = d.ReferenceNumber,
                                                                }));

                    peLstVendorBillDetails.LoadData(details);
                    tabVendorBillDetails.HeaderText = vendorBill.Details.BuildTabCount(VendorBillDetailsHeader);
                    break;
                case SearchFlag:
                    if (hidVendorBillId.Value.ToLong() != default(long))
                    {
                        var oldVendorBill = new VendorBill(hidVendorBillId.Value.ToLong(), false);
                        if (UnLock != null)
                            UnLock(this, new ViewEventArgs<VendorBill>(oldVendorBill));
                    }

                    LoadVendorBill(vendorBill);

                    vendorBillFinder.Visible = false;

                    if (vendorBillFinder.OpenForEdit && Lock != null)
                    {
                        SetEditStatus(vendorBill.ShouldEnable());
                        memberToolBar.ShowUnlock = true;
                        Lock(this, new ViewEventArgs<VendorBill>(vendorBill));
                    }
                    else
                    {
                        SetEditStatus(false);
                    }

                    if (LoadAuditLog != null) LoadAuditLog(this, new ViewEventArgs<VendorBill>(vendorBill));
                    break;
            }
        }

        protected void OnVendorBillFinderItemCancelled(object sender, EventArgs e)
        {
            vendorBillFinder.Visible = false;
        }


        protected void OnAddServiceTicketClicked(object sender, EventArgs e)
        {
            serviceTicketFinder.Visible = true;
        }

        protected void OnServiceTicketFinderMultiItemSelected(object sender, ViewEventArgs<List<ServiceTicket>> e)
        {
            var details = SortVendorBillDetails(StoredVendorBillDetails);

            foreach (var serviceTicket in e.Argument)
            {
                var charges = chkDisableVendorFiltering.Checked
                                  ? serviceTicket.Charges
                                  : serviceTicket.VendorApplicableCharges(hidVendorId.Value.ToLong());

	            details.AddRange(
                        charges
                        .Where(sc => sc.VendorBillId == default(long) || sc.VendorBillId == hidVendorBillId.Value.ToLong())
			            .Select(c => new VendorBillDetail
				            {
					            ReferenceNumber = serviceTicket.ServiceTicketNumber,
					            ReferenceType = DetailReferenceType.ServiceTicket,
					            Quantity = c.Quantity,
					            UnitBuy = c.UnitBuy,
					            ChargeCodeId = c.ChargeCodeId,
					            AccountBucketId = serviceTicket.AccountBucket.Id,
					            Note = c.Comment
				            })
			            .ToList());
            }

            peLstVendorBillDetails.LoadData(SortVendorBillDetails(details), peLstVendorBillDetails.CurrentPage);
            tabVendorBillDetails.HeaderText = details.BuildTabCount(VendorBillDetailsHeader);

            SetTotalAmount(details.Sum(d => d.TotalAmount));
            serviceTicketFinder.Visible = false;
        }

        protected void OnServiceTicketFinderItemCancelled(object sender, EventArgs e)
        {
            serviceTicketFinder.Visible = false;
        }


        protected void OnAddShipmentClicked(object sender, EventArgs e)
        {
            shipmentFinder.Visible = true;
        }

        protected void OnShipmentFinderMultiItemSelected(object sender, ViewEventArgs<List<Shipment>> e)
        {
            var details = SortVendorBillDetails(StoredVendorBillDetails);

            foreach (var shipment in e.Argument)
            {
                var charges = chkDisableVendorFiltering.Checked
                                  ? shipment.Charges
                                  : shipment.VendorApplicableCharges(hidVendorId.Value.ToLong());

	            details.AddRange(
                        charges
			            .Where(sc => sc.VendorBillId == default(long) || sc.VendorBillId == hidVendorBillId.Value.ToLong())
			            .Select(c => new VendorBillDetail
				            {
					            ReferenceNumber = shipment.ShipmentNumber,
					            ReferenceType = DetailReferenceType.Shipment,
					            Quantity = c.Quantity,
					            UnitBuy = c.UnitBuy,
					            ChargeCodeId = c.ChargeCodeId,
					            AccountBucketId = shipment.AccountBuckets.First(a => a.Primary).AccountBucketId,
					            Note = c.Comment
				            })
			            .ToList());
            }

            peLstVendorBillDetails.LoadData(SortVendorBillDetails(details), peLstVendorBillDetails.CurrentPage);
            tabVendorBillDetails.HeaderText = details.BuildTabCount(VendorBillDetailsHeader);
            SetTotalAmount(details.Sum(d => d.TotalAmount));
            shipmentFinder.Visible = false;
        }

        protected void OnShipmentFinderItemCancelled(object sender, EventArgs e)
        {
            shipmentFinder.Visible = false;
        }


        protected void OnDeleteVendorBillDetailClicked(object sender, ImageClickEventArgs e)
        {
            var imageButton = (ImageButton)sender;

            var details = SortVendorBillDetails(StoredVendorBillDetails);
            var itemIdx = imageButton.FindControl("hidVendorBillDetailIndex").ToCustomHiddenField().Value.ToInt();
            details.RemoveAt(itemIdx);

            peLstVendorBillDetails.LoadData(SortVendorBillDetails(details), peLstVendorBillDetails.CurrentPage);
            tabVendorBillDetails.HeaderText = details.BuildTabCount(VendorBillDetailsHeader);
            SetTotalAmount(details.Sum(d => d.TotalAmount));
            athtuTabUpdater.SetForUpdate(tabVendorBillDetails.ClientID, details.BuildTabCount(VendorBillDetailsHeader));
        }

        protected void OnClearDetailsClicked(object sender, EventArgs e)
        {
            peLstVendorBillDetails.LoadData(new List<VendorBillDetail>());

            SetTotalAmount(0);
            tabVendorBillDetails.HeaderText = VendorBillDetailsHeader;
            athtuTabUpdater.SetForUpdate(tabVendorBillDetails.ClientID, VendorBillDetailsHeader);
        }

        protected void OnVendorBillDetailsItemDataBound(object sender, ListViewItemEventArgs e)
        {
            var item = (ListViewDataItem)e.Item;
            var dataItem = item.DataItem;

            var invoiceType = txtBillType.Text.ToEnum<InvoiceType>();
            var isCreditOrSupplemental = invoiceType == InvoiceType.Credit || invoiceType == InvoiceType.Supplemental;
            var referenceType = dataItem.GetPropertyValue("ReferenceType").ToInt().ToEnum<DetailReferenceType>();

            var ddlReferenceType = item.FindControl("ddlReferenceType").ToDropDownList();
            ddlReferenceType.DataSource = StoredInvoiceDetailReferenceTypes;
            ddlReferenceType.DataBind();
            if (dataItem.HasGettableProperty("ReferenceType")) ddlReferenceType.SelectedValue = dataItem.GetPropertyValue("ReferenceType").ToInt().GetString();
            ddlReferenceType.Enabled = isCreditOrSupplemental;

			var ddlAccountBucket = item.FindControl("ddlAccountBucket").ToCachedObjectDropDownList();
            ddlAccountBucket.Enabled = referenceType == DetailReferenceType.Miscellaneous;
		}


        protected void OnVendorNumberEntered(object sender, EventArgs e)
        {
            if (VendorSearch != null)
                VendorSearch(this, new ViewEventArgs<string>(txtVendorNumber.Text.RetrieveSearchCodeFromAutoCompleteString()));
        }

        protected void OnVendorSearchClicked(object sender, ImageClickEventArgs e)
        {
            vendorFinder.Visible = true;
        }

        protected void OnVendorFinderItemSelected(object sender, ViewEventArgs<Vendor> e)
        {
            DisplayVendor(e.Argument);
            vendorFinder.Visible = false;
        }

        protected void OnVendorFinderSelectionCancelled(object sender, EventArgs e)
        {
            vendorFinder.Visible = false;
        }

        protected void OnVendorBillSearchClicked(object sender, ImageClickEventArgs e)
        {
            hidFlag.Value = ApplyToDocumentFlag;
            if (vendorBillFinder.OpenForEditEnabled)
            {
                vendorBillFinder.OpenForEditEnabled = false;
                vendorBillFinder.Reset();
            }
            vendorBillFinder.ShowInvoiceTypeOnly = true;
            vendorBillFinder.ExcludeInvoiceId = hidVendorBillId.Value.ToLong();
            vendorBillFinder.Visible = true;
        }


        protected void OnFileUploadedError(object sender, ViewEventArgs<string> e)
        {
            messageBox.Button = MessageButton.Ok;
            messageBox.Icon = MessageIcon.Error;
            messageBox.Message = e.Argument;

            messageBox.Visible = true;
        }

        protected void OnFileUploaderUploadCancelled(object sender, EventArgs e)
        {
            hidFlag.Value = string.Empty;
            fileUploader.Visible = false;
        }

        protected void OnFileUploaderFileUploaded(object sender, FileUploaderEventArgs e)
        {

            List<string[]> lines;
            try
            {
                lines = e.GetImportData(true);
            }
            catch (Exception ex)
            {
                DisplayMessages(new List<ValidationMessage> { ValidationMessage.Error(ex.Message) });
                return;
            }

            fileUploader.Visible = false;

            switch (hidFlag.Value)
            {
                case VendorBillImportArgs:
                    ImportVendorBills(lines);
                    break;
                case VendorBillDetailsImportArgs:
                    ImportVendorBillDetails(lines);
                    break;
            }
        }

        private void ImportVendorBillDetails(ICollection<string[]> lines)
        {
            if (lines.Count > 5000)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Batch import is limited to 5,000 records, please adjust your file size.") });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 6;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var detailReferenceTypes = ProcessorUtilities.GetAll<DetailReferenceType>().Keys.Select(type => type.GetString()).ToList();
            detailReferenceTypes.RemoveAt(DetailReferenceType.Miscellaneous.ToInt());
            msgs.AddRange(chks
                .Where(i => !detailReferenceTypes.Contains(i.Line[1]))
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedEnumErrMsg, typeof(DetailReferenceType).Name.FormattedString(), i.Index))
                .ToList());
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var accountBucketSearch = new AccountBucketSearch();
            var chargeCodeSearch = new ChargeCodeSearch();

            var accountBuckets = ProcessorVars.RegistryCache[ActiveUser.TenantId].AccountBuckets.Select(c => c.Code).ToList();
            msgs.AddRange(chks.CodesAreValid(4, accountBuckets, new AccountBucket().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var chargeCodes = ProcessorVars.RegistryCache[ActiveUser.TenantId].ChargeCodes.Select(c => c.Code).ToList();
            msgs.AddRange(chks.CodesAreValid(5, chargeCodes, new ChargeCode().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }


            var nVendorBillDetails = lines
                .Select(s => new VendorBillDetail(default(long))
                {
                    ReferenceNumber = s[0].Trim(),
                    ReferenceType = s[1].ToEnum<DetailReferenceType>(),
                    Quantity = s[2].ToInt(),
                    UnitBuy = s[3].ToDecimal(),
                    AccountBucket = accountBucketSearch.FetchAccountBucketByCode(s[4], ActiveUser.TenantId),
                    ChargeCode = new ChargeCode(chargeCodeSearch.FetchChargeCodeIdByCode(s[5], ActiveUser.TenantId)),
                    Note = string.Empty,
                    TenantId = ActiveUser.TenantId,
                })
                .ToList();

            

            // check invalid records!
            var shSearch = new ShipmentSearch();
            var stSearch = new ServiceTicketSearch();
            var errors = nVendorBillDetails
                .Where(d =>
                       (d.ReferenceType == DetailReferenceType.Shipment && !shSearch.ShipmentExists(d.ReferenceNumber, ActiveUser.TenantId)) ||
                       (d.ReferenceType == DetailReferenceType.ServiceTicket && !stSearch.ServiceTicketExists(d.ReferenceNumber, ActiveUser.TenantId)))
                .ToList();
            if (errors.Any())
            {
                var recLines = errors
                    .Select(d => string.Format("{0} {1}", d.ReferenceNumber, d.ReferenceType.FormattedString()))
                    .ToArray();
                DisplayMessages(new[]
				                	{
				                		ValidationMessage.Error("The following record(s) were not found: {0}{1}{0} Import aborted.",
				                		                        WebApplicationConstants.HtmlBreak,
                                                                string.Join(WebApplicationConstants.HtmlBreak, recLines))
				                	});
                return;
            }

            var vendorBillDetails = SortVendorBillDetails(StoredVendorBillDetails);
            vendorBillDetails.AddRange(nVendorBillDetails);
            peLstVendorBillDetails.LoadData(SortVendorBillDetails(vendorBillDetails), peLstVendorBillDetails.CurrentPage);
            tabVendorBillDetails.HeaderText = vendorBillDetails.BuildTabCount(VendorBillDetailsHeader);
            SetTotalAmount(vendorBillDetails.Sum(d => d.TotalAmount));
        }

        private void ImportVendorBills(ICollection<string[]> lines)
        {
            // indices
            const int vendorNumber = 0;
            const int documentNumber = 1;
            const int billType = 2;
            const int originalInvoiceNumber = 3;
            const int documentDate = 4;
            const int referenceNumber = 5;
            const int referenceType = 6;
            const int quantity = 7;
            const int unitBuy = 8;
            const int accountBucketCode = 9;
            const int chargeCode = 10;

            var vendorSearch = new VendorSearch();
            var vendorBillSearch = new VendorBillSearch();

            //per original code, only fetch active account buckets
            var columns = new List<ParameterColumn>();
            var column = RegistrySearchFields.Active.ToParameterColumn();
            column.DefaultValue = true.ToString();
            column.Operator = Operator.Equal;
            columns.Add(column);
            var accountBuckets = new AccountBucketSearch().FetchAccountBuckets(columns, ActiveUser.TenantId);
            var chargeCodes = ProcessorVars.RegistryCache[ActiveUser.TenantId].ChargeCodes;

            if (lines.Count > 1000)
            {
                DisplayMessages(new[] { ValidationMessage.Error("Batch import is limited to 1,000 records, please adjust your file size.") });
                return;
            }

            var msgs = new List<ValidationMessage>();
            var cnt = 2;
            var chks = lines
                .Select(l => new ImportLineObj(l, cnt++, l.Length))
                .ToList();

            const int colCnt = 11;
            msgs.AddRange(chks.LineLengthsAreValid(colCnt));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var types = ProcessorUtilities.GetAll<InvoiceType>().Keys
                .Select(type => type.ToString())
                .ToList();
            types.Remove(InvoiceType.Supplemental.ToInt().ToString()); // supplemental type is not valid for vendor bill
            if (!lines.All(i => types.Contains(i[billType])))
            {
                msgs = chks
                    .Where(i => !types.Contains(i.Line[billType]))
                    .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedEnumErrMsg, typeof(InvoiceType).Name.FormattedString(), i.Index))
                    .ToList();
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var detailReferenceTypes = ProcessorUtilities.GetAll<DetailReferenceType>().Keys.Select(type => type.GetString()).ToList();
            detailReferenceTypes.RemoveAt(DetailReferenceType.Miscellaneous.ToInt());
            msgs.AddRange(chks
                .Where(i => !detailReferenceTypes.Contains(i.Line[referenceType]))
                .Select(i => ValidationMessage.Error(WebApplicationConstants.InvalidImportedEnumErrMsg, typeof(DetailReferenceType).Name.FormattedString(), i.Index))
                .ToList());
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }


            var chargeCodesAsStrings = chargeCodes.Select(c => c.Code).ToList();
            msgs.AddRange(chks.CodesAreValid(chargeCode, chargeCodesAsStrings, new ChargeCode().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var accountBucketsAsStrings = accountBuckets.Select(c => c.Code).ToList();
            msgs.AddRange(chks.CodesAreValid(accountBucketCode, accountBucketsAsStrings, new AccountBucket().EntityName()));
            if (msgs.Any())
            {
                msgs.Add(ValidationMessage.Information(WebApplicationConstants.NoRecordsImported));
                DisplayMessages(msgs);
                return;
            }

            var groups = lines.GroupBy(i => string.Format("{0}_{1}", i[vendorNumber], i[documentNumber]), i => i);

            var bills = new List<VendorBill>();
            foreach (var @group in groups)
            {
                var first = @group.FirstOrDefault();
                if (first == null) continue;

                var vendor = vendorSearch.FetchVendorByNumber(first[vendorNumber], ActiveUser.TenantId);
                var location = vendor == null
                                ? null
                                : vendor.Locations.FirstOrDefault(l => l.RemitToLocation && l.MainRemitToLocation);


                var bill = new VendorBill
                {
                    TenantId = ActiveUser.TenantId,
                    User = ActiveUser,
                    DateCreated = DateTime.Now,
                    PostDate = DateUtility.SystemEarliestDateTime,
                    ExportDate = DateUtility.SystemEarliestDateTime,

                    VendorLocation = location,
                    DocumentNumber = first[documentNumber],
                    DocumentDate = first[documentDate].ToDateTime(),
                    BillType = first[billType].ToEnum<InvoiceType>(),
                    ApplyToDocument = first[billType].ToEnum<InvoiceType>() == InvoiceType.Credit && location != null
                                        ? vendorBillSearch.FetchVendorBillByVendorBillNumber(
                                            first[originalInvoiceNumber], location.Id, ActiveUser.TenantId)
                                        : null
                };

				

                foreach (var line in @group)
                {
                    bill.Details.Add(new VendorBillDetail
                        {
                            ReferenceNumber = line[referenceNumber],
                            ReferenceType = line[referenceType].ToEnum<DetailReferenceType>(),
                            Quantity = line[quantity].ToInt(),
                            UnitBuy = line[unitBuy].ToDecimal(),
                            AccountBucket = accountBuckets.FirstOrDefault(ab => ab.Code == line[accountBucketCode]),
                            ChargeCode = chargeCodes.FirstOrDefault(cc => cc.Code == line[chargeCode]),
                            Note = string.Empty,
                            VendorBill = bill,
                            TenantId = bill.TenantId
                        });
                }

                bills.Add(bill);
            }

            // check invalid records!
            var shSearch = new ShipmentSearch();
            var stSearch = new ServiceTicketSearch();
            var errors = bills.SelectMany(b => b.Details)
                .Where(d =>
                       (d.ReferenceType == DetailReferenceType.Shipment && !shSearch.ShipmentExists(d.ReferenceNumber, ActiveUser.TenantId)) ||
                       (d.ReferenceType == DetailReferenceType.ServiceTicket && !stSearch.ServiceTicketExists(d.ReferenceNumber, ActiveUser.TenantId)))
                .ToList();
            if (errors.Any())
            {
                var recLines = errors
                    .Select(d => string.Format("{0} {1}", d.ReferenceNumber, d.ReferenceType.FormattedString()))
                    .ToArray();
                DisplayMessages(new[]
				                	{
				                		ValidationMessage.Error("The following record(s) were not found: {0}{1}{0} Import aborted.",
				                		                        WebApplicationConstants.HtmlBreak,
                                                                string.Join(WebApplicationConstants.HtmlBreak, recLines))
				                	});
                return;
            }

            if (BatchSave != null)
                BatchSave(this, new ViewEventArgs<List<VendorBill>>(bills));
        }


        protected void OnRemoveDetailsClicked(object sender, EventArgs e)
        {
            chkRemoveDetailSelectAll.Checked = false;

            var details = SortVendorBillDetails(StoredVendorBillDetails);

            lstRemoveDetails.DataSource = details;
            lstRemoveDetails.DataBind();

            pnlRemoveDetails.Visible = true;
            pnlDimScreen.Visible = true;
        }

        protected void OnCloseRemoveDetailsClicked(object sender, EventArgs eventArgs)
        {
            pnlRemoveDetails.Visible = false;
            pnlDimScreen.Visible = false;
        }

        protected void OnRemoveSelectedDetailsClicked(object sender, EventArgs e)
        {
            var cnt = 0;
            var currentDetails = SortVendorBillDetails(StoredVendorBillDetails)
                .Select(detail => new
                {
                    Index = cnt++,
                    Detail = detail,
                    Selected = false
                })
                .ToList();

            var removeDetails = lstRemoveDetails.Items
                .Select(i => new
                {
                    Index = i.FindControl("hidDetailIndex").ToCustomHiddenField().Value.ToInt(),
                    Id = i.FindControl("hidDetailId").ToCustomHiddenField().Value.ToLong(),
                    Selected = i.FindControl("chkSelection").ToAltUniformCheckBox().Checked,
                })
                .Where(i => i.Selected)
                .ToList();

            currentDetails.RemoveAll(d => removeDetails.Exists(c => c.Index == d.Index && c.Id == d.Detail.Id));
            peLstVendorBillDetails.LoadData(SortVendorBillDetails(currentDetails.Select(cd => cd.Detail)), peLstVendorBillDetails.CurrentPage);

            SetTotalAmount(currentDetails.Sum(d => d.Detail.TotalAmount));

            pnlRemoveDetails.Visible = false;
            pnlDimScreen.Visible = false;

            tabVendorBillDetails.HeaderText = currentDetails.BuildTabCount(VendorBillDetailsHeader);
            athtuTabUpdater.SetForUpdate(tabVendorBillDetails.ClientID, currentDetails.BuildTabCount(VendorBillDetailsHeader));
        }

        protected void OnVendorBillDetailsIndexChange(object sender, EventArgs e)
        {
            var details = lstVendorBillDetails
                .Items
                .Select(i =>
                {
                    var id = i.FindControl("hidVendorDetailBillId").ToCustomHiddenField().Value.ToLong();
                    return new
                    {
                        EditIdx = i.FindControl("hidVendorBillDetailIndex").ToCustomHiddenField().Value.ToInt(),
                        Detail = new VendorBillDetail(id, id != default(long))
                        {
                            ReferenceNumber = i.FindControl("txtRefNumber").ToTextBox().Text,
                            ReferenceType = i.FindControl("ddlReferenceType").ToDropDownList().SelectedValue.ToInt().ToEnum<DetailReferenceType>(),
                            Quantity = i.FindControl("txtQuantity").ToTextBox().Text.ToInt(),
                            UnitBuy = i.FindControl("txtUnitBuy").ToTextBox().Text.ToDecimal(),
                            ChargeCodeId = i.FindControl("ddlChargeCode").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                            AccountBucketId = i.FindControl("ddlAccountBucket").ToCachedObjectDropDownList().SelectedValue.ToLong(),
                            Note = i.FindControl("txtNote").ToTextBox().Text
                        }
                    };
                })
                .ToList();

            var data = peLstVendorBillDetails.GetData<VendorBillDetail>();

            foreach (var item in details)
                data[item.EditIdx] = item.Detail;

            peLstVendorBillDetails.LoadData(data, peLstVendorBillDetails.CurrentPage);
            SetTotalAmount(data.Sum(d => d.TotalAmount));
        }

        protected void OnVendorBillDetailsTextChanged(object sender, EventArgs e)
        {
            var listViewDataItem = ((Control)sender).Parent;

            var details = StoredVendorBillDetails;

            var txtQuantity = listViewDataItem.FindControl("txtQuantity").ToTextBox();
            var txtUnitBuy = listViewDataItem.FindControl("txtUnitBuy").ToTextBox();
            var txtTotal = listViewDataItem.FindControl("txtTotalAmount").ToTextBox();

            var idx = listViewDataItem.FindControl("hidVendorBillDetailIndex").ToCustomHiddenField().Value.ToInt();
            var qty = txtQuantity.Text.ToInt();
            var unitBuy = txtUnitBuy.Text.ToDecimal();

            txtTotal.Text = (qty * unitBuy).ToString("f4");
            details[idx].Quantity = qty;
            details[idx].UnitBuy = unitBuy;

            SetTotalAmount(details.Sum(d => d.TotalAmount));

            if (((Control)sender).ClientID == txtQuantity.ClientID) hidControlToFocus.Value = txtUnitBuy.ClientID;
            if (((Control)sender).ClientID == txtUnitBuy.ClientID) hidControlToFocus.Value = txtTotal.ClientID;

        }

		protected string GetLocationFileName(object relativePath)
		{
			return relativePath == null ? string.Empty : Server.GetFileName(relativePath.ToString());
		}

		protected void OnLocationPathClicked(object sender, EventArgs e)
		{
			var button = (LinkButton)sender;
			var path = button.Parent.FindControl("hidLocationPath").ToCustomHiddenField().Value;
			Response.Export(Server.ReadFromFile(path), button.Text);
		}
    }
}
