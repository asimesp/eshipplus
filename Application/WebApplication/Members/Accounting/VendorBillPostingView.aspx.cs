﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Administration;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting
{
    public partial class VendorBillPostingView : MemberPageBase, IVendorBillPostingView
    {
        private SearchField SortByField
        {
            get { return AccountingSearchFields.VendorBillsSortFields.FirstOrDefault(c => c.Name == ddlSortBy.SelectedValue); }
        }

        private const string PostArgs = "POST";

        protected bool HasAccessToModifyVendorBillPosting { get; set; }

        public override ViewCode PageCode { get { return ViewCode.VendorBillPosting; } }

        public static string PageAddress { get { return "~/Members/Accounting/VendorBillPostingView.aspx"; } }

        public override string SetPageIconImage { set { imgPageImageLogo.ImageUrl = IconLinks.FinanceBlue; } }

        public event EventHandler<ViewEventArgs<VendorBillViewSearchCriteria>> Search;
        public event EventHandler<ViewEventArgs<List<VendorBill>>> Post;

        public void DisplaySearchResult(List<VendorBillDashboardDto> vendorBills)
        {
            litRecordCount.Text = vendorBills.BuildRecordCount();
            lstVendorBillDetails.DataSource = vendorBills;
            lstVendorBillDetails.DataBind();
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }


        private List<ToolbarMoreAction> ToolbarMoreActions()
        {
            var post = new ToolbarMoreAction
            {
                CommandArgs = PostArgs,
                ConfirmCommand = false,
                ImageUrl = IconLinks.New,
                Name = "Post Vendor Bill(s)",
                IsLink = false,
                NavigationUrl = string.Empty,
                EnableProcessingDiv = true
            };

            var moreActions = new List<ToolbarMoreAction>();

            if (ActiveUser.HasAccessToModify(ViewCode.VendorBillPosting))
                moreActions.Add(post);
            else
                memberToolBar.ShowMore = false;

            return moreActions;
        }


        private List<VendorBill> RetrieveSelectedVendorBills()
        {
            var selected = lstVendorBillDetails.Items
                .Select(s => new
                {
                    Id = s.FindControl("hidVendorBillId").ToCustomHiddenField().Value.ToLong(),
                    Detail = ((VendorBillPostingDetail2Control)s.FindControl("vendorBillPostingDetail2")),
                })
                .Where(s => s.Detail != null && s.Detail.Selected)
                .Select(s => new VendorBill(s.Id))
                .ToList();

            return selected;
        }

        private void PostVendorBills()
        {
            var vendorBills = RetrieveSelectedVendorBills();

            if (Post != null)
                Post(this, new ViewEventArgs<List<VendorBill>>(vendorBills));
            DoSearchPreProcessingThenSearch(SortByField);
        }

        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems
            {
                Columns = GetCurrentRunParameters(true),
                SortAscending = rbAsc.Checked,
                SortBy = SortByField,
            };
        }

        private void DoSearchPreProcessingThenSearch(SearchField field)
        {
            var criteria = (new VendorBillViewSearchCriteria
            {
                Parameters = GetCurrentRunParameters(false),
                ActiveUserId = ActiveUser.Id,
                SortBy = field,
                SortAscending = rbAsc.Checked
            });

            // filter for non posted only
            var postedParameter = AccountingSearchFields.Posted.ToParameterColumn();
            postedParameter.DefaultValue = false.ToString();
            postedParameter.Operator = Operator.Equal;
            criteria.Parameters.Add(postedParameter);

            DoSearch(criteria);
        }

        private void DoSearch(VendorBillViewSearchCriteria criteria)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<VendorBillViewSearchCriteria>(criteria));
            chkSelectAllRecords.Checked = false;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new VendorBillPostingHandler(this).Initialize();

            HasAccessToModifyVendorBillPosting = ActiveUser.HasAccessToModify(ViewCode.VendorBillPosting);
            memberToolBar.LoadAdditionalActions(ToolbarMoreActions());

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            // set auto referesh
            tmRefresh.Interval = WebApplicationConstants.DefaultRefreshInterval.ToInt();

            // set sort fields
            ddlSortBy.DataSource = AccountingSearchFields.VendorBillsSortFields.OrderBy(s => s.DisplayName);
            ddlSortBy.DataBind();
            ddlSortBy.SelectedValue = AccountingSearchFields.DocumentNumber.Name;

            //link up sort buttons
            lbtnSortDocumentNumber.CommandName = AccountingSearchFields.DocumentNumber.Name;
            lbtnSortDateCreated.CommandName = AccountingSearchFields.DateCreated.Name;
            lbtnSortDocumentDate.CommandName = AccountingSearchFields.DocumentDate.Name;
            lbtnSortAmountDue.CommandName = AccountingSearchFields.AmountDue.Name;
            lbtnSortUsername.CommandName = AccountingSearchFields.Username.Name;
            lbtnSortTypeText.CommandName = AccountingSearchFields.BillTypeText.Name;

            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);
            var criteria = new VendorBillViewSearchCriteria
            {
                ActiveUserId = ActiveUser.Id,
                Parameters = profile != null
                                 ? profile.Columns
                                 : AccountingSearchFields.DefaultVendorBills.Select(
                                     f => f.ToParameterColumn()).ToList(),
                SortAscending = profile == null || profile.SortAscending,
                SortBy = profile == null ? null : profile.SortBy
            };

            // ensure non posted invoices only
            var postedColumns = new List<ParameterColumn>();

            postedColumns.AddRange(criteria.Parameters.Where(c => c.ReportColumnName == AccountingSearchFields.Posted.ToParameterColumn().ReportColumnName));

            foreach (var column in postedColumns)
                criteria.Parameters.Remove(column);

            lstFilterParameters.DataSource = criteria.Parameters;
            lstFilterParameters.DataBind();

            rbAsc.Checked = criteria.SortAscending;
            rbDesc.Checked = !criteria.SortAscending;

            if (criteria.SortBy != null && ddlSortBy.ContainsValue(criteria.SortBy.Name))
                ddlSortBy.SelectedValue = criteria.SortBy.Name;
        }


        protected void OnToolbarCommand(object sender, ViewEventArgs<string> e)
        {
            switch (e.Argument)
            {
                case PostArgs:
                    PostVendorBills();
                    break;
            }
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnVendorBillDetailsItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var vb = (VendorBillDashboardDto)item.DataItem;

            if (vb == null) return;

            var detailControl = (VendorBillPostingDetail2Control)item.FindControl("vendorBillPostingDetail2");

            detailControl.LoadVendorBill(vb);
        }



        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = AccountingSearchFields.VendorBills.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            //Remove posted as available parameter because
            //we set it in DoSearchPreProcessingThenSearch as a filter per original code
            parameterSelector.LoadParameterSelector(AccountingSearchFields.VendorBills
                                .Where(f => f.Name != AccountingSearchFields.Posted.Name)
                                .ToList());
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;

        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSortData(object sender, CommandEventArgs e)
        {
            var field = AccountingSearchFields.VendorBillsSortFields.FirstOrDefault(c => c.Name == e.CommandName);
            if (field == null) return;
            if (ddlSortBy.ContainsValue(field.Name))
            {
                if (ddlSortBy.SelectedValue == field.Name && rbAsc.Checked)
                {
                    rbAsc.Checked = false;
                    rbDesc.Checked = true;
                }
                else if (ddlSortBy.SelectedValue == field.Name && rbDesc.Checked)
                {
                    rbAsc.Checked = true;
                    rbDesc.Checked = false;
                }

                ddlSortBy.SelectedValue = field.Name;
            }

            DoSearchPreProcessingThenSearch(field);
        }

        protected void OnSortBySelectedIndexChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }

        protected void OnSortOrderCheckChanged(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }



        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns.Where(c => c.ReportColumnName != AccountingSearchFields.Posted.ToParameterColumn().ReportColumnName);
            lstFilterParameters.DataBind();

            rbAsc.Checked = e.Argument.SortAscending;
            rbDesc.Checked = !e.Argument.SortAscending;

            if (e.Argument.SortBy != null && ddlSortBy.ContainsValue(e.Argument.SortBy.Name))
                ddlSortBy.SelectedValue = e.Argument.SortBy.Name;
        }

        protected void OnSearchProfilesAutoRefreshChanged(object sender, ViewEventArgs<int> e)
        {
            tmRefresh.Enabled = e.Argument > default(int);
            if (e.Argument > 0) tmRefresh.Interval = e.Argument;
        }


        protected void OnTimerTick(object sender, EventArgs e)
        {
            DoSearchPreProcessingThenSearch(SortByField);
        }
    }
}