﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="VendorChargeConfirmationView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.VendorChargeConfirmation" EnableEventValidation="false" %>

<%@ Register TagPrefix="eShip" TagName="MainToolBar" Src="~/Members/Controls/MainToolbarControl.ascx" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.Operations" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Operations" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>


<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
                Vendor Charges Confirmation<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
            </h3>
        </div>
        <hr class="dark mb10" />
        <asp:Panel runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="Charges"
                            OnProcessingError="OnSearchProfilesProcessingError" OnSearchProfileSelected="OnSearchProfilesProfileSelected" />
                    </div>
                </div>
            </div>
            <hr class="dark mb10" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                        OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>
            <hr class="fat" />
            <div class="row mb10">
                <div class="fieldgroup">
                    <asp:Button ID="btnSearch" CssClass="left" runat="server" Text="SEARCH" OnClick="OnSearchClicked" CausesValidation="False" />
                </div>
                <div class="fieldgroup right">
                    <div class="fieldgroup">
                        <asp:Button ID="btnConfirm" CssClass="right mr10" runat="server" Text="Confirm" OnClick="OnConfirmClicked" CausesValidation="False" />
                    </div>
                    <div class="fieldgroup">
                        <asp:Button ID="btnDispute" CssClass="right mr10" runat="server" Text="Dispute" OnClick="OnDisputeClicked" CausesValidation="False" />
                    </div>
                </div>
            </div>
            <div class="row mb10">
                <div class="fieldgroup right">
                    <label>Sort By:</label>
                    <asp:DropDownList runat="server" ID="ddlSortBy" DataTextField="DisplayName" DataValueField="Name"
                        OnSelectedIndexChanged="OnSortBySelectedIndexChanged" AutoPostBack="True" CssClass="mr10" />
                    <asp:RadioButton runat="server" ID="rbAsc" Text="ASC" GroupName="rbSortBy" AutoPostBack="True" ToolTip="Ascending"
                        OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                    <asp:RadioButton runat="server" ID="rbDesc" Text="DESC" GroupName="rbSortBy" Checked="True" ToolTip="Descending" AutoPostBack="True"
                        OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                </div>
            </div>
			 
        </asp:Panel>
        <script type="text/javascript">

            function SetCheckAllCharges(control) {
                $("#chargeTable input:checkbox[id*='chkSelected']").each(function () {
                    this.checked = control.checked;
                    SetAltUniformCheckBoxClickedStatus(this);
                });

                $("input:checkbox[id*='chkInvoiceSelected']").each(function () {
                    this.checked = control.checked;
                    SetAltUniformCheckBoxClickedStatus(this);
                });

                // necessary to keep frozen header and header checkbox states the same
                $('[id*=' + control.id + ']').each(function () {
                    this.checked = control.checked;
                    SetAltUniformCheckBoxClickedStatus(this);
                });
            }
            
            function SetCheckParentEntity(control, parentChkBoxId, selectedChargesDiv) {
                $("#" + parentChkBoxId + " input:checkbox[id*='chkSelected']").each(function () {
                    
                    var numChecked = ($("#" + selectedChargesDiv + " input:checkbox:checked").length);
                    this.checked = control.checked || numChecked > 0;
                    SetAltUniformCheckBoxClickedStatus(this);
                    
                    if (!this.checked && numChecked == 0) {
                        SetCheckAllCheckBoxUnChecked(control, 'chargeTable', 'chkSelectAllRecords');
                    }
                });
                
            }

            function SetCheckChildCharges(control, selectedChargesDiv) {
                $("#" + selectedChargesDiv + " input:checkbox[id*='chkInvoiceSelected']").each(function () {
                    this.checked = control.checked;
                    SetAltUniformCheckBoxClickedStatus(this);
                });
            }

        </script>
        <asp:UpdatePanel runat="server" ID="upDataUpdate">
            <ContentTemplate>
                <eShip:UpdatePanelContinuousScrollExtender ID="upcseDataUpdate" runat="server" BindableControlId="rptEntities"
                    UpdatePanelToExtendId="upDataUpdate" />
                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheChargeDashboardTable" TableId="chargeTable" HeaderZIndex="2" />
                <div class="rowgroup">
                    <table id="chargeTable" class="line2 pl2">
                        <thead>
                            <tr>
                                <th style="width: 5%;">
                                    <eShip:AltUniformCheckBox ID="chkSelectAllRecords" runat="server" OnClientSideClicked="SetCheckAllCharges(this); " />
                                </th>
                                <th style="width: 35%;">
                                    <asp:LinkButton runat="server" ID="lbtnSortType" CssClass="link_nounderline blue" CausesValidation="False"
                                        OnCommand="OnSortData" Text='<abbr title="Order Type">Type</abbr>' />
                                </th>
                                <%= ActiveUser.CanSeeAllVendorsInVendorPortal ? "<th style='width: 20%;'>Vendor Name </th>" : string.Empty %>
                                <th style="width: 30%;">
                                    <asp:LinkButton runat="server" ID="lbtnSortNumber" CssClass="link_nounderline blue" CausesValidation="False"
                                        OnCommand="OnSortData" Text='<abbr title="Identity Number">Reference Number</abbr>' />
                                </th>
                                <th style="width: 30%;">
                                    <asp:LinkButton runat="server" ID="lbtnUnitBuyTotal" CssClass="link_nounderline blue" CausesValidation="False"
                                        OnCommand="OnSortData" Text='<abbr title="Unit Buy Total">Unit Buy Total</abbr>' />
                                </th>
                            </tr>
                        </thead>
                        <asp:Repeater runat="server" ID="rptEntities" OnItemDataBound="OnChargesItemDataBound">

                            <ItemTemplate>
                                <tr>
                                    <td rowspan="2" class="top">
                                        <div id='<%# string.Format("chkSelected{0}{1}{2}",Eval("Number"),Eval("ChargeType"),Eval("VendorId") ) %>'>
                                            <eShip:AltUniformCheckBox ID="chkSelected" runat="server"
                                                OnClientSideClicked='<%# string.Format("SetCheckChildCharges(this, \"{0}{1}{2}SelectedCharges\"); SetCheckAllCheckBoxUnChecked(this, \"chargeTable\", \"chkSelectAllRecords\");",Eval("Number"),Eval("ChargeType"),Eval("VendorId")) %>' />
                                        </div>
                                    </td>
                                    <td>
                                        <%# Eval("ChargeType") %>
                                        <eShip:CustomHiddenField ID="hidChargeType" runat="server" Value='<%# Eval("ChargeType") %>' />
                                    </td>
                                    <%# ActiveUser.CanSeeAllVendorsInVendorPortal ? "<td> " + Eval("VendorName") + "</td>" : string.Empty %>
                                    <td class="top">
                                        <%# Eval("Number") %>

                                        <%# Eval("ChargeType").ToString() == ChargeType.Shipment.ToString() && ActiveUser.HasAccessTo(ViewCode.Shipment)
                                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Shipment Record'><img src='{3}' alt='' width='16' align='absmiddle'/></a>",
                                                                ResolveUrl(ShipmentView.PageAddress),
                                                                WebApplicationConstants.TransferNumber,
                                                                Eval("Number").GetString(),
                                                                ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                : string.Empty %>
                                        <%# Eval("ChargeType").ToString() == ChargeType.ServiceTicket.ToString() && ActiveUser.HasAccessTo(ViewCode.ServiceTicket)
                                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Service Ticket Record'><img src='{3}' alt='' width='16' align='absmiddle'/></a>",
                                                                ResolveUrl(ServiceTicketView.PageAddress),
                                                                WebApplicationConstants.TransferNumber,
                                                                Eval("Number").GetString(),
                                                                ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                : string.Empty %>
                                    </td>
                                    <td class="top">
                                        <%# Eval("UnitBuy").ToDecimal().ToString("c2") %>
                                    </td>
                                </tr>
                                <tr class="hidden">
                                    <td>&nbsp;</td>
                                </tr>
                                <tr class="f9">
                                    <td colspan="6" class="top forceLeftBorder">
                                        <div id='<%# string.Format("{0}{1}{2}SelectedCharges",Eval("Number"),Eval("ChargeType"),Eval("VendorId") ) %>'>
                                        <asp:Repeater runat="server" ID="rptCharges">
                                            <HeaderTemplate>
                                                <div class="rowgroup mb5">
                                                    <div class="row">
                                                        <h6 class="mt0 mb0">Charges </h6>
                                                        <table class="contain pl2" id="shipmentChargesTable">
                                                            <thead>
                                                                <tr class="bbb1">
                                                                    <th style="width: 15%;" class="no-top-border">Action</th>
                                                                    <th style="width: 25%;" class="no-top-border no-left-border">Charge Code</th>
                                                                    <%# ActiveUser.CanSeeAllVendorsInVendorPortal ? "<th  class='no-top-border no-left-border'> Vendor Name</td>" : string.Empty %>
                                                                    <th style="width: 10%;" class="no-top-border no-left-border">Quantity</th>
                                                                    <th style="width: 10%;" class="no-top-border no-left-border">Unit Buy<abbr title="Dollars">($)</abbr></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <eShip:AltUniformCheckBox runat="server" ID="chkInvoiceSelected" Visible='<%# Eval("Status").ToInt() == ChargeStatus.Open.ToInt() %>'
                                                              OnClientSideClicked='<%# string.Format("SetCheckParentEntity(this,\"chkSelected{0}{1}{2}\",\"{0}{1}{2}SelectedCharges\");",Eval("Number"),Eval("ChargeType"),Eval("VendorId")) %>' />

                                                        <label style='<%# Eval("Status").ToInt() == ChargeStatus.Open.ToInt() ? "display: none": "" %>' class="upper"><%# Eval("Status").ToString() %></label>
                                                        <%# Eval("Status").ToInt() == ChargeStatus.Billed.ToInt() && ActiveUser.HasAccessTo(ViewCode.VendorBill)
                                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Vendor Bill Record'><img src='{3}' alt='' width='16' align='absmiddle'/></a>",
                                                                ResolveUrl(VendorBillView.PageAddress),
                                                                WebApplicationConstants.TransferNumber,
                                                                Eval("VendorBillId").GetString().UrlTextEncrypt(),
                                                                ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                : string.Empty %>

                                                        <%# Eval("Status").ToInt() == ChargeStatus.Disputed.ToInt() && ActiveUser.HasAccessTo(ViewCode.VendorBill)
                                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Vendor Dispute Record'><img src='{3}' alt='' width='16' align='absmiddle'/></a>",
                                                                ResolveUrl(VendorDisputeView.PageAddress),
                                                                WebApplicationConstants.TransferDisputeVendorId,
                                                                Eval("DisputedId").GetString().UrlTextEncrypt(),
                                                                ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                : string.Empty %>
                                                    </td>
                                                    <td>
                                                        <eShip:CustomHiddenField ID="hidChargeId" runat="server" Value='<%# Eval("Id") %>' />
                                                        <eShip:CustomHiddenField ID="hidChargeNumber" runat="server" Value='<%# Eval("Number") %>' />
                                                        <input name="txtChargeCode" value='<%# Eval("ChargeCode.Code").ToString() %>' disabled="disabled" id="ChargeCode" class="w330" onblur="UpdateChargeInformation(this);" floatingpointnumbersonly="true" type="text">
                                                    </td>
                                                    <%# ActiveUser.CanSeeAllVendorsInVendorPortal ? "<td> " + Eval("VendorName") + "</td>" : string.Empty %>
                                                    <td class="text-center">
                                                        <input name="txtQuantity" value='<%# Eval("Quantity").ToString() %>' disabled="disabled" id="txtQuantity" class="w40" onblur="UpdateChargeInformation(this);" numbersonly="true" type="text">
                                                    </td>
                                                    <td class="text-right">
                                                        <input name="txtUnitBuy" value='<%# Eval("UnitBuy").ToDecimal().ToString("c2") %>' disabled="disabled" id="txtUnitBuy" class="w75" onblur="UpdateChargeInformation(this);" floatingpointnumbersonly="true" type="text">
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody>
                                                </table>
                                            </div>
                                        </div>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                        </div>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </div>


            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lbtnSortType" />
                <asp:PostBackTrigger ControlID="lbtnSortNumber" />
                <asp:PostBackTrigger ControlID="rptEntities" />
            </Triggers>
        </asp:UpdatePanel>
        <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
        <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" />
        <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />





        <asp:UpdatePanel ID="pnlEdit" runat="server" Visible="false">
            <ContentTemplate>
                <div class="popupControl popupControlOverW750">
                    <div class="popheader">
                        <h4>Approve Charges
                        </h4>
                        <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                            CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
                    </div>
                    <div class="row mt10">
                        <div class="fieldgroup right">
                            <asp:Button ID="btnSave" Text="Confirm Charges" OnClick="OnSaveClick" runat="server" CausesValidation="False" />
                        </div>
                        <div class="fieldgroup right">
                            <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnCloseClicked" runat="server" CausesValidation="false" />
                        </div>
                    </div>
                    <div style="overflow-y: auto; overflow-x: hidden;   max-height: 600px">
                        <asp:Repeater runat="server" ID="rptGroupsChargesToApprove" OnItemDataBound="OnApproveChargesItemDataBound">
                            <ItemTemplate>
                                <eShip:CustomHiddenField ID="hidParentId" runat="server" Value='<%# Eval("Charges[0].ParentId") %>' />
                                <eShip:CustomHiddenField ID="hidGroupChargeType" runat="server" Value='<%# Eval("Charges[0].ChargeType").ToInt() %>' />
                                <div class="rowgroup ml10">
                                    <div class="row mb10">
                                        <h5> <%# Eval("Charges[0].ChargeType").FormattedString() %> <%# Eval("Charges[0].Number") %>
                                            <%#  Eval("Charges[0].ChargeType").ToInt().ToEnum<ChargeType>() == ChargeType.Shipment && ActiveUser.HasAccessTo(ViewCode.Shipment) 
                                                        ? string.Format("<a href='{0}?{1}={2}' class='blue ' target='_blank' title='Go To Shipment Record'><img src={3} width='16' class='middle' /></a>", 
                                                                                ResolveUrl(ShipmentView.PageAddress),
                                                                                WebApplicationConstants.TransferNumber, 
                                                                                Eval("Charges[0].Number"),
                                                                                ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                        : ActiveUser.HasAccessTo(ViewCode.ServiceTicket)
                                                            ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Service Ticket Record'><img src={3} width='20' class='middle' /></a>", 
                                                                                ResolveUrl(ServiceTicketView.PageAddress),
                                                                                WebApplicationConstants.TransferNumber, 
                                                                                Eval("Charges[0].Number"),
                                                                                ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                            : string.Empty %>
                                        </h5>
                                    </div>
                                    <div class="row">
                                        <div class="fieldgroup">
                                            <label class="wlabel">Vendor</label>
                                            <eShip:CustomTextBox ID="txtVendorName" runat="server" ReadOnly="True" CssClass="w260 disabled" Text='<%# Eval("Charges[0].VendorName") %>' /></td>
                                        </div>
                                        <div class="fieldgroup">
                                            <label class="wlabel">Document Number</label>
                                            <eShip:CustomTextBox ID="txtDocumentNumber" runat="server" ReadOnly="False" CssClass="w150" Text='' />
                                        </div>
                                        <div class="fieldgroup">
                                            <label class="wlabel">Document Date</label>
                                            <eShip:CustomTextBox ID="txtDocumentDate" runat="server" CssClass="w130 mr10" Type="Date" placeholder="99/99/9999" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="fieldgroup">
                                            <label class="wlabel">Invoice Document</label>
                                            <asp:FileUpload runat="server" ID="fupLocationPathInvoiceDocument" CssClass="jQueryUniform" />
                                        </div>
                                        <div class="fieldgroup">
                                            <label class="wlabel">POD Document</label>
                                            <asp:FileUpload runat="server" ID="fupLocationPathPODDocument" CssClass="jQueryUniform" />
                                        </div>
                                    </div>
                                </div>
                                <h5 class="ml10">Selected Charges</h5>
                                    <table class="stripe mb30">
                                        <tr>
                                            <th style="width: 25%">Charge Code</th>
                                            <th style="width: 30%">Description</th>
                                            <th style="width: 15%">Quantity</th>
                                            <th style="width: 15%">Unit Buy</th>
                                            <th style="width: 15%">Final Unit Buy</th>
                                        </tr>

                                        <asp:Repeater runat="server" ID="rptApproveCharges">
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <eShip:CustomHiddenField ID="hidChargeId" runat="server" Value='<%# Eval("Id") %>' />
                                                        <eShip:CustomHiddenField ID="hidChargeNumber" runat="server" Value='<%# Eval("Number") %>' />
                                                        <%# Eval("ChargeCode.Code") %>
                                                    </td>
                                                    <td>
                                                        <%# Eval("Comment") %>
                                                    </td>
                                                    <td>
                                                        <%# Eval("Quantity") %>
                                                    </td>
                                                    <td class="top">
                                                        <%# Eval("UnitBuy").ToDecimal().ToString("c2")%>
                                                    </td>
                                                    <td class="top">
                                                        <%# Eval("FinalBuy").ToDecimal().ToString("c2")%>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </table>
                                                <hr style="margin: 0px;" class="mb10" />

                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSave" />
                <asp:PostBackTrigger ControlID="btnCancel" />
                <asp:PostBackTrigger ControlID="ibtnClose" />
              </Triggers>
        </asp:UpdatePanel>
		
		 <asp:UpdatePanel ID="pnlDisputeCharges" runat="server" Visible="false">
            <ContentTemplate>
                <div class="popupControl popupControlOverW750">
                    <div class="popheader">
                        <h4>Dispute Charges
                        </h4>
                        <asp:ImageButton ID="ibtnCloseDispute" ImageUrl="~/images/icons2/close.png" CssClass="close"
                            CausesValidation="false" OnClick="OnDisputeCloseClicked" runat="server" />
                    </div>
                    <div class="row mt10 pb10">
                        <div class="fieldgroup right">
                            <asp:Button ID="btnSaveDispute" Text="Dispute Charges" OnClick="OnDisputeSaveClick" runat="server" CausesValidation="False" />
                        </div>
                        <div class="fieldgroup right">
                            <asp:Button ID="btnCancelDispute" Text="Cancel" OnClick="OnDisputeCloseClicked" runat="server" CausesValidation="false" />
                        </div>
                    </div>
                    <div style="overflow-y: auto; overflow-x:hidden; max-height: 600px">
                        <asp:Repeater runat="server" ID="rptGroupsChargesToDispute" OnItemDataBound="OnDisputeChargesItemDataBound">
                            <ItemTemplate>
                                <eShip:CustomHiddenField ID="hidParentId" runat="server" Value='<%# Eval("Charges[0].ParentId") %>' />
                                <eShip:CustomHiddenField ID="hidGroupChargeType" runat="server" Value='<%# Eval("Charges[0].ChargeType").ToInt() %>' />
                                <div class="rowgroup ml10">
                                    <div class="row mb10">
                                        <h5> <%# Eval("Charges[0].ChargeType").FormattedString() %> <%# Eval("Charges[0].Number") %>
                                            <%#  Eval("Charges[0].ChargeType").ToInt().ToEnum<ChargeType>() == ChargeType.Shipment && ActiveUser.HasAccessTo(ViewCode.Shipment) 
                                                        ? string.Format("<a href='{0}?{1}={2}' class='blue ' target='_blank' title='Go To Shipment Record'><img src={3} width='16' class='middle' /></a>", 
                                                                                ResolveUrl(ShipmentView.PageAddress),
                                                                                WebApplicationConstants.TransferNumber, 
                                                                                Eval("Charges[0].Number"),
                                                                                ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                        : ActiveUser.HasAccessTo(ViewCode.ServiceTicket)
                                                            ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Service Ticket Record'><img src={3} width='20' class='middle' /></a>", 
                                                                                ResolveUrl(ServiceTicketView.PageAddress),
                                                                                WebApplicationConstants.TransferNumber, 
                                                                                Eval("Charges[0].Number"),
                                                                                ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                            : string.Empty %>
                                        </h5>
                                    </div>
                                    <div class="row">
                                        <div class="fieldgroup">
                                            <label class="wlabel">Vendor</label>
                                            <eShip:CustomTextBox ID="txtVendorName" runat="server" ReadOnly="True" CssClass="w260 disabled" Text='<%# Eval("Charges[0].VendorName") %>' /></td>
                                        </div>
										
                                        <div class="fieldgroup pl40">
                                            <label class="wlabel">Dispute Comment</label>
											
                                            <eShip:CustomTextBox ID="txtDisputeComment" runat="server" ReadOnly="False" CssClass="w360" TextMode="MultiLine" />
	                                        <br/>
											<eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDisputeChargeDescription1" TargetControlId="txtDisputeComment" MaxLength="1000"/>
                                        </div>
                                     </div>
                                </div>
                                <h5 class="ml10">Selected Charges</h5>
                                    <table class="stripe mb30 pl10 pr10">
                                        <tr>
                                            <th style="width: 25%">Charge Code</th>
                                            <th style="width: 30%">Description</th>
                                            <th style="width: 15%">Quantity</th>
                                            <th style="width: 15%">Unit Buy</th>
                                            <th style="width: 15%">Final Unit Buy</th>
                                        </tr>

                                        <asp:Repeater runat="server" ID="rptDisputeCharges">
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <eShip:CustomHiddenField ID="hidChargeId" runat="server" Value='<%# Eval("Id") %>' />
                                                        <eShip:CustomHiddenField ID="hidChargeNumber" runat="server" Value='<%# Eval("Number") %>' />
                                                        <%# Eval("ChargeCode.Code") %>
                                                    </td>
                                                    <td>
                                                        <%# Eval("Comment") %>
                                                    </td>
                                                    <td>
                                                        <%# Eval("Quantity") %>
                                                    </td>
                                                    <td class="top">
                                                        <%# Eval("UnitBuy").ToDecimal().ToString("c2")%>
                                                    </td>
                                                    <td class="top">
                                                        <%# Eval("FinalBuy").ToDecimal().ToString("c2")%>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </table>
                                                <hr style="margin: 0px;" class="mb10" />

                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSaveDispute" />
                <asp:PostBackTrigger ControlID="btnCancelDispute" />
                <asp:PostBackTrigger ControlID="ibtnCloseDispute" />
            </Triggers>
        </asp:UpdatePanel>


        <script type="text/javascript">
            $(document).ready(function () {
                $("a[id$='lnkShipmentDocumentLocationPath']").each(function () {
                    jsHelper.SetDoPostBackJsLink($(this).prop('id'));
                });
            });
        </script>

    </div>
</asp:Content>
