﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="MiscReceiptDashboardView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.MiscReceiptDashboardView" EnableEventValidation="false" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Administration" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Operations" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>
<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowMore="true" OnCommand="OnCommand"/>
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:UpdatePanel runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon" CssClass="pageHeaderIcon" />
                        Misc Receipt Dashboard<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" /></small>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </h3>
        </div>

        <hr class="dark mb10" />

        <eShip:ShipmentFinderControl runat="server" ID="shipmentFinder" Visible="false" EnableMultiSelection="false"
            OnItemSelected="OnShipmentFinderItemSelected" OnSelectionCancel="OnShipmentFinderItemCancelled" OpenForEditEnabled="false" />
        <eShip:PaymentEntryControl runat="server" ID="pecPayment" Visible="False" CanModifyPaymentAmount="True" OnClose="OnPaymentEntryCloseClicked" OnProcessingMessages="OnPaymentEntryProcessingMessages" 
            OnPaymentSuccessfullyProcessed="OnPaymentEntryPaymentSuccessfullyProcessed"/>

        <asp:Panel runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked" CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="MiscReceipts" ShowAutoRefresh="True" OnProcessingError="OnSearchProfilesProcessingError"
                            OnSearchProfileSelected="OnSearchProfilesProfileSelected" OnAutoRefreshChanged="OnSearchProfilesAutoRefreshChanged" />
                    </div>
                </div>
            </div>
            <hr class="dark mb10" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder" OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>' OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>
            <hr class="fat" />
            <div class="row mb10">
                <div class="fieldgroup">
                    <asp:Button ID="btnSearch" CssClass="left" runat="server" Text="SEARCH" OnClick="OnSearchClicked" CausesValidation="False" />
                    <asp:Button ID="btnExport" CssClass="left ml10" runat="server" Text="EXPORT" OnClick="OnExportClicked" CausesValidation="False" />
                </div>
                <div class="fieldgroup right">
                    <label>Sort By:</label>
                    <asp:DropDownList runat="server" ID="ddlSortBy" DataTextField="DisplayName" DataValueField="Name" OnSelectedIndexChanged="OnSortBySelectedIndexChanged" AutoPostBack="True" CssClass="mr10" />
                    <asp:RadioButton runat="server" ID="rbAsc" Text="ASC" GroupName="rbSortBy" AutoPostBack="True" ToolTip="Ascending" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />&nbsp;
                    <asp:RadioButton runat="server" ID="rbDesc" Text="DESC" GroupName="rbSortBy" Checked="True" ToolTip="Descending" AutoPostBack="True" OnCheckedChanged="OnSortOrderCheckChanged" CssClass="jQueryUniform" />
                </div>
            </div>
        </asp:Panel>

        <asp:UpdatePanel runat="server" ID="upDataUpdate">
            <ContentTemplate>
                <eShip:UpdatePanelContinuousScrollExtender ID="upcseDataUpdate" runat="server" BindableControlId="rptMiscReceipts" UpdatePanelToExtendId="upDataUpdate" />
                <eShip:TableFreezeHeaderExtender runat="server" ID="tfheMiscReceiptDashboardTable" TableId="miscReceiptDashboardTable" HeaderZIndex="2" />
                <asp:Timer runat="server" ID="tmRefresh" Interval="30000" OnTick="OnTimerTick" />
                <div class="rowgroup">
                    <table id="miscReceiptDashboardTable" class="line2 pl2">
                        <tr>
                            <th style="width: 10%;">
                                <asp:LinkButton runat="server" ID="lbtnSortShipmentNumber" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData" Text='<abbr title="Shipment Number">Shipment #</abbr>' />
                            </th>
                            <th style="width: 12%">
                                <asp:LinkButton runat="server" ID="lbtnSortLoadOrderNumber" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData" Text='<abbr title="Load Order Number">Load Order #</abbr>' />
                            </th>
                            <th style="width: 10%;" class="text-right">
                                <asp:LinkButton runat="server" ID="lbtnSortAmountPaid" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData" Text="Amount Paid" />
                            </th>
                            <th style="width: 20%;">
                                <asp:LinkButton runat="server" ID="lbtnSortPaymentDate" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData" Text="Payment Date" />
                            </th>
                            <th style="width: 18%;">
                                <asp:LinkButton runat="server" ID="lbtnSortGatewayTransactionId" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData" Text="Gateway Transaction Id" />
                            </th>
                            <th style="width: 12%;">
                                <asp:LinkButton runat="server" ID="lbtnSortPaymentGatewayType" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData" Text="Payment Gateway Type" />
                            </th>
                            <th style="width: 10%;" class="text-center">
                                <asp:LinkButton runat="server" ID="lbtnSortReversal" CssClass="link_nounderline blue" CausesValidation="False" OnCommand="OnSortData" Text="Reversal" />
                            </th>
                            <th style="width: 8%">Actions
                            </th>
                        </tr>
                        <asp:Repeater runat="server" ID="rptMiscReceipts" OnItemDataBound="OnMiscReceiptsItemDataBound">
                            <ItemTemplate>
                                <tr>
                                    <td rowspan='<%# Eval("MiscReceiptApplicationsPresent").ToBoolean() ? 2 : 1 %>' class="top">
                                        <eShip:CustomHiddenField runat="server" Id="hidMiscReceiptId" Value='<%# Eval("Id") %>'/>
                                        <%# Eval("ShipmentNumber") %>
                                        <%# ActiveUser.HasAccessTo(ViewCode.Shipment) && Eval("ShipmentId").ToLong() != default(long)
                                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Shipment Record'><img src='{3}' alt='' width='16' align='absmiddle'/></a>", 
                                                    ResolveUrl(ShipmentView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    Eval("ShipmentNumber").GetString(),
                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                : string.Empty  %>
                                    </td>
                                    <td class="top">
                                        <%# Eval("LoadOrderNumber") %>
                                        <%# ActiveUser.HasAccessTo(ViewCode.LoadOrder)  && Eval("LoadOrderId").ToLong() != default(long)
                                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Load Order Record'><img src='{3}' alt='' width='16' align='absmiddle'/></a>", 
                                                    ResolveUrl(LoadOrderView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    Eval("LoadOrderNumber").GetString(),
                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                : string.Empty  %>
                                    </td>
                                    <td class="top text-right">
                                        <%# Eval("AmountPaid").ToDecimal().ToString("c2") %>
                                    </td>
                                    <td class="top">
                                        <%# Eval("PaymentDate").FormattedLongDate() %>
                                    </td>
                                    <td class="top">
                                        <%# Eval("GatewayTransactionId") %>
                                    </td>
                                    <td class="top">
                                        <%# Eval("PaymentGatewayType") %>
                                    </td>
                                    <td class="top text-center">
                                        <%# Eval("Reversal").ToBoolean() ? WebApplicationConstants.HtmlCheck : string.Empty %>
                                    </td>
                                    <td rowspan='<%# Eval("MiscReceiptApplicationsPresent").ToBoolean() ? 2 : 1 %>' class="text-center top">
                                        <div class="row">
                                            <asp:HyperLink runat="server" ID="hypApplyReceiptToInvoices" Visible='<%# !Eval("Reversal").ToBoolean() && ActiveUser.HasAccessTo(ViewCode.MiscReceiptApplication) %>'
                                                Target="_blank" NavigateUrl='<%# string.Format("{0}?{1}={2}", MiscReceiptApplicationView.PageAddress, WebApplicationConstants.TransferNumber, Eval("Id").ToString().UrlTextEncrypt()) %>'>
                                                <asp:Image runat="server" ID="imgApplyReceiptToInvoices" ImageUrl="~/images/icons2/arrow_newTab.png"
                                                    ToolTip="Apply Receipt To Invoices" AlternateText="Apply Receipt To Invoices" Width="20px" CssClass="mb5" />
                                            </asp:HyperLink>
                                        </div>
                                        <asp:ImageButton runat="server" ID="ibtnRefundReceipt" ImageUrl="~/images/icons2/document.png"
                                            ToolTip="Void/Refund Receipt" OnClick="OnVoidOrCreateRefundForReceipt" Visible='<%# Access.Modify && !Eval("Reversal").ToBoolean() %>'/>
                                    </td>
                                </tr>
                                <tr class="hidden">
                                    <td>&nbsp;</td>
                                </tr>
                                <tr class="f9 <%# Eval("MiscReceiptApplicationsPresent").ToBoolean() ? string.Empty : "hidden" %>">
                                    <td colspan="6" class="top forceLeftBorder">
                                        <div class="rowgroup mb20">
                                            <div class="col_1_2">
                                                 <div class="row">
                                                     <div class="fieldgroup">
                                                         <label class="wlabel blue">User</label>
                                                         <%# Eval("UserUsername").GetString() %>
                                                         <%# ActiveUser.HasAccessTo(ViewCode.User) 
                                                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To User Record'><img src='{3}' alt='' width='16' align='absmiddle'/></a>", 
                                                                    ResolveUrl(UserView.PageAddress),
                                                                    WebApplicationConstants.TransferNumber, 
                                                                    Eval("UserId").GetString().UrlTextEncrypt(),
                                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                                : string.Empty  %>
                                                     </div>
                                                 </div>
                                            </div>
                                            <div class="col_1_2">
                                                <div class="row">
                                                     <div class="fieldgroup">
                                                         <label class="wlabel blue">Customer</label>
                                                         <%# string.Format("{0} - {1}", Eval("CustomerNumber"), Eval("CustomerName")) %>
                                                         <%# ActiveUser.HasAccessTo(ViewCode.Customer) 
                                                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Customer Record'><img src='{3}' alt='' width='16' align='absmiddle'/></a>", 
                                                                    ResolveUrl(CustomerView.PageAddress),
                                                                    WebApplicationConstants.TransferNumber, 
                                                                    Eval("CustomerId").GetString().UrlTextEncrypt(),
                                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                                : string.Empty  %>
                                                     </div>
                                                 </div>
                                            </div>
                                        </div>
                                        <asp:Repeater runat="server" ID="rptMiscReceiptApplications">
                                            <HeaderTemplate>
                                                <div class="rowgroup mb10">
                                                    <div class="row">
                                                        <h6 class="mt0 mb0">Misc Receipt Applications</h6>
                                                        <table class="contain pl2">
                                                            <tr class="bbb1">
                                                                <th style="width: 40%;" class="no-top-border">Invoice
                                                                </th>
                                                                <th style="width: 40%;" class="no-top-border no-left-border">Application Date
                                                                </th>
                                                                <th style="width: 20%;" class="no-top-border no-left-border text-right">Amount
                                                                </th>
                                                            </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr class="no-bottom-border">
                                                    <td>
                                                        <%# Eval("Invoice.InvoiceNumber") %>
                                                        <%# ActiveUser.HasAccessTo(ViewCode.Invoice) 
                                                            ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Invoice Record'><img src='{3}' alt='' width='16' align='absmiddle'/></a>", 
                                                                ResolveUrl(InvoiceView.PageAddress),
                                                                WebApplicationConstants.TransferNumber, 
                                                                Eval("Invoice.InvoiceNumber").GetString(),
                                                                ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                            : string.Empty  %>
                                                    </td>
                                                    <td>
                                                        <%# Eval("ApplicationDate") %>
                                                    </td>
                                                    <td class="text-right">
                                                        <%# Eval("Amount").ToDecimal().ToString("c4") %>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                                    </div>
                                                </div>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="lbtnSortShipmentNumber" />
                <asp:PostBackTrigger ControlID="lbtnSortLoadOrderNumber" />
                <asp:PostBackTrigger ControlID="lbtnSortAmountPaid" />
                <asp:PostBackTrigger ControlID="lbtnSortPaymentDate" />
                <asp:PostBackTrigger ControlID="lbtnSortGatewayTransactionId" />
                <asp:PostBackTrigger ControlID="lbtnSortPaymentGatewayType" />
                <asp:PostBackTrigger ControlID="lbtnSortReversal" />
                <asp:PostBackTrigger ControlID="rptMiscReceipts" />
            </Triggers>
        </asp:UpdatePanel>
        <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
        <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" HandleButtonsClientSide="Ok" Button="Ok" Icon="Information" OnYes="OnMessageBoxYes" OnNo="OnMessageBoxNo"/>
        <asp:Panel ID="pnlDimScreen" CssClass="dimBackground" runat="server" Visible="false" />
        
        <asp:Panel runat="server" ID="pnlMiscReceiptRefund" CssClass="popup popupControlOverW500" Visible="False">
            <div class="popheader">
                <h4>Enter Amount To Be Refunded For Receipt</h4>
                <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                    CausesValidation="false" OnClick="OnCloseRefundClicked" runat="server" />
            </div>
            <table class="poptable">
                <tr>
                    <td colspan="2">
                        <asp:RangeValidator runat="server" ID="rvRefundAmount" ControlToValidate="txtRefundAmount" Type="Double" MinimumValue="0" MaximumValue="0"
                            ErrorMessage="Refund amount may not exceed the difference of the receipt's amount paid and the total amount already refunded for the receipt"
                            Display="Dynamic"/>
                    </td>
                </tr>
                <tr>
                    <td class="text-right">
                        <label class="upper">Amount:</label>
                    </td>
                    <td>
                        <eShip:CustomTextBox runat="server" ID="txtRefundAmount" CssClass="w100" Type="FloatingPointNumbers"/>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button runat="server" ID="btnRefund" CausesValidation="True" Text="Refund" OnClick="OnRefundMiscReceiptClicked"/>
                        <asp:Button runat="server" ID="btnCancelRefund" CausesValidation="False" Text="Cancel" OnClick="OnCloseRefundClicked"/>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    <eShip:CustomHiddenField runat="server" ID="hidReceiptToBeRefundedOrVoidedId" />
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
</asp:Content>
