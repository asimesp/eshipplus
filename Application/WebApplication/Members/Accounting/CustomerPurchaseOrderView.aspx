﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Members/Member.master" AutoEventWireup="true" CodeBehind="CustomerPurchaseOrderView.aspx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.CustomerPurchaseOrderView" %>

<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core.Registry" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Accounting" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>

<asp:Content ID="Toolbar" ContentPlaceHolderID="MainToolbar" runat="server">
    <eShip:MainToolBar ID="memberToolBar" runat="server" ShowDashboard="true" ShowNew="true"
        OnNew="OnNewClick" OnImport="OnImportClicked" />
</asp:Content>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <div class="clearfix">
        <div class="pageHeader">
            <h3>
                <asp:Image runat="server" ID="imgPageImageLogo" AlternateText="page icon"
                    CssClass="pageHeaderIcon" />
                Customer Purchase Order<small class="ml10"><asp:Literal runat="server" ID="litRecordCount" Text="" /></small>
            </h3>
        </div>

        <hr class="dark mb10" />

        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
            <div class="rowgroup mb10">
                <div class="row mb10 pb10 bottom_shadow">
                    <div class="fieldgroup">
                        <label class="upper blue">Customer: </label>
                        <eShip:CustomHiddenField runat="server" ID="hidCustomerId" />
                        <eShip:CustomTextBox runat="server" ID="txtCustomerNumber" CssClass="w110" OnTextChanged="OnCustomerNumberEntered"
                            AutoPostBack="true" />
                        <asp:RequiredFieldValidator runat="server" ID="rfvCustomerNumber" ControlToValidate="txtCustomerNumber"
                            ErrorMessage="all fields marked with an ' * ' are required" Text="*" Display="Dynamic" />
                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/icons2/search_dark.png"
                            CausesValidation="false" OnClick="OnCustomerSearchClicked" />
                        <eShip:CustomTextBox runat="server" ID="txtCustomerName" CssClass="w280 disabled" ReadOnly="true" />
                        <ajax:AutoCompleteExtender runat="server" ID="aceCustomer" TargetControlID="txtCustomerNumber" ServiceMethod="GetCustomerList" ServicePath="~/Services/eShipPlusWSv4P.asmx"
                            EnableCaching="True" UseContextKey="True" CompletionListCssClass="AutoCompleteListDisplay" />
                        <%= hidCustomerId.Value.ToLong() != default(long) && ActiveUser.HasAccessTo(ViewCode.Customer) 
                            ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To Customer Record'><img src={3} width='20' class='middle' /></a>", 
                                                    ResolveUrl(CustomerView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    hidCustomerId.Value.UrlTextEncrypt(),
                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
                            : string.Empty %>
                    </div>
                </div>
                <div class="row">
                    <div class="fieldgroup">
                        <asp:LinkButton ID="lnkAddParameter" runat="server" Text="Add Parameter(s)" OnClick="OnParameterAddClicked"
                            CausesValidation="False" CssClass="add" />
                    </div>
                    <div class="right">
                        <eShip:UserSearchProfileControl ID="searchProfiles" runat="server" Type="CustomerPurchaseOrders" ShowAutoRefresh="False"
                            OnProcessingError="OnSearchProfilesProcessingError" OnSearchProfileSelected="OnSearchProfilesProfileSelected" />
                    </div>
                </div>
            </div>

            <hr class="dark mb10" />
            <asp:ListView runat="server" ID="lstFilterParameters" ItemPlaceholderID="itemPlaceHolder"
                OnItemDataBound="OnParameterItemDataBound">
                <LayoutTemplate>
                    <table class="mb10">
                        <asp:PlaceHolder runat="server" ID="itemPlaceHolder" />
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <eShip:ReportRunParameterControl ID="reportRunParameterControl" runat="server" ItemIndex='<%# Container.DataItemIndex %>'
                        OnRemoveParameter="OnParameterRemove" />
                </ItemTemplate>
            </asp:ListView>

            <hr class="fat" />
            <div class="rowgroup">
                <div class="row mb10">
                    <div class="left">
                        <asp:Button ID="btnSearch" CssClass="mr10" runat="server" Text="SEARCH" OnClick="OnSearchClicked"
                            CausesValidation="False" />
                        <asp:Button ID="btnExport" runat="server" Text="EXPORT" OnClick="OnExportClicked" />
                    </div>
                </div>
            </div>
        </asp:Panel>

        <eShip:TableFreezeHeaderExtender runat="server" ID="tfheCustomerPurchaseOrderTable" TableId="customerPurchaseOrderTable" HeaderZIndex="2"/>
        <div class="rowgroup">
            <table class="line2 pl2" id="customerPurchaseOrderTable">
                <tr>
                    <th style="width: 14%;">PO Number
                    </th>
                    <th style="width: 17%;">Expiration
                    </th>
                    <th style="width: 20%;">Postal Code
                    </th>
                    <th style="width: 8%;" class="text-center">Maximum Uses
                    </th>
                    <th style="width: 10%;" class="text-center">Current Uses
                    </th>
                    <th style="width: 10%;" class="text-center">Apply On Origin
                    </th>
                    <th style="width: 10%;" class="text-center">Apply on Destination
                    </th>
                    <th style="width: 10%;">Action
                    </th>
                </tr>
                <asp:ListView ID="lvwRegistry" runat="server" ItemPlaceholderID="itemPlaceHolder">
                    <LayoutTemplate>
                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td rowspan="2" class="top">
                                <eShip:CustomHiddenField ID="customerPurchaseOrderId" runat="server" Value='<%# Eval("Id") %>' />
                                <%# Eval("PurchaseOrderNumber") %>
                            </td>
                            <td>
                                <%# Eval("ExpirationDate").FormattedShortDate()%>
                            </td>
                            <td>
                                <%# Eval("ValidPostalCode") %>
                                (<%# new Country(Eval("CountryId").ToLong()).Name %>)
                            </td>
                            <td class="text-center">
                                <%# Eval("MaximumUses") %>
                            </td>
                            <td class="text-center">
                                <%# Eval("CurrentUses")%>
                            </td>
                            <td class="text-center">
                                <%# Eval("ApplyOnOrigin").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                                <eShip:CustomHiddenField runat="server" ID="hidOrigin" Value='<%# Eval("ApplyOnOrigin").ToBoolean().GetString() %>' />
                            </td>
                            <td class="text-center">
                                <%# Eval("ApplyOnDestination").ToBoolean() ? WebApplicationConstants.HtmlCheck  : string.Empty %>
                                <eShip:CustomHiddenField runat="server" ID="hidDestination" Value='<%# Eval("ApplyOnDestination").ToBoolean().GetString() %>' />
                            </td>
                            <td rowspan="2" class="top">
                                <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png"
                                    CausesValidation="false" OnClick="OnEditClicked" Enabled='<%# Access.Modify %>' />
                                <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png"
                                    CausesValidation="false" OnClick="OnDeleteClicked" Enabled='<%# Access.Remove %>' />
                                <ajax:ConfirmButtonExtender runat="server" ID="cbeDelete" TargetControlID="ibtnDelete"
                                    ConfirmText="Are you sure you want to delete record?" />
                            </td>
                        </tr>
                        <tr class="f9">
                            <td colspan="6" class="forceLeftBorder">
                                <div class="fieldgroup">
                                    <span class="blue mr10"><b>Additional Notes: </b></span><%# Eval("AdditionalPurchaseOrderNotes") %>
                                </div>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </table>
        </div>
        <asp:Panel ID="pnlEdit" runat="server" Visible="false">
            <div class="popupControl popupControlOverW500">
                <div class="popheader">
                    <h4>
                        <asp:Literal ID="litTitle" runat="server" />
                    </h4>
                    <asp:ImageButton ID="ibtnClose" ImageUrl="~/images/icons2/close.png" CssClass="close"
                        CausesValidation="false" OnClick="OnCloseClicked" runat="server" />
                </div>
                <div class="row">
                    <table class="poptable">
                        <tr>
                            <td colspan="20" class="red">
                                <asp:Literal ID="litMessage" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Purchase Order Number:</label>
                            </td>
                            <td>
                                <eShip:CustomHiddenField ID="hidCustomerPurchaseOrderId" runat="server" />
                                <eShip:CustomTextBox ID="txtPoNumber" CssClass="w200" MaxLength="50" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Postal Code:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtPostalCode" CssClass="w200" MaxLength="10" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Country:</label>
                            </td>
                            <td>
                                <eShip:CachedObjectDropDownList runat="server" ID ="ddlCountries" CssClass="w200" Type="Countries" EnableChooseOne="True" DefaultValue="0"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Maximum Uses:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtMaxUses" CssClass="w100" MaxLength="10" runat="server" />
                                <ajax:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtMaxUses"
                                    FilterType="Numbers" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Current Uses:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtCurrentUses" CssClass="w100 disabled" MaxLength="10" runat="server" ReadOnly="true" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Expiration Date:</label>
                            </td>
                            <td>
                                <eShip:CustomTextBox runat="server" ID="txtExpirationDate"  CssClass="w100" Type="Date" placeholder="99/99/9999" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right top">
                                <label class="upper">Additional Notes:</label>
                                <br />
                                <label class="lhInherit">
                                    <eShip:TextAreaMaxLengthExtender runat="server" ID="tamleDescription" TargetControlId="txtNotes" MaxLength="50" />
                                </label>
                            </td>
                            <td>
                                <eShip:CustomTextBox ID="txtNotes" runat="server" TextMode="MultiLine" Rows="3" CssClass="w200" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text-right">
                                <label class="upper">Apply On:</label>
                            </td>
                            <td id="tdApplyCheckboxes">
                                <script type="text/javascript">
                                    $(document).ready(function () {
                                        $(jsHelper.AddHashTag('tdApplyCheckboxes input:checkbox')).click(function () {
                                            if (jsHelper.IsChecked($(this).attr('id'))) {
                                                $(jsHelper.AddHashTag('tdApplyCheckboxes input:checkbox')).not(jsHelper.AddHashTag($(this).attr('id'))).each(function () {
                                                    jsHelper.UnCheckBox($(this).attr('id'));
                                                });
                                                $.uniform.update();
                                            }
                                        });
                                    });
                                </script>
                                <asp:CheckBox ID="chkApplyOnOrigin" runat="server" Enabled="true" CssClass="jQueryUniform" />
                                <label class="active mr10">Origin</label>
                                <asp:CheckBox ID="chkApplyOnDestination" runat="server" Enabled="true" CssClass="jQueryUniform" />
                                <label class="active">Destination</label>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="pt10">
                                <asp:Button ID="btnSave" Text="Save" OnClick="OnSaveClick" runat="server" />
                                <asp:Button ID="btnCancel" Text="Cancel" OnClick="OnCloseClicked" runat="server"
                                    CausesValidation="false" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>
    <eShip:ParameterSelectorControl ID="parameterSelector" runat="server" OnAdd="OnParameterSelectorAdd" OnClose="OnParameterSelectorClose" />
    <eShip:MessageBoxControl runat="server" ID="messageBox" Visible="false" Button="Ok" Icon="Information"
        OnOkay="OnOkayProcess" />
    <eShip:FileUploaderControl runat="server" ID="fileUploader" Visible="false" OnFileUploadCancelled="OnFileUploaderUploadCancelled"
        OnFileUploaded="OnFileUploaderFileUploaded" OnFileUploadError="OnFileUploadedError" />
    <eShip:CustomerFinderControl ID="customerFinder" runat="server" OnItemSelected="OnCustomerFinderItemSelected"
        OnSelectionCancel="OnCustomerFinderSelectionCancelled" Visible="false" OpenForEditEnabled="false" />
    <asp:Panel ID="pnlDimScreen" CssClass="dimBackgroundControl" runat="server" Visible="false" />
    <eShip:CustomHiddenField runat="server" ID="hidFlag" />
</asp:Content>
