﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.Administration;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls
{
    public partial class VendorFinderControl : MemberControlBase, IVendorFinderView
    {
        public new bool Visible
        {
            get
            {
	            return pnlVendorFinderContent.Visible;
            }
            set
            {
                base.Visible = true;
                pnlVendorFinderContent.Visible = value;
                pnlVendorFinderDimScreen.Visible = value;
            }
        }

        public bool OpenForEdit
        {
            get { return hidEditSelected.Value.ToBoolean(); }
        }

        public bool OpenForEditEnabled
        {
            get { return hidOpenForEditEnabled.Value.ToBoolean(); }
            set { hidOpenForEditEnabled.Value = value.ToString(); }
        }

        public bool EnableMultiSelection
        {
            get { return hidVendorFinderEnableMultiSelection.Value.ToBoolean(); }
            set
            {
                hidVendorFinderEnableMultiSelection.Value = value.ToString();
                btnSelectAll.Visible = value;
                btnSelectAll2.Visible = value;
                upcseDataUpdate.PreserveRecordSelection = value;
            }
        }

        public bool OnlyActiveResults
        {
            get { return hidOnlyActiveResults.Value.ToBoolean(); }
            set { hidOnlyActiveResults.Value = value.ToString(); }
        }

        public event EventHandler<ViewEventArgs<List<ParameterColumn>>> Search;
        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<Vendor>> ItemSelected;
        public event EventHandler<ViewEventArgs<List<Vendor>>> MultiItemSelected;
        public event EventHandler SelectionCancel;

        public void DisplaySearchResult(List<Vendor> vendors)
        {
            litRecordCount.Text = vendors.BuildRecordCount();

            upcseDataUpdate.DataSource = vendors;
            upcseDataUpdate.DataBind();
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }


        public void Reset()
        {
            hidAreFiltersShowing.Value = true.ToString();
            DisplaySearchResult(new List<Vendor>());
        }


        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems { Columns = GetCurrentRunParameters(true) };
        }

        private void DoSearchPreProcessingThenSearch()
        {
            var columns = GetCurrentRunParameters(false);

            // filter for non posted only if necessary
            if (OnlyActiveResults)
            {
                var activeParameter = AccountingSearchFields.Active.ToParameterColumn();
                activeParameter.DefaultValue = true.ToString();
                activeParameter.Operator = Operator.Equal;
                columns.Add(activeParameter);
            }

            DoSearch(columns);
        }

        private void DoSearch(List<ParameterColumn> columns)
        {
            if (Search != null)
                Search(this, new ViewEventArgs<List<ParameterColumn>>(columns));

            var control = lstSearchResults.FindControl("chkSelectAllRecords").ToAltUniformCheckBox();
            if (control != null)
            {
                control.Checked = false;
                control.Visible = EnableMultiSelection;
            }
        }


        private void ProcessItemSelection(long vendorId)
        {
            if (ItemSelected != null)
                ItemSelected(this, new ViewEventArgs<Vendor>(new Vendor(vendorId, false)));
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new VendorFinderHandler(this).Initialize();

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            hypGoToUserProfile.NavigateUrl = UserProfileView.PageAddress;
            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);

            var columns = profile != null && profile.Columns != null
                            ? profile.Columns
                            : AccountingSearchFields.DefaultVendors.Select(f => f.ToParameterColumn()).ToList();

            // filter out active column if necesary
            if (OnlyActiveResults)
            {
                var activeColumns = new List<ParameterColumn>();

                activeColumns.AddRange(columns.Where(c => c.ReportColumnName == AccountingSearchFields.Active.ToParameterColumn().ReportColumnName));

                foreach (var column in activeColumns)
                    columns.Remove(column);
            }

            lstFilterParameters.DataSource = columns;
            lstFilterParameters.DataBind();

            if (Loading != null)
                Loading(this, new EventArgs());
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            hidAreFiltersShowing.Value = ActiveUser.AlwaysShowFinderParametersOnSearch.ToString();
            upcseDataUpdate.ResetLoadCount();
            DoSearchPreProcessingThenSearch();
        }

        protected void OnSelectClicked(object sender, EventArgs e)
        {
            var button = (Button)sender;
            hidEditSelected.Value = false.ToString();
            ProcessItemSelection(button.Parent.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong());
        }

        protected void OnEditSelectClicked(object sender, EventArgs e)
        {
            var button = (Button)sender;
            hidEditSelected.Value = true.ToString();
            ProcessItemSelection(button.Parent.FindControl("hidVendorId").ToCustomHiddenField().Value.ToLong());
        }

        protected void OnCancelClicked(object sender, EventArgs eventArgs)
        {
            if (SelectionCancel != null)
                SelectionCancel(this, new EventArgs());
        }

        protected void OnSelectAllClicked(object sender, EventArgs e)
        {
            var vendors = (from item in lstSearchResults.Items
                           let hidden = item.FindControl("hidVendorId").ToCustomHiddenField()
                           let checkBox = item.FindControl("chkSelected").ToAltUniformCheckBox()
                           where checkBox != null && checkBox.Checked
                           select new Vendor(hidden.Value.ToLong()))
                .Where(u => u.KeyLoaded)
                .ToList();

            if (MultiItemSelected != null)
                MultiItemSelected(this, new ViewEventArgs<List<Vendor>>(vendors));
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = AccountingSearchFields.Vendors.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

            Reset();
        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            var columnsToBind = OnlyActiveResults
                                    ? AccountingSearchFields.Vendors.Where(
                                        s => s.Name != AccountingSearchFields.Active.Name)
                                    : AccountingSearchFields.Vendors;

            parameterSelector.LoadParameterSelector(columnsToBind.ToList());
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;

            Reset();
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();
        }
    }
}