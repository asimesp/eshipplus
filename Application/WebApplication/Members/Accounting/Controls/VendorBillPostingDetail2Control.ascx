﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VendorBillPostingDetail2Control.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls.VendorBillPostingDetail2Control" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>
<%@ Register TagPrefix="pc" TagName="AltUniformCheckBox" Src="~/Members/Controls/AltUniformCheckBox.ascx" %>
<eShip:CustomHiddenField runat="server" ID="hidItemIndex" />
<eShip:CustomHiddenField ID="hidVendorBillId" runat="server" />
<tr>
    <td rowspan="2" class="text-center top" <%# EnableMultiSelect ? string.Empty : "style='display:none;'" %>>
        <pc:AltUniformCheckBox runat="server" ID="chkSelection" Visible='<%# EnableMultiSelect %>' OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'vendorBillTable', 'chkSelectAllRecords');" />
    </td>
    <td rowspan="2" class="top no-left-border text-left">
        <asp:Literal runat="server" ID="litDocumentNumber" />
    </td>
    <td>
        <asp:Literal runat="server" ID="litDateCreated" /></td>
    <td>
        <asp:Literal runat="server" ID="litDocumentDate" /></td>
    <td>
        <asp:Literal runat="server" ID="litUser" />
        <asp:HyperLink ID="hypUser" Target="_blank" runat="server" ToolTip="Go To User Record" Visible="False">
                <img src='<%= ResolveUrl("~/images/icons2/arrow_newTab.png") %>' alt="" align="absmiddle" width="16"/>
        </asp:HyperLink>
    </td>
    <td>
        <asp:Literal runat="server" ID="litType" /></td>
    <td class="text-right">
        <asp:Literal runat="server" ID="litAmountDue" /></td>
    <td rowspan="2" class="top text-center">
        <asp:HyperLink runat="server" ID="hypVendorBillView" Target="_blank">
            <img src='<%# ResolveUrl("~/images/icons2/arrow_newTab.png") %>' alt=""/>
        </asp:HyperLink>
    </td>
</tr>
<tr class="f9">
    <td colspan="5" class="forceLeftBorder">
        <div class="rowgroup">
            <div class="col_1_3 no-right-border">
                <div class="row">
                    <label class="wlabel blue">
                        Billing
                    </label>
                    <label>
                        <asp:Literal runat="server" ID="litVendor" />
                        <asp:HyperLink ID="hypVendor" runat="server" Target="_blank" Visible='<%# ActiveUser.HasAccessTo(ViewCode.Vendor) %>' ToolTip="Go To Vendor Record">
                            <img src='<%# ResolveUrl("~/images/icons2/arrow_newTab.png") %>' alt="" width="16"/>
                        </asp:HyperLink>
                    </label>
                </div>
                <div class="row">
                    <label>
                        <asp:Literal runat="server" ID="litAddress" /></label>
                </div>
            </div>
            <div class="col_1_3 no-right-border">
                <div class="row">
                    <label class="wlabel blue">Shipments</label>
                    <label>
                        <asp:Literal runat="server" ID="litShipments" /></label>
                </div>
            </div>
            <div class="col_1_3">
                <div class="row">
                    <label class="wlabel blue">Service Tickets</label>
                    <label>
                        <asp:Literal runat="server" ID="litServiceTickets" /></label>
                </div>
            </div>
        </div>
    </td>
</tr>
