﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentRefundOrVoidControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls.PaymentRefundOrVoidControl" %>
<%@ Import Namespace="LogisticsPlus.Eship.Core" %>
<%@ Import Namespace="LogisticsPlus.Eship.Processor" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Members.Administration" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.Utilities" %>
<%@ Import Namespace="LogisticsPlus.Eship.WebApplication.ViewCodes" %>
<asp:Panel runat="server" ID="pnlPaymentRefund" CssClass="popupControl" Visible="False">
        <div class="popheader">
            <h4>Refund Or Void Payments For <asp:Literal runat="server" ID="litHeaderShipmentOrLoadOrderNumber"/></h4>
        <asp:ImageButton runat="server" ID="ibtnClosePaymentRefund" CausesValidation="False" CssClass="close" ImageUrl="~/images/icons2/close.png" OnClick="OnClosePaymentRefundClicked" />
    </div>
    <table class="stripe sm_chk" id="miscReceiptsToRefundTable">
        <tr>
            <th style="width: 3%">
                <eShip:AltUniformCheckBox ID="chkSelectAllRecords" runat="server" OnClientSideClicked="javascript:SetSelectedCheckBoxesStatus(this, 'miscReceiptsToRefundTable');" />
            </th>
            <th style="width: 15%;">User
            </th>
            <th style="width: 11%;" class="text-right">Refundable Amount
            </th>
            <th style="width: 20%;">Payment Date
            </th>
            <th style="width: 10%;">Gateway Transaction Id
            </th>
            <th style="width: 10%;">Payment Gateway Type
            </th>
            <th style="width: 23%">Status
            </th>
            <th style="width: 8%" class="text-right">Amount To Refund
            </th>
        </tr>
        <asp:Repeater runat="server" ID="rptMiscReceipts">
            <ItemTemplate>
                <tr>
                    <td>
                        <eShip:AltUniformCheckBox runat="server" ID="chkSelected" OnClientSideClicked="SetCheckAllCheckBoxUnChecked(this, 'miscReceiptsToRefundTable', 'chkSelectAllRecords');" Visible='<%# Eval("Status").ToString() != CannotVoidOrRefund %>' />
                    </td>
                    <td>
                         <eShip:CustomHiddenField runat="server" ID="hidShipmentId" Value='<%# Eval("ShipmentId") %>' />
                        <eShip:CustomHiddenField runat="server" ID="hidLoadOrderId" Value='<%# Eval("LoadOrderId") %>' />
                        <eShip:CustomHiddenField runat="server" ID="hidMiscReceiptId" Value='<%# Eval("Id") %>' />
                        <%# Eval("Username").GetString() %>
                        <%# ActiveUser.HasAccessTo(ViewCode.User) 
                                                ? string.Format("<a href='{0}?{1}={2}' class='blue' target='_blank' title='Go To User Record'><img src='{3}' alt='' width='16' align='absmiddle'/></a>", 
                                                    ResolveUrl(UserView.PageAddress),
                                                    WebApplicationConstants.TransferNumber, 
                                                    Eval("UserId").GetString().UrlTextEncrypt(),
                                                    ResolveUrl("~/images/icons2/arrow_newTab.png"))
                                                : string.Empty  %>
                    </td>
                    <td class="text-right">
                        <%# Eval("AppliableAmount").ToDecimal().ToString("c2") %>
                    </td>
                    <td>
                        <%# Eval("PaymentDate").FormattedLongDate() %>
                    </td>
                    <td>
                        <%# Eval("GatewayTransactionId") %>
                    </td>
                    <td>
                        <%# Eval("PaymentGatewayType") %>
                    </td>
                    <td>
                        <%# Eval("Status") %>
                    </td>
                    <td class="text-right">
                        <eShip:CustomTextBox runat="server" ID="txtAmountToRefund" CssClass='<%# Eval("Status").ToString() == Voidable ? "w80 disabled" : "w80" %>' Type="FloatingPointNumbers" 
                            Text='<%# Eval("AppliableAmount").ToDecimal().ToString("f4") %>' ReadOnly='<%# Eval("Status").ToString() == Voidable %>'
                            Visible='<%# Eval("Status").ToString() != CannotVoidOrRefund %>' />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
    <table class="poptable">
        <tr>
            <td style="width: 75%" class="pt10 pb10"></td>
            <td class="pt10 pb10">
                <asp:Button runat="server" ID="btnRefundVoidMiscReceipts" Text="Process Refunds or Voids" OnClick="OnRefundVoidMiscReceiptsClicked" CausesValidation="False" />
                <asp:Button runat="server" ID="btnClosePaymentRefund" Text="Cancel" OnClick="OnClosePaymentRefundClicked" CausesValidation="False" />
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="pnlPaymentRefundDimScreen" CssClass="dimBackground" runat="server" Visible="false" />



<eShip:CustomHiddenField runat="server" ID="hidLoadOrderId" />
<eShip:CustomHiddenField runat="server" ID="hidShipmentId" />