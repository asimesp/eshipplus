﻿using System.Linq;
using LogisticsPlus.Eship.Core.Accounting;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls
{
    public partial class VendorInformationControlDetailControl : MemberControlBase
    {
        public void DisplayContacts(VendorLocation location)
        {
            var contacts = location.Contacts
                    .Select(c => new
                    {
                        c.Name,
                        c.Phone,
                        c.Email,
                        c.Primary,
                        type = c.ContactType.Description
                    })
                    .OrderByDescending(c => c.Primary)
                    .ToList();


            lstServices.DataSource = contacts;
            lstServices.DataBind();
        }
    }
}