﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PaymentProfileManagerControl.ascx.cs" Inherits="LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls.PaymentProfileManagerControl" %>
<asp:Panel runat="server" ID="pnlPaymentProfileManagerControl">
    <div class="row mb20">
        <div class="fieldgroup right">
            <asp:Button runat="server" ID="btnAddPaymentProfile" Text="Add Payment Profile" CausesValidation="False" OnClick="OnAddPaymentProfileClicked" Visible="False"/>
        </div>
    </div>
    <table class="stripe" id="paymentProfilesTable">
        <tr>
            <th style="width: 12%">Card Number
            </th>
            <th style="width: 10%">Expiration
            </th>
            <th style="width: 10%">First Name
            </th>
            <th style="width: 10%">Last Name
            </th>
            <th style="width: 16%">Billing Street
            </th>
            <th style="width: 10%">Billing City
            </th>
            <th style="width: 8%">Billing State
            </th>
            <th style="width: 8%">Billing Zip
            </th>
            <th style="width: 8%">Billing Country
            </th>
            <th style="width: 8%;" class="text-center">Action
            </th>
        </tr>
        <asp:Repeater runat="server" ID="rptPaymentProfiles">
            <ItemTemplate>
                <tr>
                    <td class="top">
                        <eShip:CustomHiddenField runat="server" ID="hidProfileId" Value='<%# Eval("PaymentProfileId") %>' />
                        <asp:Literal runat="server" ID="litPaymentProfileCreditCardNumber" Text='<%# Eval("CreditCardNumber") %>' />
                    </td>
                    <td class="top">
                        <asp:Literal runat="server" ID="litPaymentProfileExpirationDateYear" Text='<%# Eval("ExpirationDateYear") %>' />
                        -
                        <asp:Literal runat="server" ID="litPaymentProfileExpirationDateMonth" Text='<%# Eval("ExpirationDateMonth") %>' />
                    </td>
                    <td class="top">
                        <asp:Literal runat="server" ID="litPaymentProfileBillingFirstName" Text='<%# Eval("BillingFirstName") %>' />
                    </td>
                    <td class="top">
                        <asp:Literal runat="server" ID="litPaymentProfileBillingLastName" Text='<%# Eval("BillingLastName") %>' />
                    </td>
                    <td class="top">
                        <asp:Literal runat="server" ID="litPaymentProfileBillingStreet" Text='<%# Eval("BillingStreet") %>' />
                    </td>
                    <td class="top">
                        <asp:Literal runat="server" ID="litPaymentProfileBillingCity" Text='<%# Eval("BillingCity") %>' />
                    </td>
                    <td class="top">
                        <asp:Literal runat="server" ID="litPaymentProfileBillingState" Text='<%# Eval("BillingState") %>' />
                    </td>
                    <td class="top">
                        <asp:Literal runat="server" ID="litPaymentProfileBillingZip" Text='<%# Eval("BillingZip") %>' />
                    </td>
                    <td class="top">
                        <asp:Literal runat="server" ID="litPaymentProfileBillingCountry" Text='<%# Eval("BillingCountry") %>' />
                    </td>
                    <td class="top text-center">
                        <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/icons2/editBlue.png" CausesValidation="false" OnClick="OnEditPaymentProfileClicked" />
                        <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/icons2/deleteX.png" CausesValidation="false" OnClick="OnDeletePaymentProfileClicked" />
                        <ajax:ConfirmButtonExtender runat="server" ID="cbeDelete" TargetControlID="ibtnDelete" ConfirmText="Are you sure you want to delete payment profile?" />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
    <asp:Panel runat="server" ID="pnlModifyPaymentProfile" CssClass="popupControl popupControlOverW500 popup-position-fixed top50" Visible="False">
        <div class="popheader">
            <h4>Add/Modify Payment Profile</h4>
            <asp:ImageButton ID="ImageButton1" ImageUrl="~/images/icons2/close.png" CssClass="close" CausesValidation="false" OnClick="OnCloseEditPaymentProfileClicked" runat="server" />
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#<%= txtPaymentProfileBillingZip.ClientID %>').change(function () {
                    GetCityAndState();
                });

                $(jsHelper.AddHashTag('<%= ddlPaymentProfileBillingCountry.ClientID %>')).change(function () {
                    GetCityAndState();
                });

                function GetCityAndState() {
                    var ids = new ControlIds();
                    ids.City = $('#<%= txtPaymentProfileBillingCity.ClientID %>').attr('id');
                    ids.State = $('#<%= txtPaymentProfileBillingState.ClientID %>').attr('id');
                    FindCityAndState(
                        $('#<%= txtPaymentProfileBillingZip.ClientID %>').val(),
                                        $('#<%= ddlPaymentProfileBillingCountry.ClientID %>').val(),
                                        ids);
                }
            });
        </script>
        <table class="poptable">
            <tr>
                <td class="text-right pt10">
                    <label class="upper">Card Number:</label>
                </td>
                <td class="pt10">
                    <eShip:CustomHiddenField runat="server" ID="hidEditPaymentProfileId" />
                    <eShip:CustomTextBox runat="server" ID="txtPaymentProfileCardNumber" CssClass="w260" AutoCompleteType="Disabled" />
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    <label class="upper">Expiration Date:</label>
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlPaymentProfileCardExpirationMonth" CssClass="w60" DataTextField="Text" DataValueField="Value" />/
                                        <asp:DropDownList runat="server" ID="ddlPaymentProfileCardExpirationYear" DataTextField="Text" DataValueField="Value" CssClass="w80" />
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    <label class="upper">Security Code:</label>
                </td>
                <td>
                    <eShip:CustomTextBox runat="server" ID="txtPaymentProfileSecurityCode" CssClass="w60" AutoCompleteType="Disabled" />
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    <label class="upper">Billing First Name:</label>
                </td>
                <td>
                    <eShip:CustomTextBox runat="server" ID="txtPaymentProfileBillingFirstName" CssClass="w260" />
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    <label class="upper">Billing Last Name:</label>
                </td>
                <td>
                    <eShip:CustomTextBox runat="server" ID="txtPaymentProfileBillingLastName" CssClass="w260" />
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    <label class="upper">Billing Street:</label>
                </td>
                <td>
                    <eShip:CustomTextBox runat="server" ID="txtPaymentProfileBillingStreet" CssClass="w260" />
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    <label class="upper">Billing City:</label>
                </td>
                <td>
                    <eShip:CustomTextBox runat="server" ID="txtPaymentProfileBillingCity" CssClass="w260" />
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    <label class="upper">Billing State:</label>
                </td>
                <td>
                    <eShip:CustomTextBox runat="server" ID="txtPaymentProfileBillingState" CssClass="w260" />
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    <label class="upper">Billing Postal Code:</label>
                </td>
                <td>
                    <eShip:CustomTextBox runat="server" ID="txtPaymentProfileBillingZip" CssClass="w260" />
                </td>
            </tr>
            <tr>
                <td class="text-right">
                    <label class="upper">Billing Country:</label>
                </td>
                <td>
                    <eShip:CachedObjectDropDownList Type="Countries" ID="ddlPaymentProfileBillingCountry" runat="server" CssClass="w260" EnableChooseOne="True" DefaultValue="0" />
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Button ID="btnEditPaymentProfileDone" Text="Done" OnClick="OnEditPaymentProfileDoneClicked" runat="server" CausesValidation="false" />
                    <asp:Button ID="btnEditPaymentProfileCancel" Text="Cancel" OnClick="OnCloseEditPaymentProfileClicked" runat="server" CausesValidation="false" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlPaymentProfileManagerControlDimScreen" CssClass="dimBackgroundControl" runat="server" Visible="false" />
</asp:Panel>
<eShip:CustomHiddenField runat="server" ID="hidFlag" />
<eShip:CustomHiddenField runat="server" ID="hidCustomerId" />
