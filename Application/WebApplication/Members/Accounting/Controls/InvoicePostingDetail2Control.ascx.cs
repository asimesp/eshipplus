﻿using System;
using System.Web.UI;
using LogisticsPlus.Eship.Core;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Searches.Administration;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.WebApplication.Members.Administration;
using LogisticsPlus.Eship.WebApplication.Members.Operations;
using LogisticsPlus.Eship.WebApplication.Utilities;
using LogisticsPlus.Eship.WebApplication.ViewCodes;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls
{
    public partial class InvoicePostingDetail2Control : MemberControlBase
    {
        public bool Selected { get { return chkSelection.Checked; } }

        public int ItemIndex
        {
            get { return hidItemIndex.Value.ToInt(); }
            set { hidItemIndex.Value = value.ToString(); }
        }

        public bool EnableMultiSelect { get; set; }


        public event EventHandler<ViewEventArgs<long>> GenerateInvoice;


        public void LoadInvoice(InvoiceDashboardDto invoice)
        {
            chkSelection.Enabled = invoice.AmountDue > 0;
            hidInvoiceId.Value = invoice.Id.ToString();

            lnkInvoiceNumber.Text = invoice.InvoiceNumber;
            lnkInvoiceNumber.Visible = invoice.Posted;
            litInvoiceNumber.Text = invoice.InvoiceNumber;
            litInvoiceNumber.Visible = !invoice.Posted;

            litCustomer.Text = string.Format("{0} - {1}", invoice.CustomerNumber, invoice.CustomerName);
            hypCustomer.NavigateUrl = string.Format("{0}?{1}={2}", CustomerView.PageAddress, WebApplicationConstants.TransferNumber, invoice.CustomerId.GetString().UrlTextEncrypt());
            litInvoiceDate.Text = invoice.InvoiceDate.FormattedShortDate();

            litAddress.Text = string.Format("{0}{1}{2}{3}{4}, {5} {6} {7}",
                                            invoice.LocationStreet1,
                                            WebApplicationConstants.HtmlBreak, invoice.LocationStreet2,
                                            string.IsNullOrEmpty(invoice.LocationStreet2)
                                                ? string.Empty
                                                : WebApplicationConstants.HtmlBreak,
                                            invoice.LocationCity, invoice.LocationState, invoice.LocationPostalCode,
                                            invoice.LocationCountryName);
            litType.Text = invoice.InvoiceType.ToString();
            litPrefix.Text = invoice.PrefixCode;

            var hasAccessToUser = ActiveUser.HasAccessTo(ViewCode.User);
            litUser.Text = invoice.Username;
            if (hasAccessToUser)
            {
                var user = new UserSearch().FetchUserByUsernameAndAccessCode(invoice.Username, ActiveUser.Tenant.Code);
                hypUser.Visible = true;
                hypUser.NavigateUrl = string.Format("{0}?{1}={2}", UserView.PageAddress, WebApplicationConstants.TransferNumber, user.Id.GetString().UrlTextEncrypt());
            }
            
            litAmountDue.Text = invoice.AmountDue.ToString("c2");
            litInstructions.Text = invoice.SpecialInstruction == string.Empty
                                       ? string.Empty
                                       : string.Format("<span class='blue'><abbr title='Special Instructions'>I</abbr>:</span> {0}", invoice.SpecialInstruction);

            var httpAddr = Request.ResolveSiteRootWithHttp();
            litShipments.Text = ActiveUser.HasAccessTo(ViewCode.Shipment)
                                    ? ShipmentView.BuildShipmentLinks(invoice.Shipments).Replace("~", httpAddr)
                                    : invoice.Shipments;
            litServiceTickets.Text = ActiveUser.HasAccessTo(ViewCode.ServiceTicket)
                                         ? ServiceTicketView.BuildServiceTicketLinks(invoice.ServiceTickets).Replace("~", httpAddr)
                                         : invoice.ServiceTickets;

            hypInvoiceView.NavigateUrl = string.Format("{0}?{1}={2}", InvoiceView.PageAddress, WebApplicationConstants.TransferNumber, invoice.InvoiceNumber);
        }


        protected void OnInvoiceNumberClicked(object sender, EventArgs e)
        {
            if (GenerateInvoice != null)
                GenerateInvoice(this, new ViewEventArgs<long>(hidInvoiceId.Value.ToLong()));
        }
    }
}