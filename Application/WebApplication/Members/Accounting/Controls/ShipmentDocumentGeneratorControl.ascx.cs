﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.Operations;
using LogisticsPlus.Eship.Processor.Searches.Operations;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls
{
    public partial class ShipmentDocumentGeneratorControl : MemberControlBase
    {
        public new bool Visible
        {
            get { return pnlShipmentDocumentGenerator.Visible; }
            set
            {
                base.Visible = value;
                pnlShipmentDocumentGenerator.Visible = value;
                pnlShipmentDocumentGeneratorDimScreen.Visible = value;
            }
        }

        public event EventHandler Close;

        protected void OnCloseClicked(object sender, ImageClickEventArgs e)
        {
            if (Close != null)
                Close(this, new EventArgs());
        }


        public void GenerateDocuments(long invoiceId)
        {
            var documents = new List<ShipmentDocument>();

            var invoice = new Invoice(invoiceId);
            if (!invoice.KeyLoaded)
            {
                lblInvoiceNumber.Text = "Unknown";
                rptDocuments.DataSource = new List<object>();
                rptDocuments.DataBind();
            }
            else
            {
                lblInvoiceNumber.Text = invoice.InvoiceNumber;

                var details = invoice.Details.Where(d => d.ReferenceType == DetailReferenceType.Shipment).Select(d => new
                {
                    d.ReferenceNumber
                }).Distinct().ToList();

                foreach (var shipment in details.Select(validReferenceNumber => new ShipmentSearch().FetchShipmentByShipmentNumber(validReferenceNumber.ReferenceNumber, ActiveUser.TenantId)).Where(shipment => shipment != null))
                {
                    shipment.LoadCollections();
                    documents.AddRange(shipment.Documents.Where(d => !d.IsNew && !d.IsInternal).ToList());
                }

                var dataSource =
                    !documents.Any()
                        ? null
                        : documents.Select(d => new
                        {
                            Text = string.Format("{0}_{1}", d.Shipment.ShipmentNumber, d.Name),
                            Url = DocumentViewer.GenerateShipmentDocumentLink(d),
                        });

                rptDocuments.DataSource = dataSource;
                rptDocuments.DataBind();
            }
        }
    }
}