﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.BusinessIntelligence;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls
{
    public partial class VendorInformationControl : MemberControlBase
    {
        public new bool Visible
        {
            get { return pnlLocation.Visible; }
            set
            {
                base.Visible = value;
                pnlLocation.Visible = value;
                pnlFinderDimScreen.Visible = value;
            }
        }

        public event EventHandler Cancel;

        public void DisplayVendorInformation(long vendorId, string originPostalCode, long originCountryId,
            string destinationPostalCode, long destinationCountryId, DateTime date, double radius)
        {
            var vendor = new Vendor(vendorId);
            litVendorName.Text = string.Format("{0} - Information", vendor.Name);

            //locations tab
            lstLocations.DataSource = vendor.Locations;
            lstLocations.DataBind();

            //statistics tab
            var statistics = new VendorStatisticsDto().RetrieveVendorStatistics(vendorId, ActiveUser.TenantId);

            litLTLCount.Text = statistics.LTLCount.ToString();
            litLTLLatePickupCount.Text = statistics.LTLLatePickup;
            litLTLLateDeliveryCount.Text = statistics.LTLLateDelivery;
            litLTLConsideredCount.Text = statistics.LTLFinalConsideredCount.ToString();

            litTLCount.Text = statistics.TLCount.ToString();
            litTLLatePickupCount.Text = statistics.TLLatePickup;
            litTLLateDeliveryCount.Text = statistics.TLLateDelivery;
            litTLConsideredCount.Text = statistics.TLFinalConsideredCount.ToString();

            litAirCount.Text = statistics.AirCount.ToString();
            litAirLatePickupCount.Text = statistics.AirLatePickup;
            litAirLateDeliveryCount.Text = statistics.AirLateDelivery;
            litAirConsideredCount.Text = statistics.AirFinalConsideredCount.ToString();

            litRailCount.Text = statistics.RailCount.ToString();
            litRailLatePickupCount.Text = statistics.RailLatePickup;
            litRailLateDeliveryCount.Text = statistics.RailLateDelivery;
            litRailConsideredCount.Text = statistics.RailFinalConsideredCount.ToString();

            litSmallPackCount.Text = statistics.SmallPackageCount.ToString();
            litSmallPackLatePickupCount.Text = statistics.SmallPackageLatePickup;
            litSmallPackLateDeliveryCount.Text = statistics.SmallPackageLateDelivery;
            litSmallPackConsideredCount.Text = statistics.SmallPackageFinalConsideredCount.ToString();

            //chart tab
            var data = new VendorStatisticsDto()
                        .RetrieveVendorCostData(vendorId, ActiveUser.TenantId, originPostalCode, originCountryId, destinationPostalCode, destinationCountryId, date, radius);

            if (data[0].Any() && data[1].Any())
            {
                pnlChartContainer.Attributes["style"] = "display: block;";
                pnlNoChart.Visible = false;
                pnlChartDataContainer.Visible = true;
                var xySeries = new XYSeries
                {
                    X = data[1],
                    Y = new List<SeriesYAxis> { new SeriesYAxis("Monthly Cost", data[0], SeriesChartType.Line, 0, Color.Empty) }
                };

                chartGenerator.Title = "Vendor Average Cost by Month";

                chartGenerator.XAxisTitle = "Year - Month";
                chartGenerator.YAxisTitle = "Average Cost";

                chartGenerator.DataSource = xySeries;
                chartGenerator.DataBind();

                var yearMonths = data[1].Select(c => new { YearMonth = c }).ToList();
                var averageCosts = data[0].Select(c => new { AverageCost = c.ToDecimal().ToString("c2") }).ToList();

                lstChartDataYearMonth.DataSource = yearMonths;
                lstChartDataYearMonth.DataBind();

                lstChartDataAverageCost.DataSource = averageCosts;
                lstChartDataAverageCost.DataBind();
            }
            else
            {
                pnlChartContainer.Attributes["style"] = "display: none;";
                pnlNoChart.Visible = true;
                pnlChartDataContainer.Visible = false;
            }
        }

        protected void OnVendorLocationsDataBound(object sender, ListViewItemEventArgs e)
        {
            var item = (ListViewDataItem)e.Item;
            var vl = (VendorLocation)item.DataItem;

            if (vl == null) return;

            var detailControl = (VendorInformationControlDetailControl)item.FindControl("vendorDetail");

            detailControl.DisplayContacts(vl);
        }

        protected void Page_Load(object sender, EventArgs e) { }

        protected void OnCancelClicked(object sender, EventArgs eventArgs)
        {
            if (Cancel != null)
                Cancel(this, new EventArgs());
        }
    }
}