﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Dto.Accounting;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls
{
    public partial class ApplyMiscReceiptsToInvoiceControl : MemberControlBase, IApplyMiscReceiptToInvoiceView
    {
        public new bool Visible
        {
            get { return pnlApplyMiscReceiptInvoiceToInvoice.Visible; }
            set
            {
                base.Visible = true;
                pnlApplyMiscReceiptInvoiceToInvoice.Visible = value;
                pnlApplyMiscReceiptInvoiceToInvoiceDimScreen.Visible = value;
            }
        }


        public event EventHandler Close;
        public event EventHandler<ViewEventArgs<List<ValidationMessage>>> DisplayProcessingMessages;
        public event EventHandler<ViewEventArgs<Invoice, List<MiscReceiptApplication>>> SaveMiscReceiptApplicationsAndInvoice;
        public event EventHandler<ViewEventArgs<Invoice>> LoadAndLockMiscReceiptsApplicableToPostedInvoice;
        public event EventHandler<ViewEventArgs<Invoice, List<MiscReceipt>>> UnlockInvoiceAndMiscReceipts;


        public void LoadInvoiceAndRelatedMiscReceipts(long invoiceId)
        {
            hidInvoiceId.Value = invoiceId.GetString();
            if(LoadAndLockMiscReceiptsApplicableToPostedInvoice != null)
                LoadAndLockMiscReceiptsApplicableToPostedInvoice(this, new ViewEventArgs<Invoice>(new Invoice(invoiceId)));
        }

        public void LogException(Exception ex)
        {
            if (ex != null) ErrorLogger.LogError(ex, Context);
        }

        
        public void FailedMiscReceiptLocks()
        {
            Visible = false;
        }


        public void DisplayMiscReceipts(List<MiscReceiptViewSearchDto> miscReceipts)
        {
            rptMiscReceipts.DataSource = miscReceipts;
            rptMiscReceipts.DataBind();
        }
        
        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (DisplayProcessingMessages != null)
                DisplayProcessingMessages(this, new ViewEventArgs<List<ValidationMessage>>(messages.ToList()));
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new ApplyMiscReceiptsToInvoiceHandler(this).Initialize();
        }


        protected void OnCloseApplyMiscReceiptToInvoiceClicked(object sender, EventArgs e)
        {
            var miscReceipts = rptMiscReceipts
                .Items
                .Cast<RepeaterItem>()
                .Select(i => new MiscReceipt(i.FindControl("hidMiscReceiptId").ToCustomHiddenField().Value.ToLong()))
                .ToList();

            if (UnlockInvoiceAndMiscReceipts != null)
                UnlockInvoiceAndMiscReceipts(this, new ViewEventArgs<Invoice, List<MiscReceipt>>(new Invoice(hidInvoiceId.Value.ToLong()), miscReceipts));

            if (Close != null)
                Close(this, new EventArgs());
        }

        protected void OnApplyMiscReceiptsToInvoiceClicked(object sender, EventArgs e)
        {
            var miscReceiptApplications = rptMiscReceipts
                .Items
                .Cast<RepeaterItem>()
                .Where(i => i.FindControl("chkSelected").ToAltUniformCheckBox().Checked && i.FindControl("txtAmountToApplyFromMiscReceipt").ToTextBox().Text.ToDecimal() > 0)
                .Select(i => new MiscReceiptApplication
                {
                    MiscReceiptId = i.FindControl("hidMiscReceiptId").ToCustomHiddenField().Value.ToLong(),
                    Amount = i.FindControl("txtAmountToApplyFromMiscReceipt").ToTextBox().Text.ToDecimal(),
                    ApplicationDate = DateTime.Now,
                    InvoiceId = hidInvoiceId.Value.ToLong(),
                    TenantId = ActiveUser.TenantId,
                })
                .ToList();

            if (!miscReceiptApplications.Any())
            {
                if (DisplayProcessingMessages != null)
                    DisplayProcessingMessages(this, new ViewEventArgs<List<ValidationMessage>>(new List<ValidationMessage> { ValidationMessage.Error("No misc receipts have been selected with an amount to apply greater than zero for this invoice.") }));
                return;
            }

            var invoice = new Invoice(hidInvoiceId.Value.ToLong(), true);
            invoice.LoadCollections();
            invoice.PaidAmount += miscReceiptApplications.Sum(m => m.Amount);

            if (SaveMiscReceiptApplicationsAndInvoice != null)
                SaveMiscReceiptApplicationsAndInvoice(this, new ViewEventArgs<Invoice, List<MiscReceiptApplication>>(invoice, miscReceiptApplications));
        }
    }
}