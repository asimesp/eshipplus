﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LogisticsPlus.Eship.Core.Accounting;
using LogisticsPlus.Eship.Core.BusinessIntelligence;
using LogisticsPlus.Eship.Processor;
using LogisticsPlus.Eship.Processor.Handlers.Accounting;
using LogisticsPlus.Eship.Processor.Searches;
using LogisticsPlus.Eship.Processor.Searches.Accounting;
using LogisticsPlus.Eship.Processor.Validation;
using LogisticsPlus.Eship.Processor.Views;
using LogisticsPlus.Eship.Processor.Views.Accounting;
using LogisticsPlus.Eship.WebApplication.Members.Administration;
using LogisticsPlus.Eship.WebApplication.Members.BusinessIntelligence.Controls;
using LogisticsPlus.Eship.WebApplication.Members.Controls;
using LogisticsPlus.Eship.WebApplication.Utilities;

namespace LogisticsPlus.Eship.WebApplication.Members.Accounting.Controls
{
    public partial class CustomerControlAccountFinderControl : MemberControlBase, ICustomerControlAccountFinderView
    {
        public new bool Visible
        {
            get { return pnlCustomerControlAccountFinderContent.Visible; }
            set
            {
                base.Visible = true;
                pnlCustomerControlAccountFinderContent.Visible = value;
                pnlCustomerControlAccountFinderDimScreen.Visible = value;
            }
        }

        public long CustomerIdFilter
        {
            set { hidCustomerId.Value = value.ToString(); }
        }

        public event EventHandler Loading;
        public event EventHandler<ViewEventArgs<CustomerControlAccountSearchCriteria>> Search;
        public event EventHandler<ViewEventArgs<CustomerControlAccount>> ItemSelected;
        public event EventHandler SelectionCancel;

        public void DisplaySearchResult(List<CustomerControlAccount> customerControlAccounts)
        {
            litRecordCount.Text = customerControlAccounts.BuildRecordCount();
            upcseDataUpdate.DataSource = customerControlAccounts;
            upcseDataUpdate.DataBind();
        }

        private UserSearchProfileControl.ProfileItems GetSearchProfileItems()
        {
            return new UserSearchProfileControl.ProfileItems { Columns = GetCurrentRunParameters(true) };
        }

        public void DisplayMessages(IEnumerable<ValidationMessage> messages)
        {
            if (messages == null) return;
            if (!messages.Any()) return;

            messageBox.Icon = messages.GenerateIcon();
            messageBox.Message = string.Join(WebApplicationConstants.HtmlBreak, messages.OrderBy(m => m.Type).Select(m => m.Message).ToArray());
            messageBox.Visible = true;
        }

        public void Reset()
        {
            hidAreFiltersShowing.Value = true.ToString(); 
            DisplaySearchResult(new List<CustomerControlAccount>());
        }


        private void DoSearchPreProcessingThenSearch()
        {
            DoSearch(new CustomerControlAccountSearchCriteria
            {
                Parameters = GetCurrentRunParameters(false),
                CustomerId = hidCustomerId.Value.ToLong()
            });
        }

        private void DoSearch(CustomerControlAccountSearchCriteria criteria)
        {

            if (Search != null)
                Search(this, new ViewEventArgs<CustomerControlAccountSearchCriteria>(criteria));
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            new CustomerControlAccountFinderHandler(this).Initialize();

            searchProfiles.GetProfileItems = GetSearchProfileItems;

            if (IsPostBack) return;

            hypGoToUserProfile.NavigateUrl = UserProfileView.PageAddress;
            var profile = searchProfiles.RetrieveDefaultProfileForTypeSet(true);
			searchProfiles.SetSelectedProfile(profile);

            var columns = profile != null && profile.Columns != null
                            ? profile.Columns
                            : AccountingSearchFields.DefaultCustomerControlAccounts.Select(f => f.ToParameterColumn()).ToList();

            lstFilterParameters.DataSource = columns;
            lstFilterParameters.DataBind();
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            hidAreFiltersShowing.Value = ActiveUser.AlwaysShowFinderParametersOnSearch.ToString();
            upcseDataUpdate.ResetLoadCount();
            DoSearchPreProcessingThenSearch();
        }

        protected void OnCancelClicked(object sender, EventArgs eventArgs)
        {
            if (SelectionCancel != null)
                SelectionCancel(this, new EventArgs());
        }

        protected void OnSelectClicked(object sender, EventArgs e)
        {
            var button = (Button)sender;
            if (ItemSelected != null)
            {
                var id = button.Parent.FindControl("customerControlAccountId").ToCustomHiddenField().Value.ToLong();
                ItemSelected(this, new ViewEventArgs<CustomerControlAccount>(new CustomerControlAccount(id, false)));
            }
        }


        private List<ParameterColumn> GetCurrentRunParameters(bool preserveKeywordDate)
        {
            var currentRunParameters = lstFilterParameters.Items
                .Select(i => ((ReportRunParameterControl)i.FindControl("reportRunParameterControl")).RetrieveParameter(preserveKeywordDate)).ToList();
            return currentRunParameters;
        }

        protected void OnParameterItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType != ListViewItemType.DataItem) return;

            var item = (ListViewDataItem)e.Item;
            var parameter = (ParameterColumn)item.DataItem;

            if (parameter == null) return;

            var field = AccountingSearchFields.CustomerControlAccounts.FirstOrDefault(f => f.DisplayName == parameter.ReportColumnName);

            if (field == null) return;

            var detailControl = (ReportRunParameterControl)item.FindControl("reportRunParameterControl");

            detailControl.LoadParameter(parameter, field.DataType, true);
        }

        protected void OnParameterRemove(object sender, ViewEventArgs<int> e)
        {
            var parameters = GetCurrentRunParameters(true);

            parameters.RemoveAt(e.Argument);

            lstFilterParameters.DataSource = parameters;
            lstFilterParameters.DataBind();

            Reset();
        }


        protected void OnParameterAddClicked(object sender, EventArgs e)
        {
            parameterSelector.LoadParameterSelector(AccountingSearchFields.CustomerControlAccounts);
            parameterSelector.Visible = true;
        }

        protected void OnParameterSelectorAdd(object sender, ViewEventArgs<List<ParameterColumn>> e)
        {
            var currentRunParameters = GetCurrentRunParameters(true);
            currentRunParameters.AddRange(e.Argument);

            lstFilterParameters.DataSource = currentRunParameters;
            lstFilterParameters.DataBind();

            parameterSelector.Visible = false;

            Reset();
        }

        protected void OnParameterSelectorClose(object sender, EventArgs e)
        {
            parameterSelector.Visible = false;
        }


        protected void OnSearchProfilesProcessingError(object sender, ViewEventArgs<string> e)
        {
            DisplayMessages(new[] { ValidationMessage.Error(e.Argument) });
        }

        protected void OnSearchProfilesProfileSelected(object sender, ViewEventArgs<SearchDefaultsItem> e)
        {
            lstFilterParameters.DataSource = e.Argument.Columns;
            lstFilterParameters.DataBind();
        }

    }
}